/* $Id:$ */
package ru.tandemservice.uniedu.settings.bo.EduProgramSubjectComparison.ui.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes;
import ru.tandemservice.uniedu.entity.EduProgramSubjectComparison;

import java.util.Arrays;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 13.03.2015
 */
@Configuration
public class EduProgramSubjectComparisonList extends BusinessComponentManager
{
    public static final String OLD_INDEXES = "oldIndexes";
    public static final String NEW_INDEXES = "newIndexes";
    public static final String NEW_SUBJECTS = "newSubjects";
    public static final String SUBJECT_LIST_DS = "subjectListDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(OLD_INDEXES, oldIndexesHandler()))
                .addDataSource(selectDS(NEW_INDEXES, newIndexesHandler()))
                .addDataSource(selectDS(NEW_SUBJECTS, newSubjectsHandler()).addColumn("titleWithCode", EduProgramSubject2013.P_TITLE_WITH_CODE))
                .addDataSource(searchListDS(SUBJECT_LIST_DS, subjectListDSColumns(), subjectListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint subjectListDSColumns()
    {
        return columnListExtPointBuilder(SUBJECT_LIST_DS)
                .addColumn(blockColumn("title", "titleBlock"))
                .addColumn(blockColumn("comparison", "comparisonBlock"))
                .addColumn(blockColumn("action", "actionBlock").width("10").hint("Редактирование"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> oldIndexesHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), EduProgramSubjectIndex.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);
                List<String> indexCodes = Arrays.asList(EduProgramSubjectIndexCodes.TITLE_2005_62,
                        EduProgramSubjectIndexCodes.TITLE_2005_65,
                        EduProgramSubjectIndexCodes.TITLE_2005_68,
                        EduProgramSubjectIndexCodes.TITLE_2009_62,
                        EduProgramSubjectIndexCodes.TITLE_2009_65,
                        EduProgramSubjectIndexCodes.TITLE_2009_68);
                dql.where(in(property(alias, EduProgramSubjectIndex.code()), indexCodes));
            }
        };
        handler.order(EduProgramSubjectIndex.priority());
        handler.filter(EduProgramSubjectIndex.title());
        return handler;
    }


    @Bean
    public IBusinessHandler<DSInput, DSOutput> newIndexesHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), EduProgramSubjectIndex.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);
                List<String> indexCodes = Arrays.asList(EduProgramSubjectIndexCodes.TITLE_2013_03,
                        EduProgramSubjectIndexCodes.TITLE_2013_04,
                        EduProgramSubjectIndexCodes.TITLE_2013_05);
                dql.where(in(property(alias, EduProgramSubjectIndex.code()), indexCodes));
            }
        };
        handler.order(EduProgramSubjectIndex.priority());
        handler.filter(EduProgramSubjectIndex.title());
        return handler;
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> subjectListDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {

                EduProgramSubjectIndex oldIndex = context.get(EduProgramSubjectComparisonListUI.PARAM_OLD_INDEX);
                String title = context.get(EduProgramSubjectComparisonListUI.PARAM_TITLE);

                String alias = "alias";

                final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EduProgramSubject.class, alias)
                        .where(eq(property(alias, EduProgramSubject.subjectIndex()), value(oldIndex)))
                        .joinEntity(alias, DQLJoinType.left, EduProgramSubjectComparison.class, "c",
                                    eq(property("c", EduProgramSubjectComparison.oldProgramSubject()), property(alias)))
                        .column(property(alias))
                        .column(property("c"))
                        .order(property(alias, EduProgramSubject.subjectCode()))
                        .order(property(alias, EduProgramSubject.title()));

                if (StringUtils.isNotEmpty(title))
                {
                    builder.where(or(
                            likeUpper(property(alias, EduProgramSubject.subjectCode()), value(CoreStringUtils.escapeLike(title))),
                            likeUpper(property(alias, EduProgramSubject.title()), value(CoreStringUtils.escapeLike(title)))
                    ));
                }

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build()
                        .transform((Object[] row) -> new SubjectComparisonWrapper(((EduProgramSubject) row[0]), ((EduProgramSubjectComparison) row[1])));
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> newSubjectsHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject2013.class)
                .filter(EduProgramSubject2013.title())
                .filter(EduProgramSubject2013.code())
                .order(EduProgramSubject2013.code())
                .order(EduProgramSubject2013.title())
                .where(EduProgramSubject2013.subjectIndex().id(), EduProgramSubjectComparisonListUI.PARAM_NEW_INDEX);
    }
}
