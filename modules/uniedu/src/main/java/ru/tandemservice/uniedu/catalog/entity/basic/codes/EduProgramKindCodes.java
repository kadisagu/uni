package ru.tandemservice.uniedu.catalog.entity.basic.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вид образовательной программы"
 * Имя сущности : eduProgramKind
 * Файл data.xml : uniedu.basic.data.xml
 */
public interface EduProgramKindCodes
{
    /** Константа кода (code) элемента : Программа подготовки квалифицированных рабочих (служащих) (title) */
    String PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_ = "2013.2.1.1";
    /** Константа кода (code) элемента : Программа подготовки специалистов среднего звена (title) */
    String PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA = "2013.2.1.2";
    /** Константа кода (code) элемента : Программа бакалавриата (title) */
    String PROGRAMMA_BAKALAVRIATA = "2013.2.2.1";
    /** Константа кода (code) элемента : Программа подготовки специалистов (title) */
    String PROGRAMMA_PODGOTOVKI_SPETSIALISTOV = "2013.2.3.1";
    /** Константа кода (code) элемента : Программа магистратуры (title) */
    String PROGRAMMA_MAGISTRATURY = "2013.2.3.2";
    /** Константа кода (code) элемента : Программа аспирантуры (адъюнктуры) (title) */
    String PROGRAMMA_ASPIRANTURY_ADYUNKTURY_ = "2013.2.4.1";
    /** Константа кода (code) элемента : Программа ординатуры (title) */
    String PROGRAMMA_ORDINATURY = "2013.2.4.2";
    /** Константа кода (code) элемента : Программа интернатуры (title) */
    String PROGRAMMA_INTERNATURY = "2013.2.4.3";
    /** Константа кода (code) элемента : Дополнительная профессиональная программа (title) */
    String DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA = "2013.3.2.1";

    Set<String> CODES = ImmutableSet.of(PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_, PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA, PROGRAMMA_BAKALAVRIATA, PROGRAMMA_PODGOTOVKI_SPETSIALISTOV, PROGRAMMA_MAGISTRATURY, PROGRAMMA_ASPIRANTURY_ADYUNKTURY_, PROGRAMMA_ORDINATURY, PROGRAMMA_INTERNATURY, DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA);
}
