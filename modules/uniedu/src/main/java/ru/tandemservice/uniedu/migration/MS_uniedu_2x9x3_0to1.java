package ru.tandemservice.uniedu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniedu_2x9x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eduTutorOrgUnit

		// сущность была удалена
		{

			final long count = tool.getNumericResult("select count(*) from edu_ourole__tutor_t");

			if(count > 0){
				throw new IllegalStateException("Try to drop table: table edu_ourole__tutor_t is not empty.");
			}

			// удалить таблицу
			tool.dropTable("edu_ourole__tutor_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("eduTutorOrgUnit");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eduGroupOrgUnit

		// сущность была удалена
		{
			final long count = tool.getNumericResult("select count(*) from edu_ourole__group_t");

			if(count > 0){
				throw new IllegalStateException("Try to drop table: table edu_ourole__group_t is not empty.");
			}

			// удалить таблицу
			tool.dropTable("edu_ourole__group_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("eduGroupOrgUnit");

		}


    }
}