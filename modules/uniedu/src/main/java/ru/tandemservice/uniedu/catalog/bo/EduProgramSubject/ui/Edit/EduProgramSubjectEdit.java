/* $Id$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.ui.Edit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author azhebko
 * @since 27.06.2014
 */
@Configuration
public class EduProgramSubjectEdit extends BusinessComponentManager
{
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .create();
    }
}