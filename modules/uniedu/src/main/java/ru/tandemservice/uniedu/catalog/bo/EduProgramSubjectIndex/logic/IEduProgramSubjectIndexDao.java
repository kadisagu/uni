package ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

/**
 * @author vdanilov
 */
public interface IEduProgramSubjectIndexDao extends INeedPersistenceSupport {

    /**
     * Удаляет квалификации, не связанные с направлениями, если на них нет ссылок.
     * Удаляет направления, которые помечены как неактуальные, если на них нет ссылок.
     * Для общих направленностей обновляются названия из направлений.
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doCleanUpSubjectIndexData();

    /** Обновляет сокр. название направлений подготовки, для которых оно не было задано. */
    void updateNewSubjectsShortTitles();
}
