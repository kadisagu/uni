package ru.tandemservice.uniedu.catalog.entity.subjects.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Квалификация профессионального образования
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramQualificationGen extends EntityBase
 implements INaturalIdentifiable<EduProgramQualificationGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification";
    public static final String ENTITY_NAME = "eduProgramQualification";
    public static final int VERSION_HASH = -689376411;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_QUALIFICATION_LEVEL_CODE = "qualificationLevelCode";
    public static final String L_SUBJECT_INDEX = "subjectIndex";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _qualificationLevelCode;     // Код уровня квалификации
    private EduProgramSubjectIndex _subjectIndex;     // Перечень
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * Код уровня квалификации (в рамках перечня одинаковое, за исключением СПО).
     * Используется только в интерфейсе выбора квалификации при создании ОП.
     *
     * @return Код уровня квалификации.
     */
    @Length(max=255)
    public String getQualificationLevelCode()
    {
        return _qualificationLevelCode;
    }

    /**
     * @param qualificationLevelCode Код уровня квалификации.
     */
    public void setQualificationLevelCode(String qualificationLevelCode)
    {
        dirty(_qualificationLevelCode, qualificationLevelCode);
        _qualificationLevelCode = qualificationLevelCode;
    }

    /**
     * Определяет принадлежность квалификации перечню
     *
     * @return Перечень. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubjectIndex getSubjectIndex()
    {
        return _subjectIndex;
    }

    /**
     * @param subjectIndex Перечень. Свойство не может быть null.
     */
    public void setSubjectIndex(EduProgramSubjectIndex subjectIndex)
    {
        dirty(_subjectIndex, subjectIndex);
        _subjectIndex = subjectIndex;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EduProgramQualificationGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EduProgramQualification)another).getCode());
            }
            setQualificationLevelCode(((EduProgramQualification)another).getQualificationLevelCode());
            setSubjectIndex(((EduProgramQualification)another).getSubjectIndex());
            setTitle(((EduProgramQualification)another).getTitle());
        }
    }

    public INaturalId<EduProgramQualificationGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EduProgramQualificationGen>
    {
        private static final String PROXY_NAME = "EduProgramQualificationNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EduProgramQualificationGen.NaturalId) ) return false;

            EduProgramQualificationGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramQualificationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramQualification.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramQualification();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "qualificationLevelCode":
                    return obj.getQualificationLevelCode();
                case "subjectIndex":
                    return obj.getSubjectIndex();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "qualificationLevelCode":
                    obj.setQualificationLevelCode((String) value);
                    return;
                case "subjectIndex":
                    obj.setSubjectIndex((EduProgramSubjectIndex) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "qualificationLevelCode":
                        return true;
                case "subjectIndex":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "qualificationLevelCode":
                    return true;
                case "subjectIndex":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "qualificationLevelCode":
                    return String.class;
                case "subjectIndex":
                    return EduProgramSubjectIndex.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgramQualification> _dslPath = new Path<EduProgramQualification>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramQualification");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * Код уровня квалификации (в рамках перечня одинаковое, за исключением СПО).
     * Используется только в интерфейсе выбора квалификации при создании ОП.
     *
     * @return Код уровня квалификации.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification#getQualificationLevelCode()
     */
    public static PropertyPath<String> qualificationLevelCode()
    {
        return _dslPath.qualificationLevelCode();
    }

    /**
     * Определяет принадлежность квалификации перечню
     *
     * @return Перечень. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification#getSubjectIndex()
     */
    public static EduProgramSubjectIndex.Path<EduProgramSubjectIndex> subjectIndex()
    {
        return _dslPath.subjectIndex();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EduProgramQualification> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _qualificationLevelCode;
        private EduProgramSubjectIndex.Path<EduProgramSubjectIndex> _subjectIndex;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EduProgramQualificationGen.P_CODE, this);
            return _code;
        }

    /**
     * Код уровня квалификации (в рамках перечня одинаковое, за исключением СПО).
     * Используется только в интерфейсе выбора квалификации при создании ОП.
     *
     * @return Код уровня квалификации.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification#getQualificationLevelCode()
     */
        public PropertyPath<String> qualificationLevelCode()
        {
            if(_qualificationLevelCode == null )
                _qualificationLevelCode = new PropertyPath<String>(EduProgramQualificationGen.P_QUALIFICATION_LEVEL_CODE, this);
            return _qualificationLevelCode;
        }

    /**
     * Определяет принадлежность квалификации перечню
     *
     * @return Перечень. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification#getSubjectIndex()
     */
        public EduProgramSubjectIndex.Path<EduProgramSubjectIndex> subjectIndex()
        {
            if(_subjectIndex == null )
                _subjectIndex = new EduProgramSubjectIndex.Path<EduProgramSubjectIndex>(L_SUBJECT_INDEX, this);
            return _subjectIndex;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EduProgramQualificationGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EduProgramQualification.class;
        }

        public String getEntityName()
        {
            return "eduProgramQualification";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
