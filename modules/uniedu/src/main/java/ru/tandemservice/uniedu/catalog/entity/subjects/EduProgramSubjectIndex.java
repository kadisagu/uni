package ru.tandemservice.uniedu.catalog.entity.subjects;

import com.google.common.collect.ImmutableList;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.gen.EduProgramSubjectIndexGen;

import java.util.List;

/**
 * Перечень направлений подготовки профессионального образования
 *
 * Определяет набор направлений одного вида ОП.
 * Формируется из приказа, воодящего перечень, и уточняющих его документов: выбирается только те части документов, которые относятся к указанному виду ОП.
 */
@SuppressWarnings("unused")
public class EduProgramSubjectIndex extends EduProgramSubjectIndexGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{
    /** @deprecated use EduProgramSubjectIndexGeneration */
    @Deprecated
    public static final List<String> OKSO_CODES = ImmutableList.of(
            EduProgramSubjectIndexCodes.TITLE_2005_50,
            EduProgramSubjectIndexCodes.TITLE_2005_62,
            EduProgramSubjectIndexCodes.TITLE_2005_65,
            EduProgramSubjectIndexCodes.TITLE_2005_68);

    // use nodejs to generate following
    // for (var i=0;i<a.length;i++) { process.stdout.write('/** '+a[i][1]+'*/\n public static final String I_'+a[i][0].replace('.', '_')+'="'+a[i][0]+'";\n'); }

    @Deprecated
    /** @deprecated use EduProgramSubjectIndexCodes */
    public static final String I_2005_50=EduProgramSubjectIndexCodes.TITLE_2005_50;

    @Deprecated
    /** @deprecated use EduProgramSubjectIndexCodes */
    public static final String I_2005_62=EduProgramSubjectIndexCodes.TITLE_2005_62;

    @Deprecated
    /** @deprecated use EduProgramSubjectIndexCodes */
    public static final String I_2005_65=EduProgramSubjectIndexCodes.TITLE_2005_65;

    @Deprecated
    /** @deprecated use EduProgramSubjectIndexCodes */
    public static final String I_2005_68=EduProgramSubjectIndexCodes.TITLE_2005_68;

    @Deprecated
    /** @deprecated use EduProgramSubjectIndexCodes */
    public static final String I_2009_40=EduProgramSubjectIndexCodes.TITLE_2009_40;

    @Deprecated
    /** @deprecated use EduProgramSubjectIndexCodes */
    public static final String I_2009_50=EduProgramSubjectIndexCodes.TITLE_2009_50;

    @Deprecated
    /** @deprecated use EduProgramSubjectIndexCodes */
    public static final String I_2009_62=EduProgramSubjectIndexCodes.TITLE_2009_62;

    @Deprecated
    /** @deprecated use EduProgramSubjectIndexCodes */
    public static final String I_2009_65=EduProgramSubjectIndexCodes.TITLE_2009_65;

    @Deprecated
    /** @deprecated use EduProgramSubjectIndexCodes */
    public static final String I_2009_68=EduProgramSubjectIndexCodes.TITLE_2009_68;

    @Deprecated
    /** @deprecated use EduProgramSubjectIndexCodes */
    public static final String I_2013_01=EduProgramSubjectIndexCodes.TITLE_2013_01;

    @Deprecated
    /** @deprecated use EduProgramSubjectIndexCodes */
    public static final String I_2013_02=EduProgramSubjectIndexCodes.TITLE_2013_02;

    @Deprecated
    /** @deprecated use EduProgramSubjectIndexCodes */
    public static final String I_2013_03=EduProgramSubjectIndexCodes.TITLE_2013_03;

    @Deprecated
    /** @deprecated use EduProgramSubjectIndexCodes */
    public static final String I_2013_04=EduProgramSubjectIndexCodes.TITLE_2013_04;

    @Deprecated
    /** @deprecated use EduProgramSubjectIndexCodes */
    public static final String I_2013_05=EduProgramSubjectIndexCodes.TITLE_2013_05;

    @Deprecated
    /** @deprecated use EduProgramSubjectIndexCodes */
    public static final String I_2013_06=EduProgramSubjectIndexCodes.TITLE_2013_06;

    @Deprecated
    /** @deprecated use EduProgramSubjectIndexCodes */
    public static final String I_2013_07=EduProgramSubjectIndexCodes.TITLE_2013_07;

    @Deprecated
    /** @deprecated use EduProgramSubjectIndexCodes */
    public static final String I_2013_08=EduProgramSubjectIndexCodes.TITLE_2013_08;

    @Deprecated
    /** @deprecated use EduProgramSubjectIndexCodes */
    public static final String I_2013_10=EduProgramSubjectIndexCodes.TITLE_2013_10;

    public String getSubjectTypeTitleDative()
    {
        switch (this.getProgramKind().getCode())
        {
            case EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_: return "направлению";
            case EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA : return "направлению";
            case EduProgramKindCodes.PROGRAMMA_MAGISTRATURY : return "направлению";
            case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_ : return "профессии";
            case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV : return "специальности";
            case EduProgramKindCodes.PROGRAMMA_ORDINATURY : return "специальности";
            case EduProgramKindCodes.PROGRAMMA_INTERNATURY : return "специальности";
            case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA: return "специальности";

            default : throw new IllegalStateException();
        }
    }

    public String getSpecializationTypeTitleDative()
    {
        switch (this.getProgramKind().getCode())
        {
            case EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_: return "профилю";
            case EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA : return "профилю";
            case EduProgramKindCodes.PROGRAMMA_MAGISTRATURY : return "профилю";
            case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_ : return "";
            case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV : return "специализации";
            case EduProgramKindCodes.PROGRAMMA_ORDINATURY : return "специализации";
            case EduProgramKindCodes.PROGRAMMA_INTERNATURY : return "специализации";
            case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA: return "специализации";

            default : throw new IllegalStateException();
        }
    }
}