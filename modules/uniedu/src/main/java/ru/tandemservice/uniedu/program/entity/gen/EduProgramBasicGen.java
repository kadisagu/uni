package ru.tandemservice.uniedu.program.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.program.entity.EduProgramBasic;
import ru.tandemservice.uniedu.program.entity.EduProgramPrimary;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Основная общеобразовательная программа
 *
 * Основное общее образование: реализуется по уровням основного образования, уровень берется из вида образовательной программы.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramBasicGen extends EduProgramPrimary
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.program.entity.EduProgramBasic";
    public static final String ENTITY_NAME = "eduProgramBasic";
    public static final int VERSION_HASH = -1493616786;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduProgramBasicGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramBasicGen> extends EduProgramPrimary.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramBasic.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramBasic();
        }
    }
    private static final Path<EduProgramBasic> _dslPath = new Path<EduProgramBasic>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramBasic");
    }
            

    public static class Path<E extends EduProgramBasic> extends EduProgramPrimary.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EduProgramBasic.class;
        }

        public String getEntityName()
        {
            return "eduProgramBasic";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
