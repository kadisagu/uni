/**
 *$Id$
 */
package ru.tandemservice.uniedu.orgunit.bo.EduOwnerOrgUnit.ui.List.logic;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;

/**
 * @author Alexander Zhebko
 * @since 04.02.2014
 */
public class EduOwnerOrgUnitListDSHandler extends DefaultSearchDataSourceHandler
{
    public EduOwnerOrgUnitListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    public DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
        .fromEntity(EduOwnerOrgUnit.class, "e")
        .column(property("e"))
        .fetchPath(DQLJoinType.inner, EduOwnerOrgUnit.orgUnit().fromAlias("e"), "ou");

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).order().build();
    }
}