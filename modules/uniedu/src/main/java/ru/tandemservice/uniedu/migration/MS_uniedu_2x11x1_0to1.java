package ru.tandemservice.uniedu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSpecialization.logic.SpecializationShortTitleBuilder;

import java.sql.SQLException;
import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniedu_2x11x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.1")
		};
    }

	private static final String specBaseTableName = "edu_specialization_base_t";

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eduProgramSpecialization

		//  свойство shortTitle стало обязательным
		{

			generateShortTitles(tool, "edu_specialization_root_t", title -> SpecializationShortTitleBuilder.buildShortTitle(title, true));
			generateShortTitles(tool, "edu_specialization_child_t", title -> SpecializationShortTitleBuilder.buildShortTitle(title, false));

			// сделать колонку NOT NULL
			tool.setColumnNullable("edu_specialization_base_t", "shorttitle_p", false);

		}


    }

	private static void generateShortTitles(DBTool tool, String specTableName, Function<String, String> titleShortener) throws SQLException
	{
		Collection<SpecRow> specToUpdate = getSpecRowsToUpdate(tool, specTableName);
		generateShortTitles(tool, specToUpdate, titleShortener);
	}

	private static Collection<SpecRow> getSpecRowsToUpdate(DBTool tool, String specTableName) throws SQLException
	{
		SQLSelectQuery specQuery = new SQLSelectQuery().from(SQLFrom.table(specBaseTableName, "baseSpec").innerJoin(SQLFrom.table(specTableName, "spec"), "baseSpec.id=spec.id"))
				.column("baseSpec.id").column("baseSpec.title_p")
				.where("baseSpec.shorttitle_p is null");
		Collection<Object[]> rows = tool.executeQuery(MigrationUtils.processor(Long.class, String.class), tool.getDialect().getSQLTranslator().toSql(specQuery));
		return rows.stream().map((Object[] row) -> new SpecRow((Long)row[0], (String)row[1])).collect(Collectors.toList());
	}

	private static void generateShortTitles(DBTool tool, Collection<SpecRow> rowsToUpdate, Function<String, String> titleShortener) throws SQLException
	{
		MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update " + specBaseTableName + " set shorttitle_p=? where id=?", DBType.EMPTY_STRING, DBType.LONG);
		rowsToUpdate.forEach(row -> updater.addBatch(titleShortener.apply(row.title), row.id));
		updater.executeUpdate(tool);
	}

	private static class SpecRow
	{
		public final long id;
		public final String title;

		public SpecRow(long id, String title)
		{
			this.id = id;
			this.title = title;
		}
	}
}