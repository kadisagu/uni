/* $Id:$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.ui.Pub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemPubUI;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.bo.EduProgramQualification.ui.Pub.EduProgramQualificationPub;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSpecialization.ui.AddEdit.EduProgramSpecializationAddEdit;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSpecialization.ui.AddEdit.EduProgramSpecializationAddEditUI;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.EduProgramSubjectManager;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.ui.Edit.EduProgramSubjectEdit;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.ui.List.EduProgramSubjectList;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.ui.QualificationAdd.EduProgramSubjectQualificationAdd;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 11/21/13
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id")
})
public class EduProgramSubjectPubUI extends BaseCatalogItemPubUI
{
    private String _selectedPage;

    // TODO: вырезать это, когда educationLevels будут вырезаны
    @Zlo("решение временное, поэтому хардкодим")
    private static final boolean eduLvlExists = (null != EntityRuntime.getMeta("educationLevels"));
    public boolean isEduLvlExists() { return eduLvlExists; }

    private List<Object/*educationLevels*/> nonChildEduLvlList = Collections.emptyList();
    public List<Object> getNonChildEduLvlList() { return this.nonChildEduLvlList; }

    private StaticListDataSource<EduProgramSubjectQualification> qualDS;
    private StaticListDataSource<EduProgramSpecialization> specDS;

	private static final int propertyColumnCount = 2;

    @Override
    @SuppressWarnings("unchecked")
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        prepareQualDS();
        prepareSpecDS();

        // TODO: это адовое зло должно исчезнуть, после того как educationLevels пропадет из интерфейса системы
        if (isEduLvlExists())
        {
            // по каждой направленности формируем список НПм - здесь перечнь только дочерних направлений
            List<ViewWrapper> wrappers = (List)ViewWrapper.getPatchedList(specDS.getRowList());
            for (ViewWrapper wrapper: wrappers) {
                wrapper.setViewProperty("eduLvlList", new ArrayList<>(4));
            }

            Map<Long, ViewWrapper> map = UniBaseDao.map(wrappers);

            nonChildEduLvlList = new ArrayList<>();

            List<Object[]> rows = IUniBaseDao.instance.get().getList(
                new DQLSelectBuilder()
                .fromEntity("educationLevels", "x")
                .column(property("x.eduProgramSpecialization.id"))
                .column(property("x"))
                .where(eq(property("x.eduProgramSubject"), value(getCatalogItem())))
                .order(property("x.title"))
            );

            for (Object[] row: rows) {
                Long specId = (Long)row[0];
                ViewWrapper wrapper = null == specId ? null : map.get(specId);
                if (null == wrapper) {
                    // если враппера нет - засовываем в глобальный список
                    nonChildEduLvlList.add(row[1]);
                    continue;
                }

                // если есть - то в список враппера
                List<Object> eduLvlList = (List<Object>) wrapper.getViewProperty("eduLvlList");
                eduLvlList.add(row[1]);
            }

            if (isEduLvlExists()) {
                specDS.addColumn(
						new PublisherLinkColumn("НПм", "fullTitleWithRootLevel", "eduLvlList")
								.setResolver(new SimplePublisherLinkResolver("id").setComponentName("UniEduProgramEduLvlPub")).setOrderable(false),
		                propertyColumnCount
                );
            }

            specDS.setupRows((List)wrappers);
        }
    }

    public void onClickQualificationAdd()
    {
        _uiActivation.asRegionDialog(EduProgramSubjectQualificationAdd.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, getHolder().getId()).activate();
    }


    public void onClickQualificationDelete()
    {
        EduProgramSubjectManager.instance().dao().deleteEduProgramSubjectQualification(getListenerParameterAsLong());
        onComponentRefresh();
    }

    public void onClickSpecAdd()
    {
        _uiActivation.asRegionDialog(EduProgramSpecializationAddEdit.class).parameter(EduProgramSpecializationAddEditUI.KEY_SUBJECT_ID, getHolder().getId()).activate();
    }

    public void onClickSpecEdit()
    {
        _uiActivation.asRegionDialog(EduProgramSpecializationAddEdit.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, getListenerParameterAsLong()).activate();
    }

    public void onClickSpecDelete()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
        onComponentRefresh();
    }

    // presenter

    public boolean isAllowQualificationAdd()
    {
        EduProgramKind programKind = ((EduProgramSubject) getCatalogItem()).getSubjectIndex().getProgramKind();
        return programKind.isProgramSecondaryProf();
    }

    public boolean isAllowSpec()
    {
        EduProgramKind programKind = ((EduProgramSubject) getCatalogItem()).getSubjectIndex().getProgramKind();
        return programKind.isProgramHigherProf();
    }

    @Override
    public String getCatalogCode()
    {
        return StringUtils.uncapitalize(EduProgramSubject.class.getSimpleName());
    }

    @Override
    public String getCatalogPub()
    {
        return EduProgramSubjectList.class.getSimpleName();
    }

    // inner

    private void prepareQualDS()
    {
        StaticListDataSource<EduProgramSubjectQualification> qualDS = new StaticListDataSource<>();
        qualDS.setupRows(IUniBaseDao.instance.get().getList(
            EduProgramSubjectQualification.class,
            EduProgramSubjectQualification.programSubject(),
            (EduProgramSubject) getCatalogItem(),
            EduProgramSubjectQualification.programQualification().title().s()
        ));

        qualDS.addColumn(new PublisherLinkColumn("Квалификация", EduProgramSubjectQualification.programQualification().title())
                .setResolver(new SimplePublisherLinkResolver(EduProgramSubjectQualification.programQualification().id()).setComponentName(EduProgramQualificationPub.class.getSimpleName())));
        if (isAllowQualificationAdd())
        {
	        AbstractColumn deleteColumn = new ActionColumn("Удалить", ActionColumn.DELETE, "onClickQualificationDelete",
			        "Удалить квалификацию «{0}» из перечня разрешенных к присвоению квалификаций?", EduProgramSubjectQualification.programQualification().title())
			        .setPermissionKey(getSec().getPermission("edit"));
            qualDS.addColumn(deleteColumn);
        }

        setQualDS(qualDS);
    }

    private void prepareSpecDS()
    {
	    ICommonDAO dao = DataAccessServices.dao();
	    final EduProgramSubject subject = (EduProgramSubject)getCatalogItem();

	    Collection<EduProgramSpecialization> specializations = new ArrayList<>();
	    EduProgramSpecializationRoot rootSpec = dao.get(EduProgramSpecializationRoot.class, EduProgramSpecialization.programSubject(), subject);
	    Collection<EduProgramSpecializationChild> childSpec = dao.getList(EduProgramSpecializationChild.class, EduProgramSpecializationChild.programSubject(), subject, EduProgramSpecializationChild.title().s());
	    if (rootSpec != null)
	        specializations.add(rootSpec);
	    specializations.addAll(childSpec);

	    StaticListDataSource<EduProgramSpecialization> specDS = new StaticListDataSource<>();
	    specDS.setupRows(specializations);

        specDS.addColumn(new SimpleColumn("Название", EduProgramSpecialization.displayableTitle()).setClickable(false).setOrderable(false));
	    specDS.addColumn(new SimpleColumn("Сокращенное название", EduProgramSpecialization.shortTitle()).setClickable(false).setOrderable(false));
        specDS.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickSpecEdit").setPermissionKey(getSec().getPermission("edit")));
        specDS.addColumn(createDeleteColumn());

        setSpecDS(specDS);
    }

	private ActionColumn createDeleteColumn()
	{
		IEntityHandler delDisabledHandler = entity ->
		{
			if (!(entity instanceof ViewWrapper))
				return true;
			ViewWrapper wrapper = (ViewWrapper) entity;
			return wrapper.getEntity() instanceof EduProgramSpecializationRoot;
		};

		ActionColumn deleteColumn = new ActionColumn("Удалить", ActionColumn.DELETE, "onClickSpecDelete");
		deleteColumn.setAlertWithParameters(new FormattedMessage("Удалить направленность (профиль, специализацию) «{0}»?", EduProgramSpecializationChild.title()));
		deleteColumn.setDisableHandler(delDisabledHandler);
		deleteColumn.setPermissionKey(getSec().getPermission("edit"));
		return deleteColumn;
	}

    public void onClickEditProgramSubject()
    {
        getActivationBuilder().asRegion(EduProgramSubjectEdit.class).parameter(IUIPresenter.PUBLISHER_ID, getHolder().getId()).activate();
    }

    // getters and setters
    public StaticListDataSource<EduProgramSubjectQualification> getQualDS()
    {
        return qualDS;
    }

    public void setQualDS(StaticListDataSource<EduProgramSubjectQualification> qualDS)
    {
        this.qualDS = qualDS;
    }

    public StaticListDataSource<EduProgramSpecialization> getSpecDS()
    {
        return specDS;
    }

    public void setSpecDS(StaticListDataSource<EduProgramSpecialization> specDS)
    {
        this.specDS = specDS;
    }

    public String getRegionName()
    {
        return EduProgramSubjectPub.TAB_PANEL_REGION_NAME;
    }

    public String getSelectedPage()
    {
        return _selectedPage;
    }

    public void setSelectedPage(String selectedPage)
    {
        _selectedPage = selectedPage;
    }
}