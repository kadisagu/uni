package ru.tandemservice.uniedu.orgunit.entity;

import org.tandemframework.caf.ui.datasource.select.ISelectValueStyleSource;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.fias.base.bo.util.NonActiveSelectValueStyle;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uniedu.orgunit.entity.gen.EduOwnerOrgUnitGen;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Выпускающее подразделение
 *
 * Позразделение, ответственное за выпуск по образовательным программам
 */
public class EduOwnerOrgUnit extends EduOwnerOrgUnitGen implements ITitled
{
    @Override
    @EntityDSLSupport(parts = EduOwnerOrgUnit.L_ORG_UNIT + "." + OrgUnit.P_FULL_TITLE)
    public String getTitle()
    {
        if (getOrgUnit() == null) {
            return this.getClass().getSimpleName();
        }
        return getOrgUnit().getFullTitle();
    }

	/** Источник по умолчанию. Кастомизирует {@link OrgUnit#defaultComboDataSourceHandler}, выводя только выпускающие подразделения. */
	public static EntityComboDataSourceHandler defaultDSHandler(String ownerId)
	{
		return OrgUnit.defaultComboDataSourceHandler(ownerId)
				.customize((alias, dql, context, filter) -> dql.where(exists(EduOwnerOrgUnit.class, EduOwnerOrgUnit.orgUnit().id().s(), property(alias, "id"))));
	}

	public static final ISelectValueStyleSource archivalWiseStyle = value -> {
		if (value instanceof OrgUnit)
		{
			OrgUnit orgUnit = (OrgUnit)value;
			if (orgUnit.isArchival())
				return NonActiveSelectValueStyle.INSTANCE;
		}
		return null;
	};

    /**
     * Специальный хэндлер для реорганизации
     * */
    public static EntityComboDataSourceHandler eduOwnerOrgUnitReorganizationCustomizedOrgUnitHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, OrgUnit.class)
                .titleProperty(OrgUnit.fullTitle().s())
                .order(OrgUnit.archival())
                .order(OrgUnit.fullTitle())
                .filter(OrgUnit.fullTitle())
                .filter(OrgUnit.orgUnitType().title())
                .customize((alias, dql, context, filter) -> dql.where(exists(EduOwnerOrgUnit.class, EduOwnerOrgUnit.orgUnit().id().s(), property(alias, "id"))));
    }
}