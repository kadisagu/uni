/* $Id$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.util;

import ru.tandemservice.uniedu.util.UniEduUtil;

/**
 * Утильный класс. Формирует сокр. название направлениям подготовки по названию.
 * @author azhebko
 * @since 27.06.2014
 */
public class EduProgramSubjectShortTitleBuilder
{
    /** Сформировать сокрю название. */
    public static String buildShortTitle(String source)
    {
        return UniEduUtil.buildAbbreviation(source);
    }
}