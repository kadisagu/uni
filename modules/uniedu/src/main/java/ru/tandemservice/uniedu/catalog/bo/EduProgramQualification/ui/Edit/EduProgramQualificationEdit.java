/* $Id:$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramQualification.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author rsizonenko
 * @since 08.09.2014
 */
@Configuration
public class EduProgramQualificationEdit extends BusinessComponentManager {
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .create();
    }
}
