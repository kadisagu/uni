package ru.tandemservice.uniedu.program.entity;

import ru.tandemservice.uniedu.program.entity.gen.EduProgramVocationalGen;

/**
 * Основная программа профессионального обучения
 *
 * Профессиональное обучение: реализуется по профессиям рабочих и должностям служащих.
 */
public class EduProgramVocational extends EduProgramVocationalGen
{

    @Override
    protected StringBuilder uniqueKeyBuilder() {
        throw new UnsupportedOperationException(); // пока мы не поддерживаем такие ОП
    }

    @Override
    protected StringBuilder titleBuilder() {
        throw new UnsupportedOperationException(); // пока мы не поддерживаем такие ОП
    }

}