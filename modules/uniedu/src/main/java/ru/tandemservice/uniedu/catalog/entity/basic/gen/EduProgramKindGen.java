package ru.tandemservice.uniedu.catalog.entity.basic.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.commonbase.base.entity.IDeclinable;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид образовательной программы
 *
 * Определяет перечень видов ОП и исх соответствие уровню (виду, подвиду) образования.
 * Берется из ФЗ, всегда актуальный, все предыдущие виды ОП приводятся к актуальному классификатору.
 * По данному справочнику определяется класс (сущность) образовательной программы.
 * http://wiki.tandemservice.ru/pages/viewpage.action?pageId=17465754
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramKindGen extends EntityBase
 implements IDeclinable, INaturalIdentifiable<EduProgramKindGen>, org.tandemframework.common.catalog.entity.ICatalogItem, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind";
    public static final String ENTITY_NAME = "eduProgramKind";
    public static final int VERSION_HASH = -1484558771;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_EDU_LEVEL = "eduLevel";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_EDU_PROGRAM_SUBJECT_KIND = "eduProgramSubjectKind";
    public static final String P_PRIORITY = "priority";
    public static final String P_PROGRAM_BASIC = "programBasic";
    public static final String P_PROGRAM_PROF = "programProf";
    public static final String P_PROGRAM_SECONDARY_PROF = "programSecondaryProf";
    public static final String P_PROGRAM_HIGHER_PROF = "programHigherProf";
    public static final String P_PROGRAM_HIGHER_QUAL = "programHigherQual";
    public static final String P_PROGRAM_ADDITIONAL_PROF = "programAdditionalProf";
    public static final String P_PROGRAM_BACHELOR_DEGREE = "programBachelorDegree";
    public static final String P_PROGRAM_SPECIALIST_DEGREE = "programSpecialistDegree";
    public static final String P_PROGRAM_MASTER_DEGREE = "programMasterDegree";
    public static final String P_PROGRAM_POSTGRADUATE = "programPostgraduate";
    public static final String P_PROGRAM_TRAINEESHIP = "programTraineeship";
    public static final String P_PROGRAM_INTERNSHIP = "programInternship";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private EduLevel _eduLevel;     // Уровень (вид, подвид) образования
    private String _shortTitle;     // Сокращенное название
    private String _eduProgramSubjectKind;     // Тип направления подготовки
    private int _priority;     // Приоритет
    private boolean _programBasic;     // Основная общеобразовательная программа
    private boolean _programProf;     // Основная профессиональная образовательная программа
    private boolean _programSecondaryProf;     // Программа СПО
    private boolean _programHigherProf;     // Программа ВО
    private boolean _programHigherQual;     // Программа подготовки кадров высшей квалификации
    private boolean _programAdditionalProf;     // Программа ДПО
    private boolean _programBachelorDegree;     // Программа бакалавриата
    private boolean _programSpecialistDegree;     // Программа специалитета
    private boolean _programMasterDegree;     // Программа магистратуры
    private boolean _programPostgraduate;     // Программа аспирантуры (адъюнктуры)
    private boolean _programTraineeship;     // Программа ординатуры
    private boolean _programInternship;     // Программа интернатуры
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Уровень (вид, подвид) образования. Свойство не может быть null.
     */
    @NotNull
    public EduLevel getEduLevel()
    {
        return _eduLevel;
    }

    /**
     * @param eduLevel Уровень (вид, подвид) образования. Свойство не может быть null.
     */
    public void setEduLevel(EduLevel eduLevel)
    {
        dirty(_eduLevel, eduLevel);
        _eduLevel = eduLevel;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Тип направления подготовки. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEduProgramSubjectKind()
    {
        return _eduProgramSubjectKind;
    }

    /**
     * @param eduProgramSubjectKind Тип направления подготовки. Свойство не может быть null.
     */
    public void setEduProgramSubjectKind(String eduProgramSubjectKind)
    {
        dirty(_eduProgramSubjectKind, eduProgramSubjectKind);
        _eduProgramSubjectKind = eduProgramSubjectKind;
    }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null и должно быть уникальным.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * true, если уровень соответствует общему образованию: «Дошкольное образование», «Начальное общее образование», «Основное общее образование», «Среднее общее образование»
     *
     * @return Основная общеобразовательная программа. Свойство не может быть null.
     *
     * Это формула "case when eduLevel.code in ('2013.1.1', '2013.1.2', '2013.1.3', '2013.1.4') then true else false end".
     */
    // @NotNull
    public boolean isProgramBasic()
    {
        initLazyForGet("programBasic");
        return _programBasic;
    }

    /**
     * @param programBasic Основная общеобразовательная программа. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setProgramBasic(boolean programBasic)
    {
        initLazyForSet("programBasic");
        dirty(_programBasic, programBasic);
        _programBasic = programBasic;
    }

    /**
     * true, если уровень соответствует уровню профессионального образования (ВО и СПО): «бакалавриат», «специалитет, магистратура», «подготовка кадров высшей квалификации»
     *
     * @return Основная профессиональная образовательная программа. Свойство не может быть null.
     *
     * Это формула "case when eduLevel.code in ('2013.2.1', '2013.2.2', '2013.2.3', '2013.2.4') then true else false end".
     */
    // @NotNull
    public boolean isProgramProf()
    {
        initLazyForGet("programProf");
        return _programProf;
    }

    /**
     * @param programProf Основная профессиональная образовательная программа. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setProgramProf(boolean programProf)
    {
        initLazyForSet("programProf");
        dirty(_programProf, programProf);
        _programProf = programProf;
    }

    /**
     * true, если уровень соответствует уровню CПО: «Среднее профессиональное образование»
     *
     * @return Программа СПО. Свойство не может быть null.
     *
     * Это формула "case when eduLevel.code in ('2013.2.1') then true else false end".
     */
    // @NotNull
    public boolean isProgramSecondaryProf()
    {
        initLazyForGet("programSecondaryProf");
        return _programSecondaryProf;
    }

    /**
     * @param programSecondaryProf Программа СПО. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setProgramSecondaryProf(boolean programSecondaryProf)
    {
        initLazyForSet("programSecondaryProf");
        dirty(_programSecondaryProf, programSecondaryProf);
        _programSecondaryProf = programSecondaryProf;
    }

    /**
     * true, если уровень соответствует уровню ВО: «бакалавриат», «специалитет, магистратура», «подготовка кадров высшей квалификации»
     *
     * @return Программа ВО. Свойство не может быть null.
     *
     * Это формула "case when eduLevel.code in ('2013.2.2', '2013.2.3', '2013.2.4') then true else false end".
     */
    // @NotNull
    public boolean isProgramHigherProf()
    {
        initLazyForGet("programHigherProf");
        return _programHigherProf;
    }

    /**
     * @param programHigherProf Программа ВО. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setProgramHigherProf(boolean programHigherProf)
    {
        initLazyForSet("programHigherProf");
        dirty(_programHigherProf, programHigherProf);
        _programHigherProf = programHigherProf;
    }

    /**
     * true для видов «аспирантура», «ординатура», «интернатура»
     *
     * @return Программа подготовки кадров высшей квалификации. Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.4.1', '2013.2.4.2', '2013.2.4.3') then true else false end".
     */
    // @NotNull
    public boolean isProgramHigherQual()
    {
        initLazyForGet("programHigherQual");
        return _programHigherQual;
    }

    /**
     * @param programHigherQual Программа подготовки кадров высшей квалификации. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setProgramHigherQual(boolean programHigherQual)
    {
        initLazyForSet("programHigherQual");
        dirty(_programHigherQual, programHigherQual);
        _programHigherQual = programHigherQual;
    }

    /**
     * true, если уровень соответствует уровню ДПО
     *
     * @return Программа ДПО. Свойство не может быть null.
     *
     * Это формула "case when eduLevel.code in ('2013.3.2') then true else false end".
     */
    // @NotNull
    public boolean isProgramAdditionalProf()
    {
        initLazyForGet("programAdditionalProf");
        return _programAdditionalProf;
    }

    /**
     * @param programAdditionalProf Программа ДПО. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setProgramAdditionalProf(boolean programAdditionalProf)
    {
        initLazyForSet("programAdditionalProf");
        dirty(_programAdditionalProf, programAdditionalProf);
        _programAdditionalProf = programAdditionalProf;
    }

    /**
     * true для вида «бакалавриат»
     *
     * @return Программа бакалавриата. Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.2.1') then true else false end".
     */
    // @NotNull
    public boolean isProgramBachelorDegree()
    {
        initLazyForGet("programBachelorDegree");
        return _programBachelorDegree;
    }

    /**
     * @param programBachelorDegree Программа бакалавриата. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setProgramBachelorDegree(boolean programBachelorDegree)
    {
        initLazyForSet("programBachelorDegree");
        dirty(_programBachelorDegree, programBachelorDegree);
        _programBachelorDegree = programBachelorDegree;
    }

    /**
     * true для вида «специалитет»
     *
     * @return Программа специалитета. Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.3.1') then true else false end".
     */
    // @NotNull
    public boolean isProgramSpecialistDegree()
    {
        initLazyForGet("programSpecialistDegree");
        return _programSpecialistDegree;
    }

    /**
     * @param programSpecialistDegree Программа специалитета. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setProgramSpecialistDegree(boolean programSpecialistDegree)
    {
        initLazyForSet("programSpecialistDegree");
        dirty(_programSpecialistDegree, programSpecialistDegree);
        _programSpecialistDegree = programSpecialistDegree;
    }

    /**
     * true для вида «магистратура»
     *
     * @return Программа магистратуры. Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.3.2') then true else false end".
     */
    // @NotNull
    public boolean isProgramMasterDegree()
    {
        initLazyForGet("programMasterDegree");
        return _programMasterDegree;
    }

    /**
     * @param programMasterDegree Программа магистратуры. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setProgramMasterDegree(boolean programMasterDegree)
    {
        initLazyForSet("programMasterDegree");
        dirty(_programMasterDegree, programMasterDegree);
        _programMasterDegree = programMasterDegree;
    }

    /**
     * true для вида «аспирантура»
     *
     * @return Программа аспирантуры (адъюнктуры). Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.4.1') then true else false end".
     */
    // @NotNull
    public boolean isProgramPostgraduate()
    {
        initLazyForGet("programPostgraduate");
        return _programPostgraduate;
    }

    /**
     * @param programPostgraduate Программа аспирантуры (адъюнктуры). Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setProgramPostgraduate(boolean programPostgraduate)
    {
        initLazyForSet("programPostgraduate");
        dirty(_programPostgraduate, programPostgraduate);
        _programPostgraduate = programPostgraduate;
    }

    /**
     * true для вида «ординатура»
     *
     * @return Программа ординатуры. Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.4.2') then true else false end".
     */
    // @NotNull
    public boolean isProgramTraineeship()
    {
        initLazyForGet("programTraineeship");
        return _programTraineeship;
    }

    /**
     * @param programTraineeship Программа ординатуры. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setProgramTraineeship(boolean programTraineeship)
    {
        initLazyForSet("programTraineeship");
        dirty(_programTraineeship, programTraineeship);
        _programTraineeship = programTraineeship;
    }

    /**
     * true для вида «интернатура»
     *
     * @return Программа интернатуры. Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.4.3') then true else false end".
     */
    // @NotNull
    public boolean isProgramInternship()
    {
        initLazyForGet("programInternship");
        return _programInternship;
    }

    /**
     * @param programInternship Программа интернатуры. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setProgramInternship(boolean programInternship)
    {
        initLazyForSet("programInternship");
        dirty(_programInternship, programInternship);
        _programInternship = programInternship;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EduProgramKindGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EduProgramKind)another).getCode());
            }
            setEduLevel(((EduProgramKind)another).getEduLevel());
            setShortTitle(((EduProgramKind)another).getShortTitle());
            setEduProgramSubjectKind(((EduProgramKind)another).getEduProgramSubjectKind());
            setPriority(((EduProgramKind)another).getPriority());
            setProgramBasic(((EduProgramKind)another).isProgramBasic());
            setProgramProf(((EduProgramKind)another).isProgramProf());
            setProgramSecondaryProf(((EduProgramKind)another).isProgramSecondaryProf());
            setProgramHigherProf(((EduProgramKind)another).isProgramHigherProf());
            setProgramHigherQual(((EduProgramKind)another).isProgramHigherQual());
            setProgramAdditionalProf(((EduProgramKind)another).isProgramAdditionalProf());
            setProgramBachelorDegree(((EduProgramKind)another).isProgramBachelorDegree());
            setProgramSpecialistDegree(((EduProgramKind)another).isProgramSpecialistDegree());
            setProgramMasterDegree(((EduProgramKind)another).isProgramMasterDegree());
            setProgramPostgraduate(((EduProgramKind)another).isProgramPostgraduate());
            setProgramTraineeship(((EduProgramKind)another).isProgramTraineeship());
            setProgramInternship(((EduProgramKind)another).isProgramInternship());
            setTitle(((EduProgramKind)another).getTitle());
        }
    }

    public INaturalId<EduProgramKindGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EduProgramKindGen>
    {
        private static final String PROXY_NAME = "EduProgramKindNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EduProgramKindGen.NaturalId) ) return false;

            EduProgramKindGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramKindGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramKind.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramKind();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "eduLevel":
                    return obj.getEduLevel();
                case "shortTitle":
                    return obj.getShortTitle();
                case "eduProgramSubjectKind":
                    return obj.getEduProgramSubjectKind();
                case "priority":
                    return obj.getPriority();
                case "programBasic":
                    return obj.isProgramBasic();
                case "programProf":
                    return obj.isProgramProf();
                case "programSecondaryProf":
                    return obj.isProgramSecondaryProf();
                case "programHigherProf":
                    return obj.isProgramHigherProf();
                case "programHigherQual":
                    return obj.isProgramHigherQual();
                case "programAdditionalProf":
                    return obj.isProgramAdditionalProf();
                case "programBachelorDegree":
                    return obj.isProgramBachelorDegree();
                case "programSpecialistDegree":
                    return obj.isProgramSpecialistDegree();
                case "programMasterDegree":
                    return obj.isProgramMasterDegree();
                case "programPostgraduate":
                    return obj.isProgramPostgraduate();
                case "programTraineeship":
                    return obj.isProgramTraineeship();
                case "programInternship":
                    return obj.isProgramInternship();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "eduLevel":
                    obj.setEduLevel((EduLevel) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "eduProgramSubjectKind":
                    obj.setEduProgramSubjectKind((String) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "programBasic":
                    obj.setProgramBasic((Boolean) value);
                    return;
                case "programProf":
                    obj.setProgramProf((Boolean) value);
                    return;
                case "programSecondaryProf":
                    obj.setProgramSecondaryProf((Boolean) value);
                    return;
                case "programHigherProf":
                    obj.setProgramHigherProf((Boolean) value);
                    return;
                case "programHigherQual":
                    obj.setProgramHigherQual((Boolean) value);
                    return;
                case "programAdditionalProf":
                    obj.setProgramAdditionalProf((Boolean) value);
                    return;
                case "programBachelorDegree":
                    obj.setProgramBachelorDegree((Boolean) value);
                    return;
                case "programSpecialistDegree":
                    obj.setProgramSpecialistDegree((Boolean) value);
                    return;
                case "programMasterDegree":
                    obj.setProgramMasterDegree((Boolean) value);
                    return;
                case "programPostgraduate":
                    obj.setProgramPostgraduate((Boolean) value);
                    return;
                case "programTraineeship":
                    obj.setProgramTraineeship((Boolean) value);
                    return;
                case "programInternship":
                    obj.setProgramInternship((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "eduLevel":
                        return true;
                case "shortTitle":
                        return true;
                case "eduProgramSubjectKind":
                        return true;
                case "priority":
                        return true;
                case "programBasic":
                        return true;
                case "programProf":
                        return true;
                case "programSecondaryProf":
                        return true;
                case "programHigherProf":
                        return true;
                case "programHigherQual":
                        return true;
                case "programAdditionalProf":
                        return true;
                case "programBachelorDegree":
                        return true;
                case "programSpecialistDegree":
                        return true;
                case "programMasterDegree":
                        return true;
                case "programPostgraduate":
                        return true;
                case "programTraineeship":
                        return true;
                case "programInternship":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "eduLevel":
                    return true;
                case "shortTitle":
                    return true;
                case "eduProgramSubjectKind":
                    return true;
                case "priority":
                    return true;
                case "programBasic":
                    return true;
                case "programProf":
                    return true;
                case "programSecondaryProf":
                    return true;
                case "programHigherProf":
                    return true;
                case "programHigherQual":
                    return true;
                case "programAdditionalProf":
                    return true;
                case "programBachelorDegree":
                    return true;
                case "programSpecialistDegree":
                    return true;
                case "programMasterDegree":
                    return true;
                case "programPostgraduate":
                    return true;
                case "programTraineeship":
                    return true;
                case "programInternship":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "eduLevel":
                    return EduLevel.class;
                case "shortTitle":
                    return String.class;
                case "eduProgramSubjectKind":
                    return String.class;
                case "priority":
                    return Integer.class;
                case "programBasic":
                    return Boolean.class;
                case "programProf":
                    return Boolean.class;
                case "programSecondaryProf":
                    return Boolean.class;
                case "programHigherProf":
                    return Boolean.class;
                case "programHigherQual":
                    return Boolean.class;
                case "programAdditionalProf":
                    return Boolean.class;
                case "programBachelorDegree":
                    return Boolean.class;
                case "programSpecialistDegree":
                    return Boolean.class;
                case "programMasterDegree":
                    return Boolean.class;
                case "programPostgraduate":
                    return Boolean.class;
                case "programTraineeship":
                    return Boolean.class;
                case "programInternship":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgramKind> _dslPath = new Path<EduProgramKind>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramKind");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Уровень (вид, подвид) образования. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#getEduLevel()
     */
    public static EduLevel.Path<EduLevel> eduLevel()
    {
        return _dslPath.eduLevel();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Тип направления подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#getEduProgramSubjectKind()
     */
    public static PropertyPath<String> eduProgramSubjectKind()
    {
        return _dslPath.eduProgramSubjectKind();
    }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * true, если уровень соответствует общему образованию: «Дошкольное образование», «Начальное общее образование», «Основное общее образование», «Среднее общее образование»
     *
     * @return Основная общеобразовательная программа. Свойство не может быть null.
     *
     * Это формула "case when eduLevel.code in ('2013.1.1', '2013.1.2', '2013.1.3', '2013.1.4') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramBasic()
     */
    public static PropertyPath<Boolean> programBasic()
    {
        return _dslPath.programBasic();
    }

    /**
     * true, если уровень соответствует уровню профессионального образования (ВО и СПО): «бакалавриат», «специалитет, магистратура», «подготовка кадров высшей квалификации»
     *
     * @return Основная профессиональная образовательная программа. Свойство не может быть null.
     *
     * Это формула "case when eduLevel.code in ('2013.2.1', '2013.2.2', '2013.2.3', '2013.2.4') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramProf()
     */
    public static PropertyPath<Boolean> programProf()
    {
        return _dslPath.programProf();
    }

    /**
     * true, если уровень соответствует уровню CПО: «Среднее профессиональное образование»
     *
     * @return Программа СПО. Свойство не может быть null.
     *
     * Это формула "case when eduLevel.code in ('2013.2.1') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramSecondaryProf()
     */
    public static PropertyPath<Boolean> programSecondaryProf()
    {
        return _dslPath.programSecondaryProf();
    }

    /**
     * true, если уровень соответствует уровню ВО: «бакалавриат», «специалитет, магистратура», «подготовка кадров высшей квалификации»
     *
     * @return Программа ВО. Свойство не может быть null.
     *
     * Это формула "case when eduLevel.code in ('2013.2.2', '2013.2.3', '2013.2.4') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramHigherProf()
     */
    public static PropertyPath<Boolean> programHigherProf()
    {
        return _dslPath.programHigherProf();
    }

    /**
     * true для видов «аспирантура», «ординатура», «интернатура»
     *
     * @return Программа подготовки кадров высшей квалификации. Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.4.1', '2013.2.4.2', '2013.2.4.3') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramHigherQual()
     */
    public static PropertyPath<Boolean> programHigherQual()
    {
        return _dslPath.programHigherQual();
    }

    /**
     * true, если уровень соответствует уровню ДПО
     *
     * @return Программа ДПО. Свойство не может быть null.
     *
     * Это формула "case when eduLevel.code in ('2013.3.2') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramAdditionalProf()
     */
    public static PropertyPath<Boolean> programAdditionalProf()
    {
        return _dslPath.programAdditionalProf();
    }

    /**
     * true для вида «бакалавриат»
     *
     * @return Программа бакалавриата. Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.2.1') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramBachelorDegree()
     */
    public static PropertyPath<Boolean> programBachelorDegree()
    {
        return _dslPath.programBachelorDegree();
    }

    /**
     * true для вида «специалитет»
     *
     * @return Программа специалитета. Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.3.1') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramSpecialistDegree()
     */
    public static PropertyPath<Boolean> programSpecialistDegree()
    {
        return _dslPath.programSpecialistDegree();
    }

    /**
     * true для вида «магистратура»
     *
     * @return Программа магистратуры. Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.3.2') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramMasterDegree()
     */
    public static PropertyPath<Boolean> programMasterDegree()
    {
        return _dslPath.programMasterDegree();
    }

    /**
     * true для вида «аспирантура»
     *
     * @return Программа аспирантуры (адъюнктуры). Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.4.1') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramPostgraduate()
     */
    public static PropertyPath<Boolean> programPostgraduate()
    {
        return _dslPath.programPostgraduate();
    }

    /**
     * true для вида «ординатура»
     *
     * @return Программа ординатуры. Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.4.2') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramTraineeship()
     */
    public static PropertyPath<Boolean> programTraineeship()
    {
        return _dslPath.programTraineeship();
    }

    /**
     * true для вида «интернатура»
     *
     * @return Программа интернатуры. Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.4.3') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramInternship()
     */
    public static PropertyPath<Boolean> programInternship()
    {
        return _dslPath.programInternship();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EduProgramKind> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private EduLevel.Path<EduLevel> _eduLevel;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _eduProgramSubjectKind;
        private PropertyPath<Integer> _priority;
        private PropertyPath<Boolean> _programBasic;
        private PropertyPath<Boolean> _programProf;
        private PropertyPath<Boolean> _programSecondaryProf;
        private PropertyPath<Boolean> _programHigherProf;
        private PropertyPath<Boolean> _programHigherQual;
        private PropertyPath<Boolean> _programAdditionalProf;
        private PropertyPath<Boolean> _programBachelorDegree;
        private PropertyPath<Boolean> _programSpecialistDegree;
        private PropertyPath<Boolean> _programMasterDegree;
        private PropertyPath<Boolean> _programPostgraduate;
        private PropertyPath<Boolean> _programTraineeship;
        private PropertyPath<Boolean> _programInternship;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EduProgramKindGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Уровень (вид, подвид) образования. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#getEduLevel()
     */
        public EduLevel.Path<EduLevel> eduLevel()
        {
            if(_eduLevel == null )
                _eduLevel = new EduLevel.Path<EduLevel>(L_EDU_LEVEL, this);
            return _eduLevel;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EduProgramKindGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Тип направления подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#getEduProgramSubjectKind()
     */
        public PropertyPath<String> eduProgramSubjectKind()
        {
            if(_eduProgramSubjectKind == null )
                _eduProgramSubjectKind = new PropertyPath<String>(EduProgramKindGen.P_EDU_PROGRAM_SUBJECT_KIND, this);
            return _eduProgramSubjectKind;
        }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EduProgramKindGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * true, если уровень соответствует общему образованию: «Дошкольное образование», «Начальное общее образование», «Основное общее образование», «Среднее общее образование»
     *
     * @return Основная общеобразовательная программа. Свойство не может быть null.
     *
     * Это формула "case when eduLevel.code in ('2013.1.1', '2013.1.2', '2013.1.3', '2013.1.4') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramBasic()
     */
        public PropertyPath<Boolean> programBasic()
        {
            if(_programBasic == null )
                _programBasic = new PropertyPath<Boolean>(EduProgramKindGen.P_PROGRAM_BASIC, this);
            return _programBasic;
        }

    /**
     * true, если уровень соответствует уровню профессионального образования (ВО и СПО): «бакалавриат», «специалитет, магистратура», «подготовка кадров высшей квалификации»
     *
     * @return Основная профессиональная образовательная программа. Свойство не может быть null.
     *
     * Это формула "case when eduLevel.code in ('2013.2.1', '2013.2.2', '2013.2.3', '2013.2.4') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramProf()
     */
        public PropertyPath<Boolean> programProf()
        {
            if(_programProf == null )
                _programProf = new PropertyPath<Boolean>(EduProgramKindGen.P_PROGRAM_PROF, this);
            return _programProf;
        }

    /**
     * true, если уровень соответствует уровню CПО: «Среднее профессиональное образование»
     *
     * @return Программа СПО. Свойство не может быть null.
     *
     * Это формула "case when eduLevel.code in ('2013.2.1') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramSecondaryProf()
     */
        public PropertyPath<Boolean> programSecondaryProf()
        {
            if(_programSecondaryProf == null )
                _programSecondaryProf = new PropertyPath<Boolean>(EduProgramKindGen.P_PROGRAM_SECONDARY_PROF, this);
            return _programSecondaryProf;
        }

    /**
     * true, если уровень соответствует уровню ВО: «бакалавриат», «специалитет, магистратура», «подготовка кадров высшей квалификации»
     *
     * @return Программа ВО. Свойство не может быть null.
     *
     * Это формула "case when eduLevel.code in ('2013.2.2', '2013.2.3', '2013.2.4') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramHigherProf()
     */
        public PropertyPath<Boolean> programHigherProf()
        {
            if(_programHigherProf == null )
                _programHigherProf = new PropertyPath<Boolean>(EduProgramKindGen.P_PROGRAM_HIGHER_PROF, this);
            return _programHigherProf;
        }

    /**
     * true для видов «аспирантура», «ординатура», «интернатура»
     *
     * @return Программа подготовки кадров высшей квалификации. Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.4.1', '2013.2.4.2', '2013.2.4.3') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramHigherQual()
     */
        public PropertyPath<Boolean> programHigherQual()
        {
            if(_programHigherQual == null )
                _programHigherQual = new PropertyPath<Boolean>(EduProgramKindGen.P_PROGRAM_HIGHER_QUAL, this);
            return _programHigherQual;
        }

    /**
     * true, если уровень соответствует уровню ДПО
     *
     * @return Программа ДПО. Свойство не может быть null.
     *
     * Это формула "case when eduLevel.code in ('2013.3.2') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramAdditionalProf()
     */
        public PropertyPath<Boolean> programAdditionalProf()
        {
            if(_programAdditionalProf == null )
                _programAdditionalProf = new PropertyPath<Boolean>(EduProgramKindGen.P_PROGRAM_ADDITIONAL_PROF, this);
            return _programAdditionalProf;
        }

    /**
     * true для вида «бакалавриат»
     *
     * @return Программа бакалавриата. Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.2.1') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramBachelorDegree()
     */
        public PropertyPath<Boolean> programBachelorDegree()
        {
            if(_programBachelorDegree == null )
                _programBachelorDegree = new PropertyPath<Boolean>(EduProgramKindGen.P_PROGRAM_BACHELOR_DEGREE, this);
            return _programBachelorDegree;
        }

    /**
     * true для вида «специалитет»
     *
     * @return Программа специалитета. Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.3.1') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramSpecialistDegree()
     */
        public PropertyPath<Boolean> programSpecialistDegree()
        {
            if(_programSpecialistDegree == null )
                _programSpecialistDegree = new PropertyPath<Boolean>(EduProgramKindGen.P_PROGRAM_SPECIALIST_DEGREE, this);
            return _programSpecialistDegree;
        }

    /**
     * true для вида «магистратура»
     *
     * @return Программа магистратуры. Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.3.2') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramMasterDegree()
     */
        public PropertyPath<Boolean> programMasterDegree()
        {
            if(_programMasterDegree == null )
                _programMasterDegree = new PropertyPath<Boolean>(EduProgramKindGen.P_PROGRAM_MASTER_DEGREE, this);
            return _programMasterDegree;
        }

    /**
     * true для вида «аспирантура»
     *
     * @return Программа аспирантуры (адъюнктуры). Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.4.1') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramPostgraduate()
     */
        public PropertyPath<Boolean> programPostgraduate()
        {
            if(_programPostgraduate == null )
                _programPostgraduate = new PropertyPath<Boolean>(EduProgramKindGen.P_PROGRAM_POSTGRADUATE, this);
            return _programPostgraduate;
        }

    /**
     * true для вида «ординатура»
     *
     * @return Программа ординатуры. Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.4.2') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramTraineeship()
     */
        public PropertyPath<Boolean> programTraineeship()
        {
            if(_programTraineeship == null )
                _programTraineeship = new PropertyPath<Boolean>(EduProgramKindGen.P_PROGRAM_TRAINEESHIP, this);
            return _programTraineeship;
        }

    /**
     * true для вида «интернатура»
     *
     * @return Программа интернатуры. Свойство не может быть null.
     *
     * Это формула "case when code in ('2013.2.4.3') then true else false end".
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#isProgramInternship()
     */
        public PropertyPath<Boolean> programInternship()
        {
            if(_programInternship == null )
                _programInternship = new PropertyPath<Boolean>(EduProgramKindGen.P_PROGRAM_INTERNSHIP, this);
            return _programInternship;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EduProgramKindGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EduProgramKind.class;
        }

        public String getEntityName()
        {
            return "eduProgramKind";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
