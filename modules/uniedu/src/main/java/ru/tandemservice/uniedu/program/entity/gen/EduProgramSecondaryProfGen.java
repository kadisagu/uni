package ru.tandemservice.uniedu.program.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Образовательная программа СПО
 *
 * Среднее профессиональное образование: реализуется по направлениям (специальностям, профессиям) и признаку углубленного изучения.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramSecondaryProfGen extends EduProgramProf
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf";
    public static final String ENTITY_NAME = "eduProgramSecondaryProf";
    public static final int VERSION_HASH = 1267872864;
    private static IEntityMeta ENTITY_META;

    public static final String P_IN_DEPTH_STUDY = "inDepthStudy";
    public static final String P_FULL_TITLE = "fullTitle";

    private boolean _inDepthStudy;     // Углубленное изучение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * Определяет уровень изучения ОП: true = углубленный уровень, false = базовый уровень
     *
     * @return Углубленное изучение. Свойство не может быть null.
     */
    @NotNull
    public boolean isInDepthStudy()
    {
        return _inDepthStudy;
    }

    /**
     * @param inDepthStudy Углубленное изучение. Свойство не может быть null.
     */
    public void setInDepthStudy(boolean inDepthStudy)
    {
        dirty(_inDepthStudy, inDepthStudy);
        _inDepthStudy = inDepthStudy;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduProgramSecondaryProfGen)
        {
            setInDepthStudy(((EduProgramSecondaryProf)another).isInDepthStudy());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramSecondaryProfGen> extends EduProgramProf.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramSecondaryProf.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramSecondaryProf();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "inDepthStudy":
                    return obj.isInDepthStudy();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "inDepthStudy":
                    obj.setInDepthStudy((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "inDepthStudy":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "inDepthStudy":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "inDepthStudy":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgramSecondaryProf> _dslPath = new Path<EduProgramSecondaryProf>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramSecondaryProf");
    }
            

    /**
     * Определяет уровень изучения ОП: true = углубленный уровень, false = базовый уровень
     *
     * @return Углубленное изучение. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf#isInDepthStudy()
     */
    public static PropertyPath<Boolean> inDepthStudy()
    {
        return _dslPath.inDepthStudy();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf#getFullTitle()
     */
    public static SupportedPropertyPath<String> fullTitle()
    {
        return _dslPath.fullTitle();
    }

    public static class Path<E extends EduProgramSecondaryProf> extends EduProgramProf.Path<E>
    {
        private PropertyPath<Boolean> _inDepthStudy;
        private SupportedPropertyPath<String> _fullTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * Определяет уровень изучения ОП: true = углубленный уровень, false = базовый уровень
     *
     * @return Углубленное изучение. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf#isInDepthStudy()
     */
        public PropertyPath<Boolean> inDepthStudy()
        {
            if(_inDepthStudy == null )
                _inDepthStudy = new PropertyPath<Boolean>(EduProgramSecondaryProfGen.P_IN_DEPTH_STUDY, this);
            return _inDepthStudy;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf#getFullTitle()
     */
        public SupportedPropertyPath<String> fullTitle()
        {
            if(_fullTitle == null )
                _fullTitle = new SupportedPropertyPath<String>(EduProgramSecondaryProfGen.P_FULL_TITLE, this);
            return _fullTitle;
        }

        public Class getEntityClass()
        {
            return EduProgramSecondaryProf.class;
        }

        public String getEntityName()
        {
            return "eduProgramSecondaryProf";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getFullTitle();
}
