/* $Id$ */
package ru.tandemservice.uniedu.base.bo.EduSettings.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

/**
 * @author Nikolay Fedorovskih
 * @since 30.04.2014
 */
public interface IEduSettingsDao extends INeedPersistenceSupport
{
    /**
     * Выставление признака "Выпускнику присваивается специальное звание" для направления подготовки профессионального образования (2009)
     * @param programSubject2009Id НППО2009
     * @param assignSpecialDegree новое значение признака
     */
    void setAssignSpecialDegreeForEduProgram2009(Long programSubject2009Id, boolean assignSpecialDegree);
}