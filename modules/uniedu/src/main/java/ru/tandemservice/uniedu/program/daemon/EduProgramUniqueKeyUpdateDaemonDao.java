/* $Id$ */
package ru.tandemservice.uniedu.program.daemon;

import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * Обновление ключей образовательных программ, чтобы оно всегда правльное было.
 *
 * @author Nikolay Fedorovskih
 * @since 11.02.2015
 */
public class EduProgramUniqueKeyUpdateDaemonDao extends UniBaseDao implements IEduProgramUniqueKeyUpdateDaemonDao
{
    public static final SyncDaemon DAEMON = new SyncDaemon(EduProgramUniqueKeyUpdateDaemonDao.class.getName(), 1441, IEduProgramUniqueKeyUpdateDaemonDao.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            try
            {
                IEduProgramUniqueKeyUpdateDaemonDao.instance.get().doUpdateEduProgramsUniqueKey();
            }
            catch (final Exception t)
            {
                Debug.exception(t.getMessage(), t);
            }
        }
    };

    /**
     * {@inheritDoc}
     */
    @Override
    public void doUpdateEduProgramsUniqueKey()
    {
        final Session session = getSession();

        final DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EduProgramProf.class, "p"); // ВПО & СПО only
        dql.column(property("p"));
        dql.fetchPath(DQLJoinType.inner, EduProgramProf.baseLevel().fromAlias("p"), "bl");
        dql.fetchPath(DQLJoinType.inner, EduProgramProf.programSubject().fromAlias("p"), "ps");
        dql.fetchPath(DQLJoinType.inner, EduProgramProf.kind().fromAlias("p"), "k");
        dql.fetchPath(DQLJoinType.inner, EduProgramProf.form().fromAlias("p"), "f");
        dql.fetchPath(DQLJoinType.left, EduProgramProf.eduProgramTrait().fromAlias("p"), "ept");
        dql.fetchPath(DQLJoinType.inner, EduProgramProf.duration().fromAlias("p"), "d");

        for (final EduProgram program : dql.createStatement(session).<EduProgramProf>list())
        {
            final String key = program.generateUniqueKey();
            if (!key.equals(program.getProgramUniqueKey()))
            {
                program.setProgramUniqueKey(key);
                session.update(program);
            }
        }
    }
}