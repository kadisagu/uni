/**
 *$Id$
 */
package ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.ui.List.logic;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;

/**
 * @author Alexander Zhebko
 * @since 03.02.2014
 */
public class EduInstitutionOrgUnitListDSHandler extends DefaultSearchDataSourceHandler
{
    public EduInstitutionOrgUnitListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    public DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
        .fromEntity(EduInstitutionOrgUnit.class, "e")
        .column(property("e"))
        .fetchPath(DQLJoinType.inner, EduInstitutionOrgUnit.orgUnit().fromAlias("e"), "ou");

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).order().build();
    }
}