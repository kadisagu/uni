/* $Id$ */
package ru.tandemservice.uniedu.entity;

import org.tandemframework.core.entity.IEntity;

/**
 * @author Ekaterina Zvereva
 * @since 17.11.2016
 */
public interface IPersistentAccreditationOwner extends IEntity
{

    String getDisplayableTitle();
}
