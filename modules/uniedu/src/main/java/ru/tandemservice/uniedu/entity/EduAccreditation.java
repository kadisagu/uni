package ru.tandemservice.uniedu.entity;

import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.entity.gen.*;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;

/** @see ru.tandemservice.uniedu.entity.gen.EduAccreditationGen */
public class EduAccreditation extends EduAccreditationGen
{

    public EduAccreditation()
    {
    }

    public EduAccreditation(IPersistentAccreditationOwner owner, EduInstitutionOrgUnit orgUnit, EduProgramSubjectIndex subjectIndex)
    {
        setOwner(owner);
        setInstitutionOrgUnit(orgUnit);
        setProgramSubjectIndex(subjectIndex);
    }

}