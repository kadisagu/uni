package ru.tandemservice.uniedu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.commonbase.utils.MigrationUtils.BatchInsertBuilder;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused"})
public class MS_uniedu_2x8x0_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eduProgramSubjectIndexGeneration

		// создана новая сущность
		tool.createTable(
				new DBTable("edu_c_pr_subject_index_gen_t",
							new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_4339d04c"),
							new DBColumn("discriminator", DBType.SHORT).setNullable(false),
							new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
							new DBColumn("priority_p", DBType.INTEGER).setNullable(false),
							new DBColumn("title_p", DBType.createVarchar(1200)))
		);

		final short entityCode = tool.entityCodes().ensure("eduProgramSubjectIndexGeneration");

		final BatchInsertBuilder insertGenerations = new BatchInsertBuilder(entityCode, "title_p", "code_p", "priority_p");
		final Long okso = insertGenerations.addRow("ОКСО", "2005", 3);
		final Long fgos2009 = insertGenerations.addRow("2009", "2009", 2);
		final Long fgos2013 = insertGenerations.addRow("2013", "2013", 1);
		insertGenerations.executeInsert(tool, "edu_c_pr_subject_index_gen_t");

		////////////////////////////////////////////////////////////////////////////////
		// сущность eduProgramSubjectIndex

		// свойство orderPosition переименовано в priority
		tool.renameColumn("edu_c_pr_subject_index_t", "orderposition_p", "priority_p");

		// создано обязательное свойство generation
		tool.createColumn("edu_c_pr_subject_index_t", new DBColumn("generation_id", DBType.LONG));
		tool.executeUpdate("update edu_c_pr_subject_index_t set generation_id=? where code_p like '2005.%'", okso);
		tool.executeUpdate("update edu_c_pr_subject_index_t set generation_id=? where code_p like '2009.%'", fgos2009);
		tool.executeUpdate("update edu_c_pr_subject_index_t set generation_id=? where code_p like '2013.%'", fgos2013);
		tool.setColumnNullable("edu_c_pr_subject_index_t", "generation_id", false);

		// добавлен элемент интернатура.

		// Берем вид ОП (создаем, если нет)
		Long internProgKind = MigrationUtils.getIdByCode(tool, "edu_c_program_kind_t", "2013.2.4.3");
		if (internProgKind == null) {
			final BatchInsertBuilder internInsert = new BatchInsertBuilder("title_p", "code_p", "shorttitle_p", "eduprogramsubjectkind_p", "edulevel_id");
			internInsert.setEntityCode(tool, "eduProgramKind");
			internProgKind = internInsert.addRow("Программа интернатуры", "2013.2.4.3", "ВО интернатура (2013)", "Специальность", MigrationUtils.getIdByCode(tool, "c_edu_level_t", "2013.2.4"));
			internInsert.executeInsert(tool, "edu_c_program_kind_t");
		}

		tool.table("edu_c_pr_subject_index_t").constraints().clear();
		Integer ordPriority = (Integer) tool.getUniqueResult("select priority_p from edu_c_pr_subject_index_t where code_p=?", "2013.08");
		tool.executeUpdate("update edu_c_pr_subject_index_t set priority_p=priority_p+1 where priority_p>?", ordPriority);

		final BatchInsertBuilder internSubjIndexInsert = new BatchInsertBuilder("title_p", "code_p", "shorttitle_p", "generation_id", "programkind_id", "priority_p", "prefix_p");
		internSubjIndexInsert.setEntityCode(tool, "eduProgramSubjectIndex");
		final Long internSubjIndex = internSubjIndexInsert.addRow("Перечень специальностей интернатуры 2013", "2013.10", "ВО интернатура (2013)", fgos2013, internProgKind, ordPriority+1, null);
		internSubjIndexInsert.executeInsert(tool, "edu_c_pr_subject_index_t");




		/* **********************************************************

		// Надо сначала думать, а потом делать...
		// --------------
		// То, что ниже, было призвано освободить пользователей от нажатия системного действия для синхронизации направлнений.
		// Но оказалось, что это зло. Поэтому оно закомментарено. Не тестировалось, не отлаживалось.
		// --------------



		// Перечень ординатуры
		final Long ordSubjIndexId = MigrationUtils.getIdByCode(tool, "edu_c_pr_subject_index_t", "2013.08");

		// Доработать справочник «Направления подготовки, профессии и специальности профессионального образования»
		// Необходимо скопировать элементы, связанные с перечнем «Перечень специальностей ординатуры 2013»

		// Получаем направления ординатуры
		final List<Object[]> baseSubjRows = tool.executeQuery(
				MigrationUtils.processor(String.class, String.class, String.class, Date.class, String.class, Long.class, Long.class),
				"select subj.code_p, subj.subjectcode_p, subj.shorttitle_p, subj.outofsyncdate_p, subj.title_p, subj13.group_id, subj.id " +
						"from edu_c_pr_subject_t subj " +
						"inner join edu_c_pr_subject__2013_t subj13 on subj13.id=subj.id " +
						"where subj.subjectindex_id=?", ordSubjIndexId
		);

		// Создаем направления для интернатуры на базе ординатуры
		final BatchInsertBuilder insBaseSubj = new BatchInsertBuilder("code_p", "subjectcode_p", "shorttitle_p", "outofsyncdate_p", "title_p", "subjectindex_id");
		final BatchInsertBuilder ins2013Subj = new BatchInsertBuilder("group_id");
		insBaseSubj.setEntityCode(tool, "eduProgramSubject2013");

		final Map<Long, Long> ordinatura2internaturaSubjMap = new HashMap<>(baseSubjRows.size()); // Сопоставление направления ординатуры новому направлению интернатуры для копирвоания квалификаций

		for (Object[] row : baseSubjRows)
		{
			// Данные направления ординатуры
			String code = (String) row[0];
			String subjCode = (String) row[1];

			// Полный код - это "2013." + код, На всякий случай проверим, что это так, и что длина кода 8 символов, и что в середине стоит .08. Так должно быть для ординатуры.
			if (!subjCode.equals("2013." + code) || code.length() != 8 || code.indexOf(".08.") != 2) {
				throw new IllegalStateException("Invalid inern program subject code (" + code + ") or full code (" + subjCode + ")");
			}

			// Заменяем в коде .08. на .10.
			code = StringUtils.replaceOnce(code, ".08.", ".10.");
			subjCode = "2013." + code;

			// Данные, которые просто копируются
			final String shortTitle = (String) row[2];
			final Date outOfSyncDate = (Date) row[3];
			final String title = (String) row[4];
			final Long groupId = (Long) row[5];

			// Теперь надо модифицировать коды и создать на основе этих данных направление интернатуры
			final Long internSubjId = insBaseSubj.addRow(code, subjCode, shortTitle, outOfSyncDate, title, internSubjIndex);
			ins2013Subj.addRowWithId(internSubjId, groupId);

			final Long ordSubjId = (Long) row[6];
			ordinatura2internaturaSubjMap.put(ordSubjId, internSubjId);
		}
		insBaseSubj.executeInsert(tool, "edu_c_pr_subject_t");
		ins2013Subj.executeInsert(tool, "edu_c_pr_subject__2013_t");


		// Получаем квалификация профессионального образования
		final List<Object[]> progQualificationRows = tool.executeQuery(
				MigrationUtils.processor(String.class, String.class, String.class),
				"select code_p, qualificationlevelcode_p, title_p, id from edu_c_pr_qual_t where subjectindex_id=?", ordSubjIndexId
		);

		// Создаем на их основе квалификации для интернатуры
		final Map<Long, Long> ordinatura2internatursQualifMap = new HashMap<>(progQualificationRows.size());
		final BatchInsertBuilder qualificationInsert = new BatchInsertBuilder("code_p", "qualificationlevelcode_p", "subjectindex_id", "title_p");
		qualificationInsert.setEntityCode(tool, "eduProgramQualification");

		for (Object[] row : progQualificationRows)
		{
			String code = (String) row[0];
			// Код для ординатуры должен начинаться с кода перечня (2013.08). Проверим это и заменим код на код интернатуры
			if (!code.startsWith("2013.08.")) {
				throw new IllegalStateException("Invalid qualification code: " + code);
			}
			code = "2013.10." + code.substring("2013.08.".length());

			final String levelCode = (String) row[1];
			final String title = (String) row[2];
			final Long internQualifId = qualificationInsert.addRow(code, levelCode, internSubjIndex, title);

			final Long ordQualifId = (Long) row[3];
			ordinatura2internatursQualifMap.put(ordQualifId, internQualifId);
		}
		qualificationInsert.executeInsert(tool, "edu_c_pr_qual_t");

		// Теперь квалификация направления подготовки (связь квалификации с направлением)
		final List<Object[]> subjQualificationRows = tool.executeQuery(
				MigrationUtils.processor(Long.class, Long.class),
				"select programsubject_id, programqualification_id from edu_c_pr_subject_qual_t where programsubject_id in (select id from edu_c_pr_subject_t where subjectindex_id=?)", ordSubjIndexId
		);

		final BatchInsertBuilder subjQualifInsert = new BatchInsertBuilder("programsubject_id", "programqualification_id");
		subjQualifInsert.setEntityCode(tool, "eduProgramSubjectQualification");

		for (Object[] row : subjQualificationRows)
		{
			final Long internSubjId = ordinatura2internaturaSubjMap.get((Long) row[0]);
			final Long internQulifId = ordinatura2internatursQualifMap.get((Long) row[1]);

			if (internSubjId == null || internQulifId == null) {
				throw new IllegalStateException(); // ошибка в коде
			}

			subjQualifInsert.addRow(internSubjId, internQulifId);
		}
		subjQualifInsert.executeInsert(tool, "edu_c_pr_subject_qual_t");

		*/
	}
}