package ru.tandemservice.uniedu.migration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.IDBConnect;
import org.tandemframework.dbsupport.ddl.IResultProcessor;
import org.tandemframework.dbsupport.ddl.ResultRowProcessor;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniedu_2x6x6_0to1 extends IndependentMigrationScript
{

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.6")
        };
    }

    // TODO: перенести в DBTool если это будет работать
    public static final IResultProcessor<List<Object[]>> processor(final Class ...classes) {
        return new ResultRowProcessor<List<Object[]>>(new ArrayList<Object[]>()) {
            @Override protected void processRow(IDBConnect ctool, ResultSet rs, List<Object[]> result) throws SQLException {
                final Object[] row = new Object[classes.length];
                for (int i=0;i<classes.length;i++) {
                    final Object v = rs.getObject(1+i);
                    row[i] = (null == v ? null : ConvertUtils.convert(rs.getObject(1+i), classes[i]));
                }
                result.add(row);
            }
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eduProgramSecondaryProf

        // создано свойство programQualification
        {
            // создать колонку
            tool.createColumn("edu_program_prof_secondary_t", new DBColumn("programqualification_id", DBType.LONG));
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eduProgramCompositeQualification

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("edu_program_qual_comp_t",
                new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                new DBColumn("subjectindex_id", DBType.LONG).setNullable(false),
                new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
                new DBColumn("qualificationlevelcode_p", DBType.createVarchar(255))
            );
            tool.createTable(dbt);
        }

        // гарантировать наличие кода сущности
        short eduProgramCompositeQualificationEntityCode = tool.entityCodes().ensure("eduProgramCompositeQualification".toLowerCase());

        // удаляем из edu_program_qual_link_t все, что не СПО
        tool.executeUpdate("delete from edu_program_qual_link_t where program_id not in (select id from edu_program_prof_secondary_t)");

        // program.id -> sorted{ qual.id }
        final Map<Long, Set<Long>> p2qSetMap = new HashMap<>();
        final List<Object[]> qlinkrows = tool.executeQuery(processor(Long.class, Long.class), "select program_id, programqualification_id from edu_program_qual_link_t");
        for (final Object[] row: qlinkrows) {
            SafeMap.safeGet(p2qSetMap, (Long)row[0], TreeSet.class).add((Long)row[1]);
        }

        // пустые ОП
        final List<Object[]> emptyqlinkrows = tool.executeQuery(processor(Long.class), "select id from edu_program_prof_secondary_t where id not in (select program_id from edu_program_qual_link_t)");
        if (emptyqlinkrows.size() > 0) {
            throw new IllegalStateException(/* если они не указали квалификации, то у них стенд не обновится. (с) Оля */);
        }

        // sorted{ qual.id } -> { program.id }
        final Map<Set<Long>, List<Long>> qSet2pListMap = new HashMap<>();
        for (final Map.Entry<Long, Set<Long>> e: p2qSetMap.entrySet()) {
            SafeMap.safeGet(qSet2pListMap, e.getValue(), ArrayList.class).add(e.getKey());
        }

        // окончательно очищаем таблицу edu_program_qual_link_t
        tool.executeUpdate("delete from edu_program_qual_link_t");

        // удалено свойство program
        {
            // удалить колонку
            tool.dropColumn("edu_program_qual_link_t", "program_id");
        }

        // создано обязательное свойство programCompositeQualification
        {
            // создать колонку
            tool.createColumn("edu_program_qual_link_t", new DBColumn("prgrmcmpstqlfctn_id", DBType.LONG));

            // сделать колонку NOT NULL
            tool.setColumnNullable("edu_program_qual_link_t", "prgrmcmpstqlfctn_id", false);
        }

        // гарантировать наличие кода сущности
        short eduProgram2QualificationLinkEntityCode = tool.entityCodes().ensure("eduProgram2QualificationLink".toLowerCase());


        // поехали
        for (Map.Entry<Set<Long>, List<Long>> e: qSet2pListMap.entrySet()) {
            if (e.getKey().isEmpty()) { throw new IllegalArgumentException(); }

            Long compQualId = EntityIDGenerator.generateNewId(eduProgramCompositeQualificationEntityCode);

            final Set<String> titleSet = new TreeSet<String>(); // non unique
            final Set<Long> subjectIndexIds = new HashSet<Long>(2); // have to be unique
            final Set<String> qLevelCodes = new HashSet<String>(2); // have to be unique
            for (Long qId: e.getKey()) {
                final Object[] row = tool.executeQuery(processor(Long.class, String.class, String.class), "select subjectindex_id, qualificationlevelcode_p, title_p from edu_c_pr_qual_t where id=?", qId).iterator().next();
                if (null != row[0]) { subjectIndexIds.add((Long)row[0]); }
                if (null != row[1]) { qLevelCodes.add((String)row[1]); }
                if (null != row[2]) { titleSet.add((String)row[2]); }
            }

            if (subjectIndexIds.size() != 1) { throw new IllegalStateException("subjectIndexIds is not unique: " + subjectIndexIds.toString() + ", e = " + e.toString()); }

            if (qLevelCodes.isEmpty()) { qLevelCodes.add(null); }
            if (qLevelCodes.size() != 1) { throw new IllegalStateException("qLevelCodes is not unique: " + qLevelCodes.toString() + ", e = " + e.toString()); }

            tool.executeUpdate(
                "insert into edu_program_qual_comp_t(id, discriminator, title_p, subjectindex_id, qualificationlevelcode_p) values(?, ?, ?, ?, ?)",
                compQualId,
                eduProgramCompositeQualificationEntityCode,
                StringUtils.join(titleSet, "; "),
                subjectIndexIds.iterator().next(),
                qLevelCodes.iterator().next()
            );

            for (Long qId: e.getKey()) {
                Long id = EntityIDGenerator.generateNewId(eduProgram2QualificationLinkEntityCode);
                tool.executeUpdate(
                    "insert into edu_program_qual_link_t(id, discriminator, prgrmcmpstqlfctn_id, programqualification_id) values (?, ?, ?, ?)",
                    id,
                    eduProgram2QualificationLinkEntityCode,
                    compQualId,
                    qId
                );
            }

            for (Long pId: e.getValue()) {
                if (1 != tool.executeUpdate(
                    "update edu_program_prof_secondary_t set programqualification_id=? where id=?",
                    compQualId, pId
                )) {
                    throw new IllegalStateException();
                }
            }

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eduProgramSecondaryProf

        // свойство programQualification стало обязательным
        {
            // сделать колонку NOT NULL
            tool.setColumnNullable("edu_program_prof_secondary_t", "programqualification_id", false);
        }
    }
}