/**
 *$Id$
 */
package ru.tandemservice.uniedu.orgunit.bo.EduOwnerOrgUnit.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniedu.orgunit.bo.EduOwnerOrgUnit.EduOwnerOrgUnitManager;
import ru.tandemservice.uniedu.orgunit.bo.EduOwnerOrgUnit.ui.Add.EduOwnerOrgUnitAdd;

/**
 * @author Alexander Zhebko
 * @since 04.02.2014
 */
public class EduOwnerOrgUnitListUI extends UIPresenter
{
    public String getEduOwnerOrgUnitDeleteAlert()
    {
        EduOwnerOrgUnit current = getConfig().getDataSource(EduOwnerOrgUnitList.EDU_OWNER_ORG_UNIT_LIST_DS).getCurrent();
        return "Удалить ответственное подразделение «" + current.getOrgUnit().getFullTitle() + "»?";
    }

    public void onClickAddEduOwnerOrgUnit()
    {
        _uiActivation.asRegionDialog(EduOwnerOrgUnitAdd.class).activate();
    }

    public void onClickDeleteEduOwnerOrgUnit()
    {
        EduOwnerOrgUnitManager.instance().dao().deleteEduOwnerOrgUnit(getListenerParameterAsLong());
    }
}