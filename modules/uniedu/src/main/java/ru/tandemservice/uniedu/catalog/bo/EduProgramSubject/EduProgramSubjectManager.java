/* $Id:$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramSubject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.logic.IEduProgramSubjectDao;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.logic.EduProgramSubjectDao;

/**
 * @author oleyba
 * @since 11/20/13
 */
@Configuration
public class EduProgramSubjectManager extends BusinessObjectManager
{
    public static EduProgramSubjectManager instance()
    {
        return instance(EduProgramSubjectManager.class);
    }

    @Bean
    public IEduProgramSubjectDao dao()
    {
        return new EduProgramSubjectDao();
    }
}
