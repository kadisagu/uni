/* $Id:$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramDuration.ui.AddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.meta.entity.data.ItemPolicy;
import org.tandemframework.core.meta.view.IViewButton;
import org.tandemframework.core.runtime.EntityDataRuntime;
import org.tandemframework.core.tool.synchronization.SynchronizeMeta;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemAddEditUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;

import java.util.Collections;
import java.util.List;

/**
 * @author oleyba
 * @since 11/18/13
 */
@Input ({
    @Bind(key = "catalogItemId", binding = "holder.id"),
    @Bind(key = DefaultCatalogAddEditModel.CATALOG_CODE, binding = "catalogCode")
})
public class EduProgramDurationAddEditUI extends BaseCatalogItemAddEditUI<EduProgramDuration>
{

    private IdentifiableWrapper _selectedOption;
    private boolean _system;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        if (!isAddForm())
        {
            EduProgramDuration eduProgramDuration = getCatalogItem();
            if (null != eduProgramDuration)
            {
                _selectedOption = eduProgramDuration.getNumberOfHours() > 0 ?
                        EduProgramDurationAddEdit.USE_HOURS_OPTION : EduProgramDurationAddEdit.USE_YEAR_AND_MONTH_OPTION;
            }

        } else
            _selectedOption = EduProgramDurationAddEdit.USE_YEAR_AND_MONTH_OPTION;

        ItemPolicy itemPolicy = EntityDataRuntime.getEntityPolicy(getCatalogCode()).getItemPolicy(getCatalogItem().getCode());
        setSystem(itemPolicy.getSynchronize() == SynchronizeMeta.system);
    }

    public void onChangeOption()
    {
        if (_selectedOption.equals(EduProgramDurationAddEdit.USE_HOURS_OPTION)) {
            getCatalogItem().setNumberOfMonths(0);
            getCatalogItem().setNumberOfYears(0);
        } else {
            getCatalogItem().setNumberOfHours(0);
        }
    }

    @Override
    public String getAdditionalPropertiesPageName()
    {
        return "ru.tandemservice.uniedu.catalog.bo.EduProgramDuration.ui.AddEdit.EduProgramDurationAdditProps";
    }

    public void onChangeDuration() {
        getCatalogItem().setTitle(CommonBaseDateUtil.getPeriodWithNames(getCatalogItem().getNumberOfYears(), getCatalogItem().getNumberOfMonths(), null, getCatalogItem().getNumberOfHours(), null));
    }

    private static IViewButton REFRESH_TITLE_BUTTON = new IViewButton() {
        @Override public String getListener() { return "onChangeDuration"; }
        @Override public String getAlert() { return null; }
        @Override public String getDisableSecondSubmit() { return null; }
        @Override public String getValidate() { return "false"; }
        @Override public String getRenderAsLink() { return null; }
        @Override public String getClick() { return null; }
        @Override public String getLabel() { return "Обновить название"; }
        @Override public String getId() { return null; }
        @Override public String getName() { return null; }
        @Override public String getBefore() { return null; }
        @Override public String getAfter() { return null; }
        @Override public String getPermissionKey() { return null; }
        @Override public String getVisible() { return null; }
        @Override public String getDisabled() { return null; }
        @Override public String getParameters() { return null; }
        @Override public boolean isReplace() { return false; }
    };

    @Override
    public List<IViewButton> getAdditionalButtons()
    {
        return Collections.singletonList(REFRESH_TITLE_BUTTON);
    }

    public boolean isInYearsAndMonths()
    {
        return _selectedOption.equals(EduProgramDurationAddEdit.USE_YEAR_AND_MONTH_OPTION);
    }

    public IdentifiableWrapper getSelectedOption()
    {
        return _selectedOption;
    }

    public void setSelectedOption(IdentifiableWrapper selectedOption)
    {
        this._selectedOption = selectedOption;
    }

    public boolean isSystem()
    {
        return _system;
    }

    public void setSystem(boolean system)
    {
        this._system = system;
    }
}