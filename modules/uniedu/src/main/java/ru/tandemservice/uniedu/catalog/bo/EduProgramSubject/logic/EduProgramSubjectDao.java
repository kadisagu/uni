/* $Id:$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.logic;

import java.util.List;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSpecialization.logic.SpecializationShortTitleBuilder;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationChild;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationRoot;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification;

/**
 * @author oleyba
 * @since 11/20/13
 */
public class EduProgramSubjectDao extends UniBaseDao implements IEduProgramSubjectDao
{
    @Override
    public EduProgramSpecializationRoot createOrGetProgramSpecializationRoot(EduProgramSubject programSubject)
    {
        this.lock(EduProgramSpecializationRoot.class.getSimpleName() + "_" +EduProgramSubject.class.getSimpleName() + ":" + programSubject.getSubjectIndex().getId());
        List<EduProgramSpecializationRoot> specializationList = this.getList(EduProgramSpecializationRoot.class, EduProgramSpecializationRoot.programSubject().s(), programSubject);
        if (specializationList.isEmpty())
        {
	        EduProgramSpecializationRoot result = new EduProgramSpecializationRoot();
            result.setProgramSubject(programSubject);
            result.setTitle(programSubject.getTitle());
            result.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(EduProgramSpecialization.class /* базовый класс!!!!!*/, "root."+Long.toString(programSubject.getId(), 32), 0));
	        result.setShortTitle(SpecializationShortTitleBuilder.buildRootShortTitle(result));
            save(result);
	        return result;
        }
        if (specializationList.size() == 1)
        {
            return specializationList.get(0);
        }
		// wtf: в базе констрейнт
		throw new IllegalStateException("Duplicate education program subjects.");
    }


    @Override
    public void addQualification(EduProgramSubject subject, String newQualCode, String newQualTitle)
    {
        EduProgramQualification qualification = new EduProgramQualification();
        qualification.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(EduProgramQualification.class));
        qualification.setQualificationLevelCode(newQualCode);
        qualification.setTitle(newQualTitle);
        qualification.setSubjectIndex(subject.getSubjectIndex());
        save(qualification);

        addQualification(subject, qualification);
    }

    @Override
    public void addQualification(EduProgramSubject subject, EduProgramQualification qualification)
    {
        EduProgramSubjectQualification rel = new EduProgramSubjectQualification();
        rel.setProgramSubject(subject);
        rel.setProgramQualification(qualification);
        save(rel);
    }

    @Override
    public void saveOrUpdateSpec(EduProgramSpecializationChild spec)
    {
        if (spec.getCode() == null)
            spec.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(EduProgramSpecialization.class));
        saveOrUpdate(spec);
    }

    @Override
    public void deleteEduProgramSubjectQualification(Long eduProgramSubjectQualificationId)
    {
        EduProgramSubjectQualification eduProgramSubjectQualification = this.get(EduProgramSubjectQualification.class, eduProgramSubjectQualificationId);

        // todo DEV-5818
//        if (existsEntity(
//            EduProgram2QualificationLink.class,
//            EduProgram2QualificationLink.L_PROGRAM_QUALIFICATION, eduProgramSubjectQualification.getProgramQualification(),
//            EduProgram2QualificationLink.program().programSubject().s(), eduProgramSubjectQualification.getProgramSubject()))
//        {
//            ContextLocal.getErrorCollector().add("Нельзя удалить квалификацию, указанную для направления подготовки, т.к. квалификация данного направления подготовки используется в образовательной программе.");
//            return;
//        }

        delete(eduProgramSubjectQualification);
    }
}