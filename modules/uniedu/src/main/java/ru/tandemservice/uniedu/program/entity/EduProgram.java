package ru.tandemservice.uniedu.program.entity;

import org.tandemframework.core.common.ITitled;
import ru.tandemservice.uniedu.program.entity.gen.EduProgramGen;

/**
 * Образовательная программа
 *
 * Образовательная программа, ОП.
 * Реализует направление подготовки в акк.ОУ в некотором году (приема).
 */
public abstract class EduProgram extends EduProgramGen implements ITitled
{
    /**
     * @return StringBuilder, содержащий значения всех ключевых параметров ОП
     */
    protected StringBuilder uniqueKeyBuilder() {
        // При изменении ключа не забыть исправить ProgramKeyUniqueHandler
        return new StringBuilder()
        .append(this.getKind().getCode()).append("/")
        .append(Long.toString(this.getInstitutionOrgUnit().getId(), 32)).append("/")
        .append(Long.toString(this.getOwnerOrgUnit().getId(), 32)).append("/")
        .append(this.getForm().getCode()).append("/")
        .append(this.getEduProgramTrait() == null ? "0" : this.getEduProgramTrait().getCode()).append("/")
        .append(this.getDuration().getCode()).append("/");
    }

    /** @return StringBuilder, содержащий названия всех ключевых параметров ОП */
    protected StringBuilder titleBuilder() {
        return new StringBuilder();
    }

    /** @return уникальный код ОП, сгенерированный на основе параметров ОП */
    public String generateUniqueKey() {
        return uniqueKeyBuilder().toString();
    }

    /** @return название ОП, сгенерированное на основе параметров ОП */
    public String generateTitle() {
        return titleBuilder().toString();
    }

    public String getFullTitle() {
        return generateTitle();
    }

}