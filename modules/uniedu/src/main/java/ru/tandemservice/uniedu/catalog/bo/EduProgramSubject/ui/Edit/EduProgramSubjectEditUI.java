/* $Id$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.ui.Edit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

/**
 * @author azhebko
 * @since 27.06.2014
 */
@Input(@Bind(key = IUIPresenter.PUBLISHER_ID, binding = "holder.id", required = true))
public class EduProgramSubjectEditUI extends UIPresenter
{
    private EntityHolder<EduProgramSubject> _holder = new EntityHolder<>();

    public EntityHolder<EduProgramSubject> getHolder(){ return _holder; }
    public EduProgramSubject getProgramSubject(){ return _holder.getValue(); }

    public void onClickApply()
    {
        IUniBaseDao.instance.get().update(getProgramSubject());
        deactivate();
    }
}