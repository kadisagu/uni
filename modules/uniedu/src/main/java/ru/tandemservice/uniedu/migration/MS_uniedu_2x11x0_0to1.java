package ru.tandemservice.uniedu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniedu_2x11x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eduProgramDuration

		// создано обязательное свойство numberOfHours
		{
			// создать колонку
			tool.createColumn("edu_c_rp_duration_t", new DBColumn("numberofhours_p", DBType.INTEGER));

			// задать значение по умолчанию
			java.lang.Integer defaultNumberOfHours = 0;
			tool.executeUpdate("update edu_c_rp_duration_t set numberofhours_p=? where numberofhours_p is null", defaultNumberOfHours);

			// сделать колонку NOT NULL
			tool.setColumnNullable("edu_c_rp_duration_t", "numberofhours_p", false);

		}


    }
}