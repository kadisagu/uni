package ru.tandemservice.uniedu.catalog.entity.subjects.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOkso;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOksoItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Направление подготовки профессионального образования (ОКСО)
 *
 * Направление классификаторов ОКСО: ОКСО-подобная структура, иерархия общая для всех типов ОП. Перечень УГС глобальный и общий для ОКСО и 2009.
 * Направление подготовки в системе ссылается на элемент общего классификатора направлений (специальностей, профессий).
 * http://wiki.tandemservice.ru/pages/viewpage.action?pageId=17465605
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramSubjectOksoGen extends EduProgramSubject
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOkso";
    public static final String ENTITY_NAME = "eduProgramSubjectOkso";
    public static final int VERSION_HASH = 1285115487;
    private static IEntityMeta ENTITY_META;

    public static final String L_ITEM = "item";

    private EduProgramSubjectOksoItem _item;     // Элемент классификатора

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Элемент классификатора. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubjectOksoItem getItem()
    {
        return _item;
    }

    /**
     * @param item Элемент классификатора. Свойство не может быть null.
     */
    public void setItem(EduProgramSubjectOksoItem item)
    {
        dirty(_item, item);
        _item = item;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduProgramSubjectOksoGen)
        {
            setItem(((EduProgramSubjectOkso)another).getItem());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramSubjectOksoGen> extends EduProgramSubject.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramSubjectOkso.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramSubjectOkso();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "item":
                    return obj.getItem();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "item":
                    obj.setItem((EduProgramSubjectOksoItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "item":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "item":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "item":
                    return EduProgramSubjectOksoItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgramSubjectOkso> _dslPath = new Path<EduProgramSubjectOkso>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramSubjectOkso");
    }
            

    /**
     * @return Элемент классификатора. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOkso#getItem()
     */
    public static EduProgramSubjectOksoItem.Path<EduProgramSubjectOksoItem> item()
    {
        return _dslPath.item();
    }

    public static class Path<E extends EduProgramSubjectOkso> extends EduProgramSubject.Path<E>
    {
        private EduProgramSubjectOksoItem.Path<EduProgramSubjectOksoItem> _item;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Элемент классификатора. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOkso#getItem()
     */
        public EduProgramSubjectOksoItem.Path<EduProgramSubjectOksoItem> item()
        {
            if(_item == null )
                _item = new EduProgramSubjectOksoItem.Path<EduProgramSubjectOksoItem>(L_ITEM, this);
            return _item;
        }

        public Class getEntityClass()
        {
            return EduProgramSubjectOkso.class;
        }

        public String getEntityName()
        {
            return "eduProgramSubjectOkso";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
