package ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.logic;

import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.dql.util.DQLNoReferenceExpressionBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.EduProgramSubjectIndexManager;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.util.EduProgramSubjectShortTitleBuilder;
import ru.tandemservice.uniedu.catalog.entity.subjects.*;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EduProgramSubjectIndexDao extends UniBaseDao implements IEduProgramSubjectIndexDao
{

    @Override
    public void doCleanUpSubjectIndexData()
    {
        // выплняем все, что нужно сделать перед удалением направлений (если есть модули, которые считают, что нужно удалить ссылающиеся объекты - они должны это сделать)
        EduProgramSubjectIndexManager.instance().beforeCleanUpSubjectIndexDataCallbackList()
                .getItems().values().forEach(java.lang.Runnable::run);

        // убиваем все связи с квалификациями для неактуальных направлений
        // направления не актуальны - у них не должно быть связей с квалификациями (даже, если их добавляли руками)
        executeAndClear(
            new DQLDeleteBuilder(EduProgramSubjectQualification.class)
            .where(in(
                property("id"),
                new DQLSelectBuilder()
                .fromEntity(EduProgramSubjectQualification.class, "l")
                .column(property("l.id"))
                .where(isNotNull(property(EduProgramSubjectQualification.programSubject().outOfSyncDate().fromAlias("l"))))
                .where(new DQLNoReferenceExpressionBuilder(EduProgramSubjectQualification.class, "l.id").getExpression())
                .buildQuery()
            ))
        );

        // убиваем все направленности, на которые нет ссылок, ссылающиеся на неактуальные направления
        // направления не актуальны - у них не должно направленностей (даже, если их создали руками)
        executeAndClear(
            new DQLDeleteBuilder(EduProgramSpecialization.class)
            .where(in(
                property("id"),
                new DQLSelectBuilder()
                .fromEntity(EduProgramSpecialization.class, "s")
                .column(property("s.id"))
                .where(isNotNull(property(EduProgramSpecialization.programSubject().outOfSyncDate().fromAlias("s"))))
                .where(new DQLNoReferenceExpressionBuilder(EduProgramSpecialization.class, "s.id").getExpression())
                .buildQuery()
            ))
        );

        // убиваем все неактуальные направления направления, на которые нет ссылок
        executeAndClear(
            new DQLDeleteBuilder(EduProgramSubject.class)
            .where(in(
                property("id"),
                new DQLSelectBuilder()
                .fromEntity(EduProgramSubject.class, "s")
                .column(property("s.id"))
                .where(isNotNull(property(EduProgramSubject.outOfSyncDate().fromAlias("s"))))
                .where(new DQLNoReferenceExpressionBuilder(EduProgramSubject.class, "s.id").getExpression())
                .buildQuery()
            ))
        );

        // убиваем квалификации, на которые нет ссылок (включая ссылки со связей направлений с квалификациями)
        executeAndClear(
            new DQLDeleteBuilder(EduProgramQualification.class)
            .where(in(
                property("id"),
                new DQLSelectBuilder()
                .fromEntity(EduProgramQualification.class, "q")
                .column(property("q.id"))
                .where(new DQLNoReferenceExpressionBuilder(EduProgramQualification.class, "q.id").getExpression())
                .buildQuery()
            ))
        );

        // для общей направленности направления указываем назнвание из направления (оно не редактируется)
        executeAndClear(
            new DQLUpdateBuilder(EduProgramSpecializationRoot.class)
            .set(EduProgramSpecializationRoot.P_TITLE, property(EduProgramSpecializationRoot.programSubject().title()))
            .where(ne(property(EduProgramSpecializationRoot.P_TITLE), property(EduProgramSpecializationRoot.programSubject().title())))
        );

        // выплняем все, что нужно сделать после удаления направлений
        EduProgramSubjectIndexManager.instance().afterCleanUpSubjectIndexDataCallbackList()
                .getItems().values().forEach(java.lang.Runnable::run);
    }


    @Override
    public void updateNewSubjectsShortTitles()
    {
        final Session session = this.getSession();

        final List<Long> ids = new DQLSelectBuilder()
            .fromEntity(EduProgramSubject.class, "s")
            .column(property("s", EduProgramSubject.id()))
            .where(isNull(property("s", EduProgramSubject.shortTitle())))
            .createStatement(session).list();

        for (List<Long> idsPart : Iterables.partition(ids, DQL.MAX_VALUES_ROW_NUMBER)) {
            final List<EduProgramSubject> subjects = EduProgramSubjectIndexDao.this.getList(EduProgramSubject.class, idsPart);
            for (EduProgramSubject subject: subjects)
            {
                String result = EduProgramSubjectShortTitleBuilder.buildShortTitle(subject.getTitle());
                if (StringUtils.isEmpty(result))
                    result = subject.getTitle();

                subject.setShortTitle(result);
                EduProgramSubjectIndexDao.this.update(subject);
            }

            session.flush();
            session.clear();
        }
    }
}
