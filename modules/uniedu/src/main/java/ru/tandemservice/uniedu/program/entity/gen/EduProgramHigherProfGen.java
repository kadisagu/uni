package ru.tandemservice.uniedu.program.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Образовательная программа ВО
 *
 * Высшее образование: реализуется по направлениям подготовки (специальностям) с указанием направленности.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramHigherProfGen extends EduProgramProf
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.program.entity.EduProgramHigherProf";
    public static final String ENTITY_NAME = "eduProgramHigherProf";
    public static final int VERSION_HASH = -1239996525;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROGRAM_SPECIALIZATION = "programSpecialization";
    public static final String L_PROGRAM_ORIENTATION = "programOrientation";

    private EduProgramSpecialization _programSpecialization;     // Направленность
    private EduProgramOrientation _programOrientation;     // Ориентация ОП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * Направленность образовательной программы
     *
     * @return Направленность. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSpecialization getProgramSpecialization()
    {
        return _programSpecialization;
    }

    /**
     * @param programSpecialization Направленность. Свойство не может быть null.
     */
    public void setProgramSpecialization(EduProgramSpecialization programSpecialization)
    {
        dirty(_programSpecialization, programSpecialization);
        _programSpecialization = programSpecialization;
    }

    /**
     * @return Ориентация ОП.
     */
    public EduProgramOrientation getProgramOrientation()
    {
        return _programOrientation;
    }

    /**
     * @param programOrientation Ориентация ОП.
     */
    public void setProgramOrientation(EduProgramOrientation programOrientation)
    {
        dirty(_programOrientation, programOrientation);
        _programOrientation = programOrientation;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduProgramHigherProfGen)
        {
            setProgramSpecialization(((EduProgramHigherProf)another).getProgramSpecialization());
            setProgramOrientation(((EduProgramHigherProf)another).getProgramOrientation());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramHigherProfGen> extends EduProgramProf.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramHigherProf.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramHigherProf();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "programSpecialization":
                    return obj.getProgramSpecialization();
                case "programOrientation":
                    return obj.getProgramOrientation();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "programSpecialization":
                    obj.setProgramSpecialization((EduProgramSpecialization) value);
                    return;
                case "programOrientation":
                    obj.setProgramOrientation((EduProgramOrientation) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "programSpecialization":
                        return true;
                case "programOrientation":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "programSpecialization":
                    return true;
                case "programOrientation":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "programSpecialization":
                    return EduProgramSpecialization.class;
                case "programOrientation":
                    return EduProgramOrientation.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgramHigherProf> _dslPath = new Path<EduProgramHigherProf>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramHigherProf");
    }
            

    /**
     * Направленность образовательной программы
     *
     * @return Направленность. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgramHigherProf#getProgramSpecialization()
     */
    public static EduProgramSpecialization.Path<EduProgramSpecialization> programSpecialization()
    {
        return _dslPath.programSpecialization();
    }

    /**
     * @return Ориентация ОП.
     * @see ru.tandemservice.uniedu.program.entity.EduProgramHigherProf#getProgramOrientation()
     */
    public static EduProgramOrientation.Path<EduProgramOrientation> programOrientation()
    {
        return _dslPath.programOrientation();
    }

    public static class Path<E extends EduProgramHigherProf> extends EduProgramProf.Path<E>
    {
        private EduProgramSpecialization.Path<EduProgramSpecialization> _programSpecialization;
        private EduProgramOrientation.Path<EduProgramOrientation> _programOrientation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * Направленность образовательной программы
     *
     * @return Направленность. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgramHigherProf#getProgramSpecialization()
     */
        public EduProgramSpecialization.Path<EduProgramSpecialization> programSpecialization()
        {
            if(_programSpecialization == null )
                _programSpecialization = new EduProgramSpecialization.Path<EduProgramSpecialization>(L_PROGRAM_SPECIALIZATION, this);
            return _programSpecialization;
        }

    /**
     * @return Ориентация ОП.
     * @see ru.tandemservice.uniedu.program.entity.EduProgramHigherProf#getProgramOrientation()
     */
        public EduProgramOrientation.Path<EduProgramOrientation> programOrientation()
        {
            if(_programOrientation == null )
                _programOrientation = new EduProgramOrientation.Path<EduProgramOrientation>(L_PROGRAM_ORIENTATION, this);
            return _programOrientation;
        }

        public Class getEntityClass()
        {
            return EduProgramHigherProf.class;
        }

        public String getEntityName()
        {
            return "eduProgramHigherProf";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
