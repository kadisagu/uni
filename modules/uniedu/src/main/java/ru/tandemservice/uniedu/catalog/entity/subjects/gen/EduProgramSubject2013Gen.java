package ru.tandemservice.uniedu.catalog.entity.subjects.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Направление подготовки профессионального образования (2013)
 *
 * Направление классификатора 2013 года: по каждому виду ОП свой классификатор.
 * Направление подготовки в системе совпадает с элементом в классификаторе.
 * Перечень областей знаний и укрупненных групп - общий.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramSubject2013Gen extends EduProgramSubject
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013";
    public static final String ENTITY_NAME = "eduProgramSubject2013";
    public static final int VERSION_HASH = 1086488709;
    private static IEntityMeta ENTITY_META;

    public static final String L_GROUP = "group";

    private EduProgramSubject2013Group _group;     // Укрупненная группа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Укрупненная группа. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubject2013Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Укрупненная группа. Свойство не может быть null.
     */
    public void setGroup(EduProgramSubject2013Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduProgramSubject2013Gen)
        {
            setGroup(((EduProgramSubject2013)another).getGroup());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramSubject2013Gen> extends EduProgramSubject.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramSubject2013.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramSubject2013();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "group":
                    return obj.getGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "group":
                    obj.setGroup((EduProgramSubject2013Group) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "group":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "group":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "group":
                    return EduProgramSubject2013Group.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgramSubject2013> _dslPath = new Path<EduProgramSubject2013>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramSubject2013");
    }
            

    /**
     * @return Укрупненная группа. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013#getGroup()
     */
    public static EduProgramSubject2013Group.Path<EduProgramSubject2013Group> group()
    {
        return _dslPath.group();
    }

    public static class Path<E extends EduProgramSubject2013> extends EduProgramSubject.Path<E>
    {
        private EduProgramSubject2013Group.Path<EduProgramSubject2013Group> _group;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Укрупненная группа. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013#getGroup()
     */
        public EduProgramSubject2013Group.Path<EduProgramSubject2013Group> group()
        {
            if(_group == null )
                _group = new EduProgramSubject2013Group.Path<EduProgramSubject2013Group>(L_GROUP, this);
            return _group;
        }

        public Class getEntityClass()
        {
            return EduProgramSubject2013.class;
        }

        public String getEntityName()
        {
            return "eduProgramSubject2013";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
