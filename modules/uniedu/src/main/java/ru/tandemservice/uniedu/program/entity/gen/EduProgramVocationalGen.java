package ru.tandemservice.uniedu.program.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.program.entity.EduProgramPrimary;
import ru.tandemservice.uniedu.program.entity.EduProgramVocational;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Основная программа профессионального обучения
 *
 * Профессиональное обучение: реализуется по профессиям рабочих и должностям служащих.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramVocationalGen extends EduProgramPrimary
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.program.entity.EduProgramVocational";
    public static final String ENTITY_NAME = "eduProgramVocational";
    public static final int VERSION_HASH = 1646451141;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduProgramVocationalGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramVocationalGen> extends EduProgramPrimary.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramVocational.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramVocational();
        }
    }
    private static final Path<EduProgramVocational> _dslPath = new Path<EduProgramVocational>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramVocational");
    }
            

    public static class Path<E extends EduProgramVocational> extends EduProgramPrimary.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EduProgramVocational.class;
        }

        public String getEntityName()
        {
            return "eduProgramVocational";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
