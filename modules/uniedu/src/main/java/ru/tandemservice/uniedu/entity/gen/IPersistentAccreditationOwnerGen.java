package ru.tandemservice.uniedu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.entity.IPersistentAccreditationOwner;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.uniedu.entity.IPersistentAccreditationOwner;

/**
 * Обьект, имеющий аккредитацию
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IPersistentAccreditationOwnerGen extends InterfaceStubBase
 implements IPersistentAccreditationOwner{
    public static final int VERSION_HASH = -1140746655;



    private static final Path<IPersistentAccreditationOwner> _dslPath = new Path<IPersistentAccreditationOwner>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.uniedu.entity.IPersistentAccreditationOwner");
    }
            

    public static class Path<E extends IPersistentAccreditationOwner> extends EntityPath<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return IPersistentAccreditationOwner.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.uniedu.entity.IPersistentAccreditationOwner";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
