/* $Id:$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationChild;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationRoot;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

/**
 * @author oleyba
 * @since 11/20/13
 */
public interface IEduProgramSubjectDao extends INeedPersistenceSupport
{
    /**
     * Подбирает подходящуюю общую направленность, соответствующую направлению подготовки. Если нет такой - создает.
     *
     * @param programSubject направление подготовки
     * @return направленность
     */
    EduProgramSpecializationRoot createOrGetProgramSpecializationRoot(EduProgramSubject programSubject);

    /**
     * Создает новую квалификацию с переданным названием в соотв. перечне и добавляет ее направлению
     * @param subject направление
     * @param newQualCode код новой квалификации
     * @param newQualTitle название новой квалификации
     */
    void addQualification(EduProgramSubject subject, String newQualCode, String newQualTitle);

    /**
     * Добавляет направлению квалификацию
     * @param subject направление
     * @param qualification квалификация
     */
    void addQualification(EduProgramSubject subject, EduProgramQualification qualification);

    /**
     * Сохраняет переданную направленность.
     * @param spec направленность
     */
    void saveOrUpdateSpec(EduProgramSpecializationChild spec);

    /**
     * Удаляет квалификацию направления подготовки.
     * @param eduProgramQualificationId id квалификации направления подготовки
     */
    void deleteEduProgramSubjectQualification(Long eduProgramQualificationId);
}