package ru.tandemservice.uniedu.program.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Образовательная программа
 *
 * Образовательная программа, ОП.
 * Реализует направление подготовки в акк.ОУ в некотором году (приема).
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.program.entity.EduProgram";
    public static final String ENTITY_NAME = "eduProgram";
    public static final int VERSION_HASH = 2062903479;
    private static IEntityMeta ENTITY_META;

    public static final String P_PROGRAM_UNIQUE_KEY = "programUniqueKey";
    public static final String P_TITLE = "title";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String L_KIND = "kind";
    public static final String L_YEAR = "year";
    public static final String L_INSTITUTION_ORG_UNIT = "institutionOrgUnit";
    public static final String L_OWNER_ORG_UNIT = "ownerOrgUnit";
    public static final String L_FORM = "form";
    public static final String L_DURATION = "duration";
    public static final String L_EDU_PROGRAM_TRAIT = "eduProgramTrait";

    private String _programUniqueKey;     // Ключ образовательной программы в рамках года
    private String _title;     // Название
    private String _shortTitle;     // Сокращенное название
    private EduProgramKind _kind;     // Вид образовательной программы
    private EducationYear _year;     // Учебный год начала обучения
    private EduInstitutionOrgUnit _institutionOrgUnit;     // Аккредитованное подразделение
    private EduOwnerOrgUnit _ownerOrgUnit;     // Выпускающее подразделение
    private EduProgramForm _form;     // Форма обучения
    private EduProgramDuration _duration;     // Продолжительность обучения
    private EduProgramTrait _eduProgramTrait;     // Особенность реализации ОП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * Генерируется и проверяется на уникальность в рамках года на основе параметров ОП (включая данные подклассов и связанных объектов)
     * http://wiki.tandemservice.ru/pages/viewpage.action?pageId=23104523
     *
     * @return Ключ образовательной программы в рамках года. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getProgramUniqueKey()
    {
        return _programUniqueKey;
    }

    /**
     * @param programUniqueKey Ключ образовательной программы в рамках года. Свойство не может быть null.
     */
    public void setProgramUniqueKey(String programUniqueKey)
    {
        dirty(_programUniqueKey, programUniqueKey);
        _programUniqueKey = programUniqueKey;
    }

    /**
     * Название образовательной программы
     *
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * Определяет вид ОП, через который определяется принадлежность уровню (виду, подвиду) образования
     *
     * @return Вид образовательной программы. Свойство не может быть null.
     */
    @NotNull
    public EduProgramKind getKind()
    {
        return _kind;
    }

    /**
     * @param kind Вид образовательной программы. Свойство не может быть null.
     */
    public void setKind(EduProgramKind kind)
    {
        dirty(_kind, kind);
        _kind = kind;
    }

    /**
     * Учебный год, в котором начинается обучение по образовательной программе
     *
     * @return Учебный год начала обучения. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getYear()
    {
        return _year;
    }

    /**
     * @param year Учебный год начала обучения. Свойство не может быть null.
     */
    public void setYear(EducationYear year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * Аккредитованное ОУ, в рамках которого происходит обучение
     *
     * @return Аккредитованное подразделение. Свойство не может быть null.
     */
    @NotNull
    public EduInstitutionOrgUnit getInstitutionOrgUnit()
    {
        return _institutionOrgUnit;
    }

    /**
     * @param institutionOrgUnit Аккредитованное подразделение. Свойство не может быть null.
     */
    public void setInstitutionOrgUnit(EduInstitutionOrgUnit institutionOrgUnit)
    {
        dirty(_institutionOrgUnit, institutionOrgUnit);
        _institutionOrgUnit = institutionOrgUnit;
    }

    /**
     * Подразделение, ответственное за выпуск по образовательной программе
     *
     * @return Выпускающее подразделение. Свойство не может быть null.
     */
    @NotNull
    public EduOwnerOrgUnit getOwnerOrgUnit()
    {
        return _ownerOrgUnit;
    }

    /**
     * @param ownerOrgUnit Выпускающее подразделение. Свойство не может быть null.
     */
    public void setOwnerOrgUnit(EduOwnerOrgUnit ownerOrgUnit)
    {
        dirty(_ownerOrgUnit, ownerOrgUnit);
        _ownerOrgUnit = ownerOrgUnit;
    }

    /**
     * Форма обучения образовательной программы
     *
     * @return Форма обучения. Свойство не может быть null.
     */
    @NotNull
    public EduProgramForm getForm()
    {
        return _form;
    }

    /**
     * @param form Форма обучения. Свойство не может быть null.
     */
    public void setForm(EduProgramForm form)
    {
        dirty(_form, form);
        _form = form;
    }

    /**
     * Продолжительность обучения по образовательной программе
     *
     * @return Продолжительность обучения. Свойство не может быть null.
     */
    @NotNull
    public EduProgramDuration getDuration()
    {
        return _duration;
    }

    /**
     * @param duration Продолжительность обучения. Свойство не может быть null.
     */
    public void setDuration(EduProgramDuration duration)
    {
        dirty(_duration, duration);
        _duration = duration;
    }

    /**
     * Особенность реализации ОП
     *
     * @return Особенность реализации ОП.
     */
    public EduProgramTrait getEduProgramTrait()
    {
        return _eduProgramTrait;
    }

    /**
     * @param eduProgramTrait Особенность реализации ОП.
     */
    public void setEduProgramTrait(EduProgramTrait eduProgramTrait)
    {
        dirty(_eduProgramTrait, eduProgramTrait);
        _eduProgramTrait = eduProgramTrait;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EduProgramGen)
        {
            setProgramUniqueKey(((EduProgram)another).getProgramUniqueKey());
            setTitle(((EduProgram)another).getTitle());
            setShortTitle(((EduProgram)another).getShortTitle());
            setKind(((EduProgram)another).getKind());
            setYear(((EduProgram)another).getYear());
            setInstitutionOrgUnit(((EduProgram)another).getInstitutionOrgUnit());
            setOwnerOrgUnit(((EduProgram)another).getOwnerOrgUnit());
            setForm(((EduProgram)another).getForm());
            setDuration(((EduProgram)another).getDuration());
            setEduProgramTrait(((EduProgram)another).getEduProgramTrait());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgram.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EduProgram is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "programUniqueKey":
                    return obj.getProgramUniqueKey();
                case "title":
                    return obj.getTitle();
                case "shortTitle":
                    return obj.getShortTitle();
                case "kind":
                    return obj.getKind();
                case "year":
                    return obj.getYear();
                case "institutionOrgUnit":
                    return obj.getInstitutionOrgUnit();
                case "ownerOrgUnit":
                    return obj.getOwnerOrgUnit();
                case "form":
                    return obj.getForm();
                case "duration":
                    return obj.getDuration();
                case "eduProgramTrait":
                    return obj.getEduProgramTrait();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "programUniqueKey":
                    obj.setProgramUniqueKey((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "kind":
                    obj.setKind((EduProgramKind) value);
                    return;
                case "year":
                    obj.setYear((EducationYear) value);
                    return;
                case "institutionOrgUnit":
                    obj.setInstitutionOrgUnit((EduInstitutionOrgUnit) value);
                    return;
                case "ownerOrgUnit":
                    obj.setOwnerOrgUnit((EduOwnerOrgUnit) value);
                    return;
                case "form":
                    obj.setForm((EduProgramForm) value);
                    return;
                case "duration":
                    obj.setDuration((EduProgramDuration) value);
                    return;
                case "eduProgramTrait":
                    obj.setEduProgramTrait((EduProgramTrait) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "programUniqueKey":
                        return true;
                case "title":
                        return true;
                case "shortTitle":
                        return true;
                case "kind":
                        return true;
                case "year":
                        return true;
                case "institutionOrgUnit":
                        return true;
                case "ownerOrgUnit":
                        return true;
                case "form":
                        return true;
                case "duration":
                        return true;
                case "eduProgramTrait":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "programUniqueKey":
                    return true;
                case "title":
                    return true;
                case "shortTitle":
                    return true;
                case "kind":
                    return true;
                case "year":
                    return true;
                case "institutionOrgUnit":
                    return true;
                case "ownerOrgUnit":
                    return true;
                case "form":
                    return true;
                case "duration":
                    return true;
                case "eduProgramTrait":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "programUniqueKey":
                    return String.class;
                case "title":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "kind":
                    return EduProgramKind.class;
                case "year":
                    return EducationYear.class;
                case "institutionOrgUnit":
                    return EduInstitutionOrgUnit.class;
                case "ownerOrgUnit":
                    return EduOwnerOrgUnit.class;
                case "form":
                    return EduProgramForm.class;
                case "duration":
                    return EduProgramDuration.class;
                case "eduProgramTrait":
                    return EduProgramTrait.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgram> _dslPath = new Path<EduProgram>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgram");
    }
            

    /**
     * Генерируется и проверяется на уникальность в рамках года на основе параметров ОП (включая данные подклассов и связанных объектов)
     * http://wiki.tandemservice.ru/pages/viewpage.action?pageId=23104523
     *
     * @return Ключ образовательной программы в рамках года. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getProgramUniqueKey()
     */
    public static PropertyPath<String> programUniqueKey()
    {
        return _dslPath.programUniqueKey();
    }

    /**
     * Название образовательной программы
     *
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * Определяет вид ОП, через который определяется принадлежность уровню (виду, подвиду) образования
     *
     * @return Вид образовательной программы. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getKind()
     */
    public static EduProgramKind.Path<EduProgramKind> kind()
    {
        return _dslPath.kind();
    }

    /**
     * Учебный год, в котором начинается обучение по образовательной программе
     *
     * @return Учебный год начала обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getYear()
     */
    public static EducationYear.Path<EducationYear> year()
    {
        return _dslPath.year();
    }

    /**
     * Аккредитованное ОУ, в рамках которого происходит обучение
     *
     * @return Аккредитованное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getInstitutionOrgUnit()
     */
    public static EduInstitutionOrgUnit.Path<EduInstitutionOrgUnit> institutionOrgUnit()
    {
        return _dslPath.institutionOrgUnit();
    }

    /**
     * Подразделение, ответственное за выпуск по образовательной программе
     *
     * @return Выпускающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getOwnerOrgUnit()
     */
    public static EduOwnerOrgUnit.Path<EduOwnerOrgUnit> ownerOrgUnit()
    {
        return _dslPath.ownerOrgUnit();
    }

    /**
     * Форма обучения образовательной программы
     *
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getForm()
     */
    public static EduProgramForm.Path<EduProgramForm> form()
    {
        return _dslPath.form();
    }

    /**
     * Продолжительность обучения по образовательной программе
     *
     * @return Продолжительность обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getDuration()
     */
    public static EduProgramDuration.Path<EduProgramDuration> duration()
    {
        return _dslPath.duration();
    }

    /**
     * Особенность реализации ОП
     *
     * @return Особенность реализации ОП.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getEduProgramTrait()
     */
    public static EduProgramTrait.Path<EduProgramTrait> eduProgramTrait()
    {
        return _dslPath.eduProgramTrait();
    }

    public static class Path<E extends EduProgram> extends EntityPath<E>
    {
        private PropertyPath<String> _programUniqueKey;
        private PropertyPath<String> _title;
        private PropertyPath<String> _shortTitle;
        private EduProgramKind.Path<EduProgramKind> _kind;
        private EducationYear.Path<EducationYear> _year;
        private EduInstitutionOrgUnit.Path<EduInstitutionOrgUnit> _institutionOrgUnit;
        private EduOwnerOrgUnit.Path<EduOwnerOrgUnit> _ownerOrgUnit;
        private EduProgramForm.Path<EduProgramForm> _form;
        private EduProgramDuration.Path<EduProgramDuration> _duration;
        private EduProgramTrait.Path<EduProgramTrait> _eduProgramTrait;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * Генерируется и проверяется на уникальность в рамках года на основе параметров ОП (включая данные подклассов и связанных объектов)
     * http://wiki.tandemservice.ru/pages/viewpage.action?pageId=23104523
     *
     * @return Ключ образовательной программы в рамках года. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getProgramUniqueKey()
     */
        public PropertyPath<String> programUniqueKey()
        {
            if(_programUniqueKey == null )
                _programUniqueKey = new PropertyPath<String>(EduProgramGen.P_PROGRAM_UNIQUE_KEY, this);
            return _programUniqueKey;
        }

    /**
     * Название образовательной программы
     *
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EduProgramGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EduProgramGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * Определяет вид ОП, через который определяется принадлежность уровню (виду, подвиду) образования
     *
     * @return Вид образовательной программы. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getKind()
     */
        public EduProgramKind.Path<EduProgramKind> kind()
        {
            if(_kind == null )
                _kind = new EduProgramKind.Path<EduProgramKind>(L_KIND, this);
            return _kind;
        }

    /**
     * Учебный год, в котором начинается обучение по образовательной программе
     *
     * @return Учебный год начала обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getYear()
     */
        public EducationYear.Path<EducationYear> year()
        {
            if(_year == null )
                _year = new EducationYear.Path<EducationYear>(L_YEAR, this);
            return _year;
        }

    /**
     * Аккредитованное ОУ, в рамках которого происходит обучение
     *
     * @return Аккредитованное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getInstitutionOrgUnit()
     */
        public EduInstitutionOrgUnit.Path<EduInstitutionOrgUnit> institutionOrgUnit()
        {
            if(_institutionOrgUnit == null )
                _institutionOrgUnit = new EduInstitutionOrgUnit.Path<EduInstitutionOrgUnit>(L_INSTITUTION_ORG_UNIT, this);
            return _institutionOrgUnit;
        }

    /**
     * Подразделение, ответственное за выпуск по образовательной программе
     *
     * @return Выпускающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getOwnerOrgUnit()
     */
        public EduOwnerOrgUnit.Path<EduOwnerOrgUnit> ownerOrgUnit()
        {
            if(_ownerOrgUnit == null )
                _ownerOrgUnit = new EduOwnerOrgUnit.Path<EduOwnerOrgUnit>(L_OWNER_ORG_UNIT, this);
            return _ownerOrgUnit;
        }

    /**
     * Форма обучения образовательной программы
     *
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getForm()
     */
        public EduProgramForm.Path<EduProgramForm> form()
        {
            if(_form == null )
                _form = new EduProgramForm.Path<EduProgramForm>(L_FORM, this);
            return _form;
        }

    /**
     * Продолжительность обучения по образовательной программе
     *
     * @return Продолжительность обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getDuration()
     */
        public EduProgramDuration.Path<EduProgramDuration> duration()
        {
            if(_duration == null )
                _duration = new EduProgramDuration.Path<EduProgramDuration>(L_DURATION, this);
            return _duration;
        }

    /**
     * Особенность реализации ОП
     *
     * @return Особенность реализации ОП.
     * @see ru.tandemservice.uniedu.program.entity.EduProgram#getEduProgramTrait()
     */
        public EduProgramTrait.Path<EduProgramTrait> eduProgramTrait()
        {
            if(_eduProgramTrait == null )
                _eduProgramTrait = new EduProgramTrait.Path<EduProgramTrait>(L_EDU_PROGRAM_TRAIT, this);
            return _eduProgramTrait;
        }

        public Class getEntityClass()
        {
            return EduProgram.class;
        }

        public String getEntityName()
        {
            return "eduProgram";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
