package ru.tandemservice.uniedu.program.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramAdditional;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дополнительная образовательная программа
 *
 * Дополнительное образование: реализуются на усмотрение ОУ, определяется видом и названием.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramAdditionalGen extends EduProgram
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.program.entity.EduProgramAdditional";
    public static final String ENTITY_NAME = "eduProgramAdditional";
    public static final int VERSION_HASH = -1103408293;
    private static IEntityMeta ENTITY_META;

    public static final String P_FULL_TITLE = "fullTitle";


    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduProgramAdditionalGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramAdditionalGen> extends EduProgram.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramAdditional.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EduProgramAdditional is abstract");
        }
    }
    private static final Path<EduProgramAdditional> _dslPath = new Path<EduProgramAdditional>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramAdditional");
    }
            

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramAdditional#getFullTitle()
     */
    public static SupportedPropertyPath<String> fullTitle()
    {
        return _dslPath.fullTitle();
    }

    public static class Path<E extends EduProgramAdditional> extends EduProgram.Path<E>
    {
        private SupportedPropertyPath<String> _fullTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramAdditional#getFullTitle()
     */
        public SupportedPropertyPath<String> fullTitle()
        {
            if(_fullTitle == null )
                _fullTitle = new SupportedPropertyPath<String>(EduProgramAdditionalGen.P_FULL_TITLE, this);
            return _fullTitle;
        }

        public Class getEntityClass()
        {
            return EduProgramAdditional.class;
        }

        public String getEntityName()
        {
            return "eduProgramAdditional";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getFullTitle();
}
