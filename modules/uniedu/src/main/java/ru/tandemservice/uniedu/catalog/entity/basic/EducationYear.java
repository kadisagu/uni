package ru.tandemservice.uniedu.catalog.entity.basic;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniedu.catalog.entity.basic.gen.EducationYearGen;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * Учебный год
 */
public class EducationYear extends EducationYearGen implements ISelectableByEducationYear
{
    public static final String PARAM_EDU_YEAR_ONLY_CURRENT = "eduYearOnlyCurrent";

    @Override
    public EducationYear getEducationYear()
    {
        return this;
    }

    public static EducationYear getCurrentRequired() {
        return EducationYearManager.instance().getCurrentRequired();
    }

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EducationYear.class)
                .customize((alias, dql, context, filter) -> {
                    final Object currentOnly = context.get(PARAM_EDU_YEAR_ONLY_CURRENT);
                    if (Boolean.TRUE.equals(currentOnly))
                        dql.where(eq(property(EducationYear.current().fromAlias(alias)), value(Boolean.TRUE)));

                    return dql;
                })
                .titleProperty(EducationYear.title().s())
                .filter(EducationYear.title())
                .order(EducationYear.intValue());
    }

}
