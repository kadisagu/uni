package ru.tandemservice.uniedu.program.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification;
import ru.tandemservice.uniedu.program.entity.EduProgramPrimary;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Основная профессиональная образовательная программа
 *
 * Профессиональное образование: реализуется по направлениям подготовки (специальностям, профессиям).
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramProfGen extends EduProgramPrimary
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.program.entity.EduProgramProf";
    public static final String ENTITY_NAME = "eduProgramProf";
    public static final int VERSION_HASH = -1119390821;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE_LEVEL = "baseLevel";
    public static final String L_PROGRAM_SUBJECT = "programSubject";
    public static final String L_PROGRAM_QUALIFICATION = "programQualification";
    public static final String L_SUBJECT_QUALIFICATION = "subjectQualification";
    public static final String P_IMPL_CONDITIONS = "implConditions";
    public static final String P_IMPL_CONDITIONS_SHORT = "implConditionsShort";
    public static final String P_IMPL_CONDITIONS_SHORT_WITH_FORM = "implConditionsShortWithForm";
    public static final String P_IMPL_CONDITIONS_WITH_FORM = "implConditionsWithForm";
    public static final String P_SHORT_TITLE_AND_CONDITIONS_SHORT = "shortTitleAndConditionsShort";
    public static final String P_SHORT_TITLE_AND_CONDITIONS_SHORT_WITH_FORM = "shortTitleAndConditionsShortWithForm";
    public static final String P_SHORT_TITLE_WITH_CODE_AND_CONDITIONS_SHORT = "shortTitleWithCodeAndConditionsShort";
    public static final String P_SHORT_TITLE_WITH_CODE_AND_CONDITIONS_SHORT_WITH_FORM = "shortTitleWithCodeAndConditionsShortWithForm";
    public static final String P_TITLE_AND_CONDITIONS = "titleAndConditions";
    public static final String P_TITLE_AND_CONDITIONS_SHORT = "titleAndConditionsShort";
    public static final String P_TITLE_AND_CONDITIONS_SHORT_WITH_FORM = "titleAndConditionsShortWithForm";
    public static final String P_TITLE_AND_CONDITIONS_WITH_FORM = "titleAndConditionsWithForm";
    public static final String P_TITLE_WITH_CODE_AND_CONDITIONS = "titleWithCodeAndConditions";
    public static final String P_TITLE_WITH_CODE_AND_CONDITIONS_SHORT = "titleWithCodeAndConditionsShort";
    public static final String P_TITLE_WITH_CODE_AND_CONDITIONS_SHORT_WITH_FORM = "titleWithCodeAndConditionsShortWithForm";
    public static final String P_TITLE_WITH_CODE_AND_CONDITIONS_WITH_FORM = "titleWithCodeAndConditionsWithForm";

    private EduLevel _baseLevel;     // На базе
    private EduProgramSubject _programSubject;     // Направление подготовки
    private EduProgramQualification _programQualification;     // Квалификация
    private EduProgramSubjectQualification _subjectQualification;     // Квалификации направления подготовки (для констрейнта)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * Необходимый уровень для обучения по образовательной программе
     *
     * @return На базе. Свойство не может быть null.
     */
    @NotNull
    public EduLevel getBaseLevel()
    {
        return _baseLevel;
    }

    /**
     * @param baseLevel На базе. Свойство не может быть null.
     */
    public void setBaseLevel(EduLevel baseLevel)
    {
        dirty(_baseLevel, baseLevel);
        _baseLevel = baseLevel;
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubject getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление подготовки. Свойство не может быть null.
     */
    public void setProgramSubject(EduProgramSubject programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    /**
     * @return Квалификация. Свойство не может быть null.
     */
    @NotNull
    public EduProgramQualification getProgramQualification()
    {
        return _programQualification;
    }

    /**
     * @param programQualification Квалификация. Свойство не может быть null.
     */
    public void setProgramQualification(EduProgramQualification programQualification)
    {
        dirty(_programQualification, programQualification);
        _programQualification = programQualification;
    }

    /**
     * Связь направления подготовки и квалификации. Ссылка нужна для констрейнта, чтобы нельзя было эту связь удалить.
     *
     * @return Квалификации направления подготовки (для констрейнта). Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubjectQualification getSubjectQualification()
    {
        return _subjectQualification;
    }

    /**
     * @param subjectQualification Квалификации направления подготовки (для констрейнта). Свойство не может быть null.
     */
    public void setSubjectQualification(EduProgramSubjectQualification subjectQualification)
    {
        dirty(_subjectQualification, subjectQualification);
        _subjectQualification = subjectQualification;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduProgramProfGen)
        {
            setBaseLevel(((EduProgramProf)another).getBaseLevel());
            setProgramSubject(((EduProgramProf)another).getProgramSubject());
            setProgramQualification(((EduProgramProf)another).getProgramQualification());
            setSubjectQualification(((EduProgramProf)another).getSubjectQualification());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramProfGen> extends EduProgramPrimary.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramProf.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EduProgramProf is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "baseLevel":
                    return obj.getBaseLevel();
                case "programSubject":
                    return obj.getProgramSubject();
                case "programQualification":
                    return obj.getProgramQualification();
                case "subjectQualification":
                    return obj.getSubjectQualification();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "baseLevel":
                    obj.setBaseLevel((EduLevel) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((EduProgramSubject) value);
                    return;
                case "programQualification":
                    obj.setProgramQualification((EduProgramQualification) value);
                    return;
                case "subjectQualification":
                    obj.setSubjectQualification((EduProgramSubjectQualification) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "baseLevel":
                        return true;
                case "programSubject":
                        return true;
                case "programQualification":
                        return true;
                case "subjectQualification":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "baseLevel":
                    return true;
                case "programSubject":
                    return true;
                case "programQualification":
                    return true;
                case "subjectQualification":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "baseLevel":
                    return EduLevel.class;
                case "programSubject":
                    return EduProgramSubject.class;
                case "programQualification":
                    return EduProgramQualification.class;
                case "subjectQualification":
                    return EduProgramSubjectQualification.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgramProf> _dslPath = new Path<EduProgramProf>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramProf");
    }
            

    /**
     * Необходимый уровень для обучения по образовательной программе
     *
     * @return На базе. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getBaseLevel()
     */
    public static EduLevel.Path<EduLevel> baseLevel()
    {
        return _dslPath.baseLevel();
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getProgramSubject()
     */
    public static EduProgramSubject.Path<EduProgramSubject> programSubject()
    {
        return _dslPath.programSubject();
    }

    /**
     * @return Квалификация. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getProgramQualification()
     */
    public static EduProgramQualification.Path<EduProgramQualification> programQualification()
    {
        return _dslPath.programQualification();
    }

    /**
     * Связь направления подготовки и квалификации. Ссылка нужна для констрейнта, чтобы нельзя было эту связь удалить.
     *
     * @return Квалификации направления подготовки (для констрейнта). Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getSubjectQualification()
     */
    public static EduProgramSubjectQualification.Path<EduProgramSubjectQualification> subjectQualification()
    {
        return _dslPath.subjectQualification();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getImplConditions()
     */
    public static SupportedPropertyPath<String> implConditions()
    {
        return _dslPath.implConditions();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getImplConditionsShort()
     */
    public static SupportedPropertyPath<String> implConditionsShort()
    {
        return _dslPath.implConditionsShort();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getImplConditionsShortWithForm()
     */
    public static SupportedPropertyPath<String> implConditionsShortWithForm()
    {
        return _dslPath.implConditionsShortWithForm();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getImplConditionsWithForm()
     */
    public static SupportedPropertyPath<String> implConditionsWithForm()
    {
        return _dslPath.implConditionsWithForm();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getShortTitleAndConditionsShort()
     */
    public static SupportedPropertyPath<String> shortTitleAndConditionsShort()
    {
        return _dslPath.shortTitleAndConditionsShort();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getShortTitleAndConditionsShortWithForm()
     */
    public static SupportedPropertyPath<String> shortTitleAndConditionsShortWithForm()
    {
        return _dslPath.shortTitleAndConditionsShortWithForm();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getShortTitleWithCodeAndConditionsShort()
     */
    public static SupportedPropertyPath<String> shortTitleWithCodeAndConditionsShort()
    {
        return _dslPath.shortTitleWithCodeAndConditionsShort();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getShortTitleWithCodeAndConditionsShortWithForm()
     */
    public static SupportedPropertyPath<String> shortTitleWithCodeAndConditionsShortWithForm()
    {
        return _dslPath.shortTitleWithCodeAndConditionsShortWithForm();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getTitleAndConditions()
     */
    public static SupportedPropertyPath<String> titleAndConditions()
    {
        return _dslPath.titleAndConditions();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getTitleAndConditionsShort()
     */
    public static SupportedPropertyPath<String> titleAndConditionsShort()
    {
        return _dslPath.titleAndConditionsShort();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getTitleAndConditionsShortWithForm()
     */
    public static SupportedPropertyPath<String> titleAndConditionsShortWithForm()
    {
        return _dslPath.titleAndConditionsShortWithForm();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getTitleAndConditionsWithForm()
     */
    public static SupportedPropertyPath<String> titleAndConditionsWithForm()
    {
        return _dslPath.titleAndConditionsWithForm();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getTitleWithCodeAndConditions()
     */
    public static SupportedPropertyPath<String> titleWithCodeAndConditions()
    {
        return _dslPath.titleWithCodeAndConditions();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getTitleWithCodeAndConditionsShort()
     */
    public static SupportedPropertyPath<String> titleWithCodeAndConditionsShort()
    {
        return _dslPath.titleWithCodeAndConditionsShort();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getTitleWithCodeAndConditionsShortWithForm()
     */
    public static SupportedPropertyPath<String> titleWithCodeAndConditionsShortWithForm()
    {
        return _dslPath.titleWithCodeAndConditionsShortWithForm();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getTitleWithCodeAndConditionsWithForm()
     */
    public static SupportedPropertyPath<String> titleWithCodeAndConditionsWithForm()
    {
        return _dslPath.titleWithCodeAndConditionsWithForm();
    }

    public static class Path<E extends EduProgramProf> extends EduProgramPrimary.Path<E>
    {
        private EduLevel.Path<EduLevel> _baseLevel;
        private EduProgramSubject.Path<EduProgramSubject> _programSubject;
        private EduProgramQualification.Path<EduProgramQualification> _programQualification;
        private EduProgramSubjectQualification.Path<EduProgramSubjectQualification> _subjectQualification;
        private SupportedPropertyPath<String> _implConditions;
        private SupportedPropertyPath<String> _implConditionsShort;
        private SupportedPropertyPath<String> _implConditionsShortWithForm;
        private SupportedPropertyPath<String> _implConditionsWithForm;
        private SupportedPropertyPath<String> _shortTitleAndConditionsShort;
        private SupportedPropertyPath<String> _shortTitleAndConditionsShortWithForm;
        private SupportedPropertyPath<String> _shortTitleWithCodeAndConditionsShort;
        private SupportedPropertyPath<String> _shortTitleWithCodeAndConditionsShortWithForm;
        private SupportedPropertyPath<String> _titleAndConditions;
        private SupportedPropertyPath<String> _titleAndConditionsShort;
        private SupportedPropertyPath<String> _titleAndConditionsShortWithForm;
        private SupportedPropertyPath<String> _titleAndConditionsWithForm;
        private SupportedPropertyPath<String> _titleWithCodeAndConditions;
        private SupportedPropertyPath<String> _titleWithCodeAndConditionsShort;
        private SupportedPropertyPath<String> _titleWithCodeAndConditionsShortWithForm;
        private SupportedPropertyPath<String> _titleWithCodeAndConditionsWithForm;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * Необходимый уровень для обучения по образовательной программе
     *
     * @return На базе. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getBaseLevel()
     */
        public EduLevel.Path<EduLevel> baseLevel()
        {
            if(_baseLevel == null )
                _baseLevel = new EduLevel.Path<EduLevel>(L_BASE_LEVEL, this);
            return _baseLevel;
        }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getProgramSubject()
     */
        public EduProgramSubject.Path<EduProgramSubject> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new EduProgramSubject.Path<EduProgramSubject>(L_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

    /**
     * @return Квалификация. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getProgramQualification()
     */
        public EduProgramQualification.Path<EduProgramQualification> programQualification()
        {
            if(_programQualification == null )
                _programQualification = new EduProgramQualification.Path<EduProgramQualification>(L_PROGRAM_QUALIFICATION, this);
            return _programQualification;
        }

    /**
     * Связь направления подготовки и квалификации. Ссылка нужна для констрейнта, чтобы нельзя было эту связь удалить.
     *
     * @return Квалификации направления подготовки (для констрейнта). Свойство не может быть null.
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getSubjectQualification()
     */
        public EduProgramSubjectQualification.Path<EduProgramSubjectQualification> subjectQualification()
        {
            if(_subjectQualification == null )
                _subjectQualification = new EduProgramSubjectQualification.Path<EduProgramSubjectQualification>(L_SUBJECT_QUALIFICATION, this);
            return _subjectQualification;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getImplConditions()
     */
        public SupportedPropertyPath<String> implConditions()
        {
            if(_implConditions == null )
                _implConditions = new SupportedPropertyPath<String>(EduProgramProfGen.P_IMPL_CONDITIONS, this);
            return _implConditions;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getImplConditionsShort()
     */
        public SupportedPropertyPath<String> implConditionsShort()
        {
            if(_implConditionsShort == null )
                _implConditionsShort = new SupportedPropertyPath<String>(EduProgramProfGen.P_IMPL_CONDITIONS_SHORT, this);
            return _implConditionsShort;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getImplConditionsShortWithForm()
     */
        public SupportedPropertyPath<String> implConditionsShortWithForm()
        {
            if(_implConditionsShortWithForm == null )
                _implConditionsShortWithForm = new SupportedPropertyPath<String>(EduProgramProfGen.P_IMPL_CONDITIONS_SHORT_WITH_FORM, this);
            return _implConditionsShortWithForm;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getImplConditionsWithForm()
     */
        public SupportedPropertyPath<String> implConditionsWithForm()
        {
            if(_implConditionsWithForm == null )
                _implConditionsWithForm = new SupportedPropertyPath<String>(EduProgramProfGen.P_IMPL_CONDITIONS_WITH_FORM, this);
            return _implConditionsWithForm;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getShortTitleAndConditionsShort()
     */
        public SupportedPropertyPath<String> shortTitleAndConditionsShort()
        {
            if(_shortTitleAndConditionsShort == null )
                _shortTitleAndConditionsShort = new SupportedPropertyPath<String>(EduProgramProfGen.P_SHORT_TITLE_AND_CONDITIONS_SHORT, this);
            return _shortTitleAndConditionsShort;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getShortTitleAndConditionsShortWithForm()
     */
        public SupportedPropertyPath<String> shortTitleAndConditionsShortWithForm()
        {
            if(_shortTitleAndConditionsShortWithForm == null )
                _shortTitleAndConditionsShortWithForm = new SupportedPropertyPath<String>(EduProgramProfGen.P_SHORT_TITLE_AND_CONDITIONS_SHORT_WITH_FORM, this);
            return _shortTitleAndConditionsShortWithForm;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getShortTitleWithCodeAndConditionsShort()
     */
        public SupportedPropertyPath<String> shortTitleWithCodeAndConditionsShort()
        {
            if(_shortTitleWithCodeAndConditionsShort == null )
                _shortTitleWithCodeAndConditionsShort = new SupportedPropertyPath<String>(EduProgramProfGen.P_SHORT_TITLE_WITH_CODE_AND_CONDITIONS_SHORT, this);
            return _shortTitleWithCodeAndConditionsShort;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getShortTitleWithCodeAndConditionsShortWithForm()
     */
        public SupportedPropertyPath<String> shortTitleWithCodeAndConditionsShortWithForm()
        {
            if(_shortTitleWithCodeAndConditionsShortWithForm == null )
                _shortTitleWithCodeAndConditionsShortWithForm = new SupportedPropertyPath<String>(EduProgramProfGen.P_SHORT_TITLE_WITH_CODE_AND_CONDITIONS_SHORT_WITH_FORM, this);
            return _shortTitleWithCodeAndConditionsShortWithForm;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getTitleAndConditions()
     */
        public SupportedPropertyPath<String> titleAndConditions()
        {
            if(_titleAndConditions == null )
                _titleAndConditions = new SupportedPropertyPath<String>(EduProgramProfGen.P_TITLE_AND_CONDITIONS, this);
            return _titleAndConditions;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getTitleAndConditionsShort()
     */
        public SupportedPropertyPath<String> titleAndConditionsShort()
        {
            if(_titleAndConditionsShort == null )
                _titleAndConditionsShort = new SupportedPropertyPath<String>(EduProgramProfGen.P_TITLE_AND_CONDITIONS_SHORT, this);
            return _titleAndConditionsShort;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getTitleAndConditionsShortWithForm()
     */
        public SupportedPropertyPath<String> titleAndConditionsShortWithForm()
        {
            if(_titleAndConditionsShortWithForm == null )
                _titleAndConditionsShortWithForm = new SupportedPropertyPath<String>(EduProgramProfGen.P_TITLE_AND_CONDITIONS_SHORT_WITH_FORM, this);
            return _titleAndConditionsShortWithForm;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getTitleAndConditionsWithForm()
     */
        public SupportedPropertyPath<String> titleAndConditionsWithForm()
        {
            if(_titleAndConditionsWithForm == null )
                _titleAndConditionsWithForm = new SupportedPropertyPath<String>(EduProgramProfGen.P_TITLE_AND_CONDITIONS_WITH_FORM, this);
            return _titleAndConditionsWithForm;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getTitleWithCodeAndConditions()
     */
        public SupportedPropertyPath<String> titleWithCodeAndConditions()
        {
            if(_titleWithCodeAndConditions == null )
                _titleWithCodeAndConditions = new SupportedPropertyPath<String>(EduProgramProfGen.P_TITLE_WITH_CODE_AND_CONDITIONS, this);
            return _titleWithCodeAndConditions;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getTitleWithCodeAndConditionsShort()
     */
        public SupportedPropertyPath<String> titleWithCodeAndConditionsShort()
        {
            if(_titleWithCodeAndConditionsShort == null )
                _titleWithCodeAndConditionsShort = new SupportedPropertyPath<String>(EduProgramProfGen.P_TITLE_WITH_CODE_AND_CONDITIONS_SHORT, this);
            return _titleWithCodeAndConditionsShort;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getTitleWithCodeAndConditionsShortWithForm()
     */
        public SupportedPropertyPath<String> titleWithCodeAndConditionsShortWithForm()
        {
            if(_titleWithCodeAndConditionsShortWithForm == null )
                _titleWithCodeAndConditionsShortWithForm = new SupportedPropertyPath<String>(EduProgramProfGen.P_TITLE_WITH_CODE_AND_CONDITIONS_SHORT_WITH_FORM, this);
            return _titleWithCodeAndConditionsShortWithForm;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.program.entity.EduProgramProf#getTitleWithCodeAndConditionsWithForm()
     */
        public SupportedPropertyPath<String> titleWithCodeAndConditionsWithForm()
        {
            if(_titleWithCodeAndConditionsWithForm == null )
                _titleWithCodeAndConditionsWithForm = new SupportedPropertyPath<String>(EduProgramProfGen.P_TITLE_WITH_CODE_AND_CONDITIONS_WITH_FORM, this);
            return _titleWithCodeAndConditionsWithForm;
        }

        public Class getEntityClass()
        {
            return EduProgramProf.class;
        }

        public String getEntityName()
        {
            return "eduProgramProf";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getImplConditions();

    public abstract String getImplConditionsShort();

    public abstract String getImplConditionsShortWithForm();

    public abstract String getImplConditionsWithForm();

    public abstract String getShortTitleAndConditionsShort();

    public abstract String getShortTitleAndConditionsShortWithForm();

    public abstract String getShortTitleWithCodeAndConditionsShort();

    public abstract String getShortTitleWithCodeAndConditionsShortWithForm();

    public abstract String getTitleAndConditions();

    public abstract String getTitleAndConditionsShort();

    public abstract String getTitleAndConditionsShortWithForm();

    public abstract String getTitleAndConditionsWithForm();

    public abstract String getTitleWithCodeAndConditions();

    public abstract String getTitleWithCodeAndConditionsShort();

    public abstract String getTitleWithCodeAndConditionsShortWithForm();

    public abstract String getTitleWithCodeAndConditionsWithForm();
}
