/* $Id:$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.program.bo.EduProgram.EduProgramManager;

/**
 * @author oleyba
 * @since 11/20/13
 */
@Configuration
public class EduProgramSubjectList extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS("subjectIndexDS", subjectIndexDSHandler()))
                .addDataSource(EduProgramManager.instance().programKindDSConfig())
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> subjectIndexDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EduProgramSubjectIndex.class)
            .order(EduProgramSubjectIndex.priority())
            .filter(EduProgramSubjectIndex.title());
    }
}