/* $Id: EducationYearModel.java 32223 2014-02-03 10:55:07Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniedu.base.bo.EducationYear.utils;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.DefaultSelectValueStyle;
import org.tandemframework.tapsupport.component.selection.ISelectValueStyle;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author euroelessar
 * @since 09.03.2010
 */
public class EducationYearModel extends DQLFullCheckSelectModel
{
public static final String CURRENT_YEAR_STYLE = "background-color:#fff6c2 !important;";

    @Override
    protected DQLSelectBuilder query(String alias, String filter)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EducationYear.class, alias);
        if (null != _currentYear) {
            dql.where(between(property(EducationYear.intValue().fromAlias(alias)), value(_currentYear - 8), value(_currentYear + 1)));
        }

        if (StringUtils.isNotEmpty(filter))
            dql.where(likeUpper(property(alias, EducationYear.title()), value(CoreStringUtils.escapeLike(filter))));
        dql.order(property(EducationYear.intValue().fromAlias(alias)));
        return dql;
    }

    //    private static List<EducationYear> getYears(final Integer currentYear)
    //    {
    //        return UniDaoFacade.getCoreDao().getCalculatedValue(new HibernateCallback<List<EducationYear>>() {
    //            @Override public List<EducationYear> doInHibernate(final Session session) throws HibernateException, SQLException {
    //                if (null == currentYear) {
    //                    return UniDaoFacade.getCoreDao().getList(EducationYear.class, EducationYear.P_INT_VALUE);
    //                }
    //
    //                final DQLSelectBuilder dql = new DQLSelectBuilder();
    //                dql.fromEntity(EducationYear.class, "y").addColumn(DQLExpressions.property("y"));
    //                dql.where(DQLExpressions.between(DQLExpressions.property(EducationYear.intValue().fromAlias("y")), DQLExpressions.value(currentYear - 8), DQLExpressions.value(currentYear + 1)));
    //                dql.order(DQLExpressions.property(EducationYear.intValue().fromAlias("y")));
    //                return dql.createStatement(session).list();
    //            }
    //        });
    //    }

    private final Integer _currentYear;
    public EducationYearModel(final Integer currentYear) {
        super("title");
        _currentYear = currentYear;
    }
    public EducationYearModel() {
        this(null);
    }

    @Override
    public ISelectValueStyle getValueStyle(final Object value)
    {
        if (Boolean.FALSE.equals(((EducationYear) value).getCurrent())) {
            return null;
        }

        return new DefaultSelectValueStyle() {
            @Override public String getRowStyle() {
                return EducationYearModel.CURRENT_YEAR_STYLE;
            }
        };
    }
}
