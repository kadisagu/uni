package ru.tandemservice.uniedu.catalog.entity.subjects.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Направление подготовки профессионального образования
 *
 * В справочнике содержатся только те элементы, на которые формируются ОП (по которым ведется прием и предоставляется внешняя отчетность).
 * http://wiki.tandemservice.ru/pages/viewpage.action?pageId=17465605
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramSubjectGen extends EntityBase
 implements INaturalIdentifiable<EduProgramSubjectGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject";
    public static final String ENTITY_NAME = "eduProgramSubject";
    public static final int VERSION_HASH = -1706132065;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_SUBJECT_INDEX = "subjectIndex";
    public static final String P_SUBJECT_CODE = "subjectCode";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_OUT_OF_SYNC_DATE = "outOfSyncDate";
    public static final String P_TITLE = "title";
    public static final String P_GROUP_TITLE = "groupTitle";
    public static final String P_SHORT_TITLE_WITH_CODE = "shortTitleWithCode";
    public static final String P_TITLE_WITH_CODE = "titleWithCode";
    public static final String P_TITLE_WITH_CODE_INDEX_AND_GEN = "titleWithCodeIndexAndGen";

    private String _code;     // Системный код
    private EduProgramSubjectIndex _subjectIndex;     // Перечень
    private String _subjectCode;     // Полный код направления подготовки
    private String _shortTitle;     // Сокращенное название
    private Date _outOfSyncDate;     // Дата утраты актуальности
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * Определяет принадлежность направления перечню
     *
     * @return Перечень. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubjectIndex getSubjectIndex()
    {
        return _subjectIndex;
    }

    /**
     * @param subjectIndex Перечень. Свойство не может быть null.
     */
    public void setSubjectIndex(EduProgramSubjectIndex subjectIndex)
    {
        dirty(_subjectIndex, subjectIndex);
        _subjectIndex = subjectIndex;
    }

    /**
     * Полный код направления подготовки в рамках перечня (определяется документом, глобально не уникален)
     *
     * @return Полный код направления подготовки.
     */
    @Length(max=255)
    public String getSubjectCode()
    {
        return _subjectCode;
    }

    /**
     * @param subjectCode Полный код направления подготовки.
     */
    public void setSubjectCode(String subjectCode)
    {
        dirty(_subjectCode, subjectCode);
        _subjectCode = subjectCode;
    }

    /**
     * Сокращенное название.
     * Формируется после синхронизации направлений подготовки и квалификаций (см. {@link ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.util.EduProgramSubjectShortTitleBuilder}) для вновь добавленных направлений.
     * Впоследствии может быть отредактировано пользователем.
     *
     * @return Сокращенное название.
     */
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * Момент времени, когда запись стала не актуальной (используется при синхронизации перечней)
     *
     * @return Дата утраты актуальности.
     */
    public Date getOutOfSyncDate()
    {
        return _outOfSyncDate;
    }

    /**
     * @param outOfSyncDate Дата утраты актуальности.
     */
    public void setOutOfSyncDate(Date outOfSyncDate)
    {
        dirty(_outOfSyncDate, outOfSyncDate);
        _outOfSyncDate = outOfSyncDate;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EduProgramSubjectGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EduProgramSubject)another).getCode());
            }
            setSubjectIndex(((EduProgramSubject)another).getSubjectIndex());
            setSubjectCode(((EduProgramSubject)another).getSubjectCode());
            setShortTitle(((EduProgramSubject)another).getShortTitle());
            setOutOfSyncDate(((EduProgramSubject)another).getOutOfSyncDate());
            setTitle(((EduProgramSubject)another).getTitle());
        }
    }

    public INaturalId<EduProgramSubjectGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EduProgramSubjectGen>
    {
        private static final String PROXY_NAME = "EduProgramSubjectNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EduProgramSubjectGen.NaturalId) ) return false;

            EduProgramSubjectGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramSubjectGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramSubject.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EduProgramSubject is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "subjectIndex":
                    return obj.getSubjectIndex();
                case "subjectCode":
                    return obj.getSubjectCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "outOfSyncDate":
                    return obj.getOutOfSyncDate();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "subjectIndex":
                    obj.setSubjectIndex((EduProgramSubjectIndex) value);
                    return;
                case "subjectCode":
                    obj.setSubjectCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "outOfSyncDate":
                    obj.setOutOfSyncDate((Date) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "subjectIndex":
                        return true;
                case "subjectCode":
                        return true;
                case "shortTitle":
                        return true;
                case "outOfSyncDate":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "subjectIndex":
                    return true;
                case "subjectCode":
                    return true;
                case "shortTitle":
                    return true;
                case "outOfSyncDate":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "subjectIndex":
                    return EduProgramSubjectIndex.class;
                case "subjectCode":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "outOfSyncDate":
                    return Date.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgramSubject> _dslPath = new Path<EduProgramSubject>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramSubject");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * Определяет принадлежность направления перечню
     *
     * @return Перечень. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getSubjectIndex()
     */
    public static EduProgramSubjectIndex.Path<EduProgramSubjectIndex> subjectIndex()
    {
        return _dslPath.subjectIndex();
    }

    /**
     * Полный код направления подготовки в рамках перечня (определяется документом, глобально не уникален)
     *
     * @return Полный код направления подготовки.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getSubjectCode()
     */
    public static PropertyPath<String> subjectCode()
    {
        return _dslPath.subjectCode();
    }

    /**
     * Сокращенное название.
     * Формируется после синхронизации направлений подготовки и квалификаций (см. {@link ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.util.EduProgramSubjectShortTitleBuilder}) для вновь добавленных направлений.
     * Впоследствии может быть отредактировано пользователем.
     *
     * @return Сокращенное название.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * Момент времени, когда запись стала не актуальной (используется при синхронизации перечней)
     *
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getOutOfSyncDate()
     */
    public static PropertyPath<Date> outOfSyncDate()
    {
        return _dslPath.outOfSyncDate();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getGroupTitle()
     */
    public static SupportedPropertyPath<String> groupTitle()
    {
        return _dslPath.groupTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getShortTitleWithCode()
     */
    public static SupportedPropertyPath<String> shortTitleWithCode()
    {
        return _dslPath.shortTitleWithCode();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getTitleWithCode()
     */
    public static SupportedPropertyPath<String> titleWithCode()
    {
        return _dslPath.titleWithCode();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getTitleWithCodeIndexAndGen()
     */
    public static SupportedPropertyPath<String> titleWithCodeIndexAndGen()
    {
        return _dslPath.titleWithCodeIndexAndGen();
    }

    public static class Path<E extends EduProgramSubject> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private EduProgramSubjectIndex.Path<EduProgramSubjectIndex> _subjectIndex;
        private PropertyPath<String> _subjectCode;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<Date> _outOfSyncDate;
        private PropertyPath<String> _title;
        private SupportedPropertyPath<String> _groupTitle;
        private SupportedPropertyPath<String> _shortTitleWithCode;
        private SupportedPropertyPath<String> _titleWithCode;
        private SupportedPropertyPath<String> _titleWithCodeIndexAndGen;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EduProgramSubjectGen.P_CODE, this);
            return _code;
        }

    /**
     * Определяет принадлежность направления перечню
     *
     * @return Перечень. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getSubjectIndex()
     */
        public EduProgramSubjectIndex.Path<EduProgramSubjectIndex> subjectIndex()
        {
            if(_subjectIndex == null )
                _subjectIndex = new EduProgramSubjectIndex.Path<EduProgramSubjectIndex>(L_SUBJECT_INDEX, this);
            return _subjectIndex;
        }

    /**
     * Полный код направления подготовки в рамках перечня (определяется документом, глобально не уникален)
     *
     * @return Полный код направления подготовки.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getSubjectCode()
     */
        public PropertyPath<String> subjectCode()
        {
            if(_subjectCode == null )
                _subjectCode = new PropertyPath<String>(EduProgramSubjectGen.P_SUBJECT_CODE, this);
            return _subjectCode;
        }

    /**
     * Сокращенное название.
     * Формируется после синхронизации направлений подготовки и квалификаций (см. {@link ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.util.EduProgramSubjectShortTitleBuilder}) для вновь добавленных направлений.
     * Впоследствии может быть отредактировано пользователем.
     *
     * @return Сокращенное название.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EduProgramSubjectGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * Момент времени, когда запись стала не актуальной (используется при синхронизации перечней)
     *
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getOutOfSyncDate()
     */
        public PropertyPath<Date> outOfSyncDate()
        {
            if(_outOfSyncDate == null )
                _outOfSyncDate = new PropertyPath<Date>(EduProgramSubjectGen.P_OUT_OF_SYNC_DATE, this);
            return _outOfSyncDate;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EduProgramSubjectGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getGroupTitle()
     */
        public SupportedPropertyPath<String> groupTitle()
        {
            if(_groupTitle == null )
                _groupTitle = new SupportedPropertyPath<String>(EduProgramSubjectGen.P_GROUP_TITLE, this);
            return _groupTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getShortTitleWithCode()
     */
        public SupportedPropertyPath<String> shortTitleWithCode()
        {
            if(_shortTitleWithCode == null )
                _shortTitleWithCode = new SupportedPropertyPath<String>(EduProgramSubjectGen.P_SHORT_TITLE_WITH_CODE, this);
            return _shortTitleWithCode;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getTitleWithCode()
     */
        public SupportedPropertyPath<String> titleWithCode()
        {
            if(_titleWithCode == null )
                _titleWithCode = new SupportedPropertyPath<String>(EduProgramSubjectGen.P_TITLE_WITH_CODE, this);
            return _titleWithCode;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject#getTitleWithCodeIndexAndGen()
     */
        public SupportedPropertyPath<String> titleWithCodeIndexAndGen()
        {
            if(_titleWithCodeIndexAndGen == null )
                _titleWithCodeIndexAndGen = new SupportedPropertyPath<String>(EduProgramSubjectGen.P_TITLE_WITH_CODE_INDEX_AND_GEN, this);
            return _titleWithCodeIndexAndGen;
        }

        public Class getEntityClass()
        {
            return EduProgramSubject.class;
        }

        public String getEntityName()
        {
            return "eduProgramSubject";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getGroupTitle();

    public abstract String getShortTitleWithCode();

    public abstract String getTitleWithCode();

    public abstract String getTitleWithCodeIndexAndGen();
}
