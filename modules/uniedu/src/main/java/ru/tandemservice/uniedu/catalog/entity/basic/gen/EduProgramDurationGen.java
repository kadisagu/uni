package ru.tandemservice.uniedu.catalog.entity.basic.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Продолжительность обучения
 *
 * Продолжительность обучения. Определяет срок для образовательной программы.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramDurationGen extends EntityBase
 implements INaturalIdentifiable<EduProgramDurationGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration";
    public static final String ENTITY_NAME = "eduProgramDuration";
    public static final int VERSION_HASH = 1844853952;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_NUMBER_OF_YEARS = "numberOfYears";
    public static final String P_NUMBER_OF_MONTHS = "numberOfMonths";
    public static final String P_NUMBER_OF_HOURS = "numberOfHours";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private int _numberOfYears;     // Число лет обучения
    private int _numberOfMonths;     // Число месяцев обучения
    private int _numberOfHours;     // Число часов обучения
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * Число полных лет обучения
     *
     * @return Число лет обучения. Свойство не может быть null.
     */
    @NotNull
    public int getNumberOfYears()
    {
        return _numberOfYears;
    }

    /**
     * @param numberOfYears Число лет обучения. Свойство не может быть null.
     */
    public void setNumberOfYears(int numberOfYears)
    {
        dirty(_numberOfYears, numberOfYears);
        _numberOfYears = numberOfYears;
    }

    /**
     * Число месяцев обучения в последнем году
     *
     * @return Число месяцев обучения. Свойство не может быть null.
     */
    @NotNull
    public int getNumberOfMonths()
    {
        return _numberOfMonths;
    }

    /**
     * @param numberOfMonths Число месяцев обучения. Свойство не может быть null.
     */
    public void setNumberOfMonths(int numberOfMonths)
    {
        dirty(_numberOfMonths, numberOfMonths);
        _numberOfMonths = numberOfMonths;
    }

    /**
     * Число часов обучения по программе ДПО
     *
     * @return Число часов обучения. Свойство не может быть null.
     */
    @NotNull
    public int getNumberOfHours()
    {
        return _numberOfHours;
    }

    /**
     * @param numberOfHours Число часов обучения. Свойство не может быть null.
     */
    public void setNumberOfHours(int numberOfHours)
    {
        dirty(_numberOfHours, numberOfHours);
        _numberOfHours = numberOfHours;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EduProgramDurationGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EduProgramDuration)another).getCode());
            }
            setNumberOfYears(((EduProgramDuration)another).getNumberOfYears());
            setNumberOfMonths(((EduProgramDuration)another).getNumberOfMonths());
            setNumberOfHours(((EduProgramDuration)another).getNumberOfHours());
            setTitle(((EduProgramDuration)another).getTitle());
        }
    }

    public INaturalId<EduProgramDurationGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EduProgramDurationGen>
    {
        private static final String PROXY_NAME = "EduProgramDurationNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EduProgramDurationGen.NaturalId) ) return false;

            EduProgramDurationGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramDurationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramDuration.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramDuration();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "numberOfYears":
                    return obj.getNumberOfYears();
                case "numberOfMonths":
                    return obj.getNumberOfMonths();
                case "numberOfHours":
                    return obj.getNumberOfHours();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "numberOfYears":
                    obj.setNumberOfYears((Integer) value);
                    return;
                case "numberOfMonths":
                    obj.setNumberOfMonths((Integer) value);
                    return;
                case "numberOfHours":
                    obj.setNumberOfHours((Integer) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "numberOfYears":
                        return true;
                case "numberOfMonths":
                        return true;
                case "numberOfHours":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "numberOfYears":
                    return true;
                case "numberOfMonths":
                    return true;
                case "numberOfHours":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "numberOfYears":
                    return Integer.class;
                case "numberOfMonths":
                    return Integer.class;
                case "numberOfHours":
                    return Integer.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgramDuration> _dslPath = new Path<EduProgramDuration>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramDuration");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * Число полных лет обучения
     *
     * @return Число лет обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration#getNumberOfYears()
     */
    public static PropertyPath<Integer> numberOfYears()
    {
        return _dslPath.numberOfYears();
    }

    /**
     * Число месяцев обучения в последнем году
     *
     * @return Число месяцев обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration#getNumberOfMonths()
     */
    public static PropertyPath<Integer> numberOfMonths()
    {
        return _dslPath.numberOfMonths();
    }

    /**
     * Число часов обучения по программе ДПО
     *
     * @return Число часов обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration#getNumberOfHours()
     */
    public static PropertyPath<Integer> numberOfHours()
    {
        return _dslPath.numberOfHours();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EduProgramDuration> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Integer> _numberOfYears;
        private PropertyPath<Integer> _numberOfMonths;
        private PropertyPath<Integer> _numberOfHours;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EduProgramDurationGen.P_CODE, this);
            return _code;
        }

    /**
     * Число полных лет обучения
     *
     * @return Число лет обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration#getNumberOfYears()
     */
        public PropertyPath<Integer> numberOfYears()
        {
            if(_numberOfYears == null )
                _numberOfYears = new PropertyPath<Integer>(EduProgramDurationGen.P_NUMBER_OF_YEARS, this);
            return _numberOfYears;
        }

    /**
     * Число месяцев обучения в последнем году
     *
     * @return Число месяцев обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration#getNumberOfMonths()
     */
        public PropertyPath<Integer> numberOfMonths()
        {
            if(_numberOfMonths == null )
                _numberOfMonths = new PropertyPath<Integer>(EduProgramDurationGen.P_NUMBER_OF_MONTHS, this);
            return _numberOfMonths;
        }

    /**
     * Число часов обучения по программе ДПО
     *
     * @return Число часов обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration#getNumberOfHours()
     */
        public PropertyPath<Integer> numberOfHours()
        {
            if(_numberOfHours == null )
                _numberOfHours = new PropertyPath<Integer>(EduProgramDurationGen.P_NUMBER_OF_HOURS, this);
            return _numberOfHours;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EduProgramDurationGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EduProgramDuration.class;
        }

        public String getEntityName()
        {
            return "eduProgramDuration";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
