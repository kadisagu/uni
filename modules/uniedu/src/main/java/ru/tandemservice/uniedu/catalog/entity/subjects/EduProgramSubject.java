package ru.tandemservice.uniedu.catalog.entity.subjects;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.gen.EduProgramSubjectGen;
import ru.tandemservice.uniedu.entity.EduAccreditation;
import ru.tandemservice.uniedu.entity.IPersistentAccreditationOwner;
import ru.tandemservice.uniedu.entity.StateAccreditationEnum;

/**
 * Направление подготовки профессионального образования
 *
 * В справочнике содержатся только те элементы, на которые формируются ОП (по которым ведется прием и предоставляется внешняя отчетность).
 * http://wiki.tandemservice.ru/pages/viewpage.action?pageId=17465605
 */
public abstract class EduProgramSubject extends EduProgramSubjectGen implements ITitled, Comparable<EduProgramSubject>
{
    @Override
    @EntityDSLSupport
    public abstract String getGroupTitle();


    @Override
    @EntityDSLSupport(parts = {EduProgramSubject.P_SUBJECT_CODE, EduProgramSubject.P_TITLE})
    public String getTitleWithCode() {
        return getSubjectCode() + " " + getTitle();
    }

    public String getTitleWithCodeOksoWithoutSpec() {
        return getTitleWithCode(); // Для ОКСО метод переопределен
    }

    @EntityDSLSupport(parts = {EduProgramSubject.P_SUBJECT_CODE, EduProgramSubject.P_TITLE, EduProgramSubject.L_SUBJECT_INDEX + "." + EduProgramSubjectIndex.P_EDU_PROGRAM_SUBJECT_TYPE, EduProgramSubject.L_SUBJECT_INDEX + "." + EduProgramSubjectIndex.P_EDU_LEVEL_AND_INDEX_NOTATION})
    public String getTitleWithCodeIndexAndGen()
    {
        return getTitleWithCode() + " (" + getSubjectIndex().getEduProgramSubjectType() +", " + getSubjectIndex().getEduLevelAndIndexNotation() + ")";
    }

    @EntityDSLSupport(parts = {EduProgramSubject.P_SUBJECT_CODE, EduProgramSubject.P_SHORT_TITLE})
    public String getShortTitleWithCode() {
        return CommonBaseStringUtil.joinWithSeparator(" ", getSubjectCode(), getShortTitle());
    }

    public EduProgramKind getEduProgramKind() {
        return getSubjectIndex().getProgramKind();
    }

    @Override
    public int compareTo(EduProgramSubject o)
    {
        if (null == o) return 1;
        return getTitleWithCode().compareTo(o.getTitleWithCode());
    }

    /* По идее это можно проверять как
            getEduProgramKind().isProgramHigherProf()
            &&
            getSubjectIndex().getGeneration().getCode() in [EduProgramSubjectIndexGenerationCodes.FGOS_2009, EduProgramSubjectIndexGenerationCodes.FGOS_2013]
       но с переопределяемым методом будет быстрее. Коли у нас есть классы, соответствующие поколениям перечней, будет использовать их.
     */
    /**
     * В зачетных ли единицах (ЗЕ) трудоемкость.
     * В ЗЕ трудоемкость выводится для направлений ВО 2009 и 2013 года. Для остальных - в неделях.
     */
    public abstract boolean isLabor();

    /**
     * Направление из перечня ОКСО (2005).
     * Метод нужен для оптимизации (чтобы не делать так: getProgramSubject().getSubjectIndex().getGeneration().getCode())
     */
    public abstract boolean isOKSO();

    /**
     * Направление из перечня ФГОС 2009 года.
     * Метод нужен для оптимизации (чтобы не делать так: getProgramSubject().getSubjectIndex().getGeneration().getCode())
     */
    public abstract boolean is2009();

    /**
     * Направление из перечня ФГОС 2013 года.
     * Метод нужен для оптимизации (чтобы не делать так: getProgramSubject().getSubjectIndex().getGeneration().getCode())
     */
    public abstract boolean is2013();

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name){
        return new EntityComboDataSourceHandler(name, EduProgramSubject.class)
                .order(EduProgramSubject.code())
                .order(EduProgramSubject.title())
                .filter(EduProgramSubject.title())
                .filter(EduProgramSubject.code());
    }

    public abstract IPersistentAccreditationOwner getSubjectGroup();

}