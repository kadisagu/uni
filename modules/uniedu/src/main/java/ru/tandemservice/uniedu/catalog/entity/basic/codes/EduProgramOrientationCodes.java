package ru.tandemservice.uniedu.catalog.entity.basic.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Ориентация образовательной программы"
 * Имя сущности : eduProgramOrientation
 * Файл data.xml : uniedu.basic.data.xml
 */
public interface EduProgramOrientationCodes
{
    /** Константа кода (code) элемента : Академический бакалавриат (title) */
    String ACADEMIC_BACHELOR_2013 = "2013.03.academicBachelor";
    /** Константа кода (code) элемента : Прикладной бакалавриат (title) */
    String APPLIED_BACHELOR_2013 = "2013.03.appliedBachelor";
    /** Константа кода (code) элемента : Академическая магистратура (title) */
    String ACADEMIC_MASTER_2013 = "2013.04.academicMaster";
    /** Константа кода (code) элемента : Прикладная магистратура (title) */
    String APPLIED_MASTER_2013 = "2013.04.appliedMaster";

    Set<String> CODES = ImmutableSet.of(ACADEMIC_BACHELOR_2013, APPLIED_BACHELOR_2013, ACADEMIC_MASTER_2013, APPLIED_MASTER_2013);
}
