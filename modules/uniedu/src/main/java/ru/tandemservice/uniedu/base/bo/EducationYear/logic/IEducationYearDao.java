package ru.tandemservice.uniedu.base.bo.EducationYear.logic;

import java.util.Map;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.program.entity.EduProgram;

/**
 * @author vdanilov
 */
public interface IEducationYearDao extends INeedPersistenceSupport {

    /**
     * @return текущий учебный год, null - если такого года нет
     */
    EducationYear getCurrent();

    /**
     * @return { educationYear.intValue -> educationYear }
     */
    Map<Integer, EducationYear> getEducationYearMapByNumber();

    /**
     * @return { educationYear.code -> educationYear }
     */
    Map<String, EducationYear> getEducationYearMapByCode();

    /**
     * Поиск копии образовательной программы в указанном году.
     * Копия ищется по совпадению {@link ru.tandemservice.uniedu.program.entity.EduProgram#getProgramUniqueKey()}.
     *
     * @param program исходная ОП
     * @param year    год, в котором ищется копия исходной ОП
     * @return копия, если найдена. Иначе - null.
     */
    <T extends EduProgram> T getProgramCopy(T program, EducationYear year);

    /**
     * Поиск копии образовательной программы в указанном году.
     * Копия ищется по совпадению {@link ru.tandemservice.uniedu.program.entity.EduProgram#getProgramUniqueKey()}.
     *
     * @param program исходная ОП
     * @param year    год, в котором ищется копия исходной ОП
     * @return копия, если найдена. Иначе - null.
     */
    <T extends EduProgram> T getProgramCopy(T program, int year);
}
