package ru.tandemservice.uniedu.catalog.entity.subjects.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Перечень направлений подготовки и квалификаций профессионального образования"
 * Имя сущности : eduProgramSubjectIndex
 * Файл data.xml : uniedu.subjects.data.xml
 */
public interface EduProgramSubjectIndexCodes
{
    /** Константа кода (code) элемента : Перечень специальностей СПО по ОКСО (title) */
    String TITLE_2005_50 = "2005.50";
    /** Константа кода (code) элемента : Перечень направлений бакалавриата по ОКСО (title) */
    String TITLE_2005_62 = "2005.62";
    /** Константа кода (code) элемента : Перечень специальностей ВПО по ОКСО (title) */
    String TITLE_2005_65 = "2005.65";
    /** Константа кода (code) элемента : Перечень направлений магистратуры по ОКСО (title) */
    String TITLE_2005_68 = "2005.68";
    /** Константа кода (code) элемента : Перечень профессий НПО 2009 (title) */
    String TITLE_2009_40 = "2009.40";
    /** Константа кода (code) элемента : Перечень специальностей СПО 2009 (title) */
    String TITLE_2009_50 = "2009.50";
    /** Константа кода (code) элемента : Перечень направлений бакалавриата 2009 (title) */
    String TITLE_2009_62 = "2009.62";
    /** Константа кода (code) элемента : Перечень специальностей ВПО 2009 (title) */
    String TITLE_2009_65 = "2009.65";
    /** Константа кода (code) элемента : Перечень направлений магистратуры 2009 (title) */
    String TITLE_2009_68 = "2009.68";
    /** Константа кода (code) элемента : Перечень направлений аспирантуры 2009 (title) */
    String TITLE_2009_00 = "2009.00";
    /** Константа кода (code) элемента : Перечень профессий СПО 2013 (title) */
    String TITLE_2013_01 = "2013.01";
    /** Константа кода (code) элемента : Перечень специальностей СПО 2013 (title) */
    String TITLE_2013_02 = "2013.02";
    /** Константа кода (code) элемента : Перечень направлений бакалавриата 2013 (title) */
    String TITLE_2013_03 = "2013.03";
    /** Константа кода (code) элемента : Перечень направлений магистратуры 2013 (title) */
    String TITLE_2013_04 = "2013.04";
    /** Константа кода (code) элемента : Перечень специальностей ВО 2013 (title) */
    String TITLE_2013_05 = "2013.05";
    /** Константа кода (code) элемента : Перечень направлений аспирантуры 2013 (title) */
    String TITLE_2013_06 = "2013.06";
    /** Константа кода (code) элемента : Перечень направлений адъюнктуры 2013 (title) */
    String TITLE_2013_07 = "2013.07";
    /** Константа кода (code) элемента : Перечень специальностей ординатуры 2013 (title) */
    String TITLE_2013_08 = "2013.08";
    /** Константа кода (code) элемента : Перечень специальностей интернатуры 2013 (title) */
    String TITLE_2013_10 = "2013.10";

    Set<String> CODES = ImmutableSet.of(TITLE_2005_50, TITLE_2005_62, TITLE_2005_65, TITLE_2005_68, TITLE_2009_40, TITLE_2009_50, TITLE_2009_62, TITLE_2009_65, TITLE_2009_68, TITLE_2009_00, TITLE_2013_01, TITLE_2013_02, TITLE_2013_03, TITLE_2013_04, TITLE_2013_05, TITLE_2013_06, TITLE_2013_07, TITLE_2013_08, TITLE_2013_10);
}
