package ru.tandemservice.uniedu.catalog.entity.basic.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Особенность реализации образовательной программы"
 * Имя сущности : eduProgramTrait
 * Файл data.xml : uniedu.basic.data.xml
 */
public interface EduProgramTraitCodes
{
    /** Константа кода (code) элемента : Дистанционная (title) */
    String DISTANTSIONNAYA = "1";
    /** Константа кода (code) элемента : Дистанционная электронная (title) */
    String DISTANTSIONNAYA_ELEKTRONNAYA = "2";
    /** Константа кода (code) элемента : Группа выходного дня (title) */
    String GRUPPA_VYHODNOGO_DNYA = "3";

    Set<String> CODES = ImmutableSet.of(DISTANTSIONNAYA, DISTANTSIONNAYA_ELEKTRONNAYA, GRUPPA_VYHODNOGO_DNYA);
}
