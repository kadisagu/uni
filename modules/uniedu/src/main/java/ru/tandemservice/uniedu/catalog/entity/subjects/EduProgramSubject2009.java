package ru.tandemservice.uniedu.catalog.entity.subjects;

import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.gen.EduProgramSubject2009Gen;

/**
 * Направление подготовки профессионального образования (2009)
 *
 * Направление классификаторов 2009 года: плоская структура (она не совпадает со структурой ОКСО в ряде мест). Перечень УГС глобальный и общий для ОКСО и 2009.
 * Направление подготовки в системе ссылается только на УГС (иерархической структуры внутри направлений и специальностей просто нет).
 * http://wiki.tandemservice.ru/pages/viewpage.action?pageId=17465605
 */
public class EduProgramSubject2009 extends EduProgramSubject2009Gen
{
    @Override
    public String getGroupTitle()
    {
        return getGroup().getTitle();
    }

    public String getSpecialDegreeTitle()
    {
        if (isAssignSpecialDegree())
        {
            switch (getSubjectIndex().getCode())
            {
                case EduProgramSubjectIndexCodes.TITLE_2009_62:
                    return "Бакалавр-инженер";
                case EduProgramSubjectIndexCodes.TITLE_2009_68:
                    return "Магистр-инженер";
            }
        }
        return null;
    }

    @Override
    public boolean isLabor()
    {
        // Для перечней ВО 2009 трудоемкость в ЗЕ
        return getEduProgramKind().isProgramHigherProf();
    }

    @Override public boolean isOKSO() { return false; }
    @Override public boolean is2009() { return true; }
    @Override public boolean is2013() { return false; }

    @Override
    public EduProgramSubjectOksoGroup getSubjectGroup()
    {
        return getGroup();
    }
}