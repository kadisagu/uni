/* $Id$ */
package ru.tandemservice.uniedu.entity;

import org.tandemframework.core.common.IIdentifiable;

/**
 * @author Ekaterina Zvereva
 * @since 28.11.2016
 */
public enum StateAccreditationEnum implements IIdentifiable
{
    ACCREDITATION_ACTIVE(1L, "Действует"),
    ACCREDITATION_SUSPENDED(2L, "Приостановлена"),
    ACCREDITATION_REVOKED(3L, "Отозвана");

    private Long _id;
    private String _title;

    StateAccreditationEnum(Long id, String title)
    {
        _id = id;
        _title = title;
    }

    public Long getId()
    {
        return this._id;
    }

    public String getTitle() {return  this._title;};
}
