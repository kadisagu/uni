/* $Id:$ */
package ru.tandemservice.uniedu.settings.bo.EduProgramSubjectComparison.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;

/**
 * @author rsizonenko
 * @since 18.03.2015
 */
public interface IEduProgramSubjectComparisonDao extends INeedPersistenceSupport
{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void fillComparisonsByNameOverlap(EduProgramSubjectIndex oldIndex, EduProgramSubjectIndex newIndex);
}
