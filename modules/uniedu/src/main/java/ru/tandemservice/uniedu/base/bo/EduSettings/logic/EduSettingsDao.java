/* $Id$ */
package ru.tandemservice.uniedu.base.bo.EduSettings.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2009;

/**
 * @author Nikolay Fedorovskih
 * @since 30.04.2014
 */
public class EduSettingsDao extends UniBaseDao implements IEduSettingsDao
{
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void setAssignSpecialDegreeForEduProgram2009(Long programSubject2009Id, boolean assignSpecialDegree)
    {
        EduProgramSubject2009 programSubject2009 = getNotNull(programSubject2009Id);
        programSubject2009.setAssignSpecialDegree(assignSpecialDegree);
        save(programSubject2009);
    }
}