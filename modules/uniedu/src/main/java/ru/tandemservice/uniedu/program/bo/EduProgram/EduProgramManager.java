/* $Id:$ */
package ru.tandemservice.uniedu.program.bo.EduProgram;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;

/**
 * @author oleyba
 * @since 7/2/15
 */
@Configuration
public class EduProgramManager extends BusinessObjectManager
{
    public static final String EDU_PROGRAM_KIND_DS = "eduProgramKindDS";

    public static EduProgramManager instance()
    {
        return instance(EduProgramManager.class);
    }

    @Bean
    public UIDataSourceConfig programKindDSConfig()
    {
        return CommonBaseStaticSelectDataSource.selectDS(EDU_PROGRAM_KIND_DS, getName(), EduProgramKind.defaultSelectDSHandler(getName()))
                .create();
    }
}
