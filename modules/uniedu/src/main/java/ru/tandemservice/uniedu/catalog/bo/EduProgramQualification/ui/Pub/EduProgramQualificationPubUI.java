/* $Id:$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramQualification.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.formatter.FormattedMessage;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.bo.EduProgramQualification.ui.Edit.EduProgramQualificationEdit;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;

/**
 * @author rsizonenko
 * @since 08.09.2014
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eduProgramQualification.id"))
public class EduProgramQualificationPubUI extends UIPresenter {

    private EduProgramQualification eduProgramQualification = new EduProgramQualification();

    // getset

    public EduProgramQualification getEduProgramQualification() {
        return eduProgramQualification;
    }

    public void setEduProgramQualification(EduProgramQualification eduProgramQualification) {
        this.eduProgramQualification = eduProgramQualification;
    }

    // override

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();
        setEduProgramQualification(IUniBaseDao.instance.get().get(EduProgramQualification.class, getEduProgramQualification().getId()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        super.onBeforeDataSourceFetch(dataSource);
        dataSource.put(EduProgramQualificationPub.EDU_PROGRAM_QUALIFICATION, getEduProgramQualification());
    }

    // listeners

    public void onClickEdit(){
        _uiActivation.asRegionDialog(EduProgramQualificationEdit.class).parameter(PUBLISHER_ID, getEduProgramQualification().getId()).activate();
    }

    public void onClickDelete()
    {
        IUniBaseDao.instance.get().delete(getEduProgramQualification());
        deactivate();
    }

    public String getInfoTableCaption(){
        return "Квалификация: " + getEduProgramQualification().getTitle();
    }

}