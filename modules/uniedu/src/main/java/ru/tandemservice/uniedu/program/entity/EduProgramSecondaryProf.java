package ru.tandemservice.uniedu.program.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniedu.program.entity.gen.EduProgramSecondaryProfGen;

/**
 * Образовательная программа СПО
 *
 * Среднее профессиональное образование: реализуется по направлениям (специальностям, профессиям) и признаку углубленного изучения.
 */
public class EduProgramSecondaryProf extends EduProgramSecondaryProfGen implements ITitled
{

    @Override
    protected StringBuilder titleBuilder() {
        StringBuilder builder = super.titleBuilder();
        if (this.isInDepthStudy()) {
            builder.append(", углубленная подготовка");
        }
        return builder;
    }

    @EntityDSLSupport(parts = EduProgramSecondaryProf.P_TITLE)
    public String getFullTitle() {
        return generateTitle();
    }

    @Override protected String getSecondaryInDepthStudyString(){ return this.isInDepthStudy() ? "углуб. изуч., " : ""; }
}