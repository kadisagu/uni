package ru.tandemservice.uniedu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniedu_2x11x0_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eduProgramSubjectComparison

        if (tool.tableExists("dip_edu_pr_subject_compare_t"))
        {
            // переименовать таблицу
            tool.renameTable("dip_edu_pr_subject_compare_t", "edu_pr_subject_compare_t");

            // переименовать код сущности
            tool.entityCodes().rename("dipEduProgramSubjectComparison", "eduProgramSubjectComparison");
        }
        else
        {
            // создана новая сущность
            // создать таблицу
            DBTable dbt = new DBTable("edu_pr_subject_compare_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eduprogramsubjectcomparison"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("oldprogramsubject_id", DBType.LONG).setNullable(false),
                                      new DBColumn("newprogramsubject_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("eduProgramSubjectComparison");

        }


    }
}