package ru.tandemservice.uniedu.catalog.entity.subjects;

import com.google.common.collect.Ordering;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.subjects.gen.EduProgramSpecializationGen;

/**
 * Направленность высшего профессионального образования
 *
 * Направленность направления подготовки в образовательном учреждении.
 * Определяет перечень реализуемых направленнстей направлений подготовки в ОУ.
 */
public abstract class EduProgramSpecialization extends EduProgramSpecializationGen implements Comparable<EduProgramSpecialization>
{
    public static final String PROP_PROGRAM_SUBJECT = "programSubject";

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EduProgramSpecialization.class)
                .titleProperty(EduProgramSpecialization.displayableTitle().s())
                .where(EduProgramSpecialization.programSubject(), PROP_PROGRAM_SUBJECT)
                .filter(EduProgramSpecialization.programSubject().title())
                .filter(EduProgramSpecialization.title())
                .order(EduProgramSpecialization.programSubject().subjectCode())
                .order(EduProgramSpecialization.programSubject().title())
                .order(EduProgramSpecialization.title());
    }

    public abstract boolean isRootSpecialization();

    @EntityDSLSupport(parts = P_TITLE)
    public abstract String getDisplayableTitle();

    @Override
    public int compareTo(EduProgramSpecialization o)
    {
        if (o == null) return 1;

        int rootCmp = Boolean.compare(isRootSpecialization(), o.isRootSpecialization());
        if (0 != rootCmp) { return -rootCmp; }

        return Ordering.natural().nullsFirst().compare(getTitle(), o.getTitle());
    }
}