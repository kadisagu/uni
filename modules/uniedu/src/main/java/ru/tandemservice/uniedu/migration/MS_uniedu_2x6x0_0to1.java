package ru.tandemservice.uniedu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramTraitCodes;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniedu_2x6x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eduProgramTrait
        if (tool.tableExists("eduprogramtrait_t")) return;

        Long remoteId = null;
		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("eduprogramtrait_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("shorttitle_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("defaulttitle_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eduProgramTrait");

            remoteId = EntityIDGenerator.generateNewId(entityCode);

            tool.executeUpdate("insert into eduprogramtrait_t (id, discriminator, title_p, shorttitle_p, defaulttitle_p, code_p) values (?, ?, ?, ?, ?, ?)", remoteId, entityCode, "Дистанционная", "дист.", "Дистанционная", EduProgramTraitCodes.DISTANTSIONNAYA);
		}

        ////////////////////////////////////////////////////////////////////////////////
		// сущность eduProgram

		// создано свойство eduProgramTrait
		{
			// создать колонку
			tool.createColumn("edu_program_t", new DBColumn("eduprogramtrait_id", DBType.LONG));
		}

        if (remoteId != null)
            tool.executeUpdate("update edu_program_t set eduprogramtrait_id = ? where remoterealization_p = ? ", remoteId, true);

		// удалено свойство remoteRealization
		{
			// удалить колонку
			tool.dropColumn("edu_program_t", "remoterealization_p");
		}
    }
}