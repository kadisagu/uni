/**
 *$Id$
 */
package ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.ui.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.ui.Pub.EduProgramSubjectPub;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.EduInstitutionOrgUnitManager;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniedu.program.bo.EduProgram.EduProgramManager;
import ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.ui.List.logic.EduProgramSecondaryProfListDSHandler;
import ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.ui.Pub.EduProgramSecondaryProfPub;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 10.02.2014
 */
@Configuration
public class EduProgramSecondaryProfList extends BusinessComponentManager
{
    public static final String EDU_PROGRAM_SECONDARY_PROF_LIST_DS = "eduProgramSecondaryProfListDS";
    public static final String EDU_PROGRAM_FORM_DS = "eduProgramFormDS";
    public static final String EDU_PROGRAM_DURATION_DS = "eduProgramDurationDS";
    public static final String EDU_YEAR_DS = "eduYearDS";
    public static final String EDU_PROGRAM_SUBJECT_INDEX_DS = "eduProgramSubjectIndexDS";
    public static final String EDU_PROGRAM_SUBJECT_DS = "eduProgramSubjectDS";
	public static final String EDU_OWNER_ORG_UNIT_DS = "eduOwnerOrgUnitDS";

	public static final String EDU_PROGRAM_KIND = "eduProgramKind";
	public static final String EDU_PROGRAM_SUBJECT_INDEX = "eduProgramSubjectIndex";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .addDataSource(CommonBaseStaticSelectDataSource.selectDS(EduProgramManager.EDU_PROGRAM_KIND_DS, getName(), EduProgramKind.defaultSelectDSHandler(getName()).pageable(true)
                    .customize((alias, dql, context, filter) -> dql.where(exists(EduProgramSecondaryProf.class, EduProgramSecondaryProf.kind().id().s(), property(alias))))))
            .addDataSource(selectDS(EDU_PROGRAM_FORM_DS, eduProgramFormDSHandler()))
            .addDataSource(selectDS(EDU_PROGRAM_DURATION_DS, eduProgramDurationDSHandler()))
            .addDataSource(selectDS(EDU_YEAR_DS, eduYearDSHandler()))
            .addDataSource(selectDS(EDU_PROGRAM_SUBJECT_INDEX_DS, eduProgramSubjectIndexDSHandler()))
            .addDataSource(selectDS(EDU_PROGRAM_SUBJECT_DS, eduProgramSubjectDSHandler())
					.addColumn(EduProgramSubject.titleWithCode().s()))
            .addDataSource(CommonBaseStaticSelectDataSource.selectDS(EDU_OWNER_ORG_UNIT_DS, getName(), EduOwnerOrgUnit.eduOwnerOrgUnitReorganizationCustomizedOrgUnitHandler(getName()))
		            .valueStyleSource(EduOwnerOrgUnit.archivalWiseStyle))
            .addDataSource(EduInstitutionOrgUnitManager.instance().eduInstitutionOrgUnitDSConfig())
            .addDataSource(CommonManager.instance().yesNoDSConfig())
            .addDataSource(searchListDS(EDU_PROGRAM_SECONDARY_PROF_LIST_DS, eduProgramSecondaryProfListDSColumns(), eduProgramSecondaryProfListDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint eduProgramSecondaryProfListDSColumns()
    {
        final String CODE_SEPARATOR = ".";
        final IFormatter<String> eduProgramSubjectIndexCodeFormatter = source -> StringUtils.substringBefore(source, CODE_SEPARATOR);

        return columnListExtPointBuilder(EDU_PROGRAM_SECONDARY_PROF_LIST_DS)
                .addColumn(checkboxColumn("check"))
                .addColumn(publisherColumn("eduProgramTitle", EduProgramSecondaryProf.title())
                        .businessComponent(EduProgramSecondaryProfPub.class).required(Boolean.TRUE).order())

                .addColumn(textColumn("eduProgramShortTitle", EduProgramSecondaryProf.shortTitle()))
                .addColumn(textColumn("eduProgramKind", EduProgramSecondaryProf.kind().shortTitle()).order())
                .addColumn(textColumn("eduProgramForm", EduProgramSecondaryProf.form().title()).order())
                .addColumn(textColumn("eduProgramDuration", EduProgramSecondaryProf.duration().title()).order())
                .addColumn(textColumn("eduYear", EduProgramSecondaryProf.year().title()).order())
                .addColumn(textColumn("eduProgramSubjectIndex", EduProgramSecondaryProf.programSubject().subjectIndex().code()).formatter(eduProgramSubjectIndexCodeFormatter).order())
                .addColumn(publisherColumn("eduProgramSubject", EduProgramSecondaryProf.programSubject().titleWithCode())
                    .businessComponent(EduProgramSubjectPub.class).primaryKeyPath(EduProgramSecondaryProf.programSubject().id()).order())

                .addColumn(publisherColumn("eduOwnerOrgUnit", EduProgramSecondaryProf.ownerOrgUnit().orgUnit().shortTitle())
                    .businessComponent(OrgUnitView.class).primaryKeyPath(EduProgramSecondaryProf.ownerOrgUnit().orgUnit().id()).order())

                .addColumn(publisherColumn("eduInstitutionOrgUnit", EduProgramSecondaryProf.institutionOrgUnit().orgUnit().shortTitle())
                    .businessComponent(OrgUnitView.class).primaryKeyPath(EduProgramSecondaryProf.institutionOrgUnit().orgUnit().id()).order())

                .addColumn(textColumn("eduProgramBaseLevel", EduProgramSecondaryProf.baseLevel().title()).order())
                .addColumn(booleanColumn("eduProgramInDepthStudy", EduProgramSecondaryProf.inDepthStudy()))
                .addColumn(textColumn("eduProgramTrait", EduProgram.eduProgramTrait().shortTitle()))
                .addColumn(textColumn("qualification", EduProgramSecondaryProf.programQualification().title()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEditEduProgram").permissionKey("edit_eduProgramSecondaryProf_list"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDeleteEduProgram").alert("ui:eduProgramDeleteAlert").permissionKey("delete_eduProgramSecondaryProf_list"))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler eduProgramSecondaryProfListDSHandler()
    {
        return new EduProgramSecondaryProfListDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramFormDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramForm.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                dql.where(in(property(alias, EduProgramForm.id()), new DQLSelectBuilder()
                        .fromEntity(EduProgramSecondaryProf.class, "ep")
                        .column(property("ep", EduProgramSecondaryProf.form().id()))
                        .predicate(DQLPredicateType.distinct)
                        .buildQuery()));

                super.applyWhereConditions(alias, dql, context);
            }
        }
                .filter(EduProgramForm.title())
                .order(EduProgramForm.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramDurationDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramDuration.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                dql.where(in(property(alias, EduProgramDuration.id()), new DQLSelectBuilder()
                        .fromEntity(EduProgramSecondaryProf.class, "ep")
                        .column(property("ep", EduProgramSecondaryProf.duration().id()))
                        .predicate(DQLPredicateType.distinct)
                        .buildQuery()));

                super.applyWhereConditions(alias, dql, context);
            }
        }
                .filter(EduProgramDuration.title())
                .order(EduProgramDuration.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduYearDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EducationYear.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                dql.where(in(property(alias, EducationYear.id()), new DQLSelectBuilder()
                        .fromEntity(EduProgramSecondaryProf.class, "ep")
                        .column(property("ep", EduProgramSecondaryProf.year().id()))
                        .predicate(DQLPredicateType.distinct)
                        .buildQuery()));

                super.applyWhereConditions(alias, dql, context);
            }
        }
                .filter(EducationYear.title())
                .order(EducationYear.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramSubjectIndexDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubjectIndex.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                dql.where(in(property(alias, EduProgramSubjectIndex.id()), new DQLSelectBuilder()
                        .fromEntity(EduProgramSecondaryProf.class, "ep")
                        .column(property("ep", EduProgramSecondaryProf.programSubject().subjectIndex().id()))
                        .predicate(DQLPredicateType.distinct)
                        .buildQuery()));

                EduProgramKind eduProgramKind = context.get(EDU_PROGRAM_KIND);
                if (eduProgramKind != null)
                    dql.where(eq(property(alias, EduProgramSubjectIndex.programKind()), value(eduProgramKind)));

                super.applyWhereConditions(alias, dql, context);
            }
        }
                .filter(EduProgramSubjectIndex.title())
                .order(EduProgramSubjectIndex.priority())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                dql.where(in(property(alias, EduProgramSubject.id()), new DQLSelectBuilder()
                        .fromEntity(EduProgramSecondaryProf.class, "ep")
                        .column(property("ep", EduProgramSecondaryProf.programSubject().id()))
                        .predicate(DQLPredicateType.distinct)
                        .buildQuery()));

                EduProgramKind eduProgramKind = context.get(EDU_PROGRAM_KIND);
                EduProgramSubjectIndex eduProgramSubjectIndex = context.get(EDU_PROGRAM_SUBJECT_INDEX);

                if (eduProgramSubjectIndex != null)
                {
                    dql.where(eq(property(alias, EduProgramSubject.subjectIndex()), value(eduProgramSubjectIndex)));

                } else if (eduProgramKind != null)
                {
                    dql.where(eq(property(alias, EduProgramSubject.subjectIndex().programKind()), value(eduProgramKind)));
                }

                super.applyWhereConditions(alias, dql, context);
            }

            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder dql = super.query(alias, filter);
                dql
                        .where(likeUpper(DQLFunctions.concat(property(alias, EduProgramSubject.subjectCode()), property(alias, EduProgramSubject.title())), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(alias, EduProgramSubject.subjectCode()))
                        .order(property(alias, EduProgramSubject.title()));

                return dql;
            }
        }.pageable(true);
    }
}