/* $Id$ */
package ru.tandemservice.uniedu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * @author azhebko
 * @since 02.06.2014
 */
public class MS_uniedu_2x6x0_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Map<String, String> instrumentalVariantTitleInsertMap = new HashMap<>(EduProgramKindCodes.CODES.size());
        {
            instrumentalVariantTitleInsertMap.put(EduProgramKindCodes.DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA, "Программой");
            instrumentalVariantTitleInsertMap.put(EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA, "Направлением подготовки");
            instrumentalVariantTitleInsertMap.put(EduProgramKindCodes.PROGRAMMA_MAGISTRATURY, "Направлением подготовки");
            instrumentalVariantTitleInsertMap.put(EduProgramKindCodes.PROGRAMMA_ORDINATURY, "Направлением подготовки");
            instrumentalVariantTitleInsertMap.put(EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_, "Профессией");
            instrumentalVariantTitleInsertMap.put(EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_, "Направлением подготовки");
            instrumentalVariantTitleInsertMap.put(EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV, "Специальностью");
            instrumentalVariantTitleInsertMap.put(EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA, "Специальностью");
        }

        Map<String, String> prepositionVariantTitleUpdateMap = new HashMap<>(4);
        {
            prepositionVariantTitleUpdateMap.put(EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA, "Направлении подготовки");
            prepositionVariantTitleUpdateMap.put(EduProgramKindCodes.PROGRAMMA_MAGISTRATURY, "Направлении подготовки");
            prepositionVariantTitleUpdateMap.put(EduProgramKindCodes.PROGRAMMA_ORDINATURY, "Направлении подготовки");
            prepositionVariantTitleUpdateMap.put(EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_, "Направлении подготовки");
        }


        // Вид образовательной программы
        Map<String, Long> programKindMap = new HashMap<>(EduProgramKindCodes.CODES.size());
        {
            Statement statement = tool.getConnection().createStatement();
            statement.execute("select id, code_p from edu_c_program_kind_t ");

            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next())
            {
                programKindMap.put(resultSet.getString(2), resultSet.getLong(1));
            }
        }

        // Падеж
        Map<String, Long> variantMap = new HashMap<>(InflectorVariantCodes.CODES.size());
        {
            Statement statement = tool.getConnection().createStatement();
            statement.execute("select id, code_p from inflectorvariant_t ");

            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next())
            {
                variantMap.put(resultSet.getString(2), resultSet.getLong(1));
            }
        }

        {
            // Добавить Творительный падеж
            PreparedStatement preparedStatement = tool.getConnection().prepareStatement("insert into declinableproperty_t (id, discriminator, declinable_id, variant_id, property_p, value_p) values (?, ?, ?, ?, ?, ?) ");

            short entityCode = tool.entityCodes().get("declinableProperty");
            preparedStatement.setShort(2, entityCode);
            preparedStatement.setString(5, "eduProgramSubjectKind");

            Long variantId = variantMap.get(InflectorVariantCodes.RU_INSTRUMENTAL);
            for (Map.Entry<String, String> programKindEntry : instrumentalVariantTitleInsertMap.entrySet())
            {
                Long programKindId = programKindMap.get(programKindEntry.getKey());

                preparedStatement.setLong(1, EntityIDGenerator.generateNewId(entityCode));
                preparedStatement.setLong(3, programKindId);
                preparedStatement.setLong(4, variantId);
                preparedStatement.setString(6, programKindEntry.getValue());

                preparedStatement.execute();
            }
        }

        {
            // Обновить Предложный падеж
            PreparedStatement preparedStatement = tool.getConnection().prepareStatement("update declinableproperty_t set value_p = ? where variant_id = ? AND declinable_id = ? AND property_p = ?");
            preparedStatement.setString(4, "eduProgramSubjectKind");

            Long variantId = variantMap.get(InflectorVariantCodes.RU_PREPOSITION);
            for (Map.Entry<String, String> programKindEntry: prepositionVariantTitleUpdateMap.entrySet())
            {
                Long programKindId = programKindMap.get(programKindEntry.getKey());

                preparedStatement.setString(1, programKindEntry.getValue());
                preparedStatement.setLong(2, variantId);
                preparedStatement.setLong(3, programKindId);

                preparedStatement.execute();
            }
        }
    }
}