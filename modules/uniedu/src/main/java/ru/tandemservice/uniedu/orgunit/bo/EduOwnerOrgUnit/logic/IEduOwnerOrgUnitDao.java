package ru.tandemservice.uniedu.orgunit.bo.EduOwnerOrgUnit.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;

/**
 * @author vdanilov
 */
public interface IEduOwnerOrgUnitDao extends INeedPersistenceSupport {

    /**
     * Создает ответственное подразделение на базе подразделения
     * @param orgUnit
     * @return eduOwnerOrgUnit e | e.orgUnit = orgUnit
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    EduOwnerOrgUnit doGetEduOwnerOrgUnit(OrgUnit orgUnit);

    /**
     * Удаляет ответственное подразделение.
     * @param eduOwnerOrgUnitId id ответственного подразделения
     */
    public void deleteEduOwnerOrgUnit(Long eduOwnerOrgUnitId);
}
