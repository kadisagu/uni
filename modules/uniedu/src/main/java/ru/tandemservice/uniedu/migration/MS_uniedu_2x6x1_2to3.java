package ru.tandemservice.uniedu.migration;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import static org.tandemframework.dbsupport.sql.SQLFrom.table;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniedu_2x6x1_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eduProgramSpecialization

		// создано свойство shortTitle
        if(tool.tableExists("edu_specialization_base_t") && !tool.columnExists("edu_specialization_base_t", "shorttitle_p"))
		{
			// создать колонку
			tool.createColumn("edu_specialization_base_t", new DBColumn("shorttitle_p", DBType.createVarchar(255)));

            PreparedStatement updateEduSpecShortTitle = tool.prepareStatement("update edu_specialization_base_t set shorttitle_p = ? where id = ?");

            ISQLTranslator translator = tool.getDialect().getSQLTranslator();

            SQLSelectQuery eduSpecQuery = new SQLSelectQuery();
            eduSpecQuery.from(
                    table("edu_specialization_base_t", "es"));
            eduSpecQuery.column("es.id", "id");
            eduSpecQuery.column("es.title_p", "title");

            String eduSpecSql = translator.toSql(eduSpecQuery);

            Statement eduSpecStatement = tool.getConnection().createStatement();
            eduSpecStatement.execute(eduSpecSql);

            ResultSet eduSpecResult = eduSpecStatement.getResultSet();

            while (eduSpecResult.next())
            {
                Long id = eduSpecResult.getLong("id");
                String title = eduSpecResult.getString("title").replaceAll("[`|«|»|—|~|!|@|#|$|%|^|&|*|_|+|\\||=|\\\\|\\[|\\]|{|}|;|:|'|\"|,|.|/|<|>|?|№]", "");
                title = title.replace("-", " ");
                title = title.replace("(", " ( ");
                title = StringUtils.normalizeSpace(title.replace(")", " ) "));
                StringBuilder shortTitleBuilder = new StringBuilder();
                for(String subStr : title.split(" "))
                {
                    if(subStr.equals("(") || subStr.equals(")"))
                        shortTitleBuilder.append(subStr);
                    else if(subStr.length() > 3)
                        shortTitleBuilder.append(StringUtils.capitalize(subStr).charAt(0));
                }

                String shortTitle = shortTitleBuilder.toString();

                updateEduSpecShortTitle.clearParameters();
                updateEduSpecShortTitle.setString(1, shortTitle);
                updateEduSpecShortTitle.setLong(2, id);
                updateEduSpecShortTitle.executeUpdate();
            }
		}
    }
}
