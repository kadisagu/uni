/* $Id:$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.ui.QualificationAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.EduProgramSubjectManager;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

import java.util.Arrays;
import java.util.List;

/**
 * @author oleyba
 * @since 11/21/13
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id")
})
public class EduProgramSubjectQualificationAddUI extends UIPresenter
{
    private final EntityHolder<EduProgramSubject> holder = new EntityHolder<>();

    private List<IdentifiableWrapper> qualOptions;
    private IdentifiableWrapper qualOption;

    private String newQualTitle;
    private String newQualCode;

    private EduProgramQualification qualification;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        setQualOptions(Arrays.asList(new IdentifiableWrapper(0L, "Создать новую квалификацию"), new IdentifiableWrapper(1L, "Выбрать из уже заданных для других направлений (профессий, специальностей)")));

        if (getQualOption() == null)
            setQualOption(new IdentifiableWrapper(0L, "Создать новую квалификацию"));
    }

    public void onClickApply() {
        if (isCreate()) {
            EduProgramSubjectManager.instance().dao().addQualification(getHolder().getValue(), getNewQualCode(), getNewQualTitle());
        }
        else {
            EduProgramSubjectManager.instance().dao().addQualification(getHolder().getValue(), getQualification());
        }
        deactivate();
    }

    // presenter


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EduProgramSubjectQualificationAdd.PARAM_SUBJ_INDEX, getHolder().getValue().getSubjectIndex());
    }

    public boolean isCreate() {
        return getQualOption() != null && getQualOption().getId().equals(0L);
    }

    // getters and setters

    public EntityHolder<EduProgramSubject> getHolder()
    {
        return holder;
    }

    public String getNewQualTitle()
    {
        return newQualTitle;
    }

    public void setNewQualTitle(String newQualTitle)
    {
        this.newQualTitle = newQualTitle;
    }

    public EduProgramQualification getQualification()
    {
        return qualification;
    }

    public void setQualification(EduProgramQualification qualification)
    {
        this.qualification = qualification;
    }

    public IdentifiableWrapper getQualOption()
    {
        return qualOption;
    }

    public void setQualOption(IdentifiableWrapper qualOption)
    {
        this.qualOption = qualOption;
    }

    public List<IdentifiableWrapper> getQualOptions()
    {
        return qualOptions;
    }

    public void setQualOptions(List<IdentifiableWrapper> qualOptions)
    {
        this.qualOptions = qualOptions;
    }

    public String getNewQualCode()
    {
        return newQualCode;
    }

    public void setNewQualCode(String newQualCode)
    {
        this.newQualCode = newQualCode;
    }
}