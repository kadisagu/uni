/**
 *$Id$
 */
package ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.EduInstitutionOrgUnitManager;

/**
 * @author Alexander Zhebko
 * @since 03.02.2014
 */
public class EduInstitutionOrgUnitAddUI extends UIPresenter
{
    private OrgUnit _orgUnit;
    public OrgUnit getOrgUnit(){ return _orgUnit; }
    public void setOrgUnit(OrgUnit orgUnit){ _orgUnit = orgUnit; }

    public void onClickApply()
    {
        EduInstitutionOrgUnitManager.instance().dao().doGetEduInstitutionOrgUnit(getOrgUnit());
        deactivate();
    }
}