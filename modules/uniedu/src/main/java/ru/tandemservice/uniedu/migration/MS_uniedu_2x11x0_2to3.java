package ru.tandemservice.uniedu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.sql.PreparedStatement;
import java.util.List;

import static org.tandemframework.shared.commonbase.utils.MigrationUtils.processor;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniedu_2x11x0_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность licenseInOrgUnit
        short licenseInOrgUnitCode;
		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("edu_license_org_unit_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_licenseinorgunit"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("programsubject_id", DBType.LONG).setNullable(false), 
				new DBColumn("institutionorgunit_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
            licenseInOrgUnitCode = tool.entityCodes().ensure("licenseInOrgUnit");

		}

        //Получаем данные для миграции
        //Головное лицензированное подразделение
        Long licensedId = (Long) tool.getUniqueResult("select iou.id from edu_ourole__inst_t iou join toporgunit_t tou on iou.orgunit_id = tou.id");
        //список лицензированных направлений подготовки
        List<Object[]> programSubjectList = tool.executeQuery(processor(1),
                                                              "select id from edu_c_pr_subject_t where license_p = ?", true);
        //число имеющихся аккредитованных УГНС
        Number count = (Number) tool.getUniqueResult("select count(id) from edu_accreditation_t");

        //если лицензированное головное в системе отсутсвует, а данные для миграции есть - создаем новое
        if (licensedId == null && (count.longValue() > 0 || !programSubjectList.isEmpty()))
        {
            Long topOrgUnitId = (Long) tool.getUniqueResult("select id from toporgunit_t tou " +
                                                                    "join orgunit_t ou on ou.id=tou.id " +
                                                                    "join orgunittype_t out on ou.type_id=out.id " +
                                                                    "where uot.code_p='top'");
            if (topOrgUnitId == null)
                throw new ApplicationException("Невозможно выполнить миграцию. В системе не указано головное подразделение.");


            short institutionCode = tool.entityCodes().get("eduInstitutionOrgUnit");
            licensedId = EntityIDGenerator.generateNewId(institutionCode);

            tool.executeUpdate("insert into edu_ourole__inst_t (id, discriminator, orgunit_id) values (?, ?, ?)", licensedId, institutionCode, topOrgUnitId);
        }


        ////////////////////////////////////////////////////////////////////////////////
		// сущность eduAccreditation

		// создано обязательное свойство institutionOrgUnit
		{
			// создать колонку
			tool.createColumn("edu_accreditation_t", new DBColumn("institutionorgunit_id", DBType.LONG));

			// задать значение по умолчанию
            tool.executeUpdate("update edu_accreditation_t set institutionorgunit_id=? where institutionorgunit_id is null", licensedId);

			// сделать колонку NOT NULL
			tool.setColumnNullable("edu_accreditation_t", "institutionorgunit_id", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eduProgramSubject

		// удалено свойство license
		{
            //Прежде, чем дропнуть колонку - надо перенести данные в новую сущность
            PreparedStatement update = tool.prepareStatement("insert into edu_license_org_unit_t (id, discriminator, institutionorgunit_id, programsubject_id) values(?, ?, ?, ?)");
            update.setShort(2, licenseInOrgUnitCode);
            update.setLong(3, licensedId);

            int counter = 0;
            for (Object[] row : programSubjectList)
            {
                update.setLong(1, EntityIDGenerator.generateNewId(licenseInOrgUnitCode));
                update.setLong(4, (Long) row[0]);
                update.addBatch();

                if (++counter> 256)
                {
                    update.executeBatch();
                    counter = 0;
                }
            }

            update.executeBatch();

			// удалить колонку
			tool.dropColumn("edu_c_pr_subject_t", "license_p");

		}


    }
}