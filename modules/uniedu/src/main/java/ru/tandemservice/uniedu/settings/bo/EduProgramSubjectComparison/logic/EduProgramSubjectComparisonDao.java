/* $Id:$ */
package ru.tandemservice.uniedu.settings.bo.EduProgramSubjectComparison.logic;

import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.entity.EduProgramSubjectComparison;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 18.03.2015
 */
public class EduProgramSubjectComparisonDao extends UniBaseDao implements IEduProgramSubjectComparisonDao
{
    @Override
    public void fillComparisonsByNameOverlap(EduProgramSubjectIndex oldIndex, EduProgramSubjectIndex newIndex)
    {
        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EduProgramSubject.class, "oldSubj")
                .column(property("oldSubj", "id"))
                .column(DQLFunctions.min(property("newSubj", "id")))
                .joinEntity("oldSubj", DQLJoinType.inner, EduProgramSubject2013.class, "newSubj",
                        eq(DQLFunctions.upper(property("oldSubj", EduProgramSubject.title())), DQLFunctions.upper(property("newSubj", EduProgramSubject.title()))))
                .where(eq(property("oldSubj", EduProgramSubject.subjectIndex()), value(oldIndex)))
                .where(eq(property("newSubj", EduProgramSubject.subjectIndex()), value(newIndex)))
                .where(notExists(EduProgramSubjectComparison.class, EduProgramSubjectComparison.oldProgramSubject().s(), property("oldSubj")))
                .group(property("oldSubj", "id"))
                .having(eq(DQLFunctions.countStar(), value(1)));

        for (Object[] record : this.<Object[]>getList(builder)) {
            final EduProgramSubject oldSubject = (EduProgramSubject) getSession().load(EduProgramSubject.class, (Long) record[0] );
            final EduProgramSubject2013 newSubject = (EduProgramSubject2013) getSession().load(EduProgramSubject2013.class, (Long) record[1] );
            final EduProgramSubjectComparison comparison = new EduProgramSubjectComparison();
            comparison.setOldProgramSubject(oldSubject);
            comparison.setNewProgramSubject(newSubject);
            save(comparison);
        }
    }

}
