/* $Id:$ */
package ru.tandemservice.uniedu.settings.bo.EduProgramSubjectComparison.ui.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.entity.EduProgramSubjectComparison;

/**
 * @author rsizonenko
 * @since 16.03.2015
 */
public class SubjectComparisonWrapper  extends IdentifiableWrapper<EduProgramSubject>{

    public SubjectComparisonWrapper(EduProgramSubject i, EduProgramSubjectComparison comparison) throws ClassCastException {
        super(i);
        setSubject(i);
        setComparison(comparison);
    }

    private EduProgramSubjectComparison comparison;
    private EduProgramSubject subject;

    public EduProgramSubjectComparison getComparison() {
        return comparison;
    }

    public void setComparison(EduProgramSubjectComparison comparison) {
        this.comparison = comparison;

    }

    public EduProgramSubject getSubject() {
        return subject;
    }

    public void setSubject(EduProgramSubject subject) {
        this.subject = subject;
    }

}
