/* $Id$ */
package ru.tandemservice.uniedu.migration;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.meta.entity.IRelationMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author Nikolay Fedorovskih
 * @since 24.06.2015
 */
public class MS_uniedu_2x8x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[] {
                new ScriptDependency("org.tandemframework", "1.6.17"),
                new ScriptDependency("org.tandemframework.shared", "1.8.2")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        migrate(tool);
    }

    public static void migrate(DBTool tool) throws SQLException
    {
        /*
        ----------- DEV-7979 -----------------------------------------

        В базе сейчас есть следующие programSubject:
        А - 08.05.02 Строительство железных дорог, мостов и транспортных тоннелей
        В - 23.05.06 Строительство железных дорог, мостов и транспортных тоннелей
        С - 08.05.03 Строительство, эксплуатация, восстановление и техническое прикрытие автомобильных дорог, мостов и тоннелей

        Если на 2.8.1 уже успели синхронизировать справочник, то есть A превращается в D:
        D - 08.05.02 Строительство, эксплуатация, восстановление и техническое прикрытие автомобильных дорог, мостов и тоннелей
        Считаем, что A == D

        Написать миграцию, которая сделает следующее:
        - Все объекты, ссылающиеся на A, перекинуть на В.
        - Удалить А.
        - исправить код С на 08.05.02

        --------------------------------------------------------------
        */

        final String sql = "select id from edu_c_pr_subject_t where subjectcode_p=? and title_p=?";
        long A = tool.getNumericResult(sql, "08.05.02", "Строительство железных дорог, мостов и транспортных тоннелей");
        long B = tool.getNumericResult(sql, "23.05.06", "Строительство железных дорог, мостов и транспортных тоннелей");
        long C = tool.getNumericResult(sql, "08.05.03", "Строительство, эксплуатация, восстановление и техническое прикрытие автомобильных дорог, мостов и тоннелей");
        long D = tool.getNumericResult(sql, "08.05.02", "Строительство, эксплуатация, восстановление и техническое прикрытие автомобильных дорог, мостов и тоннелей");

        if (A == 0L && D != 0L) {

            if (C == 0L) {
                // У ПГУПСа миграция уже проходила и кнопочку они потом нажимали - получается, что у них D - это C, а не A, как у остальных.
                // Короче, им ничего не надо мигрировать.
                return;
            }

            A = D;
        }

        if (A != 0L && B == 0L)
        {
            // После 2.8.0 кнопку синхронизации не нажимали - направления "23.05.06" вообще нет.
            // Можно просто код A меняем на код B и всё будет шоколадно.

            if (tool.executeUpdate("update edu_c_pr_subject_t set subjectcode_p=?, code_p=? where id=?", "23.05.06", "2013.23.05.06", A) > 0 && Debug.isEnabled()) {
                System.out.println("Simple update code for eduProgramSubject from '08.05.02' to '23.05.06' [part A-B]");
            }

        } else if (A != 0L) {

            // Все объекты, ссылающиеся на A, перекинуть на В. Удалить А.
            // Аналогично поступаем c eduProgramSpecializationRoot - может быть только одна общая направленность на направление (по усти связь 1 к 1) - и другими..

            if (Debug.isEnabled()) {
                System.out.println("---- Start merge A to B, drop A ----");
            }

            mergeRows(tool, A, B, ImmutableSet.of(
                    EntityRuntime.getMeta("eduProgramSubjectQualification"),
                    EntityRuntime.getMeta("eduProgramSpecializationRoot"),
                    EntityRuntime.getMeta("educationLevels")
            ));
        }


        if (C != 0L)
        {
            // Исправить код С на 08.05.02

            if (tool.executeUpdate("update edu_c_pr_subject_t set subjectcode_p=?, code_p=? where id=?", "08.05.02", "2013.08.05.02", C) > 0 && Debug.isEnabled()) {
                System.out.println("Simple update code for eduProgramSubject from '08.05.03' to '08.05.02' [part C]");
            }
        }
    }

    @SuppressWarnings("unchecked")
    private static void mergeRows(DBTool tool, long deletingId, long mainId, Set<IEntityMeta> cascadeMergeEntities) throws SQLException
    {
        final IEntityMeta entityMeta = EntityRuntime.getMeta(deletingId);

        Preconditions.checkArgument(EntityRuntime.getMeta(mainId).equals(entityMeta));

        // Собираем входящие ссылки на сущность: она сама, родители, интерфейсы - на всех может кто-то ссылаться, и надо эти ссылки помёржить.
        // Детей не берем. (?)

        final Set<IEntityMeta> metaPlainTree = new LinkedHashSet<>();
        metaPlainTree.add(entityMeta);
        metaPlainTree.addAll(entityMeta.getInterfaces());
        for (IEntityMeta parent = entityMeta.getParent(); parent != null; parent = parent.getParent()) {
            metaPlainTree.add(parent);
            metaPlainTree.addAll(parent.getInterfaces());
        }

        final Set<IRelationMeta> incomingRelations = new LinkedHashSet<>();
        for (IEntityMeta meta : metaPlainTree) {
            incomingRelations.addAll(meta.getIncomingRelations());
        }

        // Мигрируем входящие ссылки на новый идентификатор

        for (IRelationMeta relMeta : incomingRelations)
        {
            final IEntityMeta relEntity = relMeta.getForeignEntity();
            if (relEntity.hasTable())
            {
                final String relTableName = relEntity.getTableName();
                final String relColumnName = relMeta.getForeignProperty().getColumnName();

                if (0L == tool.getNumericResult("select count(*) from " + relTableName + " where " + relColumnName + "=?", deletingId)) {
                    continue; // В таблице нет ссылок на удаляемый идентификатор - ничего делать не нужно (ни мержить ни апдейтить).
                }

                // Убираем констрейнты
                tool.table(relTableName).constraints().clear();

                // Некоторые ссылающиеся сущности также надо помержить.
                // Например eduProgramSpecializationRoot, которая может быть только одна для направления.
                // Ссылки на саму себя не трогаем, чтобы не зациклиться (?).
                // Ссылки всегда должны быть 1 к 1 (хотя, можно и много к 1, но это, вроде бы, не наш случай).
                // Надо проверить также дочерние сущности (как в случае с eduProgramSpecializationRoot, которая сама по себе не ссылается на eduProgramSubject).
                // Проверяются только непосредственные дети, т.к. этого пока достаточно.

                if ((cascadeMergeEntities.contains(relEntity) || CollectionUtils.containsAny(cascadeMergeEntities, relEntity.getChildren())) && !entityMeta.equals(relMeta))
                {
                    final String sql = "select id from " + relTableName + " where " + relColumnName + "=?";
                    final long relMainId = tool.getNumericResult(sql, mainId); // Если тут валится (пришло больше одного результата), то надо разбираться
                    final long relDeletingId = tool.getNumericResult(sql, deletingId); // Если свалится (пришло больше одного результата), то можно добавить поддержку такого случая - мерж многих на одного

                    if (relDeletingId != 0L && relMainId != 0) {
                        // Рекурсивно делаем то же самое
                        mergeRows(tool, relDeletingId, relMainId, cascadeMergeEntities);
                    }
                }

                final int ret = tool.executeUpdate("update " + relTableName + " set " + relColumnName + "=? where " + relColumnName + "=?",  mainId, deletingId);

                if (Debug.isEnabled()) {
                    System.out.println(entityMeta.getName() + " move links: " + ret + " for " + relEntity.getName() + "." + relMeta.getForeignProperty().getName());
                }
            }
        }

        // Удаляем строки из всех дочерних и базовой таблиц

        for (IEntityMeta meta : metaPlainTree)
        {
            if (!meta.isInterface() && meta.hasTable())
            {
                final long ret = tool.executeUpdate("delete from " + meta.getTableName() + " where id=?", deletingId);

                if (Debug.isEnabled()) {
                    System.out.println("" + ret + " rows deleted (merged) from " + meta.getName());
                }
            }
        }
    }
}