/* $Id:$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;

/**
 * @author oleyba
 * @since 11/21/13
 */
@Configuration
public class EduProgramSubjectPub extends BusinessComponentManager
{
    public static final String TAB_PANEL = "eduProgramSubjectTabPanel";
    public static final String TAB_PANEL_REGION_NAME = "eduProgramSubjectMainRegion";

    public static final String MAIN_TAB = "mainTab";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .create();
    }

    @Bean
    public TabPanelExtPoint eduProgramSubjectTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(htmlTab(MAIN_TAB, "Pub"))
                .create();
    }
}
