/*$Id$*/
package ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author DMITRY KNYAZEV
 * @since 16.05.2014
 */
@Configuration
public class EduInstitutionOrgUnitEdit extends BusinessComponentManager
{
	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.create();
	}
}
