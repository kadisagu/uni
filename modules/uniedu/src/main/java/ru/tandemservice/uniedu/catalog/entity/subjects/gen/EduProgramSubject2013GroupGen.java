package ru.tandemservice.uniedu.catalog.entity.subjects.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013Field;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013Group;
import ru.tandemservice.uniedu.entity.IPersistentAccreditationOwner;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Укрупненная группа (перечень направлений подготовки 2013)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramSubject2013GroupGen extends EntityBase
 implements IPersistentAccreditationOwner, INaturalIdentifiable<EduProgramSubject2013GroupGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013Group";
    public static final String ENTITY_NAME = "eduProgramSubject2013Group";
    public static final int VERSION_HASH = -2110431097;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_FIELD = "field";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private EduProgramSubject2013Field _field;     // Область знаний
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Область знаний. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubject2013Field getField()
    {
        return _field;
    }

    /**
     * @param field Область знаний. Свойство не может быть null.
     */
    public void setField(EduProgramSubject2013Field field)
    {
        dirty(_field, field);
        _field = field;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EduProgramSubject2013GroupGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EduProgramSubject2013Group)another).getCode());
            }
            setField(((EduProgramSubject2013Group)another).getField());
            setTitle(((EduProgramSubject2013Group)another).getTitle());
        }
    }

    public INaturalId<EduProgramSubject2013GroupGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EduProgramSubject2013GroupGen>
    {
        private static final String PROXY_NAME = "EduProgramSubject2013GroupNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EduProgramSubject2013GroupGen.NaturalId) ) return false;

            EduProgramSubject2013GroupGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramSubject2013GroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramSubject2013Group.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramSubject2013Group();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "field":
                    return obj.getField();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "field":
                    obj.setField((EduProgramSubject2013Field) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "field":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "field":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "field":
                    return EduProgramSubject2013Field.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgramSubject2013Group> _dslPath = new Path<EduProgramSubject2013Group>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramSubject2013Group");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013Group#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Область знаний. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013Group#getField()
     */
    public static EduProgramSubject2013Field.Path<EduProgramSubject2013Field> field()
    {
        return _dslPath.field();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013Group#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EduProgramSubject2013Group> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private EduProgramSubject2013Field.Path<EduProgramSubject2013Field> _field;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013Group#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EduProgramSubject2013GroupGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Область знаний. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013Group#getField()
     */
        public EduProgramSubject2013Field.Path<EduProgramSubject2013Field> field()
        {
            if(_field == null )
                _field = new EduProgramSubject2013Field.Path<EduProgramSubject2013Field>(L_FIELD, this);
            return _field;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013Group#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EduProgramSubject2013GroupGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EduProgramSubject2013Group.class;
        }

        public String getEntityName()
        {
            return "eduProgramSubject2013Group";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
