/**
 *$Id$
 */
package ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.ui.AddEdit;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.EduProgramSubjectIndexManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.catalog.entity.codes.EduLevelCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationChild;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationRoot;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.EduProgramHigherProfManager;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.ui.Pub.EduProgramHigherProfPub;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;

import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 06.02.2014
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id"))
public class EduProgramHigherProfAddEditUI extends UIPresenter
{
    private static final IdentifiableWrapper<IEntity> EDU_PROGRAM_SPECIALIZATION_TYPE_COMMON = new IdentifiableWrapper<>(1L, "Общая (соответствует направлению подготовки, специальности)");
    private static final IdentifiableWrapper<IEntity> EDU_PROGRAM_SPECIALIZATION_TYPE_SUBJECT = new IdentifiableWrapper<>(2L, "В рамках направления подготовки, специальности");

    @SuppressWarnings("unchecked")
    private static final List<IdentifiableWrapper<IEntity>> EDU_PROGRAM_SPECIALIZATION_TYPE_LIST = ImmutableList.of(
        EDU_PROGRAM_SPECIALIZATION_TYPE_COMMON,
        EDU_PROGRAM_SPECIALIZATION_TYPE_SUBJECT
    );

    private EntityHolder<EduProgramHigherProf> _holder = new EntityHolder<>();
    private EduProgramSubjectIndex _eduProgramSubjectIndex;
    private IdentifiableWrapper<IEntity> _eduProgramSpecializationType;
    private EduProgramSpecializationChild _eduProgramSpecialization;


    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        if (getHolder().getValue() == null) {
            getHolder().setValue(new EduProgramHigherProf());
        }

        if (isEditForm())
        {
            EduProgramSpecialization specialization = getEduProgram().getProgramSpecialization();
            if (specialization instanceof EduProgramSpecializationRoot) {
                setEduProgramSpecializationType(EDU_PROGRAM_SPECIALIZATION_TYPE_COMMON);

            } else if (specialization instanceof EduProgramSpecializationChild) {
                setEduProgramSpecializationType(EDU_PROGRAM_SPECIALIZATION_TYPE_SUBJECT);
                setEduProgramSpecialization((EduProgramSpecializationChild) specialization);
            }

            setEduProgramSubjectIndex(getEduProgram().getProgramSubject().getSubjectIndex());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EduProgramHigherProfManager.EDU_PROGRAM_KIND, getEduProgram().getKind());
        dataSource.put(EduProgramSubjectIndexManager.PARAM_SUBJECT_INDEX, getEduProgramSubjectIndex());
        dataSource.put(EduProgramHigherProfAddEdit.EDU_PROGRAM_SUBJECT, getEduProgram().getProgramSubject());
        dataSource.put(EduInstitutionOrgUnit.PARAM_EDIT_FORM, isEditForm());
    }

    public void onChangeProgramKind()
    {
        if (getEduProgram().getKind() != null && getEduProgram().getKind().isBS()) {
            getEduProgram().setBaseLevel(IUniBaseDao.instance.get().getCatalogItem(EduLevel.class, EduLevelCodes.SREDNEE_OBTSHEE_OBRAZOVANIE));
        }
    }

    public void onClickApply()
    {
        EduProgramHigherProfManager.instance().dao().saveOrUpdateEduProgramHigherProf(getEduProgram(), isEduProgramSpecializationSubject() ? getEduProgramSpecialization() : null);
        deactivate();
        if (isAddForm())
            _uiActivation.asDesktopRoot(EduProgramHigherProfPub.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, getEduProgram().getId()).activate();
    }

    public boolean isAddForm(){ return getHolder().getId() == null; }
    public boolean isEditForm(){ return !isAddForm(); }
    public boolean isEduProgramSpecializationSubject(){ return getEduProgramSpecializationType() != null && getEduProgramSpecializationType().equals(EDU_PROGRAM_SPECIALIZATION_TYPE_SUBJECT); }
    public List<IdentifiableWrapper<IEntity>> getEduProgramSpecializationTypeList(){ return EDU_PROGRAM_SPECIALIZATION_TYPE_LIST; }

    public boolean isShowOrientation() {
        return IUniBaseDao.instance.get().existsEntity(EduProgramOrientation.class, EduProgramOrientation.subjectIndex().s(), getEduProgramSubjectIndex());
    }

    // getters and setters

    public EntityHolder<EduProgramHigherProf> getHolder(){ return _holder; }
    public EduProgramHigherProf getEduProgram(){ return getHolder().getValue(); }

    public EduProgramSubjectIndex getEduProgramSubjectIndex(){ return _eduProgramSubjectIndex; }
    public void setEduProgramSubjectIndex(EduProgramSubjectIndex eduProgramSubjectIndex){ _eduProgramSubjectIndex = eduProgramSubjectIndex; }

    public IdentifiableWrapper<IEntity> getEduProgramSpecializationType(){ return _eduProgramSpecializationType; }
    public void setEduProgramSpecializationType(IdentifiableWrapper<IEntity> eduProgramSpecializationType){ _eduProgramSpecializationType = eduProgramSpecializationType; }

    public EduProgramSpecializationChild getEduProgramSpecialization(){ return _eduProgramSpecialization; }
    public void setEduProgramSpecialization(EduProgramSpecializationChild eduProgramSpecialization){ _eduProgramSpecialization = eduProgramSpecialization; }

	public OrgUnit getOrgUnit()
	{
		EduOwnerOrgUnit eduOwnerOrgUnit = getEduProgram().getOwnerOrgUnit();
		return (eduOwnerOrgUnit == null) ? null : eduOwnerOrgUnit.getOrgUnit();
	}

	public void setOrgUnit(OrgUnit orgUnit)
	{
		EduOwnerOrgUnit eduOwnerOrgUnit = (orgUnit == null) ? null : DataAccessServices.dao().getByNaturalId(new EduOwnerOrgUnit.NaturalId(orgUnit));
		getEduProgram().setOwnerOrgUnit(eduOwnerOrgUnit);
	}
}