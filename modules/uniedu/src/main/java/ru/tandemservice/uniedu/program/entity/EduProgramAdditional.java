package ru.tandemservice.uniedu.program.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniedu.catalog.entity.codes.EduLevelCodes;
import ru.tandemservice.uniedu.program.entity.gen.EduProgramAdditionalGen;

/**
 * Дополнительная образовательная программа
 *
 * Дополнительное образование: реализуются на усмотрение ОУ, определяется видом и названием.
 */
public abstract class EduProgramAdditional extends EduProgramAdditionalGen
{

//    @Override
//    protected StringBuilder uniqueKeyBuilder() {
//        throw new UnsupportedOperationException(); // пока мы не поддерживаем такие ОП
//    }

    @Override
    protected StringBuilder titleBuilder() {

        StringBuilder builder = super.titleBuilder()
                .append(getTitle())
                .append(" (")
                .append(this.getDuration().getTitle())
                .append(", ")
                .append(this.getForm().getShortTitle())
                .append(")");

        return builder;
    }

    @EntityDSLSupport(parts = EduProgramSecondaryProf.P_TITLE)
    public String getFullTitle() {
        return generateTitle();
    }
}