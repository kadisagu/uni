package ru.tandemservice.uniedu.catalog.entity.subjects;

import ru.tandemservice.uniedu.catalog.entity.subjects.gen.*;
import ru.tandemservice.uniedu.entity.IPersistentAccreditationOwner;

/**
 * Направление подготовки профессионального образования (2013)
 *
 * Направление классификатора 2013 года: по каждому виду ОП свой классификатор.
 * Направление подготовки в системе совпадает с элементом в классификаторе.
 * Перечень областей знаний и укрупненных групп - общий.
 */
public class EduProgramSubject2013 extends EduProgramSubject2013Gen
{
    @Override
    public String getGroupTitle()
    {
        return getGroup().getTitle() + " (" + getGroup().getField().getTitle() + ")";
    }

    @Override
    public boolean isLabor()
    {
        // Для перечней ВО 2013 трудоемкость в ЗЕ
        return getEduProgramKind().isProgramHigherProf();
    }

    @Override public boolean isOKSO() { return false; }
    @Override public boolean is2009() { return false; }
    @Override public boolean is2013() { return true; }

    @Override
    public IPersistentAccreditationOwner getSubjectGroup()
    {
        return getGroup();
    }

}