/* $Id$ */
package ru.tandemservice.uniedu.program.daemon;

import ru.tandemservice.uni.dao.UniBaseDao;
/**
 * @author nvankov
 * @since 2/21/14
 */
public class QualificationLinkUpdateDaemonDao extends UniBaseDao implements IQualificationLinkUpdateDaemonDao
{
    // todo DEV-5818
//    public static final SyncDaemon DAEMON = new SyncDaemon(QualificationLinkUpdateDaemonDao.class.getName(), 120, IQualificationLinkUpdateDaemonDao.GLOBAL_DAEMON_LOCK) {
//        @Override protected void main()
//        {
//            // Обновляет квалификации образовательных программ для образовательных программ ВПО (по свойству квалификация образовательной программы ВПО)
//            try { IQualificationLinkUpdateDaemonDao.instance.get().doUpdateQualificationLink(); }
//            catch (final Throwable t) { Debug.exception(t.getMessage(), t); }
//        }
//    };
//
//    @Override
//    protected void initDao()
//    {
//        // вызывать демон при изменении образовательной программы ВПО
//        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EduProgramHigherProf.class, DAEMON.getAfterCompleteWakeUpListener());
//        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EduProgramHigherProf.class, DAEMON.getAfterCompleteWakeUpListener());
//    }
//
//    @Override
//    public void doUpdateQualificationLink()
//    {
//        Debug.begin("QualificationLinkUpdateDaemonDao.doUpdateQualificationLink");
//        try
//        {
//            final Session session = getSession();
//
//            // удаляем квалификации образовательных программ, у которых квалификация отличаются от квалификации обр. программы ВПО
//            executeAndClear(
//                new DQLDeleteBuilder(EduProgram2QualificationLink.class)
//                .where(in(
//                    property(EduProgram2QualificationLink.id()),
//                    new DQLSelectBuilder()
//                    .fromEntity(EduProgram2QualificationLink.class, "link")
//                    .column(property("link", EduProgram2QualificationLink.id()))
//                    .fromEntity(EduProgramHigherProf.class, "ep") // именно Higher
//                    .where(eq(property("ep", EduProgramHigherProf.id()), property("link", EduProgram2QualificationLink.id())))
//                    .where(ne(property("ep", EduProgramHigherProf.programQualification().id()), property("link", EduProgram2QualificationLink.programQualification().id())))
//                    .buildQuery()
//                ))
//            );
//
//            // создаем квалификации обр. программ, для обр. программ ВПО
//            List<Object[]> rows = new DQLSelectBuilder()
//            .fromEntity(EduProgramHigherProf.class, "ep")
//            .where(notExists(
//                new DQLSelectBuilder()
//                .fromEntity(EduProgram2QualificationLink.class, "link")
//                .column(property("link.id"))
//                .where(eq(property("link", EduProgram2QualificationLink.program()), property("ep"))) // если связь есть, то она единсвенная и уже правильная (все остальные мы выклсили выше)
//                .buildQuery()
//            ))
//            .column(property("ep", EduProgramHigherProf.id()))
//            .column(property("ep", EduProgram2QualificationLink.programQualification().id()))
//            .createStatement(session).list();
//
//            // здесь нужен batch, тем более при старте демона
//            BatchUtils.execute(rows, 256, new BatchUtils.Action<Object[]>() {
//                @Override public void execute(Collection<Object[]> rows) {
//                    for(Object[] row : rows) {
//                        Long eduProgramId = (Long) row[0];
//                        Long qualId = (Long) row[1];
//                        EduProgram2QualificationLink link = new EduProgram2QualificationLink();
//                        link.setProgram((EduProgramHigherProf) session.load(EduProgramHigherProf.class, eduProgramId));
//                        link.setProgramQualification((EduProgramQualification) session.load(EduProgramQualification.class, qualId));
//                        session.save(link);
//                    }
//
//                    session.flush();
//                    session.clear();
//                }
//            });
//        }
//        finally
//        {
//            Debug.end();
//        }
//    }
}
