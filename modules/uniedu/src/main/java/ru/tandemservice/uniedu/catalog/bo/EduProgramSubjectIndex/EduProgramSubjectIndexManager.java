/**
 *$Id: EppWorkPlanManager.java 26089 2013-02-11 08:34:37Z vdanilov $
 */
package ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex;

import java.io.InputStream;
import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtPointBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.shared.commonbase.utils.jsonImport.JsonEntitySynchronizer;

import ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.logic.EduProgramSubjectIndexDao;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.logic.IEduProgramSubjectIndexDao;

/**
 * @author Zhuj
 */
@Configuration
public class EduProgramSubjectIndexManager extends BusinessObjectManager
{
    public static final String PARAM_SUBJECT_INDEX = "subjectIndex";

    public static EduProgramSubjectIndexManager instance() {
        return instance(EduProgramSubjectIndexManager.class);
    }

    @Bean
    public IEduProgramSubjectIndexDao dao() {
        return new EduProgramSubjectIndexDao();
    }

    @Bean
    public ItemListExtPoint<String> jsonList() {

        final IItemListExtPointBuilder<String> builder = itemList(String.class);

        Arrays.stream(new String[] {
                "uniedu/init/subjects/subject-list.init.json",           /* набор перечней */

                "uniedu/init/subjects/2005.groups.init.json",            /* УГС для 2005 и 2009 (единый) */

                "uniedu/init/subjects/2005.A.items.init.json",           /* структура направлений и специальностей (только 2005 год) */
                "uniedu/init/subjects/2005.B.qlist.init.json",           /* квалификации (только 2005 год) */
                "uniedu/init/subjects/2005.C.subjs.init.json",           /* обобщенные направления (только 2005 год) */
                "uniedu/init/subjects/2005.D.links.init.json",           /* связи квалификаций и обобщенных направлений (только 2005 год) */

                "uniedu/init/subjects/2009.A.qlist.init.json",           /* квалификации (только 2009 год) */
                "uniedu/init/subjects/2009.B.subjs.init.json",           /* обобщенные направления (только 2009 год) */
                "uniedu/init/subjects/2009.C.links.init.json",           /* связи квалификаций и обобщенных направлений (только 2009 год) */

                "uniedu/init/subjects/2013.fields.init.json",            /* Области знаний (2013) */
                "uniedu/init/subjects/2013.groups.init.json",            /* Укрупненные группы (2013) */

                "uniedu/init/subjects/2013.A.subjs.init.json",           /* обобщенные направления (только 2013 год) */
                "uniedu/init/subjects/2013.B.qlist.init.json",           /* квалификации (только 2013 год) */
                "uniedu/init/subjects/2013.C.links.init.json",           /* связи квалификаций и обобщенных направлений (только 2013 год) */
        }).forEach(json -> builder.add(json, json));

        return builder.create();
    }

    /**
     * Обновляет набор перечней напрввлений подготовки и квалификаций,
     * направления подготовки и квалификации внутри перечней,
     * связи квалификаций и направлений подготовки.
     * 
     * Данные берутся из json-файлов (resources/uniedu/init/subjects).
     * Для синхронизации используется механизм {@link org.tandemframework.shared.commonbase.utils.jsonImport.JsonEntitySynchronizer}.
     * 
     * @see org.tandemframework.shared.commonbase.utils.jsonImport.JsonEntitySynchronizer
     */
    public void syncStaticData() {

        // грузим статические данные (каждый файл в своей транзакции)
        for (final String json: instance().jsonList().getItems().values()) {

            Debug.begin(json);
            try (InputStream stream = this.getClass().getClassLoader().getResourceAsStream(json)) {
                // каждый файл - в своей транзакции (чтобы не случалось ничего, связанного с переполнением памяти на стороне базы)
                JsonEntitySynchronizer.doSyncJson(stream);
            } catch (Exception t) {
                throw new RuntimeException("json: " + json+"\n"+t.getMessage(), t);
            } finally {
                Debug.end();
            }
        }

        // очищаем все, что осталось в базе
        instance().dao().doCleanUpSubjectIndexData();

        // заполняем сокр. название для новых направлений
        instance().dao().updateNewSubjectsShortTitles();
    }

    /**
     * перечень действий, совершаемых перед удалением неактуальных (outOfSyncDate is not null) направлений
     * здесь нужно произвести удаление объектов (либо очистку ссылок), ссылающихся на eduProgramSubject для которых outOfSyncDate is not null
     * или какие-то другие манипуляции
     *
     * @return перечень действий
     */
    @Bean
    public ItemListExtPoint<Runnable> beforeCleanUpSubjectIndexDataCallbackList() {
        return itemList(Runnable.class).create();
    }

    /**
     * перечень действий, совершаемых после удаления неактуальных (outOfSyncDate is not null) направлений
     *
     * @return перечень действий
     */
    @Bean
    public ItemListExtPoint<Runnable> afterCleanUpSubjectIndexDataCallbackList() {
        return itemList(Runnable.class).create();
    }
}
