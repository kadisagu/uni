/* $Id:$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramQualification.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 08.09.2014
 */
@Configuration
public class EduProgramQualificationPub extends BusinessComponentManager {

    public static final String EDU_PROGRAM_SUBJECTS_LIST_DS = "eduProgramSubjectsListDS";
    public static final String EDU_PROGRAM_SUBJECT_CODE = "eduProgramSubjectCode";
    public static final String EDU_PROGRAM_SUBJECT_TITLE = "eduProgramSubjectTitle";
    public static final String EDU_PROGRAM_QUALIFICATION = "EduProgramQualification";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(searchListDS(EDU_PROGRAM_SUBJECTS_LIST_DS, eduProgramSubjectDSColumnList(), eduProgramSubjectDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint eduProgramSubjectDSColumnList()
    {
        return columnListExtPointBuilder(EDU_PROGRAM_SUBJECTS_LIST_DS)
                .addColumn(textColumn(EDU_PROGRAM_SUBJECT_CODE, EduProgramSubject.subjectCode()))
                .addColumn(publisherColumn(EDU_PROGRAM_SUBJECT_TITLE, EduProgramSubject.title()).required(true))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);
                dql.joinEntity(alias, DQLJoinType.inner, EduProgramSubjectQualification.class, "subQuali", eq(property(alias), property(EduProgramSubjectQualification.programSubject().fromAlias("subQuali"))))
                        .where(eq(property(EduProgramSubjectQualification.programQualification().fromAlias("subQuali")), commonValue(context.get(EDU_PROGRAM_QUALIFICATION))))
                        .buildQuery();
            }
        }
                .order(EduProgramSubject.title())
                .pageable(true);
    }
}
