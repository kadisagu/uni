/**
 *$Id$
 */
package ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.logic;

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.EduProgramSubjectManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.gen.EduProgramSubjectQualificationGen;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 07.02.2014
 */
public class EduProgramHigherProfDao extends UniBaseDao implements IEduProgramHigherProfDao
{

    @Override
    public void saveOrUpdateEduProgramHigherProf(EduProgramHigherProf eduProgram, EduProgramSpecialization eduProgramSpecialization)
    {
        eduProgram.setProgramSpecialization(eduProgramSpecialization == null ? EduProgramSubjectManager.instance().dao().createOrGetProgramSpecializationRoot(eduProgram.getProgramSubject()) : eduProgramSpecialization);
        eduProgram.setSubjectQualification(this.getByNaturalId(new EduProgramSubjectQualificationGen.NaturalId(eduProgram.getProgramSubject(), eduProgram.getProgramQualification())));
        eduProgram.setProgramUniqueKey(eduProgram.generateUniqueKey());

        saveOrUpdate(eduProgram);
    }

    @Override
    public void updateParams(EduProgramHigherProf program, EduLevel baseLevel, EduProgramTrait eduProgramTrait, EduProgramDuration duration)
    {
        if (null != baseLevel) {
            program.setBaseLevel(baseLevel);
        }
        if (null != duration) {
            program.setDuration(duration);
        }
        program.setEduProgramTrait(eduProgramTrait);
        program.setProgramUniqueKey(program.generateUniqueKey());

        saveOrUpdate(program);
    }

    @Override
    public Integer createNextYearCopy(@NotNull Collection<EduProgramHigherProf> eduPrograms)
    {
        Preconditions.checkNotNull(eduPrograms);

        if (eduPrograms.isEmpty())
            return 0;

        for (EduProgramHigherProf eduProgram: eduPrograms)
        {
            EduProgramHigherProf copy = EducationYearManager.instance().dao().getProgramCopy(eduProgram, eduProgram.getYear().getIntValue() + 1);
            if (copy != null)
            {
                throw new ApplicationException("Для образовательной программы «" + eduProgram.getTitleWithCodeAndConditionsShortWithForm() + "» " + eduProgram.getYear().getTitle() +
                                                       " учебного года уже добавлена копия на следующий год: «" + copy.getTitleWithCodeAndConditionsShortWithForm() + "».");
            }
        }

        final Session session = this.getSession();
        final Map<Integer, EducationYear> yearMap = new HashMap<>();
        for (EducationYear year: this.getCatalogItemList(EducationYear.class)){
            yearMap.put(year.getIntValue(), year);
        }

        for (List<EduProgramHigherProf> elements : Iterables.partition(eduPrograms, 128)) {

            for (EduProgramHigherProf eduProgram: elements)
            {
                EducationYear nextYear = yearMap.get(eduProgram.getYear().getIntValue() + 1);
                if (nextYear == null)
                    throw new ApplicationException("Невозможно скопировать ОП. В справочнике «Учебные года» не добавлен следующий за " + eduProgram.getYear().getTitle() + " учебный год.");

                EduProgramHigherProf newEduProgram = new EduProgramHigherProf();
                newEduProgram.update(eduProgram);

                newEduProgram.setYear(nextYear);
                newEduProgram.setProgramUniqueKey(newEduProgram.generateUniqueKey());

                EduProgramHigherProfDao.this.save(newEduProgram);
            }

            session.flush();
            session.clear();
        }

        return eduPrograms.size();
    }
}