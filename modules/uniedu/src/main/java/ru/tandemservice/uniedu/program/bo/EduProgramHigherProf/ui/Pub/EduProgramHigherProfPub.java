/**
 *$Id$
 */
package ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;

/**
 * @author Alexander Zhebko
 * @since 06.02.2014
 */
@Configuration
public class EduProgramHigherProfPub extends BusinessComponentManager
{
    public static final String TAB_PANEL = "tabPanel";
    public static final String PUB_TAB = "pubTab";

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(htmlTab(PUB_TAB, "Pub").permissionKey("view_eduProgramHigherProf").create())
                .create();
    }
}