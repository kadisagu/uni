/**
 *$Id$
 */
package ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.logic;

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.gen.EduProgramSubjectQualificationGen;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 13.02.2014
 */
public class EduProgramSecondaryProfDao extends UniBaseDao implements IEduProgramSecondaryProfDao
{
    @Override
    public void saveOrUpdateEduProgramSecondaryProf(final EduProgramSecondaryProf eduProgram)
    {
        eduProgram.setProgramUniqueKey(eduProgram.generateUniqueKey());
        eduProgram.setSubjectQualification(this.getByNaturalId(new EduProgramSubjectQualificationGen.NaturalId(eduProgram.getProgramSubject(), eduProgram.getProgramQualification())));
        saveOrUpdate(eduProgram);
    }

    @Override
    public Integer createNextYearCopy(@NotNull Collection<EduProgramSecondaryProf> eduPrograms)
    {
        Preconditions.checkNotNull(eduPrograms);

        if (eduPrograms.isEmpty())
            return 0;

        for (EduProgramSecondaryProf eduProgram: eduPrograms)
        {
            EduProgramSecondaryProf copy = EducationYearManager.instance().dao().getProgramCopy(eduProgram, eduProgram.getYear().getIntValue() + 1);
            if (copy != null)
            {
                throw new ApplicationException("Для образовательной программы «" + eduProgram.getTitleWithCodeAndConditionsShortWithForm() + "» " + eduProgram.getYear().getTitle() +
                                                       " учебного года уже добавлена копия на следующий год: «" + copy.getTitleWithCodeAndConditionsShortWithForm() + "».");
            }
        }

        final Session session = this.getSession();
        final Map<Integer, EducationYear> yearMap = new HashMap<>();
        for (EducationYear year: this.getCatalogItemList(EducationYear.class))
            yearMap.put(year.getIntValue(), year);

        for (List<EduProgramSecondaryProf> elements : Iterables.partition(eduPrograms, 128)) {

            for (EduProgramSecondaryProf eduProgram : elements)
            {
                EducationYear nextYear = yearMap.get(eduProgram.getYear().getIntValue() + 1);
                if (nextYear == null)
                    throw new ApplicationException("Невозможно скопировать ОП. В справочнике «Учебные года» не добавлен следующий за " + eduProgram.getYear().getTitle() + " учебный год.");

                EduProgramSecondaryProf newEduProgram = new EduProgramSecondaryProf();
                newEduProgram.update(eduProgram);

                newEduProgram.setYear(nextYear);
                newEduProgram.setProgramUniqueKey(newEduProgram.generateUniqueKey());

                EduProgramSecondaryProfDao.this.save(newEduProgram);
            }

            session.flush();
            session.clear();
        }

        return eduPrograms.size();
    }
}