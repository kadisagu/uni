package ru.tandemservice.uniedu.catalog.entity.subjects.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Поколение перечня направлений подготовки профессионального образования"
 * Имя сущности : eduProgramSubjectIndexGeneration
 * Файл data.xml : uniedu.subjects.data.xml
 */
public interface EduProgramSubjectIndexGenerationCodes
{
    /** Константа кода (code) элемента : 2013 (title) */
    String FGOS_2013 = "2013";
    /** Константа кода (code) элемента : 2009 (title) */
    String FGOS_2009 = "2009";
    /** Константа кода (code) элемента : ОКСО (title) */
    String OKSO = "2005";

    Set<String> CODES = ImmutableSet.of(FGOS_2013, FGOS_2009, OKSO);
}
