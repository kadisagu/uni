package ru.tandemservice.uniedu.catalog.entity.subjects;

import ru.tandemservice.uniedu.catalog.entity.subjects.gen.*;

/**
 * Укрупненная группа специальностей (перечень направлений подготовки ОКСО и 2009)
 */
public class EduProgramSubjectOksoGroup extends EduProgramSubjectOksoGroupGen
{
    @Override
    public String getDisplayableTitle()
    {
        return getCode() + " " + getTitle();
    }
}