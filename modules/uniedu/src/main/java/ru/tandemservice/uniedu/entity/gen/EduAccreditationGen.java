package ru.tandemservice.uniedu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.entity.EduAccreditation;
import ru.tandemservice.uniedu.entity.IPersistentAccreditationOwner;
import ru.tandemservice.uniedu.entity.StateAccreditationEnum;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Аккредитация в лицензированном подразделении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduAccreditationGen extends EntityBase
 implements INaturalIdentifiable<EduAccreditationGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.entity.EduAccreditation";
    public static final String ENTITY_NAME = "eduAccreditation";
    public static final int VERSION_HASH = -1709969398;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER = "owner";
    public static final String L_INSTITUTION_ORG_UNIT = "institutionOrgUnit";
    public static final String L_PROGRAM_SUBJECT_INDEX = "programSubjectIndex";
    public static final String P_STATE_ACCREDITATION = "stateAccreditation";

    private IPersistentAccreditationOwner _owner;     // Объект аккредитации
    private EduInstitutionOrgUnit _institutionOrgUnit;     // Аккредитованное подразделение
    private EduProgramSubjectIndex _programSubjectIndex;     // Перечень направлений подготовки
    private StateAccreditationEnum _stateAccreditation;     // Состояние аккредитации

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Объект аккредитации. Свойство не может быть null.
     */
    @NotNull
    public IPersistentAccreditationOwner getOwner()
    {
        return _owner;
    }

    /**
     * @param owner Объект аккредитации. Свойство не может быть null.
     */
    public void setOwner(IPersistentAccreditationOwner owner)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && owner!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IPersistentAccreditationOwner.class);
            IEntityMeta actual =  owner instanceof IEntity ? EntityRuntime.getMeta((IEntity) owner) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * @return Аккредитованное подразделение. Свойство не может быть null.
     */
    @NotNull
    public EduInstitutionOrgUnit getInstitutionOrgUnit()
    {
        return _institutionOrgUnit;
    }

    /**
     * @param institutionOrgUnit Аккредитованное подразделение. Свойство не может быть null.
     */
    public void setInstitutionOrgUnit(EduInstitutionOrgUnit institutionOrgUnit)
    {
        dirty(_institutionOrgUnit, institutionOrgUnit);
        _institutionOrgUnit = institutionOrgUnit;
    }

    /**
     * @return Перечень направлений подготовки. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubjectIndex getProgramSubjectIndex()
    {
        return _programSubjectIndex;
    }

    /**
     * @param programSubjectIndex Перечень направлений подготовки. Свойство не может быть null.
     */
    public void setProgramSubjectIndex(EduProgramSubjectIndex programSubjectIndex)
    {
        dirty(_programSubjectIndex, programSubjectIndex);
        _programSubjectIndex = programSubjectIndex;
    }

    /**
     * @return Состояние аккредитации. Свойство не может быть null.
     */
    @NotNull
    public StateAccreditationEnum getStateAccreditation()
    {
        return _stateAccreditation;
    }

    /**
     * @param stateAccreditation Состояние аккредитации. Свойство не может быть null.
     */
    public void setStateAccreditation(StateAccreditationEnum stateAccreditation)
    {
        dirty(_stateAccreditation, stateAccreditation);
        _stateAccreditation = stateAccreditation;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EduAccreditationGen)
        {
            if (withNaturalIdProperties)
            {
                setOwner(((EduAccreditation)another).getOwner());
                setInstitutionOrgUnit(((EduAccreditation)another).getInstitutionOrgUnit());
                setProgramSubjectIndex(((EduAccreditation)another).getProgramSubjectIndex());
            }
            setStateAccreditation(((EduAccreditation)another).getStateAccreditation());
        }
    }

    public INaturalId<EduAccreditationGen> getNaturalId()
    {
        return new NaturalId(getOwner(), getInstitutionOrgUnit(), getProgramSubjectIndex());
    }

    public static class NaturalId extends NaturalIdBase<EduAccreditationGen>
    {
        private static final String PROXY_NAME = "EduAccreditationNaturalProxy";

        private Long _owner;
        private Long _institutionOrgUnit;
        private Long _programSubjectIndex;

        public NaturalId()
        {}

        public NaturalId(IPersistentAccreditationOwner owner, EduInstitutionOrgUnit institutionOrgUnit, EduProgramSubjectIndex programSubjectIndex)
        {
            _owner = ((IEntity) owner).getId();
            _institutionOrgUnit = ((IEntity) institutionOrgUnit).getId();
            _programSubjectIndex = ((IEntity) programSubjectIndex).getId();
        }

        public Long getOwner()
        {
            return _owner;
        }

        public void setOwner(Long owner)
        {
            _owner = owner;
        }

        public Long getInstitutionOrgUnit()
        {
            return _institutionOrgUnit;
        }

        public void setInstitutionOrgUnit(Long institutionOrgUnit)
        {
            _institutionOrgUnit = institutionOrgUnit;
        }

        public Long getProgramSubjectIndex()
        {
            return _programSubjectIndex;
        }

        public void setProgramSubjectIndex(Long programSubjectIndex)
        {
            _programSubjectIndex = programSubjectIndex;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EduAccreditationGen.NaturalId) ) return false;

            EduAccreditationGen.NaturalId that = (NaturalId) o;

            if( !equals(getOwner(), that.getOwner()) ) return false;
            if( !equals(getInstitutionOrgUnit(), that.getInstitutionOrgUnit()) ) return false;
            if( !equals(getProgramSubjectIndex(), that.getProgramSubjectIndex()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOwner());
            result = hashCode(result, getInstitutionOrgUnit());
            result = hashCode(result, getProgramSubjectIndex());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOwner());
            sb.append("/");
            sb.append(getInstitutionOrgUnit());
            sb.append("/");
            sb.append(getProgramSubjectIndex());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduAccreditationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduAccreditation.class;
        }

        public T newInstance()
        {
            return (T) new EduAccreditation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "owner":
                    return obj.getOwner();
                case "institutionOrgUnit":
                    return obj.getInstitutionOrgUnit();
                case "programSubjectIndex":
                    return obj.getProgramSubjectIndex();
                case "stateAccreditation":
                    return obj.getStateAccreditation();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "owner":
                    obj.setOwner((IPersistentAccreditationOwner) value);
                    return;
                case "institutionOrgUnit":
                    obj.setInstitutionOrgUnit((EduInstitutionOrgUnit) value);
                    return;
                case "programSubjectIndex":
                    obj.setProgramSubjectIndex((EduProgramSubjectIndex) value);
                    return;
                case "stateAccreditation":
                    obj.setStateAccreditation((StateAccreditationEnum) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "owner":
                        return true;
                case "institutionOrgUnit":
                        return true;
                case "programSubjectIndex":
                        return true;
                case "stateAccreditation":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "owner":
                    return true;
                case "institutionOrgUnit":
                    return true;
                case "programSubjectIndex":
                    return true;
                case "stateAccreditation":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "owner":
                    return IPersistentAccreditationOwner.class;
                case "institutionOrgUnit":
                    return EduInstitutionOrgUnit.class;
                case "programSubjectIndex":
                    return EduProgramSubjectIndex.class;
                case "stateAccreditation":
                    return StateAccreditationEnum.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduAccreditation> _dslPath = new Path<EduAccreditation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduAccreditation");
    }
            

    /**
     * @return Объект аккредитации. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.entity.EduAccreditation#getOwner()
     */
    public static IPersistentAccreditationOwnerGen.Path<IPersistentAccreditationOwner> owner()
    {
        return _dslPath.owner();
    }

    /**
     * @return Аккредитованное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.entity.EduAccreditation#getInstitutionOrgUnit()
     */
    public static EduInstitutionOrgUnit.Path<EduInstitutionOrgUnit> institutionOrgUnit()
    {
        return _dslPath.institutionOrgUnit();
    }

    /**
     * @return Перечень направлений подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.entity.EduAccreditation#getProgramSubjectIndex()
     */
    public static EduProgramSubjectIndex.Path<EduProgramSubjectIndex> programSubjectIndex()
    {
        return _dslPath.programSubjectIndex();
    }

    /**
     * @return Состояние аккредитации. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.entity.EduAccreditation#getStateAccreditation()
     */
    public static PropertyPath<StateAccreditationEnum> stateAccreditation()
    {
        return _dslPath.stateAccreditation();
    }

    public static class Path<E extends EduAccreditation> extends EntityPath<E>
    {
        private IPersistentAccreditationOwnerGen.Path<IPersistentAccreditationOwner> _owner;
        private EduInstitutionOrgUnit.Path<EduInstitutionOrgUnit> _institutionOrgUnit;
        private EduProgramSubjectIndex.Path<EduProgramSubjectIndex> _programSubjectIndex;
        private PropertyPath<StateAccreditationEnum> _stateAccreditation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Объект аккредитации. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.entity.EduAccreditation#getOwner()
     */
        public IPersistentAccreditationOwnerGen.Path<IPersistentAccreditationOwner> owner()
        {
            if(_owner == null )
                _owner = new IPersistentAccreditationOwnerGen.Path<IPersistentAccreditationOwner>(L_OWNER, this);
            return _owner;
        }

    /**
     * @return Аккредитованное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.entity.EduAccreditation#getInstitutionOrgUnit()
     */
        public EduInstitutionOrgUnit.Path<EduInstitutionOrgUnit> institutionOrgUnit()
        {
            if(_institutionOrgUnit == null )
                _institutionOrgUnit = new EduInstitutionOrgUnit.Path<EduInstitutionOrgUnit>(L_INSTITUTION_ORG_UNIT, this);
            return _institutionOrgUnit;
        }

    /**
     * @return Перечень направлений подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.entity.EduAccreditation#getProgramSubjectIndex()
     */
        public EduProgramSubjectIndex.Path<EduProgramSubjectIndex> programSubjectIndex()
        {
            if(_programSubjectIndex == null )
                _programSubjectIndex = new EduProgramSubjectIndex.Path<EduProgramSubjectIndex>(L_PROGRAM_SUBJECT_INDEX, this);
            return _programSubjectIndex;
        }

    /**
     * @return Состояние аккредитации. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.entity.EduAccreditation#getStateAccreditation()
     */
        public PropertyPath<StateAccreditationEnum> stateAccreditation()
        {
            if(_stateAccreditation == null )
                _stateAccreditation = new PropertyPath<StateAccreditationEnum>(EduAccreditationGen.P_STATE_ACCREDITATION, this);
            return _stateAccreditation;
        }

        public Class getEntityClass()
        {
            return EduAccreditation.class;
        }

        public String getEntityName()
        {
            return "eduAccreditation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
