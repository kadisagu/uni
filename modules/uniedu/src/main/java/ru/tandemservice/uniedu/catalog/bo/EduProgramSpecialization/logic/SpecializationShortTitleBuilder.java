package ru.tandemservice.uniedu.catalog.bo.EduProgramSpecialization.logic;

import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationChild;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationRoot;
import ru.tandemservice.uniedu.util.UniEduUtil;

import javax.validation.constraints.NotNull;

/**
 * @author avedernikov
 * @since 13.01.2017
 */
public class SpecializationShortTitleBuilder
{
	private static final String RootSpecTitlePostfix = " (Общая)";

	public static String buildRootShortTitle(@NotNull EduProgramSpecializationRoot specialization)
	{
		return buildShortTitle(specialization.getTitle(), true);
	}

	public static String buildChildShortTitle(@NotNull EduProgramSpecializationChild specialization)
	{
		return buildShortTitle(specialization.getTitle(), false);
	}

	public static String buildShortTitle(String title, boolean isRootSpec)
	{
		final String abbreviation = containOnlyDigits(title) ? title : UniEduUtil.buildAbbreviation(title);
		return isRootSpec ? (abbreviation + RootSpecTitlePostfix) : abbreviation;
	}

	private static boolean containOnlyDigits(String title)
	{
		return title.chars().allMatch(Character::isDigit);
	}
}
