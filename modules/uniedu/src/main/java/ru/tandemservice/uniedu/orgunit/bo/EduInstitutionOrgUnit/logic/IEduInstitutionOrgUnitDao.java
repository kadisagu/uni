package ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;

/**
 * @author vdanilov
 */
public interface IEduInstitutionOrgUnitDao extends INeedPersistenceSupport {

    /**
     * Создает Акк.ОУ. на базе подразделения
     * @param orgUnit
     * @return eduInstitutionOrgUnit e | e.orgUnit = orgUnit
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    EduInstitutionOrgUnit doGetEduInstitutionOrgUnit(OrgUnit orgUnit);

	/**
	 * Обновляет Акк.ОУ. на базе подразделения
	 * @param orgUnit обновляемые данные
	 * @return eduInstitutionOrgUnit
	 */
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	void updateEduInstitutionOrgUnit(EduInstitutionOrgUnit orgUnit);


    /**
     * Удаляет аккредитованное подразделение.
     * @param eduInstitutionOrgUnitId id аккредитованного подразделения
     */
    public void deleteEduInstitutionOrgUnit(Long eduInstitutionOrgUnitId);
}
