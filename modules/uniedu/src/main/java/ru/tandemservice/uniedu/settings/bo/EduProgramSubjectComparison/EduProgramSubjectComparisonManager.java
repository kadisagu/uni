/* $Id:$ */
package ru.tandemservice.uniedu.settings.bo.EduProgramSubjectComparison;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniedu.settings.bo.EduProgramSubjectComparison.logic.EduProgramSubjectComparisonDao;
import ru.tandemservice.uniedu.settings.bo.EduProgramSubjectComparison.logic.IEduProgramSubjectComparisonDao;

/**
 * @author rsizonenko
 * @since 12.03.2015
 */
@Configuration
public class EduProgramSubjectComparisonManager extends BusinessObjectManager {

    public static EduProgramSubjectComparisonManager instance()
    {
        return instance(EduProgramSubjectComparisonManager.class);
    }

    @Bean
    public IEduProgramSubjectComparisonDao dao()
    {
        return new EduProgramSubjectComparisonDao();
    }

}
