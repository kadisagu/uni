/* $Id:$ */
package ru.tandemservice.uniedu.runtime;

import org.tandemframework.core.runtime.IRuntimeExtension;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniedu.program.entity.EduProgramAdditionalProf;

/**
 * @author oleyba
 * @since 11/26/14
 */
public class EduProgramHackExtension implements IRuntimeExtension
{
    @Override
    public void init(Object object)
    {
        EducationYear currentYear = DataAccessServices.dao().get(EducationYear.class, EducationYear.current().s(), Boolean.TRUE);
        if (currentYear != null && !IUniBaseDao.instance.get().existsEntity(EduProgramAdditionalProf.class, "ep", EduProgramAdditionalProf.programUniqueKey().s(), "dpp.hack")) {
            DataAccessServices.dao().doInTransaction(session -> {
                EduProgramAdditionalProf p = new EduProgramAdditionalProf();
                p.setProgramUniqueKey("dpp.hack");
                p.setTitle("Образовательная программа ДПО");
                p.setShortTitle("ОП ДПО");
                p.setKind(DataAccessServices.dao().get(EduProgramKind.class, EduProgramKind.code().s(), EduProgramKindCodes.DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA));

                TopOrgUnit topOu = TopOrgUnit.getInstance();

                EduInstitutionOrgUnit institutionOrgUnit = DataAccessServices.dao().get(EduInstitutionOrgUnit.class, EduInstitutionOrgUnit.orgUnit(), topOu);
                if (null == institutionOrgUnit) {
                    institutionOrgUnit = new EduInstitutionOrgUnit();
                    institutionOrgUnit.setOrgUnit(topOu);
                    session.save(institutionOrgUnit);
                }
                p.setInstitutionOrgUnit(institutionOrgUnit);

                EduOwnerOrgUnit ownerOrgUnit = DataAccessServices.dao().get(EduOwnerOrgUnit.class, EduOwnerOrgUnit.orgUnit(), topOu);
                if (null == ownerOrgUnit) {
                    ownerOrgUnit = new EduOwnerOrgUnit();
                    ownerOrgUnit.setOrgUnit(topOu);
                    session.save(ownerOrgUnit);
                }
                p.setOwnerOrgUnit(ownerOrgUnit);

                p.setForm(DataAccessServices.dao().get(EduProgramForm.class, EduProgramForm.code().s(), EduProgramFormCodes.OCHNAYA));
                p.setDuration(DataAccessServices.dao().getList(EduProgramDuration.class, new String[]{EduProgramDuration.numberOfYears().s(), EduProgramDuration.numberOfMonths().s()}).get(0));

                p.setYear(currentYear);
                session.save(p);

                return null;
            });
        }
    }

    @Override
    public void destroy()
    {

    }
}

