/* $Id:$ */
package ru.tandemservice.uniedu.catalog.ext.Catalog;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;

/**
 * @author oleyba
 * @since 11/1/13
 */
@Configuration
public class CatalogExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CatalogManager catalogManager;

    @Bean
    public ItemListExtension<IDynamicCatalogDesc> itemListExtension()
    {
        return itemListExtension(catalogManager.dynamicCatalogsExtPoint())
            .add(StringUtils.uncapitalize(EduProgramKind.class.getSimpleName()), EduProgramKind.getUiDesc())
            .add(StringUtils.uncapitalize(EduProgramDuration.class.getSimpleName()), EduProgramDuration.getUiDesc())
            .create();
    }
}

