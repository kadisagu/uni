/* $Id$ */
package ru.tandemservice.uniedu.base.bo.EduSettings.ui.SpecialDegree;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2009;
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes;

import java.util.Arrays;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Nikolay Fedorovskih
 * @since 30.04.2014
 */
@Configuration
public class EduSettingsSpecialDegree extends BusinessComponentManager
{
    public static final String SPECIAL_DEGREE_DS = "specialDegreeDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SPECIAL_DEGREE_DS, specialDegreeDS(), specialDegreeDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint specialDegreeDS()
    {
        return columnListExtPointBuilder(SPECIAL_DEGREE_DS)
                .addColumn(textColumn("title", EduProgramSubject2009.P_TITLE))
                .addColumn(textColumn("code", EduProgramSubject2009.P_CODE))
                .addColumn(toggleColumn("assignSpecialDegree", EduProgramSubject2009.P_ASSIGN_SPECIAL_DEGREE)
                                   .toggleOnListener("onToggleSpecialDegreeOn").toggleOnLabel("specialDegree.on")
                                   .toggleOffListener("onToggleSpecialDegreeOff").toggleOffLabel("specialDegree.off")
                )
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> specialDegreeDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EduProgramSubject2009.class, "p")
                        .column("p")
                        .where(in(property("p", EduProgramSubject2009.subjectIndex().code()), Arrays.asList(EduProgramSubjectIndexCodes.TITLE_2009_62, EduProgramSubjectIndexCodes.TITLE_2009_68)))
                        .order(property("p", EduProgramSubject2009.P_TITLE));

                return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).build();
            }
        };
    }
}