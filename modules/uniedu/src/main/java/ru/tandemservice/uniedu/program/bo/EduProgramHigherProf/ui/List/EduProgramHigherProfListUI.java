/**
 *$Id$
 */
package ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.EduProgramHigherProfManager;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.ui.AddEdit.EduProgramHigherProfAddEdit;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.ui.List.logic.EduProgramHigherProfListDSHandler;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;

import java.util.Collection;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 06.02.2014
 */
public class EduProgramHigherProfListUI extends UIPresenter
{
	public static final String SETTINGS_EDU_PROGRAM_TITLE = "eduProgramTitle";
	public static final String SETTINGS_EDU_PROGRAM_KIND = "eduProgramKind";
	public static final String SETTINGS_EDU_PROGRAM_FORM = "eduProgramForm";
	public static final String SETTINGS_EDU_PROGRAM_DURATION = "eduProgramDuration";
	public static final String SETTINGS_EDU_YEAR = "eduYear";
	public static final String SETTINGS_EDU_PROGRAM_SUBJECT_INDEX = "eduProgramSubjectIndex";
	public static final String SETTINGS_EDU_PROGRAM_SUBJECT = "eduProgramSubject";
	public static final String SETTINGS_EDU_PROGRAM_SPECIALIZATION = "eduProgramSpecialization";
	public static final String SETTINGS_ORG_UNIT = "orgUnit";
	public static final String SETTINGS_EDU_INSTITUTION_ORG_UNIT = "eduInstitutionOrgUnit";
	public static final String SETTINGS_BIND_NEXT_YEAR_COPY = "nextYearCopy";


    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        if (UIDefines.DIALOG_REGION_NAME.equals(childRegionName)) {
            Collection<IEntity> selected = ((BaseSearchListDataSource) this.getConfig().getDataSource(EduProgramHigherProfList.EDU_PROGRAM_HIGHER_PROF_LIST_DS)).getOptionColumnSelectedObjects("check");
            selected.clear();
        }
    }

    public String getEduProgramDeleteAlert()
    {
        EduProgramHigherProf current = getConfig().getDataSource(EduProgramHigherProfList.EDU_PROGRAM_HIGHER_PROF_LIST_DS).getCurrent();
        return "Удалить образовательную программу «" + current.getTitle() +"»?";
    }

    public void onClickAddEduProgram()
    {
        _uiActivation.asRegionDialog(EduProgramHigherProfAddEdit.class).activate();
    }

    public void onClickEditEduProgram()
    {
        _uiActivation.asRegionDialog(EduProgramHigherProfAddEdit.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, getListenerParameter()).activate();
    }

    public void onClickDeleteEduProgram()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    @SuppressWarnings("unchecked")
    public void onClickCreateNextYearCopy()
    {
        Collection<IEntity> check = ((BaseSearchListDataSource) this.getConfig().getDataSource(EduProgramHigherProfList.EDU_PROGRAM_HIGHER_PROF_LIST_DS)).getOptionColumnSelectedObjects("check");
        final Integer nextYearCopy = EduProgramHigherProfManager.instance().dao().createNextYearCopy((Collection) check);
        this.getSupport().doRefresh();
        if (nextYearCopy > 0)
            this.getSupport().info("Скопировано образовательных программ: " + nextYearCopy);
    }

    public void onClickSearch()
    {
        saveSettings();
    }

    public void onClickClear()
    {
        clearSettings();
        onClickSearch();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        switch (dataSource.getName())
        {
            case EduProgramHigherProfList.EDU_PROGRAM_SUBJECT_INDEX_DS : dataSource.put(EduProgramHigherProfList.EDU_PROGRAM_KIND, getSettings().get(EduProgramHigherProfList.EDU_PROGRAM_KIND));

            case EduProgramHigherProfList.EDU_PROGRAM_SUBJECT_DS :
				dataSource.put(EduProgramHigherProfList.EDU_PROGRAM_KIND, getSettings().get(EduProgramHigherProfList.EDU_PROGRAM_KIND));
				dataSource.put(EduProgramHigherProfList.EDU_PROGRAM_SUBJECT_INDEX, getSettings().get(EduProgramHigherProfList.EDU_PROGRAM_SUBJECT_INDEX));
				break;
            case EduProgramHigherProfList.EDU_PROGRAM_SPECIALIZATION_DS :
				dataSource.put(EduProgramHigherProfList.EDU_PROGRAM_KIND, getSettings().get(EduProgramHigherProfList.EDU_PROGRAM_KIND));
				dataSource.put(EduProgramHigherProfList.EDU_PROGRAM_SUBJECT_INDEX, getSettings().get(EduProgramHigherProfList.EDU_PROGRAM_SUBJECT_INDEX));
				dataSource.put(EduProgramHigherProfList.EDU_PROGRAM_SUBJECT, getSettings().get(EduProgramHigherProfList.EDU_PROGRAM_SUBJECT));
	            break;
            case EduProgramHigherProfList.EDU_PROGRAM_HIGHER_PROF_LIST_DS :
				dataSource.put(EduProgramHigherProfListDSHandler.EDU_PROGRAM_TITLE, getSettings().get(SETTINGS_EDU_PROGRAM_TITLE));
				dataSource.put(EduProgramHigherProfListDSHandler.EDU_PROGRAM_KIND, getSettings().get(SETTINGS_EDU_PROGRAM_KIND));
				dataSource.put(EduProgramHigherProfListDSHandler.EDU_PROGRAM_FORM, getSettings().get(SETTINGS_EDU_PROGRAM_FORM));
				dataSource.put(EduProgramHigherProfListDSHandler.EDU_PROGRAM_DURATION, getSettings().get(SETTINGS_EDU_PROGRAM_DURATION));
				dataSource.put(EduProgramHigherProfListDSHandler.EDU_YEAR, getSettings().get(SETTINGS_EDU_YEAR));
				dataSource.put(EduProgramHigherProfListDSHandler.EDU_PROGRAM_SUBJECT_INDEX, getSettings().get(SETTINGS_EDU_PROGRAM_SUBJECT_INDEX));
				dataSource.put(EduProgramHigherProfListDSHandler.EDU_PROGRAM_SUBJECT, getSettings().get(SETTINGS_EDU_PROGRAM_SUBJECT));
				dataSource.put(EduProgramHigherProfListDSHandler.EDU_PROGRAM_SPECIALIZATION, getSettings().get(SETTINGS_EDU_PROGRAM_SPECIALIZATION));
				dataSource.put(EduProgramHigherProfListDSHandler.EDU_OWNER_ORG_UNIT, getEduOwnerOrgUnit());
				dataSource.put(EduProgramHigherProfListDSHandler.EDU_INSTITUTION_ORG_UNIT, getSettings().get(SETTINGS_EDU_INSTITUTION_ORG_UNIT));
				dataSource.put(EduProgramHigherProfListDSHandler.BIND_NEXT_YEAR_COPY, getSettings().get(SETTINGS_BIND_NEXT_YEAR_COPY));
	            break;
        }
    }

	private EduOwnerOrgUnit getEduOwnerOrgUnit()
	{
		OrgUnit orgUnit = getSettings().get(SETTINGS_ORG_UNIT);
		return (orgUnit == null) ? null : DataAccessServices.dao().getByNaturalId(new EduOwnerOrgUnit.NaturalId(orgUnit));
	}
}