package ru.tandemservice.uniedu.catalog.entity.subjects.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2009;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOksoGroup;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Направление подготовки профессионального образования (2009)
 *
 * Направление классификаторов 2009 года: плоская структура (она не совпадает со структурой ОКСО в ряде мест). Перечень УГС глобальный и общий для ОКСО и 2009.
 * Направление подготовки в системе ссылается только на УГС (иерархической структуры внутри направлений и специальностей просто нет).
 * http://wiki.tandemservice.ru/pages/viewpage.action?pageId=17465605
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramSubject2009Gen extends EduProgramSubject
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2009";
    public static final String ENTITY_NAME = "eduProgramSubject2009";
    public static final int VERSION_HASH = 1385697526;
    private static IEntityMeta ENTITY_META;

    public static final String L_GROUP = "group";
    public static final String P_ASSIGN_SPECIAL_DEGREE = "assignSpecialDegree";

    private EduProgramSubjectOksoGroup _group;     // Укрупненная группа
    private boolean _assignSpecialDegree;     // Выпускнику присваивается специальное звание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Укрупненная группа. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubjectOksoGroup getGroup()
    {
        return _group;
    }

    /**
     * @param group Укрупненная группа. Свойство не может быть null.
     */
    public void setGroup(EduProgramSubjectOksoGroup group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Выпускнику присваивается специальное звание. Свойство не может быть null.
     */
    @NotNull
    public boolean isAssignSpecialDegree()
    {
        return _assignSpecialDegree;
    }

    /**
     * @param assignSpecialDegree Выпускнику присваивается специальное звание. Свойство не может быть null.
     */
    public void setAssignSpecialDegree(boolean assignSpecialDegree)
    {
        dirty(_assignSpecialDegree, assignSpecialDegree);
        _assignSpecialDegree = assignSpecialDegree;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduProgramSubject2009Gen)
        {
            setGroup(((EduProgramSubject2009)another).getGroup());
            setAssignSpecialDegree(((EduProgramSubject2009)another).isAssignSpecialDegree());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramSubject2009Gen> extends EduProgramSubject.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramSubject2009.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramSubject2009();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "group":
                    return obj.getGroup();
                case "assignSpecialDegree":
                    return obj.isAssignSpecialDegree();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "group":
                    obj.setGroup((EduProgramSubjectOksoGroup) value);
                    return;
                case "assignSpecialDegree":
                    obj.setAssignSpecialDegree((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "group":
                        return true;
                case "assignSpecialDegree":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "group":
                    return true;
                case "assignSpecialDegree":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "group":
                    return EduProgramSubjectOksoGroup.class;
                case "assignSpecialDegree":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgramSubject2009> _dslPath = new Path<EduProgramSubject2009>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramSubject2009");
    }
            

    /**
     * @return Укрупненная группа. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2009#getGroup()
     */
    public static EduProgramSubjectOksoGroup.Path<EduProgramSubjectOksoGroup> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Выпускнику присваивается специальное звание. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2009#isAssignSpecialDegree()
     */
    public static PropertyPath<Boolean> assignSpecialDegree()
    {
        return _dslPath.assignSpecialDegree();
    }

    public static class Path<E extends EduProgramSubject2009> extends EduProgramSubject.Path<E>
    {
        private EduProgramSubjectOksoGroup.Path<EduProgramSubjectOksoGroup> _group;
        private PropertyPath<Boolean> _assignSpecialDegree;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Укрупненная группа. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2009#getGroup()
     */
        public EduProgramSubjectOksoGroup.Path<EduProgramSubjectOksoGroup> group()
        {
            if(_group == null )
                _group = new EduProgramSubjectOksoGroup.Path<EduProgramSubjectOksoGroup>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Выпускнику присваивается специальное звание. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2009#isAssignSpecialDegree()
     */
        public PropertyPath<Boolean> assignSpecialDegree()
        {
            if(_assignSpecialDegree == null )
                _assignSpecialDegree = new PropertyPath<Boolean>(EduProgramSubject2009Gen.P_ASSIGN_SPECIAL_DEGREE, this);
            return _assignSpecialDegree;
        }

        public Class getEntityClass()
        {
            return EduProgramSubject2009.class;
        }

        public String getEntityName()
        {
            return "eduProgramSubject2009";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
