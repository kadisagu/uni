/**
 *$Id: EppSystemActionPubAddon.java 26910 2013-04-10 05:45:04Z vdanilov $
 */
package ru.tandemservice.uniedu.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;

import ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.EduProgramSubjectIndexManager;

public class EduSystemActionPubAddon extends UIAddon
{
    public EduSystemActionPubAddon(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    /**
     * @see EduProgramSubjectIndexManager#syncStaticData()
     */
    public void onClickSyncStaticData() {
        EduProgramSubjectIndexManager.instance().syncStaticData();
    }
}
