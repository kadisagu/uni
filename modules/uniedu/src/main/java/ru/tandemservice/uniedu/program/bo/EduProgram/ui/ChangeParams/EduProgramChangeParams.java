/* $Id:$ */
package ru.tandemservice.uniedu.program.bo.EduProgram.ui.ChangeParams;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.EduProgramHigherProfManager;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.ui.AddEdit.EduProgramHigherProfAddEdit;
import ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.EduProgramSecondaryProfManager;

/**
 * @author oleyba
 * @since 7/2/15
 */
@Configuration
public class EduProgramChangeParams extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS("higherBaseLevelDS", EduProgramHigherProfManager.instance().baseLevelDS()))
            .addDataSource(selectDS("secondaryBaseLevelDS", EduProgramSecondaryProfManager.instance().eduProgramBaseLevelDSHandler()))
            .addDataSource(selectDS("eduProgramDurationDS", eduProgramDurationDSHandler()))
            .addDataSource(selectDS("eduProgramTraitDS", eduProgramTraitDSHandler()))
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramDurationDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramDuration.class)
            .order(EduProgramDuration.title())
            .filter(EduProgramDuration.title())
            .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramTraitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramTrait.class)
            .filter(EduProgramTrait.title())
            .order(EduProgramTrait.title());
    }
}