/* $Id:$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramQualification.ui.Edit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;

/**
 * @author rsizonenko
 * @since 08.09.2014
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eduProgramQualification.id"))
public class EduProgramQualificationEditUI extends UIPresenter {

    private EduProgramQualification eduProgramQualification = new EduProgramQualification();

    // getters-setters

    public EduProgramQualification getEduProgramQualification() {
        return eduProgramQualification;
    }

    public void setEduProgramQualification(EduProgramQualification eduProgramQualification) {
        this.eduProgramQualification = eduProgramQualification;
    }

    // override

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();
        setEduProgramQualification(IUniBaseDao.instance.get().get(EduProgramQualification.class, getEduProgramQualification().getId()));
    }

    // listeners
    public void onClickApply() {
        IUniBaseDao.instance.get().saveOrUpdate(getEduProgramQualification());
        deactivate();
    }
}