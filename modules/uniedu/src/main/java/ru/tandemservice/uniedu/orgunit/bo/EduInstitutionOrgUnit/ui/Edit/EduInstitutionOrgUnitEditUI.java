/*$Id$*/
package ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.ui.Edit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.EduInstitutionOrgUnitManager;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;

/**
 * @author DMITRY KNYAZEV
 * @since 16.05.2014
 */
@Input({
		       @Bind(key = UIPresenter.PUBLISHER_ID, binding = "orgUnitId", required = true)
       })
public class EduInstitutionOrgUnitEditUI extends UIPresenter
{
	private EduInstitutionOrgUnit _orgUnit;
	private Long _orgUnitId;

	public Long getOrgUnitId()
	{
		return _orgUnitId;
	}

	public void setOrgUnitId(Long orgUnitId)
	{
		_orgUnitId = orgUnitId;
	}

	public EduInstitutionOrgUnit getOrgUnit()
	{
		return _orgUnit;
	}

	public void setOrgUnit(EduInstitutionOrgUnit orgUnit)
	{
		_orgUnit = orgUnit;
	}

	@Override
	public void onComponentRefresh()
	{
		setOrgUnit(DataAccessServices.dao().getNotNull(EduInstitutionOrgUnit.class, getOrgUnitId()));
	}

	public void onClickApply()
	{
		EduInstitutionOrgUnitManager.instance().dao().updateEduInstitutionOrgUnit(getOrgUnit());
		deactivate();
	}
}
