/**
 *$Id$
 */
package ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.ui.List.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 06.02.2014
 */
public class EduProgramHigherProfListDSHandler extends DefaultSearchDataSourceHandler
{
	public static final String EDU_PROGRAM_TITLE = "eduProgramTitle";
	public static final String EDU_PROGRAM_KIND = "eduProgramKind";
	public static final String EDU_PROGRAM_FORM = "eduProgramForm";
	public static final String EDU_PROGRAM_DURATION = "eduProgramDuration";
	public static final String EDU_YEAR = "eduYear";
	public static final String EDU_PROGRAM_SUBJECT_INDEX = "eduProgramSubjectIndex";
	public static final String EDU_PROGRAM_SUBJECT = "eduProgramSubject";
	public static final String EDU_PROGRAM_SPECIALIZATION = "eduProgramSpecialization";
	public static final String EDU_OWNER_ORG_UNIT = "eduOwnerOrgUnit";
	public static final String EDU_INSTITUTION_ORG_UNIT = "eduInstitutionOrgUnit";
	public static final String BIND_NEXT_YEAR_COPY = "nextYearCopy";

    public EduProgramHigherProfListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EduProgramHigherProf.class, "e")
                .fetchPath(DQLJoinType.inner, EduProgramHigherProf.programSubject().fromAlias("e"), "ps")
                .fetchPath(DQLJoinType.inner, EduProgramHigherProf.programSubject().subjectIndex().fromAlias("e"), "psi")
                .fetchPath(DQLJoinType.inner, EduProgramHigherProf.ownerOrgUnit().orgUnit().fromAlias("e"), "ouo")
                .fetchPath(DQLJoinType.inner, EduProgramHigherProf.institutionOrgUnit().orgUnit().fromAlias("e"), "iou")
                .fetchPath(DQLJoinType.inner, EduProgramHigherProf.kind().fromAlias("e"),  "k")
                .fetchPath(DQLJoinType.inner, EduProgramHigherProf.form().fromAlias("e"),  "f")
                .fetchPath(DQLJoinType.inner, EduProgramHigherProf.duration().fromAlias("e"),  "d")
                .fetchPath(DQLJoinType.inner, EduProgramHigherProf.year().fromAlias("e"),  "y")
                .fetchPath(DQLJoinType.inner, EduProgramHigherProf.programSpecialization().fromAlias("e"),  "spec")
                .fetchPath(DQLJoinType.inner, EduProgramHigherProf.programQualification().fromAlias("e"),  "qua");

        String eduProgramTitle = StringUtils.trimToNull(context.<String>get(EDU_PROGRAM_TITLE));
        EduProgramKind eduProgramKind = context.get(EDU_PROGRAM_KIND);
        EduProgramForm eduProgramForm = context.get(EDU_PROGRAM_FORM);
        EduProgramDuration eduProgramDuration = context.get(EDU_PROGRAM_DURATION);
        EducationYear eduYear = context.get(EDU_YEAR);
        EduProgramSubjectIndex eduProgramSubjectIndex = context.get(EDU_PROGRAM_SUBJECT_INDEX);
        EduProgramSubject eduProgramSubject = context.get(EDU_PROGRAM_SUBJECT);
        EduProgramSpecialization eduProgramSpecialization = context.get(EDU_PROGRAM_SPECIALIZATION);
        EduOwnerOrgUnit eduOwnerOrgUnit = context.get(EDU_OWNER_ORG_UNIT);
        EduInstitutionOrgUnit eduInstitutionOrgUnit = context.get(EDU_INSTITUTION_ORG_UNIT);
        DataWrapper nextYearCopy = context.get(BIND_NEXT_YEAR_COPY);

        if (eduProgramTitle != null)
            builder.where(likeUpper(property("e", EduProgramHigherProf.title()), value(CoreStringUtils.escapeLike(eduProgramTitle))));

        if (eduProgramKind != null)
            builder.where(eq(property("e", EduProgramHigherProf.kind()), value(eduProgramKind)));

        if (eduProgramForm != null)
            builder.where(eq(property("e", EduProgramHigherProf.form()), value(eduProgramForm)));

        if (eduProgramDuration != null)
            builder.where(eq(property("e", EduProgramHigherProf.duration()), value(eduProgramDuration)));

        if (eduYear != null)
            builder.where(eq(property("e", EduProgramHigherProf.year()), value(eduYear)));

        if (eduProgramSubjectIndex != null)
            builder.where(eq(property("e", EduProgramHigherProf.programSubject().subjectIndex()), value(eduProgramSubjectIndex)));

        if (eduProgramSubject != null)
            builder.where(eq(property("e", EduProgramHigherProf.programSubject()), value(eduProgramSubject)));

        if (eduProgramSpecialization != null)
            builder.where(eq(property("e", EduProgramHigherProf.programSpecialization()), value(eduProgramSpecialization)));

        if (eduOwnerOrgUnit != null)
            builder.where(eq(property("e", EduProgramHigherProf.ownerOrgUnit()), value(eduOwnerOrgUnit)));

        if (eduInstitutionOrgUnit != null)
            builder.where(eq(property("e", EduProgramHigherProf.institutionOrgUnit()), value(eduInstitutionOrgUnit)));

        if (nextYearCopy != null)
        {
            IDQLSelectableQuery nextYearCopyQuery = new DQLSelectBuilder()
                .fromEntity(EduProgramHigherProf.class, "p").column(property("p.id"))
                .where(eq(property("e", EduProgramHigherProf.programUniqueKey()), property("p", EduProgramHigherProf.programUniqueKey())))
                .where(eq(plus(property("e", EduProgramHigherProf.year().intValue()), value(1)), property("p", EduProgramHigherProf.year().intValue())))
                .buildQuery();

            if (TwinComboDataSourceHandler.getSelectedValueNotNull(nextYearCopy))
                builder.where(exists(nextYearCopyQuery));
            else
                builder.where(notExists(nextYearCopyQuery));
        }

        DQLOrderDescriptionRegistry orderDescription = new DQLOrderDescriptionRegistry(EduProgramHigherProf.class, "e");
        {
            orderDescription.setOrders(EduProgramHigherProf.title(), new OrderDescription("e", EduProgramHigherProf.title()));
            orderDescription.setOrders(EduProgramHigherProf.kind().shortTitle(), new OrderDescription("e", EduProgramHigherProf.kind().shortTitle()));
            orderDescription.setOrders(EduProgramHigherProf.form().title(), new OrderDescription("e", EduProgramHigherProf.form().title()));
            orderDescription.setOrders(EduProgramHigherProf.duration().title(), new OrderDescription("e", EduProgramHigherProf.duration().title()));
            orderDescription.setOrders(EduProgramHigherProf.year().title(), new OrderDescription("e", EduProgramHigherProf.year().intValue()));
            orderDescription.setOrders(EduProgramHigherProf.programSubject().subjectIndex().code(), new OrderDescription("e", EduProgramHigherProf.programSubject().subjectIndex().code()));
            orderDescription.setOrders(EduProgramHigherProf.programSubject().titleWithCode(),
                    new OrderDescription("e", EduProgramHigherProf.programSubject().code()),
                    new OrderDescription("e", EduProgramHigherProf.programSubject().title()));

            orderDescription.setOrders(EduProgramHigherProf.programSpecialization().title(), new OrderDescription("e", EduProgramHigherProf.programSpecialization().title()));
            orderDescription.setOrders(EduProgramHigherProf.ownerOrgUnit().orgUnit().shortTitle(), new OrderDescription("e", EduProgramHigherProf.ownerOrgUnit().orgUnit().shortTitle()));
            orderDescription.setOrders(EduProgramHigherProf.institutionOrgUnit().orgUnit().shortTitle(), new OrderDescription("e", EduProgramHigherProf.institutionOrgUnit().orgUnit().shortTitle()));
        }

        orderDescription.applyOrder(builder, input.getEntityOrder());


        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
    }
}