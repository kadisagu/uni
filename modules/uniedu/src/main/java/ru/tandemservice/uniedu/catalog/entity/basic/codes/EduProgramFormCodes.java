package ru.tandemservice.uniedu.catalog.entity.basic.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Форма обучения"
 * Имя сущности : eduProgramForm
 * Файл data.xml : uniedu.basic.data.xml
 */
public interface EduProgramFormCodes
{
    /** Константа кода (code) элемента : Очная (title) */
    String OCHNAYA = "1";
    /** Константа кода (code) элемента : Заочная (title) */
    String ZAOCHNAYA = "2";
    /** Константа кода (code) элемента : Очно-заочная (title) */
    String OCHNO_ZAOCHNAYA = "3";

    Set<String> CODES = ImmutableSet.of(OCHNAYA, ZAOCHNAYA, OCHNO_ZAOCHNAYA);
}
