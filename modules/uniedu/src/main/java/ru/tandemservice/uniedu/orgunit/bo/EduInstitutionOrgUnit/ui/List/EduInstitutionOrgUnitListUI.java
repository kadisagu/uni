/**
 *$Id$
 */
package ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.EduInstitutionOrgUnitManager;
import ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.ui.Add.EduInstitutionOrgUnitAdd;
import ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.ui.Edit.EduInstitutionOrgUnitEdit;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;

/**
 * @author Alexander Zhebko
 * @since 03.02.2014
 */
public class EduInstitutionOrgUnitListUI extends UIPresenter
{
    public String getEduInstitutionOrgUnitDeleteAlert()
    {
        EduInstitutionOrgUnit current = getConfig().getDataSource(EduInstitutionOrgUnitList.EDU_INSTITUTION_ORG_UNIT_LIST_DS).getCurrent();
        return "Удалить аккредитованное подразделение «" + current.getOrgUnit().getFullTitle() + "»?";
    }

    public void onClickAddEduInstitutionOrgUnit()
    {
        _uiActivation.asRegionDialog(EduInstitutionOrgUnitAdd.class).activate();
    }

    public void onClickDeleteEduInstitutionOrgUnit()
    {
        EduInstitutionOrgUnitManager.instance().dao().deleteEduInstitutionOrgUnit(getListenerParameterAsLong());
    }

	public void onEditEntityFromList()
	{
		_uiActivation.asRegionDialog(EduInstitutionOrgUnitEdit.class)
				.parameter(UIPresenter.PUBLISHER_ID, getListenerParameter())
				.activate();
	}
}