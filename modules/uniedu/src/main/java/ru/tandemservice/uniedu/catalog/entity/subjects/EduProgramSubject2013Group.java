package ru.tandemservice.uniedu.catalog.entity.subjects;

import ru.tandemservice.uniedu.catalog.entity.subjects.gen.*;

/**
 * Укрупненная группа (перечень направлений подготовки 2013)
 */
public class EduProgramSubject2013Group extends EduProgramSubject2013GroupGen
{
    @Override
    public String getDisplayableTitle()
    {
        return getCode() + " " + getTitle();
    }
}