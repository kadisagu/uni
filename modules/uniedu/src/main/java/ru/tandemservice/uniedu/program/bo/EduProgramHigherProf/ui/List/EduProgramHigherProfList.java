/**
 * $Id$
 */
package ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.ui.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.ui.Pub.EduProgramSubjectPub;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.EduInstitutionOrgUnitManager;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniedu.program.bo.EduProgram.EduProgramManager;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.ui.List.logic.EduProgramHigherProfListDSHandler;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.ui.Pub.EduProgramHigherProfPub;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 06.02.2014
 */
@Configuration
public class EduProgramHigherProfList extends BusinessComponentManager
{
    public static final String EDU_PROGRAM_HIGHER_PROF_LIST_DS = "eduProgramHigherProfListDS";
    public static final String EDU_PROGRAM_FORM_DS = "eduProgramFormDS";
    public static final String EDU_PROGRAM_DURATION_DS = "eduProgramDurationDS";
    public static final String EDU_YEAR_DS = "eduYearDS";
    public static final String EDU_PROGRAM_SUBJECT_INDEX_DS = "eduProgramSubjectIndexDS";
    public static final String EDU_PROGRAM_SUBJECT_DS = "eduProgramSubjectDS";
    public static final String EDU_PROGRAM_SPECIALIZATION_DS = "eduProgramSpecializationDS";
	public static final String EDU_OWNER_ORG_UNIT_DS = "eduOwnerOrgUnitDS";

	public static final String EDU_PROGRAM_KIND = "eduProgramKind";
	public static final String EDU_PROGRAM_SUBJECT_INDEX = "eduProgramSubjectIndex";
	public static final String EDU_PROGRAM_SUBJECT = "eduProgramSubject";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(EduProgramManager.EDU_PROGRAM_KIND_DS, getName(), EduProgramKind.defaultSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> dql.where(exists(EduProgramHigherProf.class, EduProgramHigherProf.kind().s(), property(alias))))))
                .addDataSource(selectDS(EDU_PROGRAM_FORM_DS, eduProgramFormDSHandler()))
                .addDataSource(selectDS(EDU_PROGRAM_DURATION_DS, eduProgramDurationDSHandler()))
                .addDataSource(selectDS(EDU_YEAR_DS, eduYearDSHandler()))
                .addDataSource(selectDS(EDU_PROGRAM_SUBJECT_INDEX_DS, eduProgramSubjectIndexDSHandler()))
                .addDataSource(selectDS(EDU_PROGRAM_SUBJECT_DS, eduProgramSubjectDSHandler())
                        .addColumn(EduProgramSubject.titleWithCode().s()))
                .addDataSource(selectDS(EDU_PROGRAM_SPECIALIZATION_DS, eduProgramSpecializationDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(EDU_OWNER_ORG_UNIT_DS, getName(), EduOwnerOrgUnit.eduOwnerOrgUnitReorganizationCustomizedOrgUnitHandler(getName()))
                        .valueStyleSource(EduOwnerOrgUnit.archivalWiseStyle))
                .addDataSource(EduInstitutionOrgUnitManager.instance().eduInstitutionOrgUnitDSConfig())
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .addDataSource(searchListDS(EDU_PROGRAM_HIGHER_PROF_LIST_DS, eduProgramHigherProfListDSColumns(), eduProgramHigherProfListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint eduProgramHigherProfListDSColumns()
    {
        final String CODE_SEPARATOR = ".";
        final IFormatter<String> eduProgramSubjectIndexCodeFormatter = source -> StringUtils.substringBefore(source, CODE_SEPARATOR);

        return columnListExtPointBuilder(EDU_PROGRAM_HIGHER_PROF_LIST_DS)
                .addColumn(checkboxColumn("check"))
                .addColumn(publisherColumn("eduProgramTitle", EduProgramHigherProf.title())
                        .businessComponent(EduProgramHigherProfPub.class).required(Boolean.TRUE).order())
                .addColumn(textColumn("eduProgramShortTitle", EduProgramHigherProf.shortTitle()))
                .addColumn(textColumn("eduProgramKind", EduProgramHigherProf.kind().shortTitle()).order())
                .addColumn(textColumn("baseEduLevel", EduProgramHigherProf.baseLevel().shortTitle()))
                .addColumn(textColumn("eduProgramForm", EduProgramHigherProf.form().title()).order())
                .addColumn(textColumn("eduProgramDuration", EduProgramHigherProf.duration().title()).order())
                .addColumn(textColumn("eduYear", EduProgramHigherProf.year().title()).order())
                .addColumn(textColumn("eduProgramSubjectIndex", EduProgramHigherProf.programSubject().subjectIndex().code()).formatter(eduProgramSubjectIndexCodeFormatter).order())
                .addColumn(publisherColumn("eduProgramSubject", EduProgramHigherProf.programSubject().titleWithCode())
                        .businessComponent(EduProgramSubjectPub.class).primaryKeyPath(EduProgramHigherProf.programSubject().id()).order())
                .addColumn(textColumn("eduProgramSpecialization", EduProgramHigherProf.programSpecialization().displayableTitle()).order())
                .addColumn(textColumn("eduProgramQualification", EduProgramHigherProf.programQualification().title()).order())
                .addColumn(textColumn("eduProgramOrientation", EduProgramHigherProf.programOrientation().abbreviation()))
                .addColumn(publisherColumn("eduOwnerOrgUnit", EduProgramHigherProf.ownerOrgUnit().orgUnit().shortTitle())
                        .businessComponent(OrgUnitView.class).primaryKeyPath(EduProgramHigherProf.ownerOrgUnit().orgUnit().id()).order())
                .addColumn(publisherColumn("eduInstitutionOrgUnit", EduProgramHigherProf.institutionOrgUnit().orgUnit().shortTitle())
                        .businessComponent(OrgUnitView.class).primaryKeyPath(EduProgramHigherProf.institutionOrgUnit().orgUnit().id()).order())
                .addColumn(textColumn("eduProgramTrait", EduProgram.eduProgramTrait().shortTitle()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEditEduProgram").permissionKey("edit_eduProgramHigherProf_list"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDeleteEduProgram").alert("ui:eduProgramDeleteAlert").permissionKey("delete_eduProgramHigherProf_list"))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler eduProgramHigherProfListDSHandler()
    {
        return new EduProgramHigherProfListDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramFormDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramForm.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                dql.where(in(property(alias, EduProgramForm.id()), new DQLSelectBuilder()
                        .fromEntity(EduProgramHigherProf.class, "ep")
                        .column(property("ep", EduProgramHigherProf.form().id()))
                        .predicate(DQLPredicateType.distinct)
                        .buildQuery()));

                super.applyWhereConditions(alias, dql, context);
            }
        }
                .filter(EduProgramForm.title())
                .order(EduProgramForm.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramDurationDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramDuration.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                dql.where(in(property(alias, EduProgramDuration.id()), new DQLSelectBuilder()
                        .fromEntity(EduProgramHigherProf.class, "ep")
                        .column(property("ep", EduProgramHigherProf.duration().id()))
                        .predicate(DQLPredicateType.distinct)
                        .buildQuery()));

                super.applyWhereConditions(alias, dql, context);
            }
        }
                .filter(EduProgramDuration.title())
                .order(EduProgramDuration.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduYearDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EducationYear.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                dql.where(in(property(alias, EducationYear.id()), new DQLSelectBuilder()
                        .fromEntity(EduProgramHigherProf.class, "ep")
                        .column(property("ep", EduProgramHigherProf.year().id()))
                        .predicate(DQLPredicateType.distinct)
                        .buildQuery()));

                super.applyWhereConditions(alias, dql, context);
            }
        }
                .filter(EducationYear.title())
                .order(EducationYear.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramSubjectIndexDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubjectIndex.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                dql.where(in(property(alias, EduProgramSubjectIndex.id()), new DQLSelectBuilder()
                        .fromEntity(EduProgramHigherProf.class, "ep")
                        .column(property("ep", EduProgramHigherProf.programSubject().subjectIndex().id()))
                        .predicate(DQLPredicateType.distinct)
                        .buildQuery()));

                EduProgramKind eduProgramKind = context.get(EDU_PROGRAM_KIND);
                if (eduProgramKind != null)
                    dql.where(eq(property(alias, EduProgramSubjectIndex.programKind()), value(eduProgramKind)));

                super.applyWhereConditions(alias, dql, context);
            }
        }
                .filter(EduProgramSubjectIndex.title())
                .order(EduProgramSubjectIndex.priority())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                dql.where(in(property(alias, EduProgramSubject.id()), new DQLSelectBuilder()
                        .fromEntity(EduProgramHigherProf.class, "ep")
                        .column(property("ep", EduProgramHigherProf.programSubject().id()))
                        .predicate(DQLPredicateType.distinct)
                        .buildQuery()));

                EduProgramKind eduProgramKind = context.get(EDU_PROGRAM_KIND);
                EduProgramSubjectIndex eduProgramSubjectIndex = context.get(EDU_PROGRAM_SUBJECT_INDEX);

                if (eduProgramSubjectIndex != null) {
                    dql.where(eq(property(alias, EduProgramSubject.subjectIndex()), value(eduProgramSubjectIndex)));

                } else if (eduProgramKind != null) {
                    dql.where(eq(property(alias, EduProgramSubject.subjectIndex().programKind()), value(eduProgramKind)));
                }

                super.applyWhereConditions(alias, dql, context);
            }

            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder dql = super.query(alias, filter);
                dql
                        .where(likeUpper(DQLFunctions.concat(property(alias, EduProgramSubject.subjectCode()), property(alias, EduProgramSubject.title())), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(alias, EduProgramSubject.subjectCode()))
                        .order(property(alias, EduProgramSubject.title()));

                return dql;
            }
        }.pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramSpecializationDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSpecialization.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                dql.where(in(property(alias, EduProgramSpecialization.id()), new DQLSelectBuilder()
                        .fromEntity(EduProgramHigherProf.class, "ep")
                        .column(property("ep", EduProgramHigherProf.programSpecialization().id()))
                        .predicate(DQLPredicateType.distinct)
                        .buildQuery()));

                EduProgramKind eduProgramKind = context.get(EDU_PROGRAM_KIND);
                EduProgramSubjectIndex eduProgramSubjectIndex = context.get(EDU_PROGRAM_SUBJECT_INDEX);
                EduProgramSubject eduProgramSubject = context.get(EDU_PROGRAM_SUBJECT);

                if (eduProgramSubject != null) {
                    dql.where(eq(property(alias, EduProgramSpecialization.programSubject()), value(eduProgramSubject)));

                } else if (eduProgramSubjectIndex != null) {
                    dql.where(eq(property(alias, EduProgramSpecialization.programSubject().subjectIndex()), value(eduProgramSubjectIndex)));

                } else if (eduProgramKind != null) {
                    dql.where(eq(property(alias, EduProgramSpecialization.programSubject().subjectIndex().programKind()), value(eduProgramKind)));
                }

                super.applyWhereConditions(alias, dql, context);
            }
        }
                .filter(EduProgramSpecialization.title())
                .order(EduProgramSpecialization.title())
                .pageable(true);
    }
}