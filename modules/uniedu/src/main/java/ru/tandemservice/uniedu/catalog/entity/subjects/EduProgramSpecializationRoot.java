package ru.tandemservice.uniedu.catalog.entity.subjects;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniedu.catalog.entity.subjects.gen.EduProgramSpecializationRootGen;

/**
 * Направленность, соответствующая направлению
 *
 * Соответсвует самому направлению подготовки
 */
public class EduProgramSpecializationRoot extends EduProgramSpecializationRootGen
{
    public static final String HARD_POSTFIX_TITLE = " (общая направленность)";

    @Override public boolean isRootSpecialization() {
        return true;
    }

    @Override
    @EntityDSLSupport(parts = P_TITLE)
    public String getDisplayableTitle()
    {
        return getTitle() + HARD_POSTFIX_TITLE;
    }
}