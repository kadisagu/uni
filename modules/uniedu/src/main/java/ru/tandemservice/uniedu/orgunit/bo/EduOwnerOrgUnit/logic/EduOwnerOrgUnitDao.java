package ru.tandemservice.uniedu.orgunit.bo.EduOwnerOrgUnit.logic;

import org.hibernate.Session;
import org.tandemframework.hibsupport.HibSupportUtils;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.gen.EduOwnerOrgUnitGen;

/**
 * @author vdanilov
 */
public class EduOwnerOrgUnitDao extends UniBaseDao implements IEduOwnerOrgUnitDao {

    @Override
    public EduOwnerOrgUnit doGetEduOwnerOrgUnit(OrgUnit orgUnit) {
        Session session = lock("EduOwnerOrgUnit."+orgUnit.getId());

        EduOwnerOrgUnit ou = HibSupportUtils.getByNaturalId(session, new EduOwnerOrgUnitGen.NaturalId(orgUnit));
        if (null != ou) { return ou; }

        ou = new EduOwnerOrgUnit();
        ou.setOrgUnit(orgUnit);
        session.save(ou);
        session.flush();

        return ou;
    }

    @Override
    public void deleteEduOwnerOrgUnit(Long eduOwnerOrgUnitId)
    {
        delete(eduOwnerOrgUnitId);
    }
}
