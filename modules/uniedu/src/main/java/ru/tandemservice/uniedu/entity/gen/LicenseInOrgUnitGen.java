package ru.tandemservice.uniedu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.entity.LicenseInOrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Лицензия в лицензированном подразделении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LicenseInOrgUnitGen extends EntityBase
 implements INaturalIdentifiable<LicenseInOrgUnitGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.entity.LicenseInOrgUnit";
    public static final String ENTITY_NAME = "licenseInOrgUnit";
    public static final int VERSION_HASH = 1001984572;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROGRAM_SUBJECT = "programSubject";
    public static final String L_INSTITUTION_ORG_UNIT = "institutionOrgUnit";

    private EduProgramSubject _programSubject;     // Направление подготовки профессионального образования
    private EduInstitutionOrgUnit _institutionOrgUnit;     // Аккредитованное подразделение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направление подготовки профессионального образования. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubject getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление подготовки профессионального образования. Свойство не может быть null.
     */
    public void setProgramSubject(EduProgramSubject programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    /**
     * @return Аккредитованное подразделение. Свойство не может быть null.
     */
    @NotNull
    public EduInstitutionOrgUnit getInstitutionOrgUnit()
    {
        return _institutionOrgUnit;
    }

    /**
     * @param institutionOrgUnit Аккредитованное подразделение. Свойство не может быть null.
     */
    public void setInstitutionOrgUnit(EduInstitutionOrgUnit institutionOrgUnit)
    {
        dirty(_institutionOrgUnit, institutionOrgUnit);
        _institutionOrgUnit = institutionOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LicenseInOrgUnitGen)
        {
            if (withNaturalIdProperties)
            {
                setProgramSubject(((LicenseInOrgUnit)another).getProgramSubject());
                setInstitutionOrgUnit(((LicenseInOrgUnit)another).getInstitutionOrgUnit());
            }
        }
    }

    public INaturalId<LicenseInOrgUnitGen> getNaturalId()
    {
        return new NaturalId(getProgramSubject(), getInstitutionOrgUnit());
    }

    public static class NaturalId extends NaturalIdBase<LicenseInOrgUnitGen>
    {
        private static final String PROXY_NAME = "LicenseInOrgUnitNaturalProxy";

        private Long _programSubject;
        private Long _institutionOrgUnit;

        public NaturalId()
        {}

        public NaturalId(EduProgramSubject programSubject, EduInstitutionOrgUnit institutionOrgUnit)
        {
            _programSubject = ((IEntity) programSubject).getId();
            _institutionOrgUnit = ((IEntity) institutionOrgUnit).getId();
        }

        public Long getProgramSubject()
        {
            return _programSubject;
        }

        public void setProgramSubject(Long programSubject)
        {
            _programSubject = programSubject;
        }

        public Long getInstitutionOrgUnit()
        {
            return _institutionOrgUnit;
        }

        public void setInstitutionOrgUnit(Long institutionOrgUnit)
        {
            _institutionOrgUnit = institutionOrgUnit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof LicenseInOrgUnitGen.NaturalId) ) return false;

            LicenseInOrgUnitGen.NaturalId that = (NaturalId) o;

            if( !equals(getProgramSubject(), that.getProgramSubject()) ) return false;
            if( !equals(getInstitutionOrgUnit(), that.getInstitutionOrgUnit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getProgramSubject());
            result = hashCode(result, getInstitutionOrgUnit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getProgramSubject());
            sb.append("/");
            sb.append(getInstitutionOrgUnit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LicenseInOrgUnitGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LicenseInOrgUnit.class;
        }

        public T newInstance()
        {
            return (T) new LicenseInOrgUnit();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "programSubject":
                    return obj.getProgramSubject();
                case "institutionOrgUnit":
                    return obj.getInstitutionOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((EduProgramSubject) value);
                    return;
                case "institutionOrgUnit":
                    obj.setInstitutionOrgUnit((EduInstitutionOrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "programSubject":
                        return true;
                case "institutionOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "programSubject":
                    return true;
                case "institutionOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "programSubject":
                    return EduProgramSubject.class;
                case "institutionOrgUnit":
                    return EduInstitutionOrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LicenseInOrgUnit> _dslPath = new Path<LicenseInOrgUnit>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LicenseInOrgUnit");
    }
            

    /**
     * @return Направление подготовки профессионального образования. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.entity.LicenseInOrgUnit#getProgramSubject()
     */
    public static EduProgramSubject.Path<EduProgramSubject> programSubject()
    {
        return _dslPath.programSubject();
    }

    /**
     * @return Аккредитованное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.entity.LicenseInOrgUnit#getInstitutionOrgUnit()
     */
    public static EduInstitutionOrgUnit.Path<EduInstitutionOrgUnit> institutionOrgUnit()
    {
        return _dslPath.institutionOrgUnit();
    }

    public static class Path<E extends LicenseInOrgUnit> extends EntityPath<E>
    {
        private EduProgramSubject.Path<EduProgramSubject> _programSubject;
        private EduInstitutionOrgUnit.Path<EduInstitutionOrgUnit> _institutionOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направление подготовки профессионального образования. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.entity.LicenseInOrgUnit#getProgramSubject()
     */
        public EduProgramSubject.Path<EduProgramSubject> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new EduProgramSubject.Path<EduProgramSubject>(L_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

    /**
     * @return Аккредитованное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.entity.LicenseInOrgUnit#getInstitutionOrgUnit()
     */
        public EduInstitutionOrgUnit.Path<EduInstitutionOrgUnit> institutionOrgUnit()
        {
            if(_institutionOrgUnit == null )
                _institutionOrgUnit = new EduInstitutionOrgUnit.Path<EduInstitutionOrgUnit>(L_INSTITUTION_ORG_UNIT, this);
            return _institutionOrgUnit;
        }

        public Class getEntityClass()
        {
            return LicenseInOrgUnit.class;
        }

        public String getEntityName()
        {
            return "licenseInOrgUnit";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
