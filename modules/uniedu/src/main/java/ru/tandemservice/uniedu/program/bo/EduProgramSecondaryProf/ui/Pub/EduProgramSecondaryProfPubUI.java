/**
 *$Id$
 */
package ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.ui.Pub;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.program.bo.EduProgram.ui.ChangeParams.EduProgramChangeParams;
import ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.EduProgramSecondaryProfManager;
import ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.ui.AddEdit.EduProgramSecondaryProfAddEdit;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;

import java.util.Collection;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 10.02.2014
 */
@State(
        {
                @Bind(key = "selectedTab"),
                @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id", required = true)
        }
)
public class EduProgramSecondaryProfPubUI extends UIPresenter
{
    private EntityHolder<EduProgramSecondaryProf> _holder = new EntityHolder<>();
    public EntityHolder<EduProgramSecondaryProf> getHolder(){ return _holder; }
    public EduProgramSecondaryProf getEduProgram(){ return _holder.getValue(); }

    private String _selectedTab;

    // TODO: вырезать это, когда будут сделаны нормальные параметры обучения студентов
    @Zlo("решение временное, поэтому хардкодим")
    private static final boolean eduOuExists = (null != EntityRuntime.getMeta("educationOrgUnit"));
    public boolean isShowStudentParamBlock() { return eduOuExists && !EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_.equals(getEduProgram().getProgramSubject().getEduProgramKind().getCode()); }

    @Override
    public void onComponentRefresh()
    {
        _holder.refresh();
    }

    public void onClickEditEduProgram()
    {
        _uiActivation.asRegionDialog(EduProgramSecondaryProfAddEdit.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, getEduProgram().getId()).activate();
    }

    public void onClickEditEduProgramData()
    {
        _uiActivation.asDesktopRoot(EduProgramChangeParams.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, getEduProgram().getId()).activate();
    }

    public void onClickDeleteEduProgram()
    {
        IUniBaseDao.instance.get().delete(getHolder().getValue());
        deactivate();
    }

    public void onClickAddEduOu() {
        _uiActivation.asRegionDialog("UniEduProgramEduOuAddEdit").parameter("programId", getEduProgram().getId()).activate();
    }

    // getters and setters

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }


}