/* $Id$ */
package ru.tandemservice.uniedu.program.util;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityPath;
import org.tandemframework.core.meta.entity.IConstraintMeta;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.constraint.IConstraintViolationHandler;
import org.tandemframework.hibsupport.dql.dset.IDSet;
import org.tandemframework.hibsupport.dql.dset.impl.ClassDSwitcher;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author azhebko
 * @since 28.08.2014
 */
public class ProgramKeyUniqueHandler implements IConstraintViolationHandler
{
    private ProgramKeyUniqueHandler() {}

    @Override
    public String onError(DSetEvent event, IConstraintMeta constraint, IDSet<IEntity> invalidEntities)
    {
        final Set<Class> clsSet = invalidEntities.getOptions(ClassDSwitcher.getInstance(), event.getContext());
        if (clsSet.size() != 1) {
            return null;
        }

        final Class clazz = clsSet.iterator().next();
        final List<EntityPath> properties = new ArrayList<>(12);

        properties.add(EduProgram.year()); // Учебный год

        properties.add(EduProgram.kind()); // Вид ОП
        properties.add(EduProgram.institutionOrgUnit()); // Аккредитованное подразделение
        properties.add(EduProgram.ownerOrgUnit()); // Выпускающее подразделение
        properties.add(EduProgram.form()); // Форма обучения
        properties.add(EduProgram.eduProgramTrait()); // Особенность реализации ОП
        properties.add(EduProgram.duration()); // Продолжительность обучения

        if (EduProgramProf.class.isAssignableFrom(clazz)) {
            properties.add(EduProgramProf.baseLevel()); // Необходимый уровень для обучения по образовательной программе
            properties.add(EduProgramProf.programSubject()); // Направление подготовки
        }

        if (EduProgramHigherProf.class.isAssignableFrom(clazz)) {
            properties.add(EduProgramHigherProf.programSpecialization()); // Направленность
            properties.add(EduProgramProf.programQualification()); // Квалификация. Она тут, а не в EduProgramProf, потому что так надо для СПО.
            properties.add(EduProgramHigherProf.programOrientation()); // Ориентация ОП
        }

        final IEntityMeta meta = EntityRuntime.getMeta(clazz);
        final String propertiesStr = properties.stream()
                .map(p -> ("«" + meta.getProperty(p.getPath()).getTitle() + "»"))
                .collect(Collectors.joining(", "));

        return "Набор полей " + propertiesStr + " объекта «" + meta.getTitle() + "» должен быть уникален.";
    }

    public static IConstraintViolationHandler getInstance() {
        return new ProgramKeyUniqueHandler();
    }
}