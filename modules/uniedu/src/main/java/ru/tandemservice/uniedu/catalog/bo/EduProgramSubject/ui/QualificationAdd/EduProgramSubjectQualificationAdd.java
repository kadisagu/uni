/* $Id:$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.ui.QualificationAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;

/**
 * @author oleyba
 * @since 11/21/13
 */
@Configuration
public class EduProgramSubjectQualificationAdd extends BusinessComponentManager
{
    public static final String PARAM_SUBJ_INDEX = "subjIndex";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS("qualDS", qualDSHandler()))
            .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> qualDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EduProgramQualification.class)
        .where(EduProgramQualification.subjectIndex(), PARAM_SUBJ_INDEX)
        .order(EduProgramQualification.title())
        .filter(EduProgramQualification.title())
        ;
    }

}