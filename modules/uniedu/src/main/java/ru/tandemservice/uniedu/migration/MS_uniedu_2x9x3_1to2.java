package ru.tandemservice.uniedu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniedu_2x9x3_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eduProgramKind

		// создано обязательное свойство priority
		{
			// создать колонку
			tool.createColumn("edu_c_program_kind_t", new DBColumn("priority_p", DBType.INTEGER));

			final MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update edu_c_program_kind_t set priority_p=? where code_p=?", DBType.INTEGER, DBType.EMPTY_STRING);
			updater.addBatch(1, "2013.2.1.1");
			updater.addBatch(2, "2013.2.1.2");
			updater.addBatch(3, "2013.2.2.1");
			updater.addBatch(4, "2013.2.3.1");
			updater.addBatch(5, "2013.2.3.2");
			updater.addBatch(6, "2013.2.4.1");
			updater.addBatch(7, "2013.2.4.2");
			updater.addBatch(8, "2013.2.4.3");
			updater.addBatch(9, "2013.3.2.1");
			updater.executeUpdate(tool);

			// сделать колонку NOT NULL
			tool.setColumnNullable("edu_c_program_kind_t", "priority_p", false);

		}


    }
}