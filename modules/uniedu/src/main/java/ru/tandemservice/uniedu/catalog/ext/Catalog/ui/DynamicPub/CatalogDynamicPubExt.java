/* $Id:$ */
package ru.tandemservice.uniedu.catalog.ext.Catalog.ui.DynamicPub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.DynamicPub.CatalogDynamicPub;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;

/**
 * @author oleyba
 * @since 5/19/14
 */
@Configuration
public class CatalogDynamicPubExt extends BusinessComponentExtensionManager
{
    @Autowired
    private CatalogDynamicPub _catalogDynamicPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_catalogDynamicPub.presenterExtPoint())
            .addAction(new EduProgramKind.EditDeclinationAction("onClickEditDeclination4EduProgramKind"))
            .create();
    }

}
