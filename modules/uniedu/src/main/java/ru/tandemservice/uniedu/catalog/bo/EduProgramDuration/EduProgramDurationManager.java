/* $Id:$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramDuration;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author oleyba
 * @since 11/19/13
 */
@Configuration
public class EduProgramDurationManager extends BusinessObjectManager
{
    public static EduProgramDurationManager instance()
    {
        return instance(EduProgramDurationManager.class);
    }
}
