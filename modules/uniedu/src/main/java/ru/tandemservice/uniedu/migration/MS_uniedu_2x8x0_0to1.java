/* $Id$ */
package ru.tandemservice.uniedu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Nikolay Fedorovskih
 * @since 30.03.2015
 */
public class MS_uniedu_2x8x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.17"),
                new ScriptDependency("org.tandemframework.shared", "1.8.0")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // Неблагодарное это дело - переименования пользовательских полей
        tool.executeUpdate("update edu_c_program_kind_t set shorttitle_p=? where code_p=? and shorttitle_p=?", "аспирантура (адъюнктура)", "2013.2.4.1", "аспирантура");
        tool.executeUpdate("update edu_c_program_kind_t set eduprogramsubjectkind_p=? where code_p=?", "Специальность", "2013.2.4.2");

        // Падежи обновить
        tool.executeUpdate("delete from declinableproperty_t where declinable_id=(select id from edu_c_program_kind_t where code_p=?)", "2013.2.4.2");
    }
}