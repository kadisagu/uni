package ru.tandemservice.uniedu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;
import ru.tandemservice.uniedu.entity.EduProgramSubjectComparison;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сопоставление направлений подготовки ВО
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramSubjectComparisonGen extends EntityBase
 implements INaturalIdentifiable<EduProgramSubjectComparisonGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.entity.EduProgramSubjectComparison";
    public static final String ENTITY_NAME = "eduProgramSubjectComparison";
    public static final int VERSION_HASH = -1509168363;
    private static IEntityMeta ENTITY_META;

    public static final String L_OLD_PROGRAM_SUBJECT = "oldProgramSubject";
    public static final String L_NEW_PROGRAM_SUBJECT = "newProgramSubject";

    private EduProgramSubject _oldProgramSubject;     // Направление подготовки ВПО перечней ОКСО и 2009
    private EduProgramSubject2013 _newProgramSubject;     // Направление подготовки ВО 2013

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направление подготовки ВПО перечней ОКСО и 2009. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EduProgramSubject getOldProgramSubject()
    {
        return _oldProgramSubject;
    }

    /**
     * @param oldProgramSubject Направление подготовки ВПО перечней ОКСО и 2009. Свойство не может быть null и должно быть уникальным.
     */
    public void setOldProgramSubject(EduProgramSubject oldProgramSubject)
    {
        dirty(_oldProgramSubject, oldProgramSubject);
        _oldProgramSubject = oldProgramSubject;
    }

    /**
     * @return Направление подготовки ВО 2013. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubject2013 getNewProgramSubject()
    {
        return _newProgramSubject;
    }

    /**
     * @param newProgramSubject Направление подготовки ВО 2013. Свойство не может быть null.
     */
    public void setNewProgramSubject(EduProgramSubject2013 newProgramSubject)
    {
        dirty(_newProgramSubject, newProgramSubject);
        _newProgramSubject = newProgramSubject;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EduProgramSubjectComparisonGen)
        {
            if (withNaturalIdProperties)
            {
                setOldProgramSubject(((EduProgramSubjectComparison)another).getOldProgramSubject());
            }
            setNewProgramSubject(((EduProgramSubjectComparison)another).getNewProgramSubject());
        }
    }

    public INaturalId<EduProgramSubjectComparisonGen> getNaturalId()
    {
        return new NaturalId(getOldProgramSubject());
    }

    public static class NaturalId extends NaturalIdBase<EduProgramSubjectComparisonGen>
    {
        private static final String PROXY_NAME = "EduProgramSubjectComparisonNaturalProxy";

        private Long _oldProgramSubject;

        public NaturalId()
        {}

        public NaturalId(EduProgramSubject oldProgramSubject)
        {
            _oldProgramSubject = ((IEntity) oldProgramSubject).getId();
        }

        public Long getOldProgramSubject()
        {
            return _oldProgramSubject;
        }

        public void setOldProgramSubject(Long oldProgramSubject)
        {
            _oldProgramSubject = oldProgramSubject;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EduProgramSubjectComparisonGen.NaturalId) ) return false;

            EduProgramSubjectComparisonGen.NaturalId that = (NaturalId) o;

            if( !equals(getOldProgramSubject(), that.getOldProgramSubject()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOldProgramSubject());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOldProgramSubject());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramSubjectComparisonGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramSubjectComparison.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramSubjectComparison();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "oldProgramSubject":
                    return obj.getOldProgramSubject();
                case "newProgramSubject":
                    return obj.getNewProgramSubject();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "oldProgramSubject":
                    obj.setOldProgramSubject((EduProgramSubject) value);
                    return;
                case "newProgramSubject":
                    obj.setNewProgramSubject((EduProgramSubject2013) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "oldProgramSubject":
                        return true;
                case "newProgramSubject":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "oldProgramSubject":
                    return true;
                case "newProgramSubject":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "oldProgramSubject":
                    return EduProgramSubject.class;
                case "newProgramSubject":
                    return EduProgramSubject2013.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgramSubjectComparison> _dslPath = new Path<EduProgramSubjectComparison>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramSubjectComparison");
    }
            

    /**
     * @return Направление подготовки ВПО перечней ОКСО и 2009. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.entity.EduProgramSubjectComparison#getOldProgramSubject()
     */
    public static EduProgramSubject.Path<EduProgramSubject> oldProgramSubject()
    {
        return _dslPath.oldProgramSubject();
    }

    /**
     * @return Направление подготовки ВО 2013. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.entity.EduProgramSubjectComparison#getNewProgramSubject()
     */
    public static EduProgramSubject2013.Path<EduProgramSubject2013> newProgramSubject()
    {
        return _dslPath.newProgramSubject();
    }

    public static class Path<E extends EduProgramSubjectComparison> extends EntityPath<E>
    {
        private EduProgramSubject.Path<EduProgramSubject> _oldProgramSubject;
        private EduProgramSubject2013.Path<EduProgramSubject2013> _newProgramSubject;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направление подготовки ВПО перечней ОКСО и 2009. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.entity.EduProgramSubjectComparison#getOldProgramSubject()
     */
        public EduProgramSubject.Path<EduProgramSubject> oldProgramSubject()
        {
            if(_oldProgramSubject == null )
                _oldProgramSubject = new EduProgramSubject.Path<EduProgramSubject>(L_OLD_PROGRAM_SUBJECT, this);
            return _oldProgramSubject;
        }

    /**
     * @return Направление подготовки ВО 2013. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.entity.EduProgramSubjectComparison#getNewProgramSubject()
     */
        public EduProgramSubject2013.Path<EduProgramSubject2013> newProgramSubject()
        {
            if(_newProgramSubject == null )
                _newProgramSubject = new EduProgramSubject2013.Path<EduProgramSubject2013>(L_NEW_PROGRAM_SUBJECT, this);
            return _newProgramSubject;
        }

        public Class getEntityClass()
        {
            return EduProgramSubjectComparison.class;
        }

        public String getEntityName()
        {
            return "eduProgramSubjectComparison";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
