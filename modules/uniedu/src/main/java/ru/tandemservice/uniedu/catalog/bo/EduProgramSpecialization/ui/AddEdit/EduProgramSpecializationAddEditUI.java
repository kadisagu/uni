/* $Id:$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramSpecialization.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.EduProgramSubjectManager;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.ui.Pub.EduProgramSubjectPubUI;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationChild;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationRoot;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

/**
 * Форма добавления/редактирования направленности. Предполагается, что добавляются (в {@link EduProgramSubjectPubUI#onClickSpecAdd}) только направленности в рамках направления.
 * Общие направленности ({@link ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationRoot}) только редактируются.
 * @author oleyba
 * @since 11/27/13
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "specHolder.id"),
    @Bind(key = EduProgramSpecializationAddEditUI.KEY_SUBJECT_ID, binding = "subjectHolder.id")
})
public class EduProgramSpecializationAddEditUI extends UIPresenter
{
    public static final String KEY_SUBJECT_ID = "subjectId";

	private final EntityHolder<EduProgramSpecialization> specHolder = new EntityHolder<>();
    private final EntityHolder<EduProgramSubject> subjectHolder = new EntityHolder<>();

    public boolean isAddForm()
    {
	    return specHolder.getId() == null;
    }

    @Override
    public void onComponentRefresh()
    {
        if (isAddForm() && getSubjectHolder().getId() == null)
            throw new IllegalArgumentException();

        getSubjectHolder().refresh();

        if (!isAddForm())
            setSpec(IUniBaseDao.instance.get().getNotNull(EduProgramSpecialization.class, getSpec().getId()));
        else
        {
	        EduProgramSpecializationChild spec = new EduProgramSpecializationChild();
	        spec.setProgramSubject(getSubjectHolder().getValue());
	        setSpec(spec);
        }
    }

	public boolean isRootSpecialization()
	{
		return getSpec() instanceof EduProgramSpecializationRoot;
	}

    public void onClickApply()
    {
	    if (getSpec() instanceof EduProgramSpecializationChild)
            EduProgramSubjectManager.instance().dao().saveOrUpdateSpec((EduProgramSpecializationChild)getSpec());
	    else
		    DataAccessServices.dao().update(getSpec());
        deactivate();
    }

    // getters and setters

	public EntityHolder<EduProgramSpecialization> getSpecHolder()
	{
		return specHolder;
	}

    public EntityHolder<EduProgramSubject> getSubjectHolder()
    {
        return subjectHolder;
    }

    public EduProgramSpecialization getSpec()
    {
        return specHolder.getValue();
    }

    public void setSpec(EduProgramSpecialization spec)
    {
        specHolder.setValue(spec);
    }
}
