package ru.tandemservice.uniedu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniedu_2x11x0_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eduAccreditation

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("edu_accreditation_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eduaccreditation"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("owner_id", DBType.LONG).setNullable(false),
                new DBColumn("stateaccreditation_p", DBType.createVarchar(255)).setNullable(false)

			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eduAccreditation");

		}


        ////////////////////////////////////////////////////////////////////////////////
        // сущность eduProgramSubject

        // создано обязательное свойство license
        {
            // создать колонку
            tool.createColumn("edu_c_pr_subject_t", new DBColumn("license_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            java.lang.Boolean defaultLicense = false;
            tool.executeUpdate("update edu_c_pr_subject_t set license_p=? where license_p is null", defaultLicense);

            // сделать колонку NOT NULL
            tool.setColumnNullable("edu_c_pr_subject_t", "license_p", false);

        }


    }
}