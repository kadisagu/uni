/* $Id:$ */
package ru.tandemservice.uniedu.migration;

import org.apache.commons.beanutils.ConvertUtils;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.IDBConnect;
import org.tandemframework.dbsupport.ddl.IResultProcessor;
import org.tandemframework.dbsupport.ddl.ResultRowProcessor;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 9/4/14
 */
public class MS_uniedu_2x6x7_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.7")
        };
    }

    // TODO: перенести в DBTool если это будет работать
    public static final IResultProcessor<List<Object[]>> processor(final Class ...classes) {
        return new ResultRowProcessor<List<Object[]>>(new ArrayList<Object[]>()) {
            @Override protected void processRow(IDBConnect ctool, ResultSet rs, List<Object[]> result) throws SQLException {
                final Object[] row = new Object[classes.length];
                for (int i=0;i<classes.length;i++) {
                    final Object v = rs.getObject(1+i);
                    row[i] = (null == v ? null : ConvertUtils.convert(rs.getObject(1 + i), classes[i]));
                }
                result.add(row);
            }
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Map<Long, String> qualTitleMap = new HashMap<>();
        Map<Long, String> qualCodeMap = new HashMap<>();
        Map<Long, Long> qualIndexMap = new HashMap<>();

        final List<Object[]> qlinkrows = tool.executeQuery(processor(Long.class, String.class, String.class, Long.class),
            "select c.id, q.title_p, q.qualificationLevelCode_p, q.subjectIndex_id " +
                " from edu_program_qual_link_t l " +
                " join edu_c_pr_qual_t q on l.programQualification_id = q.id" +
                " join edu_program_qual_comp_t c on l.prgrmcmpstqlfctn_id = c.id " +
                " order by q.title_p");
        for (final Object[] row: qlinkrows) {
            Long id = (Long) row[0];
            qualTitleMap.put(id, CommonBaseStringUtil.joinWithSeparator("; ", qualTitleMap.get(id), (String) row[1]));
            qualCodeMap.put(id, (String) row[2]);
            qualIndexMap.put(id, (Long) row[3]);
        }

        Map<Long, Long> newIdMap = new HashMap<>();
        Map<String, Long> newIdByTitleMap = new HashMap<>();

        int code = 0;
        short entityCode = tool.entityCodes().ensure("eduProgramQualification".toLowerCase());
        for (Map.Entry<Long, String> entry : qualTitleMap.entrySet()) {

            Long oldId = entry.getKey();
            String title = entry.getValue();

            Long newId = newIdByTitleMap.get(title);
            if (null != newId) {
                newIdMap.put(oldId, newId);
                continue;
            }

            newId = EntityIDGenerator.generateNewId(entityCode);

            tool.executeUpdate("insert into edu_c_pr_qual_t (discriminator, id, code_p, qualificationlevelcode_p, subjectindex_id, title_p) values (?,?,?,?,?,?)",
                entityCode,
                newId,
                "comp." + (code++),
                qualCodeMap.get(oldId),
                qualIndexMap.get(oldId),
                title);

            newIdMap.put(oldId, newId);
            newIdByTitleMap.put(title, newId);
        }

        for (Map.Entry<Long, Long> entry : newIdMap.entrySet()) {
            tool.table("edu_program_prof_secondary_t").constraints().clear();
            tool.executeUpdate("update edu_program_prof_secondary_t set programQualification_id = ? where programQualification_id = ?", entry.getValue(), entry.getKey());
            if (tool.tableExists("epp_eduplan_prof_secondary_t")) {
                tool.table("epp_eduplan_prof_secondary_t").constraints().clear();
                tool.executeUpdate("update epp_eduplan_prof_secondary_t set programQualification_id = ? where programQualification_id = ?", entry.getValue(), entry.getKey());
            }
        }

        tool.dropTable("edu_program_qual_comp_t", true);
        tool.dropTable("edu_program_qual_link_t", true);

        tool.entityCodes().delete("eduProgram2QualificationLink");
        tool.entityCodes().delete("eduProgramCompositeQualification");

    }
}
