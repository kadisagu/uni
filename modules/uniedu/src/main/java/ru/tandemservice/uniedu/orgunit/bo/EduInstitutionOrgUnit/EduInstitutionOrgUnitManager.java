/**
 *$Id$
 */
package ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.logic.EduInstitutionOrgUnitDao;
import ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.logic.IEduInstitutionOrgUnitDao;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;

/**
 * @author Alexander Zhebko
 * @since 31.01.2014
 */
@Configuration
public class EduInstitutionOrgUnitManager extends BusinessObjectManager
{
    public static final String EDU_INSTITUTION_ORG_UNIT_DS = "eduInstitutionOrgUnitDS";

    public static EduInstitutionOrgUnitManager instance() {
        return instance(EduInstitutionOrgUnitManager.class);
    }

    @Bean
    public IEduInstitutionOrgUnitDao dao() {
        return new EduInstitutionOrgUnitDao();
    }

    @Bean
    public UIDataSourceConfig eduInstitutionOrgUnitDSConfig() {
        return CommonBaseStaticSelectDataSource.selectDS(
                EDU_INSTITUTION_ORG_UNIT_DS, getName(), EduInstitutionOrgUnit.defaultSelectDSHandler(getName())
        ).create();
    }

}