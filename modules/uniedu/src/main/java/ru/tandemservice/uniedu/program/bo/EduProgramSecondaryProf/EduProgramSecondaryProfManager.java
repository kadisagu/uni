/**
 *$Id$
 */
package ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.codes.EduLevelCodes;
import ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.logic.EduProgramSecondaryProfDao;
import ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.logic.IEduProgramSecondaryProfDao;

import java.util.Arrays;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 10.02.2014
 */
@Configuration
public class EduProgramSecondaryProfManager extends BusinessObjectManager
{
    public static final String EDU_PROGRAM_KIND_SECONDARY_PROF_DS = "eduProgramKindSecondaryProfDS";

    public static EduProgramSecondaryProfManager instance()
    {
        return instance(EduProgramSecondaryProfManager.class);
    }

    @Bean
    public IEduProgramSecondaryProfDao dao()
    {
        return new EduProgramSecondaryProfDao();
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramBaseLevelDSHandler()
    {
        final List<String> baseLevelCodes = Arrays.asList(EduLevelCodes.OSNOVNOE_OBTSHEE_OBRAZOVANIE, EduLevelCodes.SREDNEE_OBTSHEE_OBRAZOVANIE);

        return new EntityComboDataSourceHandler(getName(), EduLevel.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                dql.where(DQLExpressions.in(DQLExpressions.property(alias, EduLevel.code()), baseLevelCodes));
                super.applyWhereConditions(alias, dql, context);
            }
        }
                .filter(EduLevel.title())
                .order(EduLevel.title());
    }

    @Bean
    public UIDataSourceConfig programKindSecondaryProfDSConfig(){
        return CommonBaseStaticSelectDataSource.selectDS(EDU_PROGRAM_KIND_SECONDARY_PROF_DS, getName(), EduProgramKind.defaultSelectDSHandler(getName())
                .where(EduProgramKind.programSecondaryProf(), true))
                .create();
    }
}