/**
 *$Id$
 */
package ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;

import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * @author Alexander Zhebko
 * @since 13.02.2014
 */
public interface IEduProgramSecondaryProfDao extends INeedPersistenceSupport
{
    /**
     * Сохраняет образовательную программу СПО и указанные квалификации.
     * @param eduProgram образовательная программа СПО
     */
    public void saveOrUpdateEduProgramSecondaryProf(EduProgramSecondaryProf eduProgram);

    /** Создает копии образовательных программ СПО на следующий год. */
    public Integer createNextYearCopy(@NotNull Collection<EduProgramSecondaryProf> eduPrograms);
}