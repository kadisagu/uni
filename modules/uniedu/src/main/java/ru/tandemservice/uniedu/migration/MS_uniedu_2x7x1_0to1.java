/* $Id$ */
package ru.tandemservice.uniedu.migration;

import org.tandemframework.core.debug.Debug;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;

/**
 * @author Nikolay Fedorovskih
 * @since 09.02.2015
 */
@SuppressWarnings("unused")
public class MS_uniedu_2x7x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.16"),
                new ScriptDependency("org.tandemframework.shared", "1.7.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // Из ключа ОП убрали год (добавили его в юник-констрейнт)
        tool.table("edu_program_t").constraints().clear();

        SQLUpdateQuery upd = new SQLUpdateQuery("edu_program_t", "p");

        // Вырезаем год из кода ОП
        String afterYearSubStr = tool.getSubstringFunction("p.programuniquekey_p", tool.getLengthFunction("k.code_p") + "+6", "255");
        upd.set("programuniquekey_p", tool.createStringConcatenationSQL("k.code_p", afterYearSubStr));
        upd.where("p.programuniquekey_p like " + tool.createStringConcatenationSQL("k.code_p", "'/'", "cast(y.intvalue_p as varchar(4))", "'%'"));
        upd.getUpdatedTableFrom().innerJoin(SQLFrom.table("edu_c_program_kind_t", "k"), "p.kind_id=k.id");
        upd.getUpdatedTableFrom().innerJoin(SQLFrom.table("educationyear_t", "y"), "p.year_id=y.id");

        int ret = tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(upd));
        if (Debug.isEnabled())
            System.out.println(this.getClass().getSimpleName() + ": " + ret + " edu program keys migrated");
    }
}