package ru.tandemservice.uniedu.catalog.entity.subjects.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndexGeneration;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Перечень направлений подготовки и квалификаций профессионального образования
 *
 * Определяет набор направлений одного вида ОП.
 * Формируется из приказа, вводящего перечень, и уточняющих его документов: выбирается только те части документов, которые относятся к указанному виду ОП.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramSubjectIndexGen extends EntityBase
 implements INaturalIdentifiable<EduProgramSubjectIndexGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex";
    public static final String ENTITY_NAME = "eduProgramSubjectIndex";
    public static final int VERSION_HASH = 612607147;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_EDU_LEVEL_AND_INDEX_NOTATION = "eduLevelAndIndexNotation";
    public static final String P_EDU_PROGRAM_SUBJECT_TYPE = "eduProgramSubjectType";
    public static final String L_GENERATION = "generation";
    public static final String L_PROGRAM_KIND = "programKind";
    public static final String P_PRIORITY = "priority";
    public static final String P_PREFIX = "prefix";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _shortTitle;     // Сокращенное название
    private String _eduLevelAndIndexNotation;     // Обозначение уровня образ. и перечня
    private String _eduProgramSubjectType;     // Тип направления для перечня
    private EduProgramSubjectIndexGeneration _generation;     // Поколение перечня
    private EduProgramKind _programKind;     // Вид образовательной программы
    private int _priority;     // Приоритет
    private String _prefix;     // Префикс для индекса УП
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null и должно быть уникальным.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Обозначение уровня образ. и перечня. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEduLevelAndIndexNotation()
    {
        return _eduLevelAndIndexNotation;
    }

    /**
     * @param eduLevelAndIndexNotation Обозначение уровня образ. и перечня. Свойство не может быть null.
     */
    public void setEduLevelAndIndexNotation(String eduLevelAndIndexNotation)
    {
        dirty(_eduLevelAndIndexNotation, eduLevelAndIndexNotation);
        _eduLevelAndIndexNotation = eduLevelAndIndexNotation;
    }

    /**
     * @return Тип направления для перечня. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEduProgramSubjectType()
    {
        return _eduProgramSubjectType;
    }

    /**
     * @param eduProgramSubjectType Тип направления для перечня. Свойство не может быть null.
     */
    public void setEduProgramSubjectType(String eduProgramSubjectType)
    {
        dirty(_eduProgramSubjectType, eduProgramSubjectType);
        _eduProgramSubjectType = eduProgramSubjectType;
    }

    /**
     * @return Поколение перечня. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubjectIndexGeneration getGeneration()
    {
        return _generation;
    }

    /**
     * @param generation Поколение перечня. Свойство не может быть null.
     */
    public void setGeneration(EduProgramSubjectIndexGeneration generation)
    {
        dirty(_generation, generation);
        _generation = generation;
    }

    /**
     * Определяет вид ОП, для которого справедлив данный перечень
     *
     * @return Вид образовательной программы. Свойство не может быть null.
     */
    @NotNull
    public EduProgramKind getProgramKind()
    {
        return _programKind;
    }

    /**
     * @param programKind Вид образовательной программы. Свойство не может быть null.
     */
    public void setProgramKind(EduProgramKind programKind)
    {
        dirty(_programKind, programKind);
        _programKind = programKind;
    }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null и должно быть уникальным.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Префикс для индекса УП.
     */
    @Length(max=255)
    public String getPrefix()
    {
        return _prefix;
    }

    /**
     * @param prefix Префикс для индекса УП.
     */
    public void setPrefix(String prefix)
    {
        dirty(_prefix, prefix);
        _prefix = prefix;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EduProgramSubjectIndexGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EduProgramSubjectIndex)another).getCode());
            }
            setShortTitle(((EduProgramSubjectIndex)another).getShortTitle());
            setEduLevelAndIndexNotation(((EduProgramSubjectIndex)another).getEduLevelAndIndexNotation());
            setEduProgramSubjectType(((EduProgramSubjectIndex)another).getEduProgramSubjectType());
            setGeneration(((EduProgramSubjectIndex)another).getGeneration());
            setProgramKind(((EduProgramSubjectIndex)another).getProgramKind());
            setPriority(((EduProgramSubjectIndex)another).getPriority());
            setPrefix(((EduProgramSubjectIndex)another).getPrefix());
            setTitle(((EduProgramSubjectIndex)another).getTitle());
        }
    }

    public INaturalId<EduProgramSubjectIndexGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EduProgramSubjectIndexGen>
    {
        private static final String PROXY_NAME = "EduProgramSubjectIndexNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EduProgramSubjectIndexGen.NaturalId) ) return false;

            EduProgramSubjectIndexGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramSubjectIndexGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramSubjectIndex.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramSubjectIndex();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "eduLevelAndIndexNotation":
                    return obj.getEduLevelAndIndexNotation();
                case "eduProgramSubjectType":
                    return obj.getEduProgramSubjectType();
                case "generation":
                    return obj.getGeneration();
                case "programKind":
                    return obj.getProgramKind();
                case "priority":
                    return obj.getPriority();
                case "prefix":
                    return obj.getPrefix();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "eduLevelAndIndexNotation":
                    obj.setEduLevelAndIndexNotation((String) value);
                    return;
                case "eduProgramSubjectType":
                    obj.setEduProgramSubjectType((String) value);
                    return;
                case "generation":
                    obj.setGeneration((EduProgramSubjectIndexGeneration) value);
                    return;
                case "programKind":
                    obj.setProgramKind((EduProgramKind) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "prefix":
                    obj.setPrefix((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "eduLevelAndIndexNotation":
                        return true;
                case "eduProgramSubjectType":
                        return true;
                case "generation":
                        return true;
                case "programKind":
                        return true;
                case "priority":
                        return true;
                case "prefix":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "eduLevelAndIndexNotation":
                    return true;
                case "eduProgramSubjectType":
                    return true;
                case "generation":
                    return true;
                case "programKind":
                    return true;
                case "priority":
                    return true;
                case "prefix":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "eduLevelAndIndexNotation":
                    return String.class;
                case "eduProgramSubjectType":
                    return String.class;
                case "generation":
                    return EduProgramSubjectIndexGeneration.class;
                case "programKind":
                    return EduProgramKind.class;
                case "priority":
                    return Integer.class;
                case "prefix":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgramSubjectIndex> _dslPath = new Path<EduProgramSubjectIndex>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramSubjectIndex");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Обозначение уровня образ. и перечня. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex#getEduLevelAndIndexNotation()
     */
    public static PropertyPath<String> eduLevelAndIndexNotation()
    {
        return _dslPath.eduLevelAndIndexNotation();
    }

    /**
     * @return Тип направления для перечня. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex#getEduProgramSubjectType()
     */
    public static PropertyPath<String> eduProgramSubjectType()
    {
        return _dslPath.eduProgramSubjectType();
    }

    /**
     * @return Поколение перечня. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex#getGeneration()
     */
    public static EduProgramSubjectIndexGeneration.Path<EduProgramSubjectIndexGeneration> generation()
    {
        return _dslPath.generation();
    }

    /**
     * Определяет вид ОП, для которого справедлив данный перечень
     *
     * @return Вид образовательной программы. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex#getProgramKind()
     */
    public static EduProgramKind.Path<EduProgramKind> programKind()
    {
        return _dslPath.programKind();
    }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Префикс для индекса УП.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex#getPrefix()
     */
    public static PropertyPath<String> prefix()
    {
        return _dslPath.prefix();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EduProgramSubjectIndex> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _eduLevelAndIndexNotation;
        private PropertyPath<String> _eduProgramSubjectType;
        private EduProgramSubjectIndexGeneration.Path<EduProgramSubjectIndexGeneration> _generation;
        private EduProgramKind.Path<EduProgramKind> _programKind;
        private PropertyPath<Integer> _priority;
        private PropertyPath<String> _prefix;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EduProgramSubjectIndexGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EduProgramSubjectIndexGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Обозначение уровня образ. и перечня. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex#getEduLevelAndIndexNotation()
     */
        public PropertyPath<String> eduLevelAndIndexNotation()
        {
            if(_eduLevelAndIndexNotation == null )
                _eduLevelAndIndexNotation = new PropertyPath<String>(EduProgramSubjectIndexGen.P_EDU_LEVEL_AND_INDEX_NOTATION, this);
            return _eduLevelAndIndexNotation;
        }

    /**
     * @return Тип направления для перечня. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex#getEduProgramSubjectType()
     */
        public PropertyPath<String> eduProgramSubjectType()
        {
            if(_eduProgramSubjectType == null )
                _eduProgramSubjectType = new PropertyPath<String>(EduProgramSubjectIndexGen.P_EDU_PROGRAM_SUBJECT_TYPE, this);
            return _eduProgramSubjectType;
        }

    /**
     * @return Поколение перечня. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex#getGeneration()
     */
        public EduProgramSubjectIndexGeneration.Path<EduProgramSubjectIndexGeneration> generation()
        {
            if(_generation == null )
                _generation = new EduProgramSubjectIndexGeneration.Path<EduProgramSubjectIndexGeneration>(L_GENERATION, this);
            return _generation;
        }

    /**
     * Определяет вид ОП, для которого справедлив данный перечень
     *
     * @return Вид образовательной программы. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex#getProgramKind()
     */
        public EduProgramKind.Path<EduProgramKind> programKind()
        {
            if(_programKind == null )
                _programKind = new EduProgramKind.Path<EduProgramKind>(L_PROGRAM_KIND, this);
            return _programKind;
        }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EduProgramSubjectIndexGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Префикс для индекса УП.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex#getPrefix()
     */
        public PropertyPath<String> prefix()
        {
            if(_prefix == null )
                _prefix = new PropertyPath<String>(EduProgramSubjectIndexGen.P_PREFIX, this);
            return _prefix;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EduProgramSubjectIndexGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EduProgramSubjectIndex.class;
        }

        public String getEntityName()
        {
            return "eduProgramSubjectIndex";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
