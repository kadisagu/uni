package ru.tandemservice.uniedu.catalog.entity.subjects;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.uniedu.catalog.entity.subjects.gen.*;

/** @see ru.tandemservice.uniedu.catalog.entity.subjects.gen.EduProgramSubjectIndexGenerationGen */
public class EduProgramSubjectIndexGeneration extends EduProgramSubjectIndexGenerationGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{
}