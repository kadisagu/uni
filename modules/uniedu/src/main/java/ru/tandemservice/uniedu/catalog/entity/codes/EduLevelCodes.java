package ru.tandemservice.uniedu.catalog.entity.codes;

import java.util.*;

/**
 * Константы кодов сущности "Уровень (вид, подвид) образования"
 * Имя сущности : eduLevel
 * Файл data.xml : uniedu.basic.data.xml
 */
public interface EduLevelCodes
{
    /** Константа кода(code) элемента : Общее образование(title) */
    String OBTSHEE_OBRAZOVANIE = "2013.1";
    /** Константа кода(code) элемента : Дошкольное образование(title) */
    String DOSHKOLNOE_OBRAZOVANIE = "2013.1.1";
    /** Константа кода(code) элемента : Начальное общее образование(title) */
    String NACHALNOE_OBTSHEE_OBRAZOVANIE = "2013.1.2";
    /** Константа кода(code) элемента : Основное общее образование(title) */
    String OSNOVNOE_OBTSHEE_OBRAZOVANIE = "2013.1.3";
    /** Константа кода(code) элемента : Среднее общее образование(title) */
    String SREDNEE_OBTSHEE_OBRAZOVANIE = "2013.1.4";
    /** Константа кода(code) элемента : Профессиональное образование(title) */
    String PROFESSIONALNOE_OBRAZOVANIE = "2013.2";
    /** Константа кода(code) элемента : Среднее профессиональное образование(title) */
    String SREDNEE_PROFESSIONALNOE_OBRAZOVANIE = "2013.2.1";
    /** Константа кода(code) элемента : Высшее образование - бакалавриат(title) */
    String VYSSHEE_OBRAZOVANIE_BAKALAVRIAT = "2013.2.2";
    /** Константа кода(code) элемента : Высшее образование - специалитет, магистратура(title) */
    String VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA = "2013.2.3";
    /** Константа кода(code) элемента : Высшее образование - подготовка кадров высшей квалификации(title) */
    String VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII = "2013.2.4";
    /** Константа кода(code) элемента : Дополнительное образование(title) */
    String DOPOLNITELNOE_OBRAZOVANIE = "2013.3";
    /** Константа кода(code) элемента : Дополнительное образование детей и взрослых(title) */
    String DOPOLNITELNOE_OBRAZOVANIE_DETEY_I_VZROSLYH = "2013.3.1";
    /** Константа кода(code) элемента : Дополнительное профессиональное образование(title) */
    String DOPOLNITELNOE_PROFESSIONALNOE_OBRAZOVANIE = "2013.3.2";
    /** Константа кода(code) элемента : Профессиональное обучение(title) */
    String PROFESSIONALNOE_OBUCHENIE = "2013.4";

    final static Set<String> CODES = Collections.unmodifiableSet(new LinkedHashSet<String>(Arrays.asList(OBTSHEE_OBRAZOVANIE, DOSHKOLNOE_OBRAZOVANIE, NACHALNOE_OBTSHEE_OBRAZOVANIE, OSNOVNOE_OBTSHEE_OBRAZOVANIE, SREDNEE_OBTSHEE_OBRAZOVANIE, PROFESSIONALNOE_OBRAZOVANIE, SREDNEE_PROFESSIONALNOE_OBRAZOVANIE, VYSSHEE_OBRAZOVANIE_BAKALAVRIAT, VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA, VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII, DOPOLNITELNOE_OBRAZOVANIE, DOPOLNITELNOE_OBRAZOVANIE_DETEY_I_VZROSLYH, DOPOLNITELNOE_PROFESSIONALNOE_OBRAZOVANIE, PROFESSIONALNOE_OBUCHENIE)));
}
