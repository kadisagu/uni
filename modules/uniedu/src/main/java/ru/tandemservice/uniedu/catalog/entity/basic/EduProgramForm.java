package ru.tandemservice.uniedu.catalog.entity.basic;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.gen.EduProgramFormGen;

/** @see EduProgramFormGen */
public class EduProgramForm extends EduProgramFormGen
{
	public static EntityComboDataSourceHandler defaultSelectDSHandler(String name)
	{
		return new EntityComboDataSourceHandler(name, EduProgramForm.class)
				.titleProperty(EduProgramForm.P_TITLE)
				.filter(EduProgramForm.title())
				.order(EduProgramForm.code());
	}
}