/* $Id:$ */
package ru.tandemservice.uniedu.program.bo.EduProgram.ui.ChangeParams;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.EduProgramHigherProfManager;
import ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.EduProgramSecondaryProfManager;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;

/**
 * @author oleyba
 * @since 7/2/15
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "programId")
})
public class EduProgramChangeParamsUI extends UIPresenter
{
    private Long programId;
    private EduProgram program;

    private boolean secondary;

    private EduLevel _baseLevel;
    private EduProgramDuration _duration;
    private EduProgramTrait _eduProgramTrait;

    @Override
    public void onComponentRefresh()
    {
        IEntity e = DataAccessServices.dao().get(getProgramId());
        setProgram((EduProgram) e);
        setSecondary(e instanceof EduProgramSecondaryProf);
    }

    public void onClickApply() {
        if (getProgram() instanceof EduProgramSecondaryProf) {
            EduProgramSecondaryProf secondaryProf = (EduProgramSecondaryProf) getProgram();
            if (null != getBaseLevel()) {
                secondaryProf.setBaseLevel(getBaseLevel());
            }
            if (null != getDuration()) {
                secondaryProf.setDuration(getDuration());
            }
            secondaryProf.setEduProgramTrait(getEduProgramTrait());
            EduProgramSecondaryProfManager.instance().dao().saveOrUpdateEduProgramSecondaryProf((EduProgramSecondaryProf) getProgram());
        } else if (getProgram() instanceof EduProgramHigherProf) {
            EduProgramHigherProfManager.instance().dao().updateParams((EduProgramHigherProf) getProgram(), getBaseLevel(), getEduProgramTrait(), getDuration());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (null == getProgram()) return;
        dataSource.put(EduProgramHigherProfManager.EDU_PROGRAM_KIND, getProgram().getKind());
    }

    // getters and setters

    public Long getProgramId()
    {
        return programId;
    }

    public void setProgramId(Long programId)
    {
        this.programId = programId;
    }

    public EduProgram getProgram()
    {
        return program;
    }

    public void setProgram(EduProgram program)
    {
        this.program = program;
    }

    public boolean isSecondary()
    {
        return secondary;
    }

    public void setSecondary(boolean secondary)
    {
        this.secondary = secondary;
    }

    public EduLevel getBaseLevel()
    {
        return _baseLevel;
    }

    public void setBaseLevel(EduLevel baseLevel)
    {
        _baseLevel = baseLevel;
    }

    public EduProgramDuration getDuration()
    {
        return _duration;
    }

    public void setDuration(EduProgramDuration duration)
    {
        _duration = duration;
    }

    public EduProgramTrait getEduProgramTrait()
    {
        return _eduProgramTrait;
    }

    public void setEduProgramTrait(EduProgramTrait eduProgramTrait)
    {
        _eduProgramTrait = eduProgramTrait;
    }
}