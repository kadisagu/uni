package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StuExtractToComissionMember;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные о члене комиссии для выписки о пересдаче дисциплины учебного плана
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StuExtractToComissionMemberGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.StuExtractToComissionMember";
    public static final String ENTITY_NAME = "stuExtractToComissionMember";
    public static final int VERSION_HASH = 1704470283;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String P_PRIORITY = "priority";
    public static final String P_FIO = "fio";
    public static final String P_POST = "post";

    private ModularStudentExtract _extract;     // Выписка из сборного приказа
    private int _priority;     // Порядок следования в тексте
    private String _fio;     // ФИО члена комиссии
    private String _post;     // Должность члена комиссии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа. Свойство не может быть null.
     */
    @NotNull
    public ModularStudentExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка из сборного приказа. Свойство не может быть null.
     */
    public void setExtract(ModularStudentExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Порядок следования в тексте. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Порядок следования в тексте. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return ФИО члена комиссии. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFio()
    {
        return _fio;
    }

    /**
     * @param fio ФИО члена комиссии. Свойство не может быть null.
     */
    public void setFio(String fio)
    {
        dirty(_fio, fio);
        _fio = fio;
    }

    /**
     * @return Должность члена комиссии. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPost()
    {
        return _post;
    }

    /**
     * @param post Должность члена комиссии. Свойство не может быть null.
     */
    public void setPost(String post)
    {
        dirty(_post, post);
        _post = post;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StuExtractToComissionMemberGen)
        {
            setExtract(((StuExtractToComissionMember)another).getExtract());
            setPriority(((StuExtractToComissionMember)another).getPriority());
            setFio(((StuExtractToComissionMember)another).getFio());
            setPost(((StuExtractToComissionMember)another).getPost());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StuExtractToComissionMemberGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StuExtractToComissionMember.class;
        }

        public T newInstance()
        {
            return (T) new StuExtractToComissionMember();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "priority":
                    return obj.getPriority();
                case "fio":
                    return obj.getFio();
                case "post":
                    return obj.getPost();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((ModularStudentExtract) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "fio":
                    obj.setFio((String) value);
                    return;
                case "post":
                    obj.setPost((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "priority":
                        return true;
                case "fio":
                        return true;
                case "post":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "priority":
                    return true;
                case "fio":
                    return true;
                case "post":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return ModularStudentExtract.class;
                case "priority":
                    return Integer.class;
                case "fio":
                    return String.class;
                case "post":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StuExtractToComissionMember> _dslPath = new Path<StuExtractToComissionMember>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StuExtractToComissionMember");
    }
            

    /**
     * @return Выписка из сборного приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToComissionMember#getExtract()
     */
    public static ModularStudentExtract.Path<ModularStudentExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Порядок следования в тексте. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToComissionMember#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return ФИО члена комиссии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToComissionMember#getFio()
     */
    public static PropertyPath<String> fio()
    {
        return _dslPath.fio();
    }

    /**
     * @return Должность члена комиссии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToComissionMember#getPost()
     */
    public static PropertyPath<String> post()
    {
        return _dslPath.post();
    }

    public static class Path<E extends StuExtractToComissionMember> extends EntityPath<E>
    {
        private ModularStudentExtract.Path<ModularStudentExtract> _extract;
        private PropertyPath<Integer> _priority;
        private PropertyPath<String> _fio;
        private PropertyPath<String> _post;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToComissionMember#getExtract()
     */
        public ModularStudentExtract.Path<ModularStudentExtract> extract()
        {
            if(_extract == null )
                _extract = new ModularStudentExtract.Path<ModularStudentExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Порядок следования в тексте. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToComissionMember#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(StuExtractToComissionMemberGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return ФИО члена комиссии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToComissionMember#getFio()
     */
        public PropertyPath<String> fio()
        {
            if(_fio == null )
                _fio = new PropertyPath<String>(StuExtractToComissionMemberGen.P_FIO, this);
            return _fio;
        }

    /**
     * @return Должность члена комиссии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToComissionMember#getPost()
     */
        public PropertyPath<String> post()
        {
            if(_post == null )
                _post = new PropertyPath<String>(StuExtractToComissionMemberGen.P_POST, this);
            return _post;
        }

        public Class getEntityClass()
        {
            return StuExtractToComissionMember.class;
        }

        public String getEntityName()
        {
            return "stuExtractToComissionMember";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
