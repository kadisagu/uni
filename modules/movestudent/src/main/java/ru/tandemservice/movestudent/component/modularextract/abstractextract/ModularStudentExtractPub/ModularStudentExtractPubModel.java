/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.ICustomOrderExtract;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.component.visa.VisaList.IVisaOwnerModel;

import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 18.11.2008
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "extractId"),
        @Bind(key = "selectedTab", binding = "selectedTab")
})
public abstract class ModularStudentExtractPubModel<T extends ModularStudentExtract> implements IVisaOwnerModel
{
    private Long _extractId;
    private T _extract;
    private boolean _debugMode;
    private String _selectedTab;
    private String _attributesPage;
    private String _additionalDataPage;
    private String _extractProjectTitle;
    private Long _visingStatus;
    private CommonPostfixPermissionModel _secModel;
    private Map<String, Object> _visaListParameters = ParametersMap.createWith("visaOwnerModel", this);
    private boolean _individualOrder;
    private IAbstractDocument _visingDocument;
    private boolean _changesNothing = false;

    public boolean isNeedVising()
    {
        return _visingStatus != null;
    }

    // IVisaOwnerModel

    @Override
    public IAbstractDocument getDocument()
    {
        return getVisingDocument();
    }

    @Override
    public boolean isVisaListModificable()
    {
        if (getDocument() instanceof StudentModularOrder)
            return UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(_extract.getParagraph().getOrder().getState().getCode());
        else
            return UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE.equals(_extract.getState().getCode());
    }

    public String getContextEntityTitleGenitive()
    {
        if(isIndividualOrder()) return "индивидуального приказа";
        return "проекта приказа";
    }

    public boolean isBookNumberVisible()
    {
        final T extract = getExtract();
        return extract instanceof CommonStuExtract && ((CommonStuExtract) extract).getBookNumberNew() != null;
    }

    public boolean isTargetAdmissionVisible()
    {
        final T extract = getExtract();
        return extract instanceof CommonStuExtract && ((CommonStuExtract) extract).getTargetAdmissionNew() != null;
    }

    public String getStudentBlockTitle()
    {
        return "Данные студента на момент создания " + getContextEntityTitleGenitive();
    }

    @Override
    public String getSecPostfix()
    {
        return "modularExtract";
    }

    public boolean isHasAdditionalDataBlock()
    {
        return null != _additionalDataPage;
    }

    public boolean isEditable()
    {
        if (!isIndividualOrder() || null == getExtract().getParagraph())
            return UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE.equals(getExtract().getState().getCode());
        else
            return UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(getExtract().getParagraph().getOrder().getState().getCode());
    }

    public boolean isOrderAttrsEditDisabled()
    {
        return null == getExtract().getParagraph();
    }

    public String getDelMessage()
    {
        if (isIndividualOrder()) return "Удалить приказ?";
        else if (null != getExtract().getParagraph()) return "Удалить выписку?";
        else return "Удалить проект приказа?";
    }

    public boolean isThereAnyChanges()
    {
        return !isChangesNothing();
    }

    @SuppressWarnings("unchecked")
    public IModularStudentExtractPubControllerExtension getLastExtension()
    {
        List<IModularStudentExtractPubControllerExtension> extensionsList = (List<IModularStudentExtractPubControllerExtension>) ApplicationRuntime.getBean(IModularStudentExtractPubControllerExtension.MODULAR_STUDENT_EXTRACT_PUB_CONTROLLER_EXTENSIONS_BEAN_NAME);
        if (null != extensionsList && !extensionsList.isEmpty())
        {
            return extensionsList.get(extensionsList.size() - 1);
        }
        return null;
    }

    // Getters & Setters

    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        _extractId = extractId;
    }

    public T getExtract()
    {
        return _extract;
    }

    public void setExtract(T extract)
    {
        _extract = extract;
    }

    public boolean isDebugMode()
    {
        return _debugMode;
    }

    public void setDebugMode(boolean debugMode)
    {
        _debugMode = debugMode;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public String getAttributesPage()
    {
        return _attributesPage;
    }

    public void setAttributesPage(String attributesPage)
    {
        _attributesPage = attributesPage;
    }

    public String getAdditionalDataPage()
    {
        return _additionalDataPage;
    }

    public void setAdditionalDataPage(String additionalDataPage)
    {
        _additionalDataPage = additionalDataPage;
    }

    public String getExtractProjectTitle()
    {
        return _extractProjectTitle;
    }

    public void setExtractProjectTitle(String extractProjectTitle)
    {
        _extractProjectTitle = extractProjectTitle;
    }

    public Long getVisingStatus()
    {
        return _visingStatus;
    }

    public void setVisingStatus(Long visingStatus)
    {
        _visingStatus = visingStatus;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public Map<String, Object> getVisaListParameters()
    {
        return _visaListParameters;
    }

    public void setVisaListParameters(Map<String, Object> visaListParameters)
    {
        _visaListParameters = visaListParameters;
    }

    public boolean isIndividualOrder()
    {
        return _individualOrder;
    }

    public void setIndividualOrder(boolean individualOrder)
    {
        _individualOrder = individualOrder;
    }

    public IAbstractDocument getVisingDocument()
    {
        return _visingDocument;
    }

    public void setVisingDocument(IAbstractDocument visingDocument)
    {
        _visingDocument = visingDocument;
    }

    public boolean isChangesNothing()
    {
        return _changesNothing;
    }

    public void setChangesNothing(boolean changesNothing)
    {
        _changesNothing = changesNothing;
    }

    public boolean isCustom()
    {
        return getExtract() instanceof ICustomOrderExtract;
    }
}