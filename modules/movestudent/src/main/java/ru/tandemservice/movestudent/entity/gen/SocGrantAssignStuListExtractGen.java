package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О назначении социальной стипендии»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SocGrantAssignStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract";
    public static final String ENTITY_NAME = "socGrantAssignStuListExtract";
    public static final int VERSION_HASH = 1146698818;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_GRANT_SIZE = "grantSize";
    public static final String P_PREV_BEGIN_DATE = "prevBeginDate";
    public static final String P_PREV_END_DATE = "prevEndDate";

    private Course _course;     // Курс
    private Group _group;     // Группа
    private CompensationType _compensationType;     // Вид возмещения затрат
    private Date _beginDate;     // Дата начала выплаты стипендии
    private Date _endDate;     // Дата окончания выплаты стипендии
    private double _grantSize;     // Размер стипендии
    private Date _prevBeginDate;     // Дата начала выплаты социальной стипендии по предыдущему приказу
    private Date _prevEndDate;     // Дата окончания выплаты социальной стипендии по предыдущему приказу

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Дата начала выплаты стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала выплаты стипендии. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания выплаты стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания выплаты стипендии. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Размер стипендии. Свойство не может быть null.
     */
    @NotNull
    public double getGrantSize()
    {
        return _grantSize;
    }

    /**
     * @param grantSize Размер стипендии. Свойство не может быть null.
     */
    public void setGrantSize(double grantSize)
    {
        dirty(_grantSize, grantSize);
        _grantSize = grantSize;
    }

    /**
     * @return Дата начала выплаты социальной стипендии по предыдущему приказу.
     */
    public Date getPrevBeginDate()
    {
        return _prevBeginDate;
    }

    /**
     * @param prevBeginDate Дата начала выплаты социальной стипендии по предыдущему приказу.
     */
    public void setPrevBeginDate(Date prevBeginDate)
    {
        dirty(_prevBeginDate, prevBeginDate);
        _prevBeginDate = prevBeginDate;
    }

    /**
     * @return Дата окончания выплаты социальной стипендии по предыдущему приказу.
     */
    public Date getPrevEndDate()
    {
        return _prevEndDate;
    }

    /**
     * @param prevEndDate Дата окончания выплаты социальной стипендии по предыдущему приказу.
     */
    public void setPrevEndDate(Date prevEndDate)
    {
        dirty(_prevEndDate, prevEndDate);
        _prevEndDate = prevEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SocGrantAssignStuListExtractGen)
        {
            setCourse(((SocGrantAssignStuListExtract)another).getCourse());
            setGroup(((SocGrantAssignStuListExtract)another).getGroup());
            setCompensationType(((SocGrantAssignStuListExtract)another).getCompensationType());
            setBeginDate(((SocGrantAssignStuListExtract)another).getBeginDate());
            setEndDate(((SocGrantAssignStuListExtract)another).getEndDate());
            setGrantSize(((SocGrantAssignStuListExtract)another).getGrantSize());
            setPrevBeginDate(((SocGrantAssignStuListExtract)another).getPrevBeginDate());
            setPrevEndDate(((SocGrantAssignStuListExtract)another).getPrevEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SocGrantAssignStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SocGrantAssignStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new SocGrantAssignStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "compensationType":
                    return obj.getCompensationType();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "grantSize":
                    return obj.getGrantSize();
                case "prevBeginDate":
                    return obj.getPrevBeginDate();
                case "prevEndDate":
                    return obj.getPrevEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "grantSize":
                    obj.setGrantSize((Double) value);
                    return;
                case "prevBeginDate":
                    obj.setPrevBeginDate((Date) value);
                    return;
                case "prevEndDate":
                    obj.setPrevEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "group":
                        return true;
                case "compensationType":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "grantSize":
                        return true;
                case "prevBeginDate":
                        return true;
                case "prevEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "group":
                    return true;
                case "compensationType":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "grantSize":
                    return true;
                case "prevBeginDate":
                    return true;
                case "prevEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "compensationType":
                    return CompensationType.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "grantSize":
                    return Double.class;
                case "prevBeginDate":
                    return Date.class;
                case "prevEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SocGrantAssignStuListExtract> _dslPath = new Path<SocGrantAssignStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SocGrantAssignStuListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Дата начала выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Размер стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract#getGrantSize()
     */
    public static PropertyPath<Double> grantSize()
    {
        return _dslPath.grantSize();
    }

    /**
     * @return Дата начала выплаты социальной стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract#getPrevBeginDate()
     */
    public static PropertyPath<Date> prevBeginDate()
    {
        return _dslPath.prevBeginDate();
    }

    /**
     * @return Дата окончания выплаты социальной стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract#getPrevEndDate()
     */
    public static PropertyPath<Date> prevEndDate()
    {
        return _dslPath.prevEndDate();
    }

    public static class Path<E extends SocGrantAssignStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Double> _grantSize;
        private PropertyPath<Date> _prevBeginDate;
        private PropertyPath<Date> _prevEndDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Дата начала выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(SocGrantAssignStuListExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(SocGrantAssignStuListExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Размер стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract#getGrantSize()
     */
        public PropertyPath<Double> grantSize()
        {
            if(_grantSize == null )
                _grantSize = new PropertyPath<Double>(SocGrantAssignStuListExtractGen.P_GRANT_SIZE, this);
            return _grantSize;
        }

    /**
     * @return Дата начала выплаты социальной стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract#getPrevBeginDate()
     */
        public PropertyPath<Date> prevBeginDate()
        {
            if(_prevBeginDate == null )
                _prevBeginDate = new PropertyPath<Date>(SocGrantAssignStuListExtractGen.P_PREV_BEGIN_DATE, this);
            return _prevBeginDate;
        }

    /**
     * @return Дата окончания выплаты социальной стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract#getPrevEndDate()
     */
        public PropertyPath<Date> prevEndDate()
        {
            if(_prevEndDate == null )
                _prevEndDate = new PropertyPath<Date>(SocGrantAssignStuListExtractGen.P_PREV_END_DATE, this);
            return _prevEndDate;
        }

        public Class getEntityClass()
        {
            return SocGrantAssignStuListExtract.class;
        }

        public String getEntityName()
        {
            return "socGrantAssignStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
