/* $Id$ */
package ru.tandemservice.movestudent.component.order.other.StudentOtherOrderAddEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Dmitry Seleznev
 * @since 11.04.2013
 */
public interface IDAO extends IUniDao<Model>
{
    void initFieldSettings(Model model);
}