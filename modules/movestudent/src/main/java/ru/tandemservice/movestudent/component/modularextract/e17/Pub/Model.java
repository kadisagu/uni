/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e17.Pub;

import java.util.List;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.movestudent.entity.RestorationStuExtractExt;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 04.05.2009
 */
public class Model extends ModularStudentExtractPubModel<RestorationStuExtractExt>
{
    private StudentStatus _studentStatusNew;
    
    private List<StuExtractToDebtRelation> _debtsList;

    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        _studentStatusNew = studentStatusNew;
    }

    public List<StuExtractToDebtRelation> getDebtsList()
    {
        return _debtsList;
    }

    public void setDebtsList(List<StuExtractToDebtRelation> debtsList)
    {
        this._debtsList = debtsList;
    }
    
    public String getDebtsListStr()
    {
        StringBuilder builder = new StringBuilder();
        for (StuExtractToDebtRelation rel : getDebtsList())
        {
            builder.append(builder.length() > 0 ? ",</li>" : "<ol> ").append("<li type=\"1\">");
            builder.append(rel.getDiscipline()).append(" (").append(rel.getHours()).append(" ч.) - ");
            builder.append(rel.getControlAction());
        }
        builder.append(builder.length() > 0 ? "</li> </ol>" : "");
        return builder.toString();
    }
}