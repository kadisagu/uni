/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e34;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.e34.utils.FinAidAssignExtractWrapper;
import ru.tandemservice.movestudent.component.listextract.e34.utils.FinAidAssignParagraphPartWrapper;
import ru.tandemservice.movestudent.component.listextract.e34.utils.FinAidAssignParagraphWrapper;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.e86.utils.EmployeePostDecl;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 03.10.2012
 */
public class FinAidAssignStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<FinAidAssignStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
	    FinAidAssignStuListExtract extract = extracts.get(0);
	    List<FinAidAssignParagraphWrapper> paragraphWrapperList = prepareParagraphsStructure(extracts);
	    injectModifier.put("protocol", extract.getProtocolNumber());
	    injectModifier.put("responsibleForPayments", StringUtils.capitalize(EmployeePostDecl.getEmployeeStrDat(extract.getResponsibleForPayments())));
	    injectModifier.put("responsibleForAgreement", EmployeePostDecl.getEmployeeStrInst(extract.getResponsibleForAgreement()));
	    injectModifier.put("matchingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getMatchingDate()));
	    int num = paragraphWrapperList.size();
	    injectModifier.put("numberPrevEnd",String.valueOf(++num));
	    injectModifier.put("numberEnd",String.valueOf(++num));
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, paragraphWrapperList);

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }

    protected List<FinAidAssignParagraphWrapper> prepareParagraphsStructure(List<FinAidAssignStuListExtract> extracts)
    {
        List<FinAidAssignParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        int ind;
        for (FinAidAssignStuListExtract extract : extracts)
        {
            Student student = extract.getEntity();
            Person person = student.getPerson();

            Group group = student.getGroup();
            EducationOrgUnit educationOrgUnit = group.getEducationOrgUnit();
	        OrgUnit orgUnit = student.getEducationOrgUnit().getFormativeOrgUnit();

            FinAidAssignParagraphWrapper paragraphWrapper = new FinAidAssignParagraphWrapper(extract.getCompensationType(), educationOrgUnit.getDevelopForm(), educationOrgUnit.getDevelopCondition(), educationOrgUnit.getDevelopTech(), educationOrgUnit.getDevelopPeriod(), extract.getMonth(), extract.getYear(), CommonListOrderPrint.getEducationBaseText(group), extract);

            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else
                paragraphWrapper = paragraphWrapperList.get(ind);

            FinAidAssignParagraphPartWrapper paragraphPartWrapper = new FinAidAssignParagraphPartWrapper(orgUnit, group, extract);

            ind = paragraphWrapper.getParagraphPartWrapperList().indexOf(paragraphPartWrapper);
            if (ind == -1)
                paragraphWrapper.getParagraphPartWrapperList().add(paragraphPartWrapper);
            else
                paragraphPartWrapper = paragraphWrapper.getParagraphPartWrapperList().get(ind);

            paragraphPartWrapper.getExtractWrapperList().add(new FinAidAssignExtractWrapper(person, extract.getFinAidSize(), group));
        }

        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<FinAidAssignParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            int parNumber = 0;
            for (FinAidAssignParagraphWrapper paragraphWrapper: paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FIN_AID_ASSIGN_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphWrapper.getFirstExtract();
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(firstExtract.getParagraph(), firstExtract);

                CommonExtractPrint.initDevelopForm(paragraphInjectModifier, paragraphWrapper.getDevelopForm(), "");

                CompensationType compensationType = paragraphWrapper.getCompensationType();

                if (compensationType != null)
                {
                    CommonListExtractPrint.injectCompensationType(paragraphInjectModifier, compensationType, "", false);
	                if(compensationType.isBudget())
	                {
		                // из стипендиального фонда
		                paragraphInjectModifier.put("paymentSource", "из стипендиального фонда");
	                }
	                else
	                {
		                //из внебюджетных средств школы
		                paragraphInjectModifier.put("paymentSource", "из внебюджетных средств школы");
	                }

                } else
                {
                    paragraphInjectModifier.put("fefuCompensationTypeStr", "");
                    paragraphInjectModifier.put("compensationTypeStr", "");
	                paragraphInjectModifier.put("paymentSource", "");
                }

                paragraphInjectModifier.put("month_P", CommonBaseDateUtil.getMonthNameDeclined(paragraphWrapper.getMonth(), GrammaCase.PREPOSITIONAL));
                paragraphInjectModifier.put("year", String.valueOf(paragraphWrapper.getYear()));

                paragraphInjectModifier.put("parNumber", String.valueOf(++parNumber));
                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");
                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, paragraphWrapper.getDevelopCondition(), paragraphWrapper.getDevelopTech(), paragraphWrapper.getDevelopPeriod(), paragraphWrapper.getEduBaseText(), "fefuShortFastExtendedOptionalText");
                customModifier.listParagraphModifier(paragraphInjectModifier,(StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphInjectModifier.modify(paragraph);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getParagraphPartWrapperList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraph, List<FinAidAssignParagraphPartWrapper> paragraphPartWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartWrappers);
            List<IRtfElement> parList = new ArrayList<>();

            for (FinAidAssignParagraphPartWrapper paragraphPartWrapper: paragraphPartWrappers)
            {
                // Получаем шаблон подпараграфа
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FIN_AID_ASSIGN_LIST_EXTRACT), 3);
                RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphPartWrapper.getFirstExtract();
                RtfInjectModifier paragraphPartInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, firstExtract);

                paragraphPartInjectModifier.put("orgUnitTitle", paragraphPartWrapper.getOrgUnit().getTitle());
                customModifier.listParagraphModifier(paragraphPartInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                paragraphPartInjectModifier.modify(paragraphPart);

                RtfTableModifier paragraphPartTableModifier = new RtfTableModifier();
                List<FinAidAssignExtractWrapper> extractWrapperList = paragraphPartWrapper.getExtractWrapperList();

                Collections.sort(extractWrapperList);

                int j = 0;
                String[][] tableData = new String[extractWrapperList.size()][];
                for (FinAidAssignExtractWrapper extractWrapper: extractWrapperList)
                {
                    tableData[j++] = new String[]{String.valueOf(j) + ".", extractWrapper.getPerson().getFullFio(), extractWrapper.getGroup().getTitle(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(extractWrapper.getGrantSize()) + " руб."};
                }

                paragraphPartTableModifier.put("STUDENTS_TABLE", tableData);
                customModifier.listParagraphTableModifier(paragraphPartTableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                paragraphPartTableModifier.modify(paragraphPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}