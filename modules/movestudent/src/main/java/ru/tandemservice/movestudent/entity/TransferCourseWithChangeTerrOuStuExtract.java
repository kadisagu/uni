package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.TransferCourseWithChangeTerrOuStuExtractGen;

import java.util.Date;

/**
 * О переводе с курса на следующий курс со сменой территориального подразделения
 */
public class TransferCourseWithChangeTerrOuStuExtract extends TransferCourseWithChangeTerrOuStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}