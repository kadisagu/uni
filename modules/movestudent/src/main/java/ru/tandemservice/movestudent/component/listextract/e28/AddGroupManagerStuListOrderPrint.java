/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e28;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.e28.utils.AddGroupManagerParagraphPartWrapper;
import ru.tandemservice.movestudent.component.listextract.e28.utils.AddGroupManagerParagraphWrapper;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 14.09.2012
 */
public class AddGroupManagerStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<AddGroupManagerStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));

        injectModifier.put("educationYear", order.getEducationYear().getTitle());
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, prepareParagraphsStructure(extracts));

        RtfUtil.optimizeFontAndColorTable(document);
        return document;
    }

    protected List<AddGroupManagerParagraphWrapper> prepareParagraphsStructure(List<AddGroupManagerStuListExtract> extracts)
    {
        List<AddGroupManagerParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        int ind;
        for (AddGroupManagerStuListExtract extract: extracts)
        {
            Student student = extract.getEntity();
            Group group = student.getGroup();
            EducationYear educationYear = extract.getEducationYear();

            AddGroupManagerParagraphWrapper paragraphWrapper = new AddGroupManagerParagraphWrapper(student.getCompensationType(), group.getEducationOrgUnit(), educationYear, CommonListOrderPrint.getEducationBaseText(group), extract);
            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else paragraphWrapper = paragraphWrapperList.get(ind);

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GroupToExtractRelation.class, "ge");
            builder.column(DQLExpressions.property("ge", GroupToExtractRelation.group()));
            builder.where(DQLExpressions.eq(DQLExpressions.property("ge", GroupToExtractRelation.extract().id()), DQLExpressions.value(extract.getId())));
            List<Group> selectedGroups = DataAccessServices.dao().getList(builder);

            if (selectedGroups.isEmpty()) throw new ApplicationException("Невозможно сгенерировать печатную форму приказа, поскольку не указана группа, в которую назначается старостой " + student.getFio() + ".");
            for(Group selectedGroup : selectedGroups)
            {
                AddGroupManagerParagraphPartWrapper paragraphPartWrapper = new AddGroupManagerParagraphPartWrapper(selectedGroup, extract);
                ind = paragraphWrapper.getParagraphPartWrapperList().indexOf(paragraphPartWrapper);
                if (ind == -1)
                {
                    paragraphPartWrapper.setStudent(student);
                    paragraphWrapper.getParagraphPartWrapperList().add(paragraphPartWrapper);
                }
            }
        }

        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<AddGroupManagerParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            int parNumber = 0;
            for (AddGroupManagerParagraphWrapper paragraphWrapper: paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.ADD_GROUP_MANAGER_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphWrapper.getFirstExtract();
                RtfInjectModifier paragraphInjectModifier = new RtfInjectModifier();

                String code = paragraphWrapper.getEducationOrgUnit().getDevelopForm().getCode();
                switch (code)
                {
                    case DevelopFormCodes.FULL_TIME_FORM:
                        paragraphInjectModifier.put("developForm_DF", "очной");
                        break;
                    case DevelopFormCodes.CORESP_FORM:
                        paragraphInjectModifier.put("developForm_DF", "заочной");
                        break;
                    case DevelopFormCodes.PART_TIME_FORM:
                        paragraphInjectModifier.put("developForm_DF", "очно-заочной");
                        break;
                    case DevelopFormCodes.EXTERNAL_FORM:
                        paragraphInjectModifier.put("developForm_DF", "экстернату");
                        break;
                    case DevelopFormCodes.APPLICANT_FORM:
                        paragraphInjectModifier.put("developForm_DF", "самостоятельному обучению и итоговой аттестации");
                        break;
                }

                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, paragraphWrapper.getEducationOrgUnit().getFormativeOrgUnit(), "", "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, paragraphWrapper.getEducationOrgUnit(), "formativeOrgUnitStr", "");
                CommonListExtractPrint.injectCompensationType(paragraphInjectModifier, paragraphWrapper.getCompensationType(), "", false);

                EducationLevels eduLevel = paragraphWrapper.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, eduLevel);

                EducationYear educationYear = paragraphWrapper.getEducationYear();
                String educationYearStr = educationYear != null ? educationYear.getTitle() : "20__/20__";
                paragraphInjectModifier.put("educationYear", educationYearStr);
                paragraphInjectModifier.put("parNumber", String.valueOf(++parNumber));
                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");
                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, paragraphWrapper.getEducationOrgUnit(), paragraphWrapper.getEduBaseText(), "fefuShortFastExtendedOptionalText");
                CommonExtractPrint.initProgramSubjectLabels(paragraphInjectModifier, eduLevel.getEduProgramSubject(), eduLevel.getEduProgramSpecialization(), "");
                customModifier.listParagraphModifier(paragraphInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphInjectModifier.modify(paragraph);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getParagraphPartWrapperList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraph, List<AddGroupManagerParagraphPartWrapper> paragraphPartWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (AddGroupManagerParagraphPartWrapper paragraphPartWrapper: paragraphPartWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.ADD_GROUP_MANAGER_LIST_EXTRACT), 3);
                RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphPartWrapper.getFirstExtract();
                RtfInjectModifier paragraphPartInjectModifier = new RtfInjectModifier();

                paragraphPartInjectModifier.put("course", paragraphPartWrapper.getGroup().getCourse().getTitle());
                paragraphPartInjectModifier.put("targetGroup", paragraphPartWrapper.getGroup().getTitle());

                Student student = paragraphPartWrapper.getStudent();

                paragraphPartInjectModifier.put("STUDENT_LIST", student.getPerson().getFullFio());
                customModifier.listParagraphModifier(paragraphPartInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphPartInjectModifier.modify(paragraphPart);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraphPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}