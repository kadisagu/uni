package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.StudentCustomStateToExtractRelation;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дополнительный статус
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentCustomStateToExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.StudentCustomStateToExtractRelation";
    public static final String ENTITY_NAME = "studentCustomStateToExtractRelation";
    public static final int VERSION_HASH = 1141509246;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_CUSTOM_STATE = "studentCustomState";
    public static final String L_EXTRACT = "extract";

    private StudentCustomState _studentCustomState;     // Дополнительный статус студента
    private AbstractStudentExtract _extract;     // Выписка (приказ по студентам)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дополнительный статус студента. Свойство не может быть null.
     */
    @NotNull
    public StudentCustomState getStudentCustomState()
    {
        return _studentCustomState;
    }

    /**
     * @param studentCustomState Дополнительный статус студента. Свойство не может быть null.
     */
    public void setStudentCustomState(StudentCustomState studentCustomState)
    {
        dirty(_studentCustomState, studentCustomState);
        _studentCustomState = studentCustomState;
    }

    /**
     * @return Выписка (приказ по студентам). Свойство не может быть null.
     */
    @NotNull
    public AbstractStudentExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка (приказ по студентам). Свойство не может быть null.
     */
    public void setExtract(AbstractStudentExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentCustomStateToExtractRelationGen)
        {
            setStudentCustomState(((StudentCustomStateToExtractRelation)another).getStudentCustomState());
            setExtract(((StudentCustomStateToExtractRelation)another).getExtract());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentCustomStateToExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentCustomStateToExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new StudentCustomStateToExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentCustomState":
                    return obj.getStudentCustomState();
                case "extract":
                    return obj.getExtract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentCustomState":
                    obj.setStudentCustomState((StudentCustomState) value);
                    return;
                case "extract":
                    obj.setExtract((AbstractStudentExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentCustomState":
                        return true;
                case "extract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentCustomState":
                    return true;
                case "extract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentCustomState":
                    return StudentCustomState.class;
                case "extract":
                    return AbstractStudentExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentCustomStateToExtractRelation> _dslPath = new Path<StudentCustomStateToExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentCustomStateToExtractRelation");
    }
            

    /**
     * @return Дополнительный статус студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentCustomStateToExtractRelation#getStudentCustomState()
     */
    public static StudentCustomState.Path<StudentCustomState> studentCustomState()
    {
        return _dslPath.studentCustomState();
    }

    /**
     * @return Выписка (приказ по студентам). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentCustomStateToExtractRelation#getExtract()
     */
    public static AbstractStudentExtract.Path<AbstractStudentExtract> extract()
    {
        return _dslPath.extract();
    }

    public static class Path<E extends StudentCustomStateToExtractRelation> extends EntityPath<E>
    {
        private StudentCustomState.Path<StudentCustomState> _studentCustomState;
        private AbstractStudentExtract.Path<AbstractStudentExtract> _extract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дополнительный статус студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentCustomStateToExtractRelation#getStudentCustomState()
     */
        public StudentCustomState.Path<StudentCustomState> studentCustomState()
        {
            if(_studentCustomState == null )
                _studentCustomState = new StudentCustomState.Path<StudentCustomState>(L_STUDENT_CUSTOM_STATE, this);
            return _studentCustomState;
        }

    /**
     * @return Выписка (приказ по студентам). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentCustomStateToExtractRelation#getExtract()
     */
        public AbstractStudentExtract.Path<AbstractStudentExtract> extract()
        {
            if(_extract == null )
                _extract = new AbstractStudentExtract.Path<AbstractStudentExtract>(L_EXTRACT, this);
            return _extract;
        }

        public Class getEntityClass()
        {
            return StudentCustomStateToExtractRelation.class;
        }

        public String getEntityName()
        {
            return "studentCustomStateToExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
