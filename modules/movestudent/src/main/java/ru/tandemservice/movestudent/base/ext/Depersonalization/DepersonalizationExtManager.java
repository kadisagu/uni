/* $Id$ */
package ru.tandemservice.movestudent.base.ext.Depersonalization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.DepersonalizationManager;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.IDepersonalizationObject;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.ScriptItemResetToDefaultObject;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.TemplateDocumentResetToDefaultObject;
import ru.tandemservice.movestudent.base.ext.Depersonalization.objects.ChangeIdentityCardStudentExtractDO;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate;

/**
 * @author Nikolay Fedorovskih
 * @since 12.11.2014
 */
@Configuration
public class DepersonalizationExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private DepersonalizationManager _depersonalizationManager;

    @Bean
    public ItemListExtension<IDepersonalizationObject> depersonalizationObjectListExtension()
    {
        return _depersonalizationManager.extensionBuilder()
                .add(_depersonalizationManager.updateBuilder(AbstractStudentExtract.class)
                        .replaceFio(AbstractStudentExtract.P_STUDENT_TITLE)
                        .replaceFio(AbstractStudentExtract.P_STUDENT_TITLE_STR))
                .add(new TemplateDocumentResetToDefaultObject(MovestudentTemplate.class))
                .add(new ScriptItemResetToDefaultObject(MovestudentScriptItem.class))
                .add(ChangeIdentityCardStudentExtractDO.class)
                .add(_depersonalizationManager.multipleDropBuilder()
                			.title("Вспомогательные сущности, содержащие текст приказов и выписок (движение студентов)")
                            .drop(StudentExtractTextRelation.class)
							.drop(StudentOrderTextRelation.class)
							.drop(StudentOtherOrderTextRelation.class)
                            .optional(true)
							.build())
                .build();
    }
}