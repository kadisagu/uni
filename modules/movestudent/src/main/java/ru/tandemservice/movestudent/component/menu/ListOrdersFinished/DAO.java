/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.menu.ListOrdersFinished;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.commons.ExtractTypeSelectModel;
import ru.tandemservice.movestudent.component.commons.IMoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.MoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.OrderCategory;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.StudentReasonToTypeRel;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.movestudent.ui.UnimoveStudentMultiSelectModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 18.10.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderDescriptionRegistry = new OrderDescriptionRegistry(IMoveStudentMQBuilder.ORDER_ALIAS);

    static
    {
        _orderDescriptionRegistry.setOrders(Model.ORG_UNI_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.ORDER_ALIAS, new String[]{StudentListOrder.L_ORG_UNIT, OrgUnit.P_TITLE}), new OrderDescription(IMoveStudentMQBuilder.ORDER_ALIAS, new String[]{StudentListOrder.L_ORG_UNIT, OrgUnit.L_ORG_UNIT_TYPE, OrgUnitType.P_TITLE}));
    }

    @Override
    public void prepare(final Model model)
    {
        model.setEducationYearList(new EducationYearModel());

        Set<OrgUnit> set = new HashSet<>();
        set.addAll(UniDaoFacade.getOrgstructDao().getOrgUnitList(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        set.addAll(UniDaoFacade.getOrgstructDao().getOrgUnitList(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        List<OrgUnit> list = new ArrayList<>(set);
        Collections.sort(list, new EntityComparator<>(new EntityOrder(OrgUnit.P_FULL_TITLE)));
        model.setOrgUnitListModel(new LazySimpleSelectModel<>(list, OrgUnit.P_FULL_TITLE).setSearchFromStart(false));
        model.setEmployeePostModel(new OrderExecutorSelectModel());
        model.setOrderTypesListModel(new ExtractTypeSelectModel(OrderCategory.LIST).includeExistsTypesFromContext(true));

        model.setOrderReasonModel(new CommonMultiSelectModel(StudentOrderReasons.title().s())
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                List<StudentExtractType> typesList = model.getSettings().get(IMoveStudentMQBuilder.TYPE);

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(StudentReasonToTypeRel.class, "rel").column(property(StudentReasonToTypeRel.first().fromAlias("rel")))
                        .where(likeUpper(property(StudentReasonToTypeRel.first().title().fromAlias("rel")), value(CoreStringUtils.escapeLike(filter, true))));
                if (null != typesList && !typesList.isEmpty())
                    builder.where(in(property(StudentReasonToTypeRel.second().fromAlias("rel")), typesList));
                if (set != null) builder.where(in(property(StudentReasonToTypeRel.first().id().fromAlias("rel")), set));
                builder.order(property(StudentReasonToTypeRel.first().title().fromAlias("rel")));
                return new DQLListResultBuilder(builder, 50);
            }
        });


        model.setEducationLevelTypeListModel(new FullCheckSelectModel(StructureEducationLevels.P_SHORT_TITLE) {
            @SuppressWarnings("unchecked")
            @Override
            public ListResult findValues(String filter) {
                Criteria c = getSession().createCriteria(StructureEducationLevels.class, "e");

                c.add(Restrictions.isNull(StructureEducationLevels.L_PARENT));
                c.setProjection(Projections.distinct(Projections.property("e." + StructureEducationLevels.P_ID)));

                final List<Long> ids = c.list();

                if (ids.isEmpty()) {
                    return ListResult.getEmpty();
                }

                c = getSession().createCriteria(StructureEducationLevels.class);
                c.add(Restrictions.in(StructureEducationLevels.P_ID, ids));


                return new ListResult<>(c.list());
            }
        });

        model.setStudentList(new UnimoveStudentMultiSelectModel());
    }



    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(Model model)
    {
//        List<StructureEducationLevels> educationLevelsList = model.getEducationLevelTypeList();

        // init datasource
        IMoveStudentMQBuilder builder = MoveStudentMQBuilder.createOrderMQBuilder(StudentListOrder.class, model.getSettings());

        // apply user defined restrictions
        builder.getMQBuilder().add(MQExpression.eq(IMoveStudentMQBuilder.STATE_ALIAS, ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED));

        // apply filters
        builder.applyOrderEducationYear();
        builder.applyOrderCommitDate();
        builder.applyOrderCreateDate();
        builder.applyOrderNumber();
        builder.applyOrderCommitDateSystem();
        builder.applyOrderOrgUnit();
        builder.applyListOrderTypes();
        builder.applyListOrderReason();
        builder.applyOrderStudent(getSession());
        builder.applyExecutor();
        builder.applyEducationLevel(getSession(), true);


        // apply order
        _orderDescriptionRegistry.applyOrder(builder.getMQBuilder(), model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder.getMQBuilder(), getSession());

        // get statictics for view properties
        // TODO optimize in DEV-3640 - hql выжечь. Получать количество параграфов в основном запросе
        String hql = "select " + IAbstractParagraph.L_ORDER + ".id, count(*) from " + StudentListParagraph.ENTITY_CLASS + " where " + IAbstractParagraph.L_ORDER + " is not null group by " + IAbstractParagraph.L_ORDER;
        Map<Long, Integer> id2count = new HashMap<>();
        for (Object[] row : (List<Object[]>) getSession().createQuery(hql).list())
        {
            Long id = (Long) row[0];
            Number count = (Number) row[1];
            id2count.put(id, count == null ? 0 : count.intValue());
        }

        // inject view properties
        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            Integer value = id2count.get(wrapper.getId());
            wrapper.setViewProperty(AbstractStudentOrder.P_COUNT_PARAGRAPH, value == null ? 0 : value);
            boolean disabled = !CoreServices.securityService().check(SecurityRuntime.getInstance().getCommonSecurityObject(), ContextLocal.getUserContext().getPrincipalContext(), "delete_menuList_listOrdersFinished");
            wrapper.setViewProperty("deleteDisabled", disabled);
        }
    }

    @Override
    public void deleteRow(IBusinessComponent context)
    {
        IAbstractOrder order = get((Long) context.getListenerParameter());
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(order);
    }
}
