/* $Id$ */
package ru.tandemservice.movestudent.component.commons.gradation;

import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Date;

/**
 * Выписка, связанная с зачислением.
 *
 * @author Nikolay Fedorovskih
 * @since 15.12.2013
 */
public interface IEnrollmentExtract
{
    /**
     * Дата поступления (зачисления).
     */
    Date getEntranceDate();

    /**
     * Группа, в которую зачисляется студент.
     */
    Group getGroupNew();
}