package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочный приказ по студентам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentListOrderGen extends AbstractStudentOrder
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.StudentListOrder";
    public static final String ENTITY_NAME = "studentListOrder";
    public static final int VERSION_HASH = -490961991;
    private static IEntityMeta ENTITY_META;

    public static final String L_REASON = "reason";
    public static final String L_TYPE = "type";
    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String P_REASON_COMMENT = "reasonComment";

    private StudentOrderReasons _reason;     // Причина списочного приказа
    private StudentExtractType _type;     // Тип приказа. Второй уровень иерархии справочника
    private EducationYear _educationYear;     // Учебный год
    private String _reasonComment;     // Примечание к причине

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Причина списочного приказа.
     */
    public StudentOrderReasons getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина списочного приказа.
     */
    public void setReason(StudentOrderReasons reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Тип приказа. Второй уровень иерархии справочника. Свойство не может быть null.
     */
    @NotNull
    public StudentExtractType getType()
    {
        return _type;
    }

    /**
     * @param type Тип приказа. Второй уровень иерархии справочника. Свойство не может быть null.
     */
    public void setType(StudentExtractType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Примечание к причине.
     */
    @Length(max=255)
    public String getReasonComment()
    {
        return _reasonComment;
    }

    /**
     * @param reasonComment Примечание к причине.
     */
    public void setReasonComment(String reasonComment)
    {
        dirty(_reasonComment, reasonComment);
        _reasonComment = reasonComment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof StudentListOrderGen)
        {
            setReason(((StudentListOrder)another).getReason());
            setType(((StudentListOrder)another).getType());
            setEducationYear(((StudentListOrder)another).getEducationYear());
            setReasonComment(((StudentListOrder)another).getReasonComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentListOrderGen> extends AbstractStudentOrder.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentListOrder.class;
        }

        public T newInstance()
        {
            return (T) new StudentListOrder();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                    return obj.getReason();
                case "type":
                    return obj.getType();
                case "educationYear":
                    return obj.getEducationYear();
                case "reasonComment":
                    return obj.getReasonComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "reason":
                    obj.setReason((StudentOrderReasons) value);
                    return;
                case "type":
                    obj.setType((StudentExtractType) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "reasonComment":
                    obj.setReasonComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                        return true;
                case "type":
                        return true;
                case "educationYear":
                        return true;
                case "reasonComment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                    return true;
                case "type":
                    return true;
                case "educationYear":
                    return true;
                case "reasonComment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                    return StudentOrderReasons.class;
                case "type":
                    return StudentExtractType.class;
                case "educationYear":
                    return EducationYear.class;
                case "reasonComment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentListOrder> _dslPath = new Path<StudentListOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentListOrder");
    }
            

    /**
     * @return Причина списочного приказа.
     * @see ru.tandemservice.movestudent.entity.StudentListOrder#getReason()
     */
    public static StudentOrderReasons.Path<StudentOrderReasons> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Тип приказа. Второй уровень иерархии справочника. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentListOrder#getType()
     */
    public static StudentExtractType.Path<StudentExtractType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentListOrder#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Примечание к причине.
     * @see ru.tandemservice.movestudent.entity.StudentListOrder#getReasonComment()
     */
    public static PropertyPath<String> reasonComment()
    {
        return _dslPath.reasonComment();
    }

    public static class Path<E extends StudentListOrder> extends AbstractStudentOrder.Path<E>
    {
        private StudentOrderReasons.Path<StudentOrderReasons> _reason;
        private StudentExtractType.Path<StudentExtractType> _type;
        private EducationYear.Path<EducationYear> _educationYear;
        private PropertyPath<String> _reasonComment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Причина списочного приказа.
     * @see ru.tandemservice.movestudent.entity.StudentListOrder#getReason()
     */
        public StudentOrderReasons.Path<StudentOrderReasons> reason()
        {
            if(_reason == null )
                _reason = new StudentOrderReasons.Path<StudentOrderReasons>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Тип приказа. Второй уровень иерархии справочника. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentListOrder#getType()
     */
        public StudentExtractType.Path<StudentExtractType> type()
        {
            if(_type == null )
                _type = new StudentExtractType.Path<StudentExtractType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentListOrder#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Примечание к причине.
     * @see ru.tandemservice.movestudent.entity.StudentListOrder#getReasonComment()
     */
        public PropertyPath<String> reasonComment()
        {
            if(_reasonComment == null )
                _reasonComment = new PropertyPath<String>(StudentListOrderGen.P_REASON_COMMENT, this);
            return _reasonComment;
        }

        public Class getEntityClass()
        {
            return StudentListOrder.class;
        }

        public String getEntityName()
        {
            return "studentListOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
