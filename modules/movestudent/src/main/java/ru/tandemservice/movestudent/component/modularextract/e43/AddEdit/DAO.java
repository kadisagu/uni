/* $Id: DAO.java.vm 11769 2010-03-05 07:11:49Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e43.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.ChangeDateStateExaminationStuExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 16.02.2011
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<ChangeDateStateExaminationStuExtract, Model>
{
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    protected ChangeDateStateExaminationStuExtract createNewInstance()
    {
        return new ChangeDateStateExaminationStuExtract();
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        if (model.getExtractId() != null)
        {
        ChangeDateStateExaminationStuExtract extract = (ChangeDateStateExaminationStuExtract) model.getExtract();
        model.setCurrentDate(extract.getCurrentDate());
        model.setNewDate(extract.getNewDate());
        }
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setCurrentDate(model.getCurrentDate());
        model.getExtract().setNewDate(model.getNewDate());
        super.update(model);
    }
}
