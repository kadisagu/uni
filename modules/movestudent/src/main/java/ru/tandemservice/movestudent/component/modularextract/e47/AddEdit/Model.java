/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e47.AddEdit;

import com.google.common.collect.Lists;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.AddGroupManagerStuExtract;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 10.08.2012
 */
public class Model extends CommonModularStudentExtractAddEditModel<AddGroupManagerStuExtract>
{
    private ISelectModel _eduYearsList;
    private EducationYear _eduYear;
    private IMultiSelectModel _groupListModel;
    private List<Group> _groupsList = Lists.newArrayList();
    private Group _currentGroup;

    public String getCurrentGroupManager()
    {
        return "Староста группы " + _currentGroup.getTitle();
    }

    public String getCurrentGroupStudent()
    {
        String students = "";
        List<Student> studentList = UniDaoFacade.getGroupDao().getCaptainStudentList(_currentGroup);
        for (Student student :studentList)
        {
            students += (students.isEmpty() ? "" : ", ") + student.getFullFio();
        }
        return students;
    }

    public ISelectModel getEduYearsList()
    {
        return _eduYearsList;
    }

    public void setEduYearsList(ISelectModel eduYearsList)
    {
        _eduYearsList = eduYearsList;
    }

    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    public void setEduYear(EducationYear eduYear)
    {
        _eduYear = eduYear;
    }

    public IMultiSelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(IMultiSelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public List<Group> getGroupsList()
    {
        return _groupsList;
    }

    public void setGroupsList(List<Group> groupsList)
    {
        _groupsList = groupsList;
    }

    public Group getCurrentGroup()
    {
        return _currentGroup;
    }

    public void setCurrentGroup(Group currentGroup)
    {
        _currentGroup = currentGroup;
    }
}