/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e60;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.ReEducationFGOSStuExtract;

import java.util.ArrayList;
import java.util.List;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 24.08.2012
 */
public class ReEducationFGOSStuExtractPrint implements IPrintFormCreator<ReEducationFGOSStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ReEducationFGOSStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        List<String> paragraphsToRemove = new ArrayList<>();

        if (extract.isAnnulAttestationResults())
        {
            StringBuilder builder = new StringBuilder();
            builder.append("Аннулировать аттестации по всем дисциплинам, прослушанным на ");
            builder.append(extract.getPrevCourse().getTitle()).append(" курсе в ");
            builder.append(extract.getEduYear().getTitle()).append(" уч. году.");
            modifier.put("annulAttestation", builder.toString());
        }
        else
        {
            paragraphsToRemove.add("annulAttestation");
        }

        if (extract.isLiquidateGOSDifference())
        {
            StringBuilder builder = new StringBuilder();
            builder.append("Разницу в учебных планах, образовавшуюся при переходе с  ГОС II  на ФГОС, ликвидировать в срок до ");
            builder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getLiquidationDeadlineDate())).append(".");
            modifier.put("liquidateGOSDifference", builder.toString());
        }
        else
        {
            paragraphsToRemove.add("liquidateGOSDifference");
        }

        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, paragraphsToRemove, true, false);
        CommonExtractPrint.initFefuGroup(modifier, "inGroupNew", extract.getGroupNew(), extract.getEducationOrgUnitNew().getDevelopForm(), " в группе ");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}