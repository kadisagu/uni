package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITakeOffPayment;
import ru.tandemservice.movestudent.entity.gen.SocGrantPaymentStopStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О прекращении выплаты социальной стипендии
 */
public class SocGrantPaymentStopStuExtract extends SocGrantPaymentStopStuExtractGen implements ITakeOffPayment
{
    @Override
    public Date getBeginDate()
    {
        return getSocGrantPaymentStopDate();
    }

    @Override
    public Date getPaymentTakeOffDate()
    {
        return getSocGrantPaymentStopDate();
    }
}