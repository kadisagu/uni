/* $Id$ */
package ru.tandemservice.movestudent.component.customorder;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import ru.tandemservice.movestudent.entity.StudentModularOrder;

/**
 * @author Nikolay Fedorovskih
 * @since 28.10.2013
 */
public interface ICustomOrderDao
{
    String CUSTOM_ORDER_DAO_BEAN_NAME = "customOrderDao";

    final SpringBeanCache<ICustomOrderDao> instance = new SpringBeanCache<>(CUSTOM_ORDER_DAO_BEAN_NAME);

    void doCommitCustomExtract(IStudentCustomOrderExtract extract);

    void doRollbackCustomExtract(IStudentCustomOrderExtract extract);

    Long getModifiableOrderId(AbstractStudentParagraph customListOrderParagraph);

    Long getModifiableOrderId(AbstractStudentOrder customListOrder);

    /**
     * Проверяет, есть ли выписки об отмене, изменении или дополнении, ссылающиеся на данную выписку.
     *
     * @param extract выписка
     * @return да или нет
     */
    boolean hasModifyExtract(AbstractStudentExtract extract);

    /**
     * Проверяет, есть ли выписки об отмене, изменении или дополнении, ссылающиеся на данный приказ.
     *
     * @param order приказ
     * @return да или нет
     */
    boolean hasModifyExtract(AbstractStudentOrder order);
}