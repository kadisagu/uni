/* $Id$ */
package ru.tandemservice.movestudent.utils.system.advgenerators;

import org.apache.commons.collections15.map.LinkedMap;

import java.util.Map;

/**
 * Класс, содержащий мап опписаний выписок, используемый для генерации инфраструктуры для новой выписки
 * При необходимости создать новую выписку из сборного приказа, требуется создать новую строку, куда передать
 * - номер выписки (используется для идентификации и в идеале соответствует номеру компонента, а так же индексу типа выписки);
 * - Описание выписки:
 * - Индекс выписки по элементу справочника типов выписок
 * - Код элемента справочника типов выписок
 * - Название элемента справочника
 * - Код элемента справочника шаблонов выписок
 * - название файла текстового шаблона для генерации rtf-шаблона
 *
 * @author Dmitry Seleznev
 * @since 10.08.2012
 */
public class ModularExtractAdvancedData
{
    /**
     * Мап опписаний выписок, используемый для генерации инфраструктуры для новой выписки.
	 * Первые 11 приказов имеют отличные от текущих коды, но генератор обычно используется только для создания новых выписок (по одной за раз),
	 * поэтому неверные данные для старых ничего не испортят.
     */
    public static final Map<Integer, ModularExtractMetaDescription> EXTRACT_DESCRIPTIONS_MAP = new LinkedMap<>();

    static
    {
        EXTRACT_DESCRIPTIONS_MAP.put(1, new ModularExtractMetaDescription("1", "1.1", "О предоставлении академического отпуска", "1", "weekendStuExtract", new EntityMetaDescriptor("weekendStuExtract", "О предоставлении академического отпуска")));
        EXTRACT_DESCRIPTIONS_MAP.put(2, new ModularExtractMetaDescription("2", "1.2", "О восстановлении", "2", "restorationStuExtract", new EntityMetaDescriptor("restorationStuExtract", "О восстановлении")));
        EXTRACT_DESCRIPTIONS_MAP.put(3, new ModularExtractMetaDescription("3", "1.3", "О выходе из академического отпуска", "3", "weekendOutStuExtract", new EntityMetaDescriptor("weekendOutStuExtract", "О выходе из академического отпуска")));
        EXTRACT_DESCRIPTIONS_MAP.put(4, new ModularExtractMetaDescription("4", "1.4", "О зачислении", "4", "eduEnrolmentStuExtract", new EntityMetaDescriptor("eduEnrolmentStuExtract", "О зачислении")));
        EXTRACT_DESCRIPTIONS_MAP.put(5, new ModularExtractMetaDescription("5", "1.5", "О переводе на другую форму освоения", "5", "transferDevFormStuExtract", new EntityMetaDescriptor("transferDevFormStuExtract", "О переводе на другую форму освоения")));
        EXTRACT_DESCRIPTIONS_MAP.put(6, new ModularExtractMetaDescription("6", "1.6", "О переводе на другую основу оплаты обучения", "6", "transferCompTypeStuExtract", new EntityMetaDescriptor("transferCompTypeStuExtract", "О переводе на другую основу оплаты обучения")));
        EXTRACT_DESCRIPTIONS_MAP.put(7, new ModularExtractMetaDescription("7", "1.7", "О переводе на другое направление подготовки (специальность)", "7", "transferEduTypeStuExtract", new EntityMetaDescriptor("transferEduTypeStuExtract", "О переводе на другое направление подготовки (специальность)")));
        EXTRACT_DESCRIPTIONS_MAP.put(8, new ModularExtractMetaDescription("8", "1.8", "Об изменении пункта обучения", "8", "transferStuExtract", new EntityMetaDescriptor("transferStuExtract", "Об изменении пункта обучения")));
        EXTRACT_DESCRIPTIONS_MAP.put(9, new ModularExtractMetaDescription("9", "1.9", "Об отчислении", "9", "excludeStuExtract", new EntityMetaDescriptor("excludeStuExtract", "Об отчислении")));
        EXTRACT_DESCRIPTIONS_MAP.put(10, new ModularExtractMetaDescription("10", "1.10", "О смене фамилии (имени)", "10", "changeFioStuExtract", new EntityMetaDescriptor("changeFioStuExtract", "О смене фамилии (имени)")));
        EXTRACT_DESCRIPTIONS_MAP.put(11, new ModularExtractMetaDescription("11", "1.11", "Об отмене приказа", "11", "revertOrderStuExtract", new EntityMetaDescriptor("revertOrderStuExtract", "Об отмене приказа")));
        EXTRACT_DESCRIPTIONS_MAP.put(12, new ModularExtractMetaDescription("12", "1.12", "О предоставлении отпуска по беременности и родам", "12", "weekendPregnancyStuExtract", new EntityMetaDescriptor("weekendPregnancyStuExtract", "О предоставлении отпуска по беременности и родам")));
        EXTRACT_DESCRIPTIONS_MAP.put(13, new ModularExtractMetaDescription("13", "1.13", "О предоставлении отпуска по уходу за ребенком", "13", "weekendChildStuExtract", new EntityMetaDescriptor("weekendChildStuExtract", "О предоставлении отпуска по уходу за ребенком")));
        EXTRACT_DESCRIPTIONS_MAP.put(14, new ModularExtractMetaDescription("14", "1.14", "О зачислении", "14", "eduEnrolmentStuExtractExt", new EntityMetaDescriptor("eduEnrolmentStuExtractExt", "О зачислении")));
        EXTRACT_DESCRIPTIONS_MAP.put(15, new ModularExtractMetaDescription("15", "1.15", "О зачислении в порядке перевода", "15", "eduEnrAsTransferStuExtract", new EntityMetaDescriptor("eduEnrAsTransferStuExtract", "О зачислении в порядке перевода")));
        EXTRACT_DESCRIPTIONS_MAP.put(16, new ModularExtractMetaDescription("16", "1.16", "О предоставлении академического отпуска", "16", "weekendStuExtractExt", new EntityMetaDescriptor("weekendStuExtractExt", "О предоставлении академического отпуска")));
        EXTRACT_DESCRIPTIONS_MAP.put(17, new ModularExtractMetaDescription("17", "1.17", "О восстановлении", "17", "restorationStuExtractExt", new EntityMetaDescriptor("restorationStuExtractExt", "О восстановлении")));
        EXTRACT_DESCRIPTIONS_MAP.put(18, new ModularExtractMetaDescription("18", "1.18", "О переводе", "18", "transferStuExtractExt", new EntityMetaDescriptor("transferStuExtractExt", "О переводе")));
        EXTRACT_DESCRIPTIONS_MAP.put(19, new ModularExtractMetaDescription("19", "1.19", "Об отстранении от занятий", "19", "dischargingStuExtract", new EntityMetaDescriptor("dischargingStuExtract", "Об отстранении от занятий")));
        EXTRACT_DESCRIPTIONS_MAP.put(20, new ModularExtractMetaDescription("20", "1.20", "О продлении экзаменационной сессии", "20", "sessionProlongStuExtract", new EntityMetaDescriptor("sessionProlongStuExtract", "О продлении экзаменационной сессии")));
        EXTRACT_DESCRIPTIONS_MAP.put(21, new ModularExtractMetaDescription("21", "1.21", "О продлении отпуска по уходу за ребенком", "21", "prolongWeekndChildStuExtract", new EntityMetaDescriptor("prolongWeekndChildStuExtract", "О продлении отпуска по уходу за ребенком")));
        EXTRACT_DESCRIPTIONS_MAP.put(22, new ModularExtractMetaDescription("22", "1.22", "О назначении академической стипендии", "22", "acadGrantAssignStuExtract", new EntityMetaDescriptor("acadGrantAssignStuExtract", "О назначении академической стипендии")));
        EXTRACT_DESCRIPTIONS_MAP.put(23, new ModularExtractMetaDescription("23", "1.23", "О назначении социальной стипендии", "23", "socGrantAssignStuExtract", new EntityMetaDescriptor("socGrantAssignStuExtract", "О назначении социальной стипендии")));
        EXTRACT_DESCRIPTIONS_MAP.put(24, new ModularExtractMetaDescription("24", "1.24", "О приостановлении выплаты социальной стипендии", "24", "socGrantStopStuExtract", new EntityMetaDescriptor("socGrantStopStuExtract", "О приостановлении выплаты социальной стипендии")));
        EXTRACT_DESCRIPTIONS_MAP.put(25, new ModularExtractMetaDescription("25", "1.25", "О возобновлении выплаты социальной стипендии", "25", "socGrantResumptionStuExtract", new EntityMetaDescriptor("socGrantResumptionStuExtract", "О возобновлении выплаты социальной стипендии")));
        EXTRACT_DESCRIPTIONS_MAP.put(26, new ModularExtractMetaDescription("26", "1.26", "Об отчислении", "26", "excludeStuExtractExt", new EntityMetaDescriptor("excludeStuExtractExt", "Об отчислении")));
        EXTRACT_DESCRIPTIONS_MAP.put(27, new ModularExtractMetaDescription("27", "1.27", "Об отчислении в связи с переводом", "27", "excludeTransfStuExtract", new EntityMetaDescriptor("excludeTransfStuExtract", "Об отчислении в связи с переводом")));
        EXTRACT_DESCRIPTIONS_MAP.put(28, new ModularExtractMetaDescription("28", "1.28", "О выдаче дубликата зачетной книжки", "28", "giveBookDuplicateStuExtract", new EntityMetaDescriptor("giveBookDuplicateStuExtract", "О выдаче дубликата зачетной книжки")));
        EXTRACT_DESCRIPTIONS_MAP.put(29, new ModularExtractMetaDescription("29", "1.29", "О выдаче дубликата студенческого билета", "29", "giveCardDuplicateStuExtract", new EntityMetaDescriptor("giveCardDuplicateStuExtract", "О выдаче дубликата студенческого билета")));
        EXTRACT_DESCRIPTIONS_MAP.put(30, new ModularExtractMetaDescription("30", "1.30", "О выдаче дубликата диплома", "30", "giveDiplomaDuplicateStuExtract", new EntityMetaDescriptor("giveDiplomaDuplicateStuExtract", "О выдаче дубликата диплома")));
        EXTRACT_DESCRIPTIONS_MAP.put(31, new ModularExtractMetaDescription("31", "1.31", "О выдаче дубликата приложения к диплому", "31", "giveDiplAppDuplicateStuExtract", new EntityMetaDescriptor("giveDiplAppDuplicateStuExtract", "О выдаче дубликата приложения к диплому")));
        EXTRACT_DESCRIPTIONS_MAP.put(32, new ModularExtractMetaDescription("32", "1.32", "О допуске к занятиям", "32", "admissionStuExtract", new EntityMetaDescriptor("admissionStuExtract", "О допуске к занятиям")));
        EXTRACT_DESCRIPTIONS_MAP.put(33, new ModularExtractMetaDescription("33", "1.33", "О назначении выплаты пособий", "33", "assignBenefitStuExtract", new EntityMetaDescriptor("assignBenefitStuExtract", "О назначении выплаты пособий")));
        EXTRACT_DESCRIPTIONS_MAP.put(34, new ModularExtractMetaDescription("34", "1.34", "О смене имени", "34", "changeFirstNameStuExtract", new EntityMetaDescriptor("changeFirstNameStuExtract", "О смене имени")));
        EXTRACT_DESCRIPTIONS_MAP.put(35, new ModularExtractMetaDescription("35", "1.35", "О смене отчества", "35", "changeMiddleNameStuExtract", new EntityMetaDescriptor("changeMiddleNameStuExtract", "О смене отчества")));
        EXTRACT_DESCRIPTIONS_MAP.put(36, new ModularExtractMetaDescription("36", "1.36", "О повторном обучении", "36", "reEducationStuExtract", new EntityMetaDescriptor("reEducationStuExtract", "О повторном обучении")));
        EXTRACT_DESCRIPTIONS_MAP.put(37, new ModularExtractMetaDescription("37", "1.37", "О продлении академического отпуска", "37", "prolongWeekendStuExtract", new EntityMetaDescriptor("prolongWeekendStuExtract", "О продлении академического отпуска")));
        EXTRACT_DESCRIPTIONS_MAP.put(38, new ModularExtractMetaDescription("38", "1.38", "О выходе из отпуска по уходу за ребенком", "38", "weekendChildOutStuExtract", new EntityMetaDescriptor("weekendChildOutStuExtract", "О выходе из отпуска по уходу за ребенком")));
        EXTRACT_DESCRIPTIONS_MAP.put(39, new ModularExtractMetaDescription("39", "1.39", "О скидке по оплате за обучение", "39", "paymentDiscountStuExtract", new EntityMetaDescriptor("paymentDiscountStuExtract", "О скидке по оплате за обучение")));
        EXTRACT_DESCRIPTIONS_MAP.put(40, new ModularExtractMetaDescription("40", "1.40", "О выдаче диплома о неполном высшем профессиональном образовании", "40", "giveDiplIncompleteStuListExtract", new EntityMetaDescriptor("giveDiplIncompleteStuListExtract", "О выдаче диплома о неполном высшем профессиональном образовании")));
        EXTRACT_DESCRIPTIONS_MAP.put(41, new ModularExtractMetaDescription("41", "1.41", "О досрочной сдаче сессии", "41", "prescheduleSessionPassStuExtract", new EntityMetaDescriptor("prescheduleSessionPassStuExtract", "О досрочной сдаче сессии")));
        EXTRACT_DESCRIPTIONS_MAP.put(42, new ModularExtractMetaDescription("42", "1.42", "О переносе сроков сдачи защиты дипломной работы", "42", "changePassDiplomaWorkPeriodStuExtract", new EntityMetaDescriptor("changePassDiplomaWorkPeriodStuExtract", "О переносе сроков сдачи защиты дипломной работы")));
        EXTRACT_DESCRIPTIONS_MAP.put(43, new ModularExtractMetaDescription("43", "1.43", "О переносе сроков сдачи государственного экзамена", "43", "changeDateStateExaminationStuExtract", new EntityMetaDescriptor("changeDateStateExaminationStuExtract", "О переносе сроков сдачи государственного экзамена")));
        EXTRACT_DESCRIPTIONS_MAP.put(44, new ModularExtractMetaDescription("44", "1.44", "О повторной защите дипломной работы", "44", "repeatProtectDiplomaWorkStuExtract", new EntityMetaDescriptor("repeatProtectDiplomaWorkStuExtract", "О повторной защите дипломной работы")));
        EXTRACT_DESCRIPTIONS_MAP.put(45, new ModularExtractMetaDescription("45", "1.45", "О повторной сдаче государственного экзамена", "45", "repeatPassStateExamStuExtract", new EntityMetaDescriptor("repeatPassStateExamStuExtract", "О повторной сдаче государственного экзамена")));
        EXTRACT_DESCRIPTIONS_MAP.put(46, new ModularExtractMetaDescription("46", "1.46", "О свободном посещении занятий", "46", "freeStudiesAttendanceStuExtract", new EntityMetaDescriptor("freeStudiesAttendanceStuExtract", "О свободном посещении занятий")));

        EXTRACT_DESCRIPTIONS_MAP.put(47,
                new ModularExtractMetaDescription("47", "1.47", "О назначении старостой учебной группы", "47", "addGroupmanagerStuExtract",
                        new EntityMetaDescriptor("addGroupManagerStuExtract", "О назначении старостой учебной группы",
                                new EntityFieldManyToOneMetaDescriptor("educationYear", "educationYear", "Учебный год"),
                                new EntityFieldManyToOneMetaDescriptor("prevGroupManager", "student", "Предыдущий староста")
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(48,
                new ModularExtractMetaDescription("48", "1.48", "Об освобождении от обязанностей старосты учебной группы", "48", "removeGroupManagerStuExtract",
                        new EntityMetaDescriptor("removeGroupManagerStuExtract", "Об освобождении от обязанностей старосты учебной группы",
                                new EntityFieldPropertyMetaDescriptor("removalDate", "timestamp", "Дата освобождения от обязанностей старосты", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(49,
                new ModularExtractMetaDescription("49", "1.49", "О дисциплинарном взыскании", "49", "disciplinaryPenaltyStuExtract",
                        new EntityMetaDescriptor("disciplinaryPenaltyStuExtract", "О дисциплинарном взыскании")
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(50,
                new ModularExtractMetaDescription("50", "1.50", "О наложении взыскания за утерю зачетной книжки", "50", "recordBookLossPenaltyStuExtract",
                        new EntityMetaDescriptor("recordBookLossPenaltyStuExtract", "О наложении взыскания за утерю зачетной книжки")
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(51,
                new ModularExtractMetaDescription("51", "1.51", "О наложении взыскания за утерю студенческого билета", "51", "studentCardLossPenaltyStuExtract",
                        new EntityMetaDescriptor("studentCardLossPenaltyStuExtract", "О наложении взыскания за утерю студенческого билета")
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(52,
                new ModularExtractMetaDescription("52", "1.52", "О допуске к сдаче государственного экзамена", "52", "admitToStateExamsStuExtract",
                        new EntityMetaDescriptor("admitToStateExamsStuExtract", "О допуске к сдаче государственного экзамена")
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(53,
                new ModularExtractMetaDescription("53", "1.53", "О предоставлении каникул", "53", "holidayStuExtract",
                        new EntityMetaDescriptor("holidayStuExtract", "О предоставлении каникул",
                                new EntityFieldPropertyMetaDescriptor("beginDate", "timestamp", "Дата начала каникул", true),
                                new EntityFieldPropertyMetaDescriptor("endDate", "timestamp", "Дата окончания каникул", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(54,
                new ModularExtractMetaDescription("54", "1.54", "О переносе итоговой государственной аттестации", "54", "changePassStateExamsStuExtract",
                        new EntityMetaDescriptor("changePassStateExamsStuExtract", "О переносе итоговой государственной аттестации",
                                new EntityFieldPropertyMetaDescriptor("newDate", "timestamp", "Новая дата", true),
                                new EntityFieldPropertyMetaDescriptor("year", "integer", "Год", true),
                                new EntityFieldPropertyMetaDescriptor("season", "trimmedstring", "Время года", false)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(55,
                new ModularExtractMetaDescription("55", "1.55", "Об установлении индивидуального срока сдачи сессии(продление сессии)", "55", "sessionIndividualStuExtract",
                        new EntityMetaDescriptor("sessionIndividualStuExtract", "Об установлении индивидуального срока сдачи сессии(продление сессии)",
                                new EntityFieldPropertyMetaDescriptor("deadlineDate", "timestamp", "Срок сдачи", true),
                                new EntityFieldPropertyMetaDescriptor("season", "trimmedstring", "Время года", false)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(56,
                new ModularExtractMetaDescription("56", "1.56", "О переводе на индивидуальный план обучения", "56", "transferIndPlanStuExtract",
                        new EntityMetaDescriptor("transferIndPlanStuExtract", "О переводе на индивидуальный план обучения",
                                new EntityFieldManyToOneMetaDescriptor("educationYear", "educationYear", "Учебный год", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(57,
                new ModularExtractMetaDescription("57", "1.57", "О допуске к защите выпускной квалификационной работы(после ГЭ)", "57", "admitToDiplomaStuExtract",
                        new EntityMetaDescriptor("admitToDiplomaStuExtract", "О допуске к защите выпускной квалификационной работы(после ГЭ)",
                                new EntityFieldPropertyMetaDescriptor("stateExams", "boolean", "Множественное число для ГЭ", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(58,
                new ModularExtractMetaDescription("58", "1.58", "О переводе на индивидуальный график обучения", "58", "transferIndGraphStuExtract",
                        new EntityMetaDescriptor("transferIndGraphStuExtract", "О переводе на индивидуальный график обучения",
                                new EntityFieldManyToOneMetaDescriptor("educationYear", "educationYear", "Учебный год", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(59,
                new ModularExtractMetaDescription("59", "1.59", "О предоставлении отпуска по уходу за ребенком до 1,5 лет", "59", "weekendChildOneAndHalfStuExtract",
                        new EntityMetaDescriptor("weekendChildOneAndHalfStuExtract", "О предоставлении отпуска по уходу за ребенком до 1,5 лет",
                                new EntityFieldPropertyMetaDescriptor("beginDate", "timestamp", "Дата начала отпуска", true),
                                new EntityFieldPropertyMetaDescriptor("endDate", "timestamp", "Дата окончания отпуска", true),
                                new EntityFieldManyToOneMetaDescriptor("studentStatusOld", "studentStatus", "Предыдущее состояние студента", true),
                                new EntityFieldPropertyMetaDescriptor("payBenefit", "boolean", "Выплатить пособие по беременности и родам", true),
                                new EntityFieldPropertyMetaDescriptor("payBenefitBeginDate", "timestamp", "Дата начала выплаты пособия по беременности и родам"),
                                new EntityFieldPropertyMetaDescriptor("payBenefitEndDate", "timestamp", "Дата окончания выплаты пособия по беременности и родам"),
                                new EntityFieldPropertyMetaDescriptor("payOnetimeBenefit", "boolean", "Выплатить единовременное пособие", true),
                                new EntityFieldPropertyMetaDescriptor("payBenefitText", "trimmedstring", "Текст выплаты пособия по беременности и родам"),
                                new EntityFieldPropertyMetaDescriptor("payOnetimeBenefitText", "trimmedstring", "Текст выплаты единовременного пособия"),
                                new EntityFieldPropertyMetaDescriptor("freeAttendance", "boolean", "Свободное посещение занятий", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(60,
                new ModularExtractMetaDescription("60", "1.60", "О предоставлении повторного года обучения с переходом на ФГОС", "60", "reEducationFGOSStuExtract",
                        new EntityMetaDescriptor("reEducationFGOSStuExtract", "О предоставлении повторного года обучения с переходом на ФГОС", "commonStuExtract",
                                new EntityFieldPropertyMetaDescriptor("beginDate", "timestamp", "Дата начала повторного обучения", true),
                                new EntityFieldPropertyMetaDescriptor("annulAttestationResults", "boolean", "Аннулировать аттестации по прослушанным за прошедший курс дисциплинам", true),
                                new EntityFieldManyToOneMetaDescriptor("prevCourse", "course", "Курс, за который требуется аннулировать аттестации по дисциплинам"),
                                new EntityFieldManyToOneMetaDescriptor("eduYear", "educationYear", "Учебный год, за который требуется аннулировать аттестации по дисциплинам"),
                                new EntityFieldPropertyMetaDescriptor("liquidateGOSDifference", "boolean", "Ликвидировать разницу в учебных планах", true),
                                new EntityFieldPropertyMetaDescriptor("liquidationDeadlineDate", "timestamp", "Крайняя дата ликвидации разницы в учебных планах")
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(61,
                new ModularExtractMetaDescription("61", "1.61", "О восстановлении", "61", "restorationCourseStuExtract",
                        new EntityMetaDescriptor("restorationCourseStuExtract", "О восстановлении", "commonStuExtract",
                                new EntityFieldPropertyMetaDescriptor("restorationDate", "timestamp", "Дата восстановления", true),
                                new EntityFieldPropertyMetaDescriptor("dismissDate", "timestamp", "Дата отчисления", true),
                                new EntityFieldPropertyMetaDescriptor("dismissOrder", "trimmedstring", "Приказ об отчислении", true),
                                new EntityFieldPropertyMetaDescriptor("dismissOrderDate", "timestamp", "Дата приказа об отчислении", true),
                                new EntityFieldPropertyMetaDescriptor("applyDate", "timestamp", "Дата подачи заявления", true),
                                new EntityFieldPropertyMetaDescriptor("unreasonableDismissExcuse", "boolean", "Отчисление по неуважительной причине", true),
                                new EntityFieldPropertyMetaDescriptor("hasDebts", "boolean", "Разница в учебных планах", true),
                                new EntityFieldPropertyMetaDescriptor("deadlineDate", "timestamp", "Срок ликвидации задолжности"),
                                new EntityFieldPropertyMetaDescriptor("dismissedFrom", "trimmedstring", "Отчислен из (название ОУ)"),
                                new EntityFieldPropertyMetaDescriptor("dismissReason", "trimmedstring", "Причина отчисления")
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(62,
                new ModularExtractMetaDescription("62", "1.62", "Об отчислении (государственный экзамен)", "62", "excludeStateExamStuExtract",
                        new EntityMetaDescriptor("excludeStateExamStuExtract", "Об отчислении (государственный экзамен)",
                                new EntityFieldManyToOneMetaDescriptor("studentStatusOld", "studentStatus", "Состояние студента на момент проведения приказа", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(63,
                new ModularExtractMetaDescription("63", "1.63", "Об отчислении (недопуск к ВКР)", "63", "excludeUnacceptedToGPDefenceStuExtract",
                        new EntityMetaDescriptor("excludeUnacceptedToGPDefenceStuExtract", "Об отчислении (недопуск к ВКР)",
                                new EntityFieldManyToOneMetaDescriptor("studentStatusOld", "studentStatus", "Состояние студента на момент проведения приказа", true),
                                new EntityFieldPropertyMetaDescriptor("passStateExam", "boolean", "Сдача ГЭ предусмотрена"),
                                new EntityFieldPropertyMetaDescriptor("pluralForStateExam", "boolean", "Множественное число для ГЭ")
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(64,
                new ModularExtractMetaDescription("64", "1.64", "Об отчислении (ВКР)", "64", "excludeGPDefenceFailStuExtract",
                        new EntityMetaDescriptor("excludeGPDefenceFailStuExtract", "Об отчислении (ВКР)",
                                new EntityFieldManyToOneMetaDescriptor("studentStatusOld", "studentStatus", "Состояние студента на момент проведения приказа", true),
                                new EntityFieldPropertyMetaDescriptor("passStateExam", "boolean", "Сдача ГЭ предусмотрена"),
                                new EntityFieldPropertyMetaDescriptor("pluralForStateExam", "boolean", "Множественное число для ГЭ")
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(65,
                new ModularExtractMetaDescription("65", "1.65", "О переводе из группы в группу", "65", "transferGroupStuExtract",
                        new EntityMetaDescriptor("transferGroupStuExtract", "О переводе из группы в группу", "commonStuExtract")
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(66,
                new ModularExtractMetaDescription("66", "1.66", "О переводе с курса на следующий курс", "66", "transferCourseStuExtract",
                        new EntityMetaDescriptor("transferCourseStuExtract", "О переводе с курса на следующий курс", "commonStuExtract",
                                new EntityFieldPropertyMetaDescriptor("oldGroupNumber", "trimmedstring", "Номер группы на момент формирования приказа", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(67,
                new ModularExtractMetaDescription("67", "1.67", "О переводе со специальности на специальность (с направления на направление)", "67", "transferEduLevelStuExtract",
                        new EntityMetaDescriptor("transferEduLevelStuExtract", "О переводе со специальности на специальность (с направления на направление)", "commonStuExtract",
                                new EntityFieldPropertyMetaDescriptor("liquidateEduPlanDifference", "boolean", "Ликвидировать разницу в учебных планах", true),
                                new EntityFieldPropertyMetaDescriptor("liquidationDeadlineDate", "timestamp", "Крайняя дата ликвидации разницы в учебных планах")
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(68,
                new ModularExtractMetaDescription("68", "1.68", "О переводе со специальности на специальность (с направления на направление) со сменой формирующего подразделения", "68", "transferFormativeStuExtract",
                        new EntityMetaDescriptor("transferFormativeStuExtract", "О переводе со специальности на специальность (с направления на направление) со сменой формирующего подразделения", "commonStuExtract",
                                new EntityFieldPropertyMetaDescriptor("liquidateEduPlanDifference", "boolean", "Ликвидировать разницу в учебных планах", true),
                                new EntityFieldPropertyMetaDescriptor("liquidationDeadlineDate", "timestamp", "Крайняя дата ликвидации разницы в учебных планах")
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(69,
                new ModularExtractMetaDescription("69", "1.69", "О переводе со специальности на специальность (с направления на направление) со сменой территориального подразделения", "69", "transferTerritorialStuExtract",
                        new EntityMetaDescriptor("transferTerritorialStuExtract", "О переводе со специальности на специальность (с направления на направление) со сменой территориального подразделения", "commonStuExtract",
                                new EntityFieldPropertyMetaDescriptor("liquidateEduPlanDifference", "boolean", "Ликвидировать разницу в учебных планах", true),
                                new EntityFieldPropertyMetaDescriptor("liquidationDeadlineDate", "timestamp", "Крайняя дата ликвидации разницы в учебных планах")
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(70,
                new ModularExtractMetaDescription("70", "1.70", "О переводе со специальности на специальность (с направления на направление) между территориальными подразделениями", "70", "transferTerritorialExtStuExtract",
                        new EntityMetaDescriptor("transferTerritorialExtStuExtract", "О переводе со специальности на специальность (с направления на направление) между территориальными подразделениями", "commonStuExtract",
                                new EntityFieldPropertyMetaDescriptor("liquidateEduPlanDifference", "boolean", "Ликвидировать разницу в учебных планах", true),
                                new EntityFieldPropertyMetaDescriptor("liquidationDeadlineDate", "timestamp", "Крайняя дата ликвидации разницы в учебных планах")
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(71,
                new ModularExtractMetaDescription("71", "1.71", "О переводе на другие условия освоения и срок освоения", "71", "transferDevCondExtStuExtract",
                        new EntityMetaDescriptor("transferDevCondExtStuExtract", "О переводе на другие условия освоения и срок освоения", "commonStuExtract")
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(72,
                new ModularExtractMetaDescription("72", "1.72", "О переводе на другие условия освоения", "72", "transferDevCondStuExtract",
                        new EntityMetaDescriptor("transferDevCondStuExtract", "О переводе на другие условия освоения", "commonStuExtract",
                                new EntityFieldPropertyMetaDescriptor("plannedGraduateYear", "integer", "Планируемый год выпуска", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(73,
                new ModularExtractMetaDescription("73", "1.73", "О восстановлении и повторном допуске к сдаче государственного экзамена (пересдача ГЭ)", "73", "restorationAdmitStateExamsExtStuExtract",
                        new EntityMetaDescriptor("restorationAdmitStateExamsExtStuExtract", "О восстановлении и повторном допуске к сдаче государственного экзамена (пересдача ГЭ)", "commonStuExtract",
                                new EntityFieldPropertyMetaDescriptor("restorationDate", "timestamp", "Дата восстановления", true),
                                new EntityFieldPropertyMetaDescriptor("dismissDate", "timestamp", "Дата отчисления", true),
                                new EntityFieldPropertyMetaDescriptor("dismissOrder", "trimmedstring", "Приказ об отчислении", true),
                                new EntityFieldPropertyMetaDescriptor("dismissOrderDate", "timestamp", "Дата приказа об отчислении", true),
                                new EntityFieldPropertyMetaDescriptor("applyDate", "timestamp", "Дата подачи заявления", true),
                                new EntityFieldPropertyMetaDescriptor("stateExamPassDeadlineDate", "timestamp", "Срок сдачи государственного экзамена"),
                                new EntityFieldPropertyMetaDescriptor("dismissedFrom", "trimmedstring", "Отчислен из (название ОУ)"),
                                new EntityFieldPropertyMetaDescriptor("season", "trimmedstring", "Время года")
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(74,
                new ModularExtractMetaDescription("74", "1.74", "О закреплении за специализацией (профилем) и перераспеределнии в группу", "74", "transferProfileStuExtract",
                        new EntityMetaDescriptor("transferProfileStuExtract", "О закреплении за специализацией (профилем) и перераспеределнии в группу", "commonStuExtract")
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(75,
                new ModularExtractMetaDescription("75", "1.75", "О восстановлении и повторном допуске к сдаче государственного экзамена (неявка на ГЭ)", "75", "restorationAdmitStateExamsStuExtract",
                        new EntityMetaDescriptor("restorationAdmitStateExamsStuExtract", "О восстановлении и повторном допуске к сдаче государственного экзамена (неявка на ГЭ)", "commonStuExtract",
                                new EntityFieldPropertyMetaDescriptor("restorationDate", "timestamp", "Дата восстановления", true),
                                new EntityFieldPropertyMetaDescriptor("dismissDate", "timestamp", "Дата отчисления", true),
                                new EntityFieldPropertyMetaDescriptor("dismissOrder", "trimmedstring", "Приказ об отчислении", true),
                                new EntityFieldPropertyMetaDescriptor("dismissOrderDate", "timestamp", "Дата приказа об отчислении", true),
                                new EntityFieldPropertyMetaDescriptor("applyDate", "timestamp", "Дата подачи заявления", true),
                                new EntityFieldPropertyMetaDescriptor("stateExamPassDeadlineDate", "timestamp", "Срок сдачи государственного экзамена"),
                                new EntityFieldPropertyMetaDescriptor("dismissedFrom", "trimmedstring", "Отчислен из (название ОУ)"),
                                new EntityFieldPropertyMetaDescriptor("season", "trimmedstring", "Время года")
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(76,
                new ModularExtractMetaDescription("76", "1.76", "О переводе со специализации на специализацию (с профиля на профиль) и перераспределении в группу", "76", "transferProfileGroupExtStuExtract",
                        new EntityMetaDescriptor("transferProfileGroupExtStuExtract", "О переводе со специализации на специализацию (с профиля на профиль) и перераспределении в группу", "commonStuExtract")
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(77,
                new ModularExtractMetaDescription("77", "1.77", "О восстановлении и допуске к подготовке ВКР", "77", "restorationAdmitToDiplomaStuExtract",
                        new EntityMetaDescriptor("restorationAdmitToDiplomaStuExtract", "О восстановлении и допуске к подготовке ВКР", "commonStuExtract",
                                new EntityFieldPropertyMetaDescriptor("restorationDate", "timestamp", "Дата восстановления", true),
                                new EntityFieldPropertyMetaDescriptor("dismissDate", "timestamp", "Дата отчисления", true),
                                new EntityFieldPropertyMetaDescriptor("dismissOrder", "trimmedstring", "Приказ об отчислении", true),
                                new EntityFieldPropertyMetaDescriptor("dismissOrderDate", "timestamp", "Дата приказа об отчислении", true),
                                new EntityFieldPropertyMetaDescriptor("applyDate", "timestamp", "Дата подачи заявления", true),
                                new EntityFieldPropertyMetaDescriptor("dismissedFrom", "trimmedstring", "Отчислен из (название ОУ)")
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(78,
                new ModularExtractMetaDescription("78", "1.78", "О переводе на другую основу оплаты обучения (вакантное бюджетное место)", "78", "transferCompTypeExtStuExtract",
                        new EntityMetaDescriptor("transferCompTypeExtStuExtract", "О переводе на другую основу оплаты обучения (вакантное бюджетное место)", "commonStuExtract",
                                new EntityFieldPropertyMetaDescriptor("transferDate", "timestamp", "Дата перевода", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(79,
                new ModularExtractMetaDescription("79", "1.79", "О переводе со специализации на специализации (с профиля на профиль)", "79", "transferProfileExtStuExtract",
                        new EntityMetaDescriptor("transferProfileExtStuExtract", "О переводе со специализации на специализации (с профиля на профиль)", "commonStuExtract",
                                new EntityFieldPropertyMetaDescriptor("liquidateEduPlanDifference", "boolean", "Ликвидировать разницу в учебных планах", true),
                                new EntityFieldPropertyMetaDescriptor("liquidationDeadlineDate", "timestamp", "Крайняя дата ликвидации разницы в учебных планах")
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(80,
                new ModularExtractMetaDescription("80", "1.80", "Об изменении категории обучаемого", "80", "changeCategoryStuExtract",
                        new EntityMetaDescriptor("changeCategoryStuExtract", "Об изменении категории обучаемого",
                                new EntityFieldPropertyMetaDescriptor("categoryChangeDate", "timestamp", "Дата изменения категории", true),
                                new EntityFieldManyToOneMetaDescriptor("studentCategoryOld", "studentCategory", "Категория студента на момент формирования приказа"),
                                new EntityFieldManyToOneMetaDescriptor("studentCategoryNew", "studentCategory", "Новая категория студента")
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(81,
                new ModularExtractMetaDescription("81", "1.81", "О восстановлении и допуске к защите ВКР (неявка на защиту)", "81", "restorationAdmitToDiplomaDefStuExtract",
                        new EntityMetaDescriptor("restorationAdmitToDiplomaDefStuExtract", "О восстановлении и допуске к защите ВКР (неявка на защиту)", "commonStuExtract",
                                new EntityFieldPropertyMetaDescriptor("restorationDate", "timestamp", "Дата восстановления", true),
                                new EntityFieldPropertyMetaDescriptor("dismissDate", "timestamp", "Дата отчисления", true),
                                new EntityFieldPropertyMetaDescriptor("dismissOrder", "trimmedstring", "Приказ об отчислении", true),
                                new EntityFieldPropertyMetaDescriptor("dismissOrderDate", "timestamp", "Дата приказа об отчислении", true),
                                new EntityFieldPropertyMetaDescriptor("applyDate", "timestamp", "Дата подачи заявления", true),
                                new EntityFieldPropertyMetaDescriptor("dismissedFrom", "trimmedstring", "Отчислен из (название ОУ)"),
                                new EntityFieldPropertyMetaDescriptor("season", "trimmedstring", "Время года"),
                                new EntityFieldPropertyMetaDescriptor("dpDefenceDeadlineDate", "timestamp", "Срок защиты ВКР", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(82,
                new ModularExtractMetaDescription("82", "1.82", "О пересдаче дисциплин учебного плана", "82", "rePassDiscStuExtract",
                        new EntityMetaDescriptor("rePassDiscStuExtract", "О пересдаче дисциплин учебного плана")
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(83,
                new ModularExtractMetaDescription("83", "1.83", "О восстановлении и допуске к защите ВКР (повторная защита)", "83", "restorationAdmitToDiplomaDefExtStuExtract",
                        new EntityMetaDescriptor("restorationAdmitToDiplomaDefExtStuExtract", "О восстановлении и допуске к защите ВКР (повторная защита)", "commonStuExtract",
                                new EntityFieldPropertyMetaDescriptor("restorationDate", "timestamp", "Дата восстановления", true),
                                new EntityFieldPropertyMetaDescriptor("dismissDate", "timestamp", "Дата отчисления", true),
                                new EntityFieldPropertyMetaDescriptor("dismissOrder", "trimmedstring", "Приказ об отчислении", true),
                                new EntityFieldPropertyMetaDescriptor("dismissOrderDate", "timestamp", "Дата приказа об отчислении", true),
                                new EntityFieldPropertyMetaDescriptor("applyDate", "timestamp", "Дата подачи заявления", true),
                                new EntityFieldPropertyMetaDescriptor("dismissedFrom", "trimmedstring", "Отчислен из (название ОУ)"),
                                new EntityFieldPropertyMetaDescriptor("season", "trimmedstring", "Время года"),
                                new EntityFieldPropertyMetaDescriptor("dpDefenceDeadlineDate", "timestamp", "Срок защиты ВКР", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(84,
                new ModularExtractMetaDescription("84", "1.84", "О пересдаче дисциплин учебного плана комиссии", "84", "rePassDiscComissStuExtract",
                        new EntityMetaDescriptor("rePassDiscComissStuExtract", "О пересдаче дисциплин учебного плана комиссии",
                                new EntityFieldPropertyMetaDescriptor("discipline", "trimmedstring", "Наименование дисциплины", true),
                                new EntityFieldPropertyMetaDescriptor("actionType", "trimmedstring", "Форма промежуточного контроля", true),
                                new EntityFieldPropertyMetaDescriptor("actionDateTime", "timestamp", "Дата и время пересдачи", true),
                                new EntityFieldPropertyMetaDescriptor("actionPlace", "trimmedstring", "Номер аудитории", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(85,
                new ModularExtractMetaDescription("85", "1.85", "О выдаче диплома и присвоении квалификации", "85", "giveDiplomaStuExtract",
                        new EntityMetaDescriptor("giveDiplomaStuExtract", "О выдаче диплома и присвоении квалификации",
                                new EntityFieldManyToOneMetaDescriptor("studentStatusOld", "studentStatus", "Состояние студента на момент проведения приказа", true),
                                new EntityFieldPropertyMetaDescriptor("withExcellent", "boolean", "Диплом с отличием", true),
                                new EntityFieldPropertyMetaDescriptor("printDiplomaQualification", "boolean", "Печатать квалификацию по диплому", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(86,
                new ModularExtractMetaDescription("86", "1.86", "Об оказании материальной помощи", "86", "matAidAssignStuExtract",
                        new EntityMetaDescriptor("matAidAssignStuExtract", "Об оказании материальной помощи",
                                new EntityFieldPropertyMetaDescriptor("grantSize", "double", "Размер материальной помощи", true),
                                new EntityFieldPropertyMetaDescriptor("month", "integer", "Месяц оказания материальной помощи", true),
                                new EntityFieldPropertyMetaDescriptor("year", "integer", "Год оказания материальной помощи", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(87,
                new ModularExtractMetaDescription("87", "1.87", "Об однократном повышении государственной академической стипендии", "87", "grantRiseStuExtract",
                        new EntityMetaDescriptor("grantRiseStuExtract", "Об однократном повышении государственной академической стипендии",
                                new EntityFieldPropertyMetaDescriptor("grantRiseAmount", "double", "Размер повышения государственной академической стипендии", true),
                                new EntityFieldPropertyMetaDescriptor("month", "integer", "Месяц повышения государственной академической стипендии", true),
                                new EntityFieldPropertyMetaDescriptor("year", "integer", "Год повышения государственной академической стипендии", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(88,
                new ModularExtractMetaDescription("88", "1.88", "О закреплении за специализацией (профилем)", "88", "profileAssignStuExtract",
                        new EntityMetaDescriptor("profileAssignStuExtract", "О закреплении за специализацией (профилем)", "commonStuExtract")
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(89,
                new ModularExtractMetaDescription("89", "1.89", "О выходе из отпуска по беременности и родам", "89", "weekendPregnancyOutStuExtract",
                        new EntityMetaDescriptor("weekendPregnancyOutStuExtract", "О выходе из отпуска по беременности и родам",
                                new EntityFieldPropertyMetaDescriptor("weekendOutDate", "timestamp", "Дата выхода из отпуска", true),
                                new EntityFieldPropertyMetaDescriptor("resumeGrantPay", "boolean", "Возобновить выплату стипендии", true),
                                new EntityFieldPropertyMetaDescriptor("grantSum", "trimmedstring", "Размер стипендии"),
                                new EntityFieldPropertyMetaDescriptor("grantPayBeginDate", "timestamp", "Дата начала выплаты стипендии"),
                                new EntityFieldPropertyMetaDescriptor("grantPayEndDate", "timestamp", "Дата окончания выплаты стипендии"),
                                new EntityFieldManyToOneMetaDescriptor("studentStatusOld", "studentStatus", "Состояние студента на момент проведения приказа", true),
                                new EntityFieldManyToOneMetaDescriptor("groupNew", "group", "Новая группа", true),
                                new EntityFieldManyToOneMetaDescriptor("groupOld", "group", "Предыдущая группа студента", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(90,
                new ModularExtractMetaDescription("90", "1.90", "О выходе из отпуска по уходу за ребенком", "90", "weekendChildCareOutStuExtract",
                        new EntityMetaDescriptor("weekendChildCareOutStuExtract", "О выходе из отпуска по уходу за ребенком",
                                new EntityFieldPropertyMetaDescriptor("weekendOutDate", "timestamp", "Дата выхода из отпуска", true),
                                new EntityFieldPropertyMetaDescriptor("resumeGrantPay", "boolean", "Возобновить выплату стипендии", true),
                                new EntityFieldPropertyMetaDescriptor("grantSum", "trimmedstring", "Размер стипендии"),
                                new EntityFieldPropertyMetaDescriptor("grantPayBeginDate", "timestamp", "Дата начала выплаты стипендии"),
                                new EntityFieldPropertyMetaDescriptor("grantPayEndDate", "timestamp", "Дата окончания выплаты стипендии"),
                                new EntityFieldPropertyMetaDescriptor("hasDebts", "boolean", "Разница в учебных планах", true),
                                new EntityFieldPropertyMetaDescriptor("deadlineDate", "timestamp", "Срок ликвидации задолжности"),
                                new EntityFieldManyToOneMetaDescriptor("groupNew", "group", "Новая группа", true),
                                new EntityFieldManyToOneMetaDescriptor("groupOld", "group", "Предыдущая группа студента", true),
                                new EntityFieldManyToOneMetaDescriptor("studentStatusOld", "studentStatus", "Состояние студента на момент проведения приказа", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(91,
                new ModularExtractMetaDescription("91", "1.91", "О назначении надбавки к государственной академической стипендии", "91", "acadGrantBonusAssignStuExtract",
                        new EntityMetaDescriptor("acadGrantBonusAssignStuExtract", "О назначении надбавки к государственной академической стипендии",
                                new EntityFieldPropertyMetaDescriptor("grantBonusSize", "trimmedstring", "Размер надбавки к стипендии", true),
                                new EntityFieldPropertyMetaDescriptor("beginDate", "timestamp", "Дата начала выплаты надбавки к стипендии", true),
                                new EntityFieldPropertyMetaDescriptor("endDate", "timestamp", "Дата окончания выплаты надбавки к стипендии", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(92,
                new ModularExtractMetaDescription("92", "1.92", "О переводе с курса на следующий курс со сменой территориального подразделения", "92", "transferCourseWithChangeTerrOuStuExtract",
                        new EntityMetaDescriptor("transferCourseWithChangeTerrOuStuExtract", "О переводе с курса на следующий курс со сменой территориального подразделения", "commonStuExtract"
                        )
                )
        );

        
        EXTRACT_DESCRIPTIONS_MAP.put(93,
                new ModularExtractMetaDescription("93", "1.93", "О распределении в группу", "93", "groupAssignStuExtract",
                        new EntityMetaDescriptor("groupAssignStuExtract", "О распределении в группу",
                                new EntityFieldManyToOneMetaDescriptor("groupOld", "group", "Группа на момент проведения приказа"),
                                new EntityFieldManyToOneMetaDescriptor("groupNew", "group", "Новая группа", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(94,
                new ModularExtractMetaDescription("94", "1.94", "О предоставлении отпуска по уходу за ребенком до 3 лет", "94", "weekendChildThreeStuExtract",
                        new EntityMetaDescriptor("weekendChildThreeStuExtract", "О предоставлении отпуска по уходу за ребенком до 3 лет",
                                new EntityFieldPropertyMetaDescriptor("beginDate", "timestamp", "Дата начала отпуска", true),
                                new EntityFieldPropertyMetaDescriptor("endDate", "timestamp", "Дата окончания отпуска", true),
                                new EntityFieldManyToOneMetaDescriptor("studentStatusNew", "studentStatus", "Новое состояние студента", true),
                                new EntityFieldManyToOneMetaDescriptor("studentStatusOld", "studentStatus", "Предыдущее состояние студента", true),
                                new EntityFieldPropertyMetaDescriptor("payBenefit", "boolean", "Выплатить пособие по беременности и родам", true),
                                new EntityFieldPropertyMetaDescriptor("payBenefitBeginDate", "timestamp", "Дата начала выплаты пособия по беременности и родам"),
                                new EntityFieldPropertyMetaDescriptor("payBenefitEndDate", "timestamp", "Дата окончания выплаты пособия по беременности и родам"),
                                new EntityFieldPropertyMetaDescriptor("payOnetimeBenefit", "boolean", "Выплатить единовременное пособие", true),
                                new EntityFieldPropertyMetaDescriptor("payBenefitText", "trimmedstring", "Текст выплаты пособия по беременности и родам"),
                                new EntityFieldPropertyMetaDescriptor("payOnetimeBenefitText", "trimmedstring", "Текст выплаты единовременного пособия"),
                                new EntityFieldPropertyMetaDescriptor("prevBeginDate", "timestamp", "Дата начала отпуска по предыдущему приказу", true),
                                new EntityFieldPropertyMetaDescriptor("prevEndDate", "timestamp", "Дата окончания отпуска по предыдущему приказу", true),
                                new EntityFieldPropertyMetaDescriptor("freeAttendance", "boolean", "Свободное посещение занятий", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(95,
                new ModularExtractMetaDescription("95", "1.95", "Об установлении индивидуального срока сдачи сессии(досрочная сдача сессии)", "95", "sessionIndividualAheadStuExtract",
                        new EntityMetaDescriptor("sessionIndividualAheadStuExtract", "Об установлении индивидуального срока сдачи сессии(досрочная сдача сессии)",
                                new EntityFieldPropertyMetaDescriptor("sessionBeginDate", "timestamp", "Дата начала сессии", true),
                                new EntityFieldPropertyMetaDescriptor("sessionEndDate", "timestamp", "Дата окончания сессии", true),
                                new EntityFieldManyToOneMetaDescriptor("sessionType", "yearDistributionPart", "Вид сессии", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(96,
                new ModularExtractMetaDescription("96", "1.96", "О допуске к защите выпускной квалификационной работы", "96", "admitToDiplomaWithoutExamStuExtract",
                        new EntityMetaDescriptor("admitToDiplomaWithoutExamStuExtract", "О допуске к защите выпускной квалификационной работы")
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(97,
                new ModularExtractMetaDescription("97", "1.97", "О приеме гражданства Российской Федерации", "97", "adoptRussianCitizenshipStuExtract",
                        new EntityMetaDescriptor("adoptRussianCitizenshipStuExtract", "О приеме гражданства Российской Федерации",
                                new EntityFieldPropertyMetaDescriptor("citizenshipDate", "timestamp", "Дата принятия гражданства РФ", true),
                                new EntityFieldPropertyMetaDescriptor("firstNameNew", "trimmedstring", "Новое имя", true),
                                new EntityFieldPropertyMetaDescriptor("lastNameNew", "trimmedstring", "Новая фамилия", true),
                                new EntityFieldPropertyMetaDescriptor("middleNameNew", "trimmedstring", "Новое отчество"),
                                new EntityFieldPropertyMetaDescriptor("cardSeria", "trimmedstring", "Серия"),
                                new EntityFieldPropertyMetaDescriptor("cardNumber", "trimmedstring", "Номер"),
                                new EntityFieldPropertyMetaDescriptor("birthDate", "timestamp", "Дата рождения"),
                                new EntityFieldPropertyMetaDescriptor("birthPlace", "trimmedstring", "Место рождения"),
                                new EntityFieldPropertyMetaDescriptor("issuanceDate", "timestamp", "Дата выдачи удостоверения"),
                                new EntityFieldPropertyMetaDescriptor("issuancePlace", "trimmedstring", "Кем выдано удостоверение"),
                                new EntityFieldManyToOneMetaDescriptor("nationality", "nationality", "Новая национальность"),
                                new EntityFieldManyToOneMetaDescriptor("sex", "sex", "Новый пол"),
                                new EntityFieldManyToOneMetaDescriptor("citizenship", "addressCountry", "Новое гражданство"),
                                new EntityFieldManyToOneMetaDescriptor("cardType", "identityCardType", "Новый тип удостоверения личности"),
                                new EntityFieldManyToOneMetaDescriptor("lastActiveIdentityCard", "identityCard", "Предыдущее активное удостоверение личности"),
                                new EntityFieldManyToOneMetaDescriptor("identityCardNew", "identityCard", "Новое удостоверение личности")
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(98,
                new ModularExtractMetaDescription("98", "1.98", "Об аттестации практик студентов на основании академической справки", "98", "attestationPracticStuExtract",
                        new EntityMetaDescriptor("attestationPracticStuExtract", "Об аттестации практик студентов на основании академической справки",
                                new EntityFieldPropertyMetaDescriptor("practiceType", "trimmedstring", "Тип практики", true),
                                new EntityFieldPropertyMetaDescriptor("practiceDuration", "integer", "Объем(недель)", true),
                                new EntityFieldManyToOneMetaDescriptor("course", "course", "Курс прохождения практики")
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(99,
                new ModularExtractMetaDescription("99", "1.99", "О прекращении выплаты академической стипендии", "99", "acadGrantPaymentStopStuExtract",
                        new EntityMetaDescriptor("acadGrantPaymentStopStuExtract", "О прекращении выплаты академической стипендии",
                                new EntityFieldManyToOneMetaDescriptor("acadGrantExtract", "abstractStudentExtract", "Выписка о назначении академической стипендии"),
                                new EntityFieldPropertyMetaDescriptor("acadGrantOrderNumber", "trimmedstring", "Номер приказа о назначении академической стипендии", true),
                                new EntityFieldPropertyMetaDescriptor("acadGrantOrderDate", "timestamp", "Дата приказа о назначении академической стипендии", true),
                                new EntityFieldPropertyMetaDescriptor("acadGrantPaymentStopDate", "timestamp", "Дата прекращения выплаты стипендии", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(100,
                new ModularExtractMetaDescription("100", "1.100", "О прекращении выплаты социальной стипендии", "100", "socGrantPaymentStopStuExtract",
                        new EntityMetaDescriptor("socGrantPaymentStopStuExtract", "О прекращении выплаты социальной стипендии",
                                new EntityFieldManyToOneMetaDescriptor("socGrantExtract", "abstractStudentExtract", "Выписка о назначении социальной стипендии"),
                                new EntityFieldPropertyMetaDescriptor("socGrantOrderNumber", "trimmedstring", "Номер приказа о назначении социальной стипендии", true),
                                new EntityFieldPropertyMetaDescriptor("socGrantOrderDate", "timestamp", "Дата приказа о назначении социальной стипендии", true),
                                new EntityFieldPropertyMetaDescriptor("socGrantPaymentStopDate", "timestamp", "Дата прекращения выплаты стипендии", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(101,
                new ModularExtractMetaDescription("101", "1.101", "О прекращении выплаты надбавки к государственной академической стипендии ", "101", "acadGrantBonusPaymentStopStuExtract",
                        new EntityMetaDescriptor("acadGrantBonusPaymentStopStuExtract", "О прекращении выплаты надбавки к государственной академической стипендии ",
                                new EntityFieldManyToOneMetaDescriptor("acadGrantBonusExtract", "abstractStudentExtract", "Выписка о назначении надбавки к академической стипендии"),
                                new EntityFieldPropertyMetaDescriptor("acadGrantBonusOrderNumber", "trimmedstring", "Номер приказа о назначении надбавки к академической стипендии", true),
                                new EntityFieldPropertyMetaDescriptor("acadGrantBonusOrderDate", "timestamp", "Дата приказа о назначении надбавки к академической стипендии", true),
                                new EntityFieldPropertyMetaDescriptor("acadGrantBonusPaymentStopDate", "timestamp", "Дата прекращения выплаты надбавки к академической стипендии", true),
                                new EntityFieldPropertyMetaDescriptor("acadGrantBonusReason", "trimmedstring", "Причина назначения надбавки к академической стипендии", true)
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(102,
                new ModularExtractMetaDescription("102", "1.102", "Об аттестации практик студентов имеющих стаж работы по профилю обучения", "102", "attestationPracticWithServiceRecStuExtract",
                        new EntityMetaDescriptor("attestationPracticWithServiceRecStuExtract", "Об аттестации практик студентов имеющих стаж работы по профилю обучения",
                                new EntityFieldPropertyMetaDescriptor("practiceType", "trimmedstring", "Тип практики", true),
                                new EntityFieldPropertyMetaDescriptor("practiceDuration", "integer", "Объем(недель)", true),
                                new EntityFieldManyToOneMetaDescriptor("course", "course", "Курс прохождения практики"),
                                new EntityFieldManyToOneMetaDescriptor("contact", "juridicalContactor", "Контактное лицо"),
                                new EntityFieldPropertyMetaDescriptor("practicePlace", "trimmedstring", "Место прохождения практики")
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(103,
                new ModularExtractMetaDescription("103", "1.103", "Об аттестации практик студентов, прошедших стажировку", "103", "attestationPracticWithProbationStuExtract",
                        new EntityMetaDescriptor("attestationPracticWithProbationStuExtract", "Об аттестации практик студентов, прошедших стажировку",
                                new EntityFieldPropertyMetaDescriptor("practiceType", "trimmedstring", "Тип практики", true),
                                new EntityFieldPropertyMetaDescriptor("practiceDuration", "integer", "Объем(недель)", true),
                                new EntityFieldPropertyMetaDescriptor("practiceBeginDate", "timestamp", "Дата начала", true),
                                new EntityFieldPropertyMetaDescriptor("practiceEndDate", "timestamp", "Дата окончания", true),
                                new EntityFieldPropertyMetaDescriptor("practicePlace", "trimmedstring", "Место прохождения стажировки", true),
                                new EntityFieldPropertyMetaDescriptor("practiceOrderNum", "trimmedstring", "Номер приказа(о прохождении практики)", true),
                                new EntityFieldPropertyMetaDescriptor("practiceOrderDate", "timestamp", "Дата приказа(о прохождении практики)", true),
                                new EntityFieldManyToOneMetaDescriptor("course", "course", "Курс прохождения практики")
                        )
                )
        );

        EXTRACT_DESCRIPTIONS_MAP.put(104,
                new ModularExtractMetaDescription("104", "1.104", "О переводе между территориальными подразделениями", "104", "transferBetweenTerritorialStuExtract",
                        new EntityMetaDescriptor("transferBetweenTerritorialStuExtract", "О переводе между территориальными подразделениями", "commonStuExtract",
                                new EntityFieldPropertyMetaDescriptor("changeEduPlan", "boolean", "Разница в учебных планах", true),
                                new EntityFieldPropertyMetaDescriptor("changeEduPlanDeadlineDate", "timestamp", "Дата ликвидации")
                        )
                )
        );
    }
}