package ru.tandemservice.movestudent.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudent_2x6x9_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.9")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность recordBookLossPenaltyStuExtract

		// у сущности появилась таблица
		// ранее сущность не имела своей таблицы, теперь ее нужно создать и заполнить данными
		{
			// создать таблицу (все колонки как nullable)
			DBTable dbt = new DBTable("rcrdbklsspnltystextrct_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("withoutreprimand_p", DBType.BOOLEAN)
			);
			tool.createTable(dbt);

			// заполнить данными новую таблицу
			short discriminator0 = tool.entityCodes().get("recordBookLossPenaltyStuExtract");
			java.lang.Boolean defaultWithoutReprimand = false;
			tool.executeUpdate("insert into rcrdbklsspnltystextrct_t(id, withoutreprimand_p) select parent.id, ? from modularstudentextract_t parent inner join abstractstudentextract_t root on root.id=parent.id where root.discriminator in(" + discriminator0 + ")", defaultWithoutReprimand);

			// установить NOT NULL обязательным колонкам
			tool.setColumnNullable("rcrdbklsspnltystextrct_t", "withoutreprimand_p", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность studentCardLossPenaltyStuExtract

		// у сущности появилась таблица
		// ранее сущность не имела своей таблицы, теперь ее нужно создать и заполнить данными
		{
			// создать таблицу (все колонки как nullable)
			DBTable dbt = new DBTable("stdntcrdlsspnltystextrct_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("withoutreprimand_p", DBType.BOOLEAN)
			);
			tool.createTable(dbt);

			// заполнить данными новую таблицу
			short discriminator0 = tool.entityCodes().get("studentCardLossPenaltyStuExtract");
			java.lang.Boolean defaultWithoutReprimand = false;
			tool.executeUpdate("insert into stdntcrdlsspnltystextrct_t(id, withoutreprimand_p) select parent.id, ? from modularstudentextract_t parent inner join abstractstudentextract_t root on root.id=parent.id where root.discriminator in(" + discriminator0 + ")", defaultWithoutReprimand);

			// установить NOT NULL обязательным колонкам
			tool.setColumnNullable("stdntcrdlsspnltystextrct_t", "withoutreprimand_p", false);

		}


    }
}
