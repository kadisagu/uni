/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLDataSource;

/**
 * @author Nikolay Fedorovskih
 * @since 06.12.2013
 */
public abstract class DataSourceFlexJoin implements IFlexJoin
{
    private String _alias;
    private String _leftAlias;
    private DQLJoinType _joinType;
    private boolean _required;

    protected DataSourceFlexJoin(String alias, String leftAlias, DQLJoinType joinType, boolean required)
    {
        _alias = alias;
        _leftAlias = leftAlias;
        _joinType = joinType;
        _required = required;
    }

    @Override
    public boolean isRequired()
    {
        return _required;
    }

    protected abstract IDQLDataSource createDataSource();

    @Override
    public void addToBuilder(DQLSelectBuilder builder)
    {
        builder.joinDataSource(_leftAlias, _joinType, createDataSource(), _alias,
                               DQLExpressions.eq(_leftAlias + ".id", _alias + ".id"));
    }

    @Override
    public String getAlias()
    {
        return _alias;
    }

    @Override
    public String getLeftAlias()
    {
        return _leftAlias;
    }
}