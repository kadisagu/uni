/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.component.modularextract.e105.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AcceleratedLearningStuExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.base.bo.UniStudent.logic.sessionTransferProtocol.ISessionTransferProtocolProperty;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 25.10.2013
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<AcceleratedLearningStuExtract, Model> implements IDAO
{
    @Override
    protected AcceleratedLearningStuExtract createNewInstance()
    {
        return new AcceleratedLearningStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.GENITIVE;
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        AcceleratedLearningStuExtract extract = model.getExtract();
        final Student student = extract.getEntity();

        model.setEduModel(MoveStudentDaoFacade.getCommonExtractUtil().createEduModel(model.isAddForm(), model.getExtract()));
        model.getEduModel().setGroupModel(new GroupSelectModel(null, model, model, null).showChildLevels(true));
        model.getEduModel().setCourse(student.getCourse());
        model.getEduModel().setGroup(student.getGroup());

        if (model.isAddForm())
        {
            extract.setIndividualEduPlan(UniStudentManger.instance().individualEduPlanDao().getIndividualEduPlan(student));
            extract.setEducationOrgUnitNew(student.getEducationOrgUnit());
            extract.setEducationOrgUnitOld(student.getEducationOrgUnit());
            extract.setCourseNew(student.getCourse());
            extract.setCourseOld(student.getCourse());
            extract.setCompensationTypeNew(student.getCompensationType());
            extract.setCompensationTypeOld(student.getCompensationType());
            extract.setStudentStatusOld(student.getStatus());
            extract.setGroupOld(student.getGroup());
        }

        ISelectModel protocolModel = UniStudentManger.instance().sessionTransferProtocolDao().getSessionTransferProtocolModel(student);
        model.setHasUniSession(protocolModel != null);
        if (model.isHasUniSession())
            model.setProtocolModel(protocolModel);
        if (model.isEditForm())
        {
            if (model.isHasUniSession())
            {
                model.setProtocol(UniStudentManger.instance().sessionTransferProtocolDao().getSessionTransferProtocolDocument(
                        extract.getProtocolNumber(), extract.getProtocolDate(), student));
            }
        }

    }

    @Override
    public void update(Model model)
    {
        ISessionTransferProtocolProperty property = model.getProtocol();
        if (property != null)
        {
            model.getExtract().setProtocolNumber(property.getNumber());
            model.getExtract().setProtocolDate(property.getProtocolDate());
        }

        super.update(model);
    }
}