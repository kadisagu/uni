/* $Id: extractPrint.vm 8576 2009-06-18 14:17:38Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e17;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.IStudentListParagraphPrintFormatter;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.AdmitToPassDiplomaWorkStuListExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author ListExtractComponentGenerator
 * @since 04.02.2011
 */
public class AdmitToPassDiplomaWorkStuListExtractPrint implements IPrintFormCreator<AdmitToPassDiplomaWorkStuListExtract>, IListParagraphPrintFormCreator<AdmitToPassDiplomaWorkStuListExtract>, IStudentListParagraphPrintFormatter
{
    @Override
    public String formatSingleStudent(Student student, int extractNumber)
    {
        StringBuilder buffer = new StringBuilder();
        buffer.append("\\par ").append(extractNumber).append(".  ");
        buffer.append(student.getPerson().getFullFio());
        return buffer.toString();
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, AdmitToPassDiplomaWorkStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, AdmitToPassDiplomaWorkStuListExtract firstExtract)
    {
        EducationLevels eduLevel = EducationOrgUnitUtil.getParentLevel(firstExtract.getEntity().getGroup().getEducationOrgUnit().getEducationLevelHighSchool());

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract)
                .put("course", firstExtract.getEntity().getGroup().getCourse().getTitle())
                .put("group", firstExtract.getEntity().getGroup().getTitle())
                .put("speciality", eduLevel != null ? eduLevel.getTitle() : "")
                .put("developCondition_G", CommonExtractPrint.getDevelopConditionStr_G(firstExtract.getDevelopCondition()))
                .put("developForm_G", CommonExtractPrint.getDevelopFormStr_G(firstExtract.getDevelopForm()));

        UniRtfUtil.initEducationType(injectModifier, firstExtract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool(), "");
        CommonExtractPrint.initOrgUnit(injectModifier, firstExtract.getEntity().getEducationOrgUnit(), "orgUnit", "");

        return injectModifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, AdmitToPassDiplomaWorkStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, AdmitToPassDiplomaWorkStuListExtract firstExtract)
    {

    }

}
