/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeGroupCaptain.AddEdit;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditModel;
import ru.tandemservice.movestudent.entity.custom.ChangeGroupCaptainAction;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public class Model extends BaseCustomActionAddEditModel<ChangeGroupCaptainAction>
{
    private boolean _isCaptain;

    public String getActionTitle()
    {
        return isCaptain() ? "Освободить от обязанностей старосты в группе " + getStudent().getGroup().getTitle() : "Назначить старостой группы";
    }

    public boolean isCaptain()
    {
        return _isCaptain;
    }

    public void setCaptain(boolean captain)
    {
        _isCaptain = captain;
    }
}