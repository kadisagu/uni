/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.modularextract.Pub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.BaseCustomActionPubModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubController;
import ru.tandemservice.movestudent.entity.custom.ModifyModularStuExtract;

import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 28.08.2013
 */
public class Controller extends ModularStudentExtractPubController<ModifyModularStuExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        Model model = component.getModel();

        for (Map.Entry<String, String> entry : model.getActionMap().entrySet())
        {
            component.createChildRegion(entry.getKey(), new ComponentActivator(
                    entry.getValue() + ".Pub", new ParametersMap()
                    .add(BaseCustomActionPubModel.EXTRACT_ID_BIND, model.getExtract().getId())
            ));
        }
    }
}