/* $Id$ */
package ru.tandemservice.movestudent.ws.gupmsr;

import org.xml.sax.SAXException;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;

/**
 * @author Ekaterina Zvereva
 * @since 06.02.2015
 */
public class TestMSR
{

    private static String _tandemUniPath = "D:/uni_290/tandem.uni/modules/movestudent";
    private static String schemaPath = _tandemUniPath + "/src/main/java/ru/tandemservice/movestudent/ws/gupmsr";


    public static void main(String[] args) throws Exception
    {
        // 1. Поиск и создание экземпляра фабрики для языка XML Schema
        SchemaFactory factory =
                SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");

        // 2. Компиляция схемы
        // Схема загружается в объект типа java.io.File, но вы также можете использовать
        // классы java.net.URL и javax.xml.transform.Source
        File schemaLocation = new File(schemaPath + "/incoming-file.xsd");
        Schema schema = factory.newSchema(schemaLocation);

        // 3. Создание валидатора для схемы
        Validator validator = schema.newValidator();

        String fileName = "999-151104-002-0392.xml";
        File file = new File("D:/mirea/" + fileName);
        // 4. Разбор проверяемого документа
        Source source = new StreamSource(file);

        // 5. Валидация документа
        try {
            validator.validate(source);
//            System.out.println(args[0] + " is valid.");
        }
        catch (SAXException ex) {
//            System.out.println(args[0] + " is not valid because ");
            System.out.println(ex.getMessage());
        }
    }
}
