/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.uni.util.groovy.IScriptInstance;

/**
 * @author Nikolay Fedorovskih
 * @since 11.07.2013
 */
public interface IMoveStudentInjectModifier extends IScriptInstance
{
    /**
     * Заполнение меток при печати выписки из сборного приказа или индивидуального приказа.
     *
     * @param modifier контейнер для меток
     * @param extract выписка. extract.entity - студент.
     */
    void modularExtractModifier(RtfInjectModifier modifier, ModularStudentExtract extract);

    /**
     * Заполнение табличных меток при печати выписки из сборного приказа.
     *
     * @param modifier контейнер для меток
     * @param extract выписка. extract.entity - студент.
     */
    void modularExtractTableModifier(RtfTableModifier modifier, ModularStudentExtract extract);

    /**
     * Заполнение меток при печати сборного приказа.
     *
     * @param modifier контейнер для меток
     * @param order приказ
     */
    void modularOrderModifier(RtfInjectModifier modifier, StudentModularOrder order);

    /**
     * Заполнение табличных меток при печати сборного приказа.
     *
     * @param modifier контейнер для меток
     * @param order приказ
     */
    void modularOrderTableModifier(RtfTableModifier modifier, StudentModularOrder order);

    /**
     * Заполнение меток при печати индивидуального приказа.
     *
     * @param modifier контейнер для меток
     * @param order приказ
     * @param extract единственная выписка в приказе
     */
    void individualOrderModifier(RtfInjectModifier modifier, StudentModularOrder order, ModularStudentExtract extract);

    /**
     * Заполнение табличных меток при печати индивидуального приказа.
     *
     * @param modifier контейнер для меток
     * @param order приказ
     * @param extract единственная выписка в приказе
     */
    void individualOrderTableModifier(RtfTableModifier modifier, StudentModularOrder order, ModularStudentExtract extract);

    /**
     * Заполнение меток при печати выписки из списочного приказа.
     *
     * @param modifier контейнер для меток
     * @param extract выписка. extract.entity - студент.
     */
    void listExtractModifier(RtfInjectModifier modifier, ListStudentExtract extract);

    /**
     * Заполнение табличных меток при печати выписки из списочного приказа.
     *
     * @param modifier контейнер для меток
     * @param extract выписка. extract.entity - студент.
     */
    void listExtractTableModifier(RtfTableModifier modifier, ListStudentExtract extract);

    /**
     * Заполнение меток при печати списочного приказа.
     *
     * @param modifier контейнер для меток
     * @param order списочный приказ
     * @param firstExtract первая выписка из первого параграфа
     */
    void listOrderModifier(RtfInjectModifier modifier, StudentListOrder order, ListStudentExtract firstExtract);

    /**
     * Заполнение табличных меток при печати списочного приказа.
     *
     * @param modifier контейнер для меток
     * @param order списочный приказ
     */
    void listOrderTableModifier(RtfTableModifier modifier, StudentListOrder order);

    /**
     * Заполнение меток при печати параграфов (и подпараграфов) списочного приказа.
     *
     * @param modifier контейнер для меток
     * @param paragraph параграф списочного приказ
     * @param firstExtract первая выписка параграфа (подпараграфа)
     */
    void listParagraphModifier(RtfInjectModifier modifier, StudentListParagraph paragraph, ListStudentExtract firstExtract);

    /**
     * Заполнение табличных меток при печати параграфов (и подпараграфов) списочного приказа.
     *
     * @param modifier контейнер для меток
     * @param paragraph параграф списочного приказ
     * @param firstExtract первая выписка параграфа (подпараграфа)
     */
    void listParagraphTableModifier(RtfTableModifier modifier, StudentListParagraph paragraph, ListStudentExtract firstExtract);
}