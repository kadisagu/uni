/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.ui.StatusCodesRelation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

/**
 * @author Ekaterina Zvereva
 * @since 18.12.2014
 */

@Configuration
public class MovestudentMSRSettingsStatusCodesRelation extends BusinessComponentManager
{

    public static String STATUS_DS = "statusDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(STATUS_DS, statusDSColumn(),studentStatusDSHandler()) )
                .create();
    }

    @Bean
    public ColumnListExtPoint statusDSColumn()
    {
        return columnListExtPointBuilder(STATUS_DS)
                .addColumn(textColumn("status", StudentStatus.title()))
                .addColumn(blockColumn("statusCode", "statusCode"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentStatusDSHandler() {
        return new DefaultSearchDataSourceHandler(this.getName(), StudentStatus.class);
    }
}
