/* $Id: extractPrint.vm 23792 2012-08-10 10:47:17Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e103;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.e98.AttestationPracticStuExtractPrint;
import ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract;

import java.text.SimpleDateFormat;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 25.02.2013
 */
public class AttestationPracticWithProbationStuExtractPrint implements IPrintFormCreator<AttestationPracticWithProbationStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, AttestationPracticWithProbationStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        AttestationPracticStuExtractPrint.injectPracticeLabels(modifier, extract.getPracticeType(), extract.getPracticeCourse(), extract.getPracticeDurationStr(), extract.getEntity());

        modifier.put("practicePlace", extract.getPracticePlace());
        modifier.put("practiceBeginDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPracticeBeginDate()));
        modifier.put("practiceEndDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPracticeEndDate()));
        modifier.put("orderNum", extract.getPracticeOrderNum());
        modifier.put("orderDate", new SimpleDateFormat("«dd» MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(extract.getPracticeOrderDate()));

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}