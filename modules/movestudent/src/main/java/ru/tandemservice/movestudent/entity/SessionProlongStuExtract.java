package ru.tandemservice.movestudent.entity;

import java.util.Date;

import ru.tandemservice.movestudent.entity.gen.SessionProlongStuExtractGen;

/**
 * Выписка из сборного приказа по студенту. Об отстранении от занятий
 */
public class SessionProlongStuExtract extends SessionProlongStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getProlongDateFrom();
    }

    @Override
    public Date getEndDate()
    {
        return getProlongDateTo();
    }
}