/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e48.Pub;

import com.google.common.collect.Lists;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.movestudent.entity.GroupToExtractRelation;
import ru.tandemservice.movestudent.entity.RemoveGroupManagerStuExtract;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 13.08.2012
 */
public class DAO extends ModularStudentExtractPubDAO<RemoveGroupManagerStuExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GroupToExtractRelation.class, "ge");
        builder.where(DQLExpressions.eq(DQLExpressions.property("ge", GroupToExtractRelation.extract().id()), DQLExpressions.value(model.getExtract().getId())));
        builder.column(DQLExpressions.property(GroupToExtractRelation.group().fromAlias("ge"))).distinct();

        List<Group> groupList = builder.createStatement(getSession()).list();

        List<String> groupsTitles = Lists.newArrayList();

        for(Group group : groupList)
        {
            groupsTitles.add(group.getTitle());
        }

        model.setGroupsTitles(groupsTitles);
    }
}
