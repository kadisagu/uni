/* $Id$ */
package ru.tandemservice.movestudent.component.order.other.StudentOtherOrderAddEdit;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.request.IUploadFile;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SimpleListResultBuilder;
import ru.tandemservice.movestudent.component.commons.ExtractTypeSelectModel;
import ru.tandemservice.movestudent.component.commons.OrderCategory;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.EducationLevelDAO;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 11.04.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model)
    {
        model.setAddForm(null == model.getExtractId());
        model.setOtherTypesThreeLevel(MoveStudentDaoFacade.getMoveStudentDao().hasThirdLevelOtherOrders());
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setTermList(DevelopGridDAO.getTermMap().values());

        List<OrderStates> orderStatesList = new ArrayList<>();
        orderStatesList.add(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
        orderStatesList.add(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED));
        model.setOrderStatesList(orderStatesList);

        if (model.isOtherTypesThreeLevel())
        {
            model.setExtractParentTypeListModel(new ExtractTypeSelectModel(OrderCategory.OTHER));
            model.setExtractTypeListModel(new CommonSingleSelectModel()
            {
                @Override
                protected IListResultBuilder createBuilder(String filter, Object o)
                {
                    if (model.getParentExtractType() == null)
                        return new SimpleListResultBuilder<>(Collections.emptyList());

                    DQLSelectBuilder builder = new DQLSelectBuilder()
                            .fromEntity(StudentExtractType.class, "et").column("et")
                            .where(eq(property(StudentExtractType.active().fromAlias("et")), value(Boolean.TRUE)))
                            .where(eq(property(StudentExtractType.parent().fromAlias("et")), value(model.getParentExtractType())));

                    if (StringUtils.isNotEmpty(filter))
                        builder.where(likeUpper(property(StudentExtractType.title().fromAlias("et")), value(CoreStringUtils.escapeLike(filter, true))));

                    if (o != null)
                        builder.where(eq(property("et.id"), commonValue(o, PropertyType.LONG)));

                    builder.order(property(StudentExtractType.title().fromAlias("et")));

                    return new DQLListResultBuilder(builder);
                }
            });
        }
        else
        {
            model.setExtractTypeListModel(new ExtractTypeSelectModel(OrderCategory.OTHER));
        }

        model.setEmployeePostModel(new OrderExecutorSelectModel());

        if (null != model.getStudentId())
            model.setStudent(this.<Student>getNotNull(model.getStudentId()));

        if (null != model.getOrgUnitId())
            model.setOrgUnit(this.<OrgUnit>getNotNull(model.getOrgUnitId()));

        if (model.isEditForm())
        {
            model.setExtract(this.<OtherStudentExtract>getNotNull(model.getExtractId()));
            if (null == model.getStudent()) model.setStudent(model.getExtract().getEntity());
            if (null == model.getOrgUnit()) model.setOrgUnit(model.getExtract().getOrgUnit());

            model.setParagraph(get(StudentOtherParagraph.class, model.getExtract().getParagraph().getId()));
            model.setOrder(get(StudentOtherOrder.class, model.getParagraph().getOrder().getId()));
            model.setOrderState(model.getOrder().getState());

            model.setExtractType(model.getExtract().getType());
            if (null != model.getExtractType().getParent() && !StudentExtractTypeCodes.OTHER_ORDER.equals(model.getExtractType().getParent().getCode()))
                model.setParentExtractType(model.getExtractType().getParent());

            Criteria orderText = getSession().createCriteria(StudentOtherOrderTextRelation.class);
            orderText.add(Restrictions.eq(StudentOtherOrderTextRelation.L_ORDER, model.getOrder()));
            model.setPrintForm((StudentOtherOrderTextRelation) orderText.uniqueResult());

            Criteria basics = getSession().createCriteria(StuOtherOrderToBasicRelation.class, "r");
            basics.createAlias("r." + StuOtherOrderToBasicRelation.L_BASIC, "b");
            basics.add(Restrictions.eq("r." + StuOtherOrderToBasicRelation.L_EXTRACT, model.getExtract()));
            basics.addOrder(Order.asc("b." + StudentOrderBasics.P_TITLE));
            List<StuOtherOrderToBasicRelation> relationList = basics.list();

            model.getSelectedBasicList().clear();
            for (StuOtherOrderToBasicRelation relation : relationList)
                model.getSelectedBasicList().add(relation.getBasic());

            model.getCurrentBasicMap().clear();
            for (StuOtherOrderToBasicRelation relation : relationList)
            {
                model.getCurrentBasicMap().put(relation.getBasic().getId(), relation.getComment());
            }

        }
        else
        {
            model.setExtract(new OtherStudentExtract());
            model.setParagraph(new StudentOtherParagraph());
            model.setOrder(new StudentOtherOrder());

            model.getExtract().setEntity(model.getStudent());
            model.getExtract().setParagraph(model.getParagraph());
            model.getParagraph().setOrder(model.getOrder());
            model.getExtract().setCreateDate(new Date());
            model.getOrder().setCreateDate(new Date());
            model.getOrder().setOrgUnit(model.getOrgUnit());
            model.setOrderState(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED));

            IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
            if (principalContext instanceof EmployeePost)
                model.setEmployeePost((EmployeePost) principalContext);

            Student student = model.getStudent();
            if (null != student)
            {
                model.getExtract().setStudentTitleStr(student.getPerson().getFullFio());
                model.getExtract().setStudentStatusStr(student.getStatus().getTitle());
                model.getExtract().setPersonalNumberStr(String.valueOf(student.getPersonalNumber()));
                model.getExtract().setCourseStr(student.getCourse().getTitle());
                model.getExtract().setGroupStr(null != student.getGroup() ? student.getGroup().getTitle() : "-");
                model.getExtract().setCompensationTypeStr(student.getCompensationType().getShortTitle());
                model.getExtract().setFormativeOrgUnitStr(student.getEducationOrgUnit().getFormativeOrgUnit().getFullTitle());
                model.getExtract().setTerritorialOrgUnitStr(student.getEducationOrgUnit().getTerritorialOrgUnit().getFullTitle());
                model.getExtract().setEducationLevelHighSchoolStr((String) student.getEducationOrgUnit().getEducationLevelHighSchool().getProperty(EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY));
                model.getExtract().setDevelopFormStr(student.getEducationOrgUnit().getDevelopForm().getTitle());
                model.getExtract().setDevelopConditionStr(student.getEducationOrgUnit().getDevelopCondition().getTitle());
                model.getExtract().setDevelopTechStr(student.getEducationOrgUnit().getDevelopTech().getTitle());
                model.getExtract().setDevelopPeriodStr(student.getEducationOrgUnit().getDevelopPeriod().getTitle());
            }
        }

        model.setReasonListModel(new FullCheckSelectModel()
        {
            public List<StudentOrderReasons> getFilteredList()
            {
                return new DQLSelectBuilder().fromEntity(StudentReasonToTypeRel.class, "rel")
                        .column(property(StudentReasonToTypeRel.first().fromAlias("rel")))
                        .where(eq(property(StudentReasonToTypeRel.second().id().fromAlias("rel")), value(model.getExtractType().getId())))
                        .order(property(StudentReasonToTypeRel.first().title().fromAlias("rel")))
                        .createStatement(getSession()).list();
            }

            @Override
            public ListResult findValues(String filter)
            {
                if (null == model.getExtractType()) return ListResult.getEmpty();

                List<StudentOrderReasons> preResult = getFilteredList();
                filter = (filter != null) ? CoreStringUtils.escapeLike(UniStringUtils.escapePattern(filter.trim())).replaceAll("%", ".*") : "";
                List<StudentOrderReasons> list = new ArrayList<>();
                Pattern pattern = Pattern.compile(filter);

                for (StudentOrderReasons item : preResult)
                    if (filter.length() == 0 || pattern.matcher(item.getTitle().toUpperCase()).find())
                        list.add(item);

                return new ListResult<>(list.size() <= 50 ? list : list.subList(0, 50), list.size());
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                StudentOrderReasons item = DataAccessServices.dao().get(StudentOrderReasons.class, (Long) primaryKey);
                if (getFilteredList().contains(item)) return item;
                return null;
            }
        });

        model.setBasicListModel(new FullCheckSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult findValues(String filter)
            {
                if (model.getExtract() == null || model.getExtract().getReason() == null)
                    return ListResult.getEmpty();

                Criteria c = getSession().createCriteria(StudentReasonToBasicRel.class, "r");
                c.createAlias("r." + IEntityRelation.L_SECOND, "s");
                c.add(Restrictions.eq("r." + IEntityRelation.L_FIRST, model.getExtract().getReason()));
                c.setProjection(Projections.property("r." + IEntityRelation.L_SECOND));
                c.add(Restrictions.like("s." + StudentOrderBasics.P_TITLE, CoreStringUtils.escapeLike(filter)).ignoreCase());
                c.addOrder(Order.asc(("s." + StudentOrderBasics.P_TITLE)));
                return new ListResult(c.list());
            }
        });
    }

    @Override
    public void update(Model model)
    {
        Student student = model.getExtract().getEntity();

        model.getOrder().setCreateDate(model.getExtract().getCreateDate());
        model.getOrder().setCommitDateSystem(model.getOrder().getCommitDate());
        model.getOrder().setState(model.getOrderState());

        String responsible = OrderExecutorSelectModel.getExecutor(model.getResponsibleEmployee());
        if (responsible != null) model.getOrder().setResponsible(responsible);
        model.getOrder().setResponsibleEmpl(model.getResponsibleEmployee());

        String executor = OrderExecutorSelectModel.getExecutor(model.getEmployeePost());
        if (executor != null) model.getOrder().setExecutor(executor);
        model.getOrder().setExecutorEmpl(model.getEmployeePost());

        model.getExtract().setType(model.getExtractType());
        model.getExtract().setNumber(1);
        model.getExtract().setCommitted(true);
        model.getExtract().setStudentTitle(student.getPerson().getFullFio());

        if (UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(model.getOrderState().getCode()))
            model.getExtract().setState(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED));
        else if (UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getOrderState().getCode()))
            model.getExtract().setState(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));

        model.getParagraph().setNumber(1);
        getSession().saveOrUpdate(model.getOrder());
        getSession().saveOrUpdate(model.getParagraph());
        getSession().saveOrUpdate(model.getExtract());

        // Есть некий листенер, который считает, что все приказы при формировании должны приобретать статус "Формируется",
        // однако здесь нам нужно явно указать статуст, выбранный пользователем.
        if (UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(model.getOrderState().getCode()))
            model.getExtract().setState(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED));
        else if (UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getOrderState().getCode()))
            model.getExtract().setState(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));

        model.getOrder().setState(model.getOrderState());
        getSession().update(model.getOrder());
        getSession().update(model.getExtract());

        updateBasicList(model);

        IUploadFile uploadFile = model.getUploadFile();
        if (null != uploadFile)
        {

            if (uploadFile.getSize() > 5 * 1024 * 1024)
                throw new ApplicationException("Размер загружаемого файла не должен превышать 5Mb.");

            try
            {
                byte[] content = IOUtils.toByteArray(uploadFile.getStream());
                if ((content == null) || (content.length == 0)) content = new byte[]{};

                if (null != model.getPrintForm())
                {
                    model.getPrintForm().setFileType(CommonBaseUtil.getContentType(uploadFile));
                    model.getPrintForm().setFileName(uploadFile.getFileName());
                    model.getPrintForm().setContent(content);
                    update(model.getPrintForm());
                }
                else
                {
                    StudentOtherOrderTextRelation rel = new StudentOtherOrderTextRelation();
                    rel.setOrder(model.getOrder());
                    rel.setFileType(uploadFile.getContentType());
                    rel.setFileName(uploadFile.getFileName());
                    rel.setContent(content);
                    save(rel);
                }
            }
            catch (final Throwable e)
            {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }

    public void initFieldSettings(Model model)
    {
        if (null == model.getExtractType()) model.setFieldSettings(null);

        OtherStudentFieldsSettings settings = get(OtherStudentFieldsSettings.class, OtherStudentFieldsSettings.studentExtractType(), model.getExtractType());
        if (null == settings && null != model.getExtractType())
        {
            StudentExtractType parentType = model.getExtractType().getParent();
            settings = get(OtherStudentFieldsSettings.class, OtherStudentFieldsSettings.studentExtractType(), parentType);

            while (null != parentType && null == settings)
            {
                settings = get(OtherStudentFieldsSettings.class, OtherStudentFieldsSettings.studentExtractType(), parentType);
                parentType = parentType.getParent();
            }
        }
        model.setFieldSettings(settings);
    }

    private void updateBasicList(Model model)
    {
        if (model.isEditForm())
        {
            for (StuOtherOrderToBasicRelation relation : getList(StuOtherOrderToBasicRelation.class, StuOtherOrderToBasicRelation.L_EXTRACT, model.getExtract()))
                delete(relation);
            getSession().flush();
        }

        for (StudentOrderBasics basic : model.getSelectedBasicList())
        {
            StuOtherOrderToBasicRelation relation = new StuOtherOrderToBasicRelation();
            relation.setBasic(basic);
            relation.setExtract(model.getExtract());
            relation.setComment(model.getCurrentBasicMap().get(basic.getId()));
            save(relation);
        }
    }
}