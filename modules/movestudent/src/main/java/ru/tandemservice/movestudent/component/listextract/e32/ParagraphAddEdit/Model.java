/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e32.ParagraphAddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 01.10.2012
 */
public class Model extends AbstractListParagraphAddEditModel<AcadGrantAssignStuListExtract> implements IGroupModel
{
    private Course _course;
    private Group _group;
    private CompensationType _compensationType;
    private Date _beginDate;
    private Date _endDate;
    private YearDistributionPart _sessionType;

    private ISelectModel _groupListModel;
    private List<CompensationType> _compensationTypeList;
    private ISelectModel _partModel;

    private boolean _compensationTypeDisabled;
    private boolean _sessionTypeDisabled;

    private boolean _fillDefaults;

    private List<Long> _groupManagerIds = new ArrayList<>();

    //Calculate methods
    public String getGrantSizeRubleId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "grantSizeRubleId_" + dataSource.getCurrentEntity().getId();
    }

    public String getGroupManagerBonusSizeRubleId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "groupManagerBounusSizeRubleId_" + dataSource.getCurrentEntity().getId();
    }

    public boolean isNotGroupManager()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return !getGroupManagerIds().contains(dataSource.getCurrentEntity().getId());
    }


    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public Date getBeginDate()
    {
        return _beginDate;
    }

    public void setBeginDate(Date beginDate)
    {
        _beginDate = beginDate;
    }

    public Date getEndDate()
    {
        return _endDate;
    }

    public void setEndDate(Date endDate)
    {
        _endDate = endDate;
    }

    public YearDistributionPart getSessionType()
    {
        return _sessionType;
    }

    public void setSessionType(YearDistributionPart sessionType)
    {
        _sessionType = sessionType;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public ISelectModel getPartModel()
    {
        return _partModel;
    }

    public void setPartModel(ISelectModel partModel)
    {
        _partModel = partModel;
    }

    public boolean isCompensationTypeDisabled()
    {
        return _compensationTypeDisabled;
    }

    public void setCompensationTypeDisabled(boolean compensationTypeDisabled)
    {
        _compensationTypeDisabled = compensationTypeDisabled;
    }

    public boolean isSessionTypeDisabled()
    {
        return _sessionTypeDisabled;
    }

    public void setSessionTypeDisabled(boolean sessionTypeDisabled)
    {
        _sessionTypeDisabled = sessionTypeDisabled;
    }

    public boolean isFillDefaults()
    {
        return _fillDefaults;
    }

    public void setFillDefaults(boolean fillDefaults)
    {
        _fillDefaults = fillDefaults;
    }

    public List<Long> getGroupManagerIds()
    {
        return _groupManagerIds;
    }

    public void setGroupManagerIds(List<Long> groupManagerIds)
    {
        _groupManagerIds = groupManagerIds;
    }
}
