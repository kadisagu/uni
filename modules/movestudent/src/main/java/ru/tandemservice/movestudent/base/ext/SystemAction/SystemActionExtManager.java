/**
 *$Id$
 */
package ru.tandemservice.movestudent.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.movestudent.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

/**
 * @author Alexander Shaburov
 * @since 13.02.13
 */
@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
                .add("movestudent_correctNonCommitedOrdersWithCommitedExtracts", new SystemActionDefinition("movestudent", "correctNonCommitedOrdersWithCommitedExtracts", "onСorrectNonCommitedOrdersWithCommitedExtracts", SystemActionPubExt.MOVESTUDENT_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("movestudent_deleteEmptyOrders", new SystemActionDefinition("movestudent", "deleteEmptyOrders", "onDeleteEmptyOrders", SystemActionPubExt.MOVESTUDENT_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("movestudent_importStudentGupMsr", new SystemActionDefinition("movestudent", "importStudentGupMsr", "onClickImportGupMsrStudents", SystemActionPubExt.MOVESTUDENT_SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();
    }
}
