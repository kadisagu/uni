/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e47.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;

/**
 * @author ListExtractComponentGenerator
 * @since 17.01.2017
 */
public class Controller extends AbstractListParagraphAddEditController<TransferProgramSubjectStuListExtract, IDAO, Model>
{


    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        Model model = getModel(component);
        this.getDao().prepareColumns(model);
    }

    @Override
    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Пол", Student.person().identityCard().sex().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дополнительный статус", Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("status", "Состояние", Model.STATUS_KEY, SimpleFormatter.INSTANCE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Группа", Student.group().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", Student.educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Технология освоения", Student.educationOrgUnit().developTech().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", Student.educationOrgUnit().developCondition().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Срок освоения", Student.educationOrgUnit().developPeriod().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", new String[]{Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_FULL_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", new String[]{Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_FULL_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn(EducationOrgUnit.EDU_LVL_HS_COLUMN_TITLE, Student.educationOrgUnit().educationLevelHighSchool().configurableTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("newStudentEOU", "Новое направление подготовки").setRequired(true));
        dataSource.setOrder("status", OrderDirection.asc);

    }


}
