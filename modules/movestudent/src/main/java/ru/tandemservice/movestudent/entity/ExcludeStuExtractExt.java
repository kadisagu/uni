package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.movestudent.entity.gen.ExcludeStuExtractExtGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. Об отчислении
 */
public class ExcludeStuExtractExt extends ExcludeStuExtractExtGen implements IExcludeExtract
{
    @Override
    public Date getBeginDate()
    {
        return getDate();
    }

    @Override
    public Date getExcludeDate()
    {
        return getDate();
    }
}