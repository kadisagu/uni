/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e27;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.AdmitToPassDiplomaWorkExtStuListExtract;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author ListExtractComponentGenerator
 * @since 13.09.2012
 */
public class AdmitToPassDiplomaWorkExtStuListExtractPrint implements IPrintFormCreator<AdmitToPassDiplomaWorkExtStuListExtract>, IListParagraphPrintFormCreator<AdmitToPassDiplomaWorkExtStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, AdmitToPassDiplomaWorkExtStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);

        EducationOrgUnit educationOrgUnit = extract.getEntity().getGroup().getEducationOrgUnit();
        CommonExtractPrint.initOrgUnit(modifier, educationOrgUnit, "formativeOrgUnitStr", "");

        String workType;
        String qCode = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getSafeQCode();

        if (QualificationsCodes.BAKALAVR.equals(qCode))
             workType = "квалификационной работы (бакалаврской работы)";
        else if (QualificationsCodes.MAGISTR.equals(qCode))
             workType = "квалификационной работы (магистерской диссертации)";
        else workType = "квалификационной работы";

        modifier.put("workType", workType);
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, educationOrgUnit,
                                                               CommonListOrderPrint.getEducationBaseText(extract.getEntity().getGroup()), "fefuShortFastExtendedOptionalText");

        CommonExtractPrint.modifyEducationStr(modifier, educationOrgUnit.getEducationLevelHighSchool().getEducationLevel(), new String[]{"fefuEducationStr", "educationStr"});

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, AdmitToPassDiplomaWorkExtStuListExtract firstExtract)
    {
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, AdmitToPassDiplomaWorkExtStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, AdmitToPassDiplomaWorkExtStuListExtract firstExtract)
    {
    }
}