/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.movestudent.component.customorder.CustomOrderUtils;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.services.UniServiceFacade;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.UnimvDefines;
import ru.tandemservice.unimv.UnimvUtil;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.VisaTask;
import ru.tandemservice.unimv.services.visatask.ISendToCoordinationService;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskHandler;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskService;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 18.11.2008
 */
public abstract class ModularStudentExtractPubDAO<T extends ModularStudentExtract, Model extends ModularStudentExtractPubModel<T>> extends UniDao<Model> implements IModularStudentExtractPubDAO<T, Model>
{
    @Override
    public void prepare(Model model)
    {
        model.setExtract(this.<T>getNotNull(model.getExtractId()));
        AbstractStudentParagraph par = model.getExtract().getParagraph();
        model.setIndividualOrder(par != null && MoveStudentDaoFacade.getMoveStudentDao().isModularOrderIndividual((StudentModularOrder) par.getOrder()));
        model.setExtractProjectTitle(par == null ? "Проект сборного приказа" : "Выписка");

        IAbstractDocument visingDocument = model.getExtract();
        if (par != null && model.isIndividualOrder())
        {
            visingDocument = par.getOrder();
            model.setExtractProjectTitle("Индивидуальный приказ");
        }

        model.setVisingDocument(visingDocument);
        model.setVisingStatus(UnimvDaoFacade.getVisaDao().getVisingStatus(visingDocument));
        model.setSecModel(new CommonPostfixPermissionModel("modularExtract"));

        model.getExtract().getState().getCode();
        model.getExtract().getReason().getTitle();
        model.getExtract().getType().getTitle();
        model.getExtract().getEntity().getPerson().getIdentityCard().getSex().getTitle();
    }

    @Override
    public void doSendToCoordination(Model model, IPersistentPersonable initiator)
    {
        ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
        sendToCoordinationService.init(model.getExtract(), initiator);
        sendToCoordinationService.execute();
    }

    @Override
    public void doSendToFormative(Model model)
    {
        getSession().clear();
        model.setExtract(this.<T>getNotNull(model.getExtractId()));

        //1. надо проверить, что у выписки сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getExtract());
        if (visaTask != null)
            throw new ApplicationException("Нельзя отправить выписку на формирование, так как она на согласовании.");

        if (null != model.getExtract().getParagraph())
            throw new ApplicationException("Нельзя отправить на формирование выписку, уже включенную в приказ.");

        //2. надо сменить состояние на формируется
        model.getExtract().setState(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE));
        update(model.getExtract());
    }

    @Override
    public void doRollback(Model model)
    {
        T extract = model.getExtract();
        CustomOrderUtils.checkRollbackSimpleExtractAvailable(extract);
        MoveDaoFacade.getMoveDao().doRollbackExtract(extract, getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE), null);
    }

    @Override
    public void doSendOrderToCoordination(Model model, IPersistentPersonable initiator)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getExtract().getParagraph().getOrder());
        if (visaTask != null)
            errCollector.add("Нельзя отправить приказ на согласование, так как он уже на согласовании.");

        if (StringUtils.isEmpty(model.getExtract().getParagraph().getOrder().getNumber()))
            errCollector.add("Нельзя отправить приказ на согласование без номера.");

        if (model.getExtract().getParagraph().getOrder().getCommitDate() == null)
            errCollector.add("Нельзя отправить приказ на согласование без даты приказа.");

        if (!errCollector.hasErrors())
        {
            //save print form
            MoveStudentDaoFacade.getMoveStudentDao().saveModularOrderText((StudentModularOrder) model.getExtract().getParagraph().getOrder());

            ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
            sendToCoordinationService.init(model.getExtract().getParagraph().getOrder(), initiator);
            sendToCoordinationService.execute();
        }
    }

    @Override
    public void doSendOrderToFormative(Model model)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getExtract().getParagraph().getOrder());
        if (visaTask != null)
            errCollector.add("Нельзя отправить приказ на формирование, так как он на согласовании.");

        if (!errCollector.hasErrors())
        {
            //2. надо сменить состояние на формируется
            model.getExtract().getParagraph().getOrder().setState(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
            update(model.getExtract().getParagraph().getOrder());
        }
    }

    @Override
    public void doRejectOrder(Model model)
    {
        ITouchVisaTaskService rejectService = UniServiceFacade.getService(ITouchVisaTaskService.REJECT_VISA_TASK_SERVICE_NAME);
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getExtract().getParagraph().getOrder());
        if (visaTask != null)
        {
            rejectService.init(visaTask.getId(), "SYSTEM: Отменено пользователем с публикатора");
            rejectService.execute();
        }
        else
        {
            //задачи нет => документ уже согласован,
            //в историю согласования ничего не попадет. так как нет задачи согласования
            //короче запускаем процедуру отрицательного завершения процедуры согласования
            ITouchVisaTaskHandler touchService = UnimvUtil.findTouchService(model.getExtract().getParagraph().getOrder(), UnimvDefines.VISA_REJECT_SERVICE_MAP);
            touchService.init(model.getExtract().getParagraph().getOrder());
            touchService.execute();
        }
    }

    @Override
    public void doCommitOrder(Model model)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        if (model.getExtract().getParagraph().getOrder().getNumber() == null)
            errCollector.add("Нельзя проводить приказ без номера.");

        if (model.getExtract().getParagraph().getOrder().getCommitDate() == null)
            errCollector.add("Нельзя проводить приказ без даты приказа.");

        if (!errCollector.hasErrors())
        {
            //save print form
            MoveStudentDaoFacade.getMoveStudentDao().saveModularOrderText((StudentModularOrder) model.getExtract().getParagraph().getOrder());

            MoveDaoFacade.getMoveDao().doCommitOrder(model.getExtract().getParagraph().getOrder(), getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED), getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED), null);
        }
    }

    @Override
    public void doRollbackOrder(Model model)
    {
        // у каждого студента из выписок данного приказа смотрим, нет ли у него более поздней проведенной выписки.
        // если такую находим - ругаемся
        StudentModularOrder order = (StudentModularOrder) model.getExtract().getParagraph().getOrder();

        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();
        final DQLSelectBuilder extractDQL = new DQLSelectBuilder()
                .fromEntity(AbstractStudentExtract.class, "ext")
                .column("ext.entity.id")
                .where(in(
                        property("ext.entity"),
                        new DQLSelectBuilder()
                                .fromEntity(AbstractStudentExtract.class, "stu")
                                .column("stu.entity")
                                .where(eq(property(AbstractStudentExtract.paragraph().order().fromAlias("stu")), value(order)))
                                .buildQuery()
                ))
                .where(gt(property("ext.paragraph.order.commitDateSystem"), valueTimestamp(order.getCommitDateSystem())));

        List<Student> students = getList(Student.class, extractDQL.createStatement(getSession()).<Long>list());
        if (students.size() > 0)
            errCollector.add("Нельзя откатить приказ, т.к. для студента " +
                                     model.getExtract().getEntity().getPerson().getFullFio() +
                                     " есть приказы, которые вступили в силу после данного приказа");

        CustomOrderUtils.checkRollbackSimpleOrderAvailable(order);

        if (!errCollector.hasErrors())
            MoveDaoFacade.getMoveDao().doRollbackOrder(order, UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER), null);
    }
}