/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e26.utils;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 10.09.2012
 */
public class AdmitOrderParagraphPartPartDataWrapper implements Comparable<AdmitOrderParagraphPartPartDataWrapper>
{
    private final Group _group;
    private final List<Person> _personList = new ArrayList<>();
    private final ListStudentExtract _firstExtract;

    public AdmitOrderParagraphPartPartDataWrapper(Group group, ListStudentExtract firstExtract)
    {
        _group = group;
        _firstExtract = firstExtract;
    }

    public Group getGroup()
    {
        return _group;
    }

    public List<Person> getPersonList()
    {
        return _personList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof AdmitOrderParagraphPartPartDataWrapper))
            return false;

        AdmitOrderParagraphPartPartDataWrapper admitOrderParagraphPartPartDataWrapper = (AdmitOrderParagraphPartPartDataWrapper) o;

        return _group.equals(admitOrderParagraphPartPartDataWrapper.getGroup());
    }

    @Override
    public int hashCode()
    {
        return _group.hashCode();
    }

    @Override
    public int compareTo(AdmitOrderParagraphPartPartDataWrapper o)
    {
        return _group.getTitle().compareTo(o.getGroup().getTitle());
    }
}