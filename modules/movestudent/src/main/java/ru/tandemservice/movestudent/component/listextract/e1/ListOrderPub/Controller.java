/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e1.ListOrderPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderPub.AbstractListOrderPubController;
import ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderPub.AbstractListOrderPubModel;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

/**
 * @author ListExtractComponentGenerator
 * @since 07.05.2009
 */
public class Controller extends AbstractListOrderPubController<StudentListOrder, IDAO, Model>
{
    @Override
    protected void prepareListDataSource(DynamicListDataSource dataSource, IBusinessComponent component)
    {
        dataSource.addColumn(new SimpleColumn("Группа", AbstractListOrderPubModel.GROUP_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", AbstractListOrderPubModel.EDU_PROGRAM_SUBJECT).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Направленность", AbstractListOrderPubModel.EDU_PROGRAM_SPEC).setClickable(false).setOrderable(false));

    }
}