/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.listextract;

import ru.tandemservice.movestudent.component.customorder.ICustomOrderDao;
import ru.tandemservice.movestudent.entity.custom.ModifyListStuExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 07.10.2013
 */
public class ModifyOrderStuListExtractDao extends UniBaseDao implements IExtractComponentDao<ModifyListStuExtract>
{
    @Override
    public void doCommit(ModifyListStuExtract extract, Map parameters)
    {
        ICustomOrderDao.instance.get().doCommitCustomExtract(extract);
    }

    @Override
    public void doRollback(ModifyListStuExtract extract, Map parameters)
    {
        ICustomOrderDao.instance.get().doRollbackCustomExtract(extract);
    }
}