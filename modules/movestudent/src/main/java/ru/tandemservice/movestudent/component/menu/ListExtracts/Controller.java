/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.menu.ListExtracts;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.component.commons.IMoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.MoveStudentColumns;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.Date;

/**
 * @author vip_delete
 * @since 18.10.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setSettings(component.getSettings());
        model.getSettings().set(IMoveStudentMQBuilder.LAST_NAME, null);

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<ModularStudentExtract> dataSource = UniBaseUtils.createDataSource(component, getDao());
        //dataSource.addColumn(MoveStudentColumns.getCreateDateColumn());
        AbstractColumn linkColumn = new PublisherLinkColumn("Дата приказа", new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, AbstractStudentOrder.P_COMMIT_DATE}).setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public String getComponentName(IEntity entity)
            {
                IAbstractExtract extract = (IAbstractExtract) ((ViewWrapper) entity).getEntity();
                return MoveStudentUtils.getListExtractPubComponent(((StudentExtractType) extract.getType()));
            }
        }).setFormatter(new DateFormatter()
        {
            @Override
            public String format(Date date) {
                return date == null ? "не задана" : super.format(date);
            }
        });
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(MoveStudentColumns.getExtractOrderNumberColumn());
        dataSource.addColumn(MoveStudentColumns.getParagraphNumberColumn());
        dataSource.addColumn(MoveStudentColumns.getListExtractNumberColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractCommitDateSystemColumn());
        dataSource.addColumn(MoveStudentColumns.getFullFioColumn());
        dataSource.addColumn(MoveStudentColumns.getFormativeOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getTerritorialOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getProducingOrgUnitColumn());
        MoveStudentColumns.addEduLevelTitleColumns(dataSource);
        dataSource.addColumn(MoveStudentColumns.getCourseColumn());
        dataSource.addColumn(MoveStudentColumns.getGroupColumn());
        dataSource.addColumn(MoveStudentColumns.getDevelopFormColumn());
        dataSource.addColumn(MoveStudentColumns.getCompensationTypeColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractOrderTypeColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractTypeColumn("Тип выписки"));
        dataSource.addColumn(MoveStudentColumns.getListExtractReasonColumn("Причина приказа"));
        dataSource.addColumn(MoveStudentColumns.getExtractStateColumn("Состояние выписки"));
        dataSource.addColumn(MoveStudentColumns.getExtractPrintColumn(this, getModel(component).getSecModel().getPermission("print_menuList")).setDisabledProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY));
        if(MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
            dataSource.addColumn(MoveStudentColumns.getExtractPrintPdfColumn(this, getModel(component).getSecModel().getPermission("printPdf_menuList")).setDisabledProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY));
        dataSource.setOrder(linkColumn.getName(), OrderDirection.desc);
        getModel(component).setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);

        UniBaseUtils.validateDatesPeriod(model.getSettings(), "commitDateFrom", "commitDateTo");
        UniBaseUtils.validateDatesPeriod(model.getSettings(), "commitDateSystemFrom", "commitDateSystemTo");

        if (!UserContext.getInstance().getErrorCollector().isHasFieldErrors())
        {
            component.saveSettings();
            model.getDataSource().refresh();
        }
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }

    public void onClickExtractPrint(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_EXTRACT_PRINT, new ParametersMap()
                .add("extractId", component.getListenerParameter())
        ));
    }

    public void onClickExtractPrintPdf(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_EXTRACT_PRINT, new ParametersMap()
                .add("extractId", component.getListenerParameter())
                .add("printPdf", Boolean.TRUE)
        ));
    }
}
