/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e20;

import java.util.Map;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.SessionProlongStuExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 07.05.2009
 */
public class SessionProlongStuExtractDao extends UniBaseDao implements IExtractComponentDao<SessionProlongStuExtract>
{
    @Override
    public void doCommit(SessionProlongStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        // заполняем номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        else
        {
            extract.setPrevOrderDate(orderData.getSessionProlongOrderDate());
            extract.setPrevOrderNumber(orderData.getSessionProlongOrderNumber());
            extract.setPrevProlongDateTo(orderData.getSessionProlongDateTo());
        }
        orderData.setSessionProlongOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setSessionProlongOrderNumber(extract.getParagraph().getOrder().getNumber());
        orderData.setSessionProlongDateTo(extract.getProlongDateTo());
        getSession().saveOrUpdate(orderData);
    }

    @Override
    public void doRollback(SessionProlongStuExtract extract, Map parameters)
    {
        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setSessionProlongOrderDate(extract.getPrevOrderDate());
        orderData.setSessionProlongOrderNumber(extract.getPrevOrderNumber());
        orderData.setSessionProlongDateTo(extract.getPrevProlongDateTo());
        getSession().saveOrUpdate(orderData);
    }
}