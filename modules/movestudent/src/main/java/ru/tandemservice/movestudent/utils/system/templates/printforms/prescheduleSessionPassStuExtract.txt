\fi500\ql\keep\keepn\qj\b {extractNumber}. \b0 {studentFIO_G}\b0\i0, {student_D} {course} курса {developForm_G}{developCondition_G}
 формы обучения {orgUnit_G}, направления подготовки «{speciality} ({educationLevel})», группы {group}, {compensationType_G}
 основы разрешить досрочную сдачу {sessionType_G} зачетно-экзаменационной сессии {sessionYear} учебного года {reasonPrint}.\par
Основание: {listBasics}.
