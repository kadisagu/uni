/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e86;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.e86.utils.EmployeePostDecl;
import ru.tandemservice.movestudent.entity.MatAidAssignStuExtract;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 05.09.2012
 */
public class MatAidAssignStuExtractPrint implements IPrintFormCreator<MatAidAssignStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, MatAidAssignStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        String month = CommonBaseDateUtil.getMonthNameDeclined(extract.getMonth(), GrammaCase.PREPOSITIONAL);

	    if(extract.getEntity().getCompensationType().isBudget())
	    {
		    // из стипендиального фонда
		    modifier.put("paymentSource", "из стипендиального фонда");
	    }
	    else
	    {
		    //из внебюджетных средств школы
		    modifier.put("paymentSource", "из внебюджетных средств школы");
	    }
	    modifier.put("protocol", extract.getProtocolNumber());
	    modifier.put("responsibleForPayments", StringUtils.capitalize(EmployeePostDecl.getEmployeeStrDat(extract.getResponsibleForPayments())));
	    modifier.put("responsibleForAgreement", EmployeePostDecl.getEmployeeStrInst(extract.getResponsibleForAgreement()));
	    modifier.put("matchingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getMatchingDate()));
	    modifier.put("grantSize", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGrantSize()));
        modifier.put("year", String.valueOf(extract.getYear()));
        modifier.put("month_P", month);
	    CommonExtractPrint.initFefuGroup(modifier, "fefuGroup", extract.getEntity().getGroup(), extract.getEntity().getEducationOrgUnit().getDevelopForm(), " группы ");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;

    }
}