/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e34.utils;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author Alexander Zhebko
 * @since 03.10.2012
 */
public class FinAidAssignExtractWrapper implements Comparable<FinAidAssignExtractWrapper>
{
    private final Person _person;
    private final Double _grantSize;
	private final Group _group;

    public FinAidAssignExtractWrapper(Person person, Double grantSize, Group group)
    {
        _person = person;
        _grantSize = grantSize;
	    _group = group;
    }

    public Person getPerson()
    {
        return _person;
    }

    public Double getGrantSize()
    {
        return _grantSize;
    }

	public Group getGroup()
	{
		return _group;
	}

	@Override
    public int compareTo(FinAidAssignExtractWrapper o)
    {
        return Person.FULL_FIO_AND_ID_COMPARATOR.compare(_person, o.getPerson());
    }
}