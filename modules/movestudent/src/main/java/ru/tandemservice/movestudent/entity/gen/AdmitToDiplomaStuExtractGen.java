package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AdmitToDiplomaStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О допуске к защите выпускной квалификационной работы(после ГЭ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AdmitToDiplomaStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.AdmitToDiplomaStuExtract";
    public static final String ENTITY_NAME = "admitToDiplomaStuExtract";
    public static final int VERSION_HASH = 1349972613;
    private static IEntityMeta ENTITY_META;

    public static final String P_STATE_EXAMS = "stateExams";
    public static final String P_NOT_NEED_ADMISSION_TO_G_I_A = "notNeedAdmissionToGIA";

    private boolean _stateExams;     // Множественное число для ГЭ
    private boolean _notNeedAdmissionToGIA = false;     // Без допуска к ГИА

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Множественное число для ГЭ. Свойство не может быть null.
     */
    @NotNull
    public boolean isStateExams()
    {
        return _stateExams;
    }

    /**
     * @param stateExams Множественное число для ГЭ. Свойство не может быть null.
     */
    public void setStateExams(boolean stateExams)
    {
        dirty(_stateExams, stateExams);
        _stateExams = stateExams;
    }

    /**
     * @return Без допуска к ГИА. Свойство не может быть null.
     */
    @NotNull
    public boolean isNotNeedAdmissionToGIA()
    {
        return _notNeedAdmissionToGIA;
    }

    /**
     * @param notNeedAdmissionToGIA Без допуска к ГИА. Свойство не может быть null.
     */
    public void setNotNeedAdmissionToGIA(boolean notNeedAdmissionToGIA)
    {
        dirty(_notNeedAdmissionToGIA, notNeedAdmissionToGIA);
        _notNeedAdmissionToGIA = notNeedAdmissionToGIA;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof AdmitToDiplomaStuExtractGen)
        {
            setStateExams(((AdmitToDiplomaStuExtract)another).isStateExams());
            setNotNeedAdmissionToGIA(((AdmitToDiplomaStuExtract)another).isNotNeedAdmissionToGIA());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AdmitToDiplomaStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AdmitToDiplomaStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new AdmitToDiplomaStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "stateExams":
                    return obj.isStateExams();
                case "notNeedAdmissionToGIA":
                    return obj.isNotNeedAdmissionToGIA();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "stateExams":
                    obj.setStateExams((Boolean) value);
                    return;
                case "notNeedAdmissionToGIA":
                    obj.setNotNeedAdmissionToGIA((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "stateExams":
                        return true;
                case "notNeedAdmissionToGIA":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "stateExams":
                    return true;
                case "notNeedAdmissionToGIA":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "stateExams":
                    return Boolean.class;
                case "notNeedAdmissionToGIA":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AdmitToDiplomaStuExtract> _dslPath = new Path<AdmitToDiplomaStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AdmitToDiplomaStuExtract");
    }
            

    /**
     * @return Множественное число для ГЭ. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToDiplomaStuExtract#isStateExams()
     */
    public static PropertyPath<Boolean> stateExams()
    {
        return _dslPath.stateExams();
    }

    /**
     * @return Без допуска к ГИА. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToDiplomaStuExtract#isNotNeedAdmissionToGIA()
     */
    public static PropertyPath<Boolean> notNeedAdmissionToGIA()
    {
        return _dslPath.notNeedAdmissionToGIA();
    }

    public static class Path<E extends AdmitToDiplomaStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Boolean> _stateExams;
        private PropertyPath<Boolean> _notNeedAdmissionToGIA;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Множественное число для ГЭ. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToDiplomaStuExtract#isStateExams()
     */
        public PropertyPath<Boolean> stateExams()
        {
            if(_stateExams == null )
                _stateExams = new PropertyPath<Boolean>(AdmitToDiplomaStuExtractGen.P_STATE_EXAMS, this);
            return _stateExams;
        }

    /**
     * @return Без допуска к ГИА. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToDiplomaStuExtract#isNotNeedAdmissionToGIA()
     */
        public PropertyPath<Boolean> notNeedAdmissionToGIA()
        {
            if(_notNeedAdmissionToGIA == null )
                _notNeedAdmissionToGIA = new PropertyPath<Boolean>(AdmitToDiplomaStuExtractGen.P_NOT_NEED_ADMISSION_TO_G_I_A, this);
            return _notNeedAdmissionToGIA;
        }

        public Class getEntityClass()
        {
            return AdmitToDiplomaStuExtract.class;
        }

        public String getEntityName()
        {
            return "admitToDiplomaStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
