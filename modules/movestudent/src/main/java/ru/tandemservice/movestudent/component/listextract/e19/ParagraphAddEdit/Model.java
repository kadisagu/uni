/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e19.ParagraphAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Date;

/**
 * @author ListExtractComponentGenerator
 * @since 23.03.2010
 */
public class Model extends AbstractListParagraphAddEditModel<TransferDevConditionStuListExtract> implements IGroupModel
{
    private Course _course;
    private Group _group;
    private ISelectModel _groupListModel;

    private boolean _groupChangeEducationOrgUnit;
    
    private String _recordNumber;
    private Date _recordDate;

    private DevelopCondition _developConditionNew;
    private DevelopPeriod _developPeriodNew;

    private ISelectModel _developConditionsListModel;
    private ISelectModel _developPeriodsListModel;
    
    private Date _transferDate;

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        this._course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        this._group = group;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        this._groupListModel = groupListModel;
    }

    public boolean isGroupChangeEducationOrgUnit()
    {
        return _groupChangeEducationOrgUnit;
    }

    public void setGroupChangeEducationOrgUnit(boolean groupChangeEducationOrgUnit)
    {
        this._groupChangeEducationOrgUnit = groupChangeEducationOrgUnit;
    }

    public String getRecordNumber()
    {
        return _recordNumber;
    }

    public void setRecordNumber(String recordNumber)
    {
        this._recordNumber = recordNumber;
    }

    public Date getRecordDate()
    {
        return _recordDate;
    }

    public void setRecordDate(Date recordDate)
    {
        this._recordDate = recordDate;
    }

    public DevelopCondition getDevelopConditionNew()
    {
        return _developConditionNew;
    }

    public void setDevelopConditionNew(DevelopCondition developConditionNew)
    {
        this._developConditionNew = developConditionNew;
    }

    public DevelopPeriod getDevelopPeriodNew()
    {
        return _developPeriodNew;
    }

    public void setDevelopPeriodNew(DevelopPeriod developPeriodNew)
    {
        this._developPeriodNew = developPeriodNew;
    }

    public ISelectModel getDevelopConditionsListModel()
    {
        return _developConditionsListModel;
    }

    public void setDevelopConditionsListModel(ISelectModel developConditionsListModel)
    {
        this._developConditionsListModel = developConditionsListModel;
    }

    public ISelectModel getDevelopPeriodsListModel()
    {
        return _developPeriodsListModel;
    }

    public void setDevelopPeriodsListModel(ISelectModel developPeriodsListModel)
    {
        this._developPeriodsListModel = developPeriodsListModel;
    }

    public boolean isAnyOrderParametersCantBeChanged()
    {
        return !isParagraphOnlyOneInTheOrder();
    }

    public Date getTransferDate()
    {
        return _transferDate;
    }

    public void setTransferDate(Date transferDate)
    {
        this._transferDate = transferDate;
    }
}