/* $Id: extractPrint.vm 8576 2009-06-18 14:17:38Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e22;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.ChangeReviewerStuListExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 19.02.2011
 */
public class ChangeReviewerStuListExtractPrint implements IPrintFormCreator<ChangeReviewerStuListExtract>, IListParagraphPrintFormCreator<ChangeReviewerStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ChangeReviewerStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        StringBuilder reviewerStr = new StringBuilder(extract.getReviewerName());
        reviewerStr.append(", ").append(extract.getReviewerDegree());
        modifier.put("reviewerStr", reviewerStr.toString());
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, ChangeReviewerStuListExtract firstExtract)
    {
        EducationLevels eduLevel = EducationOrgUnitUtil.getParentLevel(firstExtract.getEntity().getGroup().getEducationOrgUnit().getEducationLevelHighSchool());
        EducationOrgUnit eo = firstExtract.getEntity().getEducationOrgUnit();
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract)
                .put("educationLevel", eo.getEducationLevelHighSchool().getEducationLevel().getLevelType().getTitle())
                .put("speciality", eduLevel != null ? eduLevel.getTitle() : "")
                .put("course", firstExtract.getEntity().getGroup().getCourse().getTitle())
                .put("group", firstExtract.getEntity().getGroup().getTitle())
                .put("developCondition_G", CommonExtractPrint.getDevelopConditionStr_G(eo.getDevelopCondition()))
                .put("developForm_G", CommonExtractPrint.getDevelopFormStr_G(eo.getDevelopForm()));
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, ChangeReviewerStuListExtract firstExtract)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();

        int cnt = 1;
        List<String[]> paragraphDataLines = new ArrayList<>();
        for (Object ext : paragraph.getExtractList())
        {
            ChangeReviewerStuListExtract extract = (ChangeReviewerStuListExtract) ext;
            paragraphDataLines.add(new String[]{String.valueOf(cnt++) + ".", extract.getEntity().getPerson().getFullFio(), extract.getReviewerName(), extract.getReviewerDegree()});
        }

        tableModifier.put("T", paragraphDataLines.toArray(new String[][]{}));

        return tableModifier;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, ChangeReviewerStuListExtract firstExtract)
    {
    }
}