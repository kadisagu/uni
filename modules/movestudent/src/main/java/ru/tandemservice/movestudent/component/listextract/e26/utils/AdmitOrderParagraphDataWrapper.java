/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e26.utils;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 07.09.2012
 */
public class AdmitOrderParagraphDataWrapper implements Comparable<AdmitOrderParagraphDataWrapper>
{
    private final Course _course;
    private final CompensationType _compensationType;
    private final DevelopForm _developForm;
    private final DevelopCondition _developCondition;
    private final DevelopTech _developTech;
    private final DevelopPeriod _developPeriod;
    private final Boolean _direction;
    private final OrgUnit _formativeOrgUnit;
    private final OrgUnit _territorialOrgUnit;
    private final String _eduBaseText;
    private final EducationOrgUnit _educationOrgUnit;
    private final ListStudentExtract _firstExtract;

    public AdmitOrderParagraphDataWrapper(Course course, CompensationType compensationType, EducationOrgUnit eduOrgunit, Boolean direction, String eduBaseText, ListStudentExtract firstExtract)
    {
        _course = course;
        _compensationType = compensationType;
        _developForm = eduOrgunit.getDevelopForm();
        _developCondition = eduOrgunit.getDevelopCondition();
        _developTech = eduOrgunit.getDevelopTech();
        _developPeriod = eduOrgunit.getDevelopPeriod();
        _direction = direction;
        _formativeOrgUnit = eduOrgunit.getFormativeOrgUnit();
        _territorialOrgUnit = eduOrgunit.getTerritorialOrgUnit();
        _eduBaseText = eduBaseText;
        _educationOrgUnit = eduOrgunit;
        _firstExtract = firstExtract;
    }

    private final List<AdmitOrderParagraphPartDataWrapper> _paragraphPartList = new ArrayList<>();

    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    public Course getCourse()
    {
        return _course;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public Boolean isDirection()
    {
        return _direction;
    }

    public List<AdmitOrderParagraphPartDataWrapper> getParagraphPartList()
    {
        return _paragraphPartList;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public String getEduBaseText()
    {
        return _eduBaseText;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public int compareTo(AdmitOrderParagraphDataWrapper admitOrderParagraphDataWrapper)
    {
        int result;
        result = ((Integer) _course.getIntValue()).compareTo(admitOrderParagraphDataWrapper.getCourse().getIntValue());
        if (result == 0)
            result = _compensationType.getCode().compareTo(admitOrderParagraphDataWrapper.getCompensationType().getCode());
        if (result == 0)
            result = _developForm.getCode().compareTo(admitOrderParagraphDataWrapper.getDevelopForm().getCode());
        if (result == 0)
            result = _developCondition.getCode().compareTo(admitOrderParagraphDataWrapper.getDevelopCondition().getCode());
        if (result == 0)
            result = _developTech.getCode().compareTo(admitOrderParagraphDataWrapper.getDevelopTech().getCode());
        if (result == 0)
            result = _developPeriod.getCode().compareTo(admitOrderParagraphDataWrapper.getDevelopPeriod().getCode());
        if (result == 0)
            result = _direction.compareTo(admitOrderParagraphDataWrapper.isDirection());
        if (result == 0)
            result = _eduBaseText.compareTo(admitOrderParagraphDataWrapper.getEduBaseText());

        return result;
    }

    @Override
    public int hashCode()
    {
        return _course.hashCode() & _compensationType.hashCode() & _developForm.hashCode() & _developCondition.hashCode() & _developTech.hashCode() & _developPeriod.hashCode() & _direction.hashCode() & _eduBaseText.hashCode();
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof AdmitOrderParagraphDataWrapper))
            return false;
        AdmitOrderParagraphDataWrapper paragraphDataWrapper = (AdmitOrderParagraphDataWrapper) o;
        return (_course.equals(paragraphDataWrapper.getCourse()) &&
                _compensationType.equals(paragraphDataWrapper.getCompensationType()) &&
                _developForm.equals(paragraphDataWrapper.getDevelopForm()) &&
                _developCondition.equals(paragraphDataWrapper.getDevelopCondition()) &&
                _developTech.equals(paragraphDataWrapper.getDevelopTech()) &&
                _developPeriod.equals(paragraphDataWrapper.getDevelopPeriod())) &&
                _direction.equals(paragraphDataWrapper.isDirection()) &&
                _eduBaseText.equals(paragraphDataWrapper.getEduBaseText());
    }
}