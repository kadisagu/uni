package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ChangeScientificAdviserExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О смене научных руководителей»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChangeScientificAdviserExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ChangeScientificAdviserExtract";
    public static final String ENTITY_NAME = "changeScientificAdviserExtract";
    public static final int VERSION_HASH = 1265927795;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_SCIENTIFIC_ADVISER_NAME = "scientificAdviserName";
    public static final String P_PREVIOUS_ORDER_NUMBER = "previousOrderNumber";
    public static final String P_PREVIOUS_ORDER_DATE = "previousOrderDate";

    private Course _course;     // Курс
    private Group _group;     // Группа
    private DevelopForm _developForm;     // Форма освоения
    private PpsEntry _scientificAdviserName;     // ФИО научного руководителя
    private String _previousOrderNumber;     // Номер предыдущего приказа
    private Date _previousOrderDate;     // Дата предыдущего приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return ФИО научного руководителя. Свойство не может быть null.
     */
    @NotNull
    public PpsEntry getScientificAdviserName()
    {
        return _scientificAdviserName;
    }

    /**
     * @param scientificAdviserName ФИО научного руководителя. Свойство не может быть null.
     */
    public void setScientificAdviserName(PpsEntry scientificAdviserName)
    {
        dirty(_scientificAdviserName, scientificAdviserName);
        _scientificAdviserName = scientificAdviserName;
    }

    /**
     * @return Номер предыдущего приказа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPreviousOrderNumber()
    {
        return _previousOrderNumber;
    }

    /**
     * @param previousOrderNumber Номер предыдущего приказа. Свойство не может быть null.
     */
    public void setPreviousOrderNumber(String previousOrderNumber)
    {
        dirty(_previousOrderNumber, previousOrderNumber);
        _previousOrderNumber = previousOrderNumber;
    }

    /**
     * @return Дата предыдущего приказа. Свойство не может быть null.
     */
    @NotNull
    public Date getPreviousOrderDate()
    {
        return _previousOrderDate;
    }

    /**
     * @param previousOrderDate Дата предыдущего приказа. Свойство не может быть null.
     */
    public void setPreviousOrderDate(Date previousOrderDate)
    {
        dirty(_previousOrderDate, previousOrderDate);
        _previousOrderDate = previousOrderDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ChangeScientificAdviserExtractGen)
        {
            setCourse(((ChangeScientificAdviserExtract)another).getCourse());
            setGroup(((ChangeScientificAdviserExtract)another).getGroup());
            setDevelopForm(((ChangeScientificAdviserExtract)another).getDevelopForm());
            setScientificAdviserName(((ChangeScientificAdviserExtract)another).getScientificAdviserName());
            setPreviousOrderNumber(((ChangeScientificAdviserExtract)another).getPreviousOrderNumber());
            setPreviousOrderDate(((ChangeScientificAdviserExtract)another).getPreviousOrderDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChangeScientificAdviserExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChangeScientificAdviserExtract.class;
        }

        public T newInstance()
        {
            return (T) new ChangeScientificAdviserExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "developForm":
                    return obj.getDevelopForm();
                case "scientificAdviserName":
                    return obj.getScientificAdviserName();
                case "previousOrderNumber":
                    return obj.getPreviousOrderNumber();
                case "previousOrderDate":
                    return obj.getPreviousOrderDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "scientificAdviserName":
                    obj.setScientificAdviserName((PpsEntry) value);
                    return;
                case "previousOrderNumber":
                    obj.setPreviousOrderNumber((String) value);
                    return;
                case "previousOrderDate":
                    obj.setPreviousOrderDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "group":
                        return true;
                case "developForm":
                        return true;
                case "scientificAdviserName":
                        return true;
                case "previousOrderNumber":
                        return true;
                case "previousOrderDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "group":
                    return true;
                case "developForm":
                    return true;
                case "scientificAdviserName":
                    return true;
                case "previousOrderNumber":
                    return true;
                case "previousOrderDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "developForm":
                    return DevelopForm.class;
                case "scientificAdviserName":
                    return PpsEntry.class;
                case "previousOrderNumber":
                    return String.class;
                case "previousOrderDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChangeScientificAdviserExtract> _dslPath = new Path<ChangeScientificAdviserExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChangeScientificAdviserExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeScientificAdviserExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeScientificAdviserExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeScientificAdviserExtract#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return ФИО научного руководителя. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeScientificAdviserExtract#getScientificAdviserName()
     */
    public static PpsEntry.Path<PpsEntry> scientificAdviserName()
    {
        return _dslPath.scientificAdviserName();
    }

    /**
     * @return Номер предыдущего приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeScientificAdviserExtract#getPreviousOrderNumber()
     */
    public static PropertyPath<String> previousOrderNumber()
    {
        return _dslPath.previousOrderNumber();
    }

    /**
     * @return Дата предыдущего приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeScientificAdviserExtract#getPreviousOrderDate()
     */
    public static PropertyPath<Date> previousOrderDate()
    {
        return _dslPath.previousOrderDate();
    }

    public static class Path<E extends ChangeScientificAdviserExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private DevelopForm.Path<DevelopForm> _developForm;
        private PpsEntry.Path<PpsEntry> _scientificAdviserName;
        private PropertyPath<String> _previousOrderNumber;
        private PropertyPath<Date> _previousOrderDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeScientificAdviserExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeScientificAdviserExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeScientificAdviserExtract#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return ФИО научного руководителя. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeScientificAdviserExtract#getScientificAdviserName()
     */
        public PpsEntry.Path<PpsEntry> scientificAdviserName()
        {
            if(_scientificAdviserName == null )
                _scientificAdviserName = new PpsEntry.Path<PpsEntry>(L_SCIENTIFIC_ADVISER_NAME, this);
            return _scientificAdviserName;
        }

    /**
     * @return Номер предыдущего приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeScientificAdviserExtract#getPreviousOrderNumber()
     */
        public PropertyPath<String> previousOrderNumber()
        {
            if(_previousOrderNumber == null )
                _previousOrderNumber = new PropertyPath<String>(ChangeScientificAdviserExtractGen.P_PREVIOUS_ORDER_NUMBER, this);
            return _previousOrderNumber;
        }

    /**
     * @return Дата предыдущего приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeScientificAdviserExtract#getPreviousOrderDate()
     */
        public PropertyPath<Date> previousOrderDate()
        {
            if(_previousOrderDate == null )
                _previousOrderDate = new PropertyPath<Date>(ChangeScientificAdviserExtractGen.P_PREVIOUS_ORDER_DATE, this);
            return _previousOrderDate;
        }

        public Class getEntityClass()
        {
            return ChangeScientificAdviserExtract.class;
        }

        public String getEntityName()
        {
            return "changeScientificAdviserExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
