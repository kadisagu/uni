/* $Id: DAO.java.vm 24710 2012-10-30 11:06:12Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e94.AddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.WeekendChildThreeStuExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 16.01.2013
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<WeekendChildThreeStuExtract, Model> implements IDAO
{
    public static String getBenefitTextThreeYear(Date beginDate, Date endDate)
    {
        return ApplicationRuntime.getProperty("threeYear_benefitText")
                            .replaceAll("\\{(beginDate|payBenefitBeginDate)\\}", DateFormatter.DEFAULT_DATE_FORMATTER.format(beginDate))
                            .replaceAll("\\{(endDate|payBenefitEndDate)\\}", DateFormatter.DEFAULT_DATE_FORMATTER.format(endDate));
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        if (model.getExtract().getPayBenefitText() == null)
            model.getExtract().setPayBenefitText(getBenefitTextThreeYear(model.getExtract().getBeginDate(), model.getExtract().getEndDate()));
        if (model.getExtract().getPayOnetimeBenefitText() == null)
            model.getExtract().setPayOnetimeBenefitText(ApplicationRuntime.getProperty("threeYear_onetimeBenefitText"));


        List<String> statusCodes = new ArrayList<>();

        statusCodes.add(UniDefines.CATALOG_STUDENT_STATUS_CHILD);
        statusCodes.add(UniDefines.CATALOG_STUDENT_STATUS_CHILD_WITH_ATTENDANCE);
        statusCodes.add(UniDefines.CATALOG_STUDENT_STATUS_CHILD_WITHOUT_ATTENDANCE);

        List<StudentStatus> statusList = new DQLSelectBuilder()
                .fromEntity(StudentStatus.class, "ss")
                .where(DQLExpressions.eq(DQLExpressions.property(StudentStatus.usedInSystem().fromAlias("ss")), DQLExpressions.value(Boolean.TRUE)))
                .where(DQLExpressions.in(DQLExpressions.property(StudentStatus.code().fromAlias("ss")), statusCodes))
                .order(DQLExpressions.property(StudentStatus.code().fromAlias("ss")))
                .createStatement(getSession())
                .list();

        model.setStudentStatusList(statusList);

        if (model.isAddForm())
        {
            WeekendChildThreeStuExtract extract = model.getExtract();
            extract.setBeginDate(new Date());

            if (statusList.size() == 0)
            {
                StudentStatus defStatus = DataAccessServices.dao().get(StudentStatus.class, StudentStatus.code(), UniDefines.CATALOG_STUDENT_STATUS_CHILD);
                if(null == defStatus)
                {
                    extract.setStudentStatusNew(extract.getEntity().getStatus());
                }
                else
                {
                    extract.setStudentStatusNew(defStatus);
                }
            }
        }
        if(model.isEditForm())
        {
            if (statusList.size() == 1 && statusList.get(0).getCode().equals(UniDefines.CATALOG_STUDENT_STATUS_CHILD_WITHOUT_ATTENDANCE))
            {
                model.getExtract().setFreeAttendance(false);
            }
        }
    }

    @Override
    protected WeekendChildThreeStuExtract createNewInstance()
    {
        return new WeekendChildThreeStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        Date beginDate = model.getExtract().getBeginDate();
        Date endDate = model.getExtract().getEndDate();

        if (beginDate.getTime() >= endDate.getTime())
            errors.add("Дата окончания отпуска должна быть позже даты начала", "endDate");

        Date payBenefitBeginDate = model.getExtract().getPayBenefitBeginDate();
        Date payBenefitEndDate = model.getExtract().getPayBenefitEndDate();
        if(model.getExtract().isPayBenefit())
        {
            if(payBenefitBeginDate.after(payBenefitEndDate))
                errors.add("Дата окончания выплаты пособия должна быть позже даты начала", "payBenefitEndDate");
        }
    }

    @Override
    public void update(Model model)
    {
        //save rollback data
        model.getExtract().setStudentStatusOld(model.getExtract().getEntity().getStatus());
        super.update(model);
    }
}