/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e93.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.GroupAssignStuExtract;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 11.01.2013
 */
public class Model extends CommonModularStudentExtractAddEditModel<GroupAssignStuExtract>
{
    private ISelectModel _groupModel;
    private Group _groupNew;

    public ISelectModel getGroupModel()
    {
        return _groupModel;
    }

    public void setGroupModel(ISelectModel groupModel)
    {
        _groupModel = groupModel;
    }

    public Group getGroupNew()
    {
        return _groupNew;
    }

    public void setGroupNew(Group groupNew)
    {
        _groupNew = groupNew;
    }
}
