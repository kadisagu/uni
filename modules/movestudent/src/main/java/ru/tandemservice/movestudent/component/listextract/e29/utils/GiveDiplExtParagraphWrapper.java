/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e29.utils;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 20.09.2012
 */
public class GiveDiplExtParagraphWrapper implements Comparable<GiveDiplExtParagraphWrapper>
{
    private CompensationType _compensationType;
    private Course _course;
    private DevelopForm _developForm;
    private DevelopCondition _developCondition;
    private DevelopTech _developTech;
    private DevelopPeriod _developPeriod;
    private EducationLevels _educationLevels;
    private OrgUnit _formativeOrgUnit;
    private OrgUnit _territorialOrgUnit;
    private String _eduBaseText;
    private boolean _printDiplomaQualification;
    private EduProgramQualification _qualification;
    private final ListStudentExtract _firstExtract;

    public GiveDiplExtParagraphWrapper(CompensationType compensationType, Course course, DevelopForm developForm, DevelopCondition developCondition, DevelopTech developTech, DevelopPeriod developPeriod, EducationLevels educationLevels, OrgUnit formativeOrgUnit, OrgUnit territorialOrgUnit, String eduBaseText, boolean printDiplomaQualification, EduProgramQualification qualification, ListStudentExtract firstExtract)
    {
        _compensationType = compensationType;
        _course = course;
        _developForm = developForm;
        _developCondition = developCondition;
        _developTech = developTech;
        _developPeriod = developPeriod;
        _educationLevels = educationLevels;
        _formativeOrgUnit = formativeOrgUnit;
        _territorialOrgUnit = territorialOrgUnit;
        _eduBaseText = eduBaseText;
        _printDiplomaQualification = printDiplomaQualification;
        _qualification = qualification;
        _firstExtract = firstExtract;
    }

    private List<GiveDiplExtParagraphPartWrapper> _paragraphPartWrapperList = new ArrayList<>();
    private List<GiveDiplExtParagraphPartWrapper> _paragraphPartSuccessWrapperList = new ArrayList<>();

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public void setDevelopCondition(DevelopCondition developCondition)
    {
        _developCondition = developCondition;
    }

    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public void setDevelopTech(DevelopTech developTech)
    {
        _developTech = developTech;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        _developPeriod = developPeriod;
    }

    public EducationLevels getEducationLevels()
    {
        return _educationLevels;
    }

    public void setEducationLevels(EducationLevels educationLevels)
    {
        _educationLevels = educationLevels;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    public List<GiveDiplExtParagraphPartWrapper> getParagraphPartWrapperList()
    {
        return _paragraphPartWrapperList;
    }

    public void setParagraphPartWrapperList(List<GiveDiplExtParagraphPartWrapper> paragraphPartWrapperList)
    {
        _paragraphPartWrapperList = paragraphPartWrapperList;
    }

    public List<GiveDiplExtParagraphPartWrapper> getParagraphPartSuccessWrapperList()
    {
        return _paragraphPartSuccessWrapperList;
    }

    public void setParagraphPartSuccessWrapperList(List<GiveDiplExtParagraphPartWrapper> paragraphPartSuccessWrapperList)
    {
        _paragraphPartSuccessWrapperList = paragraphPartSuccessWrapperList;
    }

    public String getEduBaseText()
    {
        return _eduBaseText;
    }

    public void setEduBaseText(String eduBaseText)
    {
        _eduBaseText = eduBaseText;
    }

    public boolean isPrintDiplomaQualification()
    {
        return _printDiplomaQualification;
    }

    public void setPrintDiplomaQualification(boolean printDiplomaQualification)
    {
        _printDiplomaQualification = printDiplomaQualification;
    }

    public EduProgramQualification getQualification()
    {
        return _qualification;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof GiveDiplExtParagraphWrapper))
            return false;

        GiveDiplExtParagraphWrapper that = (GiveDiplExtParagraphWrapper) o;

        return (_compensationType.equals(that.getCompensationType()) &&
        _course.equals(that.getCourse()) &&
        _developForm.equals(that.getDevelopForm()) &&
        _developCondition.equals(that.getDevelopCondition()) &&
        _developTech.equals(that.getDevelopTech()) &&
        _developPeriod.equals(that.getDevelopPeriod()) &&
        _educationLevels.equals(that.getEducationLevels()) &&
        _eduBaseText.equals(that.getEduBaseText()) &&
        _printDiplomaQualification == that.isPrintDiplomaQualification()
        );
    }

    @Override
    public int hashCode()
    {
        return _compensationType.hashCode() & _course.hashCode() & _developForm.hashCode() & _developCondition.hashCode() & _developTech.hashCode() & _developPeriod.hashCode() & _educationLevels.hashCode() & _eduBaseText.hashCode() & ((Boolean)_printDiplomaQualification).hashCode();
    }

    @Override
    public int compareTo(GiveDiplExtParagraphWrapper o)
    {
        int result = _compensationType.getCode().compareTo(o.getCompensationType().getCode());
        if (result == 0)
            result = ((Integer) _course.getIntValue()).compareTo(o.getCourse().getIntValue());
        if (result == 0)
            result = _developForm.getCode().compareTo(o.getDevelopForm().getCode());
        if (result == 0)
            result = _developCondition.getCode().compareTo(o.getDevelopCondition().getCode());
        if (result == 0)
            result = _developTech.getCode().compareTo(o.getDevelopTech().getCode());
        if (result == 0)
            result = _developPeriod.getCode().compareTo(o.getDevelopPeriod().getCode());

        if (result == 0)
        {
            String thisCode = _educationLevels.getTitleCodePrefix();
            String thatCode = o.getEducationLevels().getTitleCodePrefix();

            result = thisCode == null ? (thatCode == null ? 0 : -1) : (thatCode == null ? 1 : thisCode.compareTo(thatCode));
        }

        if (result == 0)
        {
            StructureEducationLevels thisLevels = _educationLevels.getLevelType();
            StructureEducationLevels thatLevels = o.getEducationLevels().getLevelType();
            boolean isThisChild = thisLevels.isSpecialization() || thisLevels.isProfile();
            boolean isThatChild = thatLevels.isSpecialization() || thatLevels.isProfile();

            result = isThisChild ? (isThatChild ? 0 : -1) : (isThatChild ? 1 : 0);
        }

        if (result == 0)
            result = _educationLevels.getTitle().compareTo(o.getEducationLevels().getTitle());

        if (result == 0)
            result = _eduBaseText.compareTo(o.getEduBaseText());

        if (result == 0)
            result = ((Boolean) _printDiplomaQualification).compareTo(o.isPrintDiplomaQualification());

        return result;
    }
}