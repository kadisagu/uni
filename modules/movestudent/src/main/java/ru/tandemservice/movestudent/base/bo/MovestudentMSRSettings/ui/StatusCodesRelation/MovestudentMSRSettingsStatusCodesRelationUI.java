/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.ui.StatusCodesRelation;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.MovestudentMSRSettingsManager;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 18.12.2014
 */
public class MovestudentMSRSettingsStatusCodesRelationUI extends UIPresenter
{

    private Map<StudentStatus, Long> _codesMap;

    public Map<StudentStatus, Long> getCodesMap()
    {
        return _codesMap;
    }

    public void setCodesMap(Map<StudentStatus, Long> codesMap)
    {
        _codesMap = codesMap;
    }

    public String getStatusCodeId()
    {
        return "statusCodeId_" + getConfig().getDataSource(MovestudentMSRSettingsStatusCodesRelation.STATUS_DS).getCurrentId();
    }

    @Override
    public void onComponentRefresh()
    {
        _codesMap = MovestudentMSRSettingsManager.instance().dao().getMapStudentStatusCodes();
    }

    public void onClickApply()
    {
        MovestudentMSRSettingsManager.instance().dao().saveOrUpdateStudentStatusCodes(_codesMap);
        deactivate();

    }

    public Long getCurrentStatusCode()
    {
        StudentStatus status = getConfig().getDataSource(MovestudentMSRSettingsStatusCodesRelation.STATUS_DS).getCurrent();
        return  _codesMap.get(status);
    }

    public void setCurrentStatusCode(Long code)
    {
        StudentStatus status = getConfig().getDataSource(MovestudentMSRSettingsStatusCodesRelation.STATUS_DS).getCurrent();
        _codesMap.put(status, code);
    }
}
