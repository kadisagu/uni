/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e31.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 27.09.2012
 */
public class CourseTransferExtParagraphWrapper implements Comparable<CourseTransferExtParagraphWrapper>
{
    private final DevelopForm _developForm;
    private final DevelopCondition _developCondition;
    private final DevelopTech _developTech;
    private final DevelopPeriod _developPeriod;
    private final EducationLevels _educationLevels;
    private final CompensationType _compensationType;
    private final ListStudentExtract _firstExtract;
    private final String _eduBaseText;

    public CourseTransferExtParagraphWrapper(DevelopForm developForm, DevelopCondition developCondition, DevelopTech developTech, DevelopPeriod developPeriod, EducationLevels educationLevels, CompensationType compensationType, ListStudentExtract firstExtract, String eduBaseText)
    {
        _developForm = developForm;
        _developCondition = developCondition;
        _developTech = developTech;
        _developPeriod = developPeriod;
        _educationLevels = educationLevels;
        _compensationType = compensationType;
        _firstExtract = firstExtract;
        _eduBaseText = eduBaseText;
    }

    private List<CourseTransferExtParagraphPartWrapper> _paragraphPartWrapperList = new ArrayList<CourseTransferExtParagraphPartWrapper>();

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public EducationLevels getEducationLevels()
    {
        return _educationLevels;
    }

    public List<CourseTransferExtParagraphPartWrapper> getParagraphPartWrapperList()
    {
        return _paragraphPartWrapperList;
    }

    public void setParagraphPartWrapperList(List<CourseTransferExtParagraphPartWrapper> paragraphPartWrapperList)
    {
        _paragraphPartWrapperList = paragraphPartWrapperList;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    public String getEduBaseText()
    {
        return _eduBaseText;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof CourseTransferExtParagraphWrapper))
            return false;

        CourseTransferExtParagraphWrapper that = (CourseTransferExtParagraphWrapper) o;

        boolean result = _educationLevels.equals(that.getEducationLevels()) && _developForm.equals(that.getDevelopForm()) && _developCondition.equals(that.getDevelopCondition()) && _developTech.equals(that.getDevelopTech()) && _developPeriod.equals(that.getDevelopPeriod()) && _eduBaseText.equals(that.getEduBaseText());
        return  (null == _compensationType || null == that.getCompensationType()) ? result : result && _compensationType.equals(that.getCompensationType());
    }

    @Override
    public int hashCode()
    {
        return _educationLevels.hashCode() & _developForm.hashCode() & _developCondition.hashCode() & _developTech.hashCode() & _developPeriod.hashCode() & _compensationType.hashCode() & _eduBaseText.hashCode();
    }

    @Override
    public int compareTo(CourseTransferExtParagraphWrapper o)
    {
        StructureEducationLevels thisLevels = _educationLevels.getLevelType();
        StructureEducationLevels thatLevels = o.getEducationLevels().getLevelType();

        if (thisLevels == null || thatLevels == null)
            return thisLevels == null ? (thatLevels == null ? 0 : 1) : -1;

        boolean isThisChild = thisLevels.isSpecialty() || thisLevels.isProfile();
        boolean isThatChild = thatLevels.isSpecialty() || thatLevels.isProfile();

        if (isThisChild != isThatChild)
            return isThisChild ? 1 : -1;

        int result = _educationLevels.getTitle().compareTo(o.getEducationLevels().getTitle());

        if (result == 0)
            result = _developForm.getCode().compareTo(o.getDevelopForm().getCode());

        if (result == 0)
            result = _developCondition.getCode().compareTo(o.getDevelopCondition().getCode());

        if (result == 0)
            result = _developTech.getCode().compareTo(o.getDevelopTech().getCode());

        if (result == 0)
            result = _developPeriod.getCode().compareTo(o.getDevelopPeriod().getCode());

        if (result == 0)
            result = _compensationType.getCode().compareTo(o.getCompensationType().getCode());

        if (result == 0)
            result = _eduBaseText.compareTo(o.getEduBaseText());

        return result;
    }
}