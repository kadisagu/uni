/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.component.commons.MoveStudentColumns;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;

/**
 * @author vip_delete
 * @since 09.04.2009
 */
public abstract class AbstractListParagraphPubController<T extends ListStudentExtract, IDAO extends IAbstractListParagraphPubDAO<T, Model>, Model extends AbstractListParagraphPubModel<T>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        rebuildListDataSource(component);
    }

    private void rebuildListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        prepareListDataSource(component);
    }

    protected void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        DynamicListDataSource<T> dataSource = UniBaseUtils.createDataSource(component, getDao());

        AbstractColumn linkColumn = new PublisherLinkColumn("ФИО", new String[]{IAbstractExtract.L_ENTITY, Student.L_PERSON, Person.P_FULLFIO})
                .setResolver(new DefaultPublisherLinkResolver()
                {
                    @Override
                    public String getComponentName(IEntity entity)
                    {
                        return MoveStudentUtils.getListExtractPubComponent(((StudentExtractType) entity.getProperty(IAbstractExtract.L_TYPE)));
                    }
                }).setOrderable(false);
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Пол", new String[]{IAbstractExtract.L_ENTITY, Student.L_PERSON, Person.L_IDENTITY_CARD, IdentityCard.L_SEX, Sex.P_SHORT_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Курс", ListStudentExtract.P_COURSE_STR).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Группа", ListStudentExtract.P_GROUP_STR).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", ListStudentExtract.P_STUDENT_STATUS_STR).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", ListStudentExtract.P_COMPENSATION_TYPE_STR).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", ListStudentExtract.P_DEVELOP_FORM_STR).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", ListStudentExtract.P_FORMATIVE_ORG_UNIT_STR).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", ListStudentExtract.P_TERRITORIAL_ORG_UNIT_STR).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn(EducationOrgUnit.EDU_LVL_HS_COLUMN_TITLE, ListStudentExtract.P_EDUCATION_LEVEL_HIGH_SCHOOL_STR).setClickable(false).setOrderable(false));

        IndicatorColumn indicatorColumn = (IndicatorColumn) MoveStudentColumns.getExtractPrintColumn(this, getModel(component).getSecModel().getPermission("printExtract")).setDisabledProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY);
        indicatorColumn.setListener(component.getName() + ":onClickExtractPrint");
        dataSource.addColumn(indicatorColumn);

        if (MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
        {
            IndicatorColumn indicatorPdfColumn = (IndicatorColumn) MoveStudentColumns.getExtractPrintPdfColumn(this, getModel(component).getSecModel().getPermission("printExtractPdf")).setDisabledProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY);
            indicatorPdfColumn.setListener(component.getName() + ":onClickExtractPrintPdf");
            dataSource.addColumn(indicatorPdfColumn);
        }

        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteExtract", "Исключить студента из параграфа «{0}»?", new Object[]{new String[]{IAbstractExtract.L_ENTITY, Student.L_PERSON, Person.P_FULLFIO}}).setPermissionKey(model.getSecModel().getPermission("deleteStudent")).setDisabledProperty(Model.NO_DELETE));

        model.setDataSource(dataSource);
    }

    public void onClickEditParagraph(IBusinessComponent component)
    {
        Model model = getModel(component);

        // validate
        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getParagraph().getOrder().getState().getCode()))
            return;

        component.createDefaultChildRegion(new ComponentActivator(MoveStudentUtils.getListParagraphAddEditComponent(model.getExtract().getType()), new ParametersMap()
                .add(AbstractListParagraphAddEditModel.PARAMETER_PARAGRAPH_ID, model.getParagraph().getId())
                .add(AbstractListParagraphAddEditModel.PARAMETER_ORDER_ID, null)
                .add(AbstractListParagraphAddEditModel.PARAMETER_PARAGRAPH_TYPE_ID, null)
        ));
    }

    public void onClickDeleteParagraph(IBusinessComponent component)
    {
        Model model = getModel(component);

        // validate
        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getParagraph().getOrder().getState().getCode()))
            return;

        MoveDaoFacade.getMoveDao().deleteParagraph(model.getParagraph(), null);

        deactivate(component);
    }

    public void onClickDeleteExtract(IBusinessComponent component)
    {
        MoveStudentDaoFacade.getMoveStudentDao().deleteListExtract((Long) component.getListenerParameter());
        rebuildListDataSource(component);
    }

    public void onClickExtractPrint(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_EXTRACT_PRINT, new ParametersMap()
                .add("extractId", component.getListenerParameter())
        ));
    }

    public void onClickExtractPrintPdf(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_EXTRACT_PRINT, new ParametersMap()
                .add("extractId", component.getListenerParameter())
                .add("printPdf", Boolean.TRUE)
        ));
    }
}