/* $Id: Model.java.vm 6933 2009-03-07 05:41:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e94.Pub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.movestudent.entity.WeekendChildThreeStuExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 16.01.2013
 */
public class Model extends ModularStudentExtractPubModel<WeekendChildThreeStuExtract>
{
    public String getDisplayFreeAttendDisplayName()
    {
        String freeAttendanceText = ApplicationRuntime.getProperty("freeAttendanceText");
        return "Выводить в приказе - «" + (!StringUtils.isEmpty(freeAttendanceText) ? freeAttendanceText : "") + "»";
    }
}
