/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;

import java.util.List;

/**
 * @author vip_delete
 * @since 05.11.2008
 */
@Input({
        @Bind(key = "studentId", binding = "studentId"),
        @Bind(key = "extractTypeId", binding = "extractType.id"),
        @Bind(key = "extractId", binding = "extractId"),
        @Bind(key = "formingOrgUnitId", binding = "formingOrgUnitId"),
        @Bind(key = "individual", binding = "individual")
})
public abstract class ModularStudentExtractAddEditModel<T extends ModularStudentExtract>
{
    private Long _extractId;
    private Long _studentId;
    private Long _formingOrgUnitId;
    private OrgUnit _formingOrgUnit;
    private T _extract;
    private boolean _addForm;
    private boolean _editForm;
    private StudentExtractType _extractType = new StudentExtractType();
    private List<StudentExtractType> _extractTypeList;
    private ISelectModel _extractTypeListModel;
    private Boolean _individual;

    // Getters & Setters

    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        _extractId = extractId;
    }

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        _studentId = studentId;
    }

    public Long getFormingOrgUnitId()
    {
        return _formingOrgUnitId;
    }

    public void setFormingOrgUnitId(Long formingOrgUnitId)
    {
        this._formingOrgUnitId = formingOrgUnitId;
    }

    public OrgUnit getFormingOrgUnit()
    {
        return _formingOrgUnit;
    }

    public void setFormingOrgUnit(OrgUnit formingOrgUnit)
    {
        this._formingOrgUnit = formingOrgUnit;
    }

    public T getExtract()
    {
        return _extract;
    }

    public void setExtract(T extract)
    {
        _extract = extract;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        _addForm = addForm;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public StudentExtractType getExtractType()
    {
        return _extractType;
    }

    public void setExtractType(StudentExtractType extractType)
    {
        _extractType = extractType;
    }

    public List<StudentExtractType> getExtractTypeList()
    {
        return _extractTypeList;
    }

    public void setExtractTypeList(List<StudentExtractType> extractTypeList)
    {
        _extractTypeList = extractTypeList;
    }

    public ISelectModel getExtractTypeListModel()
    {
        return _extractTypeListModel;
    }

    public void setExtractTypeListModel(ISelectModel extractTypeListModel)
    {
        _extractTypeListModel = extractTypeListModel;
    }

    public Boolean getIndividual()
    {
        return _individual;
    }

    public void setIndividual(Boolean individual)
    {
        _individual = individual;
    }
}