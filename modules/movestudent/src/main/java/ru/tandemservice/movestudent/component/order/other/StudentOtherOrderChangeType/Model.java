/* $Id$ */
package ru.tandemservice.movestudent.component.order.other.StudentOtherOrderChangeType;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.entity.OtherStudentExtract;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;

/**
 * @author Dmitry Seleznev
 * @since 18.04.2013
 */
@Input({
        @Bind(key = "extractId", binding = "extractId")
})
public class Model
{
    private Long _extractId;
    private OtherStudentExtract _extract;

    private ISelectModel _extractParentTypeListModel;
    private ISelectModel _extractTypeListModel;
    private StudentExtractType _parentExtractType;
    private StudentExtractType _extractType;

    private boolean _otherTypesThreeLevel = false;

    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        _extractId = extractId;
    }

    public OtherStudentExtract getExtract()
    {
        return _extract;
    }

    public void setExtract(OtherStudentExtract extract)
    {
        _extract = extract;
    }

    public ISelectModel getExtractParentTypeListModel()
    {
        return _extractParentTypeListModel;
    }

    public void setExtractParentTypeListModel(ISelectModel extractParentTypeListModel)
    {
        _extractParentTypeListModel = extractParentTypeListModel;
    }

    public ISelectModel getExtractTypeListModel()
    {
        return _extractTypeListModel;
    }

    public void setExtractTypeListModel(ISelectModel extractTypeListModel)
    {
        _extractTypeListModel = extractTypeListModel;
    }

    public StudentExtractType getParentExtractType()
    {
        return _parentExtractType;
    }

    public void setParentExtractType(StudentExtractType parentExtractType)
    {
        _parentExtractType = parentExtractType;
    }

    public StudentExtractType getExtractType()
    {
        return _extractType;
    }

    public void setExtractType(StudentExtractType extractType)
    {
        _extractType = extractType;
    }

    public boolean isOtherTypesThreeLevel()
    {
        return _otherTypesThreeLevel;
    }

    public void setOtherTypesThreeLevel(boolean otherTypesThreeLevel)
    {
        _otherTypesThreeLevel = otherTypesThreeLevel;
    }
}