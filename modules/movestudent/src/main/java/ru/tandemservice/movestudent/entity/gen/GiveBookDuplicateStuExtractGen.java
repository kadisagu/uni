package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.GiveBookDuplicateStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О выдаче дубликата зачетной книжки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GiveBookDuplicateStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.GiveBookDuplicateStuExtract";
    public static final String ENTITY_NAME = "giveBookDuplicateStuExtract";
    public static final int VERSION_HASH = -1040760833;
    private static IEntityMeta ENTITY_META;

    public static final String P_BOOK_NUMBER = "bookNumber";

    private String _bookNumber;     // Номер выдаваемого документа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер выдаваемого документа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getBookNumber()
    {
        return _bookNumber;
    }

    /**
     * @param bookNumber Номер выдаваемого документа. Свойство не может быть null.
     */
    public void setBookNumber(String bookNumber)
    {
        dirty(_bookNumber, bookNumber);
        _bookNumber = bookNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof GiveBookDuplicateStuExtractGen)
        {
            setBookNumber(((GiveBookDuplicateStuExtract)another).getBookNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GiveBookDuplicateStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GiveBookDuplicateStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new GiveBookDuplicateStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "bookNumber":
                    return obj.getBookNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "bookNumber":
                    obj.setBookNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "bookNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "bookNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "bookNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GiveBookDuplicateStuExtract> _dslPath = new Path<GiveBookDuplicateStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GiveBookDuplicateStuExtract");
    }
            

    /**
     * @return Номер выдаваемого документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GiveBookDuplicateStuExtract#getBookNumber()
     */
    public static PropertyPath<String> bookNumber()
    {
        return _dslPath.bookNumber();
    }

    public static class Path<E extends GiveBookDuplicateStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<String> _bookNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер выдаваемого документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GiveBookDuplicateStuExtract#getBookNumber()
     */
        public PropertyPath<String> bookNumber()
        {
            if(_bookNumber == null )
                _bookNumber = new PropertyPath<String>(GiveBookDuplicateStuExtractGen.P_BOOK_NUMBER, this);
            return _bookNumber;
        }

        public Class getEntityClass()
        {
            return GiveBookDuplicateStuExtract.class;
        }

        public String getEntityName()
        {
            return "giveBookDuplicateStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
