/* $Id: extractDao.vm 23805 2012-08-13 07:42:13Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e94;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.WeekendChildThreeStuExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 16.01.2013
 */
public class WeekendChildThreeStuExtractDao extends UniBaseDao implements IExtractComponentDao<WeekendChildThreeStuExtract>
{
    public void doCommit(WeekendChildThreeStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        //save rollback data
        Student student = extract.getEntity();
        extract.setStudentStatusOld(student.getStatus());

        //set new values
        student.setStatus(extract.getStudentStatusNew());

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            extract.setPrevOrderDate(orderData.getWeekendChildOrderDate());
            extract.setPrevOrderNumber(orderData.getWeekendChildOrderNumber());
            extract.setPrevBeginDate(orderData.getWeekendChildDateFrom());
            extract.setPrevEndDate(orderData.getWeekendChildDateTo());
        }
        orderData.setWeekendChildOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setWeekendChildOrderNumber(extract.getParagraph().getOrder().getNumber());
        orderData.setWeekendChildDateFrom(extract.getBeginDate());
        orderData.setWeekendChildDateTo(extract.getEndDate());
        getSession().saveOrUpdate(orderData);
    }

    public void doRollback(WeekendChildThreeStuExtract extract, Map parameters)
    {
        Student student = extract.getEntity();
        student.setStatus(extract.getStudentStatusOld());

        // возвращаем предыдущие номер и дату приказа
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setWeekendChildOrderDate(extract.getPrevOrderDate());
        orderData.setWeekendChildOrderNumber(extract.getPrevOrderNumber());
        orderData.setWeekendChildDateFrom(extract.getPrevBeginDate());
        orderData.setWeekendChildDateTo(extract.getPrevEndDate());
        getSession().saveOrUpdate(orderData);
    }
}
