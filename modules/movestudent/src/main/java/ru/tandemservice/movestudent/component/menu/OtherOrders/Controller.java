/* $Id$ */
package ru.tandemservice.movestudent.component.menu.OtherOrders;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.component.commons.IMoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.MoveStudentColumns;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.OtherStudentExtract;
import ru.tandemservice.movestudent.entity.StudentOtherOrderTextRelation;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.dao.MoveDaoFacade;

/**
 * @author Dmitry Seleznev
 * @since 11.04.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setSettings(UniBaseUtils.getDataSettings(component, "OtherOrders.filter"));
        model.getSettings().set(IMoveStudentMQBuilder.LAST_NAME, null);

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<OtherStudentExtract> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(MoveStudentColumns.getExtractCreateDateColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractOrderNumberColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractCommitDateColumn());
        dataSource.addColumn(MoveStudentColumns.getFullFioColumn());
        dataSource.addColumn(MoveStudentColumns.getFormativeOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getTerritorialOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getProducingOrgUnitColumn());
        MoveStudentColumns.addEduLevelTitleColumns(dataSource);
        dataSource.addColumn(MoveStudentColumns.getCourseColumn());
        dataSource.addColumn(MoveStudentColumns.getGroupColumn());
        dataSource.addColumn(MoveStudentColumns.getDevelopFormColumn());
        dataSource.addColumn(MoveStudentColumns.getCompensationTypeColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractTypeColumn("Тип приказа"));
        dataSource.addColumn(MoveStudentColumns.getExtractOrderStateColumn());
        dataSource.addColumn(new SimpleColumn("Примечание", OtherStudentExtract.comment().s()).setClickable(false).setOrderable(false));

        IndicatorColumn printColumn = (IndicatorColumn) MoveStudentColumns.getOtherOrderPrintColumn(this, model.getSecModel().getPermission("print_menuList"));
        printColumn.setDisabledProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY);
        dataSource.addColumn(printColumn);

        dataSource.addColumn(MoveStudentColumns.getOtherOrderEditColumn(this, getModel(component).getSecModel().getPermission("edit_menuList")));
        dataSource.addColumn(MoveStudentColumns.getOtherOrderDeleteColumn(this, getModel(component).getSecModel().getPermission("delete_menuList")));

        dataSource.setOrder(IAbstractExtract.P_CREATE_DATE, OrderDirection.desc);
        getModel(component).setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);
        UniBaseUtils.validateDatesPeriod(model.getSettings(), "createDateFrom", "createDateTo");

        if (!UserContext.getInstance().getErrorCollector().isHasFieldErrors())
        {
            DataSettingsFacade.saveSettings(model.getSettings());
            model.getDataSource().refresh();
        }
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }

    public void onClickOtherOrderPrint(IBusinessComponent component)
    {
        OtherStudentExtract extract = getDao().getNotNull(OtherStudentExtract.class, component.<Long>getListenerParameter());
        StudentOtherOrderTextRelation rel = UniDaoFacade.getCoreDao().get(StudentOtherOrderTextRelation.class, StudentOtherOrderTextRelation.order().s(), extract.getParagraph().getOrder().getId());
        if (null == rel) throw new ApplicationException("К данному приказу не прикреплена печатная форма.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(rel.getFileType()).fileName(rel.getFileName()).document(rel.getContent()), false);
    }

    public void onClickOtherOrderEdit(IBusinessComponent component)
    {
        Long extractId = (Long) component.getListenerParameter();
        component.createDefaultChildRegion(new ComponentActivator(IMoveStudentComponents.STUDENT_OTHER_ORDER_ADD_EDIT, new ParametersMap().add("extractId", extractId)));
    }

    public void onClickOtherOrderDelete(IBusinessComponent component)
    {
        OtherStudentExtract extract = getDao().get(OtherStudentExtract.class, (Long) component.getListenerParameter());
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(extract.getParagraph().getOrder());
    }
}