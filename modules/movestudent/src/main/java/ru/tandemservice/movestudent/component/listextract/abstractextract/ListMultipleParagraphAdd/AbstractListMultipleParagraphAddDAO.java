/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.abstractextract.ListMultipleParagraphAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author dseleznev
 *         Created on: 10.06.2009
 */
public abstract class AbstractListMultipleParagraphAddDAO<T extends ListStudentExtract, Model extends AbstractListMultipleParagraphAddModel<T>> extends UniDao<Model> implements IAbstractListMultipleParagraphAddDAO<T, Model>
{
    private static OrderDescriptionRegistry orderRegistry = new OrderDescriptionRegistry("p");

    static
    {
        //orderRegistry.setOrders(Student.FIO_KEY, new OrderDescription("p", Student.STUDENT_LAST_NAME), new OrderDescription("p", Student.STUDENT_FIRST_NAME), new OrderDescription("p", Student.STUDENT_MIDDLE_NAME));
    }

    @Override
    public void prepare(Model model)
    {
        model.setOrder(this.getNotNull(model.getOrderId()));
        model.setParagraphType(getNotNull(StudentExtractType.class, model.getParagraphType().getId()));

        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getOrder().getState().getCode()))
            throw new ApplicationException("Нельзя изменять приказ. Изменение приказа возможно только в состоянии «Формируется».");
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(Group.ENTITY_CLASS, "p");

        patchListDataSource(builder, model);

        builder.add(MQExpression.eq("p", Group.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.eq("p", Group.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getOrder().getOrgUnit()));

        orderRegistry.applyOrder(builder, model.getDataSource().getEntityOrder());
        List<Group> list = builder.getResultList(getSession());
        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);

        ViewWrapper.getPatchedList(model.getDataSource());
    }

    protected abstract void patchListDataSource(MQBuilder builder, Model model);

    @Override
    public void update(Model model)
    {
        // V A L I D A T E

        final Session session = getSession();

        List<StudentListParagraph> addedParagraphsList = new ArrayList<>();

        session.refresh(model.getOrder());
        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getOrder().getState().getCode()))
            throw new ApplicationException("Нельзя изменять приказ. Изменение приказа возможно только в состоянии «Формируется».");

        final List<Group> list = ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).getSelectedObjects().stream()
                .map(o -> (Group) ((ViewWrapper) o).getEntity())
                .sorted(CommonCollator.TITLED_WITH_ID_COMPARATOR)
                .collect(Collectors.toList());

        if (list.isEmpty()) {
            throw new ApplicationException("Параграф приказа не может быть пустым. При необходимости удалите параграф приказа и создайте новый.");
        }

        List<Student> studentList = getStudentsList(model, list).getResultList(session);

        Map<Long, List<Student>> studentsMap = new HashMap<>();

        // пока мы были на форме state мог очень сильно поменяться, поэтому надо все данные проверить на актуаность
        for (Student student : studentList)
        {
            List<Student> singleGroupStudentList = studentsMap.get(student.getGroup().getId());
            if (null == singleGroupStudentList) singleGroupStudentList = new ArrayList<>();
            singleGroupStudentList.add(student);
            studentsMap.put(student.getGroup().getId(), singleGroupStudentList);

            // студент не должен быть в другом приказе
            if (!MoveStudentDaoFacade.getMoveStudentDao().isMoveAccessible(student))
                throw new ApplicationException("Нельзя добавить параграф для группы " + student.getGroup().getTitle() + ", так как у студента '" + student.getPerson().getFullFio() + "' уже есть непроведенные выписки.");
        }

        // C R E A T E   P A R A G R A P H S
        int paragraphCount = model.getOrder().getParagraphCount() + 1;
        for (Group group : list)
        {
            List<Student> singleGroupStudentList = studentsMap.get(group.getId());

            if (singleGroupStudentList != null && !singleGroupStudentList.isEmpty())
            {
                singleGroupStudentList.sort(Student.FULL_FIO_AND_ID_COMPARATOR);
                StudentListParagraph paragraph = createAndFillParagraph(model, paragraphCount++);
                session.save(paragraph);

                for (Student student : singleGroupStudentList)
                {
                    // создаем выписку и заполняем в ней стандартные поля
                    T extract = createAndFillExtract(student, model, group);
                    session.save(extract);
                }

                // Перенумеруем все выписки, так чтобы сортировка была по ФИО
                List<ListStudentExtract> extractList = getList(ListStudentExtract.class, IAbstractExtract.L_PARAGRAPH, model.getParagraph());
                extractList.sort(AbstractStudentExtract.FULL_FIO_AND_ID_COMPARATOR);

                // нумеруем выписки с единицы
                int counter = 1;
                for (ListStudentExtract extract : extractList)
                {
                    extract.setNumber(counter++);
                    update(extract);
                }
                addedParagraphsList.add(paragraph);
            }
        }
        model.setAddedParagraphs(addedParagraphsList);
    }

    protected MQBuilder getStudentsList(Model model, List<Group> groupsList)
    {
        MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "s");
        builder.add(MQExpression.in("s", Student.L_GROUP, groupsList));
        builder.add(MQExpression.eq("s", Student.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.eq("s", Student.L_STATUS + "." + StudentStatus.P_ACTIVE, Boolean.TRUE));
        return builder;
    }

    // конструктор выписки
    protected abstract T createNewInstance(Model model);

    // заполняем специфические параметры выписки
    protected abstract void fillExtract(T extract, Model model, Group group);

    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.NOMINATIVE;
    }

    private T createAndFillExtract(Student student, Model model, Group group)
    {
        T extract = createNewInstance(model);

        // сохраняем данные в модели
        model.setExtract(extract);

        // в student уже есть все данные для проведения выписки
        extract.setEntity(student);
        extract.setParagraph(model.getParagraph());
        extract.setType(model.getParagraphType());
        extract.setCreateDate(model.getOrder().getCreateDate());
        extract.setNumber(null); // нет номера. перенумеруем в конце метода update

        // Хочу иметь возможность взять данные entity в этом методе
        fillExtract(extract, model, group);

        //обновляем fio в нужном падеже
        syncStudentTitle(model);

        //обновляем текстовые значения старых атрибутов
        syncPreCommitData(model);

        return extract;
    }

    private StudentListParagraph createAndFillParagraph(Model model, int paragraphNumber)
    {
        StudentListParagraph paragraph = new StudentListParagraph();

        // сохраняем данные в модели
        model.setParagraph(paragraph);
        paragraph.setOrder(model.getOrder());
        paragraph.setNumber(paragraphNumber);

        return paragraph;
    }

    private void syncPreCommitData(Model model)
    {
        T extract = model.getExtract();
        Student student = extract.getEntity();

        extract.setStudentTitleStr(student.getPerson().getFullFio());
        extract.setStudentStatusStr(student.getStatus().getTitle());
        extract.setPersonalNumberStr(student.getPersonalNumber());
        extract.setCourseStr(student.getCourse().getTitle());
        extract.setGroupStr(student.getGroup() == null ? "" : student.getGroup().getTitle());
        extract.setCompensationTypeStr(student.getCompensationType().getShortTitle());
        extract.setFormativeOrgUnitStr(student.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle());
        extract.setTerritorialOrgUnitStr(student.getEducationOrgUnit().getTerritorialOrgUnit().getTerritorialShortTitle());
        extract.setEducationLevelHighSchoolStr(student.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle());
        extract.setDevelopFormStr(student.getEducationOrgUnit().getDevelopForm().getTitle());
        extract.setDevelopConditionStr(student.getEducationOrgUnit().getDevelopCondition().getTitle());
        extract.setDevelopTechStr(student.getEducationOrgUnit().getDevelopTech().getTitle());
        extract.setDevelopPeriodStr(student.getEducationOrgUnit().getDevelopPeriod().getTitle());
    }

    private void syncStudentTitle(Model model)
    {
        GrammaCase rusCase = getStudentTitleCase();
        if (rusCase != null)
        {
            IdentityCard identityCard = model.getExtract().getEntity().getPerson().getIdentityCard();

            InflectorVariant variant = DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(rusCase);

            CoreCollectionUtils.Pair<String, InflectorVariant> lastNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.lastName().s(), variant);
            CoreCollectionUtils.Pair<String, InflectorVariant> firstNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.firstName().s(), variant);
            CoreCollectionUtils.Pair<String, InflectorVariant> middleNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.middleName().s(), variant);

            String lastName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(lastNamePairKey);
            String firstName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(firstNamePairKey);
            String middleName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(middleNamePairKey);

            StringBuilder str = new StringBuilder(lastName);
            str.append(" ").append(firstName);
            if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
                str.append(" ").append(middleName);
            model.getExtract().setStudentTitle(str.toString());
        }
        else
            model.getExtract().setStudentTitle(model.getExtract().getEntity().getPerson().getFullFio());
    }
}
