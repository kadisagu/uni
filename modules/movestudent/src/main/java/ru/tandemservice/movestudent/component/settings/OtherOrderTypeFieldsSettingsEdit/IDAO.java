/* $Id$ */
package ru.tandemservice.movestudent.component.settings.OtherOrderTypeFieldsSettingsEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Dmitry Seleznev
 * @since 11.04.2013
 */
public interface IDAO extends IUniDao<Model>
{
}