package ru.tandemservice.movestudent.entity.custom.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.custom.ChangeGroupCaptainAction;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unimove.entity.ICustomOrderAction;
import ru.tandemservice.unimove.entity.ICustomOrderExtract;
import ru.tandemservice.unimove.entity.gen.ICustomOrderExtractGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Назначение старостой/Снятие с должности старосты
 *
 * Если стоит признак "Назначить старостой", значит после проведения студент будет старостой его группы, иначе - с него снимут эту должность.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChangeGroupCaptainActionGen extends EntityBase
 implements ICustomOrderAction{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.custom.ChangeGroupCaptainAction";
    public static final String ENTITY_NAME = "changeGroupCaptainAction";
    public static final int VERSION_HASH = -1584892832;
    private static IEntityMeta ENTITY_META;

    public static final String P_GROUP_CAPTAIN = "groupCaptain";
    public static final String L_GROUP = "group";
    public static final String L_CUSTOM_EXTRACT = "customExtract";

    private boolean _groupCaptain;     // Назначить старостой
    private Group _group;     // Группа, где производится изменение
    private ICustomOrderExtract _customExtract;     // Выписка из кастомного приказа, с набором действий, определенных пользователем

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Назначить старостой. Свойство не может быть null.
     */
    @NotNull
    public boolean isGroupCaptain()
    {
        return _groupCaptain;
    }

    /**
     * @param groupCaptain Назначить старостой. Свойство не может быть null.
     */
    public void setGroupCaptain(boolean groupCaptain)
    {
        dirty(_groupCaptain, groupCaptain);
        _groupCaptain = groupCaptain;
    }

    /**
     * @return Группа, где производится изменение.
     */
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа, где производится изменение.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public ICustomOrderExtract getCustomExtract()
    {
        return _customExtract;
    }

    /**
     * @param customExtract Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     */
    public void setCustomExtract(ICustomOrderExtract customExtract)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && customExtract!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(ICustomOrderExtract.class);
            IEntityMeta actual =  customExtract instanceof IEntity ? EntityRuntime.getMeta((IEntity) customExtract) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_customExtract, customExtract);
        _customExtract = customExtract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ChangeGroupCaptainActionGen)
        {
            setGroupCaptain(((ChangeGroupCaptainAction)another).isGroupCaptain());
            setGroup(((ChangeGroupCaptainAction)another).getGroup());
            setCustomExtract(((ChangeGroupCaptainAction)another).getCustomExtract());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChangeGroupCaptainActionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChangeGroupCaptainAction.class;
        }

        public T newInstance()
        {
            return (T) new ChangeGroupCaptainAction();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "groupCaptain":
                    return obj.isGroupCaptain();
                case "group":
                    return obj.getGroup();
                case "customExtract":
                    return obj.getCustomExtract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "groupCaptain":
                    obj.setGroupCaptain((Boolean) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "customExtract":
                    obj.setCustomExtract((ICustomOrderExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "groupCaptain":
                        return true;
                case "group":
                        return true;
                case "customExtract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "groupCaptain":
                    return true;
                case "group":
                    return true;
                case "customExtract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "groupCaptain":
                    return Boolean.class;
                case "group":
                    return Group.class;
                case "customExtract":
                    return ICustomOrderExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChangeGroupCaptainAction> _dslPath = new Path<ChangeGroupCaptainAction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChangeGroupCaptainAction");
    }
            

    /**
     * @return Назначить старостой. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeGroupCaptainAction#isGroupCaptain()
     */
    public static PropertyPath<Boolean> groupCaptain()
    {
        return _dslPath.groupCaptain();
    }

    /**
     * @return Группа, где производится изменение.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeGroupCaptainAction#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeGroupCaptainAction#getCustomExtract()
     */
    public static ICustomOrderExtractGen.Path<ICustomOrderExtract> customExtract()
    {
        return _dslPath.customExtract();
    }

    public static class Path<E extends ChangeGroupCaptainAction> extends EntityPath<E>
    {
        private PropertyPath<Boolean> _groupCaptain;
        private Group.Path<Group> _group;
        private ICustomOrderExtractGen.Path<ICustomOrderExtract> _customExtract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Назначить старостой. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeGroupCaptainAction#isGroupCaptain()
     */
        public PropertyPath<Boolean> groupCaptain()
        {
            if(_groupCaptain == null )
                _groupCaptain = new PropertyPath<Boolean>(ChangeGroupCaptainActionGen.P_GROUP_CAPTAIN, this);
            return _groupCaptain;
        }

    /**
     * @return Группа, где производится изменение.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeGroupCaptainAction#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeGroupCaptainAction#getCustomExtract()
     */
        public ICustomOrderExtractGen.Path<ICustomOrderExtract> customExtract()
        {
            if(_customExtract == null )
                _customExtract = new ICustomOrderExtractGen.Path<ICustomOrderExtract>(L_CUSTOM_EXTRACT, this);
            return _customExtract;
        }

        public Class getEntityClass()
        {
            return ChangeGroupCaptainAction.class;
        }

        public String getEntityName()
        {
            return "changeGroupCaptainAction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
