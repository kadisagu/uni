/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.settings.ReasonToTypesSettingsAdd;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectFragmentBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import ru.tandemservice.movestudent.entity.StudentReasonToTypeRel;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationAdd.AbstractRelationAddDAO;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 16.10.2008
 */
public class DAO extends AbstractRelationAddDAO<Model> implements IDAO
{
    @Override
    protected Class getRelationSecondEntityClass()
    {
        return StudentExtractType.class;
    }

    @Override
    protected String getRelationSecondEntityClassSortProperty()
    {
        return StudentExtractType.P_TITLE;
    }

    @Override
    protected Class getRelationEntityClass()
    {
        return StudentReasonToTypeRel.class;
    }

    @Override
    public void prepare(Model model)
    {
        model.setFirst(getNotNull(model.getFirstId()));
        model.setHierarchical(isHierarchical());

        DQLSelectBuilder alreadyAdded = new DQLSelectBuilder().fromEntity(getRelationEntityClass(), "rel")
                .where(eq(property("rel", IEntityRelation.L_FIRST), value(model.getFirst())))
                .where(eq(property("rel", IEntityRelation.L_SECOND), property("e.id")));

        // Сборные (индивидуальные) плюс первый уровень списочных типов
        DQLSelectFragmentBuilder listAndModularTypes = new DQLSelectBuilder().fromEntity(StudentExtractType.class, "e")
                .column("e")
                .where(eq(property("e", StudentExtractType.P_ACTIVE), value(Boolean.TRUE)))
                .where(or(
                        eq(property("e", StudentExtractType.parent().code()), value(StudentExtractTypeCodes.MODULAR_ORDER)),
                        eq(property("e", StudentExtractType.parent().code()), value(StudentExtractTypeCodes.LIST_ORDER))
                ))
                .where(notExists(alreadyAdded.buildQuery()));

        // Далее - прочие с присоединением сборных и списочных
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentExtractType.class, "e")
                .column("e")
                .joinEntity("e", DQLJoinType.inner, StudentExtractType.class, "e2", eq(property("e", StudentExtractType.L_PARENT), property("e2.id")))
                .joinEntity("e2", DQLJoinType.left, StudentExtractType.class, "e3", eq(property("e2", StudentExtractType.L_PARENT), property("e3.id")))
                .where(or(
                        eq(property("e2", StudentExtractType.P_CODE), value(StudentExtractTypeCodes.OTHER_ORDER)),
                        eq(property("e3", StudentExtractType.P_CODE), value(StudentExtractTypeCodes.OTHER_ORDER))
                ))
                .where(eq(property("e", StudentExtractType.P_ACTIVE), value(Boolean.TRUE)))
                .where(notExists(alreadyAdded.buildQuery()))
                .union(listAndModularTypes.buildSelectRule(), true);

        model.setSecondHierarchyList(HierarchyUtil.listHierarchyNodesWithParents(builder.createStatement(getSession()).<StudentExtractType>list(), new StudentExtractType.ItemsComparator(), false));
    }
}