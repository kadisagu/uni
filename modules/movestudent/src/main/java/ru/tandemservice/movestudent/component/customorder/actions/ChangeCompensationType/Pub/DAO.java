/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeCompensationType.Pub;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.BaseCustomActionPubDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeCompensationTypeAction;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class DAO extends BaseCustomActionPubDAO<ChangeCompensationTypeAction, Model>
{
    @Override
    protected Class<ChangeCompensationTypeAction> getActionClass()
    {
        return ChangeCompensationTypeAction.class;
    }
}