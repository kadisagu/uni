/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractAddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author vip_delete
 * @since 05.11.2008
 */
public abstract class ModularStudentExtractAddEditController<T extends ModularStudentExtract, IDAO extends IModularStudentExtractAddEditDAO<T, Model>, Model extends ModularStudentExtractAddEditModel<T>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);
    }

    public final void onChangeExtractType(IBusinessComponent component)
    {
        ModularStudentExtractAddEditModel model = getModel(component);

        Student student = getDao().getNotNull(model.getStudentId());
        if (student.getGroup() == null && !MoveStudentDaoFacade.getMoveStudentDao().canBeCreatedExtractForStudentWithoutGroup(model.getExtractType()))
        {
            ErrorCollector errors = ContextLocal.getErrorCollector();
            errors.add("Данный тип приказа нельзя создавать для студента вне группы.", "extractType");
            return;
        }

        //deactivate(component);

        component.createDefaultChildRegion(new ComponentActivator(MoveStudentUtils.getModularExtractAddEditComponent(model.getExtractType()), new ParametersMap()
                .add("studentId", model.getStudentId())
                .add("extractTypeId", model.getExtractType().getId())
        ));
    }

    // для простоты делаем оптимистическую блокировку, так как внутри много проверок
    public final synchronized void onClickApply(IBusinessComponent component)
    {
        ErrorCollector errors = component.getUserContext().getErrorCollector();

        Model model = getModel(component);

        getDao().validate(model, errors);

        if (errors.hasErrors()) return;

        getDao().update(model);

        if (model.isAddForm())
        {
            deactivate(component, 2);//назад на 2-е страницы
            activateInRoot(component, new PublisherActivator(model.getExtract()));
        } else
            deactivate(component);
    }

    public void onClickDeactivate(IBusinessComponent component)
    {
        if (getModel(component).isAddForm())
            deactivate(component, 2);
        else
            deactivate(component);
    }
}