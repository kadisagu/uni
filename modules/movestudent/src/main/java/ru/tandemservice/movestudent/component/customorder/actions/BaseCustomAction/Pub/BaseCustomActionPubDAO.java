/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub;

import ru.tandemservice.movestudent.component.customorder.IStudentCustomOrderExtract;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.entity.ICustomOrderAction;
import ru.tandemservice.unimove.entity.ICustomOrderExtract;
import ru.tandemservice.unimove.entity.gen.ICustomOrderActionGen;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public abstract class BaseCustomActionPubDAO<T extends ICustomOrderAction, Model extends BaseCustomActionPubModel<T>> extends UniDao<Model> implements IBaseCustomActionPubDAO<T, Model>
{
    protected abstract Class<T> getActionClass();

    @Override
    public void prepare(final Model model)
    {
        IStudentCustomOrderExtract extract = (IStudentCustomOrderExtract) getNotNull(ICustomOrderExtract.class, model.getExtractId());
        model.setExtract(extract);
        model.setCustomAction(getAction(model.getExtractId()));
    }

    protected T getAction(Long extractId)
    {
        return get(getActionClass(), ICustomOrderActionGen.customExtract().id(), extractId);
    }
}