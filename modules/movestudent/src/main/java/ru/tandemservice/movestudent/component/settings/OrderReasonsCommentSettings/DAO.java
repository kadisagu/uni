/* $Id$ */
package ru.tandemservice.movestudent.component.settings.OrderReasonsCommentSettings;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;

/**
 * @author Dmitry Seleznev
 * @since 05.12.2012
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(StudentOrderReasons.ENTITY_CLASS, "o");
        new OrderDescriptionRegistry("o").applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    public void updateCommentable(long reasonId)
    {
        StudentOrderReasons reason = get(StudentOrderReasons.class, reasonId);
        if (reason != null)
        {
            if (reason.isCommentable()) reason.setCommentRequired(false);
            reason.setCommentable(!reason.isCommentable());
            update(reason);
        }
    }

    @Override
    public void updateRequired(long reasonId)
    {
        StudentOrderReasons reason = get(StudentOrderReasons.class, reasonId);
        if (reason != null)
        {
            reason.setCommentRequired(!reason.isCommentRequired());
            update(reason);
        }
    }
}