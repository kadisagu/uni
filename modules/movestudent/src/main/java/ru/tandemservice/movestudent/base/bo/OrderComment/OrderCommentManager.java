/* $Id$ */
package ru.tandemservice.movestudent.base.bo.OrderComment;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Ekaterina Zvereva
 * @since 14.01.2015
 */
@Configuration
public class OrderCommentManager extends BusinessObjectManager
{
    public static OrderCommentManager instance()
    {
        return instance(OrderCommentManager.class);
    }
}