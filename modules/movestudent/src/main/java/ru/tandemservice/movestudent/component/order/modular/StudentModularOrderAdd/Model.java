/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.component.order.modular.StudentModularOrderAdd;

import org.tandemframework.core.component.Input;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.movestudent.component.commons.ExtractListModel;
import ru.tandemservice.movestudent.entity.StudentModularOrder;

/**
 * @author vip_delete
 * @since 20.10.2008
 */
@Input(keys = {"orgUnitId"}, bindings = {"orgUnitId"})
public class Model extends ExtractListModel
{
    private StudentModularOrder _order = new StudentModularOrder();
    private Long _orgUnitId;
    private boolean _disableCommitDate;
    private boolean _disableOrderNumber;
    private ISelectModel _employeePostModel;
    private EmployeePost _employeePost;
    private EmployeePost _responsibleEmployee;

    public StudentModularOrder getOrder()
    {
        return _order;
    }

    public void setOrder(StudentModularOrder order)
    {
        _order = order;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public boolean isDisableCommitDate()
    {
        return _disableCommitDate;
    }

    public void setDisableCommitDate(boolean disableCommitDate)
    {
        _disableCommitDate = disableCommitDate;
    }

    public boolean isDisableOrderNumber()
    {
        return _disableOrderNumber;
    }

    public void setDisableOrderNumber(boolean disableOrderNumber)
    {
        _disableOrderNumber = disableOrderNumber;
    }

    public ISelectModel getEmployeePostModel()
    {
        return _employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        _employeePostModel = employeePostModel;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        _employeePost = employeePost;
    }

    public EmployeePost getResponsibleEmployee()
    {
        return _responsibleEmployee;
    }

    public void setResponsibleEmployee(EmployeePost responsibleEmployee)
    {
        _responsibleEmployee = responsibleEmployee;
    }
}
