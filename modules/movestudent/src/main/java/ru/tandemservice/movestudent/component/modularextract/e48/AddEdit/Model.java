/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e48.AddEdit;

import com.google.common.collect.Lists;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.RemoveGroupManagerStuExtract;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 13.08.2012
 */
public class Model extends CommonModularStudentExtractAddEditModel<RemoveGroupManagerStuExtract>
{
    private List<Group> _groupsList = Lists.newArrayList();
    private IMultiSelectModel _groupListModel;

    public List<Group> getGroupsList()
    {
        return _groupsList;
    }

    public void setGroupsList(List<Group> groupsList)
    {
        _groupsList = groupsList;
    }

    public IMultiSelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(IMultiSelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }
}
