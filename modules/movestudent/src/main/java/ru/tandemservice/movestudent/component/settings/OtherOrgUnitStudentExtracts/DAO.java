/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.settings.OtherOrgUnitStudentExtracts;

import java.util.List;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.movestudent.entity.OtherOrgUnitExtractType;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;

/**
 * @author dseleznev
 * Created on: 18.03.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder settingsBuilder = new MQBuilder(OtherOrgUnitExtractType.ENTITY_CLASS, "s", new String[] {OtherOrgUnitExtractType.L_TYPE});
        settingsBuilder.add(MQExpression.eq("s", OtherOrgUnitExtractType.P_ACTIVE, Boolean.TRUE));
        List<StudentExtractType> extrTypes = settingsBuilder.getResultList(getSession());
        
        MQBuilder builder = new MQBuilder(StudentExtractType.ENTITY_CLASS, "et");
        builder.add(MQExpression.eq("et", StudentExtractType.P_ACTIVE, Boolean.TRUE));
        builder.add(MQExpression.eq("et", StudentExtractType.L_PARENT, getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.MODULAR_ORDER)));
        new OrderDescriptionRegistry("et").applyOrder(builder, model.getDataSource().getEntityOrder());
        List<StudentExtractType> list = builder.getResultList(getSession());
        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);
        
        for (ViewWrapper<IEntity> wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            wrapper.setViewProperty(Model.P_CAN_BE_CREATED_BY_THE_OTHER_ORG_UNIT, extrTypes.contains(wrapper.getEntity()));
        }
    }

    @Override
    public void updateCanBeCreatedByTheOtherOrgUnit(long extractTypeId)
    {
        StudentExtractType extractType = get(StudentExtractType.class, extractTypeId);
        if (null != extractType)
        {
            OtherOrgUnitExtractType setting = get(OtherOrgUnitExtractType.class, OtherOrgUnitExtractType.L_TYPE, extractType);
            
            if (null == setting)
            {
                setting = new OtherOrgUnitExtractType();
                setting.setType(extractType);
                setting.setActive(true);
                save(setting);
            }
            else
            {
                setting.setActive(!setting.isActive());
            }
        }
    }
}