/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.utils;

import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.BusinessComponentRuntime;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.component.order.modular.StudentModularOrderPub.Controller;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.IAbstractExtract;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 05.11.2008
 */
public final class MoveStudentUtils
{
    public static final String MOVESTUDENT_PDF_ENABLED_PROPERTY = "movestudent.order.print.pdf.enabled";
    public static final boolean MOVESTUDENT_ORDER_PRINT_PDF_ENABLED = Boolean.parseBoolean(ApplicationRuntime.getProperty(MOVESTUDENT_PDF_ENABLED_PROPERTY));

    public static final String MOVESTUDENT_COMPONENTS_MAP_NAME = "movestudentComponentsMap";

    public static final String COMPONENT_MODULAR_EXTRACT_ADD_EDIT_POSTFIX = "AddEdit";

    public static final String COMPONENT_LIST_ORDER_PUB_POSTFIX = "ListOrderPub";
    public static final String COMPONENT_LIST_ORDER_ADD_EDIT_POSTFIX = "ListOrderAddEdit";
    public static final String COMPONENT_LIST_PARAGRAPH_PUB_POSTFIX = "ParagraphPub";
    public static final String COMPONENT_LIST_PARAGRAPH_ADD_POSTFIX = "ParagraphAddEdit";
    public static final String COMPONENT_LIST_MULTIPLE_PARAGRAPH_ADD_POSTFIX = "MultipleParagraphAdd";
    public static final String COMPONENT_LIST_EXTRACT_PUB_POSTFIX = "ListExtractPub";

    public static final String MODULAR_EXTRACT_PARENT_PACKAGE_NAME = "ru.tandemservice.movestudent.component.modularextract.e";
    public static final String LIST_EXTRACT_PARENT_PACKAGE_NAME = "ru.tandemservice.movestudent.component.listextract.e";

    //Comparator
    /**
     * Ссылка на компаратор сортирующий выписки сборного приказа:<p>
     * <ol>
     * <li>по названию формирующего подразделения
     * <li>по курсу
     * <li>ФИО
     * </ol>
     */
    public static ModularStudentExtractComparator MODULAR_STUDENT_EXTRACT_COMPARATOR = new ModularStudentExtractComparator();

    /**
     * Компаратор сортирующий выписки сборного приказа:<p>
     * <ol>
     * <li>по названию формирующего подразделения
     * <li>по курсу
     * <li>ФИО
     * </ol>
     */
    private static class ModularStudentExtractComparator implements Comparator<ModularStudentExtract>
    {
        @Override
        public int compare(ModularStudentExtract o1, ModularStudentExtract o2)
        {
            String title1 = o1.getEntity().getEducationOrgUnit().getFormativeOrgUnit().getTitle();
            String title2 = o2.getEntity().getEducationOrgUnit().getFormativeOrgUnit().getTitle();

            int r = title1.compareTo(title2);
            if (r != 0) return r;

            r = Course.COURSE_COMPARATOR.compare(o1.getEntity().getCourse(), o2.getEntity().getCourse());
            if (r != 0) return r;

            return Student.FULL_FIO_AND_ID_COMPARATOR.compare(o1.getEntity(), o2.getEntity());
        }
    }

    /**
     * Общий метод для получения имени компонента (добавления/редактирования, карточки) по типу выписки
     *
     * @param extractType                - тип выписки из приказа по студентам
     * @param componentPostfix           - постфикс искомого компонента
     * @param componentParentPackageName - наименование родительского пэкейджа для компонентов выписки.
     * @return - полное имя пэкейджа искомого компонента
     */
    @SuppressWarnings("unchecked")
    private static String getExtractComponentName(StudentExtractType extractType, String componentPostfix, String componentParentPackageName, boolean allowNulls)
    {
        if (null != extractType.getBusinessObjectName())
        {
            if (ApplicationRuntime.containsBean(MOVESTUDENT_COMPONENTS_MAP_NAME))
            {
                Map<String, String> componentsMap = (Map<String, String>) ApplicationRuntime.getBean(MOVESTUDENT_COMPONENTS_MAP_NAME);
                String compName = componentsMap.get(extractType.getBusinessObjectName() + componentPostfix);
                if (null != compName) return compName;
            }
        }

        if (null != extractType.getIndex())
            return componentParentPackageName + extractType.getIndex() + "." + componentPostfix;

        if (allowNulls)
            return null;

        throw new UnsupportedOperationException();
    }

    /**
     * Возвращает наименование компонента добавления проекта сборного приказа для студента (e0)
     *
     * @return - полное имя пэкейджа искомого компонента
     */
    public static String getModularExtractAddEditComponent()
    {
        return MODULAR_EXTRACT_PARENT_PACKAGE_NAME + MoveStudentDefines.EMPTY_MODULAR_STUDENT_EXTRACT_INDEX + "." + COMPONENT_MODULAR_EXTRACT_ADD_EDIT_POSTFIX;
    }

    /**
     * Возвращает имя пэкейджа компонента добавления/редактирования выписки из сборного приказа для указанного типа выписки.
     *
     * @param extractType - тип выписки из сборного приказа по студенту.
     * @return - полное имя пэкейджа искомого компонента
     */
    public static String getModularExtractAddEditComponent(StudentExtractType extractType)
    {
        return getExtractComponentName(extractType, COMPONENT_MODULAR_EXTRACT_ADD_EDIT_POSTFIX, MODULAR_EXTRACT_PARENT_PACKAGE_NAME, false);
    }

    /**
     * Возвращает имя пэкейджа компонента добавления выписки студенту на подразделении.
     *
     * @return - полное имя пэкейджа компонента добавления выписки студенту на подразделении.
     */
    public static String getOrgUnitModularExtractAddEditComponent()
    {
        return MODULAR_EXTRACT_PARENT_PACKAGE_NAME + MoveStudentDefines.EMPTY_MODULAR_STUDENT_EXTRACT_INDEX + ".OrgUnitExtractAddEdit";
    }

    /**
     * Возвращает имя пэкейджа компонента добавления/редактирования по идентификатору выписки
     *
     * @param extractId - идентификатор выписки из сборного приказа
     * @return - полное имя пэкейджа компонента добавления выписки студенту на подразделении.
     */
    public static String getModularExtractAddEditComponentNew(Long extractId)
    {
        IAbstractExtract extract = UniDaoFacade.getCoreDao().get(extractId);
        return getModularExtractAddEditComponent((StudentExtractType) extract.getType());
    }

    /**
     * Возвращает имя пэкейджа компонента добавления/редактирования списочного приказа.
     * На текущий момент для всех списочноых приказов используется один и тот же компонент.
     *
     * @param extractType - тип выписки из списочного приказа (тип списочного приказа) по студенту.
     * @return - полное имя пэкейджа искомого компонента
     */
    public static String getListOrderAddEditComponent(StudentExtractType extractType)
    {
        return getExtractComponentName(extractType, COMPONENT_LIST_ORDER_ADD_EDIT_POSTFIX, LIST_EXTRACT_PARENT_PACKAGE_NAME, false);
    }

    /**
     * Возвращает имя пэкейджа компонента карточки списочного приказа.
     * На текущий момент для всех списочноых приказов используется один и тот же компонент.
     *
     * @param extractType - тип выписки из списочного приказа (тип списочного приказа) по студенту.
     * @return - полное имя пэкейджа искомого компонента
     */
    public static String getListOrderPubComponent(StudentExtractType extractType)
    {
        return getExtractComponentName(extractType, COMPONENT_LIST_ORDER_PUB_POSTFIX, LIST_EXTRACT_PARENT_PACKAGE_NAME, false);
    }

    /**
     * Возвращает имя пэкейджа компонента добавления множества параграфов списочного приказа.
     *
     * @param extractType - тип выписки из списочного приказа (тип параграфа списочного приказа) по студенту.
     * @return - полное имя пэкейджа искомого компонента
     */
    public static String getListMultipleParagraphAddComponent(StudentExtractType extractType)
    {
        String componentName = "listextract_" + extractType.getCode() + "_MultipleParagraphAdd";
        if (null != BusinessComponentRuntime.getInstance().getComponentMeta(componentName))
            return componentName;
        return getExtractComponentName(extractType, COMPONENT_LIST_MULTIPLE_PARAGRAPH_ADD_POSTFIX, LIST_EXTRACT_PARENT_PACKAGE_NAME, true);
    }

    /**
     * Возвращает имя пэкейджа компонента добавления/редактирования параграфа списочного приказа.
     *
     * @param extractType - тип выписки из списочного приказа (тип параграфа списочного приказа) по студенту.
     * @return - полное имя пэкейджа искомого компонента
     */
    public static String getListParagraphAddEditComponent(StudentExtractType extractType)
    {
        String componentName = "listextract_" + extractType.getCode() + "_ParagraphAddEdit";
        if (null != BusinessComponentRuntime.getInstance().getComponentMeta(componentName))
            return componentName;
        return getExtractComponentName(extractType, COMPONENT_LIST_PARAGRAPH_ADD_POSTFIX, LIST_EXTRACT_PARENT_PACKAGE_NAME, false);
    }

    /**
     * Возвращает имя пэкейджа компонента карточки параграфа списочного приказа.
     *
     * @param extractType - тип выписки из списочного приказа (тип параграфа списочного приказа) по студенту.
     * @return - полное имя пэкейджа искомого компонента
     */
    public static String getListParagraphPubComponent(StudentExtractType extractType)
    {
        String componentName = "listextract_" + extractType.getCode() + "_ParagraphPub";
        if (null != BusinessComponentRuntime.getInstance().getComponentMeta(componentName))
            return componentName;
        return getExtractComponentName(extractType, COMPONENT_LIST_PARAGRAPH_PUB_POSTFIX, LIST_EXTRACT_PARENT_PACKAGE_NAME, false);
    }

    /**
     * Возвращает имя пэкейджа компонента карточки выписки списочного приказа.
     *
     * @param extractType - тип выписки из списочного приказа по студенту.
     * @return - полное имя пэкейджа искомого компонента
     */
    public static String getListExtractPubComponent(StudentExtractType extractType)
    {
        String componentName = "listextract_" + extractType.getCode() + "_ListExtractPub";
        if (null != BusinessComponentRuntime.getInstance().getComponentMeta(componentName))
            return componentName;
        return getExtractComponentName(extractType, COMPONENT_LIST_EXTRACT_PUB_POSTFIX, LIST_EXTRACT_PARENT_PACKAGE_NAME, false);
    }

    /**
     * Поскольку у абстрактных выписок куча наследников, приходится грузить не сразу сущность для базового класса,
     * а отдельно идентификаторы, а потом поклассово сами выписки.
     */
    @SuppressWarnings("unchecked")
    public static void createExtractPage(Session session, OrderDescriptionRegistry orderDescriptionRegistry, DynamicListDataSource<? extends IEntity> dataSource, MQBuilder builder, String extractAlias)
    {
        orderDescriptionRegistry.applyOrder(builder, dataSource.getEntityOrder());
        builder.getSelectAliasList().clear();
        builder.addSelect(extractAlias, new String[]{IEntity.P_ID});


        // Дальше идет копипаста из CommonBaseSearchListUtil.
        // Но ничего не поделать, т.к. нам нужны только id!
        // ---------------------------------------- start c & p
        final boolean printEnabled = dataSource.isPrintEnabled();
        final int pageSize = (int) (printEnabled ? dataSource.getPrintCountRow() : dataSource.getCountRow());

        Integer totalSize = null;
        if (dataSource.getActualStartRow() != 0L && pageSize > 0) {
            totalSize = (int) builder.getResultCount(session);
            dataSource.setTotalSize(totalSize);
        }
        // getStartRow апдейтит startRow на основе totalSize
        int startRow = (int) (printEnabled ? dataSource.getPrintStartRow() : dataSource.getStartRow());

        List list = totalSize == null || totalSize > 0
                ? (pageSize > 0 ? builder.getResultList(session, startRow, pageSize) : builder.getResultList(session))
                : Collections.emptyList();

        if (null == totalSize) {

            // Если это первая страница (startRow == 0), то размер всего списка
            // получать отдельным запросом необходимо, только если запрос списка элементов страницы
            // вернул число записей равное размеру страницы.

            if (pageSize > 0 && list.size() == pageSize) {
                totalSize = (int) builder.getResultCount(session);
            } else {
                totalSize = list.size();
            }
        }
        dataSource.setTotalSize(totalSize);
        // ---------------------------------------- c & p end

        List<AbstractStudentExtract> extractList = MoveStudentDaoFacade.getMoveStudentDao().getExtractListByIds(list, AbstractStudentExtract.class);
        extractList = CommonDAO.sort(extractList, list);

        dataSource.createPage((List) extractList);
    }

    /**
     * Поскольку у абстрактных выписок куча наследников, приходится грузить не сразу сущность для базового класса,
     * а отдельно идентификаторы, а потом поклассово сами выписки.
     */
    @SuppressWarnings("unchecked")
    public static void createExtractPage(Session session, DynamicListDataSource<? extends IEntity> dataSource, DQLSelectBuilder builder, String extractAlias)
    {
        builder.resetColumns();
        builder.column(DQLExpressions.property(extractAlias, IEntity.P_ID));


        // Дальше идет копипаста из CommonBaseSearchListUtil.
        // Но ничего не поделать, т.к. нам нужны только id!
        // ---------------------------------------- start c & p
        final boolean printEnabled = dataSource.isPrintEnabled();
        final int pageSize = (int) (printEnabled ? dataSource.getPrintCountRow() : dataSource.getCountRow());

        final DQLExecutionContext executionContext = new DQLExecutionContext(session);

        Integer totalSize = null;
        if (dataSource.getActualStartRow() != 0L && pageSize > 0) {
            final Number count = builder.createCountStatement(executionContext).uniqueResult();
            totalSize = count == null ? 0 : count.intValue();

            dataSource.setTotalSize(totalSize);
        }
        // getStartRow апдейтит startRow на основе totalSize
        int startRow = (int) (printEnabled ? dataSource.getPrintStartRow() : dataSource.getStartRow());

        List list;
        if (totalSize == null || totalSize > 0)
        {
            final IDQLStatement stmt = builder.createStatement(executionContext);
            if (pageSize > 0) {
                stmt.setFirstResult(startRow);
                stmt.setMaxResults(pageSize);
                list = stmt.list();
            } else {
                list = stmt.list();
            }
        }
        else {
            list = Collections.emptyList();
        }

        if (null == totalSize) {

            // Если это первая страница (startRow == 0), то размер всего списка
            // получать отдельным запросом необходимо, только если запрос списка элементов страницы
            // вернул число записей равное размеру страницы.

            if (pageSize > 0 && list.size() == pageSize) {
                final Number count = builder.createCountStatement(executionContext).uniqueResult();
                totalSize = count == null ? 0 : count.intValue();
            } else {
                totalSize = list.size();
            }
        }
        dataSource.setTotalSize(totalSize);
        // ---------------------------------------- c & p end

        List<AbstractStudentExtract> extractList = MoveStudentDaoFacade.getMoveStudentDao().getExtractListByIds(list, AbstractStudentExtract.class);
        extractList = CommonDAO.sort(extractList, list);

        dataSource.createPage((List) extractList);
    }

    /**
     * Возвращает имя пэкейджа компонента карточки сборного приказа.
     *
     * @return - полное имя пэкейджа публикатора сборного приказа.
     */
    public static String getModularOrderPubComponent()
    {
        return Controller.class.getPackage().getName();
    }

    @Deprecated
    public static String getModularExtractAddEditComponent(int extractIndex)
    {
        return "ru.tandemservice.movestudent.component.modularextract.e" + extractIndex + ".AddEdit";
    }

    @Deprecated
    public static String getOrgUnitModularExtractAddEditComponent(int extractIndex)
    {
        return "ru.tandemservice.movestudent.component.modularextract.e" + extractIndex + ".OrgUnitExtractAddEdit";
    }

    @Deprecated
    public static String getModularExtractAddEditComponent(Long extractId)
    {
        IAbstractExtract extract = UniDaoFacade.getCoreDao().get(extractId);
        int extractIndex = ((StudentExtractType) extract.getType()).getIndex();
        return getModularExtractAddEditComponent(extractIndex);
    }

    @Deprecated
    public static String getListOrderAddEditComponent(int orderTypeIndex)
    {
        return "ru.tandemservice.movestudent.component.listextract.e" + orderTypeIndex + ".ListOrderAddEdit";
    }

    @Deprecated
    public static String getListOrderPubComponent(int orderTypeIndex)
    {
        return "ru.tandemservice.movestudent.component.listextract.e" + orderTypeIndex + ".ListOrderPub";
    }

    @Deprecated
    public static String getListMultipleParagraphAddComponent(int extractIndex)
    {
        return "ru.tandemservice.movestudent.component.listextract.e" + extractIndex + ".MultipleParagraphAdd";
    }

    @Deprecated
    public static String getListParagraphAddEditComponent(int extractIndex)
    {
        return "ru.tandemservice.movestudent.component.listextract.e" + extractIndex + ".ParagraphAddEdit";
    }

    @Deprecated
    public static String getListParagraphPubComponent(int extractIndex)
    {
        return "ru.tandemservice.movestudent.component.listextract.e" + extractIndex + ".ParagraphPub";
    }

    @Deprecated
    public static String getListExtractPubComponent(int extractIndex)
    {
        return "ru.tandemservice.movestudent.component.listextract.e" + extractIndex + ".ListExtractPub";
    }
}