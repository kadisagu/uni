/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e90.AddEdit;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.movestudent.entity.WeekendChildCareOutStuExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.IExtEducationLevelModel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 26.12.2012
 */
public class Model extends CommonModularStudentExtractAddEditModel<WeekendChildCareOutStuExtract> implements IGroupModel, IEducationLevelModel, IExtEducationLevelModel
{
    private ISelectModel _groupModel;
    private Group _group;

    private StuExtractToDebtRelation _currentDebt;

    private List<StuExtractToDebtRelation> _debtsList = new ArrayList<StuExtractToDebtRelation>();

    private List<StuExtractToDebtRelation> _debtsToDel = new ArrayList<StuExtractToDebtRelation>();

    public ISelectModel getGroupModel()
    {
        return _groupModel;
    }

    public void setGroupModel(ISelectModel groupModel)
    {
        _groupModel = groupModel;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public StuExtractToDebtRelation getCurrentDebt()
    {
        return _currentDebt;
    }

    public void setCurrentDebt(StuExtractToDebtRelation currentDebt)
    {
        this._currentDebt = currentDebt;
    }

    public List<StuExtractToDebtRelation> getDebtsList()
    {
        return _debtsList;
    }

    public void setDebtsList(List<StuExtractToDebtRelation> debtsList)
    {
        this._debtsList = debtsList;
    }

    public List<StuExtractToDebtRelation> getDebtsToDel()
    {
        return _debtsToDel;
    }

    public void setDebtsToDel(List<StuExtractToDebtRelation> debtsToDel)
    {
        this._debtsToDel = debtsToDel;
    }

    public Integer getCurrentDebtNumber()
    {
        return getDebtsList().indexOf(getCurrentDebt());
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return getExtract().getEntity().getEducationOrgUnit().getFormativeOrgUnit();
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return getExtract().getEntity().getEducationOrgUnit().getTerritorialOrgUnit();
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return getExtract().getEntity().getEducationOrgUnit().getEducationLevelHighSchool();
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return getExtract().getEntity().getEducationOrgUnit().getDevelopForm();
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return getExtract().getEntity().getEducationOrgUnit().getDevelopCondition();
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return getExtract().getEntity().getEducationOrgUnit().getDevelopTech();
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return getExtract().getEntity().getEducationOrgUnit().getDevelopPeriod();
    }

    @Override
    public EducationLevels getParentEduLevel()
    {
        return EducationOrgUnitUtil.getParentLevel(getEducationLevelsHighSchool());
    }

    @Override
    public Course getCourse()
    {
        return getExtract().getEntity().getCourse();
    }
}
