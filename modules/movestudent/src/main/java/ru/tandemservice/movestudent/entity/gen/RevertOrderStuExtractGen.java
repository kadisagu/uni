package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.RevertOrderStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об отмене приказа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RevertOrderStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.RevertOrderStuExtract";
    public static final String ENTITY_NAME = "revertOrderStuExtract";
    public static final int VERSION_HASH = -600110180;
    private static IEntityMeta ENTITY_META;

    public static final String L_CANCELED_EXTRACT = "canceledExtract";

    private AbstractStudentExtract _canceledExtract;     // Выписка для отмены

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка для отмены. Свойство не может быть null.
     */
    @NotNull
    public AbstractStudentExtract getCanceledExtract()
    {
        return _canceledExtract;
    }

    /**
     * @param canceledExtract Выписка для отмены. Свойство не может быть null.
     */
    public void setCanceledExtract(AbstractStudentExtract canceledExtract)
    {
        dirty(_canceledExtract, canceledExtract);
        _canceledExtract = canceledExtract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RevertOrderStuExtractGen)
        {
            setCanceledExtract(((RevertOrderStuExtract)another).getCanceledExtract());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RevertOrderStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RevertOrderStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new RevertOrderStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "canceledExtract":
                    return obj.getCanceledExtract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "canceledExtract":
                    obj.setCanceledExtract((AbstractStudentExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "canceledExtract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "canceledExtract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "canceledExtract":
                    return AbstractStudentExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RevertOrderStuExtract> _dslPath = new Path<RevertOrderStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RevertOrderStuExtract");
    }
            

    /**
     * @return Выписка для отмены. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RevertOrderStuExtract#getCanceledExtract()
     */
    public static AbstractStudentExtract.Path<AbstractStudentExtract> canceledExtract()
    {
        return _dslPath.canceledExtract();
    }

    public static class Path<E extends RevertOrderStuExtract> extends ModularStudentExtract.Path<E>
    {
        private AbstractStudentExtract.Path<AbstractStudentExtract> _canceledExtract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка для отмены. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RevertOrderStuExtract#getCanceledExtract()
     */
        public AbstractStudentExtract.Path<AbstractStudentExtract> canceledExtract()
        {
            if(_canceledExtract == null )
                _canceledExtract = new AbstractStudentExtract.Path<AbstractStudentExtract>(L_CANCELED_EXTRACT, this);
            return _canceledExtract;
        }

        public Class getEntityClass()
        {
            return RevertOrderStuExtract.class;
        }

        public String getEntityName()
        {
            return "revertOrderStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
