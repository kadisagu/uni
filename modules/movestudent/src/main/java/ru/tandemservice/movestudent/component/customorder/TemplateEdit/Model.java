/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.TemplateEdit;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.movestudent.component.customorder.ICustomPrintFormContainer;

/**
 * @author Nikolay Fedorovskih
 * @since 24.09.2013
 */

@Input({
               @Bind(key = Model.OBJECT_ID_BIND, binding = Model.OBJECT_ID_BIND, required = true),
       })
public class Model
{
    public static final String OBJECT_ID_BIND = "objectId";

    private Long _objectId;
    private IUploadFile _uploadFile;
    private ICustomPrintFormContainer _userTemplateContainer;
    private Long _modifiableOrderId;

    public boolean isHasCurrent()
    {
        return getUserTemplateContainer().getFile() != null;
    }

    public boolean isHasModifiable()
    {
        return _modifiableOrderId != null;
    }

    public Long getObjectId()
    {
        return _objectId;
    }

    public void setObjectId(Long objectId)
    {
        _objectId = objectId;
    }

    public IUploadFile getUploadFile()
    {
        return _uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile)
    {
        _uploadFile = uploadFile;
    }

    public ICustomPrintFormContainer getUserTemplateContainer()
    {
        return _userTemplateContainer;
    }

    public void setUserTemplateContainer(ICustomPrintFormContainer userTemplateContainer)
    {
        _userTemplateContainer = userTemplateContainer;
    }

    public Long getModifiableOrderId()
    {
        return _modifiableOrderId;
    }

    public void setModifiableOrderId(Long modifiableOrderId)
    {
        _modifiableOrderId = modifiableOrderId;
    }
}