/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e2;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import ru.tandemservice.movestudent.entity.CourseTransferStuListExtract;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.group.IAbstractGroupTitleDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.util.GroupTitleAlgorithm;
import ru.tandemservice.unimove.dao.IExtractComponentDao;
import ru.tandemservice.unimove.dao.MoveDao;

import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 06.05.2009
 */
public class CourseTransferStuListExtractDao extends UniBaseDao implements IExtractComponentDao<CourseTransferStuListExtract>
{
    @Override
    public void doCommit(CourseTransferStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        extract.getEntity().setCourse(extract.getCourseNew());
        if (extract.isGroupChangeCourse() && !extract.getCourseNew().getCode().equals(extract.getGroup().getCourse().getCode()))
        {
            extract.getGroup().setCourse(extract.getCourseNew());
            validateGroupNumberIsntFree(extract.getGroup(), extract.getParagraph(), "При проведении", extract.getCourseOld());
            if (null == UserContext.getInstance() || !UserContext.getInstance().getErrorCollector().isHasFieldErrors())
                getSession().update(extract.getGroup());
        }
    }

    @Override
    public void doRollback(CourseTransferStuListExtract extract, Map parameters)
    {
        extract.getEntity().setCourse(extract.getCourseOld());
        if (extract.isGroupChangeCourse() && !extract.getCourseOld().getCode().equals(extract.getGroup().getCourse().getCode()))
        {
            extract.getGroup().setCourse(extract.getCourseOld());
            validateGroupNumberIsntFree(extract.getGroup(), extract.getParagraph(), "При откате", extract.getCourseNew());
            if (null == UserContext.getInstance() || !UserContext.getInstance().getErrorCollector().isHasFieldErrors())
                getSession().update(extract.getGroup());
        }
    }

    private void validateGroupNumberIsntFree(Group group, AbstractStudentParagraph paragraph, String preface, Course oldCourse)
    {
        GroupTitleAlgorithm algorithm = UniDaoFacade.getCoreDao().get(GroupTitleAlgorithm.class, GroupTitleAlgorithm.P_CURRENT, Boolean.TRUE);

        if (algorithm == null) return;

        IAbstractGroupTitleDAO generator = (IAbstractGroupTitleDAO) ApplicationRuntime.getBean(algorithm.getDaoName());
        String title = generator.getTitle(group, group.getNumber());

        if (UniDaoFacade.getGroupDao().isNonArchivalGroupExists(group, title))
        {
            MoveDao.addError(preface + " параграфа № " + paragraph.getNumber() + " приказа № " + paragraph.getOrder().getNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(paragraph.getOrder().getCommitDate()) + " группа " + group.getTitle() + " приобретает номер, уже занятый другой группой " + title + ".");
            group.setCourse(oldCourse);
        }
    }
}