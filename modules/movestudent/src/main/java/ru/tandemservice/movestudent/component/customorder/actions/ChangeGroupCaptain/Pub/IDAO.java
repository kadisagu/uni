/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeGroupCaptain.Pub;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.IBaseCustomActionPubDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeGroupCaptainAction;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public interface IDAO extends IBaseCustomActionPubDAO<ChangeGroupCaptainAction, Model>
{
}