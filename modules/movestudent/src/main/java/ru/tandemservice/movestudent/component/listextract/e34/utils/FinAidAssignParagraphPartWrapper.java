/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e34.utils;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.orgstruct.Group;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 03.10.2012
 */
public class FinAidAssignParagraphPartWrapper implements Comparable<FinAidAssignParagraphPartWrapper>
{
	private final OrgUnit _orgUnit;
	private final Group _group;
    private final ListStudentExtract _firstExtract;

    public FinAidAssignParagraphPartWrapper(OrgUnit orgUnit, Group group, ListStudentExtract firstExtract)
    {
        _orgUnit = orgUnit;
	    _group = group;
        _firstExtract = firstExtract;
    }

    private final List<FinAidAssignExtractWrapper> _extractWrapperList = new ArrayList<>();

	public OrgUnit getOrgUnit()
	{
		return _orgUnit;
	}

	public Group getGroup()
	{
		return _group;
	}

	public List<FinAidAssignExtractWrapper> getExtractWrapperList()
    {
        return _extractWrapperList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FinAidAssignParagraphPartWrapper))
            return false;

	    FinAidAssignParagraphPartWrapper that = (FinAidAssignParagraphPartWrapper) o;

        return _orgUnit.equals(that.getOrgUnit());
    }

    @Override
    public int hashCode()
    {
        return _orgUnit.hashCode();
    }

    @Override
    public int compareTo(@Nullable FinAidAssignParagraphPartWrapper o)
    {
	    if(o == null)
		    return -1;
        return _orgUnit.getId().compareTo(o.getOrgUnit().getId());
    }
}