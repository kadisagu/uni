package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.movestudent.entity.gen.ExcludeSingGrpStuListExtractGen;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * Проект приказа «Об отчислении для одной группы»
 */
public class ExcludeSingGrpStuListExtract extends ExcludeSingGrpStuListExtractGen implements IExcludeExtract
{
    public Group getGroup()
    {
        return getEntity().getGroup();
    }
}