package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.entity.gen.RePassDiscComissStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О пересдаче дисциплин учебного плана комиссии
 */
public class RePassDiscComissStuExtract extends RePassDiscComissStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getActionDateTime();
    }
}