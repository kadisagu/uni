package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AssignBenefitStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О назначении выплаты пособий
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AssignBenefitStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.AssignBenefitStuExtract";
    public static final String ENTITY_NAME = "assignBenefitStuExtract";
    public static final int VERSION_HASH = -315556470;
    private static IEntityMeta ENTITY_META;

    public static final String P_ASSIGN_BENEFIT = "assignBenefit";
    public static final String P_BENEFIT_TYPE = "benefitType";
    public static final String P_BENEFIT_BEGIN_DATE = "benefitBeginDate";
    public static final String P_BENEFIT_END_DATE = "benefitEndDate";
    public static final String P_ALLOW_INDEPENDENT_SCHEDULE = "allowIndependentSchedule";
    public static final String P_REASON_STR = "reasonStr";
    public static final String P_INDEP_SCHED_BEGIN_DATE = "indepSchedBeginDate";
    public static final String P_INDEP_SCHED_END_DATE = "indepSchedEndDate";

    private boolean _assignBenefit;     // Назначить пособие
    private String _benefitType;     // Вид пособия
    private Date _benefitBeginDate;     // Дата начала выплаты
    private Date _benefitEndDate;     // Дата окончания выплаты
    private boolean _allowIndependentSchedule;     // Предоставить свободное посещение занятий
    private String _reasonStr;     // В связи
    private Date _indepSchedBeginDate;     // Дата начала свободного посещения
    private Date _indepSchedEndDate;     // Дата окончания свободного посещения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Назначить пособие. Свойство не может быть null.
     */
    @NotNull
    public boolean isAssignBenefit()
    {
        return _assignBenefit;
    }

    /**
     * @param assignBenefit Назначить пособие. Свойство не может быть null.
     */
    public void setAssignBenefit(boolean assignBenefit)
    {
        dirty(_assignBenefit, assignBenefit);
        _assignBenefit = assignBenefit;
    }

    /**
     * @return Вид пособия.
     */
    @Length(max=255)
    public String getBenefitType()
    {
        return _benefitType;
    }

    /**
     * @param benefitType Вид пособия.
     */
    public void setBenefitType(String benefitType)
    {
        dirty(_benefitType, benefitType);
        _benefitType = benefitType;
    }

    /**
     * @return Дата начала выплаты.
     */
    public Date getBenefitBeginDate()
    {
        return _benefitBeginDate;
    }

    /**
     * @param benefitBeginDate Дата начала выплаты.
     */
    public void setBenefitBeginDate(Date benefitBeginDate)
    {
        dirty(_benefitBeginDate, benefitBeginDate);
        _benefitBeginDate = benefitBeginDate;
    }

    /**
     * @return Дата окончания выплаты.
     */
    public Date getBenefitEndDate()
    {
        return _benefitEndDate;
    }

    /**
     * @param benefitEndDate Дата окончания выплаты.
     */
    public void setBenefitEndDate(Date benefitEndDate)
    {
        dirty(_benefitEndDate, benefitEndDate);
        _benefitEndDate = benefitEndDate;
    }

    /**
     * @return Предоставить свободное посещение занятий. Свойство не может быть null.
     */
    @NotNull
    public boolean isAllowIndependentSchedule()
    {
        return _allowIndependentSchedule;
    }

    /**
     * @param allowIndependentSchedule Предоставить свободное посещение занятий. Свойство не может быть null.
     */
    public void setAllowIndependentSchedule(boolean allowIndependentSchedule)
    {
        dirty(_allowIndependentSchedule, allowIndependentSchedule);
        _allowIndependentSchedule = allowIndependentSchedule;
    }

    /**
     * @return В связи.
     */
    @Length(max=255)
    public String getReasonStr()
    {
        return _reasonStr;
    }

    /**
     * @param reasonStr В связи.
     */
    public void setReasonStr(String reasonStr)
    {
        dirty(_reasonStr, reasonStr);
        _reasonStr = reasonStr;
    }

    /**
     * @return Дата начала свободного посещения.
     */
    public Date getIndepSchedBeginDate()
    {
        return _indepSchedBeginDate;
    }

    /**
     * @param indepSchedBeginDate Дата начала свободного посещения.
     */
    public void setIndepSchedBeginDate(Date indepSchedBeginDate)
    {
        dirty(_indepSchedBeginDate, indepSchedBeginDate);
        _indepSchedBeginDate = indepSchedBeginDate;
    }

    /**
     * @return Дата окончания свободного посещения.
     */
    public Date getIndepSchedEndDate()
    {
        return _indepSchedEndDate;
    }

    /**
     * @param indepSchedEndDate Дата окончания свободного посещения.
     */
    public void setIndepSchedEndDate(Date indepSchedEndDate)
    {
        dirty(_indepSchedEndDate, indepSchedEndDate);
        _indepSchedEndDate = indepSchedEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof AssignBenefitStuExtractGen)
        {
            setAssignBenefit(((AssignBenefitStuExtract)another).isAssignBenefit());
            setBenefitType(((AssignBenefitStuExtract)another).getBenefitType());
            setBenefitBeginDate(((AssignBenefitStuExtract)another).getBenefitBeginDate());
            setBenefitEndDate(((AssignBenefitStuExtract)another).getBenefitEndDate());
            setAllowIndependentSchedule(((AssignBenefitStuExtract)another).isAllowIndependentSchedule());
            setReasonStr(((AssignBenefitStuExtract)another).getReasonStr());
            setIndepSchedBeginDate(((AssignBenefitStuExtract)another).getIndepSchedBeginDate());
            setIndepSchedEndDate(((AssignBenefitStuExtract)another).getIndepSchedEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AssignBenefitStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AssignBenefitStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new AssignBenefitStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "assignBenefit":
                    return obj.isAssignBenefit();
                case "benefitType":
                    return obj.getBenefitType();
                case "benefitBeginDate":
                    return obj.getBenefitBeginDate();
                case "benefitEndDate":
                    return obj.getBenefitEndDate();
                case "allowIndependentSchedule":
                    return obj.isAllowIndependentSchedule();
                case "reasonStr":
                    return obj.getReasonStr();
                case "indepSchedBeginDate":
                    return obj.getIndepSchedBeginDate();
                case "indepSchedEndDate":
                    return obj.getIndepSchedEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "assignBenefit":
                    obj.setAssignBenefit((Boolean) value);
                    return;
                case "benefitType":
                    obj.setBenefitType((String) value);
                    return;
                case "benefitBeginDate":
                    obj.setBenefitBeginDate((Date) value);
                    return;
                case "benefitEndDate":
                    obj.setBenefitEndDate((Date) value);
                    return;
                case "allowIndependentSchedule":
                    obj.setAllowIndependentSchedule((Boolean) value);
                    return;
                case "reasonStr":
                    obj.setReasonStr((String) value);
                    return;
                case "indepSchedBeginDate":
                    obj.setIndepSchedBeginDate((Date) value);
                    return;
                case "indepSchedEndDate":
                    obj.setIndepSchedEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "assignBenefit":
                        return true;
                case "benefitType":
                        return true;
                case "benefitBeginDate":
                        return true;
                case "benefitEndDate":
                        return true;
                case "allowIndependentSchedule":
                        return true;
                case "reasonStr":
                        return true;
                case "indepSchedBeginDate":
                        return true;
                case "indepSchedEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "assignBenefit":
                    return true;
                case "benefitType":
                    return true;
                case "benefitBeginDate":
                    return true;
                case "benefitEndDate":
                    return true;
                case "allowIndependentSchedule":
                    return true;
                case "reasonStr":
                    return true;
                case "indepSchedBeginDate":
                    return true;
                case "indepSchedEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "assignBenefit":
                    return Boolean.class;
                case "benefitType":
                    return String.class;
                case "benefitBeginDate":
                    return Date.class;
                case "benefitEndDate":
                    return Date.class;
                case "allowIndependentSchedule":
                    return Boolean.class;
                case "reasonStr":
                    return String.class;
                case "indepSchedBeginDate":
                    return Date.class;
                case "indepSchedEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AssignBenefitStuExtract> _dslPath = new Path<AssignBenefitStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AssignBenefitStuExtract");
    }
            

    /**
     * @return Назначить пособие. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AssignBenefitStuExtract#isAssignBenefit()
     */
    public static PropertyPath<Boolean> assignBenefit()
    {
        return _dslPath.assignBenefit();
    }

    /**
     * @return Вид пособия.
     * @see ru.tandemservice.movestudent.entity.AssignBenefitStuExtract#getBenefitType()
     */
    public static PropertyPath<String> benefitType()
    {
        return _dslPath.benefitType();
    }

    /**
     * @return Дата начала выплаты.
     * @see ru.tandemservice.movestudent.entity.AssignBenefitStuExtract#getBenefitBeginDate()
     */
    public static PropertyPath<Date> benefitBeginDate()
    {
        return _dslPath.benefitBeginDate();
    }

    /**
     * @return Дата окончания выплаты.
     * @see ru.tandemservice.movestudent.entity.AssignBenefitStuExtract#getBenefitEndDate()
     */
    public static PropertyPath<Date> benefitEndDate()
    {
        return _dslPath.benefitEndDate();
    }

    /**
     * @return Предоставить свободное посещение занятий. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AssignBenefitStuExtract#isAllowIndependentSchedule()
     */
    public static PropertyPath<Boolean> allowIndependentSchedule()
    {
        return _dslPath.allowIndependentSchedule();
    }

    /**
     * @return В связи.
     * @see ru.tandemservice.movestudent.entity.AssignBenefitStuExtract#getReasonStr()
     */
    public static PropertyPath<String> reasonStr()
    {
        return _dslPath.reasonStr();
    }

    /**
     * @return Дата начала свободного посещения.
     * @see ru.tandemservice.movestudent.entity.AssignBenefitStuExtract#getIndepSchedBeginDate()
     */
    public static PropertyPath<Date> indepSchedBeginDate()
    {
        return _dslPath.indepSchedBeginDate();
    }

    /**
     * @return Дата окончания свободного посещения.
     * @see ru.tandemservice.movestudent.entity.AssignBenefitStuExtract#getIndepSchedEndDate()
     */
    public static PropertyPath<Date> indepSchedEndDate()
    {
        return _dslPath.indepSchedEndDate();
    }

    public static class Path<E extends AssignBenefitStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Boolean> _assignBenefit;
        private PropertyPath<String> _benefitType;
        private PropertyPath<Date> _benefitBeginDate;
        private PropertyPath<Date> _benefitEndDate;
        private PropertyPath<Boolean> _allowIndependentSchedule;
        private PropertyPath<String> _reasonStr;
        private PropertyPath<Date> _indepSchedBeginDate;
        private PropertyPath<Date> _indepSchedEndDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Назначить пособие. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AssignBenefitStuExtract#isAssignBenefit()
     */
        public PropertyPath<Boolean> assignBenefit()
        {
            if(_assignBenefit == null )
                _assignBenefit = new PropertyPath<Boolean>(AssignBenefitStuExtractGen.P_ASSIGN_BENEFIT, this);
            return _assignBenefit;
        }

    /**
     * @return Вид пособия.
     * @see ru.tandemservice.movestudent.entity.AssignBenefitStuExtract#getBenefitType()
     */
        public PropertyPath<String> benefitType()
        {
            if(_benefitType == null )
                _benefitType = new PropertyPath<String>(AssignBenefitStuExtractGen.P_BENEFIT_TYPE, this);
            return _benefitType;
        }

    /**
     * @return Дата начала выплаты.
     * @see ru.tandemservice.movestudent.entity.AssignBenefitStuExtract#getBenefitBeginDate()
     */
        public PropertyPath<Date> benefitBeginDate()
        {
            if(_benefitBeginDate == null )
                _benefitBeginDate = new PropertyPath<Date>(AssignBenefitStuExtractGen.P_BENEFIT_BEGIN_DATE, this);
            return _benefitBeginDate;
        }

    /**
     * @return Дата окончания выплаты.
     * @see ru.tandemservice.movestudent.entity.AssignBenefitStuExtract#getBenefitEndDate()
     */
        public PropertyPath<Date> benefitEndDate()
        {
            if(_benefitEndDate == null )
                _benefitEndDate = new PropertyPath<Date>(AssignBenefitStuExtractGen.P_BENEFIT_END_DATE, this);
            return _benefitEndDate;
        }

    /**
     * @return Предоставить свободное посещение занятий. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AssignBenefitStuExtract#isAllowIndependentSchedule()
     */
        public PropertyPath<Boolean> allowIndependentSchedule()
        {
            if(_allowIndependentSchedule == null )
                _allowIndependentSchedule = new PropertyPath<Boolean>(AssignBenefitStuExtractGen.P_ALLOW_INDEPENDENT_SCHEDULE, this);
            return _allowIndependentSchedule;
        }

    /**
     * @return В связи.
     * @see ru.tandemservice.movestudent.entity.AssignBenefitStuExtract#getReasonStr()
     */
        public PropertyPath<String> reasonStr()
        {
            if(_reasonStr == null )
                _reasonStr = new PropertyPath<String>(AssignBenefitStuExtractGen.P_REASON_STR, this);
            return _reasonStr;
        }

    /**
     * @return Дата начала свободного посещения.
     * @see ru.tandemservice.movestudent.entity.AssignBenefitStuExtract#getIndepSchedBeginDate()
     */
        public PropertyPath<Date> indepSchedBeginDate()
        {
            if(_indepSchedBeginDate == null )
                _indepSchedBeginDate = new PropertyPath<Date>(AssignBenefitStuExtractGen.P_INDEP_SCHED_BEGIN_DATE, this);
            return _indepSchedBeginDate;
        }

    /**
     * @return Дата окончания свободного посещения.
     * @see ru.tandemservice.movestudent.entity.AssignBenefitStuExtract#getIndepSchedEndDate()
     */
        public PropertyPath<Date> indepSchedEndDate()
        {
            if(_indepSchedEndDate == null )
                _indepSchedEndDate = new PropertyPath<Date>(AssignBenefitStuExtractGen.P_INDEP_SCHED_END_DATE, this);
            return _indepSchedEndDate;
        }

        public Class getEntityClass()
        {
            return AssignBenefitStuExtract.class;
        }

        public String getEntityName()
        {
            return "assignBenefitStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
