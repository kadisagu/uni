/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.component.student.MoveStudentPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.runtime.BusinessComponentRuntime;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.base.bo.OrderComment.ui.Edit.OrderCommentEdit;
import ru.tandemservice.movestudent.component.commons.ExtractListModel;
import ru.tandemservice.movestudent.component.commons.MoveStudentColumns;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.uni.component.student.StudentPub.IStudentModel;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.IPrintableExtract;
import ru.tandemservice.unimove.dao.MoveDaoFacade;

/**
 * @author vip_delete
 * @since 01.11.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setStudentModel((IStudentModel) component.getModel(component.getName()));

        model.getIndividualOrdersModel().setSettings(UniBaseUtils.getDataSettings(component, "MoveStudent_individualOrders.filter"));
        model.getProjectsModel().setSettings(UniBaseUtils.getDataSettings(component, "MoveStudent_projects.filter"));
        model.getExtractsModel().setSettings(UniBaseUtils.getDataSettings(component, "MoveStudent_extracts.filter"));
        model.getListextractsModel().setSettings(UniBaseUtils.getDataSettings(component, "MoveStudent_listextracts.filter"));
        model.getOtherOrdersModel().setSettings(UniBaseUtils.getDataSettings(component, "MoveStudent_otherOrders.filter"));
        model.getOrdersModel().setSettings(UniBaseUtils.getDataSettings(component, "MoveStudent_orders.filter"));

        getDao().prepare(model);

        prepareIndividualOrdersListDataSource(model, component);
        prepareProjectsListDataSource(model, component);
        prepareExtractsListDataSource(model, component);
        prepareListExtractsListDataSource(model, component);
        prepareOtherOrdersListDataSource(model, component);
        prepareOrdersDataSource(model, component);
    }

    // prepare data source

    private void prepareIndividualOrdersListDataSource(final Model model, IBusinessComponent component)
    {
        if (model.getIndividualOrdersModel().getDataSource() != null) return;

        DynamicListDataSource dataSource = new DynamicListDataSource(component, component1 -> {
            getDao().prepareIndividualOrdersDataSource(model);
        });
        dataSource.addColumn(MoveStudentColumns.getCreateDateColumn().setOrderable(true));
        dataSource.addColumn(MoveStudentColumns.getExtractTypeColumn("Тип приказа").setOrderable(false));
        dataSource.addColumn(MoveStudentColumns.getExtractOrderNumberColumn().setOrderable(true));
        dataSource.addColumn(MoveStudentColumns.getExtractCommitDateColumn().setOrderable(true));
        dataSource.addColumn(MoveStudentColumns.getExtractStateColumn("Состояние приказа").setOrderable(false));
        dataSource.addColumn(MoveStudentColumns.getIndividualOrderPrintColumn(this, model.getIndividualOrdersModel().getSecModel().getPermission("print_individualOrder")));
        if (MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
            dataSource.addColumn(MoveStudentColumns.getIndividualOrderPrintPdfColumn(this, model.getIndividualOrdersModel().getSecModel().getPermission("printPdf_individualOrder")));
        dataSource.addColumn(MoveStudentColumns.getIndividualOrderEditColumn(this, model.getIndividualOrdersModel().getSecModel().getPermission("edit_individualOrder")));
        dataSource.addColumn(MoveStudentColumns.getIndividualOrderDeleteColumn(this, model.getIndividualOrdersModel().getSecModel().getPermission("delete_individualOrder")));

        model.getIndividualOrdersModel().setDataSource(dataSource);
    }

    private void prepareProjectsListDataSource(final Model model, IBusinessComponent component)
    {
        if (model.getProjectsModel().getDataSource() != null) return;

        DynamicListDataSource dataSource = new DynamicListDataSource(component, component1 -> {
            getDao().prepareProjectsDataSource(model);
        });
        dataSource.addColumn(MoveStudentColumns.getCreateDateColumn().setOrderable(true));
        dataSource.addColumn(MoveStudentColumns.getExtractTypeColumn("Тип приказа").setOrderable(false));
        dataSource.addColumn(MoveStudentColumns.getExtractStateColumn("Состояние проекта приказа").setOrderable(false));
        dataSource.addColumn(MoveStudentColumns.getExtractPrintColumn(this, model.getProjectsModel().getSecModel().getPermission("print_extract")));
        if (MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
            dataSource.addColumn(MoveStudentColumns.getExtractPrintPdfColumn(this, model.getProjectsModel().getSecModel().getPermission("printPdf_extract")));
        dataSource.addColumn(MoveStudentColumns.getExtractEditColumn(this, model.getProjectsModel().getSecModel().getPermission("edit_extract")));
        dataSource.addColumn(MoveStudentColumns.getExtractDeleteColumn(this, model.getProjectsModel().getSecModel().getPermission("delete_extract")));

        model.getProjectsModel().setDataSource(dataSource);
    }

    private void prepareExtractsListDataSource(final Model model, IBusinessComponent component)
    {
        if (model.getExtractsModel().getDataSource() != null) return;

        DynamicListDataSource<ModularStudentExtract> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareExtractsDataSource(model);
        });
        dataSource.addColumn(MoveStudentColumns.getCreateDateColumn().setOrderable(false));
        dataSource.addColumn(MoveStudentColumns.getExtractOrderNumberColumn().setOrderable(false));
        dataSource.addColumn(MoveStudentColumns.getModularExtractNumberColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractCommitDateColumn().setOrderable(true));
        dataSource.addColumn(MoveStudentColumns.getExtractCommitDateSystemColumn().setOrderable(true));
        dataSource.addColumn(MoveStudentColumns.getExtractTypeColumn("Тип приказа").setOrderable(false));
        dataSource.addColumn(MoveStudentColumns.getExtractStateColumn("Состояние выписки").setOrderable(false));
        dataSource.addColumn(MoveStudentColumns.getExtractPrintColumn(this, model.getExtractsModel().getSecModel().getPermission("print_extract")).setDisabledProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY));
        if (MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
            dataSource.addColumn(MoveStudentColumns.getExtractPrintPdfColumn(this, model.getExtractsModel().getSecModel().getPermission("printPdf_extract")));
        dataSource.addColumn(MoveStudentColumns.getExtractDeleteColumn(this, model.getExtractsModel().getSecModel().getPermission("delete_extract")));

        dataSource.setOrder(MoveStudentColumns.COMMIT_DATE_SYSTEM_COLUMN, OrderDirection.desc);
        model.getExtractsModel().setDataSource(dataSource);
    }

    private void prepareListExtractsListDataSource(final Model model, IBusinessComponent component)
    {
        if (model.getListextractsModel().getDataSource() != null) return;

        DynamicListDataSource<ListStudentExtract> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListExtractsDataSource(model);
        });
        AbstractColumn linkColumn = new PublisherLinkColumn("Дата формирования", IAbstractExtract.P_CREATE_DATE)
                .setResolver(new DefaultPublisherLinkResolver()
                {
                    @Override
                    public String getComponentName(IEntity entity)
                    {
                        IAbstractExtract extract = (IAbstractExtract) ((ViewWrapper) entity).getEntity();
                        return MoveStudentUtils.getListExtractPubComponent((StudentExtractType) extract.getType());
                    }
                }).setFormatter(DateFormatter.DATE_FORMATTER_WITH_TIME);

        dataSource.addColumn(linkColumn);
        dataSource.addColumn(MoveStudentColumns.getExtractCommitDateColumn().setOrderable(true));
        dataSource.addColumn(MoveStudentColumns.getExtractOrderNumberColumn().setOrderable(false));
        dataSource.addColumn(MoveStudentColumns.getParagraphNumberColumn());
        dataSource.addColumn(MoveStudentColumns.getListExtractNumberColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractCommitDateSystemColumn().setOrderable(true));
        dataSource.addColumn(MoveStudentColumns.getExtractTypeColumn("Тип выписки").setOrderable(false));
        dataSource.addColumn(MoveStudentColumns.getExtractStateColumn("Состояние выписки").setOrderable(false));

        IndicatorColumn printColumn = (IndicatorColumn) MoveStudentColumns.getExtractPrintColumn(this, model.getListextractsModel().getSecModel().getPermission("print_extract"));
        printColumn.setDisabledProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY);
        printColumn.setListener(getClass().getPackage().getName() + ":onClickListExtractPrint");
        dataSource.addColumn(printColumn);

        if (MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
        {
            IndicatorColumn printPdfColumn = (IndicatorColumn) MoveStudentColumns.getExtractPrintPdfColumn(this, model.getListextractsModel().getSecModel().getPermission("printPdf_extract"));
            printPdfColumn.setDisabledProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY);
            printPdfColumn.setListener(getClass().getPackage().getName() + ":onClickListExtractPrintPdf");
            dataSource.addColumn(printPdfColumn);
        }

        dataSource.addColumn(MoveStudentColumns.getExtractDeleteColumn(this, model.getListextractsModel().getSecModel().getPermission("delete_extract")));
        dataSource.setOrder(MoveStudentColumns.COMMIT_DATE_SYSTEM_COLUMN, OrderDirection.desc);
        model.getListextractsModel().setDataSource(dataSource);
    }

    private void prepareOtherOrdersListDataSource(final Model model, IBusinessComponent component)
    {
        if (model.getOtherOrdersModel().getDataSource() != null) return;

        DynamicListDataSource dataSource = new DynamicListDataSource(component, component1 -> {
            getDao().prepareOtherOrdersDataSource(model);
        });
        dataSource.addColumn(MoveStudentColumns.getExtractCreateDateColumn().setOrderable(true));
        dataSource.addColumn(MoveStudentColumns.getExtractTypeColumn("Тип приказа").setOrderable(false));
        dataSource.addColumn(MoveStudentColumns.getExtractOrderNumberColumn().setOrderable(false));
        dataSource.addColumn(MoveStudentColumns.getExtractCommitDateColumn().setOrderable(true));
        dataSource.addColumn(MoveStudentColumns.getExtractStateColumn("Состояние приказа").setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Примечание", OtherStudentExtract.comment().s()).setClickable(false).setOrderable(false));
        IndicatorColumn printColumn = (IndicatorColumn) MoveStudentColumns.getOtherOrderPrintColumn(this, model.getOtherOrdersModel().getSecModel().getPermission("print_otherOrder"));
        printColumn.setDisabledProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY);
        dataSource.addColumn(printColumn);

        dataSource.addColumn(MoveStudentColumns.getOtherOrderEditColumn(this, model.getOtherOrdersModel().getSecModel().getPermission("edit_otherOrder")));
        dataSource.addColumn(MoveStudentColumns.getOtherOrderDeleteColumn(this, model.getOtherOrdersModel().getSecModel().getPermission("delete_otherOrder")));

        model.getOtherOrdersModel().setDataSource(dataSource);
    }

    private void prepareOrdersDataSource(final Model model, IBusinessComponent component)
    {
        if (model.getOrdersModel().getDataSource() != null) return;

        DynamicListDataSource<AbstractStudentExtract> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareOrdersDataSource(model);
        });
        AbstractColumn linkColumn = new PublisherLinkColumn("Тип приказа", new String[]{"extractTitle"}).setResolver(new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                IAbstractExtract extract = (IAbstractExtract) ((ViewWrapper) entity).getEntity();
                if (extract.getType() instanceof StudentExtractType) return extract.getId();
                else return extract.getParagraph().getOrder().getId();
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                IAbstractExtract extract = (IAbstractExtract) ((ViewWrapper) entity).getEntity();
                if (extract instanceof ModularStudentExtract) return null;
                if (extract.getType() instanceof StudentExtractType)
                {
                    if (extract instanceof ListStudentExtract)
                        return MoveStudentUtils.getListExtractPubComponent(((StudentExtractType) extract.getType()));
                }
                else
                {
                    return BusinessComponentRuntime.getInstance().getPublisher(extract.getParagraph().getOrder().getClass());
                }
                return null;
            }
        }).setOrderable(false);

        AbstractColumn extractOrderNumberColumn = new PublisherLinkColumn("№ приказа", new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, IAbstractOrder.P_NUMBER}).setResolver(new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                IAbstractExtract extract = (IAbstractExtract) ((ViewWrapper) entity).getEntity();
                if (extract.getType() instanceof StudentExtractType)
                {
                    boolean individual = MoveStudentDaoFacade.getMoveStudentDao().isModularOrderIndividual((StudentExtractType) extract.getType());
                    if (individual || extract instanceof OtherStudentExtract) return extract.getId();
                }
                return extract.getParagraph().getOrder().getId();
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                IAbstractExtract extract = (IAbstractExtract) ((ViewWrapper) entity).getEntity();
                if (extract.getType() instanceof StudentExtractType)
                {
                    boolean individual = MoveStudentDaoFacade.getMoveStudentDao().isModularOrderIndividual((StudentExtractType) extract.getType());
                    if (individual || extract instanceof OtherStudentExtract) return null;
                }
                return BusinessComponentRuntime.getInstance().getPublisher(extract.getParagraph().getOrder().getClass());
            }
        });
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(MoveStudentColumns.getExtractCommitDateColumn());
        dataSource.addColumn(extractOrderNumberColumn);
        dataSource.addColumn(MoveStudentColumns.getExtractCommitDateSystemColumn());
        dataSource.addColumn(new SimpleColumn("№ параграфа приказа", IDAO.P_PARAGRAPH_NUMBER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("№ пункта приказа", IDAO.P_EXTRACT_NUMBER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Примечание", IDAO.P_COMMENT).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать примечание", ActionColumn.EDIT, getClass().getPackage().getName() + ":onClickEditOrderComment").setPermissionKey("editOrderComment").setDisabledProperty(DAO.EXTRACT_COMMENT_EDIT_DISABLED));
        dataSource.addColumn(MoveStudentColumns.getExtractPrintColumn(this, model.getOrdersModel().getSecModel().getPermission("print_extract"), "Печать выписки").setDisabledProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY));
        dataSource.addColumn(MoveStudentColumns.getOrderPrintColumn(this, model.getOrdersModel().getSecModel().getPermission("print_order"), "Печать приказа").setDisabledProperty(DAO.ORDER_PRINT_DISABLED_VIEW_PROPERTY));
        if (MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
        {
            dataSource.addColumn(MoveStudentColumns.getExtractPrintPdfColumn(this, model.getOrdersModel().getSecModel().getPermission("printPdf_extract"), "Печать выписки PDF").setDisabledProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY));
            dataSource.addColumn(MoveStudentColumns.getOrderPrintPdfColumn(this, model.getOrdersModel().getSecModel().getPermission("printPdf_order"), "Печать приказа PDF").setDisabledProperty(DAO.ORDER_PRINT_DISABLED_VIEW_PROPERTY));
        }
        dataSource.setOrder(MoveStudentColumns.COMMIT_DATE_SYSTEM_COLUMN, OrderDirection.desc);
        model.getOrdersModel().setDataSource(dataSource);
    }

    // event listeners

    public void onClickAddIndividualOrder(IBusinessComponent component)
    {
        component.createChildRegion("individualOrders", new ComponentActivator(MoveStudentUtils.getModularExtractAddEditComponent(), new ParametersMap()
                .add("studentId", getModel(component).getStudentModel().getStudent().getId())
                .add("individual", Boolean.TRUE)
        ));
    }

    public void onClickAddExtract(IBusinessComponent component)
    {
        component.createChildRegion("projects", new ComponentActivator(MoveStudentUtils.getModularExtractAddEditComponent()));
    }

    public void onClickAddOtherOrder(IBusinessComponent component)
    {
        component.createChildRegion("otherOrders", new ComponentActivator(IMoveStudentComponents.STUDENT_OTHER_ORDER_ADD_EDIT, new ParametersMap()
                .add("studentId", getModel(component).getStudentModel().getStudent().getId())
        ));
    }

    public void onClickIndividualOrderPrint(IBusinessComponent component)
    {
        ModularStudentExtract extract = DataAccessServices.dao().get((Long) component.getListenerParameter());
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_PRINT, new ParametersMap()
                .add("orderId", extract.getParagraph().getOrder().getId())
        ));
    }

    public void onClickIndividualOrderPrintPdf(IBusinessComponent component)
    {
        ModularStudentExtract extract = DataAccessServices.dao().get((Long) component.getListenerParameter());
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_PRINT, new ParametersMap()
                .add("orderId", extract.getParagraph().getOrder().getId())
                .add("printPdf", Boolean.TRUE)
        ));
    }

    public void onClickExtractPrint(IBusinessComponent component)
    {
        IAbstractExtract extract = getDao().get((Long) component.getListenerParameter());
        if (extract instanceof IPrintableExtract)
        {
            ((IPrintableExtract) extract).doExtractPrint(false);
        }
    }

    public void onClickExtractPrintPdf(IBusinessComponent component)
    {
        IAbstractExtract extract = getDao().get((Long) component.getListenerParameter());
        if (extract instanceof IPrintableExtract)
        {
            ((IPrintableExtract) extract).doExtractPrint(true);
        }
    }

    public void onClickOrderPrint(IBusinessComponent component)
    {
        IAbstractExtract extract = getDao().get((Long) component.getListenerParameter());
        if (extract instanceof IPrintableExtract)
        {
            ((IPrintableExtract) extract).doOrderPrint(false);
        }
    }

    public void onClickOrderPrintPdf(IBusinessComponent component)
    {
        IAbstractExtract extract = getDao().get((Long) component.getListenerParameter());
        if (extract instanceof IPrintableExtract)
        {
            ((IPrintableExtract) extract).doOrderPrint(true);
        }
    }

    public void onClickOtherOrderPrint(IBusinessComponent component)
    {
        IAbstractExtract extract = getDao().get((Long) component.getListenerParameter());
        if (extract instanceof IPrintableExtract)
        {
            ((IPrintableExtract) extract).doOrderPrint(false);
        }
    }

    public void onClickIndividualOrderEdit(IBusinessComponent component)
    {
        Long extractId = (Long) component.getListenerParameter();
        component.createChildRegion("moveStudent", new ComponentActivator(MoveStudentUtils.getModularExtractAddEditComponentNew(extractId), new ParametersMap().add("extractId", extractId)));
    }

    public void onClickExtractEdit(IBusinessComponent component)
    {
        Long extractId = (Long) component.getListenerParameter();
        component.createChildRegion("moveStudent", new ComponentActivator(MoveStudentUtils.getModularExtractAddEditComponentNew(extractId), new ParametersMap().add("extractId", extractId)));
    }

    public void onClickOtherOrderEdit(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_OTHER_ORDER_ADD_EDIT, new ParametersMap()
                .add("extractId", component.getListenerParameter())
        ));
    }

    public void onClickExtractDelete(IBusinessComponent component)
    {
        ModularStudentExtract extract = getDao().get(ModularStudentExtract.class, (Long) component.getListenerParameter());

        getDao().deleteRow(component);

        //если выписка в сборном приказе, то при ее удалении необходимо переназначить номера параграфов
        if (extract != null && extract.getParagraph() != null)
            MoveStudentDaoFacade.getMoveStudentDao().updateParagraphsNumber(getDao().get(StudentModularOrder.class, extract.getParagraph().getOrder().getId()));
    }

    public void onClickIndividualOrderDelete(IBusinessComponent component)
    {
        ModularStudentExtract extract = getDao().get(ModularStudentExtract.class, (Long) component.getListenerParameter());
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(extract.getParagraph().getOrder());
    }

    public void onClickOtherOrderDelete(IBusinessComponent component)
    {
        OtherStudentExtract extract = getDao().get(OtherStudentExtract.class, (Long) component.getListenerParameter());
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(extract.getParagraph().getOrder());
    }

    public void onClickPrintOrderList(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.ORDER_LIST_PRINT, new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, getModel(component).getStudentModel().getStudent().getId())));
    }

    public void onClickPrintOrderListPdf(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.ORDER_LIST_PRINT, new ParametersMap()
                .add(PublisherActivator.PUBLISHER_ID_KEY, getModel(component).getStudentModel().getStudent().getId())
                .add("printPdf", Boolean.TRUE)
        ));
    }

    public void onClickListExtractPrint(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_EXTRACT_PRINT, new ParametersMap()
                .add("extractId", component.getListenerParameter())
        ));
    }

    public void onClickListExtractPrintPdf(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_EXTRACT_PRINT, new ParametersMap()
                .add("extractId", component.getListenerParameter())
                .add("printPdf", Boolean.TRUE)
        ));
    }

    public void onClickOtherOrdersSearch(IBusinessComponent component)
    {
        ExtractListModel model = getModel(component).getOtherOrdersModel();

        if (!UserContext.getInstance().getErrorCollector().isHasFieldErrors())
        {
            DataSettingsFacade.saveSettings(model.getSettings());
            model.getDataSource().refresh();
        }
    }

    public void onClickOtherOrdersClear(IBusinessComponent component)
    {
        getModel(component).getOtherOrdersModel().getSettings().clear();
        onClickOtherOrdersSearch(component);
    }

    public void onClickOrdersSearch(IBusinessComponent component)
    {
        ExtractListModel model = getModel(component).getOrdersModel();

        if (!UserContext.getInstance().getErrorCollector().isHasFieldErrors())
        {
            DataSettingsFacade.saveSettings(model.getSettings());
            model.getDataSource().refresh();
        }
    }

    public void onClickOrdersClear(IBusinessComponent component)
    {
        getModel(component).getOrdersModel().getSettings().clear();
        onClickOrdersSearch(component);
    }

    public void onClickEditOrderComment(IBusinessComponent component)
    {
        IAbstractExtract extract =  getDao().getNotNull((Long) component.getListenerParameter());
        if (extract != null && extract instanceof AbstractStudentExtract)
        {
            ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(OrderCommentEdit.class.getSimpleName(),
                         new ParametersMap().add("extractId", extract.getId())));
        }

    }
}