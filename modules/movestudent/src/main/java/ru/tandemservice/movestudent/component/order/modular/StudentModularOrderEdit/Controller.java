/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.modular.StudentModularOrderEdit;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author vip_delete
 * @since 20.10.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setDisableCommitDate(!CoreServices.securityService().check(model.getOrder(), component.getUserContext().getPrincipalContext(), "editCommitDate"));
        model.setDisableOrderNumber(!CoreServices.securityService().check(model.getOrder(), component.getUserContext().getPrincipalContext(), "editOrderNumber"));
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().update(model);
        deactivate(component);
    }
}
