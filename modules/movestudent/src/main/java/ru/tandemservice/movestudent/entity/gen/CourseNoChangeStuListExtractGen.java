package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.CourseNoChangeStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «Считать на прежнем курсе»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CourseNoChangeStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.CourseNoChangeStuListExtract";
    public static final String ENTITY_NAME = "courseNoChangeStuListExtract";
    public static final int VERSION_HASH = -501702466;
    private static IEntityMeta ENTITY_META;

    public static final String P_EXTRACT_REASON = "extractReason";
    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_COURSE_OLD = "courseOld";

    private String _extractReason; 
    private Course _course;     // Курс
    private Group _group;     // Группа
    private Course _courseOld;     // Курс

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getExtractReason()
    {
        return _extractReason;
    }

    /**
     * @param extractReason 
     */
    public void setExtractReason(String extractReason)
    {
        dirty(_extractReason, extractReason);
        _extractReason = extractReason;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourseOld()
    {
        return _courseOld;
    }

    /**
     * @param courseOld Курс. Свойство не может быть null.
     */
    public void setCourseOld(Course courseOld)
    {
        dirty(_courseOld, courseOld);
        _courseOld = courseOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof CourseNoChangeStuListExtractGen)
        {
            setExtractReason(((CourseNoChangeStuListExtract)another).getExtractReason());
            setCourse(((CourseNoChangeStuListExtract)another).getCourse());
            setGroup(((CourseNoChangeStuListExtract)another).getGroup());
            setCourseOld(((CourseNoChangeStuListExtract)another).getCourseOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CourseNoChangeStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CourseNoChangeStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new CourseNoChangeStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "extractReason":
                    return obj.getExtractReason();
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "courseOld":
                    return obj.getCourseOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "extractReason":
                    obj.setExtractReason((String) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "courseOld":
                    obj.setCourseOld((Course) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "extractReason":
                        return true;
                case "course":
                        return true;
                case "group":
                        return true;
                case "courseOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "extractReason":
                    return true;
                case "course":
                    return true;
                case "group":
                    return true;
                case "courseOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "extractReason":
                    return String.class;
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "courseOld":
                    return Course.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CourseNoChangeStuListExtract> _dslPath = new Path<CourseNoChangeStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CourseNoChangeStuListExtract");
    }
            

    /**
     * @return 
     * @see ru.tandemservice.movestudent.entity.CourseNoChangeStuListExtract#getExtractReason()
     */
    public static PropertyPath<String> extractReason()
    {
        return _dslPath.extractReason();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseNoChangeStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseNoChangeStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseNoChangeStuListExtract#getCourseOld()
     */
    public static Course.Path<Course> courseOld()
    {
        return _dslPath.courseOld();
    }

    public static class Path<E extends CourseNoChangeStuListExtract> extends ListStudentExtract.Path<E>
    {
        private PropertyPath<String> _extractReason;
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private Course.Path<Course> _courseOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return 
     * @see ru.tandemservice.movestudent.entity.CourseNoChangeStuListExtract#getExtractReason()
     */
        public PropertyPath<String> extractReason()
        {
            if(_extractReason == null )
                _extractReason = new PropertyPath<String>(CourseNoChangeStuListExtractGen.P_EXTRACT_REASON, this);
            return _extractReason;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseNoChangeStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseNoChangeStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseNoChangeStuListExtract#getCourseOld()
     */
        public Course.Path<Course> courseOld()
        {
            if(_courseOld == null )
                _courseOld = new Course.Path<Course>(L_COURSE_OLD, this);
            return _courseOld;
        }

        public Class getEntityClass()
        {
            return CourseNoChangeStuListExtract.class;
        }

        public String getEntityName()
        {
            return "courseNoChangeStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
