package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О восстановлении и повторном допуске к сдаче государственного экзамена (неявка на ГЭ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RestorationAdmitStateExamsStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract";
    public static final String ENTITY_NAME = "restorationAdmitStateExamsStuExtract";
    public static final int VERSION_HASH = -298184636;
    private static IEntityMeta ENTITY_META;

    public static final String P_RESTORATION_DATE = "restorationDate";
    public static final String P_DISMISS_DATE = "dismissDate";
    public static final String P_DISMISS_ORDER = "dismissOrder";
    public static final String P_DISMISS_ORDER_DATE = "dismissOrderDate";
    public static final String P_APPLY_DATE = "applyDate";
    public static final String P_STATE_EXAM_PASS_DEADLINE_DATE = "stateExamPassDeadlineDate";
    public static final String P_DISMISSED_FROM = "dismissedFrom";
    public static final String P_SEASON = "season";
    public static final String P_STATE_EXAM_PLURAL = "stateExamPlural";

    private Date _restorationDate;     // Дата восстановления
    private Date _dismissDate;     // Дата отчисления
    private String _dismissOrder;     // Приказ об отчислении
    private Date _dismissOrderDate;     // Дата приказа об отчислении
    private Date _applyDate;     // Дата подачи заявления
    private Date _stateExamPassDeadlineDate;     // Срок сдачи государственного экзамена
    private String _dismissedFrom;     // Отчислен из (название ОУ)
    private String _season;     // Время года
    private boolean _stateExamPlural;     // Множественное число для ГЭ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата восстановления. Свойство не может быть null.
     */
    @NotNull
    public Date getRestorationDate()
    {
        return _restorationDate;
    }

    /**
     * @param restorationDate Дата восстановления. Свойство не может быть null.
     */
    public void setRestorationDate(Date restorationDate)
    {
        dirty(_restorationDate, restorationDate);
        _restorationDate = restorationDate;
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     */
    @NotNull
    public Date getDismissDate()
    {
        return _dismissDate;
    }

    /**
     * @param dismissDate Дата отчисления. Свойство не может быть null.
     */
    public void setDismissDate(Date dismissDate)
    {
        dirty(_dismissDate, dismissDate);
        _dismissDate = dismissDate;
    }

    /**
     * @return Приказ об отчислении. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDismissOrder()
    {
        return _dismissOrder;
    }

    /**
     * @param dismissOrder Приказ об отчислении. Свойство не может быть null.
     */
    public void setDismissOrder(String dismissOrder)
    {
        dirty(_dismissOrder, dismissOrder);
        _dismissOrder = dismissOrder;
    }

    /**
     * @return Дата приказа об отчислении. Свойство не может быть null.
     */
    @NotNull
    public Date getDismissOrderDate()
    {
        return _dismissOrderDate;
    }

    /**
     * @param dismissOrderDate Дата приказа об отчислении. Свойство не может быть null.
     */
    public void setDismissOrderDate(Date dismissOrderDate)
    {
        dirty(_dismissOrderDate, dismissOrderDate);
        _dismissOrderDate = dismissOrderDate;
    }

    /**
     * @return Дата подачи заявления. Свойство не может быть null.
     */
    @NotNull
    public Date getApplyDate()
    {
        return _applyDate;
    }

    /**
     * @param applyDate Дата подачи заявления. Свойство не может быть null.
     */
    public void setApplyDate(Date applyDate)
    {
        dirty(_applyDate, applyDate);
        _applyDate = applyDate;
    }

    /**
     * @return Срок сдачи государственного экзамена.
     */
    public Date getStateExamPassDeadlineDate()
    {
        return _stateExamPassDeadlineDate;
    }

    /**
     * @param stateExamPassDeadlineDate Срок сдачи государственного экзамена.
     */
    public void setStateExamPassDeadlineDate(Date stateExamPassDeadlineDate)
    {
        dirty(_stateExamPassDeadlineDate, stateExamPassDeadlineDate);
        _stateExamPassDeadlineDate = stateExamPassDeadlineDate;
    }

    /**
     * @return Отчислен из (название ОУ).
     */
    @Length(max=255)
    public String getDismissedFrom()
    {
        return _dismissedFrom;
    }

    /**
     * @param dismissedFrom Отчислен из (название ОУ).
     */
    public void setDismissedFrom(String dismissedFrom)
    {
        dirty(_dismissedFrom, dismissedFrom);
        _dismissedFrom = dismissedFrom;
    }

    /**
     * @return Время года.
     */
    @Length(max=255)
    public String getSeason()
    {
        return _season;
    }

    /**
     * @param season Время года.
     */
    public void setSeason(String season)
    {
        dirty(_season, season);
        _season = season;
    }

    /**
     * @return Множественное число для ГЭ. Свойство не может быть null.
     */
    @NotNull
    public boolean isStateExamPlural()
    {
        return _stateExamPlural;
    }

    /**
     * @param stateExamPlural Множественное число для ГЭ. Свойство не может быть null.
     */
    public void setStateExamPlural(boolean stateExamPlural)
    {
        dirty(_stateExamPlural, stateExamPlural);
        _stateExamPlural = stateExamPlural;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RestorationAdmitStateExamsStuExtractGen)
        {
            setRestorationDate(((RestorationAdmitStateExamsStuExtract)another).getRestorationDate());
            setDismissDate(((RestorationAdmitStateExamsStuExtract)another).getDismissDate());
            setDismissOrder(((RestorationAdmitStateExamsStuExtract)another).getDismissOrder());
            setDismissOrderDate(((RestorationAdmitStateExamsStuExtract)another).getDismissOrderDate());
            setApplyDate(((RestorationAdmitStateExamsStuExtract)another).getApplyDate());
            setStateExamPassDeadlineDate(((RestorationAdmitStateExamsStuExtract)another).getStateExamPassDeadlineDate());
            setDismissedFrom(((RestorationAdmitStateExamsStuExtract)another).getDismissedFrom());
            setSeason(((RestorationAdmitStateExamsStuExtract)another).getSeason());
            setStateExamPlural(((RestorationAdmitStateExamsStuExtract)another).isStateExamPlural());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RestorationAdmitStateExamsStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RestorationAdmitStateExamsStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new RestorationAdmitStateExamsStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "restorationDate":
                    return obj.getRestorationDate();
                case "dismissDate":
                    return obj.getDismissDate();
                case "dismissOrder":
                    return obj.getDismissOrder();
                case "dismissOrderDate":
                    return obj.getDismissOrderDate();
                case "applyDate":
                    return obj.getApplyDate();
                case "stateExamPassDeadlineDate":
                    return obj.getStateExamPassDeadlineDate();
                case "dismissedFrom":
                    return obj.getDismissedFrom();
                case "season":
                    return obj.getSeason();
                case "stateExamPlural":
                    return obj.isStateExamPlural();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "restorationDate":
                    obj.setRestorationDate((Date) value);
                    return;
                case "dismissDate":
                    obj.setDismissDate((Date) value);
                    return;
                case "dismissOrder":
                    obj.setDismissOrder((String) value);
                    return;
                case "dismissOrderDate":
                    obj.setDismissOrderDate((Date) value);
                    return;
                case "applyDate":
                    obj.setApplyDate((Date) value);
                    return;
                case "stateExamPassDeadlineDate":
                    obj.setStateExamPassDeadlineDate((Date) value);
                    return;
                case "dismissedFrom":
                    obj.setDismissedFrom((String) value);
                    return;
                case "season":
                    obj.setSeason((String) value);
                    return;
                case "stateExamPlural":
                    obj.setStateExamPlural((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "restorationDate":
                        return true;
                case "dismissDate":
                        return true;
                case "dismissOrder":
                        return true;
                case "dismissOrderDate":
                        return true;
                case "applyDate":
                        return true;
                case "stateExamPassDeadlineDate":
                        return true;
                case "dismissedFrom":
                        return true;
                case "season":
                        return true;
                case "stateExamPlural":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "restorationDate":
                    return true;
                case "dismissDate":
                    return true;
                case "dismissOrder":
                    return true;
                case "dismissOrderDate":
                    return true;
                case "applyDate":
                    return true;
                case "stateExamPassDeadlineDate":
                    return true;
                case "dismissedFrom":
                    return true;
                case "season":
                    return true;
                case "stateExamPlural":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "restorationDate":
                    return Date.class;
                case "dismissDate":
                    return Date.class;
                case "dismissOrder":
                    return String.class;
                case "dismissOrderDate":
                    return Date.class;
                case "applyDate":
                    return Date.class;
                case "stateExamPassDeadlineDate":
                    return Date.class;
                case "dismissedFrom":
                    return String.class;
                case "season":
                    return String.class;
                case "stateExamPlural":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RestorationAdmitStateExamsStuExtract> _dslPath = new Path<RestorationAdmitStateExamsStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RestorationAdmitStateExamsStuExtract");
    }
            

    /**
     * @return Дата восстановления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract#getRestorationDate()
     */
    public static PropertyPath<Date> restorationDate()
    {
        return _dslPath.restorationDate();
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract#getDismissDate()
     */
    public static PropertyPath<Date> dismissDate()
    {
        return _dslPath.dismissDate();
    }

    /**
     * @return Приказ об отчислении. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract#getDismissOrder()
     */
    public static PropertyPath<String> dismissOrder()
    {
        return _dslPath.dismissOrder();
    }

    /**
     * @return Дата приказа об отчислении. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract#getDismissOrderDate()
     */
    public static PropertyPath<Date> dismissOrderDate()
    {
        return _dslPath.dismissOrderDate();
    }

    /**
     * @return Дата подачи заявления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract#getApplyDate()
     */
    public static PropertyPath<Date> applyDate()
    {
        return _dslPath.applyDate();
    }

    /**
     * @return Срок сдачи государственного экзамена.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract#getStateExamPassDeadlineDate()
     */
    public static PropertyPath<Date> stateExamPassDeadlineDate()
    {
        return _dslPath.stateExamPassDeadlineDate();
    }

    /**
     * @return Отчислен из (название ОУ).
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract#getDismissedFrom()
     */
    public static PropertyPath<String> dismissedFrom()
    {
        return _dslPath.dismissedFrom();
    }

    /**
     * @return Время года.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract#getSeason()
     */
    public static PropertyPath<String> season()
    {
        return _dslPath.season();
    }

    /**
     * @return Множественное число для ГЭ. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract#isStateExamPlural()
     */
    public static PropertyPath<Boolean> stateExamPlural()
    {
        return _dslPath.stateExamPlural();
    }

    public static class Path<E extends RestorationAdmitStateExamsStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<Date> _restorationDate;
        private PropertyPath<Date> _dismissDate;
        private PropertyPath<String> _dismissOrder;
        private PropertyPath<Date> _dismissOrderDate;
        private PropertyPath<Date> _applyDate;
        private PropertyPath<Date> _stateExamPassDeadlineDate;
        private PropertyPath<String> _dismissedFrom;
        private PropertyPath<String> _season;
        private PropertyPath<Boolean> _stateExamPlural;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата восстановления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract#getRestorationDate()
     */
        public PropertyPath<Date> restorationDate()
        {
            if(_restorationDate == null )
                _restorationDate = new PropertyPath<Date>(RestorationAdmitStateExamsStuExtractGen.P_RESTORATION_DATE, this);
            return _restorationDate;
        }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract#getDismissDate()
     */
        public PropertyPath<Date> dismissDate()
        {
            if(_dismissDate == null )
                _dismissDate = new PropertyPath<Date>(RestorationAdmitStateExamsStuExtractGen.P_DISMISS_DATE, this);
            return _dismissDate;
        }

    /**
     * @return Приказ об отчислении. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract#getDismissOrder()
     */
        public PropertyPath<String> dismissOrder()
        {
            if(_dismissOrder == null )
                _dismissOrder = new PropertyPath<String>(RestorationAdmitStateExamsStuExtractGen.P_DISMISS_ORDER, this);
            return _dismissOrder;
        }

    /**
     * @return Дата приказа об отчислении. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract#getDismissOrderDate()
     */
        public PropertyPath<Date> dismissOrderDate()
        {
            if(_dismissOrderDate == null )
                _dismissOrderDate = new PropertyPath<Date>(RestorationAdmitStateExamsStuExtractGen.P_DISMISS_ORDER_DATE, this);
            return _dismissOrderDate;
        }

    /**
     * @return Дата подачи заявления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract#getApplyDate()
     */
        public PropertyPath<Date> applyDate()
        {
            if(_applyDate == null )
                _applyDate = new PropertyPath<Date>(RestorationAdmitStateExamsStuExtractGen.P_APPLY_DATE, this);
            return _applyDate;
        }

    /**
     * @return Срок сдачи государственного экзамена.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract#getStateExamPassDeadlineDate()
     */
        public PropertyPath<Date> stateExamPassDeadlineDate()
        {
            if(_stateExamPassDeadlineDate == null )
                _stateExamPassDeadlineDate = new PropertyPath<Date>(RestorationAdmitStateExamsStuExtractGen.P_STATE_EXAM_PASS_DEADLINE_DATE, this);
            return _stateExamPassDeadlineDate;
        }

    /**
     * @return Отчислен из (название ОУ).
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract#getDismissedFrom()
     */
        public PropertyPath<String> dismissedFrom()
        {
            if(_dismissedFrom == null )
                _dismissedFrom = new PropertyPath<String>(RestorationAdmitStateExamsStuExtractGen.P_DISMISSED_FROM, this);
            return _dismissedFrom;
        }

    /**
     * @return Время года.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract#getSeason()
     */
        public PropertyPath<String> season()
        {
            if(_season == null )
                _season = new PropertyPath<String>(RestorationAdmitStateExamsStuExtractGen.P_SEASON, this);
            return _season;
        }

    /**
     * @return Множественное число для ГЭ. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract#isStateExamPlural()
     */
        public PropertyPath<Boolean> stateExamPlural()
        {
            if(_stateExamPlural == null )
                _stateExamPlural = new PropertyPath<Boolean>(RestorationAdmitStateExamsStuExtractGen.P_STATE_EXAM_PLURAL, this);
            return _stateExamPlural;
        }

        public Class getEntityClass()
        {
            return RestorationAdmitStateExamsStuExtract.class;
        }

        public String getEntityName()
        {
            return "restorationAdmitStateExamsStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
