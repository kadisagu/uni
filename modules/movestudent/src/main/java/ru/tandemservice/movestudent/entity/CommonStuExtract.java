package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.entity.gen.CommonStuExtractGen;

import java.util.Date;

/**
 * Базовая выписка из сборного (индивидуального) приказа о переводе
 */
public class CommonStuExtract extends CommonStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getDate() != null ? getDate() : super.getBeginDate();
    }

    public Date getTransferDate()
    {
        return getDate();
    }
}