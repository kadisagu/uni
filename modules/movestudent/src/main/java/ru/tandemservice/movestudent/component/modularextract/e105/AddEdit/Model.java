/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.component.modularextract.e105.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.AcceleratedLearningStuExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.base.bo.UniStudent.logic.sessionTransferProtocol.ISessionTransferProtocolProperty;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.IExtEducationLevelModel;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 25.10.2013
 */
public class Model extends CommonModularStudentExtractAddEditModel<AcceleratedLearningStuExtract> implements IExtEducationLevelModel, IGroupModel
{
    private CommonExtractModel _eduModel;
    private boolean _hasUniSession;
    private ISelectModel _protocolModel;
    private ISessionTransferProtocolProperty _protocol;

    public CommonExtractModel getEduModel()
    {
        return _eduModel;
    }

    public void setEduModel(CommonExtractModel eduModel)
    {
        _eduModel = eduModel;
    }

    public boolean isHasUniSession()
    {
        return _hasUniSession;
    }

    public void setHasUniSession(boolean hasUniSession)
    {
        _hasUniSession = hasUniSession;
    }

    public ISelectModel getProtocolModel()
    {
        return _protocolModel;
    }

    public void setProtocolModel(ISelectModel protocolModel)
    {
        _protocolModel = protocolModel;
    }

    public ISessionTransferProtocolProperty getProtocol()
    {
        return _protocol;
    }

    public void setProtocol(ISessionTransferProtocolProperty protocol)
    {
        _protocol = protocol;
    }


    @Override
    public EducationLevels getParentEduLevel()
    {
        return EducationOrgUnitUtil.getParentLevel(getEduModel().getGroup().getEducationOrgUnit().getEducationLevelHighSchool());
    }

    @Override
    public Course getCourse()
    {
        return _eduModel.getCourse();
    }
}
