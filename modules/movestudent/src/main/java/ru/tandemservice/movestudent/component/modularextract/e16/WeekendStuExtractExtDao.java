/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e16;

import java.util.Map;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.WeekendStuExtractExt;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 30.04.2009
 */
public class WeekendStuExtractExtDao extends UniBaseDao implements IExtractComponentDao<WeekendStuExtractExt>
{
    @Override
    public void doCommit(WeekendStuExtractExt extract, Map parameters)
    {
      //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        //save rollback data
        Student student = extract.getEntity();
        extract.setStudentStatusOld(student.getStatus());

        //set new values
        student.setStatus(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACADEM));

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        else
        {
            extract.setPrevOrderDate(orderData.getWeekendOrderDate());
            extract.setPrevOrderNumber(orderData.getWeekendOrderNumber());
            extract.setPrevBeginDate(orderData.getWeekendDateFrom());
            extract.setPrevEndDate(orderData.getWeekendDateTo());
        }
        orderData.setWeekendOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setWeekendOrderNumber(extract.getParagraph().getOrder().getNumber());
        orderData.setWeekendDateFrom(extract.getBeginDate());
        orderData.setWeekendDateTo(extract.getEndDate());
        getSession().saveOrUpdate(orderData);
    }

    @Override
    public void doRollback(WeekendStuExtractExt extract, Map parameters)
    {
        Student student = extract.getEntity();
        student.setStatus(extract.getStudentStatusOld());

        // возвращаем предыдущие номер и дату приказа
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setWeekendOrderDate(extract.getPrevOrderDate());
        orderData.setWeekendOrderNumber(extract.getPrevOrderNumber());
        orderData.setWeekendDateFrom(extract.getPrevBeginDate());
        orderData.setWeekendDateTo(extract.getPrevEndDate());
        getSession().saveOrUpdate(orderData);        
    }
}