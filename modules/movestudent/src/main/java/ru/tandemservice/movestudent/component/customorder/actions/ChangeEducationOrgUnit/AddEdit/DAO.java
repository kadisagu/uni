/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeEducationOrgUnit.AddEdit;

import ru.tandemservice.movestudent.component.customorder.IStudentCustomOrderExtract;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeEducationOrgUnitAction;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.EducationLevelsHighSchoolSelectModel;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class DAO extends BaseCustomActionAddEditDAO<ChangeEducationOrgUnitAction, Model>
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        if (model.isActionActive())
        {
            model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
            model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
            model.setEducationLevelsHighSchoolModel(new EducationLevelsHighSchoolSelectModel(model));
            model.setDevelopFormModel(EducationOrgUnitUtil.getDevelopFormSelectModel(model));
            model.setDevelopConditionModel(EducationOrgUnitUtil.getDevelopConditionSelectModel(model));
            model.setDevelopTechModel(EducationOrgUnitUtil.getDevelopTechSelectModel(model));
            model.setDevelopPeriodModel(EducationOrgUnitUtil.getDevelopPeriodSelectModel(model));

            if (model.isEdit())
            {
                EducationOrgUnit educationOrgUnit = model.getCustomAction().getNewEducationOrgUnit();
                model.setFormativeOrgUnit(educationOrgUnit.getFormativeOrgUnit());
                model.setTerritorialOrgUnit(educationOrgUnit.getTerritorialOrgUnit());
                model.setEducationLevelsHighSchool(educationOrgUnit.getEducationLevelHighSchool());
                model.setDevelopForm(educationOrgUnit.getDevelopForm());
                model.setDevelopCondition(educationOrgUnit.getDevelopCondition());
                model.setDevelopTech(educationOrgUnit.getDevelopTech());
                model.setDevelopPeriod(educationOrgUnit.getDevelopPeriod());
            }
        }
    }

    @Override
    protected Class<ChangeEducationOrgUnitAction> getActionClass()
    {
        return ChangeEducationOrgUnitAction.class;
    }

    @Override
    public void updatePrepare(Model model, ChangeEducationOrgUnitAction action, IStudentCustomOrderExtract extract)
    {
        Student student = (Student) extract.getEntity();
        action.setOldEducationOrgUnit(student.getEducationOrgUnit());
        action.setNewEducationOrgUnit(UniDaoFacade.getEducationLevelDao().getEducationOrgUnit(model));
    }

    @Override
    public void doCommit(ChangeEducationOrgUnitAction action)
    {
        Student student = (Student) action.getCustomExtract().getEntity();

        action.setOldEducationOrgUnit(student.getEducationOrgUnit());
        update(action);

        student.setEducationOrgUnit(action.getNewEducationOrgUnit());
        update(student);
    }

    @Override
    public void doRollback(ChangeEducationOrgUnitAction action)
    {
        Student student = (Student) action.getCustomExtract().getEntity();
        student.setEducationOrgUnit(action.getOldEducationOrgUnit());
        update(student);
    }
}