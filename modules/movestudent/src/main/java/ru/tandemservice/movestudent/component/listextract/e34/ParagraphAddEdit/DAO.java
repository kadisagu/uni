/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e34.ParagraphAddEdit;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEditAlternative.AbstractParagraphAddEditAlternativeDAO;
import ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract;
import ru.tandemservice.movestudent.utils.ExtEducationLevelsHighSchoolSelectModel;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ListExtractComponentGenerator
 * @since 03.10.2012
 */
public class DAO extends AbstractParagraphAddEditAlternativeDAO<FinAidAssignStuListExtract, Model> implements IDAO
{
	@Override
	public void prepare(final Model model)
	{
		super.prepare(model);
		model.setCourseList(DevelopGridDAO.getCourseList());
		model.setCompensationTypeList(getCatalogItemList(CompensationType.class));

		model.setGroupListModel(new GroupSelectModel(model, model.getOrgUnit())
		{
			@Override
			protected boolean isNeedRequest()
			{
				return true;
			}
		}.extModel(model).showChildLevels(true));

		model.setEduLevelHighSchoolListModel(new ExtEducationLevelsHighSchoolSelectModel(null, null, model.getOrgUnit()));

		List<IdentifiableWrapper> monthList = new ArrayList<>();
		for (int i = 1; i <= 12; i++)
			monthList.add(new IdentifiableWrapper((long) i, CommonBaseDateUtil.getMonthNameDeclined(i, GrammaCase.NOMINATIVE)));

		model.setMonthList(monthList);

		if (model.getParagraphId() == null)
		{
			model.setCompensationType(get(CompensationType.class, CompensationType.P_CODE, CompensationTypeCodes.COMPENSATION_TYPE_BUDGET));
		}
		FinAidAssignStuListExtract extract = model.getFirstExtract();
		if (model.isEditForm())
		{
			model.setMonth(model.getMonthList().get(extract.getMonth() - 1));
			model.setYear(extract.getYear());
		}
		model.setEmployeePostModel(new OrderExecutorSelectModel());
		if (extract != null)
		{
			model.setMatchingDate(extract.getMatchingDate());
			model.setProtocolNumber(extract.getProtocolNumber());
			model.setResponsibleForAgreement(extract.getResponsibleForAgreement());
			model.setResponsibleForPayments(extract.getResponsibleForPayments());
		}
	}

	@Override
	protected FinAidAssignStuListExtract createNewInstance(Model model)
	{
		return new FinAidAssignStuListExtract();
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void fillExtract(FinAidAssignStuListExtract extract, Student student, Model model)
	{
		IValueMapHolder finAidSizeHolder = (IValueMapHolder) model.getDataSource().getColumn("finAidSize");
		Map<Long, Object> finAidSizeMap = (null == finAidSizeHolder ? Collections.emptyMap() : finAidSizeHolder.getValueMap());

		Number finAidSize = (Number) finAidSizeMap.get(student.getId());

		extract.setCourse(student.getCourse());
		extract.setGroup(student.getGroup());
		extract.setCompensationType(student.getCompensationType());
		extract.setMonth(model.getMonth().getId().intValue());
		extract.setYear(model.getYear());
		extract.setFinAidSize(finAidSize.doubleValue());
		extract.setMatchingDate(model.getMatchingDate());
		extract.setProtocolNumber(model.getProtocolNumber());
		extract.setResponsibleForAgreement(model.getResponsibleForAgreement());
		extract.setResponsibleForPayments(model.getResponsibleForPayments());
	}

	@Override
	@SuppressWarnings("unchecked")
	public void update(Model model)
	{
		ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

		IValueMapHolder finAidSizeHolder = (IValueMapHolder) model.getDataSource().getColumn("finAidSize");
		Map<Long, Double> finAidSizeMap = (null == finAidSizeHolder ? Collections.emptyMap() : finAidSizeHolder.getValueMap());

		List<ViewWrapper<Student>> viewlist = new ArrayList<ViewWrapper<Student>>((Collection) model.getDataSource().getEntityList());

		for (ViewWrapper<Student> wrapper : viewlist)
		{
			if (finAidSizeMap.get(wrapper.getEntity().getId()) == null)
				errorCollector.add("Поле «Размер материальной помощи» обязательно для заполнения.", "finAidSizeId_" + wrapper.getEntity().getId());
		}

		if (errorCollector.hasErrors())
			return;

		super.update(model);
	}

	@Override
	protected void patchSearchDataSource(DQLSelectBuilder builder, Model model)
	{
		if (model.getGroup() != null)
		{
			builder.where(eq(property(STUDENT_ALIAS, Student.group()), value(model.getGroup())));
		}
		else
		{
			if (model.getCourse() != null)
			{
				builder.where(eq(property(STUDENT_ALIAS, Student.course()), value(model.getCourse())));
			}
			if (model.getEducationLevelsHighSchool() != null)
			{
				builder.where(eq(property(STUDENT_ALIAS, Student.group().educationOrgUnit().educationLevelHighSchool()), value(model.getEducationLevelsHighSchool())));
			}
		}

		if (model.getCompensationType() != null)
		{
			builder.where(eq(property(STUDENT_ALIAS, Student.compensationType()), value(model.getCompensationType())));
		}

		if (model.getStudentCustomStateCIs() != null && !model.getStudentCustomStateCIs().isEmpty())
		{
			DQLSelectBuilder customState = new DQLSelectBuilder().fromEntity(StudentCustomState.class, "cs")
					.column(property("cs", StudentCustomState.student()));
			customState.where(in(property("cs", StudentCustomState.customState()), model.getStudentCustomStateCIs()));

			List<Student> stud = customState.createStatement(getSession()).list();
			builder.where(in(property(STUDENT_ALIAS, Student.id()), stud));
		}
	}
}