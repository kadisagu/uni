/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.EducationLevelsHighSchoolSelectModel;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;

import java.util.Date;

/**
 * Смысл этой утилиты в том, чтобы в сгенерированные бизнес-компоненты
 * вносить как можно меньше изменений.
 * Выделение базовых классов, вроде криво, так как будет много изменений
 *
 * @author vip_delete
 * @since 18.11.2008
 */
public class CommonExtractUtil implements ICommonExtractUtil
{
    @Override
    public CommonExtractModel createEduModel(boolean isAddForm, CommonStuExtract extract)
    {
        return createEduModel(isAddForm, extract, false);
    }

    @Override
    public CommonExtractModel createEduModel(boolean isAddForm, CommonStuExtract extract, boolean hideInactive)
    {
        final CommonExtractModel model = new CommonExtractModel();

        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setGroupModel(new GroupSelectModel(model, model).hideInactiveProgramSubject(hideInactive));
        model.setEducationLevelsHighSchoolModel(new EducationLevelsHighSchoolSelectModel(model));
        model.setDevelopFormModel(EducationOrgUnitUtil.getDevelopFormSelectModel(model));
        model.setDevelopConditionModel(EducationOrgUnitUtil.getDevelopConditionSelectModel(model));
        model.setDevelopTechModel(EducationOrgUnitUtil.getDevelopTechSelectModel(model));
        model.setDevelopPeriodModel(EducationOrgUnitUtil.getDevelopPeriodSelectModel(model));
        model.setCompensationTypeList(DataAccessServices.dao().getList(CompensationType.class, CompensationType.P_CODE));

        if (isAddForm)
            extract.setDate(new Date());
        else
            initEduModel(model, extract);

        return model;
    }

    @Override
    public void initEduModel(CommonExtractModel model, CommonStuExtract extract)
    {
        model.setFormativeOrgUnit(extract.getEducationOrgUnitNew().getFormativeOrgUnit());
        model.setTerritorialOrgUnit(extract.getEducationOrgUnitNew().getTerritorialOrgUnit());
        model.setCourse(extract.getCourseNew());
        model.setGroup(extract.getGroupNew());
        model.setEducationLevelsHighSchool(extract.getEducationOrgUnitNew().getEducationLevelHighSchool());
        model.setDevelopForm(extract.getEducationOrgUnitNew().getDevelopForm());
        model.setDevelopCondition(extract.getEducationOrgUnitNew().getDevelopCondition());
        model.setDevelopTech(extract.getEducationOrgUnitNew().getDevelopTech());
        model.setDevelopPeriod(extract.getEducationOrgUnitNew().getDevelopPeriod());
        model.setCompensationType(extract.getCompensationTypeNew());
    }

    @Override
    public void update(CommonExtractModel model, CommonStuExtract extract)
    {
        EducationOrgUnit educationOrgUnit = UniDaoFacade.getEducationLevelDao().getEducationOrgUnit(model);

        if (educationOrgUnit == null) {
            throw new ApplicationException("Подходящих направлений подготовки (профилей, специальностей, специализаций) не найдено. Возможно, для них не указаны параметры обучения в настройке «Параметры обучения студентов (НПП)».");
        }

        //save rollback data
        extract.setEducationOrgUnitOld(extract.getEntity().getEducationOrgUnit());
        extract.setCourseOld(extract.getEntity().getCourse());
        extract.setGroupOld(extract.getEntity().getGroup());
        extract.setStudentStatusOld(extract.getEntity().getStatus());
        extract.setCompensationTypeOld(extract.getEntity().getCompensationType());
        extract.setBookNumberOld(extract.getEntity().getBookNumber());
        extract.setTargetAdmissionOld(extract.getEntity().isTargetAdmission());

        //save new data
        extract.setEducationOrgUnitNew(educationOrgUnit);
        extract.setCourseNew(model.getCourse());
        extract.setGroupNew(model.getGroup());
        extract.setCompensationTypeNew(model.getCompensationType());
    }

    @Override
    public void handleGroupChange(CommonExtractModel model)
    {
        if (model.getGroup() != null)
        {
            //ситуация, когда значение будет невалидным, вроде, невозможна
            //при возникновении таковой все равно форма добавления не закроется
            //так как отработает метод getValue в ISelectModel
            EducationOrgUnit educationOrgUnit = model.getGroup().getEducationOrgUnit();
            model.setEducationLevelsHighSchool(educationOrgUnit.getEducationLevelHighSchool());
            model.setDevelopForm(educationOrgUnit.getDevelopForm());
            model.setDevelopCondition(educationOrgUnit.getDevelopCondition());
            model.setDevelopTech(educationOrgUnit.getDevelopTech());
            model.setDevelopPeriod(educationOrgUnit.getDevelopPeriod());
        }
    }

    @Override
    public void doCommitWithoutChangeStatus(CommonStuExtract extract)
    {
        Student student = extract.getEntity();

        //еще раз сохраняем старые значения студента
        extract.setStudentStatusOld(student.getStatus());
        extract.setCourseOld(student.getCourse());
        extract.setGroupOld(student.getGroup());
        extract.setEducationOrgUnitOld(student.getEducationOrgUnit());
        extract.setCompensationTypeOld(student.getCompensationType());
        extract.setTargetAdmissionOld(student.isTargetAdmission());
        extract.setBookNumberOld(student.getBookNumber());

        //присваиваем студенту новые значения
        student.setCourse(extract.getCourseNew());
        student.setGroup(extract.getGroupNew());
        student.setEducationOrgUnit(extract.getEducationOrgUnitNew());
        student.setCompensationType(extract.getCompensationTypeNew());
        if (extract.getBookNumberNew() != null) {
            student.setBookNumber(extract.getBookNumberNew());
        }
        if (extract.getTargetAdmissionNew() != null) {
            student.setTargetAdmission(extract.getTargetAdmissionNew());
        }
    }

    @Override
    public void doCommit(CommonStuExtract extract, IUniBaseDao coreDao)
    {
        doCommitWithoutChangeStatus(extract);
        extract.getEntity().setStatus(coreDao.getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE));
    }

    @Override
    public void doRollbackWithoutChangeStatus(CommonStuExtract extract)
    {
        Student student = extract.getEntity();

        student.setCourse(extract.getCourseOld());
        student.setGroup(extract.getGroupOld());
        student.setEducationOrgUnit(extract.getEducationOrgUnitOld());
        student.setCompensationType(extract.getCompensationTypeOld());
        if (extract.getBookNumberNew() != null && extract.getBookNumberOld() != null) {
            student.setBookNumber(extract.getBookNumberOld());
        }
        if (extract.getTargetAdmissionNew() != null && extract.getTargetAdmissionOld() != null) {
            student.setTargetAdmission(extract.getTargetAdmissionOld());
        }
    }

    @Override
    public void doRollback(CommonStuExtract extract)
    {
        doRollbackWithoutChangeStatus(extract);
        extract.getEntity().setStatus(extract.getStudentStatusOld());
    }

    @Override
    public Qualifications getQualification(EducationLevels eduLevel)
    {
        Qualifications qualification = eduLevel.getQualification();
        if (null != qualification) return qualification;

        Qualifications tmpQualification = null;
        EducationLevels tmpEduLevel = eduLevel.getParentLevel();

        while (null != tmpEduLevel && null == tmpQualification)
        {
            tmpQualification = tmpEduLevel.getQualification();
            tmpEduLevel = tmpEduLevel.getParentLevel();
        }

        return tmpQualification;
    }

    public static String getModifiedFio(Person person, GrammaCase rusCase, boolean shortFio)
    {
        if (null == rusCase) return person.getFullFio();

        IdentityCard identityCard = person.getIdentityCard();
        boolean isMaleSex = identityCard.getSex().isMale();

        StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), rusCase, isMaleSex));
        if (shortFio)
        {
            str.append(" ").append(identityCard.getFirstName().substring(0, 1)).append(".");
            if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            {
                str.append(identityCard.getMiddleName().substring(0, 1)).append(".");
            }
        } else
        {
            str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), rusCase, isMaleSex));
            if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
                str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), rusCase, isMaleSex));
        }

        return str.toString();
    }
}