package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.SocGrantStopStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О приостановлении выплаты социальной стипендии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SocGrantStopStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.SocGrantStopStuExtract";
    public static final String ENTITY_NAME = "socGrantStopStuExtract";
    public static final int VERSION_HASH = -1792739962;
    private static IEntityMeta ENTITY_META;

    public static final String P_STOP_PAYING_DATE = "stopPayingDate";
    public static final String P_BEFORE_LIQUIDATION = "beforeLiquidation";

    private Date _stopPayingDate;     // Дата приостановления выплаты социальной стипендии
    private String _beforeLiquidation;     // До ликвидации

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата приостановления выплаты социальной стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getStopPayingDate()
    {
        return _stopPayingDate;
    }

    /**
     * @param stopPayingDate Дата приостановления выплаты социальной стипендии. Свойство не может быть null.
     */
    public void setStopPayingDate(Date stopPayingDate)
    {
        dirty(_stopPayingDate, stopPayingDate);
        _stopPayingDate = stopPayingDate;
    }

    /**
     * @return До ликвидации. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getBeforeLiquidation()
    {
        return _beforeLiquidation;
    }

    /**
     * @param beforeLiquidation До ликвидации. Свойство не может быть null.
     */
    public void setBeforeLiquidation(String beforeLiquidation)
    {
        dirty(_beforeLiquidation, beforeLiquidation);
        _beforeLiquidation = beforeLiquidation;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SocGrantStopStuExtractGen)
        {
            setStopPayingDate(((SocGrantStopStuExtract)another).getStopPayingDate());
            setBeforeLiquidation(((SocGrantStopStuExtract)another).getBeforeLiquidation());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SocGrantStopStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SocGrantStopStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new SocGrantStopStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "stopPayingDate":
                    return obj.getStopPayingDate();
                case "beforeLiquidation":
                    return obj.getBeforeLiquidation();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "stopPayingDate":
                    obj.setStopPayingDate((Date) value);
                    return;
                case "beforeLiquidation":
                    obj.setBeforeLiquidation((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "stopPayingDate":
                        return true;
                case "beforeLiquidation":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "stopPayingDate":
                    return true;
                case "beforeLiquidation":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "stopPayingDate":
                    return Date.class;
                case "beforeLiquidation":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SocGrantStopStuExtract> _dslPath = new Path<SocGrantStopStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SocGrantStopStuExtract");
    }
            

    /**
     * @return Дата приостановления выплаты социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantStopStuExtract#getStopPayingDate()
     */
    public static PropertyPath<Date> stopPayingDate()
    {
        return _dslPath.stopPayingDate();
    }

    /**
     * @return До ликвидации. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantStopStuExtract#getBeforeLiquidation()
     */
    public static PropertyPath<String> beforeLiquidation()
    {
        return _dslPath.beforeLiquidation();
    }

    public static class Path<E extends SocGrantStopStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _stopPayingDate;
        private PropertyPath<String> _beforeLiquidation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата приостановления выплаты социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantStopStuExtract#getStopPayingDate()
     */
        public PropertyPath<Date> stopPayingDate()
        {
            if(_stopPayingDate == null )
                _stopPayingDate = new PropertyPath<Date>(SocGrantStopStuExtractGen.P_STOP_PAYING_DATE, this);
            return _stopPayingDate;
        }

    /**
     * @return До ликвидации. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantStopStuExtract#getBeforeLiquidation()
     */
        public PropertyPath<String> beforeLiquidation()
        {
            if(_beforeLiquidation == null )
                _beforeLiquidation = new PropertyPath<String>(SocGrantStopStuExtractGen.P_BEFORE_LIQUIDATION, this);
            return _beforeLiquidation;
        }

        public Class getEntityClass()
        {
            return SocGrantStopStuExtract.class;
        }

        public String getEntityName()
        {
            return "socGrantStopStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
