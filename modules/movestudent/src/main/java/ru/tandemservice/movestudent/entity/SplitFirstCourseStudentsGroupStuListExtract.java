package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.SplitFirstCourseStudentsGroupStuListExtractGen;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Date;

/**
 * Проект приказа «О распределении студентов 1 курса в группы»
 */
public class SplitFirstCourseStudentsGroupStuListExtract extends SplitFirstCourseStudentsGroupStuListExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getBeginDate();
    }

    @Override
    public Group getGroupOld()
    {
        return null;
    }

    @Override
    public Group getGroupNew()
    {
        return getNewGroup();
    }
}