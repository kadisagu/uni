/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.modularextract.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.movestudent.entity.custom.ModifyModularStuExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 28.08.2013
 */
public interface IDAO extends IModularStudentExtractPubDAO<ModifyModularStuExtract, Model>
{
}