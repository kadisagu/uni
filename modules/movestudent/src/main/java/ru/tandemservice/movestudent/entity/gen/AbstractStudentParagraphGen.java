package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параграф
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AbstractStudentParagraphGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.AbstractStudentParagraph";
    public static final String ENTITY_NAME = "abstractStudentParagraph";
    public static final int VERSION_HASH = -1557741162;
    private static IEntityMeta ENTITY_META;

    public static final String P_NUMBER = "number";
    public static final String L_ORDER = "order";

    private int _number;     // Номер параграфа в приказе
    private AbstractStudentOrder _order;     // Приказ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер параграфа в приказе. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер параграфа в приказе. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Приказ. Свойство не может быть null.
     */
    @NotNull
    public AbstractStudentOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ. Свойство не может быть null.
     */
    public void setOrder(AbstractStudentOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof AbstractStudentParagraphGen)
        {
            setNumber(((AbstractStudentParagraph)another).getNumber());
            setOrder(((AbstractStudentParagraph)another).getOrder());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AbstractStudentParagraphGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AbstractStudentParagraph.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("AbstractStudentParagraph is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "number":
                    return obj.getNumber();
                case "order":
                    return obj.getOrder();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "order":
                    obj.setOrder((AbstractStudentOrder) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "number":
                        return true;
                case "order":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "number":
                    return true;
                case "order":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "number":
                    return Integer.class;
                case "order":
                    return AbstractStudentOrder.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AbstractStudentParagraph> _dslPath = new Path<AbstractStudentParagraph>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AbstractStudentParagraph");
    }
            

    /**
     * @return Номер параграфа в приказе. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentParagraph#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Приказ. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentParagraph#getOrder()
     */
    public static AbstractStudentOrder.Path<AbstractStudentOrder> order()
    {
        return _dslPath.order();
    }

    public static class Path<E extends AbstractStudentParagraph> extends EntityPath<E>
    {
        private PropertyPath<Integer> _number;
        private AbstractStudentOrder.Path<AbstractStudentOrder> _order;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер параграфа в приказе. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentParagraph#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(AbstractStudentParagraphGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Приказ. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentParagraph#getOrder()
     */
        public AbstractStudentOrder.Path<AbstractStudentOrder> order()
        {
            if(_order == null )
                _order = new AbstractStudentOrder.Path<AbstractStudentOrder>(L_ORDER, this);
            return _order;
        }

        public Class getEntityClass()
        {
            return AbstractStudentParagraph.class;
        }

        public String getEntityName()
        {
            return "abstractStudentParagraph";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
