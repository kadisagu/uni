/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e31;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.CourseTransferExtStuListExtract;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 24.09.2012
 */
public class CourseTransferExtStuListExtractDao extends UniBaseDao implements IExtractComponentDao<CourseTransferExtStuListExtract>
{
    @Override
    public void doCommit(CourseTransferExtStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        doCaptainStudent(extract.getEntity(), extract.getGroup(), extract.getGroupNew());

        extract.getEntity().setCourse(extract.getCourseNew());
        extract.getEntity().setGroup(extract.getGroupNew());
    }

    @Override
    public void doRollback(CourseTransferExtStuListExtract extract, Map parameters)
    {
        doCaptainStudent(extract.getEntity(), extract.getGroupNew(), extract.getGroup());

        extract.getEntity().setCourse(extract.getCourseOld());
        extract.getEntity().setGroup(extract.getGroup());
    }

    private void doCaptainStudent(Student student, Group groupOld, Group groupNew)
    {
	    if ((UniDaoFacade.getGroupDao().isCaptainStudent(groupOld, student)) && !UniDaoFacade.getGroupDao().isCaptainStudent(groupNew, student))
	    {
		    UniDaoFacade.getGroupDao().deleteCaptainStudent(groupOld, student);
		    UniDaoFacade.getGroupDao().addCaptainStudent(groupNew, student);
	    }
    }
}