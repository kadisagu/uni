/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e47.AddEdit;

import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDao;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AddGroupManagerStuExtract;
import ru.tandemservice.movestudent.entity.GroupToExtractRelation;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 10.08.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<AddGroupManagerStuExtract, Model>
{
    @Override
    protected AddGroupManagerStuExtract createNewInstance()
    {
        return new AddGroupManagerStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        Group group = model.getExtract().getEntity().getGroup();

        model.setEduYearsList(new EducationYearModel());
        if (model.isAddForm())
        {
            model.setEduYear(get(EducationYear.class, EducationYear.P_CURRENT, Boolean.TRUE));

            List<Group> groupList = (model.getGroupsList().isEmpty()) ? new ArrayList<Group>() : model.getGroupsList();
            if (!groupList.contains(group))
            {
                groupList.add(group);
            }
            model.setGroupsList(groupList);
        }
        else
        {
            model.setEduYear(model.getExtract().getEducationYear());

            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(GroupToExtractRelation.class, "ge")
                    .distinct()
                    .column(property("ge", GroupToExtractRelation.group()))
                    .where(eq(property("ge", GroupToExtractRelation.extract().id()), value(model.getExtract().getId())));

            model.setGroupsList(builder.createStatement(getSession()).<Group>list());
        }

        model.setGroupListModel(new BaseMultiSelectModel()
        {
            public ListResult findValues(String filter)
            {
                EducationOrgUnit educationOrgUnit = model.getExtract().getEntity().getEducationOrgUnit();
                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder
                        .fromEntity(Group.class, "g")
                        .column("g")
                        .where(eq(property(Group.archival().fromAlias("g")), value(Boolean.FALSE)))
                        .where(eq(property(Group.educationOrgUnit().formativeOrgUnit().fromAlias("g")), value(educationOrgUnit.getFormativeOrgUnit())))
                        .where(eq(property(Group.educationOrgUnit().territorialOrgUnit().fromAlias("g")), value(educationOrgUnit.getTerritorialOrgUnit())));

                builder.where(like(DQLFunctions.upper(property(Group.title().fromAlias("g"))), value(CoreStringUtils.escapeLike(filter))));
                MoveStudentDaoFacade.getMoveStudentDao().addCustomConditionToGroupSelectBuilder(builder, "g");
                builder.order(property(Group.title().fromAlias("g")));

                int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                if (count > MoveStudentDao.MAX_ROWS)
                    builder.top(MoveStudentDao.MAX_ROWS);

                return new ListResult<>(builder.createStatement(getSession()).<Group>list(), count);
            }

            @Override
            public List getValues(Set primaryKeys)
            {
                return getSession().createCriteria(Group.class).add(Restrictions.in("id", primaryKeys)).list();
            }
        });
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);
        if (null == model.getExtract().getEntity().getGroup())
            errors.add("Студент не может быть назначен старостой группы, поскольку он не причислен ни к одной из академических групп.");
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setEducationYear(model.getEduYear());

        super.update(model);

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(GroupToExtractRelation.class);
        deleteBuilder.where(eq(property(GroupToExtractRelation.extract().id()), value(model.getExtract().getId())));
        deleteBuilder.createStatement(getSession()).execute();

        for(Group group : model.getGroupsList())
        {
            List<Student> captainList = UniDaoFacade.getGroupDao().getCaptainStudentList(group);
            if (captainList.isEmpty())
                addGroupToExtractRelation(group, model.getExtract(), null);
            else
                for (Student captain : captainList)
                {
                    addGroupToExtractRelation(group, model.getExtract(), captain);
                }
        }
    }

    private void addGroupToExtractRelation(Group group, AbstractStudentExtract extract, Student student)
    {
        GroupToExtractRelation groupToExtractRelation = new GroupToExtractRelation();
        groupToExtractRelation.setGroup(group);
        groupToExtractRelation.setExtract(extract);
        groupToExtractRelation.setPrevGroupManager(student);
        save(groupToExtractRelation);
    }
}