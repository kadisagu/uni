package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.movestudent.entity.gen.ExcludeStateExamStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. Об отчислении (государственный экзамен)
 */
public class ExcludeStateExamStuExtract extends ExcludeStateExamStuExtractGen implements IExcludeExtract
{
    @Override
    public Date getExcludeDate()
    {
        return getBeginDate();
    }
}