/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e28.ParagraphAddEdit;

import com.google.common.collect.Lists;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.movestudent.entity.AddGroupManagerStuListExtract;
import ru.tandemservice.movestudent.entity.GroupToExtractRelation;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 14.09.2012
 */
@SuppressWarnings("unchecked")
public class Controller extends AbstractListParagraphAddEditController<AddGroupManagerStuListExtract, IDAO, Model>
{
    @Override
    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дополнительный статус", Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Пол", Student.person().identityCard().sex().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("status", "Состояние", Model.STATUS_KEY, SimpleFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn(EDU_LVL_HS_COLUMN_TITLE, Student.educationOrgUnit().educationLevelHighSchool().configurableTitle().s()).setClickable(false));
        dataSource.addColumn(new BlockColumn("selectedGroup","Группа").setClickable(false).setOrderable(false));
        dataSource.setOrder("status", OrderDirection.asc);

        Model model = getModel(component);
        if (model.isEditForm())
        {
            IValueMapHolder selectedGroupHolder = (IValueMapHolder) dataSource.getColumn("selectedGroup");
            Map<Long, List<Group>> selectedGroupMap = (null == selectedGroupHolder ? Collections.emptyMap() : selectedGroupHolder.getValueMap());

            for (AddGroupManagerStuListExtract extract: (List<AddGroupManagerStuListExtract>) model.getParagraph().getExtractList())
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GroupToExtractRelation.class, "ge");
                builder.where(DQLExpressions.eq(DQLExpressions.property("ge", GroupToExtractRelation.extract().id()), DQLExpressions.value(extract.getId())));
                List<GroupToExtractRelation> groupToExtractRelations = ISharedBaseDao.instance.get().getList(builder);
                List<Group> selectedGroups = Lists.newArrayList();

                for(GroupToExtractRelation extractRelation : groupToExtractRelations)
                {
                    selectedGroups.add(extractRelation.getGroup());
                }
                selectedGroupMap.put(extract.getEntity().getId(), selectedGroups);
            }
        }
    }

    @Override
    public void onSelectStudent(IBusinessComponent component)
    {
        super.onSelectStudent(component);
        if (null != component.getListenerParameter())
        {
            Student student = DataAccessServices.dao().get(((ViewWrapper) component.getListenerParameter()).getId());
            if (null != student)
            {
                Model model = getModel(component);
                IValueMapHolder selectedGroupHolder = (IValueMapHolder) model.getDataSource().getColumn("selectedGroup");
                Map<Long, List<Group>> selectedGroupMap = (null == selectedGroupHolder ? Collections.emptyMap() : selectedGroupHolder.getValueMap());

                if ((Boolean) model.getDataSource().getColumn("checkbox").getContent(student))
                {
                    if (null != selectedGroupMap.get(student.getId()))
                    {
                        if (selectedGroupMap.get(student.getId()).isEmpty())
                        {
                            if (model.getSelectedGroupListModel().findValues("").getObjects().contains(student.getGroup()))
                            {
                                List<Group> groups = Lists.newArrayList();
                                groups.add(student.getGroup());
                                selectedGroupMap.put(student.getId(), groups);
                            }
                        }
                    }
                    else
                    {
                        if (model.getSelectedGroupListModel().findValues("").getObjects().contains(student.getGroup()))
                        {
                            List<Group> groups = Lists.newArrayList();
                            groups.add(student.getGroup());
                            selectedGroupMap.put(student.getId(), groups);
                        }
                    }
                }
                else
                {
                    selectedGroupMap.put(student.getId(), Collections.<Group>emptyList());
                }
            }
        }
    }
}
