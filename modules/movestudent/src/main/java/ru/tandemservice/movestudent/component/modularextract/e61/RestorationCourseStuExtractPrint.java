/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e61;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.RestorationCourseStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;

import java.util.Collections;
import java.util.List;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 28.08.2012
 */
public class RestorationCourseStuExtractPrint implements IPrintFormCreator<RestorationCourseStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, RestorationCourseStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        String deadlineDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDeadlineDate());

        CommonExtractPrint.injectFefuDevelopConditionAndTech(modifier, extract.getEducationOrgUnitNew(), extract.getGroupNew(), "fefuShortFastExtendedOptionalTextNew");

        modifier.put("dismissed_A", extract.getEntity().getPerson().getIdentityCard().getSex().isMale() ? "отчисленного" : "отчисленную");
        modifier.put("vuzTitle", extract.getDismissedFrom());
        modifier.put("reasonDismiss", extract.getDismissReason());
        CommonExtractPrint.addEntryDateToModifier(modifier, extract.getRestorationDate());
        modifier.put("disOrderNum", extract.getDismissOrder());
        modifier.put("disOrderDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDismissOrderDate()));
        if (extract.isHasDebts())
        {
            modifier.put("debts", ApplicationRuntime.getProperty("liquidateEduPlanDifferenceColon").replaceAll("\\{deadlineDate\\}", deadlineDate));
        }
        else
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("debts"), false, false);

        RtfTableModifier table = new RtfTableModifier();

        if (!extract.isHasDebts())
        {
            table.remove("DEBTS_TABLE");
        }
        else
        {
            int i = 0;
            List<StuExtractToDebtRelation> relsList = MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(extract);
            String[][] tableData = new String[relsList.size()][];
            for(StuExtractToDebtRelation rel : relsList)
            {
                tableData[i++] = new String[] {String.valueOf(i), rel.getDiscipline(), String.valueOf(rel.getHours()), rel.getControlAction()};
            }
            table.put("DEBTS_TABLE", tableData);
        }
        table.modify(document);

        RtfString debtsList = new RtfString();
        if (!extract.isHasDebts())
        {
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("debtsList"), false, false);
        }
        else
        {
            int i = 0;
            List<StuExtractToDebtRelation> relsList = MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(extract);
            for(StuExtractToDebtRelation rel : relsList)
            {
                debtsList.append(String.valueOf(++i)).append(". «").append(rel.getDiscipline()).append("» - ").append(String.valueOf(rel.getHours())).append(" ч. - ").append(rel.getControlAction());
                if(relsList.indexOf(rel) < relsList.size() - 1) debtsList.par();
                else debtsList.append(".");
            }
            modifier.put("debtsList", debtsList);
        }

        CommonExtractPrint.initFefuGroup(modifier, "intoGroupNew", extract.getGroupNew(), extract.getEducationOrgUnitNew().getDevelopForm(), " в группу ");

        additionalModify(modifier, extract);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    public void additionalModify(RtfInjectModifier modifier, RestorationCourseStuExtract extract)
    {
    }
}