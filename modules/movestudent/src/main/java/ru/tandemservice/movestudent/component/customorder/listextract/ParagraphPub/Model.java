/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.customorder.listextract.ParagraphPub;

import ru.tandemservice.movestudent.component.customorder.CustomOrderUtils;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubModel;
import ru.tandemservice.movestudent.entity.custom.ModifyListStuExtract;

import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 07.10.2013
 */
public class Model extends AbstractListParagraphPubModel<ModifyListStuExtract>
{
    private Map<String, String> _actionMap;
    private String _currentComponentName;

    public String getModifiedOrderFieldName()
    {
        return CustomOrderUtils.getModifiableOrderFieldName(getExtract().getType().getCode());
    }

    public Map<String, String> getActionMap()
    {
        return _actionMap;
    }

    public void setActionMap(Map<String, String> actionMap)
    {
        _actionMap = actionMap;
    }

    public String getCurrentComponentName()
    {
        return _currentComponentName;
    }

    public void setCurrentComponentName(String currentComponentName)
    {
        _currentComponentName = currentComponentName;
    }
}