/* $Id:$ */
package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IPracticeInExtract;
import ru.tandemservice.movestudent.entity.gen.SendPracticeInnerStuListExtractGen;
import ru.tandemservice.uni.entity.catalog.Course;

import java.util.Date;

/**
 * Проект приказа «О направлении на практику в пределах ОУ»
 */
public class SendPracticeInnerStuListExtract extends SendPracticeInnerStuListExtractGen implements ISendPracticeExtract, IPracticeInExtract
{
    @Override
    public Date getBeginDate()
    {
        return getPracticeBeginDate();
    }

    @Override
    public Date getEndDate()
    {
        return getPracticeEndDate();
    }

    @Override
    public String getPracticePlaceFromExtr()
    {
        return getPracticeOrgUnit().getTitle();
    }

    @Override
    public Date getPracticeBeginDateFromExtr()
    {
        return getPracticeBeginDate();
    }

    @Override
    public Date getPracticeEndDateFromExtr()
    {
        return getPracticeEndDate();
    }

    @Override
    public Course getPracticeCourseFromExtr()
    {
        return getPracticeCourse();
    }

    @Override
    public String getPracticeDurationFromExtr()
    {
        return getPracticeDurationStr();
    }

    @Override
    public String getPracticePlace()
    {
        return getPracticeOrgUnitStr();
    }

    @Override
    public String getPracticeHeaderInnerStr()
    {
        return getPracticeHeaderStr();
    }
}