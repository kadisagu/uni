/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e45.utils;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.Date;

/**
 * Created by dseleznev on 03.06.2016.
 */
public interface AdmitToPassSomething
{
    IAbstractParagraph getParagraph();
    Student getEntity();
    EducationLevelsHighSchool getEducationLevelsHighSchool();
    DevelopForm getDevelopForm();
    Course getCourse();
    Date getBeginDate();
    Date getEndDate();
    boolean isVKR();
}