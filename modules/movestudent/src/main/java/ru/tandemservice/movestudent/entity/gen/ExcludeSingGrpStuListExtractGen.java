package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ExcludeSingGrpStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «Об отчислении для одной группы»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExcludeSingGrpStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ExcludeSingGrpStuListExtract";
    public static final String ENTITY_NAME = "excludeSingGrpStuListExtract";
    public static final int VERSION_HASH = 1989278990;
    private static IEntityMeta ENTITY_META;

    public static final String L_STATUS_OLD = "statusOld";
    public static final String L_STATUS_NEW = "statusNew";
    public static final String P_EXCLUDE_DATE = "excludeDate";
    public static final String P_PREV_GRADUATE_ORDER_DATE = "prevGraduateOrderDate";
    public static final String P_PREV_GRADUATE_ORDER_NUMBER = "prevGraduateOrderNumber";
    public static final String P_PROVIDED_EXAM = "providedExam";
    public static final String P_FINISHED_YEAR = "finishedYear";

    private StudentStatus _statusOld;     // Состояние студента
    private StudentStatus _statusNew;     // Состояние студента
    private Date _excludeDate;     // Дата отчисления
    private Date _prevGraduateOrderDate;     // Дата предыдущего приказа о выпуске
    private String _prevGraduateOrderNumber;     // Номер предыдущего приказа о выпуске
    private boolean _providedExam = true;     // Сдача ГЭ предусмотрена
    private Integer _finishedYear;     // Год окончания на момент проведения приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStatusOld()
    {
        return _statusOld;
    }

    /**
     * @param statusOld Состояние студента. Свойство не может быть null.
     */
    public void setStatusOld(StudentStatus statusOld)
    {
        dirty(_statusOld, statusOld);
        _statusOld = statusOld;
    }

    /**
     * @return Состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStatusNew()
    {
        return _statusNew;
    }

    /**
     * @param statusNew Состояние студента. Свойство не может быть null.
     */
    public void setStatusNew(StudentStatus statusNew)
    {
        dirty(_statusNew, statusNew);
        _statusNew = statusNew;
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     */
    @NotNull
    public Date getExcludeDate()
    {
        return _excludeDate;
    }

    /**
     * @param excludeDate Дата отчисления. Свойство не может быть null.
     */
    public void setExcludeDate(Date excludeDate)
    {
        dirty(_excludeDate, excludeDate);
        _excludeDate = excludeDate;
    }

    /**
     * @return Дата предыдущего приказа о выпуске.
     */
    public Date getPrevGraduateOrderDate()
    {
        initLazyForGet("prevGraduateOrderDate");
        return _prevGraduateOrderDate;
    }

    /**
     * @param prevGraduateOrderDate Дата предыдущего приказа о выпуске.
     */
    public void setPrevGraduateOrderDate(Date prevGraduateOrderDate)
    {
        initLazyForSet("prevGraduateOrderDate");
        dirty(_prevGraduateOrderDate, prevGraduateOrderDate);
        _prevGraduateOrderDate = prevGraduateOrderDate;
    }

    /**
     * @return Номер предыдущего приказа о выпуске.
     */
    @Length(max=255)
    public String getPrevGraduateOrderNumber()
    {
        initLazyForGet("prevGraduateOrderNumber");
        return _prevGraduateOrderNumber;
    }

    /**
     * @param prevGraduateOrderNumber Номер предыдущего приказа о выпуске.
     */
    public void setPrevGraduateOrderNumber(String prevGraduateOrderNumber)
    {
        initLazyForSet("prevGraduateOrderNumber");
        dirty(_prevGraduateOrderNumber, prevGraduateOrderNumber);
        _prevGraduateOrderNumber = prevGraduateOrderNumber;
    }

    /**
     * @return Сдача ГЭ предусмотрена. Свойство не может быть null.
     */
    @NotNull
    public boolean isProvidedExam()
    {
        return _providedExam;
    }

    /**
     * @param providedExam Сдача ГЭ предусмотрена. Свойство не может быть null.
     */
    public void setProvidedExam(boolean providedExam)
    {
        dirty(_providedExam, providedExam);
        _providedExam = providedExam;
    }

    /**
     * @return Год окончания на момент проведения приказа.
     */
    public Integer getFinishedYear()
    {
        return _finishedYear;
    }

    /**
     * @param finishedYear Год окончания на момент проведения приказа.
     */
    public void setFinishedYear(Integer finishedYear)
    {
        dirty(_finishedYear, finishedYear);
        _finishedYear = finishedYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ExcludeSingGrpStuListExtractGen)
        {
            setStatusOld(((ExcludeSingGrpStuListExtract)another).getStatusOld());
            setStatusNew(((ExcludeSingGrpStuListExtract)another).getStatusNew());
            setExcludeDate(((ExcludeSingGrpStuListExtract)another).getExcludeDate());
            setPrevGraduateOrderDate(((ExcludeSingGrpStuListExtract)another).getPrevGraduateOrderDate());
            setPrevGraduateOrderNumber(((ExcludeSingGrpStuListExtract)another).getPrevGraduateOrderNumber());
            setProvidedExam(((ExcludeSingGrpStuListExtract)another).isProvidedExam());
            setFinishedYear(((ExcludeSingGrpStuListExtract)another).getFinishedYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExcludeSingGrpStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExcludeSingGrpStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new ExcludeSingGrpStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "statusOld":
                    return obj.getStatusOld();
                case "statusNew":
                    return obj.getStatusNew();
                case "excludeDate":
                    return obj.getExcludeDate();
                case "prevGraduateOrderDate":
                    return obj.getPrevGraduateOrderDate();
                case "prevGraduateOrderNumber":
                    return obj.getPrevGraduateOrderNumber();
                case "providedExam":
                    return obj.isProvidedExam();
                case "finishedYear":
                    return obj.getFinishedYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "statusOld":
                    obj.setStatusOld((StudentStatus) value);
                    return;
                case "statusNew":
                    obj.setStatusNew((StudentStatus) value);
                    return;
                case "excludeDate":
                    obj.setExcludeDate((Date) value);
                    return;
                case "prevGraduateOrderDate":
                    obj.setPrevGraduateOrderDate((Date) value);
                    return;
                case "prevGraduateOrderNumber":
                    obj.setPrevGraduateOrderNumber((String) value);
                    return;
                case "providedExam":
                    obj.setProvidedExam((Boolean) value);
                    return;
                case "finishedYear":
                    obj.setFinishedYear((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "statusOld":
                        return true;
                case "statusNew":
                        return true;
                case "excludeDate":
                        return true;
                case "prevGraduateOrderDate":
                        return true;
                case "prevGraduateOrderNumber":
                        return true;
                case "providedExam":
                        return true;
                case "finishedYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "statusOld":
                    return true;
                case "statusNew":
                    return true;
                case "excludeDate":
                    return true;
                case "prevGraduateOrderDate":
                    return true;
                case "prevGraduateOrderNumber":
                    return true;
                case "providedExam":
                    return true;
                case "finishedYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "statusOld":
                    return StudentStatus.class;
                case "statusNew":
                    return StudentStatus.class;
                case "excludeDate":
                    return Date.class;
                case "prevGraduateOrderDate":
                    return Date.class;
                case "prevGraduateOrderNumber":
                    return String.class;
                case "providedExam":
                    return Boolean.class;
                case "finishedYear":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExcludeSingGrpStuListExtract> _dslPath = new Path<ExcludeSingGrpStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExcludeSingGrpStuListExtract");
    }
            

    /**
     * @return Состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpStuListExtract#getStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> statusOld()
    {
        return _dslPath.statusOld();
    }

    /**
     * @return Состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpStuListExtract#getStatusNew()
     */
    public static StudentStatus.Path<StudentStatus> statusNew()
    {
        return _dslPath.statusNew();
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpStuListExtract#getExcludeDate()
     */
    public static PropertyPath<Date> excludeDate()
    {
        return _dslPath.excludeDate();
    }

    /**
     * @return Дата предыдущего приказа о выпуске.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpStuListExtract#getPrevGraduateOrderDate()
     */
    public static PropertyPath<Date> prevGraduateOrderDate()
    {
        return _dslPath.prevGraduateOrderDate();
    }

    /**
     * @return Номер предыдущего приказа о выпуске.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpStuListExtract#getPrevGraduateOrderNumber()
     */
    public static PropertyPath<String> prevGraduateOrderNumber()
    {
        return _dslPath.prevGraduateOrderNumber();
    }

    /**
     * @return Сдача ГЭ предусмотрена. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpStuListExtract#isProvidedExam()
     */
    public static PropertyPath<Boolean> providedExam()
    {
        return _dslPath.providedExam();
    }

    /**
     * @return Год окончания на момент проведения приказа.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpStuListExtract#getFinishedYear()
     */
    public static PropertyPath<Integer> finishedYear()
    {
        return _dslPath.finishedYear();
    }

    public static class Path<E extends ExcludeSingGrpStuListExtract> extends ListStudentExtract.Path<E>
    {
        private StudentStatus.Path<StudentStatus> _statusOld;
        private StudentStatus.Path<StudentStatus> _statusNew;
        private PropertyPath<Date> _excludeDate;
        private PropertyPath<Date> _prevGraduateOrderDate;
        private PropertyPath<String> _prevGraduateOrderNumber;
        private PropertyPath<Boolean> _providedExam;
        private PropertyPath<Integer> _finishedYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpStuListExtract#getStatusOld()
     */
        public StudentStatus.Path<StudentStatus> statusOld()
        {
            if(_statusOld == null )
                _statusOld = new StudentStatus.Path<StudentStatus>(L_STATUS_OLD, this);
            return _statusOld;
        }

    /**
     * @return Состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpStuListExtract#getStatusNew()
     */
        public StudentStatus.Path<StudentStatus> statusNew()
        {
            if(_statusNew == null )
                _statusNew = new StudentStatus.Path<StudentStatus>(L_STATUS_NEW, this);
            return _statusNew;
        }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpStuListExtract#getExcludeDate()
     */
        public PropertyPath<Date> excludeDate()
        {
            if(_excludeDate == null )
                _excludeDate = new PropertyPath<Date>(ExcludeSingGrpStuListExtractGen.P_EXCLUDE_DATE, this);
            return _excludeDate;
        }

    /**
     * @return Дата предыдущего приказа о выпуске.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpStuListExtract#getPrevGraduateOrderDate()
     */
        public PropertyPath<Date> prevGraduateOrderDate()
        {
            if(_prevGraduateOrderDate == null )
                _prevGraduateOrderDate = new PropertyPath<Date>(ExcludeSingGrpStuListExtractGen.P_PREV_GRADUATE_ORDER_DATE, this);
            return _prevGraduateOrderDate;
        }

    /**
     * @return Номер предыдущего приказа о выпуске.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpStuListExtract#getPrevGraduateOrderNumber()
     */
        public PropertyPath<String> prevGraduateOrderNumber()
        {
            if(_prevGraduateOrderNumber == null )
                _prevGraduateOrderNumber = new PropertyPath<String>(ExcludeSingGrpStuListExtractGen.P_PREV_GRADUATE_ORDER_NUMBER, this);
            return _prevGraduateOrderNumber;
        }

    /**
     * @return Сдача ГЭ предусмотрена. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpStuListExtract#isProvidedExam()
     */
        public PropertyPath<Boolean> providedExam()
        {
            if(_providedExam == null )
                _providedExam = new PropertyPath<Boolean>(ExcludeSingGrpStuListExtractGen.P_PROVIDED_EXAM, this);
            return _providedExam;
        }

    /**
     * @return Год окончания на момент проведения приказа.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpStuListExtract#getFinishedYear()
     */
        public PropertyPath<Integer> finishedYear()
        {
            if(_finishedYear == null )
                _finishedYear = new PropertyPath<Integer>(ExcludeSingGrpStuListExtractGen.P_FINISHED_YEAR, this);
            return _finishedYear;
        }

        public Class getEntityClass()
        {
            return ExcludeSingGrpStuListExtract.class;
        }

        public String getEntityName()
        {
            return "excludeSingGrpStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
