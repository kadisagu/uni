/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e31.ParagraphAddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDao;
import ru.tandemservice.movestudent.entity.CourseTransferExtStuListExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;

/**
 * @author ListExtractComponentGenerator
 * @since 24.09.2012
 */
public class DAO extends AbstractListParagraphAddEditDAO<CourseTransferExtStuListExtract, Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        super.prepare(model);
        CourseTransferExtStuListExtract extract = model.getFirstExtract();
        if (!model.isParagraphOnlyOneInTheOrder())
        {
            model.setCompensationType(extract.getCompensationType());
            model.setCompensationTypeDisabled(true);
            model.setParentEduLevel(extract.getGroup().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());
        }

        model.setGroupListModel(
                new GroupSelectModel(model, model, model, model.getParagraph().getOrder().getOrgUnit())
                        .showChildLevels(true)
        );

        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setCourseOldListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getGroup() == null) return ListResult.getEmpty();

                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder
                        .fromEntity(Student.class, "s")
                        .column(DQLExpressions.property(Student.course().id().fromAlias("s")))
                        .where(DQLExpressions.eq(DQLExpressions.property(Student.group().fromAlias("s")), DQLExpressions.value(model.getGroup())))
                        .group(DQLExpressions.property(Student.course().id().fromAlias("s")));

                DQLSelectBuilder resultBuilder = new DQLSelectBuilder();
                resultBuilder
                        .fromEntity(Course.class, "c")
                        .where(DQLExpressions.in(DQLExpressions.property(Course.id().fromAlias("c")), builder.buildQuery()));

                return new ListResult<>(resultBuilder.createStatement(getSession()).<Course>list());
            }
        });
        model.setCourseNewListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getCourseOld() == null) return ListResult.getEmpty();

                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder
                        .fromEntity(Course.class, "c")
                        .where(DQLExpressions.gt(DQLExpressions.property(Course.intValue().fromAlias("c")), DQLExpressions.value(model.getCourseOld().getIntValue())));

                return new ListResult<>(builder.createStatement(getSession()).<Course>list());
            }
        });

        model.setGroupNewListModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getCourseNew() == null || model.getGroup() == null) return ListResult.getEmpty();

                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder
                        .fromEntity(Group.class, "gn")
                        .where(DQLExpressions.eq(DQLExpressions.property(Group.course().fromAlias("gn")), DQLExpressions.value(model.getCourseNew())))
                        .where(DQLExpressions.eq(DQLExpressions.property(Group.archival().fromAlias("gn")), DQLExpressions.value(Boolean.FALSE)))
                        .where(DQLExpressions.eq(DQLExpressions.property(Group.educationOrgUnit().fromAlias("gn")), DQLExpressions.value(model.getGroup().getEducationOrgUnit())));

                builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(Group.title().fromAlias("gn"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));

                int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                if (count > MoveStudentDao.MAX_ROWS)
                    builder.top(MoveStudentDao.MAX_ROWS);

                return new ListResult<>(builder.createStatement(getSession()).<Group>list(), count);
            }
        });

        // заполняем поля сохраненными значениями
        if (model.getParagraphId() != null)
        {
            model.setTransferDate(extract.getTransferDate());
            model.setCourse(extract.getCourse());
            model.setGroup(extract.getGroup());
            model.setCompensationType(extract.getCompensationType());
            model.setCourseOld(extract.getCourseOld());
            model.setCourseNew(extract.getCourseNew());
            model.setGroupNew(extract.getGroupNew());
        }
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COURSE, model.getCourseOld()));
        if (model.getCompensationType() != null)
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));
    }

    @Override
    protected CourseTransferExtStuListExtract createNewInstance(Model model)
    {
        return new CourseTransferExtStuListExtract();
    }

    @Override
    protected void fillExtract(CourseTransferExtStuListExtract extract, Student student, Model model)
    {
        extract.setTransferDate(model.getTransferDate());
        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setCompensationType(model.getCompensationType());
        extract.setCourseOld(model.getCourseOld());
        extract.setCourseNew(model.getCourseNew());
        extract.setGroupNew(model.getGroupNew());
    }
}