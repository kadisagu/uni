/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.logic.FormativeOrgUnitHandler;
import ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.logic.IMovestudentMSRSettingsDao;
import ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.logic.MovestudentMSRSettingsDao;

/**
 * @author Ekaterina Zvereva
 * @since 03.07.2015
 */
@Configuration
public class MovestudentMSRSettingsManager extends BusinessObjectManager
{
    public static MovestudentMSRSettingsManager instance() {return instance(MovestudentMSRSettingsManager.class);}

    @Bean
    public IMovestudentMSRSettingsDao dao()
    {
        return new MovestudentMSRSettingsDao();
    }

    @Bean
    public IDefaultSearchDataSourceHandler createOrgUnitDataSource()
    {
        return new FormativeOrgUnitHandler(getName());
    }
}
