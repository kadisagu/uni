/* $Id: Model.java.vm 7906 2009-05-12 11:40:06Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e23.ListExtractPub;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubModel;
import ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract;
import ru.tandemservice.movestudent.entity.DegreeToExtractRelation;
import ru.tandemservice.movestudent.entity.StatusToExtractRelation;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author ListExtractComponentGenerator
 * @since 19.02.2011
 */
public class Model extends AbstractListExtractPubModel<ChangeDiplomaWorkTopicStuListExtract>
{
    public String getTitleDegrees()
    {
        List<ScienceDegree> degreesList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(DegreeToExtractRelation.class, "e")
                        .column(property(DegreeToExtractRelation.degree().fromAlias("e")))
                        .where(eq(property(DegreeToExtractRelation.extract().fromAlias("e")), value(getExtract())))
                        .order(property(DegreeToExtractRelation.degree().type().level().fromAlias("e")), OrderDirection.desc)
                        .order(property(DegreeToExtractRelation.degree().shortTitle().fromAlias("e")))
        );

        StringBuilder degreesBuilder = new StringBuilder();
        for (int i = 0; i < degreesList.size(); i++)
            degreesBuilder.append(degreesBuilder.length() > 0 ? ", " : "").append(degreesList.get(i).getShortTitle());
        return degreesBuilder.toString();
    }

    public String getTitleStatuses()
    {
        List<ScienceStatus> statusesList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(StatusToExtractRelation.class, "e")
                        .column(property(StatusToExtractRelation.status().fromAlias("e")))
                        .where(eq(property(StatusToExtractRelation.extract().fromAlias("e")), value(getExtract())))
                        .order(property(StatusToExtractRelation.status().type().level().fromAlias("e")), OrderDirection.desc)
                        .order(property(StatusToExtractRelation.status().shortTitle().fromAlias("e")))
        );

        StringBuilder statusesBuilder = new StringBuilder();
        for (int i = 0; i < statusesList.size(); i++)
            statusesBuilder.append(statusesBuilder.length() > 0 ? ", " : "").append(statusesList.get(i).getShortTitle());
        return statusesBuilder.toString();
    }
}