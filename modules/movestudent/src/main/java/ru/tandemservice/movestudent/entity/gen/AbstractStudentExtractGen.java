package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimv.IAbstractDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка (приказ по студентам)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AbstractStudentExtractGen extends EntityBase
 implements IAbstractDocument, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.AbstractStudentExtract";
    public static final String ENTITY_NAME = "abstractStudentExtract";
    public static final int VERSION_HASH = -491732787;
    private static IEntityMeta ENTITY_META;

    public static final String P_CREATE_DATE = "createDate";
    public static final String P_NUMBER = "number";
    public static final String P_COMMITTED = "committed";
    public static final String P_STUDENT_TITLE = "studentTitle";
    public static final String L_ENTITY = "entity";
    public static final String L_STATE = "state";
    public static final String L_TYPE = "type";
    public static final String L_PARAGRAPH = "paragraph";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_STUDENT_TITLE_STR = "studentTitleStr";
    public static final String P_STUDENT_STATUS_STR = "studentStatusStr";
    public static final String P_PERSONAL_NUMBER_STR = "personalNumberStr";
    public static final String P_COURSE_STR = "courseStr";
    public static final String P_GROUP_STR = "groupStr";
    public static final String P_COMPENSATION_TYPE_STR = "compensationTypeStr";
    public static final String P_FORMATIVE_ORG_UNIT_STR = "formativeOrgUnitStr";
    public static final String P_TERRITORIAL_ORG_UNIT_STR = "territorialOrgUnitStr";
    public static final String P_EDUCATION_LEVEL_HIGH_SCHOOL_STR = "educationLevelHighSchoolStr";
    public static final String P_DEVELOP_FORM_STR = "developFormStr";
    public static final String P_DEVELOP_CONDITION_STR = "developConditionStr";
    public static final String P_DEVELOP_TECH_STR = "developTechStr";
    public static final String P_DEVELOP_PERIOD_STR = "developPeriodStr";
    public static final String P_PREV_ORDER_DATE = "prevOrderDate";
    public static final String P_PREV_ORDER_NUMBER = "prevOrderNumber";
    public static final String P_COMMENT = "comment";
    public static final String P_COMMENT_EDITED = "commentEdited";
    public static final String L_EXECUTOR = "executor";

    private Date _createDate;     // Дата формирования
    private Integer _number;     // Номер выписки в параграфе
    private boolean _committed;     // Проведена
    private String _studentTitle;     // ФИО студента в корректном падеже
    private Student _entity;     // Студент
    private ExtractStates _state;     // Состояние выписки
    private StudentExtractType _type;     // Тип выписки по студенту
    private AbstractStudentParagraph _paragraph;     // Параграф
    private OrgUnit _orgUnit;     // Подразделение
    private String _studentTitleStr;     // ФИО студента в именительном падеже
    private String _studentStatusStr;     // Состояние
    private String _personalNumberStr;     // Личный номер
    private String _courseStr;     // Курс
    private String _groupStr;     // Группа
    private String _compensationTypeStr;     // Вид возмещения затрат
    private String _formativeOrgUnitStr;     // Формирующее подр.
    private String _territorialOrgUnitStr;     // Территориальное подр.
    private String _educationLevelHighSchoolStr;     // Направление подготовки (специальность)
    private String _developFormStr;     // Форма освоения
    private String _developConditionStr;     // Условие освоения
    private String _developTechStr;     // Технология освоения
    private String _developPeriodStr;     // Срок освоения
    private Date _prevOrderDate;     // Дата предыдущего приказа
    private String _prevOrderNumber;     // Номер предыдущего приказа
    private String _comment;     // Комментарий
    private boolean _commentEdited = false;     // Комментарий изменен пользователем
    private EmployeePost _executor;     // Исполнитель (должность)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Номер выписки в параграфе.
     */
    public Integer getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер выписки в параграфе.
     */
    public void setNumber(Integer number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Проведена. Свойство не может быть null.
     */
    @NotNull
    public boolean isCommitted()
    {
        return _committed;
    }

    /**
     * @param committed Проведена. Свойство не может быть null.
     */
    public void setCommitted(boolean committed)
    {
        dirty(_committed, committed);
        _committed = committed;
    }

    /**
     * @return ФИО студента в корректном падеже. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStudentTitle()
    {
        return _studentTitle;
    }

    /**
     * @param studentTitle ФИО студента в корректном падеже. Свойство не может быть null.
     */
    public void setStudentTitle(String studentTitle)
    {
        dirty(_studentTitle, studentTitle);
        _studentTitle = studentTitle;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getEntity()
    {
        return _entity;
    }

    /**
     * @param entity Студент. Свойство не может быть null.
     */
    public void setEntity(Student entity)
    {
        dirty(_entity, entity);
        _entity = entity;
    }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     */
    @NotNull
    public ExtractStates getState()
    {
        return _state;
    }

    /**
     * @param state Состояние выписки. Свойство не может быть null.
     */
    public void setState(ExtractStates state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Тип выписки по студенту. Свойство не может быть null.
     */
    @NotNull
    public StudentExtractType getType()
    {
        return _type;
    }

    /**
     * @param type Тип выписки по студенту. Свойство не может быть null.
     */
    public void setType(StudentExtractType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Параграф.
     */
    public AbstractStudentParagraph getParagraph()
    {
        return _paragraph;
    }

    /**
     * @param paragraph Параграф.
     */
    public void setParagraph(AbstractStudentParagraph paragraph)
    {
        dirty(_paragraph, paragraph);
        _paragraph = paragraph;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return ФИО студента в именительном падеже. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStudentTitleStr()
    {
        initLazyForGet("studentTitleStr");
        return _studentTitleStr;
    }

    /**
     * @param studentTitleStr ФИО студента в именительном падеже. Свойство не может быть null.
     */
    public void setStudentTitleStr(String studentTitleStr)
    {
        initLazyForSet("studentTitleStr");
        dirty(_studentTitleStr, studentTitleStr);
        _studentTitleStr = studentTitleStr;
    }

    /**
     * @return Состояние. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStudentStatusStr()
    {
        initLazyForGet("studentStatusStr");
        return _studentStatusStr;
    }

    /**
     * @param studentStatusStr Состояние. Свойство не может быть null.
     */
    public void setStudentStatusStr(String studentStatusStr)
    {
        initLazyForSet("studentStatusStr");
        dirty(_studentStatusStr, studentStatusStr);
        _studentStatusStr = studentStatusStr;
    }

    /**
     * @return Личный номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPersonalNumberStr()
    {
        initLazyForGet("personalNumberStr");
        return _personalNumberStr;
    }

    /**
     * @param personalNumberStr Личный номер. Свойство не может быть null.
     */
    public void setPersonalNumberStr(String personalNumberStr)
    {
        initLazyForSet("personalNumberStr");
        dirty(_personalNumberStr, personalNumberStr);
        _personalNumberStr = personalNumberStr;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCourseStr()
    {
        initLazyForGet("courseStr");
        return _courseStr;
    }

    /**
     * @param courseStr Курс. Свойство не может быть null.
     */
    public void setCourseStr(String courseStr)
    {
        initLazyForSet("courseStr");
        dirty(_courseStr, courseStr);
        _courseStr = courseStr;
    }

    /**
     * @return Группа.
     */
    @Length(max=255)
    public String getGroupStr()
    {
        initLazyForGet("groupStr");
        return _groupStr;
    }

    /**
     * @param groupStr Группа.
     */
    public void setGroupStr(String groupStr)
    {
        initLazyForSet("groupStr");
        dirty(_groupStr, groupStr);
        _groupStr = groupStr;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCompensationTypeStr()
    {
        initLazyForGet("compensationTypeStr");
        return _compensationTypeStr;
    }

    /**
     * @param compensationTypeStr Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationTypeStr(String compensationTypeStr)
    {
        initLazyForSet("compensationTypeStr");
        dirty(_compensationTypeStr, compensationTypeStr);
        _compensationTypeStr = compensationTypeStr;
    }

    /**
     * @return Формирующее подр.. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFormativeOrgUnitStr()
    {
        initLazyForGet("formativeOrgUnitStr");
        return _formativeOrgUnitStr;
    }

    /**
     * @param formativeOrgUnitStr Формирующее подр.. Свойство не может быть null.
     */
    public void setFormativeOrgUnitStr(String formativeOrgUnitStr)
    {
        initLazyForSet("formativeOrgUnitStr");
        dirty(_formativeOrgUnitStr, formativeOrgUnitStr);
        _formativeOrgUnitStr = formativeOrgUnitStr;
    }

    /**
     * @return Территориальное подр.. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTerritorialOrgUnitStr()
    {
        initLazyForGet("territorialOrgUnitStr");
        return _territorialOrgUnitStr;
    }

    /**
     * @param territorialOrgUnitStr Территориальное подр.. Свойство не может быть null.
     */
    public void setTerritorialOrgUnitStr(String territorialOrgUnitStr)
    {
        initLazyForSet("territorialOrgUnitStr");
        dirty(_territorialOrgUnitStr, territorialOrgUnitStr);
        _territorialOrgUnitStr = territorialOrgUnitStr;
    }

    /**
     * @return Направление подготовки (специальность). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEducationLevelHighSchoolStr()
    {
        initLazyForGet("educationLevelHighSchoolStr");
        return _educationLevelHighSchoolStr;
    }

    /**
     * @param educationLevelHighSchoolStr Направление подготовки (специальность). Свойство не может быть null.
     */
    public void setEducationLevelHighSchoolStr(String educationLevelHighSchoolStr)
    {
        initLazyForSet("educationLevelHighSchoolStr");
        dirty(_educationLevelHighSchoolStr, educationLevelHighSchoolStr);
        _educationLevelHighSchoolStr = educationLevelHighSchoolStr;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDevelopFormStr()
    {
        initLazyForGet("developFormStr");
        return _developFormStr;
    }

    /**
     * @param developFormStr Форма освоения. Свойство не может быть null.
     */
    public void setDevelopFormStr(String developFormStr)
    {
        initLazyForSet("developFormStr");
        dirty(_developFormStr, developFormStr);
        _developFormStr = developFormStr;
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDevelopConditionStr()
    {
        initLazyForGet("developConditionStr");
        return _developConditionStr;
    }

    /**
     * @param developConditionStr Условие освоения. Свойство не может быть null.
     */
    public void setDevelopConditionStr(String developConditionStr)
    {
        initLazyForSet("developConditionStr");
        dirty(_developConditionStr, developConditionStr);
        _developConditionStr = developConditionStr;
    }

    /**
     * @return Технология освоения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDevelopTechStr()
    {
        initLazyForGet("developTechStr");
        return _developTechStr;
    }

    /**
     * @param developTechStr Технология освоения. Свойство не может быть null.
     */
    public void setDevelopTechStr(String developTechStr)
    {
        initLazyForSet("developTechStr");
        dirty(_developTechStr, developTechStr);
        _developTechStr = developTechStr;
    }

    /**
     * @return Срок освоения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDevelopPeriodStr()
    {
        initLazyForGet("developPeriodStr");
        return _developPeriodStr;
    }

    /**
     * @param developPeriodStr Срок освоения. Свойство не может быть null.
     */
    public void setDevelopPeriodStr(String developPeriodStr)
    {
        initLazyForSet("developPeriodStr");
        dirty(_developPeriodStr, developPeriodStr);
        _developPeriodStr = developPeriodStr;
    }

    /**
     * @return Дата предыдущего приказа.
     */
    public Date getPrevOrderDate()
    {
        initLazyForGet("prevOrderDate");
        return _prevOrderDate;
    }

    /**
     * @param prevOrderDate Дата предыдущего приказа.
     */
    public void setPrevOrderDate(Date prevOrderDate)
    {
        initLazyForSet("prevOrderDate");
        dirty(_prevOrderDate, prevOrderDate);
        _prevOrderDate = prevOrderDate;
    }

    /**
     * @return Номер предыдущего приказа.
     */
    @Length(max=255)
    public String getPrevOrderNumber()
    {
        initLazyForGet("prevOrderNumber");
        return _prevOrderNumber;
    }

    /**
     * @param prevOrderNumber Номер предыдущего приказа.
     */
    public void setPrevOrderNumber(String prevOrderNumber)
    {
        initLazyForSet("prevOrderNumber");
        dirty(_prevOrderNumber, prevOrderNumber);
        _prevOrderNumber = prevOrderNumber;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        initLazyForGet("comment");
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        initLazyForSet("comment");
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Комментарий изменен пользователем. Свойство не может быть null.
     */
    @NotNull
    public boolean isCommentEdited()
    {
        return _commentEdited;
    }

    /**
     * @param commentEdited Комментарий изменен пользователем. Свойство не может быть null.
     */
    public void setCommentEdited(boolean commentEdited)
    {
        dirty(_commentEdited, commentEdited);
        _commentEdited = commentEdited;
    }

    /**
     * @return Исполнитель (должность).
     */
    public EmployeePost getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Исполнитель (должность).
     */
    public void setExecutor(EmployeePost executor)
    {
        dirty(_executor, executor);
        _executor = executor;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof AbstractStudentExtractGen)
        {
            setCreateDate(((AbstractStudentExtract)another).getCreateDate());
            setNumber(((AbstractStudentExtract)another).getNumber());
            setCommitted(((AbstractStudentExtract)another).isCommitted());
            setStudentTitle(((AbstractStudentExtract)another).getStudentTitle());
            setEntity(((AbstractStudentExtract)another).getEntity());
            setState(((AbstractStudentExtract)another).getState());
            setType(((AbstractStudentExtract)another).getType());
            setParagraph(((AbstractStudentExtract)another).getParagraph());
            setOrgUnit(((AbstractStudentExtract)another).getOrgUnit());
            setStudentTitleStr(((AbstractStudentExtract)another).getStudentTitleStr());
            setStudentStatusStr(((AbstractStudentExtract)another).getStudentStatusStr());
            setPersonalNumberStr(((AbstractStudentExtract)another).getPersonalNumberStr());
            setCourseStr(((AbstractStudentExtract)another).getCourseStr());
            setGroupStr(((AbstractStudentExtract)another).getGroupStr());
            setCompensationTypeStr(((AbstractStudentExtract)another).getCompensationTypeStr());
            setFormativeOrgUnitStr(((AbstractStudentExtract)another).getFormativeOrgUnitStr());
            setTerritorialOrgUnitStr(((AbstractStudentExtract)another).getTerritorialOrgUnitStr());
            setEducationLevelHighSchoolStr(((AbstractStudentExtract)another).getEducationLevelHighSchoolStr());
            setDevelopFormStr(((AbstractStudentExtract)another).getDevelopFormStr());
            setDevelopConditionStr(((AbstractStudentExtract)another).getDevelopConditionStr());
            setDevelopTechStr(((AbstractStudentExtract)another).getDevelopTechStr());
            setDevelopPeriodStr(((AbstractStudentExtract)another).getDevelopPeriodStr());
            setPrevOrderDate(((AbstractStudentExtract)another).getPrevOrderDate());
            setPrevOrderNumber(((AbstractStudentExtract)another).getPrevOrderNumber());
            setComment(((AbstractStudentExtract)another).getComment());
            setCommentEdited(((AbstractStudentExtract)another).isCommentEdited());
            setExecutor(((AbstractStudentExtract)another).getExecutor());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AbstractStudentExtractGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AbstractStudentExtract.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("AbstractStudentExtract is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "createDate":
                    return obj.getCreateDate();
                case "number":
                    return obj.getNumber();
                case "committed":
                    return obj.isCommitted();
                case "studentTitle":
                    return obj.getStudentTitle();
                case "entity":
                    return obj.getEntity();
                case "state":
                    return obj.getState();
                case "type":
                    return obj.getType();
                case "paragraph":
                    return obj.getParagraph();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "studentTitleStr":
                    return obj.getStudentTitleStr();
                case "studentStatusStr":
                    return obj.getStudentStatusStr();
                case "personalNumberStr":
                    return obj.getPersonalNumberStr();
                case "courseStr":
                    return obj.getCourseStr();
                case "groupStr":
                    return obj.getGroupStr();
                case "compensationTypeStr":
                    return obj.getCompensationTypeStr();
                case "formativeOrgUnitStr":
                    return obj.getFormativeOrgUnitStr();
                case "territorialOrgUnitStr":
                    return obj.getTerritorialOrgUnitStr();
                case "educationLevelHighSchoolStr":
                    return obj.getEducationLevelHighSchoolStr();
                case "developFormStr":
                    return obj.getDevelopFormStr();
                case "developConditionStr":
                    return obj.getDevelopConditionStr();
                case "developTechStr":
                    return obj.getDevelopTechStr();
                case "developPeriodStr":
                    return obj.getDevelopPeriodStr();
                case "prevOrderDate":
                    return obj.getPrevOrderDate();
                case "prevOrderNumber":
                    return obj.getPrevOrderNumber();
                case "comment":
                    return obj.getComment();
                case "commentEdited":
                    return obj.isCommentEdited();
                case "executor":
                    return obj.getExecutor();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "committed":
                    obj.setCommitted((Boolean) value);
                    return;
                case "studentTitle":
                    obj.setStudentTitle((String) value);
                    return;
                case "entity":
                    obj.setEntity((Student) value);
                    return;
                case "state":
                    obj.setState((ExtractStates) value);
                    return;
                case "type":
                    obj.setType((StudentExtractType) value);
                    return;
                case "paragraph":
                    obj.setParagraph((AbstractStudentParagraph) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "studentTitleStr":
                    obj.setStudentTitleStr((String) value);
                    return;
                case "studentStatusStr":
                    obj.setStudentStatusStr((String) value);
                    return;
                case "personalNumberStr":
                    obj.setPersonalNumberStr((String) value);
                    return;
                case "courseStr":
                    obj.setCourseStr((String) value);
                    return;
                case "groupStr":
                    obj.setGroupStr((String) value);
                    return;
                case "compensationTypeStr":
                    obj.setCompensationTypeStr((String) value);
                    return;
                case "formativeOrgUnitStr":
                    obj.setFormativeOrgUnitStr((String) value);
                    return;
                case "territorialOrgUnitStr":
                    obj.setTerritorialOrgUnitStr((String) value);
                    return;
                case "educationLevelHighSchoolStr":
                    obj.setEducationLevelHighSchoolStr((String) value);
                    return;
                case "developFormStr":
                    obj.setDevelopFormStr((String) value);
                    return;
                case "developConditionStr":
                    obj.setDevelopConditionStr((String) value);
                    return;
                case "developTechStr":
                    obj.setDevelopTechStr((String) value);
                    return;
                case "developPeriodStr":
                    obj.setDevelopPeriodStr((String) value);
                    return;
                case "prevOrderDate":
                    obj.setPrevOrderDate((Date) value);
                    return;
                case "prevOrderNumber":
                    obj.setPrevOrderNumber((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "commentEdited":
                    obj.setCommentEdited((Boolean) value);
                    return;
                case "executor":
                    obj.setExecutor((EmployeePost) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "createDate":
                        return true;
                case "number":
                        return true;
                case "committed":
                        return true;
                case "studentTitle":
                        return true;
                case "entity":
                        return true;
                case "state":
                        return true;
                case "type":
                        return true;
                case "paragraph":
                        return true;
                case "orgUnit":
                        return true;
                case "studentTitleStr":
                        return true;
                case "studentStatusStr":
                        return true;
                case "personalNumberStr":
                        return true;
                case "courseStr":
                        return true;
                case "groupStr":
                        return true;
                case "compensationTypeStr":
                        return true;
                case "formativeOrgUnitStr":
                        return true;
                case "territorialOrgUnitStr":
                        return true;
                case "educationLevelHighSchoolStr":
                        return true;
                case "developFormStr":
                        return true;
                case "developConditionStr":
                        return true;
                case "developTechStr":
                        return true;
                case "developPeriodStr":
                        return true;
                case "prevOrderDate":
                        return true;
                case "prevOrderNumber":
                        return true;
                case "comment":
                        return true;
                case "commentEdited":
                        return true;
                case "executor":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "createDate":
                    return true;
                case "number":
                    return true;
                case "committed":
                    return true;
                case "studentTitle":
                    return true;
                case "entity":
                    return true;
                case "state":
                    return true;
                case "type":
                    return true;
                case "paragraph":
                    return true;
                case "orgUnit":
                    return true;
                case "studentTitleStr":
                    return true;
                case "studentStatusStr":
                    return true;
                case "personalNumberStr":
                    return true;
                case "courseStr":
                    return true;
                case "groupStr":
                    return true;
                case "compensationTypeStr":
                    return true;
                case "formativeOrgUnitStr":
                    return true;
                case "territorialOrgUnitStr":
                    return true;
                case "educationLevelHighSchoolStr":
                    return true;
                case "developFormStr":
                    return true;
                case "developConditionStr":
                    return true;
                case "developTechStr":
                    return true;
                case "developPeriodStr":
                    return true;
                case "prevOrderDate":
                    return true;
                case "prevOrderNumber":
                    return true;
                case "comment":
                    return true;
                case "commentEdited":
                    return true;
                case "executor":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "createDate":
                    return Date.class;
                case "number":
                    return Integer.class;
                case "committed":
                    return Boolean.class;
                case "studentTitle":
                    return String.class;
                case "entity":
                    return Student.class;
                case "state":
                    return ExtractStates.class;
                case "type":
                    return StudentExtractType.class;
                case "paragraph":
                    return AbstractStudentParagraph.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "studentTitleStr":
                    return String.class;
                case "studentStatusStr":
                    return String.class;
                case "personalNumberStr":
                    return String.class;
                case "courseStr":
                    return String.class;
                case "groupStr":
                    return String.class;
                case "compensationTypeStr":
                    return String.class;
                case "formativeOrgUnitStr":
                    return String.class;
                case "territorialOrgUnitStr":
                    return String.class;
                case "educationLevelHighSchoolStr":
                    return String.class;
                case "developFormStr":
                    return String.class;
                case "developConditionStr":
                    return String.class;
                case "developTechStr":
                    return String.class;
                case "developPeriodStr":
                    return String.class;
                case "prevOrderDate":
                    return Date.class;
                case "prevOrderNumber":
                    return String.class;
                case "comment":
                    return String.class;
                case "commentEdited":
                    return Boolean.class;
                case "executor":
                    return EmployeePost.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AbstractStudentExtract> _dslPath = new Path<AbstractStudentExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AbstractStudentExtract");
    }
            

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Номер выписки в параграфе.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Проведена. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#isCommitted()
     */
    public static PropertyPath<Boolean> committed()
    {
        return _dslPath.committed();
    }

    /**
     * @return ФИО студента в корректном падеже. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getStudentTitle()
     */
    public static PropertyPath<String> studentTitle()
    {
        return _dslPath.studentTitle();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getEntity()
     */
    public static Student.Path<Student> entity()
    {
        return _dslPath.entity();
    }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getState()
     */
    public static ExtractStates.Path<ExtractStates> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Тип выписки по студенту. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getType()
     */
    public static StudentExtractType.Path<StudentExtractType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Параграф.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getParagraph()
     */
    public static AbstractStudentParagraph.Path<AbstractStudentParagraph> paragraph()
    {
        return _dslPath.paragraph();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return ФИО студента в именительном падеже. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getStudentTitleStr()
     */
    public static PropertyPath<String> studentTitleStr()
    {
        return _dslPath.studentTitleStr();
    }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getStudentStatusStr()
     */
    public static PropertyPath<String> studentStatusStr()
    {
        return _dslPath.studentStatusStr();
    }

    /**
     * @return Личный номер. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getPersonalNumberStr()
     */
    public static PropertyPath<String> personalNumberStr()
    {
        return _dslPath.personalNumberStr();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getCourseStr()
     */
    public static PropertyPath<String> courseStr()
    {
        return _dslPath.courseStr();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getGroupStr()
     */
    public static PropertyPath<String> groupStr()
    {
        return _dslPath.groupStr();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getCompensationTypeStr()
     */
    public static PropertyPath<String> compensationTypeStr()
    {
        return _dslPath.compensationTypeStr();
    }

    /**
     * @return Формирующее подр.. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getFormativeOrgUnitStr()
     */
    public static PropertyPath<String> formativeOrgUnitStr()
    {
        return _dslPath.formativeOrgUnitStr();
    }

    /**
     * @return Территориальное подр.. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getTerritorialOrgUnitStr()
     */
    public static PropertyPath<String> territorialOrgUnitStr()
    {
        return _dslPath.territorialOrgUnitStr();
    }

    /**
     * @return Направление подготовки (специальность). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getEducationLevelHighSchoolStr()
     */
    public static PropertyPath<String> educationLevelHighSchoolStr()
    {
        return _dslPath.educationLevelHighSchoolStr();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getDevelopFormStr()
     */
    public static PropertyPath<String> developFormStr()
    {
        return _dslPath.developFormStr();
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getDevelopConditionStr()
     */
    public static PropertyPath<String> developConditionStr()
    {
        return _dslPath.developConditionStr();
    }

    /**
     * @return Технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getDevelopTechStr()
     */
    public static PropertyPath<String> developTechStr()
    {
        return _dslPath.developTechStr();
    }

    /**
     * @return Срок освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getDevelopPeriodStr()
     */
    public static PropertyPath<String> developPeriodStr()
    {
        return _dslPath.developPeriodStr();
    }

    /**
     * @return Дата предыдущего приказа.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getPrevOrderDate()
     */
    public static PropertyPath<Date> prevOrderDate()
    {
        return _dslPath.prevOrderDate();
    }

    /**
     * @return Номер предыдущего приказа.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getPrevOrderNumber()
     */
    public static PropertyPath<String> prevOrderNumber()
    {
        return _dslPath.prevOrderNumber();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Комментарий изменен пользователем. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#isCommentEdited()
     */
    public static PropertyPath<Boolean> commentEdited()
    {
        return _dslPath.commentEdited();
    }

    /**
     * @return Исполнитель (должность).
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getExecutor()
     */
    public static EmployeePost.Path<EmployeePost> executor()
    {
        return _dslPath.executor();
    }

    public static class Path<E extends AbstractStudentExtract> extends EntityPath<E>
    {
        private PropertyPath<Date> _createDate;
        private PropertyPath<Integer> _number;
        private PropertyPath<Boolean> _committed;
        private PropertyPath<String> _studentTitle;
        private Student.Path<Student> _entity;
        private ExtractStates.Path<ExtractStates> _state;
        private StudentExtractType.Path<StudentExtractType> _type;
        private AbstractStudentParagraph.Path<AbstractStudentParagraph> _paragraph;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<String> _studentTitleStr;
        private PropertyPath<String> _studentStatusStr;
        private PropertyPath<String> _personalNumberStr;
        private PropertyPath<String> _courseStr;
        private PropertyPath<String> _groupStr;
        private PropertyPath<String> _compensationTypeStr;
        private PropertyPath<String> _formativeOrgUnitStr;
        private PropertyPath<String> _territorialOrgUnitStr;
        private PropertyPath<String> _educationLevelHighSchoolStr;
        private PropertyPath<String> _developFormStr;
        private PropertyPath<String> _developConditionStr;
        private PropertyPath<String> _developTechStr;
        private PropertyPath<String> _developPeriodStr;
        private PropertyPath<Date> _prevOrderDate;
        private PropertyPath<String> _prevOrderNumber;
        private PropertyPath<String> _comment;
        private PropertyPath<Boolean> _commentEdited;
        private EmployeePost.Path<EmployeePost> _executor;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(AbstractStudentExtractGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Номер выписки в параграфе.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(AbstractStudentExtractGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Проведена. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#isCommitted()
     */
        public PropertyPath<Boolean> committed()
        {
            if(_committed == null )
                _committed = new PropertyPath<Boolean>(AbstractStudentExtractGen.P_COMMITTED, this);
            return _committed;
        }

    /**
     * @return ФИО студента в корректном падеже. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getStudentTitle()
     */
        public PropertyPath<String> studentTitle()
        {
            if(_studentTitle == null )
                _studentTitle = new PropertyPath<String>(AbstractStudentExtractGen.P_STUDENT_TITLE, this);
            return _studentTitle;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getEntity()
     */
        public Student.Path<Student> entity()
        {
            if(_entity == null )
                _entity = new Student.Path<Student>(L_ENTITY, this);
            return _entity;
        }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getState()
     */
        public ExtractStates.Path<ExtractStates> state()
        {
            if(_state == null )
                _state = new ExtractStates.Path<ExtractStates>(L_STATE, this);
            return _state;
        }

    /**
     * @return Тип выписки по студенту. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getType()
     */
        public StudentExtractType.Path<StudentExtractType> type()
        {
            if(_type == null )
                _type = new StudentExtractType.Path<StudentExtractType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Параграф.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getParagraph()
     */
        public AbstractStudentParagraph.Path<AbstractStudentParagraph> paragraph()
        {
            if(_paragraph == null )
                _paragraph = new AbstractStudentParagraph.Path<AbstractStudentParagraph>(L_PARAGRAPH, this);
            return _paragraph;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return ФИО студента в именительном падеже. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getStudentTitleStr()
     */
        public PropertyPath<String> studentTitleStr()
        {
            if(_studentTitleStr == null )
                _studentTitleStr = new PropertyPath<String>(AbstractStudentExtractGen.P_STUDENT_TITLE_STR, this);
            return _studentTitleStr;
        }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getStudentStatusStr()
     */
        public PropertyPath<String> studentStatusStr()
        {
            if(_studentStatusStr == null )
                _studentStatusStr = new PropertyPath<String>(AbstractStudentExtractGen.P_STUDENT_STATUS_STR, this);
            return _studentStatusStr;
        }

    /**
     * @return Личный номер. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getPersonalNumberStr()
     */
        public PropertyPath<String> personalNumberStr()
        {
            if(_personalNumberStr == null )
                _personalNumberStr = new PropertyPath<String>(AbstractStudentExtractGen.P_PERSONAL_NUMBER_STR, this);
            return _personalNumberStr;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getCourseStr()
     */
        public PropertyPath<String> courseStr()
        {
            if(_courseStr == null )
                _courseStr = new PropertyPath<String>(AbstractStudentExtractGen.P_COURSE_STR, this);
            return _courseStr;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getGroupStr()
     */
        public PropertyPath<String> groupStr()
        {
            if(_groupStr == null )
                _groupStr = new PropertyPath<String>(AbstractStudentExtractGen.P_GROUP_STR, this);
            return _groupStr;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getCompensationTypeStr()
     */
        public PropertyPath<String> compensationTypeStr()
        {
            if(_compensationTypeStr == null )
                _compensationTypeStr = new PropertyPath<String>(AbstractStudentExtractGen.P_COMPENSATION_TYPE_STR, this);
            return _compensationTypeStr;
        }

    /**
     * @return Формирующее подр.. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getFormativeOrgUnitStr()
     */
        public PropertyPath<String> formativeOrgUnitStr()
        {
            if(_formativeOrgUnitStr == null )
                _formativeOrgUnitStr = new PropertyPath<String>(AbstractStudentExtractGen.P_FORMATIVE_ORG_UNIT_STR, this);
            return _formativeOrgUnitStr;
        }

    /**
     * @return Территориальное подр.. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getTerritorialOrgUnitStr()
     */
        public PropertyPath<String> territorialOrgUnitStr()
        {
            if(_territorialOrgUnitStr == null )
                _territorialOrgUnitStr = new PropertyPath<String>(AbstractStudentExtractGen.P_TERRITORIAL_ORG_UNIT_STR, this);
            return _territorialOrgUnitStr;
        }

    /**
     * @return Направление подготовки (специальность). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getEducationLevelHighSchoolStr()
     */
        public PropertyPath<String> educationLevelHighSchoolStr()
        {
            if(_educationLevelHighSchoolStr == null )
                _educationLevelHighSchoolStr = new PropertyPath<String>(AbstractStudentExtractGen.P_EDUCATION_LEVEL_HIGH_SCHOOL_STR, this);
            return _educationLevelHighSchoolStr;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getDevelopFormStr()
     */
        public PropertyPath<String> developFormStr()
        {
            if(_developFormStr == null )
                _developFormStr = new PropertyPath<String>(AbstractStudentExtractGen.P_DEVELOP_FORM_STR, this);
            return _developFormStr;
        }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getDevelopConditionStr()
     */
        public PropertyPath<String> developConditionStr()
        {
            if(_developConditionStr == null )
                _developConditionStr = new PropertyPath<String>(AbstractStudentExtractGen.P_DEVELOP_CONDITION_STR, this);
            return _developConditionStr;
        }

    /**
     * @return Технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getDevelopTechStr()
     */
        public PropertyPath<String> developTechStr()
        {
            if(_developTechStr == null )
                _developTechStr = new PropertyPath<String>(AbstractStudentExtractGen.P_DEVELOP_TECH_STR, this);
            return _developTechStr;
        }

    /**
     * @return Срок освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getDevelopPeriodStr()
     */
        public PropertyPath<String> developPeriodStr()
        {
            if(_developPeriodStr == null )
                _developPeriodStr = new PropertyPath<String>(AbstractStudentExtractGen.P_DEVELOP_PERIOD_STR, this);
            return _developPeriodStr;
        }

    /**
     * @return Дата предыдущего приказа.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getPrevOrderDate()
     */
        public PropertyPath<Date> prevOrderDate()
        {
            if(_prevOrderDate == null )
                _prevOrderDate = new PropertyPath<Date>(AbstractStudentExtractGen.P_PREV_ORDER_DATE, this);
            return _prevOrderDate;
        }

    /**
     * @return Номер предыдущего приказа.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getPrevOrderNumber()
     */
        public PropertyPath<String> prevOrderNumber()
        {
            if(_prevOrderNumber == null )
                _prevOrderNumber = new PropertyPath<String>(AbstractStudentExtractGen.P_PREV_ORDER_NUMBER, this);
            return _prevOrderNumber;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(AbstractStudentExtractGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Комментарий изменен пользователем. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#isCommentEdited()
     */
        public PropertyPath<Boolean> commentEdited()
        {
            if(_commentEdited == null )
                _commentEdited = new PropertyPath<Boolean>(AbstractStudentExtractGen.P_COMMENT_EDITED, this);
            return _commentEdited;
        }

    /**
     * @return Исполнитель (должность).
     * @see ru.tandemservice.movestudent.entity.AbstractStudentExtract#getExecutor()
     */
        public EmployeePost.Path<EmployeePost> executor()
        {
            if(_executor == null )
                _executor = new EmployeePost.Path<EmployeePost>(L_EXECUTOR, this);
            return _executor;
        }

        public Class getEntityClass()
        {
            return AbstractStudentExtract.class;
        }

        public String getEntityName()
        {
            return "abstractStudentExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
