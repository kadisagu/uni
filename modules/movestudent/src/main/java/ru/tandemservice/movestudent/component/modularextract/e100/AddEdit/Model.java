/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e100.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.SocGrantPaymentStopStuExtract;

import java.util.Date;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 21.02.2013
 */
public class Model extends CommonModularStudentExtractAddEditModel<SocGrantPaymentStopStuExtract>
{
    private ISelectModel _grantOrderModel;
    private AbstractStudentExtract _socGrantExtract;
    private Date _socGrantOrderDate;
    private String _socGrantOrderNumber;

    public ISelectModel getGrantOrderModel()
    {
        return _grantOrderModel;
    }

    public void setGrantOrderModel(ISelectModel grantOrderModel)
    {
        _grantOrderModel = grantOrderModel;
    }

    public Date getSocGrantOrderDate()
    {
        return _socGrantOrderDate;
    }

    public void setSocGrantOrderDate(Date socGrantOrderDate)
    {
        _socGrantOrderDate = socGrantOrderDate;
    }

    public String getSocGrantOrderNumber()
    {
        return _socGrantOrderNumber;
    }

    public void setSocGrantOrderNumber(String socGrantOrderNumber)
    {
        _socGrantOrderNumber = socGrantOrderNumber;
    }

    public AbstractStudentExtract getSocGrantExtract()
    {
        return _socGrantExtract;
    }

    public void setSocGrantExtract(AbstractStudentExtract socGrantExtract)
    {
        _socGrantExtract = socGrantExtract;
    }
}
