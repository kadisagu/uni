package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О назначении надбавки к государственной академической стипендии»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AcadGrantBonusAssignStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract";
    public static final String ENTITY_NAME = "acadGrantBonusAssignStuListExtract";
    public static final int VERSION_HASH = 1780200534;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_GRANT_BONUS_SIZE = "grantBonusSize";
    public static final String L_SESSION_TYPE = "sessionType";
    public static final String P_PREV_BEGIN_DATE = "prevBeginDate";
    public static final String P_PREV_END_DATE = "prevEndDate";

    private Course _course;     // Курс
    private Group _group;     // Группа
    private CompensationType _compensationType;     // Вид возмещения затрат
    private Date _beginDate;     // Дата начала выплаты надбавки
    private Date _endDate;     // Дата окончания выплаты надбавки
    private double _grantBonusSize;     // Размер надбавки
    private YearDistributionPart _sessionType;     // Вид сессии
    private Date _prevBeginDate;     // Дата начала выплаты надбавки к академической стипендии по предыдущему приказу
    private Date _prevEndDate;     // Дата окончания выплаты надбавки к академической стипендии по предыдущему приказу

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Дата начала выплаты надбавки. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала выплаты надбавки. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания выплаты надбавки. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания выплаты надбавки. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Размер надбавки. Свойство не может быть null.
     */
    @NotNull
    public double getGrantBonusSize()
    {
        return _grantBonusSize;
    }

    /**
     * @param grantBonusSize Размер надбавки. Свойство не может быть null.
     */
    public void setGrantBonusSize(double grantBonusSize)
    {
        dirty(_grantBonusSize, grantBonusSize);
        _grantBonusSize = grantBonusSize;
    }

    /**
     * @return Вид сессии.
     */
    public YearDistributionPart getSessionType()
    {
        return _sessionType;
    }

    /**
     * @param sessionType Вид сессии.
     */
    public void setSessionType(YearDistributionPart sessionType)
    {
        dirty(_sessionType, sessionType);
        _sessionType = sessionType;
    }

    /**
     * @return Дата начала выплаты надбавки к академической стипендии по предыдущему приказу.
     */
    public Date getPrevBeginDate()
    {
        return _prevBeginDate;
    }

    /**
     * @param prevBeginDate Дата начала выплаты надбавки к академической стипендии по предыдущему приказу.
     */
    public void setPrevBeginDate(Date prevBeginDate)
    {
        dirty(_prevBeginDate, prevBeginDate);
        _prevBeginDate = prevBeginDate;
    }

    /**
     * @return Дата окончания выплаты надбавки к академической стипендии по предыдущему приказу.
     */
    public Date getPrevEndDate()
    {
        return _prevEndDate;
    }

    /**
     * @param prevEndDate Дата окончания выплаты надбавки к академической стипендии по предыдущему приказу.
     */
    public void setPrevEndDate(Date prevEndDate)
    {
        dirty(_prevEndDate, prevEndDate);
        _prevEndDate = prevEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof AcadGrantBonusAssignStuListExtractGen)
        {
            setCourse(((AcadGrantBonusAssignStuListExtract)another).getCourse());
            setGroup(((AcadGrantBonusAssignStuListExtract)another).getGroup());
            setCompensationType(((AcadGrantBonusAssignStuListExtract)another).getCompensationType());
            setBeginDate(((AcadGrantBonusAssignStuListExtract)another).getBeginDate());
            setEndDate(((AcadGrantBonusAssignStuListExtract)another).getEndDate());
            setGrantBonusSize(((AcadGrantBonusAssignStuListExtract)another).getGrantBonusSize());
            setSessionType(((AcadGrantBonusAssignStuListExtract)another).getSessionType());
            setPrevBeginDate(((AcadGrantBonusAssignStuListExtract)another).getPrevBeginDate());
            setPrevEndDate(((AcadGrantBonusAssignStuListExtract)another).getPrevEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AcadGrantBonusAssignStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AcadGrantBonusAssignStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new AcadGrantBonusAssignStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "compensationType":
                    return obj.getCompensationType();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "grantBonusSize":
                    return obj.getGrantBonusSize();
                case "sessionType":
                    return obj.getSessionType();
                case "prevBeginDate":
                    return obj.getPrevBeginDate();
                case "prevEndDate":
                    return obj.getPrevEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "grantBonusSize":
                    obj.setGrantBonusSize((Double) value);
                    return;
                case "sessionType":
                    obj.setSessionType((YearDistributionPart) value);
                    return;
                case "prevBeginDate":
                    obj.setPrevBeginDate((Date) value);
                    return;
                case "prevEndDate":
                    obj.setPrevEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "group":
                        return true;
                case "compensationType":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "grantBonusSize":
                        return true;
                case "sessionType":
                        return true;
                case "prevBeginDate":
                        return true;
                case "prevEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "group":
                    return true;
                case "compensationType":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "grantBonusSize":
                    return true;
                case "sessionType":
                    return true;
                case "prevBeginDate":
                    return true;
                case "prevEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "compensationType":
                    return CompensationType.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "grantBonusSize":
                    return Double.class;
                case "sessionType":
                    return YearDistributionPart.class;
                case "prevBeginDate":
                    return Date.class;
                case "prevEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AcadGrantBonusAssignStuListExtract> _dslPath = new Path<AcadGrantBonusAssignStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AcadGrantBonusAssignStuListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Дата начала выплаты надбавки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания выплаты надбавки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Размер надбавки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract#getGrantBonusSize()
     */
    public static PropertyPath<Double> grantBonusSize()
    {
        return _dslPath.grantBonusSize();
    }

    /**
     * @return Вид сессии.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract#getSessionType()
     */
    public static YearDistributionPart.Path<YearDistributionPart> sessionType()
    {
        return _dslPath.sessionType();
    }

    /**
     * @return Дата начала выплаты надбавки к академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract#getPrevBeginDate()
     */
    public static PropertyPath<Date> prevBeginDate()
    {
        return _dslPath.prevBeginDate();
    }

    /**
     * @return Дата окончания выплаты надбавки к академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract#getPrevEndDate()
     */
    public static PropertyPath<Date> prevEndDate()
    {
        return _dslPath.prevEndDate();
    }

    public static class Path<E extends AcadGrantBonusAssignStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Double> _grantBonusSize;
        private YearDistributionPart.Path<YearDistributionPart> _sessionType;
        private PropertyPath<Date> _prevBeginDate;
        private PropertyPath<Date> _prevEndDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Дата начала выплаты надбавки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(AcadGrantBonusAssignStuListExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания выплаты надбавки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(AcadGrantBonusAssignStuListExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Размер надбавки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract#getGrantBonusSize()
     */
        public PropertyPath<Double> grantBonusSize()
        {
            if(_grantBonusSize == null )
                _grantBonusSize = new PropertyPath<Double>(AcadGrantBonusAssignStuListExtractGen.P_GRANT_BONUS_SIZE, this);
            return _grantBonusSize;
        }

    /**
     * @return Вид сессии.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract#getSessionType()
     */
        public YearDistributionPart.Path<YearDistributionPart> sessionType()
        {
            if(_sessionType == null )
                _sessionType = new YearDistributionPart.Path<YearDistributionPart>(L_SESSION_TYPE, this);
            return _sessionType;
        }

    /**
     * @return Дата начала выплаты надбавки к академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract#getPrevBeginDate()
     */
        public PropertyPath<Date> prevBeginDate()
        {
            if(_prevBeginDate == null )
                _prevBeginDate = new PropertyPath<Date>(AcadGrantBonusAssignStuListExtractGen.P_PREV_BEGIN_DATE, this);
            return _prevBeginDate;
        }

    /**
     * @return Дата окончания выплаты надбавки к академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract#getPrevEndDate()
     */
        public PropertyPath<Date> prevEndDate()
        {
            if(_prevEndDate == null )
                _prevEndDate = new PropertyPath<Date>(AcadGrantBonusAssignStuListExtractGen.P_PREV_END_DATE, this);
            return _prevEndDate;
        }

        public Class getEntityClass()
        {
            return AcadGrantBonusAssignStuListExtract.class;
        }

        public String getEntityName()
        {
            return "acadGrantBonusAssignStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
