/* $Id$ */
package ru.tandemservice.movestudent.component.catalog.studentOrderReasons.StudentOrderReasonsPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;

/**
 * @author AutoGenerator
 *         Created on 05.12.2012
 */
public class Controller extends DefaultCatalogPubController<StudentOrderReasons, Model, IDAO>
{
    @Override
    protected DynamicListDataSource<StudentOrderReasons> createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<StudentOrderReasons> dataSource = new DynamicListDataSource<>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", StudentOrderReasons.P_TITLE));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", StudentOrderReasons.P_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        return dataSource;
    }
}