/* $Id: Model.java.vm 7906 2009-05-12 11:40:06Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e40.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.CommonListExtractUtil;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubModel;
import ru.tandemservice.movestudent.entity.GroupTransferExtStuListExtract;

/**
 * @author ListExtractComponentGenerator
 * @since 30.01.2013
 */
public class Model extends AbstractListExtractPubModel<GroupTransferExtStuListExtract>
{
    public boolean isTransferDateVisible()
    {
        return CommonListExtractUtil.SOME_LIST_EXTRACT_TRANSFER_DATE_VISIBLE;
    }
}
