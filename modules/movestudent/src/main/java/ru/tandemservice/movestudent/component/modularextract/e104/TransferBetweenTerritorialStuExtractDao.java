/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e104;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.TransferBetweenTerritorialStuExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 25.10.2013
 */
public class TransferBetweenTerritorialStuExtractDao extends UniBaseDao implements IExtractComponentDao<TransferBetweenTerritorialStuExtract>
{
    public void doCommit(TransferBetweenTerritorialStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        extract.setGroupOld(extract.getGroupOld());
        extract.getEntity().setGroup(extract.getGroupNew());

        extract.setCompensationTypeOld(extract.getEntity().getCompensationType());
        extract.getEntity().setCompensationType(extract.getCompensationTypeNew());

        extract.setEducationOrgUnitOld(extract.getEntity().getEducationOrgUnit());
        extract.getEntity().setEducationOrgUnit(extract.getEducationOrgUnitNew());

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);

        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            extract.setPrevOrderDate(orderData.getSplitToSpecializationOrderDate());
            extract.setPrevOrderNumber(orderData.getSplitToSpecializationOrderNumber());
        }

        orderData.setSplitToSpecializationOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setSplitToSpecializationOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);
    }

    public void doRollback(TransferBetweenTerritorialStuExtract extract, Map parameters)
    {
        Student student = extract.getEntity();
        student.setGroup(extract.getGroupOld());
        student.setCompensationType(extract.getCompensationTypeOld());
        student.setEducationOrgUnit(extract.getEducationOrgUnitOld());

        // возвращаем предыдущие номер и дату приказа
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setSplitToSpecializationOrderDate(extract.getPrevOrderDate());
        orderData.setSplitToSpecializationOrderNumber(extract.getPrevOrderNumber());
        getSession().saveOrUpdate(orderData);
    }
}
