package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.RePassDiscStuExtract;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О пересдаче дисциплин учебного плана
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RePassDiscStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.RePassDiscStuExtract";
    public static final String ENTITY_NAME = "rePassDiscStuExtract";
    public static final int VERSION_HASH = -333087893;
    private static IEntityMeta ENTITY_META;

    public static final String L_DEST_TERM = "destTerm";
    public static final String L_DEST_EDU_YEAR = "destEduYear";

    private Term _destTerm;     // Семестр пересдачи
    private EducationYear _destEduYear;     // Учебный год семестра пересдачи

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Семестр пересдачи. Свойство не может быть null.
     */
    @NotNull
    public Term getDestTerm()
    {
        return _destTerm;
    }

    /**
     * @param destTerm Семестр пересдачи. Свойство не может быть null.
     */
    public void setDestTerm(Term destTerm)
    {
        dirty(_destTerm, destTerm);
        _destTerm = destTerm;
    }

    /**
     * @return Учебный год семестра пересдачи. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getDestEduYear()
    {
        return _destEduYear;
    }

    /**
     * @param destEduYear Учебный год семестра пересдачи. Свойство не может быть null.
     */
    public void setDestEduYear(EducationYear destEduYear)
    {
        dirty(_destEduYear, destEduYear);
        _destEduYear = destEduYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RePassDiscStuExtractGen)
        {
            setDestTerm(((RePassDiscStuExtract)another).getDestTerm());
            setDestEduYear(((RePassDiscStuExtract)another).getDestEduYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RePassDiscStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RePassDiscStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new RePassDiscStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "destTerm":
                    return obj.getDestTerm();
                case "destEduYear":
                    return obj.getDestEduYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "destTerm":
                    obj.setDestTerm((Term) value);
                    return;
                case "destEduYear":
                    obj.setDestEduYear((EducationYear) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "destTerm":
                        return true;
                case "destEduYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "destTerm":
                    return true;
                case "destEduYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "destTerm":
                    return Term.class;
                case "destEduYear":
                    return EducationYear.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RePassDiscStuExtract> _dslPath = new Path<RePassDiscStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RePassDiscStuExtract");
    }
            

    /**
     * @return Семестр пересдачи. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RePassDiscStuExtract#getDestTerm()
     */
    public static Term.Path<Term> destTerm()
    {
        return _dslPath.destTerm();
    }

    /**
     * @return Учебный год семестра пересдачи. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RePassDiscStuExtract#getDestEduYear()
     */
    public static EducationYear.Path<EducationYear> destEduYear()
    {
        return _dslPath.destEduYear();
    }

    public static class Path<E extends RePassDiscStuExtract> extends ModularStudentExtract.Path<E>
    {
        private Term.Path<Term> _destTerm;
        private EducationYear.Path<EducationYear> _destEduYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Семестр пересдачи. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RePassDiscStuExtract#getDestTerm()
     */
        public Term.Path<Term> destTerm()
        {
            if(_destTerm == null )
                _destTerm = new Term.Path<Term>(L_DEST_TERM, this);
            return _destTerm;
        }

    /**
     * @return Учебный год семестра пересдачи. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RePassDiscStuExtract#getDestEduYear()
     */
        public EducationYear.Path<EducationYear> destEduYear()
        {
            if(_destEduYear == null )
                _destEduYear = new EducationYear.Path<EducationYear>(L_DEST_EDU_YEAR, this);
            return _destEduYear;
        }

        public Class getEntityClass()
        {
            return RePassDiscStuExtract.class;
        }

        public String getEntityName()
        {
            return "rePassDiscStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
