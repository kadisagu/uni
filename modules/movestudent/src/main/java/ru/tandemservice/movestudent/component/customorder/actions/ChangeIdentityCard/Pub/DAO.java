/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeIdentityCard.Pub;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.BaseCustomActionPubDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class DAO extends BaseCustomActionPubDAO<ChangeIdentityCardAction, Model>
{
    @Override
    protected Class<ChangeIdentityCardAction> getActionClass()
    {
        return ChangeIdentityCardAction.class;
    }
}