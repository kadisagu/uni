package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IEnrollmentExtract;
import ru.tandemservice.movestudent.entity.gen.RestorationAdmitStateExamsStuExtractGen;

import java.util.Date;

/**
 * О восстановлении и повторном допуске к сдаче государственного экзамена (неявка на ГЭ)
 */
public class RestorationAdmitStateExamsStuExtract extends RestorationAdmitStateExamsStuExtractGen implements IEnrollmentExtract
{
    @Override
    public Date getEntranceDate()
    {
        return getRestorationDate();
    }
}