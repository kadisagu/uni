/* $Id$ */
package ru.tandemservice.movestudent.utils;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.EducationLevelsHighSchoolSelectModel;
import ru.tandemservice.uni.util.IExtEducationLevelModel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 30.10.2013
 */
public class ExtEducationLevelsHighSchoolSelectModel extends EducationLevelsHighSchoolSelectModel
{
    private OrgUnit _orgUnit;
    private IExtEducationLevelModel _extModel;
    private boolean _dependFromFUTS;
    private boolean _showParentLevel;

    public ExtEducationLevelsHighSchoolSelectModel(IEducationLevelModel model, IExtEducationLevelModel extModel)
    {
        this(model, extModel, null);
    }

    public ExtEducationLevelsHighSchoolSelectModel(IEducationLevelModel model, IExtEducationLevelModel extModel, OrgUnit orgUnit)
    {
        super(model);

        _extModel = extModel;
        _orgUnit = orgUnit;
    }

    public ExtEducationLevelsHighSchoolSelectModel dependFromFUTS(boolean value)
    {
        _dependFromFUTS = value;
        return this;
    }

    public ExtEducationLevelsHighSchoolSelectModel showParentLevel(boolean value)
    {
        _showParentLevel = value;
        return this;
    }

    @Override
    protected boolean isNeedRequest()
    {
        return ((_orgUnit != null) || (getModel().getFormativeOrgUnit() != null)) &&
                (!_dependFromFUTS || (getModel().getDevelopForm() != null && getModel().getDevelopCondition() != null && getModel().getDevelopTech() != null && getModel().getDevelopPeriod() != null));
    }

    @Override
    protected DQLSelectBuilder createEducationLevelsHighSchoolDQL(String alias)
    {
        final DQLSelectBuilder dql = super.createEducationLevelsHighSchoolDQL(alias);

        final EducationLevels parentLevel = _extModel != null ? _extModel.getParentEduLevel() : null;
        if (parentLevel != null) {

            IDQLExpression expression = eq(property(alias, EducationLevelsHighSchool.educationLevel().parentLevel()), value(parentLevel));
            if (_showParentLevel) {
                expression = or(expression, eq(property(alias, EducationLevelsHighSchool.educationLevel()), value(parentLevel)));
            }

            dql.where(expression);
        }
        return dql;
    }

    @Override
    protected DQLSelectBuilder createEducationOrgUnitBuilder(String alias)
    {
        final DQLSelectBuilder dql = super.createEducationOrgUnitBuilder(alias);

        if (_orgUnit != null) {
            dql.where(or(
                    eq(property(alias, EducationOrgUnit.formativeOrgUnit()), value(_orgUnit)),
                    eq(property(alias, EducationOrgUnit.territorialOrgUnit()), value(_orgUnit))
            ));
        }

        if (_dependFromFUTS) {
            dql.where(eq(property(alias, EducationOrgUnit.developForm()), value(getModel().getDevelopForm())));
            dql.where(eq(property(alias, EducationOrgUnit.developCondition()), value(getModel().getDevelopCondition())));
            dql.where(eq(property(alias, EducationOrgUnit.developTech()), value(getModel().getDevelopTech())));
            dql.where(eq(property(alias, EducationOrgUnit.developPeriod()), value(getModel().getDevelopPeriod())));
        }

        return dql;
    }
}