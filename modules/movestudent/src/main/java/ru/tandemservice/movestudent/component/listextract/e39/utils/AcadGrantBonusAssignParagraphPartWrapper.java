/* $Id:$ */
package ru.tandemservice.movestudent.component.listextract.e39.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 1/21/13
 * Time: 4:31 PM
 */
public class AcadGrantBonusAssignParagraphPartWrapper implements Comparable<AcadGrantBonusAssignParagraphPartWrapper>
{
    private final Course _course;
    private final Group _group;
    private final ListStudentExtract _firstExtract;

    public AcadGrantBonusAssignParagraphPartWrapper(Course course, Group group, ListStudentExtract firstExtract)
    {
        _course = course;
        _group = group;
        _firstExtract = firstExtract;
    }

    private final List<AcadGrantBonusAssignExtractWrapper> _extractWrapperList = new ArrayList<AcadGrantBonusAssignExtractWrapper>();

    public Course getCourse()
    {
        return _course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public List<AcadGrantBonusAssignExtractWrapper> getExtractWrapperList()
    {
        return _extractWrapperList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof AcadGrantBonusAssignParagraphPartWrapper))
            return false;

        AcadGrantBonusAssignParagraphPartWrapper that = (AcadGrantBonusAssignParagraphPartWrapper) o;

        return _course.equals(that.getCourse()) && _group.equals(that.getGroup());
    }

    @Override
    public int hashCode()
    {
        return _course.hashCode() & _group.hashCode();
    }

    @Override
    public int compareTo(AcadGrantBonusAssignParagraphPartWrapper o)
    {
        int result = Course.COURSE_COMPARATOR.compare(_course, o.getCourse());
        if (result == 0)
            result = _group.getTitle().compareTo(o.getGroup().getTitle());

        return result;
    }
}
