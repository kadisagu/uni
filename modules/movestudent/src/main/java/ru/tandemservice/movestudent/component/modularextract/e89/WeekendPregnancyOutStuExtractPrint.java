/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e89;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract;

import java.util.Collections;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 10.12.2012
 */
public class WeekendPregnancyOutStuExtractPrint implements IPrintFormCreator<WeekendPregnancyOutStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, WeekendPregnancyOutStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        if (extract.isResumeGrantPay())
        {
            modifier.put("resumeGrantPay", ApplicationRuntime.getProperty("weekendPregnancyOut.grantPaymentResumption")
                    .replaceAll("\\{grantSum\\}", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGrantSum()))
                    .replaceAll("\\{beginDate\\}", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getGrantPayBeginDate()))
                    .replaceAll("\\{endDate\\}", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getGrantPayEndDate())));

        }
        else
        {
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("resumeGrantPay"), true, false);
        }

        modifier.put("groupNew", extract.getGroupNew().getTitle());
        CommonExtractPrint.initFefuGroup(modifier, "intoGroupNew", extract.getGroupNew(), extract.getEntity().getEducationOrgUnit().getDevelopForm(), " в группу ");

        modifier.put("weekendOutDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getWeekendOutDate()));
        CommonExtractPrint.initEducationType(modifier, extract.getEntity().getEducationOrgUnit(), "Direction", true, false);

        additionalModify(modifier, extract);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    public void additionalModify(RtfInjectModifier modifier, WeekendPregnancyOutStuExtract extract)
    {
    }
}