package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.movestudent.entity.gen.StudentReasonToBasicRelGen;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;

public class StudentReasonToBasicRel extends StudentReasonToBasicRelGen implements IEntityRelation<StudentOrderReasons, StudentOrderBasics>
{
}