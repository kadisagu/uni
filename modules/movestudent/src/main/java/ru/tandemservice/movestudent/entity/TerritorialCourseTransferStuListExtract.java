package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.TerritorialCourseTransferStuListExtractGen;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Date;

/**
 * Проект приказа «О переводе с курса на следующий курс (по направлениям подготовки) со сменой территориального подразделения»
 */
public class TerritorialCourseTransferStuListExtract extends TerritorialCourseTransferStuListExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }

    @Override
    public Date getBeginDate()
    {
        return getTransferDate();
    }

    @Override
    public Group getGroupOld()
    {
        return getGroup();
    }
}