package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.TransferTerritorialExtStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О переводе со специальности на специальность (с направления на направление) между территориальными подразделениями
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TransferTerritorialExtStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.TransferTerritorialExtStuExtract";
    public static final String ENTITY_NAME = "transferTerritorialExtStuExtract";
    public static final int VERSION_HASH = -503453450;
    private static IEntityMeta ENTITY_META;

    public static final String P_LIQUIDATE_EDU_PLAN_DIFFERENCE = "liquidateEduPlanDifference";
    public static final String P_LIQUIDATION_DEADLINE_DATE = "liquidationDeadlineDate";
    public static final String P_FOR_STATE_FINAL_ATTESTATION = "forStateFinalAttestation";

    private boolean _liquidateEduPlanDifference;     // Ликвидировать разницу в учебных планах
    private Date _liquidationDeadlineDate;     // Крайняя дата ликвидации разницы в учебных планах
    private boolean _forStateFinalAttestation;     // Для прохождения ГИА

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Ликвидировать разницу в учебных планах. Свойство не может быть null.
     */
    @NotNull
    public boolean isLiquidateEduPlanDifference()
    {
        return _liquidateEduPlanDifference;
    }

    /**
     * @param liquidateEduPlanDifference Ликвидировать разницу в учебных планах. Свойство не может быть null.
     */
    public void setLiquidateEduPlanDifference(boolean liquidateEduPlanDifference)
    {
        dirty(_liquidateEduPlanDifference, liquidateEduPlanDifference);
        _liquidateEduPlanDifference = liquidateEduPlanDifference;
    }

    /**
     * @return Крайняя дата ликвидации разницы в учебных планах.
     */
    public Date getLiquidationDeadlineDate()
    {
        return _liquidationDeadlineDate;
    }

    /**
     * @param liquidationDeadlineDate Крайняя дата ликвидации разницы в учебных планах.
     */
    public void setLiquidationDeadlineDate(Date liquidationDeadlineDate)
    {
        dirty(_liquidationDeadlineDate, liquidationDeadlineDate);
        _liquidationDeadlineDate = liquidationDeadlineDate;
    }

    /**
     * @return Для прохождения ГИА. Свойство не может быть null.
     */
    @NotNull
    public boolean isForStateFinalAttestation()
    {
        return _forStateFinalAttestation;
    }

    /**
     * @param forStateFinalAttestation Для прохождения ГИА. Свойство не может быть null.
     */
    public void setForStateFinalAttestation(boolean forStateFinalAttestation)
    {
        dirty(_forStateFinalAttestation, forStateFinalAttestation);
        _forStateFinalAttestation = forStateFinalAttestation;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TransferTerritorialExtStuExtractGen)
        {
            setLiquidateEduPlanDifference(((TransferTerritorialExtStuExtract)another).isLiquidateEduPlanDifference());
            setLiquidationDeadlineDate(((TransferTerritorialExtStuExtract)another).getLiquidationDeadlineDate());
            setForStateFinalAttestation(((TransferTerritorialExtStuExtract)another).isForStateFinalAttestation());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TransferTerritorialExtStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TransferTerritorialExtStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new TransferTerritorialExtStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "liquidateEduPlanDifference":
                    return obj.isLiquidateEduPlanDifference();
                case "liquidationDeadlineDate":
                    return obj.getLiquidationDeadlineDate();
                case "forStateFinalAttestation":
                    return obj.isForStateFinalAttestation();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "liquidateEduPlanDifference":
                    obj.setLiquidateEduPlanDifference((Boolean) value);
                    return;
                case "liquidationDeadlineDate":
                    obj.setLiquidationDeadlineDate((Date) value);
                    return;
                case "forStateFinalAttestation":
                    obj.setForStateFinalAttestation((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "liquidateEduPlanDifference":
                        return true;
                case "liquidationDeadlineDate":
                        return true;
                case "forStateFinalAttestation":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "liquidateEduPlanDifference":
                    return true;
                case "liquidationDeadlineDate":
                    return true;
                case "forStateFinalAttestation":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "liquidateEduPlanDifference":
                    return Boolean.class;
                case "liquidationDeadlineDate":
                    return Date.class;
                case "forStateFinalAttestation":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TransferTerritorialExtStuExtract> _dslPath = new Path<TransferTerritorialExtStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TransferTerritorialExtStuExtract");
    }
            

    /**
     * @return Ликвидировать разницу в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferTerritorialExtStuExtract#isLiquidateEduPlanDifference()
     */
    public static PropertyPath<Boolean> liquidateEduPlanDifference()
    {
        return _dslPath.liquidateEduPlanDifference();
    }

    /**
     * @return Крайняя дата ликвидации разницы в учебных планах.
     * @see ru.tandemservice.movestudent.entity.TransferTerritorialExtStuExtract#getLiquidationDeadlineDate()
     */
    public static PropertyPath<Date> liquidationDeadlineDate()
    {
        return _dslPath.liquidationDeadlineDate();
    }

    /**
     * @return Для прохождения ГИА. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferTerritorialExtStuExtract#isForStateFinalAttestation()
     */
    public static PropertyPath<Boolean> forStateFinalAttestation()
    {
        return _dslPath.forStateFinalAttestation();
    }

    public static class Path<E extends TransferTerritorialExtStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<Boolean> _liquidateEduPlanDifference;
        private PropertyPath<Date> _liquidationDeadlineDate;
        private PropertyPath<Boolean> _forStateFinalAttestation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Ликвидировать разницу в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferTerritorialExtStuExtract#isLiquidateEduPlanDifference()
     */
        public PropertyPath<Boolean> liquidateEduPlanDifference()
        {
            if(_liquidateEduPlanDifference == null )
                _liquidateEduPlanDifference = new PropertyPath<Boolean>(TransferTerritorialExtStuExtractGen.P_LIQUIDATE_EDU_PLAN_DIFFERENCE, this);
            return _liquidateEduPlanDifference;
        }

    /**
     * @return Крайняя дата ликвидации разницы в учебных планах.
     * @see ru.tandemservice.movestudent.entity.TransferTerritorialExtStuExtract#getLiquidationDeadlineDate()
     */
        public PropertyPath<Date> liquidationDeadlineDate()
        {
            if(_liquidationDeadlineDate == null )
                _liquidationDeadlineDate = new PropertyPath<Date>(TransferTerritorialExtStuExtractGen.P_LIQUIDATION_DEADLINE_DATE, this);
            return _liquidationDeadlineDate;
        }

    /**
     * @return Для прохождения ГИА. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferTerritorialExtStuExtract#isForStateFinalAttestation()
     */
        public PropertyPath<Boolean> forStateFinalAttestation()
        {
            if(_forStateFinalAttestation == null )
                _forStateFinalAttestation = new PropertyPath<Boolean>(TransferTerritorialExtStuExtractGen.P_FOR_STATE_FINAL_ATTESTATION, this);
            return _forStateFinalAttestation;
        }

        public Class getEntityClass()
        {
            return TransferTerritorialExtStuExtract.class;
        }

        public String getEntityName()
        {
            return "transferTerritorialExtStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
