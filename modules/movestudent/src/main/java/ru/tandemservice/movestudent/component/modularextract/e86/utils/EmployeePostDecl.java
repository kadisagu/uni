/* $Id$ */
package ru.tandemservice.movestudent.component.modularextract.e86.utils;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;

/**
 * @author nvankov
 * @since 11/21/13
 */
public class EmployeePostDecl
{
    public static String getEmployeeStrInst(EmployeePost employeePost)
    {
        StringBuilder emplPostStrBuilder = new StringBuilder();
	    if (employeePost!= null)
	    {
		    String emplPostIT = employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().getInstrumentalCaseTitle();
		    String emplPostInstrumental = !StringUtils.isEmpty(emplPostIT) ? emplPostIT :
				    employeePost.getPostRelation().getPostBoundedWithQGandQL().getTitle();

		    if (!StringUtils.isEmpty(emplPostInstrumental))
			    emplPostStrBuilder.append(StringUtils.isEmpty(emplPostStrBuilder.toString()) ? "" : " ").append(emplPostInstrumental.toLowerCase());

		    emplPostStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedFioInitials(employeePost.getPerson().getIdentityCard(), GrammaCase.INSTRUMENTAL).replaceAll(" ", "\u00A0"));
	    }

        return emplPostStrBuilder.toString();
    }

    public static String getEmployeeStrDat(EmployeePost employeePost)
    {
        StringBuilder emplPostStrBuilder = new StringBuilder();
	    if(employeePost != null)
	    {
		    String emplPostIT = employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().getDativeCaseTitle();
		    String emplPostDative = !StringUtils.isEmpty(emplPostIT) ? emplPostIT :
				    employeePost.getPostRelation().getPostBoundedWithQGandQL().getTitle();

		    if (!StringUtils.isEmpty(emplPostDative))
			    emplPostStrBuilder.append(StringUtils.isEmpty(emplPostStrBuilder.toString()) ? "" : " ").append(emplPostDative.toLowerCase());

		    emplPostStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedFioInitials(employeePost.getPerson().getIdentityCard(), GrammaCase.DATIVE).replaceAll(" ", "\u00A0"));
	    }

        return emplPostStrBuilder.toString();
    }
}
