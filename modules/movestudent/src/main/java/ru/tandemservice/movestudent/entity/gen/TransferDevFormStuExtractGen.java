package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.TransferDevFormStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О переводе на другую форму освоения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TransferDevFormStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.TransferDevFormStuExtract";
    public static final String ENTITY_NAME = "transferDevFormStuExtract";
    public static final int VERSION_HASH = 1933825175;
    private static IEntityMeta ENTITY_META;

    public static final String P_TRANSFER_DATE = "transferDate";

    private Date _transferDate;     // Дата перевода

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата перевода.
     */
    public Date getTransferDate()
    {
        return _transferDate;
    }

    /**
     * @param transferDate Дата перевода.
     */
    public void setTransferDate(Date transferDate)
    {
        dirty(_transferDate, transferDate);
        _transferDate = transferDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TransferDevFormStuExtractGen)
        {
            setTransferDate(((TransferDevFormStuExtract)another).getTransferDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TransferDevFormStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TransferDevFormStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new TransferDevFormStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return obj.getTransferDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "transferDate":
                    obj.setTransferDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TransferDevFormStuExtract> _dslPath = new Path<TransferDevFormStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TransferDevFormStuExtract");
    }
            

    /**
     * @return Дата перевода.
     * @see ru.tandemservice.movestudent.entity.TransferDevFormStuExtract#getTransferDate()
     */
    public static PropertyPath<Date> transferDate()
    {
        return _dslPath.transferDate();
    }

    public static class Path<E extends TransferDevFormStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<Date> _transferDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата перевода.
     * @see ru.tandemservice.movestudent.entity.TransferDevFormStuExtract#getTransferDate()
     */
        public PropertyPath<Date> transferDate()
        {
            if(_transferDate == null )
                _transferDate = new PropertyPath<Date>(TransferDevFormStuExtractGen.P_TRANSFER_DATE, this);
            return _transferDate;
        }

        public Class getEntityClass()
        {
            return TransferDevFormStuExtract.class;
        }

        public String getEntityName()
        {
            return "transferDevFormStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
