package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О выпуске студентов с отличием»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GraduateSuccessStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract";
    public static final String ENTITY_NAME = "graduateSuccessStuListExtract";
    public static final int VERSION_HASH = -946940857;
    private static IEntityMeta ENTITY_META;

    public static final String P_GRADUATE_DATE = "graduateDate";
    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_STATUS_OLD = "statusOld";
    public static final String L_STATUS_NEW = "statusNew";
    public static final String P_PREV_GRADUATE_SUCC_ORDER_DATE = "prevGraduateSuccOrderDate";
    public static final String P_PREV_GRADUATE_SUCC_ORDER_NUMBER = "prevGraduateSuccOrderNumber";
    public static final String P_FINISHED_YEAR = "finishedYear";

    private Date _graduateDate;     // Дата выпуска
    private Course _course;     // Курс
    private Group _group;     // Группа
    private StudentStatus _statusOld;     // Состояние студента
    private StudentStatus _statusNew;     // Состояние студента
    private Date _prevGraduateSuccOrderDate;     // Дата предыдущего приказа о выпуске (с отличием)
    private String _prevGraduateSuccOrderNumber;     // Номер предыдущего приказа о выпуске (с отличием)
    private Integer _finishedYear;     // Год окончания на момент проведения приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата выпуска.
     */
    public Date getGraduateDate()
    {
        return _graduateDate;
    }

    /**
     * @param graduateDate Дата выпуска.
     */
    public void setGraduateDate(Date graduateDate)
    {
        dirty(_graduateDate, graduateDate);
        _graduateDate = graduateDate;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStatusOld()
    {
        return _statusOld;
    }

    /**
     * @param statusOld Состояние студента. Свойство не может быть null.
     */
    public void setStatusOld(StudentStatus statusOld)
    {
        dirty(_statusOld, statusOld);
        _statusOld = statusOld;
    }

    /**
     * @return Состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStatusNew()
    {
        return _statusNew;
    }

    /**
     * @param statusNew Состояние студента. Свойство не может быть null.
     */
    public void setStatusNew(StudentStatus statusNew)
    {
        dirty(_statusNew, statusNew);
        _statusNew = statusNew;
    }

    /**
     * @return Дата предыдущего приказа о выпуске (с отличием).
     */
    public Date getPrevGraduateSuccOrderDate()
    {
        initLazyForGet("prevGraduateSuccOrderDate");
        return _prevGraduateSuccOrderDate;
    }

    /**
     * @param prevGraduateSuccOrderDate Дата предыдущего приказа о выпуске (с отличием).
     */
    public void setPrevGraduateSuccOrderDate(Date prevGraduateSuccOrderDate)
    {
        initLazyForSet("prevGraduateSuccOrderDate");
        dirty(_prevGraduateSuccOrderDate, prevGraduateSuccOrderDate);
        _prevGraduateSuccOrderDate = prevGraduateSuccOrderDate;
    }

    /**
     * @return Номер предыдущего приказа о выпуске (с отличием).
     */
    @Length(max=255)
    public String getPrevGraduateSuccOrderNumber()
    {
        initLazyForGet("prevGraduateSuccOrderNumber");
        return _prevGraduateSuccOrderNumber;
    }

    /**
     * @param prevGraduateSuccOrderNumber Номер предыдущего приказа о выпуске (с отличием).
     */
    public void setPrevGraduateSuccOrderNumber(String prevGraduateSuccOrderNumber)
    {
        initLazyForSet("prevGraduateSuccOrderNumber");
        dirty(_prevGraduateSuccOrderNumber, prevGraduateSuccOrderNumber);
        _prevGraduateSuccOrderNumber = prevGraduateSuccOrderNumber;
    }

    /**
     * @return Год окончания на момент проведения приказа.
     */
    public Integer getFinishedYear()
    {
        return _finishedYear;
    }

    /**
     * @param finishedYear Год окончания на момент проведения приказа.
     */
    public void setFinishedYear(Integer finishedYear)
    {
        dirty(_finishedYear, finishedYear);
        _finishedYear = finishedYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof GraduateSuccessStuListExtractGen)
        {
            setGraduateDate(((GraduateSuccessStuListExtract)another).getGraduateDate());
            setCourse(((GraduateSuccessStuListExtract)another).getCourse());
            setGroup(((GraduateSuccessStuListExtract)another).getGroup());
            setStatusOld(((GraduateSuccessStuListExtract)another).getStatusOld());
            setStatusNew(((GraduateSuccessStuListExtract)another).getStatusNew());
            setPrevGraduateSuccOrderDate(((GraduateSuccessStuListExtract)another).getPrevGraduateSuccOrderDate());
            setPrevGraduateSuccOrderNumber(((GraduateSuccessStuListExtract)another).getPrevGraduateSuccOrderNumber());
            setFinishedYear(((GraduateSuccessStuListExtract)another).getFinishedYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GraduateSuccessStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GraduateSuccessStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new GraduateSuccessStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "graduateDate":
                    return obj.getGraduateDate();
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "statusOld":
                    return obj.getStatusOld();
                case "statusNew":
                    return obj.getStatusNew();
                case "prevGraduateSuccOrderDate":
                    return obj.getPrevGraduateSuccOrderDate();
                case "prevGraduateSuccOrderNumber":
                    return obj.getPrevGraduateSuccOrderNumber();
                case "finishedYear":
                    return obj.getFinishedYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "graduateDate":
                    obj.setGraduateDate((Date) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "statusOld":
                    obj.setStatusOld((StudentStatus) value);
                    return;
                case "statusNew":
                    obj.setStatusNew((StudentStatus) value);
                    return;
                case "prevGraduateSuccOrderDate":
                    obj.setPrevGraduateSuccOrderDate((Date) value);
                    return;
                case "prevGraduateSuccOrderNumber":
                    obj.setPrevGraduateSuccOrderNumber((String) value);
                    return;
                case "finishedYear":
                    obj.setFinishedYear((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "graduateDate":
                        return true;
                case "course":
                        return true;
                case "group":
                        return true;
                case "statusOld":
                        return true;
                case "statusNew":
                        return true;
                case "prevGraduateSuccOrderDate":
                        return true;
                case "prevGraduateSuccOrderNumber":
                        return true;
                case "finishedYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "graduateDate":
                    return true;
                case "course":
                    return true;
                case "group":
                    return true;
                case "statusOld":
                    return true;
                case "statusNew":
                    return true;
                case "prevGraduateSuccOrderDate":
                    return true;
                case "prevGraduateSuccOrderNumber":
                    return true;
                case "finishedYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "graduateDate":
                    return Date.class;
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "statusOld":
                    return StudentStatus.class;
                case "statusNew":
                    return StudentStatus.class;
                case "prevGraduateSuccOrderDate":
                    return Date.class;
                case "prevGraduateSuccOrderNumber":
                    return String.class;
                case "finishedYear":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GraduateSuccessStuListExtract> _dslPath = new Path<GraduateSuccessStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GraduateSuccessStuListExtract");
    }
            

    /**
     * @return Дата выпуска.
     * @see ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract#getGraduateDate()
     */
    public static PropertyPath<Date> graduateDate()
    {
        return _dslPath.graduateDate();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract#getStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> statusOld()
    {
        return _dslPath.statusOld();
    }

    /**
     * @return Состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract#getStatusNew()
     */
    public static StudentStatus.Path<StudentStatus> statusNew()
    {
        return _dslPath.statusNew();
    }

    /**
     * @return Дата предыдущего приказа о выпуске (с отличием).
     * @see ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract#getPrevGraduateSuccOrderDate()
     */
    public static PropertyPath<Date> prevGraduateSuccOrderDate()
    {
        return _dslPath.prevGraduateSuccOrderDate();
    }

    /**
     * @return Номер предыдущего приказа о выпуске (с отличием).
     * @see ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract#getPrevGraduateSuccOrderNumber()
     */
    public static PropertyPath<String> prevGraduateSuccOrderNumber()
    {
        return _dslPath.prevGraduateSuccOrderNumber();
    }

    /**
     * @return Год окончания на момент проведения приказа.
     * @see ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract#getFinishedYear()
     */
    public static PropertyPath<Integer> finishedYear()
    {
        return _dslPath.finishedYear();
    }

    public static class Path<E extends GraduateSuccessStuListExtract> extends ListStudentExtract.Path<E>
    {
        private PropertyPath<Date> _graduateDate;
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private StudentStatus.Path<StudentStatus> _statusOld;
        private StudentStatus.Path<StudentStatus> _statusNew;
        private PropertyPath<Date> _prevGraduateSuccOrderDate;
        private PropertyPath<String> _prevGraduateSuccOrderNumber;
        private PropertyPath<Integer> _finishedYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата выпуска.
     * @see ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract#getGraduateDate()
     */
        public PropertyPath<Date> graduateDate()
        {
            if(_graduateDate == null )
                _graduateDate = new PropertyPath<Date>(GraduateSuccessStuListExtractGen.P_GRADUATE_DATE, this);
            return _graduateDate;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract#getStatusOld()
     */
        public StudentStatus.Path<StudentStatus> statusOld()
        {
            if(_statusOld == null )
                _statusOld = new StudentStatus.Path<StudentStatus>(L_STATUS_OLD, this);
            return _statusOld;
        }

    /**
     * @return Состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract#getStatusNew()
     */
        public StudentStatus.Path<StudentStatus> statusNew()
        {
            if(_statusNew == null )
                _statusNew = new StudentStatus.Path<StudentStatus>(L_STATUS_NEW, this);
            return _statusNew;
        }

    /**
     * @return Дата предыдущего приказа о выпуске (с отличием).
     * @see ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract#getPrevGraduateSuccOrderDate()
     */
        public PropertyPath<Date> prevGraduateSuccOrderDate()
        {
            if(_prevGraduateSuccOrderDate == null )
                _prevGraduateSuccOrderDate = new PropertyPath<Date>(GraduateSuccessStuListExtractGen.P_PREV_GRADUATE_SUCC_ORDER_DATE, this);
            return _prevGraduateSuccOrderDate;
        }

    /**
     * @return Номер предыдущего приказа о выпуске (с отличием).
     * @see ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract#getPrevGraduateSuccOrderNumber()
     */
        public PropertyPath<String> prevGraduateSuccOrderNumber()
        {
            if(_prevGraduateSuccOrderNumber == null )
                _prevGraduateSuccOrderNumber = new PropertyPath<String>(GraduateSuccessStuListExtractGen.P_PREV_GRADUATE_SUCC_ORDER_NUMBER, this);
            return _prevGraduateSuccOrderNumber;
        }

    /**
     * @return Год окончания на момент проведения приказа.
     * @see ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract#getFinishedYear()
     */
        public PropertyPath<Integer> finishedYear()
        {
            if(_finishedYear == null )
                _finishedYear = new PropertyPath<Integer>(GraduateSuccessStuListExtractGen.P_FINISHED_YEAR, this);
            return _finishedYear;
        }

        public Class getEntityClass()
        {
            return GraduateSuccessStuListExtract.class;
        }

        public String getEntityName()
        {
            return "graduateSuccessStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
