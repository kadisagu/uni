/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e12.MultipleParagraphAdd;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListMultipleParagraphAdd.AbstractListMultipleParagraphAddModel;
import ru.tandemservice.movestudent.entity.CourseTransferDebtorStuListExtract;
import ru.tandemservice.uni.entity.catalog.Course;

import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 23.07.2009
 */
public class Model extends AbstractListMultipleParagraphAddModel<CourseTransferDebtorStuListExtract>
{
    private Course _course;
    private Course _courseOld;
    private Course _courseNew;
    private boolean _groupChangeCourse;
    private List<Course> _courseList;
    private ISelectModel _courseOldListModel;
    private ISelectModel _courseNewListModel;

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        this._course = course;
    }

    public Course getCourseOld()
    {
        return _courseOld;
    }

    public void setCourseOld(Course courseOld)
    {
        this._courseOld = courseOld;
    }

    public Course getCourseNew()
    {
        return _courseNew;
    }

    public void setCourseNew(Course courseNew)
    {
        this._courseNew = courseNew;
    }

    public boolean isGroupChangeCourse()
    {
        return _groupChangeCourse;
    }

    public void setGroupChangeCourse(boolean groupChangeCourse)
    {
        this._groupChangeCourse = groupChangeCourse;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        this._courseList = courseList;
    }

    public ISelectModel getCourseOldListModel()
    {
        return _courseOldListModel;
    }

    public void setCourseOldListModel(ISelectModel courseOldListModel)
    {
        this._courseOldListModel = courseOldListModel;
    }

    public ISelectModel getCourseNewListModel()
    {
        return _courseNewListModel;
    }

    public void setCourseNewListModel(ISelectModel courseNewListModel)
    {
        this._courseNewListModel = courseNewListModel;
    }
}