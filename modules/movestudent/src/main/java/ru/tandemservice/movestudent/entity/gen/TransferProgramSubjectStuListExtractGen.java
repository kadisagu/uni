package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О переводе на направление из нового перечня»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TransferProgramSubjectStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract";
    public static final String ENTITY_NAME = "transferProgramSubjectStuListExtract";
    public static final int VERSION_HASH = 376731005;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_ORG_UNIT_OLD = "educationOrgUnitOld";
    public static final String L_EDUCATION_ORG_UNIT_NEW = "educationOrgUnitNew";
    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String P_TRANSFER_DATE = "transferDate";
    public static final String P_CHANGE_GROUP_EDUCATION_O_U = "changeGroupEducationOU";
    public static final String L_GROUP_EDUCATION_O_U_OLD = "groupEducationOUOld";
    public static final String L_GROUP_EDUCATION_O_U_NEW = "groupEducationOUNew";
    public static final String L_EDU_PROGRAM_SUBJECT_OLD = "eduProgramSubjectOld";
    public static final String L_EDU_PROGRAM_SUBJECT_NEW = "eduProgramSubjectNew";

    private EducationOrgUnit _educationOrgUnitOld;     // Направление подготовки (старое)
    private EducationOrgUnit _educationOrgUnitNew;     // Направление подготовки (новое)
    private Course _course;     // Курс
    private Group _group;     // Группа
    private Date _transferDate;     // Дата перевода
    private boolean _changeGroupEducationOU = true;     // Сменить направление подготовки для группы
    private EducationOrgUnit _groupEducationOUOld;     // Направление подготовки группы(старое)
    private EducationOrgUnit _groupEducationOUNew;     // Направление подготовки группы (новое)
    private EduProgramSubject _eduProgramSubjectOld;     // Направление подготовки профессионального образов. (старое)
    private EduProgramSubject2013 _eduProgramSubjectNew;     // Направление подготовки профессионального образов. (новое)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направление подготовки (старое).
     */
    public EducationOrgUnit getEducationOrgUnitOld()
    {
        return _educationOrgUnitOld;
    }

    /**
     * @param educationOrgUnitOld Направление подготовки (старое).
     */
    public void setEducationOrgUnitOld(EducationOrgUnit educationOrgUnitOld)
    {
        dirty(_educationOrgUnitOld, educationOrgUnitOld);
        _educationOrgUnitOld = educationOrgUnitOld;
    }

    /**
     * @return Направление подготовки (новое). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitNew()
    {
        return _educationOrgUnitNew;
    }

    /**
     * @param educationOrgUnitNew Направление подготовки (новое). Свойство не может быть null.
     */
    public void setEducationOrgUnitNew(EducationOrgUnit educationOrgUnitNew)
    {
        dirty(_educationOrgUnitNew, educationOrgUnitNew);
        _educationOrgUnitNew = educationOrgUnitNew;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Дата перевода.
     */
    public Date getTransferDate()
    {
        return _transferDate;
    }

    /**
     * @param transferDate Дата перевода.
     */
    public void setTransferDate(Date transferDate)
    {
        dirty(_transferDate, transferDate);
        _transferDate = transferDate;
    }

    /**
     * @return Сменить направление подготовки для группы. Свойство не может быть null.
     */
    @NotNull
    public boolean isChangeGroupEducationOU()
    {
        return _changeGroupEducationOU;
    }

    /**
     * @param changeGroupEducationOU Сменить направление подготовки для группы. Свойство не может быть null.
     */
    public void setChangeGroupEducationOU(boolean changeGroupEducationOU)
    {
        dirty(_changeGroupEducationOU, changeGroupEducationOU);
        _changeGroupEducationOU = changeGroupEducationOU;
    }

    /**
     * @return Направление подготовки группы(старое).
     */
    public EducationOrgUnit getGroupEducationOUOld()
    {
        return _groupEducationOUOld;
    }

    /**
     * @param groupEducationOUOld Направление подготовки группы(старое).
     */
    public void setGroupEducationOUOld(EducationOrgUnit groupEducationOUOld)
    {
        dirty(_groupEducationOUOld, groupEducationOUOld);
        _groupEducationOUOld = groupEducationOUOld;
    }

    /**
     * @return Направление подготовки группы (новое).
     */
    public EducationOrgUnit getGroupEducationOUNew()
    {
        return _groupEducationOUNew;
    }

    /**
     * @param groupEducationOUNew Направление подготовки группы (новое).
     */
    public void setGroupEducationOUNew(EducationOrgUnit groupEducationOUNew)
    {
        dirty(_groupEducationOUNew, groupEducationOUNew);
        _groupEducationOUNew = groupEducationOUNew;
    }

    /**
     * @return Направление подготовки профессионального образов. (старое). Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubject getEduProgramSubjectOld()
    {
        return _eduProgramSubjectOld;
    }

    /**
     * @param eduProgramSubjectOld Направление подготовки профессионального образов. (старое). Свойство не может быть null.
     */
    public void setEduProgramSubjectOld(EduProgramSubject eduProgramSubjectOld)
    {
        dirty(_eduProgramSubjectOld, eduProgramSubjectOld);
        _eduProgramSubjectOld = eduProgramSubjectOld;
    }

    /**
     * @return Направление подготовки профессионального образов. (новое). Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubject2013 getEduProgramSubjectNew()
    {
        return _eduProgramSubjectNew;
    }

    /**
     * @param eduProgramSubjectNew Направление подготовки профессионального образов. (новое). Свойство не может быть null.
     */
    public void setEduProgramSubjectNew(EduProgramSubject2013 eduProgramSubjectNew)
    {
        dirty(_eduProgramSubjectNew, eduProgramSubjectNew);
        _eduProgramSubjectNew = eduProgramSubjectNew;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TransferProgramSubjectStuListExtractGen)
        {
            setEducationOrgUnitOld(((TransferProgramSubjectStuListExtract)another).getEducationOrgUnitOld());
            setEducationOrgUnitNew(((TransferProgramSubjectStuListExtract)another).getEducationOrgUnitNew());
            setCourse(((TransferProgramSubjectStuListExtract)another).getCourse());
            setGroup(((TransferProgramSubjectStuListExtract)another).getGroup());
            setTransferDate(((TransferProgramSubjectStuListExtract)another).getTransferDate());
            setChangeGroupEducationOU(((TransferProgramSubjectStuListExtract)another).isChangeGroupEducationOU());
            setGroupEducationOUOld(((TransferProgramSubjectStuListExtract)another).getGroupEducationOUOld());
            setGroupEducationOUNew(((TransferProgramSubjectStuListExtract)another).getGroupEducationOUNew());
            setEduProgramSubjectOld(((TransferProgramSubjectStuListExtract)another).getEduProgramSubjectOld());
            setEduProgramSubjectNew(((TransferProgramSubjectStuListExtract)another).getEduProgramSubjectNew());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TransferProgramSubjectStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TransferProgramSubjectStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new TransferProgramSubjectStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "educationOrgUnitOld":
                    return obj.getEducationOrgUnitOld();
                case "educationOrgUnitNew":
                    return obj.getEducationOrgUnitNew();
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "transferDate":
                    return obj.getTransferDate();
                case "changeGroupEducationOU":
                    return obj.isChangeGroupEducationOU();
                case "groupEducationOUOld":
                    return obj.getGroupEducationOUOld();
                case "groupEducationOUNew":
                    return obj.getGroupEducationOUNew();
                case "eduProgramSubjectOld":
                    return obj.getEduProgramSubjectOld();
                case "eduProgramSubjectNew":
                    return obj.getEduProgramSubjectNew();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "educationOrgUnitOld":
                    obj.setEducationOrgUnitOld((EducationOrgUnit) value);
                    return;
                case "educationOrgUnitNew":
                    obj.setEducationOrgUnitNew((EducationOrgUnit) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "transferDate":
                    obj.setTransferDate((Date) value);
                    return;
                case "changeGroupEducationOU":
                    obj.setChangeGroupEducationOU((Boolean) value);
                    return;
                case "groupEducationOUOld":
                    obj.setGroupEducationOUOld((EducationOrgUnit) value);
                    return;
                case "groupEducationOUNew":
                    obj.setGroupEducationOUNew((EducationOrgUnit) value);
                    return;
                case "eduProgramSubjectOld":
                    obj.setEduProgramSubjectOld((EduProgramSubject) value);
                    return;
                case "eduProgramSubjectNew":
                    obj.setEduProgramSubjectNew((EduProgramSubject2013) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationOrgUnitOld":
                        return true;
                case "educationOrgUnitNew":
                        return true;
                case "course":
                        return true;
                case "group":
                        return true;
                case "transferDate":
                        return true;
                case "changeGroupEducationOU":
                        return true;
                case "groupEducationOUOld":
                        return true;
                case "groupEducationOUNew":
                        return true;
                case "eduProgramSubjectOld":
                        return true;
                case "eduProgramSubjectNew":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationOrgUnitOld":
                    return true;
                case "educationOrgUnitNew":
                    return true;
                case "course":
                    return true;
                case "group":
                    return true;
                case "transferDate":
                    return true;
                case "changeGroupEducationOU":
                    return true;
                case "groupEducationOUOld":
                    return true;
                case "groupEducationOUNew":
                    return true;
                case "eduProgramSubjectOld":
                    return true;
                case "eduProgramSubjectNew":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "educationOrgUnitOld":
                    return EducationOrgUnit.class;
                case "educationOrgUnitNew":
                    return EducationOrgUnit.class;
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "transferDate":
                    return Date.class;
                case "changeGroupEducationOU":
                    return Boolean.class;
                case "groupEducationOUOld":
                    return EducationOrgUnit.class;
                case "groupEducationOUNew":
                    return EducationOrgUnit.class;
                case "eduProgramSubjectOld":
                    return EduProgramSubject.class;
                case "eduProgramSubjectNew":
                    return EduProgramSubject2013.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TransferProgramSubjectStuListExtract> _dslPath = new Path<TransferProgramSubjectStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TransferProgramSubjectStuListExtract");
    }
            

    /**
     * @return Направление подготовки (старое).
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#getEducationOrgUnitOld()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
    {
        return _dslPath.educationOrgUnitOld();
    }

    /**
     * @return Направление подготовки (новое). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#getEducationOrgUnitNew()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
    {
        return _dslPath.educationOrgUnitNew();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Дата перевода.
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#getTransferDate()
     */
    public static PropertyPath<Date> transferDate()
    {
        return _dslPath.transferDate();
    }

    /**
     * @return Сменить направление подготовки для группы. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#isChangeGroupEducationOU()
     */
    public static PropertyPath<Boolean> changeGroupEducationOU()
    {
        return _dslPath.changeGroupEducationOU();
    }

    /**
     * @return Направление подготовки группы(старое).
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#getGroupEducationOUOld()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> groupEducationOUOld()
    {
        return _dslPath.groupEducationOUOld();
    }

    /**
     * @return Направление подготовки группы (новое).
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#getGroupEducationOUNew()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> groupEducationOUNew()
    {
        return _dslPath.groupEducationOUNew();
    }

    /**
     * @return Направление подготовки профессионального образов. (старое). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#getEduProgramSubjectOld()
     */
    public static EduProgramSubject.Path<EduProgramSubject> eduProgramSubjectOld()
    {
        return _dslPath.eduProgramSubjectOld();
    }

    /**
     * @return Направление подготовки профессионального образов. (новое). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#getEduProgramSubjectNew()
     */
    public static EduProgramSubject2013.Path<EduProgramSubject2013> eduProgramSubjectNew()
    {
        return _dslPath.eduProgramSubjectNew();
    }

    public static class Path<E extends TransferProgramSubjectStuListExtract> extends ListStudentExtract.Path<E>
    {
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitOld;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitNew;
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private PropertyPath<Date> _transferDate;
        private PropertyPath<Boolean> _changeGroupEducationOU;
        private EducationOrgUnit.Path<EducationOrgUnit> _groupEducationOUOld;
        private EducationOrgUnit.Path<EducationOrgUnit> _groupEducationOUNew;
        private EduProgramSubject.Path<EduProgramSubject> _eduProgramSubjectOld;
        private EduProgramSubject2013.Path<EduProgramSubject2013> _eduProgramSubjectNew;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направление подготовки (старое).
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#getEducationOrgUnitOld()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
        {
            if(_educationOrgUnitOld == null )
                _educationOrgUnitOld = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_OLD, this);
            return _educationOrgUnitOld;
        }

    /**
     * @return Направление подготовки (новое). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#getEducationOrgUnitNew()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
        {
            if(_educationOrgUnitNew == null )
                _educationOrgUnitNew = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_NEW, this);
            return _educationOrgUnitNew;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Дата перевода.
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#getTransferDate()
     */
        public PropertyPath<Date> transferDate()
        {
            if(_transferDate == null )
                _transferDate = new PropertyPath<Date>(TransferProgramSubjectStuListExtractGen.P_TRANSFER_DATE, this);
            return _transferDate;
        }

    /**
     * @return Сменить направление подготовки для группы. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#isChangeGroupEducationOU()
     */
        public PropertyPath<Boolean> changeGroupEducationOU()
        {
            if(_changeGroupEducationOU == null )
                _changeGroupEducationOU = new PropertyPath<Boolean>(TransferProgramSubjectStuListExtractGen.P_CHANGE_GROUP_EDUCATION_O_U, this);
            return _changeGroupEducationOU;
        }

    /**
     * @return Направление подготовки группы(старое).
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#getGroupEducationOUOld()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> groupEducationOUOld()
        {
            if(_groupEducationOUOld == null )
                _groupEducationOUOld = new EducationOrgUnit.Path<EducationOrgUnit>(L_GROUP_EDUCATION_O_U_OLD, this);
            return _groupEducationOUOld;
        }

    /**
     * @return Направление подготовки группы (новое).
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#getGroupEducationOUNew()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> groupEducationOUNew()
        {
            if(_groupEducationOUNew == null )
                _groupEducationOUNew = new EducationOrgUnit.Path<EducationOrgUnit>(L_GROUP_EDUCATION_O_U_NEW, this);
            return _groupEducationOUNew;
        }

    /**
     * @return Направление подготовки профессионального образов. (старое). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#getEduProgramSubjectOld()
     */
        public EduProgramSubject.Path<EduProgramSubject> eduProgramSubjectOld()
        {
            if(_eduProgramSubjectOld == null )
                _eduProgramSubjectOld = new EduProgramSubject.Path<EduProgramSubject>(L_EDU_PROGRAM_SUBJECT_OLD, this);
            return _eduProgramSubjectOld;
        }

    /**
     * @return Направление подготовки профессионального образов. (новое). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract#getEduProgramSubjectNew()
     */
        public EduProgramSubject2013.Path<EduProgramSubject2013> eduProgramSubjectNew()
        {
            if(_eduProgramSubjectNew == null )
                _eduProgramSubjectNew = new EduProgramSubject2013.Path<EduProgramSubject2013>(L_EDU_PROGRAM_SUBJECT_NEW, this);
            return _eduProgramSubjectNew;
        }

        public Class getEntityClass()
        {
            return TransferProgramSubjectStuListExtract.class;
        }

        public String getEntityName()
        {
            return "transferProgramSubjectStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
