/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e12;

import java.util.Map;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 11.01.2009
 */
public class WeekendPregnancyStuExtractDao extends UniBaseDao implements IExtractComponentDao<WeekendPregnancyStuExtract>
{
    @Override
    public void doCommit(WeekendPregnancyStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        //save rollback data
        Student student = extract.getEntity();
        extract.setStudentStatusOld(student.getStatus());

        //set new values
        student.setStatus(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_PREGNANCY));
        
        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        else
        {
            extract.setPrevOrderDate(orderData.getWeekendPregnancyOrderDate());
            extract.setPrevOrderNumber(orderData.getWeekendPregnancyOrderNumber());
            extract.setPrevBeginDate(orderData.getWeekendPregnancyDateFrom());
            extract.setPrevEndDate(orderData.getWeekendPregnancyDateTo());
        }
        
        if (null != extract.getParagraph() && null != extract.getParagraph().getOrder())
        {
            orderData.setWeekendPregnancyOrderDate(extract.getParagraph().getOrder().getCommitDate());
            orderData.setWeekendPregnancyOrderNumber(extract.getParagraph().getOrder().getNumber());
        }
        orderData.setWeekendPregnancyDateFrom(extract.getBeginDate());
        orderData.setWeekendPregnancyDateTo(extract.getEndDate());
        getSession().saveOrUpdate(orderData);        
    }

    @Override
    public void doRollback(WeekendPregnancyStuExtract extract, Map parameters)
    {
        Student student = extract.getEntity();
        student.setStatus(extract.getStudentStatusOld());
        
        // возвращаем предыдущие номер и дату приказа
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setWeekendPregnancyOrderDate(extract.getPrevOrderDate());
        orderData.setWeekendPregnancyOrderNumber(extract.getPrevOrderNumber());
        orderData.setWeekendPregnancyDateFrom(extract.getPrevBeginDate());
        orderData.setWeekendPregnancyDateTo(extract.getPrevEndDate());
        getSession().saveOrUpdate(orderData);        
    }
}