/* $Id$ */
package ru.tandemservice.movestudent.component.order.list.StudentListExtractPrint;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.movestudent.entity.ListStudentExtract;

/**
 * @author dseleznev
 * @since 27.01.2012
 */
@State({
        @Bind(key = "extractId", binding = "extractId"),
        @Bind(key = "printPdf", binding = "printPdf")
})
public class Model
{
    private Long _extractId;
    private ListStudentExtract _extract;
    private byte[] _data;
    private Boolean _printPdf;

    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        _extractId = extractId;
    }

    public ListStudentExtract getExtract()
    {
        return _extract;
    }

    public void setExtract(ListStudentExtract extract)
    {
        _extract = extract;
    }

    public byte[] getData()
    {
        return _data;
    }

    public void setData(byte[] data)
    {
        _data = data;
    }

    public Boolean getPrintPdf()
    {
        return _printPdf;
    }

    public void setPrintPdf(Boolean printPdf)
    {
        _printPdf = printPdf;
    }
}