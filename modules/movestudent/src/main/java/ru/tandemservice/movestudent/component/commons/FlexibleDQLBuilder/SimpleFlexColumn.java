/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

/**
 * @author Nikolay Fedorovskih
 * @since 02.12.2013
 */
public class SimpleFlexColumn extends BaseFlexColumn
{
    public SimpleFlexColumn(String alias, String nameAndPath)
    {
        this(alias, nameAndPath, nameAndPath);
    }

    public SimpleFlexColumn(String alias, String propertyPath, String name)
    {
        this(alias, propertyPath, name, true);
    }

    public SimpleFlexColumn(String alias, String propertyPath, String name, boolean canBeHidden)
    {
        super(name, propertyPath, alias, canBeHidden);
    }

    public SimpleFlexColumn(SimpleFlexColumn baseColumn)
    {
        this(baseColumn.getAlias(), baseColumn.getPath(), baseColumn.getName(), baseColumn.canBeHidden());
    }

    @Override
    public boolean isEntityColumn()
    {
        return getPath() == null;
    }

}