package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.RestorationStuExtractExtGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О восстановлении
 */
public class RestorationStuExtractExt extends RestorationStuExtractExtGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}