/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.customorder.listextract.ParagraphPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.BaseCustomActionPubModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubController;
import ru.tandemservice.movestudent.entity.custom.ModifyListStuExtract;

import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 07.10.2013
 */
public class Controller extends AbstractListParagraphPubController<ModifyListStuExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        Model model = component.getModel();

        for (Map.Entry<String, String> entry : model.getActionMap().entrySet())
        {
            component.createChildRegion(entry.getKey(), new ComponentActivator(
                    entry.getValue() + ".Pub", new ParametersMap()
                    .add(BaseCustomActionPubModel.EXTRACT_ID_BIND, model.getExtract().getId())
            ));
        }
    }

    public void onClickEditTemplate(IBusinessComponent component)
    {
        Model model = getModel(component);
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.CUSTOM_ORDER_TEMPLATE_EDIT, new ParametersMap()
                .add(ru.tandemservice.movestudent.component.customorder.TemplateEdit.Model.OBJECT_ID_BIND, model.getParagraph().getId())
        ));
    }

    @Override
    protected void prepareListDataSource(IBusinessComponent component)
    {
        super.prepareListDataSource(component);
        DynamicListDataSource<ModifyListStuExtract> ds = getModel(component).getDataSource();
        AbstractColumn column;
        if ((column = ds.getColumn("print")) != null)
            column.setVisible(false);
        if ((column = ds.getColumn("printPdf")) != null)
            column.setVisible(false);
    }
}
