/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.utils.system;

/**
 * @author vip_delete
 * @since 09.04.2009
 */
public class ListExtractData
{
    //список всех entity выписок из списочных приказов
    public static final String[][] EXTRACT_LIST = new String[][]{
            /* 1*/{"studentListOrder", "Списочный приказ", "-"},
            /* 2*/{"courseTransferStuListExtract", "Перевести на следующий курс", "true"},
            /* 3*/{"courseNoChangeStuListExtract", "Считать на прежнем курсе", "true"},
            /* 4*/{"excludeStuListExtract", "Отчислить", "true"},
            /* 5*/{"admitToStateExamsStuListExtract", "Допустить к Государственной итоговой аттестации", "true"},
            /* 6*/{"giveDiplStuListExtract", "О выдаче диплома и присвоении квалификации", "true"},
            /* 7*/{"giveDiplSuccessStuListExtract", "О выдаче диплома с отличием и присвоении квалификации", "true"},
            /* 8*/{"holidayStuListExtract", "О предоставлении каникул", "true"},
            /* 9*/{"graduateSuccessStuListExtract", "О выпуске студентов с отличием", "true"},
            /*10*/{"graduateStuListExtract", "О выпуске студентов", "true"},
            /*11*/{"gradExcludeStuListExtract", "Об исключении студентов, не выполнивших учебный план", "true"},
            /*12*/{"courseTransferDebtorStuListExtract", "Перевести на следующий курс студентов, имеющих задолженности", "true"},
            /*13*/{"territorialTransferStuListExtract", "Перевести студентов территориального подразделения", "false"},
            /*14*/{"excludeSingGrpSuccStuListExtract", "Отчислить студентов с выдачей дипломов с отличием", "false"},
            /*15*/{"excludeSingGrpStuListExtract", "Отчислить студентов с выдачей дипломов без отличия", "false"},
            /*16*/{"excludeSingGrpCathStuListExtract", "Отчислить студентов (кафедральный)", "false"},
            /*17*/{"admitToPassDiplomaWorkStuListExtract", "Допустить к защите дипломной работы", "false"},
            /*18*/{"splitStudentsStuListExtract", "Назначить студентам специализацию", "false"},
            /*19*/{"transferDevConditionStuListExtract", "Об изменении условий освоения", "false"},
            /*20*/{"setReviewerStuListExtract", "Назначить рецензентов", "false"},
            /*21*/{"changeScientificAdviserExtract", "О смене научных руководителей", "false"},
            /*22*/{"changeReviewerStuListExtract", "О смене рецензентов", "false"},
            /*23*/{"changeDiplomaWorkTopicStuListExtract", "Об изменении темы дипломных работ", "false"},
            /*24*/{"setDiplomaWorkTopicAndScientificAdviserStuListExtract", "Об утверждении тем дипломных работ и назначении научных руководителей", "false"},
            /*25*/{"splitStudentsGroupStuListExtract", "Назначить студентам специализацию (профиль) и перераспределить в группу", "false"},
            /*26*/{"admitToStateExamsExtStuListExtract", "Допустить к сдаче государственного экзамена", "false"},
            /*27*/{"admitToPassDiplomaWorkExtStuListExtract", "Допустить к защите выпускной квалификационной работы", "false"},
            /*28*/{"addGroupManagerStuListExtract", "Назначить старостой учебной группы", "false"},
            /*29*/{"giveDiplExtStuListExtract", "О выдаче диплома и присвоении квалификации", "false"},
            /*30*/{"giveDiplSuccessExtStuListExtract", "О выдаче диплома с отличием и присвоении квалификации", "false"},
            /*31*/{"courseTransferExtStuListExtract", "Перевести на следующий курс", "false"},
            /*32*/{"acadGrantAssignStuListExtract", "Назначить академическую стипендию", "false"},
            /*33*/{"socGrantAssignStuListExtract", "Назначить социальную стипендию", "false"},
            /*34*/{"finAidAssignStuListExtract", "Оказать материальную помощь", "false"},
            /*35*/{"splitFirstCourseStudentsGroupStuListExtract", "Распределить студентов 1 курса в группы", "false"},
            /*36*/{"grantRiseStuListExtract", "Однократно повысить государственную академическую стипендию", "false"},
            /*37*/{"territorialCourseTransferStuListExtract", "Перевести на следующий курс со сменой территориального подразделения", "false"},
            /*38*/{"splitStudentsExtStuListExtract", "Закрепить студентов за специализацией (профилем)", "false"},
            /*39*/{"acadGrantBonusAssignStuListExtract", "Назначить надбавку к государственной академической стипендии", "false"},
            /*40*/{"groupTransferExtStuListExtract", "Перевести студентов в группу", "false"},
            /*41*/{"sendPracticeInnerStuListExtract", "Направить на практику в пределах ОУ", "false"},
            /*42*/{"territorialTransferExtStuListExtract", "Перевести студентов территориального подразделения", "false"},
            /*43*/{"changeProfileStuListExtract", "О переводе со специализации на специализацию (с профиля на профиль)", "false"},
            /*44*/{"changeProfileAndGroupStuListExtract", "О переводе со специализации на специализацию (с профиля на профиль) и перераспределении в группу", "false"},
            /*45*/{"admitToPassStatateExamStuListExtract", "О допуске к государственной итоговой аттестации в форме государственного экзамена", "false"},
            /*46*/{"admitToPassQualificationWorkStuListExtract", "О допуске к государственной итоговой аттестации в форме защиты выпускной квалификационной работы", "false"},
            /*47*/{"transferProgramSubjectStuListExtract", "О переводе на направление из нового перечня", "false"},
            /*48*/{"courseTransferCustomStuListExtract", "О переводе с курса на курс", "false"}
    };
}