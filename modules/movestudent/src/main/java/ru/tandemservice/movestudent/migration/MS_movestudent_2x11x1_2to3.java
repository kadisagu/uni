/* $Id$ */
package ru.tandemservice.movestudent.migration;

import com.google.common.collect.Lists;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.shared.commonbase.utils.MigrationUtils.processor;

/**
 * @author Ekaterina Zvereva
 * @since 09.02.2017
 */
public class MS_movestudent_2x11x1_2to3 extends IndependentMigrationScript
{

    /**
     * Миграция заполняет год выпуска у студентов, имеющих проведенные приказы о выпуске и у архивных студентов
     */
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.11.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {

        ////////////////////////////////////////////////////////////////////////////////
        // set null у  активных
        tool.executeUpdate("update student_t set finishyear_p = null where id in (" +
                                   "select s.id from student_t s " +
                                   "join studentstatus_t st on st.id=s.status_id where st.active_p=? and s.archival_p=?)", true, false);

        //Поднимаем данные о последних проведенных приказах об отчислении с выдачей диплома

        final List<Object[]> rows = tool.executeQuery(
                processor(Long.class, Date.class),
                "select ex.entity_id, cast(max(ord.commitdate_p) as DATE) " +
                        "from abstractstudentextract_t ex " +
                        "join studentextracttype_t extyp on extyp.id=ex.type_id " +
                        "join student_t st on ex.entity_id=st.id " +
                        "join studentstatus_t sst on st.status_id=sst.id " +
                        "join abstractstudentparagraph_t par on ex.paragraph_id=par.id " +
                        "join abstractStudentorder_t ord on ord.id=par.order_id " +
                        "join extractStates_t state on ex.state_id=state.id " +
                        "where (st.archival_p=? or (st.archival_p=? and sst.active_p=?)) and state.code_p='6' and extyp.code_p in ('1.85', '2.2.1', '2.2.2', '2.4.1', '2.8.1', '2.8.2', '2.29.1', '2.29.2', 'unidip-2.1', '2.fefu6.1', '2.fefu6.1') " +
                        "group by ex.entity_id ", true, false, false);

        Map<Long, Integer> studentToYearMap = new HashMap<>();
        addResultToMap(studentToYearMap, rows);

        //Для проекта САФУ
        if( ApplicationRuntime.hasModule("movestudentrmc") )
        {

            final List<Object[]> rowsModular = tool.executeQuery(
                    processor(Long.class, Date.class),
                    "select ds.STUDENT_ID, cast(max(do.COMMITDATE_P) as DATE) from DOCREPRESENTSTUDENTBASE_T ds " +
                            "join student_t stud on ds.student_id=stud.id " +
                            "join studentstatus_t sst on stud.status_id=sst.id " +
                            "join REPRESENTDIPLOMANDEXCLUDE_T rep on ds.REPRESENTATION_ID = rep.id " +
                            "join DOCORDREPRESENT_T dor on rep.id=dor.REPRESENTATION_ID " +
                            "join DOCUMENTORDER_T do on dor.ORDER_ID = do.ID " +
                            "join MOVESTUDENTORDERSTATES_T st on do.STATE_ID = st.id " +
                            "where st.CODE_P='5' and (stud.archival_p=? or (stud.archival_p=? and sst.active_p=?)) " +
                            "group by ds.STUDENT_ID", true, false, false);

            addResultToMap(studentToYearMap, rowsModular);


            final List<Object[]> rowsList = tool.executeQuery(
                    processor(Long.class, Date.class),
                    "select ds.STUDENT_ID, cast(max(do.COMMITDATE_P) as DATE) from RELLISTREPRESENTSTUDENTS_T ds " +
                            "join student_t stud on ds.student_id=stud.id " +
                            "join studentstatus_t sst on stud.status_id=sst.id " +
                            "join lst_rep_diplom_and_excl_t rep on ds.REPRESENTATION_ID = rep.id " +
                            "join LISTORDLISTREPRESENT_T dor on rep.id=dor.REPRESENTATION_ID " +
                            "join LISTORDER_T do on dor.ORDER_ID = do.ID " +
                            "join MOVESTUDENTORDERSTATES_T st on do.STATE_ID = st.id " +
                            "where st.CODE_P='5' and (stud.archival_p=? or (stud.archival_p=? and sst.active_p=?)) " +
                            "group by ds.STUDENT_ID", true, false, false
            );

            addResultToMap(studentToYearMap, rowsList);
        }

        Map<Integer, List<Long>> studentsToUpdate = new HashMap<>();

        for (Map.Entry<Long, Integer> entry : studentToYearMap.entrySet())
        {
            studentsToUpdate.putIfAbsent(entry.getValue(), new ArrayList<>());
            studentsToUpdate.get(entry.getValue()).add(entry.getKey());
        }

        // устанавливаем год выпуска равный году проведения последнего приказа о выдаче диплома и отчислении
        for (Map.Entry<Integer, List<Long>> entry : studentsToUpdate.entrySet())
        {
            for (List<Long> ids : Lists.partition(entry.getValue(), 500))
            {
                PreparedStatement updateStatement = tool.prepareStatement("update student_t set finishyear_p=? where id in (" + MigrationUtils.idsToStr(ids) + ")");
                updateStatement.setInt(1, entry.getKey());
                updateStatement.executeUpdate();
            }

        }

    }

    private void addResultToMap(Map<Long, Integer> studentToYearMap, List<Object[]> rowsList)
    {
        for (Object[] item : rowsList)
        {
            Long id = (Long) item[0];
            Integer year = CoreDateUtils.getYear((Date) item[1]);
            if (studentToYearMap.get(id) == null || studentToYearMap.get(id) < year)
            {
                studentToYearMap.put(id, year);
            }
        }
    }

}