/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e34.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEditAlternative.AbstractParagraphAddEditAlternativeController;
import ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 03.10.2012
 */
@SuppressWarnings("unchecked")
public class Controller extends AbstractParagraphAddEditAlternativeController<FinAidAssignStuListExtract, IDAO, Model>
{
	@Override
    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
    {
	    ActionColumn clone = new ActionColumn("Заполнить по образцу", "clone", "onClickCopyGrantSize");
	    clone.setValidate(true);
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дополнительный статус", Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Пол", Student.person().identityCard().sex().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("status", "Состояние", Model.STATUS_KEY, SimpleFormatter.INSTANCE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false).setOrderable(false));
	    dataSource.addColumn(new SimpleColumn("Группа", Student.group().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
	    dataSource.addColumn(new SimpleColumn("Форма освоения", Student.educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("finAidSize","Размер материальной помощи").setClickable(false).setOrderable(false));
        dataSource.addColumn(clone);
	    dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteStudent", "Исключить из параграфа студента «{0}»?", Student.FIO_KEY));
        dataSource.setOrder("status", OrderDirection.asc);

        Model model = getModel(component);
        if (model.isEditForm())
        {
            IValueMapHolder finAidSizeHolder = (IValueMapHolder) dataSource.getColumn("finAidSize");
            Map<Long, Double> finAidSizeMap = (null == finAidSizeHolder ? Collections.emptyMap() : finAidSizeHolder.getValueMap());

            for (FinAidAssignStuListExtract finAidExtract: (List<FinAidAssignStuListExtract>) model.getParagraph().getExtractList())
            {
                finAidSizeMap.put(finAidExtract.getEntity().getId(), finAidExtract.getFinAidSize());
            }
        }
    }

    public void onClickCopyGrantSize(IBusinessComponent component)
    {
        Model model = getModel(component);

        final IValueMapHolder finAidSizeHolder = (IValueMapHolder) model.getDataSource().getColumn("finAidSize");
        final Map<Long, Double> finAidSizeMap = (null == finAidSizeHolder ? Collections.emptyMap() : finAidSizeHolder.getValueMap());

        Number currentFinAidSize = finAidSizeMap.get((Long) component.getListenerParameter());

        DynamicListDataSource dataSource = model.getDataSource();

        for (ViewWrapper<Student> wrapper: (List<ViewWrapper<Student>>) dataSource.getEntityList())
        {
            if (finAidSizeMap.get(wrapper.getEntity().getId()) == null)
            {
                finAidSizeMap.put(wrapper.getEntity().getId(), currentFinAidSize == null ? 0D : currentFinAidSize.doubleValue());
            }
        }

        model.getDataSource().refresh();
    }
}
