package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.SplitStudentsGroupStuListExtract;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О закреплении за специализациями (профилями) и перераспределении в группу»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SplitStudentsGroupStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.SplitStudentsGroupStuListExtract";
    public static final String ENTITY_NAME = "splitStudentsGroupStuListExtract";
    public static final int VERSION_HASH = -231320322;
    private static IEntityMeta ENTITY_META;

    public static final String L_GROUP_NEW = "groupNew";
    public static final String L_GROUP_OLD = "groupOld";
    public static final String L_EDUCATION_ORG_UNIT_NEW = "educationOrgUnitNew";
    public static final String L_EDUCATION_ORG_UNIT_OLD = "educationOrgUnitOld";

    private Group _groupNew;     // Группа
    private Group _groupOld;     // Группа
    private EducationOrgUnit _educationOrgUnitNew;     // Параметры обучения студентов по направлению подготовки (НПП)
    private EducationOrgUnit _educationOrgUnitOld;     // Параметры обучения студентов по направлению подготовки (НПП)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroupNew()
    {
        return _groupNew;
    }

    /**
     * @param groupNew Группа. Свойство не может быть null.
     */
    public void setGroupNew(Group groupNew)
    {
        dirty(_groupNew, groupNew);
        _groupNew = groupNew;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroupOld()
    {
        return _groupOld;
    }

    /**
     * @param groupOld Группа. Свойство не может быть null.
     */
    public void setGroupOld(Group groupOld)
    {
        dirty(_groupOld, groupOld);
        _groupOld = groupOld;
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitNew()
    {
        return _educationOrgUnitNew;
    }

    /**
     * @param educationOrgUnitNew Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    public void setEducationOrgUnitNew(EducationOrgUnit educationOrgUnitNew)
    {
        dirty(_educationOrgUnitNew, educationOrgUnitNew);
        _educationOrgUnitNew = educationOrgUnitNew;
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitOld()
    {
        return _educationOrgUnitOld;
    }

    /**
     * @param educationOrgUnitOld Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    public void setEducationOrgUnitOld(EducationOrgUnit educationOrgUnitOld)
    {
        dirty(_educationOrgUnitOld, educationOrgUnitOld);
        _educationOrgUnitOld = educationOrgUnitOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SplitStudentsGroupStuListExtractGen)
        {
            setGroupNew(((SplitStudentsGroupStuListExtract)another).getGroupNew());
            setGroupOld(((SplitStudentsGroupStuListExtract)another).getGroupOld());
            setEducationOrgUnitNew(((SplitStudentsGroupStuListExtract)another).getEducationOrgUnitNew());
            setEducationOrgUnitOld(((SplitStudentsGroupStuListExtract)another).getEducationOrgUnitOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SplitStudentsGroupStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SplitStudentsGroupStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new SplitStudentsGroupStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "groupNew":
                    return obj.getGroupNew();
                case "groupOld":
                    return obj.getGroupOld();
                case "educationOrgUnitNew":
                    return obj.getEducationOrgUnitNew();
                case "educationOrgUnitOld":
                    return obj.getEducationOrgUnitOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "groupNew":
                    obj.setGroupNew((Group) value);
                    return;
                case "groupOld":
                    obj.setGroupOld((Group) value);
                    return;
                case "educationOrgUnitNew":
                    obj.setEducationOrgUnitNew((EducationOrgUnit) value);
                    return;
                case "educationOrgUnitOld":
                    obj.setEducationOrgUnitOld((EducationOrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "groupNew":
                        return true;
                case "groupOld":
                        return true;
                case "educationOrgUnitNew":
                        return true;
                case "educationOrgUnitOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "groupNew":
                    return true;
                case "groupOld":
                    return true;
                case "educationOrgUnitNew":
                    return true;
                case "educationOrgUnitOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "groupNew":
                    return Group.class;
                case "groupOld":
                    return Group.class;
                case "educationOrgUnitNew":
                    return EducationOrgUnit.class;
                case "educationOrgUnitOld":
                    return EducationOrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SplitStudentsGroupStuListExtract> _dslPath = new Path<SplitStudentsGroupStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SplitStudentsGroupStuListExtract");
    }
            

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitStudentsGroupStuListExtract#getGroupNew()
     */
    public static Group.Path<Group> groupNew()
    {
        return _dslPath.groupNew();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitStudentsGroupStuListExtract#getGroupOld()
     */
    public static Group.Path<Group> groupOld()
    {
        return _dslPath.groupOld();
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitStudentsGroupStuListExtract#getEducationOrgUnitNew()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
    {
        return _dslPath.educationOrgUnitNew();
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitStudentsGroupStuListExtract#getEducationOrgUnitOld()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
    {
        return _dslPath.educationOrgUnitOld();
    }

    public static class Path<E extends SplitStudentsGroupStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Group.Path<Group> _groupNew;
        private Group.Path<Group> _groupOld;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitNew;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitStudentsGroupStuListExtract#getGroupNew()
     */
        public Group.Path<Group> groupNew()
        {
            if(_groupNew == null )
                _groupNew = new Group.Path<Group>(L_GROUP_NEW, this);
            return _groupNew;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitStudentsGroupStuListExtract#getGroupOld()
     */
        public Group.Path<Group> groupOld()
        {
            if(_groupOld == null )
                _groupOld = new Group.Path<Group>(L_GROUP_OLD, this);
            return _groupOld;
        }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitStudentsGroupStuListExtract#getEducationOrgUnitNew()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
        {
            if(_educationOrgUnitNew == null )
                _educationOrgUnitNew = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_NEW, this);
            return _educationOrgUnitNew;
        }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitStudentsGroupStuListExtract#getEducationOrgUnitOld()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
        {
            if(_educationOrgUnitOld == null )
                _educationOrgUnitOld = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_OLD, this);
            return _educationOrgUnitOld;
        }

        public Class getEntityClass()
        {
            return SplitStudentsGroupStuListExtract.class;
        }

        public String getEntityName()
        {
            return "splitStudentsGroupStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
