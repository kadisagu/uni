/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e29.utils;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 20.09.2012
 */
public class GiveDiplExtParagraphPartWrapper implements Comparable<GiveDiplExtParagraphPartWrapper>
{
    private EducationLevelsHighSchool _eduLevelsHighSchool;
    private final ListStudentExtract _firstExtract;

    public GiveDiplExtParagraphPartWrapper(EducationLevelsHighSchool eduLevelsHighSchool, ListStudentExtract firstExtract)
    {
        _eduLevelsHighSchool = eduLevelsHighSchool;
        _firstExtract = firstExtract;
    }

    private GiveDiplExtParagraphWrapper _paragraphWrapper;
    private List<Person> _personList = new ArrayList<Person>();


    public EducationLevelsHighSchool getEduLevelsHighSchool()
    {
        return _eduLevelsHighSchool;
    }

    public void setEduLevelsHighSchool(EducationLevelsHighSchool eduLevelsHighSchool)
    {
        _eduLevelsHighSchool = eduLevelsHighSchool;
    }

    public GiveDiplExtParagraphWrapper getParagraphWrapper()
    {
        return _paragraphWrapper;
    }

    public void setParagraphWrapper(GiveDiplExtParagraphWrapper paragraphWrapper)
    {
        _paragraphWrapper = paragraphWrapper;
    }

    public List<Person> getPersonList()
    {
        return _personList;
    }

    public void setPersonList(List<Person> personList)
    {
        _personList = personList;
    }


    public ListStudentExtract getFirstExtract() {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof GiveDiplExtParagraphPartWrapper))
            return false;

        GiveDiplExtParagraphPartWrapper that = (GiveDiplExtParagraphPartWrapper) o;

        return _eduLevelsHighSchool == null ? (that.getEduLevelsHighSchool() == null) : (that.getEduLevelsHighSchool() != null && _eduLevelsHighSchool.equals(that.getEduLevelsHighSchool()));
    }

    @Override
    public int hashCode()
    {
        return _eduLevelsHighSchool != null ? _eduLevelsHighSchool.hashCode() : 0;
    }

    @Override
    public int compareTo(GiveDiplExtParagraphPartWrapper o)
    {
        if (_eduLevelsHighSchool == null || o.getEduLevelsHighSchool() == null)
            return _eduLevelsHighSchool == null ? -1 : 1;

        return _eduLevelsHighSchool.getTitle().compareTo(o.getEduLevelsHighSchool().getTitle());
    }
}