/* $Id: extractPrint.vm 23792 2012-08-10 10:47:17Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e98;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AttestationPracticStuExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.employee.Student;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 08.02.2013
 */
public class AttestationPracticStuExtractPrint implements IPrintFormCreator<AttestationPracticStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, AttestationPracticStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        injectPracticeLabels(modifier, extract.getPracticeType(), extract.getCourse(), extract.getPracticeDurationStr(), extract.getEntity());

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    public static void injectPracticeLabels(RtfInjectModifier modifier, String practiceType, Course course, String practiceDurationStr, Student student)
    {
        String prType = MoveStudentDaoFacade.getMoveStudentDao().getDeclinableFullPracticeKind(practiceType, GrammaCase.PREPOSITIONAL);

        modifier.put("practiceType_P", prType != null ? prType : practiceType)
                .put("practiceCourse", course.getTitle())
                .put("duration", practiceDurationStr + " нед.");

        StructureEducationLevels level = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
        modifier.put("strEdLvlShort", level.getGenitiveCaseSimpleShortTitle());

        if (level.isHighGos2())
        {
            modifier.put("edLvl", " согласно ГОС ВПО");
        }
        else if (level.isHighGos3())
        {
            modifier.put("edLvl", " согласно ФГОС");
        }
        else if (level.isMiddleGos2())
        {
            modifier.put("edLvl", " согласно ГОС CПО");
        }
        else if (level.isMiddleGos3())
        {
            modifier.put("edLvl", " согласно ФГОС СПО");
        }
        else
        {
            modifier.put("edLvl", "");
        }

        modifier.put("sEnd", student.getPerson().isMale() ? "его" : "ую");
    }
}