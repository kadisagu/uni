/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeCourse.AddEdit;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.IBaseCustomActionAddEditDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeCourseAction;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public interface IDAO extends IBaseCustomActionAddEditDAO<ChangeCourseAction, Model>
{
}