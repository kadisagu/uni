package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.StudentExtractTextRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь выписки и текста выписки (приказы по студентам)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentExtractTextRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.StudentExtractTextRelation";
    public static final String ENTITY_NAME = "studentExtractTextRelation";
    public static final int VERSION_HASH = 2058779686;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String P_TEXT = "text";
    public static final String P_CODE = "code";

    private AbstractStudentExtract _extract;     // Выписка (приказ по студентам)
    private byte[] _text;     // Сохраненная печатная форма
    private int _code;     // Код печатной формы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка (приказ по студентам). Свойство не может быть null.
     */
    @NotNull
    public AbstractStudentExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка (приказ по студентам). Свойство не может быть null.
     */
    public void setExtract(AbstractStudentExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     */
    @NotNull
    public byte[] getText()
    {
        return _text;
    }

    /**
     * @param text Сохраненная печатная форма. Свойство не может быть null.
     */
    public void setText(byte[] text)
    {
        dirty(_text, text);
        _text = text;
    }

    /**
     * @return Код печатной формы. Свойство не может быть null.
     */
    @NotNull
    public int getCode()
    {
        return _code;
    }

    /**
     * @param code Код печатной формы. Свойство не может быть null.
     */
    public void setCode(int code)
    {
        dirty(_code, code);
        _code = code;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentExtractTextRelationGen)
        {
            setExtract(((StudentExtractTextRelation)another).getExtract());
            setText(((StudentExtractTextRelation)another).getText());
            setCode(((StudentExtractTextRelation)another).getCode());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentExtractTextRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentExtractTextRelation.class;
        }

        public T newInstance()
        {
            return (T) new StudentExtractTextRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "text":
                    return obj.getText();
                case "code":
                    return obj.getCode();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((AbstractStudentExtract) value);
                    return;
                case "text":
                    obj.setText((byte[]) value);
                    return;
                case "code":
                    obj.setCode((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "text":
                        return true;
                case "code":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "text":
                    return true;
                case "code":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return AbstractStudentExtract.class;
                case "text":
                    return byte[].class;
                case "code":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentExtractTextRelation> _dslPath = new Path<StudentExtractTextRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentExtractTextRelation");
    }
            

    /**
     * @return Выписка (приказ по студентам). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentExtractTextRelation#getExtract()
     */
    public static AbstractStudentExtract.Path<AbstractStudentExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentExtractTextRelation#getText()
     */
    public static PropertyPath<byte[]> text()
    {
        return _dslPath.text();
    }

    /**
     * @return Код печатной формы. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentExtractTextRelation#getCode()
     */
    public static PropertyPath<Integer> code()
    {
        return _dslPath.code();
    }

    public static class Path<E extends StudentExtractTextRelation> extends EntityPath<E>
    {
        private AbstractStudentExtract.Path<AbstractStudentExtract> _extract;
        private PropertyPath<byte[]> _text;
        private PropertyPath<Integer> _code;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка (приказ по студентам). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentExtractTextRelation#getExtract()
     */
        public AbstractStudentExtract.Path<AbstractStudentExtract> extract()
        {
            if(_extract == null )
                _extract = new AbstractStudentExtract.Path<AbstractStudentExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentExtractTextRelation#getText()
     */
        public PropertyPath<byte[]> text()
        {
            if(_text == null )
                _text = new PropertyPath<byte[]>(StudentExtractTextRelationGen.P_TEXT, this);
            return _text;
        }

    /**
     * @return Код печатной формы. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentExtractTextRelation#getCode()
     */
        public PropertyPath<Integer> code()
        {
            if(_code == null )
                _code = new PropertyPath<Integer>(StudentExtractTextRelationGen.P_CODE, this);
            return _code;
        }

        public Class getEntityClass()
        {
            return StudentExtractTextRelation.class;
        }

        public String getEntityName()
        {
            return "studentExtractTextRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
