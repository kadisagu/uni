package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об аттестации практик студентов, прошедших стажировку
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AttestationPracticWithProbationStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract";
    public static final String ENTITY_NAME = "attestationPracticWithProbationStuExtract";
    public static final int VERSION_HASH = -766915962;
    private static IEntityMeta ENTITY_META;

    public static final String P_PRACTICE_TYPE = "practiceType";
    public static final String P_PRACTICE_DURATION_STR = "practiceDurationStr";
    public static final String P_PRACTICE_BEGIN_DATE = "practiceBeginDate";
    public static final String P_PRACTICE_END_DATE = "practiceEndDate";
    public static final String P_PRACTICE_PLACE = "practicePlace";
    public static final String P_PRACTICE_ORDER_NUM = "practiceOrderNum";
    public static final String P_PRACTICE_ORDER_DATE = "practiceOrderDate";
    public static final String L_PRACTICE_COURSE = "practiceCourse";

    private String _practiceType;     // Тип практики
    private String _practiceDurationStr;     // Объем(недель)
    private Date _practiceBeginDate;     // Дата начала
    private Date _practiceEndDate;     // Дата окончания
    private String _practicePlace;     // Место прохождения стажировки
    private String _practiceOrderNum;     // Номер приказа(о прохождении практики)
    private Date _practiceOrderDate;     // Дата приказа(о прохождении практики)
    private Course _practiceCourse;     // Курс прохождения практики

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип практики. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPracticeType()
    {
        return _practiceType;
    }

    /**
     * @param practiceType Тип практики. Свойство не может быть null.
     */
    public void setPracticeType(String practiceType)
    {
        dirty(_practiceType, practiceType);
        _practiceType = practiceType;
    }

    /**
     * @return Объем(недель). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPracticeDurationStr()
    {
        return _practiceDurationStr;
    }

    /**
     * @param practiceDurationStr Объем(недель). Свойство не может быть null.
     */
    public void setPracticeDurationStr(String practiceDurationStr)
    {
        dirty(_practiceDurationStr, practiceDurationStr);
        _practiceDurationStr = practiceDurationStr;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getPracticeBeginDate()
    {
        return _practiceBeginDate;
    }

    /**
     * @param practiceBeginDate Дата начала. Свойство не может быть null.
     */
    public void setPracticeBeginDate(Date practiceBeginDate)
    {
        dirty(_practiceBeginDate, practiceBeginDate);
        _practiceBeginDate = practiceBeginDate;
    }

    /**
     * @return Дата окончания. Свойство не может быть null.
     */
    @NotNull
    public Date getPracticeEndDate()
    {
        return _practiceEndDate;
    }

    /**
     * @param practiceEndDate Дата окончания. Свойство не может быть null.
     */
    public void setPracticeEndDate(Date practiceEndDate)
    {
        dirty(_practiceEndDate, practiceEndDate);
        _practiceEndDate = practiceEndDate;
    }

    /**
     * @return Место прохождения стажировки. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPracticePlace()
    {
        return _practicePlace;
    }

    /**
     * @param practicePlace Место прохождения стажировки. Свойство не может быть null.
     */
    public void setPracticePlace(String practicePlace)
    {
        dirty(_practicePlace, practicePlace);
        _practicePlace = practicePlace;
    }

    /**
     * @return Номер приказа(о прохождении практики). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPracticeOrderNum()
    {
        return _practiceOrderNum;
    }

    /**
     * @param practiceOrderNum Номер приказа(о прохождении практики). Свойство не может быть null.
     */
    public void setPracticeOrderNum(String practiceOrderNum)
    {
        dirty(_practiceOrderNum, practiceOrderNum);
        _practiceOrderNum = practiceOrderNum;
    }

    /**
     * @return Дата приказа(о прохождении практики). Свойство не может быть null.
     */
    @NotNull
    public Date getPracticeOrderDate()
    {
        return _practiceOrderDate;
    }

    /**
     * @param practiceOrderDate Дата приказа(о прохождении практики). Свойство не может быть null.
     */
    public void setPracticeOrderDate(Date practiceOrderDate)
    {
        dirty(_practiceOrderDate, practiceOrderDate);
        _practiceOrderDate = practiceOrderDate;
    }

    /**
     * @return Курс прохождения практики.
     */
    public Course getPracticeCourse()
    {
        return _practiceCourse;
    }

    /**
     * @param practiceCourse Курс прохождения практики.
     */
    public void setPracticeCourse(Course practiceCourse)
    {
        dirty(_practiceCourse, practiceCourse);
        _practiceCourse = practiceCourse;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof AttestationPracticWithProbationStuExtractGen)
        {
            setPracticeType(((AttestationPracticWithProbationStuExtract)another).getPracticeType());
            setPracticeDurationStr(((AttestationPracticWithProbationStuExtract)another).getPracticeDurationStr());
            setPracticeBeginDate(((AttestationPracticWithProbationStuExtract)another).getPracticeBeginDate());
            setPracticeEndDate(((AttestationPracticWithProbationStuExtract)another).getPracticeEndDate());
            setPracticePlace(((AttestationPracticWithProbationStuExtract)another).getPracticePlace());
            setPracticeOrderNum(((AttestationPracticWithProbationStuExtract)another).getPracticeOrderNum());
            setPracticeOrderDate(((AttestationPracticWithProbationStuExtract)another).getPracticeOrderDate());
            setPracticeCourse(((AttestationPracticWithProbationStuExtract)another).getPracticeCourse());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AttestationPracticWithProbationStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AttestationPracticWithProbationStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new AttestationPracticWithProbationStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "practiceType":
                    return obj.getPracticeType();
                case "practiceDurationStr":
                    return obj.getPracticeDurationStr();
                case "practiceBeginDate":
                    return obj.getPracticeBeginDate();
                case "practiceEndDate":
                    return obj.getPracticeEndDate();
                case "practicePlace":
                    return obj.getPracticePlace();
                case "practiceOrderNum":
                    return obj.getPracticeOrderNum();
                case "practiceOrderDate":
                    return obj.getPracticeOrderDate();
                case "practiceCourse":
                    return obj.getPracticeCourse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "practiceType":
                    obj.setPracticeType((String) value);
                    return;
                case "practiceDurationStr":
                    obj.setPracticeDurationStr((String) value);
                    return;
                case "practiceBeginDate":
                    obj.setPracticeBeginDate((Date) value);
                    return;
                case "practiceEndDate":
                    obj.setPracticeEndDate((Date) value);
                    return;
                case "practicePlace":
                    obj.setPracticePlace((String) value);
                    return;
                case "practiceOrderNum":
                    obj.setPracticeOrderNum((String) value);
                    return;
                case "practiceOrderDate":
                    obj.setPracticeOrderDate((Date) value);
                    return;
                case "practiceCourse":
                    obj.setPracticeCourse((Course) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "practiceType":
                        return true;
                case "practiceDurationStr":
                        return true;
                case "practiceBeginDate":
                        return true;
                case "practiceEndDate":
                        return true;
                case "practicePlace":
                        return true;
                case "practiceOrderNum":
                        return true;
                case "practiceOrderDate":
                        return true;
                case "practiceCourse":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "practiceType":
                    return true;
                case "practiceDurationStr":
                    return true;
                case "practiceBeginDate":
                    return true;
                case "practiceEndDate":
                    return true;
                case "practicePlace":
                    return true;
                case "practiceOrderNum":
                    return true;
                case "practiceOrderDate":
                    return true;
                case "practiceCourse":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "practiceType":
                    return String.class;
                case "practiceDurationStr":
                    return String.class;
                case "practiceBeginDate":
                    return Date.class;
                case "practiceEndDate":
                    return Date.class;
                case "practicePlace":
                    return String.class;
                case "practiceOrderNum":
                    return String.class;
                case "practiceOrderDate":
                    return Date.class;
                case "practiceCourse":
                    return Course.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AttestationPracticWithProbationStuExtract> _dslPath = new Path<AttestationPracticWithProbationStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AttestationPracticWithProbationStuExtract");
    }
            

    /**
     * @return Тип практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract#getPracticeType()
     */
    public static PropertyPath<String> practiceType()
    {
        return _dslPath.practiceType();
    }

    /**
     * @return Объем(недель). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract#getPracticeDurationStr()
     */
    public static PropertyPath<String> practiceDurationStr()
    {
        return _dslPath.practiceDurationStr();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract#getPracticeBeginDate()
     */
    public static PropertyPath<Date> practiceBeginDate()
    {
        return _dslPath.practiceBeginDate();
    }

    /**
     * @return Дата окончания. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract#getPracticeEndDate()
     */
    public static PropertyPath<Date> practiceEndDate()
    {
        return _dslPath.practiceEndDate();
    }

    /**
     * @return Место прохождения стажировки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract#getPracticePlace()
     */
    public static PropertyPath<String> practicePlace()
    {
        return _dslPath.practicePlace();
    }

    /**
     * @return Номер приказа(о прохождении практики). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract#getPracticeOrderNum()
     */
    public static PropertyPath<String> practiceOrderNum()
    {
        return _dslPath.practiceOrderNum();
    }

    /**
     * @return Дата приказа(о прохождении практики). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract#getPracticeOrderDate()
     */
    public static PropertyPath<Date> practiceOrderDate()
    {
        return _dslPath.practiceOrderDate();
    }

    /**
     * @return Курс прохождения практики.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract#getPracticeCourse()
     */
    public static Course.Path<Course> practiceCourse()
    {
        return _dslPath.practiceCourse();
    }

    public static class Path<E extends AttestationPracticWithProbationStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<String> _practiceType;
        private PropertyPath<String> _practiceDurationStr;
        private PropertyPath<Date> _practiceBeginDate;
        private PropertyPath<Date> _practiceEndDate;
        private PropertyPath<String> _practicePlace;
        private PropertyPath<String> _practiceOrderNum;
        private PropertyPath<Date> _practiceOrderDate;
        private Course.Path<Course> _practiceCourse;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract#getPracticeType()
     */
        public PropertyPath<String> practiceType()
        {
            if(_practiceType == null )
                _practiceType = new PropertyPath<String>(AttestationPracticWithProbationStuExtractGen.P_PRACTICE_TYPE, this);
            return _practiceType;
        }

    /**
     * @return Объем(недель). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract#getPracticeDurationStr()
     */
        public PropertyPath<String> practiceDurationStr()
        {
            if(_practiceDurationStr == null )
                _practiceDurationStr = new PropertyPath<String>(AttestationPracticWithProbationStuExtractGen.P_PRACTICE_DURATION_STR, this);
            return _practiceDurationStr;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract#getPracticeBeginDate()
     */
        public PropertyPath<Date> practiceBeginDate()
        {
            if(_practiceBeginDate == null )
                _practiceBeginDate = new PropertyPath<Date>(AttestationPracticWithProbationStuExtractGen.P_PRACTICE_BEGIN_DATE, this);
            return _practiceBeginDate;
        }

    /**
     * @return Дата окончания. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract#getPracticeEndDate()
     */
        public PropertyPath<Date> practiceEndDate()
        {
            if(_practiceEndDate == null )
                _practiceEndDate = new PropertyPath<Date>(AttestationPracticWithProbationStuExtractGen.P_PRACTICE_END_DATE, this);
            return _practiceEndDate;
        }

    /**
     * @return Место прохождения стажировки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract#getPracticePlace()
     */
        public PropertyPath<String> practicePlace()
        {
            if(_practicePlace == null )
                _practicePlace = new PropertyPath<String>(AttestationPracticWithProbationStuExtractGen.P_PRACTICE_PLACE, this);
            return _practicePlace;
        }

    /**
     * @return Номер приказа(о прохождении практики). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract#getPracticeOrderNum()
     */
        public PropertyPath<String> practiceOrderNum()
        {
            if(_practiceOrderNum == null )
                _practiceOrderNum = new PropertyPath<String>(AttestationPracticWithProbationStuExtractGen.P_PRACTICE_ORDER_NUM, this);
            return _practiceOrderNum;
        }

    /**
     * @return Дата приказа(о прохождении практики). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract#getPracticeOrderDate()
     */
        public PropertyPath<Date> practiceOrderDate()
        {
            if(_practiceOrderDate == null )
                _practiceOrderDate = new PropertyPath<Date>(AttestationPracticWithProbationStuExtractGen.P_PRACTICE_ORDER_DATE, this);
            return _practiceOrderDate;
        }

    /**
     * @return Курс прохождения практики.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract#getPracticeCourse()
     */
        public Course.Path<Course> practiceCourse()
        {
            if(_practiceCourse == null )
                _practiceCourse = new Course.Path<Course>(L_PRACTICE_COURSE, this);
            return _practiceCourse;
        }

        public Class getEntityClass()
        {
            return AttestationPracticWithProbationStuExtract.class;
        }

        public String getEntityName()
        {
            return "attestationPracticWithProbationStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
