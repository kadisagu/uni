package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.TransferDevCondStuExtractGen;

import java.util.Date;

/**
 * О переводе на другие условия освоения
 */
public class TransferDevCondStuExtract extends TransferDevCondStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}