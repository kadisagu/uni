/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeGroup.Pub;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.BaseCustomActionPubController;
import ru.tandemservice.movestudent.entity.custom.ChangeGroupAction;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class Controller extends BaseCustomActionPubController<ChangeGroupAction, IDAO, Model>
{
}