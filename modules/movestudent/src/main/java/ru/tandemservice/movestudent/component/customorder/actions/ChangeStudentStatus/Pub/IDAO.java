/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeStudentStatus.Pub;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.IBaseCustomActionPubDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeStudentStatusAction;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public interface IDAO extends IBaseCustomActionPubDAO<ChangeStudentStatusAction, Model>
{
}