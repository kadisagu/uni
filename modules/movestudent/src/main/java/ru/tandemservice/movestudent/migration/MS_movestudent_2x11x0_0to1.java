/* $Id$ */
package ru.tandemservice.movestudent.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Ekaterina Zvereva
 * @since 15.11.2016
 */
public class MS_movestudent_2x11x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.11.0")
                };
    }


    @Override
    public void run(DBTool tool) throws Exception
    {
        //удаление остатков ДПО из приказов
        final String sql = "(select id from studentextracttype_t t where t.code_p in ('dpo2.1.1', 'dpo2.1'))";

        tool.executeUpdate("delete from movestudenttemplate_t where type_id in " + sql);
        tool.executeUpdate("delete from stulistordertobasicrelation_t where order_id in (select o.id from studentlistorder_t o where o.type_id in " + sql + ")");
        tool.executeUpdate("delete from studentlistorder_t where type_id in " + sql);
        tool.executeUpdate("delete from studentextracttypetogroup_t where type_id in " + sql);
        tool.executeUpdate("delete from studentreasontotyperel_t where second_id in " + sql);
        tool.executeUpdate("delete from codesmsrforstudentorders_t where extractType_id in" + sql);
        tool.executeUpdate("delete from studentextracttype_t where id in " + sql);
    }
}