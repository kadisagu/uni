/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e90;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.WeekendChildCareOutStuExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 26.12.2012
 */
public class WeekendChildCareOutStuExtractDao extends UniBaseDao implements IExtractComponentDao<WeekendChildCareOutStuExtract>
{
    public void doCommit(WeekendChildCareOutStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        extract.setStudentStatusOld(extract.getEntity().getStatus());
        extract.getEntity().setStatus(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE));

        Student student = extract.getEntity();

        //еще раз сохраняем старые значения студента
        extract.setGroupOld(student.getGroup());

        //присваиваем студенту новые значения
        student.setGroup(extract.getGroupNew());
    }

    public void doRollback(WeekendChildCareOutStuExtract extract, Map parameters)
    {
        Student student = extract.getEntity();

        student.setGroup(extract.getGroupOld());

        extract.getEntity().setStatus(extract.getStudentStatusOld());
    }
}