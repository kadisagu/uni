/* $Id$ */
package ru.tandemservice.movestudent.component.catalog.studentOrderReasons.StudentOrderReasonsAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;

/**
 * @author AutoGenerator
 *         Created on 05.12.2012
 */
public interface IDAO extends IDefaultCatalogAddEditDAO<StudentOrderReasons, Model>
{
}