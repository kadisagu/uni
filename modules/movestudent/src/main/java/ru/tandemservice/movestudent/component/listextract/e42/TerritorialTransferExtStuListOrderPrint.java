/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e42;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.e42.utils.TerritorialTransferExtParagraphWrapper;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 28.08.2013
 */
public class TerritorialTransferExtStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<TerritorialTransferExtStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        List<TerritorialTransferExtParagraphWrapper> preparedParagraphsStructure = prepareParagraphsStructure(extracts);

        if (extracts.get(0).isChangeEduPlan())
        {
            RtfString changeEduPlan = new RtfString();
            changeEduPlan.append("Разницу в учебных планах ликвидировать в срок до ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extracts.get(0).getChangeEduPlanDeadlineDate())).append(".").par().par();
            injectModifier.put("changeEduPlan", changeEduPlan);
        }
        else
        {
            injectModifier.put("changeEduPlan", "");
        }

        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, preparedParagraphsStructure);

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }

    protected List<TerritorialTransferExtParagraphWrapper> prepareParagraphsStructure(List<TerritorialTransferExtStuListExtract> extracts)
    {
        int ind;
        List<TerritorialTransferExtParagraphWrapper> paragraphWrapperList = new ArrayList<>();

        for (TerritorialTransferExtStuListExtract extract : extracts)
        {
            Student student = extract.getEntity();
            Person person = extract.getEntity().getPerson();

            TerritorialTransferExtParagraphWrapper paragraphWrapper = new TerritorialTransferExtParagraphWrapper(
                    student.getCourse(),
                    student.getStudentCategory(),
                    extract.getGroupOld(),
                    extract.getGroupNew(),
                    student.getCompensationType(),
                    extract.getCompensationTypeNew(),
                    student.getEducationOrgUnit(),
                    extract.getEducationOrgUnitNew(),
                    student.getEducationOrgUnit().getDevelopForm(),
                    extract);

            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else paragraphWrapper = paragraphWrapperList.get(ind);

            paragraphWrapper.getPersonList().add(person);
        }
        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<TerritorialTransferExtParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            int parNumber = 0;
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (TerritorialTransferExtParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.TRANSFER_TERRITORIAL_VARIANT_2_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphWrapper.getFirstExtract();
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(firstExtract.getParagraph(), firstExtract);

                CompensationType compensationTypeOld = paragraphWrapper.getCompensationTypeOld();
                CompensationType compensationTypeNew = paragraphWrapper.getCompensationTypeNew();

                EducationOrgUnit educationOrgUnitOld = paragraphWrapper.getEducationOrgUnitOld();
                EducationOrgUnit educationOrgUnitNew = paragraphWrapper.getEducationOrgUnitNew();

                paragraphInjectModifier.put("parNumber", paragraphWrappers.size() == 1 ? "" : String.valueOf(++parNumber) + ". ");
                paragraphInjectModifier.put("course", paragraphWrapper.getCourse().getTitle());
                paragraphInjectModifier.put("intoGroupOld", " группы " + paragraphWrapper.getGroupOld().getTitle());
                paragraphInjectModifier.put("intoGroupNew", " в группу " + paragraphWrapper.getGroupNew().getTitle());

                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, educationOrgUnitOld.getDevelopCondition(), educationOrgUnitOld.getDevelopTech(), educationOrgUnitOld.getDevelopPeriod(), CommonListOrderPrint.getEducationBaseText(paragraphWrapper.getGroupOld()), "fefuShortFastExtendedOptionalTextOld");
                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, educationOrgUnitNew.getDevelopCondition(), educationOrgUnitNew.getDevelopTech(), educationOrgUnitNew.getDevelopPeriod(), CommonListOrderPrint.getEducationBaseText(paragraphWrapper.getGroupNew()), "fefuShortFastExtendedOptionalTextNew");
                CommonExtractPrint.initFefuGroup(paragraphInjectModifier, "intoFefuGroupOld", paragraphWrapper.getGroupOld(), educationOrgUnitOld.getDevelopForm(), " группы ");
                CommonExtractPrint.initFefuGroup(paragraphInjectModifier, "intoFefuGroupNew", paragraphWrapper.getGroupNew(), educationOrgUnitNew.getDevelopForm(), " в группу ");
                CommonListExtractPrint.injectStudentsCategory(paragraphInjectModifier, paragraphWrapper.getStudentCategory());

                CommonExtractPrint.initCompensationType(paragraphInjectModifier, "Old", compensationTypeOld, true, false);
                paragraphInjectModifier.put("compensationTypeStrOld", compensationTypeOld != null ? compensationTypeOld.isBudget() ? "на бюджетной основе" : "по договору, предусматривающему оплату обучения" : "");
                CommonExtractPrint.initCompensationType(paragraphInjectModifier, "New", compensationTypeNew, true, false);
                paragraphInjectModifier.put("compensationTypeStrNew", compensationTypeNew != null ? compensationTypeNew.isBudget() ? "на бюджетной основе" : "по договору, предусматривающему оплату обучения" : "");

                String code = paragraphWrapper.getDevelopForm().getCode();
                switch (code)
                {
                    case DevelopFormCodes.FULL_TIME_FORM:
                        paragraphInjectModifier.put("developForm_DF", "очной");
                        break;
                    case DevelopFormCodes.CORESP_FORM:
                        paragraphInjectModifier.put("developForm_DF", "заочной");
                        break;
                    case DevelopFormCodes.PART_TIME_FORM:
                        paragraphInjectModifier.put("developForm_DF", "очно-заочной");
                        break;
                    case DevelopFormCodes.EXTERNAL_FORM:
                        paragraphInjectModifier.put("developForm_DF", "экстернату");
                        break;
                    case DevelopFormCodes.APPLICANT_FORM:
                        paragraphInjectModifier.put("developForm_DF", "самостоятельному обучению и итоговой аттестации");
                        break;
                }

                paragraphInjectModifier.put("formativeOrgUnitOld_P", educationOrgUnitOld.getFormativeOrgUnit().getPrepositionalCaseTitle() != null ? educationOrgUnitOld.getFormativeOrgUnit().getPrepositionalCaseTitle() : educationOrgUnitOld.getFormativeOrgUnit().getPrintTitle());
                paragraphInjectModifier.put("formativeOrgUnitNew_P", educationOrgUnitNew.getFormativeOrgUnit().getPrepositionalCaseTitle() != null ? educationOrgUnitNew.getFormativeOrgUnit().getPrepositionalCaseTitle() : educationOrgUnitNew.getFormativeOrgUnit().getPrintTitle());

                paragraphInjectModifier.put("territorialOrgUnitOld_P", educationOrgUnitOld.getTerritorialOrgUnit().getTerritorialTitle().equals(TopOrgUnit.getInstance().getTerritorialTitle()) ? "" : " (" + educationOrgUnitOld.getTerritorialOrgUnit().getPrintTitle() + ")");
                paragraphInjectModifier.put("territorialOrgUnitNew_P", educationOrgUnitNew.getTerritorialOrgUnit().getTerritorialTitle().equals(TopOrgUnit.getInstance().getTerritorialTitle()) ? "" : " (" + educationOrgUnitNew.getTerritorialOrgUnit().getPrintTitle() + ")");

                StructureEducationLevels levelOld = educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel().getLevelType();

                if (educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel().isProfileOrSpecialization())
                {
                    levelOld = (EducationOrgUnitUtil.getParentLevel(educationOrgUnitOld.getEducationLevelHighSchool())).getLevelType();
                }

                String educationStrNew = levelOld.isSpecialty() ? "указанной специальности" : levelOld.isMaster() ? "указанному магистерскому направлению подготовки" : levelOld.isBachelor() ? "указанному направлению подготовки" : "";
                String fefuEducationStrNew = levelOld.isSpecialty() ? "указанной специальности" : levelOld.isMaster() || levelOld.isBachelor() ? "указанному направлению подготовки" : "";


                // если нет профиля или специализации у старого НП
                if (!educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel().isProfileOrSpecialization())
                {
                    // если есть профиль или специализации у нового НП
                    if (educationOrgUnitNew.getEducationLevelHighSchool().getEducationLevel().isProfileOrSpecialization())
                    {
                        educationStrNew += (levelOld.isSpecialty() ? " и специализации" : levelOld.isMaster() ? " и магистерской программе" : levelOld.isBachelor() ? " и профилю" : "") + " «" + educationOrgUnitNew.getEducationLevelHighSchool().getTitle() + "»";
                        fefuEducationStrNew += (levelOld.isSpecialty() ? ", специализации" : levelOld.isMaster() ? ", магистерской программе" : levelOld.isBachelor() ? ", профилю" : "") + " «" + educationOrgUnitNew.getEducationLevelHighSchool().getTitle() + "»";
                    }
                }
                else
                {
                    EducationLevels parentLevelOld = educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel();

                    // если новое НП является направлением или специализацией (когда новое НП не выбрано)
                    if (!educationOrgUnitNew.getEducationLevelHighSchool().getEducationLevel().isProfileOrSpecialization())
                    {
                        parentLevelOld = educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel().getParentLevel();
                    }

                    if (!parentLevelOld.equals(educationOrgUnitNew.getEducationLevelHighSchool().getEducationLevel()))
                    {
                        educationStrNew += (levelOld.isSpecialty() ? " и специализации" : levelOld.isMaster() ? " и магистерской программе" : levelOld.isBachelor() ? " и профилю" : "") + " «" + educationOrgUnitNew.getEducationLevelHighSchool().getTitle() + "»";
                        fefuEducationStrNew += (levelOld.isSpecialty() ? ", специализации" : levelOld.isMaster() ? ", магистерской программе" : levelOld.isBachelor() ? ", профилю" : "") + " «" + educationOrgUnitNew.getEducationLevelHighSchool().getTitle() + "»";
                    }
                    else
                    {
                        educationStrNew += levelOld.isSpecialty() ? " и специализации" : levelOld.isMaster() ? " и магистерской программе" : levelOld.isBachelor() ? " и профилю" : "";
	                    fefuEducationStrNew += parentLevelOld.isProfileOrSpecialization()? (levelOld.isSpecialty() ? ", специализации" : levelOld.isMaster() ? ", магистерской программе" : levelOld.isBachelor() ? ", профилю" : "") : "";
                    }
                }

                paragraphInjectModifier.put("educationStrNew_D", educationStrNew);
                paragraphInjectModifier.put("fefuEducationStrNew_D", fefuEducationStrNew);

                CommonExtractPrint.initEducationType(paragraphInjectModifier, educationOrgUnitOld, "Old"); //для usue
                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel(), "Old");// для остального и fefu

                // Получаем список студентов
                List<Person> personList = paragraphWrapper.getPersonList();
                personList.sort(Person.FULL_FIO_AND_ID_COMPARATOR);
                RtfString rtfString = new RtfString();

                int j = 0;
                for (; j < personList.size() - 1; j++)
                {
                    rtfString.append(IRtfData.TAB).append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio()).par();
                }

                rtfString.append(IRtfData.TAB).append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio());
                paragraphInjectModifier.put("STUDENT_LIST", rtfString);
                customModifier.listParagraphModifier(paragraphInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                paragraphInjectModifier.modify(paragraph);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraph);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }
            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}