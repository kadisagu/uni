package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IEnrollmentExtract;
import ru.tandemservice.movestudent.entity.gen.RestorationAdmitToDiplomaDefExtStuExtractGen;

import java.util.Date;

/**
 * О восстановлении и допуске к защите ВКР (повторная защита)
 */
public class RestorationAdmitToDiplomaDefExtStuExtract extends RestorationAdmitToDiplomaDefExtStuExtractGen implements IEnrollmentExtract
{
    @Override
    public Date getEntranceDate()
    {
        return getRestorationDate();
    }
}