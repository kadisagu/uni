/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e87;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.GrantRiseStuExtract;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 20.11.2012
 */
public class GrantRiseStuExtractPrint implements IPrintFormCreator<GrantRiseStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, GrantRiseStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        String month = CommonBaseDateUtil.getMonthNameDeclined(extract.getMonth(), GrammaCase.PREPOSITIONAL);

        modifier.put("grantRiseAmount", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGrantRiseAmount()));
        modifier.put("year", String.valueOf(extract.getYear()));
        modifier.put("month_P", month);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}