package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.WeekendChildCareOutStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О выходе из отпуска по уходу за ребенком
 */
public class WeekendChildCareOutStuExtract extends WeekendChildCareOutStuExtractGen implements ITransferExtract
{
    @Override
    public Date getBeginDate()
    {
        return getWeekendOutDate();
    }

    @Override
    public Date getTransferOrOrderDate()
    {
        return getWeekendOutDate();
    }
}