package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.ProlongWeekendStuExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О продлении академического отпуска
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ProlongWeekendStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ProlongWeekendStuExtract";
    public static final String ENTITY_NAME = "prolongWeekendStuExtract";
    public static final int VERSION_HASH = 1504322546;
    private static IEntityMeta ENTITY_META;

    public static final String P_PROLONG_DATE_FROM = "prolongDateFrom";
    public static final String P_PROLONG_DATE_TO = "prolongDateTo";
    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";
    public static final String P_PREV_END_DATE = "prevEndDate";

    private Date _prolongDateFrom;     // Продлить отпуск с
    private Date _prolongDateTo;     // Продлить отпуск по
    private StudentStatus _studentStatusOld;     // Предыдущее состояние студента
    private Date _prevEndDate;     // Дата окончания отпуска по предыдущему приказу

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Продлить отпуск с. Свойство не может быть null.
     */
    @NotNull
    public Date getProlongDateFrom()
    {
        return _prolongDateFrom;
    }

    /**
     * @param prolongDateFrom Продлить отпуск с. Свойство не может быть null.
     */
    public void setProlongDateFrom(Date prolongDateFrom)
    {
        dirty(_prolongDateFrom, prolongDateFrom);
        _prolongDateFrom = prolongDateFrom;
    }

    /**
     * @return Продлить отпуск по. Свойство не может быть null.
     */
    @NotNull
    public Date getProlongDateTo()
    {
        return _prolongDateTo;
    }

    /**
     * @param prolongDateTo Продлить отпуск по. Свойство не может быть null.
     */
    public void setProlongDateTo(Date prolongDateTo)
    {
        dirty(_prolongDateTo, prolongDateTo);
        _prolongDateTo = prolongDateTo;
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Предыдущее состояние студента. Свойство не может быть null.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    /**
     * @return Дата окончания отпуска по предыдущему приказу.
     */
    public Date getPrevEndDate()
    {
        return _prevEndDate;
    }

    /**
     * @param prevEndDate Дата окончания отпуска по предыдущему приказу.
     */
    public void setPrevEndDate(Date prevEndDate)
    {
        dirty(_prevEndDate, prevEndDate);
        _prevEndDate = prevEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ProlongWeekendStuExtractGen)
        {
            setProlongDateFrom(((ProlongWeekendStuExtract)another).getProlongDateFrom());
            setProlongDateTo(((ProlongWeekendStuExtract)another).getProlongDateTo());
            setStudentStatusOld(((ProlongWeekendStuExtract)another).getStudentStatusOld());
            setPrevEndDate(((ProlongWeekendStuExtract)another).getPrevEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ProlongWeekendStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ProlongWeekendStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new ProlongWeekendStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "prolongDateFrom":
                    return obj.getProlongDateFrom();
                case "prolongDateTo":
                    return obj.getProlongDateTo();
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
                case "prevEndDate":
                    return obj.getPrevEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "prolongDateFrom":
                    obj.setProlongDateFrom((Date) value);
                    return;
                case "prolongDateTo":
                    obj.setProlongDateTo((Date) value);
                    return;
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
                case "prevEndDate":
                    obj.setPrevEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "prolongDateFrom":
                        return true;
                case "prolongDateTo":
                        return true;
                case "studentStatusOld":
                        return true;
                case "prevEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "prolongDateFrom":
                    return true;
                case "prolongDateTo":
                    return true;
                case "studentStatusOld":
                    return true;
                case "prevEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "prolongDateFrom":
                    return Date.class;
                case "prolongDateTo":
                    return Date.class;
                case "studentStatusOld":
                    return StudentStatus.class;
                case "prevEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ProlongWeekendStuExtract> _dslPath = new Path<ProlongWeekendStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ProlongWeekendStuExtract");
    }
            

    /**
     * @return Продлить отпуск с. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ProlongWeekendStuExtract#getProlongDateFrom()
     */
    public static PropertyPath<Date> prolongDateFrom()
    {
        return _dslPath.prolongDateFrom();
    }

    /**
     * @return Продлить отпуск по. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ProlongWeekendStuExtract#getProlongDateTo()
     */
    public static PropertyPath<Date> prolongDateTo()
    {
        return _dslPath.prolongDateTo();
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ProlongWeekendStuExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    /**
     * @return Дата окончания отпуска по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.ProlongWeekendStuExtract#getPrevEndDate()
     */
    public static PropertyPath<Date> prevEndDate()
    {
        return _dslPath.prevEndDate();
    }

    public static class Path<E extends ProlongWeekendStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _prolongDateFrom;
        private PropertyPath<Date> _prolongDateTo;
        private StudentStatus.Path<StudentStatus> _studentStatusOld;
        private PropertyPath<Date> _prevEndDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Продлить отпуск с. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ProlongWeekendStuExtract#getProlongDateFrom()
     */
        public PropertyPath<Date> prolongDateFrom()
        {
            if(_prolongDateFrom == null )
                _prolongDateFrom = new PropertyPath<Date>(ProlongWeekendStuExtractGen.P_PROLONG_DATE_FROM, this);
            return _prolongDateFrom;
        }

    /**
     * @return Продлить отпуск по. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ProlongWeekendStuExtract#getProlongDateTo()
     */
        public PropertyPath<Date> prolongDateTo()
        {
            if(_prolongDateTo == null )
                _prolongDateTo = new PropertyPath<Date>(ProlongWeekendStuExtractGen.P_PROLONG_DATE_TO, this);
            return _prolongDateTo;
        }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ProlongWeekendStuExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

    /**
     * @return Дата окончания отпуска по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.ProlongWeekendStuExtract#getPrevEndDate()
     */
        public PropertyPath<Date> prevEndDate()
        {
            if(_prevEndDate == null )
                _prevEndDate = new PropertyPath<Date>(ProlongWeekendStuExtractGen.P_PREV_END_DATE, this);
            return _prevEndDate;
        }

        public Class getEntityClass()
        {
            return ProlongWeekendStuExtract.class;
        }

        public String getEntityName()
        {
            return "prolongWeekendStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
