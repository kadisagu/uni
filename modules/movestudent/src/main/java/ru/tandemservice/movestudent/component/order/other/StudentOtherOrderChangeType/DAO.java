/* $Id$ */
package ru.tandemservice.movestudent.component.order.other.StudentOtherOrderChangeType;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.SimpleListResultBuilder;
import ru.tandemservice.movestudent.component.commons.ExtractTypeSelectModel;
import ru.tandemservice.movestudent.component.commons.OrderCategory;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.OtherStudentExtract;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDao;

import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 18.04.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setExtract(getNotNull(OtherStudentExtract.class, model.getExtractId()));

        model.setExtractType(model.getExtract().getType());
        model.setParentExtractType(model.getExtractType().getParent());
        model.setOtherTypesThreeLevel(MoveStudentDaoFacade.getMoveStudentDao().hasThirdLevelOtherOrders());

        if (model.isOtherTypesThreeLevel())
        {
            model.setExtractParentTypeListModel(new ExtractTypeSelectModel(OrderCategory.OTHER));
            model.setExtractTypeListModel(new CommonSingleSelectModel()
            {
                @Override
                protected IListResultBuilder createBuilder(String filter, Object o)
                {
                    if (model.getParentExtractType() == null)
                        return new SimpleListResultBuilder<>(Collections.emptyList());

                    DQLSelectBuilder builder = new DQLSelectBuilder()
                            .fromEntity(StudentExtractType.class, "et").column("et")
                            .where(eq(property(StudentExtractType.active().fromAlias("et")), value(Boolean.TRUE)))
                            .where(eq(property(StudentExtractType.parent().fromAlias("et")), value(model.getParentExtractType())));

                    if (StringUtils.isNotEmpty(filter))
                        builder.where(likeUpper(property(StudentExtractType.title().fromAlias("et")), value(CoreStringUtils.escapeLike(filter, true))));

                    builder.order(property(StudentExtractType.title().fromAlias("et")));

                    if (o != null)
                        builder.where(eq(property("et.id"), commonValue(o, PropertyType.LONG)));

                    return new DQLListResultBuilder(builder);
                }
            });
        }
        else
        {
            model.setExtractTypeListModel(new ExtractTypeSelectModel(OrderCategory.OTHER));
        }
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setType(model.getExtractType());
        update(model.getExtract());
    }
}