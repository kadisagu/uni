/* $Id:$ */
package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IAssignPaymentExtract;
import ru.tandemservice.movestudent.entity.gen.AcadGrantBonusAssignStuExtractGen;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О назначении надбавки к государственной академической стипендии
 */
public class AcadGrantBonusAssignStuExtract extends AcadGrantBonusAssignStuExtractGen implements IAssignPaymentExtract
{
    @Override
    public BigDecimal getPaymentAmount()
    {
        return new BigDecimal(getGrantBonusSize());
    }

    @Override
    public Date getPaymentBeginDate()
    {
        return getBeginDate();
    }

    @Override
    public Date getPaymentEndDate()
    {
        return getEndDate();
    }
}