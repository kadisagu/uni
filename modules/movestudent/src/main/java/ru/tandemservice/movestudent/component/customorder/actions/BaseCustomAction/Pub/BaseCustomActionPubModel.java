/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.movestudent.component.customorder.IStudentCustomOrderExtract;
import ru.tandemservice.unimove.entity.ICustomOrderAction;

@Input({
               @Bind(key = BaseCustomActionPubModel.EXTRACT_ID_BIND, binding = BaseCustomActionPubModel.EXTRACT_ID_BIND, required = true)
       })
public class BaseCustomActionPubModel<T extends ICustomOrderAction>
{
    public static final String EXTRACT_ID_BIND = "extractId";

    private Long _extractId;
    private IStudentCustomOrderExtract _extract;
    private T _customAction;

    public T getCustomAction()
    {
        return _customAction;
    }

    public void setCustomAction(T customAction)
    {
        _customAction = customAction;
    }

    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        _extractId = extractId;
    }

    public IStudentCustomOrderExtract getExtract()
    {
        return _extract;
    }

    public void setExtract(IStudentCustomOrderExtract extract)
    {
        _extract = extract;
    }
}