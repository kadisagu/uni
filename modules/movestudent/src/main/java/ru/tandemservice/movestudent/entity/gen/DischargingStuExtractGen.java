package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.DischargingStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об отстранении от занятий
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DischargingStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.DischargingStuExtract";
    public static final String ENTITY_NAME = "dischargingStuExtract";
    public static final int VERSION_HASH = -220935340;
    private static IEntityMeta ENTITY_META;

    public static final String P_DISCHARGING_DATE = "dischargingDate";
    public static final String P_BEFORE_LIQUIDATION = "beforeLiquidation";

    private Date _dischargingDate;     // Дата отстранения
    private String _beforeLiquidation;     // До ликвидации

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата отстранения.
     */
    public Date getDischargingDate()
    {
        return _dischargingDate;
    }

    /**
     * @param dischargingDate Дата отстранения.
     */
    public void setDischargingDate(Date dischargingDate)
    {
        dirty(_dischargingDate, dischargingDate);
        _dischargingDate = dischargingDate;
    }

    /**
     * @return До ликвидации. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getBeforeLiquidation()
    {
        return _beforeLiquidation;
    }

    /**
     * @param beforeLiquidation До ликвидации. Свойство не может быть null.
     */
    public void setBeforeLiquidation(String beforeLiquidation)
    {
        dirty(_beforeLiquidation, beforeLiquidation);
        _beforeLiquidation = beforeLiquidation;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof DischargingStuExtractGen)
        {
            setDischargingDate(((DischargingStuExtract)another).getDischargingDate());
            setBeforeLiquidation(((DischargingStuExtract)another).getBeforeLiquidation());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DischargingStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DischargingStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new DischargingStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "dischargingDate":
                    return obj.getDischargingDate();
                case "beforeLiquidation":
                    return obj.getBeforeLiquidation();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "dischargingDate":
                    obj.setDischargingDate((Date) value);
                    return;
                case "beforeLiquidation":
                    obj.setBeforeLiquidation((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dischargingDate":
                        return true;
                case "beforeLiquidation":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dischargingDate":
                    return true;
                case "beforeLiquidation":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "dischargingDate":
                    return Date.class;
                case "beforeLiquidation":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DischargingStuExtract> _dslPath = new Path<DischargingStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DischargingStuExtract");
    }
            

    /**
     * @return Дата отстранения.
     * @see ru.tandemservice.movestudent.entity.DischargingStuExtract#getDischargingDate()
     */
    public static PropertyPath<Date> dischargingDate()
    {
        return _dslPath.dischargingDate();
    }

    /**
     * @return До ликвидации. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.DischargingStuExtract#getBeforeLiquidation()
     */
    public static PropertyPath<String> beforeLiquidation()
    {
        return _dslPath.beforeLiquidation();
    }

    public static class Path<E extends DischargingStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _dischargingDate;
        private PropertyPath<String> _beforeLiquidation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата отстранения.
     * @see ru.tandemservice.movestudent.entity.DischargingStuExtract#getDischargingDate()
     */
        public PropertyPath<Date> dischargingDate()
        {
            if(_dischargingDate == null )
                _dischargingDate = new PropertyPath<Date>(DischargingStuExtractGen.P_DISCHARGING_DATE, this);
            return _dischargingDate;
        }

    /**
     * @return До ликвидации. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.DischargingStuExtract#getBeforeLiquidation()
     */
        public PropertyPath<String> beforeLiquidation()
        {
            if(_beforeLiquidation == null )
                _beforeLiquidation = new PropertyPath<String>(DischargingStuExtractGen.P_BEFORE_LIQUIDATION, this);
            return _beforeLiquidation;
        }

        public Class getEntityClass()
        {
            return DischargingStuExtract.class;
        }

        public String getEntityName()
        {
            return "dischargingStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
