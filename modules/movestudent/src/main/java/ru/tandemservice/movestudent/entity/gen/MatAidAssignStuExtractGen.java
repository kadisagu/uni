package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudent.entity.MatAidAssignStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об оказании материальной помощи
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MatAidAssignStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.MatAidAssignStuExtract";
    public static final String ENTITY_NAME = "matAidAssignStuExtract";
    public static final int VERSION_HASH = 1649247755;
    private static IEntityMeta ENTITY_META;

    public static final String P_GRANT_SIZE = "grantSize";
    public static final String P_MONTH = "month";
    public static final String P_YEAR = "year";
    public static final String P_PROTOCOL_NUMBER = "protocolNumber";
    public static final String P_RESPONSIBLE_FOR_PAYMENTS_STR = "responsibleForPaymentsStr";
    public static final String P_RESPONSIBLE_FOR_AGREEMENT_STR = "responsibleForAgreementStr";
    public static final String L_RESPONSIBLE_FOR_PAYMENTS = "responsibleForPayments";
    public static final String L_RESPONSIBLE_FOR_AGREEMENT = "responsibleForAgreement";
    public static final String P_MATCHING_DATE = "matchingDate";

    private double _grantSize;     // Размер материальной помощи
    private int _month;     // Месяц оказания материальной помощи
    private int _year;     // Год оказания материальной помощи
    private String _protocolNumber;     // Номер протокола
    private String _responsibleForPaymentsStr;     // Ответственный за начисление выплат (печать)
    private String _responsibleForAgreementStr;     // Ответственный за согласование начислений выплат (печать)
    private EmployeePost _responsibleForPayments;     // Ответственный за начисление выплат
    private EmployeePost _responsibleForAgreement;     // Ответственный за согласование начислений выплат
    private Date _matchingDate;     // Дата согласования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Размер материальной помощи. Свойство не может быть null.
     */
    @NotNull
    public double getGrantSize()
    {
        return _grantSize;
    }

    /**
     * @param grantSize Размер материальной помощи. Свойство не может быть null.
     */
    public void setGrantSize(double grantSize)
    {
        dirty(_grantSize, grantSize);
        _grantSize = grantSize;
    }

    /**
     * @return Месяц оказания материальной помощи. Свойство не может быть null.
     */
    @NotNull
    public int getMonth()
    {
        return _month;
    }

    /**
     * @param month Месяц оказания материальной помощи. Свойство не может быть null.
     */
    public void setMonth(int month)
    {
        dirty(_month, month);
        _month = month;
    }

    /**
     * @return Год оказания материальной помощи. Свойство не может быть null.
     */
    @NotNull
    public int getYear()
    {
        return _year;
    }

    /**
     * @param year Год оказания материальной помощи. Свойство не может быть null.
     */
    public void setYear(int year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Номер протокола.
     */
    @Length(max=255)
    public String getProtocolNumber()
    {
        return _protocolNumber;
    }

    /**
     * @param protocolNumber Номер протокола.
     */
    public void setProtocolNumber(String protocolNumber)
    {
        dirty(_protocolNumber, protocolNumber);
        _protocolNumber = protocolNumber;
    }

    /**
     * @return Ответственный за начисление выплат (печать).
     */
    @Length(max=255)
    public String getResponsibleForPaymentsStr()
    {
        return _responsibleForPaymentsStr;
    }

    /**
     * @param responsibleForPaymentsStr Ответственный за начисление выплат (печать).
     */
    public void setResponsibleForPaymentsStr(String responsibleForPaymentsStr)
    {
        dirty(_responsibleForPaymentsStr, responsibleForPaymentsStr);
        _responsibleForPaymentsStr = responsibleForPaymentsStr;
    }

    /**
     * @return Ответственный за согласование начислений выплат (печать).
     */
    @Length(max=255)
    public String getResponsibleForAgreementStr()
    {
        return _responsibleForAgreementStr;
    }

    /**
     * @param responsibleForAgreementStr Ответственный за согласование начислений выплат (печать).
     */
    public void setResponsibleForAgreementStr(String responsibleForAgreementStr)
    {
        dirty(_responsibleForAgreementStr, responsibleForAgreementStr);
        _responsibleForAgreementStr = responsibleForAgreementStr;
    }

    /**
     * @return Ответственный за начисление выплат.
     */
    public EmployeePost getResponsibleForPayments()
    {
        return _responsibleForPayments;
    }

    /**
     * @param responsibleForPayments Ответственный за начисление выплат.
     */
    public void setResponsibleForPayments(EmployeePost responsibleForPayments)
    {
        dirty(_responsibleForPayments, responsibleForPayments);
        _responsibleForPayments = responsibleForPayments;
    }

    /**
     * @return Ответственный за согласование начислений выплат.
     */
    public EmployeePost getResponsibleForAgreement()
    {
        return _responsibleForAgreement;
    }

    /**
     * @param responsibleForAgreement Ответственный за согласование начислений выплат.
     */
    public void setResponsibleForAgreement(EmployeePost responsibleForAgreement)
    {
        dirty(_responsibleForAgreement, responsibleForAgreement);
        _responsibleForAgreement = responsibleForAgreement;
    }

    /**
     * @return Дата согласования.
     */
    public Date getMatchingDate()
    {
        return _matchingDate;
    }

    /**
     * @param matchingDate Дата согласования.
     */
    public void setMatchingDate(Date matchingDate)
    {
        dirty(_matchingDate, matchingDate);
        _matchingDate = matchingDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof MatAidAssignStuExtractGen)
        {
            setGrantSize(((MatAidAssignStuExtract)another).getGrantSize());
            setMonth(((MatAidAssignStuExtract)another).getMonth());
            setYear(((MatAidAssignStuExtract)another).getYear());
            setProtocolNumber(((MatAidAssignStuExtract)another).getProtocolNumber());
            setResponsibleForPaymentsStr(((MatAidAssignStuExtract)another).getResponsibleForPaymentsStr());
            setResponsibleForAgreementStr(((MatAidAssignStuExtract)another).getResponsibleForAgreementStr());
            setResponsibleForPayments(((MatAidAssignStuExtract)another).getResponsibleForPayments());
            setResponsibleForAgreement(((MatAidAssignStuExtract)another).getResponsibleForAgreement());
            setMatchingDate(((MatAidAssignStuExtract)another).getMatchingDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MatAidAssignStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MatAidAssignStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new MatAidAssignStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "grantSize":
                    return obj.getGrantSize();
                case "month":
                    return obj.getMonth();
                case "year":
                    return obj.getYear();
                case "protocolNumber":
                    return obj.getProtocolNumber();
                case "responsibleForPaymentsStr":
                    return obj.getResponsibleForPaymentsStr();
                case "responsibleForAgreementStr":
                    return obj.getResponsibleForAgreementStr();
                case "responsibleForPayments":
                    return obj.getResponsibleForPayments();
                case "responsibleForAgreement":
                    return obj.getResponsibleForAgreement();
                case "matchingDate":
                    return obj.getMatchingDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "grantSize":
                    obj.setGrantSize((Double) value);
                    return;
                case "month":
                    obj.setMonth((Integer) value);
                    return;
                case "year":
                    obj.setYear((Integer) value);
                    return;
                case "protocolNumber":
                    obj.setProtocolNumber((String) value);
                    return;
                case "responsibleForPaymentsStr":
                    obj.setResponsibleForPaymentsStr((String) value);
                    return;
                case "responsibleForAgreementStr":
                    obj.setResponsibleForAgreementStr((String) value);
                    return;
                case "responsibleForPayments":
                    obj.setResponsibleForPayments((EmployeePost) value);
                    return;
                case "responsibleForAgreement":
                    obj.setResponsibleForAgreement((EmployeePost) value);
                    return;
                case "matchingDate":
                    obj.setMatchingDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "grantSize":
                        return true;
                case "month":
                        return true;
                case "year":
                        return true;
                case "protocolNumber":
                        return true;
                case "responsibleForPaymentsStr":
                        return true;
                case "responsibleForAgreementStr":
                        return true;
                case "responsibleForPayments":
                        return true;
                case "responsibleForAgreement":
                        return true;
                case "matchingDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "grantSize":
                    return true;
                case "month":
                    return true;
                case "year":
                    return true;
                case "protocolNumber":
                    return true;
                case "responsibleForPaymentsStr":
                    return true;
                case "responsibleForAgreementStr":
                    return true;
                case "responsibleForPayments":
                    return true;
                case "responsibleForAgreement":
                    return true;
                case "matchingDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "grantSize":
                    return Double.class;
                case "month":
                    return Integer.class;
                case "year":
                    return Integer.class;
                case "protocolNumber":
                    return String.class;
                case "responsibleForPaymentsStr":
                    return String.class;
                case "responsibleForAgreementStr":
                    return String.class;
                case "responsibleForPayments":
                    return EmployeePost.class;
                case "responsibleForAgreement":
                    return EmployeePost.class;
                case "matchingDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MatAidAssignStuExtract> _dslPath = new Path<MatAidAssignStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MatAidAssignStuExtract");
    }
            

    /**
     * @return Размер материальной помощи. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.MatAidAssignStuExtract#getGrantSize()
     */
    public static PropertyPath<Double> grantSize()
    {
        return _dslPath.grantSize();
    }

    /**
     * @return Месяц оказания материальной помощи. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.MatAidAssignStuExtract#getMonth()
     */
    public static PropertyPath<Integer> month()
    {
        return _dslPath.month();
    }

    /**
     * @return Год оказания материальной помощи. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.MatAidAssignStuExtract#getYear()
     */
    public static PropertyPath<Integer> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Номер протокола.
     * @see ru.tandemservice.movestudent.entity.MatAidAssignStuExtract#getProtocolNumber()
     */
    public static PropertyPath<String> protocolNumber()
    {
        return _dslPath.protocolNumber();
    }

    /**
     * @return Ответственный за начисление выплат (печать).
     * @see ru.tandemservice.movestudent.entity.MatAidAssignStuExtract#getResponsibleForPaymentsStr()
     */
    public static PropertyPath<String> responsibleForPaymentsStr()
    {
        return _dslPath.responsibleForPaymentsStr();
    }

    /**
     * @return Ответственный за согласование начислений выплат (печать).
     * @see ru.tandemservice.movestudent.entity.MatAidAssignStuExtract#getResponsibleForAgreementStr()
     */
    public static PropertyPath<String> responsibleForAgreementStr()
    {
        return _dslPath.responsibleForAgreementStr();
    }

    /**
     * @return Ответственный за начисление выплат.
     * @see ru.tandemservice.movestudent.entity.MatAidAssignStuExtract#getResponsibleForPayments()
     */
    public static EmployeePost.Path<EmployeePost> responsibleForPayments()
    {
        return _dslPath.responsibleForPayments();
    }

    /**
     * @return Ответственный за согласование начислений выплат.
     * @see ru.tandemservice.movestudent.entity.MatAidAssignStuExtract#getResponsibleForAgreement()
     */
    public static EmployeePost.Path<EmployeePost> responsibleForAgreement()
    {
        return _dslPath.responsibleForAgreement();
    }

    /**
     * @return Дата согласования.
     * @see ru.tandemservice.movestudent.entity.MatAidAssignStuExtract#getMatchingDate()
     */
    public static PropertyPath<Date> matchingDate()
    {
        return _dslPath.matchingDate();
    }

    public static class Path<E extends MatAidAssignStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Double> _grantSize;
        private PropertyPath<Integer> _month;
        private PropertyPath<Integer> _year;
        private PropertyPath<String> _protocolNumber;
        private PropertyPath<String> _responsibleForPaymentsStr;
        private PropertyPath<String> _responsibleForAgreementStr;
        private EmployeePost.Path<EmployeePost> _responsibleForPayments;
        private EmployeePost.Path<EmployeePost> _responsibleForAgreement;
        private PropertyPath<Date> _matchingDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Размер материальной помощи. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.MatAidAssignStuExtract#getGrantSize()
     */
        public PropertyPath<Double> grantSize()
        {
            if(_grantSize == null )
                _grantSize = new PropertyPath<Double>(MatAidAssignStuExtractGen.P_GRANT_SIZE, this);
            return _grantSize;
        }

    /**
     * @return Месяц оказания материальной помощи. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.MatAidAssignStuExtract#getMonth()
     */
        public PropertyPath<Integer> month()
        {
            if(_month == null )
                _month = new PropertyPath<Integer>(MatAidAssignStuExtractGen.P_MONTH, this);
            return _month;
        }

    /**
     * @return Год оказания материальной помощи. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.MatAidAssignStuExtract#getYear()
     */
        public PropertyPath<Integer> year()
        {
            if(_year == null )
                _year = new PropertyPath<Integer>(MatAidAssignStuExtractGen.P_YEAR, this);
            return _year;
        }

    /**
     * @return Номер протокола.
     * @see ru.tandemservice.movestudent.entity.MatAidAssignStuExtract#getProtocolNumber()
     */
        public PropertyPath<String> protocolNumber()
        {
            if(_protocolNumber == null )
                _protocolNumber = new PropertyPath<String>(MatAidAssignStuExtractGen.P_PROTOCOL_NUMBER, this);
            return _protocolNumber;
        }

    /**
     * @return Ответственный за начисление выплат (печать).
     * @see ru.tandemservice.movestudent.entity.MatAidAssignStuExtract#getResponsibleForPaymentsStr()
     */
        public PropertyPath<String> responsibleForPaymentsStr()
        {
            if(_responsibleForPaymentsStr == null )
                _responsibleForPaymentsStr = new PropertyPath<String>(MatAidAssignStuExtractGen.P_RESPONSIBLE_FOR_PAYMENTS_STR, this);
            return _responsibleForPaymentsStr;
        }

    /**
     * @return Ответственный за согласование начислений выплат (печать).
     * @see ru.tandemservice.movestudent.entity.MatAidAssignStuExtract#getResponsibleForAgreementStr()
     */
        public PropertyPath<String> responsibleForAgreementStr()
        {
            if(_responsibleForAgreementStr == null )
                _responsibleForAgreementStr = new PropertyPath<String>(MatAidAssignStuExtractGen.P_RESPONSIBLE_FOR_AGREEMENT_STR, this);
            return _responsibleForAgreementStr;
        }

    /**
     * @return Ответственный за начисление выплат.
     * @see ru.tandemservice.movestudent.entity.MatAidAssignStuExtract#getResponsibleForPayments()
     */
        public EmployeePost.Path<EmployeePost> responsibleForPayments()
        {
            if(_responsibleForPayments == null )
                _responsibleForPayments = new EmployeePost.Path<EmployeePost>(L_RESPONSIBLE_FOR_PAYMENTS, this);
            return _responsibleForPayments;
        }

    /**
     * @return Ответственный за согласование начислений выплат.
     * @see ru.tandemservice.movestudent.entity.MatAidAssignStuExtract#getResponsibleForAgreement()
     */
        public EmployeePost.Path<EmployeePost> responsibleForAgreement()
        {
            if(_responsibleForAgreement == null )
                _responsibleForAgreement = new EmployeePost.Path<EmployeePost>(L_RESPONSIBLE_FOR_AGREEMENT, this);
            return _responsibleForAgreement;
        }

    /**
     * @return Дата согласования.
     * @see ru.tandemservice.movestudent.entity.MatAidAssignStuExtract#getMatchingDate()
     */
        public PropertyPath<Date> matchingDate()
        {
            if(_matchingDate == null )
                _matchingDate = new PropertyPath<Date>(MatAidAssignStuExtractGen.P_MATCHING_DATE, this);
            return _matchingDate;
        }

        public Class getEntityClass()
        {
            return MatAidAssignStuExtract.class;
        }

        public String getEntityName()
        {
            return "matAidAssignStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
