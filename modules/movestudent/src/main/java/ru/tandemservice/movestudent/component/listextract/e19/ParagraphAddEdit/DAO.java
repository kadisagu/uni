/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e19.ParagraphAddEdit;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 23.03.2010
 */
public class DAO extends AbstractListParagraphAddEditDAO<TransferDevConditionStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));

        final TransferDevConditionStuListExtract firstExtract = model.getFirstExtract();
        if (null != firstExtract)
        {
            if (null != model.getParagraphId())
            {
                model.setCourse(firstExtract.getCourse());
                model.setGroup(firstExtract.getGroup());
            }
            model.setRecordDate(firstExtract.getRecordDate());
            model.setRecordNumber(firstExtract.getRecordNumber());
            model.setDevelopConditionNew(firstExtract.getEducationOrgUnitNew().getDevelopCondition());
            model.setDevelopPeriodNew(firstExtract.getEducationOrgUnitNew().getDevelopPeriod());
            model.setTransferDate(firstExtract.getTransferDate());
            model.setGroupChangeEducationOrgUnit(firstExtract.isGroupChangeEducationOrgUnit());
        }


        model.setDevelopConditionsListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getGroup() == null) return ListResult.getEmpty();
                if (!model.isParagraphOnlyOneInTheOrder() && null != firstExtract)
                {
                    List<DevelopCondition> result = new ArrayList<>();
                    result.add(firstExtract.getEducationOrgUnitNew().getDevelopCondition());
                    return new ListResult<>(result);
                }

                MQBuilder builder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou", new String[]{EducationOrgUnit.L_DEVELOP_CONDITION});
                builder.add(MQExpression.eq("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getGroup().getEducationOrgUnit().getEducationLevelHighSchool()));
                builder.add(MQExpression.eq("eou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getGroup().getEducationOrgUnit().getFormativeOrgUnit()));
                builder.add(MQExpression.eq("eou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getGroup().getEducationOrgUnit().getTerritorialOrgUnit()));
                builder.add(MQExpression.eq("eou", EducationOrgUnit.P_USED, Boolean.TRUE));
                builder.add(MQExpression.eq("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.P_ALLOW_STUDENTS, Boolean.TRUE));
                builder.addOrder("eou", EducationOrgUnit.L_DEVELOP_CONDITION + "." + DevelopCondition.P_CODE);
                builder.setNeedDistinct(true);
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });

        model.setDevelopPeriodsListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getGroup() == null || model.getDevelopConditionNew() == null) return ListResult.getEmpty();
                if (!model.isParagraphOnlyOneInTheOrder() && null != firstExtract)
                {
                    List<DevelopPeriod> result = new ArrayList<>();
                    result.add(firstExtract.getEducationOrgUnitNew().getDevelopPeriod());
                    return new ListResult<>(result);
                }

                MQBuilder builder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou", new String[]{EducationOrgUnit.L_DEVELOP_PERIOD});
                builder.add(MQExpression.eq("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getGroup().getEducationOrgUnit().getEducationLevelHighSchool()));
                builder.add(MQExpression.eq("eou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getGroup().getEducationOrgUnit().getFormativeOrgUnit()));
                builder.add(MQExpression.eq("eou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getGroup().getEducationOrgUnit().getTerritorialOrgUnit()));
                builder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopConditionNew()));
                builder.add(MQExpression.eq("eou", EducationOrgUnit.P_USED, Boolean.TRUE));
                builder.add(MQExpression.eq("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.P_ALLOW_STUDENTS, Boolean.TRUE));
                builder.addOrder("eou", EducationOrgUnit.L_DEVELOP_PERIOD + "." + DevelopPeriod.P_CODE);
                builder.setNeedDistinct(true);
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
    }

    @Override
    protected TransferDevConditionStuListExtract createNewInstance(Model model)
    {
        return new TransferDevConditionStuListExtract();
    }

    @Override
    protected void fillExtract(TransferDevConditionStuListExtract extract, Student student, Model model)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        MQBuilder builder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou");
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, student.getEducationOrgUnit().getFormativeOrgUnit()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, student.getEducationOrgUnit().getTerritorialOrgUnit()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, student.getEducationOrgUnit().getEducationLevelHighSchool()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_FORM, student.getEducationOrgUnit().getDevelopForm()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_TECH, student.getEducationOrgUnit().getDevelopTech()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopConditionNew()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_PERIOD, model.getDevelopPeriodNew()));
        List<EducationOrgUnit> acceptableEduOrgUnitsList = builder.getResultList(getSession());

        if (acceptableEduOrgUnitsList.isEmpty())
            errCollector.add("Студент " + student.getPerson().getFullFio() + " не может быть переведен на указанное условие освоения, поскольку для направления подготовки «" + student.getEducationOrgUnit().getTitle() + "», на котором числится студент, не существует...");
        else extract.setEducationOrgUnitNew(acceptableEduOrgUnitsList.get(0));

        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setRecordDate(model.getRecordDate());
        extract.setRecordNumber(model.getRecordNumber());
        extract.setEducationOrgUnitOld(student.getEducationOrgUnit());
        extract.setGroupChangeEducationOrgUnit(model.isGroupChangeEducationOrgUnit());
        extract.setTransferDate(model.getTransferDate());

        if (model.isGroupChangeEducationOrgUnit())
        {
            Group group = model.getGroup();
            MQBuilder grpBuilder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou");
            grpBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, group.getEducationOrgUnit().getFormativeOrgUnit()));
            grpBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, group.getEducationOrgUnit().getTerritorialOrgUnit()));
            grpBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, group.getEducationOrgUnit().getEducationLevelHighSchool()));
            grpBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_FORM, group.getEducationOrgUnit().getDevelopForm()));
            grpBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_TECH, group.getEducationOrgUnit().getDevelopTech()));
            grpBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopConditionNew()));
            grpBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_PERIOD, model.getDevelopPeriodNew()));
            List<EducationOrgUnit> acceptableGrpEduOrgUnitsList = grpBuilder.getResultList(getSession());

            if (acceptableGrpEduOrgUnitsList.isEmpty())
                errCollector.add("Группа " + group.getTitle() + " не может быть переведена на указанное условие освоения, поскольку для направления подготовки «" + group.getEducationOrgUnit().getTitle() + "», на котором числится группа, не существует...");
            else
            {
                extract.setGroupEducationOrgUnitOld(group.getEducationOrgUnit());
                extract.setGroupEducationOrgUnitNew(acceptableGrpEduOrgUnitsList.get(0));
            }
        }
        else
        {
            extract.setGroupEducationOrgUnitNew(null);
            extract.setGroupEducationOrgUnitOld(null);
        }
    }
}