/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e19;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.IStudentListParagraphPrintFormatter;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author ListExtractComponentGenerator
 * @since 23.03.2010
 */
public class TransferDevConditionStuListExtractPrint implements IPrintFormCreator<TransferDevConditionStuListExtract>, IListParagraphPrintFormCreator<TransferDevConditionStuListExtract>, IStudentListParagraphPrintFormatter
{
    @Override
    public String formatSingleStudent(Student student, int extractNumber)
    {
        StringBuilder buffer = new StringBuilder();
        buffer.append("\\par ").append(extractNumber).append(".  ");
        buffer.append(student.getPerson().getFullFio());
        return buffer.toString();
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, TransferDevConditionStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);

        modifier.put("developCondition_A", getDevelopConditionFullStr_A(extract.getEducationOrgUnitNew().getDevelopCondition()));
        modifier.put("orgUnitShortTitle", extract.getParagraph().getOrder().getOrgUnit().getShortTitle());
        modifier.put("compensationType_Alt", extract.getEntity().getCompensationType().isBudget() ? "на бюджетной основе" : "по договору с оплатой за обучение");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, TransferDevConditionStuListExtract firstExtract)
    {
        modifier.put("developCondition_A", getDevelopConditionFullStr_A(firstExtract.getEducationOrgUnitNew().getDevelopCondition()));
        modifier.put("orgUnitShortTitle", ((StudentListOrder) order).getOrgUnit().getShortTitle());
        modifier.put("compensationType_Alt", firstExtract.getEntity().getCompensationType().isBudget() ? "на бюджетной основе" : "по договору с оплатой за обучение");
    }

    public static String getDevelopConditionFullStr_A(DevelopCondition developCondition)
    {
        switch (developCondition.getCode())
        {
            case UniDefines.DEVELOP_CONDITION_SHORT:
                return "сокращенную программу";
            case UniDefines.DEVELOP_CONDITION_FAST:
                return "ускоренную программу";
            case UniDefines.DEVELOP_CONDITION_SHORT_FAST:
                return "сокращенную ускоренную программу";
            default:
                return "полную программу";
        }
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, TransferDevConditionStuListExtract firstExtract)
    {
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract)
                .put("orgUnitShortTitle", ((StudentListOrder) paragraph.getOrder()).getOrgUnit().getShortTitle())
                .put("course", firstExtract.getEntity().getCourse().getTitle())
                .put("group", firstExtract.getGroup().getTitle())
                .put("developCondition_A", getDevelopConditionFullStr_A(firstExtract.getEducationOrgUnitNew().getDevelopCondition()));
        return CommonExtractPrint.addEntryDateToModifier(injectModifier, firstExtract.getTransferDate());
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, TransferDevConditionStuListExtract firstExtract)
    {
        return null;
    }
}