/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.modularextract;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.component.customorder.ICustomOrderDao;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.custom.ModifyModularStuExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 28.08.2013
 */
public class ModifyOrderStuExtractDao extends UniBaseDao implements IExtractComponentDao<ModifyModularStuExtract>
{
    @Override
    public void doCommit(ModifyModularStuExtract extract, Map parameters)
    {
        if (!extract.isIndividual())
            MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);
        ICustomOrderDao.instance.get().doCommitCustomExtract(extract);
    }

    @Override
    public void doRollback(ModifyModularStuExtract extract, Map parameters)
    {
        ICustomOrderDao.instance.get().doRollbackCustomExtract(extract);
    }
}