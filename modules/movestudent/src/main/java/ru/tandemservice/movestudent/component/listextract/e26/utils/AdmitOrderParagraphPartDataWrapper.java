/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e26.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 10.09.2012
 */
public class AdmitOrderParagraphPartDataWrapper implements Comparable<AdmitOrderParagraphPartDataWrapper>
{
    private final EducationLevelsHighSchool _educationLevelsHighSchool;
    private final List<AdmitOrderParagraphPartPartDataWrapper> _paragraphPartPartList = new ArrayList<>();
    private final ListStudentExtract _firstExtract;

    public AdmitOrderParagraphPartDataWrapper(EducationLevelsHighSchool educationLevelsHighSchool, ListStudentExtract firstExtract)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
        _firstExtract = firstExtract;
    }

    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public List<AdmitOrderParagraphPartPartDataWrapper> getParagraphPartPartList()
    {
        return _paragraphPartPartList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof AdmitOrderParagraphPartDataWrapper))
            return false;

        AdmitOrderParagraphPartDataWrapper admitOrderParagraphPartDataWrapper = (AdmitOrderParagraphPartDataWrapper) o;

        return _educationLevelsHighSchool.equals(admitOrderParagraphPartDataWrapper.getEducationLevelsHighSchool());
    }

    @Override
    public int hashCode()
    {
        return _educationLevelsHighSchool.hashCode();
    }

    @Override
    public int compareTo(AdmitOrderParagraphPartDataWrapper o)
    {
        StructureEducationLevels thisLevel = _educationLevelsHighSchool.getEducationLevel().getLevelType();
        StructureEducationLevels thatLevel = o.getEducationLevelsHighSchool().getEducationLevel().getLevelType();
        boolean isThisParent = !thisLevel.isProfile() && !thisLevel.isSpecialization();
        boolean isThatParent = !thatLevel.isProfile() && !thatLevel.isSpecialization();
        boolean isLevelAlike = isThisParent == isThatParent;

        String thisCode = _educationLevelsHighSchool.getEducationLevel().getTitleCodePrefix();
        String thatCode = o.getEducationLevelsHighSchool().getEducationLevel().getTitleCodePrefix();

        int result = 0;
        if (thisCode != null && thatCode != null)
            result = thisCode.compareTo(thatCode);

        if (result == 0 && !isLevelAlike)
        {
            if (isThisParent)
                result = 1;
            else result = -1;
        }

        if (result == 0 && !isThisParent)
            result = _educationLevelsHighSchool.getEducationLevel().getParentLevel().getTitle().compareTo(o.getEducationLevelsHighSchool().getEducationLevel().getParentLevel().getTitle());

        if (result == 0)
            result = _educationLevelsHighSchool.getTitle().compareTo(o.getEducationLevelsHighSchool().getTitle());

        return result;
    }
}