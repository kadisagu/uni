/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e40;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.e25.SplitStudentsGroupStuListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.e40.utils.GroupTransferParagraphPartWrapper;
import ru.tandemservice.movestudent.component.listextract.e40.utils.GroupTransferParagraphWrapper;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.GroupTransferExtStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 27.11.2012
 */
public class GroupTransferExtStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<GroupTransferExtStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        List<GroupTransferParagraphWrapper> preparedParagraphsStructure = prepareParagraphsStructure(extracts);

        Qualifications qualification = preparedParagraphsStructure.get(0).getQualification();
        String qualificationCode = null != qualification ? qualification.getCode() : null;
        if (null != qualificationCode && !SplitStudentsGroupStuListExtractPrint.PROCESSED_QUALIFICATION_CODES.contains(qualificationCode))
        {
            qualificationCode = null;
        }

        for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
        {
            injectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[i]);
            injectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i) + "P", SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_PLURAL_CASES_ARRAY.get(qualificationCode)[i]);
        }

        injectModifier.put("transferDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extracts.get(0).getTransferOrOrderDate()));

        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, preparedParagraphsStructure);

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }

    protected List<GroupTransferParagraphWrapper> prepareParagraphsStructure(List<GroupTransferExtStuListExtract> extracts)
    {
        int ind;
        List<GroupTransferParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        for (GroupTransferExtStuListExtract extract : extracts)
        {
            Student student = extract.getEntity();
            Person person = student.getPerson();
            Group groupNew = extract.getGroupNew();
            EducationOrgUnit educationOrgUnit = extract.getEntity().getEducationOrgUnit();

            EducationLevels eduLevels = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel();
            Qualifications qualification = eduLevels.getQualification();

            if (null == qualification)
            {
                EducationLevels tmpLevel = eduLevels.getParentLevel();
                while (null != tmpLevel && null == qualification)
                {
                    qualification = tmpLevel.getQualification();
                    tmpLevel = tmpLevel.getParentLevel();
                }
            }

            GroupTransferParagraphWrapper paragraphWrapper = new GroupTransferParagraphWrapper(student.getCourse(), extract.getGroupOld(), student.getCompensationType(), educationOrgUnit.getEducationLevelHighSchool(), educationOrgUnit.getFormativeOrgUnit(), educationOrgUnit.getTerritorialOrgUnit(), educationOrgUnit.getDevelopForm(), educationOrgUnit.getDevelopCondition(), educationOrgUnit.getDevelopTech(), educationOrgUnit.getDevelopPeriod(), qualification, extract);

            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else
                paragraphWrapper = paragraphWrapperList.get(ind);

            GroupTransferParagraphPartWrapper paragraphPartWrapper = new GroupTransferParagraphPartWrapper(educationOrgUnit.getEducationLevelHighSchool(), groupNew, qualification, extract);

            ind = paragraphWrapper.getParagraphPartWrapperList().indexOf(paragraphPartWrapper);
            if (ind == -1)
                paragraphWrapper.getParagraphPartWrapperList().add(paragraphPartWrapper);
            else
                paragraphPartWrapper = paragraphWrapper.getParagraphPartWrapperList().get(ind);
            paragraphPartWrapper.getPersonList().add(person);
        }

        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<GroupTransferParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            int parNumber = 0;
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (GroupTransferParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.GROUP_TRANSFER_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphWrapper.getFirstExtract();
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(firstExtract.getParagraph(), firstExtract);

                parNumber++;
                paragraphInjectModifier.put("parNumber", String.valueOf(parNumber));
                paragraphInjectModifier.put("paragraphNumber", String.valueOf(parNumber));
                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");
                paragraphInjectModifier.put("paragraphNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");

                String code = paragraphWrapper.getDevelopForm().getCode();
                switch (code)
                {
                    case DevelopFormCodes.FULL_TIME_FORM:
                        paragraphInjectModifier.put("developForm_DF", "очной");
                        break;
                    case DevelopFormCodes.CORESP_FORM:
                        paragraphInjectModifier.put("developForm_DF", "заочной");
                        break;
                    case DevelopFormCodes.PART_TIME_FORM:
                        paragraphInjectModifier.put("developForm_DF", "очно-заочной");
                        break;
                    case DevelopFormCodes.EXTERNAL_FORM:
                        paragraphInjectModifier.put("developForm_DF", "экстернату");
                        break;
                    case DevelopFormCodes.APPLICANT_FORM:
                        paragraphInjectModifier.put("developForm_DF", "самостоятельному обучению и итоговой аттестации");
                        break;
                }

                Student firstStudent = firstExtract.getEntity();

                CommonListExtractPrint.injectCompensationType(paragraphInjectModifier, firstStudent.getCompensationType(), "", false);

                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstStudent.getEducationOrgUnit().getFormativeOrgUnit(), "", "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstStudent.getEducationOrgUnit(), "formativeOrgUnitStr", "");

                Qualifications qualification = paragraphWrapper.getQualification();
                String qualificationCode = null != qualification ? qualification.getCode() : null;
                if (null != qualificationCode && !SplitStudentsGroupStuListExtractPrint.PROCESSED_QUALIFICATION_CODES.contains(qualificationCode))
                {
                    qualificationCode = null;
                }

                for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
                {
                    paragraphInjectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[i]);
                    paragraphInjectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i) + "P", SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_PLURAL_CASES_ARRAY.get(qualificationCode)[i]);
                }

                paragraphInjectModifier.put("course", paragraphWrapper.getCourse().getTitle());
                paragraphInjectModifier.put("group", paragraphWrapper.getGroup().getTitle());
                CommonExtractPrint.initFefuGroup(paragraphInjectModifier, "groupInternal_G", paragraphWrapper.getGroup(), paragraphWrapper.getDevelopForm(), " группы ");
                UniRtfUtil.initEducationType(paragraphInjectModifier, paragraphWrapper.getEducationLevels(), "");
                paragraphInjectModifier.put("educationOrgUnit", paragraphWrapper.getEducationLevels().getDisplayableTitle());

                EducationLevels speciality = EducationOrgUnitUtil.getParentLevel(paragraphWrapper.getEducationLevels());
                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, speciality, new String[]{"fefuEducationStrDirection"}, false);

                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, paragraphWrapper.getEducationLevels().getEducationLevel());
                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, paragraphWrapper.getDevelopCondition(), paragraphWrapper.getDevelopTech(), paragraphWrapper.getDevelopPeriod(), CommonListOrderPrint.getEducationBaseText(paragraphWrapper.getGroup()), "fefuShortFastExtendedOptionalText");

                paragraphInjectModifier.put("transferDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(paragraphWrapper.getFirstExtract().getTransferOrOrderDate()));
                customModifier.listParagraphModifier(paragraphInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphInjectModifier.modify(paragraph);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getParagraphPartWrapperList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraph, List<GroupTransferParagraphPartWrapper> paragraphPartWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartWrappers);
            List<IRtfElement> parList = new ArrayList<>();

            for (GroupTransferParagraphPartWrapper paragraphPartWrapper : paragraphPartWrappers)
            {
                // Получаем шаблон подпараграфа
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.GROUP_TRANSFER_LIST_EXTRACT), 3);
                RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphPartWrapper.getFirstExtract();
                RtfInjectModifier paragraphPartInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, firstExtract);

                Qualifications qualification = paragraphPartWrapper.getQualification();
                String qualificationCode = null != qualification ? qualification.getCode() : null;
                if (null != qualificationCode && !SplitStudentsGroupStuListExtractPrint.PROCESSED_QUALIFICATION_CODES.contains(qualificationCode))
                {
                    //qualification = null;
                    qualificationCode = null;
                }

                for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
                {
                    String casePostfix = UniRtfUtil.CASE_POSTFIX.get(i);
                    paragraphPartInjectModifier.put("profileString" + casePostfix, SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[i]);
                    paragraphPartInjectModifier.put("profileString" + casePostfix + "P", SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_PLURAL_CASES_ARRAY.get(qualificationCode)[i]);
                    paragraphPartInjectModifier.put("ProfileString" + casePostfix, StringUtils.capitalize(SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[i]));
                    paragraphPartInjectModifier.put("ProfileString" + casePostfix + "P", StringUtils.capitalize(SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_PLURAL_CASES_ARRAY.get(qualificationCode)[i]));
                }

                EducationLevelsHighSchool educationLevelsHighSchool = paragraphPartWrapper.getEducationLevel();
                boolean isProfileOrSpecialization = educationLevelsHighSchool.getEducationLevel().isProfileOrSpecialization();

                if (isProfileOrSpecialization)
                {
                    paragraphPartInjectModifier.put("specialization", educationLevelsHighSchool.getTitle());
                    paragraphPartInjectModifier.put("profile", educationLevelsHighSchool.getTitle());
                }
                else
                {
                    SharedRtfUtil.removeParagraphsWithTagsRecursive(paragraphPart, Arrays.asList("specialization", "profile"), false, false);
                }

                paragraphPartInjectModifier.put("groupNew", paragraphPartWrapper.getGroupNew().getTitle());

                List<Person> personList = paragraphPartWrapper.getPersonList();
                personList.sort(Person.FULL_FIO_AND_ID_COMPARATOR);

                RtfString rtfString = new RtfString();

                int j = 0;

                for (; j < personList.size() - 1; j++)
                {
                    rtfString.append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio()).par();
                }

                rtfString.append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio());

                paragraphPartInjectModifier.put("STUDENT_LIST", rtfString);
                customModifier.listParagraphModifier(paragraphPartInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                paragraphPartInjectModifier.modify(paragraphPart);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraphPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}