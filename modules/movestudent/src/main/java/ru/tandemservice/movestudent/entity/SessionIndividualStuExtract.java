package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.entity.gen.SessionIndividualStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. Об установлении индивидуального срока сдачи сессии
 */
public class SessionIndividualStuExtract extends SessionIndividualStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getDeadlineDate();
    }
}