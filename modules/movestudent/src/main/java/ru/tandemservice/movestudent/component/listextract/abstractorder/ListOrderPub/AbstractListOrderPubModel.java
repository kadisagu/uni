/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderPub;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.component.visa.VisaList.IVisaOwnerModel;

import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 06.05.2009
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orderId"),
        @Bind(key = "selectedTab", binding = "selectedTab")
})
public abstract class AbstractListOrderPubModel<T extends StudentListOrder> implements IVisaOwnerModel
{
    public static final String FIRST_EXTRACT = "firstExtractViewProperty";
    public static final String EXTRACT_COUNT = "extractCountViewProperty";
    public static final String GROUP_TITLE = "groupTitle";
    public static final String READ_ONLY = "readonlyViewProperty";
    public static final String EDU_LEVEL_HS = "eduTitle";
    public static final String EXTRACT_TYPE_TITLE = "extractTypeTitle";
    public static final String PAR_COMPONENT = "parComponent";
    public static final String PAR_TITLE = "title";
    public static final String COURSE_TITLE = "courseTitle";
    public static final String DEVELOP_FORM_TITLE = "developFormTitle";

    public static final String EDU_PROGRAM_SUBJECT = "eduProgramSubj";
    public static final String EDU_PROGRAM_SPEC = "eduProgramSpec";

    private IDataSettings _settings;
    private CommonPostfixPermissionModel _secModel;
    private T _order;
    private Long _orderId;
    private String _selectedTab;
    private Long _visingStatus;
    private String _basicsTitle;

    private DynamicListDataSource<DataWrapper> _dataSource;
    private List<DataWrapper> _wrappers;

    public List<DataWrapper> getWrappers()
    {
        return _wrappers;
    }

    public void setWrappers(List<DataWrapper> wrappers)
    {
        _wrappers = wrappers;
    }

    private String _searchListSettingsKey;

    private String _actionsPage;
    private String _paramsPage;
    private int _paragraphCount;

    private boolean _multipleParagraphAddEnabled = false;

    private Map<String, Object> _visaListParameters = ParametersMap.createWith("visaOwnerModel", this);

    public boolean isNeedVising()
    {
        return _visingStatus != null;
    }

    // IVisaOwnerModel

    @Override
    public IAbstractDocument getDocument()
    {
        return _order;
    }

    @Override
    public boolean isVisaListModificable()
    {
        return UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(_order.getState().getCode());
    }

    @Override
    public String getSecPostfix()
    {
        return "studentListOrder";
    }

    @SuppressWarnings("unchecked")
    public IListStudentOrderPubControllerExtension getLastExtension()
    {
        List<IListStudentOrderPubControllerExtension> extensionsList = (List<IListStudentOrderPubControllerExtension>) ApplicationRuntime.getBean(IListStudentOrderPubControllerExtension.LIST_STUDENT_ORDER_PUB_CONTROLLER_EXTENSIONS_BEAN_NAME);
        if (null != extensionsList && !extensionsList.isEmpty())
        {
            return extensionsList.get(extensionsList.size() - 1);
        }
        return null;
    }

    // Getters & Setters

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public T getOrder()
    {
        return _order;
    }

    public void setOrder(T order)
    {
        _order = order;
    }

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public Long getVisingStatus()
    {
        return _visingStatus;
    }

    public void setVisingStatus(Long visingStatus)
    {
        _visingStatus = visingStatus;
    }

    public String getBasicsTitle()
    {
        return _basicsTitle;
    }

    public void setBasicsTitle(String basicsTitle)
    {
        _basicsTitle = basicsTitle;
    }

    public DynamicListDataSource<DataWrapper> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<DataWrapper> dataSource)
    {
        _dataSource = dataSource;
    }

    public String getSearchListSettingsKey()
    {
        return _searchListSettingsKey;
    }

    public void setSearchListSettingsKey(String searchListSettingsKey)
    {
        _searchListSettingsKey = searchListSettingsKey;
    }

    public Map<String, Object> getVisaListParameters()
    {
        return _visaListParameters;
    }

    public void setVisaListParameters(Map<String, Object> visaListParameters)
    {
        _visaListParameters = visaListParameters;
    }

    public String getActionsPage()
    {
        return _actionsPage;
    }

    public void setActionsPage(String actionsPage)
    {
        _actionsPage = actionsPage;
    }

    public String getParamsPage()
    {
        return _paramsPage;
    }

    public void setParamsPage(String paramsPage)
    {
        _paramsPage = paramsPage;
    }

    public boolean isMultipleParagraphAddEnabled()
    {
        return _multipleParagraphAddEnabled;
    }

    public void setMultipleParagraphAddEnabled(boolean multipleParagraphAddEnabled)
    {
        this._multipleParagraphAddEnabled = multipleParagraphAddEnabled;
    }

    public boolean isCustom()
    {
        return MoveStudentDaoFacade.getMoveStudentDao().isCustomOrderType(getOrder().getType());
    }

    public int getParagraphCount()
    {
        return _paragraphCount;
    }

    public void setParagraphCount(int paragraphCount)
    {
        _paragraphCount = paragraphCount;
    }
}