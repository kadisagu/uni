/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e81;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract;

import java.util.Arrays;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 31.08.2012
 */
public class RestorationAdmitToDiplomaDefStuExtractPrint implements IPrintFormCreator<RestorationAdmitToDiplomaDefStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, RestorationAdmitToDiplomaDefStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        CommonExtractPrint.injectDiplomaEnd(modifier, extract.getEducationOrgUnitNew().getEducationLevelHighSchool().getEducationLevel().getLevelType());
        int ind = Arrays.asList(ApplicationRuntime.getProperty("seasonTerm_N").split(";")).indexOf(extract.getSeason());
        String season = ind > -1 ? Arrays.asList(ApplicationRuntime.getProperty("seasonTerm_P").split(";")).get(ind) : "______";

        CommonExtractPrint.injectFefuDevelopConditionAndTech(modifier, extract.getEducationOrgUnitNew(), extract.getGroupNew(), "fefuShortFastExtendedOptionalTextNew");

        modifier.put("vuzTitle", extract.getDismissedFrom());
        modifier.put("disOrderNum", extract.getDismissOrder());
        modifier.put("disOrderDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDismissOrderDate()));
        modifier.put("period", season);
        modifier.put("year", DateFormatter.DATE_FORMATTER_JUST_YEAR.format(extract.getDpDefenceDeadlineDate()));
        modifier.put("newDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDpDefenceDeadlineDate()));
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}