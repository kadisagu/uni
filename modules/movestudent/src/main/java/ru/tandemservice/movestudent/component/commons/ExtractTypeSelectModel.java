/* $Id$ */
package ru.tandemservice.movestudent.component.commons;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.list.IViewSelectValueStyle;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.sec.entity.Admin;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import ru.tandemservice.movestudent.entity.ExtractCreationRule;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.unimove.IAbstractExtract;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Модель для селектов и списков для вывода типов приказов по студентам.
 * Создана, чтобы не городить в каждом списке свою модель со сложной логикой.
 * Данный класс покрывает большинство мест использования фильтров/списков по типу приказа.
 *
 * @author Nikolay Fedorovskih
 * @since 23.12.2013
 */
public class ExtractTypeSelectModel extends BaseSelectModel implements ISingleSelectModel, IMultiSelectModel, IHierarchicalSelectModel
{
    private Collection<OrderCategory> _categories; // Категории типов приказов
    private boolean _includeExistsTypesFromContext; // Включать ли в выборку неиспользуемые типы, но на которые имеются выписки соответствующего типа
    private boolean _hierarchical; // Иерархичность
    private boolean _showUnused; // Показывать ли неиспользуемые типы
    private boolean _disableTop = true; // Дизаблить ли элементы верхнего уровня
    private boolean _thirdLevel = true; // Выводить ли элементы третьего уровня в иерархическом списке
    private IOrderCategoryFilteredModel _model; // Модель для списка, где есть фильтр по категории приказа, от которого зависит данный фильтр (по типу приказа)
    private boolean _admin;

    protected static final String MAIN_ALIAS = "e";

    public ExtractTypeSelectModel()
    {
        super(UserContext.getInstance().getPrincipalContext() instanceof Admin ? StudentExtractType.P_TITLE_WITH_CODE : StudentExtractType.P_TITLE);
        _admin = UserContext.getInstance().getPrincipalContext() instanceof Admin;
    }

    public ExtractTypeSelectModel(IOrderCategoryFilteredModel model)
    {
        super(UserContext.getInstance().getPrincipalContext() instanceof Admin ? StudentExtractType.P_TITLE_WITH_CODE : StudentExtractType.P_TITLE);
        _admin = UserContext.getInstance().getPrincipalContext() instanceof Admin;
        _model = model;
    }

    public ExtractTypeSelectModel(OrderCategory category)
    {
        super(UserContext.getInstance().getPrincipalContext() instanceof Admin ? StudentExtractType.P_TITLE_WITH_CODE : StudentExtractType.P_TITLE);
        _admin = UserContext.getInstance().getPrincipalContext() instanceof Admin;
        _categories = new ArrayList<>(1);
        _categories.add(category);
    }

    public ExtractTypeSelectModel includeExistsTypesFromContext(boolean value)
    {
        _includeExistsTypesFromContext = value;
        return this;
    }

    public ExtractTypeSelectModel hierarchical(boolean value)
    {
        _hierarchical = value;
        return this;
    }

    public ExtractTypeSelectModel disableTop(boolean value)
    {
        _disableTop = value;
        return this;
    }

    public ExtractTypeSelectModel thirdLevel(boolean value)
    {
        _thirdLevel = value;
        return this;
    }

    public ExtractTypeSelectModel showUnused(boolean value)
    {
        _showUnused = value;
        return this;
    }


    // TODO _includeExistsTypesFromContext - заменить на какой-нибудь ExecutionContextHandler, чтобы четко под конкретный список подстраивалось, а не под все выписки какого-то типа.
    // TODO пройтись по всем местам, где есть модели на StudentExtractType и заменить на эту, если возможно

    public DQLSelectBuilder createBuilder(String filter, Object o)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentExtractType.class, MAIN_ALIAS)
                .column(MAIN_ALIAS)
                .joinPath(DQLJoinType.left, StudentExtractType.parent().fromAlias(MAIN_ALIAS), "p1");

        if (!_hierarchical)
        {
            // Модель придумана для иерархического селекта
            if (_model != null)
                throw new IllegalStateException();

            // В плоском списке могу выводиться типы только одной категории приказов
            if (_categories == null || _categories.size() != 1)
                throw new IllegalStateException();

            OrderCategory category = _categories.iterator().next();

            // Условие для плоского списка простое - родительский элемент типа приказа должен быть одной из категорий (сборный или списочный)
            builder.where(eq(property("p1", StudentExtractType.P_CODE), value(category.getBaseExtractTypeCode())));

            switch (category)
            {
                case MODULAR:
                case INDIVIDUAL:


                    // Для сборных/индивидуальных нужно проверять признак индивидуальности
                    builder.joinEntity(MAIN_ALIAS, DQLJoinType.left, ExtractCreationRule.class, "rule",
                                       eq(property(MAIN_ALIAS + ".id"), property("rule", ExtractCreationRule.L_STUDENT_EXTRACT_TYPE)));

                    IDQLExpression expression;
                    if (category == OrderCategory.INDIVIDUAL)
                        expression = eq(property("rule", ExtractCreationRule.P_INDIVIDUAL_ORDER), value(Boolean.TRUE));
                    else
                        expression = or(isNull("rule.id"), eq(property("rule", ExtractCreationRule.P_INDIVIDUAL_ORDER), value(Boolean.FALSE)));

                    if (!_showUnused)
                    {
                        // Скрываем неиспользуемые типы, если нужно
                        expression = and(expression, eq(property(MAIN_ALIAS, StudentExtractType.P_ACTIVE), value(Boolean.TRUE)));
                    }

                    if (_includeExistsTypesFromContext)
                    {
                        // Но если у нас это фильтр в списке каких-то выписок/приказов, то нужно выводить даже неактивные и [не]индивидуальные типы,
                        // если приказы (выписки) такого типа есть в данном списке

                        DQLSelectBuilder currentOrders = new DQLSelectBuilder().fromEntity(ModularStudentExtract.class, "me")
                                .column(property("mp", AbstractStudentParagraph.order().id()))
                                .joinEntity("me", DQLJoinType.inner, AbstractStudentParagraph.class, "mp", eq("me." + IAbstractExtract.L_PARAGRAPH, "mp.id"))
                                .joinEntity("me", DQLJoinType.left, ExtractCreationRule.class, "cr", eq(property("me." + IAbstractExtract.L_TYPE), property("cr." + ExtractCreationRule.L_STUDENT_EXTRACT_TYPE)))
                                .group(property("mp", AbstractStudentParagraph.order().id()));

                        if (category == OrderCategory.MODULAR)
                        {
                            // Для сборных приказов всё сложно из-за кривой архитектуры, появившейся после впиливания индивидуальных приказов
                            // В фильтре необходимо выводить типы без признака индивидуальности плюс те типы, на которые есть выписки среди сборных приказов.
                            // А чтобы понять есть ли тип приказа среди выписок сборных приказов, надо отличить выписки из сборных приказов от индивидуальный.
                            // Но вот засада: выписки с признаков индивидуальности true могут быть в сборном приказе! И они автоматически считаются выпиской из сборного приказа.
                            currentOrders.having(or(
                                    ne(DQLFunctions.countStar(), value(1)),
                                    eq(DQLFunctions.coalesce(
                                            DQLFunctions.min(DQLFunctions.cast(property("cr." + ExtractCreationRule.P_INDIVIDUAL_ORDER), PropertyType.INTEGER)),
                                            value(0)),
                                       value(0))
                            ));
                        }
                        else
                        {
                            // Для индивидуальных приказов всё аналогично сборным, только наоборот.
                            currentOrders.having(and(
                                    eq(DQLFunctions.countStar(), value(1)),
                                    eq(DQLFunctions.min(DQLFunctions.cast(property("cr." + ExtractCreationRule.P_INDIVIDUAL_ORDER), PropertyType.INTEGER)), value(1))
                            ));
                        }

                        DQLSelectBuilder myExtractTypeInCurrentOrdersList = new DQLSelectBuilder().fromEntity(ModularStudentExtract.class, "mee")
                                .column("mee.id")
                                .joinEntity("mee", DQLJoinType.inner, AbstractStudentParagraph.class, "mpp", eq("mee." + IAbstractExtract.L_PARAGRAPH, "mpp.id"))
                                .where(eq(property("mee", ModularStudentExtract.type().id()), property(MAIN_ALIAS + ".id")))
                                .where(in(property("mpp", AbstractStudentParagraph.order().id()), currentOrders.buildQuery()));

                        // Либо выполняется стандартное условие (активный тип без признака индивидуальности), либо тип есть среди выписок из сборных приказов
                        expression = or(expression, exists(myExtractTypeInCurrentOrdersList.buildQuery()));
                    }

                    builder.where(expression);
                    break;

                case LIST: // Для списочных

                    if (!_showUnused)
                    {
                        if (!_includeExistsTypesFromContext)
                        {
                            // Скрываем неиспользуемые типы, если нужно
                            builder.where(eq(property(MAIN_ALIAS, StudentExtractType.P_ACTIVE), value(Boolean.TRUE)));
                        }
                        else
                        {
                            // Но для контекстнозависимых фильтров нужно показывать в фильтре типов приказов не только используемые типы, но и те, на котоыре созданы выписки
                            DQLSelectBuilder anyUsed = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "extract")
                                    .where(eq(property(AbstractStudentExtract.type().parent().fromAlias("extract")), property(MAIN_ALIAS + ".id")));

                            builder.where(or(
                                    eq(property(MAIN_ALIAS, StudentExtractType.P_ACTIVE), value(Boolean.TRUE)),
                                    exists(anyUsed.buildQuery())
                            ));
                        }
                    }
                    break;

                case OTHER:
                    if (_includeExistsTypesFromContext)
                        throw new IllegalStateException("operation is not supported"); // Пока не было необходимости в контекстнозависимом _плоском_ списке прочих типов - обычно они иерархичные

                    if (!_showUnused)
                        builder.where(eq(property(MAIN_ALIAS, StudentExtractType.P_ACTIVE), value(Boolean.TRUE)));

                    break;

                default:
                    throw new IllegalStateException();
            }
        }
        else // Для варианта с иерархией
        {
            // Учесть, какие категории выводить
            if (_categories != null || _model != null)
            {
                if (_categories != null && _model != null)
                    throw new IllegalStateException(); // Одновременно два конкурирующих источника данных

                Collection<OrderCategory> categories = _categories != null ? _categories : _model.getSelectedCategories();

                if (categories.size() < OrderCategory.values().length)
                {
                    Set<String> rootCodes = new HashSet<>(3);
                    for (OrderCategory category : categories)
                    {
                        rootCodes.add(category.getBaseExtractTypeCode());
                    }

                    builder.joinPath(DQLJoinType.left, StudentExtractType.parent().fromAlias("p1"), "p2");
                    builder.where(or(
                            in(property("p1", StudentExtractType.P_CODE), rootCodes),
                            _thirdLevel ? in(property("p2", StudentExtractType.P_CODE), rootCodes) : null,
                            in(property(MAIN_ALIAS, StudentExtractType.P_CODE), rootCodes)
                    ));

                    if (categories.contains(OrderCategory.INDIVIDUAL) != categories.contains(OrderCategory.MODULAR))
                    {
                        // Для сборных/индивидуальных нужно проверять признак индивидуальности
                        builder.joinEntity(MAIN_ALIAS, DQLJoinType.left, ExtractCreationRule.class, "rule",
                                           eq(property(MAIN_ALIAS + ".id"), property("rule", ExtractCreationRule.L_STUDENT_EXTRACT_TYPE)));

                        if (categories.contains(OrderCategory.INDIVIDUAL))
                            builder.where(eq(property("rule", ExtractCreationRule.P_INDIVIDUAL_ORDER), value(Boolean.TRUE)));
                        else
                            builder.where(or(
                                    isNull(property("rule.id")),
                                    eq(property("rule", ExtractCreationRule.P_INDIVIDUAL_ORDER), value(Boolean.FALSE))
                            ));
                    }
                }
            }

            if (!_showUnused)
                builder.where(eq(property(MAIN_ALIAS, StudentExtractType.P_ACTIVE), value(Boolean.TRUE)));
        }

        if (o != null)
        {
            if (o instanceof Collection)
                builder.where(in(property(MAIN_ALIAS + ".id"), (Collection) o));
            else
                builder.where(eq(property(MAIN_ALIAS + ".id"), commonValue(o, PropertyType.LONG)));
        }

        if (StringUtils.isNotEmpty(filter))
        {
            if (_admin)
            {
                builder.where(or(
                        likeUpper(property(MAIN_ALIAS, StudentExtractType.P_TITLE), value(CoreStringUtils.escapeLike(filter, true))),
                        likeUpper(property(MAIN_ALIAS, StudentExtractType.P_CODE), value(CoreStringUtils.escapeLike(filter, true)))
                ));
            } else
            {
                builder.where(likeUpper(property(MAIN_ALIAS, StudentExtractType.P_TITLE), value(CoreStringUtils.escapeLike(filter, true))));
            }
        }

        builder.order(property(MAIN_ALIAS, StudentExtractType.P_TITLE));

        return builder;
    }

    @Override
    public List getValues(Set primaryKeys)
    {
        return DataAccessServices.dao().getList(createBuilder(null, primaryKeys));
    }

    @Override
    public Object getValue(Object primaryKey)
    {
        return new DQLListResultBuilder(createBuilder(null, primaryKey)).getSingleValue();
    }

    @Override
    public ListResult<StudentExtractType> findValues(String filter)
    {
        DQLSelectBuilder builder = createBuilder(filter, null);
        List<StudentExtractType> list;
        int count;
        if (_hierarchical)
        {
            // Для иерархических вариантов списка не делаем ограничения по количеству элементов (постраничность с иерархией плохо сочетаются)
            list = DataAccessServices.dao().getList(builder);
            count = list.size();

            // Все родители должны быть обязательно, даже если они (эти родители) не попали в выборку
            Set<StudentExtractType> set = new HashSet<>(list);
            for (StudentExtractType item : set)
            {
                StudentExtractType parent = item.getParent();
                if (parent != null && !set.contains(parent))
                {
                    list.add(parent);
                    parent = parent.getParent();
                    if (parent != null && !set.contains(parent))
                        list.add(parent);
                }
            }
        }
        else
        {
            int maxRows = 50;
            list = DataAccessServices.dao().getList(builder, 0, maxRows);
            if (list == null)
                list = new ArrayList<>();
            count = list.size();
            if (count == maxRows)
                count = DataAccessServices.dao().getCount(builder);
        }

        Collections.sort(list, new StudentExtractType.ItemsComparator());

        return new ListResult<>(list, count);
    }

    @Override
    public int getLevel(Object value)
    {
        int level = 0;
        if (_hierarchical)
        {
            StudentExtractType item = (StudentExtractType) value;
            while (item.getParent() != null)
            {
                item = item.getParent();
                level++;
            }
        }
        return level;
    }

    @Override
    public IViewSelectValueStyle getValueStyle(Object value)
    {
        StudentExtractType type = (StudentExtractType) value;
        if (type.getParent() != null || !_disableTop)
            return super.getValueStyle(value);

        // Задизаблены рутовые и неактивные типы
        DefaultSelectValueStyle valueStyle = new DefaultSelectValueStyle();
        valueStyle.setDisabled(true);
        valueStyle.setRowStyle("background-color:#D2D2D2");
        return valueStyle;
    }
}