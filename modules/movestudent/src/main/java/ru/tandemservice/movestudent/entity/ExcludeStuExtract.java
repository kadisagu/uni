package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.movestudent.entity.gen.ExcludeStuExtractGen;

import java.util.Date;

public class ExcludeStuExtract extends ExcludeStuExtractGen implements IExcludeExtract
{
    @Override
    public Date getBeginDate()
    {
        return getDate();
    }

    @Override
    public Date getExcludeDate()
    {
        return getDate();
    }
}