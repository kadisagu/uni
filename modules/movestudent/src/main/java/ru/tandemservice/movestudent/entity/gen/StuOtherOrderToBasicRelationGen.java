package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.OtherStudentExtract;
import ru.tandemservice.movestudent.entity.StuOtherOrderToBasicRelation;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь выписки из прочего приказа с основанием
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StuOtherOrderToBasicRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.StuOtherOrderToBasicRelation";
    public static final String ENTITY_NAME = "stuOtherOrderToBasicRelation";
    public static final int VERSION_HASH = -1876149095;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String L_BASIC = "basic";
    public static final String P_COMMENT = "comment";

    private OtherStudentExtract _extract;     // Выписка (прочие приказы по студентам)
    private StudentOrderBasics _basic;     // Основание приказа по студенту
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка (прочие приказы по студентам).
     */
    public OtherStudentExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка (прочие приказы по студентам).
     */
    public void setExtract(OtherStudentExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Основание приказа по студенту.
     */
    public StudentOrderBasics getBasic()
    {
        return _basic;
    }

    /**
     * @param basic Основание приказа по студенту.
     */
    public void setBasic(StudentOrderBasics basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StuOtherOrderToBasicRelationGen)
        {
            setExtract(((StuOtherOrderToBasicRelation)another).getExtract());
            setBasic(((StuOtherOrderToBasicRelation)another).getBasic());
            setComment(((StuOtherOrderToBasicRelation)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StuOtherOrderToBasicRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StuOtherOrderToBasicRelation.class;
        }

        public T newInstance()
        {
            return (T) new StuOtherOrderToBasicRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "basic":
                    return obj.getBasic();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((OtherStudentExtract) value);
                    return;
                case "basic":
                    obj.setBasic((StudentOrderBasics) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "basic":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "basic":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return OtherStudentExtract.class;
                case "basic":
                    return StudentOrderBasics.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StuOtherOrderToBasicRelation> _dslPath = new Path<StuOtherOrderToBasicRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StuOtherOrderToBasicRelation");
    }
            

    /**
     * @return Выписка (прочие приказы по студентам).
     * @see ru.tandemservice.movestudent.entity.StuOtherOrderToBasicRelation#getExtract()
     */
    public static OtherStudentExtract.Path<OtherStudentExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Основание приказа по студенту.
     * @see ru.tandemservice.movestudent.entity.StuOtherOrderToBasicRelation#getBasic()
     */
    public static StudentOrderBasics.Path<StudentOrderBasics> basic()
    {
        return _dslPath.basic();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.movestudent.entity.StuOtherOrderToBasicRelation#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends StuOtherOrderToBasicRelation> extends EntityPath<E>
    {
        private OtherStudentExtract.Path<OtherStudentExtract> _extract;
        private StudentOrderBasics.Path<StudentOrderBasics> _basic;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка (прочие приказы по студентам).
     * @see ru.tandemservice.movestudent.entity.StuOtherOrderToBasicRelation#getExtract()
     */
        public OtherStudentExtract.Path<OtherStudentExtract> extract()
        {
            if(_extract == null )
                _extract = new OtherStudentExtract.Path<OtherStudentExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Основание приказа по студенту.
     * @see ru.tandemservice.movestudent.entity.StuOtherOrderToBasicRelation#getBasic()
     */
        public StudentOrderBasics.Path<StudentOrderBasics> basic()
        {
            if(_basic == null )
                _basic = new StudentOrderBasics.Path<StudentOrderBasics>(L_BASIC, this);
            return _basic;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.movestudent.entity.StuOtherOrderToBasicRelation#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(StuOtherOrderToBasicRelationGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return StuOtherOrderToBasicRelation.class;
        }

        public String getEntityName()
        {
            return "stuOtherOrderToBasicRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
