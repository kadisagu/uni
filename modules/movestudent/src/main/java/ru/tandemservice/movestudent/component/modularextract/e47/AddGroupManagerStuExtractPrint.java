/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e47;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.AddGroupManagerStuExtract;
import ru.tandemservice.movestudent.entity.GroupToExtractRelation;

import java.util.List;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 10.08.2012
 */
public class AddGroupManagerStuExtractPrint implements IPrintFormCreator<AddGroupManagerStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, AddGroupManagerStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        modifier.put("educationYear", extract.getEducationYear().getTitle());

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GroupToExtractRelation.class, "ge");
        builder.column(DQLExpressions.property("ge", GroupToExtractRelation.group().title())).distinct();
        builder.where(DQLExpressions.eq(DQLExpressions.property("ge", GroupToExtractRelation.extract().id()), DQLExpressions.value(extract.getId())));
        List<String> groupToExtractRelations = ISharedBaseDao.instance.get().getList(builder);

        if(!groupToExtractRelations.isEmpty())
        {
            if(groupToExtractRelations.size() > 1)
            {
                modifier.put("stdGroup", "учебных групп");

                modifier.put("targetGroups", StringUtils.join(groupToExtractRelations, ", "));
            }
            else
            {
                modifier.put("stdGroup", "учебной группы");
                modifier.put("targetGroups", groupToExtractRelations.get(0));
            }
        }
        else
        {
            modifier.put("stdGroup", "учебной группы");
            modifier.put("targetGroups", "______");
        }

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}