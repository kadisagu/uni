package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.FreeStudiesAttendanceStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О свободном посещении занятий
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FreeStudiesAttendanceStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.FreeStudiesAttendanceStuExtract";
    public static final String ENTITY_NAME = "freeStudiesAttendanceStuExtract";
    public static final int VERSION_HASH = 351819791;
    private static IEntityMeta ENTITY_META;

    public static final String P_FROM_DATE = "fromDate";
    public static final String P_TO_DATE = "toDate";

    private Date _fromDate;     // Дата начала свободного посещения
    private Date _toDate;     // Дата окончания свободного посещения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата начала свободного посещения. Свойство не может быть null.
     */
    @NotNull
    public Date getFromDate()
    {
        return _fromDate;
    }

    /**
     * @param fromDate Дата начала свободного посещения. Свойство не может быть null.
     */
    public void setFromDate(Date fromDate)
    {
        dirty(_fromDate, fromDate);
        _fromDate = fromDate;
    }

    /**
     * @return Дата окончания свободного посещения. Свойство не может быть null.
     */
    @NotNull
    public Date getToDate()
    {
        return _toDate;
    }

    /**
     * @param toDate Дата окончания свободного посещения. Свойство не может быть null.
     */
    public void setToDate(Date toDate)
    {
        dirty(_toDate, toDate);
        _toDate = toDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FreeStudiesAttendanceStuExtractGen)
        {
            setFromDate(((FreeStudiesAttendanceStuExtract)another).getFromDate());
            setToDate(((FreeStudiesAttendanceStuExtract)another).getToDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FreeStudiesAttendanceStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FreeStudiesAttendanceStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new FreeStudiesAttendanceStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "fromDate":
                    return obj.getFromDate();
                case "toDate":
                    return obj.getToDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "fromDate":
                    obj.setFromDate((Date) value);
                    return;
                case "toDate":
                    obj.setToDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "fromDate":
                        return true;
                case "toDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "fromDate":
                    return true;
                case "toDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "fromDate":
                    return Date.class;
                case "toDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FreeStudiesAttendanceStuExtract> _dslPath = new Path<FreeStudiesAttendanceStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FreeStudiesAttendanceStuExtract");
    }
            

    /**
     * @return Дата начала свободного посещения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.FreeStudiesAttendanceStuExtract#getFromDate()
     */
    public static PropertyPath<Date> fromDate()
    {
        return _dslPath.fromDate();
    }

    /**
     * @return Дата окончания свободного посещения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.FreeStudiesAttendanceStuExtract#getToDate()
     */
    public static PropertyPath<Date> toDate()
    {
        return _dslPath.toDate();
    }

    public static class Path<E extends FreeStudiesAttendanceStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _fromDate;
        private PropertyPath<Date> _toDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата начала свободного посещения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.FreeStudiesAttendanceStuExtract#getFromDate()
     */
        public PropertyPath<Date> fromDate()
        {
            if(_fromDate == null )
                _fromDate = new PropertyPath<Date>(FreeStudiesAttendanceStuExtractGen.P_FROM_DATE, this);
            return _fromDate;
        }

    /**
     * @return Дата окончания свободного посещения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.FreeStudiesAttendanceStuExtract#getToDate()
     */
        public PropertyPath<Date> toDate()
        {
            if(_toDate == null )
                _toDate = new PropertyPath<Date>(FreeStudiesAttendanceStuExtractGen.P_TO_DATE, this);
            return _toDate;
        }

        public Class getEntityClass()
        {
            return FreeStudiesAttendanceStuExtract.class;
        }

        public String getEntityName()
        {
            return "freeStudiesAttendanceStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
