/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e64;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ExcludeGPDefenceFailStuExtract;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;

import java.util.Collections;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 28.08.2012
 */
public class ExcludeGPDefenceFailStuExtractPrint implements IPrintFormCreator<ExcludeGPDefenceFailStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ExcludeGPDefenceFailStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        String contractTerminationStr = ApplicationRuntime.getProperty("commonextract.contractTerminationStr");
        if (null != contractTerminationStr && CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(extract.getEntity().getCompensationType().getCode()))
        {
            modifier.put("contractTerminationStr", contractTerminationStr);
        } else
        {
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("contractTerminationStr"), true, false);
        }

        if (null != extract.getPassStateExam())
        {
            if (extract.getPassStateExam())
            {
                if (extract.getPluralForStateExam())
                {
                    modifier.put("passedStateExam", " " + modifier.getStringValue("passed_A") + " государственные экзамены и");
                } else
                {
                    modifier.put("passedStateExam", " " + modifier.getStringValue("passed_A") + " государственный экзамен и");
                }
            } else
            {
                modifier.put("passedStateExam", "");
            }
        } else
        {
            modifier.put("passedStateExam", " " + modifier.getStringValue("passed_A") + " государственный экзамен и");
        }

        Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());

        if (qualification != null)
        {
            if (qualification.getCode().equals(QualificationsCodes.BAKALAVR))
            {
                modifier.put("finalQualifyingWork", " (бакалаврской работы)");
            } else
            {
                if (qualification.getCode().equals(QualificationsCodes.MAGISTR))
                {
                    modifier.put("finalQualifyingWork", " (магистерской диссертации)");
                } else
                {
                    modifier.put("finalQualifyingWork", "");
                }
            }
        } else
        {
            modifier.put("finalQualifyingWork", "");
        }

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}