package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «Об изменении условия освоения»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TransferDevConditionStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract";
    public static final String ENTITY_NAME = "transferDevConditionStuListExtract";
    public static final int VERSION_HASH = -773842346;
    private static IEntityMeta ENTITY_META;

    public static final String P_TRANSFER_DATE = "transferDate";
    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_EDUCATION_ORG_UNIT_NEW = "educationOrgUnitNew";
    public static final String L_EDUCATION_ORG_UNIT_OLD = "educationOrgUnitOld";
    public static final String L_GROUP_EDUCATION_ORG_UNIT_NEW = "groupEducationOrgUnitNew";
    public static final String L_GROUP_EDUCATION_ORG_UNIT_OLD = "groupEducationOrgUnitOld";
    public static final String P_GROUP_CHANGE_EDUCATION_ORG_UNIT = "groupChangeEducationOrgUnit";
    public static final String P_RECORD_NUMBER = "recordNumber";
    public static final String P_RECORD_DATE = "recordDate";

    private Date _transferDate;     // Дата перевода
    private Course _course;     // Курс
    private Group _group;     // Группа
    private EducationOrgUnit _educationOrgUnitNew;     // Параметры обучения студентов по направлению подготовки (НПП)
    private EducationOrgUnit _educationOrgUnitOld;     // Параметры обучения студентов по направлению подготовки (НПП)
    private EducationOrgUnit _groupEducationOrgUnitNew;     // Параметры обучения студентов по направлению подготовки (НПП)
    private EducationOrgUnit _groupEducationOrgUnitOld;     // Параметры обучения студентов по направлению подготовки (НПП)
    private boolean _groupChangeEducationOrgUnit; 
    private String _recordNumber;     // Номер протокола заседания
    private Date _recordDate;     // Дата протокола заседания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата перевода.
     */
    public Date getTransferDate()
    {
        return _transferDate;
    }

    /**
     * @param transferDate Дата перевода.
     */
    public void setTransferDate(Date transferDate)
    {
        dirty(_transferDate, transferDate);
        _transferDate = transferDate;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitNew()
    {
        return _educationOrgUnitNew;
    }

    /**
     * @param educationOrgUnitNew Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    public void setEducationOrgUnitNew(EducationOrgUnit educationOrgUnitNew)
    {
        dirty(_educationOrgUnitNew, educationOrgUnitNew);
        _educationOrgUnitNew = educationOrgUnitNew;
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitOld()
    {
        return _educationOrgUnitOld;
    }

    /**
     * @param educationOrgUnitOld Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    public void setEducationOrgUnitOld(EducationOrgUnit educationOrgUnitOld)
    {
        dirty(_educationOrgUnitOld, educationOrgUnitOld);
        _educationOrgUnitOld = educationOrgUnitOld;
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП).
     */
    public EducationOrgUnit getGroupEducationOrgUnitNew()
    {
        return _groupEducationOrgUnitNew;
    }

    /**
     * @param groupEducationOrgUnitNew Параметры обучения студентов по направлению подготовки (НПП).
     */
    public void setGroupEducationOrgUnitNew(EducationOrgUnit groupEducationOrgUnitNew)
    {
        dirty(_groupEducationOrgUnitNew, groupEducationOrgUnitNew);
        _groupEducationOrgUnitNew = groupEducationOrgUnitNew;
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП).
     */
    public EducationOrgUnit getGroupEducationOrgUnitOld()
    {
        return _groupEducationOrgUnitOld;
    }

    /**
     * @param groupEducationOrgUnitOld Параметры обучения студентов по направлению подготовки (НПП).
     */
    public void setGroupEducationOrgUnitOld(EducationOrgUnit groupEducationOrgUnitOld)
    {
        dirty(_groupEducationOrgUnitOld, groupEducationOrgUnitOld);
        _groupEducationOrgUnitOld = groupEducationOrgUnitOld;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isGroupChangeEducationOrgUnit()
    {
        return _groupChangeEducationOrgUnit;
    }

    /**
     * @param groupChangeEducationOrgUnit  Свойство не может быть null.
     */
    public void setGroupChangeEducationOrgUnit(boolean groupChangeEducationOrgUnit)
    {
        dirty(_groupChangeEducationOrgUnit, groupChangeEducationOrgUnit);
        _groupChangeEducationOrgUnit = groupChangeEducationOrgUnit;
    }

    /**
     * @return Номер протокола заседания.
     */
    @Length(max=255)
    public String getRecordNumber()
    {
        return _recordNumber;
    }

    /**
     * @param recordNumber Номер протокола заседания.
     */
    public void setRecordNumber(String recordNumber)
    {
        dirty(_recordNumber, recordNumber);
        _recordNumber = recordNumber;
    }

    /**
     * @return Дата протокола заседания.
     */
    public Date getRecordDate()
    {
        return _recordDate;
    }

    /**
     * @param recordDate Дата протокола заседания.
     */
    public void setRecordDate(Date recordDate)
    {
        dirty(_recordDate, recordDate);
        _recordDate = recordDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TransferDevConditionStuListExtractGen)
        {
            setTransferDate(((TransferDevConditionStuListExtract)another).getTransferDate());
            setCourse(((TransferDevConditionStuListExtract)another).getCourse());
            setGroup(((TransferDevConditionStuListExtract)another).getGroup());
            setEducationOrgUnitNew(((TransferDevConditionStuListExtract)another).getEducationOrgUnitNew());
            setEducationOrgUnitOld(((TransferDevConditionStuListExtract)another).getEducationOrgUnitOld());
            setGroupEducationOrgUnitNew(((TransferDevConditionStuListExtract)another).getGroupEducationOrgUnitNew());
            setGroupEducationOrgUnitOld(((TransferDevConditionStuListExtract)another).getGroupEducationOrgUnitOld());
            setGroupChangeEducationOrgUnit(((TransferDevConditionStuListExtract)another).isGroupChangeEducationOrgUnit());
            setRecordNumber(((TransferDevConditionStuListExtract)another).getRecordNumber());
            setRecordDate(((TransferDevConditionStuListExtract)another).getRecordDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TransferDevConditionStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TransferDevConditionStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new TransferDevConditionStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return obj.getTransferDate();
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "educationOrgUnitNew":
                    return obj.getEducationOrgUnitNew();
                case "educationOrgUnitOld":
                    return obj.getEducationOrgUnitOld();
                case "groupEducationOrgUnitNew":
                    return obj.getGroupEducationOrgUnitNew();
                case "groupEducationOrgUnitOld":
                    return obj.getGroupEducationOrgUnitOld();
                case "groupChangeEducationOrgUnit":
                    return obj.isGroupChangeEducationOrgUnit();
                case "recordNumber":
                    return obj.getRecordNumber();
                case "recordDate":
                    return obj.getRecordDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "transferDate":
                    obj.setTransferDate((Date) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "educationOrgUnitNew":
                    obj.setEducationOrgUnitNew((EducationOrgUnit) value);
                    return;
                case "educationOrgUnitOld":
                    obj.setEducationOrgUnitOld((EducationOrgUnit) value);
                    return;
                case "groupEducationOrgUnitNew":
                    obj.setGroupEducationOrgUnitNew((EducationOrgUnit) value);
                    return;
                case "groupEducationOrgUnitOld":
                    obj.setGroupEducationOrgUnitOld((EducationOrgUnit) value);
                    return;
                case "groupChangeEducationOrgUnit":
                    obj.setGroupChangeEducationOrgUnit((Boolean) value);
                    return;
                case "recordNumber":
                    obj.setRecordNumber((String) value);
                    return;
                case "recordDate":
                    obj.setRecordDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                        return true;
                case "course":
                        return true;
                case "group":
                        return true;
                case "educationOrgUnitNew":
                        return true;
                case "educationOrgUnitOld":
                        return true;
                case "groupEducationOrgUnitNew":
                        return true;
                case "groupEducationOrgUnitOld":
                        return true;
                case "groupChangeEducationOrgUnit":
                        return true;
                case "recordNumber":
                        return true;
                case "recordDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return true;
                case "course":
                    return true;
                case "group":
                    return true;
                case "educationOrgUnitNew":
                    return true;
                case "educationOrgUnitOld":
                    return true;
                case "groupEducationOrgUnitNew":
                    return true;
                case "groupEducationOrgUnitOld":
                    return true;
                case "groupChangeEducationOrgUnit":
                    return true;
                case "recordNumber":
                    return true;
                case "recordDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return Date.class;
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "educationOrgUnitNew":
                    return EducationOrgUnit.class;
                case "educationOrgUnitOld":
                    return EducationOrgUnit.class;
                case "groupEducationOrgUnitNew":
                    return EducationOrgUnit.class;
                case "groupEducationOrgUnitOld":
                    return EducationOrgUnit.class;
                case "groupChangeEducationOrgUnit":
                    return Boolean.class;
                case "recordNumber":
                    return String.class;
                case "recordDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TransferDevConditionStuListExtract> _dslPath = new Path<TransferDevConditionStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TransferDevConditionStuListExtract");
    }
            

    /**
     * @return Дата перевода.
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#getTransferDate()
     */
    public static PropertyPath<Date> transferDate()
    {
        return _dslPath.transferDate();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#getEducationOrgUnitNew()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
    {
        return _dslPath.educationOrgUnitNew();
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#getEducationOrgUnitOld()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
    {
        return _dslPath.educationOrgUnitOld();
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП).
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#getGroupEducationOrgUnitNew()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> groupEducationOrgUnitNew()
    {
        return _dslPath.groupEducationOrgUnitNew();
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП).
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#getGroupEducationOrgUnitOld()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> groupEducationOrgUnitOld()
    {
        return _dslPath.groupEducationOrgUnitOld();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#isGroupChangeEducationOrgUnit()
     */
    public static PropertyPath<Boolean> groupChangeEducationOrgUnit()
    {
        return _dslPath.groupChangeEducationOrgUnit();
    }

    /**
     * @return Номер протокола заседания.
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#getRecordNumber()
     */
    public static PropertyPath<String> recordNumber()
    {
        return _dslPath.recordNumber();
    }

    /**
     * @return Дата протокола заседания.
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#getRecordDate()
     */
    public static PropertyPath<Date> recordDate()
    {
        return _dslPath.recordDate();
    }

    public static class Path<E extends TransferDevConditionStuListExtract> extends ListStudentExtract.Path<E>
    {
        private PropertyPath<Date> _transferDate;
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitNew;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitOld;
        private EducationOrgUnit.Path<EducationOrgUnit> _groupEducationOrgUnitNew;
        private EducationOrgUnit.Path<EducationOrgUnit> _groupEducationOrgUnitOld;
        private PropertyPath<Boolean> _groupChangeEducationOrgUnit;
        private PropertyPath<String> _recordNumber;
        private PropertyPath<Date> _recordDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата перевода.
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#getTransferDate()
     */
        public PropertyPath<Date> transferDate()
        {
            if(_transferDate == null )
                _transferDate = new PropertyPath<Date>(TransferDevConditionStuListExtractGen.P_TRANSFER_DATE, this);
            return _transferDate;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#getEducationOrgUnitNew()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
        {
            if(_educationOrgUnitNew == null )
                _educationOrgUnitNew = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_NEW, this);
            return _educationOrgUnitNew;
        }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#getEducationOrgUnitOld()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
        {
            if(_educationOrgUnitOld == null )
                _educationOrgUnitOld = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_OLD, this);
            return _educationOrgUnitOld;
        }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП).
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#getGroupEducationOrgUnitNew()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> groupEducationOrgUnitNew()
        {
            if(_groupEducationOrgUnitNew == null )
                _groupEducationOrgUnitNew = new EducationOrgUnit.Path<EducationOrgUnit>(L_GROUP_EDUCATION_ORG_UNIT_NEW, this);
            return _groupEducationOrgUnitNew;
        }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП).
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#getGroupEducationOrgUnitOld()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> groupEducationOrgUnitOld()
        {
            if(_groupEducationOrgUnitOld == null )
                _groupEducationOrgUnitOld = new EducationOrgUnit.Path<EducationOrgUnit>(L_GROUP_EDUCATION_ORG_UNIT_OLD, this);
            return _groupEducationOrgUnitOld;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#isGroupChangeEducationOrgUnit()
     */
        public PropertyPath<Boolean> groupChangeEducationOrgUnit()
        {
            if(_groupChangeEducationOrgUnit == null )
                _groupChangeEducationOrgUnit = new PropertyPath<Boolean>(TransferDevConditionStuListExtractGen.P_GROUP_CHANGE_EDUCATION_ORG_UNIT, this);
            return _groupChangeEducationOrgUnit;
        }

    /**
     * @return Номер протокола заседания.
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#getRecordNumber()
     */
        public PropertyPath<String> recordNumber()
        {
            if(_recordNumber == null )
                _recordNumber = new PropertyPath<String>(TransferDevConditionStuListExtractGen.P_RECORD_NUMBER, this);
            return _recordNumber;
        }

    /**
     * @return Дата протокола заседания.
     * @see ru.tandemservice.movestudent.entity.TransferDevConditionStuListExtract#getRecordDate()
     */
        public PropertyPath<Date> recordDate()
        {
            if(_recordDate == null )
                _recordDate = new PropertyPath<Date>(TransferDevConditionStuListExtractGen.P_RECORD_DATE, this);
            return _recordDate;
        }

        public Class getEntityClass()
        {
            return TransferDevConditionStuListExtract.class;
        }

        public String getEntityName()
        {
            return "transferDevConditionStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
