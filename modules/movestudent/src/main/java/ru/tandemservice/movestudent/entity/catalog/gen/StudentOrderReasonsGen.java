package ru.tandemservice.movestudent.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Причина приказа по студенту
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentOrderReasonsGen extends EntityBase
 implements INaturalIdentifiable<StudentOrderReasonsGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons";
    public static final String ENTITY_NAME = "studentOrderReasons";
    public static final int VERSION_HASH = 1768179795;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_PRINT_TITLE = "printTitle";
    public static final String P_COMMENTABLE = "commentable";
    public static final String P_COMMENT_REQUIRED = "commentRequired";
    public static final String P_HELP = "help";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _printTitle;     // Печатное название
    private boolean _commentable;     // Комментируемо
    private boolean _commentRequired;     // Примечание обязательно
    private String _help;     // Разъяснение
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Печатное название.
     */
    @Length(max=255)
    public String getPrintTitle()
    {
        return _printTitle;
    }

    /**
     * @param printTitle Печатное название.
     */
    public void setPrintTitle(String printTitle)
    {
        dirty(_printTitle, printTitle);
        _printTitle = printTitle;
    }

    /**
     * @return Комментируемо. Свойство не может быть null.
     */
    @NotNull
    public boolean isCommentable()
    {
        return _commentable;
    }

    /**
     * @param commentable Комментируемо. Свойство не может быть null.
     */
    public void setCommentable(boolean commentable)
    {
        dirty(_commentable, commentable);
        _commentable = commentable;
    }

    /**
     * @return Примечание обязательно. Свойство не может быть null.
     */
    @NotNull
    public boolean isCommentRequired()
    {
        return _commentRequired;
    }

    /**
     * @param commentRequired Примечание обязательно. Свойство не может быть null.
     */
    public void setCommentRequired(boolean commentRequired)
    {
        dirty(_commentRequired, commentRequired);
        _commentRequired = commentRequired;
    }

    /**
     * @return Разъяснение.
     */
    public String getHelp()
    {
        return _help;
    }

    /**
     * @param help Разъяснение.
     */
    public void setHelp(String help)
    {
        dirty(_help, help);
        _help = help;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentOrderReasonsGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((StudentOrderReasons)another).getCode());
            }
            setPrintTitle(((StudentOrderReasons)another).getPrintTitle());
            setCommentable(((StudentOrderReasons)another).isCommentable());
            setCommentRequired(((StudentOrderReasons)another).isCommentRequired());
            setHelp(((StudentOrderReasons)another).getHelp());
            setTitle(((StudentOrderReasons)another).getTitle());
        }
    }

    public INaturalId<StudentOrderReasonsGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<StudentOrderReasonsGen>
    {
        private static final String PROXY_NAME = "StudentOrderReasonsNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof StudentOrderReasonsGen.NaturalId) ) return false;

            StudentOrderReasonsGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentOrderReasonsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentOrderReasons.class;
        }

        public T newInstance()
        {
            return (T) new StudentOrderReasons();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "printTitle":
                    return obj.getPrintTitle();
                case "commentable":
                    return obj.isCommentable();
                case "commentRequired":
                    return obj.isCommentRequired();
                case "help":
                    return obj.getHelp();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "printTitle":
                    obj.setPrintTitle((String) value);
                    return;
                case "commentable":
                    obj.setCommentable((Boolean) value);
                    return;
                case "commentRequired":
                    obj.setCommentRequired((Boolean) value);
                    return;
                case "help":
                    obj.setHelp((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "printTitle":
                        return true;
                case "commentable":
                        return true;
                case "commentRequired":
                        return true;
                case "help":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "printTitle":
                    return true;
                case "commentable":
                    return true;
                case "commentRequired":
                    return true;
                case "help":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "printTitle":
                    return String.class;
                case "commentable":
                    return Boolean.class;
                case "commentRequired":
                    return Boolean.class;
                case "help":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentOrderReasons> _dslPath = new Path<StudentOrderReasons>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentOrderReasons");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Печатное название.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons#getPrintTitle()
     */
    public static PropertyPath<String> printTitle()
    {
        return _dslPath.printTitle();
    }

    /**
     * @return Комментируемо. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons#isCommentable()
     */
    public static PropertyPath<Boolean> commentable()
    {
        return _dslPath.commentable();
    }

    /**
     * @return Примечание обязательно. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons#isCommentRequired()
     */
    public static PropertyPath<Boolean> commentRequired()
    {
        return _dslPath.commentRequired();
    }

    /**
     * @return Разъяснение.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons#getHelp()
     */
    public static PropertyPath<String> help()
    {
        return _dslPath.help();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends StudentOrderReasons> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _printTitle;
        private PropertyPath<Boolean> _commentable;
        private PropertyPath<Boolean> _commentRequired;
        private PropertyPath<String> _help;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(StudentOrderReasonsGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Печатное название.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons#getPrintTitle()
     */
        public PropertyPath<String> printTitle()
        {
            if(_printTitle == null )
                _printTitle = new PropertyPath<String>(StudentOrderReasonsGen.P_PRINT_TITLE, this);
            return _printTitle;
        }

    /**
     * @return Комментируемо. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons#isCommentable()
     */
        public PropertyPath<Boolean> commentable()
        {
            if(_commentable == null )
                _commentable = new PropertyPath<Boolean>(StudentOrderReasonsGen.P_COMMENTABLE, this);
            return _commentable;
        }

    /**
     * @return Примечание обязательно. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons#isCommentRequired()
     */
        public PropertyPath<Boolean> commentRequired()
        {
            if(_commentRequired == null )
                _commentRequired = new PropertyPath<Boolean>(StudentOrderReasonsGen.P_COMMENT_REQUIRED, this);
            return _commentRequired;
        }

    /**
     * @return Разъяснение.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons#getHelp()
     */
        public PropertyPath<String> help()
        {
            if(_help == null )
                _help = new PropertyPath<String>(StudentOrderReasonsGen.P_HELP, this);
            return _help;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(StudentOrderReasonsGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return StudentOrderReasons.class;
        }

        public String getEntityName()
        {
            return "studentOrderReasons";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
