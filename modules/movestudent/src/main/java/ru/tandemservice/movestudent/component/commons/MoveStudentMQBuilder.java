/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.commons;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.movestudent.entity.gen.AbstractStudentExtractGen;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vip_delete
 * @since 23.12.2008
 * TODO optimize in DEV-3640 - переделать на DQL, доставать не сущности, а конкретные свойства (просмотреть какие вью-проперти припатчиваются в разных списках)
 */
public class MoveStudentMQBuilder implements IMoveStudentMQBuilder
{
    private IDataSettings _settings;
    private MQBuilder _builder;

    // MQBuilder factory methods

    public static <T extends IAbstractOrder> IMoveStudentMQBuilder createOrderMQBuilder(Class<T> orderClass, IDataSettings settings)
    {
        // TODO optimize in DEV-3640 - разделить по типам. Не делать запросы к Abstract-классам. Возможно, отказаться от поднятися сущностей.
        MoveStudentMQBuilder builder = new MoveStudentMQBuilder();
        builder._settings = settings;
        builder._builder = new MQBuilder(orderClass.getName(), ORDER_ALIAS);
        builder._builder.addJoin(ORDER_ALIAS, IAbstractOrder.L_STATE, STATE_ALIAS);
        return builder;
    }

    public static <T extends IAbstractExtract> IMoveStudentMQBuilder createExtractMQBuilder(Class<T> extractClass, IDataSettings settings)
    {
        MoveStudentMQBuilder builder = new MoveStudentMQBuilder();
        builder._settings = settings;
        builder._builder = new MQBuilder(extractClass.getName(), IMoveStudentMQBuilder.EXTRACT_ALIAS);
        builder._builder.addJoin(IMoveStudentMQBuilder.EXTRACT_ALIAS, IAbstractExtract.L_STATE, IMoveStudentMQBuilder.STATE_ALIAS);
        builder._builder.addLeftJoin(IMoveStudentMQBuilder.EXTRACT_ALIAS, IAbstractExtract.L_PARAGRAPH, IMoveStudentMQBuilder.PARAGRAPH_ALIAS);
        builder._builder.addLeftJoin(IMoveStudentMQBuilder.PARAGRAPH_ALIAS, IAbstractParagraph.L_ORDER, IMoveStudentMQBuilder.ORDER_ALIAS);
        builder._builder.addJoin(IMoveStudentMQBuilder.EXTRACT_ALIAS, IAbstractExtract.L_ENTITY, IMoveStudentMQBuilder.STUDENT_ALIAS);
        builder._builder.addJoin(IMoveStudentMQBuilder.STUDENT_ALIAS, Student.L_PERSON, "person");
        builder._builder.addJoin("person", Person.L_IDENTITY_CARD, IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS);
        builder._builder.addJoin(IMoveStudentMQBuilder.STUDENT_ALIAS, Student.L_EDUCATION_ORG_UNIT, IMoveStudentMQBuilder.EDUCATION_ORGUNIT_ALIAS);
        builder._builder.addJoin(IMoveStudentMQBuilder.EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, IMoveStudentMQBuilder.EDUCATION_LEVEL_HIGHSCHOOL_ALIAS);
        return builder;
    }

    // MQBuilder delegate method

    @Override
    public MQBuilder getMQBuilder()
    {
        return _builder;
    }

    // filter applyers ordered by name

    @Override
    public void applyListOrderType()
    {
        StudentExtractType type = _settings.get(TYPE);
        if (type != null)
            _builder.add(MQExpression.eq(ORDER_ALIAS, StudentListOrder.L_TYPE, type));
    }

    @Override
    public void applyListOrderTypes()
    {
        List<StudentExtractType> types = _settings.get(TYPE);
        if (types != null && !types.isEmpty())
            _builder.add(MQExpression.in(ORDER_ALIAS, StudentListOrder.L_TYPE, types));
    }

    @Override
    public void applyListOrderReason()
    {
        List<StudentOrderReasons> reasons = _settings.get(ORDER_REASON);
        if (reasons != null && !reasons.isEmpty())
            _builder.add(MQExpression.in(ORDER_ALIAS, StudentListOrder.L_REASON, reasons));
    }

    @Override
    public void applyOrderCommitDate()
    {
        Date commitDateFrom = _settings.get(COMMIT_DATE_FROM);
        Date commitDateTo = _settings.get(COMMIT_DATE_TO);

        if (commitDateFrom != null)
            _builder.add(MQExpression.greatOrEq(ORDER_ALIAS, AbstractStudentOrder.P_COMMIT_DATE, CoreDateUtils.getDayFirstTimeMoment(commitDateFrom)));

        if (commitDateTo != null)
            _builder.add(MQExpression.less(ORDER_ALIAS, AbstractStudentOrder.P_COMMIT_DATE, CoreDateUtils.getNextDayFirstTimeMoment(commitDateTo, 1)));
    }

    @Override
    public void applyOrderCommitDateSystem()
    {
        Date commitDateSystemFrom = _settings.get(COMMIT_DATE_SYSTEM_FROM);
        Date commitDateSystemTo = _settings.get(COMMIT_DATE_SYSTEM_TO);

        if (commitDateSystemFrom != null)
            _builder.add(MQExpression.greatOrEq(ORDER_ALIAS, IAbstractOrder.P_COMMIT_DATE_SYSTEM, CoreDateUtils.getDayFirstTimeMoment(commitDateSystemFrom)));

        if (commitDateSystemTo != null)
            _builder.add(MQExpression.less(ORDER_ALIAS, IAbstractOrder.P_COMMIT_DATE_SYSTEM, CoreDateUtils.getNextDayFirstTimeMoment(commitDateSystemTo, 1)));
    }

    @Override
    public void applyOrderCreateDate()
    {
        Date createDateFrom = _settings.get(CREATE_DATE_FROM);
        Date createDateTo = _settings.get(CREATE_DATE_TO);

        if (createDateFrom != null)
            _builder.add(MQExpression.greatOrEq(ORDER_ALIAS, IAbstractOrder.P_CREATE_DATE, CoreDateUtils.getDayFirstTimeMoment(createDateFrom)));

        if (createDateTo != null)
            _builder.add(MQExpression.less(ORDER_ALIAS, IAbstractOrder.P_CREATE_DATE, CoreDateUtils.getNextDayFirstTimeMoment(createDateTo, 1)));
    }

    @Override
    public void applyOrderEducationYear()
    {
        EducationYear educationYear = _settings.get(EDUCATION_YEAR);
        if (educationYear != null)
            _builder.add(UniMQExpression.eqEduYear(ORDER_ALIAS, IAbstractOrder.P_CREATE_DATE, educationYear));
    }

    @Override
    public void applyListOrderEducationYear()
    {
        EducationYear educationYear = _settings.get(EDUCATION_YEAR);
        if (educationYear != null)
            _builder.add(MQExpression.eq(ORDER_ALIAS, StudentListOrder.L_EDUCATION_YEAR, educationYear));
    }

    @Override
    public void applyOrderNumber()
    {
        String number = _settings.get(NUMBER);
        if (StringUtils.isNotEmpty(number))
            _builder.add(MQExpression.like(ORDER_ALIAS, IAbstractOrder.P_NUMBER, "%" + number));
    }

    @Override
    public void applyOrderOrgUnit()
    {
        OrgUnit orgUnit = _settings.get(ORG_UNIT);
        if (orgUnit != null)
            _builder.add(MQExpression.eq(ORDER_ALIAS, AbstractStudentOrder.L_ORG_UNIT, orgUnit));
    }

    @Override
    public void applyOrderState()
    {
        ICatalogItem state = _settings.get(STATE);
        if (state != null)
            _builder.add(MQExpression.eq(ORDER_ALIAS, IAbstractOrder.L_STATE, state));
    }

    @Override
    // TODO optimize in DEV-3640 - wtf?
    public void applyOrderStudent(Session session)
    {
        List<Student> students = _settings.get(STUDENTS);
        if (students != null && !students.isEmpty())
        {
            final int threshold = DQL.MAX_VALUES_ROW_NUMBER;

            List<Long> ordersIds = new MQBuilder(AbstractStudentExtract.ENTITY_CLASS, "e", new String[]{AbstractStudentExtract.paragraph().order().id().s()})
                    .add(MQExpression.in("e", AbstractStudentExtractGen.L_ENTITY, students))
                    .getResultList(session);

            if (ordersIds.size() < threshold)
            {
                _builder.add(MQExpression.in(ORDER_ALIAS, IAbstractOrder.P_ID, ordersIds));

            } else
            {
                final List<AbstractExpression> expressionList = new ArrayList<>();

                BatchUtils.execute(ordersIds, threshold, elements -> expressionList.add(MQExpression.in(ORDER_ALIAS, IAbstractOrder.P_ID, elements)));

                _builder.add(MQExpression.or(expressionList.toArray(new AbstractExpression[expressionList.size()])));
            }
        }
    }

    // extract filters ordered by name

    @Override
    public void applyExtractCompensationType()
    {
        CompensationType compensationType = _settings.get(COMPENSATION_TYPE);
        if (compensationType != null)
            _builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, compensationType));
    }

    @Override
    public void applyExtractCourse()
    {
        Course course = _settings.get(COURSE);
        if (course != null)
            _builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COURSE, course));
    }

    @Override
    public void applyExtractDevelopForm()
    {
        DevelopForm developForm = _settings.get(DEVELOP_FORM);
        if (developForm != null)
            _builder.add(MQExpression.eq(EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_DEVELOP_FORM, developForm));
    }

    @Override
    public void applyExtractCreateDate()
    {
        Date createDateFrom = _settings.get(CREATE_DATE_FROM);
        Date createDateTo = _settings.get(CREATE_DATE_TO);

        if (null == createDateFrom && null == createDateTo)
        {
            Date createDate = _settings.get(CREATE_DATE);
            if (null != createDate)
            {
                createDateFrom = createDate;
                createDateTo = createDate;
            }
        }

        if (createDateFrom != null)
            _builder.add(MQExpression.greatOrEq(EXTRACT_ALIAS, IAbstractExtract.P_CREATE_DATE, CoreDateUtils.getDayFirstTimeMoment(createDateFrom)));

        if (createDateTo != null)
            _builder.add(MQExpression.less(EXTRACT_ALIAS, IAbstractExtract.P_CREATE_DATE, CoreDateUtils.getNextDayFirstTimeMoment(createDateTo, 1)));
    }

    @Override
    public void applyExtractOrderDate()
    {
        Date commitDateFrom = _settings.get(COMMIT_DATE_FROM);
        Date commitDateTo = _settings.get(COMMIT_DATE_TO);

        if (commitDateFrom != null)
            _builder.add(MQExpression.greatOrEq(ORDER_ALIAS, AbstractStudentOrder.P_COMMIT_DATE, CoreDateUtils.getDayFirstTimeMoment(commitDateFrom)));

        if (commitDateTo != null)
            _builder.add(MQExpression.less(ORDER_ALIAS, AbstractStudentOrder.P_COMMIT_DATE, CoreDateUtils.getNextDayFirstTimeMoment(commitDateTo, 1)));
    }

    @Override
    public void applyExtractEducationLevelHighSchool()
    {
        EducationLevelsHighSchool educationLevelHighSchool = _settings.get(EDUCATION_LEVEL_HIGH_SCHOOL);
        if (educationLevelHighSchool != null)
            _builder.add(MQExpression.eq(EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, educationLevelHighSchool));
    }

    @Override
    public void applyExtractEducationYear()
    {
        EducationYear educationYear = _settings.get(EDUCATION_YEAR);
        if (educationYear != null)
            _builder.add(UniMQExpression.eqEduYear(EXTRACT_ALIAS, IAbstractExtract.P_CREATE_DATE, educationYear));
    }

    @Override
    public void applyExtractOrderEducationYear()
    {
        EducationYear educationYear = _settings.get(EDUCATION_YEAR);
        if (educationYear != null)
            _builder.add(UniMQExpression.eqEduYear(EXTRACT_ALIAS, IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + AbstractStudentOrder.P_COMMIT_DATE, educationYear));
    }

    @Override
    public void applyExtractFormativeOrgUnit()
    {
        OrgUnit formativeOrgUnit = _settings.get(FORMATIVE_ORGUNIT);
        if (formativeOrgUnit != null)
            _builder.add(MQExpression.eq(EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, formativeOrgUnit));
    }

    @Override
    public void applyExtractProducingOrgUnit()
    {
        OrgUnit producingOrgUnit = _settings.get(PRODUCING_ORGUNIT);
        if (producingOrgUnit != null)
            _builder.add(MQExpression.eq(EDUCATION_LEVEL_HIGHSCHOOL_ALIAS, EducationLevelsHighSchool.L_ORG_UNIT, producingOrgUnit));
    }

    @Override
    public void applyExtractState()
    {
        ExtractStates state = _settings.get(STATE);
        if (state != null)
            _builder.add(MQExpression.eq(EXTRACT_ALIAS, IAbstractExtract.L_STATE, state));
    }

    @Override
    public void applyExtractReason()
    {
        List<StudentOrderReasons> reasons = _settings.get(EXTRACT_REASON);
        if (reasons != null && !reasons.isEmpty())
            _builder.add(MQExpression.in(EXTRACT_ALIAS, ModularStudentExtract.L_REASON, reasons));
    }

    @Override
    public void applyOrderReason()
    {
        List<StudentOrderReasons> reasons = _settings.get(EXTRACT_REASON);
        if (reasons != null && !reasons.isEmpty())
            _builder.add(MQExpression.in(ORDER_ALIAS, StudentListOrder.L_REASON, reasons));
    }

    @Override
    public void applyExtractLastName()
    {
        String lastName = _settings.get(LAST_NAME);
        if (StringUtils.isNotEmpty(lastName))
            _builder.add(MQExpression.like(IDENTITY_CARD_ALIAS, IdentityCard.P_LAST_NAME, "%" + lastName));
    }

    @Override
    public void applyExtractTerritorialOrgUnit()
    {
        OrgUnit territorialOrgUnit = _settings.get(TERRITORIAL_ORGUNIT);
        if (territorialOrgUnit != null)
            _builder.add(MQExpression.eq(EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, territorialOrgUnit));
    }

    @Override
    public void applyExtractType()
    {
        List<StudentExtractType> types = _settings.get(TYPE);
        if (types != null && !types.isEmpty())
            _builder.add(MQExpression.in(EXTRACT_ALIAS, IAbstractExtract.L_TYPE, types));
    }

    @Override
    public void applyListExtractType()
    {
        List<StudentExtractType> types = _settings.get(TYPE);
        if (types != null && !types.isEmpty())
            _builder.add(MQExpression.in(ORDER_ALIAS, StudentListOrder.L_TYPE, types));
    }

    @Override
    public void applyExecutor()
    {
        EmployeePost executor = _settings.get(EXECUTOR);
        if(executor != null)
        {
            _builder.add(MQExpression.or(
                    MQExpression.like(ORDER_ALIAS, AbstractStudentOrder.P_EXECUTOR, "%" + executor.getPerson().getIdentityCard().getIof() + "%"),
                    MQExpression.eq(ORDER_ALIAS, AbstractStudentOrder.L_EXECUTOR_EMPL, executor)
            ));
        }
    }

    @Override
    public void applyExtractExecutor()
    {
        List<EmployeePost> executors=_settings.get(EXTRACT_EXECUTOR);
        if (executors != null && !executors.isEmpty())
        {
            _builder.add(MQExpression.in(EXTRACT_ALIAS, AbstractStudentExtract.L_EXECUTOR, executors));
        }
    }

    @Override
    // TODO optimize in DEV-3640 - wtf?
    public void applyExtractStudent(Session session)
    {
        List<Student> students = _settings.get(STUDENTS);
        if (students != null && !students.isEmpty())
        {
            final int threshold = DQL.MAX_VALUES_ROW_NUMBER;

            List<Long> extractsIds = new MQBuilder(AbstractStudentExtract.ENTITY_CLASS, "e", new String[]{AbstractStudentExtract.id().s()})
                    .add(MQExpression.in("e", AbstractStudentExtractGen.L_ENTITY, students))
                    .getResultList(session);

            if (extractsIds.size() < threshold)
            {
                _builder.add(MQExpression.in(EXTRACT_ALIAS, AbstractStudentExtract.P_ID, extractsIds));

            } else
            {
                final List<AbstractExpression> expressionList = new ArrayList<>();

                BatchUtils.execute(extractsIds, threshold, elements -> expressionList.add(MQExpression.in(EXTRACT_ALIAS, AbstractStudentExtract.P_ID, elements)));

                _builder.add(MQExpression.or(expressionList.toArray(new AbstractExpression[expressionList.size()])));
            }
        }
    }

    // create page

    @SuppressWarnings("unchecked")
    @Override
    public void createPage(Session session, OrderDescriptionRegistry orderDescriptionRegistry, DynamicListDataSource<? extends IEntity> dataSource)
    {
        MoveStudentUtils.createExtractPage(session, orderDescriptionRegistry, dataSource, _builder, IMoveStudentMQBuilder.EXTRACT_ALIAS);
    }

    public void applyEducationLevel(Session session, boolean typeOrderFormationOrFinished) {
        List<StructureEducationLevels> levelsList = _settings.get(EDUCATION_LEVEL_TYPE_LIST);
        if (levelsList != null && !levelsList.isEmpty()) {
            Set<Long> eduLevelTypeIdSet = getAllChildLevels(levelsList, session);
            MQBuilder mq = new MQBuilder(ListStudentExtract.ENTITY_CLASS, "lex");
            mq.add(MQExpression.eqProperty("lex", ListStudentExtract.paragraph().order().id().s(), ORDER_ALIAS, "id"));
            mq.add(MQExpression.in("lex", ListStudentExtract.entity().educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().id().s(), eduLevelTypeIdSet));
            _builder.add(MQExpression.exists(mq));
        }
    }

    public void applyEducationLevelIndividual(Session session) {
        List<StructureEducationLevels> levelsList = _settings.get(EDUCATION_LEVEL_TYPE_LIST);
        if (levelsList != null && !levelsList.isEmpty()) {
            Set<Long> eduLevelTypeIdSet = getAllChildLevels(levelsList, session);
            if (eduLevelTypeIdSet.size() != 0) {
                _builder.add(MQExpression.in(IMoveStudentMQBuilder.EXTRACT_ALIAS, ModularStudentExtract.entity().educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().id().s(), eduLevelTypeIdSet));
            }
        }
    }

    public static Set<Long> getAllChildLevels(List<StructureEducationLevels> rootLevels, Session session) {
        Set<Long> levelIds = new HashSet<>();
        Map<Long, Set<Long>> levelParentMap = SafeMap.get(HashSet.class);
        DQLSelectBuilder levelParentBuilder = new DQLSelectBuilder()
                .fromEntity(StructureEducationLevels.class, "s")
                .column(property("s", StructureEducationLevels.id()))
                .column(property("s", StructureEducationLevels.parent().id()));
        List<Object[]> l = levelParentBuilder.createStatement(session).list();
        for (Object[] row : l) {
            Long id = (Long) row[0];
            Long parentId = (Long) row[1];

            levelParentMap.get(parentId).add(id);
        }
        levelIds.addAll(UniBaseDao.ids(rootLevels));
        for (StructureEducationLevels lev : rootLevels) {
            collectValues(levelParentMap, lev.getId(), levelIds);
        }
        return levelIds;
    }

    private static void collectValues(Map<Long, Set<Long>> valueMap, Long currentValue, Set<Long> result) {
        Set<Long> values = valueMap.get(currentValue);
        result.addAll(values);
        for (Long value : values) {
            collectValues(valueMap, value, result);
        }
    }


}