/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e45;

import ru.tandemservice.movestudent.entity.AdmitToPassStatateExamStuListExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 01.06.2016
 */
public class AdmitToPassStatateExamStuListExtractDao extends UniBaseDao implements IExtractComponentDao<AdmitToPassStatateExamStuListExtract>
{
    @Override
    public void doCommit(AdmitToPassStatateExamStuListExtract extract, Map parameters)
    {
        //save print form
        //MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);
    }

    @Override
    public void doRollback(AdmitToPassStatateExamStuListExtract extract, Map parameters)
    {

    }
}