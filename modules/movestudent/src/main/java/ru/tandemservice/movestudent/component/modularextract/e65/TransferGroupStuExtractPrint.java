/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e65;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.TransferGroupStuExtract;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 28.08.2012
 */
public class TransferGroupStuExtractPrint implements IPrintFormCreator<TransferGroupStuExtract>
{
    private static final List<String> SPEC_QUALIFICATION_CODES = new ArrayList<String>();

    static
    {
        SPEC_QUALIFICATION_CODES.add(QualificationsCodes.SPETSIALIST);
        SPEC_QUALIFICATION_CODES.add(QualificationsCodes.BAZOVYY_UROVEN_S_P_O);
        SPEC_QUALIFICATION_CODES.add(QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O);
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, TransferGroupStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());

        if (null != qualification && SPEC_QUALIFICATION_CODES.contains(qualification.getCode()))
        {
            modifier.put("eduLevelText", "вышеуказанной специальности");
        } else
        {
            modifier.put("eduLevelText", "вышеуказанному направлению");
        }

        CommonExtractPrint.injectFefuEducationLevelNewText(modifier, "fefuEduLevelText", extract.getEducationOrgUnitOld(), extract.getEducationOrgUnitNew());

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}