/* $Id$ */
package ru.tandemservice.movestudent.component.modularextract.e105;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.AcceleratedLearningStuExtract;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 09.12.2015
 */
public class AcceleratedLearningStuExtractPrint implements IPrintFormCreator<AcceleratedLearningStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, AcceleratedLearningStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        String eduDocumentStr = "";
        PersonEduDocument personEduDocument = UniStudentManger.instance().sessionTransferProtocolDao()
                .getEduDocumentFromProtocol(extract.getProtocolNumber(), extract.getProtocolDate(), extract.getEntity());
        if (personEduDocument != null)
        {
            String seria = personEduDocument.getSeria();
            if (seria != null) seria = " серия " + seria;
            String number = personEduDocument.getNumber();
            if (number != null) number = " номер " + number;
            Date date = personEduDocument.getIssuanceDate();

            eduDocumentStr = StringUtils.uncapitalize(personEduDocument.getDocumentKindTitle())
                    + seria + number
                    + (date == null ? "" : " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(date));
        }

        modifier.put("eduDocument", eduDocumentStr)
                .put("entryIntoForceDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDate()))
                .put("protocolNumber", extract.getProtocolNumber())
                .put("protocolDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getProtocolDate()))
                .put("individualEduPlan", extract.getIndividualEduPlan());


        additionalModify(modifier, extract);
        modifier.modify(document);
        return document;
    }

    protected void additionalModify(RtfInjectModifier modifier, AcceleratedLearningStuExtract extract)
    {
    }
}