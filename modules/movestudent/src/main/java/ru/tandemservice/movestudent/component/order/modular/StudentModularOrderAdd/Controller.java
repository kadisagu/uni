/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.modular.StudentModularOrderAdd;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.component.commons.MoveStudentColumns;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;

/**
 * @author vip_delete
 * @since 20.10.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setSettings(UniBaseUtils.getDataSettings(component, "StudentModularOrderAdd.filter"));

        prepareListDataSource(component);

        model.setDisableCommitDate(!CoreServices.securityService().check(model.getOrder(), component.getUserContext().getPrincipalContext(), "editCommitDate"));
        model.setDisableOrderNumber(!CoreServices.securityService().check(model.getOrder(), component.getUserContext().getPrincipalContext(), "editOrderNumber"));
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        DynamicListDataSource dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new CheckboxColumn("checkbox").setSelectCaption("Выбор"));
        dataSource.addColumn(MoveStudentColumns.getCreateDateColumn());
        dataSource.addColumn(MoveStudentColumns.getFullFioColumn());
        dataSource.addColumn(MoveStudentColumns.getFormativeOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getTerritorialOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getProducingOrgUnitColumn());
        MoveStudentColumns.addEduLevelTitleColumns(dataSource);
        dataSource.addColumn(MoveStudentColumns.getCourseColumn());
        dataSource.addColumn(MoveStudentColumns.getGroupColumn());
        dataSource.addColumn(MoveStudentColumns.getDevelopFormColumn());
        dataSource.addColumn(MoveStudentColumns.getCompensationTypeColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractTypeColumn("Тип приказа"));
        dataSource.addColumn(MoveStudentColumns.getExtractPrintColumn(this, "printExtractFromModularOrderAddExtracts"));
        if(MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
            dataSource.addColumn(MoveStudentColumns.getExtractPrintPdfColumn(this, "printExtractFromModularOrderAddExtractsPdf"));
        dataSource.setOrder(IAbstractExtract.P_CREATE_DATE, OrderDirection.desc);
        getModel(component).setDataSource(dataSource);
    }

    public void onClickExtractPrint(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.MODULAR_STUDENT_EXTRACT_PRINT, new ParametersMap()
        .add("extractId", component.getListenerParameter())
        ));
    }

    public void onClickExtractPrintPdf(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.MODULAR_STUDENT_EXTRACT_PRINT, new ParametersMap()
                .add("extractId", component.getListenerParameter())
                .add("printPdf", Boolean.TRUE)
        ));
    }

    public synchronized final void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().update(model);
        MoveStudentDaoFacade.getMoveStudentDao().updateParagraphsNumber(model.getOrder());
        deactivate(component);
        activateInRoot(component, new PublisherActivator(model.getOrder()));
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getSettings().clear();
        onClickSearch(component);
    }
}
