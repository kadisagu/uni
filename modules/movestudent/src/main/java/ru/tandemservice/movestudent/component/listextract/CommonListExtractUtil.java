/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author vip_delete
 * @since 06.05.2009
 */
public class CommonListExtractUtil
{
    // Пропертя, показывающая поле вовода даты перевода приказов 2.39, 2.44
    public static final boolean SOME_LIST_EXTRACT_TRANSFER_DATE_VISIBLE = "true".equals(ApplicationRuntime.getProperty("movestudent.transfer-date-in-some-list-extract.enabled"));

    public static void doCommit(final IUniBaseDao dao, ListStudentExtract extract)
    {
        // создаем студента и заполняем его
        Student student = new Student();
        student.setCompensationType(extract.getEntity().getCompensationType());
        //student.setCourse(extract.getCourse());
        student.setEducationOrgUnit(extract.getEntity().getEducationOrgUnit());
        student.setDevelopPeriodAuto(extract.getEntity().getEducationOrgUnit().getDevelopPeriod());
        student.setEntranceYear(UniBaseUtils.getCurrentYear());
        student.setStudentCategory(extract.getEntity().getStudentCategory());
        //student.setPerson(extract.getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson());
        student.setStatus(dao.getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE));
        //student.setSpecialPurposeRecruit(extract.getEntity().isTargetAdmission());

        // сохраняем
        dao.save(student);

        // записываем его в выписку
        //extract.setStudentNew(student);

        // устанавливается состояние «Зачислен» для выбранного направления приема
        //extract.getEntity().getRequestedEnrollmentDirection().setState(dao.getCatalogItem(EntrantState.class, UniecDefines.ENTRANT_STATE_ENROLED_CODE));
        //dao.update(extract.getEntity().getRequestedEnrollmentDirection());
    }

    public static void doRollback(final IUniBaseDao dao, ListStudentExtract extract)
    {
        // удаляем студента
        //dao.delete(extract.getStudentNew());
        //extract.setStudentNew(null);

        // устанавливается состояние «В приказе» для выбранного направления приема
        //extract.getEntity().getRequestedEnrollmentDirection().setState(dao.getCatalogItem(EntrantState.class, UniecDefines.ENTRANT_STATE_IN_ORDER));
        //dao.update(extract.getEntity().getRequestedEnrollmentDirection());
    }

    public static String buildStudentTransferError(String messagePrefix, EducationOrgUnit educationOrgUnit)
    {
        StringBuilder errBuilder = new StringBuilder(messagePrefix).append(", поскольку ");
        errBuilder.append(educationOrgUnit.getTerritorialOrgUnit() instanceof TopOrgUnit ? educationOrgUnit.getFormativeOrgUnit().getFullTitle() : educationOrgUnit.getTerritorialOrgUnit().getTerritorialFullTitle());
        errBuilder.append(" не осуществляет подготовку по направлению «").append(educationOrgUnit.getTitle()).append("» для ");

        if (DevelopFormCodes.FULL_TIME_FORM.equals(educationOrgUnit.getDevelopForm().getCode()))
            errBuilder.append("очной формы, ");
        else if (DevelopFormCodes.CORESP_FORM.equals(educationOrgUnit.getDevelopForm().getCode()))
            errBuilder.append("заочной формы, ");
        else if (DevelopFormCodes.PART_TIME_FORM.equals(educationOrgUnit.getDevelopForm().getCode()))
            errBuilder.append("очно-заочной формы, ");
        else if (DevelopFormCodes.EXTERNAL_FORM.equals(educationOrgUnit.getDevelopForm().getCode()))
            errBuilder.append("экстерната, ");
        else if (DevelopFormCodes.APPLICANT_FORM.equals(educationOrgUnit.getDevelopForm().getCode()))
            errBuilder.append("самостоятельного обучения, ");

        if (UniDefines.DEVELOP_TECH_REMOTE.equals(educationOrgUnit.getDevelopTech().getCode()))
            errBuilder.append("дистанционного обучения, ");

        if (UniDefines.DEVELOP_CONDITION_FAST.equals(educationOrgUnit.getDevelopCondition().getCode()))
            errBuilder.append("ускоренной программы, ");
        else if (UniDefines.DEVELOP_CONDITION_SHORT.equals(educationOrgUnit.getDevelopCondition().getCode()))
            errBuilder.append("сокращенной программы, ");
        else if (UniDefines.DEVELOP_CONDITION_SHORT_FAST.equals(educationOrgUnit.getDevelopCondition().getCode()))
            errBuilder.append("сокращенной ускоренной программы, ");

        errBuilder.append("срока освоения ").append(educationOrgUnit.getDevelopPeriod().getTitle()).append(".");

        return errBuilder.toString();
    }
}