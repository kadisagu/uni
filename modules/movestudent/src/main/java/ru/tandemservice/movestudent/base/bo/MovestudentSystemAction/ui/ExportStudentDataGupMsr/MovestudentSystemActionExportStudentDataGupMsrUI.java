/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MovestudentSystemAction.ui.ExportStudentDataGupMsr;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.process.*;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.base.bo.MovestudentSystemAction.MovestudentSystemActionManager;
import ru.tandemservice.movestudent.base.bo.MovestudentSystemAction.logic.ScriptResultData;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;

import java.util.List;
import java.util.Map;


/**
 * @author Ekaterina Zvereva
 * @since 16.12.2014
 */
public class MovestudentSystemActionExportStudentDataGupMsrUI extends UIPresenter
{
    private boolean _unloadAllStudents;
    private boolean _onlyFullData;

    private ISelectModel _formativeOrgUnitModel;
    private OrgUnit _formativeOrgUnit;
    private List<StudentCustomStateCI> _studentCustomStateCIs;
    private ScriptResultData result = null;

    public boolean isOnlyFullData()
    {
        return _onlyFullData;
    }

    public void setOnlyFullData(boolean onlyFullData)
    {
        _onlyFullData = onlyFullData;
    }

    public boolean isUnloadAllStudents()
    {
        return _unloadAllStudents;
    }

    public void setUnloadAllStudents(boolean unloadAllStudents)
    {
        _unloadAllStudents = unloadAllStudents;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public List<StudentCustomStateCI> getStudentCustomStateCIs()
    {
        return _studentCustomStateCIs;
    }

    public void setStudentCustomStateCIs(List<StudentCustomStateCI> studentCustomStateCIs)
    {
        _studentCustomStateCIs = studentCustomStateCIs;
    }

    public ScriptResultData getResult()
    {
        return result;
    }

    public void setResult(ScriptResultData result)
    {
        this.result = result;
    }

    @Override
    public void onComponentRefresh()
    {
        setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
    }

    public void onClickApply()
    {
        BusinessComponentUtils.runProcess(new BackgroundProcessThread("Формирование отчета", new BackgroundProcessBase() {
            @Override
            public ProcessResult run(ProcessState state) {
                result = MovestudentSystemActionManager.instance().dao().generateUnloadingStudentsData(_formativeOrgUnit, _unloadAllStudents, _onlyFullData, _studentCustomStateCIs);
                return null;
            }
        }, ProcessDisplayMode.unknown));

    }


    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        if (result != null)
        {
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName(result.getFileName()).document(result.getContent()), true);
        }
    }

}
