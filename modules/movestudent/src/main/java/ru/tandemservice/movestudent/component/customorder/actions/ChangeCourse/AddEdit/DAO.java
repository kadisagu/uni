/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeCourse.AddEdit;

import ru.tandemservice.movestudent.component.customorder.IStudentCustomOrderExtract;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeCourseAction;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class DAO extends BaseCustomActionAddEditDAO<ChangeCourseAction, Model>
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        if (model.isActionActive())
        {
            model.setNewCourseList(DevelopGridDAO.getCourseList());
            if (model.getStudent() != null)
                model.getNewCourseList().remove(model.getStudent().getCourse());
        }
    }

    @Override
    protected Class<ChangeCourseAction> getActionClass()
    {
        return ChangeCourseAction.class;
    }

    @Override
    public void updatePrepare(Model model, ChangeCourseAction action, IStudentCustomOrderExtract extract)
    {
        Student student = (Student) extract.getEntity();
        action.setOldCourse(student.getCourse());
    }

    @Override
    public void doCommit(ChangeCourseAction action)
    {
        Student student = (Student) action.getCustomExtract().getEntity();

        action.setOldCourse(student.getCourse());
        update(action);

        student.setCourse(action.getNewCourse());
        update(student);
    }

    @Override
    public void doRollback(ChangeCourseAction action)
    {
        Student student = (Student) action.getCustomExtract().getEntity();
        student.setCourse(action.getOldCourse());
        update(student);
    }
}