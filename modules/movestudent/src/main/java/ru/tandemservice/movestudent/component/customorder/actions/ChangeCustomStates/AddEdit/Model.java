/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeCustomStates.AddEdit;

import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditModel;
import ru.tandemservice.movestudent.entity.custom.ChangeCustomStatesAction;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public class Model extends BaseCustomActionAddEditModel<ChangeCustomStatesAction>
{
    private IMultiSelectModel _oldStatesToDeleteModel;
    private List<StudentCustomStateCI> _oldStatesToDelete;
    private IMultiSelectModel _newStatesModel;
    private List<StudentCustomStateCI> _newStates;

    public List<StudentCustomStateCI> getOldStatesToDelete()
    {
        return _oldStatesToDelete;
    }

    public void setOldStatesToDelete(List<StudentCustomStateCI> oldStatesToDelete)
    {
        _oldStatesToDelete = oldStatesToDelete;
    }

    public List<StudentCustomStateCI> getNewStates()
    {
        return _newStates;
    }

    public void setNewStates(List<StudentCustomStateCI> newStates)
    {
        _newStates = newStates;
    }

    public IMultiSelectModel getNewStatesModel()
    {
        return _newStatesModel;
    }

    public void setNewStatesModel(IMultiSelectModel newStatesModel)
    {
        _newStatesModel = newStatesModel;
    }

    public IMultiSelectModel getOldStatesToDeleteModel()
    {
        return _oldStatesToDeleteModel;
    }

    public void setOldStatesToDeleteModel(IMultiSelectModel oldStatesToDeleteModel)
    {
        _oldStatesToDeleteModel = oldStatesToDeleteModel;
    }
}