package ru.tandemservice.movestudent.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тип выписки по студенту
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentExtractTypeGen extends EntityBase
 implements INaturalIdentifiable<StudentExtractTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.catalog.StudentExtractType";
    public static final String ENTITY_NAME = "studentExtractType";
    public static final int VERSION_HASH = 994897419;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_ACTIVE = "active";
    public static final String P_INDEX = "index";
    public static final String P_BUSINESS_OBJECT_NAME = "businessObjectName";
    public static final String P_DESCRIPTION = "description";
    public static final String L_PARENT = "parent";
    public static final String P_TITLE = "title";
    public static final String P_TITLE_WITH_CODE = "titleWithCode";

    private String _code;     // Системный код
    private boolean _active;     // Активный
    private Integer _index;     // Индекс
    private String _businessObjectName;     // Тип бизнес-объекта
    private String _description;     // Описание
    private StudentExtractType _parent;     // Тип выписки по студенту
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Активный. Свойство не может быть null.
     */
    @NotNull
    public boolean isActive()
    {
        return _active;
    }

    /**
     * @param active Активный. Свойство не может быть null.
     */
    public void setActive(boolean active)
    {
        dirty(_active, active);
        _active = active;
    }

    /**
     * @return Индекс.
     */
    public Integer getIndex()
    {
        return _index;
    }

    /**
     * @param index Индекс.
     */
    public void setIndex(Integer index)
    {
        dirty(_index, index);
        _index = index;
    }

    /**
     * @return Тип бизнес-объекта.
     */
    @Length(max=255)
    public String getBusinessObjectName()
    {
        return _businessObjectName;
    }

    /**
     * @param businessObjectName Тип бизнес-объекта.
     */
    public void setBusinessObjectName(String businessObjectName)
    {
        dirty(_businessObjectName, businessObjectName);
        _businessObjectName = businessObjectName;
    }

    /**
     * @return Описание.
     */
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Описание.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Тип выписки по студенту.
     */
    public StudentExtractType getParent()
    {
        return _parent;
    }

    /**
     * @param parent Тип выписки по студенту.
     */
    public void setParent(StudentExtractType parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentExtractTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((StudentExtractType)another).getCode());
            }
            setActive(((StudentExtractType)another).isActive());
            setIndex(((StudentExtractType)another).getIndex());
            setBusinessObjectName(((StudentExtractType)another).getBusinessObjectName());
            setDescription(((StudentExtractType)another).getDescription());
            setParent(((StudentExtractType)another).getParent());
            setTitle(((StudentExtractType)another).getTitle());
        }
    }

    public INaturalId<StudentExtractTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<StudentExtractTypeGen>
    {
        private static final String PROXY_NAME = "StudentExtractTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof StudentExtractTypeGen.NaturalId) ) return false;

            StudentExtractTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentExtractTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentExtractType.class;
        }

        public T newInstance()
        {
            return (T) new StudentExtractType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "active":
                    return obj.isActive();
                case "index":
                    return obj.getIndex();
                case "businessObjectName":
                    return obj.getBusinessObjectName();
                case "description":
                    return obj.getDescription();
                case "parent":
                    return obj.getParent();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "active":
                    obj.setActive((Boolean) value);
                    return;
                case "index":
                    obj.setIndex((Integer) value);
                    return;
                case "businessObjectName":
                    obj.setBusinessObjectName((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "parent":
                    obj.setParent((StudentExtractType) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "active":
                        return true;
                case "index":
                        return true;
                case "businessObjectName":
                        return true;
                case "description":
                        return true;
                case "parent":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "active":
                    return true;
                case "index":
                    return true;
                case "businessObjectName":
                    return true;
                case "description":
                    return true;
                case "parent":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "active":
                    return Boolean.class;
                case "index":
                    return Integer.class;
                case "businessObjectName":
                    return String.class;
                case "description":
                    return String.class;
                case "parent":
                    return StudentExtractType.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentExtractType> _dslPath = new Path<StudentExtractType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentExtractType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentExtractType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Активный. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentExtractType#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    /**
     * @return Индекс.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentExtractType#getIndex()
     */
    public static PropertyPath<Integer> index()
    {
        return _dslPath.index();
    }

    /**
     * @return Тип бизнес-объекта.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentExtractType#getBusinessObjectName()
     */
    public static PropertyPath<String> businessObjectName()
    {
        return _dslPath.businessObjectName();
    }

    /**
     * @return Описание.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentExtractType#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Тип выписки по студенту.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentExtractType#getParent()
     */
    public static StudentExtractType.Path<StudentExtractType> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentExtractType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudent.entity.catalog.StudentExtractType#getTitleWithCode()
     */
    public static SupportedPropertyPath<String> titleWithCode()
    {
        return _dslPath.titleWithCode();
    }

    public static class Path<E extends StudentExtractType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Boolean> _active;
        private PropertyPath<Integer> _index;
        private PropertyPath<String> _businessObjectName;
        private PropertyPath<String> _description;
        private StudentExtractType.Path<StudentExtractType> _parent;
        private PropertyPath<String> _title;
        private SupportedPropertyPath<String> _titleWithCode;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentExtractType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(StudentExtractTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Активный. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentExtractType#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(StudentExtractTypeGen.P_ACTIVE, this);
            return _active;
        }

    /**
     * @return Индекс.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentExtractType#getIndex()
     */
        public PropertyPath<Integer> index()
        {
            if(_index == null )
                _index = new PropertyPath<Integer>(StudentExtractTypeGen.P_INDEX, this);
            return _index;
        }

    /**
     * @return Тип бизнес-объекта.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentExtractType#getBusinessObjectName()
     */
        public PropertyPath<String> businessObjectName()
        {
            if(_businessObjectName == null )
                _businessObjectName = new PropertyPath<String>(StudentExtractTypeGen.P_BUSINESS_OBJECT_NAME, this);
            return _businessObjectName;
        }

    /**
     * @return Описание.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentExtractType#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(StudentExtractTypeGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Тип выписки по студенту.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentExtractType#getParent()
     */
        public StudentExtractType.Path<StudentExtractType> parent()
        {
            if(_parent == null )
                _parent = new StudentExtractType.Path<StudentExtractType>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentExtractType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(StudentExtractTypeGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudent.entity.catalog.StudentExtractType#getTitleWithCode()
     */
        public SupportedPropertyPath<String> titleWithCode()
        {
            if(_titleWithCode == null )
                _titleWithCode = new SupportedPropertyPath<String>(StudentExtractTypeGen.P_TITLE_WITH_CODE, this);
            return _titleWithCode;
        }

        public Class getEntityClass()
        {
            return StudentExtractType.class;
        }

        public String getEntityName()
        {
            return "studentExtractType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitleWithCode();
}
