/* $Id$ */
package ru.tandemservice.movestudent.component.commons.gradation;

import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;

/**
 * @author Nikolay Fedorovskih
 * @since 04.07.2013
 */
public interface IPracticeOutExtract extends IPracticeExtract
{
    ExternalOrgUnit getPracticeExtOrgUnit();
}