package ru.tandemservice.movestudent.entity.catalog;

import ru.tandemservice.movestudent.entity.catalog.gen.StudentOrderReasonsGen;

public class StudentOrderReasons extends StudentOrderReasonsGen
{
    public static final String P_COMMENT_REQUIRED_DISABLED = "commentRequiredDisabled";

    public boolean isCommentRequiredDisabled()
    {
        return !isCommentable();
    }
}