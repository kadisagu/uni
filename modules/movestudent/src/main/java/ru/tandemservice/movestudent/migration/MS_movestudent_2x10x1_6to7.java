package ru.tandemservice.movestudent.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_movestudent_2x10x1_6to7 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность admitToPassQualificationWorkStuListExtract

        // удалено свойство educationOrgUnit
        {
            // удалить колонку
            tool.dropColumn("dmttpssqlfctnwrkstlstextrct_t", "educationorgunit_id");
        }

        // создано обязательное свойство educationLevelsHighSchool
        if (tool.tableExists("dmttpssqlfctnwrkstlstextrct_t"))
        {
            // создать колонку
            tool.createColumn("dmttpssqlfctnwrkstlstextrct_t", new DBColumn("educationlevelshighschool_id", DBType.LONG));

            // сделать колонку NOT NULL
            tool.setColumnNullable("dmttpssqlfctnwrkstlstextrct_t", "educationlevelshighschool_id", false);

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность admitToPassStatateExamStuListExtract

        // удалено свойство educationOrgUnit
        {
            // удалить колонку
            tool.dropColumn("dmttpssstttexmstlstextrct_t", "educationorgunit_id");
        }

        // создано обязательное свойство educationLevelsHighSchool
        if (tool.tableExists("dmttpssstttexmstlstextrct_t"))
        {
            // создать колонку
            tool.createColumn("dmttpssstttexmstlstextrct_t", new DBColumn("educationlevelshighschool_id", DBType.LONG));

            // сделать колонку NOT NULL
            tool.setColumnNullable("dmttpssstttexmstlstextrct_t", "educationlevelshighschool_id", false);
        }
    }
}