/*$Id$*/
package ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEditAlternative;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.ListStudentExtract;

/**
 * @author DMITRY KNYAZEV
 * @since 30.05.2014
 */
public interface IAbstractParagraphAddEditAlternativeDAO<T extends ListStudentExtract, Model extends AbstractParagraphAddEditAlternativeModel<T>> extends IAbstractListParagraphAddEditDAO<T, Model>
{
}
