/* $Id$ */
package ru.tandemservice.movestudent.component.commons;

import java.util.Collection;

/**
 * @author Nikolay Fedorovskih
 * @since 24.12.2013
 */
public interface IOrderCategoryFilteredModel
{
    Collection<OrderCategory> getSelectedCategories();
}