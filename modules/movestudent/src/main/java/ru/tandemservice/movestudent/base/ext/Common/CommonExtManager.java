/* $Id:$ */
package ru.tandemservice.movestudent.base.ext.Common;

import org.apache.commons.collections15.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.ModuleStatusReportBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.util.IModuleStatusReporter;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ExtractCreationRule;
import ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;
import ru.tandemservice.unimv.entity.visa.VisaHistoryItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/11/14
 */
@Configuration
public class CommonExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CommonManager _commonManager;

    @Bean
    public ItemListExtension<IModuleStatusReporter> moduleStatusExtPoint()
    {
        return itemListExtension(_commonManager.moduleStatusExtPoint())
            .add("movestudent", () -> {
                IUniBaseDao dao = IUniBaseDao.instance.get();
                List<String> result = new ArrayList<>();

                String alias = "a";

                List<Object[]> commitedList = dao.getList(new DQLSelectBuilder()
                                .fromEntity(AbstractStudentExtract.class, alias)
                                .where(eq(property(alias, AbstractStudentExtract.state().code()), value(ExtractStatesCodes.FINISHED)))
                                .column(property(alias, AbstractStudentExtract.type().title()))
                                .column(DQLFunctions.year(property(alias, AbstractStudentExtract.createDate())))
                                .column(DQLFunctions.count(property(alias, AbstractStudentExtract.id())))
                                .group(property(alias, AbstractStudentExtract.type().title()))
                                .group(DQLFunctions.year(property(alias, AbstractStudentExtract.createDate())))
                                .order(property(alias, AbstractStudentExtract.type().title()))
                                .order(DQLFunctions.year(property(alias, AbstractStudentExtract.createDate())))
                                .distinct()
                );

                final Collection<String> commited = CollectionUtils.collect(commitedList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER);

                List<Object[]> acceptedList = dao.getList(new DQLSelectBuilder()
                                .fromEntity(AbstractStudentExtract.class, alias)
                                .where(eq(property(alias, AbstractStudentExtract.state().code()), value(ExtractStatesCodes.ACCEPTED)))
                                .column(property(alias, AbstractStudentExtract.type().title()))
                                .column(DQLFunctions.year(property(alias, AbstractStudentExtract.createDate())))
                                .column(DQLFunctions.count(property(alias, AbstractStudentExtract.id())))
                                .group(property(alias, AbstractStudentExtract.type().title()))
                                .group(DQLFunctions.year(property(alias, AbstractStudentExtract.createDate())))
                                .order(property(alias, AbstractStudentExtract.type().title()))
                                .order(DQLFunctions.year(property(alias, AbstractStudentExtract.createDate())))
                                .distinct()
                );

                final Collection<String> accepted = CollectionUtils.collect(acceptedList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER);

                List<Object[]> decanatDivList = dao.getList(new DQLSelectBuilder()
                                .fromEntity(AbstractStudentExtract.class, alias)
                                .where(eq(property(alias, AbstractStudentExtract.state().code()), value(ExtractStatesCodes.FINISHED)))
                                .column(property(alias, AbstractStudentExtract.entity().educationOrgUnit().groupOrgUnit().title()))
                                .column(DQLFunctions.year(property(alias, AbstractStudentExtract.createDate())))
                                .column(DQLFunctions.count(property(alias, AbstractStudentExtract.id())))
                                .group(property(alias, AbstractStudentExtract.entity().educationOrgUnit().groupOrgUnit().title()))
                                .group(DQLFunctions.year(property(alias, AbstractStudentExtract.createDate())))
                                .order(property(alias, AbstractStudentExtract.entity().educationOrgUnit().groupOrgUnit().title()))
                                .order(DQLFunctions.year(property(alias, AbstractStudentExtract.createDate())))
                                .distinct()
                );

                final Collection<String> decanatDiv = CollectionUtils.collect(decanatDivList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER);

                result.add("Число проведенных выписок: ");
                result.addAll(commited);
                result.add("Число согласованных выписок: ");
                result.addAll(accepted);
                result.add("Число пользовательских шаблонов в справочнике: " + dao.getCount(new DQLSelectBuilder().fromEntity(MovestudentTemplate.class, alias)
                                .where(isNotNull(property(alias, MovestudentTemplate.document()))))
                );
                result.add("Число проведенных выписок по деканатам:");
                result.addAll(decanatDiv);

                final List<String> extractCreationWithIndividualOrder = dao.getList(new DQLSelectBuilder().fromEntity(ExtractCreationRule.class, alias)
                                .where(eq(property(alias, ExtractCreationRule.individualOrder()), value(true)))
                                .column(property(alias, ExtractCreationRule.studentExtractType().title()))
                                .distinct()
                );
                result.add("Типы выписок, для которых приказ может формироваться индивидуально:");
                result.addAll(extractCreationWithIndividualOrder);

                result.add("Используется ли визирование (число выписок с визами): " + dao.getCount(new DQLSelectBuilder()
                        .fromEntity(AbstractStudentExtract.class, alias)
                        .joinEntity(alias, DQLJoinType.inner, VisaHistoryItem.class, "v", eq(property("v", VisaHistoryItem.document().id()), property(alias, AbstractStudentExtract.id())))
                        .column(property(alias, AbstractStudentExtract.id()))
                        .distinct()
                ));

                return result;
            })
            .create();
    }
}
