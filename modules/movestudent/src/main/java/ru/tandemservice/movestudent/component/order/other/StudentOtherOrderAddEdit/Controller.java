/* $Id$ */
package ru.tandemservice.movestudent.component.order.other.StudentOtherOrderAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author Dmitry Seleznev
 * @since 11.04.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        getDao().initFieldSettings(getModel(component));
    }

    public void onSelectExtractType(IBusinessComponent component)
    {
        getDao().initFieldSettings(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
        deactivate(component);
    }
}