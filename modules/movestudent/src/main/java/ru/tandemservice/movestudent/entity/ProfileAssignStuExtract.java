package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.ProfileAssignStuExtractGen;

import java.util.Date;

/**
 * О закреплении за специализацией (профилем)
 */
public class ProfileAssignStuExtract extends ProfileAssignStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}