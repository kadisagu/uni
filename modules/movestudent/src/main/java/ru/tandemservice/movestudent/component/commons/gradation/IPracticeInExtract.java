/* $Id$ */
package ru.tandemservice.movestudent.component.commons.gradation;

import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author Nikolay Fedorovskih
 * @since 04.07.2013
 */
public interface IPracticeInExtract extends IPracticeExtract
{
    OrgUnit getPracticeOrgUnit();
}