package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.movestudent.entity.gen.ExcludeStuListExtractGen;

import java.util.Date;

/**
 * excludeStuListExtract
 */
public class ExcludeStuListExtract extends ExcludeStuListExtractGen implements IExcludeExtract
{
    @Override
    public Date getBeginDate()
    {
        return getExcludeDate();
    }
}