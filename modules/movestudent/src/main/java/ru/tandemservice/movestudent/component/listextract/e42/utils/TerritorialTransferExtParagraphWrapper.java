/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e42.utils;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 28.08.2013
 */
public class TerritorialTransferExtParagraphWrapper implements Comparable<TerritorialTransferExtParagraphWrapper>
{
    private final Course _course;
    private final StudentCategory _studentCategory;
    private final Group _groupOld;
    private final Group _groupNew;
    private final CompensationType _compensationTypeOld;
    private final CompensationType _compensationTypeNew;
    private final EducationOrgUnit _educationOrgUnitOld;
    private final EducationOrgUnit _educationOrgUnitNew;
    private final DevelopForm _developForm;
    private final ListStudentExtract _firstExtract;

    private final List<Person> _personList = new ArrayList<>();

    public TerritorialTransferExtParagraphWrapper(Course course, StudentCategory studentCategory, Group groupOld, Group groupNew, CompensationType compensationTypeOld, CompensationType compensationTypeNew, EducationOrgUnit educationOrgUnitOld, EducationOrgUnit educationOrgUnitNew, DevelopForm developForm, ListStudentExtract firstExtract)
    {
        _course = course;
        _studentCategory = studentCategory;
        _groupOld = groupOld;
        _groupNew = groupNew;
        _compensationTypeOld = compensationTypeOld;
        _compensationTypeNew = compensationTypeNew;
        _educationOrgUnitOld = educationOrgUnitOld;
        _educationOrgUnitNew = educationOrgUnitNew;
        _developForm = developForm;
        _firstExtract = firstExtract;
    }

    public Course getCourse()
    {
        return _course;
    }

    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    public Group getGroupOld()
    {
        return _groupOld;
    }

    public Group getGroupNew()
    {
        return _groupNew;
    }

    public CompensationType getCompensationTypeOld()
    {
        return _compensationTypeOld;
    }

    public CompensationType getCompensationTypeNew()
    {
        return _compensationTypeNew;
    }

    public EducationOrgUnit getEducationOrgUnitOld()
    {
        return _educationOrgUnitOld;
    }

    public EducationOrgUnit getEducationOrgUnitNew()
    {
        return _educationOrgUnitNew;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public List<Person> getPersonList()
    {
        return _personList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof TerritorialTransferExtParagraphWrapper))
            return false;

        TerritorialTransferExtParagraphWrapper that = (TerritorialTransferExtParagraphWrapper) o;

        return _course.equals(that.getCourse())
                && _studentCategory.equals(that.getStudentCategory())
                && _groupOld.equals(that.getGroupOld())
                && _groupNew.equals(that.getGroupNew())
                && _compensationTypeOld.equals(that.getCompensationTypeOld())
                && _compensationTypeNew.equals(that.getCompensationTypeNew())
                && _educationOrgUnitOld.equals(that.getEducationOrgUnitOld())
                && _educationOrgUnitNew.equals(that.getEducationOrgUnitNew())
                && _developForm.equals(that.getDevelopForm());
    }

    @Override
    public int compareTo(TerritorialTransferExtParagraphWrapper o)
    {
        int result = 0;
        if (_course.getIntValue() > o.getCourse().getIntValue())
            return 1;
        else if (_course.getIntValue() < o.getCourse().getIntValue())
            return -1;
        else
        {
            if (StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(_studentCategory.getCode()) && StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(o.getStudentCategory().getCode()))
                return -1;
            else if (StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(_studentCategory.getCode()) && StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(o.getStudentCategory().getCode()))
                return 1;
            else
            {
                boolean isThisBudgetOld = CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(_compensationTypeOld.getCode());
                boolean isThisBudgetNew = CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(_compensationTypeNew.getCode());
                boolean isThisContractOld = CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(_compensationTypeOld.getCode());
                boolean isThisContractNew = CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(_compensationTypeNew.getCode());

                boolean isThatBudgetOld = CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(o.getCompensationTypeOld().getCode());
                boolean isThatBudgetNew = CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(o.getCompensationTypeNew().getCode());
                boolean isThatContractOld = CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(o.getCompensationTypeOld().getCode());
                boolean isThatContractNew = CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(o.getCompensationTypeNew().getCode());

                if (isThisBudgetOld && isThatContractOld) return -1;
                else if (isThisContractOld && isThatBudgetOld) return 1;
                else
                {
                    if (isThisBudgetOld)
                    {
                        if (isThisBudgetOld && isThatContractNew) return -1;
                        else if (isThisContractNew && isThatBudgetOld) return 1;
                        else
                        {
                            if (result == 0)
                                result = _educationOrgUnitOld.getEducationLevelHighSchool().getTitle().compareTo(o.getEducationOrgUnitOld().getEducationLevelHighSchool().getTitle());
                            if (result == 0)
                                result = _educationOrgUnitNew.getEducationLevelHighSchool().getTitle().compareTo(o.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle());
                        }
                    }
                    else if (isThisContractOld)
                    {
                        if (isThisContractOld && isThatBudgetNew) return -1;
                        else if (isThisBudgetNew && isThatContractOld) return 1;
                        else
                        {
                            if (result == 0)
                                result = _educationOrgUnitOld.getEducationLevelHighSchool().getTitle().compareTo(o.getEducationOrgUnitOld().getEducationLevelHighSchool().getTitle());
                            if (result == 0)
                                result = _educationOrgUnitNew.getEducationLevelHighSchool().getTitle().compareTo(o.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle());
                        }
                    }
                }
            }
        }
        return result;
    }
}
