/* $Id: Model.java.vm 6933 2009-03-07 05:41:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e34.AddEdit;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.form.validator.BaseValidator;
import org.apache.tapestry.form.validator.Required;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.ChangeFirstNameStuExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 18.12.2009
 */
public class Model extends CommonModularStudentExtractAddEditModel<ChangeFirstNameStuExtract>
{
	private ISelectModel _citizenshipModel;
	private List<IdentityCardType> _identityCardTypeList;
	private ISelectModel _nationalityModel;
	private List<Sex> _sexList;

    public ISelectModel getCitizenshipModel()
    {
        return _citizenshipModel;
    }

    public void setCitizenshipModel(ISelectModel citizenshipModel)
    {
        _citizenshipModel = citizenshipModel;
    }

    public List<IdentityCardType> getIdentityCardTypeList()
	{
		return _identityCardTypeList;
	}

	public void setIdentityCardTypeList(final List<IdentityCardType> identityCardTypeList)
	{
		_identityCardTypeList = identityCardTypeList;
	}

    public ISelectModel getNationalityModel()
    {
        return _nationalityModel;
    }

    public void setNationalityModel(ISelectModel nationalityModel)
    {
        _nationalityModel = nationalityModel;
    }

    public List<Sex> getSexList()
	{
		return _sexList;
	}

	public void setSexList(final List<Sex> sexList)
	{
		_sexList = sexList;
	}

    public List<BaseValidator> getRequiredMiddleNameValidators ()
    {
        List<BaseValidator> validatorList = new ArrayList<>();
        if (getExtract().getCardType() != null)
        {
            validatorList = getExtract().getCardType().getMiddleNameValidators();
        }
        return validatorList;
    }

    public List<BaseValidator> getLastNameValidators ()
    {
        if (getExtract().getCardType() != null)
        {
            return getExtract().getCardType().getLastNameValidators();
        }
        else
        {
            List<BaseValidator> validatorList = new ArrayList<>();
            validatorList.add(new Required());
            return validatorList;
        }
    }

    public List<BaseValidator> getFirstNameValidators ()
    {
        if (getExtract().getCardType() != null)
        {
            return getExtract().getCardType().getFirstNameValidators();
        }
        else
        {
            List<BaseValidator> validatorList = new ArrayList<>();
            validatorList.add(new Required());
            return validatorList;
        }
    }
}
