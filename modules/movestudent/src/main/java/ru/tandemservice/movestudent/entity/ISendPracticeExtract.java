/* $Id:$ */
package ru.tandemservice.movestudent.entity;

import ru.tandemservice.uni.entity.catalog.Course;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 2/27/13
 * Time: 1:27 PM
 */
public interface ISendPracticeExtract
{
    String getPracticePlaceFromExtr();
    Date getPracticeBeginDateFromExtr();
    Date getPracticeEndDateFromExtr();
    Course getPracticeCourseFromExtr();
    String getPracticeDurationFromExtr();
}
