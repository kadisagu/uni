package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IEnrollmentExtract;
import ru.tandemservice.movestudent.entity.gen.RestorationCourseStuExtractGen;

import java.util.Date;

/**
 * О восстановлении
 */
public class RestorationCourseStuExtract extends RestorationCourseStuExtractGen implements IEnrollmentExtract
{
    @Override
    public Date getEntranceDate()
    {
        return getRestorationDate();
    }
}