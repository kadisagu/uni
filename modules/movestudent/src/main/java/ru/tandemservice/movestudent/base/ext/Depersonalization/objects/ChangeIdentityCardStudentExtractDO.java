/* $Id$ */
package ru.tandemservice.movestudent.base.ext.Depersonalization.objects;

import com.google.common.collect.ImmutableList;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.application.IModuleMeta;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.DepersonalizationUtils;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.IDepersonalizationObject;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 13.11.2014
 */
public class ChangeIdentityCardStudentExtractDO implements IDepersonalizationObject
{
    public static final String NAME = ChangeIdentityCardStudentExtractDO.class.getSimpleName();

    @Override
    public String getTitle()
    {
        return "Данные по удостоверениям личности и ФИО из выписок";
    }

    @Override
    public Collection<Class<? extends IEntity>> getEntityClasses()
    {
        final ImmutableList.Builder<Class<? extends IEntity>> builder = ImmutableList.builder();
        return builder
                .add(ChangeIdentityCardAction.class)
                .add(ChangeFioStuExtract.class)
                .add(ChangeFirstNameStuExtract.class)
                .add(ChangeMiddleNameStuExtract.class)
                .add(AdoptRussianCitizenshipStuExtract.class)
                .add(StuExtractToComissionMember.class)
                .build();
    }

    @Override
    public String getSettingsName()
    {
        return NAME;
    }

    @Override
    public Collection<String> getDescription()
    {
        return ImmutableList.of(
                "ФИО заменяется на " + REPLACEMENT_FIO + ".",
                "Серия УЛ заменяется на " + REPLACEMENT_SERIA + ".",
                "Номер УЛ заменяется на " + REPLACEMENT_NUMBER + ".",
                "Место и дата рождения, место и дата выдачи УЛ удаляются."
        );
    }

    @Override
    public IModuleMeta getModule()
    {
        return DepersonalizationUtils.getModuleMeta(ChangeFioStuExtract.class);
    }

    @Override
    public void apply(Session session)
    {
        CommonDAO.executeAndClear(new DQLUpdateBuilder(ChangeIdentityCardAction.class)
                .set(ChangeIdentityCardAction.P_LAST_NAME_NEW, value(REPLACEMENT_LAST_NAME))
                .set(ChangeIdentityCardAction.P_FIRST_NAME_NEW, value(REPLACEMENT_FIRST_NAME))
                .set(ChangeIdentityCardAction.P_MIDDLE_NAME_NEW, value(REPLACEMENT_MIDDLE_NAME))
                .set(ChangeIdentityCardAction.P_CARD_SERIA, value(REPLACEMENT_SERIA))
                .set(ChangeIdentityCardAction.P_CARD_NUMBER, value(REPLACEMENT_NUMBER))
                .set(ChangeIdentityCardAction.P_BIRTH_DATE, nul(PropertyType.DATE))
                .set(ChangeIdentityCardAction.P_BIRTH_PLACE, nul(PropertyType.STRING))
                .set(ChangeIdentityCardAction.P_ISSUANCE_DATE, nul(PropertyType.DATE))
                .set(ChangeIdentityCardAction.P_ISSUANCE_PLACE, nul(PropertyType.STRING)),
                session);

        CommonDAO.executeAndClear(new DQLUpdateBuilder(ChangeFioStuExtract.class)
                .set(ChangeFioStuExtract.P_LAST_NAME_NEW, value(REPLACEMENT_LAST_NAME))
                .set(ChangeFioStuExtract.P_FIRST_NAME_NEW, value(REPLACEMENT_FIRST_NAME))
                .set(ChangeFioStuExtract.P_MIDDLE_NAME_NEW, value(REPLACEMENT_MIDDLE_NAME))
                .set(ChangeFioStuExtract.P_CARD_SERIA, value(REPLACEMENT_SERIA))
                .set(ChangeFioStuExtract.P_CARD_NUMBER, value(REPLACEMENT_NUMBER))
                .set(ChangeFioStuExtract.P_BIRTH_DATE, nul(PropertyType.DATE))
                .set(ChangeFioStuExtract.P_BIRTH_PLACE, nul(PropertyType.STRING))
                .set(ChangeFioStuExtract.P_ISSUANCE_DATE, nul(PropertyType.DATE))
                .set(ChangeFioStuExtract.P_ISSUANCE_PLACE, nul(PropertyType.STRING)),
                session);

        CommonDAO.executeAndClear(new DQLUpdateBuilder(ChangeFirstNameStuExtract.class)
                .set(ChangeFirstNameStuExtract.P_LAST_NAME_NEW, value(REPLACEMENT_LAST_NAME))
                .set(ChangeFirstNameStuExtract.P_FIRST_NAME_NEW, value(REPLACEMENT_FIRST_NAME))
                .set(ChangeFirstNameStuExtract.P_MIDDLE_NAME_NEW, value(REPLACEMENT_MIDDLE_NAME))
                .set(ChangeFirstNameStuExtract.P_CARD_SERIA, value(REPLACEMENT_SERIA))
                .set(ChangeFirstNameStuExtract.P_CARD_NUMBER, value(REPLACEMENT_NUMBER))
                .set(ChangeFirstNameStuExtract.P_BIRTH_DATE, nul(PropertyType.DATE))
                .set(ChangeFirstNameStuExtract.P_BIRTH_PLACE, nul(PropertyType.STRING))
                .set(ChangeFirstNameStuExtract.P_ISSUANCE_DATE, nul(PropertyType.DATE))
                .set(ChangeFirstNameStuExtract.P_ISSUANCE_PLACE, nul(PropertyType.STRING)),
                session);


        CommonDAO.executeAndClear(new DQLUpdateBuilder(ChangeMiddleNameStuExtract.class)
                .set(ChangeMiddleNameStuExtract.P_LAST_NAME_NEW, value(REPLACEMENT_LAST_NAME))
                .set(ChangeMiddleNameStuExtract.P_FIRST_NAME_NEW, value(REPLACEMENT_FIRST_NAME))
                .set(ChangeMiddleNameStuExtract.P_MIDDLE_NAME_NEW, value(REPLACEMENT_MIDDLE_NAME))
                .set(ChangeMiddleNameStuExtract.P_CARD_SERIA, value(REPLACEMENT_SERIA))
                .set(ChangeMiddleNameStuExtract.P_CARD_NUMBER, value(REPLACEMENT_NUMBER))
                .set(ChangeMiddleNameStuExtract.P_BIRTH_DATE, nul(PropertyType.DATE))
                .set(ChangeMiddleNameStuExtract.P_BIRTH_PLACE, nul(PropertyType.STRING))
                .set(ChangeMiddleNameStuExtract.P_ISSUANCE_DATE, nul(PropertyType.DATE))
                .set(ChangeMiddleNameStuExtract.P_ISSUANCE_PLACE, nul(PropertyType.STRING)),
                session);

        CommonDAO.executeAndClear(new DQLUpdateBuilder(AdoptRussianCitizenshipStuExtract.class)
                .set(AdoptRussianCitizenshipStuExtract.P_LAST_NAME_NEW, value(REPLACEMENT_LAST_NAME))
                .set(AdoptRussianCitizenshipStuExtract.P_FIRST_NAME_NEW, value(REPLACEMENT_FIRST_NAME))
                .set(AdoptRussianCitizenshipStuExtract.P_MIDDLE_NAME_NEW, value(REPLACEMENT_MIDDLE_NAME))
                .set(AdoptRussianCitizenshipStuExtract.P_CARD_SERIA, value(REPLACEMENT_SERIA))
                .set(AdoptRussianCitizenshipStuExtract.P_CARD_NUMBER, value(REPLACEMENT_NUMBER))
                .set(AdoptRussianCitizenshipStuExtract.P_BIRTH_DATE, nul(PropertyType.DATE))
                .set(AdoptRussianCitizenshipStuExtract.P_BIRTH_PLACE, nul(PropertyType.STRING))
                .set(AdoptRussianCitizenshipStuExtract.P_ISSUANCE_DATE, nul(PropertyType.DATE))
                .set(AdoptRussianCitizenshipStuExtract.P_ISSUANCE_PLACE, nul(PropertyType.STRING)),
                session);

        CommonDAO.executeAndClear(new DQLUpdateBuilder(StuExtractToComissionMember.class).set(StuExtractToComissionMember.P_FIO, value(REPLACEMENT_FIO)), session);
    }
}