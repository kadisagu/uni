/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.movestudent.dao.IMoveStudentDao;
import ru.tandemservice.movestudent.entity.*;

/**
 * @author Nikolay Fedorovskih
 * @since 11.07.2013
 */
public class MoveStudentInjectModifier implements IMoveStudentInjectModifier
{
    @Override
    public void modularExtractModifier(RtfInjectModifier modifier, ModularStudentExtract extract)
    {
        final IMoveStudentInjectModifier script = IMoveStudentDao.instance.get().getScriptInjectModifier();
        if (script != null) {
            script.modularExtractModifier(modifier, extract);
        }
    }

    @Override
    public void modularExtractTableModifier(RtfTableModifier modifier, ModularStudentExtract extract)
    {
        final IMoveStudentInjectModifier script = IMoveStudentDao.instance.get().getScriptInjectModifier();
        if (script != null) {
            script.modularExtractTableModifier(modifier, extract);
        }
    }

    @Override
    public void modularOrderModifier(RtfInjectModifier modifier, StudentModularOrder order)
    {
        final IMoveStudentInjectModifier script = IMoveStudentDao.instance.get().getScriptInjectModifier();
        if (script != null) {
            script.modularOrderModifier(modifier, order);
        }
    }

    @Override
    public void modularOrderTableModifier(RtfTableModifier modifier, StudentModularOrder order)
    {
        final IMoveStudentInjectModifier script = IMoveStudentDao.instance.get().getScriptInjectModifier();
        if (script != null) {
            script.modularOrderTableModifier(modifier, order);
        }
    }

    @Override
    public void individualOrderModifier(RtfInjectModifier modifier, StudentModularOrder order, ModularStudentExtract extract)
    {
        final IMoveStudentInjectModifier script = IMoveStudentDao.instance.get().getScriptInjectModifier();
        if (script != null) {
            script.individualOrderModifier(modifier, order, extract);
        }
    }

    @Override
    public void individualOrderTableModifier(RtfTableModifier modifier, StudentModularOrder order, ModularStudentExtract extract)
    {
        final IMoveStudentInjectModifier script = IMoveStudentDao.instance.get().getScriptInjectModifier();
        if (script != null) {
            script.individualOrderTableModifier(modifier, order, extract);
        }
    }

    @Override
    public void listExtractModifier(RtfInjectModifier modifier, ListStudentExtract extract)
    {
        final IMoveStudentInjectModifier script = IMoveStudentDao.instance.get().getScriptInjectModifier();
        if (script != null) {
            script.listExtractModifier(modifier, extract);
        }
    }

    @Override
    public void listExtractTableModifier(RtfTableModifier modifier, ListStudentExtract extract)
    {
        final IMoveStudentInjectModifier script = IMoveStudentDao.instance.get().getScriptInjectModifier();
        if (script != null) {
            script.listExtractTableModifier(modifier, extract);
        }
    }

    @Override
    public void listOrderModifier(RtfInjectModifier modifier, StudentListOrder order, ListStudentExtract firstExtract)
    {
        final IMoveStudentInjectModifier script = IMoveStudentDao.instance.get().getScriptInjectModifier();
        if (script != null) {
            script.listOrderModifier(modifier, order, firstExtract);
        }
    }

    @Override
    public void listOrderTableModifier(RtfTableModifier modifier, StudentListOrder order)
    {
        final IMoveStudentInjectModifier script = IMoveStudentDao.instance.get().getScriptInjectModifier();
        if (script != null) {
            script.listOrderTableModifier(modifier, order);
        }
    }

    @Override
    public void listParagraphModifier(RtfInjectModifier modifier, StudentListParagraph paragraph, ListStudentExtract firstExtract)
    {
        final IMoveStudentInjectModifier script = IMoveStudentDao.instance.get().getScriptInjectModifier();
        if (script != null) {
            script.listParagraphModifier(modifier, paragraph, firstExtract);
        }
    }

    @Override
    public void listParagraphTableModifier(RtfTableModifier modifier, StudentListParagraph paragraph, ListStudentExtract firstExtract)
    {
        final IMoveStudentInjectModifier script = IMoveStudentDao.instance.get().getScriptInjectModifier();
        if (script != null) {
            script.listParagraphTableModifier(modifier, paragraph, firstExtract);
        }
    }
}