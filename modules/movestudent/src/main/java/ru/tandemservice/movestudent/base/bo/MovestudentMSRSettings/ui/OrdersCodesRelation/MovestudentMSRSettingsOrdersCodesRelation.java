/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.ui.OrdersCodesRelation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;


/**
 * @author Ekaterina Zvereva
 * @since 19.12.2014
 */
@Configuration
public class MovestudentMSRSettingsOrdersCodesRelation extends BusinessComponentManager
{
    public static String ORDERS_TYPE_DS = "orderTypeDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(ORDERS_TYPE_DS, ordersDSColumn(), ordersDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint ordersDSColumn()
    {
        return columnListExtPointBuilder(ORDERS_TYPE_DS)
                .addColumn(textColumn("order", StudentExtractType.title()))
                .addColumn(blockColumn("orderCode", "orderCode"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> ordersDSHandler() {
        return new DefaultSearchDataSourceHandler(this.getName(), StudentExtractType.class);
    }
}
