/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e85;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.GiveDiplomaStuExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 03.09.2012
 */
public class GiveDiplomaStuExtractPrint implements IPrintFormCreator<GiveDiplomaStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, GiveDiplomaStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        EducationLevelsHighSchool eduHs = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool();
//        EducationLevels eduLevel = eduHs.getEducationLevel();
//        boolean isGos2 = eduLevel.getLevelType().isGos2();

        printQualification(extract, modifier, eduHs);

        if (TopOrgUnit.getInstance().equals(extract.getEntity().getEducationOrgUnit().getTerritorialOrgUnit()))
            modifier.put("rootOUShortTitleForNonTerritorial", " " + TopOrgUnit.getInstance().getShortTitle());
        else
            modifier.put("rootOUShortTitleForNonTerritorial", "");

        modifier.put("withExcellent", extract.isWithExcellent() ? "с отличием " : "");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    protected void printQualification(GiveDiplomaStuExtract extract, RtfInjectModifier modifier, EducationLevelsHighSchool eduHs)
    {
        EducationLevels eduLevel = eduHs.getEducationLevel();
        String qualificationCode = eduLevel.getSafeQCode();
        StringBuilder diplomaQualificationAward = new StringBuilder();
        final EduProgramQualification qualification = eduHs.getAssignedQualification();
        final String assignedQualificationTitle = qualification != null ? qualification.getTitle().toUpperCase() : "___________________________";

        switch (qualificationCode)
        {
            case QualificationsCodes.BAKALAVR:
            case QualificationsCodes.MAGISTR:
            {
                modifier.put("graduate_A", "степень");
                modifier.put("graduate_G", "степени");
                modifier.put("graduateWithLevel_A", "квалификацию " + assignedQualificationTitle);
                modifier.put("eduLevelTypeStr_P", CommonExtractPrint.getEduLevelTypeStr_P(eduLevel));

                if (extract.isPrintDiplomaQualification() && eduHs.getSpecialDegreeTitle() != null)
                {
                    diplomaQualificationAward.append(" с присвоением специального звания ");
                    diplomaQualificationAward.append(eduHs.getSpecialDegreeTitle());
                }
                break;
            }
            case QualificationsCodes.BAZOVYY_UROVEN_S_P_O:
            case QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O:
            case QualificationsCodes.SPETSIALIST:
            {
                modifier.put("graduate_A", "квалификацию");
                modifier.put("graduate_G", "квалификации");
                modifier.put("graduateWithLevel_A", "квалификацию " + assignedQualificationTitle);
                modifier.put("eduLevelTypeStr_P", CommonExtractPrint.getEduLevelTypeStr_P(eduLevel));
                break;
            }
            default:
            {
                modifier.put("graduate_A", "__________");
                modifier.put("graduate_G", "__________");
                modifier.put("graduateWithLevel_A", "______________________________");
                modifier.put("eduLevelTypeStr_P", "__________");
            }
        }
        modifier.put("diplomaName_G", getDiplomaName_G(qualificationCode));
        modifier.put("diplomaQualificationAward", diplomaQualificationAward.toString());
    }

    public static String getDiplomaName_G(String qualificationCode)
    {
        switch (qualificationCode)
        {
            case QualificationsCodes.BAKALAVR:
                return  "бакалавра";
            case QualificationsCodes.MAGISTR:
                return  "магистра";
            case QualificationsCodes.BAZOVYY_UROVEN_S_P_O:
            case QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O:
                return "государственного образца о среднем профессиональном образовании";
            case QualificationsCodes.SPETSIALIST:
                return  "специалиста";
        }
        return "государственного образца о высшем образовании";
    }
}
