/* $Id:$ */
package ru.tandemservice.movestudent.component.listextract.e41.utils;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 2/19/13
 * Time: 4:40 PM
 */
public class EmployeePostVO extends DataWrapper
{
    private final EmployeePost _employeePost;
    private final PersonAcademicDegree _academicDegree;

    public EmployeePostVO(EmployeePost employeePost, PersonAcademicDegree academicDegree)
    {
        super(employeePost.getId(), employeePost.getTitle());

        _employeePost = employeePost;
        _academicDegree = academicDegree;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public PersonAcademicDegree getAcademicDegree()
    {
        return _academicDegree;
    }
}
