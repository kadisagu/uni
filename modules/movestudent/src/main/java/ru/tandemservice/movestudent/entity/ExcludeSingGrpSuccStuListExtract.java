package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.movestudent.entity.gen.ExcludeSingGrpSuccStuListExtractGen;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Date;

/**
 * excludeSingGrpSuccStuListExtract
 */
public class ExcludeSingGrpSuccStuListExtract extends ExcludeSingGrpSuccStuListExtractGen implements IExcludeExtract
{
    public Group getGroup()
    {
        return getEntity().getGroup();
    }

    @Override
    public Date getBeginDate()
    {
        return getExcludeDate();
    }
}