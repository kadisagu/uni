/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e73;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsExtStuExtract;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.Arrays;
import java.util.List;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 29.08.2012
 */
public class RestorationAdmitStateExamsExtStuExtractPrint implements IPrintFormCreator<RestorationAdmitStateExamsExtStuExtract>
{
    private final List<String> STATE_EXAM_SINGULAR = Arrays.asList("государственный экзамен", "государственного экзамена", "государственному экзамену", "государственный экзамен", "государственным экзаменом", "государственном экзамене");
    private final List<String> STATE_EXAM_PLURAL = Arrays.asList("государственные экзамены", "государственных экзаменов", "государственным экзаменам", "государственные экзамены", "государственными экзаменами", "государственных экзаменах");

    @Override
    public RtfDocument createPrintForm(byte[] template, RestorationAdmitStateExamsExtStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        CommonExtractPrint.injectFefuDevelopConditionAndTech(modifier, extract.getEducationOrgUnitNew(), extract.getGroupNew(), "fefuShortFastExtendedOptionalTextNew");

        int indexOfSeasonTerm = Arrays.asList((ApplicationRuntime.getProperty("seasonTerm_A")).split(";")).indexOf(extract.getSeason());
        String seasonTermPrint = indexOfSeasonTerm == -1 ? null : (ApplicationRuntime.getProperty("seasonTerm_P")).split(";")[indexOfSeasonTerm];

        modifier.put("vuzTitle", extract.getDismissedFrom());
        modifier.put("disOrderNum", extract.getDismissOrder());
        modifier.put("disOrderDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDismissOrderDate()));
        modifier.put("period", "в " + (seasonTermPrint != null ? seasonTermPrint : "________________________"));
        modifier.put("year", DateFormatter.DATE_FORMATTER_JUST_YEAR.format(extract.getStateExamPassDeadlineDate()));
        modifier.put("newDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStateExamPassDeadlineDate()));

        List<String> stateExams = extract.isStateExamPlural() ? STATE_EXAM_PLURAL : STATE_EXAM_SINGULAR;

        for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
        {
            modifier.put("stateExams" + UniRtfUtil.CASE_POSTFIX.get(i), stateExams.get(i));
        }

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}