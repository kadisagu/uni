/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e18;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.movestudent.entity.TransferStuExtractExt;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 05.05.2009
 */
public class TransferStuExtractExtPrint implements IPrintFormCreator<TransferStuExtractExt>
{
    @Override
    @SuppressWarnings("deprecation")
    public RtfDocument createPrintForm(byte[] template, TransferStuExtractExt extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        
        List<String> tagsToDel = new ArrayList<>();
        List<String> tagsToDelWithLineBefore = new ArrayList<>();
        
        if(extract.isByReAttestationResults())
            modifier.put("asReAttestationResult", "По итогам переаттестации");
        else
            tagsToDelWithLineBefore.add("asReAttestationResult");
        
        if(UniDefines.DEVELOP_CONDITION_SHORT.equals(extract.getEducationOrgUnitNew().getDevelopCondition().getCode()))
            modifier.put("shortCourse", "на сокращенный срок обучения ");
        else
            modifier.put("shortCourse", "");
        
        GrammaCase rusCase = GrammaCase.DATIVE;
        IdentityCard identityCard = extract.getEntity().getPerson().getIdentityCard();
        //boolean isMaleSex = identityCard.getSex().isMale();

        InflectorVariant variant = DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(rusCase);

        CoreCollectionUtils.Pair<String, InflectorVariant> lastNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.lastName().s(), variant);
        CoreCollectionUtils.Pair<String, InflectorVariant> firstNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.firstName().s(), variant);
        CoreCollectionUtils.Pair<String, InflectorVariant> middleNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.middleName().s(), variant);

        String lastName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(lastNamePairKey);
        String firstName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(firstNamePairKey);
        String middleName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(middleNamePairKey);

        StringBuilder fio_D = new StringBuilder(lastName);
        fio_D.append(" ").append(firstName);
        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            fio_D.append(" ").append(middleName);
        
        int number = 2;
        if(extract.isStopGrantsPaying())
            modifier.put("stopGrantPayingStr", number++ + ". " + modifier.getStringValue("Student_D") + " " + fio_D.toString() + " выплату стипендии отменить с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStopPayingDate()) + " г.");
        else
            tagsToDel.add("stopGrantPayingStr");
        
        RtfTableModifier table = new RtfTableModifier();
        if(!extract.isHasDebts())
        {
            tagsToDel.add("debts");
            UniRtfUtil.removeTableByName(document, "T", true, false);
        }
        else
        {
            modifier.put("debts", number + ". " + modifier.getStringValue("Student_D") + " " + fio_D.toString() + " ликвидировать разницу в учебных планах в срок до " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDeadline()) + " г.");

            int i = 0;
            List<StuExtractToDebtRelation> relsList = MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(extract);
            String[][] tableData = new String[relsList.size()][];
            for(StuExtractToDebtRelation rel : relsList)
            {
                tableData[i++] = new String[] {String.valueOf(i), rel.getDiscipline(), String.valueOf(rel.getHours()), rel.getControlAction()};
            }
            table.put("T", tableData);
        }
        table.modify(document);
        
        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, tagsToDel, false, false);
        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, tagsToDelWithLineBefore, true, false);

        CommonExtractPrint.addEntryDateToModifier(modifier, extract.getTransferDate()).modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}