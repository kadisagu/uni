/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.modularextract.Pub;

import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.movestudent.component.customorder.CustomOrderUtils;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.custom.ModifyModularStuExtract;
import ru.tandemservice.movestudent.entity.custom.OrderActionType;
import ru.tandemservice.unimove.entity.ICustomOrderAction;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 28.08.2013
 */
public class DAO extends ModularStudentExtractPubDAO<ModifyModularStuExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        if (model.getExtract().getOrderActionType() == OrderActionType.DO_CUSTOM_ACTION)
        {
            Map<String, String> compNames = new LinkedHashMap<>();
            for (Map.Entry<ICustomOrderAction, String> entry : MoveStudentDaoFacade.getMoveStudentDao().getCustomActions(model.getExtractId(), false).entrySet())
            {
                compNames.put(entry.getKey().getClass().getSimpleName(), entry.getValue());
            }
            model.setActionMap(compNames);
        }
        else
            model.setActionMap(Collections.<String, String>emptyMap());
    }

    @Override
    public void doSendToCoordination(Model model, IPersistentPersonable initiator)
    {
        if (model.getExtract().getUserPrintForm() == null)
            throw new ApplicationException(model.getExtract().getTitle() + ". " + CustomOrderUtils.NEED_UPLOAD_PRINT_DOC);

        super.doSendToCoordination(model, initiator);
    }
}