package ru.tandemservice.movestudent.entity.custom;

import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.component.customorder.CustomOrderUtils;
import ru.tandemservice.movestudent.component.customorder.ICustomPrintFormContainer;
import ru.tandemservice.movestudent.component.customorder.IStudentCustomOrderExtract;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.movestudent.entity.custom.gen.ModifyModularStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту с кастомной логикой: пользователь сам выбирает набор действий и шаблон печати
 */
public class ModifyModularStuExtract extends ModifyModularStuExtractGen implements IStudentCustomOrderExtract, ICustomPrintFormContainer
{
    @Override
    public Date getBeginDate()
    {
        return null;
    }

    @Override
    public Date getEndDate()
    {
        return null;
    }

    /**
     * Является ли изменяемый приказ индивидуальным
     */
    public boolean isModifiableOrderIndividual()
    {
        return getModifiableExtract() != null && getModifiableExtract() instanceof ModularStudentExtract &&
                MoveStudentDaoFacade.getMoveStudentDao().isModularOrderIndividual((StudentModularOrder) getModifiableExtract().getParagraph().getOrder());
    }

    /**
     * Номер изменяемой выписки
     */
    public String getModifiableExtractNumber()
    {
        return (getModifiableExtract() != null) ? ("" + getModifiableExtract().getParagraph().getNumber() + "/" + getModifiableExtract().getNumber()) : null;
    }

    /**
     * Название отменяемого приказа
     */
    public String getModifiableOrderTitle()
    {
        return getModifiableExtract() != null ? CustomOrderUtils.getOrderTitle(getModifiableExtract()) : null;
    }

    @Override
    public DatabaseFile getFile()
    {
        return getUserPrintForm();
    }

    @Override
    public void setFile(DatabaseFile file)
    {
        setUserPrintForm(file);
    }

    @Override
    public byte[] getBasicTemplate()
    {
        return MoveStudentDaoFacade.getMoveStudentDao().getTemplate(
                getType(),
                isIndividual() ? MoveStudentDefines.INDIVIDUAL_ORDER_TEXT_CODE : MoveStudentDefines.EXTRACT_IN_ORDER_TEXT_CODE);
    }

    @Override
    public Long getModifiableOrderId()
    {
        return getModifiableExtract() != null ? getModifiableExtract().getParagraph().getOrder().getId() : null;
    }
}