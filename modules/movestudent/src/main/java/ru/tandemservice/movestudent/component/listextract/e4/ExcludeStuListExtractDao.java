/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e4;

import org.tandemframework.core.CoreDateUtils;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ExcludeStuListExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 08.06.2009
 */
public class ExcludeStuListExtractDao extends UniBaseDao implements IExtractComponentDao<ExcludeStuListExtract>
{
    @Override
    public void doCommit(ExcludeStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        extract.setStatusOld(extract.getEntity().getStatus());
        extract.getEntity().setStatus(extract.getStatusNew());

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            extract.setPrevOrderDate(orderData.getExcludeOrderDate());
            extract.setPrevOrderNumber(orderData.getExcludeOrderNumber());
        }

        extract.setFinishedYear(student.getFinishYear());
        student.setFinishYear(CoreDateUtils.getYear(extract.getParagraph().getOrder().getCommitDate()));
        orderData.setExcludeOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setExcludeOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);
    }

    @Override
    public void doRollback(ExcludeStuListExtract extract, Map parameters)
    {
        extract.getEntity().setStatus(extract.getStatusOld());

        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }

        student.setFinishYear(extract.getFinishedYear());

        orderData.setExcludeOrderDate(extract.getPrevOrderDate());
        orderData.setExcludeOrderNumber(extract.getPrevOrderNumber());
        getSession().saveOrUpdate(orderData);
    }
}