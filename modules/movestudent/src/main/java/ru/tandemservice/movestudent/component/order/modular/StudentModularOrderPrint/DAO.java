/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.modular.StudentModularOrderPrint;

import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.movestudent.component.customorder.CustomOrderUtils;
import ru.tandemservice.movestudent.component.customorder.ICustomPrintFormContainer;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StudentOrderTextRelation;
import ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimove.entity.ICustomOrderExtract;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vip_delete
 * @since 20.10.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setOrder(getNotNull(model.getOrderId()));

        ModularStudentExtract firstExtract = MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(model.getOrder().getId());
        if (firstExtract != null && model.getOrder().getExtractCount() == 1)
        {
            model.setStudentFio(firstExtract.getEntity().getPerson().getFio().replaceAll("\\.", ""));
            model.setCustomIndividual(firstExtract instanceof ICustomOrderExtract && firstExtract.isIndividual());
        }

        // если приказ на согласовании, согласован или проведен, то печатная форма должна быть сохранена
        String stateCode = model.getOrder().getState().getCode();
        if (UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTABLE.equals(stateCode)
                || UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED.equals(stateCode)
                || UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(stateCode))
        {
            // приказ проведен => печатная форма сохранена
            StudentOrderTextRelation rel = get(StudentOrderTextRelation.class, StudentOrderTextRelation.L_ORDER, model.getOrder());
            model.setData(rel.getText());
        }
        else
        {
            if (!model.isCustomIndividual())
            {
                StudentExtractType modularOrderType = getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.MODULAR_ORDER);
                model.setData(get(MovestudentTemplate.class, MovestudentTemplate.L_TYPE, modularOrderType).getContent());
            }
            else if (firstExtract != null)
            {
                ICustomPrintFormContainer container = (ICustomPrintFormContainer) firstExtract;
                model.setData(container.getFile() != null ? container.getFile().getContent() : null);
                if (model.getData() == null)
                    throw new ApplicationException(CustomOrderUtils.NEED_UPLOAD_PRINT_DOC);
            }
            List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(model.getOrder());
            List<String[]> visaData = new ArrayList<>();
            for (Visa visa : visaList)
            {
                visaData.add(new String[]{visa.getPossibleVisa().getTitle(), visa.getPossibleVisa().getEntity().getPerson().getFullFio()});
            }
            model.setVisaData(visaData.toArray(new String[visaData.size()][]));
        }
    }
}
