/* $Id: DAO.java.vm 6933 2009-03-07 05:41:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e34.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.fias.base.bo.util.CountryAutocompleteModel;

import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.ChangeFirstNameStuExtract;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Nationality;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.entity.employee.Student;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 18.12.2009
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<ChangeFirstNameStuExtract, Model>
{
	@Override
	protected ChangeFirstNameStuExtract createNewInstance() {

		return new ChangeFirstNameStuExtract();
	}

	@Override
	protected GrammaCase getStudentTitleCase() {

		return GrammaCase.ACCUSATIVE;
	}

	@Override
	public void prepare(final Model model)
	{
		super.prepare(model);

        model.setCitizenshipModel(new CountryAutocompleteModel());
		model.setIdentityCardTypeList(getCatalogItemListOrderByCode(IdentityCardType.class));
		model.setNationalityModel(new LazySimpleSelectModel<Nationality>(Nationality.class).setRecordLimit(50));
		model.setSexList(getCatalogItemList(Sex.class));

		final ChangeFirstNameStuExtract extract = model.getExtract();
		final Student student = extract.getEntity();

		if (model.isAddForm())
		{
			final IdentityCard card = student.getPerson().getIdentityCard();
			extract.setLastNameNew(card.getLastName());
			extract.setFirstNameNew(card.getFirstName());
			extract.setMiddleNameNew(card.getMiddleName());
			extract.setSex(card.getSex());
			extract.setBirthDate(card.getBirthDate());
			extract.setBirthPlace(card.getBirthPlace());
			extract.setNationality(card.getNationality());
            extract.setCitizenship(card.getCitizenship());
		}
	}

	@Override
	public void update(final Model model)
	{
		model.getExtract().setFirstNameNew(StringUtils.capitalize(model.getExtract().getFirstNameNew()));
		model.getExtract().setLastNameNew(StringUtils.capitalize(model.getExtract().getLastNameNew()));
		model.getExtract().setMiddleNameNew(StringUtils.capitalize(model.getExtract().getMiddleNameNew()));

		//save rollback data
		model.getExtract().setLastActiveIdentityCard(model.getExtract().getEntity().getPerson().getIdentityCard());
		super.update(model);
	}

}
