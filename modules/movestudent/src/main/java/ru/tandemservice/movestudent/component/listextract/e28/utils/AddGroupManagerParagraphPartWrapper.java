/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e28.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author Alexander Zhebko
 * @since 14.09.2012
 */
public class AddGroupManagerParagraphPartWrapper implements Comparable<AddGroupManagerParagraphPartWrapper>
{
    private Group _group;
    private final ListStudentExtract _firstExtract;

    public AddGroupManagerParagraphPartWrapper(Group group, ListStudentExtract firstExtract)
    {
        _group = group;
        _firstExtract = firstExtract;
    }

    private Student _student;

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public ListStudentExtract getFirstExtract() {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof AddGroupManagerParagraphPartWrapper))
            return false;

        AddGroupManagerParagraphPartWrapper that = (AddGroupManagerParagraphPartWrapper) o;

        return _group.equals(that.getGroup());
    }

    @Override
    public int hashCode()
    {
        return _group.hashCode();
    }

    @Override
    public int compareTo(AddGroupManagerParagraphPartWrapper o)
    {
        int result = ((Integer) _group.getCourse().getIntValue()).compareTo((Integer) o.getGroup().getCourse().getIntValue());
        if (result == 0)
            result = _group.getTitle().compareTo(o.getGroup().getTitle());
        return result;
    }
}