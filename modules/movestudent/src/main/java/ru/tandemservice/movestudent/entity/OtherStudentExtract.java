package ru.tandemservice.movestudent.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.gen.OtherStudentExtractGen;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.IPrintableExtract;

import java.util.List;

/**
 * Выписка (прочие приказы по студентам)
 */
public class OtherStudentExtract extends OtherStudentExtractGen implements IPrintableExtract
{
    public String getBasicListStr()
    {
        List<StuOtherOrderToBasicRelation> list = MoveStudentDaoFacade.getMoveStudentDao().getPrioritySortedExtractBasicsList(this);
        String[] title = new String[list.size()];
        for (int i = 0; i < list.size(); i++)
            title[i] = list.get(i).getBasic().getTitle() + (StringUtils.isEmpty(list.get(i).getComment()) ? "" : " " + list.get(i).getComment());
        return StringUtils.join(title, ", ");
    }

    public String getReasonFullTitle()
    {
        if (null == getReason()) return null;
        return getReason().getTitle() + (null != getReasonComment() ? (" " + getReasonComment()) : "");
    }

    @Override
    public String getTitle()
    {
        if (getEntity() == null) {
            return this.getClass().getSimpleName();
        }
        String studentWord = SexCodes.FEMALE.equals(getEntity().getPerson().getIdentityCard().getSex().getCode()) ? "студентка" : "студент";

        if (getParagraph()!= null && getParagraph().getOrder().getNumber() != null)
            return "Приказ «" + getType().getTitle() + "» №" + getParagraph().getOrder().getNumber() + " | " + studentWord + " " + getEntity().getPerson().getFullFio();
        else
            return "Приказ «" + getType().getTitle() + "» | " + studentWord + " " + getEntity().getPerson().getFullFio();
    }

    @Override
    public boolean canBePrinted()
    {
        return false;
    }

    @Override
    public void doExtractPrint(boolean printPdf)
    {
        throw new ApplicationException("Выписка не может быть распечатана.");
    }

    @Override
    public void doOrderPrint(boolean printPdf)
    {
        StudentOtherOrderTextRelation rel = UniDaoFacade.getCoreDao().get(StudentOtherOrderTextRelation.class, StudentOtherOrderTextRelation.order().s(), getParagraph().getOrder().getId());
        if (null == rel) throw new ApplicationException("К данному приказу не прикреплена печатная форма.");
        if (printPdf)
            BusinessComponentUtils.downloadDocument(UniReportUtils.createConvertingToPdfRenderer(rel.getFileName().replaceFirst("rtf", "pdf"), rel.getContent()), false);
        else
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(rel.getFileType()).fileName(rel.getFileName()).document(rel.getContent()), false);
    }

    public boolean hasOrderPrintForm()
    {
        if (getParagraph() == null) return false;
        return UniDaoFacade.getCoreDao().get(StudentOtherOrderTextRelation.class, StudentOtherOrderTextRelation.order().s(), getParagraph().getOrder().getId()) != null;
    }
}