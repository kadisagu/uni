package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.movestudent.entity.gen.ExcludeUnacceptedToGPDefenceStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. Об отчислении (недопуск к ВКР)
 */
public class ExcludeUnacceptedToGPDefenceStuExtract extends ExcludeUnacceptedToGPDefenceStuExtractGen implements IExcludeExtract
{
    @Override
    public Date getExcludeDate()
    {
        return getBeginDate();
    }
}