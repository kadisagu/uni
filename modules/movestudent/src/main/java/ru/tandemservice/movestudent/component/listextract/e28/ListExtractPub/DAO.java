/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e28.ListExtractPub;

import com.google.common.collect.Lists;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.movestudent.entity.AddGroupManagerStuListExtract;
import ru.tandemservice.movestudent.entity.GroupToExtractRelation;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 14.09.2012
 */
public class DAO extends AbstractListExtractPubDAO<AddGroupManagerStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GroupToExtractRelation.class, "ge");
        builder.where(DQLExpressions.eq(DQLExpressions.property("ge", GroupToExtractRelation.extract().id()), DQLExpressions.value(model.getExtract().getId())));
        List<GroupToExtractRelation> groupToExtractRelations = builder.createStatement(getSession()).list();

        List<String> groupsTitles = Lists.newArrayList();

        for(GroupToExtractRelation relation : groupToExtractRelations)
        {
            Group group = relation.getGroup();
            groupsTitles.add(group.getTitle() + " - " + (null != relation.getPrevGroupManager() ? ("староста " + relation.getPrevGroupManager().getFio()) : "(староста не назначен)"));
        }

        model.setGroupsTitles(groupsTitles);
    }
}
