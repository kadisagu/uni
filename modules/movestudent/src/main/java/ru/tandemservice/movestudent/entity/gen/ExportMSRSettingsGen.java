package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ExportMSRSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройки кодов Вуза и идентификатора отправителя данных в ГУП МСР
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExportMSRSettingsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ExportMSRSettings";
    public static final String ENTITY_NAME = "exportMSRSettings";
    public static final int VERSION_HASH = -1392458720;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE_HIGH_SCHOOL = "codeHighSchool";
    public static final String P_CODE_SENDER = "codeSender";
    public static final String P_FILE_VERSION = "fileVersion";
    public static final String P_USER_SCRIPT_EXPORT_STUDENT = "userScriptExportStudent";

    private String _codeHighSchool;     // Код ВУЗа
    private String _codeSender;     // Идентификатор отправителя
    private String _fileVersion;     // Версия файла данных
    private String _userScriptExportStudent;     // Скрипт выгрузки данных по студентам

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Код ВУЗа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCodeHighSchool()
    {
        return _codeHighSchool;
    }

    /**
     * @param codeHighSchool Код ВУЗа. Свойство не может быть null.
     */
    public void setCodeHighSchool(String codeHighSchool)
    {
        dirty(_codeHighSchool, codeHighSchool);
        _codeHighSchool = codeHighSchool;
    }

    /**
     * @return Идентификатор отправителя. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCodeSender()
    {
        return _codeSender;
    }

    /**
     * @param codeSender Идентификатор отправителя. Свойство не может быть null.
     */
    public void setCodeSender(String codeSender)
    {
        dirty(_codeSender, codeSender);
        _codeSender = codeSender;
    }

    /**
     * @return Версия файла данных. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFileVersion()
    {
        return _fileVersion;
    }

    /**
     * @param fileVersion Версия файла данных. Свойство не может быть null.
     */
    public void setFileVersion(String fileVersion)
    {
        dirty(_fileVersion, fileVersion);
        _fileVersion = fileVersion;
    }

    /**
     * @return Скрипт выгрузки данных по студентам.
     */
    public String getUserScriptExportStudent()
    {
        return _userScriptExportStudent;
    }

    /**
     * @param userScriptExportStudent Скрипт выгрузки данных по студентам.
     */
    public void setUserScriptExportStudent(String userScriptExportStudent)
    {
        dirty(_userScriptExportStudent, userScriptExportStudent);
        _userScriptExportStudent = userScriptExportStudent;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExportMSRSettingsGen)
        {
            setCodeHighSchool(((ExportMSRSettings)another).getCodeHighSchool());
            setCodeSender(((ExportMSRSettings)another).getCodeSender());
            setFileVersion(((ExportMSRSettings)another).getFileVersion());
            setUserScriptExportStudent(((ExportMSRSettings)another).getUserScriptExportStudent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExportMSRSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExportMSRSettings.class;
        }

        public T newInstance()
        {
            return (T) new ExportMSRSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "codeHighSchool":
                    return obj.getCodeHighSchool();
                case "codeSender":
                    return obj.getCodeSender();
                case "fileVersion":
                    return obj.getFileVersion();
                case "userScriptExportStudent":
                    return obj.getUserScriptExportStudent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "codeHighSchool":
                    obj.setCodeHighSchool((String) value);
                    return;
                case "codeSender":
                    obj.setCodeSender((String) value);
                    return;
                case "fileVersion":
                    obj.setFileVersion((String) value);
                    return;
                case "userScriptExportStudent":
                    obj.setUserScriptExportStudent((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "codeHighSchool":
                        return true;
                case "codeSender":
                        return true;
                case "fileVersion":
                        return true;
                case "userScriptExportStudent":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "codeHighSchool":
                    return true;
                case "codeSender":
                    return true;
                case "fileVersion":
                    return true;
                case "userScriptExportStudent":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "codeHighSchool":
                    return String.class;
                case "codeSender":
                    return String.class;
                case "fileVersion":
                    return String.class;
                case "userScriptExportStudent":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExportMSRSettings> _dslPath = new Path<ExportMSRSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExportMSRSettings");
    }
            

    /**
     * @return Код ВУЗа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExportMSRSettings#getCodeHighSchool()
     */
    public static PropertyPath<String> codeHighSchool()
    {
        return _dslPath.codeHighSchool();
    }

    /**
     * @return Идентификатор отправителя. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExportMSRSettings#getCodeSender()
     */
    public static PropertyPath<String> codeSender()
    {
        return _dslPath.codeSender();
    }

    /**
     * @return Версия файла данных. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExportMSRSettings#getFileVersion()
     */
    public static PropertyPath<String> fileVersion()
    {
        return _dslPath.fileVersion();
    }

    /**
     * @return Скрипт выгрузки данных по студентам.
     * @see ru.tandemservice.movestudent.entity.ExportMSRSettings#getUserScriptExportStudent()
     */
    public static PropertyPath<String> userScriptExportStudent()
    {
        return _dslPath.userScriptExportStudent();
    }

    public static class Path<E extends ExportMSRSettings> extends EntityPath<E>
    {
        private PropertyPath<String> _codeHighSchool;
        private PropertyPath<String> _codeSender;
        private PropertyPath<String> _fileVersion;
        private PropertyPath<String> _userScriptExportStudent;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Код ВУЗа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExportMSRSettings#getCodeHighSchool()
     */
        public PropertyPath<String> codeHighSchool()
        {
            if(_codeHighSchool == null )
                _codeHighSchool = new PropertyPath<String>(ExportMSRSettingsGen.P_CODE_HIGH_SCHOOL, this);
            return _codeHighSchool;
        }

    /**
     * @return Идентификатор отправителя. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExportMSRSettings#getCodeSender()
     */
        public PropertyPath<String> codeSender()
        {
            if(_codeSender == null )
                _codeSender = new PropertyPath<String>(ExportMSRSettingsGen.P_CODE_SENDER, this);
            return _codeSender;
        }

    /**
     * @return Версия файла данных. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExportMSRSettings#getFileVersion()
     */
        public PropertyPath<String> fileVersion()
        {
            if(_fileVersion == null )
                _fileVersion = new PropertyPath<String>(ExportMSRSettingsGen.P_FILE_VERSION, this);
            return _fileVersion;
        }

    /**
     * @return Скрипт выгрузки данных по студентам.
     * @see ru.tandemservice.movestudent.entity.ExportMSRSettings#getUserScriptExportStudent()
     */
        public PropertyPath<String> userScriptExportStudent()
        {
            if(_userScriptExportStudent == null )
                _userScriptExportStudent = new PropertyPath<String>(ExportMSRSettingsGen.P_USER_SCRIPT_EXPORT_STUDENT, this);
            return _userScriptExportStudent;
        }

        public Class getEntityClass()
        {
            return ExportMSRSettings.class;
        }

        public String getEntityName()
        {
            return "exportMSRSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
