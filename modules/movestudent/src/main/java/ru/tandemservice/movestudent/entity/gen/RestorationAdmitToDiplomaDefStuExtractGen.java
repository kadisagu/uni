package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О восстановлении и допуске к защите ВКР (неявка на защиту)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RestorationAdmitToDiplomaDefStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract";
    public static final String ENTITY_NAME = "restorationAdmitToDiplomaDefStuExtract";
    public static final int VERSION_HASH = -1640795001;
    private static IEntityMeta ENTITY_META;

    public static final String P_RESTORATION_DATE = "restorationDate";
    public static final String P_DISMISS_DATE = "dismissDate";
    public static final String P_DISMISS_ORDER = "dismissOrder";
    public static final String P_DISMISS_ORDER_DATE = "dismissOrderDate";
    public static final String P_APPLY_DATE = "applyDate";
    public static final String P_DISMISSED_FROM = "dismissedFrom";
    public static final String P_SEASON = "season";
    public static final String P_DP_DEFENCE_DEADLINE_DATE = "dpDefenceDeadlineDate";

    private Date _restorationDate;     // Дата восстановления
    private Date _dismissDate;     // Дата отчисления
    private String _dismissOrder;     // Приказ об отчислении
    private Date _dismissOrderDate;     // Дата приказа об отчислении
    private Date _applyDate;     // Дата подачи заявления
    private String _dismissedFrom;     // Отчислен из (название ОУ)
    private String _season;     // Время года
    private Date _dpDefenceDeadlineDate;     // Срок защиты ВКР

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата восстановления. Свойство не может быть null.
     */
    @NotNull
    public Date getRestorationDate()
    {
        return _restorationDate;
    }

    /**
     * @param restorationDate Дата восстановления. Свойство не может быть null.
     */
    public void setRestorationDate(Date restorationDate)
    {
        dirty(_restorationDate, restorationDate);
        _restorationDate = restorationDate;
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     */
    @NotNull
    public Date getDismissDate()
    {
        return _dismissDate;
    }

    /**
     * @param dismissDate Дата отчисления. Свойство не может быть null.
     */
    public void setDismissDate(Date dismissDate)
    {
        dirty(_dismissDate, dismissDate);
        _dismissDate = dismissDate;
    }

    /**
     * @return Приказ об отчислении. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDismissOrder()
    {
        return _dismissOrder;
    }

    /**
     * @param dismissOrder Приказ об отчислении. Свойство не может быть null.
     */
    public void setDismissOrder(String dismissOrder)
    {
        dirty(_dismissOrder, dismissOrder);
        _dismissOrder = dismissOrder;
    }

    /**
     * @return Дата приказа об отчислении. Свойство не может быть null.
     */
    @NotNull
    public Date getDismissOrderDate()
    {
        return _dismissOrderDate;
    }

    /**
     * @param dismissOrderDate Дата приказа об отчислении. Свойство не может быть null.
     */
    public void setDismissOrderDate(Date dismissOrderDate)
    {
        dirty(_dismissOrderDate, dismissOrderDate);
        _dismissOrderDate = dismissOrderDate;
    }

    /**
     * @return Дата подачи заявления. Свойство не может быть null.
     */
    @NotNull
    public Date getApplyDate()
    {
        return _applyDate;
    }

    /**
     * @param applyDate Дата подачи заявления. Свойство не может быть null.
     */
    public void setApplyDate(Date applyDate)
    {
        dirty(_applyDate, applyDate);
        _applyDate = applyDate;
    }

    /**
     * @return Отчислен из (название ОУ).
     */
    @Length(max=255)
    public String getDismissedFrom()
    {
        return _dismissedFrom;
    }

    /**
     * @param dismissedFrom Отчислен из (название ОУ).
     */
    public void setDismissedFrom(String dismissedFrom)
    {
        dirty(_dismissedFrom, dismissedFrom);
        _dismissedFrom = dismissedFrom;
    }

    /**
     * @return Время года.
     */
    @Length(max=255)
    public String getSeason()
    {
        return _season;
    }

    /**
     * @param season Время года.
     */
    public void setSeason(String season)
    {
        dirty(_season, season);
        _season = season;
    }

    /**
     * @return Срок защиты ВКР. Свойство не может быть null.
     */
    @NotNull
    public Date getDpDefenceDeadlineDate()
    {
        return _dpDefenceDeadlineDate;
    }

    /**
     * @param dpDefenceDeadlineDate Срок защиты ВКР. Свойство не может быть null.
     */
    public void setDpDefenceDeadlineDate(Date dpDefenceDeadlineDate)
    {
        dirty(_dpDefenceDeadlineDate, dpDefenceDeadlineDate);
        _dpDefenceDeadlineDate = dpDefenceDeadlineDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RestorationAdmitToDiplomaDefStuExtractGen)
        {
            setRestorationDate(((RestorationAdmitToDiplomaDefStuExtract)another).getRestorationDate());
            setDismissDate(((RestorationAdmitToDiplomaDefStuExtract)another).getDismissDate());
            setDismissOrder(((RestorationAdmitToDiplomaDefStuExtract)another).getDismissOrder());
            setDismissOrderDate(((RestorationAdmitToDiplomaDefStuExtract)another).getDismissOrderDate());
            setApplyDate(((RestorationAdmitToDiplomaDefStuExtract)another).getApplyDate());
            setDismissedFrom(((RestorationAdmitToDiplomaDefStuExtract)another).getDismissedFrom());
            setSeason(((RestorationAdmitToDiplomaDefStuExtract)another).getSeason());
            setDpDefenceDeadlineDate(((RestorationAdmitToDiplomaDefStuExtract)another).getDpDefenceDeadlineDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RestorationAdmitToDiplomaDefStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RestorationAdmitToDiplomaDefStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new RestorationAdmitToDiplomaDefStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "restorationDate":
                    return obj.getRestorationDate();
                case "dismissDate":
                    return obj.getDismissDate();
                case "dismissOrder":
                    return obj.getDismissOrder();
                case "dismissOrderDate":
                    return obj.getDismissOrderDate();
                case "applyDate":
                    return obj.getApplyDate();
                case "dismissedFrom":
                    return obj.getDismissedFrom();
                case "season":
                    return obj.getSeason();
                case "dpDefenceDeadlineDate":
                    return obj.getDpDefenceDeadlineDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "restorationDate":
                    obj.setRestorationDate((Date) value);
                    return;
                case "dismissDate":
                    obj.setDismissDate((Date) value);
                    return;
                case "dismissOrder":
                    obj.setDismissOrder((String) value);
                    return;
                case "dismissOrderDate":
                    obj.setDismissOrderDate((Date) value);
                    return;
                case "applyDate":
                    obj.setApplyDate((Date) value);
                    return;
                case "dismissedFrom":
                    obj.setDismissedFrom((String) value);
                    return;
                case "season":
                    obj.setSeason((String) value);
                    return;
                case "dpDefenceDeadlineDate":
                    obj.setDpDefenceDeadlineDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "restorationDate":
                        return true;
                case "dismissDate":
                        return true;
                case "dismissOrder":
                        return true;
                case "dismissOrderDate":
                        return true;
                case "applyDate":
                        return true;
                case "dismissedFrom":
                        return true;
                case "season":
                        return true;
                case "dpDefenceDeadlineDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "restorationDate":
                    return true;
                case "dismissDate":
                    return true;
                case "dismissOrder":
                    return true;
                case "dismissOrderDate":
                    return true;
                case "applyDate":
                    return true;
                case "dismissedFrom":
                    return true;
                case "season":
                    return true;
                case "dpDefenceDeadlineDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "restorationDate":
                    return Date.class;
                case "dismissDate":
                    return Date.class;
                case "dismissOrder":
                    return String.class;
                case "dismissOrderDate":
                    return Date.class;
                case "applyDate":
                    return Date.class;
                case "dismissedFrom":
                    return String.class;
                case "season":
                    return String.class;
                case "dpDefenceDeadlineDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RestorationAdmitToDiplomaDefStuExtract> _dslPath = new Path<RestorationAdmitToDiplomaDefStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RestorationAdmitToDiplomaDefStuExtract");
    }
            

    /**
     * @return Дата восстановления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract#getRestorationDate()
     */
    public static PropertyPath<Date> restorationDate()
    {
        return _dslPath.restorationDate();
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract#getDismissDate()
     */
    public static PropertyPath<Date> dismissDate()
    {
        return _dslPath.dismissDate();
    }

    /**
     * @return Приказ об отчислении. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract#getDismissOrder()
     */
    public static PropertyPath<String> dismissOrder()
    {
        return _dslPath.dismissOrder();
    }

    /**
     * @return Дата приказа об отчислении. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract#getDismissOrderDate()
     */
    public static PropertyPath<Date> dismissOrderDate()
    {
        return _dslPath.dismissOrderDate();
    }

    /**
     * @return Дата подачи заявления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract#getApplyDate()
     */
    public static PropertyPath<Date> applyDate()
    {
        return _dslPath.applyDate();
    }

    /**
     * @return Отчислен из (название ОУ).
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract#getDismissedFrom()
     */
    public static PropertyPath<String> dismissedFrom()
    {
        return _dslPath.dismissedFrom();
    }

    /**
     * @return Время года.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract#getSeason()
     */
    public static PropertyPath<String> season()
    {
        return _dslPath.season();
    }

    /**
     * @return Срок защиты ВКР. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract#getDpDefenceDeadlineDate()
     */
    public static PropertyPath<Date> dpDefenceDeadlineDate()
    {
        return _dslPath.dpDefenceDeadlineDate();
    }

    public static class Path<E extends RestorationAdmitToDiplomaDefStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<Date> _restorationDate;
        private PropertyPath<Date> _dismissDate;
        private PropertyPath<String> _dismissOrder;
        private PropertyPath<Date> _dismissOrderDate;
        private PropertyPath<Date> _applyDate;
        private PropertyPath<String> _dismissedFrom;
        private PropertyPath<String> _season;
        private PropertyPath<Date> _dpDefenceDeadlineDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата восстановления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract#getRestorationDate()
     */
        public PropertyPath<Date> restorationDate()
        {
            if(_restorationDate == null )
                _restorationDate = new PropertyPath<Date>(RestorationAdmitToDiplomaDefStuExtractGen.P_RESTORATION_DATE, this);
            return _restorationDate;
        }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract#getDismissDate()
     */
        public PropertyPath<Date> dismissDate()
        {
            if(_dismissDate == null )
                _dismissDate = new PropertyPath<Date>(RestorationAdmitToDiplomaDefStuExtractGen.P_DISMISS_DATE, this);
            return _dismissDate;
        }

    /**
     * @return Приказ об отчислении. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract#getDismissOrder()
     */
        public PropertyPath<String> dismissOrder()
        {
            if(_dismissOrder == null )
                _dismissOrder = new PropertyPath<String>(RestorationAdmitToDiplomaDefStuExtractGen.P_DISMISS_ORDER, this);
            return _dismissOrder;
        }

    /**
     * @return Дата приказа об отчислении. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract#getDismissOrderDate()
     */
        public PropertyPath<Date> dismissOrderDate()
        {
            if(_dismissOrderDate == null )
                _dismissOrderDate = new PropertyPath<Date>(RestorationAdmitToDiplomaDefStuExtractGen.P_DISMISS_ORDER_DATE, this);
            return _dismissOrderDate;
        }

    /**
     * @return Дата подачи заявления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract#getApplyDate()
     */
        public PropertyPath<Date> applyDate()
        {
            if(_applyDate == null )
                _applyDate = new PropertyPath<Date>(RestorationAdmitToDiplomaDefStuExtractGen.P_APPLY_DATE, this);
            return _applyDate;
        }

    /**
     * @return Отчислен из (название ОУ).
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract#getDismissedFrom()
     */
        public PropertyPath<String> dismissedFrom()
        {
            if(_dismissedFrom == null )
                _dismissedFrom = new PropertyPath<String>(RestorationAdmitToDiplomaDefStuExtractGen.P_DISMISSED_FROM, this);
            return _dismissedFrom;
        }

    /**
     * @return Время года.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract#getSeason()
     */
        public PropertyPath<String> season()
        {
            if(_season == null )
                _season = new PropertyPath<String>(RestorationAdmitToDiplomaDefStuExtractGen.P_SEASON, this);
            return _season;
        }

    /**
     * @return Срок защиты ВКР. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationAdmitToDiplomaDefStuExtract#getDpDefenceDeadlineDate()
     */
        public PropertyPath<Date> dpDefenceDeadlineDate()
        {
            if(_dpDefenceDeadlineDate == null )
                _dpDefenceDeadlineDate = new PropertyPath<Date>(RestorationAdmitToDiplomaDefStuExtractGen.P_DP_DEFENCE_DEADLINE_DATE, this);
            return _dpDefenceDeadlineDate;
        }

        public Class getEntityClass()
        {
            return RestorationAdmitToDiplomaDefStuExtract.class;
        }

        public String getEntityName()
        {
            return "restorationAdmitToDiplomaDefStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
