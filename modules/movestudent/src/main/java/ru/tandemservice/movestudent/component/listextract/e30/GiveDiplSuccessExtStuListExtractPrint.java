/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e30;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.GiveDiplSuccessExtStuListExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author ListExtractComponentGenerator
 * @since 17.09.2012
 */
public class GiveDiplSuccessExtStuListExtractPrint implements IPrintFormCreator<GiveDiplSuccessExtStuListExtract>, IListParagraphPrintFormCreator<GiveDiplSuccessExtStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, GiveDiplSuccessExtStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        CommonExtractPrint.modifyEducationStr(modifier, extract.getEducationLevelsHighSchool().getEducationLevel());

        Student student = extract.getEntity();
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
        CommonExtractPrint.initOrgUnit(modifier, educationOrgUnit, "formativeOrgUnitStr", "");

        EducationLevels educationLevels = extract.getEducationLevelsHighSchool().getEducationLevel();
        String qCode = educationLevels.getSafeQCode();
        final EduProgramQualification eduProgramQualification = extract.getEducationLevelsHighSchool().getAssignedQualification();
        final String assignedQualificationTitle = eduProgramQualification != null ? eduProgramQualification.getTitle().toUpperCase() : "______________________________";
        StringBuilder diplomaQualificationAward = new StringBuilder();

        if (QualificationsCodes.BAKALAVR.equals(qCode) || QualificationsCodes.MAGISTR.equals(qCode))
        {
            modifier.put("graduate_G", "степени");
            modifier.put("graduateWithLevel_A", "степень " + assignedQualificationTitle);

            String specialDegreeTitle = extract.getEducationLevelsHighSchool().getSpecialDegreeTitle();
            if (extract.isPrintDiplomaQualification() && specialDegreeTitle != null)
            {
                diplomaQualificationAward.append(", присвоить специальное звание ");
                diplomaQualificationAward.append(specialDegreeTitle);
            }
        }
        else if (QualificationsCodes.BAZOVYY_UROVEN_S_P_O.equals(qCode) || QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O.equals(qCode) || QualificationsCodes.SPETSIALIST.equals(qCode))
        {
            modifier.put("graduate_G", "квалификации");
            modifier.put("graduateWithLevel_A", "квалификацию " + assignedQualificationTitle);
        }
        else
        {
            modifier.put("graduate_G", "__________");
            modifier.put("graduateWithLevel_A", "______________________________");
        }

        modifier.put("eduLevelTypeStr_P", CommonExtractPrint.getEduLevelTypeStr_P(educationLevels));
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, educationOrgUnit, CommonListOrderPrint.getEducationBaseText(student.getGroup()), "fefuShortFastExtendedOptionalText");
        modifier.put("diplomaQualificationAward", diplomaQualificationAward.toString());

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, GiveDiplSuccessExtStuListExtract firstExtract)
    {
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, GiveDiplSuccessExtStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, GiveDiplSuccessExtStuListExtract firstExtract)
    {
    }
}