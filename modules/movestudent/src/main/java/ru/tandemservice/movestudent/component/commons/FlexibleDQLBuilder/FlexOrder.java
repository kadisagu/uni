/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

import org.tandemframework.core.entity.OrderDirection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 02.12.2013
 */
public class FlexOrder implements IFlexOrder
{
    private String _name;
    private String _path;
    private String _alias;
    private OrderDirection _defaultDirection;
    private List<IFlexOrder> _subOrders = new ArrayList<>();

    public FlexOrder(String name, String alias, String path, OrderDirection defaultDirection, IFlexOrder... subOrders)
    {
        _name = name;
        _path = path;
        _alias = alias;
        _defaultDirection = defaultDirection;
        addSubOrders(subOrders);
    }

    public FlexOrder(String name, String alias, String path, IFlexOrder... subOrders)
    {
        this(name, alias, path, OrderDirection.asc, subOrders);
    }

    public FlexOrder(IFlexColumn column)
    {
        this(column, OrderDirection.asc);
    }

    public FlexOrder(IFlexColumn column, OrderDirection defaultDirection)
    {
        this(column.getName(), column.getAlias(), column.getPath(), defaultDirection);
    }

    @Override
    public String getName()
    {
        return _name;
    }

    @Override
    public String getPath()
    {
        return _path;
    }

    @Override
    public String getAlias()
    {
        return _alias;
    }

    @Override
    public OrderDirection getDefaultDirection()
    {
        return _defaultDirection;
    }

    @Override
    public List<IFlexOrder> getSubOrders()
    {
        return _subOrders;
    }

    @Override
    public IFlexOrder addSubOrders(IFlexOrder... orders)
    {
        Collections.addAll(_subOrders, orders);
        return this;
    }
}