/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.modularextract.AddEdit;

import org.tandemframework.core.common.ITitledEntity;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.customorder.CustomOrderUtils;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.custom.ModifyModularStuExtract;
import ru.tandemservice.movestudent.entity.custom.OrderActionType;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 28.08.2013
 */
public class Model extends CommonModularStudentExtractAddEditModel<ModifyModularStuExtract>
{
    private ISelectModel _modifiedExtractModel;
    private Map<String, String> _possibleActionComponents;
    private String _currentRegion;
    private IBusinessComponent _context;
    private ITitledEntity _modifiableExtractWrapper;
    private List<OrderActionType> _actionTypeList;

    public Collection<String> getRegionList()
    {
        return _possibleActionComponents.keySet();
    }

    public String getCurrentRegion()
    {
        return _currentRegion;
    }

    public void setCurrentRegion(String currentRegion)
    {
        _currentRegion = currentRegion;
    }

    public ISelectModel getModifiedExtractModel()
    {
        return _modifiedExtractModel;
    }

    public void setModifiedExtractModel(ISelectModel modifiedExtractModel)
    {
        _modifiedExtractModel = modifiedExtractModel;
    }

    public boolean isShowExtractNumber()
    {
        return getExtract() != null && getExtract().getModifiableExtract() != null && !getExtract().isModifiableOrderIndividual();
    }

    public String getModifiedOrderFieldName()
    {
        StudentExtractType extractType = getExtract() == null ? getExtractType() : (getExtractType() == null ? getExtract().getType() : getExtractType());
        if (extractType != null && extractType.getCode() != null)
            return CustomOrderUtils.getModifiableOrderFieldName(extractType.getCode());
        return null;
    }

    public Map<String, String> getPossibleActionComponents()
    {
        return _possibleActionComponents;
    }

    public void setPossibleActionComponents(Map<String, String> possibleActionComponents)
    {
        _possibleActionComponents = possibleActionComponents;
    }

    public boolean isShowCustomActions()
    {
        return getExtract() != null && OrderActionType.DO_CUSTOM_ACTION.equals(getExtract().getOrderActionType());
    }

    public IBusinessComponent getContext()
    {
        return _context;
    }

    public void setContext(IBusinessComponent context)
    {
        _context = context;
    }

    public List<OrderActionType> getActionTypeList()
    {
        return _actionTypeList;
    }

    public void setActionTypeList(List<OrderActionType> actionTypeList)
    {
        _actionTypeList = actionTypeList;
    }
}