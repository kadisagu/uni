/* $Id$ */
package ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;

/**
 * @author Dmitry Seleznev
 * @since 15.07.2013
 */
public interface IModularStudentExtractPubControllerExtension
{
    String MODULAR_STUDENT_EXTRACT_PUB_CONTROLLER_EXTENSIONS_BEAN_NAME = "modularStudentExtractPubControllerExtensions";

    void doSendToCoordinationAdditionalAction(IBusinessComponent component, AbstractStudentOrder order);

    void doExecuteAdditionalAction(IBusinessComponent component, AbstractStudentOrder order);
}