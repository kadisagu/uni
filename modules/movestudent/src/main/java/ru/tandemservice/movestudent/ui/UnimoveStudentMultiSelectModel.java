/**
 *$Id$
 */
package ru.tandemservice.movestudent.ui;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.movestudent.dao.MoveStudentDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author Alexander Zhebko
 * @since 12.02.2013
 */
public class UnimoveStudentMultiSelectModel extends CommonMultiSelectModel
{
    private OrgUnit _orgUnit;

    public UnimoveStudentMultiSelectModel()
    {
    }

    public UnimoveStudentMultiSelectModel(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    @Override
    protected IListResultBuilder createBuilder(String filter, Set set)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(Student.class, "s").column("s")
                .joinPath(DQLJoinType.inner, Student.person().identityCard().fromAlias("s"), "i")
                .joinPath(DQLJoinType.left, Student.group().fromAlias("s"), "g")
                .where(eq(property("s", Student.archival()), value(Boolean.FALSE)));

        if (_orgUnit != null)
        {
            builder
                    .joinPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias("s"), "ou")
                    .where(or(
                            eq(property("ou", EducationOrgUnit.formativeOrgUnit()), value(_orgUnit)),
                            eq(property("ou", EducationOrgUnit.territorialOrgUnit()), value(_orgUnit)),
                            eq(property("ou", EducationOrgUnit.educationLevelHighSchool().orgUnit()), value(_orgUnit))
                    ));

        }

        if (filter != null && !filter.isEmpty())
        {
            filter = CoreStringUtils.escapeLike(filter, true);
            builder.where(likeUpper(DQLFunctions.concat(property("i", IdentityCard.fullFio()), value(" "),
                                                        DQLFunctions.coalesce(property("g", Group.title()), value(""))), value(filter)));
        }

        if (set != null)
        {
            builder.where(in(property("s", Student.id()), set));
        }

        builder.order(property("i", IdentityCard.P_LAST_NAME));
        builder.order(property("i", IdentityCard.P_FIRST_NAME));
        builder.order(property("i", IdentityCard.P_MIDDLE_NAME));
        builder.order(property("g", Group.P_TITLE));

        return new DQLListResultBuilder<Student>(builder, MoveStudentDao.MAX_ROWS);
    }

    @Override
    public String getLabelFor(Object value, int columnIndex)
    {
        Student student = (Student) value;
        return student.getPerson().getIdentityCard().getFullFio() + (student.getGroup() == null ? "" : " (" + student.getGroup().getTitle() + ")");
    }
}