/* $Id$ */
package ru.tandemservice.movestudent.component.order.other.StudentOtherOrderUploadPrintForm;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.movestudent.entity.StudentOtherOrder;
import ru.tandemservice.movestudent.entity.StudentOtherOrderTextRelation;

/**
 * @author Dmitry Seleznev
 * @since 15.04.2013
 */
@Input({
        @Bind(key = "orderId", binding = "orderId")
})
public class Model
{
    private Long _orderId;
    private StudentOtherOrder _order;
    private IUploadFile _uploadFile;

    private StudentOtherOrderTextRelation _printForm;

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public StudentOtherOrder getOrder()
    {
        return _order;
    }

    public void setOrder(StudentOtherOrder order)
    {
        _order = order;
    }

    public IUploadFile getUploadFile()
    {
        return _uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile)
    {
        _uploadFile = uploadFile;
    }

    public StudentOtherOrderTextRelation getPrintForm()
    {
        return _printForm;
    }

    public void setPrintForm(StudentOtherOrderTextRelation printForm)
    {
        _printForm = printForm;
    }
}