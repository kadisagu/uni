/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e12.ParagraphAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.CourseTransferDebtorStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Date;

/**
 * @author ListExtractComponentGenerator
 * @since 23.07.2009
 */
public class Model extends AbstractListParagraphAddEditModel<CourseTransferDebtorStuListExtract> implements IGroupModel
{
    private Course _course;
    private ISelectModel _groupListModel;
    private Group _group;
    private Course _courseOld;
    private ISelectModel _courseOldListModel;
    private ISelectModel _courseNewListModel;
    private Course _courseNew;
    private Date _transferDate;

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public Course getCourseOld()
    {
        return _courseOld;
    }

    public void setCourseOld(Course courseOld)
    {
        _courseOld = courseOld;
    }

    public ISelectModel getCourseOldListModel()
    {
        return _courseOldListModel;
    }

    public void setCourseOldListModel(ISelectModel courseOldListModel)
    {
        _courseOldListModel = courseOldListModel;
    }

    public ISelectModel getCourseNewListModel()
    {
        return _courseNewListModel;
    }

    public void setCourseNewListModel(ISelectModel courseNewListModel)
    {
        _courseNewListModel = courseNewListModel;
    }

    public Course getCourseNew()
    {
        return _courseNew;
    }

    public void setCourseNew(Course courseNew)
    {
        _courseNew = courseNew;
    }

    public Date getTransferDate()
    {
        return _transferDate;
    }

    public void setTransferDate(Date transferDate)
    {
        this._transferDate = transferDate;
    }
}