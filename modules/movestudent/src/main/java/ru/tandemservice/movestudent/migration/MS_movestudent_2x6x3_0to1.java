package ru.tandemservice.movestudent.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudent_2x6x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность transferTerritorialExtStuExtract

		// создано обязательное свойство forStateFinalAttestation
        if (!tool.columnExists("trnsfrtrrtrlextstextrct_t", "forstatefinalattestation_p"))
		{
			// создать колонку
			tool.createColumn("trnsfrtrrtrlextstextrct_t", new DBColumn("forstatefinalattestation_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultForStateFinalAttestation = false;
			tool.executeUpdate("update trnsfrtrrtrlextstextrct_t set forstatefinalattestation_p=? where forstatefinalattestation_p is null", defaultForStateFinalAttestation);

			// сделать колонку NOT NULL
			tool.setColumnNullable("trnsfrtrrtrlextstextrct_t", "forstatefinalattestation_p", false);
		}
    }
}