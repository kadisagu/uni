/* $Id: extractDao.vm 8134 2009-05-26 09:03:40Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e24;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 19.02.2011
 */
public class SetDiplomaWorkTopicAndScientificAdviserStuListExtractDao extends UniBaseDao implements IExtractComponentDao<SetDiplomaWorkTopicAndScientificAdviserStuListExtract>
{
    @Override
    public void doCommit(SetDiplomaWorkTopicAndScientificAdviserStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        extract.setDiplomaWorkTopicOld(extract.getEntity().getFinalQualifyingWorkTheme());
        extract.setScientificAdviserNameOld(extract.getEntity().getPpsEntry());
        extract.setScientificAdviserNameStrOld(extract.getEntity().getFinalQualifyingWorkAdvisor());
        extract.getEntity().setFinalQualifyingWorkTheme(extract.getDiplomaWorkTopicNew());
        extract.getEntity().setPpsEntry(extract.getScientificAdviserName());
        extract.getEntity().setFinalQualifyingWorkAdvisor(MoveStudentDaoFacade.getMoveStudentDao().getStuListExtractScientificAdvisorStr(extract, extract.getEntity().getPpsEntry()));
    }

    @Override
    public void doRollback(SetDiplomaWorkTopicAndScientificAdviserStuListExtract extract, Map parameters)
    {
        extract.getEntity().setFinalQualifyingWorkTheme(extract.getDiplomaWorkTopicOld());
        extract.getEntity().setFinalQualifyingWorkAdvisor(extract.getScientificAdviserNameStrOld());
        extract.getEntity().setPpsEntry(extract.getScientificAdviserNameOld());
    }
}