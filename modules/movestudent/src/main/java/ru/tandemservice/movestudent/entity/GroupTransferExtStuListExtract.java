package ru.tandemservice.movestudent.entity;

import org.apache.commons.lang.ObjectUtils;
import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.GroupTransferExtStuListExtractGen;

import java.util.Date;

/**
 * Проект приказа «О переводе из группы в группу»
 */
public class GroupTransferExtStuListExtract extends GroupTransferExtStuListExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return (Date) ObjectUtils.defaultIfNull(getTransferDate(), getBeginDate());
    }
}