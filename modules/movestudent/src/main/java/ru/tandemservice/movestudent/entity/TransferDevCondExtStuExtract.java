package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.TransferDevCondExtStuExtractGen;

import java.util.Date;

/**
 * О переводе на другие условия освоения и срок освоения
 */
public class TransferDevCondExtStuExtract extends TransferDevCondExtStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}