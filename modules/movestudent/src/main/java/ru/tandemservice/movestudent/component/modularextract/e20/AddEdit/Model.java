/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.component.modularextract.e20.AddEdit;

import java.util.List;

import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.SessionProlongStuExtract;
import ru.tandemservice.uni.entity.catalog.SessionType;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 07.05.2009
 */
public class Model extends CommonModularStudentExtractAddEditModel<SessionProlongStuExtract>
{
    private ISelectModel _eduYearsList;
    private List<SessionType> _sessionTypesList;

    public ISelectModel getEduYearsList()
    {
        return _eduYearsList;
    }

    public void setEduYearsList(ISelectModel eduYearsList)
    {
        this._eduYearsList = eduYearsList;
    }

    public List<SessionType> getSessionTypesList()
    {
        return _sessionTypesList;
    }

    public void setSessionTypesList(List<SessionType> sessionTypesList)
    {
        this._sessionTypesList = sessionTypesList;
    }
}