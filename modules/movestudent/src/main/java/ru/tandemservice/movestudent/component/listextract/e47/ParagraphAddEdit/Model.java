/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e47.ParagraphAddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;

/**
 * @author ListExtractComponentGenerator
 * @since 17.01.2017
 */
public class Model extends AbstractListParagraphAddEditModel<TransferProgramSubjectStuListExtract> implements IGroupModel, IEducationLevelModel
{

    private DevelopForm _developForm;
    private Course _course;
    private Group _group;
    private boolean _changeGroupEduOU;
    private EducationOrgUnit _eduOUGroupNew;
    private EduProgramSubject _oldProgramSubject;
    private EduProgramSubject2013 _newProgramSubject;

    private ISelectModel _developFormModel;
    private ISelectModel _groupListModel;
    private ISelectModel _eduOUModel;
    private ISelectModel _oldProgramSubjectModel;
    private ISelectModel _newProgramSubjectModel;
    private ISelectModel _studentEduOUModel;

    public boolean getStudentDisabled()
    {
        Boolean disabled = (Boolean) getDataSource().getCurrentValueEntity().getProperty(AbstractListParagraphAddEditModel.P_STUDENT_DISABLED);
        return null != disabled ? disabled : false ;
    }

    public String getCurrentEOUId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "currentEOUId_" + dataSource.getCurrentEntity().getId();
    }


    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return getParagraph().getOrder().getOrgUnit();
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return getGroup() == null? null : getGroup().getEducationOrgUnit().getTerritorialOrgUnit();
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return null;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return getGroup() == null? null : getGroup().getEducationOrgUnit().getDevelopCondition();
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return getGroup() == null? null : getGroup().getEducationOrgUnit().getDevelopTech();
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return getGroup() == null? null : getGroup().getEducationOrgUnit().getDevelopPeriod();
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }


    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public boolean isChangeGroupEduOU()
    {
        return _changeGroupEduOU;
    }

    public void setChangeGroupEduOU(boolean changeGroupEduOU)
    {
        _changeGroupEduOU = changeGroupEduOU;
    }

    public ISelectModel getEduOUModel()
    {
        return _eduOUModel;
    }

    public void setEduOUModel(ISelectModel eduOUModel)
    {
        _eduOUModel = eduOUModel;
    }

    public EducationOrgUnit getEduOUGroupNew()
    {
        return _eduOUGroupNew;
    }

    public void setEduOUGroupNew(EducationOrgUnit eduOUGroupNew)
    {
        _eduOUGroupNew = eduOUGroupNew;
    }

    public EduProgramSubject getOldProgramSubject()
    {
        return _oldProgramSubject;
    }

    public void setOldProgramSubject(EduProgramSubject oldProgramSubject)
    {
        _oldProgramSubject = oldProgramSubject;
    }

    public EduProgramSubject2013 getNewProgramSubject()
    {
        return _newProgramSubject;
    }

    public void setNewProgramSubject(EduProgramSubject2013 newProgramSubject)
    {
        _newProgramSubject = newProgramSubject;
    }

    public ISelectModel getOldProgramSubjectModel()
    {
        return _oldProgramSubjectModel;
    }

    public void setOldProgramSubjectModel(ISelectModel oldProgramSubjectModel)
    {
        _oldProgramSubjectModel = oldProgramSubjectModel;
    }

    public ISelectModel getNewProgramSubjectModel()
    {
        return _newProgramSubjectModel;
    }

    public void setNewProgramSubjectModel(ISelectModel newProgramSubjectModel)
    {
        _newProgramSubjectModel = newProgramSubjectModel;
    }

    public ISelectModel getStudentEduOUModel()
    {
        return _studentEduOUModel;
    }

    public void setStudentEduOUModel(ISelectModel studentEduOUModel)
    {
        _studentEduOUModel = studentEduOUModel;
    }
}
