/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e38;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.e25.SplitStudentsGroupStuListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.e38.utils.SplitStudentsExtParagraphWrapper;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.SplitStudentsExtStuListExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 15.10.2013
 */
public class SplitStudentsExtStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        final RtfDocument document = new RtfReader().read(template);

        List<SplitStudentsExtStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        List<SplitStudentsExtParagraphWrapper> preparedParagraphsStructure = prepareParagraphsStructure(extracts);

        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, preparedParagraphsStructure);
        RtfUtil.optimizeFontAndColorTable(document);
        return document;
    }

    protected List<SplitStudentsExtParagraphWrapper> prepareParagraphsStructure(List<SplitStudentsExtStuListExtract> extracts)
    {
        int ind;
        List<SplitStudentsExtParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        for (SplitStudentsExtStuListExtract extract : extracts)
        {
            Student student = extract.getEntity();
            Person person = extract.getEntity().getPerson();

            SplitStudentsExtParagraphWrapper paragraphWrapper = new SplitStudentsExtParagraphWrapper(
                    student.getStudentCategory(),
                    student.getCompensationType(),
                    extract.getEducationOrgUnitNew().getEducationLevelHighSchool(),
                    extract);

            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else
                paragraphWrapper = paragraphWrapperList.get(ind);

            paragraphWrapper.getPersonList().add(person);
        }
        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<SplitStudentsExtParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            int parNumber = 0;
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            // Получаем шаблон параграфа
            byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.SET_SPECIALIZATION_LIST_EXTRACT), 1);
            for (SplitStudentsExtParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphWrapper.getFirstExtract();
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(firstExtract.getParagraph(), firstExtract);

                SplitStudentsExtStuListExtract extract = (SplitStudentsExtStuListExtract) firstExtract;
                CommonListExtractPrint.injectCommonListExtractData(paragraphInjectModifier, extract);

                paragraphInjectModifier.put("parNumber", paragraphWrappers.size() == 1 ? "" : String.valueOf(++parNumber) + ". ");
                paragraphInjectModifier.put("paragraphNumber", String.valueOf(parNumber));
                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");
                paragraphInjectModifier.put("paragraphNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");

                CommonListExtractPrint.injectStudentsCategory(paragraphInjectModifier, paragraphWrapper.getStudentCategory());
                CommonExtractPrint.initDevelopForm(paragraphInjectModifier, extract.getEducationOrgUnitOld().getDevelopForm(), "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, extract.getEntity().getEducationOrgUnit(), "formativeOrgUnitStr", "");

                Student student = extract.getEntity();

                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, student.getEducationOrgUnit().getFormativeOrgUnit(), "", "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, student.getEducationOrgUnit(), "formativeOrgUnitStr", "");

                Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(extract.getEducationOrgUnitOld().getEducationLevelHighSchool().getEducationLevel());
                String qualificationCode = null != qualification ? qualification.getCode() : null;
                if (null != qualificationCode && !SplitStudentsGroupStuListExtractPrint.PROCESSED_QUALIFICATION_CODES.contains(qualificationCode))
                    qualificationCode = null;

                for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
                {
                    paragraphInjectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[i]);
                    paragraphInjectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i) + "P", SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_PLURAL_CASES_ARRAY.get(qualificationCode)[i]);
                }
                paragraphInjectModifier.put("specialization", extract.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle());
                paragraphInjectModifier.put("specializationNew", extract.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle());
                paragraphInjectModifier.put("profileNew", extract.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle());

                StringBuilder fefuEducationStrProfileNew = new StringBuilder();
                StringBuilder fefuEducationStrProfileNew_I = new StringBuilder();

                fefuEducationStrProfileNew.append(SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[0]).append(" «");
                fefuEducationStrProfileNew.append(extract.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle()).append("»");
                paragraphInjectModifier.put("fefuEducationStrProfileNew", StringUtils.capitalize(fefuEducationStrProfileNew.toString()));

                fefuEducationStrProfileNew_I.append(SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[4]).append(" «");
                fefuEducationStrProfileNew_I.append(extract.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle()).append("»");
                paragraphInjectModifier.put("fefuEducationStrProfileNew_I", fefuEducationStrProfileNew_I.toString());

                EducationLevels speciality = EducationOrgUnitUtil.getParentLevel(extract.getEducationOrgUnitOld().getEducationLevelHighSchool());
                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, speciality, new String[]{"fefuEducationStrDirectionOld"}, false);

                CommonExtractPrint.initFefuGroup(paragraphInjectModifier, "groupInternal_G", extract.getEntity().getGroup(), extract.getEducationOrgUnitOld().getDevelopForm(), " группы ");

                UniRtfUtil.initEducationType(paragraphInjectModifier, extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool(), "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit(), "orgUnit", "");
                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, extract.getEducationOrgUnitOld(), CommonListOrderPrint.getEducationBaseText(extract.getEntity().getGroup()), "fefuShortFastExtendedOptionalText");
                customModifier.listParagraphModifier(paragraphInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphInjectModifier.modify(paragraph);

                // Получаем список студентов
                List<Person> personList = paragraphWrapper.getPersonList();
                personList.sort(Person.FULL_FIO_AND_ID_COMPARATOR);
                RtfString rtfString = new RtfString();

                int j = 0;
                for (; j < personList.size() - 1; j++)
                {
                    rtfString.append(IRtfData.TAB).append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio()).par();
                }

                rtfString.append(IRtfData.TAB).append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio());
                paragraphInjectModifier.put("STUDENT_LIST", rtfString);
                customModifier.listParagraphModifier(paragraphInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                paragraphInjectModifier.modify(paragraph);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraph);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }
            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}
