package ru.tandemservice.movestudent.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudent_2x9x0_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность commonStuExtract

		// создано свойство bookNumberNew
		{
			// создать колонку
			tool.createColumn("commonstuextract_t", new DBColumn("booknumbernew_p", DBType.createVarchar(255)));

		}

		// создано свойство targetAdmissionNew
		{
			// создать колонку
			tool.createColumn("commonstuextract_t", new DBColumn("targetadmissionnew_p", DBType.BOOLEAN));

		}

		// создано свойство bookNumberOld
		{
			// создать колонку
			tool.createColumn("commonstuextract_t", new DBColumn("booknumberold_p", DBType.createVarchar(255)));

		}

		// создано свойство targetAdmissionOld
		{
			// создать колонку
			tool.createColumn("commonstuextract_t", new DBColumn("targetadmissionold_p", DBType.BOOLEAN));

		}


    }
}