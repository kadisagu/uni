/* $Id: DAO.java.vm 12068 2010-03-29 10:41:18Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e17.ParagraphAddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.AdmitToPassDiplomaWorkStuListExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author ListExtractComponentGenerator
 * @since 04.02.2011
 */
public class DAO extends AbstractListParagraphAddEditDAO<AdmitToPassDiplomaWorkStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));
        model.setDevelopConditionList(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        // заполняем поля по умолчанию
        if (model.getParagraphId() != null)
        {
            AdmitToPassDiplomaWorkStuListExtract extract = model.getFirstExtract();
            model.setCourse(extract.getCourse());
            model.setGroup(extract.getGroup());
            model.setDevelopForm(extract.getDevelopForm());
            model.setDevelopCondition(extract.getDevelopCondition());
        }
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developCondition(), model.getDevelopCondition()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developForm(), model.getDevelopForm()));
    }
    
    @Override
    protected AdmitToPassDiplomaWorkStuListExtract createNewInstance(Model model)
    {
        return new AdmitToPassDiplomaWorkStuListExtract();
    }

    @Override
    protected void fillExtract(AdmitToPassDiplomaWorkStuListExtract extract, Student student, Model model)
    {
        extract.setCourse(model.getCourse());
        extract.setDevelopForm(model.getDevelopForm());
        extract.setDevelopCondition(model.getDevelopCondition());
        extract.setGroup(model.getGroup());
    }
}