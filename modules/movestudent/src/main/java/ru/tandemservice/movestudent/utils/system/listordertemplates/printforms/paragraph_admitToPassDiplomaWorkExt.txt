\qj\fi0
Следующих студентов {course} курса {formativeOrgUnitStr_G}, обучавшихся {compensationTypeStr}, выполнивших содержание учебного плана по {educationStr_D}  по {developForm_DF} форме обучения и сдавших государственный экзамен, допустить к защите выпускной {workType}.\par
\par
{PARAGRAPH_CONTENT}