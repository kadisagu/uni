/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e57;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AdmitToDiplomaStuExtract;
import ru.tandemservice.uni.entity.catalog.Qualifications;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 21.08.2012
 */
public class AdmitToDiplomaStuExtractPrint implements IPrintFormCreator<AdmitToDiplomaStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, AdmitToDiplomaStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());
        String qualificationCode = null != qualification ? qualification.getCode() : null;

        String workType;
        if (null != qualificationCode && qualificationCode.equals("62"))
            workType = "квалификационной работы (бакалаврской работы)";
        else if (null != qualificationCode && qualificationCode.equals("68"))
            workType = "квалификационной работы (магистерской диссертации)";
        else workType = "квалификационной работы";

        modifier.put("workType", workType);

        String stateExams;
        if(extract.isStateExams())
        {
            stateExams = "государственные экзамены";
        }
        else
        {
            stateExams = "государственный экзамен";
        }
        modifier.put("stateExams", stateExams);

        additionalModify(modifier, extract);

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    public void additionalModify(RtfInjectModifier modifier, AdmitToDiplomaStuExtract extract) {}
}