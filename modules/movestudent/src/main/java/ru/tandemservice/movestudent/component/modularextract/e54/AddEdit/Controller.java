/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e54.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.movestudent.entity.ChangePassStateExamsStuExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 15.08.2012
 */
public class Controller extends CommonModularStudentExtractAddEditController<ChangePassStateExamsStuExtract, IDAO, Model>
{
    public void onClickChangeYear(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setYear(Integer.parseInt(DateFormatter.DATE_FORMATTER_JUST_YEAR.format(model.getExtract().getNewDate())));
    }
}
