/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e36.ParagraphAddEdit;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.GrantRiseStuListExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.*;

/**
 * @author ListExtractComponentGenerator
 * @since 22.11.2012
 */
public class DAO extends AbstractListParagraphAddEditDAO<GrantRiseStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));
        List<IdentifiableWrapper> monthList = new ArrayList<>();
        for (int i = 1; i <= 12; i++)
            monthList.add(new IdentifiableWrapper((long) i, CommonBaseDateUtil.getMonthNameDeclined(i, GrammaCase.NOMINATIVE)));

        model.setMonthList(monthList);

        if (model.getParagraphId() == null)
        {
            model.setCompensationType(get(CompensationType.class, CompensationType.P_CODE, CompensationTypeCodes.COMPENSATION_TYPE_BUDGET));
        }

        if (model.isEditForm())
        {
            GrantRiseStuListExtract extract = model.getFirstExtract();
            model.setCourse(extract.getCourse());
            model.setGroup(extract.getGroup());
            model.setCompensationType(extract.getCompensationType());
            model.setMonth(model.getMonthList().get(extract.getMonth() - 1));
            model.setYear(extract.getYear());
        }
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        if (model.getCompensationType() != null)
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));
    }

    @Override
    protected GrantRiseStuListExtract createNewInstance(Model model)
    {
        return new GrantRiseStuListExtract();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void fillExtract(GrantRiseStuListExtract extract, Student student, Model model)
    {
        IValueMapHolder<Object> grantRiseAmountHolder = (IValueMapHolder<Object>) model.getDataSource().getColumn("grantRiseAmount");
        Map<Long, Object> grantRiseAmountMap = (null == grantRiseAmountHolder ? Collections.<Long, Object>emptyMap() : grantRiseAmountHolder.getValueMap());

        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setCompensationType(model.getCompensationType());
        extract.setMonth(model.getMonth().getId().intValue());
        extract.setYear(model.getYear());
        Number value = (Number) grantRiseAmountMap.get(student.getId());
        extract.setGrantRiseAmount(value.doubleValue());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        IValueMapHolder<Double> grantRiseAmountHolder = (IValueMapHolder<Double>) model.getDataSource().getColumn("grantRiseAmount");
        Map<Long, Double> grantRiseAmountMap = (null == grantRiseAmountHolder ? Collections.<Long, Double>emptyMap() : grantRiseAmountHolder.getValueMap());

        List<ViewWrapper<Student>> viewlist = new ArrayList<ViewWrapper<Student>>((Collection) ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).getSelectedObjects());

        for (ViewWrapper<Student> wrapper : viewlist)
        {
            if (grantRiseAmountMap.get(wrapper.getEntity().getId()) == null)
                errorCollector.add("Поле «Размер повышения стипендии» обязательно для заполнения.", "grantRiseAmountId_" + wrapper.getEntity().getId());
            else
            {
                Number riseAmount = grantRiseAmountMap.get(wrapper.getEntity().getId());
                if (riseAmount.doubleValue() <= 0)
                    errorCollector.add("Размер повышения стипендии должен быть больше нуля.", "grantRiseAmountId_" + wrapper.getEntity().getId());
            }
        }

        if (errorCollector.hasErrors()) return;

        super.update(model);
    }
}