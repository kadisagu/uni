/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.customorder.listextract.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.customorder.CustomOrderUtils;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.custom.ModifyListStuExtract;
import ru.tandemservice.movestudent.entity.custom.OrderActionType;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 07.10.2013
 */
public class Model extends AbstractListParagraphAddEditModel<ModifyListStuExtract>
{
    private StudentListOrder _modifiableOrder;
    private ISelectModel _modifiableOrderModel;
    private OrderActionType _orderActionType;
    private List<OrderActionType> _actionTypeList;
    private IBusinessComponent _context;
    private Map<String, String> _possibleActionComponents;
    private String _currentRegion;

    public Collection<String> getRegionList()
    {
        return _possibleActionComponents.keySet();
    }

    public String getModifiableOrderFieldName()
    {
        return CustomOrderUtils.getModifiableOrderFieldName(getParagraph().getOrder().getType().getCode());
    }

    public StudentListOrder getModifiableOrder()
    {
        return _modifiableOrder;
    }

    public void setModifiableOrder(StudentListOrder modifiableOrder)
    {
        _modifiableOrder = modifiableOrder;
    }

    public ISelectModel getModifiableOrderModel()
    {
        return _modifiableOrderModel;
    }

    public void setModifiableOrderModel(ISelectModel modifiableOrderModel)
    {
        _modifiableOrderModel = modifiableOrderModel;
    }

    public OrderActionType getOrderActionType()
    {
        return _orderActionType;
    }

    public void setOrderActionType(OrderActionType orderActionType)
    {
        _orderActionType = orderActionType;
    }

    public List<OrderActionType> getActionTypeList()
    {
        return _actionTypeList;
    }

    public void setActionTypeList(List<OrderActionType> actionTypeList)
    {
        _actionTypeList = actionTypeList;
    }

    public IBusinessComponent getContext()
    {
        return _context;
    }

    public void setContext(IBusinessComponent context)
    {
        _context = context;
    }

    public Map<String, String> getPossibleActionComponents()
    {
        return _possibleActionComponents;
    }

    public void setPossibleActionComponents(Map<String, String> possibleActionComponents)
    {
        _possibleActionComponents = possibleActionComponents;
    }

    public String getCurrentRegion()
    {
        return _currentRegion;
    }

    public void setCurrentRegion(String currentRegion)
    {
        _currentRegion = currentRegion;
    }

    public boolean isShowCustomActions()
    {
        return getOrderActionType() == OrderActionType.DO_CUSTOM_ACTION;
    }
}