package ru.tandemservice.movestudent.entity.custom;

import ru.tandemservice.movestudent.entity.custom.gen.*;

/**
 * Связь действия изменения доп. статусов студента с доп. статусом
 *
 * Если признак "Новый статус" установлен, данный статус будет добавлен студенту, иначе - снят со студента.
 */
public class ChangeCustomStateActionRelation extends ChangeCustomStateActionRelationGen
{
}