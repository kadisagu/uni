/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.BiFunction;

/**
 * @author vip_delete
 * @since 24.12.2008
 */
public class CommonExtractPrint
{
    public static final Map<String, String[]> DIFFERENT_WORD_MALE_SEX_CASES_ARRAY = new HashMap<>();
    public static final Map<String, String[]> DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY = new HashMap<>();
    private static final Map<String, String[]> STUDENT_CATEGORY_MALE_SEX_CASES_ARRAY = new HashMap<>();
    private static final Map<String, String[]> STUDENT_CATEGORY_FEMALE_SEX_CASES_ARRAY = new HashMap<>();

    private static final List<String> SPEC_QUALIFICATION_CODES = new ArrayList<>();

    public static final Map<GrammaCase, String> GRAMMA_CASE_POSTFIX = new HashMap<>();

    public static final String PARAGRAPH_CONTENT = "PARAGRAPH_CONTENT";

    public static final String[] DIRECTION_CASES = new String[]{"направление", "направления", "направлению", "направление", "направлением", "направлении"};
    public static final String[] PROFILE_CASES = new String[]{"профиль", "профиля", "профилю", "профиль", "профилем", "профиле"};
    public static final String[] SPECIALIZATION_CASES = new String[]{"специализация", "специализации", "специализации", "специализацию", "специализацией", "специализации"};
    public static final String[] MASTER_PROGRAM_CASES = new String[]{"магистерская программа", "магистерской программы", "магистерской программе", "магистерскую программу", "магистерской программой", "магистерской программе"};

    public static final String[] DIRECTION_PLURAL_CASES = new String[]{"направления", "направлений", "направлениям", "направления", "направлениями", "направлениях"};
    public static final String[] PROFILE_PLURAL_CASES = new String[]{"профили", "профилей", "профилям", "профили", "профилями", "профилях"};
    public static final String[] SPECIALIZATION_PLURAL_CASES = new String[]{"специализации", "специализаций", "специализациям", "специализации", "специализациями", "специализациях"};
    public static final String[] MASTER_PROGRAM_PLURAL_CASES = new String[]{"магистерские программы", "магистерских программ", "магистерским программам", "магистерские программы", "магистерскими программами", "магистерских программах"};

    public static final String WITH_REMOTE_EDU_TECH = " с применением дистанционных образовательных технологий";

    static
    {
        GRAMMA_CASE_POSTFIX.put(GrammaCase.NOMINATIVE, "N");
        GRAMMA_CASE_POSTFIX.put(GrammaCase.GENITIVE, "G");
        GRAMMA_CASE_POSTFIX.put(GrammaCase.DATIVE, "D");
        GRAMMA_CASE_POSTFIX.put(GrammaCase.ACCUSATIVE, "A");
        GRAMMA_CASE_POSTFIX.put(GrammaCase.INSTRUMENTAL, "I");
        GRAMMA_CASE_POSTFIX.put(GrammaCase.PREPOSITIONAL, "P");

        // male sex cases
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("gender", new String[]{"он", "его", "ему", "его", "им", "нем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("student", new String[]{"студент", "студента", "студенту", "студента", "студентом", "студенте"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("Student", new String[]{"Студент", "Студента", "Студенту", "Студента", "Студентом", "Студенте"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("learned", new String[]{"обучающийся", "обучающегося", "обучающемуся", "обучающегося", "обучающимся", "обучающимся"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("learned_past", new String[]{"обучался", "обучавшемуся", "обучавшемуся", "обучавшегося", "обучавшимся", "обучавшемуся"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("toBegin", new String[]{"приступивший", "приступившего", "приступившему", "приступившего", "приступившим", "приступившем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("developed", new String[]{"освоивший", "освоившего", "освоившему", "освоившего", "освоившим", "освоившем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("proved", new String[]{"проявивший", "проявившего", "проявившему", "проявившего", "проявившим", "проявившем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("tested", new String[]{"прошедший", "прошедшего", "прошедшему", "прошедшего", "прошедшим", "прошедшем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("usuePass", new String[]{"прошедший", "прошедшего", "прошедшему", "прошедшего", "прошедшим", "прошедшем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("finished", new String[]{"окончивший", "окончившего", "окончившему", "окончившего", "окончившим", "окончившем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("finishedAlt", new String[]{"закончивший", "закончившего", "закончившему", "закончившего", "закончившим", "закончившем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("executed", new String[]{"выполнивший", "выполнившего", "выполнившему", "выполнившего", "выполнившим", "выполнившем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("defended", new String[]{"защитивший", "защитившего", "защитившему", "защитившего", "защитившим", "защитившем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("passed", new String[]{"сдавший", "сдавшего", "сдавшему", "сдавшего", "сдавшим", "сдавшем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("usueOut", new String[]{"вышедший", "вышедшего", "вышедшему", "вышедшего", "вышедшим", "вышедшем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("exited_past", new String[]{"вышедший", "вышедшего", "вышедшему", "вышедшего", "вышедшим", "вышедшем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("admited_past", new String[]{"допущенный", "допущенного", "допущенному", "допущенного", "допущенным", "допущенном"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("appeared", new String[]{"явившийся", "явившегося", "явившемуся", "явившегося", "явившимся", "явившемся"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("dismissed", new String[]{"отчисленный", "отчисленного", "отчисленному", "отчисленного", "отчисленным", "отчисленном"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("accepted", new String[]{"зачиленный", "зачисленного", "зачисленному", "зачисленного", "зачисленным", "зачисленном"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("transferred", new String[]{"переведённый", "переведённого", "переведённому", "переведённого", "переведённым", "переведённом"});
        STUDENT_CATEGORY_MALE_SEX_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_STUDENT, new String[]{"студент", "студента", "студенту", "студента", "студентом", "студенте"});
        STUDENT_CATEGORY_MALE_SEX_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY, new String[]{"студент", "студента", "студенту", "студента", "студентом", "студенте"});
        STUDENT_CATEGORY_MALE_SEX_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_LISTENER, new String[]{"слушатель", "слушателя", "слушателю", "слушателя", "слушателем", "слушателе"});
        STUDENT_CATEGORY_MALE_SEX_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_DPP, new String[]{"слушатель", "слушателя", "слушателю", "слушателя", "слушателем", "слушателе"});

        // female sex cases
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("gender", new String[]{"она", "ее", "ей", "ее", "ей", "ней"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("student", new String[]{"студентка", "студентки", "студентке", "студентку", "студенткой", "студентке"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("Student", new String[]{"Студентка", "Студентки", "Студентке", "Студентку", "Студенткой", "Студентке"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("learned", new String[]{"обучающаяся", "обучающейся", "обучающейся", "обучающуюся", "обучающейся", "обучающейся"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("learned_past", new String[]{"обучалась", "обучавшейся", "обучавшейся", "обучавшуюся", "обучавшейся", "обучавшейся"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("toBegin", new String[]{"приступившая", "приступившей", "приступившей", "приступившую", "приступившей", "приступившем"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("developed", new String[]{"освоившая", "освоившей", "освоившей", "освоившую", "освоившей", "освоившей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("proved", new String[]{"проявившая", "проявившей", "проявившей", "проявившую", "проявившей", "проявившей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("tested", new String[]{"прошедшая", "прошедшей", "прошедшей", "прошедшую", "прошедшей", "прошедшей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("usuePass", new String[]{"прошедшая", "прошедшей", "прошедшей", "прошедшую", "прошедшей", "прошедшей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("finished", new String[]{"окончившая", "окончившей", "окончившей", "окончившую", "окончившей", "окончившей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("finishedAlt", new String[]{"закончившая", "закончившей", "закончившей", "закончившую", "закончившей", "закончившей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("executed", new String[]{"выполнившая", "выполнившей", "выполнившей", "выполнившую", "выполнившей", "выполнившей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("defended", new String[]{"защитившая", "защитившей", "защитившей", "защитившую", "защитившей", "защитившей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("passed", new String[]{"сдавшая", "сдавшей", "сдавшей", "сдавшую", "сдавшей", "сдавшей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("usueOut", new String[]{"вышедшая", "вышедшей", "вышедшей", "вышедшею", "вышедшей", "вышедшей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("exited_past", new String[]{"вышедшая", "вышедшей", "вышедшей", "вышедшею", "вышедшей", "вышедшей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("admited_past", new String[]{"допущенная", "допущенной", "допущенной", "допущенную", "допущенной", "допущенной"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("appeared", new String[]{"явившаяся", "явившейся", "явившейся", "явившуюся", "явившейся", "явившейся"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("dismissed", new String[]{"отчисленная", "отчисленной", "отчисленной", "отчисленную", "отчисленной", "отчисленной"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("accepted", new String[]{"зачисленная", "зачисленной", "зачисленной", "зачисленную", "зачисленной", "зачисленной"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("transferred", new String[]{"переведённая", "переведённой", "переведённой", "переведённую", "переведённой", "переведённой"});
        STUDENT_CATEGORY_FEMALE_SEX_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_STUDENT, new String[]{"студентка", "студентки", "студентке", "студентку", "студенткой", "студентке"});
        STUDENT_CATEGORY_FEMALE_SEX_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY, new String[]{"студентка", "студентки", "студентке", "студентку", "студенткой", "студентке"});
        STUDENT_CATEGORY_FEMALE_SEX_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_LISTENER, new String[]{"слушатель", "слушателя", "слушателю", "слушателя", "слушателем", "слушателе"});
        STUDENT_CATEGORY_FEMALE_SEX_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_DPP, new String[]{"слушатель", "слушателя", "слушателю", "слушателя", "слушателем", "слушателе"});

        SPEC_QUALIFICATION_CODES.add(QualificationsCodes.SPETSIALIST);
        SPEC_QUALIFICATION_CODES.add(QualificationsCodes.BAZOVYY_UROVEN_S_P_O);
        SPEC_QUALIFICATION_CODES.add(QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O);
    }

    public static void initCompensationType(RtfInjectModifier modifier, String postfix, CompensationType compensationType, Boolean spaceBefore, boolean isTargetAdmission )
    {
        if (compensationType != null)
        {
            initCompensationType(modifier, postfix, compensationType.isBudget(), spaceBefore, isTargetAdmission);
        }
        else
        {
            modifier.put("fefuCompensationTypeStr", "");
            modifier.put("usmaCompensationTypeStr", "");
            modifier.put("compensationTypeStr", "");
        }
    }

    public static void initCompensationType(RtfInjectModifier modifier, String postfix, Boolean budget, boolean isTargetAdmission)
    {
        initCompensationType(modifier, postfix, budget, false, isTargetAdmission);
    }

    public static String getFefuCompTypeStr(Boolean budget, Boolean spaceBefore)
    {
        return (spaceBefore ? " " : "") + (budget ?
                "за счет бюджетных ассигнований федерального бюджета" :
                "на основе договора об оказании платных образовательных услуг");
    }

    private static void initCompensationType(RtfInjectModifier modifier, String postfix, Boolean budget, Boolean spaceBefore, boolean isTargetAdmission)
    {
        final String sp = spaceBefore ? " " : "";
        modifier.put("compensationTypeStr" + postfix, sp + (budget ? "на бюджетной основе" : "по договору, предусматривающему оплату обучения"));
        modifier.put("compensationTypeStr" + postfix + "_A", sp + (budget ? "на бюджетную основу" : "на контрактную основу"));
        modifier.put("compensationTypeStr" + postfix + "_G", sp + (budget ? "бюджетной основы" : "контрактной основы"));
        modifier.put("compensationTypeStr" + postfix + "_Alt", sp + (budget ? "на бюджетной основе" : "на внебюджетной основе"));
        modifier.put("compensationTypeStr" + postfix + "_D_Alt", sp + (budget ? "бюджетной основе" : "внебюджетной основе"));
        modifier.put("compensationTypeStr" + postfix + "_G_Alt", sp + (budget ? "бюджетной основы" : "внебюджетной основы"));
        if (postfix.equals(""))
            modifier.put("compensationTypeStr" + postfix + "_A_Alt", sp + (budget ? "на бюджетную основу" : "на внебюджетную основу"));
        else
            modifier.put("compensationTypeStr" + postfix + "_A_Alt", sp + (budget ? "бюджетную основу" : "внебюджетную основу"));
        modifier.put("usueCompensationTypeStr" + postfix, sp + (budget ? "на бюджетной основе" : "на коммерческой основе"));
        modifier.put("usueCompensationTypeStr" + postfix + "_A", sp + (budget ? "на бюджетную основу" : "на коммерческую основу"));
        modifier.put("usueCompensationTypeStr" + postfix + "_G", sp + (budget ? "бюджетной основы" : "коммерческой основы"));
        modifier.put("fefuCompensationTypeStr" + postfix, getFefuCompTypeStr(budget, spaceBefore));

        String admissionType;
        String admissionType_A;
        String admissionType_Other;
        if (isTargetAdmission)
        {
            admissionType = budget ? "целевой " : "целевому ";
            admissionType_A = "целевую ";
            admissionType_Other = "целевой ";
        }
        else admissionType = admissionType_A = admissionType_Other = "";
        modifier.put("usmaCompensationTypeStr" + postfix, sp + (budget ? "на " + admissionType + "бюджетной основе" : admissionType + "по " + admissionType + "договору, предусматривающему оплату обучения"));
        modifier.put("usmaCompensationTypeStr" + postfix + "_A", sp + (budget ? "на " + admissionType_A + "бюджетную основу" : admissionType + "на " + admissionType_A + "контрактную основу"));
        modifier.put("usmaCompensationTypeStr" + postfix + "_G", sp + (budget ? admissionType_Other + "бюджетной основы" : admissionType_Other + "контрактной основы"));
        modifier.put("usmaCompensationTypeStr" + postfix + "_D_Alt", sp + (budget ? admissionType_Other + "бюджетной основе" : admissionType_Other + "внебюджетной основе"));
        modifier.put("usmaCompensationTypeStr" + postfix + "_G_Alt", sp + (budget ? admissionType_Other + "бюджетной основы" : admissionType_Other + "внебюджетной основы"));
        modifier.put("usmaCompensationTypeStr" + postfix + "_A_Alt", sp + (budget ? admissionType_A + "бюджетную основу" : admissionType_A + "внебюджетную основу"));
    }

    public static void initFefuGroup(RtfInjectModifier modifier, String label, Group group, DevelopForm developForm, String prefix)
    {
        modifier.put(label, (group == null || !DevelopFormCodes.FULL_TIME_FORM.equals(developForm.getCode())) ? "" : (prefix + group.getTitle()));
    }

    private static void initGroup(RtfInjectModifier modifier, String postfix, Group group, DevelopForm developForm)
    {
        modifier.put("group" + postfix, group == null ? "" : group.getTitle());
        initFefuGroup(modifier, "groupInternal" + postfix + "_G", group, developForm, " группы ");
    }

    public static RtfInjectModifier createModularExtractInjectModifier(ModularStudentExtract extract)
    {
        final IEntityMeta meta = EntityRuntime.getMeta(extract.getId());
        final Student student = extract.getEntity();
        final EducationOrgUnit eduOrgUnit = student.getEducationOrgUnit();

        final RtfInjectModifier modifier = new RtfInjectModifier();
        final String[] postfixList = new String[]{"Old", "New"};
        injectAcademy(modifier);

        modifier.put("studentPersonalNumber", student.getPersonalNumber());
        modifier.put("studentPersonalFileNumber", null != student.getPersonalFileNumber() ? student.getPersonalFileNumber() : "");
        modifier.put("studentBookNumber", null != student.getBookNumber() ? student.getBookNumber() : "");

        final boolean maleSex = student.getPerson().getIdentityCard().getSex().isMale();
        injectDifferentWords(modifier, maleSex ? DIFFERENT_WORD_MALE_SEX_CASES_ARRAY : DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY);

        // extractType
        modifier.put("extractType", extract.getParagraph() == null ? "проект приказа" : "выписка");
        // DEV-4953
        modifier.put("extractTypeAlt", extract.getParagraph() == null ? "Проект" : "Выписка");

        Map<String, String[]> studentCategoryCasesMap = maleSex ? STUDENT_CATEGORY_MALE_SEX_CASES_ARRAY : STUDENT_CATEGORY_FEMALE_SEX_CASES_ARRAY;
        String[] categoryCase = studentCategoryCasesMap.get(student.getStudentCategory().getCode());
        if (null != categoryCase)
        {
            for (String casePostfix : UniRtfUtil.CASE_POSTFIX)
            {
                modifier.put("studentCategory" + casePostfix, categoryCase[UniRtfUtil.CASE_POSTFIX.indexOf(casePostfix)]);
            }
        }


        /*
         * При печати выписки, если территориальное подр. не указано, то в () должно выводиться название населенного
         * пункта из фактического адреса объекта "ОУ", в офрмате "г. Екатеринбург", если фактический адрес не заполнен,
         * то в () будет пусто. Сейчас в () пусто всегда, когда не указано территориальное подр.. Если же территориальное
         * подр. указано, то в скобочках выводить название населенного пункта из фактического адреса территориального
         * подр., в формате "г. Саратов", если в фактическом адресе не указано населенного пункта, то выводим
         * в () название территориального подр. в именительном падеже, если и в именительном падеже название не задано,
         * то выводим просто в стандартном формате "название подразделения (тип подразделения)".
         */

        // property + (Old | New)
        for (String postfix : postfixList)
        {
            if (meta.getProperty("educationOrgUnit" + postfix) != null)
            {
                EducationOrgUnit postfixEducationOrgUnit = (EducationOrgUnit) extract.getProperty("educationOrgUnit" + postfix);

                // orgUnit (+*)
                initOrgUnit(modifier, postfixEducationOrgUnit, "orgUnit", postfix);
                initOrgUnit(modifier, postfixEducationOrgUnit, "formativeOrgUnitStr", postfix);
                initTerritorialOrgUnit(modifier, postfixEducationOrgUnit.getTerritorialOrgUnit(), "territorialOrgUnit", postfix);

                // educationType (+*)
                UniRtfUtil.initEducationType(modifier, postfixEducationOrgUnit.getEducationLevelHighSchool(), postfix);
                initEducationType(modifier, postfixEducationOrgUnit, postfix);

                // developForm (+*)
                initDevelopForm(modifier, postfixEducationOrgUnit, postfix);

                // educationOrgUnit
                modifier.put("educationOrgUnit" + postfix, postfixEducationOrgUnit.getEducationLevelHighSchool().getPrintTitleWithoutOrgUnit());

                // Новые направления
                EducationLevels educationLevels = postfixEducationOrgUnit.getEducationLevelHighSchool().getEducationLevel();
                initProgramSubjectLabels(modifier, educationLevels.getEduProgramSubject(), educationLevels.getEduProgramSpecialization(), postfix);
            }
        }
        // orgUnit simple
        initOrgUnit(modifier, eduOrgUnit, "orgUnit", "");
        initOrgUnit(modifier, eduOrgUnit, "formativeOrgUnitStr", "");
        initTerritorialOrgUnit(modifier, eduOrgUnit.getTerritorialOrgUnit(), "territorialOrgUnit", "");

        // educationOrgUnit
        modifier.put("educationOrgUnit", eduOrgUnit.getEducationLevelHighSchool().getPrintTitleWithoutOrgUnit());

        // Новые направления
        EducationLevels educationLevels = eduOrgUnit.getEducationLevelHighSchool().getEducationLevel();
        initProgramSubjectLabels(modifier, educationLevels.getEduProgramSubject(), educationLevels.getEduProgramSpecialization(), "");

        // educationType simple
        UniRtfUtil.initEducationType(modifier, eduOrgUnit.getEducationLevelHighSchool(), "");
        initEducationType(modifier, eduOrgUnit, "");

        // developForm simple
        initDevelopForm(modifier, eduOrgUnit, "");

        // compensationTypeStr
        for (String postfix : postfixList)
        {
            // compensationTypeStr (+*)
            if (meta.getProperty("compensationType" + postfix) != null)
            {
                boolean budget = ((CompensationType) (extract.getProperty("compensationType" + postfix))).isBudget();
                initCompensationType(modifier, postfix, budget, student.isTargetAdmission());
            }
        }
        initCompensationType(modifier, "", student.getCompensationType().isBudget(), student.isTargetAdmission());

        //Вид ОП eduProgramKind
        EduProgramKind programKind = educationLevels.getEduProgramSubject() == null? null : educationLevels.getEduProgramSubject().getEduProgramKind();
        initEduProgramKind("eduProgramKind", modifier, programKind);


        // course
        for (String postfix : postfixList)
        {
            if (meta.getProperty("course" + postfix) != null)
                modifier.put("course" + postfix, ((Course) extract.getProperty("course" + postfix)).getTitle());
        }
        modifier.put("course", student.getCourse().getTitle());

        // fio
        modifier.put("fio", extract.getStudentTitle());
        modifier.put("FIO", extract.getStudentTitle().toUpperCase());

        // fio in Nominative
        modifier.put("fio_N", student.getPerson().getFullFio());
        modifier.put("FIO_N", student.getPerson().getFullFio().toUpperCase());

        // all fio cases and variations
        injectFio(modifier, student.getPerson().getIdentityCard());

        // group
        for (String postfix : postfixList)
        {
            if (meta.getProperty("group" + postfix) != null)
                initGroup(modifier, postfix, (Group) extract.getProperty("group" + postfix), eduOrgUnit.getDevelopForm());
        }
        initGroup(modifier, "", student.getGroup(), eduOrgUnit.getDevelopForm());

        // developCondition, developTech
        injectFefuDevelopConditionAndTech(modifier, eduOrgUnit, student.getGroup(), "fefuShortFastExtendedOptionalText");

        // developPeriod NOT USED

        final Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(eduOrgUnit.getEducationLevelHighSchool().getEducationLevel());
        final String eduLevelHighSchoolTitle = eduOrgUnit.getEducationLevelHighSchool().getTitle();
        String educationOrgUnitWithLevel;

        //educationOrgUnitWithLevel
        if (null == qualification)
        {
            educationOrgUnitWithLevel = "по направлению \"" + eduLevelHighSchoolTitle + "\"";
        }
        else
        {
            switch (qualification.getCode())
            {
                case QualificationsCodes.MAGISTR:
                    //направление магистров
                    educationOrgUnitWithLevel = "по направлению магистратуры \"" + eduLevelHighSchoolTitle + "\"";
                    break;
                case QualificationsCodes.BAKALAVR:
                    //направление бакалавров
                    educationOrgUnitWithLevel = "по направлению бакалавриата \"" + eduLevelHighSchoolTitle + "\"";
                    break;
                case QualificationsCodes.SPETSIALIST:
                    //направление специальностей
                    educationOrgUnitWithLevel = "по специальности \"" + eduLevelHighSchoolTitle + "\"";
                    break;
                case QualificationsCodes.BAZOVYY_UROVEN_S_P_O:
                    //специальность (базовый уровень)
                    educationOrgUnitWithLevel = "по специальности \"" + eduLevelHighSchoolTitle + " (базовый уровень)\"";
                    break;
                case QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O:
                    //специальность (повышенный уровень)
                    educationOrgUnitWithLevel = "по специальности \"" + eduLevelHighSchoolTitle + " (повышенный уровень)\"";
                    break;
                default:
                    educationOrgUnitWithLevel = "по специальности \"" + eduLevelHighSchoolTitle + "\"";
            }
        }
        modifier.put("educationOrgUnitWithLevel", educationOrgUnitWithLevel);


        // orderType
        modifier.put("orderType", extract.getType().getTitle());

        // date
        if (meta.getProperty("date") != null)
            modifier.put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) extract.getProperty("date")));

        // createDate
        modifier.put("createDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getCreateDate()));

        // commitDate
        Date commitDate;
        if (extract.getParagraph() == null || (commitDate = extract.getParagraph().getOrder().getCommitDate()) == null)
            modifier.put("commitDate", "        ");
        else
            modifier.put("commitDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(commitDate));

        // order number
        modifier.put("orderNumber", extract.getParagraph() == null ? "     " : extract.getParagraph().getOrder().getNumber());

        // extractNumber
        modifier.put("extractNumber", extract.getParagraph() == null ? "     " : Integer.toString(extract.getParagraph().getNumber()));

        // listBasics
        modifier.put("listBasics", extract.getBasicListStr());

        // reason
        injectReason(modifier, extract.getReason(), extract.getReasonComment());

        // SPECIAL CODES
        if (meta.getProperty("firstNameNew") != null)
        {
            String firstNameNew = (String) extract.getProperty("firstNameNew");
            String lastNameNew = (String) extract.getProperty("lastNameNew");
            String middleNameNew = (String) extract.getProperty("middleNameNew");
            StringBuilder str = new StringBuilder(lastNameNew);
            if (StringUtils.isNotEmpty(firstNameNew))
                str.append(" ").append(firstNameNew);
            if (StringUtils.isNotEmpty(middleNameNew))
                str.append(" ").append(middleNameNew);
            modifier.put("fullFioNew", str.toString());
            modifier.put("firstNameNew", firstNameNew);
            modifier.put("lastNameNew", lastNameNew);
            modifier.put("middleNameNew", middleNameNew);
        }

        modifier.put("birthDate", student.getPerson().getBirthDateStr());

        if (meta.getProperty("university") != null)
            modifier.put("university", (String) extract.getProperty("university"));
        if (meta.getProperty("deadline") != null)
            modifier.put("deadline", DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) extract.getProperty("deadline")));
        if (meta.getProperty("canceledExtract") != null)
        {
            ModularStudentExtract canceledExtract = (ModularStudentExtract) extract.getProperty("canceledExtract");
            modifier.put("revertedOrderNumber", canceledExtract.getParagraph() == null ? "" : canceledExtract.getParagraph().getOrder().getNumber());
            modifier.put("revertedExtractNumber", canceledExtract.getParagraph() == null ? "" : Integer.toString(canceledExtract.getParagraph().getNumber()));
            modifier.put("revertedCommitDate", canceledExtract.getParagraph() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(canceledExtract.getParagraph().getOrder().getCommitDate()) + " г.");
            modifier.put("revertedOrderType", canceledExtract.getType().getTitle());
        }

        // DEV-4482
        modifier.put("beginDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginDate()));
        modifier.put("endDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndDate()));

        modifier.put("VISAS", ""); //TODO: заглушка. Какой-то умелец удалил зачем-то вставку виз в выписках. Будем искать...

        // Точка расширения для проектных модулей
        MoveStudentPrintModifierManager.instance().modifier().modularExtractModifier(modifier, extract);
        return modifier;
    }

    public static final BiFunction<ITitled, String, String> TITLED_ENTITY_LABEL_FUNC = (titledEntity, inflectedVariant) -> {
        String result = null;
        if (StringUtils.isNotEmpty(inflectedVariant)) {
            result = inflectedVariant;
        } else if (titledEntity != null) {
            result = titledEntity.getTitle();
        }
        return StringUtils.lowerCase(result);
    };


    public static void initEduProgramKind(String label, RtfInjectModifier modifier, EduProgramKind programKind) {
        modifier.put(label, TITLED_ENTITY_LABEL_FUNC.apply(programKind, null));
        Map<InflectorVariant, String> declinableMap = DeclinableManager.instance().dao().getPropertyValuesMap(programKind, EduProgramKind.P_TITLE);
        for (Map.Entry<GrammaCase, String> entry : GRAMMA_CASE_POSTFIX.entrySet())
        {
            InflectorVariant inflectorVariant = DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(entry.getKey());
            String inflectedValue = TITLED_ENTITY_LABEL_FUNC.apply(programKind, declinableMap.get(inflectorVariant));
            String labelWithPostfix = label + "_" + entry.getValue();
            modifier.put(labelWithPostfix, inflectedValue);
        }
    }

    public static void injectReason(RtfInjectModifier modifier, StudentOrderReasons reason, String reasonComment)
    {
        String title = reason != null ? reason.getTitle() : "";
        String printReason = reason != null && !StringUtils.isEmpty(reason.getPrintTitle()) ? reason.getPrintTitle() : title;

        modifier.put("reasonTitle", title);
        modifier.put("ReasonTitle", StringUtils.capitalize(title));
        modifier.put("reasonShort", printReason);
        modifier.put("ReasonShort", StringUtils.capitalize(printReason));

        if (".".equals(printReason))
            printReason = reasonComment != null ? reasonComment : "";
        else if (reason != null && !StringUtils.isEmpty(reasonComment))
            printReason += " " + reasonComment;

        String printReasonCap = StringUtils.capitalize(printReason);
        modifier.put("reason", printReason);
        modifier.put("Reason", printReasonCap);
        modifier.put("reasonPrint", printReason);
        modifier.put("ReasonPrint", printReasonCap);
    }

    public static RtfInjectModifier createModularOrderInjectModifier(StudentModularOrder order, boolean individual, List<ModularStudentExtract> extractList)
    {
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("orderId", String.valueOf(order.getId()));
        injectModifier.put("orderIdWithLabel", "[orderId]=" + order.getId());

        injectAcademy(injectModifier);
        injectModifier.put("DEVELOPFORM", "");
        injectModifier.put("commitDate", order.getCommitDate() == null ? "          " : DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()));
        injectModifier.put("orderNumber", StringUtils.isEmpty(order.getNumber()) ? "          " : order.getNumber());
        String executor = order.getExecutor();
        injectModifier.put("executor", "");
        injectModifier.put("executorPhone", "");
        if (StringUtils.isNotEmpty(executor))
        {
            int phoneStart = executor.indexOf(" т. ");
            if (phoneStart == -1)
            {
                injectModifier.put("executor", executor);
                injectModifier.put("executorPhone", "");
            }
            else
            {
                injectModifier.put("executor", executor.substring(0, phoneStart));
                injectModifier.put("executorPhone", executor.substring(phoneStart + 1));
            }
        }

        initOrgUnit(injectModifier, order.getOrgUnit(), "orderOrgUnit", "");

        if (individual) {
            MoveStudentPrintModifierManager.instance().modifier().individualOrderModifier(injectModifier, order, extractList.get(0));
        } else {
            MoveStudentPrintModifierManager.instance().modifier().modularOrderModifier(injectModifier, order);
        }
        return injectModifier;
    }

    public static RtfTableModifier createModularOrderTableModifier(StudentModularOrder order, boolean individual, List<ModularStudentExtract> extractList)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(order);
        List<String[]> visaData = new ArrayList<>();
        List<String[]> primaryVisaData = new ArrayList<>();
        List<String[]> secondaryVisaData = new ArrayList<>();
        Map<String, List<String[]>> printLabelMap = new HashMap<>();
        // DEV-5087
        // мапа для определения, выводить печатное название группы или уже нет
        Map<String, Boolean> printTitleMap = new HashMap<>();

        for (GroupsMemberVising group : UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(GroupsMemberVising.class))
        {
            printLabelMap.put(group.getPrintLabel(), new ArrayList<>());
            // DEV-5087
            // для меток с печатным названием групп
            printLabelMap.put(group.getPrintLabelTitle(), new ArrayList<>());
            printTitleMap.put(group.getPrintLabelTitle(), false);
        }

        for (Visa visa : visaList)
        {
            final IdentityCard identityCard = (IdentityCard) visa.getPossibleVisa().getEntity().getPerson().getIdentityCard();
            final String iof = identityCard.getIof();

            printLabelMap.get(visa.getGroupMemberVising().getPrintLabel()).add(new String[]{visa.getPossibleVisa().getTitle(), iof});
            // DEV-5087
            // для этой группы мы уже вывели печатное название?
            if (!printTitleMap.get(visa.getGroupMemberVising().getPrintLabelTitle()))
            {
                printTitleMap.put(visa.getGroupMemberVising().getPrintLabelTitle(), true);
                printLabelMap.get(visa.getGroupMemberVising().getPrintLabelTitle()).add(new String[]{visa.getGroupMemberVising().getPrintTitle(), ""});
            }
            printLabelMap.get(visa.getGroupMemberVising().getPrintLabelTitle()).add(new String[]{visa.getPossibleVisa().getTitle(), iof});

            visaData.add(new String[]{visa.getPossibleVisa().getTitle(), iof});

            if (visa.getGroupMemberVising().getCode().equals("2"))
                primaryVisaData.add(new String[]{visa.getPossibleVisa().getTitle(), iof});
            else
                secondaryVisaData.add(new String[]{visa.getPossibleVisa().getTitle(), iof});
        }

        for (Map.Entry<String, List<String[]>> entry : printLabelMap.entrySet())
        {
            if (!entry.getValue().isEmpty())
                tableModifier.put(entry.getKey(), entry.getValue().toArray(new String[entry.getValue().size()][]));
            else
                tableModifier.put(entry.getKey(), new String[][]{});
        }

        // эти метки выводятся _всегда_ по-старому, без печатных названий групп!
        tableModifier.put("VISAS", visaData.toArray(new String[visaData.size()][]));
        tableModifier.put("PRIMARY_VISAS", primaryVisaData.toArray(new String[primaryVisaData.size()][]));
        tableModifier.put("SECONDARY_VISAS", secondaryVisaData.toArray(new String[secondaryVisaData.size()][]));

        if (individual) {
            MoveStudentPrintModifierManager.instance().modifier().individualOrderTableModifier(tableModifier, order, extractList.get(0));
        } else {
            MoveStudentPrintModifierManager.instance().modifier().modularOrderTableModifier(tableModifier, order);
        }

        return tableModifier;
    }

    public static void initOrgUnit(RtfInjectModifier modifier, EducationOrgUnit educationOrgUnit, String prefix, String postfix)
    {
        initOrgUnit(modifier, educationOrgUnit, educationOrgUnit.getFormativeOrgUnit(), educationOrgUnit.getTerritorialOrgUnit(), prefix, postfix);
    }

    public static void initOrgUnit(RtfInjectModifier modifier, OrgUnit formativeOrgunit, String prefix, String postfix)
    {
        initOrgUnit(modifier, (EducationLevels) null, formativeOrgunit, null, prefix, postfix);
    }

    public static String getFefuTerritorialStr(EducationLevels eduLevel, OrgUnit territorialOrgUnit)
    {
        return (eduLevel != null && eduLevel.getLevelType().isMiddle()) ?
                "" : getFefuTerritorialStr(territorialOrgUnit);
    }

    public static String getFefuTerritorialStr(OrgUnit territorialOrgUnit)
    {
        if (territorialOrgUnit != null && !(territorialOrgUnit instanceof TopOrgUnit))
        {
            String normativeCase = territorialOrgUnit.getNominativeCaseTitle();
            return " (" + (normativeCase == null ? territorialOrgUnit.getFullTitle() : normativeCase) + ")";
        }
        return "";
    }

    public static void initOrgUnit(RtfInjectModifier modifier, EducationOrgUnit educationOrgUnit, OrgUnit orgUnit, OrgUnit territorialOrgUnit, String prefix, String postfix)
    {
        initOrgUnit(modifier, educationOrgUnit.getEducationLevelHighSchool().getEducationLevel(), orgUnit, territorialOrgUnit, prefix, postfix);
    }

    public static void initOrgUnit(RtfInjectModifier modifier, EducationLevels eduLevel, OrgUnit orgUnit, OrgUnit territorialOrgUnit, String prefix, String postfix)
    {
        String title = "";
        String territorialTitle = getFefuTerritorialStr(eduLevel, territorialOrgUnit);

        String nominative = null;
        String genitive = null;
        String dative = null;
        String accusative = null;
        String instrumental = null;
        String prepositional = null;
        String label;

        if (orgUnit != null)
        {
            title = orgUnit.getFullTitle();
            nominative = orgUnit.getNominativeCaseTitle();
            genitive = orgUnit.getGenitiveCaseTitle();
            dative = orgUnit.getDativeCaseTitle();
            accusative = orgUnit.getAccusativeCaseTitle();
            instrumental = orgUnit.getInstrumentalCaseTitle();
            prepositional = orgUnit.getPrepositionalCaseTitle();
        }

        label = prefix + postfix;
        modifier.put(label, nominative == null ? title : nominative);
        modifier.put(label + "_N", nominative == null ? title : nominative);
        modifier.put(label + "_G", genitive == null ? title : genitive);
        modifier.put(label + "_D", dative == null ? title : dative);
        modifier.put(label + "_A", accusative == null ? title : accusative);
        modifier.put(label + "_I", instrumental == null ? title : instrumental);
        modifier.put(label + "_P", prepositional == null ? title : prepositional);

        label = prefix + "WithTerritorial" + postfix;
        modifier.put(label, (nominative == null ? title : nominative) + territorialTitle);
        modifier.put(label + "_N", (nominative == null ? title : nominative) + territorialTitle);
        modifier.put(label + "_G", (genitive == null ? title : genitive) + territorialTitle);
        modifier.put(label + "_D", (dative == null ? title : dative) + territorialTitle);
        modifier.put(label + "_A", (accusative == null ? title : accusative) + territorialTitle);
        modifier.put(label + "_I", (instrumental == null ? title : instrumental) + territorialTitle);
        modifier.put(label + "_P", (prepositional == null ? title : prepositional) + territorialTitle);

        if (null != orgUnit)
        {
            label = "_" + prefix + postfix;
            modifier.put(label, nominative == null ? (" " + title) : (" " + nominative));
            modifier.put(label + "_N", nominative == null ? (" " + title) : (" " + nominative));
            modifier.put(label + "_G", genitive == null ? (" " + title) : (" " + genitive));
            modifier.put(label + "_D", dative == null ? (" " + title) : (" " + dative));
            modifier.put(label + "_A", accusative == null ? (" " + title) : (" " + accusative));
            modifier.put(label + "_I", instrumental == null ? (" " + title) : (" " + instrumental));
            modifier.put(label + "_P", prepositional == null ? (" " + title) : (" " + prepositional));

            label = "_" + prefix + "WithTerritorial" + postfix;
            modifier.put(label, (nominative == null ? (" " + title) : (" " + nominative)) + territorialTitle);
            modifier.put(label + "_N", (nominative == null ? (" " + title) : (" " + nominative)) + territorialTitle);
            modifier.put(label + "_G", (genitive == null ? (" " + title) : (" " + genitive)) + territorialTitle);
            modifier.put(label + "_D", (dative == null ? (" " + title) : (" " + dative)) + territorialTitle);
            modifier.put(label + "_A", (accusative == null ? (" " + title) : (" " + accusative)) + territorialTitle);
            modifier.put(label + "_I", (instrumental == null ? (" " + title) : (" " + instrumental)) + territorialTitle);
            modifier.put(label + "_P", (prepositional == null ? (" " + title) : (" " + prepositional)) + territorialTitle);

            if (OrgUnitTypeCodes.CATHEDRA.equals(orgUnit.getOrgUnitType().getCode()) || OrgUnitTypeCodes.FACULTY.equals(orgUnit.getOrgUnitType().getCode()))
            {
                modifier.put("orgUnitPrep", "на");
                modifier.put("orgUnitPrep" + postfix, "на");
                modifier.put("orgUnitPrep_IN", "на");
                modifier.put("orgUnitPrep_OUT", "с");
            }
            else
            {
                modifier.put("orgUnitPrep", "в");
                modifier.put("orgUnitPrep" + postfix, "в");
                modifier.put("orgUnitPrep_IN", "в");
                modifier.put("orgUnitPrep_OUT", "из");
            }
        }
        else
        {
            label = "_" + prefix + postfix;
            modifier.put(label, "");
            modifier.put(label + "_N", "");
            modifier.put(label + "_G", "");
            modifier.put(label + "_D", "");
            modifier.put(label + "_A", "");
            modifier.put(label + "_I", "");
            modifier.put(label + "_P", "");

            label = "_" + prefix + "WithTerritorial" + postfix;
            modifier.put(label, "");
            modifier.put(label + "_N", "");
            modifier.put(label + "_G", "");
            modifier.put(label + "_D", "");
            modifier.put(label + "_A", "");
            modifier.put(label + "_I", "");
            modifier.put(label + "_P", "");

            modifier.put("orgUnitPrep", "");
            modifier.put("orgUnitPrep" + postfix, "");
            modifier.put("orgUnitPrep_IN", "");
            modifier.put("orgUnitPrep_OUT", "");
        }
    }

    private static void initTerritorialOrgUnit(RtfInjectModifier modifier, OrgUnit orgUnit, String prefix, String postfix)
    {
        AddressDetailed address = orgUnit.getAddress();

        if (address == null || address.getSettlement() == null)
            modifier.put(prefix + postfix, "");
        else
            modifier.put(prefix + postfix, address.getSettlement().getAddressType().getShortTitle() + " " + address.getSettlement().getTitle());
    }

    public static void initDevelopForm(RtfInjectModifier modifier, EducationOrgUnit educationOrgUnit, String postfix)
    {
        initDevelopForm(modifier, educationOrgUnit.getDevelopForm(), postfix);
    }

    public static void initDevelopForm(RtfInjectModifier modifier, DevelopForm developForm, String postfix)
    {
        final String devForm = "developForm" + postfix;
        switch (developForm.getCode())
        {
            case DevelopFormCodes.FULL_TIME_FORM:
                modifier.put("DEVELOPFORM", "ОЧНОЕ ОТДЕЛЕНИЕ");

                // для мужского пола объекта, например "очное отделение"
                modifier.put(devForm, "очное");
                modifier.put(devForm + "_G", "очного");
                modifier.put(devForm + "_D", "очному");
                modifier.put(devForm + "_A", "очное");
                modifier.put(devForm + "_I", "очным");
                modifier.put(devForm + "_P", "очном");

                // для женского пола объекта, например "очная форма обучения"
                modifier.put(devForm + "__F", "очная");
                modifier.put(devForm + "_GF", "очной");
                modifier.put(devForm + "_DF", "очной");
                modifier.put(devForm + "_AF", "очную");
                modifier.put(devForm + "_IF", "очной");
                modifier.put(devForm + "_PF", "очной");
                break;
            case DevelopFormCodes.CORESP_FORM:
                modifier.put("DEVELOPFORM", "ЗАОЧНОЕ ОТДЕЛЕНИЕ");

                // для мужского пола объекта, например "очное отделение"
                modifier.put(devForm, "заочное");
                modifier.put(devForm + "_G", "заочного");
                modifier.put(devForm + "_D", "заочному");
                modifier.put(devForm + "_A", "заочное");
                modifier.put(devForm + "_I", "заочным");
                modifier.put(devForm + "_P", "заочном");

                // для женского пола объекта, например "очная форма обучения"
                modifier.put(devForm + "__F", "заочная");
                modifier.put(devForm + "_GF", "заочной");
                modifier.put(devForm + "_DF", "заочной");
                modifier.put(devForm + "_AF", "заочную");
                modifier.put(devForm + "_IF", "заочной");
                modifier.put(devForm + "_PF", "заочной");
                break;
            case DevelopFormCodes.PART_TIME_FORM:
                modifier.put("DEVELOPFORM", "ЗАОЧНОЕ ОТДЕЛЕНИЕ");

                // для мужского пола объекта, например "очное отделение"
                modifier.put(devForm, "очно-заочное");
                modifier.put(devForm + "_G", "очно-заочного");
                modifier.put(devForm + "_D", "очно-заочному");
                modifier.put(devForm + "_A", "очно-заочное");
                modifier.put(devForm + "_I", "очно-заочным");
                modifier.put(devForm + "_P", "очно-заочном");

                // для женского пола объекта, например "очная форма обучения"
                modifier.put(devForm + "__F", "очно-заочная");
                modifier.put(devForm + "_GF", "очно-заочной");
                modifier.put(devForm + "_DF", "очно-заочной");
                modifier.put(devForm + "_AF", "очно-заочную");
                modifier.put(devForm + "_IF", "очно-заочной");
                modifier.put(devForm + "_PF", "очно-заочной");
                break;
            case DevelopFormCodes.EXTERNAL_FORM:
                modifier.put("DEVELOPFORM", "");

                // для мужского пола объекта, например "очное отделение"
                modifier.put(devForm, "экстернат");
                modifier.put(devForm + "_G", "экстерната");
                modifier.put(devForm + "_D", "экстернату");
                modifier.put(devForm + "_A", "экстернат");
                modifier.put(devForm + "_I", "экстернатом");
                modifier.put(devForm + "_P", "экстернате");

                // для женского пола объекта, например "очная форма обучения"
                modifier.put(devForm + "__F", "экстернат");
                modifier.put(devForm + "_GF", "экстерната");
                modifier.put(devForm + "_DF", "экстернату");
                modifier.put(devForm + "_AF", "экстернат");
                modifier.put(devForm + "_IF", "экстернатом");
                modifier.put(devForm + "_PF", "экстернате");
                break;
            case DevelopFormCodes.APPLICANT_FORM:
                modifier.put("DEVELOPFORM", "");

                // для мужского пола объекта, например "очное отделение"
                modifier.put(devForm, "самостоятельное обучение и итоговая аттестация");
                modifier.put(devForm + "_G", "самостоятельного обучения и итоговой аттестации");
                modifier.put(devForm + "_D", "самостоятельному обучению и итоговой аттестации");
                modifier.put(devForm + "_A", "самостоятельное обучение и итоговую аттестацию");
                modifier.put(devForm + "_I", "самостоятельным обучением и итоговой аттестацией");
                modifier.put(devForm + "_P", "самостоятельном обучении и итоговой аттестации");

                // для женского пола объекта, например "очная форма обучения"
                modifier.put(devForm + "__F", "самостоятельное обучение и итоговая аттестация");
                modifier.put(devForm + "_GF", "самостоятельного обучения и итоговой аттестации");
                modifier.put(devForm + "_DF", "самостоятельному обучению и итоговой аттестации");
                modifier.put(devForm + "_AF", "самостоятельное обучение и итоговую аттестацию");
                modifier.put(devForm + "_IF", "самостоятельным обучением и итоговой аттестацией");
                modifier.put(devForm + "_PF", "самостоятельном обучении и итоговой аттестации");
                break;
            default:
                throw new IllegalStateException();
        }
    }

    private static void injectAcademy(RtfInjectModifier injectModifier)
    {
        TopOrgUnit academy = TopOrgUnit.getInstance();
        injectModifier.put("academy", null != academy.getNominativeCaseTitle() ? academy.getNominativeCaseTitle() : academy.getFullTitle());
        injectModifier.put("academy_N", null != academy.getNominativeCaseTitle() ? academy.getNominativeCaseTitle() : academy.getFullTitle());
        injectModifier.put("academy_G", null != academy.getGenitiveCaseTitle() ? academy.getGenitiveCaseTitle() : academy.getFullTitle());
        injectModifier.put("academy_D", null != academy.getDativeCaseTitle() ? academy.getDativeCaseTitle() : academy.getFullTitle());
        injectModifier.put("academy_A", null != academy.getAccusativeCaseTitle() ? academy.getAccusativeCaseTitle() : academy.getFullTitle());
        injectModifier.put("academy_P", null != academy.getPrepositionalCaseTitle() ? academy.getPrepositionalCaseTitle() : academy.getFullTitle());

        injectModifier.put("topOrgUnitFullTitle", academy.getTitle());
        injectModifier.put("topOrgUnitCityTitle",
                           (null != academy.getPostalAddress() && null != academy.getPostalAddress().getSettlement()) ? academy.getPostalAddress().getSettlement().getTitle() :
                                   (null != academy.getLegalAddress() && null != academy.getLegalAddress().getSettlement()) ? academy.getLegalAddress().getSettlement().getTitle() :
                                           (null != academy.getAddress() && null != academy.getAddress().getSettlement()) ? academy.getAddress().getSettlement().getTitle() : "");
    }

    public static void injectDifferentWords(RtfInjectModifier modifier, Map<String, String[]> differentWords)
    {
        for (Map.Entry<String, String[]> entry : differentWords.entrySet())
        {
            for (String casePostfix : UniRtfUtil.CASE_POSTFIX)
            {
                modifier.put(entry.getKey() + casePostfix, entry.getValue()[UniRtfUtil.CASE_POSTFIX.indexOf(casePostfix)]);
            }
        }
    }

    public static void initEducationType(RtfInjectModifier modifier, EducationOrgUnit educationOrgUnit, String postfix)
    {
        initEducationType(modifier, educationOrgUnit, postfix, true, true);
    }

    public static String getFefuHighEduLevelStr(EducationLevels eduLevel, String title)
    {
        StructureEducationLevels levelType = eduLevel.getLevelType();
        if (levelType.isSpecialization() || (levelType.isProfile() && (levelType.isMaster() || levelType.isBachelor())))
        {
            eduLevel = eduLevel.getParentLevel();
        }

        String fefuMiddleFgosCustomize = "";
        String code = eduLevel.getTitleCodePrefix();
        if (eduLevel.getLevelType().isSpecialty() && eduLevel.getLevelType().isMiddleGos3() &&
                (eduLevel.getEduProgramSubject() == null || EduProgramSubjectIndexCodes.TITLE_2009_50.equals(eduLevel.getEduProgramSubject().getSubjectIndex().getCode())))
        {
            if (code.endsWith("." + QualificationsCodes.BAZOVYY_UROVEN_S_P_O))
            {
                code = code.substring(0, code.length() - 3);
                fefuMiddleFgosCustomize = " базовой подготовки";
            }
            else if (code.endsWith("." + QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O))
            {
                code = code.substring(0, code.length() - 3);
                fefuMiddleFgosCustomize = " углубленной подготовки";
            }
        }

        return (levelType.isMiddle() ? "среднего профессионального образования " : "") +
                code +
                " «" + (StringUtils.isEmpty(title) ? eduLevel.getTitle() : title) + "»" +
                fefuMiddleFgosCustomize;
    }

    public static String getFefuHighEduLevelStr(EducationLevels eduLevel)
    {
        return getFefuHighEduLevelStr(eduLevel, null);
    }

    public static void initEducationType(RtfInjectModifier modifier, EducationOrgUnit educationOrgUnit, String postfix, boolean printParentLevel, boolean printChildLevel)
    {
        final boolean needComma = printParentLevel && printChildLevel;
        final EducationLevels eduLevel = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel();
        final StructureEducationLevels levelType = eduLevel.getLevelType();
        final String title = " «" + educationOrgUnit.getEducationLevelHighSchool().getTitle() + "»";
        final String labelUsue = "usueEducationStr" + postfix;
        final String labelFefu = "fefuEducationStr" + postfix;

        if (levelType.isSpecialization() || (levelType.isProfile() && (levelType.isMaster() || levelType.isBachelor())))
        {
            String parent = " «" + eduLevel.getParentLevel().getTitle() + "»";
            String fefuParent = getFefuHighEduLevelStr(eduLevel);

            String[] parents = levelType.isSpecialization() ? UniRtfUtil.SPECIALITY_CASES : DIRECTION_CASES;
            String[] children;
            if (levelType.isSpecialization())
                children = SPECIALIZATION_CASES; // специализация
            else if (levelType.isMaster())
                children = MASTER_PROGRAM_CASES; // магистерская программа
            else
                children = PROFILE_CASES; // бакалаврский профиль

            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
            {
                modifier.put(labelUsue + UniRtfUtil.CASE_POSTFIX.get(i),
                             (printParentLevel ? (parents[i] + parent) : "") + (printChildLevel ? " " + children[i] + title : ""));
                modifier.put(labelFefu + UniRtfUtil.CASE_POSTFIX.get(i),
                             (printParentLevel ? (parents[i] + " " + fefuParent) : "") + (needComma ? ", " : "") + (printChildLevel ? children[i] + title : ""));
            }
        }
        else
        {
            // специальност или направление

            String fefuTitle = getFefuHighEduLevelStr(eduLevel, educationOrgUnit.getEducationLevelHighSchool().getTitle());
            String[] types = levelType.isSpecialty() ? UniRtfUtil.SPECIALITY_CASES : DIRECTION_CASES;

            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
            {
                modifier.put(labelUsue + UniRtfUtil.CASE_POSTFIX.get(i), types[i] + title);
                modifier.put(labelFefu + UniRtfUtil.CASE_POSTFIX.get(i), types[i] + " " + fefuTitle);
            }
        }
    }

    public static void initProgramSubjectLabels(RtfInjectModifier modifier, EduProgramSubject programSubject, EduProgramSpecialization programSpecialization, String postfix)
    {
        modifier.put("programSubjectTitle" + postfix, programSubject != null ? programSubject.getTitle() : "");
        modifier.put("programSubjectCode" + postfix, programSubject != null ? programSubject.getSubjectCode() : "");
        modifier.put("programSubjectTitleWithCode" + postfix, programSubject != null ? programSubject.getTitleWithCode() : "");
        modifier.put("programSpecializationTitle" + postfix, programSpecialization != null ? programSpecialization.getTitle() : "");
    }

    public static void modifyEducationStr(RtfInjectModifier modifier, EducationLevels educationLevel, String[] labels)
    {
        modifyEducationStr(modifier, educationLevel, labels, true);
    }

    public static void modifyEducationStr(RtfInjectModifier modifier, EducationLevels educationLevel, String[] labels, Boolean printChild)
    {
        final StructureEducationLevels levelType = educationLevel.getLevelType();
        final String title = " «" + educationLevel.getTitle() + "»";
        final String direction = " " + getFefuHighEduLevelStr(educationLevel);
        String casePostfix, educationStrDirection;

        if (levelType.isSpecialization()) // специализация
        {
            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
            {
                educationStrDirection = UniRtfUtil.SPECIALITY_CASES[i] + direction + (printChild ? " " + SPECIALIZATION_CASES[i] + title : "");
                casePostfix = UniRtfUtil.CASE_POSTFIX.get(i);
                for (final String label : labels)
                {
                    modifier.put(label + casePostfix, educationStrDirection);
                }
            }
        }
        else if (levelType.isSpecialty()) // специальность
        {
            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
            {
                casePostfix = UniRtfUtil.CASE_POSTFIX.get(i);
                for (final String label : labels)
                {
                    modifier.put(label + casePostfix, UniRtfUtil.SPECIALITY_CASES[i] + direction);
                }
            }
        }
        else if (levelType.isProfile() && levelType.isBachelor()) // бакалаврский профиль
        {
            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
            {
                educationStrDirection = UniRtfUtil.EDU_DIRECTION_CASES[i] + direction + (printChild ? " " + PROFILE_CASES[i] + title : "");
                casePostfix = UniRtfUtil.CASE_POSTFIX.get(i);
                for (final String label : labels)
                {
                    modifier.put(label + casePostfix, educationStrDirection);
                }
            }
        }
        else if (levelType.isProfile() && levelType.isMaster()) // магистерский профиль
        {
            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
            {
                educationStrDirection = UniRtfUtil.EDU_DIRECTION_CASES[i] + direction + (printChild ? " " + MASTER_PROGRAM_CASES[i] + title : "");
                casePostfix = UniRtfUtil.CASE_POSTFIX.get(i);
                for (final String label : labels)
                {
                    modifier.put(label + casePostfix, educationStrDirection);
                }
            }
        }
        else // направление подготовки
        {
            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
            {
                educationStrDirection = UniRtfUtil.EDU_DIRECTION_CASES[i] + direction;
                casePostfix = UniRtfUtil.CASE_POSTFIX.get(i);
                for (final String label : labels)
                {
                    modifier.put(label + casePostfix, educationStrDirection);
                }
            }
        }
    }

    public static void modifyEducationStr(RtfInjectModifier modifier, EducationOrgUnit educationOrgUnit)
    {
        modifyEducationStr(modifier, educationOrgUnit.getEducationLevelHighSchool().getEducationLevel());
    }

    public static void modifyEducationStr(RtfInjectModifier modifier, EducationLevels educationLevels)
    {
        modifyEducationStr(modifier, educationLevels, "");
    }

    public static void modifyEducationStr(RtfInjectModifier modifier, EducationLevels educationLevels, String postfix)
    {
        modifyEducationStr(modifier, educationLevels, new String[]{"fefuEducationStr" + postfix, "educationStr" + postfix});
    }

    public static RtfInjectModifier addEntryDateToModifier(RtfInjectModifier modifier, Date entryDate)
    {
        modifier.put("entryIntoForceDate", null != entryDate ? DateFormatter.DEFAULT_DATE_FORMATTER.format(entryDate) : "__.__.____");
        modifier.put("entryIntoForceDateStr", null != entryDate ? new SimpleDateFormat("«dd» MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(entryDate) : "__ _______ ____");
        return modifier;
    }

    public static void injectFio(RtfInjectModifier modifier, IdentityCard identityCard)
    {
        int indexOfSpace;
        Map<InflectorVariant, String> fioDeclinations = PersonManager.instance().declinationDao().getCalculatedFIODeclinations(identityCard);

        for (InflectorVariant variant : DeclinationDao.INFLECTOR_VARIANT_LIST)
        {
            String postfix;
            if (InflectorVariantCodes.RU_NOMINATIVE.equals(variant.getCode())) postfix = "N";
            else if (InflectorVariantCodes.RU_GENITIVE.equals(variant.getCode())) postfix = "G";
            else if (InflectorVariantCodes.RU_DATIVE.equals(variant.getCode())) postfix = "D";
            else if (InflectorVariantCodes.RU_ACCUSATIVE.equals(variant.getCode())) postfix = "A";
            else if (InflectorVariantCodes.RU_INSTRUMENTAL.equals(variant.getCode())) postfix = "I";
            else if (InflectorVariantCodes.RU_PREPOSITION.equals(variant.getCode())) postfix = "P";
            else
                break;

            String fioDeclinated = fioDeclinations.get(variant);
            modifier.put("fio_" + postfix, fioDeclinated);

            indexOfSpace = fioDeclinated.indexOf(" ");
            modifier.put("Fio_" + postfix, indexOfSpace <= 0 ? fioDeclinated.toUpperCase() : (fioDeclinated.substring(0, indexOfSpace).toUpperCase() + fioDeclinated.substring(indexOfSpace)));
            modifier.put("FIO_" + postfix, fioDeclinated.toUpperCase());

            String[] fioArr = StringUtils.split(fioDeclinated, " ");
            String fioInitials = fioArr[0] + " " + fioArr[1].charAt(0) + "." + (fioArr.length > 2 ? (fioArr[2].charAt(0) + ".") : "");
            modifier.put("fio_" + postfix + "_initials", fioInitials);

            //indexOfSpace = fioDeclinated.indexOf(" ");
            modifier.put("Fio_" + postfix + "_initials", fioInitials.toUpperCase());
            modifier.put("FIO_" + postfix + "_initials", fioInitials.toUpperCase());
        }
    }

    /**
     * @return Возвращает строку формата Фамилия И.О.
     */
    public static String getModifiedFioInitials(IdentityCard identityCard, GrammaCase rusCase)
    {
        if (null == rusCase) return identityCard.getFullFio();

        InflectorVariant variant = DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(rusCase);
        CoreCollectionUtils.Pair<String, InflectorVariant> lastNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.lastName().s(), variant);
        String lastName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(lastNamePairKey);

        StringBuilder str = new StringBuilder(lastName);
        str.append(" ").append(identityCard.getFirstName().toCharArray()[0]).append(".");
        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            str.append(identityCard.getMiddleName().toCharArray()[0]).append(".");
        return str.toString();
    }

    public static void injectFefuDevelopConditionAndTech(RtfInjectModifier modifier, EducationOrgUnit eduOrgUnit, Group group, String key)
    {
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, eduOrgUnit, CommonListOrderPrint.getEducationBaseText(group), key);
    }


    public static void injectDiplomaEnd(RtfInjectModifier modifier, StructureEducationLevels level)
    {
        if (level.isBachelor())
        {
            modifier.put("diplomaEnd" + "_N", " (бакалаврская работа)");
            modifier.put("diplomaEnd" + "_G", " (бакалаврской работы)");
            modifier.put("diplomaEnd" + "_D", " (бакалаврской работе)");
            modifier.put("diplomaEnd" + "_A", " (бакалаврскую работу)");
            modifier.put("diplomaEnd" + "_I", " (бакалаврской работой)");
            modifier.put("diplomaEnd" + "_P", " (бакалаврской работе)");
        }
        else if (level.isMaster())
        {
            modifier.put("diplomaEnd" + "_N", " (магистерская диссертация)");
            modifier.put("diplomaEnd" + "_G", " (магистерской диссертации)");
            modifier.put("diplomaEnd" + "_D", " (магистерской диссертации)");
            modifier.put("diplomaEnd" + "_A", " (магистерскую диссертацию)");
            modifier.put("diplomaEnd" + "_I", " (магистерской диссертацией)");
            modifier.put("diplomaEnd" + "_P", " (магистерской диссертации)");
        }
        else
        {
            modifier.put("diplomaEnd" + "_N", "");
            modifier.put("diplomaEnd" + "_G", "");
            modifier.put("diplomaEnd" + "_D", "");
            modifier.put("diplomaEnd" + "_A", "");
            modifier.put("diplomaEnd" + "_I", "");
            modifier.put("diplomaEnd" + "_P", "");
        }
    }

    public static void injectFefuEducationLevelNewText(RtfInjectModifier modifier, String labelName, EducationOrgUnit educationOrgUnitOld, EducationOrgUnit educationOrgUnitNew)
    {
        injectFefuEducationLevelNewText(modifier, new String[]{labelName}, educationOrgUnitOld, educationOrgUnitNew);
    }

    public static void injectFefuEducationLevelNewText(RtfInjectModifier modifier, String[] labels, EducationOrgUnit educationOrgUnitOld, EducationOrgUnit educationOrgUnitNew)
    {
        String eduLevelText;
        EducationLevelsHighSchool levelsHighSchoolOld = educationOrgUnitOld.getEducationLevelHighSchool();
        EducationLevelsHighSchool levelsHighSchoolNew = educationOrgUnitNew.getEducationLevelHighSchool();
        StructureEducationLevels levelTypeNew = levelsHighSchoolNew.getEducationLevel().getLevelType();

        EducationLevels parentLevelOld = EducationOrgUnitUtil.getParentLevel(levelsHighSchoolOld);
        EducationLevels parentLevelNew = EducationOrgUnitUtil.getParentLevel(levelsHighSchoolNew);

        String parentOld = parentLevelOld.getLevelType().isSpecialty() ? "вышеуказанной специальности" : "вышеуказанному направлению";
        String parentNew = parentLevelNew.getLevelType().isSpecialty() ? "специальности" : "направлению";
        String childrenNew = levelTypeNew.isSpecialization() ? "специализации" : levelTypeNew.isMaster() ? "магистерской программе" : "профилю";

        if (levelsHighSchoolOld.equals(levelsHighSchoolNew))
        {
            eduLevelText = parentOld;
            if (levelsHighSchoolNew.getEducationLevel().isProfileOrSpecialization())
                eduLevelText += ", " + childrenNew;
        }
        else if (levelsHighSchoolNew.getEducationLevel().isProfileOrSpecialization())
        {
            if (parentLevelOld.equals(parentLevelNew))
                eduLevelText = parentOld + ", " + childrenNew + " «" + levelsHighSchoolNew.getTitle() + "»";
            else
                eduLevelText = parentNew + " «" + parentLevelNew.getTitle() + "», " + childrenNew + " «" + levelsHighSchoolNew.getTitle() + "»";
        }
        else
        {
            if (levelsHighSchoolOld.getEducationLevel().isProfileOrSpecialization() && parentLevelOld.equals(parentLevelNew))
                eduLevelText = parentOld;
            else
                eduLevelText = parentNew + " «" + levelsHighSchoolNew.getTitle() + "»";
        }
        for (String label : labels)
        {
            modifier.put(label, eduLevelText);
        }
    }

    public static void injectSpecializationTransferOrderTitlePart(RtfInjectModifier modifier, String labelName, EducationOrgUnit eduOUold, EducationOrgUnit eduOUnew)
    {
        Qualifications qualificationOld = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(eduOUold.getEducationLevelHighSchool().getEducationLevel());
        Qualifications qualificationNew = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(eduOUnew.getEducationLevelHighSchool().getEducationLevel());

        StringBuilder eduLevelText = new StringBuilder();

        if (null != qualificationOld && SPEC_QUALIFICATION_CODES.contains(qualificationOld.getCode()))
            eduLevelText.append("со специальности");
        else if (null != qualificationOld)
            eduLevelText.append("с направления");

        if (null != qualificationNew && SPEC_QUALIFICATION_CODES.contains(qualificationNew.getCode()))
            eduLevelText.append(eduLevelText.length() == 0 ? "со специальности" : "").append(" на специальность");
        else if (null != qualificationNew)
            eduLevelText.append(eduLevelText.length() == 0 ? "с направления" : "").append(" на направление");

        modifier.put(labelName, eduLevelText.toString());
    }

    public static RtfTableModifier createFefuVisasTableModifier(AbstractStudentExtract extract)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        IMoveStudentInjectModifier modifier = MoveStudentPrintModifierManager.instance().modifier();
        if (extract instanceof ModularStudentExtract)
        {
            modifier.modularExtractTableModifier(tableModifier, (ModularStudentExtract) extract);
        }
        else if (extract instanceof ListStudentExtract)
        {
            modifier.listExtractTableModifier(tableModifier, (ListStudentExtract) extract);
        }

        return tableModifier;
    }

    @SuppressWarnings({"unchecked"})
    public static <T> IPrintFormCreator<T> getPrintFormCreator(String name)
    {
        try
        {
            return ApplicationRuntime.getBean(name, IPrintFormCreator.class);
        }
        catch (NoSuchBeanDefinitionException e)
        {
            return null;
        }
    }

    public static String getEduLevelTypeStr_P(EducationLevels educationLevels)
    {
        String qCode = educationLevels.getSafeQCode();
        String eduLevelTypeStr = (QualificationsCodes.BAKALAVR.equals(qCode) || QualificationsCodes.MAGISTR.equals(qCode) || QualificationsCodes.SPETSIALIST.equals(qCode)) ? "высшем" : "среднем";
        if (educationLevels.getLevelType().isMiddle())
            eduLevelTypeStr += " профессиональном";
        return eduLevelTypeStr;
    }

    public static String getDevelopConditionStr_G(DevelopCondition developCondition)
    {
        switch (developCondition.getCode())
        {
            case UniDefines.DEVELOP_CONDITION_SHORT:
                return "сокращенной";
            case UniDefines.DEVELOP_CONDITION_FAST:
                return "ускоренной";
            case UniDefines.DEVELOP_CONDITION_SHORT_FAST:
                return "сокращенной ускоренной";
            default:
                return "";
        }
    }

    public static String getDevelopConditionStr2_G(DevelopCondition developCondition)
    {
        switch (developCondition.getCode())
        {
            case UniDefines.DEVELOP_CONDITION_SHORT:
                return " (сокращенной)";
            case UniDefines.DEVELOP_CONDITION_FAST:
                return " (ускоренной)";
            case UniDefines.DEVELOP_CONDITION_SHORT_FAST:
                return " (сокращенной ускоренной)";
            default:
                return "";
        }
    }

    public static String getDevelopFormStr_G(DevelopForm developForm)
    {
        switch (developForm.getCode())
        {
            case DevelopFormCodes.FULL_TIME_FORM:
                return "очной";
            case DevelopFormCodes.CORESP_FORM:
                return "заочной";
            case DevelopFormCodes.PART_TIME_FORM:
                return "очно-заочной";
            default:
                return "";
        }
    }

    public static String getCompensationTypeStr_G(CompensationType compensationType)
    {
        switch (compensationType.getCode())
        {
            case UniDefines.COMPENSATION_TYPE_BUDGET:
                return "бюджетной";
            case UniDefines.COMPENSATION_TYPE_CONTRACT:
                return "внебюджетной";
            default:
                return "";
        }
    }
}