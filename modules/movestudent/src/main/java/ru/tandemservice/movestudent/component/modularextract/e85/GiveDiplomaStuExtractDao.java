/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e85;

import org.tandemframework.core.CoreDateUtils;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.GiveDiplomaStuExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 03.09.2012
 */
public class GiveDiplomaStuExtractDao extends UniBaseDao implements IExtractComponentDao<GiveDiplomaStuExtract>
{
    public void doCommit(GiveDiplomaStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        extract.getEntity().setStatus(this.getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_GRADUATED_WITH_DIPLOMA));

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            extract.setPrevOrderDate(orderData.getGraduateDiplomaOrderDate());
            extract.setPrevOrderNumber(orderData.getGraduateDiplomaOrderNumber());
        }

        extract.setFinishedYear(student.getFinishYear());
        student.setFinishYear(CoreDateUtils.getYear(extract.getParagraph().getOrder().getCommitDate()));

        orderData.setGraduateDiplomaOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setGraduateDiplomaOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);
    }

    public void doRollback(GiveDiplomaStuExtract extract, Map parameters)
    {
        extract.getEntity().setStatus(extract.getStudentStatusOld());

        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }

        student.setFinishYear(extract.getFinishedYear());

        orderData.setGraduateDiplomaOrderDate(extract.getPrevOrderDate());
        orderData.setGraduateDiplomaOrderNumber(extract.getPrevOrderNumber());
        getSession().saveOrUpdate(orderData);
    }
}