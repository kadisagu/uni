/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeGroupCaptain.AddEdit;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.component.customorder.IStudentCustomOrderExtract;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditDAO;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.GroupToExtractRelation;
import ru.tandemservice.movestudent.entity.custom.ChangeGroupCaptainAction;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unimove.entity.ICustomOrderExtract;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class DAO extends BaseCustomActionAddEditDAO<ChangeGroupCaptainAction, Model>
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        Student student = model.getStudent();
        if (student == null)
            throw new IllegalStateException("Student must be.");

        if (model.isEdit())
            model.setCaptain(!model.getCustomAction().isGroupCaptain());
        else
        {
            List<Student> captainList = UniDaoFacade.getGroupDao().getCaptainStudentList(student.getGroup());
            model.setCaptain(student.getGroup() != null && captainList.contains(student));
        }
    }

    @Override
    protected Class<ChangeGroupCaptainAction> getActionClass()
    {
        return ChangeGroupCaptainAction.class;
    }

    @Override
    public void updatePrepare(Model model, ChangeGroupCaptainAction action, IStudentCustomOrderExtract extract)
    {
        action.setGroup(model.getStudent().getGroup());
        action.setGroupCaptain(!model.isCaptain());
    }

    @Override
    public void doCommit(ChangeGroupCaptainAction action)
    {
        Student student = (Student) action.getCustomExtract().getEntity();
        ICustomOrderExtract customExtract = action.getCustomExtract();
        Group group = student.getGroup();
        List<Student> captainList = UniDaoFacade.getGroupDao().getCaptainStudentList(group);

        if (action.isGroupCaptain())
        {
            if (group == null)
                throw new ApplicationException("Невозможно назначить старостой студента без группы.");

            action.setGroup(group);
            action.setCustomExtract(customExtract);

            for (Student captain : captainList)
            {
                GroupToExtractRelation groupToExtractRelation = new GroupToExtractRelation();
                groupToExtractRelation.setGroup(group);
                groupToExtractRelation.setExtract((AbstractStudentExtract) customExtract);
                groupToExtractRelation.setPrevGroupManager(captain);
                save(groupToExtractRelation);
            }

            UniDaoFacade.getGroupDao().deleteCaptainStudentList(group);
            UniDaoFacade.getGroupDao().addCaptainStudent(group, student);
        }
        else
        {
            group = action.getGroup();
            if (group == null || captainList.isEmpty() || !captainList.contains(student))
                throw new ApplicationException("Нельзя снять студента с должности старосты группы, т.к. он старостой данной группы не является, либо не имеет группы.");
            UniDaoFacade.getGroupDao().deleteCaptainStudent(group, student);
        }
    }

    @Override
    public void doRollback(ChangeGroupCaptainAction action)
    {
        Student student = (Student) action.getCustomExtract().getEntity();
        Group group = action.getGroup();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GroupToExtractRelation.class, "ge");
        builder.where(eq(property("ge", GroupToExtractRelation.group().id()), value(group.getId())));
        builder.where(eq(property("ge", GroupToExtractRelation.extract().id()), value(action.getCustomExtract().getId())));

        List<GroupToExtractRelation> prevCaptainActionList = builder.createStatement(getSession()).list();
        List<Student> captainList = UniDaoFacade.getGroupDao().getCaptainStudentList(group);

        UniDaoFacade.getGroupDao().deleteCaptainStudentList(group);

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(GroupToExtractRelation.class);
        deleteBuilder.where(eq(property(GroupToExtractRelation.extract().id()), value(action.getCustomExtract().getId())));
        deleteBuilder.createStatement(getSession()).execute();

        if (action.isGroupCaptain())
        {
            for (GroupToExtractRelation relation : prevCaptainActionList)
            {
                if (!captainList.contains(student))
                    throw new ApplicationException("Нельзя забрать у студента должности старосты, т.к. он старостой той группы уже не является.");

                UniDaoFacade.getGroupDao().addCaptainStudent(group, relation.getPrevGroupManager());
            }
        }
        else
        {
            UniDaoFacade.getGroupDao().addCaptainStudent(group, student);
        }
    }
}