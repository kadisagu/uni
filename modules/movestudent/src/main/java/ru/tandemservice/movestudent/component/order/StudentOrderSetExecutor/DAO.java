/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.StudentOrderSetExecutor;

import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * @author vip_delete
 * @since 15.12.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setOrder(refresh(this.<AbstractStudentOrder>getNotNull(model.getOrderId())));
        model.setEmployeePostModel(new OrderExecutorSelectModel());
    }

    @Override
    public void update(Model model)
    {
        AbstractStudentOrder order = model.getOrder();

        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(MoveStudentDaoFacade.getMoveStudentDao().getOrderStateCode(order.getId())))
            throw new ApplicationException("Редактирование приказа возможно только на этапе формирования.");

        order.setExecutor(OrderExecutorSelectModel.getExecutor(order.getExecutorEmpl()));
        update(order);
    }
}
