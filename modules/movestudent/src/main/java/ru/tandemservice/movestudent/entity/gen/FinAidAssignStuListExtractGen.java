package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «Об оказании материальной помощи»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FinAidAssignStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract";
    public static final String ENTITY_NAME = "finAidAssignStuListExtract";
    public static final int VERSION_HASH = 439806323;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_MONTH = "month";
    public static final String P_YEAR = "year";
    public static final String P_FIN_AID_SIZE = "finAidSize";
    public static final String P_PROTOCOL_NUMBER = "protocolNumber";
    public static final String L_RESPONSIBLE_FOR_PAYMENTS = "responsibleForPayments";
    public static final String L_RESPONSIBLE_FOR_AGREEMENT = "responsibleForAgreement";
    public static final String P_MATCHING_DATE = "matchingDate";

    private Course _course;     // Курс
    private Group _group;     // Группа
    private CompensationType _compensationType;     // Вид возмещения затрат
    private int _month;     // Месяц
    private int _year;     // Год
    private double _finAidSize;     // Размер материальной помощи
    private String _protocolNumber;     // Номер протокола
    private EmployeePost _responsibleForPayments;     // Ответственный за начисление выплат
    private EmployeePost _responsibleForAgreement;     // Ответственный за согласование начислений выплат
    private Date _matchingDate;     // Дата согласования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Месяц. Свойство не может быть null.
     */
    @NotNull
    public int getMonth()
    {
        return _month;
    }

    /**
     * @param month Месяц. Свойство не может быть null.
     */
    public void setMonth(int month)
    {
        dirty(_month, month);
        _month = month;
    }

    /**
     * @return Год. Свойство не может быть null.
     */
    @NotNull
    public int getYear()
    {
        return _year;
    }

    /**
     * @param year Год. Свойство не может быть null.
     */
    public void setYear(int year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Размер материальной помощи. Свойство не может быть null.
     */
    @NotNull
    public double getFinAidSize()
    {
        return _finAidSize;
    }

    /**
     * @param finAidSize Размер материальной помощи. Свойство не может быть null.
     */
    public void setFinAidSize(double finAidSize)
    {
        dirty(_finAidSize, finAidSize);
        _finAidSize = finAidSize;
    }

    /**
     * @return Номер протокола.
     */
    @Length(max=255)
    public String getProtocolNumber()
    {
        return _protocolNumber;
    }

    /**
     * @param protocolNumber Номер протокола.
     */
    public void setProtocolNumber(String protocolNumber)
    {
        dirty(_protocolNumber, protocolNumber);
        _protocolNumber = protocolNumber;
    }

    /**
     * @return Ответственный за начисление выплат.
     */
    public EmployeePost getResponsibleForPayments()
    {
        return _responsibleForPayments;
    }

    /**
     * @param responsibleForPayments Ответственный за начисление выплат.
     */
    public void setResponsibleForPayments(EmployeePost responsibleForPayments)
    {
        dirty(_responsibleForPayments, responsibleForPayments);
        _responsibleForPayments = responsibleForPayments;
    }

    /**
     * @return Ответственный за согласование начислений выплат.
     */
    public EmployeePost getResponsibleForAgreement()
    {
        return _responsibleForAgreement;
    }

    /**
     * @param responsibleForAgreement Ответственный за согласование начислений выплат.
     */
    public void setResponsibleForAgreement(EmployeePost responsibleForAgreement)
    {
        dirty(_responsibleForAgreement, responsibleForAgreement);
        _responsibleForAgreement = responsibleForAgreement;
    }

    /**
     * @return Дата согласования.
     */
    public Date getMatchingDate()
    {
        return _matchingDate;
    }

    /**
     * @param matchingDate Дата согласования.
     */
    public void setMatchingDate(Date matchingDate)
    {
        dirty(_matchingDate, matchingDate);
        _matchingDate = matchingDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FinAidAssignStuListExtractGen)
        {
            setCourse(((FinAidAssignStuListExtract)another).getCourse());
            setGroup(((FinAidAssignStuListExtract)another).getGroup());
            setCompensationType(((FinAidAssignStuListExtract)another).getCompensationType());
            setMonth(((FinAidAssignStuListExtract)another).getMonth());
            setYear(((FinAidAssignStuListExtract)another).getYear());
            setFinAidSize(((FinAidAssignStuListExtract)another).getFinAidSize());
            setProtocolNumber(((FinAidAssignStuListExtract)another).getProtocolNumber());
            setResponsibleForPayments(((FinAidAssignStuListExtract)another).getResponsibleForPayments());
            setResponsibleForAgreement(((FinAidAssignStuListExtract)another).getResponsibleForAgreement());
            setMatchingDate(((FinAidAssignStuListExtract)another).getMatchingDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FinAidAssignStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FinAidAssignStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new FinAidAssignStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "compensationType":
                    return obj.getCompensationType();
                case "month":
                    return obj.getMonth();
                case "year":
                    return obj.getYear();
                case "finAidSize":
                    return obj.getFinAidSize();
                case "protocolNumber":
                    return obj.getProtocolNumber();
                case "responsibleForPayments":
                    return obj.getResponsibleForPayments();
                case "responsibleForAgreement":
                    return obj.getResponsibleForAgreement();
                case "matchingDate":
                    return obj.getMatchingDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "month":
                    obj.setMonth((Integer) value);
                    return;
                case "year":
                    obj.setYear((Integer) value);
                    return;
                case "finAidSize":
                    obj.setFinAidSize((Double) value);
                    return;
                case "protocolNumber":
                    obj.setProtocolNumber((String) value);
                    return;
                case "responsibleForPayments":
                    obj.setResponsibleForPayments((EmployeePost) value);
                    return;
                case "responsibleForAgreement":
                    obj.setResponsibleForAgreement((EmployeePost) value);
                    return;
                case "matchingDate":
                    obj.setMatchingDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "group":
                        return true;
                case "compensationType":
                        return true;
                case "month":
                        return true;
                case "year":
                        return true;
                case "finAidSize":
                        return true;
                case "protocolNumber":
                        return true;
                case "responsibleForPayments":
                        return true;
                case "responsibleForAgreement":
                        return true;
                case "matchingDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "group":
                    return true;
                case "compensationType":
                    return true;
                case "month":
                    return true;
                case "year":
                    return true;
                case "finAidSize":
                    return true;
                case "protocolNumber":
                    return true;
                case "responsibleForPayments":
                    return true;
                case "responsibleForAgreement":
                    return true;
                case "matchingDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "compensationType":
                    return CompensationType.class;
                case "month":
                    return Integer.class;
                case "year":
                    return Integer.class;
                case "finAidSize":
                    return Double.class;
                case "protocolNumber":
                    return String.class;
                case "responsibleForPayments":
                    return EmployeePost.class;
                case "responsibleForAgreement":
                    return EmployeePost.class;
                case "matchingDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FinAidAssignStuListExtract> _dslPath = new Path<FinAidAssignStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FinAidAssignStuListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Месяц. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getMonth()
     */
    public static PropertyPath<Integer> month()
    {
        return _dslPath.month();
    }

    /**
     * @return Год. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getYear()
     */
    public static PropertyPath<Integer> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Размер материальной помощи. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getFinAidSize()
     */
    public static PropertyPath<Double> finAidSize()
    {
        return _dslPath.finAidSize();
    }

    /**
     * @return Номер протокола.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getProtocolNumber()
     */
    public static PropertyPath<String> protocolNumber()
    {
        return _dslPath.protocolNumber();
    }

    /**
     * @return Ответственный за начисление выплат.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getResponsibleForPayments()
     */
    public static EmployeePost.Path<EmployeePost> responsibleForPayments()
    {
        return _dslPath.responsibleForPayments();
    }

    /**
     * @return Ответственный за согласование начислений выплат.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getResponsibleForAgreement()
     */
    public static EmployeePost.Path<EmployeePost> responsibleForAgreement()
    {
        return _dslPath.responsibleForAgreement();
    }

    /**
     * @return Дата согласования.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getMatchingDate()
     */
    public static PropertyPath<Date> matchingDate()
    {
        return _dslPath.matchingDate();
    }

    public static class Path<E extends FinAidAssignStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<Integer> _month;
        private PropertyPath<Integer> _year;
        private PropertyPath<Double> _finAidSize;
        private PropertyPath<String> _protocolNumber;
        private EmployeePost.Path<EmployeePost> _responsibleForPayments;
        private EmployeePost.Path<EmployeePost> _responsibleForAgreement;
        private PropertyPath<Date> _matchingDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Месяц. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getMonth()
     */
        public PropertyPath<Integer> month()
        {
            if(_month == null )
                _month = new PropertyPath<Integer>(FinAidAssignStuListExtractGen.P_MONTH, this);
            return _month;
        }

    /**
     * @return Год. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getYear()
     */
        public PropertyPath<Integer> year()
        {
            if(_year == null )
                _year = new PropertyPath<Integer>(FinAidAssignStuListExtractGen.P_YEAR, this);
            return _year;
        }

    /**
     * @return Размер материальной помощи. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getFinAidSize()
     */
        public PropertyPath<Double> finAidSize()
        {
            if(_finAidSize == null )
                _finAidSize = new PropertyPath<Double>(FinAidAssignStuListExtractGen.P_FIN_AID_SIZE, this);
            return _finAidSize;
        }

    /**
     * @return Номер протокола.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getProtocolNumber()
     */
        public PropertyPath<String> protocolNumber()
        {
            if(_protocolNumber == null )
                _protocolNumber = new PropertyPath<String>(FinAidAssignStuListExtractGen.P_PROTOCOL_NUMBER, this);
            return _protocolNumber;
        }

    /**
     * @return Ответственный за начисление выплат.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getResponsibleForPayments()
     */
        public EmployeePost.Path<EmployeePost> responsibleForPayments()
        {
            if(_responsibleForPayments == null )
                _responsibleForPayments = new EmployeePost.Path<EmployeePost>(L_RESPONSIBLE_FOR_PAYMENTS, this);
            return _responsibleForPayments;
        }

    /**
     * @return Ответственный за согласование начислений выплат.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getResponsibleForAgreement()
     */
        public EmployeePost.Path<EmployeePost> responsibleForAgreement()
        {
            if(_responsibleForAgreement == null )
                _responsibleForAgreement = new EmployeePost.Path<EmployeePost>(L_RESPONSIBLE_FOR_AGREEMENT, this);
            return _responsibleForAgreement;
        }

    /**
     * @return Дата согласования.
     * @see ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract#getMatchingDate()
     */
        public PropertyPath<Date> matchingDate()
        {
            if(_matchingDate == null )
                _matchingDate = new PropertyPath<Date>(FinAidAssignStuListExtractGen.P_MATCHING_DATE, this);
            return _matchingDate;
        }

        public Class getEntityClass()
        {
            return FinAidAssignStuListExtract.class;
        }

        public String getEntityName()
        {
            return "finAidAssignStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
