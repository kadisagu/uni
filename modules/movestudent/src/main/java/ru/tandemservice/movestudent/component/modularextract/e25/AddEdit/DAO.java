/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e25.AddEdit;

import java.util.Date;

import org.tandemframework.core.info.ErrorCollector;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.SocGrantResumptionStuExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 13.05.2009
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<SocGrantResumptionStuExtract, Model>
{
    @Override
    protected SocGrantResumptionStuExtract createNewInstance()
    {
        return new SocGrantResumptionStuExtract();
    }
    
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }
    
    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        Date startDate = model.getExtract().getStartDate();
        Date endDate = model.getExtract().getEndDate();
        
        if (startDate.getTime() >= endDate.getTime())
            errors.add("Дата окончания выплаты стипендии должна быть позже даты начала", "endDate");
    }
}