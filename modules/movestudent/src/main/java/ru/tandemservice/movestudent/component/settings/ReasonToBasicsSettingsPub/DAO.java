/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.settings.ReasonToBasicsSettingsPub;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.entity.StudentReasonToBasicRel;
import ru.tandemservice.unibase.UniBaseUtils;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import ru.tandemservice.uni.dao.UniDao;

import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 16.10.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setFirst(get(model.getFirstId()));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(StudentReasonToBasicRel.ENTITY_NAME, "o");
        builder.add(MQExpression.eq("o", IEntityRelation.L_FIRST, model.getFirst()));
        builder.addOrder("o", StudentReasonToBasicRel.priority().s());
        UniBaseUtils.createPage(model.getDataSource(), builder.<StudentReasonToBasicRel>getResultList(getSession()));
    }

    @Override
    public void updatePriority(Model model, Long reasonId, boolean up)
    {
        StudentReasonToBasicRel updatingRel = getNotNull(reasonId);

        List<StudentReasonToBasicRel> basicsList = new DQLSelectBuilder()
                .fromEntity(StudentReasonToBasicRel.class, "rel").column("rel")
                .where(eq(property(StudentReasonToBasicRel.first().id().fromAlias("rel")), value(model.getFirstId())))
                .order(property(StudentReasonToBasicRel.priority().fromAlias("rel")))
                .createStatement(getSession()).list();

        int updateingRelIndex = basicsList.indexOf(updatingRel);
        if ((up && updateingRelIndex == 0) || (!up && updateingRelIndex == basicsList.size() - 1)) return;

        Collections.swap(basicsList, updateingRelIndex, (up ? (updateingRelIndex - 1) : (updateingRelIndex + 1)));

        int priority = 1;
        for (StudentReasonToBasicRel rel : basicsList)
        {
            if (rel.getPriority() != priority)
            {
                rel.setPriority(priority);
                update(rel);
            }

            priority++;
        }
    }

    @Override
    public void doRefreshPriorities(Model model)
    {
        List<StudentReasonToBasicRel> basicsList = new DQLSelectBuilder()
                .fromEntity(StudentReasonToBasicRel.class, "rel").column("rel")
                .where(eq(property(StudentReasonToBasicRel.first().id().fromAlias("rel")), value(model.getFirstId())))
                .order(property(StudentReasonToBasicRel.priority().fromAlias("rel")))
                .createStatement(getSession()).list();

        int priority = 1;
        for (StudentReasonToBasicRel rel : basicsList)
        {
            if (rel.getPriority() != priority)
            {
                rel.setPriority(priority);
                update(rel);
            }

            priority++;
        }
    }
}