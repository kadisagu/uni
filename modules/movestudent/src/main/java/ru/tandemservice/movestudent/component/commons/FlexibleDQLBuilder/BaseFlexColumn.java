/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Nikolay Fedorovskih
 * @since 27.11.2013
 */
public abstract class BaseFlexColumn implements IFlexColumn
{
    private String _name;
    private String _propertyPath;
    private String _alias;
    private Set<String> _viewNames;
    private boolean _canBeHidden;

    @Override
    public Set<String> getViewNames()
    {
        return _viewNames;
    }

    public BaseFlexColumn(String name, String propertyPath, String alias, boolean canBeHidden)
    {
        _name = name;
        _propertyPath = propertyPath;
        _alias = alias;
        _canBeHidden = canBeHidden;
        if (canBeHidden)
        {
            addViewNames(name);
        }
    }

    @Override
    public String getAlias()
    {
        return _alias;
    }

    @Override
    public String getName()
    {
        return _name;
    }

    @Override
    public String getPath()
    {
        return _propertyPath;
    }

    public IFlexColumn setViewNames(String... viewNames)
    {
        if (_viewNames == null)
            _viewNames = new HashSet<>(viewNames.length);
        else
            _viewNames.clear();
        Collections.addAll(_viewNames, viewNames);
        return this;
    }

    @Override
    public IFlexColumn addViewNames(String... viewNames)
    {
        if (_viewNames == null)
            _viewNames = new HashSet<>(viewNames.length);
        Collections.addAll(_viewNames, viewNames);
        return this;
    }

    @Override
    public void addToBuilder(DQLSelectBuilder builder, String alias)
    {
        builder.column(DQLExpressions.property(_propertyPath != null ? (alias + "." + _propertyPath) : alias), _name);
    }

    public void deploy(DataWrapper wrapper, Object value)
    {
        wrapper.setProperty(getName(), value);
    }

    @Override
    public boolean isRoot()
    {
        return false;
    }

    @Override
    public boolean canBeHidden()
    {
        return _canBeHidden && _viewNames != null && !_viewNames.isEmpty();
    }

    public IFlexColumn setCanBeHidden(boolean canBeHidden)
    {
        _canBeHidden = canBeHidden;
        return this;
    }
}