/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e38.AddEdit;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.WeekendChildOutStuExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.IExtEducationLevelModel;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 15.03.2010
 */
public class Model extends CommonModularStudentExtractAddEditModel<WeekendChildOutStuExtract> implements IGroupModel, IEducationLevelModel
{
    private CommonExtractModel _eduModel;

    private String _studentStatusNewStr;

    public CommonExtractModel getEduModel()
    {
        return _eduModel;
    }

    public void setEduModel(CommonExtractModel eduModel)
    {
        _eduModel = eduModel;
    }

    public String getStudentStatusNewStr()
    {
        return _studentStatusNewStr;
    }

    public void setStudentStatusNewStr(String studentStatusNewStr)
    {
        _studentStatusNewStr = studentStatusNewStr;
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return _eduModel.getFormativeOrgUnit();
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return _eduModel.getTerritorialOrgUnit();
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _eduModel.getEducationLevelsHighSchool();
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return _eduModel.getDevelopForm();
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return _eduModel.getDevelopCondition();
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return _eduModel.getDevelopTech();
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return _eduModel.getDevelopPeriod();
    }

    @Override
    public Course getCourse()
    {
        return _eduModel.getCourse();
    }
}