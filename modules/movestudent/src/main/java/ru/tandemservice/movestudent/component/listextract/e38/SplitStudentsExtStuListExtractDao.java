/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e38;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.SplitStudentsExtStuListExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 12.12.2012
 */
public class SplitStudentsExtStuListExtractDao extends UniBaseDao implements IExtractComponentDao<SplitStudentsExtStuListExtract>
{
    @Override
    public void doCommit(SplitStudentsExtStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        extract.setEducationOrgUnitOld(extract.getEntity().getEducationOrgUnit());
        extract.getEntity().setEducationOrgUnit(extract.getEducationOrgUnitNew());

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            extract.setPrevOrderDate(orderData.getSplitToSpecializationOrderDate());
            extract.setPrevOrderNumber(orderData.getSplitToSpecializationOrderNumber());
        }
        orderData.setSplitToSpecializationOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setSplitToSpecializationOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);
    }

    @Override
    public void doRollback(SplitStudentsExtStuListExtract extract, Map parameters)
    {
        extract.getEntity().setEducationOrgUnit(extract.getEducationOrgUnitOld());

        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setSplitToSpecializationOrderDate(extract.getPrevOrderDate());
        orderData.setSplitToSpecializationOrderNumber(extract.getPrevOrderNumber());
        getSession().saveOrUpdate(orderData);
    }
}