package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.entity.gen.*;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. Об аттестации практик студентов, прошедших стажировку
 */
public class AttestationPracticWithProbationStuExtract extends AttestationPracticWithProbationStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getPracticeBeginDate();
    }

    @Override
    public Date getEndDate()
    {
        return getPracticeEndDate();
    }
}