/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import java.util.Set;

/**
 * @author Nikolay Fedorovskih
 * @since 27.11.2013
 */
public interface IFlexColumn
{
    String getName();

    String getAlias();

    String getPath();

    Set<String> getViewNames();

    IFlexColumn addViewNames(String... viewNames);

    void addToBuilder(DQLSelectBuilder builder, String alias);

    void deploy(DataWrapper wrapper, Object value);

    /**
     * Является ли колонка "рутовой". Рутовая колонка - это особая колонка, которая обычно содержит какой-то подзапрос.
     * Из-за того, что MSSQL до 2011 версии не поддерживает OFFSET, для
     * постраничного вывода приходится ухищряться с функцией ROW_NUMBER().
     *
     * Для примера. Есть запрос на получение списка приказов. Нам хочется в отдельной колонке получить количество параграфов в приказе.
     * Запрос будет такой:
     * select o.id, (select count(*) from abstractStudentParagraph_t p where p.id = o.id) from abstractStudentOrder_t o order by o.id
     *
     * Для вывода, например, второй страницы серчлиста, на PostgreSQL запрос будет таким:
     * select o.id, (select count(*) from abstractStudentParagraph_t p where p.id = o.id) from abstractStudentOrder_t o order by o.id
     * offset 10 limit 10
     * Но для MSSQL до версии 2011 нужно городить такое:
     * select o.id, o.cnt from
     * (select o.id, (select count(*) from abstractStudentParagraph_t p where p.id = o.id) cnt, ROW_NUMBER() OVER(order by o.id) as rn from abstractStudentOrder_t o) o
     * where rn between 11 and 20 order by o.id
     *
     * Понятно, что вычислять подзапрос на количество параграфов имеет смысл только для результирующего набора (10 элементов) вместо
     * вообще всех элементов, если не предполагается сортировать по нему. Для этого запрос надо привести к виду
     * select o.id, (select count(*) from abstractStudentParagraph_t p where p.id = o.id) from
     * (select o.id, ROW_NUMBER() OVER(order by o.id) as rn from abstractStudentOrder_t o) o
     * where rn between 11 and 20 order by o.id
     *
     * т.е. как бы вынести подзапрос в "рутовый" select, тогда он будет выполняться только 10 раз, а не 10000, к примеру.
     *
     * ВНИМАНИЕ! СОРТИРОВАТЬ ПО "РУТОВОЙ" КОЛОНКЕ НЕЛЬЗЯ!!!
     *
     * @return выносить ли колонку в "рутовый" select
     */
    boolean isRoot();

    boolean isEntityColumn();

    boolean canBeHidden();
}