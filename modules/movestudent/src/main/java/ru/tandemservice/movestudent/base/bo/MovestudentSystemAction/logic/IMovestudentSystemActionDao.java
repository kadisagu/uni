/**
 *$Id$
 */
package ru.tandemservice.movestudent.base.bo.MovestudentSystemAction.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 13.02.13
 */
public interface IMovestudentSystemActionDao extends INeedPersistenceSupport
{
    void doCorrectNonCommitedOrdersWithCommitedExtracts();

    void deleteEmptyOrders();

    ScriptResultData generateUnloadingStudentsData(OrgUnit formativeOrgUnit, boolean isAllStudent, boolean onlyFullData, List<StudentCustomStateCI> studentCustomStates);

    int updateCounter();
}