/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.RtfParagraph;
import org.tandemframework.rtf.document.text.field.RtfField;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.component.customorder.CustomOrderUtils;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.movestudent.entity.custom.ModifyModularStuExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.IAbstractParagraph;

import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * @author vip_delete
 * @since 11.01.2009
 */
public class StudentModularOrderPrint implements IPrintFormCreator<StudentModularOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentModularOrder order)
    {
        final RtfDocument document;
        final List<ModularStudentExtract> extractList = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, false);
        final boolean individual = extractList.size() == 1 && MoveStudentDaoFacade.getMoveStudentDao().isModularOrderIndividual(extractList.get(0).getType());

        if (individual && extractList.get(0) instanceof ModifyModularStuExtract) {
            ModifyModularStuExtract extract = (ModifyModularStuExtract) extractList.get(0);
            if (extract.getUserPrintForm() == null || extract.getUserPrintForm().getContent() == null)
                throw new ApplicationException(order.getTitle() + ". " + CustomOrderUtils.NEED_UPLOAD_PRINT_DOC);

            document = new RtfReader().read(extract.getUserPrintForm().getContent());
            applyModifiers(order, document, true, extractList);
        }
        else
        {
            document = new RtfReader().read(template);
            applyModifiers(order, document, individual, extractList);
            injectExtracts(document, order, extractList);
        }

        return document;
    }

    private void applyModifiers(StudentModularOrder order, RtfDocument document, boolean individual, List<ModularStudentExtract> extractList)
    {
        CommonExtractPrint.createModularOrderInjectModifier(order, individual, extractList).modify(document);
        CommonExtractPrint.createModularOrderTableModifier(order, individual, extractList).modify(document);
    }

    protected void injectExtracts(final RtfDocument document, StudentModularOrder order, List<ModularStudentExtract> extractList)
    {
        // 1. ищем ключевое слово EXTRACTS
        List<IRtfElement> elementList = null;
        int index = 0;
        boolean found = false;
        for (IRtfElement element : document.getElementList())
        {
            if (element instanceof RtfParagraph)
            {
                index = 0;
                elementList = ((RtfParagraph) element).getElementList();
                while (index < elementList.size() && !(elementList.get(index) instanceof RtfField && "EXTRACTS".equals(((RtfField) elementList.get(index)).getFieldName())))
                    index++;
                if (index < elementList.size())
                {
                    found = true;
                    break;
                }
            }
        }

        // 2. Если нашли, то вместо него вставляем все выписки
        if (found)
        {
            List<IRtfElement> parList = new ArrayList<>();

            final List<? extends IAbstractParagraph> paragraphList = order.getParagraphList();
            final Map<IAbstractParagraph, ModularStudentExtract> paragraph2extract = new HashMap<>(paragraphList.size());
            for (ModularStudentExtract extract : extractList) {
                if (paragraph2extract.put(extract.getParagraph(), extract) != null) {
                    throw new IllegalStateException("More than one modular extract in paragraph " + extract.getParagraph().getId());
                }
            }

            // порядок сортировки:
            // 1. по названию формирующего подразделения
            // 2. по курсу
            // 3. ФИО.
            // 4. id
            Collections.sort(
                    paragraphList,
                    (p1, p2) -> {
                        final Student s1 = paragraph2extract.get(p1).getEntity();
                        final Student s2 = paragraph2extract.get(p2).getEntity();

                        int i;
                        if (0 != (i = CommonCollator.TITLED_COMPARATOR.compare(s1.getEducationOrgUnit().getFormativeOrgUnit(), s2.getEducationOrgUnit().getFormativeOrgUnit()))) { return i; }
                        if (0 != (i = Course.COURSE_COMPARATOR.compare(s1.getCourse(), s2.getCourse()))) { return i; }

                        return Student.FULL_FIO_AND_ID_COMPARATOR.compare(s1, s2);
                    }
            );
            int extractNumberInText = 1;
            for (IAbstractParagraph paragraph : paragraphList)
            {
                ModularStudentExtract extract = paragraph2extract.get(paragraph);
                // создаем выписку
                byte[] extractTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(extract.getType(), MoveStudentDefines.EXTRACT_IN_ORDER_TEXT_CODE);

                String printName = EntityRuntime.getMeta(extract).getName() + "_extractPrint";
                IPrintFormCreator<ModularStudentExtract> componentPrint = CommonExtractPrint.getPrintFormCreator(printName);
                int number = paragraph.getNumber();
                paragraph.setNumber(extractNumberInText++);
                assert componentPrint != null;
                RtfDocument extractDocument = componentPrint.createPrintForm(extractTemplate, extract);
                paragraph.setNumber(number);

                // подготавливаем ее для вставки в приказ
                RtfUtil.modifySourceList(document.getHeader(), extractDocument.getHeader(), extractDocument.getElementList());

                // дальше добавляем выписку
                parList.addAll(extractDocument.getElementList());

                // в конец добавляем перевод строки
                parList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                if (paragraphList.indexOf(paragraph) + 1 == paragraphList.size())
                {
                    parList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.KEEPN));
                    parList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.NOWIDCTLPAR));
                }
	            addNewline(parList);
            }

            // полученный список вставляем вместо ключевого слова
            elementList.remove(index);
            elementList.addAll(index, parList);
        }
    }

	//
	protected void addNewline(@NotNull List<IRtfElement> elements){
		elements.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
	}
}
