/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.list.StudentListOrderAdd;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;

/**
 * @author vip_delete
 * @since 07.05.2009
 */
@Input({
        @Bind(key = "orgUnitId", binding = "orgUnitId"),
        @Bind(key = "extractTypeGroups", binding = "extractTypeGroups")
})
public class Model
{
    private Long _orgUnitId;
    private List<StudentExtractType> _typeList;
    private StudentExtractType _type;
    private String[] _extractTypeGroups;
    private ISelectModel _orderTypeListModel;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public List<StudentExtractType> getTypeList()
    {
        return _typeList;
    }

    public void setTypeList(List<StudentExtractType> typeList)
    {
        _typeList = typeList;
    }

    public StudentExtractType getType()
    {
        return _type;
    }

    public void setType(StudentExtractType type)
    {
        _type = type;
    }

    public String[] getExtractTypeGroups()
    {
        return _extractTypeGroups;
    }

    public void setExtractTypeGroups(String[] extractTypeGroups)
    {
        _extractTypeGroups = extractTypeGroups;
    }

    public ISelectModel getOrderTypeListModel()
    {
        return _orderTypeListModel;
    }

    public void setOrderTypeListModel(ISelectModel orderTypeListModel)
    {
        _orderTypeListModel = orderTypeListModel;
    }
}