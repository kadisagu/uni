/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e46.ParagraphAddEdit;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.AdmitToPassQualificationWorkStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Date;
import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 01.06.2016
 */
public class Model extends AbstractListParagraphAddEditModel<AdmitToPassQualificationWorkStuListExtract> implements IGroupModel, IEducationLevelModel
{
    private OrgUnit _formativeOrgUnit;
    private EducationLevelsHighSchool _educationLevelsHighSchool;
    private DevelopForm _developForm;
    private Course _course;
    private List<Group> _groupList;
    private CompensationType _compensationType;
    private Date _examPeriodBegin;
    private Date _examPeriodEnd;

    private ISelectModel _specializationsListModel;
    private ISelectModel _developFormModel;
    private ISelectModel _groupNewListModel;
    private List<CompensationType> _compensationTypeList;

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return null;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return null;
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return null;
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return null;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public List<Group> getGroupList()
    {
        return _groupList;
    }

    public void setGroupList(List<Group> groupList)
    {
        _groupList = groupList;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public Date getExamPeriodBegin()
    {
        return _examPeriodBegin;
    }

    public void setExamPeriodBegin(Date examPeriodBegin)
    {
        _examPeriodBegin = examPeriodBegin;
    }

    public Date getExamPeriodEnd()
    {
        return _examPeriodEnd;
    }

    public void setExamPeriodEnd(Date examPeriodEnd)
    {
        _examPeriodEnd = examPeriodEnd;
    }

    public ISelectModel getSpecializationsListModel()
    {
        return _specializationsListModel;
    }

    public void setSpecializationsListModel(ISelectModel specializationsListModel)
    {
        _specializationsListModel = specializationsListModel;
    }

    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public ISelectModel getGroupNewListModel()
    {
        return _groupNewListModel;
    }

    public void setGroupNewListModel(ISelectModel groupNewListModel)
    {
        _groupNewListModel = groupNewListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }
}
