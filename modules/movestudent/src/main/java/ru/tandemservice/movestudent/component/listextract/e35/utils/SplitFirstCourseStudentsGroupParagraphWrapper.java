/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e35.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 14.10.2013
 */
public class SplitFirstCourseStudentsGroupParagraphWrapper implements Comparable<SplitFirstCourseStudentsGroupParagraphWrapper>
{
    private final StudentCategory _studentCategory;
    private final CompensationType _compensationType;
    private final DevelopForm _developForm;
    private final DevelopCondition _developCondition;
    private final DevelopTech _developTech;
    private final DevelopPeriod _developPeriod;

    private final ListStudentExtract _firstExtract;

    private final List<SplitFirstCourseStudentsGroupParagraphPartWrapper> _paragraphPartWrapperList = new ArrayList<>();

    public SplitFirstCourseStudentsGroupParagraphWrapper(StudentCategory studentCategory, CompensationType compensationType, DevelopForm developForm, DevelopCondition developCondition, DevelopTech developTech, DevelopPeriod developPeriod, ListStudentExtract firstExtract)
    {
        _studentCategory = studentCategory;
        _compensationType = compensationType;
        _developForm = developForm;
        _developCondition = developCondition;
        _developTech = developTech;
        _developPeriod = developPeriod;
        _firstExtract = firstExtract;
    }

    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    public List<SplitFirstCourseStudentsGroupParagraphPartWrapper> getParagraphPartWrapperList()
    {
        return _paragraphPartWrapperList;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof SplitFirstCourseStudentsGroupParagraphWrapper))
            return false;

        SplitFirstCourseStudentsGroupParagraphWrapper that = (SplitFirstCourseStudentsGroupParagraphWrapper) o;

        return _studentCategory.equals(that.getStudentCategory())
                && _compensationType.equals(that.getCompensationType())
                && _developForm.equals(that.getDevelopForm())
                && _developCondition.equals(that.getDevelopCondition())
                && _developTech.equals(that.getDevelopTech())
                && _developPeriod.equals(that.getDevelopPeriod());
    }

    @Override
    public int compareTo(SplitFirstCourseStudentsGroupParagraphWrapper o)
    {
        if (StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(_studentCategory.getCode()) && StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(o.getStudentCategory().getCode()))
            return -1;
        else if (StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(_studentCategory.getCode()) && StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(o.getStudentCategory().getCode()))
            return 1;

        if(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(_compensationType.getCode()) && CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(o.getCompensationType().getCode()))
            return -1;
        else if(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(_compensationType.getCode()) && CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(o.getCompensationType().getCode()))
            return 1;

        return 0;
    }
}
