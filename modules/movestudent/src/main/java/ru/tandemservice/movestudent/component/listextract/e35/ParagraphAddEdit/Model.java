/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e35.ParagraphAddEdit;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author ListExtractComponentGenerator
 * @since 22.10.2012
 */
public class Model extends AbstractListParagraphAddEditModel<SplitFirstCourseStudentsGroupStuListExtract> implements IEducationLevelModel, IGroupModel
{
    private Course _course;
    private Group _newGroup;
    private EducationLevelsHighSchool _educationLevelsHighSchool;
    private DevelopForm _developForm;
    private DevelopCondition _developCondition;
    private DevelopTech _developTech;
    private DevelopPeriod _developPeriod;

    private ISelectModel _newGroupListModel;
    private ISelectModel _educationLevelsHighSchoolModel;
    private ISelectModel _developFormModel;
    private ISelectModel _developConditionModel;
    private ISelectModel _developTechModel;
    private ISelectModel _developPeriodModel;

    private boolean _oneParagraph;


    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getNewGroup()
    {
        return _newGroup;
    }

    public void setNewGroup(Group newGroup)
    {
        _newGroup = newGroup;
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public void setDevelopCondition(DevelopCondition developCondition)
    {
        _developCondition = developCondition;
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public void setDevelopTech(DevelopTech developTech)
    {
        _developTech = developTech;
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        _developPeriod = developPeriod;
    }

    public ISelectModel getNewGroupListModel()
    {
        return _newGroupListModel;
    }

    public void setNewGroupListModel(ISelectModel newGroupListModel)
    {
        _newGroupListModel = newGroupListModel;
    }

    public ISelectModel getEducationLevelsHighSchoolModel()
    {
        return _educationLevelsHighSchoolModel;
    }

    public void setEducationLevelsHighSchoolModel(ISelectModel educationLevelsHighSchoolModel)
    {
        _educationLevelsHighSchoolModel = educationLevelsHighSchoolModel;
    }

    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public ISelectModel getDevelopConditionModel()
    {
        return _developConditionModel;
    }

    public void setDevelopConditionModel(ISelectModel developConditionModel)
    {
        _developConditionModel = developConditionModel;
    }

    public ISelectModel getDevelopTechModel()
    {
        return _developTechModel;
    }

    public void setDevelopTechModel(ISelectModel developTechModel)
    {
        _developTechModel = developTechModel;
    }

    public ISelectModel getDevelopPeriodModel()
    {
        return _developPeriodModel;
    }

    public void setDevelopPeriodModel(ISelectModel developPeriodModel)
    {
        _developPeriodModel = developPeriodModel;
    }

    public boolean isOneParagraph()
    {
        return _oneParagraph;
    }

    public void setOneParagraph(boolean oneParagraph)
    {
        _oneParagraph = oneParagraph;
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return null;
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return null;
    }
}
