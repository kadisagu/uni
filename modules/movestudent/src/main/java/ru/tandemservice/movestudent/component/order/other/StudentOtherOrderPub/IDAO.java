/* $Id$ */
package ru.tandemservice.movestudent.component.order.other.StudentOtherOrderPub;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Dmitry Seleznev
 * @since 15.04.2013
 */
public interface IDAO extends IUniDao<Model>
{
    void deleteAttachedPrintForm(Model model);
}