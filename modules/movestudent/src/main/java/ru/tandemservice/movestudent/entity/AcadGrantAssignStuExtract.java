package ru.tandemservice.movestudent.entity;

import java.util.Date;

import ru.tandemservice.movestudent.entity.gen.AcadGrantAssignStuExtractGen;

/**
 * Выписка из сборного приказа по студенту. О назначении академической стипендии
 */
public class AcadGrantAssignStuExtract extends AcadGrantAssignStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getStartDate();
    }
}