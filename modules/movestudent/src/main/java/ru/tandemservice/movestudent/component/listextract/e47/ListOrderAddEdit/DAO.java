/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e47.ListOrderAddEdit;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudent.component.listextract.e1.ListOrderAddEdit.Model;

import java.util.Date;

/**
 * @author Ekaterina Zvereva
 * @since 01.02.2017
 */
public class DAO extends ru.tandemservice.movestudent.component.listextract.e1.ListOrderAddEdit.DAO
{
    public static final String TEXT_PARAGRAPH = "На основании приказа Министерства образования России №1 от " +
            DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()) +" «Об утверждении федерального государственного стандарта высшего образования" +
            " по направлению подготовки 01.01.01 Название направления (программа бакалавриата)».";


    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        //Если форма добавления
        if (model.getOrderId() == null)
            model.getOrder().setTextParagraph(TEXT_PARAGRAPH);
    }
}