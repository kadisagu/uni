/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e3.ParagraphAddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.CourseNoChangeStuListExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author ListExtractComponentGenerator
 * @since 26.05.2009
 */
public class DAO extends AbstractListParagraphAddEditDAO<CourseNoChangeStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));

        model.setCourseOldListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getGroup() == null) return ListResult.getEmpty();

                MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "s", new String[]{Student.L_COURSE});
                builder.add(MQExpression.eq("s", Student.L_GROUP, model.getGroup()));
                builder.addOrder("s", Student.L_COURSE + "." + Course.P_CODE);
                builder.setNeedDistinct(true);
                return new ListResult<>(builder.<Course>getResultList(getSession()));
            }
        });
        // заполняем поля по умолчанию
        if (model.getParagraphId() != null)
        {
            CourseNoChangeStuListExtract extract = model.getFirstExtract();
            model.setCourse(extract.getCourse());
            model.setGroup(extract.getGroup());
            model.setCourseOld(extract.getCourseOld());
            model.setExtractReason(extract.getExtractReason());
        }
    }

    @Override
    protected CourseNoChangeStuListExtract createNewInstance(Model model)
    {
        return new CourseNoChangeStuListExtract();
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COURSE, model.getCourseOld()));
    }

    @Override
    protected void fillExtract(CourseNoChangeStuListExtract extract, Student student, Model model)
    {
        extract.setExtractReason(model.getExtractReason());
        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setCourseOld(model.getCourseOld());
    }
}