/* $Id: Model.java.vm 11769 2010-03-05 07:11:49Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e98.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.AttestationPracticStuExtract;
import ru.tandemservice.uni.entity.catalog.Course;

import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 08.02.2013
 */
public class Model extends CommonModularStudentExtractAddEditModel<AttestationPracticStuExtract>
{
    private String practiceType;
    private String practiceKind;
    private List<String> _practiceTypeList;
    private ISelectModel practiceKindModel;
    private List<Course> _courseList;

    public List<String> getPracticeTypeList()
    {
        return _practiceTypeList;
    }

    public void setPracticeTypeList(List<String> practiceTypeList)
    {
        _practiceTypeList = practiceTypeList;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public String getPracticeType()
    {
        return practiceType;
    }

    public void setPracticeType(String practiceType)
    {
        this.practiceType = practiceType;
    }

    public String getPracticeKind()
    {
        return practiceKind;
    }

    public void setPracticeKind(String practiceKind)
    {
        this.practiceKind = practiceKind;
    }

    public ISelectModel getPracticeKindModel()
    {
        return practiceKindModel;
    }

    public void setPracticeKindModel(ISelectModel practiceKindModel)
    {
        this.practiceKindModel = practiceKindModel;
    }
}
