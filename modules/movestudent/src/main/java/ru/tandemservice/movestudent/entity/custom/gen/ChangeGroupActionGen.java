package ru.tandemservice.movestudent.entity.custom.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.custom.ChangeGroupAction;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unimove.entity.ICustomOrderAction;
import ru.tandemservice.unimove.entity.ICustomOrderExtract;
import ru.tandemservice.unimove.entity.gen.ICustomOrderExtractGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Смена группы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChangeGroupActionGen extends EntityBase
 implements ICustomOrderAction{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.custom.ChangeGroupAction";
    public static final String ENTITY_NAME = "changeGroupAction";
    public static final int VERSION_HASH = -1432792102;
    private static IEntityMeta ENTITY_META;

    public static final String L_OLD_GROUP = "oldGroup";
    public static final String L_NEW_GROUP = "newGroup";
    public static final String L_CUSTOM_EXTRACT = "customExtract";

    private Group _oldGroup;     // Старая группа
    private Group _newGroup;     // Новая группа
    private ICustomOrderExtract _customExtract;     // Выписка из кастомного приказа, с набором действий, определенных пользователем

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Старая группа.
     */
    public Group getOldGroup()
    {
        return _oldGroup;
    }

    /**
     * @param oldGroup Старая группа.
     */
    public void setOldGroup(Group oldGroup)
    {
        dirty(_oldGroup, oldGroup);
        _oldGroup = oldGroup;
    }

    /**
     * @return Новая группа.
     */
    public Group getNewGroup()
    {
        return _newGroup;
    }

    /**
     * @param newGroup Новая группа.
     */
    public void setNewGroup(Group newGroup)
    {
        dirty(_newGroup, newGroup);
        _newGroup = newGroup;
    }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public ICustomOrderExtract getCustomExtract()
    {
        return _customExtract;
    }

    /**
     * @param customExtract Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     */
    public void setCustomExtract(ICustomOrderExtract customExtract)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && customExtract!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(ICustomOrderExtract.class);
            IEntityMeta actual =  customExtract instanceof IEntity ? EntityRuntime.getMeta((IEntity) customExtract) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_customExtract, customExtract);
        _customExtract = customExtract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ChangeGroupActionGen)
        {
            setOldGroup(((ChangeGroupAction)another).getOldGroup());
            setNewGroup(((ChangeGroupAction)another).getNewGroup());
            setCustomExtract(((ChangeGroupAction)another).getCustomExtract());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChangeGroupActionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChangeGroupAction.class;
        }

        public T newInstance()
        {
            return (T) new ChangeGroupAction();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "oldGroup":
                    return obj.getOldGroup();
                case "newGroup":
                    return obj.getNewGroup();
                case "customExtract":
                    return obj.getCustomExtract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "oldGroup":
                    obj.setOldGroup((Group) value);
                    return;
                case "newGroup":
                    obj.setNewGroup((Group) value);
                    return;
                case "customExtract":
                    obj.setCustomExtract((ICustomOrderExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "oldGroup":
                        return true;
                case "newGroup":
                        return true;
                case "customExtract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "oldGroup":
                    return true;
                case "newGroup":
                    return true;
                case "customExtract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "oldGroup":
                    return Group.class;
                case "newGroup":
                    return Group.class;
                case "customExtract":
                    return ICustomOrderExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChangeGroupAction> _dslPath = new Path<ChangeGroupAction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChangeGroupAction");
    }
            

    /**
     * @return Старая группа.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeGroupAction#getOldGroup()
     */
    public static Group.Path<Group> oldGroup()
    {
        return _dslPath.oldGroup();
    }

    /**
     * @return Новая группа.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeGroupAction#getNewGroup()
     */
    public static Group.Path<Group> newGroup()
    {
        return _dslPath.newGroup();
    }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeGroupAction#getCustomExtract()
     */
    public static ICustomOrderExtractGen.Path<ICustomOrderExtract> customExtract()
    {
        return _dslPath.customExtract();
    }

    public static class Path<E extends ChangeGroupAction> extends EntityPath<E>
    {
        private Group.Path<Group> _oldGroup;
        private Group.Path<Group> _newGroup;
        private ICustomOrderExtractGen.Path<ICustomOrderExtract> _customExtract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Старая группа.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeGroupAction#getOldGroup()
     */
        public Group.Path<Group> oldGroup()
        {
            if(_oldGroup == null )
                _oldGroup = new Group.Path<Group>(L_OLD_GROUP, this);
            return _oldGroup;
        }

    /**
     * @return Новая группа.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeGroupAction#getNewGroup()
     */
        public Group.Path<Group> newGroup()
        {
            if(_newGroup == null )
                _newGroup = new Group.Path<Group>(L_NEW_GROUP, this);
            return _newGroup;
        }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeGroupAction#getCustomExtract()
     */
        public ICustomOrderExtractGen.Path<ICustomOrderExtract> customExtract()
        {
            if(_customExtract == null )
                _customExtract = new ICustomOrderExtractGen.Path<ICustomOrderExtract>(L_CUSTOM_EXTRACT, this);
            return _customExtract;
        }

        public Class getEntityClass()
        {
            return ChangeGroupAction.class;
        }

        public String getEntityName()
        {
            return "changeGroupAction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
