/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e35.utils;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 15.10.2013
 */
public class SplitFirstCourseStudentsGroupParagraphPartWrapper implements Comparable<SplitFirstCourseStudentsGroupParagraphPartWrapper>
{
    private final EducationLevelsHighSchool _educationLevelsHighSchool;
    private final Group _newGroup;
    private final List<Person> _personList = new ArrayList<>();
    private final ListStudentExtract _firstExtract;

    public SplitFirstCourseStudentsGroupParagraphPartWrapper(EducationLevelsHighSchool educationLevelsHighSchool, Group newGroup, ListStudentExtract firstExtract)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
        _newGroup = newGroup;
        _firstExtract = firstExtract;
    }

    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public Group getNewGroup()
    {
        return _newGroup;
    }

    public List<Person> getPersonList()
    {
        return _personList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof SplitFirstCourseStudentsGroupParagraphPartWrapper))
            return false;

        SplitFirstCourseStudentsGroupParagraphPartWrapper that = (SplitFirstCourseStudentsGroupParagraphPartWrapper) o;

        return _educationLevelsHighSchool.equals(that.getEducationLevelsHighSchool())
                && _newGroup.equals(that.getNewGroup());
    }

    @Override
    public int compareTo(SplitFirstCourseStudentsGroupParagraphPartWrapper o)
    {
        int result = _educationLevelsHighSchool.getTitle().compareTo(o.getEducationLevelsHighSchool().getTitle());
        if (result == 0) result = _newGroup.getTitle().compareTo(o.getNewGroup().getTitle());
        return result;
    }
}
