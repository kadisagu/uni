package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.RecordBookLossPenaltyStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О наложении взыскания за утерю зачетной книжки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RecordBookLossPenaltyStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.RecordBookLossPenaltyStuExtract";
    public static final String ENTITY_NAME = "recordBookLossPenaltyStuExtract";
    public static final int VERSION_HASH = -1844598602;
    private static IEntityMeta ENTITY_META;

    public static final String P_WITHOUT_REPRIMAND = "withoutReprimand";

    private boolean _withoutReprimand = false;     // Без объявления выговора

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Без объявления выговора. Свойство не может быть null.
     */
    @NotNull
    public boolean isWithoutReprimand()
    {
        return _withoutReprimand;
    }

    /**
     * @param withoutReprimand Без объявления выговора. Свойство не может быть null.
     */
    public void setWithoutReprimand(boolean withoutReprimand)
    {
        dirty(_withoutReprimand, withoutReprimand);
        _withoutReprimand = withoutReprimand;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RecordBookLossPenaltyStuExtractGen)
        {
            setWithoutReprimand(((RecordBookLossPenaltyStuExtract)another).isWithoutReprimand());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RecordBookLossPenaltyStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RecordBookLossPenaltyStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new RecordBookLossPenaltyStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "withoutReprimand":
                    return obj.isWithoutReprimand();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "withoutReprimand":
                    obj.setWithoutReprimand((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "withoutReprimand":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "withoutReprimand":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "withoutReprimand":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RecordBookLossPenaltyStuExtract> _dslPath = new Path<RecordBookLossPenaltyStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RecordBookLossPenaltyStuExtract");
    }
            

    /**
     * @return Без объявления выговора. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RecordBookLossPenaltyStuExtract#isWithoutReprimand()
     */
    public static PropertyPath<Boolean> withoutReprimand()
    {
        return _dslPath.withoutReprimand();
    }

    public static class Path<E extends RecordBookLossPenaltyStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Boolean> _withoutReprimand;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Без объявления выговора. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RecordBookLossPenaltyStuExtract#isWithoutReprimand()
     */
        public PropertyPath<Boolean> withoutReprimand()
        {
            if(_withoutReprimand == null )
                _withoutReprimand = new PropertyPath<Boolean>(RecordBookLossPenaltyStuExtractGen.P_WITHOUT_REPRIMAND, this);
            return _withoutReprimand;
        }

        public Class getEntityClass()
        {
            return RecordBookLossPenaltyStuExtract.class;
        }

        public String getEntityName()
        {
            return "recordBookLossPenaltyStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
