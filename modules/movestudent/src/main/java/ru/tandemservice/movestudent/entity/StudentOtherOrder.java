package ru.tandemservice.movestudent.entity;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudent.entity.gen.*;

/**
 * Прочие приказы по студентам
 */
public class StudentOtherOrder extends StudentOtherOrderGen
{
    @Override
    public String getTitle()
    {
        return "Приказ" + (getNumber() == null ? "" : " №" + getNumber()) + (getCommitDate() == null ? "" : " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCommitDate()));
    }

    @Override
    public ICatalogItem getType()
    {
        return null;
    }
}