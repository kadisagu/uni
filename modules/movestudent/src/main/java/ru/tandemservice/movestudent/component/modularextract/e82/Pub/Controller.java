/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e82.Pub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubController;
import ru.tandemservice.movestudent.entity.RePassDiscStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToRePassDiscipline;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 31.08.2012
 */
public class Controller extends ModularStudentExtractPubController<RePassDiscStuExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        prepareListDataSource(component);

        getModel(component).setAdditionalDataPage(getClass().getPackage().getName() + ".DisciplinesList");
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<StuExtractToRePassDiscipline> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Наименование дисциплины", StuExtractToRePassDiscipline.discipline().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма промежуточного контроля", StuExtractToRePassDiscipline.controlAction().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Семестр чтения", StuExtractToRePassDiscipline.srcTerm().title().s()).setClickable(false).setOrderable(false));
        model.setDataSource(dataSource);
    }
}