/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e59.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract;
import ru.tandemservice.uni.UniDefines;

import static ru.tandemservice.movestudent.component.modularextract.e12.AddEdit.DAO.getBenefitText;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 23.08.2012
 */
public class Controller extends CommonModularStudentExtractAddEditController<WeekendChildOneAndHalfStuExtract, IDAO, Model>
{
    public void onChangeDate(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getExtract().setPayBenefitText(getBenefitText(model.getExtract().getPayBenefitBeginDate(), model.getExtract().getPayBenefitEndDate()));
    }

    public void onChangeStudentStatus(IBusinessComponent component)
    {
        Model model = getModel(component);
        if(UniDefines.CATALOG_STUDENT_STATUS_CHILD_WITHOUT_ATTENDANCE.equals(model.getExtract().getStudentStatusNew().getCode()))
        {
            model.getExtract().setFreeAttendance(false);
        }
    }
}
