package ru.tandemservice.movestudent.entity;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.gen.StudentListOrderGen;

/**
 * Списочный приказ по студентам
 */
public class StudentListOrder extends StudentListOrderGen
{
    public static final String P_REASON_FULL_TITLE = "reasonFullTitle";

    public String getEditableTitle()
    {
        return "«" + getType().getTitle() + "»" + (getNumber() == null ? "" : " №" + getNumber()) + (getCommitDate() == null ? "" : " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCommitDate()));
    }
    
    @Override
    public String getTitle()
    {
        if (getType() == null) {
            return this.getClass().getSimpleName();
        }
        return "Списочный приказ «" + getType().getTitle() + "»" + (getNumber() == null ? "" : " №" + getNumber()) + (getCommitDate() == null ? "" : " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCommitDate()));
    }
    
    public String getBasicListStr()
    {
        List<StuListOrderToBasicRelation> list = MoveStudentDaoFacade.getMoveStudentDao().getPrioritySortedListOrderBasicsList(this);
        String[] title = new String[list.size()];
        for (int i = 0; i < list.size(); i++)
            title[i] = list.get(i).getBasic().getTitle() + (StringUtils.isEmpty(list.get(i).getComment()) ? "" : " " + list.get(i).getComment());
        return StringUtils.join(title, ", ");
    }

    public String getReasonFullTitle()
    {
        if(null == getReason()) return null;
        return getReason().getTitle() + (null != getReasonComment() ? (" " + getReasonComment()) : "");
    }
}