/* $Id: Model.java.vm 11769 2010-03-05 07:11:49Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e41.AddEdit;

import java.util.List;

import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.PrescheduleSessionPassStuExtract;
import ru.tandemservice.uni.entity.catalog.SessionType;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 08.02.2011
 */
public class Model extends CommonModularStudentExtractAddEditModel<PrescheduleSessionPassStuExtract>
{
    private SessionType _sessionType;
    private List<SessionType> _sessionTypeList;
    private ISelectModel _eduYearsList;

    public SessionType getSessionType() {
        return _sessionType;
    }

    public void setSessionType(SessionType _sessionType) {
        this._sessionType = _sessionType;
    }

    public List<SessionType> getSessionTypeList() {
        return _sessionTypeList;
    }

    public void setSessionTypeList(List<SessionType> _sessionTypeList) {
        this._sessionTypeList = _sessionTypeList;
    }

    public ISelectModel getEduYearsList(){
        return _eduYearsList;
    }

    public void setEduYearsList(ISelectModel eduYearsList){
        this._eduYearsList = eduYearsList;
    }
}
