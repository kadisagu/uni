package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITakeOffPayment;
import ru.tandemservice.movestudent.entity.gen.ReEducationFGOSStuExtractGen;

import java.util.Date;

/**
 * О предоставлении повторного года обучения с переходом на ФГОС
 */
public class ReEducationFGOSStuExtract extends ReEducationFGOSStuExtractGen implements ITakeOffPayment
{
    @Override
    public Date getPaymentTakeOffDate()
    {
        return getBeginDate();
    }
}