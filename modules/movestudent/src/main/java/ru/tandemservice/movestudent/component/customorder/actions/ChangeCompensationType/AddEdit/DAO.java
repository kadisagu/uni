/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeCompensationType.AddEdit;

import ru.tandemservice.movestudent.component.customorder.IStudentCustomOrderExtract;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeCompensationTypeAction;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class DAO extends BaseCustomActionAddEditDAO<ChangeCompensationTypeAction, Model>
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        if (model.isActionActive())
        {
            model.setNewCompensationTypeList(getCatalogItemListOrderByCode(CompensationType.class));
            if (model.getStudent() != null)
                model.getNewCompensationTypeList().remove(model.getStudent().getCompensationType());
        }
    }

    @Override
    protected Class<ChangeCompensationTypeAction> getActionClass()
    {
        return ChangeCompensationTypeAction.class;
    }

    @Override
    public void updatePrepare(Model model, ChangeCompensationTypeAction action, IStudentCustomOrderExtract extract)
    {
        Student student = (Student) extract.getEntity();
        action.setOldCompensationType(student.getCompensationType());
    }

    @Override
    public void doCommit(ChangeCompensationTypeAction action)
    {
        Student student = (Student) action.getCustomExtract().getEntity();

        action.setOldCompensationType(student.getCompensationType());
        update(action);

        student.setCompensationType(action.getNewCompensationType());
        update(student);
    }

    @Override
    public void doRollback(ChangeCompensationTypeAction action)
    {
        Student student = (Student) action.getCustomExtract().getEntity();
        student.setCompensationType(action.getOldCompensationType());
        update(student);
    }
}