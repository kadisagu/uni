/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeGroup.Pub;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.BaseCustomActionPubModel;
import ru.tandemservice.movestudent.entity.custom.ChangeGroupAction;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public class Model extends BaseCustomActionPubModel<ChangeGroupAction>
{
}