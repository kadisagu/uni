/* $Id$ */
package ru.tandemservice.movestudent.utils.system.advgenerators;

/**
 * @author Dmitry Seleznev
 * @since 10.08.2012
 */
public class EntityFieldBaseMetaDescriptor
{
    private String _fieldName; // Имя поля, в описании сущности соответствует атрибуту "name"
    private String _fieldTitle; // Человеко-читаемое имя поля, в описании сущности соответствует атрибуту "title"
    private String _unique; // Ключ уникальности, в описании сущности соответствует атрибуту "unique". Может принимать строковые значения. Если пусто, или "true", то воспринимается как "true"
    private boolean _required; // Признак обязательности заполнения, в описании сущности соответствует атрибуту "required". "false" воспринимается как отсутствие явного указание данного атрибута.

    /**
     * Конструктор
     *
     * @param fieldName  - Имя поля, в описании сущности соответствует атрибуту "name"
     * @param fieldTitle - Человеко-читаемое имя поля, в описании сущности соответствует атрибуту "title"
     */
    protected EntityFieldBaseMetaDescriptor(String fieldName, String fieldTitle)
    {
        _fieldName = fieldName;
        _fieldTitle = fieldTitle;
    }

    /**
     * Конструктор
     *
     * @param fieldName  - Имя поля, в описании сущности соответствует атрибуту "name"
     * @param fieldTitle - Человеко-читаемое имя поля, в описании сущности соответствует атрибуту "title"
     * @param required   - Признак обязательности заполнения, в описании сущности соответствует атрибуту "required". "false" воспринимается как отсутствие явного указание данного атрибута.
     */
    protected EntityFieldBaseMetaDescriptor(String fieldName, String fieldTitle, boolean required)
    {
        _fieldName = fieldName;
        _fieldTitle = fieldTitle;
        _required = required;
    }

    /**
     * Конструктор
     *
     * @param fieldName  - Имя поля, в описании сущности соответствует атрибуту "name"
     * @param fieldTitle - Человеко-читаемое имя поля, в описании сущности соответствует атрибуту "title"
     * @param required   - Признак обязательности заполнения, в описании сущности соответствует атрибуту "required". "false" воспринимается как отсутствие явного указание данного атрибута.
     * @param unique     - Ключ уникальности, в описании сущности соответствует атрибуту "unique". Может принимать строковые значения. Если пусто, или "true", то воспринимается как "true"
     */
    protected EntityFieldBaseMetaDescriptor(String fieldName, String fieldTitle, boolean required, String unique)
    {
        _fieldName = fieldName;
        _fieldTitle = fieldTitle;
        _required = required;
        _unique = unique.length() < 1 ? "true" : unique;
    }

    /**
     * Имя поля, в описании сущности соответствует атрибуту "name"
     *
     * @return - имя поля
     */
    public String getFieldName()
    {
        return _fieldName;
    }

    /**
     * Сеттер для имени поля, в описании сущности соответствует атрибуту "name"
     *
     * @param fieldName - имя поля
     */
    public void setFieldName(String fieldName)
    {
        _fieldName = fieldName;
    }

    /**
     * Человеко-читаемое имя поля, в описании сущности соответствует атрибуту "title"
     *
     * @return - человеко-читаемое имя поля
     */
    public String getFieldTitle()
    {
        return _fieldTitle;
    }

    /**
     * Сеттер для человеко-читаемого имени поля, в описании сущности соответствует атрибуту "title"
     *
     * @param fieldTitle - человеко-читаемое имя поля
     */
    public void setFieldTitle(String fieldTitle)
    {
        _fieldTitle = fieldTitle;
    }

    /**
     * Ключ уникальности, в описании сущности соответствует атрибуту "unique". Может принимать строковые значения. Если пусто, или "true", то воспринимается как "true"
     *
     * @return - ключ уникальности
     */
    public String getUnique()
    {
        return _unique;
    }

    /**
     * Сеттер для ключа уникальности, в описании сущности соответствует атрибуту "unique". Может принимать строковые значения. Если пусто, или "true", то воспринимается как "true"
     *
     * @param unique - ключ уникальности
     */
    public void setUnique(String unique)
    {
        _unique = unique;
    }

    /**
     * Признак обязательности заполнения, в описании сущности соответствует атрибуту "required". "false" воспринимается как отсутствие явного указание данного атрибута.
     *
     * @return - признак обязательности заполнения
     */
    public boolean isRequired()
    {
        return _required;
    }

    /**
     * Сеттер для признака обязательности заполнения, в описании сущности соответствует атрибуту "required". "false" воспринимается как отсутствие явного указание данного атрибута.
     *
     * @param required - признак обязательности заполнения
     */
    public void setRequired(boolean required)
    {
        _required = required;
    }
}