package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О назначении надбавки к государственной академической стипендии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AcadGrantBonusAssignStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuExtract";
    public static final String ENTITY_NAME = "acadGrantBonusAssignStuExtract";
    public static final int VERSION_HASH = -1594681955;
    private static IEntityMeta ENTITY_META;

    public static final String P_GRANT_BONUS_SIZE = "grantBonusSize";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_PREV_BEGIN_DATE = "prevBeginDate";
    public static final String P_PREV_END_DATE = "prevEndDate";

    private double _grantBonusSize;     // Размер надбавки к стипендии
    private Date _beginDate;     // Дата начала выплаты надбавки к стипендии
    private Date _endDate;     // Дата окончания выплаты надбавки к стипендии
    private Date _prevBeginDate;     // Дата начала выплаты надбавки к академической стипендии по предыдущему приказу
    private Date _prevEndDate;     // Дата окончания выплаты надбавки к академической стипендии по предыдущему приказу

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Размер надбавки к стипендии. Свойство не может быть null.
     */
    @NotNull
    public double getGrantBonusSize()
    {
        return _grantBonusSize;
    }

    /**
     * @param grantBonusSize Размер надбавки к стипендии. Свойство не может быть null.
     */
    public void setGrantBonusSize(double grantBonusSize)
    {
        dirty(_grantBonusSize, grantBonusSize);
        _grantBonusSize = grantBonusSize;
    }

    /**
     * @return Дата начала выплаты надбавки к стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала выплаты надбавки к стипендии. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания выплаты надбавки к стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания выплаты надбавки к стипендии. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Дата начала выплаты надбавки к академической стипендии по предыдущему приказу.
     */
    public Date getPrevBeginDate()
    {
        return _prevBeginDate;
    }

    /**
     * @param prevBeginDate Дата начала выплаты надбавки к академической стипендии по предыдущему приказу.
     */
    public void setPrevBeginDate(Date prevBeginDate)
    {
        dirty(_prevBeginDate, prevBeginDate);
        _prevBeginDate = prevBeginDate;
    }

    /**
     * @return Дата окончания выплаты надбавки к академической стипендии по предыдущему приказу.
     */
    public Date getPrevEndDate()
    {
        return _prevEndDate;
    }

    /**
     * @param prevEndDate Дата окончания выплаты надбавки к академической стипендии по предыдущему приказу.
     */
    public void setPrevEndDate(Date prevEndDate)
    {
        dirty(_prevEndDate, prevEndDate);
        _prevEndDate = prevEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof AcadGrantBonusAssignStuExtractGen)
        {
            setGrantBonusSize(((AcadGrantBonusAssignStuExtract)another).getGrantBonusSize());
            setBeginDate(((AcadGrantBonusAssignStuExtract)another).getBeginDate());
            setEndDate(((AcadGrantBonusAssignStuExtract)another).getEndDate());
            setPrevBeginDate(((AcadGrantBonusAssignStuExtract)another).getPrevBeginDate());
            setPrevEndDate(((AcadGrantBonusAssignStuExtract)another).getPrevEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AcadGrantBonusAssignStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AcadGrantBonusAssignStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new AcadGrantBonusAssignStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "grantBonusSize":
                    return obj.getGrantBonusSize();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "prevBeginDate":
                    return obj.getPrevBeginDate();
                case "prevEndDate":
                    return obj.getPrevEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "grantBonusSize":
                    obj.setGrantBonusSize((Double) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "prevBeginDate":
                    obj.setPrevBeginDate((Date) value);
                    return;
                case "prevEndDate":
                    obj.setPrevEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "grantBonusSize":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "prevBeginDate":
                        return true;
                case "prevEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "grantBonusSize":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "prevBeginDate":
                    return true;
                case "prevEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "grantBonusSize":
                    return Double.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "prevBeginDate":
                    return Date.class;
                case "prevEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AcadGrantBonusAssignStuExtract> _dslPath = new Path<AcadGrantBonusAssignStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AcadGrantBonusAssignStuExtract");
    }
            

    /**
     * @return Размер надбавки к стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuExtract#getGrantBonusSize()
     */
    public static PropertyPath<Double> grantBonusSize()
    {
        return _dslPath.grantBonusSize();
    }

    /**
     * @return Дата начала выплаты надбавки к стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания выплаты надбавки к стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Дата начала выплаты надбавки к академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuExtract#getPrevBeginDate()
     */
    public static PropertyPath<Date> prevBeginDate()
    {
        return _dslPath.prevBeginDate();
    }

    /**
     * @return Дата окончания выплаты надбавки к академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuExtract#getPrevEndDate()
     */
    public static PropertyPath<Date> prevEndDate()
    {
        return _dslPath.prevEndDate();
    }

    public static class Path<E extends AcadGrantBonusAssignStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Double> _grantBonusSize;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Date> _prevBeginDate;
        private PropertyPath<Date> _prevEndDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Размер надбавки к стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuExtract#getGrantBonusSize()
     */
        public PropertyPath<Double> grantBonusSize()
        {
            if(_grantBonusSize == null )
                _grantBonusSize = new PropertyPath<Double>(AcadGrantBonusAssignStuExtractGen.P_GRANT_BONUS_SIZE, this);
            return _grantBonusSize;
        }

    /**
     * @return Дата начала выплаты надбавки к стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(AcadGrantBonusAssignStuExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания выплаты надбавки к стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(AcadGrantBonusAssignStuExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Дата начала выплаты надбавки к академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuExtract#getPrevBeginDate()
     */
        public PropertyPath<Date> prevBeginDate()
        {
            if(_prevBeginDate == null )
                _prevBeginDate = new PropertyPath<Date>(AcadGrantBonusAssignStuExtractGen.P_PREV_BEGIN_DATE, this);
            return _prevBeginDate;
        }

    /**
     * @return Дата окончания выплаты надбавки к академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuExtract#getPrevEndDate()
     */
        public PropertyPath<Date> prevEndDate()
        {
            if(_prevEndDate == null )
                _prevEndDate = new PropertyPath<Date>(AcadGrantBonusAssignStuExtractGen.P_PREV_END_DATE, this);
            return _prevEndDate;
        }

        public Class getEntityClass()
        {
            return AcadGrantBonusAssignStuExtract.class;
        }

        public String getEntityName()
        {
            return "acadGrantBonusAssignStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
