/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit;

import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.movestudent.component.customorder.IStudentCustomOrderExtract;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.ICustomOrderAction;
import ru.tandemservice.unimove.entity.ICustomOrderExtract;
import ru.tandemservice.unimove.entity.gen.ICustomOrderActionGen;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public abstract class BaseCustomActionAddEditDAO<T extends ICustomOrderAction, Model extends BaseCustomActionAddEditModel<T>> extends UniDao<Model> implements IBaseCustomActionAddEditDAO<T, Model>
{
    protected abstract Class<T> getActionClass();

    @Override
    public void prepare(final Model model)
    {
        if (model.getCustomAction() == null)
        {
            if (model.getExtractId() != null)
            {
                model.setCustomAction(getAction(model.getExtractId()));
                model.setActionActive(model.getCustomAction() != null); // Если экшен есть, значит он выбран (активен)
            }

            if (model.getCustomAction() == null)
                model.setCustomAction(createActionInstance());
        }

        if (model.getExtractId() != null && model.getExtract() == null && model.isActionActive())
        {
            IStudentCustomOrderExtract extract = (IStudentCustomOrderExtract) getNotNull(ICustomOrderExtract.class, model.getExtractId());
            model.setExtract(extract);
        }

        if (model.getStudentId() != null && model.getStudent() == null)
        {
            model.setStudent(getNotNull(Student.class, model.getStudentId()));
        }

        model.setEdit(model.getCustomAction() != null && model.getCustomAction().getId() != null);
    }

    @Override
    public void updateForExtract(Model model, IStudentCustomOrderExtract extract, boolean isEdit)
    {
        if (isEdit)
        {
            if (extract instanceof ListStudentExtract)
            {
                AbstractStudentOrder order = extract.getParagraph().getOrder();
                refresh(order);
                if (!order.getState().getCode().equals(UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE))
                    throw new ApplicationException("Редактировать приказ можно только в состоянии формирования.");
            }
            else if (extract instanceof ModularStudentExtract)
            {
                if (MoveStudentDaoFacade.getMoveStudentDao().isModularOrderIndividual(extract.getType()))
                {
                    AbstractStudentOrder order = extract.getParagraph().getOrder();
                    refresh(order);
                    if (!order.getState().getCode().equals(UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE))
                        throw new ApplicationException("Редактировать приказ можно только в состоянии формирования.");
                }
                else
                {
                    refresh(extract);
                    if (!extract.getState().getCode().equals(UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE))
                        throw new ApplicationException("Редактировать выписку можно только в состоянии формирования.");
                }
            }
            else
            {
                throw new IllegalStateException();
            }
        }

        T action = getAction(extract.getId());
        if (model.isActionActive())
        {
            if (action == null)
                action = createActionInstance();

            if (model.getCustomAction() != null)
                action.update(model.getCustomAction());

            action.setCustomExtract(extract);
            updatePrepare(model, action, extract);
            saveOrUpdate(action);
        }
        else if (action != null && action.getId() != null)
        {
            delete(action);
        }
    }

    protected T getAction(Long extractId)
    {
        return get(getActionClass(), ICustomOrderActionGen.customExtract().id(), extractId);
    }

    protected T createActionInstance()
    {
        try
        {
            return getActionClass().newInstance();
        }
        catch (InstantiationException | IllegalAccessException e)
        {
            throw new RuntimeException(e);
        }
    }
}