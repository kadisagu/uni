package ru.tandemservice.movestudent.entity;

import org.apache.commons.lang.ObjectUtils;
import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.*;

import java.util.Date;

/**
 * Проект приказа «О переводе со специализации на специализацию (с профиля на профиль) и перераспределении в группу»
 */
public class ChangeProfileAndGroupStuListExtract extends ChangeProfileAndGroupStuListExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return (Date) ObjectUtils.defaultIfNull(getTransferDate(), getBeginDate());
    }
}