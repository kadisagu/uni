package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.GroupAssignStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О распределении в группу
 */
public class GroupAssignStuExtract extends GroupAssignStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getBeginDate();
    }
}