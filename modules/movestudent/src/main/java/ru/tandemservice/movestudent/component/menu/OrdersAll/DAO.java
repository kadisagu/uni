/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.menu.OrdersAll;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import ru.tandemservice.movestudent.component.commons.ExtractListUtil;
import ru.tandemservice.movestudent.component.commons.ExtractTypeSelectModel;
import ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder.*;
import ru.tandemservice.movestudent.component.commons.OrderCategory;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.movestudent.component.commons.OrderFlexBuilder.*;

/**
 * @author vip_delete
 * @since 22.12.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        ExtractListUtil.initExtractListModel(model, "ordersAll", null);
        model.setEmployeePostModel(new OrderExecutorSelectModel());
        model.setOrderCategoryModel(new StaticSelectModel("id", "title", Arrays.asList(OrderCategory.LIST_ORDER_CATEGORY, OrderCategory.MODULAR_ORDER_CATEGORY, OrderCategory.INDIVIDUAL_ORDER_CATEGORY, OrderCategory.OTHERS_ORDER_CATEGORY)));
        model.setOrderTypeModel(new ExtractTypeSelectModel(model).hierarchical(true));
    }

    @Override
    @Zlo(value = "Эксперимент с FlexDQLBuilder признан несостоявшимся. Нужно всё переосмыслить.")
    public void prepareListDataSource(final Model model)
    {
        // TODO
        // 1. Переделать запрос. Тормозит при некоторых условиях.
        // 2. Комментарий прочего приказа выносить в descriptionColumn и брать оттуда, чтобы не грузить потом выписку.

        final IFlexColumn countCol = new SimpleFlexColumn(ORDER_DATA_JOIN_ALIAS, EXTRACT_COUNT).addViewNames(ORDER_DESCRIPTION);
        final IFlexColumn isOtherOrderCol = new SimpleFlexColumn(ORDER_DATA_JOIN_ALIAS, IS_OTHER_ORDER);
        final IFlexColumn singleExtractCol = new SimpleFlexColumn(ORDER_DATA_JOIN_ALIAS, SINGLE_EXTRACT).setViewNames(ORDER_DESCRIPTION);
        final IFlexColumn otherOrderCanPrintCol = new SimpleFlexColumn(ORDER_DATA_JOIN_ALIAS, OTHER_ORDER_CAN_PRINT);
        final boolean showDescriptionColumn = FlexDQLBuilder.isColumnVisible(model.getDataSource(), ORDER_DESCRIPTION);
        final Collection<IEntity> categorySet = model.getSettings().get(ORDER_CATEGORY);

        IFlexFilter reasonFilter = new BaseFlexFilter(ORDER_REASON, ORDER_ALIAS, "id")
        {
            @Override
            protected IDQLExpression internalCreateCondition(Object o)
            {
                Collection collection = (Collection) o;
                boolean all = categorySet == null || categorySet.isEmpty();

                IDQLExpression listExpr = null, modularExpr = null, otherExpr = null;

                if (all || categorySet.contains(OrderCategory.LIST_ORDER_CATEGORY))
                {
                    listExpr = in(property(getAlias(), getPath()),
                                  new DQLSelectBuilder().fromEntity(StudentListOrder.class, "list")
                                          .column("list.id")
                                          .where(in("list." + StudentListOrder.L_REASON, collection)).buildQuery());
                }
                if (all || categorySet.contains(OrderCategory.MODULAR_ORDER_CATEGORY) || categorySet.contains(OrderCategory.INDIVIDUAL_ORDER_CATEGORY))
                {
                    modularExpr = in(property(getAlias(), getPath()),
                                     new DQLSelectBuilder().fromEntity(ModularStudentExtract.class, "mod")
                                             .column("par." + IAbstractParagraph.L_ORDER)
                                             .joinPath(DQLJoinType.inner, "mod." + IAbstractExtract.L_PARAGRAPH, "par")
                                             .where(in("mod." + ModularStudentExtract.L_REASON, collection)).buildQuery());
                }
                if (all || categorySet.contains(OrderCategory.OTHERS_ORDER_CATEGORY))
                {
                    otherExpr = in(property(getAlias(), getPath()),
                                   new DQLSelectBuilder().fromEntity(OtherStudentExtract.class, "other")
                                           .column("par." + IAbstractParagraph.L_ORDER)
                                           .joinPath(DQLJoinType.inner, "other." + IAbstractExtract.L_PARAGRAPH, "par")
                                           .where(in("other." + OtherStudentExtract.L_REASON, collection)).buildQuery());
                }

                return or(listExpr, modularExpr, otherExpr);
            }
        };

        IFlexConfig extractConfig = new FlexConfig(AbstractStudentExtract.class, EXTRACT_ALIAS)
                .addColumns(
                        new SimpleFlexColumn(PARAGRAPH_ALIAS, IAbstractParagraph.L_ORDER)
                )
                .addJoins(
                        new SimpleFlexJoin(PARAGRAPH_ALIAS, EXTRACT_ALIAS, IAbstractExtract.L_PARAGRAPH, true),
                        STUDENT_JOIN,
                        EDUCATION_ORGUNIT_JOIN,
                        EDUCATION_LEVELS_HIGH_SCHOOL_JOIN
                )
                .addFilters(
                        STUDENT_FILTER,
                        FORMATIVE_ORG_UNIT_FILTER,
                        TERRITORIAL_ORG_UNIT_FILTER,
                        PRODUCING_ORG_UNIT_FILTER,
                        EDUCATION_LEVEL_HIGH_SCHOOL_FILTER,
                        COURSE_FILTER,
                        DEVELOP_FORM_FILTER,
                        COMPENSATION_TYPE_FILTER,
                        EXTRACT_TYPE_FILTER
                );


        IFlexConfig mainConfig = new FlexConfig(AbstractStudentOrder.class, ORDER_ALIAS)
                .addColumns(
                        ORDER_CREATE_DATE_COLUMN,
                        ORDER_COMMIT_DATE_COLUMN,
                        ORDER_NUMBER_COLUMN,
                        ORDER_COMMIT_DATE_SYSTEM_COLUMN,
                        countCol,
                        singleExtractCol
                )
                .addFilters(
                        ORDER_FINISHED_FILTER,
                        ORDER_EDU_YEAR_FILTER,
                        ORDER_COMMIT_DATE_PERIOD_FILTER,
                        ORDER_COMMIT_DATE_SYSTEM_PERIOD_FILTER,
                        ORDER_CREATE_DATE_PERIOD_FILTER,
                        ORDER_NUMBER_FILTER,
                        new SubQueryFlexFilter(ORDER_ALIAS, "id", extractConfig, IAbstractParagraph.L_ORDER),
                        reasonFilter,
                        EXECUTOR_FILTER
                )
                .addOrders(
                        ORDER_CREATE_DATE_SORT,
                        ORDER_COMMIT_DATE_SORT,
                        ORDER_COMMIT_SYSTEM_DATE_SORT,
                        ORDER_NUMBER_SORT,
                        ORDER_EXTRACT_COUNT_SORT
                )
                .addJoins(
                        new OrdersDataJoin(categorySet, DQLJoinType.inner, false, showDescriptionColumn, true, false)
                );

        final boolean includeOtherOrders = categorySet == null || categorySet.isEmpty() || categorySet.contains(OrderCategory.OTHERS_ORDER_CATEGORY);
        if (includeOtherOrders)
            mainConfig.addColumns(otherOrderCanPrintCol, isOtherOrderCol);

        FlexDQLBuilder flexBuilder = new FlexDQLBuilder(mainConfig, false);

        final Map<Long, DataWrapper> extractIdsForComment = new HashMap<>();
        flexBuilder.fillDataSource(getSession(), model.getSettings(), model.getDataSource(), new IPatchResolver()
        {
            @Override
            public void patch(List<Object[]> srcList, List<IEntity> destList, List<IFlexColumn> columns)
            {
                int singleExtractIdx = columns.indexOf(singleExtractCol);
                int extractCountIdx = columns.indexOf(countCol);
                int otherOrderCanPrintIdx = columns.indexOf(otherOrderCanPrintCol);
                int isOtherIdx = columns.indexOf(isOtherOrderCol);

                for (int i = 0; i < srcList.size(); i++)
                {
                    Object[] cols = srcList.get(i);
                    DataWrapper wrapper = (DataWrapper) destList.get(i);
                    wrapper.setProperty(Model.EDIT_DISABLED, true);

                    if (showDescriptionColumn)
                    {
                        String description = "";
                        if (((Number) cols[extractCountIdx]).longValue() == 1L)
                        {
                            extractIdsForComment.put((Long) cols[singleExtractIdx], wrapper);
                            wrapper.setProperty(Model.EDIT_DISABLED, false);
                        }
                        wrapper.setProperty(ORDER_DESCRIPTION, description);
                    }

                    boolean isOtherOrder = includeOtherOrders && (Boolean) cols[isOtherIdx];
                    wrapper.setProperty(DISABLE_PRINT, isOtherOrder && !((Boolean) cols[otherOrderCanPrintIdx]));
                    wrapper.setProperty(DISABLE_PRINT_PDF, isOtherOrder);
                }
            }
        });

        // Получаем комментарии, если нужны
        if (!extractIdsForComment.isEmpty())
        {
            List<Object[]> commentItems = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                    .column(property("e.id"))
                    .column(property("e", AbstractStudentExtract.P_COMMENT))
                    .where(in("e.id", extractIdsForComment.keySet()))
                    .createStatement(getSession()).list();

            for (Object[] item : commentItems)
            {
                DataWrapper wrapper = extractIdsForComment.get((Long) item[0]);
                wrapper.setProperty(ORDER_DESCRIPTION, item[1]);
            }
        }

        // Конечно, очистка сессии выглядить спорно.
        // Но другого выхода не нашлось - на списке из 500 элементов размер сессии - 4 Мб, а при листании еще и постоянно растет.
        getSession().clear();
    }

    @Override
    public void deleteRow(IBusinessComponent context)
    {
        IAbstractOrder order = get((Long) context.getListenerParameter());
        ExtractStates excepted = getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED);
        MoveDaoFacade.getMoveDao().deleteOrder(order, excepted);
    }
}
