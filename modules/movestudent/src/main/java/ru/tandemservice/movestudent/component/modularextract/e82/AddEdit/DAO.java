/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e82.AddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.RePassDiscStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToRePassDiscipline;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 31.08.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<RePassDiscStuExtract, Model> implements IDAO
{
    @Override
    protected RePassDiscStuExtract createNewInstance()
    {
        return new RePassDiscStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setChangesNothing(true);
        model.setControlActionsList(Arrays.asList((ApplicationRuntime.getProperty("commonextract.controlActionsList")).split(";")));
        model.setTermsList(DevelopGridDAO.getTermMap().values());
        model.setEduYearList(new EducationYearModel());

        if (model.isEditForm())
        {
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(StuExtractToRePassDiscipline.class, "d").column("d")
                    .where(eq(property(StuExtractToRePassDiscipline.extract().id().fromAlias("d")), value(model.getExtractId())))
                    .order(property(StuExtractToRePassDiscipline.priority().fromAlias("d")));
            model.setDisciplineList(builder.createStatement(getSession()).<StuExtractToRePassDiscipline>list());
        } else
        {
            List<StuExtractToRePassDiscipline> discList = new ArrayList<>();
            discList.add(new StuExtractToRePassDiscipline());
            model.setDisciplineList(discList);
        }
    }

    @Override
    public void update(Model model)
    {
        super.update(model);

        new DQLDeleteBuilder(StuExtractToRePassDiscipline.class)
                .where(eq(property(StuExtractToRePassDiscipline.extract().id()), value(model.getExtract().getId())))
                .createStatement(getSession()).execute();

        int i = 0;
        for (StuExtractToRePassDiscipline discipline : model.getDisciplineList())
        {
            StuExtractToRePassDiscipline newDiscipline = new StuExtractToRePassDiscipline();
            newDiscipline.update(discipline);

            newDiscipline.setPriority(i++);
            newDiscipline.setExtract(model.getExtract());
            getSession().save(newDiscipline);
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);
        if (model.getDisciplineList().isEmpty())
            errors.add("Необходимо указать хотя бы одну пересдаваемую дисциплину.", "");
    }
}