/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.component.commons;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StudentReasonToTypeRel;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.movestudent.ui.UnimoveStudentMultiSelectModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.EducationLevelsHighSchoolSelectModel;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 01.11.2008
 */
public class ExtractListUtil
{
    public static void initExtractListModel(final ExtractListModel model, String secPostfix, OrderCategory orderCategory)
    {
        final IUniBaseDao dao = UniDaoFacade.getCoreDao();

        model.setSecModel(new CommonPostfixPermissionModel(secPostfix));
        model.setEducationYearList(new EducationYearModel());
        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setProducingOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING));
        model.setEducationLevelHighSchool(new EducationLevelsHighSchoolSelectModel(model));
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setCompensationTypeList(dao.getCatalogItemList(CompensationType.class));
        model.setExtractStateList(dao.getCatalogItemList(ExtractStates.class));
        model.setStudentList(new UnimoveStudentMultiSelectModel());
        if (orderCategory != null)
            model.setExtractTypesListModel(new ExtractTypeSelectModel(orderCategory).includeExistsTypesFromContext(true).hierarchical(orderCategory == OrderCategory.OTHER));
        else
            model.setExtractTypesListModel(new ExtractTypeSelectModel().hierarchical(true));

        model.setExtractReasonModel(new CommonMultiSelectModel(StudentOrderReasons.title().s())
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                Object o = model.getSettings().get(IMoveStudentMQBuilder.TYPE);
                Collection typesList;
                if (o instanceof Collection)
                    typesList = (Collection) o;
                else
                    typesList = new HashSet<>(Arrays.asList(o));

                    DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(StudentReasonToTypeRel.class, "rel").column(property(StudentReasonToTypeRel.first().fromAlias("rel")))
                    .where(likeUpper(property(StudentReasonToTypeRel.first().title().fromAlias("rel")), value(CoreStringUtils.escapeLike(filter, true))));
                    if (o != null && !typesList.isEmpty())
                        builder.where(in(property(StudentReasonToTypeRel.second().fromAlias("rel")), typesList));
                    if (set != null) builder.where(in(property(StudentReasonToTypeRel.first().id().fromAlias("rel")), set));
                    builder.order(property(StudentReasonToTypeRel.first().title().fromAlias("rel")));
                    return new DQLListResultBuilder(builder, 50);
            }
        });
    }

    /**
     * Подготавливает запрос для получения списка проектов из сборных приказов
     *
     * @param settings настройки фильтрации
     * @param orgUnit  подразделение, на публикаторе которого находится список (null, если список для всего ОУ)
     * @return сформированный MQBuilder
     */
    public static MQBuilder prepareProjectListBuilder(IDataSettings settings, OrgUnit orgUnit)
    {
        MQBuilder builder = prepareModularExtractListBuilder(settings, orgUnit);
        builder.add(MQExpression.isNull("extract", IAbstractExtract.L_PARAGRAPH));

        if (settings.get("extractState") == null)
        {
            Object[] extractStatesCodes = {UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTABLE, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED, UnimoveDefines.CATALOG_EXTRACT_STATE_REJECTED};
            builder.add(MQExpression.in("state", ICatalogItem.CATALOG_ITEM_CODE, extractStatesCodes));
        }
        return builder;
    }

    /**
     * Подготавливает запрос для получения списка выписок из сборных приказов
     *
     * @param settings настройки фильтрации
     * @param orgUnit  подразделение, на публикаторе которого находится список (null, если список для всего ОУ)
     * @return сформированный MQBuilder
     */
    public static MQBuilder prepareExtractListBuilder(IDataSettings settings, OrgUnit orgUnit)
    {
        MQBuilder builder = prepareModularExtractListBuilder(settings, orgUnit);
        builder.addJoin("extract", IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER, "order_");

        addFilter(builder, settings, ExpressionType.LIKE, "order_", IAbstractOrder.P_NUMBER, "orderNumber");
        addFilter(builder, settings, ExpressionType.DATE_EQ, "order_", AbstractStudentOrder.P_COMMIT_DATE, "extractCommitDate");

        if (settings.get("extractState") == null)
        {
            Object[] extractStatesCodes = {UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED};
            builder.add(MQExpression.in("state", ICatalogItem.CATALOG_ITEM_CODE, extractStatesCodes));
        }
        return builder;
    }

    private static MQBuilder prepareModularExtractListBuilder(IDataSettings settings, OrgUnit orgUnit)
    {
        MQBuilder builder = new MQBuilder(ModularStudentExtract.ENTITY_CLASS, "extract");
        builder.addJoin("extract", IAbstractExtract.L_STATE, "state");
        builder.addJoin("extract", IAbstractExtract.L_ENTITY, "student");
        builder.addJoin("student", Student.L_PERSON + "." + Person.L_IDENTITY_CARD, "idCard");
        builder.addJoin("student", Student.L_EDUCATION_ORG_UNIT, "educationOrgUnit");
        builder.addJoin("educationOrgUnit", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "educationLevelHighSchool");

        if (orgUnit != null)
        {
            AbstractExpression eqFormative = MQExpression.eq("educationOrgUnit", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, orgUnit);
            AbstractExpression eqTerritorial = MQExpression.eq("educationOrgUnit", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, orgUnit);
            builder.add(MQExpression.or(eqFormative, eqTerritorial));
        }

        addFilter(builder, settings, ExpressionType.EDUCATION_YEAR_EQ, "extract", IAbstractExtract.P_CREATE_DATE, "educationYear");
        addFilter(builder, settings, ExpressionType.DATE_EQ, "extract", IAbstractExtract.P_CREATE_DATE, "extractCreateDate");
        addFilter(builder, settings, ExpressionType.LIKE, "order_", IAbstractOrder.P_NUMBER, "orderNumber");
        addFilter(builder, settings, ExpressionType.DATE_EQ, "order_", AbstractStudentOrder.P_COMMIT_DATE, "extractCommitDate");
        addFilter(builder, settings, ExpressionType.DATE_EQ, "order_", IAbstractOrder.P_COMMIT_DATE_SYSTEM, "extractCommitDateSystem");
        addFilter(builder, settings, ExpressionType.LIKE, "idCard", IdentityCard.P_LAST_NAME, "studentLastName");
        addFilter(builder, settings, ExpressionType.EQ, "educationOrgUnit", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, "formativeOrgUnit");
        addFilter(builder, settings, ExpressionType.EQ, "educationOrgUnit", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, "territorialOrgUnit");
        addFilter(builder, settings, ExpressionType.EQ, "educationLevelHighSchool", EducationLevelsHighSchool.L_ORG_UNIT, "producingOrgUnit");
        addFilter(builder, settings, ExpressionType.EQ, "educationOrgUnit", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "educationLevelHighSchool");
        addFilter(builder, settings, ExpressionType.EQ, "student", Student.L_COURSE, "course");
        addFilter(builder, settings, ExpressionType.EQ, "educationOrgUnit", EducationOrgUnit.L_DEVELOP_FORM, "developForm");
        addFilter(builder, settings, ExpressionType.EQ, "student", Student.L_COMPENSATION_TYPE, "compensationType");
        addFilter(builder, settings, ExpressionType.EQ, "extract", IAbstractExtract.L_TYPE, "extractType");
        addFilter(builder, settings, ExpressionType.EQ, "extract", IAbstractExtract.L_STATE, "extractState");

        return builder;
    }

    public static enum ExpressionType
    {
        EQ, EDUCATION_YEAR_EQ, DATE_EQ, LIKE
    }

    public static void addFilter(MQBuilder builder, IDataSettings settings, ExpressionType expressionType, String alias, String property, String key)
    {
        Object value = settings.get(key);
        if (value == null)
            return;

        AbstractExpression expression;
        switch (expressionType)
        {
            case EQ:
                expression = MQExpression.eq(alias, property, value);
                break;
            case EDUCATION_YEAR_EQ:
                expression = UniMQExpression.eqEduYear(alias, property, (EducationYear) value);
                break;
            case DATE_EQ:
                expression = UniMQExpression.eqDate(alias, property, (Date) value);
                break;
            case LIKE:
                expression = MQExpression.like(alias, property, "%" + value);
                break;
            default:
                throw new RuntimeException("Unknown expression type: " + expressionType);
        }
        builder.add(expression);
    }
}