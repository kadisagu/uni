/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeEducationOrgUnit.AddEdit;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditController;
import ru.tandemservice.movestudent.entity.custom.ChangeEducationOrgUnitAction;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class Controller extends BaseCustomActionAddEditController<ChangeEducationOrgUnitAction, IDAO, Model>
{
}