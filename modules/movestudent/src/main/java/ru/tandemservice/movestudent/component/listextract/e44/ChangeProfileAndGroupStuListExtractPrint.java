/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e44;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.IStudentListParagraphPrintFormatter;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ChangeProfileAndGroupStuListExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* @author ListExtractComponentGenerator
* @since 14.04.2014
*/
public class ChangeProfileAndGroupStuListExtractPrint implements IPrintFormCreator<ChangeProfileAndGroupStuListExtract>, IListParagraphPrintFormCreator<ChangeProfileAndGroupStuListExtract>, IStudentListParagraphPrintFormatter
{
    public static final List<String> PROCESSED_QUALIFICATION_CODES = Arrays.asList(QualificationsCodes.BAKALAVR, QualificationsCodes.MAGISTR, QualificationsCodes.SPETSIALIST);
    public static final Map<String, String[]> PROFILE_STRING_CASES_ARRAY = new HashMap<>();
    public static final Map<String, String[]> PROFILE_STRING_PLURAL_CASES_ARRAY = new HashMap<>();

    static
    {
        PROFILE_STRING_CASES_ARRAY.put(QualificationsCodes.BAKALAVR, CommonExtractPrint.PROFILE_CASES);
        PROFILE_STRING_CASES_ARRAY.put(QualificationsCodes.MAGISTR, CommonExtractPrint.MASTER_PROGRAM_CASES);
        PROFILE_STRING_CASES_ARRAY.put(QualificationsCodes.SPETSIALIST, CommonExtractPrint.SPECIALIZATION_CASES);
        PROFILE_STRING_CASES_ARRAY.put(null, CommonExtractPrint.DIRECTION_CASES);

        PROFILE_STRING_PLURAL_CASES_ARRAY.put(QualificationsCodes.BAKALAVR, CommonExtractPrint.PROFILE_PLURAL_CASES);
        PROFILE_STRING_PLURAL_CASES_ARRAY.put(QualificationsCodes.MAGISTR, CommonExtractPrint.MASTER_PROGRAM_PLURAL_CASES);
        PROFILE_STRING_PLURAL_CASES_ARRAY.put(QualificationsCodes.SPETSIALIST, CommonExtractPrint.SPECIALIZATION_PLURAL_CASES);
        PROFILE_STRING_PLURAL_CASES_ARRAY.put(null, CommonExtractPrint.DIRECTION_PLURAL_CASES);
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, ChangeProfileAndGroupStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, ChangeProfileAndGroupStuListExtract firstExtract)
    {
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);

        CommonExtractPrint.initOrgUnit(injectModifier, firstExtract.getEntity().getEducationOrgUnit(), "formativeOrgUnitStr", "");
	    CommonExtractPrint.initEducationType(injectModifier, firstExtract.getEducationOrgUnitOld(), "Old");

        EducationLevels speciality = EducationOrgUnitUtil.getParentLevel(firstExtract.getEducationOrgUnitOld().getEducationLevelHighSchool());
        CommonExtractPrint.modifyEducationStr(injectModifier, speciality, new String[]{"fefuEducationStrDirectionOld"}, false);

        injectModifier.put("groupNew", firstExtract.getGroupNew().getTitle());
        injectModifier.put("specialization", firstExtract.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle());
        injectModifier.put("specializationNew", firstExtract.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle());
	    injectModifier.put("eduTypeString", firstExtract.getEducationOrgUnitOld().getEducationLevelHighSchool().isSpeciality() ? "вышеуказанной специальности" : "вышеуказанному направлению подготовки");

        Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(firstExtract.getEducationOrgUnitNew().getEducationLevelHighSchool().getEducationLevel());
        String qualificationCode = null != qualification ? qualification.getCode() : null;
        if (null != qualificationCode && !PROCESSED_QUALIFICATION_CODES.contains(qualificationCode))
            qualificationCode = null;

        for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
        {
            injectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[i]);
            injectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i) + "P", PROFILE_STRING_PLURAL_CASES_ARRAY.get(qualificationCode)[i]);
        }


        StringBuilder fefuEducationStrProfileNew = new StringBuilder();
        StringBuilder fefuEducationStrProfileNew_I = new StringBuilder();

        fefuEducationStrProfileNew.append(PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[0]).append(" «");
        fefuEducationStrProfileNew.append(firstExtract.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle()).append("»");
        injectModifier.put("fefuEducationStrProfileNew", StringUtils.capitalize(fefuEducationStrProfileNew.toString()));

        fefuEducationStrProfileNew_I.append(PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[4]).append(" «");
        fefuEducationStrProfileNew_I.append(firstExtract.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle()).append("»");

        CommonListOrderPrint.injectFefuDevelopConditionAndTech(injectModifier, firstExtract.getEducationOrgUnitOld(), CommonListOrderPrint.getEducationBaseText(firstExtract.getGroupOld()), "fefuShortFastExtendedOptionalText");

        injectModifier.put("fefuEducationStrProfileNew_I", fefuEducationStrProfileNew_I.toString());

        injectModifier.put("transferDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(firstExtract.getTransferOrOrderDate()));

        return injectModifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, ChangeProfileAndGroupStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, ChangeProfileAndGroupStuListExtract firstExtract)
    {
        CommonExtractPrint.initOrgUnit(modifier, firstExtract.getEntity().getEducationOrgUnit(), "formativeOrgUnitStr", "");

        modifier.put("course", firstExtract.getEntity().getCourse().getTitle());
        modifier.put("group", firstExtract.getGroupOld().getTitle());

        CommonListExtractPrint.injectCompensationType(modifier, firstExtract.getEntity().getCompensationType(), "", firstExtract.getEntity().isTargetAdmission());
        UniRtfUtil.initEducationType(modifier, firstExtract.getEducationOrgUnitOld().getEducationLevelHighSchool(), "");
        modifier.put("educationOrgUnit", firstExtract.getEducationOrgUnitOld().getEducationLevelHighSchool().getPrintTitleWithoutOrgUnit());
        CommonExtractPrint.initDevelopForm(modifier, firstExtract.getEducationOrgUnitOld(), "");

        Qualifications qualification = firstExtract.getEducationOrgUnitNew().getEducationLevelHighSchool().getEducationLevel().getQualification();
        String qualificationCode = null != qualification ? qualification.getCode() : null;
        if (null != qualificationCode && !PROCESSED_QUALIFICATION_CODES.contains(qualificationCode))
        {
            qualificationCode = null;
        }

        for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
        {
            modifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[i]);
            modifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i) + "P", PROFILE_STRING_PLURAL_CASES_ARRAY.get(qualificationCode)[i]);
        }

        EducationLevels speciality = EducationOrgUnitUtil.getParentLevel(firstExtract.getEducationOrgUnitOld().getEducationLevelHighSchool());
        CommonExtractPrint.modifyEducationStr(modifier, speciality, new String[]{"fefuEducationStrDirectionOld"}, false);
    }

    @Override
    public String formatSingleStudent(Student student, int extractNumber)
    {
        return "\\par " + extractNumber + ".  " + student.getPerson().getFullFio();
    }
}