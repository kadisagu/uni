package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AcadGrantBonusPaymentStopStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О прекращении выплаты надбавки к государственной академической стипендии 
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AcadGrantBonusPaymentStopStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.AcadGrantBonusPaymentStopStuExtract";
    public static final String ENTITY_NAME = "acadGrantBonusPaymentStopStuExtract";
    public static final int VERSION_HASH = -17266711;
    private static IEntityMeta ENTITY_META;

    public static final String L_ACAD_GRANT_BONUS_EXTRACT = "acadGrantBonusExtract";
    public static final String P_ACAD_GRANT_BONUS_ORDER_NUMBER = "acadGrantBonusOrderNumber";
    public static final String P_ACAD_GRANT_BONUS_ORDER_DATE = "acadGrantBonusOrderDate";
    public static final String P_ACAD_GRANT_BONUS_PAYMENT_STOP_DATE = "acadGrantBonusPaymentStopDate";
    public static final String P_ACAD_GRANT_BONUS_REASON = "acadGrantBonusReason";

    private AbstractStudentExtract _acadGrantBonusExtract;     // Выписка о назначении надбавки к академической стипендии
    private String _acadGrantBonusOrderNumber;     // Номер приказа о назначении надбавки к академической стипендии
    private Date _acadGrantBonusOrderDate;     // Дата приказа о назначении надбавки к академической стипендии
    private Date _acadGrantBonusPaymentStopDate;     // Дата прекращения выплаты надбавки к академической стипендии
    private String _acadGrantBonusReason;     // Причина назначения надбавки к академической стипендии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка о назначении надбавки к академической стипендии.
     */
    public AbstractStudentExtract getAcadGrantBonusExtract()
    {
        return _acadGrantBonusExtract;
    }

    /**
     * @param acadGrantBonusExtract Выписка о назначении надбавки к академической стипендии.
     */
    public void setAcadGrantBonusExtract(AbstractStudentExtract acadGrantBonusExtract)
    {
        dirty(_acadGrantBonusExtract, acadGrantBonusExtract);
        _acadGrantBonusExtract = acadGrantBonusExtract;
    }

    /**
     * @return Номер приказа о назначении надбавки к академической стипендии. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getAcadGrantBonusOrderNumber()
    {
        return _acadGrantBonusOrderNumber;
    }

    /**
     * @param acadGrantBonusOrderNumber Номер приказа о назначении надбавки к академической стипендии. Свойство не может быть null.
     */
    public void setAcadGrantBonusOrderNumber(String acadGrantBonusOrderNumber)
    {
        dirty(_acadGrantBonusOrderNumber, acadGrantBonusOrderNumber);
        _acadGrantBonusOrderNumber = acadGrantBonusOrderNumber;
    }

    /**
     * @return Дата приказа о назначении надбавки к академической стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getAcadGrantBonusOrderDate()
    {
        return _acadGrantBonusOrderDate;
    }

    /**
     * @param acadGrantBonusOrderDate Дата приказа о назначении надбавки к академической стипендии. Свойство не может быть null.
     */
    public void setAcadGrantBonusOrderDate(Date acadGrantBonusOrderDate)
    {
        dirty(_acadGrantBonusOrderDate, acadGrantBonusOrderDate);
        _acadGrantBonusOrderDate = acadGrantBonusOrderDate;
    }

    /**
     * @return Дата прекращения выплаты надбавки к академической стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getAcadGrantBonusPaymentStopDate()
    {
        return _acadGrantBonusPaymentStopDate;
    }

    /**
     * @param acadGrantBonusPaymentStopDate Дата прекращения выплаты надбавки к академической стипендии. Свойство не может быть null.
     */
    public void setAcadGrantBonusPaymentStopDate(Date acadGrantBonusPaymentStopDate)
    {
        dirty(_acadGrantBonusPaymentStopDate, acadGrantBonusPaymentStopDate);
        _acadGrantBonusPaymentStopDate = acadGrantBonusPaymentStopDate;
    }

    /**
     * @return Причина назначения надбавки к академической стипендии. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getAcadGrantBonusReason()
    {
        return _acadGrantBonusReason;
    }

    /**
     * @param acadGrantBonusReason Причина назначения надбавки к академической стипендии. Свойство не может быть null.
     */
    public void setAcadGrantBonusReason(String acadGrantBonusReason)
    {
        dirty(_acadGrantBonusReason, acadGrantBonusReason);
        _acadGrantBonusReason = acadGrantBonusReason;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof AcadGrantBonusPaymentStopStuExtractGen)
        {
            setAcadGrantBonusExtract(((AcadGrantBonusPaymentStopStuExtract)another).getAcadGrantBonusExtract());
            setAcadGrantBonusOrderNumber(((AcadGrantBonusPaymentStopStuExtract)another).getAcadGrantBonusOrderNumber());
            setAcadGrantBonusOrderDate(((AcadGrantBonusPaymentStopStuExtract)another).getAcadGrantBonusOrderDate());
            setAcadGrantBonusPaymentStopDate(((AcadGrantBonusPaymentStopStuExtract)another).getAcadGrantBonusPaymentStopDate());
            setAcadGrantBonusReason(((AcadGrantBonusPaymentStopStuExtract)another).getAcadGrantBonusReason());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AcadGrantBonusPaymentStopStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AcadGrantBonusPaymentStopStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new AcadGrantBonusPaymentStopStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "acadGrantBonusExtract":
                    return obj.getAcadGrantBonusExtract();
                case "acadGrantBonusOrderNumber":
                    return obj.getAcadGrantBonusOrderNumber();
                case "acadGrantBonusOrderDate":
                    return obj.getAcadGrantBonusOrderDate();
                case "acadGrantBonusPaymentStopDate":
                    return obj.getAcadGrantBonusPaymentStopDate();
                case "acadGrantBonusReason":
                    return obj.getAcadGrantBonusReason();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "acadGrantBonusExtract":
                    obj.setAcadGrantBonusExtract((AbstractStudentExtract) value);
                    return;
                case "acadGrantBonusOrderNumber":
                    obj.setAcadGrantBonusOrderNumber((String) value);
                    return;
                case "acadGrantBonusOrderDate":
                    obj.setAcadGrantBonusOrderDate((Date) value);
                    return;
                case "acadGrantBonusPaymentStopDate":
                    obj.setAcadGrantBonusPaymentStopDate((Date) value);
                    return;
                case "acadGrantBonusReason":
                    obj.setAcadGrantBonusReason((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "acadGrantBonusExtract":
                        return true;
                case "acadGrantBonusOrderNumber":
                        return true;
                case "acadGrantBonusOrderDate":
                        return true;
                case "acadGrantBonusPaymentStopDate":
                        return true;
                case "acadGrantBonusReason":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "acadGrantBonusExtract":
                    return true;
                case "acadGrantBonusOrderNumber":
                    return true;
                case "acadGrantBonusOrderDate":
                    return true;
                case "acadGrantBonusPaymentStopDate":
                    return true;
                case "acadGrantBonusReason":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "acadGrantBonusExtract":
                    return AbstractStudentExtract.class;
                case "acadGrantBonusOrderNumber":
                    return String.class;
                case "acadGrantBonusOrderDate":
                    return Date.class;
                case "acadGrantBonusPaymentStopDate":
                    return Date.class;
                case "acadGrantBonusReason":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AcadGrantBonusPaymentStopStuExtract> _dslPath = new Path<AcadGrantBonusPaymentStopStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AcadGrantBonusPaymentStopStuExtract");
    }
            

    /**
     * @return Выписка о назначении надбавки к академической стипендии.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusPaymentStopStuExtract#getAcadGrantBonusExtract()
     */
    public static AbstractStudentExtract.Path<AbstractStudentExtract> acadGrantBonusExtract()
    {
        return _dslPath.acadGrantBonusExtract();
    }

    /**
     * @return Номер приказа о назначении надбавки к академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusPaymentStopStuExtract#getAcadGrantBonusOrderNumber()
     */
    public static PropertyPath<String> acadGrantBonusOrderNumber()
    {
        return _dslPath.acadGrantBonusOrderNumber();
    }

    /**
     * @return Дата приказа о назначении надбавки к академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusPaymentStopStuExtract#getAcadGrantBonusOrderDate()
     */
    public static PropertyPath<Date> acadGrantBonusOrderDate()
    {
        return _dslPath.acadGrantBonusOrderDate();
    }

    /**
     * @return Дата прекращения выплаты надбавки к академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusPaymentStopStuExtract#getAcadGrantBonusPaymentStopDate()
     */
    public static PropertyPath<Date> acadGrantBonusPaymentStopDate()
    {
        return _dslPath.acadGrantBonusPaymentStopDate();
    }

    /**
     * @return Причина назначения надбавки к академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusPaymentStopStuExtract#getAcadGrantBonusReason()
     */
    public static PropertyPath<String> acadGrantBonusReason()
    {
        return _dslPath.acadGrantBonusReason();
    }

    public static class Path<E extends AcadGrantBonusPaymentStopStuExtract> extends ModularStudentExtract.Path<E>
    {
        private AbstractStudentExtract.Path<AbstractStudentExtract> _acadGrantBonusExtract;
        private PropertyPath<String> _acadGrantBonusOrderNumber;
        private PropertyPath<Date> _acadGrantBonusOrderDate;
        private PropertyPath<Date> _acadGrantBonusPaymentStopDate;
        private PropertyPath<String> _acadGrantBonusReason;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка о назначении надбавки к академической стипендии.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusPaymentStopStuExtract#getAcadGrantBonusExtract()
     */
        public AbstractStudentExtract.Path<AbstractStudentExtract> acadGrantBonusExtract()
        {
            if(_acadGrantBonusExtract == null )
                _acadGrantBonusExtract = new AbstractStudentExtract.Path<AbstractStudentExtract>(L_ACAD_GRANT_BONUS_EXTRACT, this);
            return _acadGrantBonusExtract;
        }

    /**
     * @return Номер приказа о назначении надбавки к академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusPaymentStopStuExtract#getAcadGrantBonusOrderNumber()
     */
        public PropertyPath<String> acadGrantBonusOrderNumber()
        {
            if(_acadGrantBonusOrderNumber == null )
                _acadGrantBonusOrderNumber = new PropertyPath<String>(AcadGrantBonusPaymentStopStuExtractGen.P_ACAD_GRANT_BONUS_ORDER_NUMBER, this);
            return _acadGrantBonusOrderNumber;
        }

    /**
     * @return Дата приказа о назначении надбавки к академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusPaymentStopStuExtract#getAcadGrantBonusOrderDate()
     */
        public PropertyPath<Date> acadGrantBonusOrderDate()
        {
            if(_acadGrantBonusOrderDate == null )
                _acadGrantBonusOrderDate = new PropertyPath<Date>(AcadGrantBonusPaymentStopStuExtractGen.P_ACAD_GRANT_BONUS_ORDER_DATE, this);
            return _acadGrantBonusOrderDate;
        }

    /**
     * @return Дата прекращения выплаты надбавки к академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusPaymentStopStuExtract#getAcadGrantBonusPaymentStopDate()
     */
        public PropertyPath<Date> acadGrantBonusPaymentStopDate()
        {
            if(_acadGrantBonusPaymentStopDate == null )
                _acadGrantBonusPaymentStopDate = new PropertyPath<Date>(AcadGrantBonusPaymentStopStuExtractGen.P_ACAD_GRANT_BONUS_PAYMENT_STOP_DATE, this);
            return _acadGrantBonusPaymentStopDate;
        }

    /**
     * @return Причина назначения надбавки к академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantBonusPaymentStopStuExtract#getAcadGrantBonusReason()
     */
        public PropertyPath<String> acadGrantBonusReason()
        {
            if(_acadGrantBonusReason == null )
                _acadGrantBonusReason = new PropertyPath<String>(AcadGrantBonusPaymentStopStuExtractGen.P_ACAD_GRANT_BONUS_REASON, this);
            return _acadGrantBonusReason;
        }

        public Class getEntityClass()
        {
            return AcadGrantBonusPaymentStopStuExtract.class;
        }

        public String getEntityName()
        {
            return "acadGrantBonusPaymentStopStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
