package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.GroupToExtractRelation;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Группа в приказе, которой назначен староста
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GroupToExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.GroupToExtractRelation";
    public static final String ENTITY_NAME = "groupToExtractRelation";
    public static final int VERSION_HASH = -767767739;
    private static IEntityMeta ENTITY_META;

    public static final String L_GROUP = "group";
    public static final String L_EXTRACT = "extract";
    public static final String L_PREV_GROUP_MANAGER = "prevGroupManager";

    private Group _group;     // Группа
    private AbstractStudentExtract _extract;     // Выписка (приказ по студентам)
    private Student _prevGroupManager;     // Предыдущий староста

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Выписка (приказ по студентам). Свойство не может быть null.
     */
    @NotNull
    public AbstractStudentExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка (приказ по студентам). Свойство не может быть null.
     */
    public void setExtract(AbstractStudentExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Предыдущий староста.
     */
    public Student getPrevGroupManager()
    {
        return _prevGroupManager;
    }

    /**
     * @param prevGroupManager Предыдущий староста.
     */
    public void setPrevGroupManager(Student prevGroupManager)
    {
        dirty(_prevGroupManager, prevGroupManager);
        _prevGroupManager = prevGroupManager;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof GroupToExtractRelationGen)
        {
            setGroup(((GroupToExtractRelation)another).getGroup());
            setExtract(((GroupToExtractRelation)another).getExtract());
            setPrevGroupManager(((GroupToExtractRelation)another).getPrevGroupManager());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GroupToExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GroupToExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new GroupToExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "group":
                    return obj.getGroup();
                case "extract":
                    return obj.getExtract();
                case "prevGroupManager":
                    return obj.getPrevGroupManager();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "extract":
                    obj.setExtract((AbstractStudentExtract) value);
                    return;
                case "prevGroupManager":
                    obj.setPrevGroupManager((Student) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "group":
                        return true;
                case "extract":
                        return true;
                case "prevGroupManager":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "group":
                    return true;
                case "extract":
                    return true;
                case "prevGroupManager":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "group":
                    return Group.class;
                case "extract":
                    return AbstractStudentExtract.class;
                case "prevGroupManager":
                    return Student.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GroupToExtractRelation> _dslPath = new Path<GroupToExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GroupToExtractRelation");
    }
            

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GroupToExtractRelation#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Выписка (приказ по студентам). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GroupToExtractRelation#getExtract()
     */
    public static AbstractStudentExtract.Path<AbstractStudentExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Предыдущий староста.
     * @see ru.tandemservice.movestudent.entity.GroupToExtractRelation#getPrevGroupManager()
     */
    public static Student.Path<Student> prevGroupManager()
    {
        return _dslPath.prevGroupManager();
    }

    public static class Path<E extends GroupToExtractRelation> extends EntityPath<E>
    {
        private Group.Path<Group> _group;
        private AbstractStudentExtract.Path<AbstractStudentExtract> _extract;
        private Student.Path<Student> _prevGroupManager;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GroupToExtractRelation#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Выписка (приказ по студентам). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GroupToExtractRelation#getExtract()
     */
        public AbstractStudentExtract.Path<AbstractStudentExtract> extract()
        {
            if(_extract == null )
                _extract = new AbstractStudentExtract.Path<AbstractStudentExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Предыдущий староста.
     * @see ru.tandemservice.movestudent.entity.GroupToExtractRelation#getPrevGroupManager()
     */
        public Student.Path<Student> prevGroupManager()
        {
            if(_prevGroupManager == null )
                _prevGroupManager = new Student.Path<Student>(L_PREV_GROUP_MANAGER, this);
            return _prevGroupManager;
        }

        public Class getEntityClass()
        {
            return GroupToExtractRelation.class;
        }

        public String getEntityName()
        {
            return "groupToExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
