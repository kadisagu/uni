/* $Id: extractPrint.vm 6933 2009-03-07 05:41:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e44;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.RepeatProtectDiplomaWorkStuExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 18.02.2011
 */
public class RepeatProtectDiplomaWorkStuExtractPrint implements IPrintFormCreator<RepeatProtectDiplomaWorkStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, RepeatProtectDiplomaWorkStuExtract extract)
    {
        String educationLevel = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getTitle();
        String speciality = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getTitle();
        String extractTitle = extract.getType().getTitle();

        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("extractTitle", extractTitle);
        modifier.put("developCondition_G", CommonExtractPrint.getDevelopConditionStr2_G(extract.getEntity().getEducationOrgUnit().getDevelopCondition()));
        modifier.put("developForm_G", CommonExtractPrint.getDevelopFormStr_G(extract.getEntity().getEducationOrgUnit().getDevelopForm()));
        modifier.put("educationLevel", educationLevel);
        modifier.put("speciality", speciality);

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}
