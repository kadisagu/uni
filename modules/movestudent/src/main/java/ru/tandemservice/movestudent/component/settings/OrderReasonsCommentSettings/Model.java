/* $Id$ */
package ru.tandemservice.movestudent.component.settings.OrderReasonsCommentSettings;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;

/**
 * @author Dmitry Seleznev
 * @since 05.12.2012
 */
public class Model
{
    private DynamicListDataSource<StudentOrderReasons> _dataSource;

    public DynamicListDataSource<StudentOrderReasons> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<StudentOrderReasons> dataSource)
    {
        _dataSource = dataSource;
    }
}