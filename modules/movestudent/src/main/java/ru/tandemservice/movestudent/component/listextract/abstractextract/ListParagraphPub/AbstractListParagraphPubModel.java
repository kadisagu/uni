/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListParagraph;

import java.util.List;

/**
 * @author vip_delete
 * @since 09.04.2009
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "paragraph.id")
})
public abstract class AbstractListParagraphPubModel<T extends ListStudentExtract>
{
    public static final String NO_DELETE = "noDelete";

    private StudentListParagraph _paragraph = new StudentListParagraph();
    private String _formTitle;
    private T _extract;
    private CommonPostfixPermissionModel _secModel;
    private DynamicListDataSource<T> _dataSource;
    private String _searchListSettingsKey;
    private List<T> _extractList;

    public List<T> getExtractList()
    {
        return _extractList;
    }

    public void setExtractList(List<T> extractList)
    {
        _extractList = extractList;
    }

    public StudentListParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(StudentListParagraph paragraph)
    {
        _paragraph = paragraph;
    }

    public String getFormTitle()
    {
        return _formTitle;
    }

    public void setFormTitle(String formTitle)
    {
        _formTitle = formTitle;
    }

    public T getExtract()
    {
        return _extract;
    }

    public void setExtract(T extract)
    {
        _extract = extract;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public DynamicListDataSource<T> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<T> dataSource)
    {
        _dataSource = dataSource;
    }

    public String getSearchListSettingsKey()
    {
        return _searchListSettingsKey;
    }

    public void setSearchListSettingsKey(String searchListSettingsKey)
    {
        _searchListSettingsKey = searchListSettingsKey;
    }
}
