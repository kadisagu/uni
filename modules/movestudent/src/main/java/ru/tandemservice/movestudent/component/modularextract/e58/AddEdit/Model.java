/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e58.AddEdit;

import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.TransferIndGraphStuExtract;
import ru.tandemservice.uni.entity.catalog.Term;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 21.08.2012
 */
public class Model extends CommonModularStudentExtractAddEditModel<TransferIndGraphStuExtract>
{
    private ISelectModel _educationYearsList;
    private IMultiSelectModel _termListModel;
    private List<Term> _selectedTermList = new ArrayList<Term>();

    public ISelectModel getEducationYearsList()
    {
        return _educationYearsList;
    }

    public void setEducationYearsList(ISelectModel educationYearsList)
    {
        _educationYearsList = educationYearsList;
    }

    public IMultiSelectModel getTermListModel()
    {
        return _termListModel;
    }

    public void setTermListModel(IMultiSelectModel termListModel)
    {
        _termListModel = termListModel;
    }

    public List<Term> getSelectedTermList()
    {
        return _selectedTermList;
    }

    public void setSelectedTermList(List<Term> selectedTermList)
    {
        _selectedTermList = selectedTermList;
    }
}
