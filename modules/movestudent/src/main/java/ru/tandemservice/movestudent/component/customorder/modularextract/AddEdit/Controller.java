/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.modularextract.AddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.movestudent.entity.custom.ModifyModularStuExtract;
import ru.tandemservice.movestudent.entity.custom.OrderActionType;

import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 28.08.2013
 */
public class Controller extends CommonModularStudentExtractAddEditController<ModifyModularStuExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        final Model model = this.getModel(component);
        model.setContext(component);
        activateCustomComponents(component);
    }

    public void onChangeActionType(IBusinessComponent component)
    {
        activateCustomComponents(component);
    }

    private void activateCustomComponents(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (OrderActionType.DO_CUSTOM_ACTION == model.getExtract().getOrderActionType())
        {
            for (Map.Entry<String, String> entry : model.getPossibleActionComponents().entrySet())
            {
                if (component.getChildRegion(entry.getKey()) == null)
                {
                    component.createChildRegion(entry.getKey(), new ComponentActivator(
                            entry.getValue() + ".AddEdit", new ParametersMap()
                            .add(BaseCustomActionAddEditModel.EXTRACT_ID_BIND, model.getExtract().getId())
                            .add(BaseCustomActionAddEditModel.STUDENT_ID_BIND, model.getExtract().getId() == null ? model.getStudentId() : model.getExtract().getEntity().getId())
                    ));
                }
            }
        }
    }
}