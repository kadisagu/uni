/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e47.ParagraphAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract;
import org.tandemframework.hibsupport.builder.MQBuilder;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationRoot;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;
import ru.tandemservice.uniedu.entity.EduProgramSubjectComparison;
import ru.tandemservice.unimove.IAbstractExtract;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ListExtractComponentGenerator
 * @since 17.01.2017
 */
public class DAO extends AbstractListParagraphAddEditDAO<TransferProgramSubjectStuListExtract, Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setGroupListModel(new GroupSelectModel(model, model, null, model.getFormativeOrgUnit())
        {
            @Override
            protected boolean isNeedRequest()
            {
                if (getGroupModel().getCourse() == null) return false;
                if (getEduLevelModel().getDevelopForm() == null) return false;
                return getOrgUnit() != null || getEduLevelModel().getFormativeOrgUnit() != null;
            }

            protected DQLSelectBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, G_ALIAS).column(G_ALIAS);

                if (o != null)
                {
                    if (o instanceof Long) builder.where(eq(property(G_ALIAS + ".id"), value((Long) o)));
                    else if (o instanceof Collection) builder.where(in(property(G_ALIAS + ".id"), (Collection) o));
                }

                builder.where(eq(property(G_ALIAS, Group.educationOrgUnit().formativeOrgUnit()), value(getEduLevelModel().getFormativeOrgUnit())));
                builder.where(eq(property(G_ALIAS, Group.course()), value(getGroupModel().getCourse())));
                builder.where(eq(property(G_ALIAS, Group.educationOrgUnit().developForm()), value(getEduLevelModel().getDevelopForm())));
                builder.where(eq(property(G_ALIAS, Group.archival()), value(Boolean.FALSE)));
                builder.where(exists(new DQLSelectBuilder().fromEntity(Student.class, "st")
                                             .where(eq(property("st", Student.group()), property(G_ALIAS)))
                                             .joinPath(DQLJoinType.inner, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject().fromAlias("st"), "eps")
                                             .where(notInstanceOf("eps", EduProgramSubject2013.class)).buildQuery()));

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property(G_ALIAS, Group.P_TITLE), value(CoreStringUtils.escapeLike(filter, true))));

                builder.order(property(G_ALIAS, Group.P_TITLE), OrderDirection.asc);

                return builder;
            }
        });
        model.setDevelopFormModel(EducationOrgUnitUtil.getDevelopFormSelectModel(model, false));
        model.setEduOUModel(new FullCheckSelectModel(EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+"."+ EducationLevelsHighSchool.P_FULL_TITLE_EXTENDED)
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getGroup() == null)
                    return ListResult.getEmpty();

                EducationLevels eduLevelGroup = model.getGroup().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "eou")
                        .column(property("eou"))
                        .where(eq(property("eou", EducationOrgUnit.developForm()), value(model.getDevelopForm())))  // ФУТС
                        .where(eq(property("eou", EducationOrgUnit.developCondition()), value(model.getDevelopCondition())))
                        .where(eq(property("eou", EducationOrgUnit.developTech()), value(model.getDevelopTech())))
                        .where(eq(property("eou", EducationOrgUnit.developPeriod()), value(model.getDevelopPeriod())))
                        // вид ОП
                        .where(eq(property("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject().subjectIndex().programKind()),
                                  value(eduLevelGroup.getEduProgramSubject().getSubjectIndex().getProgramKind())))
                        // формирующее
                        .where(eq(property("eou", EducationOrgUnit.formativeOrgUnit()), value(model.getGroup().getEducationOrgUnit().getFormativeOrgUnit())))
                        //территориальное
                        .where(eq(property("eou", EducationOrgUnit.territorialOrgUnit()), value(model.getGroup().getEducationOrgUnit().getTerritorialOrgUnit())))
                        //выпускающее
                        .where(eq(property("eou", EducationOrgUnit.educationLevelHighSchool().orgUnit()), value(model.getGroup().getEducationOrgUnit().getEducationLevelHighSchool().getOrgUnit())))
                        //Направление подготовки из перечня 2013
                        .joinPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject().fromAlias("eou"), "eps")
                        .where(instanceOf("eps", EduProgramSubject2013.class));

                //Если в настройке указано сопоставление старого и нового НП Проф, то учитываем настройку
                List<Long> ids = getPropertiesList(EduProgramSubjectComparison.class, EduProgramSubjectComparison.oldProgramSubject(), eduLevelGroup.getEduProgramSubject(), false, EduProgramSubjectComparison.newProgramSubject().id());
                if (!ids.isEmpty())
                    builder.where(in(property("eps.id"), ids));

                // специализация - общая либо названия специализаций должны совпадать
                if (eduLevelGroup.getEduProgramSpecialization() == null)
                    builder.where(isNull(property("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSpecialization())));
                else
                {
                    builder.where(isNotNull(property("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSpecialization())))
                            .joinPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSpecialization().fromAlias("eou"), "espec");
                    if (eduLevelGroup.getEduProgramSpecialization().isRootSpecialization())
                        builder.where(instanceOf("espec", EduProgramSpecializationRoot.class));
                    else
                        builder.where(eq(property("espec", EduProgramSpecialization.title()), value(eduLevelGroup.getEduProgramSpecialization().getTitle())));
                }

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject().title()), value(CoreStringUtils.escapeLike(filter, true))));

                return new ListResult<>(builder.createStatement(getSession()).list());
            }
        });

        model.setOldProgramSubjectModel(new FullCheckSelectModel(EduProgramSubject.P_TITLE_WITH_CODE_INDEX_AND_GEN)
        {
            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "s")
                        .where(eq(property("s", Student.group()), value(model.getGroup())))
                        .where(eq(property("s", Student.course()), value(model.getCourse())))
                        .where(eq(property("s", Student.archival()), value(false)))
                        .where(eq(property("s", Student.educationOrgUnit().developForm()), value(model.getDevelopForm())))
                        .joinPath(DQLJoinType.inner, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject().fromAlias("s"), "eps")
                        .where(notInstanceOf("eps", EduProgramSubject2013.class))
                        .column(property("eps"))
                        .distinct();

                return new ListResult(builder.createStatement(getSession()).list());
            }

        });


        model.setNewProgramSubjectModel(new FullCheckSelectModel(EduProgramSubject.P_TITLE_WITH_CODE_INDEX_AND_GEN)
        {
            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EduProgramSubjectComparison.class, "c")
                        .column(property("c", EduProgramSubjectComparison.newProgramSubject()))
                        .where(eq(property("c", EduProgramSubjectComparison.oldProgramSubject()), value(model.getOldProgramSubject())));
                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property("c", EduProgramSubjectComparison.newProgramSubject().title()), value(CoreStringUtils.escapeLike(filter))));

                return new ListResult(builder.createStatement(getSession()).list());
            }
        });

        model.setStudentEduOUModel(new FullCheckSelectModel(EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+"."+ EducationLevelsHighSchool.P_FULL_TITLE_EXTENDED)
        {
            @Override
            public ListResult findValues(String filter)
            {
                EducationOrgUnit studentOrgUnit = (EducationOrgUnit) model.getDataSource().getCurrentValueEntity().getProperty(Student.educationOrgUnit());
                if (studentOrgUnit == null)
                    ListResult.getEmpty();

                assert studentOrgUnit != null;
                EducationLevels educationLevel = studentOrgUnit.getEducationLevelHighSchool().getEducationLevel();

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "eou")
                        .column(property("eou"))
                        .where(eq(property("eou", EducationOrgUnit.developForm()), value(studentOrgUnit.getDevelopForm())))  // ФУТС
                        .where(eq(property("eou", EducationOrgUnit.developCondition()), value(studentOrgUnit.getDevelopCondition())))
                        .where(eq(property("eou", EducationOrgUnit.developTech()), value(studentOrgUnit.getDevelopTech())))
                        .where(eq(property("eou", EducationOrgUnit.developPeriod()), value(studentOrgUnit.getDevelopPeriod())))
                        // НП Проф новое
                        .where(eq(property("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject()),
                                  value(model.getNewProgramSubject())))
                        // формирующее
                        .where(eq(property("eou", EducationOrgUnit.formativeOrgUnit()), value(studentOrgUnit.getFormativeOrgUnit())))
                        //территориальное
                        .where(eq(property("eou", EducationOrgUnit.territorialOrgUnit()), value(studentOrgUnit.getTerritorialOrgUnit())))
                        //выпускающее
                        .where(eq(property("eou", EducationOrgUnit.educationLevelHighSchool().orgUnit()), value(studentOrgUnit.getEducationLevelHighSchool().getOrgUnit())));

                // специализация - общая либо названия специализаций должны совпадать
                if (educationLevel.getEduProgramSpecialization() == null)
                    builder.where(isNull(property("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSpecialization())));
                else
                {
                    builder.where(isNotNull(property("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSpecialization())))
                            .joinPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSpecialization().fromAlias("eou"), "espec");
                    if (educationLevel.getEduProgramSpecialization().isRootSpecialization())
                        builder.where(instanceOf("espec", EduProgramSpecializationRoot.class));
                    else
                        builder.where(eq(property("espec", EduProgramSpecialization.title()), value(educationLevel.getEduProgramSpecialization().getTitle())));
                }

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject().title()), value(CoreStringUtils.escapeLike(filter, true))));

                return new ListResult(builder.createStatement(getSession()).list());

            }
        });
        model.setChangeGroupEduOU(true);


        TransferProgramSubjectStuListExtract firstExtract = model.getFirstExtract();
        if (null != firstExtract)

        {
            model.setGroup(firstExtract.getEntity().getGroup());
            model.setCourse(firstExtract.getEntity().getGroup().getCourse());
            model.setDevelopForm(firstExtract.getEntity().getGroup().getEducationOrgUnit().getDevelopForm());
            model.setOldProgramSubject(firstExtract.getEduProgramSubjectOld());
            model.setNewProgramSubject(firstExtract.getEduProgramSubjectNew());
            if (firstExtract.isChangeGroupEducationOU())
            {
                model.setChangeGroupEduOU(firstExtract.isChangeGroupEducationOU());
                model.setEduOUGroupNew(firstExtract.getGroupEducationOUNew());

            }
        }

    }


    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.course().s(), model.getCourse()));
        builder.add(MQExpression.in(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developForm().s(), model.getDevelopForm()));

        builder.addOrder(STUDENT_ALIAS, Student.person().identityCard().fullFio().s());
    }

    @Override
    protected TransferProgramSubjectStuListExtract createNewInstance(Model model)
    {
        return new TransferProgramSubjectStuListExtract();
    }

    @Override
    protected void fillExtract(TransferProgramSubjectStuListExtract extract, Student student, Model model)
    {
        extract.setChangeGroupEducationOU(model.isChangeGroupEduOU());
        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setEducationOrgUnitOld(student.getEducationOrgUnit());
        extract.setEduProgramSubjectOld(model.getOldProgramSubject());
        extract.setEduProgramSubjectNew(model.getNewProgramSubject());
        if (model.isChangeGroupEduOU())
        {
            extract.setGroupEducationOUNew(model.getEduOUGroupNew());
        }

        ErrorCollector errs = UserContext.getInstance().getErrorCollector();

        final IValueMapHolder educationOrhUnitValueHolder = (IValueMapHolder) model.getDataSource().getColumn("newStudentEOU");
        final Map<Long, EducationOrgUnit> educationOUValueMap = (null == educationOrhUnitValueHolder ? Collections.emptyMap() : educationOrhUnitValueHolder.getValueMap());
        if (educationOUValueMap.get(student.getId()) == null)
            errs.add("Поле «Новое направление подготовки» у выбранных студентов обязательно для заполнения.", "currentEOUId_" + student.getId());
        else
        {
            extract.setEducationOrgUnitNew(educationOUValueMap.get(student.getId()));
        }

    }

    @Override
    protected void setAdditionalViewProperties(Model model, DynamicListDataSource<Student> dataSource, List<Student> studentList, boolean needCustomStates)
    {
        super.setAdditionalViewProperties(model, dataSource, studentList, needCustomStates);
        // дизаблим чекбоксы у студентов, направление подготовки которых не совпадает с выбранным
        ViewWrapper.getPatchedList(dataSource).stream()
                .filter(e->!((Student)e.getEntity()).getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().equals(model.getOldProgramSubject()))
                .forEach(e->e.setViewProperty(AbstractListParagraphAddEditModel.P_STUDENT_DISABLED, true));


    }

    @Override
    public void prepareColumns(Model model)
    {
        if (model.getParagraphId() != null)
        {
            Map<Long, EducationOrgUnit> newEducationOrgUnitMap = new HashMap<>();
            List<TransferProgramSubjectStuListExtract> extractList = getList(TransferProgramSubjectStuListExtract.class, IAbstractExtract.L_PARAGRAPH, model.getParagraph());

            for (TransferProgramSubjectStuListExtract ext : extractList)
            {
                newEducationOrgUnitMap.put(ext.getEntity().getId(), ext.getEducationOrgUnitNew());
            }
            ((BlockColumn) model.getDataSource().getColumn("newStudentEOU")).setValueMap(newEducationOrgUnitMap);
        }
    }
}