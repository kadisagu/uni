/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e87.AddEdit;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.GrantRiseStuExtract;

import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 20.11.2012
 */
public class Model extends CommonModularStudentExtractAddEditModel<GrantRiseStuExtract>
{
    private Integer _year;
    private IdentifiableWrapper _month;
    private List<IdentifiableWrapper> _monthList;

    public Integer getYear()
    {
        return _year;
    }

    public void setYear(Integer year)
    {
        _year = year;
    }

    public IdentifiableWrapper getMonth()
    {
        return _month;
    }

    public void setMonth(IdentifiableWrapper month)
    {
        _month = month;
    }

    public List<IdentifiableWrapper> getMonthList()
    {
        return _monthList;
    }

    public void setMonthList(List<IdentifiableWrapper> monthList)
    {
        _monthList = monthList;
    }
}