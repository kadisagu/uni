/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e29;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.e29.utils.GiveDiplExtParagraphPartWrapper;
import ru.tandemservice.movestudent.component.listextract.e29.utils.GiveDiplExtParagraphWrapper;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 19.09.2012
 */
public class GiveDiplExtStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order);

        AbstractStudentExtract listOrderExtract = MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(order.getId());
        EducationLevelsHighSchool edu;
        if (listOrderExtract instanceof GiveDiplExtStuListExtract)
        {
            edu = ((GiveDiplExtStuListExtract) listOrderExtract).getEducationLevelsHighSchool();
        }
        else if (listOrderExtract instanceof GiveDiplSuccessExtStuListExtract)
        {
            edu = ((GiveDiplSuccessExtStuListExtract) listOrderExtract).getEducationLevelsHighSchool();
        }
        else
        {
            throw new IllegalStateException();
        }

        switch (edu.getEducationLevel().getSafeQCode())
        {
            case QualificationsCodes.BAKALAVR:
            case QualificationsCodes.MAGISTR:
                injectModifier.put("graduate_G", "степени");
                break;
            case QualificationsCodes.BAZOVYY_UROVEN_S_P_O:
            case QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O:
            case QualificationsCodes.SPETSIALIST:
                injectModifier.put("graduate_G", "квалификации");
                break;
            default:
                injectModifier.put("graduate_G", "__________");
                break;
        }

        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, prepareParagraphsStructure(order));

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }

    protected List<GiveDiplExtParagraphWrapper> prepareParagraphsStructure(StudentListOrder order)
    {
        List<GiveDiplExtParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        int ind;

        final List<GiveDiplExtStuListExtract> woSuccessExtract = new ArrayList<>();
        final List<GiveDiplSuccessExtStuListExtract> withSuccessExtract = new ArrayList<>();
        MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, false).forEach(e -> {
            if (e instanceof GiveDiplExtStuListExtract) {
                woSuccessExtract.add((GiveDiplExtStuListExtract) e);
            } else if (e instanceof GiveDiplSuccessExtStuListExtract){
                withSuccessExtract.add((GiveDiplSuccessExtStuListExtract) e);
            } else {
                throw new IllegalStateException();
            }
        });

        for (GiveDiplExtStuListExtract extract : woSuccessExtract)
        {
            // выписка приказа "О выпуске (диплом)"
            Student student = extract.getEntity();
            Person person = student.getPerson();

            final EducationLevels educationLevels = EducationOrgUnitUtil.getParentLevel(extract.getEducationLevelsHighSchool());

            // todo DEV-7133

            GiveDiplExtParagraphWrapper paragraphWrapper = new GiveDiplExtParagraphWrapper(student.getCompensationType(), extract.getCourse(), extract.getDevelopForm(), extract.getDevelopCondition(), extract.getDevelopTech(), extract.getDevelopPeriod(), educationLevels, extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit(), extract.getEntity().getEducationOrgUnit().getTerritorialOrgUnit(), CommonListOrderPrint.getEducationBaseText(student.getGroup()), extract.isPrintDiplomaQualification(), extract.getEducationLevelsHighSchool().getAssignedQualification(), extract);
            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else
                paragraphWrapper = paragraphWrapperList.get(ind);

            GiveDiplExtParagraphPartWrapper paragraphPartWrapper;
            StructureEducationLevels levels = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
            paragraphPartWrapper = new GiveDiplExtParagraphPartWrapper(levels != null && (levels.isSpecialization() || levels.isProfile()) ? student.getEducationOrgUnit().getEducationLevelHighSchool() : null, extract);

            ind = paragraphWrapper.getParagraphPartWrapperList().indexOf(paragraphPartWrapper);
            if (ind == -1)
            {
                paragraphWrapper.getParagraphPartWrapperList().add(paragraphPartWrapper);
                paragraphPartWrapper.setParagraphWrapper(paragraphWrapper);
            }
            else paragraphPartWrapper = paragraphWrapper.getParagraphPartWrapperList().get(ind);
            paragraphPartWrapper.getPersonList().add(person);
        }

        for (GiveDiplSuccessExtStuListExtract extract : withSuccessExtract)
        {
            // выписка приказа "О выпуске (диплом с отличием)"
            Student student = extract.getEntity();
            Person person = student.getPerson();

            EducationLevels educationLevels = EducationOrgUnitUtil.getParentLevel(extract.getEducationLevelsHighSchool());

            GiveDiplExtParagraphWrapper paragraphWrapper = new GiveDiplExtParagraphWrapper(student.getCompensationType(), extract.getCourse(), extract.getDevelopForm(), extract.getDevelopCondition(), extract.getDevelopTech(), extract.getDevelopPeriod(), educationLevels, extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit(), extract.getEntity().getEducationOrgUnit().getTerritorialOrgUnit(), CommonListOrderPrint.getEducationBaseText(student.getGroup()), extract.isPrintDiplomaQualification(), extract.getEducationLevelsHighSchool().getAssignedQualification(), extract);
            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else paragraphWrapper = paragraphWrapperList.get(ind);

            GiveDiplExtParagraphPartWrapper paragraphPartSuccessWrapper;
            StructureEducationLevels levels = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
            paragraphPartSuccessWrapper = new GiveDiplExtParagraphPartWrapper(levels != null && (levels.isSpecialization() || levels.isProfile()) ? student.getEducationOrgUnit().getEducationLevelHighSchool() : null, extract);

            ind = paragraphWrapper.getParagraphPartSuccessWrapperList().indexOf(paragraphPartSuccessWrapper);
            if (ind == -1)
            {
                paragraphWrapper.getParagraphPartSuccessWrapperList().add(paragraphPartSuccessWrapper);
                paragraphPartSuccessWrapper.setParagraphWrapper(paragraphWrapper);
            }
            else paragraphPartSuccessWrapper = paragraphWrapper.getParagraphPartSuccessWrapperList().get(ind);
            paragraphPartSuccessWrapper.getPersonList().add(person);
        }

        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<GiveDiplExtParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            int parNumber = 0;
            for (GiveDiplExtParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.GIVE_GENERAL_DIPLOMA_VARIANT_2_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphWrapper.getFirstExtract();
                RtfInjectModifier paragraphInjectModifier = new RtfInjectModifier();

                CommonExtractPrint.initDevelopForm(paragraphInjectModifier, paragraphWrapper.getDevelopForm(), "");
                CommonListExtractPrint.injectCompensationType(paragraphInjectModifier, paragraphWrapper.getCompensationType(), "", false);

                paragraphInjectModifier.put("highSchoolShortTitle",
                                            paragraphWrapper.getTerritorialOrgUnit().getOrgUnitType().isRootType() ? " " + TopOrgUnit.getInstance().getShortTitle() : "");

                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, paragraphWrapper.getEducationLevels(), paragraphWrapper.getFormativeOrgUnit(), paragraphWrapper.getTerritorialOrgUnit(), "formativeOrgUnitStr", "");

                paragraphInjectModifier.put("course", paragraphWrapper.getCourse().getTitle());

                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, paragraphWrapper.getEducationLevels(), new String[]{"fefuEducationStrDirection", "educationStrDirection"}, false);

                EducationLevels educationLevels = paragraphWrapper.getEducationLevels();
                String qCode = educationLevels.getSafeQCode();
                final EduProgramQualification qualification = paragraphWrapper.getQualification();
                final String assignedQualificationTitle = qualification != null ? qualification.getTitle().toUpperCase() : "______________________________";
                StringBuilder diplomaQualificationAward = new StringBuilder();
                boolean isGos2 = paragraphWrapper.getEducationLevels().getLevelType().isGos2();

                switch (qCode)
                {
                    case QualificationsCodes.BAKALAVR:
                    case QualificationsCodes.MAGISTR:
                        paragraphInjectModifier.put("graduateWithLevel_A", "степень " + assignedQualificationTitle);

                        if (paragraphWrapper.isPrintDiplomaQualification() && !isGos2)
                        {
                            diplomaQualificationAward.append(", присвоить специальное звание ");
                            diplomaQualificationAward.append(assignedQualificationTitle);
                        }
                        break;
                    case QualificationsCodes.BAZOVYY_UROVEN_S_P_O:
                    case QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O:
                    case QualificationsCodes.SPETSIALIST:
                        paragraphInjectModifier.put("graduateWithLevel_A", "квалификацию " + assignedQualificationTitle);
                        break;
                    default:
                        paragraphInjectModifier.put("graduateWithLevel_A", "______________________________");
                        break;
                }

                paragraphInjectModifier.put("parNumber", String.valueOf(++parNumber));
                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");
                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, paragraphWrapper.getDevelopCondition(), paragraphWrapper.getDevelopTech(), paragraphWrapper.getDevelopPeriod(), paragraphWrapper.getEduBaseText(), "fefuShortFastExtendedOptionalText");
                paragraphInjectModifier.put("diplomaQualificationAward", diplomaQualificationAward.toString());
                customModifier.listParagraphModifier(paragraphInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphInjectModifier.modify(paragraph);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getParagraphPartWrapperList(), paragraphWrapper.getParagraphPartSuccessWrapperList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraph, List<GiveDiplExtParagraphPartWrapper> paragraphPartWrappers,
                                       List<GiveDiplExtParagraphPartWrapper> paragraphPartSuccessWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartWrappers);
            Collections.sort(paragraphPartSuccessWrappers);

            List<IRtfElement> parList = new ArrayList<>();
            int i = 1;
            int subParCount = paragraphPartSuccessWrappers.size() + paragraphPartWrappers.size();

            // Подпараграфы "О выпуске (диплом с отличием)"
            for (GiveDiplExtParagraphPartWrapper paragraphPartSuccessWrapper : paragraphPartSuccessWrappers)
            {
                // Получаем шаблон подпараграфа
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.GIVE_DIPLOMA_WITH_HONOURS_VARIANT_2_LIST_EXTRACT), 3);
                RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphPartSuccessWrapper.getFirstExtract();
                RtfInjectModifier paragraphPartInjectModifier = new RtfInjectModifier();

                EducationLevelsHighSchool educationLevelsHighSchool = paragraphPartSuccessWrapper.getEduLevelsHighSchool();
                modifyEducationStrProfile(paragraphPartInjectModifier, educationLevelsHighSchool);

                EducationLevels educationLevels = educationLevelsHighSchool == null ? paragraphPartSuccessWrapper.getParagraphWrapper().getEducationLevels() : educationLevelsHighSchool.getEducationLevel();
                paragraphPartInjectModifier.put("eduLevelTypeStr_P", CommonExtractPrint.getEduLevelTypeStr_P(educationLevels));

                paragraphPartInjectModifier.put("subParNumber", subParCount > 1 ? (i++ + ". ") : "");

                List<Person> personList = paragraphPartSuccessWrapper.getPersonList();
                personList.sort(Person.FULL_FIO_AND_ID_COMPARATOR);
                RtfString rtfString = new RtfString();

                int j = 0;

                for (; j < personList.size() - 1; j++)
                {
                    rtfString.append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio()).par();
                }

                rtfString.append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio());

                if ((i == paragraphPartSuccessWrappers.size() + 1) && (i <= subParCount))
                {
                    // Если потом пойдут еще подпараграфы о простой выдаче диплома, надо вставить пустую строку
                    rtfString.par();
                }

                paragraphPartInjectModifier.put("STUDENT_LIST", rtfString);
                customModifier.listParagraphModifier(paragraphPartInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphPartInjectModifier.modify(paragraphPart);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraphPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // Подпараграфы "О выпуске (диплом)"
            for (GiveDiplExtParagraphPartWrapper paragraphPartWrapper : paragraphPartWrappers)
            {
                // Получаем шаблон подпараграфа
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.GIVE_GENERAL_DIPLOMA_VARIANT_2_LIST_EXTRACT), 3);
                RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphPartWrapper.getFirstExtract();
                RtfInjectModifier paragraphPartInjectModifier = new RtfInjectModifier();

                EducationLevelsHighSchool educationLevelsHighSchool = paragraphPartWrapper.getEduLevelsHighSchool();
                modifyEducationStrProfile(paragraphPartInjectModifier, educationLevelsHighSchool);


                EducationLevels educationLevels = educationLevelsHighSchool == null ? paragraphPartWrapper.getParagraphWrapper().getEducationLevels() : educationLevelsHighSchool.getEducationLevel();
                paragraphPartInjectModifier.put("eduLevelTypeStr_P", CommonExtractPrint.getEduLevelTypeStr_P(educationLevels));

                paragraphPartInjectModifier.put("subParNumber", subParCount > 1 ? (i++ + ". ") : "");

                List<Person> personList = paragraphPartWrapper.getPersonList();
                personList.sort(Person.FULL_FIO_AND_ID_COMPARATOR);
                RtfString rtfString = new RtfString();

                int j = 0;

                for (; j < personList.size() - 1; j++)
                {
                    rtfString.append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio()).par();
                }

                rtfString.append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio());

                paragraphPartInjectModifier.put("STUDENT_LIST", rtfString);
                customModifier.listParagraphModifier(paragraphPartInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphPartInjectModifier.modify(paragraphPart);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraphPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void modifyEducationStrProfile(RtfInjectModifier modifier, EducationLevelsHighSchool educationLevelsHighSchool)
    {
        modifier.put("fefuEducationStrProfile", "");
        modifier.put("educationStrProfile", "");
        if (educationLevelsHighSchool == null)
            return;

        StructureEducationLevels levelType = educationLevelsHighSchool.getEducationLevel().getLevelType();
        String[] eduTypeCases;

        if (levelType.isSpecialization()) // специализация
            eduTypeCases = CommonExtractPrint.SPECIALIZATION_CASES;
        else if (levelType.isBachelor() && levelType.isProfile()) // бакалаврский профиль
            eduTypeCases = CommonExtractPrint.PROFILE_CASES;
        else if (levelType.isMaster() && levelType.isProfile()) // магистерский профиль
            eduTypeCases = CommonExtractPrint.MASTER_PROGRAM_CASES;
        else
            return;

        String profile = " «" + educationLevelsHighSchool.getTitle() + "»";
        for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
        {
            String educationStrProfile = StringUtils.capitalize(eduTypeCases[i]) + profile;
            String postfix = UniRtfUtil.CASE_POSTFIX.get(i);
            modifier.put("fefuEducationStrProfile" + postfix, educationStrProfile);
            modifier.put("educationStrProfile" + postfix, educationStrProfile);
        }
    }
}