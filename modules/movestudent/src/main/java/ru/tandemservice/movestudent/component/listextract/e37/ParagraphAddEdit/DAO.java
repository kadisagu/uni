/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e37.ParagraphAddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDao;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.EducationLevelsHighSchoolSelectModel;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ListExtractComponentGenerator
 * @since 11.12.2012
 */
public class DAO extends AbstractListParagraphAddEditDAO<TerritorialCourseTransferStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));

        model.setCourseOldListModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getGroup() == null) return ListResult.getEmpty();

                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder
                        .fromEntity(Student.class, "s")
                        .column(property(Student.course().id().fromAlias("s")))
                        .where(eq(property(Student.group().fromAlias("s")), value(model.getGroup())))
                        .group(property(Student.course().id().fromAlias("s")));

                DQLSelectBuilder resultBuilder = new DQLSelectBuilder();
                resultBuilder
                        .fromEntity(Course.class, "c")
                        .where(in(property(Course.id().fromAlias("c")), builder.buildQuery()));

                return new ListResult<>(resultBuilder.createStatement(getSession()).<Course> list());
            }
        });

        model.setCourseNewListModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getCourseOld() == null) return ListResult.getEmpty();

                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder
                        .fromEntity(Course.class, "c")
                        .where(gt(property(Course.intValue().fromAlias("c")), value(model.getCourseOld().getIntValue())));

                return new ListResult<>(builder.createStatement(getSession()).list());
            }
        });

        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));

        model.setEducationLevelsHighSchoolModel(new EducationLevelsHighSchoolSelectModel(model)
        {
            @Override
            protected boolean isNeedRequest()
            {
                return model.getFormativeOrgUnit() != null && model.getTerritorialOrgUnit() != null;
            }
        });

        model.setDevelopFormModel(EducationOrgUnitUtil.getDevelopFormSelectModel(model));
        model.setDevelopConditionModel(EducationOrgUnitUtil.getDevelopConditionSelectModel(model));
        model.setDevelopTechModel(EducationOrgUnitUtil.getDevelopTechSelectModel(model));
        model.setDevelopPeriodModel(EducationOrgUnitUtil.getDevelopPeriodSelectModel(model));

        model.setGroupNewListModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                return findValues(filter, true);
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                IEntity entity = UniDaoFacade.getCoreDao().get((Long) primaryKey);
                if (findValues("", false).getObjects().contains(entity))
                {
                    return entity;
                }

                return null;
            }

            public ListResult findValues(String filter, boolean simple)
            {
                if (model.getCourseNew() == null || model.getGroup() == null) return ListResult.getEmpty();

                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder
                        .fromEntity(Group.class, "gn")
                        .column("gn")
                        .where(eq(property(Group.course().fromAlias("gn")), value(model.getCourseNew())))
                        .where(eq(property(Group.archival().fromAlias("gn")), value(Boolean.FALSE)))
                        .where(eq(property(Group.educationOrgUnit().formativeOrgUnit().fromAlias("gn")), value(model.getFormativeOrgUnit())))
                        .where(eq(property(Group.educationOrgUnit().territorialOrgUnit().fromAlias("gn")), value(model.getTerritorialOrgUnit())));

                builder.where(like(DQLFunctions.upper(property(Group.title().fromAlias("gn"))), value(CoreStringUtils.escapeLike(filter))));
                MoveStudentDaoFacade.getMoveStudentDao().addCustomConditionToGroupSelectBuilder(builder, "gn");
                if (!simple){return new ListResult<>(builder.createStatement(getSession()).list());}

                int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                if (count > MoveStudentDao.MAX_ROWS)
                    builder.top(MoveStudentDao.MAX_ROWS);

                return new ListResult<>(builder.createStatement(getSession()).list(), count);
            }
        });

        // заполняем поля сохраненными значениями
        if (model.getParagraphId() != null)
        {
            TerritorialCourseTransferStuListExtract extract = model.getFirstExtract();
            model.setTransferDate(extract.getTransferDate());
            model.setCourse(extract.getCourse());
            model.setGroup(extract.getGroup());
            model.setCompensationType(extract.getCompensationType());
            model.setCourseOld(extract.getCourseOld());
            model.setCourseNew(extract.getCourseNew());

            EducationOrgUnit educationOrgUnit = extract.getEducationOrgUnitNew();
            model.setFormativeOrgUnit(educationOrgUnit.getFormativeOrgUnit());
            model.setTerritorialOrgUnit(educationOrgUnit.getTerritorialOrgUnit());
            model.setEducationLevelsHighSchool(educationOrgUnit.getEducationLevelHighSchool());
            model.setDevelopForm(educationOrgUnit.getDevelopForm());
            model.setDevelopCondition(educationOrgUnit.getDevelopCondition());
            model.setDevelopTech(educationOrgUnit.getDevelopTech());
            model.setDevelopPeriod(educationOrgUnit.getDevelopPeriod());

            model.setGroupNew(extract.getGroupNew());
        }
    }


    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP,  model.getGroup()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COURSE, model.getCourseOld()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));
    }

    @Override
    protected TerritorialCourseTransferStuListExtract createNewInstance(Model model)
    {
        return new TerritorialCourseTransferStuListExtract();
    }

    @Override
    protected void fillExtract(TerritorialCourseTransferStuListExtract extract, Student student, Model model)
    {
        extract.setTransferDate(model.getTransferDate());
        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setCompensationType(model.getCompensationType());
        extract.setCourseOld(model.getCourseOld());
        extract.setCourseNew(model.getCourseNew());
        extract.setEducationOrgUnitNew(model.getEducationOrgUnit());
        extract.setEducationOrgUnitOld(student.getEducationOrgUnit());
        extract.setGroupNew(model.getGroupNew());
    }

    @Override
    public void update(final Model model)
    {
        EducationOrgUnit educationOrgUnit = new DQLSelectBuilder()
                .fromEntity(EducationOrgUnit.class, "eou")
                .where(eq(property(EducationOrgUnit.formativeOrgUnit().fromAlias("eou")), value(model.getFormativeOrgUnit())))
                .where(eq(property(EducationOrgUnit.territorialOrgUnit().fromAlias("eou")), value(model.getTerritorialOrgUnit())))
                .where(eq(property(EducationOrgUnit.educationLevelHighSchool().fromAlias("eou")), value(model.getEducationLevelsHighSchool())))
                .where(eq(property(EducationOrgUnit.developForm().fromAlias("eou")), value(model.getDevelopForm())))
                .where(eq(property(EducationOrgUnit.developCondition().fromAlias("eou")), value(model.getDevelopCondition())))
                .where(eq(property(EducationOrgUnit.developTech().fromAlias("eou")), value(model.getDevelopTech())))
                .where(eq(property(EducationOrgUnit.developPeriod().fromAlias("eou")), value(model.getDevelopPeriod())))
                .createStatement(getSession())
                .uniqueResult();

        model.setEducationOrgUnit(educationOrgUnit);
        super.update(model);
    }
}