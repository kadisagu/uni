package ru.tandemservice.movestudent.entity.custom.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.fias.base.entity.ICitizenship;
import org.tandemframework.shared.fias.base.entity.gen.ICitizenshipGen;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Nationality;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction;
import ru.tandemservice.unimove.entity.ICustomOrderAction;
import ru.tandemservice.unimove.entity.ICustomOrderExtract;
import ru.tandemservice.unimove.entity.gen.ICustomOrderExtractGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Смена удостоверения личности
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChangeIdentityCardActionGen extends EntityBase
 implements ICustomOrderAction{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction";
    public static final String ENTITY_NAME = "changeIdentityCardAction";
    public static final int VERSION_HASH = 780253707;
    private static IEntityMeta ENTITY_META;

    public static final String P_CHANGE_FIO_DATE = "changeFioDate";
    public static final String P_FIRST_NAME_NEW = "firstNameNew";
    public static final String P_LAST_NAME_NEW = "lastNameNew";
    public static final String P_MIDDLE_NAME_NEW = "middleNameNew";
    public static final String P_CARD_SERIA = "cardSeria";
    public static final String P_CARD_NUMBER = "cardNumber";
    public static final String P_BIRTH_DATE = "birthDate";
    public static final String P_BIRTH_PLACE = "birthPlace";
    public static final String P_ISSUANCE_DATE = "issuanceDate";
    public static final String P_ISSUANCE_PLACE = "issuancePlace";
    public static final String L_NATIONALITY = "nationality";
    public static final String L_SEX = "sex";
    public static final String L_CITIZENSHIP = "citizenship";
    public static final String L_CARD_TYPE = "cardType";
    public static final String L_LAST_ACTIVE_IDENTITY_CARD = "lastActiveIdentityCard";
    public static final String L_IDENTITY_CARD_NEW = "identityCardNew";
    public static final String L_CUSTOM_EXTRACT = "customExtract";

    private Date _changeFioDate;     // Дата смены фамилии
    private String _firstNameNew;     // Новое имя
    private String _lastNameNew;     // Новая фамилия
    private String _middleNameNew;     // Новое отчество
    private String _cardSeria;     // Серия
    private String _cardNumber;     // Номер
    private Date _birthDate;     // Дата рождения
    private String _birthPlace;     // Место рождения
    private Date _issuanceDate;     // Дата выдачи удостоверения
    private String _issuancePlace;     // Кем выдано удостоверение
    private Nationality _nationality;     // Национальные группы
    private Sex _sex;     // Пол
    private ICitizenship _citizenship;     // Гражданство
    private IdentityCardType _cardType;     // Тип удостоверения личности
    private IdentityCard _lastActiveIdentityCard;     // Последнее активное удостоверение личности
    private IdentityCard _identityCardNew;     // Новое удостоверение личности
    private ICustomOrderExtract _customExtract;     // Выписка из кастомного приказа, с набором действий, определенных пользователем

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата смены фамилии.
     */
    public Date getChangeFioDate()
    {
        return _changeFioDate;
    }

    /**
     * @param changeFioDate Дата смены фамилии.
     */
    public void setChangeFioDate(Date changeFioDate)
    {
        dirty(_changeFioDate, changeFioDate);
        _changeFioDate = changeFioDate;
    }

    /**
     * @return Новое имя. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFirstNameNew()
    {
        return _firstNameNew;
    }

    /**
     * @param firstNameNew Новое имя. Свойство не может быть null.
     */
    public void setFirstNameNew(String firstNameNew)
    {
        dirty(_firstNameNew, firstNameNew);
        _firstNameNew = firstNameNew;
    }

    /**
     * @return Новая фамилия. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLastNameNew()
    {
        return _lastNameNew;
    }

    /**
     * @param lastNameNew Новая фамилия. Свойство не может быть null.
     */
    public void setLastNameNew(String lastNameNew)
    {
        dirty(_lastNameNew, lastNameNew);
        _lastNameNew = lastNameNew;
    }

    /**
     * @return Новое отчество.
     */
    @Length(max=255)
    public String getMiddleNameNew()
    {
        return _middleNameNew;
    }

    /**
     * @param middleNameNew Новое отчество.
     */
    public void setMiddleNameNew(String middleNameNew)
    {
        dirty(_middleNameNew, middleNameNew);
        _middleNameNew = middleNameNew;
    }

    /**
     * @return Серия.
     */
    @Length(max=255)
    public String getCardSeria()
    {
        return _cardSeria;
    }

    /**
     * @param cardSeria Серия.
     */
    public void setCardSeria(String cardSeria)
    {
        dirty(_cardSeria, cardSeria);
        _cardSeria = cardSeria;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getCardNumber()
    {
        return _cardNumber;
    }

    /**
     * @param cardNumber Номер.
     */
    public void setCardNumber(String cardNumber)
    {
        dirty(_cardNumber, cardNumber);
        _cardNumber = cardNumber;
    }

    /**
     * @return Дата рождения.
     */
    public Date getBirthDate()
    {
        return _birthDate;
    }

    /**
     * @param birthDate Дата рождения.
     */
    public void setBirthDate(Date birthDate)
    {
        dirty(_birthDate, birthDate);
        _birthDate = birthDate;
    }

    /**
     * @return Место рождения.
     */
    @Length(max=255)
    public String getBirthPlace()
    {
        return _birthPlace;
    }

    /**
     * @param birthPlace Место рождения.
     */
    public void setBirthPlace(String birthPlace)
    {
        dirty(_birthPlace, birthPlace);
        _birthPlace = birthPlace;
    }

    /**
     * @return Дата выдачи удостоверения.
     */
    public Date getIssuanceDate()
    {
        return _issuanceDate;
    }

    /**
     * @param issuanceDate Дата выдачи удостоверения.
     */
    public void setIssuanceDate(Date issuanceDate)
    {
        dirty(_issuanceDate, issuanceDate);
        _issuanceDate = issuanceDate;
    }

    /**
     * @return Кем выдано удостоверение.
     */
    @Length(max=255)
    public String getIssuancePlace()
    {
        return _issuancePlace;
    }

    /**
     * @param issuancePlace Кем выдано удостоверение.
     */
    public void setIssuancePlace(String issuancePlace)
    {
        dirty(_issuancePlace, issuancePlace);
        _issuancePlace = issuancePlace;
    }

    /**
     * @return Национальные группы.
     */
    public Nationality getNationality()
    {
        return _nationality;
    }

    /**
     * @param nationality Национальные группы.
     */
    public void setNationality(Nationality nationality)
    {
        dirty(_nationality, nationality);
        _nationality = nationality;
    }

    /**
     * @return Пол. Свойство не может быть null.
     */
    @NotNull
    public Sex getSex()
    {
        return _sex;
    }

    /**
     * @param sex Пол. Свойство не может быть null.
     */
    public void setSex(Sex sex)
    {
        dirty(_sex, sex);
        _sex = sex;
    }

    /**
     * @return Гражданство. Свойство не может быть null.
     */
    @NotNull
    public ICitizenship getCitizenship()
    {
        return _citizenship;
    }

    /**
     * @param citizenship Гражданство. Свойство не может быть null.
     */
    public void setCitizenship(ICitizenship citizenship)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && citizenship!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(ICitizenship.class);
            IEntityMeta actual =  citizenship instanceof IEntity ? EntityRuntime.getMeta((IEntity) citizenship) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_citizenship, citizenship);
        _citizenship = citizenship;
    }

    /**
     * @return Тип удостоверения личности. Свойство не может быть null.
     */
    @NotNull
    public IdentityCardType getCardType()
    {
        return _cardType;
    }

    /**
     * @param cardType Тип удостоверения личности. Свойство не может быть null.
     */
    public void setCardType(IdentityCardType cardType)
    {
        dirty(_cardType, cardType);
        _cardType = cardType;
    }

    /**
     * @return Последнее активное удостоверение личности. Свойство не может быть null.
     */
    @NotNull
    public IdentityCard getLastActiveIdentityCard()
    {
        return _lastActiveIdentityCard;
    }

    /**
     * @param lastActiveIdentityCard Последнее активное удостоверение личности. Свойство не может быть null.
     */
    public void setLastActiveIdentityCard(IdentityCard lastActiveIdentityCard)
    {
        dirty(_lastActiveIdentityCard, lastActiveIdentityCard);
        _lastActiveIdentityCard = lastActiveIdentityCard;
    }

    /**
     * @return Новое удостоверение личности.
     */
    public IdentityCard getIdentityCardNew()
    {
        return _identityCardNew;
    }

    /**
     * @param identityCardNew Новое удостоверение личности.
     */
    public void setIdentityCardNew(IdentityCard identityCardNew)
    {
        dirty(_identityCardNew, identityCardNew);
        _identityCardNew = identityCardNew;
    }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public ICustomOrderExtract getCustomExtract()
    {
        return _customExtract;
    }

    /**
     * @param customExtract Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     */
    public void setCustomExtract(ICustomOrderExtract customExtract)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && customExtract!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(ICustomOrderExtract.class);
            IEntityMeta actual =  customExtract instanceof IEntity ? EntityRuntime.getMeta((IEntity) customExtract) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_customExtract, customExtract);
        _customExtract = customExtract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ChangeIdentityCardActionGen)
        {
            setChangeFioDate(((ChangeIdentityCardAction)another).getChangeFioDate());
            setFirstNameNew(((ChangeIdentityCardAction)another).getFirstNameNew());
            setLastNameNew(((ChangeIdentityCardAction)another).getLastNameNew());
            setMiddleNameNew(((ChangeIdentityCardAction)another).getMiddleNameNew());
            setCardSeria(((ChangeIdentityCardAction)another).getCardSeria());
            setCardNumber(((ChangeIdentityCardAction)another).getCardNumber());
            setBirthDate(((ChangeIdentityCardAction)another).getBirthDate());
            setBirthPlace(((ChangeIdentityCardAction)another).getBirthPlace());
            setIssuanceDate(((ChangeIdentityCardAction)another).getIssuanceDate());
            setIssuancePlace(((ChangeIdentityCardAction)another).getIssuancePlace());
            setNationality(((ChangeIdentityCardAction)another).getNationality());
            setSex(((ChangeIdentityCardAction)another).getSex());
            setCitizenship(((ChangeIdentityCardAction)another).getCitizenship());
            setCardType(((ChangeIdentityCardAction)another).getCardType());
            setLastActiveIdentityCard(((ChangeIdentityCardAction)another).getLastActiveIdentityCard());
            setIdentityCardNew(((ChangeIdentityCardAction)another).getIdentityCardNew());
            setCustomExtract(((ChangeIdentityCardAction)another).getCustomExtract());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChangeIdentityCardActionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChangeIdentityCardAction.class;
        }

        public T newInstance()
        {
            return (T) new ChangeIdentityCardAction();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "changeFioDate":
                    return obj.getChangeFioDate();
                case "firstNameNew":
                    return obj.getFirstNameNew();
                case "lastNameNew":
                    return obj.getLastNameNew();
                case "middleNameNew":
                    return obj.getMiddleNameNew();
                case "cardSeria":
                    return obj.getCardSeria();
                case "cardNumber":
                    return obj.getCardNumber();
                case "birthDate":
                    return obj.getBirthDate();
                case "birthPlace":
                    return obj.getBirthPlace();
                case "issuanceDate":
                    return obj.getIssuanceDate();
                case "issuancePlace":
                    return obj.getIssuancePlace();
                case "nationality":
                    return obj.getNationality();
                case "sex":
                    return obj.getSex();
                case "citizenship":
                    return obj.getCitizenship();
                case "cardType":
                    return obj.getCardType();
                case "lastActiveIdentityCard":
                    return obj.getLastActiveIdentityCard();
                case "identityCardNew":
                    return obj.getIdentityCardNew();
                case "customExtract":
                    return obj.getCustomExtract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "changeFioDate":
                    obj.setChangeFioDate((Date) value);
                    return;
                case "firstNameNew":
                    obj.setFirstNameNew((String) value);
                    return;
                case "lastNameNew":
                    obj.setLastNameNew((String) value);
                    return;
                case "middleNameNew":
                    obj.setMiddleNameNew((String) value);
                    return;
                case "cardSeria":
                    obj.setCardSeria((String) value);
                    return;
                case "cardNumber":
                    obj.setCardNumber((String) value);
                    return;
                case "birthDate":
                    obj.setBirthDate((Date) value);
                    return;
                case "birthPlace":
                    obj.setBirthPlace((String) value);
                    return;
                case "issuanceDate":
                    obj.setIssuanceDate((Date) value);
                    return;
                case "issuancePlace":
                    obj.setIssuancePlace((String) value);
                    return;
                case "nationality":
                    obj.setNationality((Nationality) value);
                    return;
                case "sex":
                    obj.setSex((Sex) value);
                    return;
                case "citizenship":
                    obj.setCitizenship((ICitizenship) value);
                    return;
                case "cardType":
                    obj.setCardType((IdentityCardType) value);
                    return;
                case "lastActiveIdentityCard":
                    obj.setLastActiveIdentityCard((IdentityCard) value);
                    return;
                case "identityCardNew":
                    obj.setIdentityCardNew((IdentityCard) value);
                    return;
                case "customExtract":
                    obj.setCustomExtract((ICustomOrderExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "changeFioDate":
                        return true;
                case "firstNameNew":
                        return true;
                case "lastNameNew":
                        return true;
                case "middleNameNew":
                        return true;
                case "cardSeria":
                        return true;
                case "cardNumber":
                        return true;
                case "birthDate":
                        return true;
                case "birthPlace":
                        return true;
                case "issuanceDate":
                        return true;
                case "issuancePlace":
                        return true;
                case "nationality":
                        return true;
                case "sex":
                        return true;
                case "citizenship":
                        return true;
                case "cardType":
                        return true;
                case "lastActiveIdentityCard":
                        return true;
                case "identityCardNew":
                        return true;
                case "customExtract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "changeFioDate":
                    return true;
                case "firstNameNew":
                    return true;
                case "lastNameNew":
                    return true;
                case "middleNameNew":
                    return true;
                case "cardSeria":
                    return true;
                case "cardNumber":
                    return true;
                case "birthDate":
                    return true;
                case "birthPlace":
                    return true;
                case "issuanceDate":
                    return true;
                case "issuancePlace":
                    return true;
                case "nationality":
                    return true;
                case "sex":
                    return true;
                case "citizenship":
                    return true;
                case "cardType":
                    return true;
                case "lastActiveIdentityCard":
                    return true;
                case "identityCardNew":
                    return true;
                case "customExtract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "changeFioDate":
                    return Date.class;
                case "firstNameNew":
                    return String.class;
                case "lastNameNew":
                    return String.class;
                case "middleNameNew":
                    return String.class;
                case "cardSeria":
                    return String.class;
                case "cardNumber":
                    return String.class;
                case "birthDate":
                    return Date.class;
                case "birthPlace":
                    return String.class;
                case "issuanceDate":
                    return Date.class;
                case "issuancePlace":
                    return String.class;
                case "nationality":
                    return Nationality.class;
                case "sex":
                    return Sex.class;
                case "citizenship":
                    return ICitizenship.class;
                case "cardType":
                    return IdentityCardType.class;
                case "lastActiveIdentityCard":
                    return IdentityCard.class;
                case "identityCardNew":
                    return IdentityCard.class;
                case "customExtract":
                    return ICustomOrderExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChangeIdentityCardAction> _dslPath = new Path<ChangeIdentityCardAction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChangeIdentityCardAction");
    }
            

    /**
     * @return Дата смены фамилии.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getChangeFioDate()
     */
    public static PropertyPath<Date> changeFioDate()
    {
        return _dslPath.changeFioDate();
    }

    /**
     * @return Новое имя. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getFirstNameNew()
     */
    public static PropertyPath<String> firstNameNew()
    {
        return _dslPath.firstNameNew();
    }

    /**
     * @return Новая фамилия. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getLastNameNew()
     */
    public static PropertyPath<String> lastNameNew()
    {
        return _dslPath.lastNameNew();
    }

    /**
     * @return Новое отчество.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getMiddleNameNew()
     */
    public static PropertyPath<String> middleNameNew()
    {
        return _dslPath.middleNameNew();
    }

    /**
     * @return Серия.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getCardSeria()
     */
    public static PropertyPath<String> cardSeria()
    {
        return _dslPath.cardSeria();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getCardNumber()
     */
    public static PropertyPath<String> cardNumber()
    {
        return _dslPath.cardNumber();
    }

    /**
     * @return Дата рождения.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getBirthDate()
     */
    public static PropertyPath<Date> birthDate()
    {
        return _dslPath.birthDate();
    }

    /**
     * @return Место рождения.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getBirthPlace()
     */
    public static PropertyPath<String> birthPlace()
    {
        return _dslPath.birthPlace();
    }

    /**
     * @return Дата выдачи удостоверения.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getIssuanceDate()
     */
    public static PropertyPath<Date> issuanceDate()
    {
        return _dslPath.issuanceDate();
    }

    /**
     * @return Кем выдано удостоверение.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getIssuancePlace()
     */
    public static PropertyPath<String> issuancePlace()
    {
        return _dslPath.issuancePlace();
    }

    /**
     * @return Национальные группы.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getNationality()
     */
    public static Nationality.Path<Nationality> nationality()
    {
        return _dslPath.nationality();
    }

    /**
     * @return Пол. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getSex()
     */
    public static Sex.Path<Sex> sex()
    {
        return _dslPath.sex();
    }

    /**
     * @return Гражданство. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getCitizenship()
     */
    public static ICitizenshipGen.Path<ICitizenship> citizenship()
    {
        return _dslPath.citizenship();
    }

    /**
     * @return Тип удостоверения личности. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getCardType()
     */
    public static IdentityCardType.Path<IdentityCardType> cardType()
    {
        return _dslPath.cardType();
    }

    /**
     * @return Последнее активное удостоверение личности. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getLastActiveIdentityCard()
     */
    public static IdentityCard.Path<IdentityCard> lastActiveIdentityCard()
    {
        return _dslPath.lastActiveIdentityCard();
    }

    /**
     * @return Новое удостоверение личности.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getIdentityCardNew()
     */
    public static IdentityCard.Path<IdentityCard> identityCardNew()
    {
        return _dslPath.identityCardNew();
    }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getCustomExtract()
     */
    public static ICustomOrderExtractGen.Path<ICustomOrderExtract> customExtract()
    {
        return _dslPath.customExtract();
    }

    public static class Path<E extends ChangeIdentityCardAction> extends EntityPath<E>
    {
        private PropertyPath<Date> _changeFioDate;
        private PropertyPath<String> _firstNameNew;
        private PropertyPath<String> _lastNameNew;
        private PropertyPath<String> _middleNameNew;
        private PropertyPath<String> _cardSeria;
        private PropertyPath<String> _cardNumber;
        private PropertyPath<Date> _birthDate;
        private PropertyPath<String> _birthPlace;
        private PropertyPath<Date> _issuanceDate;
        private PropertyPath<String> _issuancePlace;
        private Nationality.Path<Nationality> _nationality;
        private Sex.Path<Sex> _sex;
        private ICitizenshipGen.Path<ICitizenship> _citizenship;
        private IdentityCardType.Path<IdentityCardType> _cardType;
        private IdentityCard.Path<IdentityCard> _lastActiveIdentityCard;
        private IdentityCard.Path<IdentityCard> _identityCardNew;
        private ICustomOrderExtractGen.Path<ICustomOrderExtract> _customExtract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата смены фамилии.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getChangeFioDate()
     */
        public PropertyPath<Date> changeFioDate()
        {
            if(_changeFioDate == null )
                _changeFioDate = new PropertyPath<Date>(ChangeIdentityCardActionGen.P_CHANGE_FIO_DATE, this);
            return _changeFioDate;
        }

    /**
     * @return Новое имя. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getFirstNameNew()
     */
        public PropertyPath<String> firstNameNew()
        {
            if(_firstNameNew == null )
                _firstNameNew = new PropertyPath<String>(ChangeIdentityCardActionGen.P_FIRST_NAME_NEW, this);
            return _firstNameNew;
        }

    /**
     * @return Новая фамилия. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getLastNameNew()
     */
        public PropertyPath<String> lastNameNew()
        {
            if(_lastNameNew == null )
                _lastNameNew = new PropertyPath<String>(ChangeIdentityCardActionGen.P_LAST_NAME_NEW, this);
            return _lastNameNew;
        }

    /**
     * @return Новое отчество.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getMiddleNameNew()
     */
        public PropertyPath<String> middleNameNew()
        {
            if(_middleNameNew == null )
                _middleNameNew = new PropertyPath<String>(ChangeIdentityCardActionGen.P_MIDDLE_NAME_NEW, this);
            return _middleNameNew;
        }

    /**
     * @return Серия.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getCardSeria()
     */
        public PropertyPath<String> cardSeria()
        {
            if(_cardSeria == null )
                _cardSeria = new PropertyPath<String>(ChangeIdentityCardActionGen.P_CARD_SERIA, this);
            return _cardSeria;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getCardNumber()
     */
        public PropertyPath<String> cardNumber()
        {
            if(_cardNumber == null )
                _cardNumber = new PropertyPath<String>(ChangeIdentityCardActionGen.P_CARD_NUMBER, this);
            return _cardNumber;
        }

    /**
     * @return Дата рождения.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getBirthDate()
     */
        public PropertyPath<Date> birthDate()
        {
            if(_birthDate == null )
                _birthDate = new PropertyPath<Date>(ChangeIdentityCardActionGen.P_BIRTH_DATE, this);
            return _birthDate;
        }

    /**
     * @return Место рождения.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getBirthPlace()
     */
        public PropertyPath<String> birthPlace()
        {
            if(_birthPlace == null )
                _birthPlace = new PropertyPath<String>(ChangeIdentityCardActionGen.P_BIRTH_PLACE, this);
            return _birthPlace;
        }

    /**
     * @return Дата выдачи удостоверения.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getIssuanceDate()
     */
        public PropertyPath<Date> issuanceDate()
        {
            if(_issuanceDate == null )
                _issuanceDate = new PropertyPath<Date>(ChangeIdentityCardActionGen.P_ISSUANCE_DATE, this);
            return _issuanceDate;
        }

    /**
     * @return Кем выдано удостоверение.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getIssuancePlace()
     */
        public PropertyPath<String> issuancePlace()
        {
            if(_issuancePlace == null )
                _issuancePlace = new PropertyPath<String>(ChangeIdentityCardActionGen.P_ISSUANCE_PLACE, this);
            return _issuancePlace;
        }

    /**
     * @return Национальные группы.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getNationality()
     */
        public Nationality.Path<Nationality> nationality()
        {
            if(_nationality == null )
                _nationality = new Nationality.Path<Nationality>(L_NATIONALITY, this);
            return _nationality;
        }

    /**
     * @return Пол. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getSex()
     */
        public Sex.Path<Sex> sex()
        {
            if(_sex == null )
                _sex = new Sex.Path<Sex>(L_SEX, this);
            return _sex;
        }

    /**
     * @return Гражданство. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getCitizenship()
     */
        public ICitizenshipGen.Path<ICitizenship> citizenship()
        {
            if(_citizenship == null )
                _citizenship = new ICitizenshipGen.Path<ICitizenship>(L_CITIZENSHIP, this);
            return _citizenship;
        }

    /**
     * @return Тип удостоверения личности. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getCardType()
     */
        public IdentityCardType.Path<IdentityCardType> cardType()
        {
            if(_cardType == null )
                _cardType = new IdentityCardType.Path<IdentityCardType>(L_CARD_TYPE, this);
            return _cardType;
        }

    /**
     * @return Последнее активное удостоверение личности. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getLastActiveIdentityCard()
     */
        public IdentityCard.Path<IdentityCard> lastActiveIdentityCard()
        {
            if(_lastActiveIdentityCard == null )
                _lastActiveIdentityCard = new IdentityCard.Path<IdentityCard>(L_LAST_ACTIVE_IDENTITY_CARD, this);
            return _lastActiveIdentityCard;
        }

    /**
     * @return Новое удостоверение личности.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getIdentityCardNew()
     */
        public IdentityCard.Path<IdentityCard> identityCardNew()
        {
            if(_identityCardNew == null )
                _identityCardNew = new IdentityCard.Path<IdentityCard>(L_IDENTITY_CARD_NEW, this);
            return _identityCardNew;
        }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction#getCustomExtract()
     */
        public ICustomOrderExtractGen.Path<ICustomOrderExtract> customExtract()
        {
            if(_customExtract == null )
                _customExtract = new ICustomOrderExtractGen.Path<ICustomOrderExtract>(L_CUSTOM_EXTRACT, this);
            return _customExtract;
        }

        public Class getEntityClass()
        {
            return ChangeIdentityCardAction.class;
        }

        public String getEntityName()
        {
            return "changeIdentityCardAction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
