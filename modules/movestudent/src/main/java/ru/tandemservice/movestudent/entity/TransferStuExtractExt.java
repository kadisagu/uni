package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.TransferStuExtractExtGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О переводе
 */
public class TransferStuExtractExt extends TransferStuExtractExtGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}