/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e35.ParagraphAddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract;
import ru.tandemservice.movestudent.utils.ExtEducationLevelsHighSchoolSelectModel;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ListExtractComponentGenerator
 * @since 22.10.2012
 */
public class DAO extends AbstractListParagraphAddEditDAO<SplitFirstCourseStudentsGroupStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        final OrgUnit orgUnit = model.getParagraph().getOrder().getOrgUnit();
        model.setOneParagraph(model.isParagraphOnlyOneInTheOrder());
        model.setCourse(DevelopGridDAO.getCourseMap().get(1));
        final SplitFirstCourseStudentsGroupStuListExtract firstExtract = model.getFirstExtract();
        if (firstExtract != null && model.getParagraphId() == null)
        {
            model.setDevelopForm(firstExtract.getDevelopForm());
        }

        model.setNewGroupListModel(new GroupSelectModel(model, orgUnit).hideInactiveProgramSubject(true));

        model.setEducationLevelsHighSchoolModel(new ExtEducationLevelsHighSchoolSelectModel(null, null, orgUnit)
        {
            @Override
            protected DQLSelectBuilder createEducationLevelsHighSchoolDQL(String alias)
            {
                final DQLSelectBuilder builder = super.createEducationLevelsHighSchoolDQL(alias);

                if (firstExtract != null)
                {
                    EducationLevelsHighSchool hs = firstExtract.getEducationLevelsHighSchool();
                    StructureEducationLevels structure = hs.getEducationLevel().getLevelType();
                    if (structure != null && !structure.isSpecialization() && !structure.isProfile())
                    {
                        builder.where(or(
                                eq(property(alias), value(hs)),
                                eq(property(alias, EducationLevelsHighSchool.educationLevel().parentLevel()), value(hs.getEducationLevel()))
                        ));
                    }
                    else
                    {
                        builder.where(eq(property(alias, EducationLevelsHighSchool.educationLevel().parentLevel()), value(EducationOrgUnitUtil.getParentLevel(hs))));
                    }
                }

                return builder;
            }
        });

        model.setDevelopFormModel(EducationOrgUnitUtil.getDevelopFormSelectModel(model, orgUnit));
        model.setDevelopConditionModel(EducationOrgUnitUtil.getDevelopConditionSelectModel(model, orgUnit));
        model.setDevelopTechModel(EducationOrgUnitUtil.getDevelopTechSelectModel(model, orgUnit));
        model.setDevelopPeriodModel(EducationOrgUnitUtil.getDevelopPeriodSelectModel(model, orgUnit));

        if (model.isEditForm() && firstExtract != null)
        {
            model.setNewGroup(firstExtract.getNewGroup());
            model.setEducationLevelsHighSchool(firstExtract.getEducationLevelsHighSchool());
            model.setDevelopForm(firstExtract.getDevelopForm());
            model.setDevelopCondition(firstExtract.getDevelopCondition());
            model.setDevelopTech(firstExtract.getDevelopTech());
            model.setDevelopPeriod(firstExtract.getDevelopPeriod());
        }
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        if (model.getNewGroup() == null) {
            builder.add(MQExpression.eq(STUDENT_ALIAS, "id", 0L));
        } else {
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COURSE, model.getCourse()));
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopForm()));
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopCondition()));
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_TECH, model.getDevelopTech()));
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_PERIOD, model.getDevelopPeriod()));
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getNewGroup().getEducationOrgUnit().getFormativeOrgUnit()));
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getNewGroup().getEducationOrgUnit().getTerritorialOrgUnit()));
            builder.add(MQExpression.isNull(STUDENT_ALIAS, Student.L_GROUP));
        }
    }

    @Override
    protected void appendStudentsWithGroup(MQBuilder builder)
    {
        /* Нужны студенты без академических групп */
    }

    @Override
    protected SplitFirstCourseStudentsGroupStuListExtract createNewInstance(Model model)
    {
        return new SplitFirstCourseStudentsGroupStuListExtract();
    }

    @Override
    protected void fillExtract(SplitFirstCourseStudentsGroupStuListExtract extract, Student student, Model model)
    {
        extract.setNewGroup(model.getNewGroup());
        extract.setEducationLevelsHighSchool(model.getEducationLevelsHighSchool());
        extract.setDevelopForm(model.getDevelopForm());
        extract.setDevelopCondition(model.getDevelopCondition());
        extract.setDevelopTech(model.getDevelopTech());
        extract.setDevelopPeriod(model.getDevelopPeriod());
    }
}