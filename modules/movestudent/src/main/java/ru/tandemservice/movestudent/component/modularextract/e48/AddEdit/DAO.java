/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e48.AddEdit;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.EntityUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDao;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.GroupToExtractRelation;
import ru.tandemservice.movestudent.entity.RemoveGroupManagerStuExtract;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.GroupCaptainStudent;

import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 13.08.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<RemoveGroupManagerStuExtract, Model>
{
    @Override
    protected RemoveGroupManagerStuExtract createNewInstance()
    {
        return new RemoveGroupManagerStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setGroupListModel(new BaseMultiSelectModel()
        {
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder
                        .fromEntity(GroupCaptainStudent.class, "g")
                        .column(property(GroupCaptainStudent.group().fromAlias("g")))
                        .where(eq(property(GroupCaptainStudent.group().archival().fromAlias("g")), value(Boolean.FALSE)))
                        .where(eq(property(GroupCaptainStudent.student().fromAlias("g")), value(model.getExtract().getEntity())))
                        .where(like(DQLFunctions.upper(property(GroupCaptainStudent.group().title().fromAlias("g"))), value(CoreStringUtils.escapeLike(filter))))
                        .order(property(GroupCaptainStudent.group().title().fromAlias("g")));

                int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                if (count > MoveStudentDao.MAX_ROWS)
                    builder.top(MoveStudentDao.MAX_ROWS);

                return new ListResult<>(builder.createStatement(getSession()).<Group>list(), count);
            }

            @Override
            public List getValues(Set primaryKeys)
            {
                return getSession().createCriteria(Group.class).add(Restrictions.in("id", primaryKeys)).list();
            }
        });
        if(model.isAddForm())
        {
            model.setGroupsList(model.getGroupListModel().findValues("").getObjects());
        }
        else
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GroupToExtractRelation.class, "ge");
            builder.column(property("ge", GroupToExtractRelation.group()));
            builder.where(eq(property("ge", GroupToExtractRelation.extract().id()), value(model.getExtractId())));

            List<Group> currentManagerGroups = model.getGroupListModel().findValues("").getObjects();
            List<Group> selectedGroups = builder.createStatement(getSession()).<Group>list();

            List<Group> currentGroups = Lists.newArrayList(model.getGroupListModel().findValues("").getObjects());

            for(Group currentManagerGroup : currentManagerGroups)
            {
                if(!selectedGroups.contains(currentManagerGroup))
                    currentGroups.remove(currentManagerGroup);
            }

            model.setGroupsList(currentGroups);
        }

    }

    @Override
    public void update(Model model)
    {
        super.update(model);

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(GroupToExtractRelation.class);
        deleteBuilder.where(eq(property(GroupToExtractRelation.extract().id()), value(model.getExtract().getId())));
        deleteBuilder.createStatement(getSession()).execute();

        for(Group group : model.getGroupsList())
        {
            List<Student> studentList = UniDaoFacade.getGroupDao().getCaptainStudentList(group);
            for (Student student : studentList)
            {
                GroupToExtractRelation groupToExtractRelation = new GroupToExtractRelation();
                groupToExtractRelation.setExtract(model.getExtract());
                groupToExtractRelation.setGroup(group);
                groupToExtractRelation.setPrevGroupManager(student);
                save(groupToExtractRelation);
            }
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        if(null != model.getGroupsList() && model.getGroupsList().size() > 0 && null != model.getExtract().getEntity().getGroup())
        {
            boolean groupManager = false;
            for(Group group : model.getGroupsList())
            {
                List<Student> studentList = UniDaoFacade.getGroupDao().getCaptainStudentList(group);
                if(!studentList.isEmpty() && studentList.contains(model.getExtract().getEntity()))
                    groupManager = true;
            }
            if(!groupManager) errors.add("Нельзя снять со студента обязанности старосты, поскольку он не является старостой учебных групп.");
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GroupToExtractRelation.class, "ge");
        builder.where(in(property("ge", GroupToExtractRelation.group().id()), EntityUtils.getIdsFromEntityList(model.getGroupsList())));
        builder.where(eq(property("ge", GroupToExtractRelation.extract().committed()), value(false)));
        if(model.isEditForm())
            builder.where(ne(property("ge", GroupToExtractRelation.extract().id()), value(model.getExtract().getId())));

        List<GroupToExtractRelation> selectedInOtherExtractsGroups = builder.createStatement(getSession()).list();
        if(!selectedInOtherExtractsGroups.isEmpty())
        {
            List<String> groupsInfo = Lists.newArrayList();
            for(GroupToExtractRelation relation : selectedInOtherExtractsGroups)
            {
                String info = relation.getGroup().getTitle();
                AbstractStudentOrder order = relation.getExtract().getParagraph() == null ? null : relation.getExtract().getParagraph().getOrder();
                if(null != order)
                {
                    if(null != order.getNumber() && null != order.getCommitDate())
                        info += " (приказ №"+ order.getNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()) + ")";
                    else
                        info += " (приказ созданный - " + order.getCreateDateFormatted() + ")";
                }
                else
                {
                    info += " (проект приказа студента " + relation.getExtract().getEntity().getFio() + ")";
                }
                groupsInfo.add(info);
            }
            errors.add((groupsInfo.size() > 1 ? "Группы выбраны в других приказах, проектах приказов: " : "Группа выбрана в другом приказе, проекте приказа: ") +
                            StringUtils.join(groupsInfo, ", "), "selectedGroups");
        }
    }
}