/* $Id: DAO.java.vm 11769 2010-03-05 07:11:49Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e41.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.PrescheduleSessionPassStuExtract;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.SessionType;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 08.02.2011
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<PrescheduleSessionPassStuExtract, Model>
{
    @Override
    public void prepare(final Model model){
        super.prepare(model);
        model.setEduYearsList(new EducationYearModel());
        model.setSessionTypeList(getCatalogItemList(SessionType.class));
        if (model.getExtractId() != null){
            PrescheduleSessionPassStuExtract extract = model.getExtract();
            model.setSessionType(extract.getSessionType());
        }
        if(model.isAddForm()){
            model.getExtract().setSessionYear(UniDaoFacade.getCoreDao().get(EducationYear.class, EducationYear.P_CURRENT, Boolean.TRUE));
        }
    }

    @Override
    protected GrammaCase getStudentTitleCase(){
        return GrammaCase.DATIVE;
    }

    @Override
    protected PrescheduleSessionPassStuExtract createNewInstance(){
        return new PrescheduleSessionPassStuExtract();
    }

    @Override
    public void update(Model model){
        model.getExtract().setSessionType(model.getSessionType());
        //save rollback data
        super.update(model);
    }
}
