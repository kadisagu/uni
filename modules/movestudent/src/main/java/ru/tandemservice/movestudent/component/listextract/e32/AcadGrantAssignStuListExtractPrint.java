/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e32;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract;
import ru.tandemservice.movestudent.utils.SessionPartModel;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author ListExtractComponentGenerator
 * @since 01.10.2012
 */
public class AcadGrantAssignStuListExtractPrint implements IPrintFormCreator<AcadGrantAssignStuListExtract>, IListParagraphPrintFormCreator<AcadGrantAssignStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, AcadGrantAssignStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);

        Student student = extract.getEntity();

        CommonExtractPrint.modifyEducationStr(modifier, student.getGroup().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());

        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
        Group group = student.getGroup();
        CommonExtractPrint.initOrgUnit(modifier, extract.getEntity().getEducationOrgUnit(), "formativeOrgUnitStr", "");

        modifier.put("sessionType_G", SessionPartModel.getSessionShortTitle(extract.getSessionType(), GrammaCase.GENITIVE));
        modifier.put("sessionType_A", SessionPartModel.getSessionShortTitle(extract.getSessionType(), GrammaCase.ACCUSATIVE));

        modifier.put("dateBegin", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginDate()));
        modifier.put("dateEnd", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndDate()));

        StringBuilder grantSize = new StringBuilder(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGrantSizeRuble()));

        if (extract.getGroupManagerBonusSizeRuble() != null)
        {
            grantSize.append("+");
            grantSize.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGroupManagerBonusSizeRuble()));
        }

        grantSize.append(" руб.");
        modifier.put("grantSize", grantSize.toString());
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, educationOrgUnit, CommonListOrderPrint.getEducationBaseText(group), "fefuShortFastExtendedOptionalText");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, AcadGrantAssignStuListExtract firstExtract)
    {
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, AcadGrantAssignStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, AcadGrantAssignStuListExtract firstExtract)
    {
    }
}