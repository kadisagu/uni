/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.movestudent.component.customorder.IStudentCustomOrderExtract;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.entity.ICustomOrderAction;

@Input({
               @Bind(key = BaseCustomActionAddEditModel.EXTRACT_ID_BIND, binding = BaseCustomActionAddEditModel.EXTRACT_ID_BIND),
               @Bind(key = BaseCustomActionAddEditModel.STUDENT_ID_BIND, binding = BaseCustomActionAddEditModel.STUDENT_ID_BIND),
       })
public class BaseCustomActionAddEditModel<T extends ICustomOrderAction>
{
    public static final String EXTRACT_ID_BIND = "extractId";
    public static final String STUDENT_ID_BIND = "studentId";

    private boolean _isEdit;
    private Long _extractId;
    private Long _studentId;
    private Student _student;
    private IStudentCustomOrderExtract _extract;
    private T _customAction;
    private boolean _isActionActive;

    public T getCustomAction()
    {
        return _customAction;
    }

    public void setCustomAction(T customAction)
    {
        _customAction = customAction;
    }

    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        _extractId = extractId;
    }

    public IStudentCustomOrderExtract getExtract()
    {
        return _extract;
    }

    public void setExtract(IStudentCustomOrderExtract extract)
    {
        _extract = extract;
    }

    public boolean isActionActive()
    {
        return _isActionActive;
    }

    public void setActionActive(boolean actionActive)
    {
        _isActionActive = actionActive;
    }

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        _studentId = studentId;
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public boolean isEdit()
    {
        return _isEdit;
    }

    public void setEdit(boolean edit)
    {
        _isEdit = edit;
    }
}