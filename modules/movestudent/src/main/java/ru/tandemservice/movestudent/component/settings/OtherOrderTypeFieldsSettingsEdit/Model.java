/* $Id$ */
package ru.tandemservice.movestudent.component.settings.OtherOrderTypeFieldsSettingsEdit;

import org.tandemframework.core.component.Input;
import ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;

/**
 * @author Dmitry Seleznev
 * @since 11.04.2013
 */
@Input(keys = "studentExtractTypeId", bindings = "studentExtractTypeId")
public class Model
{
    private Long _studentExtractTypeId;
    private StudentExtractType _studentExtractType;
    private OtherStudentFieldsSettings _fieldSettings;

    public Long getStudentExtractTypeId()
    {
        return _studentExtractTypeId;
    }

    public void setStudentExtractTypeId(Long studentExtractTypeId)
    {
        _studentExtractTypeId = studentExtractTypeId;
    }

    public StudentExtractType getStudentExtractType()
    {
        return _studentExtractType;
    }

    public void setStudentExtractType(StudentExtractType studentExtractType)
    {
        _studentExtractType = studentExtractType;
    }

    public OtherStudentFieldsSettings getFieldSettings()
    {
        return _fieldSettings;
    }

    public void setFieldSettings(OtherStudentFieldsSettings fieldSettings)
    {
        _fieldSettings = fieldSettings;
    }
}