/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.catalog.studentExtractType.StudentExtractTypePub;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.movestudent.component.commons.ExtractTypeSelectModel;
import ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author AutoGenerator
 * Created on 16.10.2008
 */
public class DAO extends DefaultCatalogPubDAO<StudentExtractType, Model> implements IDAO
{
    @Override
    public void prepareListItemDataSource(Model model)
    {
        List<StudentExtractType> result;
        String title = model.getSettings().get("title");

        result = new ExtractTypeSelectModel().hierarchical(true).findValues(title).getObjects();
        model.getDataSource().setCountRow(result.size());
        UniBaseUtils.createPage(model.getDataSource(), result);

        Map<StudentExtractType, String> groupMap = new HashMap<>();
        for (StudentExtractTypeToGroup rel : getList(StudentExtractTypeToGroup.class))
        {
            String group = groupMap.get(rel.getType());
            if (null == group)
                group = rel.getGroup().getTitle();
            else
                group = group + ", " + rel.getGroup().getTitle();
            groupMap.put(rel.getType(), group);
        }

        for (ViewWrapper<StudentExtractType> wrapper : ViewWrapper.<StudentExtractType>getPatchedList(model.getDataSource()))
        {
            final String groupTitle = groupMap.get(wrapper.getEntity());
            wrapper.setViewProperty("group", groupTitle == null ? "" : groupTitle);
        }
    }
}
