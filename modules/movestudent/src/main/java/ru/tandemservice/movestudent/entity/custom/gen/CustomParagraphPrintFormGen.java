package ru.tandemservice.movestudent.entity.custom.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import ru.tandemservice.movestudent.entity.custom.CustomParagraphPrintForm;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Печатная форма параграфа кастомного приказа, загруженная пользователем
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CustomParagraphPrintFormGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.custom.CustomParagraphPrintForm";
    public static final String ENTITY_NAME = "customParagraphPrintForm";
    public static final int VERSION_HASH = -114946677;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARAGRAPH = "paragraph";
    public static final String L_FILE = "file";

    private AbstractStudentParagraph _paragraph;     // Параграф
    private DatabaseFile _file;     // Пользовательская печатная форма

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Параграф. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public AbstractStudentParagraph getParagraph()
    {
        return _paragraph;
    }

    /**
     * @param paragraph Параграф. Свойство не может быть null и должно быть уникальным.
     */
    public void setParagraph(AbstractStudentParagraph paragraph)
    {
        dirty(_paragraph, paragraph);
        _paragraph = paragraph;
    }

    /**
     * @return Пользовательская печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getFile()
    {
        return _file;
    }

    /**
     * @param file Пользовательская печатная форма. Свойство не может быть null.
     */
    public void setFile(DatabaseFile file)
    {
        dirty(_file, file);
        _file = file;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof CustomParagraphPrintFormGen)
        {
            setParagraph(((CustomParagraphPrintForm)another).getParagraph());
            setFile(((CustomParagraphPrintForm)another).getFile());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CustomParagraphPrintFormGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CustomParagraphPrintForm.class;
        }

        public T newInstance()
        {
            return (T) new CustomParagraphPrintForm();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "paragraph":
                    return obj.getParagraph();
                case "file":
                    return obj.getFile();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "paragraph":
                    obj.setParagraph((AbstractStudentParagraph) value);
                    return;
                case "file":
                    obj.setFile((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "paragraph":
                        return true;
                case "file":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "paragraph":
                    return true;
                case "file":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "paragraph":
                    return AbstractStudentParagraph.class;
                case "file":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CustomParagraphPrintForm> _dslPath = new Path<CustomParagraphPrintForm>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CustomParagraphPrintForm");
    }
            

    /**
     * @return Параграф. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.custom.CustomParagraphPrintForm#getParagraph()
     */
    public static AbstractStudentParagraph.Path<AbstractStudentParagraph> paragraph()
    {
        return _dslPath.paragraph();
    }

    /**
     * @return Пользовательская печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.CustomParagraphPrintForm#getFile()
     */
    public static DatabaseFile.Path<DatabaseFile> file()
    {
        return _dslPath.file();
    }

    public static class Path<E extends CustomParagraphPrintForm> extends EntityPath<E>
    {
        private AbstractStudentParagraph.Path<AbstractStudentParagraph> _paragraph;
        private DatabaseFile.Path<DatabaseFile> _file;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Параграф. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.custom.CustomParagraphPrintForm#getParagraph()
     */
        public AbstractStudentParagraph.Path<AbstractStudentParagraph> paragraph()
        {
            if(_paragraph == null )
                _paragraph = new AbstractStudentParagraph.Path<AbstractStudentParagraph>(L_PARAGRAPH, this);
            return _paragraph;
        }

    /**
     * @return Пользовательская печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.CustomParagraphPrintForm#getFile()
     */
        public DatabaseFile.Path<DatabaseFile> file()
        {
            if(_file == null )
                _file = new DatabaseFile.Path<DatabaseFile>(L_FILE, this);
            return _file;
        }

        public Class getEntityClass()
        {
            return CustomParagraphPrintForm.class;
        }

        public String getEntityName()
        {
            return "customParagraphPrintForm";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
