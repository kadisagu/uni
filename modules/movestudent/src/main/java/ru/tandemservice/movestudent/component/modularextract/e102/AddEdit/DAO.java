/* $Id: DAO.java.vm 24710 2012-10-30 11:06:12Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e102.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.IMoveStudentDao;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AttestationPracticWithServiceRecStuExtract;
import ru.tandemservice.movestudent.utils.CachedSingleSelectTextModel;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 22.02.2013
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<AttestationPracticWithServiceRecStuExtract, Model> implements IDAO
{
    @Override
    protected AttestationPracticWithServiceRecStuExtract createNewInstance()
    {
        return new AttestationPracticWithServiceRecStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        IMoveStudentDao mvDao = MoveStudentDaoFacade.getMoveStudentDao();

        model.setPracticeTypeList(mvDao.getStudentPracticeTypes());
        model.setPracticeKindModel(new CachedSingleSelectTextModel(mvDao.getStudentPracticeKinds()));
        model.setCourseList(DevelopGridDAO.getCourseList());

        /*
        model.setContactModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if(null == model.getExtract().getExternalOrgUnit())
                    return ListResult.getEmpty();

                MQBuilder builder = new MQBuilder(JuridicalContactor.ENTITY_CLASS, "c");

                builder.add(MQExpression.eq("c", JuridicalContactor.P_ACTIVE, Boolean.TRUE));

                builder.add(MQExpression.eq("c", JuridicalContactor.L_EXTERNAL_ORG_UNIT + "." + ExternalOrgUnit.P_ID, model.getExtract().getExternalOrgUnit().getId()));


                builder.add(MQExpression.like("c", JuridicalContactor.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FULL_FIO, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("c", JuridicalContactor.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FULL_FIO, OrderDirection.asc);

                return new ListResult(builder.getResultList(getSession(), 0, 50), builder.getResultCount(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                JuridicalContactor ou = get((Long) primaryKey);
                return ou;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                JuridicalContactor contactor = (JuridicalContactor) value;
                return contactor.getPerson().getFullFio() + (null != contactor.getPost() ? "(" + contactor.getPost() +  ")" : "");
            }
        });
        */

        if (model.isAddForm())
        {
            model.getExtract().setCourse(model.getExtract().getEntity().getCourse());
        }
        else
        {
            model.setPracticeType(mvDao.extractPracticeType(model.getExtract().getPracticeType()));
            model.setPracticeKind(mvDao.extractPracticeKind(model.getExtract().getPracticeType()));
        }
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setPracticeType(MoveStudentDaoFacade.getMoveStudentDao().getFullPracticeKind(model.getPracticeType(), model.getPracticeKind()));
        super.update(model);
    }
}