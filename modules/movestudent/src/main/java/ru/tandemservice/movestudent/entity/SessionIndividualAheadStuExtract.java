package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.entity.gen.*;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. Об установлении индивидуального срока сдачи сессии(досрочная сдача сессии)
 */
public class SessionIndividualAheadStuExtract extends SessionIndividualAheadStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getSessionBeginDate();
    }

    @Override
    public Date getEndDate()
    {
        return getSessionEndDate();
    }
}