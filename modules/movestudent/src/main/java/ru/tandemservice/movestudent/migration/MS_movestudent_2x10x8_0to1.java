package ru.tandemservice.movestudent.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_movestudent_2x10x8_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность excludeSingGrpCathStuListExtract

		// создано обязательное свойство providedExam
		{
			// создать колонку
			tool.createColumn("xcldsnggrpcthstlstextrct_t", new DBColumn("providedexam_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update xcldsnggrpcthstlstextrct_t set providedexam_p=? where providedexam_p is null", Boolean.TRUE);

			// сделать колонку NOT NULL
			tool.setColumnNullable("xcldsnggrpcthstlstextrct_t", "providedexam_p", false);
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность excludeSingGrpStuListExtract

		// создано обязательное свойство providedExam
		{
			// создать колонку
			tool.createColumn("excludesinggrpstulistextract_t", new DBColumn("providedexam_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update excludesinggrpstulistextract_t set providedexam_p=? where providedexam_p is null", Boolean.TRUE);

			// сделать колонку NOT NULL
			tool.setColumnNullable("excludesinggrpstulistextract_t", "providedexam_p", false);
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность excludeSingGrpSuccStuListExtract

		// создано обязательное свойство providedExam
		{
			// создать колонку
			tool.createColumn("xcldsnggrpsccstlstextrct_t", new DBColumn("providedexam_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update xcldsnggrpsccstlstextrct_t set providedexam_p=? where providedexam_p is null", Boolean.TRUE);

			// сделать колонку NOT NULL
			tool.setColumnNullable("xcldsnggrpsccstlstextrct_t", "providedexam_p", false);
		}


    }
}