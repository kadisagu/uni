/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e25;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.e25.utils.SplitStudentsGroupParagraphPartWrapper;
import ru.tandemservice.movestudent.component.listextract.e25.utils.SplitStudentsGroupParagraphWrapper;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.SplitStudentsGroupStuListExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 27.11.2012
 */
public class SplitStudentsGroupStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<SplitStudentsGroupStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        List<SplitStudentsGroupParagraphWrapper> preparedParagraphsStructure = prepareParagraphsStructure(extracts);

        Qualifications qualification = preparedParagraphsStructure.get(0).getQualification();
        String qualificationCode = null != qualification ? qualification.getCode() : null;
        if (null != qualificationCode && !SplitStudentsGroupStuListExtractPrint.PROCESSED_QUALIFICATION_CODES.contains(qualificationCode))
            qualificationCode = null;

        for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
        {
            injectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[i]);
            injectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i) + "P", SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_PLURAL_CASES_ARRAY.get(qualificationCode)[i]);
        }

        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, preparedParagraphsStructure, order);

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }

    protected List<SplitStudentsGroupParagraphWrapper> prepareParagraphsStructure(List<SplitStudentsGroupStuListExtract> extracts)
    {
        int ind;
        List<SplitStudentsGroupParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        for (SplitStudentsGroupStuListExtract extract : extracts)
        {
            Student student = extract.getEntity();
            Person person = student.getPerson();


            Group groupNew = extract.getGroupNew();
            EducationOrgUnit educationOrgUnitNew = extract.getEducationOrgUnitNew();
            EducationOrgUnit educationOrgUnitOld = extract.getEducationOrgUnitOld();

            EducationLevels eduLevelsNew = educationOrgUnitNew.getEducationLevelHighSchool().getEducationLevel();
            Qualifications qualification = eduLevelsNew.getQualification();

            if (null == qualification)
            {
                EducationLevels tmpLevel = eduLevelsNew.getParentLevel();
                while (null != tmpLevel && null == qualification)
                {
                    qualification = tmpLevel.getQualification();
                    tmpLevel = tmpLevel.getParentLevel();
                }
            }

            SplitStudentsGroupParagraphWrapper paragraphWrapper = new SplitStudentsGroupParagraphWrapper(student.getCourse(), extract.getGroupOld(), student.getCompensationType(), educationOrgUnitOld.getEducationLevelHighSchool(), educationOrgUnitOld.getFormativeOrgUnit(), educationOrgUnitOld.getTerritorialOrgUnit(), educationOrgUnitOld.getDevelopForm(), educationOrgUnitOld.getDevelopCondition(), educationOrgUnitOld.getDevelopTech(), educationOrgUnitOld.getDevelopPeriod(), qualification, extract);

            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else paragraphWrapper = paragraphWrapperList.get(ind);

            SplitStudentsGroupParagraphPartWrapper paragraphPartWrapper = new SplitStudentsGroupParagraphPartWrapper(educationOrgUnitNew.getEducationLevelHighSchool(), groupNew, qualification, extract);

            ind = paragraphWrapper.getParagraphPartWrapperList().indexOf(paragraphPartWrapper);
            if (ind == -1)
                paragraphWrapper.getParagraphPartWrapperList().add(paragraphPartWrapper);
            else
                paragraphPartWrapper = paragraphWrapper.getParagraphPartWrapperList().get(ind);

            paragraphPartWrapper.getPersonList().add(person);
        }

        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<SplitStudentsGroupParagraphWrapper> paragraphWrappers, StudentListOrder order)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            int parNumber = 0;
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (SplitStudentsGroupParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.SET_SPECIALIZATION_AND_REASSIGN_GROUP_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphWrapper.getFirstExtract();
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(firstExtract.getParagraph(), firstExtract);
                CommonExtractPrint.injectReason(paragraphInjectModifier, order.getReason(), order.getReasonComment());

                parNumber++;
                paragraphInjectModifier.put("parNumber", String.valueOf(parNumber));
                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");
                paragraphInjectModifier.put("paragraphNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");

                String code = paragraphWrapper.getDevelopForm().getCode();
                if (DevelopFormCodes.FULL_TIME_FORM.equals(code))
                    paragraphInjectModifier.put("developForm_DF", "очной");
                else if (DevelopFormCodes.CORESP_FORM.equals(code))
                    paragraphInjectModifier.put("developForm_DF", "заочной");
                else if (DevelopFormCodes.PART_TIME_FORM.equals(code))
                    paragraphInjectModifier.put("developForm_DF", "очно-заочной");
                else if (DevelopFormCodes.EXTERNAL_FORM.equals(code))
                    paragraphInjectModifier.put("developForm_DF", "экстернату");
                else if (DevelopFormCodes.APPLICANT_FORM.equals(code))
                    paragraphInjectModifier.put("developForm_DF", "самостоятельному обучению и итоговой аттестации");

                Student firstStudent = firstExtract.getEntity();

                CommonListExtractPrint.injectCompensationType(paragraphInjectModifier, firstStudent.getCompensationType(), "", false);

                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstStudent.getEducationOrgUnit().getFormativeOrgUnit(), "", "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstStudent.getEducationOrgUnit(), "formativeOrgUnitStr", "");

                Qualifications qualification = paragraphWrapper.getQualification();
                String qualificationCode = null != qualification ? qualification.getCode() : null;
                if (null != qualificationCode && !SplitStudentsGroupStuListExtractPrint.PROCESSED_QUALIFICATION_CODES.contains(qualificationCode))
                {
                    qualificationCode = null;
                }

                for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
                {
                    paragraphInjectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[i]);
                    paragraphInjectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i) + "P", SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_PLURAL_CASES_ARRAY.get(qualificationCode)[i]);
                }

                paragraphInjectModifier.put("course", paragraphWrapper.getCourse().getTitle());
                paragraphInjectModifier.put("group", paragraphWrapper.getGroup().getTitle());
                CommonExtractPrint.initFefuGroup(paragraphInjectModifier, "groupInternal_G", paragraphWrapper.getGroup(), paragraphWrapper.getDevelopForm(), " группы ");
                UniRtfUtil.initEducationType(paragraphInjectModifier, paragraphWrapper.getEducationLevels(), "");
                paragraphInjectModifier.put("educationOrgUnit", paragraphWrapper.getEducationLevels().getDisplayableTitle());


                EducationLevels speciality = EducationOrgUnitUtil.getParentLevel(paragraphWrapper.getEducationLevels());
                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, speciality, new String[]{"fefuEducationStrDirectionOld"}, false);


                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, paragraphWrapper.getEducationLevels().getEducationLevel());
                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, paragraphWrapper.getDevelopCondition(), paragraphWrapper.getDevelopTech(), paragraphWrapper.getDevelopPeriod(), CommonListOrderPrint.getEducationBaseText(paragraphWrapper.getGroup()), "fefuShortFastExtendedOptionalText");
                customModifier.listParagraphModifier(paragraphInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphInjectModifier.modify(paragraph);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getParagraphPartWrapperList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraph, List<SplitStudentsGroupParagraphPartWrapper> paragraphPartWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartWrappers);
            List<IRtfElement> parList = new ArrayList<>();

            for (SplitStudentsGroupParagraphPartWrapper paragraphPartWrapper : paragraphPartWrappers)
            {
                // Получаем шаблон подпараграфа
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.SET_SPECIALIZATION_AND_REASSIGN_GROUP_LIST_EXTRACT), 3);
                RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphPartWrapper.getFirstExtract();
                RtfInjectModifier paragraphPartInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, firstExtract);

                Qualifications qualification = paragraphPartWrapper.getQualification();
                String qualificationCode = null != qualification ? qualification.getCode() : null;
                if (null != qualificationCode && !SplitStudentsGroupStuListExtractPrint.PROCESSED_QUALIFICATION_CODES.contains(qualificationCode))
                {
                    //qualification = null;
                    qualificationCode = null;
                }

                for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
                {
                    paragraphPartInjectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[i]);
                    paragraphPartInjectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i) + "P", SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_PLURAL_CASES_ARRAY.get(qualificationCode)[i]);
                    paragraphPartInjectModifier.put("ProfileString" + UniRtfUtil.CASE_POSTFIX.get(i), StringUtils.capitalize(SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[i]));
                    paragraphPartInjectModifier.put("ProfileString" + UniRtfUtil.CASE_POSTFIX.get(i) + "P", StringUtils.capitalize(SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_PLURAL_CASES_ARRAY.get(qualificationCode)[i]));
                }

                paragraphPartInjectModifier.put("specialization", paragraphPartWrapper.getEducationLevel().getTitle());
                paragraphPartInjectModifier.put("specializationNew", paragraphPartWrapper.getEducationLevel().getTitle());
                paragraphPartInjectModifier.put("profileNew", paragraphPartWrapper.getEducationLevel().getTitle());
                paragraphPartInjectModifier.put("groupNew", paragraphPartWrapper.getGroupNew().getTitle());

                List<Person> personList = paragraphPartWrapper.getPersonList();
                personList.sort(Person.FULL_FIO_AND_ID_COMPARATOR);
                RtfString rtfString = new RtfString();

                int j = 0;

                for (; j < personList.size() - 1; j++)
                {
                    rtfString.append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio()).par();
                }

                rtfString.append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio());

                paragraphPartInjectModifier.put("STUDENT_LIST", rtfString);
                customModifier.listParagraphModifier(paragraphPartInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphPartInjectModifier.modify(paragraphPart);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraphPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}