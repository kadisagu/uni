package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.entity.gen.SocGrantStopStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О приостановлении выплаты социальной стипендии
 */
public class SocGrantStopStuExtract extends SocGrantStopStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getStopPayingDate();
    }
}