/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e30.ParagraphAddEdit;

import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author Andrey Avetisov
 * @since 07.05.2014
 */
public class ExtEduModel extends Model
{
    private Model _model;

    public ExtEduModel(Model model)
    {
        _model = model;
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return  _model.getParagraph().getOrder().getOrgUnit();
    }
}
