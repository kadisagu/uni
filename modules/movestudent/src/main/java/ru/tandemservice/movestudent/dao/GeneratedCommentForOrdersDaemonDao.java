/* $Id$ */
package ru.tandemservice.movestudent.dao;

import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.OtherStudentExtract;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.Collection;
import java.util.List;


import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 13.01.2015
 */
public class GeneratedCommentForOrdersDaemonDao extends UniBaseDao implements IGeneratedCommentForOrdersDaemonDao
{

    public static final int DAEMON_ITERATION_TIME = 2880;

    public static final SyncDaemon DAEMON = new SyncDaemon(GeneratedCommentForOrdersDaemonDao.class.getName(), DAEMON_ITERATION_TIME, IGeneratedCommentForOrdersDaemonDao.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            try
            {
                IGeneratedCommentForOrdersDaemonDao.instance.get().generateCommentForOrders();
            } catch (final Exception t)
            {
                Debug.exception(t.getMessage(), t);
            }

        }
    };


    @Override
    public void generateCommentForOrders()
    {
        final List<Long> rowList = new DQLSelectBuilder()
                .fromEntity(AbstractStudentExtract.class, "ext")
                .column(property(AbstractStudentExtract.id().fromAlias("ext")))
                .where(notInstanceOf("ext", OtherStudentExtract.class))
                .where(eq(property(AbstractStudentExtract.commentEdited().fromAlias("ext")), value(false)))
                .where(isNull(property(AbstractStudentExtract.comment().fromAlias("ext"))))
                .createStatement(getSession()).list();

        if (rowList.size() > 0)
        {
            BatchUtils.execute(rowList, DQL.MAX_VALUES_ROW_NUMBER, new BatchUtils.Action<Long>()
            {
                @Override
                public void execute(final Collection<Long> elements)
                {
                    IGeneratedCommentForOrdersDaemonDao.instance.get().updateOrderComments(elements);
                }
            });
        }

    }

    @Override
    public void updateOrderComments(Collection<Long> ids)
    {
        final DQLValuesBuilder valuesBuilder = new DQLValuesBuilder(
                new String[]{"id", "comment"},
                new PropertyType[]{PropertyType.LONG, PropertyType.STRING}
        );

        for (Long e : ids)
        {
            AbstractStudentExtract extract = getNotNull(AbstractStudentExtract.class, e);
            String comment = extract.getGeneratedComment(false) != null ? extract.getGeneratedComment(false) : "";
            // формируем фрагмент данных
            valuesBuilder.row(value(e), value(comment));
        }

       DQLUpdateBuilder builder = new DQLUpdateBuilder(AbstractStudentExtract.class)
                .fromDataSource(valuesBuilder.build(), "src")
                .where(eq(property("id"), property("src.id")))
                .set(AbstractStudentExtract.P_COMMENT, property("src.comment"));

               builder.createStatement(getSession()).execute();
    }
}