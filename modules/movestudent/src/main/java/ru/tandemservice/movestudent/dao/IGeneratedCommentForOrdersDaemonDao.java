/* $Id$ */
package ru.tandemservice.movestudent.dao;


import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.util.cache.SpringBeanCache;


import java.util.Collection;

/**
 * @author Ekaterina Zvereva
 * @since 13.01.2015
 */
public interface IGeneratedCommentForOrdersDaemonDao extends INeedPersistenceSupport
{

    final String GLOBAL_DAEMON_LOCK = IGeneratedCommentForOrdersDaemonDao.class.getName() + ".global-lock";
    final SpringBeanCache<IGeneratedCommentForOrdersDaemonDao> instance = new SpringBeanCache<>(IGeneratedCommentForOrdersDaemonDao.class.getName());


    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void generateCommentForOrders();

    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    void updateOrderComments(Collection<Long> ids);
}