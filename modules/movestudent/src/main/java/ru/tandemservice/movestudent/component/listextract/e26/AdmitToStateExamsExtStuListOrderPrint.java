/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e26;

import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.e26.utils.AdmitOrderParagraphDataWrapper;
import ru.tandemservice.movestudent.component.listextract.e26.utils.AdmitOrderParagraphPartDataWrapper;
import ru.tandemservice.movestudent.component.listextract.e26.utils.AdmitOrderParagraphPartPartDataWrapper;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AdmitToStateExamsExtStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 07.09.2012
 */
public class AdmitToStateExamsExtStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    public static final String SUB_PARAGRAPH_CONTENT = "SUB_PARAGRAPH_CONTENT";

    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);
        List<AdmitToStateExamsExtStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, prepareParagraphsStructure(extracts));

        RtfUtil.optimizeFontAndColorTable(document);
        return document;
    }

    protected List<AdmitOrderParagraphDataWrapper> prepareParagraphsStructure(List<AdmitToStateExamsExtStuListExtract> extracts)
    {
        List<AdmitOrderParagraphDataWrapper> paragraphDataWrapperList = new ArrayList<>();
        int ind;
        for (AdmitToStateExamsExtStuListExtract extract : extracts)
        {
            Student student = extract.getEntity();
            EducationOrgUnit educationOrgUnit = student.getGroup().getEducationOrgUnit();
            String qCode = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getSafeQCode();
            boolean isDirection = QualificationsCodes.BAKALAVR.equals(qCode) || QualificationsCodes.MAGISTR.equals(qCode);
            String eduBaseText = CommonListOrderPrint.getEducationBaseText(student.getGroup());

            AdmitOrderParagraphDataWrapper paragraphDataWrapper = new AdmitOrderParagraphDataWrapper(student.getGroup().getCourse(), student.getCompensationType(),
                   educationOrgUnit, isDirection, eduBaseText, extract);
            ind = paragraphDataWrapperList.indexOf(paragraphDataWrapper);
            if (ind == -1)
                paragraphDataWrapperList.add(paragraphDataWrapper);
            else paragraphDataWrapper = paragraphDataWrapperList.get(ind);

            AdmitOrderParagraphPartDataWrapper paragraphPartDataWrapper = new AdmitOrderParagraphPartDataWrapper(educationOrgUnit.getEducationLevelHighSchool(), extract);
            ind = paragraphDataWrapper.getParagraphPartList().indexOf(paragraphPartDataWrapper);
            if (ind == -1)
                paragraphDataWrapper.getParagraphPartList().add(paragraphPartDataWrapper);
            else paragraphPartDataWrapper = paragraphDataWrapper.getParagraphPartList().get(ind);

            AdmitOrderParagraphPartPartDataWrapper paragraphPartPartDataWrapper = new AdmitOrderParagraphPartPartDataWrapper(student.getGroup(), extract);
            ind = paragraphPartDataWrapper.getParagraphPartPartList().indexOf(paragraphPartPartDataWrapper);
            if (ind == -1)
                paragraphPartDataWrapper.getParagraphPartPartList().add(paragraphPartPartDataWrapper);
            else paragraphPartPartDataWrapper = paragraphPartDataWrapper.getParagraphPartPartList().get(ind);

            Person person = student.getPerson();

            if (!paragraphPartPartDataWrapper.getPersonList().contains(person))
                paragraphPartPartDataWrapper.getPersonList().add(person);
        }

        return paragraphDataWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<AdmitOrderParagraphDataWrapper> paragraphDataWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphDataWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            int parNumber = 0;
            for (AdmitOrderParagraphDataWrapper paragraphDataWrapper : paragraphDataWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.ADMIT_TO_STATE_EXAMS_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphDataWrapper.getFirstExtract();
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(firstExtract.getParagraph(), firstExtract);

                CommonExtractPrint.initDevelopForm(paragraphInjectModifier, paragraphDataWrapper.getDevelopForm(), "");

                paragraphInjectModifier.put("course", paragraphDataWrapper.getCourse().getTitle());
                CommonListExtractPrint.injectCompensationType(paragraphInjectModifier, paragraphDataWrapper.getCompensationType(), "", false);
                paragraphInjectModifier.put("directionString_D", paragraphDataWrapper.isDirection() ? "направлению" : "специальности");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, paragraphDataWrapper.getEducationOrgUnit(), "formativeOrgUnitStr", "");

                paragraphInjectModifier.put("parNumber", String.valueOf(++parNumber));
                paragraphInjectModifier.put("parNumberConditional", paragraphDataWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");
                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, paragraphDataWrapper.getDevelopCondition(), paragraphDataWrapper.getDevelopTech(), paragraphDataWrapper.getDevelopPeriod(), paragraphDataWrapper.getEduBaseText(), "fefuShortFastExtendedOptionalText");
                customModifier.listParagraphModifier(paragraphInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphInjectModifier.modify(paragraph);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphDataWrapper.getParagraphPartList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraph, List<AdmitOrderParagraphPartDataWrapper> paragraphPartDataWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartDataWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (AdmitOrderParagraphPartDataWrapper paragraphPartDataWrapper : paragraphPartDataWrappers)
            {
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.ADMIT_TO_STATE_EXAMS_LIST_EXTRACT), 3);
                RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphPartDataWrapper.getFirstExtract();
                RtfInjectModifier paragraphPartInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, firstExtract);

                String educationStrDirection;
                String educationStrProfile;

                EducationLevelsHighSchool educationLevelsHighSchool = paragraphPartDataWrapper.getEducationLevelsHighSchool();
                StructureEducationLevels levelType = educationLevelsHighSchool.getEducationLevel().getLevelType();

                if (levelType.isSpecialization()) // специализация
                {
                    educationStrDirection = "Специальность ";
                    educationStrProfile = "Специализация «" + educationLevelsHighSchool.getTitle() + "»";
                }
                else if (levelType.isSpecialty()) // специальность
                {
                    educationStrDirection = "Специальность ";
                    educationStrProfile = "";
                }
                else if (levelType.isBachelor() && levelType.isProfile()) // бакалаврский профиль
                {
                    educationStrDirection = "Направление ";
                    educationStrProfile = "Профиль «" + educationLevelsHighSchool.getTitle() + "»";
                }
                else if (levelType.isMaster() && levelType.isProfile()) // магистерский профиль
                {
                    educationStrDirection = "Направление ";
                    educationStrProfile = "Магистерская программа «" + educationLevelsHighSchool.getTitle() + "»";
                }
                else // направление подготовки
                {
                    educationStrDirection = "Направление подготовки ";
                    educationStrProfile = "";
                }

                String direction = CommonExtractPrint.getFefuHighEduLevelStr(educationLevelsHighSchool.getEducationLevel());

                paragraphPartInjectModifier.put("fefuEducationStrDirection", educationStrDirection + direction);
                paragraphPartInjectModifier.put("fefuEducationStrProfile", educationStrProfile);
                paragraphPartInjectModifier.put("educationStrDirection", educationStrDirection + direction);
                paragraphPartInjectModifier.put("educationStrProfile", educationStrProfile);
                customModifier.listParagraphModifier(paragraphPartInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphPartInjectModifier.modify(paragraphPart);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraphPart);

                // Вставляем список подподпараграфов
                injectSubSubParagraphs(paragraphPart, paragraphPartDataWrapper.getParagraphPartPartList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор подпараграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubSubParagraphs(RtfDocument paragraphPart, List<AdmitOrderParagraphPartPartDataWrapper> paragraphPartPartDataWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraphPart, SUB_PARAGRAPH_CONTENT);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartPartDataWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (AdmitOrderParagraphPartPartDataWrapper paragraphPartPartDataWrapper : paragraphPartPartDataWrappers)
            {
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.ADMIT_TO_STATE_EXAMS_LIST_EXTRACT), 4);
                RtfDocument paragraphPartPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphPartPartDataWrapper.getFirstExtract();
                RtfInjectModifier paragraphPartPartInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, firstExtract);

                paragraphPartPartInjectModifier.put("group", paragraphPartPartDataWrapper.getGroup().getTitle());

                // Вставляем список подподпараграфов

                List<Person> personList = paragraphPartPartDataWrapper.getPersonList();
                personList.sort(Person.FULL_FIO_AND_ID_COMPARATOR);
                RtfString rtfString = new RtfString();

                int i = 0;

                for (; i < personList.size() - 1; i++)
                {
                    rtfString.append(String.valueOf(i + 1)).append(". ").append(personList.get(i).getFullFio()).par();
                }

                rtfString.append(String.valueOf(i + 1)).append(". ").append(personList.get(i).getFullFio());

                paragraphPartPartInjectModifier.put("STUDENT_LIST", rtfString);
                customModifier.listParagraphModifier(paragraphPartPartInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphPartPartInjectModifier.modify(paragraphPartPart);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraphPartPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPartPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор подподпараграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}