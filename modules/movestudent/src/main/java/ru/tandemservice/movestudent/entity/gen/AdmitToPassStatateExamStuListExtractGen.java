package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AdmitToPassStatateExamStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О допуске к государственной итоговой аттестации в форме государственного экзамена»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AdmitToPassStatateExamStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.AdmitToPassStatateExamStuListExtract";
    public static final String ENTITY_NAME = "admitToPassStatateExamStuListExtract";
    public static final int VERSION_HASH = -379483499;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_LEVELS_HIGH_SCHOOL = "educationLevelsHighSchool";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_COURSE = "course";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";

    private EducationLevelsHighSchool _educationLevelsHighSchool;     // Направление подготовки
    private DevelopForm _developForm;     // Форма освоения
    private Course _course;     // Курс
    private Date _beginDate;     // Дата начала срока проведения аттестации
    private Date _endDate;     // Дата окончания срока проведения аттестации

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    /**
     * @param educationLevelsHighSchool Направление подготовки. Свойство не может быть null.
     */
    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        dirty(_educationLevelsHighSchool, educationLevelsHighSchool);
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Дата начала срока проведения аттестации.
     */
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала срока проведения аттестации.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания срока проведения аттестации.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания срока проведения аттестации.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof AdmitToPassStatateExamStuListExtractGen)
        {
            setEducationLevelsHighSchool(((AdmitToPassStatateExamStuListExtract)another).getEducationLevelsHighSchool());
            setDevelopForm(((AdmitToPassStatateExamStuListExtract)another).getDevelopForm());
            setCourse(((AdmitToPassStatateExamStuListExtract)another).getCourse());
            setBeginDate(((AdmitToPassStatateExamStuListExtract)another).getBeginDate());
            setEndDate(((AdmitToPassStatateExamStuListExtract)another).getEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AdmitToPassStatateExamStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AdmitToPassStatateExamStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new AdmitToPassStatateExamStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "educationLevelsHighSchool":
                    return obj.getEducationLevelsHighSchool();
                case "developForm":
                    return obj.getDevelopForm();
                case "course":
                    return obj.getCourse();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "educationLevelsHighSchool":
                    obj.setEducationLevelsHighSchool((EducationLevelsHighSchool) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationLevelsHighSchool":
                        return true;
                case "developForm":
                        return true;
                case "course":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationLevelsHighSchool":
                    return true;
                case "developForm":
                    return true;
                case "course":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "educationLevelsHighSchool":
                    return EducationLevelsHighSchool.class;
                case "developForm":
                    return DevelopForm.class;
                case "course":
                    return Course.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AdmitToPassStatateExamStuListExtract> _dslPath = new Path<AdmitToPassStatateExamStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AdmitToPassStatateExamStuListExtract");
    }
            

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassStatateExamStuListExtract#getEducationLevelsHighSchool()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
    {
        return _dslPath.educationLevelsHighSchool();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassStatateExamStuListExtract#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassStatateExamStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Дата начала срока проведения аттестации.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassStatateExamStuListExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания срока проведения аттестации.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassStatateExamStuListExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    public static class Path<E extends AdmitToPassStatateExamStuListExtract> extends ListStudentExtract.Path<E>
    {
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _educationLevelsHighSchool;
        private DevelopForm.Path<DevelopForm> _developForm;
        private Course.Path<Course> _course;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassStatateExamStuListExtract#getEducationLevelsHighSchool()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
        {
            if(_educationLevelsHighSchool == null )
                _educationLevelsHighSchool = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_EDUCATION_LEVELS_HIGH_SCHOOL, this);
            return _educationLevelsHighSchool;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassStatateExamStuListExtract#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassStatateExamStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Дата начала срока проведения аттестации.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassStatateExamStuListExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(AdmitToPassStatateExamStuListExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания срока проведения аттестации.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassStatateExamStuListExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(AdmitToPassStatateExamStuListExtractGen.P_END_DATE, this);
            return _endDate;
        }

        public Class getEntityClass()
        {
            return AdmitToPassStatateExamStuListExtract.class;
        }

        public String getEntityName()
        {
            return "admitToPassStatateExamStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
