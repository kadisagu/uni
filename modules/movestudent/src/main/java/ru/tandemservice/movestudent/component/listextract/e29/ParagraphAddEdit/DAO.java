/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e29.ParagraphAddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.GiveDiplExtStuListExtract;
import ru.tandemservice.movestudent.utils.ExtEducationLevelsHighSchoolSelectModel;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;

import java.util.Collection;

/**
 * @author ListExtractComponentGenerator
 * @since 17.09.2012
 */
public class DAO extends AbstractListParagraphAddEditDAO<GiveDiplExtStuListExtract, Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(Model model)
    {
        super.prepare(model);
        final OrgUnit orgUnit = model.getParagraph().getOrder().getOrgUnit();
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));

        model.setGroupListModel(new GroupSelectModel(new ExtEduModel(model), model, model, orgUnit)
                                        .showChildLevels(true));

        model.setEducationLevelsHighSchoolListModel(new ExtEducationLevelsHighSchoolSelectModel(model, null, orgUnit));
        model.setDevelopFormModel(EducationOrgUnitUtil.getDevelopFormSelectModel(model, orgUnit));
        model.setDevelopConditionModel(EducationOrgUnitUtil.getDevelopConditionSelectModel(model, orgUnit));
        model.setDevelopTechModel(EducationOrgUnitUtil.getDevelopTechSelectModel(model, orgUnit));
        model.setDevelopPeriodModel(EducationOrgUnitUtil.getDevelopPeriodSelectModel(model, orgUnit));

        //заполняем поля сохраненными данными
        if (model.getParagraphId() != null)
        {
            GiveDiplExtStuListExtract extract = model.getFirstExtract();
            model.setCourse(extract.getCourse());
            model.setGroup(extract.getGroup());
            model.setCompensationType(extract.getCompensationType());
            model.setEducationLevelsHighSchool(extract.getEducationLevelsHighSchool());
            model.setDevelopForm(extract.getDevelopForm());
            model.setDevelopCondition(extract.getDevelopCondition());
            model.setDevelopTech(extract.getDevelopTech());
            model.setDevelopPeriod(extract.getDevelopPeriod());
            model.setPrintDiplomaQualification(extract.isPrintDiplomaQualification());
        }
    }

    @Override
    @SuppressWarnings("deprecation")
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COURSE, model.getCourse()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopForm()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopCondition()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_TECH, model.getDevelopTech()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_PERIOD, model.getDevelopPeriod()));

        if (model.getEducationLevelsHighSchool() != null)
        {
            StructureEducationLevels levels = model.getEducationLevelsHighSchool().getEducationLevel().getLevelType();

            if (levels != null && (levels.isSpecialization() || levels.isProfile()))
            {
                builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));
            }
            else
            {
                Collection<Long> hsIDs = MoveStudentDaoFacade.getMoveStudentDao().getChildSpecializationIDs(model.getEducationLevelsHighSchool(), model.getParagraph().getOrder().getOrgUnit(), null);
                hsIDs.add(model.getEducationLevelsHighSchool().getId());
                builder.add(MQExpression.in(STUDENT_ALIAS, Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + ".id", hsIDs));
            }
        }

        if (model.getGroup() != null)
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        if (model.getCompensationType() != null)
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));
    }

    @Override
    protected GiveDiplExtStuListExtract createNewInstance(Model model)
    {
        return new GiveDiplExtStuListExtract();
    }

    @Override
    protected void fillExtract(GiveDiplExtStuListExtract extract, Student student, Model model)
    {
        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setEducationLevelsHighSchool(model.getEducationLevelsHighSchool());
        extract.setDevelopForm(model.getDevelopForm());
        extract.setDevelopCondition(model.getDevelopCondition());
        extract.setDevelopTech(model.getDevelopTech());
        extract.setDevelopPeriod(model.getDevelopPeriod());
        extract.setCompensationType(model.getCompensationType());
        extract.setStudentStatusOld(extract.getEntity().getStatus());
        extract.setStudentStatusNew(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_GRADUATED_WITH_DIPLOMA));
        extract.setPrintDiplomaQualification(model.isPrintDiplomaQualification());
    }
}