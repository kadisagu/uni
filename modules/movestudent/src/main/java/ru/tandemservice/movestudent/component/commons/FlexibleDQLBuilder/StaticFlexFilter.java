/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

/**
 * @author Nikolay Fedorovskih
 * @since 03.12.2013
 */
public class StaticFlexFilter implements IFlexFilter
{
    private String _path;
    private String _alias;

    private Object[] _staticValues;

    public StaticFlexFilter(String path, String alias, Object... staticValues)
    {
        _path = path;
        _alias = alias;
        _staticValues = staticValues;
    }

    @Override
    public boolean addToBuilder(IDataSettings dataSettings, DQLSelectBuilder builder, IJoinCreator joinCreator)
    {
        joinCreator.checkAlias(_alias);
        if (_staticValues.length == 1)
            builder.where(DQLExpressions.eq(DQLExpressions.property(_alias, _path), DQLExpressions.commonValue(_staticValues[0])));
        else
            builder.where(DQLExpressions.in(DQLExpressions.property(_alias, _path), _staticValues));
        return true;
    }

    @Override
    public String getAlias()
    {
        return _alias;
    }
}