/* $Id$ */
package ru.tandemservice.movestudent.utils;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao;
import org.tandemframework.tapsupport.component.selection.BaseSelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.catalog.codes.YearDistributionCodes;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 31.01.2014
 */
public class SessionPartModel extends BaseSelectModel implements ISingleSelectModel
{
    protected static final String PART_ALIAS = "part";

    protected DQLSelectBuilder createBuilder(String filter, Object o)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(YearDistributionPart.class, PART_ALIAS)
                .column(PART_ALIAS)
                .where(eq(property(PART_ALIAS, YearDistributionPart.yearDistribution().inUse()), value(Boolean.TRUE)))
                .where(eq(property(PART_ALIAS, YearDistributionPart.yearDistribution().code()), value(YearDistributionCodes.YEAR_DISTRIBUTION_2)));

        if (o != null)
            builder.where(eq(property(PART_ALIAS + ".id"), commonValue(o, PropertyType.LONG)));

        return builder;
    }

    @Override
    public Object getValue(Object primaryKey)
    {
        return new DQLListResultBuilder(createBuilder(null, primaryKey)).getSingleValue();
    }

    @Override
    public ListResult findValues(String filter)
    {
        return new DQLListResultBuilder(createBuilder(filter, null)).findOptions();
    }

    @Override
    public String getLabelFor(Object value, int columnIndex)
    {
        return getSessionShortTitle((YearDistributionPart) value, GrammaCase.NOMINATIVE);
    }

    public static String getSessionShortTitle(YearDistributionPart yearDistributionPart, GrammaCase grammaCase)
    {
        return DeclinableManager.instance().dao().getPropertyValue(
                yearDistributionPart, YearDistributionPart.DECLINATION_PROPERTY_SESSION_SHORT_TITLE, DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(grammaCase));
    }
}