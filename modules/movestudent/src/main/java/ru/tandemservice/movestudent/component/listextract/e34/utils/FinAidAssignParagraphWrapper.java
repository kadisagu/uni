/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e34.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 03.10.2012
 */
public class FinAidAssignParagraphWrapper implements Comparable<FinAidAssignParagraphWrapper>
{
    private final CompensationType _compensationType;
    private final DevelopForm _developForm;
    private final DevelopCondition _developCondition;
    private final DevelopTech _developTech;
    private final DevelopPeriod _developPeriod;
    private final int _month;
    private final int _year;
    private final String _eduBaseText;
    private final ListStudentExtract _firstExtract;

    public FinAidAssignParagraphWrapper(CompensationType compensationType,DevelopForm developForm, DevelopCondition developCondition, DevelopTech developTech, DevelopPeriod developPeriod, int month, int year, String eduBaseText, ListStudentExtract firstExtract)
    {
        _compensationType = compensationType;
        _developForm = developForm;
        _developCondition = developCondition;
        _developTech = developTech;
        _developPeriod = developPeriod;
        _month = month;
        _year = year;
        _eduBaseText = eduBaseText;
        _firstExtract = firstExtract;
    }

    private final List<FinAidAssignParagraphPartWrapper> _paragraphPartWrapperList = new ArrayList<>();

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public int getMonth()
    {
        return _month;
    }

    public int getYear()
    {
        return _year;
    }

    public List<FinAidAssignParagraphPartWrapper> getParagraphPartWrapperList()
    {
        return _paragraphPartWrapperList;
    }

    public String getEduBaseText()
    {
        return _eduBaseText;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FinAidAssignParagraphWrapper))
            return false;

        FinAidAssignParagraphWrapper that = (FinAidAssignParagraphWrapper) o;

        return
        (_compensationType == null ? that.getCompensationType() == null : _compensationType.equals(that.getCompensationType())) &&
        _developForm.equals(that.getDevelopForm()) &&
        _developCondition.equals(that.getDevelopCondition()) &&
        _developTech.equals(that.getDevelopTech()) &&
        _developPeriod.equals(that.getDevelopPeriod()) &&
        _month == that.getMonth() &&
        _year == that.getYear() &&
        _eduBaseText.equals(that.getEduBaseText());
    }

    @Override
    public int hashCode()
    {
        return _compensationType.hashCode() & _developForm.hashCode() & _developCondition.hashCode() & _developTech.hashCode() & _developPeriod.hashCode() & ((Integer) _month).hashCode() & ((Integer) _year).hashCode() & _eduBaseText.hashCode();
    }

    @Override
    public int compareTo(FinAidAssignParagraphWrapper o)
    {
        if (_compensationType == null || o.getCompensationType() == null)
        {
            if (!(_compensationType == null && o.getCompensationType() == null))
                return _compensationType == null ? 1 : -1;

        } else if (_compensationType.getCode().compareTo(o.getCompensationType().getCode()) != 0)
            return _compensationType.getCode().compareTo(o.getCompensationType().getCode());

        int result = _developForm.getCode().compareTo(o.getDevelopForm().getCode());

        if (result == 0)
            result = _developCondition.getCode().compareTo(o.getDevelopCondition().getCode());

        if (result == 0)
            result = _developTech.getCode().compareTo(o.getDevelopTech().getCode());

        if (result == 0)
            result = _developPeriod.getCode().compareTo(o.getDevelopPeriod().getCode());

        if (result == 0)
            result = ((Integer) _month).compareTo(o.getMonth());

        if (result == 0)
            result = ((Integer) _year).compareTo(o.getYear());

        if (result == 0)
            result = _eduBaseText.compareTo(o.getEduBaseText());

        return result;
    }
}