/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e104;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.TransferBetweenTerritorialStuExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 25.10.2013
 */
public class TransferBetweenTerritorialStuExtractPrint implements IPrintFormCreator<TransferBetweenTerritorialStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, TransferBetweenTerritorialStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        EducationOrgUnit educationOrgUnitOld = extract.getEducationOrgUnitOld();
        EducationOrgUnit educationOrgUnitNew = extract.getEducationOrgUnitNew();

        EducationLevels educationLevelOld = educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel();
        EducationLevels educationLevelNew = educationOrgUnitNew.getEducationLevelHighSchool().getEducationLevel();

        modifier.put("course", extract.getCourseOld().getTitle());

        modifier.put("formativeOrgUnitOld_P", educationOrgUnitOld.getFormativeOrgUnit().getPrepositionalCaseTitle() != null ? educationOrgUnitOld.getFormativeOrgUnit().getPrepositionalCaseTitle() : educationOrgUnitOld.getFormativeOrgUnit().getPrintTitle());
        modifier.put("formativeOrgUnitNew_P", educationOrgUnitNew.getFormativeOrgUnit().getPrepositionalCaseTitle() != null ? educationOrgUnitNew.getFormativeOrgUnit().getPrepositionalCaseTitle() : educationOrgUnitNew.getFormativeOrgUnit().getPrintTitle());

        modifier.put("territorialOrgUnitOld_P", educationOrgUnitOld.getTerritorialOrgUnit().getTerritorialTitle().equals(TopOrgUnit.getInstance().getTerritorialTitle()) ? "" :" (" + educationOrgUnitOld.getTerritorialOrgUnit().getPrintTitle() + ")");
        modifier.put("territorialOrgUnitNew_P", educationOrgUnitNew.getTerritorialOrgUnit().getTerritorialTitle().equals(TopOrgUnit.getInstance().getTerritorialTitle()) ? "" :" (" + educationOrgUnitNew.getTerritorialOrgUnit().getPrintTitle() + ")");

        CommonExtractPrint.modifyEducationStr(modifier, educationLevelOld, new String[]{"educationStrOld"});

        StructureEducationLevels levelOld = (educationLevelOld.isProfileOrSpecialization() ? educationLevelOld.getParentLevel() : educationLevelOld).getLevelType();

        String educationStrNew = levelOld.isSpecialty() ? "указанной специальности" : levelOld.isMaster() ? "указанному магистерскому направлению" : levelOld.isBachelor() ? "указанному направлению" : "";
        String fefuEducationStrNew = levelOld.isSpecialty() ? "указанной специальности" : levelOld.isMaster() || levelOld.isBachelor() ? "указанному направлению" : "";

        // если нет профиля или специализации у старого НП
        if (!educationLevelOld.isProfileOrSpecialization())
        {
            // если есть профиль или специализации у нового НП
            if (educationLevelNew.isProfileOrSpecialization())
            {
                educationStrNew += (levelOld.isSpecialty() ? " и специализации" : levelOld.isMaster() ? " и магистерской программе" : levelOld.isBachelor() ? " и профилю" : "") + " «" + educationOrgUnitNew.getEducationLevelHighSchool().getTitle() + "»";
                fefuEducationStrNew += (levelOld.isSpecialty() ? ", специализации" : levelOld.isMaster() ? ", магистерской программе" : levelOld.isBachelor() ? ", профилю" : "") + " «" + educationOrgUnitNew.getEducationLevelHighSchool().getTitle() + "»";
            }
        }
        else
        {
            // если новое НП является направлением или специализацией (когда новое НП не выбрано)
            if (!educationLevelNew.isProfileOrSpecialization())
                educationLevelOld = educationLevelOld.getParentLevel();

            if (!educationLevelOld.equals(educationLevelNew))
            {
                educationStrNew += (levelOld.isSpecialty() ? " и специализации" : levelOld.isMaster() ? " и магистерской программе" : levelOld.isBachelor() ? " и профилю" : "") + " «" + educationOrgUnitNew.getEducationLevelHighSchool().getTitle() + "»";
                fefuEducationStrNew += (levelOld.isSpecialty() ? ", специализации" : levelOld.isMaster() ? ", магистерской программе" : levelOld.isBachelor() ? ", профилю" : "") + " «" + educationOrgUnitNew.getEducationLevelHighSchool().getTitle() + "»";
            }
            else
            {
                educationStrNew += levelOld.isSpecialty() ? " и специализации" : levelOld.isMaster() ? " и магистерской программе" : levelOld.isBachelor() ? " и профилю" : "";
                fefuEducationStrNew += levelOld.isSpecialty() ? ", специализации" : levelOld.isMaster() ? ", магистерской программе" : levelOld.isBachelor() ? ", профилю" : "";
            }
        }

        modifier.put("educationStrNew_D", educationStrNew);
        modifier.put("fefuEducationStrNew_D", fefuEducationStrNew);

        CommonExtractPrint.initEducationType(modifier, educationOrgUnitOld, "Old");

        if (extract.isChangeEduPlan())
        {
            RtfString changeEduPlan = new RtfString();
            changeEduPlan.par().append("Разницу в учебных планах ликвидировать в срок до ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getChangeEduPlanDeadlineDate())).append(".").par();
            modifier.put("changeEduPlan", changeEduPlan);
        }
        else
            modifier.put("changeEduPlan", "");

        CommonExtractPrint.initEducationType(modifier, educationOrgUnitOld, "Old");

        additionalModify(modifier, extract);
        modifier.modify(document);
        return document;
    }

    public void additionalModify(RtfInjectModifier modifier, TransferBetweenTerritorialStuExtract extract)
    {
    }
}