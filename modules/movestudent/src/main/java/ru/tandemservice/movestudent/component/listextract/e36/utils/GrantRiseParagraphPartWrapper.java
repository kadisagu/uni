/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e36.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 22.11.2012
 */
public class GrantRiseParagraphPartWrapper implements Comparable<GrantRiseParagraphPartWrapper>
{
    private final Course _course;
    private final Group _group;
    private final ListStudentExtract _firstExtract;

    public GrantRiseParagraphPartWrapper(Course course, Group group, ListStudentExtract firstExtract)
    {
        _course = course;
        _group = group;
        _firstExtract = firstExtract;
    }

    private final List<GrantRiseExtractWrapper> _extractWrapperList = new ArrayList<GrantRiseExtractWrapper>();

    public Course getCourse()
    {
        return _course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public List<GrantRiseExtractWrapper> getExtractWrapperList()
    {
        return _extractWrapperList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof GrantRiseParagraphPartWrapper))
            return false;

        GrantRiseParagraphPartWrapper that = (GrantRiseParagraphPartWrapper) o;

        return _course.equals(that.getCourse()) && _group.equals(that.getGroup());
    }

    @Override
    public int hashCode()
    {
        return _course.hashCode() & _group.hashCode();
    }

    @Override
    public int compareTo(GrantRiseParagraphPartWrapper o)
    {
        int result = ((Integer) _course.getIntValue()).compareTo((Integer) o.getCourse().getIntValue());
        if (result == 0)
            result = _group.getTitle().compareTo(o.getGroup().getTitle());

        return result;
    }
}