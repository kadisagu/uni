/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e47;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AddGroupManagerStuExtract;
import ru.tandemservice.movestudent.entity.GroupToExtractRelation;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.List;
import java.util.Map;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 10.08.2012
 */
public class AddGroupManagerStuExtractDao extends UniBaseDao implements IExtractComponentDao<AddGroupManagerStuExtract>
{
    public void doCommit(AddGroupManagerStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        Student student = extract.getEntity();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GroupToExtractRelation.class, "ge");
        builder.where(DQLExpressions.eq(DQLExpressions.property("ge", GroupToExtractRelation.extract().id()), DQLExpressions.value(extract.getId())));
        builder.column(DQLExpressions.property(GroupToExtractRelation.group().fromAlias("ge"))).distinct();

        List<Group> groupList = builder.createStatement(getSession()).list();

        if (groupList.isEmpty())
            throw new ApplicationException("Невозможно провести приказ, поскольку не указаны группы, в которую назначается старостой " + student.getFio() + ".");

        for(Group group : groupList)
        {
            if (UniDaoFacade.getGroupDao().isCaptainStudent(group, student))
                throw new ApplicationException("Невозможно провести приказ, поскольку студент уже является старостой группы " + group.getTitle() + ".");

            UniDaoFacade.getGroupDao().addCaptainStudent(group, student);
        }

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            extract.setPrevOrderDate(orderData.getPublicOrderOrderDate());
            extract.setPrevOrderNumber(orderData.getPublicOrderOrderNumber());
        }
        orderData.setPublicOrderOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setPublicOrderOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);
    }

    public void doRollback(AddGroupManagerStuExtract extract, Map parameters)
    {
        Student student = extract.getEntity();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GroupToExtractRelation.class, "ge");
        builder.where(DQLExpressions.eq(DQLExpressions.property("ge", GroupToExtractRelation.extract().id()), DQLExpressions.value(extract.getId())));
        builder.column(DQLExpressions.property(GroupToExtractRelation.group().fromAlias("ge"))).distinct();

        List<Group> groupList = builder.createStatement(getSession()).list();

        for(Group group : groupList)
        {
            UniDaoFacade.getGroupDao().deleteCaptainStudent(group, student);
        }

        // возвращаем предыдущие номер и дату приказа
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setPublicOrderOrderDate(extract.getPrevOrderDate());
        orderData.setPublicOrderOrderNumber(extract.getPrevOrderNumber());
        getSession().saveOrUpdate(orderData);
    }
}