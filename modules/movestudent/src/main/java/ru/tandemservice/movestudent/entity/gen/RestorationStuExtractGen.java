package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.RestorationStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О восстановлении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RestorationStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.RestorationStuExtract";
    public static final String ENTITY_NAME = "restorationStuExtract";
    public static final int VERSION_HASH = -18992240;
    private static IEntityMeta ENTITY_META;

    public static final String P_RESTORATION_DATE = "restorationDate";

    private Date _restorationDate;     // Дата восстановления

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата восстановления.
     */
    public Date getRestorationDate()
    {
        return _restorationDate;
    }

    /**
     * @param restorationDate Дата восстановления.
     */
    public void setRestorationDate(Date restorationDate)
    {
        dirty(_restorationDate, restorationDate);
        _restorationDate = restorationDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RestorationStuExtractGen)
        {
            setRestorationDate(((RestorationStuExtract)another).getRestorationDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RestorationStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RestorationStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new RestorationStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "restorationDate":
                    return obj.getRestorationDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "restorationDate":
                    obj.setRestorationDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "restorationDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "restorationDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "restorationDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RestorationStuExtract> _dslPath = new Path<RestorationStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RestorationStuExtract");
    }
            

    /**
     * @return Дата восстановления.
     * @see ru.tandemservice.movestudent.entity.RestorationStuExtract#getRestorationDate()
     */
    public static PropertyPath<Date> restorationDate()
    {
        return _dslPath.restorationDate();
    }

    public static class Path<E extends RestorationStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<Date> _restorationDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата восстановления.
     * @see ru.tandemservice.movestudent.entity.RestorationStuExtract#getRestorationDate()
     */
        public PropertyPath<Date> restorationDate()
        {
            if(_restorationDate == null )
                _restorationDate = new PropertyPath<Date>(RestorationStuExtractGen.P_RESTORATION_DATE, this);
            return _restorationDate;
        }

        public Class getEntityClass()
        {
            return RestorationStuExtract.class;
        }

        public String getEntityName()
        {
            return "restorationStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
