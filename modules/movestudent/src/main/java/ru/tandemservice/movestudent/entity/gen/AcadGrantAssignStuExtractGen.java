package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AcadGrantAssignStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О назначении академической стипендии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AcadGrantAssignStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.AcadGrantAssignStuExtract";
    public static final String ENTITY_NAME = "acadGrantAssignStuExtract";
    public static final int VERSION_HASH = -1787444607;
    private static IEntityMeta ENTITY_META;

    public static final String P_START_DATE = "startDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_REASON_STR = "reasonStr";
    public static final String P_PREV_BEGIN_DATE = "prevBeginDate";
    public static final String P_PREV_END_DATE = "prevEndDate";

    private Date _startDate;     // Дата начала выплаты стипендии
    private Date _endDate;     // Дата окончания выплаты стипендии
    private String _reasonStr;     // В связи с чем назначается стипендия
    private Date _prevBeginDate;     // Дата начала выплаты академической стипендии по предыдущему приказу
    private Date _prevEndDate;     // Дата окончания выплаты академической стипендии по предыдущему приказу

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата начала выплаты стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Дата начала выплаты стипендии. Свойство не может быть null.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Дата окончания выплаты стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания выплаты стипендии. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return В связи с чем назначается стипендия. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getReasonStr()
    {
        return _reasonStr;
    }

    /**
     * @param reasonStr В связи с чем назначается стипендия. Свойство не может быть null.
     */
    public void setReasonStr(String reasonStr)
    {
        dirty(_reasonStr, reasonStr);
        _reasonStr = reasonStr;
    }

    /**
     * @return Дата начала выплаты академической стипендии по предыдущему приказу.
     */
    public Date getPrevBeginDate()
    {
        return _prevBeginDate;
    }

    /**
     * @param prevBeginDate Дата начала выплаты академической стипендии по предыдущему приказу.
     */
    public void setPrevBeginDate(Date prevBeginDate)
    {
        dirty(_prevBeginDate, prevBeginDate);
        _prevBeginDate = prevBeginDate;
    }

    /**
     * @return Дата окончания выплаты академической стипендии по предыдущему приказу.
     */
    public Date getPrevEndDate()
    {
        return _prevEndDate;
    }

    /**
     * @param prevEndDate Дата окончания выплаты академической стипендии по предыдущему приказу.
     */
    public void setPrevEndDate(Date prevEndDate)
    {
        dirty(_prevEndDate, prevEndDate);
        _prevEndDate = prevEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof AcadGrantAssignStuExtractGen)
        {
            setStartDate(((AcadGrantAssignStuExtract)another).getStartDate());
            setEndDate(((AcadGrantAssignStuExtract)another).getEndDate());
            setReasonStr(((AcadGrantAssignStuExtract)another).getReasonStr());
            setPrevBeginDate(((AcadGrantAssignStuExtract)another).getPrevBeginDate());
            setPrevEndDate(((AcadGrantAssignStuExtract)another).getPrevEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AcadGrantAssignStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AcadGrantAssignStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new AcadGrantAssignStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "startDate":
                    return obj.getStartDate();
                case "endDate":
                    return obj.getEndDate();
                case "reasonStr":
                    return obj.getReasonStr();
                case "prevBeginDate":
                    return obj.getPrevBeginDate();
                case "prevEndDate":
                    return obj.getPrevEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "reasonStr":
                    obj.setReasonStr((String) value);
                    return;
                case "prevBeginDate":
                    obj.setPrevBeginDate((Date) value);
                    return;
                case "prevEndDate":
                    obj.setPrevEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "startDate":
                        return true;
                case "endDate":
                        return true;
                case "reasonStr":
                        return true;
                case "prevBeginDate":
                        return true;
                case "prevEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "startDate":
                    return true;
                case "endDate":
                    return true;
                case "reasonStr":
                    return true;
                case "prevBeginDate":
                    return true;
                case "prevEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "startDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "reasonStr":
                    return String.class;
                case "prevBeginDate":
                    return Date.class;
                case "prevEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AcadGrantAssignStuExtract> _dslPath = new Path<AcadGrantAssignStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AcadGrantAssignStuExtract");
    }
            

    /**
     * @return Дата начала выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuExtract#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Дата окончания выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return В связи с чем назначается стипендия. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuExtract#getReasonStr()
     */
    public static PropertyPath<String> reasonStr()
    {
        return _dslPath.reasonStr();
    }

    /**
     * @return Дата начала выплаты академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuExtract#getPrevBeginDate()
     */
    public static PropertyPath<Date> prevBeginDate()
    {
        return _dslPath.prevBeginDate();
    }

    /**
     * @return Дата окончания выплаты академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuExtract#getPrevEndDate()
     */
    public static PropertyPath<Date> prevEndDate()
    {
        return _dslPath.prevEndDate();
    }

    public static class Path<E extends AcadGrantAssignStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _startDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<String> _reasonStr;
        private PropertyPath<Date> _prevBeginDate;
        private PropertyPath<Date> _prevEndDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата начала выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuExtract#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(AcadGrantAssignStuExtractGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Дата окончания выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(AcadGrantAssignStuExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return В связи с чем назначается стипендия. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuExtract#getReasonStr()
     */
        public PropertyPath<String> reasonStr()
        {
            if(_reasonStr == null )
                _reasonStr = new PropertyPath<String>(AcadGrantAssignStuExtractGen.P_REASON_STR, this);
            return _reasonStr;
        }

    /**
     * @return Дата начала выплаты академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuExtract#getPrevBeginDate()
     */
        public PropertyPath<Date> prevBeginDate()
        {
            if(_prevBeginDate == null )
                _prevBeginDate = new PropertyPath<Date>(AcadGrantAssignStuExtractGen.P_PREV_BEGIN_DATE, this);
            return _prevBeginDate;
        }

    /**
     * @return Дата окончания выплаты академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuExtract#getPrevEndDate()
     */
        public PropertyPath<Date> prevEndDate()
        {
            if(_prevEndDate == null )
                _prevEndDate = new PropertyPath<Date>(AcadGrantAssignStuExtractGen.P_PREV_END_DATE, this);
            return _prevEndDate;
        }

        public Class getEntityClass()
        {
            return AcadGrantAssignStuExtract.class;
        }

        public String getEntityName()
        {
            return "acadGrantAssignStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
