/* $Id: Controller.java.vm 7834 2009-05-06 09:39:03Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e22.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.ChangeReviewerStuListExtract;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author ListExtractComponentGenerator
 * @since 19.02.2011
 */
public class Controller extends AbstractListParagraphAddEditController<ChangeReviewerStuListExtract, IDAO, Model>
{
      @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        model.setSearchListSettingsKey("StudentListParagraphAddEdit." + model.getParagraphType().getCode() + ".");
        prepareListDataSource(component);
        DAO.prepareColumns(model);
    }



    private void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<Student> dataSource = UniBaseUtils.createDataSource(component, getDao());

        CheckboxColumn checkboxColumn = new CheckboxColumn("checkbox", "", true);
        if (model.getSelectedItemList() != null)
            checkboxColumn.setSelectedObjects(model.getSelectedItemList());
        checkboxColumn.setDisabledProperty(AbstractListParagraphAddEditModel.P_STUDENT_DISABLED);
        checkboxColumn.setDisplayTotal(true);

        dataSource.addColumn(checkboxColumn);

        prepareListDataSource(component, dataSource);
        model.setDataSource(dataSource);
    }

        @Override
    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Дополнительный статус", Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("status", "Состояние", Model.STATUS_KEY, SimpleFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("reviewerName", "ФИО рецензента").setRequired(true));
        dataSource.addColumn(new BlockColumn("reviewerDegree", "Степень, звание, должность").setRequired(true));
        dataSource.setOrder("status", OrderDirection.asc);
    }
}
