package ru.tandemservice.movestudent.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "SpellCheckingInspection"})
public class MS_movestudent_2x9x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[] {
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.0")
		};
    }

	private void fillTranferDateFromOrder(DBTool tool, String tableName) throws Exception
	{
		final SQLUpdateQuery upd = new SQLUpdateQuery(tableName, "t");
		upd.set("transferdate_p", "o.commitdate_p");
		upd.getUpdatedTableFrom()
				.innerJoin(SQLFrom.table("abstractstudentextract_t", "a"), "t.id=a.id")
				.innerJoin(SQLFrom.table("abstractstudentparagraph_t", "p"), "a.paragraph_id=p.id")
				.innerJoin(SQLFrom.table("abstractstudentorder_t", "o"), "p.order_id=o.id");

		final int ret = tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(upd));
		if (Debug.isEnabled()) {
			System.out.println(this.getClass().getSimpleName() + ": " + ret + " transferDate filled from orders in " + tableName);
		}
	}

    @Override
    public void run(DBTool tool) throws Exception
    {
		// сущность changeProfileAndGroupStuListExtract - создано свойство transferDate
		tool.createColumn("chngprflandgrpstlstextrct_t", new DBColumn("transferdate_p", DBType.DATE));
		// Заполняем датой приказа
		fillTranferDateFromOrder(tool, "chngprflandgrpstlstextrct_t");

		// сущность groupTransferExtStuListExtract - создано свойство transferDate
		tool.createColumn("grptrnsfrextstlstextrct_t", new DBColumn("transferdate_p", DBType.DATE));
		// Заполняем датой приказа
		fillTranferDateFromOrder(tool, "grptrnsfrextstlstextrct_t");
    }
}