/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e9.MultipleParagraphAdd;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListMultipleParagraphAdd.AbstractListMultipleParagraphAddDAO;
import ru.tandemservice.movestudent.entity.GraduateSuccessStuListExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author ListExtractComponentGenerator
 * @since 17.06.2009
 */
public class DAO extends AbstractListMultipleParagraphAddDAO<GraduateSuccessStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setStatusNew(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_GRADUATED_WITH_DIPLOMA));
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq("p", Group.L_COURSE, model.getCourse()));
    }

    @Override
    protected GraduateSuccessStuListExtract createNewInstance(Model model)
    {
        return new GraduateSuccessStuListExtract();
    }

    @Override
    protected void fillExtract(GraduateSuccessStuListExtract extract, Model model, Group group)
    {
        extract.setGroup(group);
        extract.setCourse(model.getCourse());
        extract.setStatusOld(extract.getEntity().getStatus());
        extract.setStatusNew(model.getStatusNew());
    }
}