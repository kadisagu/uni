package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.entity.gen.*;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.IAbstractExtract;

import java.util.List;

/**
 * Параграф прочего приказа по студентам
 */
public class StudentOtherParagraph extends StudentOtherParagraphGen
{
    @Override
    public List<? extends IAbstractExtract> getExtractList()
    {
        return UniDaoFacade.getCoreDao().getList(OtherStudentExtract.class, IAbstractExtract.L_PARAGRAPH, this, IAbstractExtract.P_NUMBER);
    }
}