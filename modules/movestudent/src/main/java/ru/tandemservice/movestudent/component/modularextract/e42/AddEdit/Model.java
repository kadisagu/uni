/* $Id: Model.java.vm 11769 2010-03-05 07:11:49Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e42.AddEdit;

import java.util.Date;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.ChangePassDiplomaWorkPeriodStuExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 15.02.2011
 */
public class Model extends CommonModularStudentExtractAddEditModel<ChangePassDiplomaWorkPeriodStuExtract>
{
    Date _currentDate;
    Date _newDate;

    public Date getCurrentDate() {
        return _currentDate;
    }

    public void setCurrentDate(Date _currentDate) {
        this._currentDate = _currentDate;
    }

    public Date getNewDate() {
        return _newDate;
    }

    public void setNewDate(Date _newDate) {
        this._newDate = _newDate;
    }
}
