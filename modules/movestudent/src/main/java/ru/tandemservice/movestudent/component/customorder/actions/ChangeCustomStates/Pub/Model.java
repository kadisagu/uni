/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeCustomStates.Pub;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.BaseCustomActionPubModel;
import ru.tandemservice.movestudent.entity.custom.ChangeCustomStatesAction;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public class Model extends BaseCustomActionPubModel<ChangeCustomStatesAction>
{
    private String _oldStatesToDelete;
    private String _newStates;

    public String getOldStatesToDelete()
    {
        return _oldStatesToDelete;
    }

    public void setOldStatesToDelete(String oldStatesToDelete)
    {
        _oldStatesToDelete = oldStatesToDelete;
    }

    public String getNewStates()
    {
        return _newStates;
    }

    public void setNewStates(String newStates)
    {
        _newStates = newStates;
    }
}