/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e59;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract;

import java.util.Collections;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 23.08.2012
 */
public class WeekendChildOneAndHalfStuExtractPrint implements IPrintFormCreator<WeekendChildOneAndHalfStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, WeekendChildOneAndHalfStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        if (extract.isPayBenefit())
            modifier.put("payBenefit", extract.getPayBenefitText());
        else
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("payBenefit"), true, false);
        if (extract.isPayOnetimeBenefit())
            modifier.put("payOnetimeBenefit", extract.getPayOnetimeBenefitText());
        else
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("payOnetimeBenefit"), true, false);

        String freeAttendanceText = ApplicationRuntime.getProperty("freeAttendanceText");
        if(extract.isFreeAttendance() && !StringUtils.isEmpty(freeAttendanceText))
        {
            modifier.put("freeAttendance", " " + freeAttendanceText);
        }
        else
        {
            modifier.put("freeAttendance", "");
        }

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}