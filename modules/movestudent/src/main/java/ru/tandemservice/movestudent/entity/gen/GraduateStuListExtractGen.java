package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.GraduateStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * проект приказа «О выпуске студентов»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GraduateStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.GraduateStuListExtract";
    public static final String ENTITY_NAME = "graduateStuListExtract";
    public static final int VERSION_HASH = 1147594680;
    private static IEntityMeta ENTITY_META;

    public static final String P_GRADUATE_DATE = "graduateDate";
    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_STATUS_OLD = "statusOld";
    public static final String L_STATUS_NEW = "statusNew";
    public static final String P_PREV_GRADUATE_ORDER_DATE = "prevGraduateOrderDate";
    public static final String P_PREV_GRADUATE_ORDER_NUMBER = "prevGraduateOrderNumber";
    public static final String P_FINISHED_YEAR = "finishedYear";

    private Date _graduateDate;     // Дата выпуска
    private Course _course;     // Курс
    private Group _group;     // Группа
    private StudentStatus _statusOld;     // Состояние студента
    private StudentStatus _statusNew;     // Состояние студента
    private Date _prevGraduateOrderDate;     // Дата предыдущего приказа о выпуске
    private String _prevGraduateOrderNumber;     // Номер предыдущего приказа о выпуске
    private Integer _finishedYear;     // Год окончания на момент проведения приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата выпуска.
     */
    public Date getGraduateDate()
    {
        return _graduateDate;
    }

    /**
     * @param graduateDate Дата выпуска.
     */
    public void setGraduateDate(Date graduateDate)
    {
        dirty(_graduateDate, graduateDate);
        _graduateDate = graduateDate;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStatusOld()
    {
        return _statusOld;
    }

    /**
     * @param statusOld Состояние студента. Свойство не может быть null.
     */
    public void setStatusOld(StudentStatus statusOld)
    {
        dirty(_statusOld, statusOld);
        _statusOld = statusOld;
    }

    /**
     * @return Состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStatusNew()
    {
        return _statusNew;
    }

    /**
     * @param statusNew Состояние студента. Свойство не может быть null.
     */
    public void setStatusNew(StudentStatus statusNew)
    {
        dirty(_statusNew, statusNew);
        _statusNew = statusNew;
    }

    /**
     * @return Дата предыдущего приказа о выпуске.
     */
    public Date getPrevGraduateOrderDate()
    {
        initLazyForGet("prevGraduateOrderDate");
        return _prevGraduateOrderDate;
    }

    /**
     * @param prevGraduateOrderDate Дата предыдущего приказа о выпуске.
     */
    public void setPrevGraduateOrderDate(Date prevGraduateOrderDate)
    {
        initLazyForSet("prevGraduateOrderDate");
        dirty(_prevGraduateOrderDate, prevGraduateOrderDate);
        _prevGraduateOrderDate = prevGraduateOrderDate;
    }

    /**
     * @return Номер предыдущего приказа о выпуске.
     */
    @Length(max=255)
    public String getPrevGraduateOrderNumber()
    {
        initLazyForGet("prevGraduateOrderNumber");
        return _prevGraduateOrderNumber;
    }

    /**
     * @param prevGraduateOrderNumber Номер предыдущего приказа о выпуске.
     */
    public void setPrevGraduateOrderNumber(String prevGraduateOrderNumber)
    {
        initLazyForSet("prevGraduateOrderNumber");
        dirty(_prevGraduateOrderNumber, prevGraduateOrderNumber);
        _prevGraduateOrderNumber = prevGraduateOrderNumber;
    }

    /**
     * @return Год окончания на момент проведения приказа.
     */
    public Integer getFinishedYear()
    {
        return _finishedYear;
    }

    /**
     * @param finishedYear Год окончания на момент проведения приказа.
     */
    public void setFinishedYear(Integer finishedYear)
    {
        dirty(_finishedYear, finishedYear);
        _finishedYear = finishedYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof GraduateStuListExtractGen)
        {
            setGraduateDate(((GraduateStuListExtract)another).getGraduateDate());
            setCourse(((GraduateStuListExtract)another).getCourse());
            setGroup(((GraduateStuListExtract)another).getGroup());
            setStatusOld(((GraduateStuListExtract)another).getStatusOld());
            setStatusNew(((GraduateStuListExtract)another).getStatusNew());
            setPrevGraduateOrderDate(((GraduateStuListExtract)another).getPrevGraduateOrderDate());
            setPrevGraduateOrderNumber(((GraduateStuListExtract)another).getPrevGraduateOrderNumber());
            setFinishedYear(((GraduateStuListExtract)another).getFinishedYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GraduateStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GraduateStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new GraduateStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "graduateDate":
                    return obj.getGraduateDate();
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "statusOld":
                    return obj.getStatusOld();
                case "statusNew":
                    return obj.getStatusNew();
                case "prevGraduateOrderDate":
                    return obj.getPrevGraduateOrderDate();
                case "prevGraduateOrderNumber":
                    return obj.getPrevGraduateOrderNumber();
                case "finishedYear":
                    return obj.getFinishedYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "graduateDate":
                    obj.setGraduateDate((Date) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "statusOld":
                    obj.setStatusOld((StudentStatus) value);
                    return;
                case "statusNew":
                    obj.setStatusNew((StudentStatus) value);
                    return;
                case "prevGraduateOrderDate":
                    obj.setPrevGraduateOrderDate((Date) value);
                    return;
                case "prevGraduateOrderNumber":
                    obj.setPrevGraduateOrderNumber((String) value);
                    return;
                case "finishedYear":
                    obj.setFinishedYear((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "graduateDate":
                        return true;
                case "course":
                        return true;
                case "group":
                        return true;
                case "statusOld":
                        return true;
                case "statusNew":
                        return true;
                case "prevGraduateOrderDate":
                        return true;
                case "prevGraduateOrderNumber":
                        return true;
                case "finishedYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "graduateDate":
                    return true;
                case "course":
                    return true;
                case "group":
                    return true;
                case "statusOld":
                    return true;
                case "statusNew":
                    return true;
                case "prevGraduateOrderDate":
                    return true;
                case "prevGraduateOrderNumber":
                    return true;
                case "finishedYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "graduateDate":
                    return Date.class;
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "statusOld":
                    return StudentStatus.class;
                case "statusNew":
                    return StudentStatus.class;
                case "prevGraduateOrderDate":
                    return Date.class;
                case "prevGraduateOrderNumber":
                    return String.class;
                case "finishedYear":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GraduateStuListExtract> _dslPath = new Path<GraduateStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GraduateStuListExtract");
    }
            

    /**
     * @return Дата выпуска.
     * @see ru.tandemservice.movestudent.entity.GraduateStuListExtract#getGraduateDate()
     */
    public static PropertyPath<Date> graduateDate()
    {
        return _dslPath.graduateDate();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GraduateStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GraduateStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GraduateStuListExtract#getStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> statusOld()
    {
        return _dslPath.statusOld();
    }

    /**
     * @return Состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GraduateStuListExtract#getStatusNew()
     */
    public static StudentStatus.Path<StudentStatus> statusNew()
    {
        return _dslPath.statusNew();
    }

    /**
     * @return Дата предыдущего приказа о выпуске.
     * @see ru.tandemservice.movestudent.entity.GraduateStuListExtract#getPrevGraduateOrderDate()
     */
    public static PropertyPath<Date> prevGraduateOrderDate()
    {
        return _dslPath.prevGraduateOrderDate();
    }

    /**
     * @return Номер предыдущего приказа о выпуске.
     * @see ru.tandemservice.movestudent.entity.GraduateStuListExtract#getPrevGraduateOrderNumber()
     */
    public static PropertyPath<String> prevGraduateOrderNumber()
    {
        return _dslPath.prevGraduateOrderNumber();
    }

    /**
     * @return Год окончания на момент проведения приказа.
     * @see ru.tandemservice.movestudent.entity.GraduateStuListExtract#getFinishedYear()
     */
    public static PropertyPath<Integer> finishedYear()
    {
        return _dslPath.finishedYear();
    }

    public static class Path<E extends GraduateStuListExtract> extends ListStudentExtract.Path<E>
    {
        private PropertyPath<Date> _graduateDate;
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private StudentStatus.Path<StudentStatus> _statusOld;
        private StudentStatus.Path<StudentStatus> _statusNew;
        private PropertyPath<Date> _prevGraduateOrderDate;
        private PropertyPath<String> _prevGraduateOrderNumber;
        private PropertyPath<Integer> _finishedYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата выпуска.
     * @see ru.tandemservice.movestudent.entity.GraduateStuListExtract#getGraduateDate()
     */
        public PropertyPath<Date> graduateDate()
        {
            if(_graduateDate == null )
                _graduateDate = new PropertyPath<Date>(GraduateStuListExtractGen.P_GRADUATE_DATE, this);
            return _graduateDate;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GraduateStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GraduateStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GraduateStuListExtract#getStatusOld()
     */
        public StudentStatus.Path<StudentStatus> statusOld()
        {
            if(_statusOld == null )
                _statusOld = new StudentStatus.Path<StudentStatus>(L_STATUS_OLD, this);
            return _statusOld;
        }

    /**
     * @return Состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GraduateStuListExtract#getStatusNew()
     */
        public StudentStatus.Path<StudentStatus> statusNew()
        {
            if(_statusNew == null )
                _statusNew = new StudentStatus.Path<StudentStatus>(L_STATUS_NEW, this);
            return _statusNew;
        }

    /**
     * @return Дата предыдущего приказа о выпуске.
     * @see ru.tandemservice.movestudent.entity.GraduateStuListExtract#getPrevGraduateOrderDate()
     */
        public PropertyPath<Date> prevGraduateOrderDate()
        {
            if(_prevGraduateOrderDate == null )
                _prevGraduateOrderDate = new PropertyPath<Date>(GraduateStuListExtractGen.P_PREV_GRADUATE_ORDER_DATE, this);
            return _prevGraduateOrderDate;
        }

    /**
     * @return Номер предыдущего приказа о выпуске.
     * @see ru.tandemservice.movestudent.entity.GraduateStuListExtract#getPrevGraduateOrderNumber()
     */
        public PropertyPath<String> prevGraduateOrderNumber()
        {
            if(_prevGraduateOrderNumber == null )
                _prevGraduateOrderNumber = new PropertyPath<String>(GraduateStuListExtractGen.P_PREV_GRADUATE_ORDER_NUMBER, this);
            return _prevGraduateOrderNumber;
        }

    /**
     * @return Год окончания на момент проведения приказа.
     * @see ru.tandemservice.movestudent.entity.GraduateStuListExtract#getFinishedYear()
     */
        public PropertyPath<Integer> finishedYear()
        {
            if(_finishedYear == null )
                _finishedYear = new PropertyPath<Integer>(GraduateStuListExtractGen.P_FINISHED_YEAR, this);
            return _finishedYear;
        }

        public Class getEntityClass()
        {
            return GraduateStuListExtract.class;
        }

        public String getEntityName()
        {
            return "graduateStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
