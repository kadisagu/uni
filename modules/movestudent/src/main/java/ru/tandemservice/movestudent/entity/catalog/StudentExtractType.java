/* $Id$ */
package ru.tandemservice.movestudent.entity.catalog;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.meta.entity.data.IdValue;
import org.tandemframework.core.meta.entity.data.ItemPolicy;
import org.tandemframework.core.meta.entity.impl.PropertyMeta;
import org.tandemframework.core.runtime.EntityDataRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.tool.synchronization.SynchronizeMeta;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import ru.tandemservice.movestudent.entity.catalog.gen.StudentExtractTypeGen;

import java.util.Collection;
import java.util.Comparator;
import java.util.Stack;

public class StudentExtractType extends StudentExtractTypeGen implements IHierarchyItem
{
    public static final String P_DELETE_DISABLED = "deleteDisabled";

    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return getParent();
    }

    public boolean isDeleteDisabled()
    {
        if(null == getParent()) return true;

        IdValue idv = new IdValue();
        ItemPolicy itemPolicy;
        Collection naturalId = EntityRuntime.getMeta(this).getRoot().getNaturalId().getDeclaredProperties();

        if (naturalId.size() > 1)
        {
            for (Object meta : naturalId)
            {
                String property = ((PropertyMeta) meta).getName();
                idv.setPropertyValue(property, (String) getProperty(property));
            }

            itemPolicy = EntityDataRuntime.getEntityPolicy(StudentExtractType.ENTITY_NAME).getItemPolicy(idv);
        } else
        {
            String property = ((PropertyMeta) naturalId.toArray()[0]).getName();
            itemPolicy = EntityDataRuntime.getEntityPolicy(StudentExtractType.ENTITY_NAME).getItemPolicy((String) getProperty(property));
        }

        // системные элементы не могут быть удалены из справочника
        return itemPolicy != null && !SynchronizeMeta.temp.equals(itemPolicy.getSynchronize());

    }

    public static class ItemsComparator implements Comparator<StudentExtractType>
    {
        @Override
        public int compare(StudentExtractType o1, StudentExtractType o2)
        {
            Stack<StudentExtractType> parents1 = new Stack<StudentExtractType>();
            Stack<StudentExtractType> parents2 = new Stack<StudentExtractType>();

            while (o1 != null)
            {
                parents1.push(o1);
                o1 = o1.getParent();
            }

            while (o2 != null)
            {
                parents2.push(o2);
                o2 = o2.getParent();
            }

            int result = parents1.pop().getCode().compareTo(parents2.pop().getCode());

            if (result != 0){return result;}

            while (!parents1.empty() && !parents2.empty())
            {
                StudentExtractType p1 = parents1.pop();
                StudentExtractType p2 = parents2.pop();
                if (p1.equals(p2)) continue;
                result = p1.getTitle().compareToIgnoreCase(p2.getTitle());
                if (result == 0) result = p1.getCode().compareTo(p2.getCode());
                if (result != 0) return result;
            }

            if (!parents1.empty() || !parents2.empty())
            {
                result = parents1.empty() ? -1 : 1;
            }

            return result;
        }

        @Override
        public boolean equals(Object obj)
        {
            return obj instanceof ItemsComparator;
        }
    }

    @EntityDSLSupport(parts = {StudentExtractType.P_TITLE, StudentExtractType.P_CODE})
    public String getTitleWithCode()
    {
        return getTitle() + " - (" + getCode() + ")";
    }
}