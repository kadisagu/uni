/* $Id$ */
package ru.tandemservice.movestudent.utils;

import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 26.06.2013
 */
public class CachedSingleSelectTextModel extends SingleSelectTextModel
{
    private List<String> cachedList;
    private int maxListSize = 200;

    public CachedSingleSelectTextModel(List<String> cachedList)
    {
        this.cachedList = cachedList;
    }

    @Override
    public ListResult findValues(String filter)
    {
        List<String> list = new ArrayList<>();
        filter = filter.toLowerCase();
        for (String item : cachedList)
        {
            if (item.contains(filter))
                list.add(item);
        }
        return new ListResult<>(list.size() <= maxListSize ? list : list.subList(0, maxListSize), list.size());
    }
}