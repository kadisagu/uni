/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.modularextract;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.customorder.CustomOrderUtils;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.custom.ModifyModularStuExtract;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

/**
 * @author Nikolay Fedorovskih
 * @since 28.08.2013
 */
public class ModifyOrderStuExtractPrint implements IPrintFormCreator<ModifyModularStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ModifyModularStuExtract extract)
    {
        if (extract.getUserPrintForm() == null || extract.getUserPrintForm().getContent() == null)
            throw new ApplicationException(extract.getTitle() + ". " + CustomOrderUtils.NEED_UPLOAD_PRINT_DOC);

//        test();

        RtfDocument document = new RtfReader().read(template);
        RtfDocument customUserDocument = new RtfReader().read(extract.getUserPrintForm().getContent());

        RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, "USER_PRINT_FORM");
        if (rtfSearchResult.isFound())
        {
            // Значит печатается выписка из сборного приказа - щапку берем из шаблона, а метку USER_PRINT_FORM заменяем на пользовательский документ
            RtfUtil.modifySourceList(document.getHeader(), customUserDocument.getHeader(), customUserDocument.getElementList());
            IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
            rtfGroup.setElementList(customUserDocument.getElementList());
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), rtfGroup.getElementList());
        }
        else
        {
            // Печатается в составе сборного приказа или как индивидуальный приказ
            document = customUserDocument; // вставляем как есть
        }

        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

//    public void test()
//    {
//        try (InputStream i1 = new FileInputStream("d:\\test-rtf\\main.rtf");
//             InputStream i2 = new FileInputStream("d:\\test-rtf\\sub.rtf");
//             OutputStream out = new FileOutputStream("d:\\test-rtf\\result.rtf"))
//        {
//            RtfDocument mainDoc = new RtfReader().read(IOUtils.toByteArray(i1));
//            RtfDocument injectedDoc = new RtfReader().read(IOUtils.toByteArray(i2));
//
//            RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(mainDoc, "USER_PRINT_FORM");
//            if (rtfSearchResult.isFound())
//            {
//                RtfUtil.modifySourceList(mainDoc.getHeader(), injectedDoc.getHeader(), injectedDoc.getElementList());
//                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
//                rtfGroup.setElementList(injectedDoc.getElementList());
//                rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
//                rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), rtfGroup.getElementList());
//            }
//
//            RtfUtil.optimizeFontAndColorTable(mainDoc);
//
//            RtfWriter.write(mainDoc, out);
//        }
//        catch (Throwable e)
//        {
//            e.printStackTrace();
//        }
//    }
}