package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.movestudent.entity.gen.GraduateStuListExtractGen;

import java.util.Date;

/**
 * проект приказа «О выпуске студентов»
 */
public class GraduateStuListExtract extends GraduateStuListExtractGen implements IExcludeExtract
{
    @Override
    public Date getExcludeDate()
    {
        return getGraduateDate();
    }
}