/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e10.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.IBusinessComponent;

import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.movestudent.entity.ChangeFioStuExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 21.11.2008
 */
public class Controller extends CommonModularStudentExtractAddEditController<ChangeFioStuExtract, IDAO, Model>
{
    public void onRefresh(IBusinessComponent component)
    {
        ChangeFioStuExtract extract = getModel(component).getExtract();
        IdentityCardType cardType = extract.getCardType();
        if (cardType != null && cardType.isSeriaRequired() && isNeedChange(extract.getCardSeria(), cardType.getMaxSeriaLength()))
        {
            extract.setCardSeria(getDefaultValue(cardType.getMaxSeriaLength()));
        }
        if (cardType != null && cardType.isNumberRequired() && isNeedChange(extract.getCardNumber(), cardType.getMaxNumberLength()))
        {
            extract.setCardNumber(getDefaultValue(cardType.getMaxNumberLength()));
        }
        if (cardType == null && StringUtils.containsOnly(extract.getCardNumber(), "0"))
        {
            extract.setCardNumber(null);
        }
        if (null != cardType && !cardType.isShowSeria())
        {
            extract.setCardSeria(null);
        }
        if (null != cardType)
        {
            extract.setCitizenship(cardType.getCitizenshipDefault());
        }
    }

    private static boolean isNeedChange(String value, int maxLength)
    {
        return StringUtils.isEmpty(value) || StringUtils.containsOnly(value, "0") || value.length() > maxLength;
    }

    private static String getDefaultValue(int maxLength)
    {
        return String.format("%" + maxLength + "s", "").replaceAll(" ", "0");
    }
}