package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AttestationPracticStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об аттестации практик студентов на основании академической справки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AttestationPracticStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.AttestationPracticStuExtract";
    public static final String ENTITY_NAME = "attestationPracticStuExtract";
    public static final int VERSION_HASH = 1627595436;
    private static IEntityMeta ENTITY_META;

    public static final String P_PRACTICE_TYPE = "practiceType";
    public static final String P_PRACTICE_DURATION_STR = "practiceDurationStr";
    public static final String L_COURSE = "course";

    private String _practiceType;     // Тип практики
    private String _practiceDurationStr;     // Объем(недель)
    private Course _course;     // Курс прохождения практики

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип практики. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPracticeType()
    {
        return _practiceType;
    }

    /**
     * @param practiceType Тип практики. Свойство не может быть null.
     */
    public void setPracticeType(String practiceType)
    {
        dirty(_practiceType, practiceType);
        _practiceType = practiceType;
    }

    /**
     * @return Объем(недель). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPracticeDurationStr()
    {
        return _practiceDurationStr;
    }

    /**
     * @param practiceDurationStr Объем(недель). Свойство не может быть null.
     */
    public void setPracticeDurationStr(String practiceDurationStr)
    {
        dirty(_practiceDurationStr, practiceDurationStr);
        _practiceDurationStr = practiceDurationStr;
    }

    /**
     * @return Курс прохождения практики.
     */
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс прохождения практики.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof AttestationPracticStuExtractGen)
        {
            setPracticeType(((AttestationPracticStuExtract)another).getPracticeType());
            setPracticeDurationStr(((AttestationPracticStuExtract)another).getPracticeDurationStr());
            setCourse(((AttestationPracticStuExtract)another).getCourse());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AttestationPracticStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AttestationPracticStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new AttestationPracticStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "practiceType":
                    return obj.getPracticeType();
                case "practiceDurationStr":
                    return obj.getPracticeDurationStr();
                case "course":
                    return obj.getCourse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "practiceType":
                    obj.setPracticeType((String) value);
                    return;
                case "practiceDurationStr":
                    obj.setPracticeDurationStr((String) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "practiceType":
                        return true;
                case "practiceDurationStr":
                        return true;
                case "course":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "practiceType":
                    return true;
                case "practiceDurationStr":
                    return true;
                case "course":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "practiceType":
                    return String.class;
                case "practiceDurationStr":
                    return String.class;
                case "course":
                    return Course.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AttestationPracticStuExtract> _dslPath = new Path<AttestationPracticStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AttestationPracticStuExtract");
    }
            

    /**
     * @return Тип практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticStuExtract#getPracticeType()
     */
    public static PropertyPath<String> practiceType()
    {
        return _dslPath.practiceType();
    }

    /**
     * @return Объем(недель). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticStuExtract#getPracticeDurationStr()
     */
    public static PropertyPath<String> practiceDurationStr()
    {
        return _dslPath.practiceDurationStr();
    }

    /**
     * @return Курс прохождения практики.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticStuExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    public static class Path<E extends AttestationPracticStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<String> _practiceType;
        private PropertyPath<String> _practiceDurationStr;
        private Course.Path<Course> _course;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticStuExtract#getPracticeType()
     */
        public PropertyPath<String> practiceType()
        {
            if(_practiceType == null )
                _practiceType = new PropertyPath<String>(AttestationPracticStuExtractGen.P_PRACTICE_TYPE, this);
            return _practiceType;
        }

    /**
     * @return Объем(недель). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticStuExtract#getPracticeDurationStr()
     */
        public PropertyPath<String> practiceDurationStr()
        {
            if(_practiceDurationStr == null )
                _practiceDurationStr = new PropertyPath<String>(AttestationPracticStuExtractGen.P_PRACTICE_DURATION_STR, this);
            return _practiceDurationStr;
        }

    /**
     * @return Курс прохождения практики.
     * @see ru.tandemservice.movestudent.entity.AttestationPracticStuExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

        public Class getEntityClass()
        {
            return AttestationPracticStuExtract.class;
        }

        public String getEntityName()
        {
            return "attestationPracticStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
