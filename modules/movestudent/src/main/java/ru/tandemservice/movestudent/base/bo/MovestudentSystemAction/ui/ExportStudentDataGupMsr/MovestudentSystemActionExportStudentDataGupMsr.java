/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MovestudentSystemAction.ui.ExportStudentDataGupMsr;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;

/**
 * @author Ekaterina Zvereva
 * @since 16.12.2014
 */
@Configuration
public class MovestudentSystemActionExportStudentDataGupMsr extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(UniStudentManger.instance().studentCustomStateCIDSConfig())
                .create();
    }
}
