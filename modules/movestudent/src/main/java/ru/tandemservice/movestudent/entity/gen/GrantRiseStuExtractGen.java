package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.GrantRiseStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об однократном повышении государственной академической стипендии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GrantRiseStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.GrantRiseStuExtract";
    public static final String ENTITY_NAME = "grantRiseStuExtract";
    public static final int VERSION_HASH = 673394661;
    private static IEntityMeta ENTITY_META;

    public static final String P_GRANT_RISE_AMOUNT = "grantRiseAmount";
    public static final String P_MONTH = "month";
    public static final String P_YEAR = "year";

    private double _grantRiseAmount;     // Размер повышения государственной академической стипендии
    private int _month;     // Месяц повышения государственной академической стипендии
    private int _year;     // Год повышения государственной академической стипендии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Размер повышения государственной академической стипендии. Свойство не может быть null.
     */
    @NotNull
    public double getGrantRiseAmount()
    {
        return _grantRiseAmount;
    }

    /**
     * @param grantRiseAmount Размер повышения государственной академической стипендии. Свойство не может быть null.
     */
    public void setGrantRiseAmount(double grantRiseAmount)
    {
        dirty(_grantRiseAmount, grantRiseAmount);
        _grantRiseAmount = grantRiseAmount;
    }

    /**
     * @return Месяц повышения государственной академической стипендии. Свойство не может быть null.
     */
    @NotNull
    public int getMonth()
    {
        return _month;
    }

    /**
     * @param month Месяц повышения государственной академической стипендии. Свойство не может быть null.
     */
    public void setMonth(int month)
    {
        dirty(_month, month);
        _month = month;
    }

    /**
     * @return Год повышения государственной академической стипендии. Свойство не может быть null.
     */
    @NotNull
    public int getYear()
    {
        return _year;
    }

    /**
     * @param year Год повышения государственной академической стипендии. Свойство не может быть null.
     */
    public void setYear(int year)
    {
        dirty(_year, year);
        _year = year;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof GrantRiseStuExtractGen)
        {
            setGrantRiseAmount(((GrantRiseStuExtract)another).getGrantRiseAmount());
            setMonth(((GrantRiseStuExtract)another).getMonth());
            setYear(((GrantRiseStuExtract)another).getYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GrantRiseStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GrantRiseStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new GrantRiseStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "grantRiseAmount":
                    return obj.getGrantRiseAmount();
                case "month":
                    return obj.getMonth();
                case "year":
                    return obj.getYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "grantRiseAmount":
                    obj.setGrantRiseAmount((Double) value);
                    return;
                case "month":
                    obj.setMonth((Integer) value);
                    return;
                case "year":
                    obj.setYear((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "grantRiseAmount":
                        return true;
                case "month":
                        return true;
                case "year":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "grantRiseAmount":
                    return true;
                case "month":
                    return true;
                case "year":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "grantRiseAmount":
                    return Double.class;
                case "month":
                    return Integer.class;
                case "year":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GrantRiseStuExtract> _dslPath = new Path<GrantRiseStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GrantRiseStuExtract");
    }
            

    /**
     * @return Размер повышения государственной академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GrantRiseStuExtract#getGrantRiseAmount()
     */
    public static PropertyPath<Double> grantRiseAmount()
    {
        return _dslPath.grantRiseAmount();
    }

    /**
     * @return Месяц повышения государственной академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GrantRiseStuExtract#getMonth()
     */
    public static PropertyPath<Integer> month()
    {
        return _dslPath.month();
    }

    /**
     * @return Год повышения государственной академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GrantRiseStuExtract#getYear()
     */
    public static PropertyPath<Integer> year()
    {
        return _dslPath.year();
    }

    public static class Path<E extends GrantRiseStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Double> _grantRiseAmount;
        private PropertyPath<Integer> _month;
        private PropertyPath<Integer> _year;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Размер повышения государственной академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GrantRiseStuExtract#getGrantRiseAmount()
     */
        public PropertyPath<Double> grantRiseAmount()
        {
            if(_grantRiseAmount == null )
                _grantRiseAmount = new PropertyPath<Double>(GrantRiseStuExtractGen.P_GRANT_RISE_AMOUNT, this);
            return _grantRiseAmount;
        }

    /**
     * @return Месяц повышения государственной академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GrantRiseStuExtract#getMonth()
     */
        public PropertyPath<Integer> month()
        {
            if(_month == null )
                _month = new PropertyPath<Integer>(GrantRiseStuExtractGen.P_MONTH, this);
            return _month;
        }

    /**
     * @return Год повышения государственной академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GrantRiseStuExtract#getYear()
     */
        public PropertyPath<Integer> year()
        {
            if(_year == null )
                _year = new PropertyPath<Integer>(GrantRiseStuExtractGen.P_YEAR, this);
            return _year;
        }

        public Class getEntityClass()
        {
            return GrantRiseStuExtract.class;
        }

        public String getEntityName()
        {
            return "grantRiseStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
