/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e35;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.IStudentListParagraphPrintFormatter;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author ListExtractComponentGenerator
 * @since 22.10.2012
 */
public class SplitFirstCourseStudentsGroupStuListExtractPrint implements IPrintFormCreator<SplitFirstCourseStudentsGroupStuListExtract>, IListParagraphPrintFormCreator<SplitFirstCourseStudentsGroupStuListExtract>, IStudentListParagraphPrintFormatter
{
    @Override
    public String formatSingleStudent(Student student, int extractNumber)
    {
        StringBuilder buffer = new StringBuilder();
        buffer.append("\\par ").append(extractNumber).append(".  ");
        buffer.append(student.getPerson().getFullFio());
        return buffer.toString();
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, SplitFirstCourseStudentsGroupStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);

        modifier.put("newGroup", extract.getNewGroup().getTitle());

        CommonExtractPrint.initOrgUnit(modifier, extract.getEntity().getEducationOrgUnit(), "formativeOrgUnitStr", "");

        CommonExtractPrint.modifyEducationStr(modifier, EducationOrgUnitUtil.getParentLevel(extract.getEducationLevelsHighSchool()));
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, extract.getDevelopCondition(), extract.getDevelopTech(), extract.getDevelopPeriod(), CommonListOrderPrint.getEducationBaseText(extract.getNewGroup()), "fefuShortFastExtendedOptionalText");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, SplitFirstCourseStudentsGroupStuListExtract firstExtract)
    {
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract)
                .put("newGroup", "Группа " + firstExtract.getNewGroup().getTitle());
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, SplitFirstCourseStudentsGroupStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, SplitFirstCourseStudentsGroupStuListExtract firstExtract)
    {
        modifier.put("course", "1");
        modifier.put("orderType", order.getType().getTitle());

        CommonExtractPrint.initOrgUnit(modifier, firstExtract.getEntity().getEducationOrgUnit(), "formativeOrgUnitStr", "");

        CommonExtractPrint.modifyEducationStr(modifier, EducationOrgUnitUtil.getParentLevel(firstExtract.getEducationLevelsHighSchool()));
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, firstExtract.getDevelopCondition(), firstExtract.getDevelopTech(), firstExtract.getDevelopPeriod(), CommonListOrderPrint.getEducationBaseText(firstExtract.getNewGroup()), "fefuShortFastExtendedOptionalText");
        CommonExtractPrint.initDevelopForm(modifier, firstExtract.getDevelopForm(), "");
    }
}