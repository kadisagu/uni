/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e48.ListOrderAddEdit;

import ru.tandemservice.movestudent.component.listextract.e1.ListOrderAddEdit.Model;

/**
 * @author Ekaterina Zvereva
 * @since 07.02.2017
 */
public class DAO extends ru.tandemservice.movestudent.component.listextract.e1.ListOrderAddEdit.DAO
{
    public static final String TEXT_PARAGRAPH = "В соответствии с Временным положением о текущем контроле успеваемости и промежуточной аттестации " +
            "по образовательным программам высшего образования - программам бакалавриата, программам специалитета и программам магистратуры";


    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        //Если форма добавления
        if (model.getOrderId() == null)
            model.getOrder().setTextParagraph(TEXT_PARAGRAPH);
    }
}