/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.modularextract.Pub;

import ru.tandemservice.movestudent.component.customorder.CustomOrderUtils;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.movestudent.entity.custom.ModifyModularStuExtract;

import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 28.08.2013
 */
public class Model extends ModularStudentExtractPubModel<ModifyModularStuExtract>
{
    private Map<String, String> _actionMap;
    private String _currentComponentName;

    public String getModifiedOrderFieldName()
    {
        return CustomOrderUtils.getModifiableOrderFieldName(getExtract().getType().getCode());
    }

    public boolean isShowExtractNumber()
    {
        return !getExtract().isModifiableOrderIndividual();
    }

    public Map<String, String> getActionMap()
    {
        return _actionMap;
    }

    public void setActionMap(Map<String, String> actionMap)
    {
        _actionMap = actionMap;
    }

    public String getCurrentComponentName()
    {
        return _currentComponentName;
    }

    public void setCurrentComponentName(String currentComponentName)
    {
        _currentComponentName = currentComponentName;
    }
}