/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.dao;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.transaction.DaoCacheFacade;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.IOrgUnitPermissionCustomizer;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.movestudent.entity.codes.MovestudentScriptItemCodes;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.movestudent.utils.system.StudentExcludeOrderData;
import ru.tandemservice.uni.dao.IOrgstructDAO;
import ru.tandemservice.uni.dao.IUniScriptDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.codes.StudentExtractGroupCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.ICustomOrderAction;
import ru.tandemservice.unimove.entity.gen.ICustomOrderActionGen;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 18.10.2008
 */
public class MoveStudentDao extends UniBaseDao implements IMoveStudentDao, IOrgUnitPermissionCustomizer
{
    private static final String DAO_SIMPLE_NAME = MoveStudentDao.class.getSimpleName();
    public static final int MAX_ROWS = 50;

    @Override
    public String getObjectStateCode(Class<? extends IEntity> clazz, Long objectId)
    {
        return new DQLSelectBuilder().fromEntity(clazz, "o")
                .column(property("o.state.code"))
                .where(eq(property("o.id"), value(objectId)))
                .createStatement(getSession()).uniqueResult();
    }

    @Override
    public String getOrderStateCode(Long orderId)
    {
        return getObjectStateCode(AbstractStudentOrder.class, orderId);
    }

    @Override
    public String getExtractStateCode(Long extractId)
    {
        return getObjectStateCode(AbstractStudentExtract.class, extractId);
    }

    @Override
    public boolean canBeCreatedExtractForStudentWithoutGroup(StudentExtractType extractType)
    {
        // Пока разрешаем только для выписки о распределении в группу
        return extractType.getCode().equals(StudentExtractTypeCodes.GROUP_ASSIGN_MODULAR_ORDER);
    }

    @Override
    public List<String> getStudentPracticeTypes()
    {
        return Arrays.asList(ApplicationRuntime.getProperty("practiceTypes_N").split(";"));
    }

    @Override
    public List<String> getStudentPracticeKinds()
    {
        return Arrays.asList(ApplicationRuntime.getProperty("practicKinds_N").split(";"));
    }

    @Override
    public String getFullPracticeKind(String practiceType, String practiceKind)
    {
        return practiceType + (!StringUtils.isEmpty(practiceKind) ? " (" + practiceKind + ")" : "");
    }

    @Override
    public String extractPracticeKind(String fullPracticeKind)
    {
        int l = fullPracticeKind.indexOf('('), r = fullPracticeKind.lastIndexOf(')');
        return (l > 0 && r > l) ? fullPracticeKind.substring(l + 1, r) : "";
    }

    @Override
    public String extractPracticeType(String fullPracticeKind)
    {
        int i = fullPracticeKind.indexOf(" (");
        return i < 0 ? fullPracticeKind : fullPracticeKind.substring(0, i);
    }

    @Override
    public String getDeclinablePracticeType(String type, GrammaCase grammaCase)
    {
        if (GrammaCase.NOMINATIVE == grammaCase)
            return type;
        final int idx = Arrays.asList(ApplicationRuntime.getProperty("practiceTypes_N").split(";")).indexOf(type);
        if (idx < 0)
            return null;
        final String postfix = CommonExtractPrint.GRAMMA_CASE_POSTFIX.get(grammaCase);
        return Arrays.asList(ApplicationRuntime.getProperty("practiceTypes_" + postfix).split(";")).get(idx);
    }

    @Override
    public String getDeclinableFullPracticeKind(String fullPracticeKind, GrammaCase grammaCase)
    {
        String type = getDeclinablePracticeType(extractPracticeType(fullPracticeKind), grammaCase);
        return type != null ? getFullPracticeKind(type, extractPracticeKind(fullPracticeKind)) : null;
    }

    @Override
    public boolean isOrgUnitMoveStudentTabVisible(OrgUnit orgUnit)
    {
        return IOrgstructDAO.instance.get().isOrgUnitFormingOrTerritorial(orgUnit);
    }

    private static Set<Long> _MANY_CHILD_ORDER_TYPES = null;

    @Override
    public boolean isVariousExtractTypeInOrder(StudentExtractType orderType)
    {
        if (_MANY_CHILD_ORDER_TYPES == null)
        {
            // Получаем все типы списочных приказов, у которых больше одного дочеренего типа (тип параграфа)
            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(StudentExtractType.class, "s")
                    .column(DQLFunctions.min(property("s", StudentExtractType.parent().id())))
                    .where(eq(property("s", StudentExtractType.parent().parent().code()), value(StudentExtractTypeCodes.LIST_ORDER)))
                    .group(property("s", StudentExtractType.parent().code()))
                    .having(gt(DQLFunctions.countStar(), value(1)));

            _MANY_CHILD_ORDER_TYPES = new HashSet<>(dql.createStatement(getSession()).<Long>list());
        }
        return _MANY_CHILD_ORDER_TYPES.contains(orderType.getId());
    }

    private static Set<Long> _MULTIPLE_PARAGRAPHS_ADD_ORDER_TYPES = null;

    @Override
    public boolean isMultipleAddParagraphsOrder(StudentExtractType orderType)
    {
        if (_MULTIPLE_PARAGRAPHS_ADD_ORDER_TYPES == null)
        {
            // Получаем все типы списочных приказов, у которых больше одного дочеренего типа (тип параграфа)
            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(StudentExtractType.class, "s")
                    .column("s")
                    .column(property("s", StudentExtractType.parent().id()))
                    .where(eq(property("s", StudentExtractType.parent().parent().code()), value(StudentExtractTypeCodes.LIST_ORDER)));

            _MULTIPLE_PARAGRAPHS_ADD_ORDER_TYPES = new HashSet<>();
            for (Object[] item : dql.createStatement(getSession()).<Object[]>list())
            {
                try
                {
                    Class.forName(MoveStudentUtils.getListMultipleParagraphAddComponent((StudentExtractType) item[0]) + ".Controller");
                    _MULTIPLE_PARAGRAPHS_ADD_ORDER_TYPES.add((Long) item[1]);
                }
                catch (ClassNotFoundException e)
                { /* nop */ }
            }
        }
        return _MULTIPLE_PARAGRAPHS_ADD_ORDER_TYPES.contains(orderType.getId());
    }

    @Override
    public void customize(OrgUnitType orgUnitType, Map<String, PermissionGroupMeta> permissionGroupMap)
    {
        boolean hidden = IOrgstructDAO.instance.get().hidePermissionsBasedOnFormingAndTerritorialKind(orgUnitType);
        String code = orgUnitType.getCode();

        permissionGroupMap.get(code + "MoveStudentPermissionGroup").setHidden(hidden);
    }

    @Override
    public boolean isMoveAccessible(Student student)
    {
        return isMoveAccessible(student, null);
    }

    @Override
    public boolean isMoveAccessible(Student student, Long excludeParagraphId)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                .top(1).column("e.id")
                .where(eq(property("e", IAbstractExtract.L_ENTITY), value(student)))
                .where(or(
                        isNull(property("e", IAbstractExtract.L_PARAGRAPH)),
                        and(
                                excludeParagraphId != null ? ne(property("e", IAbstractExtract.L_PARAGRAPH), value(excludeParagraphId)) : null,
                                exists(
                                        new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e2").column("e2.id")
                                                .where(eq("e.id", "e2.id"))
                                                .where(notIn(property("e2", AbstractStudentExtract.paragraph().order().state().code()), UnimoveDefines.CATALOG_ORDER_STATE_FINISHED, UnimoveDefines.CATALOG_ORDER_STATE_REJECTED))
                                                .buildQuery()
                                )
                        )
                ));

        return builder.createStatement(getSession()).uniqueResult() == null;
    }

    @Override
    public List<StudentExtractType> getModularStudentExtractTypeList()
    {
        return new DQLSelectBuilder().fromEntity(StudentExtractType.class, "e")
                .where(eq(property("e", StudentExtractType.parent().code()), value(StudentExtractTypeCodes.MODULAR_ORDER)))
                .where(eq(property("e", StudentExtractType.active()), value(Boolean.TRUE)))
                .order(property("e", StudentExtractType.title()))
                .createStatement(getSession()).list();
    }

    @Override
    public ListResult<StudentExtractType> getModularStudentExtractTypeList(String filter, String parentExtractTypeCode)
    {
        // TODO optimize in DEV-3640
        MQBuilder builder = new MQBuilder(StudentExtractType.ENTITY_CLASS, "sext")
                .add(MQExpression.eq("sext", StudentExtractType.parent().code().s(), parentExtractTypeCode))
                .add(MQExpression.or(
                        MQExpression.or(
                                MQExpression.eq("sext", StudentExtractType.P_ACTIVE, Boolean.TRUE),
                                MQExpression.in("sext", StudentExtractType.CATALOG_ITEM_CODE,
                                                new MQBuilder(AbstractStudentExtract.ENTITY_CLASS, "ext", new String[]{AbstractStudentExtract.type().code().s()}))
                        ),
                        MQExpression.in("sext", StudentExtractType.CATALOG_ITEM_CODE,
                                        new MQBuilder(AbstractStudentExtract.ENTITY_CLASS, "ext", new String[]{AbstractStudentExtract.type().parent().code().s()}))
                ));

        if (null != filter)
            builder.add(MQExpression.like("sext", StudentExtractType.P_TITLE, CoreStringUtils.escapeLike(filter)));
        builder.addOrder("sext", StudentExtractType.P_TITLE);
        return new ListResult<>(builder.<StudentExtractType>getResultList(getSession()), builder.getResultCount(getSession()));
    }

    @Override
    public Long getLastCommittedExtractId(Long studentId)
    {
        return new DQLSelectBuilder()
                .fromEntity(AbstractStudentExtract.class, "e")
                .top(1).column("e.id")
                .where(eq(property("e", AbstractStudentExtract.entity().id()), value(studentId)))
                .where(eq(property("e", AbstractStudentExtract.committed()), value(Boolean.TRUE)))
                .order(property("e", AbstractStudentExtract.paragraph().order().commitDateSystem()), OrderDirection.desc)
                .createStatement(getSession()).uniqueResult();
    }

    @Override
    public void saveModularExtractText(ModularStudentExtract extract, int textCode)
    {
        final Session session = getSession();
        StudentExtractTextRelation rel = getExtractTextRelation(extract.getId(), textCode);
        if (rel == null)
        {
            rel = new StudentExtractTextRelation();
            rel.setExtract(extract);
            rel.setCode(textCode);
        }
        String printBeanName = EntityRuntime.getMeta(extract).getName() + "_extractPrint";
        rel.setText(createPrintDocumentFromTemplate(extract, extract.getType(), textCode, printBeanName));
        session.saveOrUpdate(rel);
    }

    @Override
    public void saveModularOrderText(StudentModularOrder order)
    {
        StudentOrderTextRelation rel = get(StudentOrderTextRelation.class, StudentOrderTextRelation.L_ORDER, order);
        if (rel == null)
        {
            rel = new StudentOrderTextRelation();
            rel.setOrder(order);
        }
        StudentExtractType modularOrderType = get(StudentExtractType.class, StudentExtractType.P_CODE, StudentExtractTypeCodes.MODULAR_ORDER);
        rel.setText(createPrintDocumentFromTemplate(order, modularOrderType, MoveStudentDefines.ORDER_TEXT_CODE, "studentModularOrder_orderPrint"));
        getSession().saveOrUpdate(rel);
    }

    @Override
    public StudentExtractTextRelation getExtractTextRelation(Long extractId, int textCode)
    {
        return new DQLSelectBuilder().fromEntity(StudentExtractTextRelation.class, "rel").column("rel")
                .where(eq(property(StudentExtractTextRelation.extract().id().fromAlias("rel")), value(extractId)))
                .where(eq(property(StudentExtractTextRelation.code().fromAlias("rel")), value(textCode)))
                .createStatement(getSession()).uniqueResult();
    }

    @Override
    public StudentOrderTextRelation getOrderTextRelation(Long orderId)
    {
        return new DQLSelectBuilder().fromEntity(StudentOrderTextRelation.class, "rel").column("rel")
                .where(eq(property(StudentOrderTextRelation.order().id().fromAlias("rel")), value(orderId)))
                .createStatement(getSession()).uniqueResult();
    }

    @Override
    public void saveListExtractText(ListStudentExtract extract, int textCode)
    {
        final Session session = getSession();
        StudentExtractTextRelation rel = getExtractTextRelation(extract.getId(), textCode);
        if (rel == null)
        {
            rel = new StudentExtractTextRelation();
            rel.setExtract(extract);
            rel.setCode(textCode);
        }

        String printBeanName = EntityRuntime.getMeta(extract).getName() + "_extractPrint";
        rel.setText(createPrintDocumentFromTemplate(extract, extract.getType(), textCode, printBeanName));
        session.saveOrUpdate(rel);
    }

    @Override
    public void saveListOrderText(StudentListOrder order)
    {
        StudentOrderTextRelation rel = get(StudentOrderTextRelation.class, StudentOrderTextRelation.L_ORDER, order);
        if (rel == null)
        {
            rel = new StudentOrderTextRelation();
            rel.setOrder(order);
        }
        byte[] doc = createPrintDocumentFromTemplate(order, order.getType(), MoveStudentDefines.ORDER_TEXT_CODE, "studentListOrder_" + order.getType().getCode() + "_orderPrint");
        if (doc == null)
        {
            doc = createPrintDocumentFromTemplate(order, order.getType(), MoveStudentDefines.ORDER_TEXT_CODE, "studentListOrder" + order.getType().getIndex() + "_orderPrint");
        }
        rel.setText(doc);
        getSession().saveOrUpdate(rel);
    }

    @Override
    public boolean isCustomOrderType(StudentExtractType extractType)
    {
        return MoveStudentDefines.CUSTOM_EXTRACT_TYPES.contains(extractType.getCode());
    }

    @Override
    public <T> byte[] createPrintDocumentFromTemplate(T object, StudentExtractType extractType, int templateIndex, String printBeanName)
    {
        IPrintFormCreator<T> componentPrint = CommonExtractPrint.getPrintFormCreator(printBeanName);
        if (componentPrint == null)
            return null;
        byte[] template = getTemplate(extractType, templateIndex);
        RtfDocument document = componentPrint.createPrintForm(template, object);
        return RtfUtil.toByteArray(document);
    }

    @Override
    public void deleteListExtract(Long extractId)
    {
        ListStudentExtract extract = getNotNull(ListStudentExtract.class, extractId);
        StudentListParagraph paragraph = (StudentListParagraph) extract.getParagraph();
        getSession().refresh(paragraph.getOrder());

        if (paragraph.getOrder().isReadonly())
            throw new ApplicationException("Приказ не может быть изменен.");

        if (paragraph.getExtractCount() == 1)
            throw new ApplicationException("Параграф приказа не может быть пустым. При необходимости удалите параграф приказа и создайте новый.");

        MoveDaoFacade.getMoveDao().deleteExtractFromParagraph(extract);

        // TODO optimize in DEV-3640

        // Перенумеруем все выписки, так чтобы сортировка была по ФИО
        List<ListStudentExtract> extractList = getList(ListStudentExtract.class, IAbstractExtract.L_PARAGRAPH, paragraph);
        extractList.sort(AbstractStudentExtract.FULL_FIO_AND_ID_COMPARATOR);

        // нумеруем выписки с единицы
        int counter = 1;
        for (ListStudentExtract ListStudentExtract : extractList)
        {
            ListStudentExtract.setNumber(counter++);
            update(ListStudentExtract);
        }
    }


    private final DaoCacheFacade.CacheEntryDefinition<CoreCollectionUtils.Pair<Long, Integer>, byte[]> CACHE_TEMPLATE = new DaoCacheFacade.CacheEntryDefinition<CoreCollectionUtils.Pair<Long, Integer>, byte[]>(
            DAO_SIMPLE_NAME + ".getTemplate", 64
    )
    {
        @Override
        public void fill(Map<Pair<Long, Integer>, byte[]> cache, Collection<Pair<Long, Integer>> templateIdsSlice)
        {
            for (CoreCollectionUtils.Pair<Long, Integer> key : templateIdsSlice)
            {
                MovestudentTemplate t = new DQLSelectBuilder()
                        .fromEntity(MovestudentTemplate.class, "t").column(property("t"))
                        .where(eq(property(MovestudentTemplate.type().id().fromAlias("t")), value(key.getX())))
                        .where(eq(property(MovestudentTemplate.index().fromAlias("t")), value(key.getY())))
                        .createStatement(getSession()).uniqueResult();
                if (null == t)
                    throw new ApplicationException("Печать невозможна. Не найден печатный шаблон.");
                cache.put(key,t.getContent());
            }
        }
    };

    @Override
    public byte[] getTemplate(final StudentExtractType extractType, final int templateIndex)
    {
        final CoreCollectionUtils.Pair<Long, Integer> pairKey = new CoreCollectionUtils.Pair<>(extractType.getId(), templateIndex);
        return DaoCacheFacade.getEntry(CACHE_TEMPLATE).getRecords(Collections.singleton(pairKey)).get(pairKey);
    }

    @Override
    @Deprecated
    public List<StuExtractToDebtRelation> getStudentExtractDebtsList(ModularStudentExtract extract)
    {
        MQBuilder builder = new MQBuilder(StuExtractToDebtRelation.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", StuExtractToDebtRelation.L_EXTRACT, extract));
        builder.addOrder("rel", StuExtractToDebtRelation.P_DISCIPLINE);
        return builder.getResultList(getSession());
    }

    @Override
    @Deprecated
    public EducationLevels getSpeciality(EducationLevelsHighSchool eduLevel)
    {
        if (null != eduLevel.getEducationLevel().getParentLevel() && eduLevel.isSpecialization())
            return eduLevel.getEducationLevel().getParentLevel();
        else if (!eduLevel.isAllowStudents())
            throw new ApplicationException("Для группы указан неверный уровень образования.");
        else
            return eduLevel.getEducationLevel();

    }

    @Override
    public void checkOrderNumber(AbstractStudentOrder order)
    {
        String number = order.getNumber();
        if (number == null) return;
        number = number.trim();
        if (number.isEmpty()) return;

        Date date = order.getCreateDate();
        if (date == null)
            throw new ApplicationException("Нельзя проверить номер приказа. Не указана дата формирования.");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AbstractStudentOrder.class, "o")
                .column("o.id")
                .where(eq(property("o", AbstractStudentOrder.number()), value(number)));

        FilterUtils.applyInYearFilter(builder, "o", AbstractStudentOrder.P_COMMIT_DATE, CoreDateUtils.getYear(date));

        if (order.getId() != null)
            builder.where(ne(property("o.id"), value(order.getId())));

        if (existsEntity(builder.buildQuery()))
            throw new ApplicationException("Номер приказа должен быть уникален в рамках календарного года среди всех приказов по студентам.");
    }

    @Override
    public <T extends AbstractStudentExtract> T getParagraphFirstExtract(Long paragraphId, boolean onlyProxy)
    {
        if (paragraphId != null)
        {
            Long id = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                    .top(1).column(property("e.id"))
                    .where(eq(property("e", IAbstractExtract.L_PARAGRAPH), value(paragraphId)))
                    .order(property("e", IAbstractExtract.P_NUMBER))
                    .createStatement(getSession()).uniqueResult();

            if (id == null)
                return null;

            if (onlyProxy)
                return proxy(id);

            return new DQLSelectBuilder().fromEntity(EntityRuntime.getMeta(id).getEntityClass(), "e", true)
                    .where(eq(property("e.id"), value(id)))
                    .createStatement(getSession()).uniqueResult();
        }
        return null;
    }

    @Override
    public StudentExtractType getAnyExtractTypeByParagraph(Long paragraphId)
    {
        if (paragraphId != null)
        {
            return new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                    .top(1).column(property("e", IAbstractExtract.L_TYPE))
                    .where(eq(property("e", IAbstractExtract.L_PARAGRAPH), value(paragraphId)))
                    .createStatement(getSession()).uniqueResult();
        }
        return null;
    }

    @Override
    public int getExtractCount(Long orderId)
    {
        return orderId == null ? 0 : new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                .column(DQLFunctions.countStar())
                .where(eq(property("e", AbstractStudentExtract.paragraph().order()), value(orderId)))
                .createStatement(getSession()).<Long>uniqueResult().intValue();
    }

    @Override
    public <T extends AbstractStudentExtract> T getOrderFirstExtract(Long orderId, boolean onlyProxy)
    {
        Long id = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                .column("e.id").top(1)
                .joinPath(DQLJoinType.inner, AbstractStudentExtract.paragraph().fromAlias("e"), "p")
                .where(eq(property("p", IAbstractParagraph.L_ORDER), value(orderId)))
                .order(property("p", IAbstractParagraph.P_NUMBER))
                .order(property("e", IAbstractExtract.P_NUMBER))
                .createStatement(getSession()).uniqueResult();

        if (id == null)
            return null;

        if (onlyProxy)
            return proxy(id);

        return new DQLSelectBuilder().fromEntity(EntityRuntime.getMeta(id).getEntityClass(), "e", true)
                .column("e")
                .where(eq(property("e.id"), value(id)))
                .createStatement(getSession()).uniqueResult();
    }

    @Override
    public <T extends AbstractStudentExtract> T getOrderFirstExtract(Long orderId)
    {
        return getOrderFirstExtract(orderId, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends AbstractStudentExtract> List<T> getExtractListByIds(Collection<Long> extractIds, Class<T> checkClass)
    {
        if (extractIds == null || extractIds.isEmpty()) {
            return ImmutableList.of();
        }

        if (extractIds.size() == 1) {
            return ImmutableList.of(get(extractIds.iterator().next()));
        }

        final Multimap<Class, Long> map = ArrayListMultimap.create();
        for (Long id : extractIds) {
            final Class clazz = EntityRuntime.getMeta(id).getEntityClass();
            if (!checkClass.isAssignableFrom(clazz)) {
                throw new IllegalArgumentException("Class for id " + id + " is " + clazz.getSimpleName() + " - it's not child class for  " + checkClass.getSimpleName() + ".");
            }
            map.put(clazz, id);
        }

        final List<T> extractList = new ArrayList<>(extractIds.size());
        for (Map.Entry<Class, Collection<Long>> entry : map.asMap().entrySet()) {
            for (List<Long> idsPart : Iterables.partition(entry.getValue(), DQL.MAX_VALUES_ROW_NUMBER)) {
                extractList.addAll(this.<T>getList(entry.getKey(), idsPart));
            }
        }

        return extractList;
    }

    @Override
    public void updateParagraphsNumber(StudentModularOrder order)
    {
        // TODO optimize in DEV-3640

        //достаем все выписки нашего приказа
        MQBuilder builder = new MQBuilder(ModularStudentExtract.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", ModularStudentExtract.paragraph().order().s(), order));
        List<ModularStudentExtract> extractList = builder.getResultList(getSession());
        //сортируем
        Collections.sort(extractList, MoveStudentUtils.MODULAR_STUDENT_EXTRACT_COMPARATOR);

        //присваем сначала фейковые номера, что бы не возникло конфликта уникальности
        int i = extractList.size() + 100;
        for (ModularStudentExtract extract : extractList)
        {
            extract.getParagraph().setNumber(i++);
            update(extract.getParagraph());
        }
        getSession().flush();

        //затем присваем окончательные номера параграфам приказа
        i = 1;
        for (ModularStudentExtract extract : extractList)
        {
            extract.getParagraph().setNumber(i++);
            update(extract.getParagraph());
        }
        getSession().flush();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends AbstractStudentExtract> List<T> getExtractListByParagraph(Long paragraphId, boolean sort)
    {
        final List<Long> extractIds = getPropertiesList(AbstractStudentExtract.class, AbstractStudentExtract.paragraph().id(), paragraphId, false, AbstractStudentExtract.id());
        final List<AbstractStudentExtract> extractList = getExtractListByIds(extractIds, AbstractStudentExtract.class);
        if (extractList.size() > 1) {
            Collections.sort(extractList, AbstractStudentExtract.BY_EXTRACT_NUMBER_COMPARATOR);
        }
        return (List<T>) extractList;
    }

    @Override
    public List<Student> getStudentsByParagraph(Long paragraphId)
    {
        return new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                .column(property("e", AbstractStudentExtract.entity()))
                .where(eq(property("e", IAbstractExtract.L_PARAGRAPH), value(paragraphId)))
                .order(property("e", IAbstractExtract.P_NUMBER))
                .createStatement(getSession()).list();
    }

    @Override
    public List<String> getStudentsFioByParagraph(Long paragraphId)
    {
        return new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                .column(property("e", AbstractStudentExtract.entity().person().identityCard().fullFio()))
                .where(eq(property("e", IAbstractExtract.L_PARAGRAPH), value(paragraphId)))
                .order(property("e", IAbstractExtract.P_NUMBER))
                .createStatement(getSession()).list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends AbstractStudentExtract> List<T> getOrderExtractList(AbstractStudentOrder order, boolean sort)
    {
        final List<Long> extractIds = getPropertiesList(AbstractStudentExtract.class, AbstractStudentExtract.paragraph().order(), order, false, AbstractStudentExtract.id());
        final List<AbstractStudentExtract> extractList = getExtractListByIds(extractIds, AbstractStudentExtract.class);
        if (extractList.size() > 1) {
            Collections.sort(extractList, AbstractStudentExtract.BY_PAR_AND_EXTRACT_NUMBER_COMPARATOR);
        }
        return (List<T>) extractList;
    }


    @Override
    public Set<Long> getDisabledFormPrintingExtracts(List<AbstractStudentExtract> extractsList, final int extractTemplateIndex)
    {
        // TODO optimize in DEV-3640 - кэшировать признак печатаемости для типов выписок.

        final Set<Long> enabledExtractIds = new HashSet<>();
        BatchUtils.execute(extractsList, DQL.MAX_VALUES_ROW_NUMBER, extractsPortion -> {
            final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentExtractTextRelation.class, "etr")
                    .where(in(property(StudentExtractTextRelation.extract().fromAlias("etr")), extractsPortion))
                    .where(eq(property(StudentExtractTextRelation.code().fromAlias("etr")), value(extractTemplateIndex)))
                    .column(property(StudentExtractTextRelation.extract().id().fromAlias("etr")));
            enabledExtractIds.addAll(builder.createStatement(getSession()).<Long>list());
        });

        Set<StudentExtractType> extractTypeSet = new HashSet<>();
        for (AbstractStudentExtract extract : extractsList) extractTypeSet.add(extract.getType());

        final Set<Long> extractTypesWithTemplateSet = new HashSet<>();
        BatchUtils.execute(extractTypeSet, DQL.MAX_VALUES_ROW_NUMBER, extractTypePortionSet -> {
            final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(MovestudentTemplate.class, "dt")
                    .where(in(property(MovestudentTemplate.type().fromAlias("dt")), extractTypePortionSet))
                    .where(eq(property(MovestudentTemplate.index().fromAlias("dt")), value(extractTemplateIndex)))
                    .column(property(MovestudentTemplate.type().id().fromAlias("dt")))
                    .predicate(DQLPredicateType.distinct);
            extractTypesWithTemplateSet.addAll(builder.createStatement(getSession()).<Long>list());
        });

        Set<Long> wholeIdsSet = new HashSet<>();
        for (AbstractStudentExtract extract : extractsList)
        {
            wholeIdsSet.add(extract.getId());
            if (!UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED.equals(extract.getState().getCode()))
            {
                if (extractTypesWithTemplateSet.contains(extract.getType().getId()))
                {
                    String printName = EntityRuntime.getMeta(extract.getId()).getName() + "_extractPrint";
                    if (ApplicationRuntime.containsBean(printName) && ApplicationRuntime.getBean(printName) instanceof IPrintFormCreator)
                    {
                        IPrintFormCreator componentPrint = (IPrintFormCreator) ApplicationRuntime.getBean(printName);
                        if (null != componentPrint)
                        {
                            enabledExtractIds.add(extract.getId());
                        }
                    }
                }
            }
        }

        wholeIdsSet.removeAll(enabledExtractIds);
        return wholeIdsSet;
    }

    @Override
    public List<Term> getExtractTerms(ModularStudentExtract extract)
    {
        return new DQLSelectBuilder()
                .fromEntity(StuExtractToTermRelation.class, "settr")
                .column(property(StuExtractToTermRelation.term().fromAlias("settr")))
                .where(eq(property(StuExtractToTermRelation.extract().fromAlias("settr")), value(extract)))
                .order(property(StuExtractToTermRelation.term().intValue().fromAlias("settr")))
                .createStatement(getSession())
                .list();
    }

    @Override
    public StudentExcludeOrderData getStudentExcludeOrderData(Student student)
    {
        DQLSelectBuilder typeBuilder = new DQLSelectBuilder();
        typeBuilder
                .fromEntity(StudentExtractTypeToGroup.class, "settg")
                .column(property(StudentExtractTypeToGroup.type().id().fromAlias("settg")))
                .where(eq(property(StudentExtractTypeToGroup.group().code().fromAlias("settg")), value(StudentExtractGroupCodes.EXCLUDE)));

        // TODO optimize in DEV-3640
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder
                .fromEntity(AbstractStudentExtract.class, "ase")
                .where(eq(property(AbstractStudentExtract.entity().id().fromAlias("ase")), value(student.getId())))
                .where(eq(property(AbstractStudentExtract.committed().fromAlias("ase")), value(Boolean.TRUE)))
                .where(in(property(AbstractStudentExtract.type().id().fromAlias("ase")), typeBuilder.buildQuery()))
                .order(property(AbstractStudentExtract.paragraph().order().commitDate().fromAlias("ase")), OrderDirection.desc);

        AbstractStudentExtract extract = builder.createStatement(getSession()).setMaxResults(1).uniqueResult();

        if (extract == null)
            return null;

        StudentOrderReasons reason = extract instanceof ModularStudentExtract ? ((ModularStudentExtract) extract).getReason() : ((StudentListOrder) extract.getParagraph().getOrder()).getReason();

        return new StudentExcludeOrderData(
                extract.getParagraph().getOrder().getNumber(),
                extract.getParagraph().getOrder().getCommitDate(),
                extract.getBeginDate(),
                reason != null ? (reason.getPrintTitle() != null ? reason.getPrintTitle() : reason.getTitle()) : null);
    }

    @Override
    @Deprecated
    public Collection<Long> getChildSpecializationIDs(EducationLevelsHighSchool eduLevelsHighSchool, OrgUnit formativeOrgUnit, OrgUnit territorialOrgUnit)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "e")
                .predicate(DQLPredicateType.distinct)
                .column(property("e", EducationOrgUnit.educationLevelHighSchool().id()))
                .where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().educationLevel().parentLevel()), value(EducationOrgUnitUtil.getParentLevel(eduLevelsHighSchool))));

        if (null != formativeOrgUnit)
            builder.where(eq(property("e", EducationOrgUnit.formativeOrgUnit()), value(formativeOrgUnit)));

        if (null != territorialOrgUnit)
            builder.where(eq(property("e", EducationOrgUnit.territorialOrgUnit()), value(territorialOrgUnit)));

        return builder.createStatement(getSession()).list();
    }

    @Override
    public boolean isModularOrderIndividual(StudentExtractType extractType)
    {
        return existsEntity(
                ExtractCreationRule.class,
                ExtractCreationRule.L_STUDENT_EXTRACT_TYPE, extractType,
                ExtractCreationRule.P_INDIVIDUAL_ORDER, Boolean.TRUE
        );
    }

    @Override
    public boolean isModularOrderIndividual(StudentModularOrder order)
    {
        List<Boolean> result = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                .top(2)
                .column(property("r", ExtractCreationRule.individualOrder()))
                .joinEntity("e", DQLJoinType.left, ExtractCreationRule.class, "r",
                            eq(property("e", AbstractStudentExtract.type().id()),
                               property("r", ExtractCreationRule.studentExtractType().id()))
                )
                .where(eq(property("e", AbstractStudentExtract.paragraph().order().id()), value(order.getId())))
                .createStatement(getSession()).list();
        return result.size() == 1 && result.get(0) != null && result.get(0);
    }

    @Override
    public List<StuExtractToBasicRelation> getPrioritySortedExtractBasicsList(ModularStudentExtract extract)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(StuExtractToBasicRelation.class, "sebr").column("sebr")
                .joinEntity("sebr", DQLJoinType.left, StudentReasonToBasicRel.class, "rel",
                            eq(property("sebr", StuExtractToBasicRelation.basic().id()),
                               property("rel", StudentReasonToBasicRel.second().id()))
                )
                .where(eq(property(StuExtractToBasicRelation.extract().id().fromAlias("sebr")), value(extract.getId())))
                .where(eq(property("rel", StudentReasonToBasicRel.first().id()), value(extract.getReason().getId())))
                .order(property("rel", StudentReasonToBasicRel.priority()));

        return builder.createStatement(getSession()).list();
    }

    @Override
    public List<StuListOrderToBasicRelation> getPrioritySortedListOrderBasicsList(StudentListOrder order)
    {
        if (order.getReason() == null)
            return Collections.emptyList();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(StuListOrderToBasicRelation.class, "sebr").column("sebr")
                .joinEntity("sebr", DQLJoinType.left, StudentReasonToBasicRel.class, "rel",
                            eq(property("sebr", StuListOrderToBasicRelation.basic().id()),
                               property("rel", StudentReasonToBasicRel.second().id()))
                )
                .where(eq(property(StuListOrderToBasicRelation.order().id().fromAlias("sebr")), value(order.getId())))
                .where(eq(property("rel", StudentReasonToBasicRel.first().id()), value(order.getReason().getId())))
                .order(property("rel", StudentReasonToBasicRel.priority()));

        return builder.createStatement(getSession()).list();
    }

    @Override
    public List<StuOtherOrderToBasicRelation> getPrioritySortedExtractBasicsList(OtherStudentExtract extract)
    {
        if (extract.getReason() == null)
            return Collections.emptyList();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(StuOtherOrderToBasicRelation.class, "sebr").column("sebr")
                .joinEntity("sebr", DQLJoinType.left, StudentReasonToBasicRel.class, "rel",
                            eq(property("sebr", StuOtherOrderToBasicRelation.basic().id()),
                               property("rel", StudentReasonToBasicRel.second().id()))
                )
                .where(eq(property(StuOtherOrderToBasicRelation.extract().id().fromAlias("sebr")), value(extract.getId())))
                .where(eq(property("rel", StudentReasonToBasicRel.first().id()), value(extract.getReason().getId())))
                .order(property("rel", StudentReasonToBasicRel.priority()));

        return builder.createStatement(getSession()).list();
    }

    @Override
    public Set<Long> getPrintFormAttachedOtherOrders(List<OtherStudentExtract> extractsList)
    {
        // TODO optimize in DEV-3640
        final Set<Long> enablePrintExtractIds = new HashSet<>();

        BatchUtils.execute(extractsList, DQL.MAX_VALUES_ROW_NUMBER, extractsPortion -> {
            final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(OtherStudentExtract.class, "se")
                    .joinEntity("se", DQLJoinType.inner, StudentOtherParagraph.class, "sp", eq(property(OtherStudentExtract.paragraph().id().fromAlias("se")), property(StudentOtherParagraph.id().fromAlias("sp"))))
                    .joinEntity("sp", DQLJoinType.inner, StudentOtherOrder.class, "so", eq(property(StudentOtherParagraph.order().id().fromAlias("sp")), property(StudentOtherOrder.id().fromAlias("so"))))
                    .joinEntity("so", DQLJoinType.inner, StudentOtherOrderTextRelation.class, "rel", eq(property(StudentOtherOrder.id().fromAlias("so")), property(StudentOtherOrderTextRelation.order().id().fromAlias("rel"))))
                    .where(in(property("se"), extractsPortion))
                    .column(property(OtherStudentExtract.id().fromAlias("se")));
            enablePrintExtractIds.addAll(builder.createStatement(getSession()).<Long>list());
        });

        Set<Long> wholeIdsSet = new HashSet<>();
        for (AbstractStudentExtract extract : extractsList)
        {
            wholeIdsSet.add(extract.getId());
        }

        wholeIdsSet.removeAll(enablePrintExtractIds);
        return wholeIdsSet;
    }

    @Override
    @SuppressWarnings("unchecked")
    public String getCustomActionComponent(ICustomOrderAction action)
    {
        Map<String, String> componentMap = (Map<String, String>) ApplicationRuntime.getBean(MoveStudentDefines.CUSTOM_ACTIONS_COMPONENT_MAP);
        return componentMap.get(EntityRuntime.getMeta(action).getName());
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<String, String> getPossibleCustomActionComponents(boolean isListOrder)
    {
        Map<String, String> componentMap = (Map<String, String>) ApplicationRuntime.getBean(MoveStudentDefines.CUSTOM_ACTIONS_COMPONENT_MAP);
        Set<String> customActions = (Set) ApplicationRuntime.getBean(isListOrder ? MoveStudentDefines.LIST_CUSTOM_ACTIONS_ENTRY_SET : MoveStudentDefines.MODULAR_CUSTOM_ACTIONS_ENTRY_SET);
        Map<String, String> resultMap = new LinkedHashMap<>(customActions.size());
        for (String entryName : customActions)
        {
            String componentName = componentMap.get(entryName);
            if (componentName != null && !componentName.isEmpty())
                resultMap.put(entryName, componentName);
        }
        return resultMap;
    }

    @Override
    public Map<ICustomOrderAction, String> getCustomActions(Long extractId, boolean isListOrder)
    {
        List<ICustomOrderAction> actionList = getList(ICustomOrderAction.class, ICustomOrderActionGen.customExtract().id(), extractId);

        // Далее немного химичим с мапами, чтобы порядок экшенов был такой, который задана в спринге
        // Это важно для отображения на публикаторе и для порядка проведения (чтобы смена группы шла до назначения старостой)
        Map<String, ICustomOrderAction> actionStrMap = new HashMap<>(actionList.size());
        for (ICustomOrderAction action : actionList)
        {
            actionStrMap.put(getCustomActionComponent(action), action);
        }

        Map<ICustomOrderAction, String> resultActionStrMap = new LinkedHashMap<>(actionList.size());
        for (String component : getPossibleCustomActionComponents(isListOrder).values())
        {
            ICustomOrderAction action = actionStrMap.get(component);
            if (action != null)
            {
                resultActionStrMap.put(action, component);
                actionStrMap.remove(component);
            }
        }

        // Если вдруг остались экшены, которые не определены в спринге для данного типа приказа... то это, вроде бы, нехорошо.
        if (!actionStrMap.isEmpty())
            throw new IllegalStateException();

        return resultActionStrMap;
    }

    @Override
    public boolean hasThirdLevelOtherOrders()
    {
        return existsEntity(StudentExtractType.class, StudentExtractType.parent().parent().code().s(), StudentExtractTypeCodes.OTHER_ORDER);
    }

    @Override
    public void addCustomConditionToGroupSelectBuilder(DQLSelectBuilder builder, String alias)
    {
    }

    @Override
    public void addFormativeOrgUnitToBuilder(DQLSelectBuilder builder, OrgUnit formativeOrgUnit)
    {
        builder.where(eq(property(EducationOrgUnit.formativeOrgUnit().fromAlias("eou")), value(formativeOrgUnit)));
    }

    private static Long _print_mod_id_cache;

    @Override
    public IMoveStudentInjectModifier getScriptInjectModifier()
    {
        final MovestudentScriptItem scriptItem;
        if (_print_mod_id_cache == null) {
            scriptItem = getCatalogItem(MovestudentScriptItem.class, MovestudentScriptItemCodes.ORDERS_PRINT_MODIFIER);
            _print_mod_id_cache = scriptItem.getId();
        } else {
            scriptItem = get(_print_mod_id_cache);
        }

        if (scriptItem.getUserScriptEditDate() != null && scriptItem.getUserScript() != null) {
            return IUniScriptDao.instance.get().buildScriptItemInstance(scriptItem, IMoveStudentInjectModifier.class);
        }

        return null;
    }

    @Override
    public String getStuListExtractScientificAdvisorStr(AbstractStudentExtract extract, PpsEntry advisor)
    {
        if (null == advisor) return "";

        StringBuilder sb = new StringBuilder();
        sb.append(advisor.getTitleFioInfoOrgUnit());
        List<DegreeToExtractRelation> degreesList = getList(DegreeToExtractRelation.class, DegreeToExtractRelation.extract().s(), extract);
        List<StatusToExtractRelation> statusList = getList(StatusToExtractRelation.class, StatusToExtractRelation.extract().s(), extract);
        for (DegreeToExtractRelation rel : degreesList) sb.append(", ").append(rel.getDegree().getShortTitle());
        for (StatusToExtractRelation rel : statusList) sb.append(", ").append(rel.getStatus().getShortTitle());
        return sb.toString();
    }
}