/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e42.ParagraphAddEdit;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.IExtEducationLevelModel;

import java.util.Date;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 28.08.2013
 */
public class Model extends AbstractListParagraphAddEditModel<TerritorialTransferExtStuListExtract> implements IEducationLevelModel, IExtEducationLevelModel, IGroupModel
{
    private Course _course;

    private ISelectModel _groupNewListModel;
    private Group _groupNew;

    private ISelectModel _groupOldListModel;
    private Group _groupOld;

    private ISelectModel _territorialOrgUnitsListModel;
    private OrgUnit _territorialOrgUnitNew;
    private OrgUnit _territorialOrgUnitOld;
    private EducationLevelsHighSchool _educationLevelsHighSchool;

    private ISelectModel _specializationsListModel;

    private List<CompensationType> _compensationTypeList;
    private CompensationType _compensationTypeOld;
    private CompensationType _compensationTypeNew;

    private boolean _changeEduPlan;
    private Date _changeEduPlanDeadlineDate;


    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public OrgUnit getTerritorialOrgUnitNew()
    {
        return _territorialOrgUnitNew;
    }

    public void setTerritorialOrgUnitNew(OrgUnit territorialOrgUnitNew)
    {
        _territorialOrgUnitNew = territorialOrgUnitNew;
    }

    public Group getGroupNew()
    {
        return _groupNew;
    }

    public void setGroupNew(Group groupNew)
    {
        _groupNew = groupNew;
    }

    public Group getGroupOld()
    {
        return _groupOld;
    }

    public void setGroupOld(Group groupOld)
    {
        _groupOld = groupOld;
    }

    public ISelectModel getGroupNewListModel()
    {
        return _groupNewListModel;
    }

    public void setGroupNewListModel(ISelectModel groupNewListModel)
    {
        _groupNewListModel = groupNewListModel;
    }

    public ISelectModel getGroupOldListModel()
    {
        return _groupOldListModel;
    }

    public void setGroupOldListModel(ISelectModel groupOldListModel)
    {
        _groupOldListModel = groupOldListModel;
    }

    public OrgUnit getTerritorialOrgUnitOld()
    {
        return _territorialOrgUnitOld;
    }

    public void setTerritorialOrgUnitOld(OrgUnit territorialOrgUnitOld)
    {
        _territorialOrgUnitOld = territorialOrgUnitOld;
    }

    public ISelectModel getTerritorialOrgUnitsListModel()
    {
        return _territorialOrgUnitsListModel;
    }

    public void setTerritorialOrgUnitsListModel(ISelectModel territorialOrgUnitsListModel)
    {
        _territorialOrgUnitsListModel = territorialOrgUnitsListModel;
    }

    public ISelectModel getSpecializationsListModel()
    {
        return _specializationsListModel;
    }

    public void setSpecializationsListModel(ISelectModel specializationsListModel)
    {
        _specializationsListModel = specializationsListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public CompensationType getCompensationTypeOld()
    {
        return _compensationTypeOld;
    }

    public void setCompensationTypeOld(CompensationType compensationTypeOld)
    {
        _compensationTypeOld = compensationTypeOld;
    }

    public CompensationType getCompensationTypeNew()
    {
        return _compensationTypeNew;
    }

    public void setCompensationTypeNew(CompensationType compensationTypeNew)
    {
        _compensationTypeNew = compensationTypeNew;
    }

    public boolean isChangeEduPlan()
    {
        return _changeEduPlan;
    }

    public void setChangeEduPlan(boolean changeEduPlan)
    {
        this._changeEduPlan = changeEduPlan;
    }

    public Date getChangeEduPlanDeadlineDate()
    {
        return _changeEduPlanDeadlineDate;
    }

    public void setChangeEduPlanDeadlineDate(Date changeEduPlanDeadlineDate)
    {
        this._changeEduPlanDeadlineDate = changeEduPlanDeadlineDate;
    }

    public boolean isAnyOrderParametersCantBeChanged()
    {
        return !isParagraphOnlyOneInTheOrder();
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return getGroupOld() != null ? getGroupOld().getEducationOrgUnit().getFormativeOrgUnit() : null;
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return getTerritorialOrgUnitNew();
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return getGroupOld() != null? getGroupOld().getEducationOrgUnit().getDevelopForm():null;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return getGroupOld() != null? getGroupOld().getEducationOrgUnit().getDevelopCondition():null;
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return getGroupOld() != null? getGroupOld().getEducationOrgUnit().getDevelopTech():null;
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return getGroupOld() != null? getGroupOld().getEducationOrgUnit().getDevelopPeriod():null;
    }

    @Override
    public EducationLevels getParentEduLevel()
    {
        return EducationOrgUnitUtil.getParentLevel(getGroupNew().getEducationOrgUnit().getEducationLevelHighSchool());
    }
}
