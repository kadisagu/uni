/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e0.OrgUnitExtractAddEdit;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author dseleznev
 * Created on: 18.03.2010
 */
@Input({
    @Bind(key = "orgUnitId", binding = "orgUnitId")
})
@Output({
    @Bind(key = "formingOrgUnitId", binding = "orgUnitId"),
    @Bind(key = "studentId", binding = "student.id"),
    @Bind(key = "extractTypeId", binding = "extractType.id")
})
public class Model
{
    Long _orgUnitId;
    
    Student _student;
    ISelectModel _studentModel;
    
    private StudentExtractType _extractType;
    private List<StudentExtractType> _extractTypeList;
    private ISelectModel _extractTypeListModel;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this._orgUnitId = orgUnitId;
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        this._student = student;
    }

    public ISelectModel getStudentModel()
    {
        return _studentModel;
    }

    public void setStudentModel(ISelectModel studentModel)
    {
        this._studentModel = studentModel;
    }

    public StudentExtractType getExtractType()
    {
        return _extractType;
    }

    public void setExtractType(StudentExtractType extractType)
    {
        this._extractType = extractType;
    }

    public List<StudentExtractType> getExtractTypeList()
    {
        return _extractTypeList;
    }

    public void setExtractTypeList(List<StudentExtractType> extractTypeList)
    {
        this._extractTypeList = extractTypeList;
    }

    public ISelectModel getExtractTypeListModel()
    {
        return _extractTypeListModel;
    }

    public void setExtractTypeListModel(ISelectModel extractTypeListModel)
    {
        _extractTypeListModel = extractTypeListModel;
    }
}