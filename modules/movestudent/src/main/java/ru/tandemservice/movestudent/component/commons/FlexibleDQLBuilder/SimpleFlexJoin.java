/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

/**
 * @author Nikolay Fedorovskih
 * @since 29.11.2013
 */
public class SimpleFlexJoin implements IFlexJoin
{
    private String _alias;
    private String _leftAlias;
    private String _path;
    private DQLJoinType _joinType;
    private boolean _required;

    public SimpleFlexJoin(String alias, String leftAlias, String path, DQLJoinType joinType)
    {
        _alias = alias;
        _leftAlias = leftAlias;
        _path = path;
        _joinType = joinType;
    }

    public SimpleFlexJoin(String alias, String leftAlias, String path, boolean required)
    {
        this(alias, leftAlias, path, DQLJoinType.inner);
        _required = required;
    }

    public SimpleFlexJoin(String alias, String leftAlias, String path)
    {
        this(alias, leftAlias, path, DQLJoinType.inner);
    }

    @Override
    public String getLeftAlias()
    {
        return _leftAlias;
    }

    @Override
    public boolean isRequired()
    {
        return _required;
    }

    @Override
    public String getAlias()
    {
        return _alias;
    }

    @Override
    public void addToBuilder(DQLSelectBuilder builder)
    {
        builder.joinPath(_joinType, _leftAlias + "." + _path, _alias);
    }
}