package ru.tandemservice.movestudent.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;


import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudent_2x7x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {


		////////////////////////////////////////////////////////////////////////////////
		// сущность abstractStudentExtract


        // создано свойство comment
        {
            // создать колонку
            tool.createColumn("abstractstudentextract_t", new DBColumn("comment_p", DBType.TEXT));

        }

		// создано обязательное свойство commentEdited
		{
			// создать колонку
			tool.createColumn("abstractstudentextract_t", new DBColumn("commentedited_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultCommentEdited = false;
			tool.executeUpdate("update abstractstudentextract_t set commentedited_p=? where commentedited_p is null", defaultCommentEdited);

			// сделать колонку NOT NULL
			tool.setColumnNullable("abstractstudentextract_t", "commentedited_p", false);

		}

        copyEditedComment(tool);
        copyOtherOrdersComment(tool);

        // удалено свойство orderComment
        {
            // удалить колонку
            tool.dropColumn("abstractstudentextract_t", "ordercomment_id");

        }



        ////////////////////////////////////////////////////////////////////////////////
		// сущность otherStudentExtract

		// удалено свойство comment
		{
			// удалить колонку
			tool.dropColumn("otherstudentextract_t", "comment_p");

		}

        ////////////////////////////////////////////////////////////////////////////////
        // сущность orderComment

        // сущность была удалена
        {
            // удалить таблицу
            tool.dropTable("ordercomment_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("orderComment");

        }

    }


    /**
     * Получаем выписки, для которых были сгенерированы комментарии и переносим эти комментарии в абстрактную выписку
     * @param tool
     */
    private void copyEditedComment(DBTool tool) throws Exception
    {

        String queryComment = "SELECT extr.id, com.comment_p\n" +
                "FROM abstractstudentextract_t extr\n" +
                "inner join ordercomment_t com on extr.ordercomment_id =com.id\n" +
                "WHERE com.edit_p = ?\n" +
                "ORDER BY extr.id asc";


        PreparedStatement commentPrpSt = tool.prepareStatement(queryComment);

        commentPrpSt.setBoolean(1, true); // комментарий отредактирован вручную

        ResultSet resSet = commentPrpSt.executeQuery();
        while (resSet.next())
        {
            Long extractId = resSet.getLong(1);
            String comment = resSet.getString(2);

            PreparedStatement updateEx = tool.prepareStatement("UPDATE abstractstudentextract_t " +
                                                                       "SET " +
                                                                       "comment_p=?, commentedited_p=? " +
                                                                       "WHERE id=?");
            updateEx.setString(1, comment);
            updateEx.setBoolean(2, true);
            updateEx.setLong(3, extractId);
            updateEx.execute();
        }
    }


    /**
     * Переносим комментарии из прочих приказов
     */
    private void copyOtherOrdersComment(DBTool tool) throws Exception
    {
        String queryComment = "SELECT extr.id, extr.comment_p\n" +
                "FROM otherstudentextract_t extr\n" +
                "WHERE extr.comment_p IS NOT NULL\n" +
                "ORDER BY extr.id asc";

        PreparedStatement commentPrpSt = tool.prepareStatement(queryComment);
        ResultSet resSet = commentPrpSt.executeQuery();

        PreparedStatement updateEx = tool.prepareStatement("UPDATE abstractstudentextract_t " +
                                                                   "SET " +
                                                                   "comment_p=?, commentedited_p=? " +
                                                                   "WHERE id=?");

        while (resSet.next())
        {
            Long extractId = resSet.getLong(1);
            String comment = resSet.getString(2);
            updateEx.clearParameters();

            updateEx.setString(1, comment);
            updateEx.setBoolean(2, true);
            updateEx.setLong(3, extractId);
            updateEx.execute();
        }
    }

}