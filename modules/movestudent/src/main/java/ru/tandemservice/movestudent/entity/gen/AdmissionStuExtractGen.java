package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AdmissionStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О допуске к занятиям
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AdmissionStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.AdmissionStuExtract";
    public static final String ENTITY_NAME = "admissionStuExtract";
    public static final int VERSION_HASH = -1065883787;
    private static IEntityMeta ENTITY_META;

    public static final String P_ADMISSION_DATE = "admissionDate";
    public static final String P_PAYMENTS_DEBT = "paymentsDebt";
    public static final String P_DISCHARGE_ORDER_NUMBER = "dischargeOrderNumber";
    public static final String P_DISCHARGE_ORDER_DATE = "dischargeOrderDate";
    public static final String P_PAYING_OFF_DATE = "payingOffDate";
    public static final String P_HAS_DEBTS = "hasDebts";
    public static final String P_DEADLINE = "deadline";

    private Date _admissionDate;     // Дата допуска
    private boolean _paymentsDebt;     // Была задолженность по оплате
    private String _dischargeOrderNumber;     // № приказа
    private Date _dischargeOrderDate;     // Дата приказа
    private Date _payingOffDate;     // Дата погашения задолженности
    private boolean _hasDebts;     // Разница в учебных планах
    private Date _deadline;     // Срок ликвидации задолженности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата допуска.
     */
    public Date getAdmissionDate()
    {
        return _admissionDate;
    }

    /**
     * @param admissionDate Дата допуска.
     */
    public void setAdmissionDate(Date admissionDate)
    {
        dirty(_admissionDate, admissionDate);
        _admissionDate = admissionDate;
    }

    /**
     * @return Была задолженность по оплате. Свойство не может быть null.
     */
    @NotNull
    public boolean isPaymentsDebt()
    {
        return _paymentsDebt;
    }

    /**
     * @param paymentsDebt Была задолженность по оплате. Свойство не может быть null.
     */
    public void setPaymentsDebt(boolean paymentsDebt)
    {
        dirty(_paymentsDebt, paymentsDebt);
        _paymentsDebt = paymentsDebt;
    }

    /**
     * @return № приказа.
     */
    @Length(max=255)
    public String getDischargeOrderNumber()
    {
        return _dischargeOrderNumber;
    }

    /**
     * @param dischargeOrderNumber № приказа.
     */
    public void setDischargeOrderNumber(String dischargeOrderNumber)
    {
        dirty(_dischargeOrderNumber, dischargeOrderNumber);
        _dischargeOrderNumber = dischargeOrderNumber;
    }

    /**
     * @return Дата приказа.
     */
    public Date getDischargeOrderDate()
    {
        return _dischargeOrderDate;
    }

    /**
     * @param dischargeOrderDate Дата приказа.
     */
    public void setDischargeOrderDate(Date dischargeOrderDate)
    {
        dirty(_dischargeOrderDate, dischargeOrderDate);
        _dischargeOrderDate = dischargeOrderDate;
    }

    /**
     * @return Дата погашения задолженности.
     */
    public Date getPayingOffDate()
    {
        return _payingOffDate;
    }

    /**
     * @param payingOffDate Дата погашения задолженности.
     */
    public void setPayingOffDate(Date payingOffDate)
    {
        dirty(_payingOffDate, payingOffDate);
        _payingOffDate = payingOffDate;
    }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     */
    @NotNull
    public boolean isHasDebts()
    {
        return _hasDebts;
    }

    /**
     * @param hasDebts Разница в учебных планах. Свойство не может быть null.
     */
    public void setHasDebts(boolean hasDebts)
    {
        dirty(_hasDebts, hasDebts);
        _hasDebts = hasDebts;
    }

    /**
     * @return Срок ликвидации задолженности.
     */
    public Date getDeadline()
    {
        return _deadline;
    }

    /**
     * @param deadline Срок ликвидации задолженности.
     */
    public void setDeadline(Date deadline)
    {
        dirty(_deadline, deadline);
        _deadline = deadline;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof AdmissionStuExtractGen)
        {
            setAdmissionDate(((AdmissionStuExtract)another).getAdmissionDate());
            setPaymentsDebt(((AdmissionStuExtract)another).isPaymentsDebt());
            setDischargeOrderNumber(((AdmissionStuExtract)another).getDischargeOrderNumber());
            setDischargeOrderDate(((AdmissionStuExtract)another).getDischargeOrderDate());
            setPayingOffDate(((AdmissionStuExtract)another).getPayingOffDate());
            setHasDebts(((AdmissionStuExtract)another).isHasDebts());
            setDeadline(((AdmissionStuExtract)another).getDeadline());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AdmissionStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AdmissionStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new AdmissionStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "admissionDate":
                    return obj.getAdmissionDate();
                case "paymentsDebt":
                    return obj.isPaymentsDebt();
                case "dischargeOrderNumber":
                    return obj.getDischargeOrderNumber();
                case "dischargeOrderDate":
                    return obj.getDischargeOrderDate();
                case "payingOffDate":
                    return obj.getPayingOffDate();
                case "hasDebts":
                    return obj.isHasDebts();
                case "deadline":
                    return obj.getDeadline();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "admissionDate":
                    obj.setAdmissionDate((Date) value);
                    return;
                case "paymentsDebt":
                    obj.setPaymentsDebt((Boolean) value);
                    return;
                case "dischargeOrderNumber":
                    obj.setDischargeOrderNumber((String) value);
                    return;
                case "dischargeOrderDate":
                    obj.setDischargeOrderDate((Date) value);
                    return;
                case "payingOffDate":
                    obj.setPayingOffDate((Date) value);
                    return;
                case "hasDebts":
                    obj.setHasDebts((Boolean) value);
                    return;
                case "deadline":
                    obj.setDeadline((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "admissionDate":
                        return true;
                case "paymentsDebt":
                        return true;
                case "dischargeOrderNumber":
                        return true;
                case "dischargeOrderDate":
                        return true;
                case "payingOffDate":
                        return true;
                case "hasDebts":
                        return true;
                case "deadline":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "admissionDate":
                    return true;
                case "paymentsDebt":
                    return true;
                case "dischargeOrderNumber":
                    return true;
                case "dischargeOrderDate":
                    return true;
                case "payingOffDate":
                    return true;
                case "hasDebts":
                    return true;
                case "deadline":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "admissionDate":
                    return Date.class;
                case "paymentsDebt":
                    return Boolean.class;
                case "dischargeOrderNumber":
                    return String.class;
                case "dischargeOrderDate":
                    return Date.class;
                case "payingOffDate":
                    return Date.class;
                case "hasDebts":
                    return Boolean.class;
                case "deadline":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AdmissionStuExtract> _dslPath = new Path<AdmissionStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AdmissionStuExtract");
    }
            

    /**
     * @return Дата допуска.
     * @see ru.tandemservice.movestudent.entity.AdmissionStuExtract#getAdmissionDate()
     */
    public static PropertyPath<Date> admissionDate()
    {
        return _dslPath.admissionDate();
    }

    /**
     * @return Была задолженность по оплате. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmissionStuExtract#isPaymentsDebt()
     */
    public static PropertyPath<Boolean> paymentsDebt()
    {
        return _dslPath.paymentsDebt();
    }

    /**
     * @return № приказа.
     * @see ru.tandemservice.movestudent.entity.AdmissionStuExtract#getDischargeOrderNumber()
     */
    public static PropertyPath<String> dischargeOrderNumber()
    {
        return _dslPath.dischargeOrderNumber();
    }

    /**
     * @return Дата приказа.
     * @see ru.tandemservice.movestudent.entity.AdmissionStuExtract#getDischargeOrderDate()
     */
    public static PropertyPath<Date> dischargeOrderDate()
    {
        return _dslPath.dischargeOrderDate();
    }

    /**
     * @return Дата погашения задолженности.
     * @see ru.tandemservice.movestudent.entity.AdmissionStuExtract#getPayingOffDate()
     */
    public static PropertyPath<Date> payingOffDate()
    {
        return _dslPath.payingOffDate();
    }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmissionStuExtract#isHasDebts()
     */
    public static PropertyPath<Boolean> hasDebts()
    {
        return _dslPath.hasDebts();
    }

    /**
     * @return Срок ликвидации задолженности.
     * @see ru.tandemservice.movestudent.entity.AdmissionStuExtract#getDeadline()
     */
    public static PropertyPath<Date> deadline()
    {
        return _dslPath.deadline();
    }

    public static class Path<E extends AdmissionStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _admissionDate;
        private PropertyPath<Boolean> _paymentsDebt;
        private PropertyPath<String> _dischargeOrderNumber;
        private PropertyPath<Date> _dischargeOrderDate;
        private PropertyPath<Date> _payingOffDate;
        private PropertyPath<Boolean> _hasDebts;
        private PropertyPath<Date> _deadline;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата допуска.
     * @see ru.tandemservice.movestudent.entity.AdmissionStuExtract#getAdmissionDate()
     */
        public PropertyPath<Date> admissionDate()
        {
            if(_admissionDate == null )
                _admissionDate = new PropertyPath<Date>(AdmissionStuExtractGen.P_ADMISSION_DATE, this);
            return _admissionDate;
        }

    /**
     * @return Была задолженность по оплате. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmissionStuExtract#isPaymentsDebt()
     */
        public PropertyPath<Boolean> paymentsDebt()
        {
            if(_paymentsDebt == null )
                _paymentsDebt = new PropertyPath<Boolean>(AdmissionStuExtractGen.P_PAYMENTS_DEBT, this);
            return _paymentsDebt;
        }

    /**
     * @return № приказа.
     * @see ru.tandemservice.movestudent.entity.AdmissionStuExtract#getDischargeOrderNumber()
     */
        public PropertyPath<String> dischargeOrderNumber()
        {
            if(_dischargeOrderNumber == null )
                _dischargeOrderNumber = new PropertyPath<String>(AdmissionStuExtractGen.P_DISCHARGE_ORDER_NUMBER, this);
            return _dischargeOrderNumber;
        }

    /**
     * @return Дата приказа.
     * @see ru.tandemservice.movestudent.entity.AdmissionStuExtract#getDischargeOrderDate()
     */
        public PropertyPath<Date> dischargeOrderDate()
        {
            if(_dischargeOrderDate == null )
                _dischargeOrderDate = new PropertyPath<Date>(AdmissionStuExtractGen.P_DISCHARGE_ORDER_DATE, this);
            return _dischargeOrderDate;
        }

    /**
     * @return Дата погашения задолженности.
     * @see ru.tandemservice.movestudent.entity.AdmissionStuExtract#getPayingOffDate()
     */
        public PropertyPath<Date> payingOffDate()
        {
            if(_payingOffDate == null )
                _payingOffDate = new PropertyPath<Date>(AdmissionStuExtractGen.P_PAYING_OFF_DATE, this);
            return _payingOffDate;
        }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmissionStuExtract#isHasDebts()
     */
        public PropertyPath<Boolean> hasDebts()
        {
            if(_hasDebts == null )
                _hasDebts = new PropertyPath<Boolean>(AdmissionStuExtractGen.P_HAS_DEBTS, this);
            return _hasDebts;
        }

    /**
     * @return Срок ликвидации задолженности.
     * @see ru.tandemservice.movestudent.entity.AdmissionStuExtract#getDeadline()
     */
        public PropertyPath<Date> deadline()
        {
            if(_deadline == null )
                _deadline = new PropertyPath<Date>(AdmissionStuExtractGen.P_DEADLINE, this);
            return _deadline;
        }

        public Class getEntityClass()
        {
            return AdmissionStuExtract.class;
        }

        public String getEntityName()
        {
            return "admissionStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
