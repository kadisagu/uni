/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.IDQLExpression;

/**
 * @author Nikolay Fedorovskih
 * @since 27.11.2013
 */
public class StringFlexFilter extends BaseFlexFilter
{
    public StringFlexFilter(String name, String alias, String path)
    {
        super(name, alias, path);
    }

    @Override
    protected IDQLExpression internalCreateCondition(Object o)
    {
        String filter = (String) o;
        if (StringUtils.isNotEmpty((String) o))
            return DQLExpressions.likeUpper(DQLExpressions.property(getAlias(), getPath()), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true)));

        return null;
    }
}