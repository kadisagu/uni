/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e28.ParagraphAddEdit;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.EntityUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDao;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unimove.IAbstractExtract;

import java.util.*;

/**
 * @author ListExtractComponentGenerator
 * @since 14.09.2012
 */
public class DAO extends AbstractListParagraphAddEditDAO<AddGroupManagerStuListExtract, Model> implements IDAO
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setEducationYearList(getCatalogItemList(EducationYear.class));
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));

        model.setSelectedGroupListModel(new BaseMultiSelectModel()
        {
            public ListResult findValues(String filter)
            {
                return findValues(filter, false);
            }

            public ListResult findValues(String filter, boolean allResults)
            {
                if (model.getGroup() == null)
                {
                    return ListResult.getEmpty();
                }

                EducationOrgUnit educationOrgUnit = (EducationOrgUnit) model.getDataSource().getCurrentValueEntity().getProperty(Student.educationOrgUnit());
                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder
                        .fromEntity(Group.class, "g")
                        .column("g")
                        .where(DQLExpressions.eq(DQLExpressions.property(Group.archival().fromAlias("g")), DQLExpressions.value(Boolean.FALSE)))
                        .where(DQLExpressions.eq(DQLExpressions.property(Group.educationOrgUnit().formativeOrgUnit().fromAlias("g")), DQLExpressions.value(educationOrgUnit.getFormativeOrgUnit())))
                        .where(DQLExpressions.eq(DQLExpressions.property(Group.educationOrgUnit().territorialOrgUnit().fromAlias("g")), DQLExpressions.value(educationOrgUnit.getTerritorialOrgUnit())));

                builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(Group.title().fromAlias("g"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                builder.order(DQLExpressions.property(Group.title().fromAlias("g")));

                if (allResults)
                    return new ListResult<>(builder.createStatement(getSession()).<Group>list());

                //Убираем уже выбранные в этом параграфе
                IValueMapHolder selectedGroupHolder = (IValueMapHolder) model.getDataSource().getColumn("selectedGroup");
                Map<Long, List<Group>> selectedGroupMap = (null == selectedGroupHolder ? Collections.emptyMap() : selectedGroupHolder.getValueMap());

                Long currentEntityId = model.getDataSource().getCurrentValueEntity().getId();

                List<Long> selectedGroupsIds = Lists.newArrayList();

                for (Long entityId : selectedGroupMap.keySet())
                {
                    if (!entityId.equals(currentEntityId))
                    {
                        selectedGroupsIds.addAll(EntityUtils.getIdsFromEntityList(selectedGroupMap.get(entityId)));

                    }
                }

                builder.where(DQLExpressions.not(DQLExpressions.in(DQLExpressions.property(Group.id().fromAlias("g")), selectedGroupsIds)));

                //Убираем выбранные групп в других параграфах данного приказа
                DQLSelectBuilder selectedGroupsBuilder = new DQLSelectBuilder().fromEntity(GroupToExtractRelation.class, "ge");
                selectedGroupsBuilder.where(DQLExpressions.eq(DQLExpressions.property("ge", GroupToExtractRelation.extract().paragraph().order().id()), DQLExpressions.value(model.getParagraph().getOrder().getId())));
                selectedGroupsBuilder.where(DQLExpressions.eq(DQLExpressions.property("ge", GroupToExtractRelation.extract().committed()), DQLExpressions.value(Boolean.FALSE)));

                if (model.isEditForm())
                {
                    selectedGroupsBuilder.where(DQLExpressions.not((DQLExpressions.eq(DQLExpressions.property("ge", GroupToExtractRelation.extract().paragraph().id()), DQLExpressions.value(model.getParagraph().getId())))));
                }

                List<GroupToExtractRelation> groupToExtractRelations = selectedGroupsBuilder.createStatement(getSession()).list();
                List<Long> selectedGroupsIdsFromOtherParagraphs = Lists.newArrayList();

                for (GroupToExtractRelation relation : groupToExtractRelations)
                {
                    selectedGroupsIdsFromOtherParagraphs.add(relation.getGroup().getId());
                }

                builder.where(DQLExpressions.not(DQLExpressions.in(DQLExpressions.property(Group.id().fromAlias("g")), selectedGroupsIdsFromOtherParagraphs)));
                MoveStudentDaoFacade.getMoveStudentDao().addCustomConditionToGroupSelectBuilder(builder, "g");

                int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                if (count > MoveStudentDao.MAX_ROWS)
                    builder.top(MoveStudentDao.MAX_ROWS);

                return new ListResult<>(builder.createStatement(getSession()).<Group>list(), count);
            }

            @Override
            public List getValues(Set primaryKeys)
            {
                List<Group> resultList = Lists.newArrayList();

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, "g");
                builder.where(DQLExpressions.in(DQLExpressions.property("g", Group.id()), primaryKeys));
                List<Group> groups = builder.createStatement(getSession()).list();

                for (Group group : groups)
                {
                    if (findValues("", true).getObjects().contains(group))
                        resultList.add(group);
                }
                return resultList;
            }
        });

        //заполняем поля значениями по умолчанию
        if (model.getParagraphId() == null)
        {
            model.setEducationYear(get(EducationYear.class, EducationYear.P_CURRENT, Boolean.TRUE));
        }

        //заполняем поля сохраненными данными
        if (model.getParagraphId() != null)
        {
            AddGroupManagerStuListExtract extract = model.getFirstExtract();
            model.setEducationYear(extract.getEducationYear());
            model.setCourse(extract.getCourse());
            model.setGroup(extract.getGroup());
            model.setCompensationType(extract.getCompensationType());
        }
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        if (model.getCompensationType() != null)
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));
    }

    @Override
    protected AddGroupManagerStuListExtract createNewInstance(Model model)
    {
        return new AddGroupManagerStuListExtract();
    }

    @Override
    protected void fillExtract(AddGroupManagerStuListExtract extract, Student student, Model model)
    {
        extract.setEducationYear(model.getEducationYear());
        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setCompensationType(model.getCompensationType());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        IValueMapHolder selectedGroupHolder = (IValueMapHolder) model.getDataSource().getColumn("selectedGroup");
        Map<Long, List<Group>> selectedGroupMap = (null == selectedGroupHolder ? Collections.emptyMap() : selectedGroupHolder.getValueMap());

        List<ViewWrapper<Student>> viewlist = new ArrayList<ViewWrapper<Student>>((Collection) ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).getSelectedObjects());

        Map<Long, List<GroupToExtractRelation>> selectedInOtherExtractsGroups = getSelectedInOtherExtractsGroups(selectedGroupMap, model.isEditForm() ? model.getParagraph() : null);

        for (ViewWrapper<Student> wrapper : viewlist)
        {
            if (selectedGroupMap.get(wrapper.getEntity().getId()).isEmpty())
                errorCollector.add("Поле «Группа» обязательно для заполнения.", "selectedGroupId_" + wrapper.getEntity().getId());
            else
            {
                if (null != selectedInOtherExtractsGroups.get(wrapper.getEntity().getId()))
                {
                    if (!selectedInOtherExtractsGroups.get(wrapper.getEntity().getId()).isEmpty())
                    {
                        List<String> groupsInfo = Lists.newArrayList();
                        for (GroupToExtractRelation relation : selectedInOtherExtractsGroups.get(wrapper.getEntity().getId()))
                        {
                            String info = relation.getGroup().getTitle();
                            AbstractStudentOrder order = relation.getExtract().getParagraph().getOrder();
                            if (null != order)
                            {
                                if (null != order.getNumber() && null != order.getCommitDate())
                                    info += " (приказ №" + order.getNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()) + ")";
                                else
                                    info += " (приказ созданный - " + order.getCreateDateFormatted() + ")";
                            }
                            else
                            {
                                info += " (проект приказа студента " + relation.getExtract().getEntity().getFio() + ")";
                            }
                            groupsInfo.add(info);
                        }
                        errorCollector.add(
                                (groupsInfo.size() > 1 ? "Группы выбраны в других приказах, проектах приказов: " : "Группа выбрана в другом приказе, проекте приказа: ") +
                                        StringUtils.join(groupsInfo, ", "), "selectedGroupId_" + wrapper.getEntity().getId()
                        );
                    }
                }
            }
        }

        super.update(model);

        List<AddGroupManagerStuListExtract> extractList = getList(AddGroupManagerStuListExtract.class, IAbstractExtract.L_PARAGRAPH, model.getParagraph());
        for (AddGroupManagerStuListExtract extract : extractList)
        {
            if (model.isEditForm())
            {
                DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(GroupToExtractRelation.class);
                deleteBuilder.where(DQLExpressions.eq(DQLExpressions.property(GroupToExtractRelation.extract().id()), DQLExpressions.value(extract.getId())));
                deleteBuilder.createStatement(getSession()).execute();
            }

            List<Group> selectedGroups = selectedGroupMap.get(extract.getEntity().getId());

            for (Group group : selectedGroups)
            {
                if (UniDaoFacade.getGroupDao().isCaptainStudent(group, extract.getEntity()))
                    errorCollector.add("Нельзя назначить студента старостой, поскольку он уже является старостой группы " + group.getTitle() + ".", "selectedGroupId_" + extract.getEntity().getId());

                List<Student> captainList = UniDaoFacade.getGroupDao().getCaptainStudentList(group);
                if (captainList.isEmpty())
                    insertGroupToExtractRelation(group, extract, null);
                else
                    for (Student captain : captainList)
                    {
                        insertGroupToExtractRelation(group, extract, captain);
                    }
            }
        }
    }

    private void insertGroupToExtractRelation(Group group, AbstractStudentExtract extract, Student student)
    {
        GroupToExtractRelation groupToExtractRelation = new GroupToExtractRelation();
        groupToExtractRelation.setGroup(group);
        groupToExtractRelation.setExtract(extract);
        groupToExtractRelation.setPrevGroupManager(student);
        save(groupToExtractRelation);
    }

    private Map<Long, List<GroupToExtractRelation>> getSelectedInOtherExtractsGroups(Map<Long, List<Group>> selectedGroups, StudentListParagraph paragraph)
    {
        Map<Long, List<GroupToExtractRelation>> selectedInOtherExtractsGroups = Maps.newHashMap();

        List<Long> selectedGroupsIds = Lists.newArrayList();

        for (Long entityId : selectedGroups.keySet())
        {
            selectedGroupsIds.addAll(EntityUtils.getIdsFromEntityList(selectedGroups.get(entityId)));
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GroupToExtractRelation.class, "ge");

        builder.where(DQLExpressions.in(DQLExpressions.property("ge", GroupToExtractRelation.group().id()), selectedGroupsIds));
        if (null != paragraph)
            builder.where(DQLExpressions.not(DQLExpressions.in(DQLExpressions.property("ge", GroupToExtractRelation.extract().id()), EntityUtils.getIdsFromEntityList(paragraph.getExtractList()))));
        List<GroupToExtractRelation> relations = builder.createStatement(getSession()).list();
        for (Long entityId : selectedGroups.keySet())
        {
            for (GroupToExtractRelation relation : relations)
            {
                if (selectedGroups.get(entityId).contains(relation.getGroup()))
                {
                    if (null == selectedInOtherExtractsGroups.get(entityId))
                        selectedInOtherExtractsGroups.put(entityId, Lists.<GroupToExtractRelation>newArrayList());
                    if (!selectedInOtherExtractsGroups.get(entityId).contains(relation))
                        selectedInOtherExtractsGroups.get(entityId).add(relation);
                }
            }
        }
        return selectedInOtherExtractsGroups;
    }
}