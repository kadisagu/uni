/* $Id$ */
package ru.tandemservice.movestudent.component.settings.IndividualOrderSettings;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.entity.ExtractCreationRule;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 25.09.2012
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        List<ExtractCreationRule> ruleList = new DQLSelectBuilder().fromEntity(ExtractCreationRule.class, "cr").column("cr")
                .where(eq(property(ExtractCreationRule.studentExtractType().active().fromAlias("cr")), value(Boolean.TRUE)))
                .order(property(ExtractCreationRule.studentExtractType().title().fromAlias("cr")))
                .createStatement(getSession()).list();

        model.getDataSource().setCountRow(ruleList.size());
        UniBaseUtils.createPage(model.getDataSource(), ruleList);
    }

    @Override
    public void updateForNewExtracts()
    {
        List<StudentExtractType> alreadyAddedTypes = new DQLSelectBuilder().fromEntity(ExtractCreationRule.class, "cr")
                .column(property(ExtractCreationRule.studentExtractType().fromAlias("cr")))
                .createStatement(getSession()).list();

        List<StudentExtractType> extractTypesList = new DQLSelectBuilder().fromEntity(StudentExtractType.class, "et").column("et")
                .where(eq(property(StudentExtractType.parent().code().fromAlias("et")), value(StudentExtractTypeCodes.MODULAR_ORDER)))
                .createStatement(getSession()).list();

        for (StudentExtractType type : extractTypesList)
        {
            if (!alreadyAddedTypes.contains(type))
            {
                ExtractCreationRule rule = new ExtractCreationRule();
                rule.setStudentExtractType(type);
                rule.setIndividualOrder(false);
                save(rule);
            }
        }
    }

    @Override
    public void updateExtractIndividual(long id)
    {
        ExtractCreationRule rule = get(ExtractCreationRule.class, id);

        if (rule != null)
        {
            rule.setIndividualOrder(!rule.isIndividualOrder());
            update(rule);
        }
    }
}