/* $Id: extractPrint.vm 6933 2009-03-07 05:41:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e35;

import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 22.12.2009
 */
public class ChangeMiddleNameStuExtractPrint implements IPrintFormCreator<ChangeMiddleNameStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ChangeMiddleNameStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        List<String> tagsToDel = new ArrayList<>();
        if (null != extract.getReasonStr())
        {
            modifier.put("textCause", extract.getReasonStr());
        }
        else
        {
            tagsToDel.add("textCause");
        }

        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, tagsToDel, true, false);

        GrammaCase rusCase = GrammaCase.INSTRUMENTAL;
        boolean isMaleSex = extract.getSex().isMale();
        StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(extract.getLastNameNew(), rusCase, isMaleSex));
        str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(extract.getFirstNameNew(), rusCase, isMaleSex));
        str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(extract.getMiddleNameNew(), rusCase, isMaleSex));
        modifier.put("fullFioNew", str.toString());
        CommonExtractPrint.addEntryDateToModifier(modifier, extract.getChangeMiddleNameDate()).modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}