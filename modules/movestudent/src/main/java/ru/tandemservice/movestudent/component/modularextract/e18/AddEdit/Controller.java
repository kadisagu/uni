/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e18.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.movestudent.entity.TransferStuExtractExt;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 05.05.2009
 */
public class Controller extends CommonModularStudentExtractAddEditController<TransferStuExtractExt, IDAO, Model>
{
    public void onChangeGroup(IBusinessComponent component)
    {
        MoveStudentDaoFacade.getCommonExtractUtil().handleGroupChange(getModel(component).getEduModel());
    }
    
    public void onClickAddDebt(IBusinessComponent component)
    {
        getModel(component).getDebtsList().add(new StuExtractToDebtRelation());
    }
    
    public void onClickDeleteDebt(IBusinessComponent component)
    {
        Model model = getModel(component);
        if(model.getDebtsList().size() == 1) return;
        
        Integer debtNumber = (Integer)component.getListenerParameter();
        StuExtractToDebtRelation rel = getModel(component).getDebtsList().get(debtNumber);
        getModel(component).getDebtsList().remove(rel);
        if(!getModel(component).getDebtsToDel().contains(rel) && null != rel.getId())
            getModel(component).getDebtsToDel().add(rel);
    }
}