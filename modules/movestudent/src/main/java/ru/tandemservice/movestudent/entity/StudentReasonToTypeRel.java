package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.movestudent.entity.gen.StudentReasonToTypeRelGen;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;

public class StudentReasonToTypeRel extends StudentReasonToTypeRelGen implements IEntityRelation<StudentOrderReasons, StudentExtractType>
{
}