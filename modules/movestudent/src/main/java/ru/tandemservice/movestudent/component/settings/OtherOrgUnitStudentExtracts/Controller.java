/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.settings.OtherOrgUnitStudentExtracts;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author dseleznev
 * Created on: 18.03.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        boolean admin = UserContext.getInstance().getPrincipalContext().isAdmin();

        DynamicListDataSource<StudentExtractType> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Название", admin ? StudentExtractType.titleWithCode().s() : StudentExtractType.title().s()).setClickable(false));
        dataSource.addColumn(new ToggleColumn("Формировать для студентов другого подразделения", Model.P_CAN_BE_CREATED_BY_THE_OTHER_ORG_UNIT).setListener("onClickCreatableByTheOtherOrgUnit"));
        model.setDataSource(dataSource);
    }

    public void onClickCreatableByTheOtherOrgUnit(IBusinessComponent component)
    {
        getDao().updateCanBeCreatedByTheOtherOrgUnit((Long)component.getListenerParameter());
    }
}