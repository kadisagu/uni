/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e4.ParagraphAddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.ExcludeStuListExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author ListExtractComponentGenerator
 * @since 08.06.2009
 */
public class DAO extends AbstractListParagraphAddEditDAO<ExcludeStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));
        model.setStudentStatusesList(getCatalogItemList(StudentStatus.class));

        // заполняем поля по умолчанию
        if (model.getParagraphId() != null)
        {
            ExcludeStuListExtract extract = model.getFirstExtract();
            model.setCourse(extract.getCourse());
            model.setGroup(extract.getGroup());
            model.setStudentStatusNew(extract.getStatusNew());
            model.setExcludeDate(extract.getExcludeDate());
        }
        else
        {
            model.setStudentStatusNew(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_GRADUATED_WITH_DIPLOMA));
        }
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
    }

    @Override
    protected ExcludeStuListExtract createNewInstance(Model model)
    {
        return new ExcludeStuListExtract();
    }

    @Override
    protected void fillExtract(ExcludeStuListExtract extract, Student student, Model model)
    {
        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setStatusNew(model.getStudentStatusNew());
        extract.setStatusOld(student.getStatus());
        extract.setExcludeDate(model.getExcludeDate());
    }
}