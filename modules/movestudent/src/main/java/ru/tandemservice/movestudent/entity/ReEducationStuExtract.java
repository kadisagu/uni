package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.ReEducationStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О повторном обучении
 */
public class ReEducationStuExtract extends ReEducationStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}