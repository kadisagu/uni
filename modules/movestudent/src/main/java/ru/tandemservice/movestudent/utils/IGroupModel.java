package ru.tandemservice.movestudent.utils;

import ru.tandemservice.uni.entity.catalog.Course;

/**
 * @author Alexey Lopatin
 * @since 21.11.2013
 */
public interface IGroupModel
{
    Course getCourse();
}
