/* $Id$ */
package ru.tandemservice.movestudent.component.commons.gradation;

import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Date;

/**
 * Выписка о переводе (в другую группу, с курса на курс и т.п.))
 *
 * @author Nikolay Fedorovskih
 * @since 15.12.2013
 */
public interface ITransferExtract
{
    /**
     * Дата перевода. Либо дата приказа, если даты перевода как таковой нет.
     */
    Date getTransferOrOrderDate();

    /**
     * Старая группа.
     */
    Group getGroupOld();

    /**
     * Новая группа.
     */
    Group getGroupNew();
}