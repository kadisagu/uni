package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IEnrollmentExtract;
import ru.tandemservice.movestudent.entity.gen.EduEnrolmentStuExtractExtGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О зачислении
 */
public class EduEnrolmentStuExtractExt extends EduEnrolmentStuExtractExtGen implements IEnrollmentExtract
{
    @Override
    public Date getEntranceDate()
    {
        return getEntryDate();
    }
}