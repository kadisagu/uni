/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e80;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.ChangeCategoryStuExtract;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.HashMap;
import java.util.Map;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 31.08.2012
 */
public class ChangeCategoryStuExtractPrint implements IPrintFormCreator<ChangeCategoryStuExtract>
{
    private static final Map<String, String[]> CATEGORY_MALE_STRING_CASES_ARRAY = new HashMap<>();
    private static final Map<String, String[]> CATEGORY_FEMALE_STRING_CASES_ARRAY = new HashMap<>();

    static
    {
        CATEGORY_MALE_STRING_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_STUDENT, new String[]{"студент", "студента", "студенту", "студента", "студентом", "студенте"});
        CATEGORY_MALE_STRING_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY, new String[]{"студент", "студента", "студенту", "студента", "студентом", "студенте"});
        CATEGORY_MALE_STRING_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_LISTENER, new String[]{"слушатель", "слушателя", "слушателю", "слушателя", "слушателем", "слушателе"});
        CATEGORY_MALE_STRING_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_DPP, new String[]{"слушатель", "слушателя", "слушателю", "слушателя", "слушателем", "слушателе"});
        CATEGORY_FEMALE_STRING_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_STUDENT, new String[]{"студентка", "студентки", "студентке", "студентку", "студенткой", "студентке"});
        CATEGORY_FEMALE_STRING_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY, new String[]{"студентка", "студентки", "студентке", "студентку", "студенткой", "студентке"});
        CATEGORY_FEMALE_STRING_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_LISTENER, new String[]{"слушатель", "слушателя", "слушателю", "слушателя", "слушателем", "слушателе"});
        CATEGORY_FEMALE_STRING_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_DPP, new String[]{"слушатель", "слушателя", "слушателю", "слушателя", "слушателем", "слушателе"});
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, ChangeCategoryStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        boolean maleSex = extract.getEntity().getPerson().getIdentityCard().getSex().isMale();
        Map<String, String[]> categoryMapForSex = maleSex ? CATEGORY_MALE_STRING_CASES_ARRAY : CATEGORY_FEMALE_STRING_CASES_ARRAY;
        String[] oldCategoryCases = categoryMapForSex.get(extract.getStudentCategoryOld().getCode());
        String[] newCategoryCases = categoryMapForSex.get(extract.getStudentCategoryNew().getCode());

        for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
        {
            modifier.put("categoryStudentOld" + UniRtfUtil.CASE_POSTFIX.get(i), oldCategoryCases[i]);
            modifier.put("categoryStudentNew" + UniRtfUtil.CASE_POSTFIX.get(i), newCategoryCases[i]);
        }

        modifier.put("categoryChangeDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getCategoryChangeDate()));

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}