/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IExtractsMassPrintDao;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.MoveStudentInjectModifier;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.ExtractsMassPrintDao;

/**
 * @author Nikolay Fedorovskih
 * @since 11.07.2013
 */
@Configuration
public class MoveStudentPrintModifierManager extends BusinessObjectManager
{
    public static MoveStudentPrintModifierManager instance()
    {
        return instance(MoveStudentPrintModifierManager.class);
    }

    @Bean
    public IMoveStudentInjectModifier modifier()
    {
        return new MoveStudentInjectModifier();
    }

    @Bean
    public IExtractsMassPrintDao printDao()
    {
        return new ExtractsMassPrintDao();
    }
}