/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.component.student.MoveStudentPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.movestudent.component.commons.ExtractListModel;
import ru.tandemservice.uni.component.student.StudentPub.IStudentModel;

/**
 * @author vip_delete
 * @since 01.11.2008
 */
@State({
        @Bind(key = "moveStudentPubSelectedTab", binding = "moveStudentPubSelectedTab")
})
public class Model
{
    private IStudentModel _studentModel;
    private ExtractListModel _individualOrdersModel = new ExtractListModel();
    private ExtractListModel _projectsModel = new ExtractListModel();
    private ExtractListModel _extractsModel = new ExtractListModel();
    private ExtractListModel _listextractsModel = new ExtractListModel();
    private ExtractListModel _otherOrdersModel = new ExtractListModel();
    private ExtractListModel _ordersModel = new ExtractListModel();
    private IMultiSelectModel _orderReasonListModel;
    private String _moveStudentPubSelectedTab;

    // Getters & Setters

    public IStudentModel getStudentModel()
    {
        return _studentModel;
    }

    public void setStudentModel(IStudentModel studentModel)
    {
        _studentModel = studentModel;
    }

    public ExtractListModel getIndividualOrdersModel()
    {
        return _individualOrdersModel;
    }

    public void setIndividualOrdersModel(ExtractListModel individualOrdersModel)
    {
        _individualOrdersModel = individualOrdersModel;
    }

    public ExtractListModel getProjectsModel()
    {
        return _projectsModel;
    }

    public void setProjectsModel(ExtractListModel projectsModel)
    {
        _projectsModel = projectsModel;
    }

    public ExtractListModel getExtractsModel()
    {
        return _extractsModel;
    }

    public void setExtractsModel(ExtractListModel extractsModel)
    {
        _extractsModel = extractsModel;
    }

    public ExtractListModel getListextractsModel()
    {
        return _listextractsModel;
    }

    public void setListextractsModel(ExtractListModel listextractsModel)
    {
        _listextractsModel = listextractsModel;
    }

    public ExtractListModel getOtherOrdersModel()
    {
        return _otherOrdersModel;
    }

    public void setOtherOrdersModel(ExtractListModel otherOrdersModel)
    {
        _otherOrdersModel = otherOrdersModel;
    }

    public ExtractListModel getOrdersModel()
    {
        return _ordersModel;
    }

    public void setOrdersModel(ExtractListModel ordersModel)
    {
        _ordersModel = ordersModel;
    }

    public String getMoveStudentPubSelectedTab()
    {
        return _moveStudentPubSelectedTab;
    }

    public void setMoveStudentPubSelectedTab(String moveStudentPubSelectedTab)
    {
        _moveStudentPubSelectedTab = moveStudentPubSelectedTab;
    }

    public IMultiSelectModel getOrderReasonListModel()
    {
        return _orderReasonListModel;
    }

    public void setOrderReasonListModel(IMultiSelectModel orderReasonListModel)
    {
        _orderReasonListModel = orderReasonListModel;
    }
}