/*$Id$*/
package ru.tandemservice.movestudent.component.listextract.e45.utils;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractUtil;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2014
 */
public class AdmitToPassStateExamStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<AdmitToPassSomething> extracts = new ArrayList<>();
        extracts.addAll(MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true));

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, (ListStudentExtract) extracts.get(0));
        List<AdmitToPassStateExamParagraphWrapper> preparedParagraphsStructure = prepareParagraphsStructure(extracts);
        Collections.sort(preparedParagraphsStructure);

        if (!extracts.isEmpty())
        {
            injectModifier.put("postParNum1", String.valueOf(preparedParagraphsStructure.size() + 1));
            injectModifier.put("postParNum2", String.valueOf(preparedParagraphsStructure.size() + 2));
        }

        if (null != order.getResponsibleEmpl())
        {
            OrgUnit orgUnit = order.getResponsibleEmpl().getOrgUnit();
            Post post = order.getResponsibleEmpl().getPostRelation().getPostBoundedWithQGandQL().getPost();
            String respFioA = CommonExtractUtil.getModifiedFio(order.getResponsibleEmpl().getPerson(), GrammaCase.ACCUSATIVE, true);

            injectModifier.put("controllerPost_A", null != post.getAccusativeCaseTitle() ? post.getAccusativeCaseTitle() : post.getTitle());
            injectModifier.put("controllerOrgUnit_G", null != orgUnit.getGenitiveCaseTitle() ? orgUnit.getGenitiveCaseTitle() : orgUnit.getTitle());
            injectModifier.put("controllerFio_A", respFioA);
        } else
        {
            injectModifier.put("controllerPost_A", "");
            injectModifier.put("controllerOrgUnit_G", "");
            injectModifier.put("controllerFio_A", "____________________________________________________________________________________________________________________________________________________________________________________________.");
        }

        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, preparedParagraphsStructure);

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }

    protected List<AdmitToPassStateExamParagraphWrapper> prepareParagraphsStructure(List<AdmitToPassSomething> extracts)
    {
        int ind;
        List<AdmitToPassStateExamParagraphWrapper> paragraphWrapperList = new ArrayList<>();

        if (extracts.isEmpty()) return paragraphWrapperList;

        for (AdmitToPassSomething extract : extracts)
        {
            EducationOrgUnit educationOrgUnitOld = extract.getEntity().getEducationOrgUnit();

            /*EducationLevels eduLevelsNew = educationOrgUnitNew.getEducationLevelHighSchool().getEducationLevel();
            Qualifications qualification = eduLevelsNew.getQualification();

            if (null == qualification)
            {
                EducationLevels tmpLevel = eduLevelsNew.getParentLevel();
                while (null != tmpLevel && null == qualification)
                {
                    qualification = tmpLevel.getQualification();
                    tmpLevel = tmpLevel.getParentLevel();
                }
            }    */

            AdmitToPassStateExamParagraphWrapper paragraphWrapper = new AdmitToPassStateExamParagraphWrapper(
                    educationOrgUnitOld.getFormativeOrgUnit(),
                    educationOrgUnitOld.getEducationLevelHighSchool(),
                    educationOrgUnitOld.getDevelopForm(),
                    extract.getEntity().getCourse(),
                    extract.getEntity().getCompensationType(),
                    (StudentListParagraph) extract.getParagraph());

            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1) paragraphWrapperList.add(paragraphWrapper);
            else paragraphWrapper = paragraphWrapperList.get(ind);

            paragraphWrapper.getStudentsList().add(extract);
        }

        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<AdmitToPassStateExamParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            int parNumber = 0;
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();


            for (AdmitToPassStateExamParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, ((ListStudentExtract)paragraphWrapper.getFirstExtract()).getType().getCode()), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate).getClone();

                // Вносим необходимые метки
                final AdmitToPassSomething firstExtract = paragraphWrapper.getFirstExtract();
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(firstExtract.getParagraph(), (ListStudentExtract) firstExtract);

                parNumber++;
                paragraphInjectModifier.put("parNumber", String.valueOf(parNumber));
                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");
                paragraphInjectModifier.put("paragraphNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");

                String code = paragraphWrapper.getDevelopForm().getCode();
                switch (code)
                {
                    case DevelopFormCodes.FULL_TIME_FORM:
                        paragraphInjectModifier.put("developForm_GF", "очной");
                        break;
                    case DevelopFormCodes.CORESP_FORM:
                        paragraphInjectModifier.put("developForm_GF", "заочной");
                        break;
                    case DevelopFormCodes.PART_TIME_FORM:
                        paragraphInjectModifier.put("developForm_GF", "очно-заочной");
                        break;
                    case DevelopFormCodes.EXTERNAL_FORM:
                        paragraphInjectModifier.put("developForm_GF", "экстернату");
                        break;
                    case DevelopFormCodes.APPLICANT_FORM:
                        paragraphInjectModifier.put("developForm_GF", "самостоятельному обучению и итоговой аттестации");
                        break;
                }

                Student firstStudent = firstExtract.getEntity();

                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstStudent.getEducationOrgUnit().getFormativeOrgUnit(), "formativeOrgUnit", "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstStudent.getEducationOrgUnit().getFormativeOrgUnit(), "", "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstStudent.getEducationOrgUnit(), "formativeOrgUnitStr", "");

                paragraphInjectModifier.put("course", paragraphWrapper.getCourse().getTitle());
                UniRtfUtil.initEducationType(paragraphInjectModifier, paragraphWrapper.getEducationLevels(), "");
                addEducationLevelData(paragraphWrapper.getEducationLevels().getEducationLevel(), paragraphInjectModifier);
                paragraphInjectModifier.put("groupsWord", paragraphWrapper.getGroups().size() > 1 ? "групп" : "группы");
                paragraphInjectModifier.put("groups", paragraphWrapper.getGroupsStr());

                paragraphInjectModifier.put("compensationTypeMirea", CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(firstStudent.getCompensationType().getCode())
                        ? "на местах, финансируемых за счет бюджетных ассигнований федерального бюджета"
                        : "на местах с оплатой стоимости обучения физическими лицами");

                if (null != paragraphWrapper.getFirstExtract().getBeginDate())
                {
                    paragraphInjectModifier.put("examBeginDay", String.valueOf(RussianDateFormatUtils.getDayString(paragraphWrapper.getFirstExtract().getBeginDate(), true)));
                    paragraphInjectModifier.put("examBeginMonthStr_G", String.valueOf(RussianDateFormatUtils.getMonthName(paragraphWrapper.getFirstExtract().getBeginDate(), false)));
                    paragraphInjectModifier.put("examBeginYear", String.valueOf(RussianDateFormatUtils.getYearString(paragraphWrapper.getFirstExtract().getBeginDate(), false)));
                } else
                {
                    paragraphInjectModifier.put("examBeginDay", "___");
                    paragraphInjectModifier.put("examBeginMonthStr_G", "___________");
                    paragraphInjectModifier.put("examBeginYear", "_____");
                }

                if (null != paragraphWrapper.getFirstExtract().getEndDate())
                {
                    paragraphInjectModifier.put("examEndDay", String.valueOf(RussianDateFormatUtils.getDayString(paragraphWrapper.getFirstExtract().getEndDate(), true)));
                    paragraphInjectModifier.put("examEndMonthStr_G", String.valueOf(RussianDateFormatUtils.getMonthName(paragraphWrapper.getFirstExtract().getEndDate(), false)));
                    paragraphInjectModifier.put("examEndYear", String.valueOf(RussianDateFormatUtils.getYearString(paragraphWrapper.getFirstExtract().getEndDate(), false)));
                } else
                {
                    paragraphInjectModifier.put("examEndDay", "___");
                    paragraphInjectModifier.put("examEndMonthStr_G", "___________");
                    paragraphInjectModifier.put("examEndYear", "_____");
                }


                int i = 1;
                RtfTableModifier tableModifier = new RtfTableModifier();


                List<AdmitToPassSomething> studList = paragraphWrapper.<AdmitToPassSomething>getStudentsList();
                Collections.sort(studList, new Comparator<AdmitToPassSomething>()
                {
                    @Override
                    public int compare(AdmitToPassSomething o1, AdmitToPassSomething o2)
                    {
                        int result = 0;
                        if (null != o1.getEntity().getGroup() && null == o2.getEntity().getGroup()) return 1;
                        if (null == o1.getEntity().getGroup() && null != o2.getEntity().getGroup()) return -1;
                        if (null != o1.getEntity().getGroup() && null != o2.getEntity().getGroup())
                            result = o1.getEntity().getGroup().getTitle().compareTo(o2.getEntity().getGroup().getTitle());
                        if (0 != result) return result;

                        return o1.getEntity().getPerson().getIdentityCard().getFullFio().compareTo(o2.getEntity().getPerson().getIdentityCard().getFullFio());
                    }
                });


                List<String[]> studentList = new ArrayList<>(paragraphWrapper.getStudentsList().size());
                for (AdmitToPassSomething extract : studList)
                {
                    studentList.add(new String[]{
                            String.valueOf(i++) + ".",
                            extract.getEntity().getPerson().getFullFio(),
                            extract.getEntity().getPersonalNumber(),
                            null != extract.getEntity().getGroup() ? extract.getEntity().getGroup().getTitle() : ""
                    });
                }

                if (studentList.isEmpty()) tableModifier.put("T", new String[][]{});
                else tableModifier.put("T", studentList.toArray(new String[][]{}));

                customModifier.listParagraphModifier(paragraphInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), (ListStudentExtract) firstExtract);
                paragraphInjectModifier.modify(paragraph);
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), (ListStudentExtract) firstExtract);
                tableModifier.modify(paragraph);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);

            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    private void addEducationLevelData(EducationLevels educationLevel, RtfInjectModifier modifier)
    {
        String levelTypeStr = "";

        if (educationLevel.getLevelType().isMiddle())
        {
            if (QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O.equals(educationLevel.getQualification().getCode()))
            {
                levelTypeStr = " специальности";
            } else if (QualificationsCodes.BAZOVYY_UROVEN_S_P_O.equals(educationLevel.getQualification().getCode()))
            {
                levelTypeStr = " специальности";
            }
        } else if (educationLevel.getLevelType().isSpecialty() || educationLevel.getLevelType().isSpecialization())
        {
            levelTypeStr = " специальности";
        } else if (educationLevel.getLevelType().isMaster())
        {
            levelTypeStr = "направлению подготовки магистров";
        } else if (educationLevel.getLevelType().isBachelor())
        {
            levelTypeStr = "направлению подготовки бакалавров";
        }

        modifier.put("levelTypeStr_D", levelTypeStr);
    }
}
