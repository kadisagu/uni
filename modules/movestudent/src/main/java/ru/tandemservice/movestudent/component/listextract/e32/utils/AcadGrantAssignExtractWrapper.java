/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e32.utils;

import org.tandemframework.shared.person.base.entity.Person;

/**
 * @author Alexander Zhebko
 * @since 02.10.2012
 */
public class AcadGrantAssignExtractWrapper implements Comparable<AcadGrantAssignExtractWrapper>
{
    private final Person _person;
    private final String _grantSize;

    public AcadGrantAssignExtractWrapper(Person person, String grantSize)
    {
        _person = person;
        _grantSize = grantSize;
    }

    public Person getPerson()
    {
        return _person;
    }

    public String getGrantSize()
    {
        return _grantSize;
    }

    @Override
    public int compareTo(AcadGrantAssignExtractWrapper o)
    {
        return Person.FULL_FIO_AND_ID_COMPARATOR.compare(_person, o.getPerson());
    }
}