/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.TemplateEdit;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.component.customorder.ICustomPrintFormContainer;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentModularOrder;

/**
 * @author Nikolay Fedorovskih
 * @since 24.09.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onClickDownloadTemplate(IBusinessComponent component)
    {
        byte[] template = getModel(component).getUserTemplateContainer().getBasicTemplate();
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("template.rtf").document(template), false);
    }

    public void onClickDownloadCurrentDocument(IBusinessComponent component)
    {
        ICustomPrintFormContainer container = getModel(component).getUserTemplateContainer();
        byte[] content = container.getFile() != null ? container.getFile().getContent() : null;
        if (content == null)
            throw new ApplicationException("Печатный документ пока не задан.");

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("document.rtf").document(content), false);
    }

    public void onClickDownloadModifiableOrderDocument(IBusinessComponent component)
    {
        Long orderId = getModel(component).getModifiableOrderId();
        if (orderId != null)
        {
            AbstractStudentOrder order = getDao().getNotNull(orderId);
            String printComponentName;
            if (order instanceof StudentModularOrder)
                printComponentName = IMoveStudentComponents.STUDENT_MODULAR_ORDER_PRINT;
            else if (order instanceof StudentListOrder)
                printComponentName = IMoveStudentComponents.STUDENT_LIST_ORDER_PRINT;
            else
                throw new IllegalStateException("Unsupported order type");

            ContextLocal.createDesktop(new ComponentActivator(printComponentName, new ParametersMap().add("orderId", orderId)), null);
        }
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
        deactivate(component);
    }
}