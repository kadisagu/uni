/* $Id$ */
package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IAssignPaymentExtract;
import ru.tandemservice.movestudent.entity.gen.SocGrantAssignStuListExtractGen;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Проект приказа «О назначении социальной стипендии»
 */
public class SocGrantAssignStuListExtract extends SocGrantAssignStuListExtractGen implements IAssignPaymentExtract
{
    @Override
    public BigDecimal getPaymentAmount()
    {
        return new BigDecimal(getGrantSize());
    }

    @Override
    public Date getPaymentBeginDate()
    {
        return getBeginDate();
    }

    @Override
    public Date getPaymentEndDate()
    {
        return getEndDate();
    }
}