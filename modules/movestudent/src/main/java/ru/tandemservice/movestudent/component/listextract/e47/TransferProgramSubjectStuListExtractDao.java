/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e47;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 17.01.2017
 */
public class TransferProgramSubjectStuListExtractDao extends UniBaseDao implements IExtractComponentDao<TransferProgramSubjectStuListExtract>
{
    @Override
    public void doCommit(TransferProgramSubjectStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        if (extract.isChangeGroupEducationOU() && !extract.getGroupEducationOUNew().equals(extract.getGroup().getEducationOrgUnit()))
        {
            extract.setGroupEducationOUOld(extract.getGroup().getEducationOrgUnit());
            extract.getGroup().setEducationOrgUnit(extract.getGroupEducationOUNew());

            getSession().update(extract.getGroup());
        }

        extract.setEducationOrgUnitOld(extract.getEntity().getEducationOrgUnit());
        extract.getEntity().setEducationOrgUnit(extract.getEducationOrgUnitNew());

    }

    @Override
    public void doRollback(TransferProgramSubjectStuListExtract extract, Map parameters)
    {
        if (extract.isChangeGroupEducationOU() && !extract.getGroupEducationOUOld().equals(extract.getGroup().getEducationOrgUnit()))
        {
            extract.getGroup().setEducationOrgUnit(extract.getGroupEducationOUOld());
            getSession().update(extract.getGroup());
        }

        extract.getEntity().setEducationOrgUnit(extract.getEducationOrgUnitOld());

    }
}