package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.movestudent.entity.gen.ExcludeGPDefenceFailStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. Об отчислении (ВКР)
 */
public class ExcludeGPDefenceFailStuExtract extends ExcludeGPDefenceFailStuExtractGen implements IExcludeExtract
{
    @Override
    public Date getExcludeDate()
    {
        return getBeginDate();
    }
}