/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e18.ParagraphAddEdit;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.SplitStudentsStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.IExtEducationLevelModel;

/**
 * @author ListExtractComponentGenerator
 * @since 30.07.2009
 */
public class Model extends AbstractListParagraphAddEditModel<SplitStudentsStuListExtract> implements IEducationLevelModel, IExtEducationLevelModel, IGroupModel
{
    private Course _course;
    private Group _group;
    private ISelectModel _groupListModel;

    private ISelectModel _specializationsListModel;
    private EducationLevelsHighSchool _educationLevelsHighSchool;


    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        this._course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        this._group = group;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        this._groupListModel = groupListModel;
    }

    public ISelectModel getSpecializationsListModel()
    {
        return _specializationsListModel;
    }

    public void setSpecializationsListModel(ISelectModel specializationsListModel)
    {
        this._specializationsListModel = specializationsListModel;
    }

    public boolean isAnyOrderParametersCantBeChanged()
    {
        return !isParagraphOnlyOneInTheOrder();
    }

    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return getGroup() != null ? getGroup().getEducationOrgUnit().getFormativeOrgUnit() : null;
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return getGroup() != null ? getGroup().getEducationOrgUnit().getTerritorialOrgUnit() : null;
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return getGroup() != null ? getGroup().getEducationOrgUnit().getDevelopForm() : null;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return getGroup() != null ? getGroup().getEducationOrgUnit().getDevelopCondition() : null;
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return getGroup() != null ? getGroup().getEducationOrgUnit().getDevelopTech() : null;
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return getGroup() != null ? getGroup().getEducationOrgUnit().getDevelopPeriod() : null;
    }

    @Override
    public EducationLevels getParentEduLevel()
    {
        return getGroup() != null ? EducationOrgUnitUtil.getParentLevel(getGroup().getEducationOrgUnit().getEducationLevelHighSchool()) : null;
    }
}