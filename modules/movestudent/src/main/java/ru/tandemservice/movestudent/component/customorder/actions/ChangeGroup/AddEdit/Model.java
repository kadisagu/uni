/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeGroup.AddEdit;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditModel;
import ru.tandemservice.movestudent.entity.custom.ChangeGroupAction;
import ru.tandemservice.uni.entity.catalog.Course;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public class Model extends BaseCustomActionAddEditModel<ChangeGroupAction>
{
    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private List<Course> _courseList;
    private ISelectModel _groupModel;

    private OrgUnit _formativeOrgUnit;
    private OrgUnit _territorialOrgUnit;
    private Course _course;

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public ISelectModel getGroupModel()
    {
        return _groupModel;
    }

    public void setGroupModel(ISelectModel groupModel)
    {
        _groupModel = groupModel;
    }
}