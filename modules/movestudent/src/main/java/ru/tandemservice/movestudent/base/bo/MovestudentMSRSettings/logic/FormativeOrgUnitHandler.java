/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;


/**
 * @author Ekaterina Zvereva
 * @since 22.12.2014
 */
public class FormativeOrgUnitHandler extends DefaultSearchDataSourceHandler
{
    public FormativeOrgUnitHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(OrgUnit.class, "ou")
                .column(DQLExpressions.property("ou"))
                .where(DQLExpressions.in(DQLExpressions.property(OrgUnit.id().fromAlias("ou")),
                                         new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "r")
                                                 .column(DQLExpressions.property(OrgUnitToKindRelation.orgUnit().id().fromAlias("r")))
                                                 .where(DQLExpressions.eq(DQLExpressions.property(OrgUnitToKindRelation.orgUnit().archival().fromAlias("r")), DQLExpressions.value(Boolean.FALSE)))
                                                 .where(DQLExpressions.eq(DQLExpressions.property(OrgUnitToKindRelation.orgUnitKind().code().fromAlias("r")), DQLExpressions.value(UniDefines.CATALOG_ORGUNIT_KIND_FORMING)))
                                                 .buildQuery())
                );


        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(OrgUnit.class, "ou");

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order(orderRegistry).pageable(false).build();
    }

}
