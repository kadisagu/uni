/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentIdentityCard;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.NumberConvertingUtil;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 07.05.2009
 */
public class CommonListOrderPrint
{
    public static final String PARAGRAPHS = "PARAGRAPHS";
    public static final String STUDENT_LIST = "STUDENT_LIST";

    public static final IStudentListParagraphPrintFormatter STUDENT_LIST_FORMATTER = new IStudentListParagraphPrintFormatter()
    {
        @Override
        public String formatSingleStudent(Student student, int extractNumber)
        {
            return "\\par " + extractNumber + ".  " + student.getPerson().getFullFio() + " (" + student.getCompensationType().getShortTitle() + ")";
        }
    };

    public static void injectFefuGroupPar(RtfInjectModifier modifier, Group group, DevelopForm developForm)
    {
        injectFefuGroupPar(modifier, group, developForm, true, false);
    }

    public static void injectFefuGroupPar(RtfInjectModifier modifier, Group group, DevelopForm developForm, boolean parBefore, boolean parAfter)
    {
        RtfString rtfString = new RtfString();
        if ((group != null && DevelopFormCodes.FULL_TIME_FORM.equals(developForm.getCode())))
        {
            if (parBefore) rtfString.par();
            rtfString.append("Группа ").append(group.getTitle());
            if (parAfter) rtfString.par();
        }
        modifier.put("groupInternalPar", rtfString);
    }

    public static RtfInjectModifier createListOrderInjectModifier(StudentListOrder order)
    {
        return createListOrderInjectModifier(order, null);
    }

    public static RtfInjectModifier createListOrderInjectModifier(StudentListOrder order, ListStudentExtract firstExtract)
    {
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("orderId", String.valueOf(order.getId()));
        injectModifier.put("orderIdWithLabel", "[orderId]=" + order.getId());

        firstExtract = firstExtract == null ? (ListStudentExtract) MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(order.getId()) : firstExtract;
        DevelopForm developForm = firstExtract.getEntity().getEducationOrgUnit().getDevelopForm();
        if (DevelopFormCodes.FULL_TIME_FORM.equals(developForm.getCode()))
            injectModifier.put("DEVELOPFORM", "ОЧНОЕ ОТДЕЛЕНИЕ");
        else if (DevelopFormCodes.CORESP_FORM.equals(developForm.getCode()))
            injectModifier.put("DEVELOPFORM", "ЗАОЧНОЕ ОТДЕЛЕНИЕ");
        else if (DevelopFormCodes.PART_TIME_FORM.equals(developForm.getCode()))
            injectModifier.put("DEVELOPFORM", "ЗАОЧНОЕ ОТДЕЛЕНИЕ");
        else injectModifier.put("DEVELOPFORM", "");

        injectAcademy(injectModifier);

        injectModifier.put("commitDate", order.getCommitDate() == null ? "          " : DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()));
        injectModifier.put("orderNumber", StringUtils.isEmpty(order.getNumber()) ? "          " : order.getNumber());
        injectModifier.put("eduYear", order.getEducationYear().getTitle());
        injectModifier.put("textParagraph", order.getTextParagraph());
        String executor = order.getExecutor();
        injectModifier.put("executor", "");
        injectModifier.put("executorPhone", "");
        if (StringUtils.isNotEmpty(executor))
        {
            int phoneStart = executor.indexOf(" т. ");
            if (phoneStart == -1)
            {
                injectModifier.put("executor", executor);
            }
            else
            {
                injectModifier.put("executor", executor.substring(0, phoneStart));
                injectModifier.put("executorPhone", executor.substring(phoneStart + 1));
            }
        }

        CommonExtractPrint.initOrgUnit(injectModifier, order.getOrgUnit(), "formativeOrgUnit", "");
        CommonExtractPrint.injectReason(injectModifier, order.getReason(), order.getReasonComment());

        String listBasics = order.getBasicListStr();
        injectModifier.put("listBasicsWithoutPoint", listBasics);
        if (listBasics.trim().length() > 0) listBasics += ".";
        injectModifier.put("listBasics", listBasics);

        // Точка расширения для проектных модулей
        MoveStudentPrintModifierManager.instance().modifier().listOrderModifier(injectModifier, order, firstExtract);

        return injectModifier;
    }

    public static RtfTableModifier createListOrderTableModifier(StudentListOrder order)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(order);
        List<String[]> visaData = new ArrayList<>();
        List<String[]> primaryVisaData = new ArrayList<>();
        List<String[]> secondaryVisaData = new ArrayList<>();
        Map<String, List<String[]>> printLabelMap = new HashMap<>();
        // DEV-5087
        // мапа для определения, выводить печатное название группы или уже нет
        Map<String, Boolean> printTitleMap = new HashMap<>();

        for (GroupsMemberVising group : UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(GroupsMemberVising.class))
        {
            printLabelMap.put(group.getPrintLabel(), new ArrayList<>());
            // DEV-5087
            // для меток с печатным названием групп
            printLabelMap.put(group.getPrintLabelTitle(), new ArrayList<>());
            printTitleMap.put(group.getPrintLabelTitle(), false);
        }
        for (Visa visa : visaList)
        {
            IPersistentIdentityCard identityCard = visa.getPossibleVisa().getEntity().getPerson().getIdentityCard();
            String lastName = identityCard.getLastName();
            String firstName = identityCard.getFirstName();
            String middleName = identityCard.getMiddleName();

            StringBuilder str = new StringBuilder();
            if (StringUtils.isNotEmpty(firstName))
                str.append(firstName.substring(0, 1).toUpperCase()).append(".");
            if (StringUtils.isNotEmpty(middleName))
                str.append(middleName.substring(0, 1).toUpperCase()).append(".");
            str.append(" ").append(lastName);

            printLabelMap.get(visa.getGroupMemberVising().getPrintLabel()).add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});
            // DEV-5087
            // для этой группы мы уже вывели печатное название?
            if (!printTitleMap.get(visa.getGroupMemberVising().getPrintLabelTitle()))
            {
                printTitleMap.put(visa.getGroupMemberVising().getPrintLabelTitle(), true);
                printLabelMap.get(visa.getGroupMemberVising().getPrintLabelTitle()).add(new String[]{visa.getGroupMemberVising().getPrintTitle(), ""});
            }
            printLabelMap.get(visa.getGroupMemberVising().getPrintLabelTitle()).add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});

            visaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});

            if (visa.getGroupMemberVising().getCode().equals("2"))
                primaryVisaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});
            else
                secondaryVisaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});
        }
        for (Map.Entry<String, List<String[]>> entry : printLabelMap.entrySet())
            if (!entry.getValue().isEmpty())
                tableModifier.put(entry.getKey(), entry.getValue().toArray(new String[entry.getValue().size()][]));
            else
                tableModifier.put(entry.getKey(), new String[][]{});

        // эти метки выводятся _всегда_ по-старому, без печатных названий групп!
        tableModifier.put("VISAS", visaData.toArray(new String[visaData.size()][]));
        tableModifier.put("PRIMARY_VISAS", primaryVisaData.toArray(new String[primaryVisaData.size()][]));
        tableModifier.put("SECONDARY_VISAS", secondaryVisaData.toArray(new String[secondaryVisaData.size()][]));

        // Точка расширения для проектных модулей
        MoveStudentPrintModifierManager.instance().modifier().listOrderTableModifier(tableModifier, order);

        return tableModifier;
    }

    public static void injectParagraphs(final RtfDocument document, StudentListOrder order, IStudentListParagraphPrintFormatter formatter, ListStudentExtract orderFirstExtract)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            List<IRtfElement> parList = new ArrayList<>();

            boolean firstPar = true;
            for (IAbstractParagraph paragraph : order.getParagraphList())
            {
                RtfDocument paragraphPart = getParagraphPart((StudentListParagraph) paragraph, null != formatter ? formatter : STUDENT_LIST_FORMATTER, firstPar ? orderFirstExtract : null);
                firstPar = false;

                RtfUtil.modifySourceList(document.getHeader(), paragraphPart.getHeader(), paragraphPart.getElementList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный список вставляем вместо ключевого слова
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    public static String createParagraphText(StudentListParagraph paragraph, IStudentListParagraphPrintFormatter formatter)
    {
        // получаем студентов из параграфа
        StringBuilder buffer = new StringBuilder();
        int counter = 1;
        for (Student student : MoveStudentDaoFacade.getMoveStudentDao().getStudentsByParagraph(paragraph.getId()))
        {
            buffer.append(formatter.formatSingleStudent(student, counter++));
        }
        buffer.append("\\par");
        return buffer.toString();
    }

    public static RtfInjectModifier createListOrderParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, ListStudentExtract firstExtract)
    {
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        if (null != paragraph) {
            injectModifier.put("parNumber", Integer.toString(paragraph.getNumber()));
            injectModifier.put("parNumberConditional", paragraph.getOrder().getParagraphCount() > 1 ? String.valueOf(paragraph.getNumber()) + ". " : "");
            injectModifier.put("parNumberRoman", NumberConvertingUtil.convertDecimal2Roman(paragraph.getNumber()));
            injectModifier.put("eduYear", ((StudentListOrder) paragraph.getOrder()).getEducationYear().getTitle());
            injectAcademy(injectModifier);

            int extractCount = paragraph.getExtractCount();
            injectModifier.put("extractCount", String.valueOf(extractCount));
            injectModifier.put("extractCountStr", NumberSpellingUtil.spellNumberMasculineGender(extractCount));
            injectModifier.put("people", CommonBaseStringUtil.numberPostfixCase(extractCount, "человек", "человека", "человек"));

            if (null == firstExtract)
                firstExtract = (ListStudentExtract) ((AbstractStudentParagraph) paragraph).getFirstExtract();
        }

        if (null != firstExtract)
        {
            EducationOrgUnit eo = firstExtract.getEntity().getEducationOrgUnit();

            injectModifier.put("diplomaQualification", eo.getEducationLevelHighSchool().getQualificationTitleNullSafe());
            injectModifier.put("educationOrgUnit", eo.getEducationLevelHighSchool().getPrintTitleWithoutOrgUnit());
            injectModifier.put("developForm", eo.getDevelopForm().getTitle());
            injectModifier.put("course", firstExtract.getCourseStr());
            injectModifier.put("group", firstExtract.getGroupStr());

            UniRtfUtil.initEducationType(injectModifier, eo.getEducationLevelHighSchool(), "");
            CommonExtractPrint.initOrgUnit(injectModifier, eo, "orgUnit", "");
            CommonExtractPrint.initDevelopForm(injectModifier, eo, "");

            IEntityMeta meta = EntityRuntime.getMeta(firstExtract.getId());
            for (String postfix : new String[]{"Old", "New"})
            {
                String property = "educationOrgUnit" + postfix;
                if (meta.getProperty(property) != null)
                {
                    EducationOrgUnit eduOU = (EducationOrgUnit) firstExtract.getProperty("educationOrgUnit" + postfix);

                    // Новые направления
                    EducationLevels educationLevels = eduOU.getEducationLevelHighSchool().getEducationLevel();
                    CommonExtractPrint.initProgramSubjectLabels(injectModifier, educationLevels.getEduProgramSubject(), educationLevels.getEduProgramSpecialization(), postfix);
                }
            }

            // Новые направления
            EducationLevels educationLevels = eo.getEducationLevelHighSchool().getEducationLevel();
            CommonExtractPrint.initProgramSubjectLabels(injectModifier, educationLevels.getEduProgramSubject(), educationLevels.getEduProgramSpecialization(), "");

            //Вид ОП eduProgramKind
            EduProgramKind programKind = educationLevels.getEduProgramSubject()==null? null : educationLevels.getEduProgramSubject().getEduProgramKind();
            CommonExtractPrint.initEduProgramKind("eduProgramKind", injectModifier, programKind);
        }

        return injectModifier;
    }

    @SuppressWarnings({"unchecked"})
    public static <T> IListParagraphPrintFormCreator<T> getListParagraphPrintForm(String name)
    {
        return ApplicationRuntime.getBean(name, IListParagraphPrintFormCreator.class);
    }

    public static RtfDocument getParagraphPart(StudentListParagraph paragraph, IStudentListParagraphPrintFormatter formatter, ListStudentExtract firstExtract)
    {
        // первая выписка из параграфа
        firstExtract =  firstExtract == null ? MoveStudentDaoFacade.getMoveStudentDao().getParagraphFirstExtract(paragraph.getId(), false) : firstExtract;
        if (firstExtract == null)
            throw new ApplicationException("Невозможно распечатать пустой параграф (№" + paragraph.getNumber() + ").");

        byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(firstExtract.getType(), MoveStudentDefines.PARAGRAPH_TEXT_CODE);

        RtfDocument paragraphPart = new RtfReader().read(paragraphTemplate);

        IListParagraphPrintFormCreator<ListStudentExtract> printForm = getListParagraphPrintForm(EntityRuntime.getMeta(firstExtract).getName() + "_extractPrint");

        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        RtfInjectModifier injectModifier = printForm.createParagraphInjectModifier(paragraph, firstExtract);
        customModifier.listParagraphModifier(injectModifier, paragraph, firstExtract);
        injectModifier.modify(paragraphPart);

        RtfTableModifier injectTableModifier = printForm.createParagraphTableModifier(paragraph, firstExtract);
        if (injectTableModifier == null) {
            injectTableModifier = new RtfTableModifier();
        }
        customModifier.listParagraphTableModifier(injectTableModifier, paragraph, firstExtract);
        injectTableModifier.modify(paragraphPart);


        RtfSearchResult searchResult = UniRtfUtil.findRtfMark(paragraphPart, STUDENT_LIST);
        if (searchResult.isFound())
        {
            IListParagraphTextFormatter paragraphTextFormatter = (printForm instanceof IListParagraphTextFormatter) ? (IListParagraphTextFormatter) printForm : null;
            String paragraphText = null != paragraphTextFormatter ? paragraphTextFormatter.createParagraphText(paragraph) :
                    createParagraphText(paragraph, printForm instanceof IStudentListParagraphPrintFormatter ? (IStudentListParagraphPrintFormatter) printForm : formatter);
            IRtfText text = RtfBean.getElementFactory().createRtfText(paragraphText);
            text.setRaw(true);

            searchResult.getElementList().set(searchResult.getIndex(), text);
        }

        return paragraphPart;
    }

    public static void injectFefuDevelopConditionAndTech(RtfInjectModifier modifier, EducationOrgUnit eduOrgUnit, String educationBaseText, String key)
    {
        injectFefuDevelopConditionAndTech(modifier, eduOrgUnit.getDevelopCondition(), eduOrgUnit.getDevelopTech(), eduOrgUnit.getDevelopPeriod(), educationBaseText, key);
    }

    public static void injectFefuDevelopConditionAndTech(RtfInjectModifier modifier, DevelopCondition developCondition, DevelopTech developTech, DevelopPeriod developPeriod, String educationBaseText, String key)
    {
        StringBuilder fefuShortFastExtendedOptionalText = new StringBuilder();

        // developCondition
        if (UniDefines.DEVELOP_CONDITION_SHORT.equals(developCondition.getCode()))
        {
            fefuShortFastExtendedOptionalText.append(" по сокращенной (").append(developPeriod.getTitle()).append(") основной образовательной программе");
        }
        else if (UniDefines.DEVELOP_CONDITION_FAST.equals(developCondition.getCode()))
        {
            fefuShortFastExtendedOptionalText.append(" в ускоренные (").append(developPeriod.getTitle()).append(") сроки");
        }
        else if (UniDefines.DEVELOP_CONDITION_SHORT_FAST.equals(developCondition.getCode()))
        {
            fefuShortFastExtendedOptionalText.append(" по сокращенной ускоренной (").append(developPeriod.getTitle()).append(") основной образовательной программе");
        }

        if (fefuShortFastExtendedOptionalText.length() > 0)
        {
            fefuShortFastExtendedOptionalText.append(educationBaseText);
        }

        // developTech NOT USED
        if (UniDefines.DEVELOP_TECH_REMOTE.equals(developTech.getCode()))
        {
            fefuShortFastExtendedOptionalText.append(CommonExtractPrint.WITH_REMOTE_EDU_TECH);
        }

        modifier.put(key, fefuShortFastExtendedOptionalText.toString());
    }

    public static String getEducationBaseText(Group group)
    {
        String result = "";
        if (null != group)
        {
            String groupTitle = group.getTitle();

            if (groupTitle.startsWith("со") || groupTitle.contains("-со"))
                result = " на базе среднего профессионального образования";
            else if (groupTitle.startsWith("спец") || groupTitle.contains("-спец"))
                result = " на базе высшего профессионального образования";
        }

        return result;
    }

    private static void injectAcademy(RtfInjectModifier injectModifier)
    {
        TopOrgUnit academy = TopOrgUnit.getInstance();
        injectModifier.put("academy", null != academy.getNominativeCaseTitle() ? academy.getNominativeCaseTitle() : academy.getFullTitle());
        injectModifier.put("academy_N", null != academy.getNominativeCaseTitle() ? academy.getNominativeCaseTitle() : academy.getFullTitle());
        injectModifier.put("academy_G", null != academy.getGenitiveCaseTitle() ? academy.getGenitiveCaseTitle() : academy.getFullTitle());
        injectModifier.put("academy_D", null != academy.getDativeCaseTitle() ? academy.getDativeCaseTitle() : academy.getFullTitle());
        injectModifier.put("academy_A", null != academy.getAccusativeCaseTitle() ? academy.getAccusativeCaseTitle() : academy.getFullTitle());
        injectModifier.put("academy_I", null != academy.getInstrumentalCaseTitle() ? academy.getInstrumentalCaseTitle() : academy.getFullTitle());
        injectModifier.put("academy_P", null != academy.getPrepositionalCaseTitle() ? academy.getPrepositionalCaseTitle() : academy.getFullTitle());

        injectModifier.put("topOrgUnitFullTitle", academy.getTitle());
        injectModifier.put("topOrgUnitCityTitle",
                           (null != academy.getPostalAddress() && null != academy.getPostalAddress().getSettlement()) ? academy.getPostalAddress().getSettlement().getTitle() :
		                           (null != academy.getLegalAddress() && null != academy.getLegalAddress().getSettlement()) ? academy.getLegalAddress().getSettlement().getTitle() :
				                           (null != academy.getAddress() && null != academy.getAddress().getSettlement()) ? academy.getAddress().getSettlement().getTitle() : "");
    }
}