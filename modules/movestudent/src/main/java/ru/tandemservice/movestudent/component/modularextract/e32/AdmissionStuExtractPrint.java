/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e32;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AdmissionStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 19.05.2009
 */
public class AdmissionStuExtractPrint implements IPrintFormCreator<AdmissionStuExtract>
{
    @Override
    @SuppressWarnings("deprecation")
    public RtfDocument createPrintForm(byte[] template, AdmissionStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        
        List<String> tagsToDel = new ArrayList<>();
        List<String> tagsToDelWithLineBefore = new ArrayList<>();
        
        if(extract.isPaymentsDebt())
        {
            String admissed = " не допущен";
            if (extract.getEntity().getPerson().getIdentityCard().getSex().isMale())
                admissed += " ";
            else
                admissed += "а ";
            
            StringBuilder preambula = new StringBuilder(modifier.getStringValue("Student"));
            preambula.append(" ").append(modifier.getStringValue("fio_N"));
            preambula.append(" ").append(modifier.getStringValue("course")).append(" курса ");
            preambula.append(modifier.getStringValue("orgUnit_G")).append(" ");
            preambula.append(modifier.getStringValue("developForm_GF")).append(" формы обучения ");
            preambula.append(modifier.getStringValue("compensationTypeStr_G"));
            preambula.append(" приказом ректора № ").append(extract.getDischargeOrderNumber());
            preambula.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDischargeOrderDate()));
            preambula.append(admissed).append("к учебному процессу в связи с отсутствием оплаты за обучение.");
            
            modifier.put("debtPresence", preambula.toString());
            modifier.put("payingOff", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPayingOffDate()) + " задолженность по оплате за обучение оплачена в полном объеме.");
        }
        else
        {
            tagsToDelWithLineBefore.add("debtPresence");
            tagsToDel.add("payingOff");
        }
        
        int number = 1;
        RtfTableModifier table = new RtfTableModifier();
        if(!extract.isHasDebts())
        {
            tagsToDel.add("debts");
            UniRtfUtil.removeTableByName(document, "T", true, false);
        }
        else
        {
            GrammaCase rusCase = GrammaCase.DATIVE;
            IdentityCard identityCard = extract.getEntity().getPerson().getIdentityCard();

            InflectorVariant variant = DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(rusCase);

            CoreCollectionUtils.Pair<String, InflectorVariant> lastNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.lastName().s(), variant);
            CoreCollectionUtils.Pair<String, InflectorVariant> firstNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.firstName().s(), variant);
            CoreCollectionUtils.Pair<String, InflectorVariant> middleNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.middleName().s(), variant);

            String lastName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(lastNamePairKey);
            String firstName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(firstNamePairKey);
            String middleName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(middleNamePairKey);

            StringBuilder fio_D = new StringBuilder(lastName);
            fio_D.append(" ").append(firstName);
            if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
                fio_D.append(" ").append(middleName);
            
            modifier.put("debts", ++number + ". " + modifier.getStringValue("Student_D") + " " + fio_D.toString() + " ликвидировать разницу в учебных планах в срок до " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDeadline()) + " г.");

            int i = 0;
            List<StuExtractToDebtRelation> relsList = MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(extract);
            String[][] tableData = new String[relsList.size()][];
            for(StuExtractToDebtRelation rel : relsList)
            {
                tableData[i++] = new String[] {String.valueOf(i), rel.getDiscipline(), String.valueOf(rel.getHours()), rel.getControlAction()};
            }
            table.put("T", tableData);
        }
        table.modify(document);
        
        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, tagsToDel, false, false);
        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, tagsToDelWithLineBefore, true, false);

        CommonExtractPrint.addEntryDateToModifier(modifier, extract.getAdmissionDate()).modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}