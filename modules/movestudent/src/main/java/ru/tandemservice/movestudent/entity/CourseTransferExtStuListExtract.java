package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.CourseTransferExtStuListExtractGen;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Date;

/**
 * Проект приказа «О переводе с курса на следующий курс (по направлениям подготовки)»
 */
public class CourseTransferExtStuListExtract extends CourseTransferExtStuListExtractGen implements ITransferExtract
{
    @Override
    public Date getBeginDate()
    {
        return getTransferDate();
    }

    @Override
    public Group getGroupOld()
    {
        return getGroup();
    }

    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}