package ru.tandemservice.movestudent.entity.custom;

import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.component.customorder.CustomOrderUtils;
import ru.tandemservice.movestudent.component.customorder.ICustomPrintFormContainer;
import ru.tandemservice.movestudent.component.customorder.IStudentCustomOrderExtract;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.custom.gen.ModifyListStuExtractGen;

import java.util.Date;

/**
 * Выписка из списочного приказа по студенту с кастомной логикой: пользователь сам выбирает набор действий и шаблон печати
 */
public class ModifyListStuExtract extends ModifyListStuExtractGen implements IStudentCustomOrderExtract, ICustomPrintFormContainer
{
    @Override
    public Date getBeginDate()
    {
        return null;
    }

    @Override
    public Date getEndDate()
    {
        return null;
    }

    /**
     * Номер изменяемой выписки
     */
    public String getModifiableExtractNumber()
    {
        return (getModifiableExtract() != null) ? ("" + getModifiableExtract().getParagraph().getNumber() + "/" + getModifiableExtract().getNumber()) : null;
    }

    /**
     * Название отменяемого приказа
     */
    public String getModifiableOrderTitle()
    {
        return getModifiableExtract() != null ? CustomOrderUtils.getOrderTitle(getModifiableExtract()) : null;
    }

    @Override
    public DatabaseFile getFile()
    {
        return getUserPrintForm();
    }

    @Override
    public void setFile(DatabaseFile file)
    {
        setUserPrintForm(file);
    }

    @Override
    public byte[] getBasicTemplate()
    {
        return MoveStudentDaoFacade.getMoveStudentDao().getTemplate(getType(), MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);
    }

    @Override
    public Long getModifiableOrderId()
    {
        return getModifiableExtract() != null ? getModifiableExtract().getParagraph().getOrder().getId() : null;
    }
}