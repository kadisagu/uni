/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.list.StudentListOrderPrint;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;

import ru.tandemservice.movestudent.entity.StudentListOrder;

/**
 * @author vip_delete
 * @since 08.04.2009
 */
@State({
        @Bind(key = "orderId", binding = "order.id"),
        @Bind(key = "printPdf", binding = "printPdf")
})
public class Model
{
    private StudentListOrder _order = new StudentListOrder();
    private byte[] _data;
    private Boolean _printPdf;
    private String _barcode;
    private boolean _addNumOfPages = false;
    private String _fileName;


    public StudentListOrder getOrder()
    {
        return _order;
    }

    public void setOrder(StudentListOrder order)
    {
        _order = order;
    }

    public byte[] getData()
    {
        return _data;
    }

    public void setData(byte[] data)
    {
        _data = data;
    }

    public Boolean getPrintPdf()
    {
        return _printPdf;
    }

    public void setPrintPdf(Boolean printPdf)
    {
        _printPdf = printPdf;
    }

    public String getBarcode()
    {
        return _barcode;
    }

    public void setBarcode(String barcode)
    {
        _barcode = barcode;
    }

    public boolean isAddNumOfPages()
    {
        return _addNumOfPages;
    }

    public void setAddNumOfPages(boolean addNumOfPages)
    {
        _addNumOfPages = addNumOfPages;
    }

    public String getFileName()
    {
        return _fileName;
    }

    public void setFileName(String fileName)
    {
        _fileName = fileName;
    }
}