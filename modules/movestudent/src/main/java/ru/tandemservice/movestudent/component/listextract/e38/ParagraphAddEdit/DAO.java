/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e38.ParagraphAddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.SplitStudentsExtStuListExtract;
import ru.tandemservice.movestudent.utils.ExtEducationLevelsHighSchoolSelectModel;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;

/**
 * @author ListExtractComponentGenerator
 * @since 12.12.2012
 */
public class DAO extends AbstractListParagraphAddEditDAO<SplitStudentsExtStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setCompensationTypeList(UniDaoFacade.getCoreDao().getCatalogItemList(CompensationType.class));
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));

        model.setDevelopFormModel(EducationOrgUnitUtil.getDevelopFormSelectModel(model, false));
        model.setDevelopConditionModel(EducationOrgUnitUtil.getDevelopConditionSelectModel(model, false));
        model.setDevelopTechModel(EducationOrgUnitUtil.getDevelopTechSelectModel(model, false));
        model.setDevelopPeriodModel(EducationOrgUnitUtil.getDevelopPeriodSelectModel(model, false));

        SplitStudentsExtStuListExtract firstExtract = model.getFirstExtract();
        if (null!=firstExtract)
        {
            if (null != model.getParagraphId())
            {
                model.setEducationLevelsHighSchool(firstExtract.getEducationOrgUnitNew().getEducationLevelHighSchool());
            }
            model.setGroup(firstExtract.getEntity().getGroup());
            model.setCourse(firstExtract.getEntity().getGroup().getCourse());
            model.setParentEduLevel(EducationOrgUnitUtil.getParentLevel(model.getGroup().getEducationOrgUnit().getEducationLevelHighSchool()));
            model.setDevelopForm(firstExtract.getEntity().getGroup().getEducationOrgUnit().getDevelopForm());
            model.setDevelopCondition(firstExtract.getEntity().getGroup().getEducationOrgUnit().getDevelopCondition());
            model.setDevelopTech(firstExtract.getEntity().getGroup().getEducationOrgUnit().getDevelopTech());
            model.setDevelopPeriod(firstExtract.getEntity().getGroup().getEducationOrgUnit().getDevelopPeriod());
            model.setCompensationType(firstExtract.getEntity().getCompensationType());
        }


        model.setSpecializationsListModel(new ExtEducationLevelsHighSchoolSelectModel(model, model).dependFromFUTS(true));
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        if (null != model.getDevelopForm())
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developForm().s(), model.getDevelopForm()));
        if (null != model.getDevelopCondition())
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developCondition().s(), model.getDevelopCondition()));
        if (null != model.getDevelopTech())
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developTech().s(), model.getDevelopTech()));
        if (null != model.getDevelopPeriod())
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developPeriod().s(), model.getDevelopPeriod()));
        if (null != model.getCompensationType())
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.compensationType().s(), model.getCompensationType()));
    }

    @Override
    protected SplitStudentsExtStuListExtract createNewInstance(Model model)
    {
        return new SplitStudentsExtStuListExtract();
    }

    @Override
    protected void fillExtract(SplitStudentsExtStuListExtract extract, Student student, Model model)
    {
        extract.setEducationOrgUnitOld(student.getEducationOrgUnit());
        extract.setEducationOrgUnitNew(UniDaoFacade.getEducationLevelDao().getEducationOrgUnit(model));
    }
}