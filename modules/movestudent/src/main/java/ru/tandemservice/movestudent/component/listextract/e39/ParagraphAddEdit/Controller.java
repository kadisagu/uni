/* $Id: Controller.java.vm 7834 2009-05-06 09:39:03Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e39.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 21.01.2013
 */
@SuppressWarnings("unchecked")
public class Controller extends AbstractListParagraphAddEditController<AcadGrantBonusAssignStuListExtract, IDAO, Model>
{
    @Override
    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дополнительный статус", Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Пол", Student.person().identityCard().sex().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("status", "Состояние", Model.STATUS_KEY, SimpleFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("grantBonusSize","Размер надбавки").setClickable(false).setOrderable(false));
	    ActionColumn clone = new ActionColumn("Заполнить по образцу", "clone", "onClickCopyGrantBonusSize");
	    clone.setValidate(true);
	    dataSource.addColumn(clone);
	    dataSource.setOrder("status", OrderDirection.asc);

        Model model = getModel(component);
        if (model.isEditForm())
        {
            IValueMapHolder grantBonusSizeHolder = (IValueMapHolder) dataSource.getColumn("grantBonusSize");
            Map<Long, Double> grantBonusSizeMap = (null == grantBonusSizeHolder ? Collections.emptyMap() : grantBonusSizeHolder.getValueMap());

            for (AcadGrantBonusAssignStuListExtract grantBonusExtract: (List<AcadGrantBonusAssignStuListExtract>) model.getParagraph().getExtractList())
            {
                grantBonusSizeMap.put(grantBonusExtract.getEntity().getId(), grantBonusExtract.getGrantBonusSize());
            }
        }
    }

    public void onClickCopyGrantBonusSize(IBusinessComponent component)
    {
        Model model = getModel(component);

        final IValueMapHolder grantBonusSizeHolder = (IValueMapHolder) model.getDataSource().getColumn("grantBonusSize");
        final Map<Long, Double> grantBonusSizeMap = (null == grantBonusSizeHolder ? Collections.emptyMap() : grantBonusSizeHolder.getValueMap());

        Number currentGrantBonusSize = grantBonusSizeMap.get((Long) component.getListenerParameter());

        DynamicListDataSource dataSource = model.getDataSource();

        for (ViewWrapper<Student> wrapper: (List<ViewWrapper<Student>>) dataSource.getSelectedEntities())
        {
            if (grantBonusSizeMap.get(wrapper.getEntity().getId()) == null && currentGrantBonusSize != null)
            {
	            grantBonusSizeMap.put(wrapper.getEntity().getId(), currentGrantBonusSize.doubleValue());
            }
        }

        model.getDataSource().refresh();
    }
}
