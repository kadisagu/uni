/* $Id$ */
package ru.tandemservice.movestudent.component.commons.gradation;

import java.util.Date;

/**
 * Выписка о прекращении выплаты студенту.
 *
 * @author Nikolay Fedorovskih
 * @since 15.12.2013
 */
public interface ITakeOffPayment
{
    /**
     * Дата прекращения выплаты.
     */
    Date getPaymentTakeOffDate();
}