/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author vip_delete
 * @since 09.04.2009
 */
public abstract class AbstractListParagraphAddEditController<T extends ListStudentExtract, IDAO extends IAbstractListParagraphAddEditDAO<T, Model>, Model extends AbstractListParagraphAddEditModel<T>> extends AbstractBusinessController<IDAO, Model>
{
    public static final String EDU_LVL_HS_COLUMN_TITLE = EducationOrgUnit.EDU_LVL_HS_COLUMN_TITLE;

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        // настройка действует в рамках типа параграфа
        model.setSearchListSettingsKey("StudentListParagraphAddEdit." + model.getParagraphType().getCode() + ".");

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<Student> dataSource = UniBaseUtils.createDataSource(component, getDao());

        CheckboxColumn checkboxColumn = new CheckboxColumn("checkbox", "", true);
        if (model.getSelectedItemList() != null)
            checkboxColumn.setSelectedObjects(model.getSelectedItemList());
        checkboxColumn.setDisabledProperty(AbstractListParagraphAddEditModel.P_STUDENT_DISABLED);
        checkboxColumn.setListener("onSelectStudent");
        checkboxColumn.setDisplayTotal(true);

        checkboxColumn.setParametersResolver((entity, valueEntity) -> entity
        );
        dataSource.addColumn(checkboxColumn);

        prepareListDataSource(component, dataSource);
        model.setDataSource(dataSource);
    }

    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Пол", Student.person().identityCard().sex().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дополнительный статус", Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("status", "Состояние", Model.STATUS_KEY, SimpleFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Группа", Student.group().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", Student.educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", new String[]{Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_FULL_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", new String[]{Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_FULL_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn(EducationOrgUnit.EDU_LVL_HS_COLUMN_TITLE, Student.educationOrgUnit().educationLevelHighSchool().configurableTitle().s()).setClickable(false));
        dataSource.setOrder("status", OrderDirection.asc);
    }

    public void onSelectStudent(IBusinessComponent component)
    {
    }

    public void onClickApply(IBusinessComponent component)
    {
        ErrorCollector errors = component.getUserContext().getErrorCollector();

        Model model = getModel(component);

        getDao().validate(model, errors);

        if (errors.hasErrors()) return;

        getDao().update(getModel(component));

        if (getModel(component).getParagraphId() == null)
        {
            // форма добавления
            deactivate(component, 2);//назад на 2-е страницы
            activateInRoot(component, new ComponentActivator(MoveStudentUtils.getListParagraphPubComponent(model.getParagraph().getAnyExtractType()), new ParametersMap()
                    .add(PublisherActivator.PUBLISHER_ID_KEY, model.getParagraph().getId()
            )));
        } else
            deactivate(component);
    }

    public void onClickDeactivate(IBusinessComponent component)
    {
        if (getModel(component).getParagraphId() == null)
            deactivate(component, 2);// форма добавления
        else
            deactivate(component);
    }
}
