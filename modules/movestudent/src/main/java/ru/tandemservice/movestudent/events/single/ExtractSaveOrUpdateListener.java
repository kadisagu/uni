/* $Id$ */
package ru.tandemservice.movestudent.events.single;

import org.tandemframework.hibsupport.event.single.FilteredSingleEntityEventListener;
import org.tandemframework.hibsupport.event.single.ISingleEntityEvent;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.OtherStudentExtract;


/**
 * @author Ekaterina Zvereva
 * @since 01.12.2014
 */
public class ExtractSaveOrUpdateListener extends FilteredSingleEntityEventListener
{
    /**
     * Метод, который должны реализовать наследники класса
     * {@link org.tandemframework.hibsupport.event.single.FilteredSingleEntityEventListener}. В качестве аргументов передаются
     * уже отфильтрованные события
     *
     * @param event отфильтрованное событие
     */
    @Override
    public void onFilteredEvent(ISingleEntityEvent event)
    {
        AbstractStudentExtract extract = (AbstractStudentExtract) event.getEntity();
        if (extract instanceof OtherStudentExtract)
            extract.setCommentEdited(true);
        else if (!extract.isCommentEdited())
            extract.setComment(extract.getGeneratedComment(false));
    }
}
