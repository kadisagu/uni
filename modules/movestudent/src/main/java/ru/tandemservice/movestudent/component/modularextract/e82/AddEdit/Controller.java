/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e82.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.movestudent.entity.RePassDiscStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToRePassDiscipline;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 31.08.2012
 */
public class Controller extends CommonModularStudentExtractAddEditController<RePassDiscStuExtract, IDAO, Model>
{
    public void onClickAddDiscipline(IBusinessComponent component)
    {
        getModel(component).getDisciplineList().add(new StuExtractToRePassDiscipline());
    }

    public void onClickDeleteDiscipline(IBusinessComponent component)
    {
        Model model = getModel(component);
        if(model.getDisciplineList().size() == 1)
        {
            model.getDisciplineList().clear();
            model.getDisciplineList().add(new StuExtractToRePassDiscipline());
            return;
        }

        Integer discNumber = (Integer)component.getListenerParameter();
        StuExtractToRePassDiscipline disc = model.getDisciplineList().get(discNumber);
        model.getDisciplineList().remove(disc);
    }
}