/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e8.ParagraphAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.HolidayStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Date;

/**
 * @author ListExtractComponentGenerator
 * @since 09.06.2009
 */
public class Model extends AbstractListParagraphAddEditModel<HolidayStuListExtract> implements IGroupModel
{
    private Course _course;
    private ISelectModel _groupListModel;
    private Group _group;
    private Date _beginDate;
    private Date _endDate;

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        this._course = course;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        this._groupListModel = groupListModel;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        this._group = group;
    }

    public Date getBeginDate()
    {
        return _beginDate;
    }

    public void setBeginDate(Date beginDate)
    {
        this._beginDate = beginDate;
    }

    public Date getEndDate()
    {
        return _endDate;
    }

    public void setEndDate(Date endDate)
    {
        this._endDate = endDate;
    }
}