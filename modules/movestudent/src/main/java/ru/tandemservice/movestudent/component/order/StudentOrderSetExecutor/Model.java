/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.StudentOrderSetExecutor;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.movestudent.entity.AbstractStudentOrder;

/**
 * @author vip_delete
 * @since 15.12.2008
 */
@Input(keys = {"orderId"}, bindings = {"orderId"})
public class Model
{
    private Long _orderId;
    private AbstractStudentOrder _order;
    private ISelectModel _employeePostModel;

    public boolean getShowExecutorPrev()
    {
        return !(StringUtils.isEmpty(_order.getExecutor()) && null == _order.getExecutorEmpl() || null != _order.getExecutorEmpl());
    }

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public AbstractStudentOrder getOrder()
    {
        return _order;
    }

    public void setOrder(AbstractStudentOrder order)
    {
        _order = order;
    }

    public ISelectModel getEmployeePostModel()
    {
        return _employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        _employeePostModel = employeePostModel;
    }
}