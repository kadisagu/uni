package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.GiveCardDuplicateStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О выдаче дубликата студенческого билета
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GiveCardDuplicateStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.GiveCardDuplicateStuExtract";
    public static final String ENTITY_NAME = "giveCardDuplicateStuExtract";
    public static final int VERSION_HASH = -1165839610;
    private static IEntityMeta ENTITY_META;

    public static final String P_CARD_NUMBER = "cardNumber";

    private String _cardNumber;     // Номер выдаваемого документа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер выдаваемого документа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCardNumber()
    {
        return _cardNumber;
    }

    /**
     * @param cardNumber Номер выдаваемого документа. Свойство не может быть null.
     */
    public void setCardNumber(String cardNumber)
    {
        dirty(_cardNumber, cardNumber);
        _cardNumber = cardNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof GiveCardDuplicateStuExtractGen)
        {
            setCardNumber(((GiveCardDuplicateStuExtract)another).getCardNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GiveCardDuplicateStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GiveCardDuplicateStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new GiveCardDuplicateStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "cardNumber":
                    return obj.getCardNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "cardNumber":
                    obj.setCardNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "cardNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "cardNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "cardNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GiveCardDuplicateStuExtract> _dslPath = new Path<GiveCardDuplicateStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GiveCardDuplicateStuExtract");
    }
            

    /**
     * @return Номер выдаваемого документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GiveCardDuplicateStuExtract#getCardNumber()
     */
    public static PropertyPath<String> cardNumber()
    {
        return _dslPath.cardNumber();
    }

    public static class Path<E extends GiveCardDuplicateStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<String> _cardNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер выдаваемого документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GiveCardDuplicateStuExtract#getCardNumber()
     */
        public PropertyPath<String> cardNumber()
        {
            if(_cardNumber == null )
                _cardNumber = new PropertyPath<String>(GiveCardDuplicateStuExtractGen.P_CARD_NUMBER, this);
            return _cardNumber;
        }

        public Class getEntityClass()
        {
            return GiveCardDuplicateStuExtract.class;
        }

        public String getEntityName()
        {
            return "giveCardDuplicateStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
