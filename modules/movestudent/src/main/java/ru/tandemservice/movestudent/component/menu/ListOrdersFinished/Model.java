/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.menu.ListOrdersFinished;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

import java.util.List;

/**
 * @author vip_delete
 * @since 18.10.2008
 */
public class Model
{
    public static final String[] ORG_UNI_FULL_TITLE = new String[]{AbstractStudentOrder.L_ORG_UNIT, OrgUnit.P_FULL_TITLE};

    private ISelectModel _educationYearList;
    private ISelectModel _orgUnitListModel;
    private IMultiSelectModel _orderTypesListModel;
    private IMultiSelectModel _orderReasonModel;
    private DynamicListDataSource _dataSource;
    private IDataSettings _settings;
    private ISelectModel _studentList;
    private ISelectModel employeePostModel;

    private ISelectModel _educationLevelTypeListModel;
    private List<StructureEducationLevels> _educationLevelTypeList;

    public ISelectModel getEducationLevelTypeListModel()
    {
        return _educationLevelTypeListModel;
    }

    public void setEducationLevelTypeListModel(ISelectModel educationLevelTypeListModel)
    {
        _educationLevelTypeListModel = educationLevelTypeListModel;
    }



    // Getters & Setters

    public ISelectModel getEducationYearList()
    {
        return _educationYearList;
    }

    public void setEducationYearList(ISelectModel educationYearList)
    {
        _educationYearList = educationYearList;
    }

    public ISelectModel getOrgUnitListModel()
    {
        return _orgUnitListModel;
    }

    public void setOrgUnitListModel(ISelectModel orgUnitListModel)
    {
        _orgUnitListModel = orgUnitListModel;
    }

    public IMultiSelectModel getOrderTypesListModel()
    {
        return _orderTypesListModel;
    }

    public void setOrderTypesListModel(IMultiSelectModel orderTypesListModel)
    {
        _orderTypesListModel = orderTypesListModel;
    }

    public IMultiSelectModel getOrderReasonModel()
    {
        return _orderReasonModel;
    }

    public void setOrderReasonModel(IMultiSelectModel orderReasonModel)
    {
        _orderReasonModel = orderReasonModel;
    }

    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public ISelectModel getStudentList()
    {
        return _studentList;
    }

    public void setStudentList(ISelectModel studentList)
    {
        _studentList = studentList;
    }

    public ISelectModel getEmployeePostModel()
    {
        return employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        this.employeePostModel = employeePostModel;
    }
}
