package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IEnrollmentExtract;
import ru.tandemservice.movestudent.entity.gen.RestorationAdmitStateExamsExtStuExtractGen;

import java.util.Date;

/**
 * О восстановлении и повторном допуске к сдаче государственного экзамена (пересдача ГЭ)
 */
public class RestorationAdmitStateExamsExtStuExtract extends RestorationAdmitStateExamsExtStuExtractGen implements IEnrollmentExtract
{
    @Override
    public Date getEntranceDate()
    {
        return getRestorationDate();
    }
}