\keep\keepn\qj\b {extractNumber}. ОБЪЯВИТЬ:\b0\par
{fio} — {student_D} {course} курса {developForm_G} отделения группы {group} {orgUnit_G}, {learned_D} {compensationTypeStr}, по {educationType_D} «{educationOrgUnit}», выговор за утерю студенческого билета и выдать дубликат взамен утерянного.\par
  Основание: {listBasics}.