/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderAddEdit;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StuListOrderToBasicRelation;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentReasonToBasicRel;
import ru.tandemservice.movestudent.entity.StudentReasonToTypeRel;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.Date;
import java.util.List;

/**
 * @author vip_delete
 * @since 05.05.2009
 */
public abstract class AbstractListOrderAddEditDAO<T extends StudentListOrder, Model extends AbstractListOrderAddEditModel<T>> extends UniDao<Model> implements IAbstractListOrderAddEditDAO<T, Model>
{
    @Override
    @SuppressWarnings({"unchecked"})
    public void prepare(final Model model)
    {
        if (model.getOrderId() != null)
        {
            // форма редактирования
            model.setOrder(refresh((T) getNotNull(model.getOrderId())));
            for (StuListOrderToBasicRelation rel : getList(StuListOrderToBasicRelation.class, StuListOrderToBasicRelation.L_ORDER, model.getOrder()))
                model.getBasicList().add(rel.getBasic());
        }
        else
        {
            // форма добавления
            T order = createNewInstance();
            if (null != model.getOrgUnitId())
                order.setOrgUnit((OrgUnit) getNotNull(model.getOrgUnitId()));
            order.setCreateDate(new Date());
            order.setType(getNotNull(StudentExtractType.class, model.getOrderTypeId()));
            model.setOrder(order);
        }

        model.setReasonListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                List<StudentOrderReasons> list = getFirstList(StudentReasonToTypeRel.class, model.getOrder().getType());
                return new ListResult<>(list);
            }
        });
        model.setBasicListModel(new FullCheckSelectModel(ICatalogItem.CATALOG_ITEM_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getOrder().getReason() == null)
                    return ListResult.getEmpty();

                Criteria c = getSession().createCriteria(StudentReasonToBasicRel.class, "r");
                c.createAlias("r." + IEntityRelation.L_SECOND, "s");
                c.add(Restrictions.eq("r." + IEntityRelation.L_FIRST, model.getOrder().getReason()));
                c.setProjection(Projections.property("r." + IEntityRelation.L_SECOND));
                if (filter != null)
                    c.add(Restrictions.like("s." + StudentOrderBasics.P_TITLE, "%" + filter));
                c.addOrder(Order.asc(("s." + StudentOrderBasics.P_TITLE)));
                return new ListResult(c.list());
            }
        });

        model.setEducationYearsList(new EducationYearModel());

        model.setEmployeePostModel(new OrderExecutorSelectModel());

        if (null == model.getOrderId())
            model.getOrder().setEducationYear(UniDaoFacade.getCoreDao().get(EducationYear.class, EducationYear.P_CURRENT, Boolean.TRUE));

        if (null != model.getOrder().getId())
        {
            Criteria c = getSession().createCriteria(StuListOrderToBasicRelation.class, "r");
            c.createAlias("r." + StuListOrderToBasicRelation.L_BASIC, "b");
            c.add(Restrictions.eq("r." + StuListOrderToBasicRelation.L_ORDER, model.getOrder()));
            c.addOrder(Order.asc("b." + StudentOrderBasics.P_TITLE));
            List<StuListOrderToBasicRelation> relationList = c.list();

            model.getBasicList().clear();
            for (StuListOrderToBasicRelation relation : relationList)
                model.getBasicList().add(relation.getBasic());

            model.getCurrentBasicMap().clear();
            for (StuListOrderToBasicRelation relation : relationList)
            {
                model.getCurrentBasicMap().put(relation.getBasic().getId(), relation.getComment());
            }
        }
        else
        {
            IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
            if (principalContext instanceof EmployeePost)
                model.setEmployeePost((EmployeePost) principalContext);
        }
    }

    protected abstract T createNewInstance();

    @Override
    public void update(Model model)
    {
        T order = model.getOrder();
        if (order.getId() != null)
        {
            if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(MoveStudentDaoFacade.getMoveStudentDao().getOrderStateCode(order.getId())))
                throw new ApplicationException("Редактирование приказа возможно только на этапе формирования.");
        }

        MoveStudentDaoFacade.getMoveStudentDao().checkOrderNumber(order);

        order.setExecutorEmpl(model.getEmployeePost());
        order.setResponsibleEmpl(model.getResponsibleEmployee());
        String responsible = OrderExecutorSelectModel.getExecutor(model.getResponsibleEmployee());
        if (responsible != null) order.setExecutor(responsible);
        String executor = OrderExecutorSelectModel.getExecutor(model.getEmployeePost());
        if (executor != null) order.setExecutor(executor);

        saveOrUpdate(order);
        updateBasicList(model);
    }

    private void updateBasicList(Model model)
    {
        if (null != model.getOrderId())
        {
            for (StuListOrderToBasicRelation relation : getList(StuListOrderToBasicRelation.class, StuListOrderToBasicRelation.L_ORDER, model.getOrder()))
                delete(relation);
            getSession().flush();
        }

        for (StudentOrderBasics basic : model.getBasicList())
        {
            StuListOrderToBasicRelation relation = new StuListOrderToBasicRelation();
            relation.setBasic(basic);
            relation.setOrder(model.getOrder());
            relation.setComment(model.getCurrentBasicMap().get(basic.getId()));
            save(relation);
        }
    }
}
