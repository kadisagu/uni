/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

import org.tandemframework.core.entity.IEntity;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 28.11.2013
 */
public interface IPatchResolver
{
    /**
     * Здесь заполняем все необходимые view properties
     */
    void patch(List<Object[]> srcList, List<IEntity> destList, List<IFlexColumn> columns);
}