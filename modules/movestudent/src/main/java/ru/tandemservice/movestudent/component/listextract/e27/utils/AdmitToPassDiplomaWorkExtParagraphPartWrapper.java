/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e27.utils;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 13.09.2012
 */
public class AdmitToPassDiplomaWorkExtParagraphPartWrapper implements Comparable<AdmitToPassDiplomaWorkExtParagraphPartWrapper>
{
    private Group _group;
    private final ListStudentExtract _firstExtract;

    public AdmitToPassDiplomaWorkExtParagraphPartWrapper(Group group, ListStudentExtract firstExtract)
    {
        _group = group;
        _firstExtract = firstExtract;
    }

    private List<Person> _personList = new ArrayList<Person>();

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public List<Person> getPersonList()
    {
        return _personList;
    }

    public void setPersonList(List<Person> personList)
    {
        _personList = personList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof AdmitToPassDiplomaWorkExtParagraphPartWrapper))
            return false;

        AdmitToPassDiplomaWorkExtParagraphPartWrapper that = (AdmitToPassDiplomaWorkExtParagraphPartWrapper) o;

        return (_group.equals(that.getGroup()));
    }

    @Override
    public int hashCode()
    {
        return _group.hashCode();
    }

    @Override
    public int compareTo(AdmitToPassDiplomaWorkExtParagraphPartWrapper o)
    {
        return _group.getTitle().compareTo(o.getGroup().getTitle());
    }
}