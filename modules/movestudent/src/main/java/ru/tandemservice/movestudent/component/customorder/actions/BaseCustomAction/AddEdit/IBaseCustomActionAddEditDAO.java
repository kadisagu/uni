/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit;

import ru.tandemservice.movestudent.component.customorder.IStudentCustomOrderExtract;
import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.unimove.entity.ICustomOrderAction;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public interface IBaseCustomActionAddEditDAO<T extends ICustomOrderAction, Model extends BaseCustomActionAddEditModel<T>> extends IUniDao<Model>
{
    void updateForExtract(Model model, IStudentCustomOrderExtract extract, boolean isEdit);

    void updatePrepare(Model model, T action, IStudentCustomOrderExtract extract);

    void doCommit(T action);

    void doRollback(T action);
}