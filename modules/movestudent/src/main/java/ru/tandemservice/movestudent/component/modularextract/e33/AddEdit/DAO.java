/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e33.AddEdit;

import org.tandemframework.core.info.ErrorCollector;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.AssignBenefitStuExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 19.05.2009
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<AssignBenefitStuExtract, Model>
{
    @Override
    protected AssignBenefitStuExtract createNewInstance()
    {
        return new AssignBenefitStuExtract();
    }
    
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        
        if(model.isAddForm() && null == model.getExtract().getReasonStr())
        {
            model.getExtract().setReasonStr("с рождением ребенка");
        }
    }

    @Override
    public void update(Model model)
    {
        if(!model.getExtract().isAssignBenefit())
        {
            model.getExtract().setBenefitType(null);
            model.getExtract().setBenefitBeginDate(null);
            model.getExtract().setBenefitEndDate(null);
        }
        
        if(!model.getExtract().isAllowIndependentSchedule())
        {
            model.getExtract().setReasonStr(null);
            model.getExtract().setIndepSchedBeginDate(null);
            model.getExtract().setIndepSchedEndDate(null);
        }
        
        super.update(model);
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);
        
        if(model.getExtract().isAssignBenefit())
        {
            if(model.getExtract().getBenefitBeginDate().getTime() >= model.getExtract().getBenefitEndDate().getTime())
                errors.add("Дата начала выплаты пособия должна быть раньше даты ее окончания.", "benefitBeginDate", "benefitEndDate");
        }
        
        if(model.getExtract().isAllowIndependentSchedule())
        {
            if(model.getExtract().getIndepSchedBeginDate().getTime() >= model.getExtract().getIndepSchedEndDate().getTime())
                errors.add("Дата начала свободного посещения знаятий должна быть раньше даты его окончания.", "indepSchedBeginDate", "indepSchedEndDate");
        }
    }
}