package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.entity.catalog.StudentExtractGroup;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь типа приказа с группой
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentExtractTypeToGroupGen extends EntityBase
 implements INaturalIdentifiable<StudentExtractTypeToGroupGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup";
    public static final String ENTITY_NAME = "studentExtractTypeToGroup";
    public static final int VERSION_HASH = -1764398352;
    private static IEntityMeta ENTITY_META;

    public static final String L_TYPE = "type";
    public static final String L_GROUP = "group";

    private StudentExtractType _type;     // Тип выписки по студенту
    private StudentExtractGroup _group;     // Группы приказов по движению студентов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип выписки по студенту. Свойство не может быть null.
     */
    @NotNull
    public StudentExtractType getType()
    {
        return _type;
    }

    /**
     * @param type Тип выписки по студенту. Свойство не может быть null.
     */
    public void setType(StudentExtractType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Группы приказов по движению студентов. Свойство не может быть null.
     */
    @NotNull
    public StudentExtractGroup getGroup()
    {
        return _group;
    }

    /**
     * @param group Группы приказов по движению студентов. Свойство не может быть null.
     */
    public void setGroup(StudentExtractGroup group)
    {
        dirty(_group, group);
        _group = group;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentExtractTypeToGroupGen)
        {
            if (withNaturalIdProperties)
            {
                setType(((StudentExtractTypeToGroup)another).getType());
                setGroup(((StudentExtractTypeToGroup)another).getGroup());
            }
        }
    }

    public INaturalId<StudentExtractTypeToGroupGen> getNaturalId()
    {
        return new NaturalId(getType(), getGroup());
    }

    public static class NaturalId extends NaturalIdBase<StudentExtractTypeToGroupGen>
    {
        private static final String PROXY_NAME = "StudentExtractTypeToGroupNaturalProxy";

        private Long _type;
        private Long _group;

        public NaturalId()
        {}

        public NaturalId(StudentExtractType type, StudentExtractGroup group)
        {
            _type = ((IEntity) type).getId();
            _group = ((IEntity) group).getId();
        }

        public Long getType()
        {
            return _type;
        }

        public void setType(Long type)
        {
            _type = type;
        }

        public Long getGroup()
        {
            return _group;
        }

        public void setGroup(Long group)
        {
            _group = group;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof StudentExtractTypeToGroupGen.NaturalId) ) return false;

            StudentExtractTypeToGroupGen.NaturalId that = (NaturalId) o;

            if( !equals(getType(), that.getType()) ) return false;
            if( !equals(getGroup(), that.getGroup()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getType());
            result = hashCode(result, getGroup());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getType());
            sb.append("/");
            sb.append(getGroup());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentExtractTypeToGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentExtractTypeToGroup.class;
        }

        public T newInstance()
        {
            return (T) new StudentExtractTypeToGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "type":
                    return obj.getType();
                case "group":
                    return obj.getGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "type":
                    obj.setType((StudentExtractType) value);
                    return;
                case "group":
                    obj.setGroup((StudentExtractGroup) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "type":
                        return true;
                case "group":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "type":
                    return true;
                case "group":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "type":
                    return StudentExtractType.class;
                case "group":
                    return StudentExtractGroup.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentExtractTypeToGroup> _dslPath = new Path<StudentExtractTypeToGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentExtractTypeToGroup");
    }
            

    /**
     * @return Тип выписки по студенту. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup#getType()
     */
    public static StudentExtractType.Path<StudentExtractType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Группы приказов по движению студентов. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup#getGroup()
     */
    public static StudentExtractGroup.Path<StudentExtractGroup> group()
    {
        return _dslPath.group();
    }

    public static class Path<E extends StudentExtractTypeToGroup> extends EntityPath<E>
    {
        private StudentExtractType.Path<StudentExtractType> _type;
        private StudentExtractGroup.Path<StudentExtractGroup> _group;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип выписки по студенту. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup#getType()
     */
        public StudentExtractType.Path<StudentExtractType> type()
        {
            if(_type == null )
                _type = new StudentExtractType.Path<StudentExtractType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Группы приказов по движению студентов. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup#getGroup()
     */
        public StudentExtractGroup.Path<StudentExtractGroup> group()
        {
            if(_group == null )
                _group = new StudentExtractGroup.Path<StudentExtractGroup>(L_GROUP, this);
            return _group;
        }

        public Class getEntityClass()
        {
            return StudentExtractTypeToGroup.class;
        }

        public String getEntityName()
        {
            return "studentExtractTypeToGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
