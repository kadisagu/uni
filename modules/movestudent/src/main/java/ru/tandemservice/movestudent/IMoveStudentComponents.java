/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent;

/**
 * @author vip_delete
 * @since 16.10.2008
 */
public interface IMoveStudentComponents
{
    String MODULAR_STUDENT_EXTRACT_PRINT = "ru.tandemservice.movestudent.component.order.modular.ModularStudentExtractPrint";
    String REASON_PRINT_TITLE_SETTINGS_EDIT = "ru.tandemservice.movestudent.component.settings.ReasonPrintTitleSettingsEdit";
    String REASON_TO_BASICS_SETTINGS_PUB = "ru.tandemservice.movestudent.component.settings.ReasonToBasicsSettingsPub";
    String REASON_TO_BASICS_SETTINGS_ADD = "ru.tandemservice.movestudent.component.settings.ReasonToBasicsSettingsAdd";
    String REASON_TO_TYPES_SETTINGS_PUB = "ru.tandemservice.movestudent.component.settings.ReasonToTypesSettingsPub";
    String REASON_TO_TYPES_SETTINGS_ADD = "ru.tandemservice.movestudent.component.settings.ReasonToTypesSettingsAdd";
    String OTHER_ORDER_TYPE_FIELD_SETTINGS_EDIT = "ru.tandemservice.movestudent.component.settings.OtherOrderTypeFieldsSettingsEdit";
    String STUDENT_LIST_ORDER_ADD = "ru.tandemservice.movestudent.component.order.list.StudentListOrderAdd";
    String STUDENT_LIST_ORDER_PRINT = "ru.tandemservice.movestudent.component.order.list.StudentListOrderPrint";
    String STUDENT_LIST_MULTIPLE_PARAGRAPH_ADD = "ru.tandemservice.movestudent.component.order.list.StudentListMultipleParagraphAdd";
    String STUDENT_LIST_PARAGRAPH_ADD = "ru.tandemservice.movestudent.component.order.list.StudentListParagraphAdd";
    String STUDENT_LIST_EXTRACT_PRINT = "ru.tandemservice.movestudent.component.order.list.StudentListExtractPrint";
    String STUDENT_MODULAR_ORDER_ADD = "ru.tandemservice.movestudent.component.order.modular.StudentModularOrderAdd";
    String STUDENT_MODULAR_ORDER_ADD_EXTRACTS = "ru.tandemservice.movestudent.component.order.modular.StudentModularOrderAddExtracts";
    String STUDENT_MODULAR_ORDER_EDIT = "ru.tandemservice.movestudent.component.order.modular.StudentModularOrderEdit";
    String STUDENT_MODULAR_ORDER_PRINT = "ru.tandemservice.movestudent.component.order.modular.StudentModularOrderPrint";
    String STUDENT_OTHER_ORDER_ADD_EDIT = "ru.tandemservice.movestudent.component.order.other.StudentOtherOrderAddEdit";
    String STUDENT_OTHER_ORDER_CHANGE_TYPE = "ru.tandemservice.movestudent.component.order.other.StudentOtherOrderChangeType";
    String STUDENT_ORDER_SET_EXECUTOR = "ru.tandemservice.movestudent.component.order.StudentOrderSetExecutor";
    String ORDER_LIST_PRINT = "ru.tandemservice.movestudent.component.student.OrderListPrint";
    String CUSTOM_ORDER_TEMPLATE_EDIT = "ru.tandemservice.movestudent.component.customorder.TemplateEdit";

}