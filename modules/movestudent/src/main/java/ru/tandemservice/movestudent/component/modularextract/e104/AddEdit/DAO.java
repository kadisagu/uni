/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e104.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.SimpleListResultBuilder;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.TransferBetweenTerritorialStuExtract;
import ru.tandemservice.movestudent.utils.ExtEducationLevelsHighSchoolSelectModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;

import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 25.10.2013
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<TransferBetweenTerritorialStuExtract, Model> implements IDAO
{
    @Override
    protected TransferBetweenTerritorialStuExtract createNewInstance()
    {
        return new TransferBetweenTerritorialStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        final Student student = model.getExtract().getEntity();
        final EducationOrgUnit eou = student.getGroup().getEducationOrgUnit();

        model.setEduModel(MoveStudentDaoFacade.getCommonExtractUtil().createEduModel(model.isAddForm(), model.getExtract()));
        model.getEduModel().setFormativeOrgUnit(eou.getFormativeOrgUnit());
        model.getEduModel().setDevelopForm(eou.getDevelopForm());
        model.getEduModel().setDevelopCondition(eou.getDevelopCondition());
        model.getEduModel().setDevelopTech(eou.getDevelopTech());
        model.getEduModel().setDevelopPeriod(eou.getDevelopPeriod());
        model.getEduModel().setCourse(student.getCourse());
        if (model.isEditForm() && model.getEduModel().getGroup().getEducationOrgUnit().equals(model.getExtract().getEducationOrgUnitNew()))
        {
            model.getEduModel().setEducationLevelsHighSchool(null);
        }

        model.getEduModel().setTerritorialOrgUnitModel(new CommonSingleSelectModel()
        {
            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                OrgUnit orgUnit = (OrgUnit) value;
                return orgUnit.getTerritorialTitle();
            }

            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                EducationLevels educationLevels = EducationOrgUnitUtil.getParentLevel(eou.getEducationLevelHighSchool());

                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(Group.class, "g")
                        .predicate(DQLPredicateType.distinct)
                        .joinPath(DQLJoinType.inner, Group.educationOrgUnit().fromAlias("g"), "eou")
                        .column(property("eou", EducationOrgUnit.territorialOrgUnit().id()))
                        .where(or(
                                eq(property(EducationOrgUnit.educationLevelHighSchool().educationLevel().fromAlias("eou")), value(educationLevels)),
                                eq(property(EducationOrgUnit.educationLevelHighSchool().educationLevel().parentLevel().fromAlias("eou")), value(educationLevels))))
                        .where(eq(property(EducationOrgUnit.formativeOrgUnit().fromAlias("eou")), value(eou.getFormativeOrgUnit())))
                        .where(eq(property(EducationOrgUnit.developForm().fromAlias("eou")), value(eou.getDevelopForm())))
                        .where(eq(property(EducationOrgUnit.developCondition().fromAlias("eou")), value(eou.getDevelopCondition())))
                        .where(eq(property(EducationOrgUnit.developTech().fromAlias("eou")), value(eou.getDevelopTech())))
                        .where(eq(property(EducationOrgUnit.developPeriod().fromAlias("eou")), value(eou.getDevelopPeriod())))
                        .where(ne(property(EducationOrgUnit.territorialOrgUnit().fromAlias("eou")), value(eou.getTerritorialOrgUnit())));

                if (StringUtils.isNotEmpty(filter))
                    subBuilder.where(likeUpper(property(EducationOrgUnit.territorialOrgUnit().territorialFullTitle().fromAlias("eou")), value(CoreStringUtils.escapeLike(filter, true))));

                UniDaoFacade.getEducationLevelDao().applyUsedFilterForEducationOrgUnit(subBuilder, "eou");

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(OrgUnit.class, "ou")
                        .where(in(property("ou.id"), subBuilder.buildQuery()))
                        .order(property(OrgUnit.territorialFullTitle().fromAlias("ou")));

                if (o != null)
                    builder.where(eq(property("ou.id"), commonValue(o, PropertyType.LONG)));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        model.getEduModel().setGroupModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (model.getEduModel().getTerritorialOrgUnit() == null)
                    return new SimpleListResultBuilder<>(Collections.emptyList());

                EducationLevels educationLevels = EducationOrgUnitUtil.getParentLevel(eou.getEducationLevelHighSchool());

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, "g")
                        .column("g")
                        .joinPath(DQLJoinType.inner, Group.educationOrgUnit().fromAlias("g"), "eou")
                        .where(eq(property("g", Group.course()), value(student.getCourse())))
                        .where(eq(property("g", Group.archival()), value(Boolean.FALSE)))
                        .where(eq(property("eou", EducationOrgUnit.formativeOrgUnit()), value(eou.getFormativeOrgUnit())))
                        .where(eq(property("eou", EducationOrgUnit.territorialOrgUnit()), value(model.getEduModel().getTerritorialOrgUnit())))
                        .where(or(
                                eq(property("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel()), value(educationLevels)),
                                eq(property("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel().parentLevel()), value(educationLevels))))
                        .where(eq(property("eou", EducationOrgUnit.developForm()), value(eou.getDevelopForm())))
                        .where(eq(property("eou", EducationOrgUnit.developCondition()), value(eou.getDevelopCondition())))
                        .where(eq(property("eou", EducationOrgUnit.developTech()), value(eou.getDevelopTech())))
                        .where(eq(property("eou", EducationOrgUnit.developPeriod()), value(eou.getDevelopPeriod())))
                        .order(property("g", Group.title()));

                UniDaoFacade.getEducationLevelDao().applyUsedFilterForEducationOrgUnit(builder, "eou");

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property("g", Group.title()), value(CoreStringUtils.escapeLike(filter, true))));

                if (o != null)
                    builder.where(eq(property("g.id"), commonValue(o, PropertyType.LONG)));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        model.getEduModel().setEducationLevelsHighSchoolModel(new ExtEducationLevelsHighSchoolSelectModel(model.getEduModel(), model)
        {
            @Override
            protected boolean isNeedRequest()
            {
                return super.isNeedRequest() && model.getEduModel().getTerritorialOrgUnit() != null && model.getEduModel().getGroup() != null;
            }
        }.dependFromFUTS(true).showParentLevel(true));
    }

    @Override
    public void update(Model model)
    {
        if (model.getEduModel().getEducationLevelsHighSchool() == null)
        {
            model.getEduModel().setEducationLevelsHighSchool(model.getEduModel().getGroup().getEducationOrgUnit().getEducationLevelHighSchool());
        }
        MoveStudentDaoFacade.getCommonExtractUtil().update(model.getEduModel(), model.getExtract());
        if (!model.getExtract().isChangeEduPlan())
        {
            model.getExtract().setChangeEduPlanDeadlineDate(null);
        }
        super.update(model);
    }
}