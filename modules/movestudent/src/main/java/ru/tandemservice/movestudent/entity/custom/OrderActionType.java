/* $Id$ */
package ru.tandemservice.movestudent.entity.custom;

/**
 * Это персистентный ENUM! В базе хранится в виде названий элементов.
 * Если меняете название элементов - пишите миграцию!
 *
 * @author Nikolay Fedorovskih
 * @since 27.08.2013
 */
public enum OrderActionType
{
    DO_NOTHING("Не изменять данные студента"),
    DO_REVERT("Отменить действие выбранного приказа"),
    DO_CUSTOM_ACTION("Выбрать действия для актуализации данных студента");

    private String _title;
    public String getTitle()
    {
        return _title;
    }

    private OrderActionType(String title)
    {
        _title = title;
    }
}