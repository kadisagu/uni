/* $Id: extractDao.vm 24048 2012-09-06 05:21:10Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e40;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.GroupTransferExtStuListExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 30.01.2013
 */
public class GroupTransferExtStuListExtractDao extends UniBaseDao implements IExtractComponentDao<GroupTransferExtStuListExtract>
{
    @Override
    public void doCommit(GroupTransferExtStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        extract.setGroupOld(extract.getEntity().getGroup());
        extract.getEntity().setGroup(extract.getGroupNew());
    }

    @Override
    public void doRollback(GroupTransferExtStuListExtract extract, Map parameters)
    {
        extract.getEntity().setGroup(extract.getGroupOld());
    }
}