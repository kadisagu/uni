/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.component.student.MoveStudentPub;

import org.hibernate.Session;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.component.commons.ExtractListUtil;
import ru.tandemservice.movestudent.component.commons.IMoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.OrderCategory;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.movestudent.entity.custom.ModifyModularStuExtract;
import ru.tandemservice.movestudent.utils.ICommentableExtract;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.IPrintableExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 01.11.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    public static final String ORDER_PRINT_DISABLED_VIEW_PROPERTY = "orderPrintDisable";
    public static final String EXTRACT_COMMENT_EDIT_DISABLED = "commentEditDisabled";

    @Override
    public void prepare(final Model model)
    {
        ExtractListUtil.initExtractListModel(model.getIndividualOrdersModel(), "student_individualOrders", OrderCategory.INDIVIDUAL);
        ExtractListUtil.initExtractListModel(model.getProjectsModel(), "student_projects", OrderCategory.MODULAR);
        ExtractListUtil.initExtractListModel(model.getExtractsModel(), "student_extracts", null);
        ExtractListUtil.initExtractListModel(model.getListextractsModel(), "student_listextracts", OrderCategory.LIST);
        ExtractListUtil.initExtractListModel(model.getOtherOrdersModel(), "student_otherOrders", OrderCategory.OTHER);
        ExtractListUtil.initExtractListModel(model.getOrdersModel(), "student_orders", null);

        if (null == model.getMoveStudentPubSelectedTab())
        {
            model.setMoveStudentPubSelectedTab("moveStudentOrdersTab");
        }

        model.setOrderReasonListModel(new CommonMultiSelectModel(StudentOrderReasons.title().s())
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                List<StudentExtractType> typesList = model.getOrdersModel().getSettings().get(IMoveStudentMQBuilder.TYPE);

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(StudentReasonToTypeRel.class, "rel").column(property(StudentReasonToTypeRel.first().fromAlias("rel")))
                        .where(likeUpper(property(StudentReasonToTypeRel.first().title().fromAlias("rel")), value(CoreStringUtils.escapeLike(filter, true))));
                if (null != typesList && !typesList.isEmpty())
                    builder.where(in(property(StudentReasonToTypeRel.second().fromAlias("rel")), typesList));
                if (set != null) builder.where(in(property(StudentReasonToTypeRel.first().id().fromAlias("rel")), set));
                builder.order(property(StudentReasonToTypeRel.first().title().fromAlias("rel")));
                return new DQLListResultBuilder(builder, 50);
            }
        });
    }

    @Override
    public void prepareIndividualOrdersDataSource(Model model)
    {
        DQLSelectBuilder individualSubBuilder = new DQLSelectBuilder()
                .fromEntity(ModularStudentExtract.class, "e")
                .column(property(ModularStudentExtract.paragraph().order().id().fromAlias("e")))
                .where(eq(property(ModularStudentExtract.entity().id().fromAlias("e")), value(model.getStudentModel().getStudent().getId())))
                .group(property(ModularStudentExtract.paragraph().order().id().fromAlias("e")))
                .having(eq(DQLFunctions.count(ModularStudentExtract.id().fromAlias("e")), value(1)));

        DQLSelectBuilder individualBuilder = new DQLSelectBuilder()
                .fromEntity(ModularStudentExtract.class, "se").column(property("se"))
                .joinEntity("se", DQLJoinType.inner, ExtractCreationRule.class, "cr", eq(property(ModularStudentExtract.type().fromAlias("se")), property(ExtractCreationRule.studentExtractType().fromAlias("cr"))))
                .where(eq(property(ExtractCreationRule.individualOrder().fromAlias("cr")), value(Boolean.TRUE)))
                .where(in(property(ModularStudentExtract.paragraph().order().id().fromAlias("se")), individualSubBuilder.buildQuery()));


        final EntityOrder orderRule = model.getIndividualOrdersModel().getDataSource().getEntityOrder();
        new DQLOrderDescriptionRegistry(ModularStudentExtract.class, "se").applyOrder(individualBuilder,orderRule);
        MoveStudentUtils.createExtractPage(getSession(), model.getIndividualOrdersModel().getDataSource(), individualBuilder, "se");
    }

    @Override
    public void prepareProjectsDataSource(Model model)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ModularStudentExtract.class, "me")
                .where(isNull(property(ModularStudentExtract.paragraph().fromAlias("me"))))
                .where(eq(property("me", IAbstractExtract.L_TYPE+ "." + StudentExtractType.L_PARENT + "." + ICatalogItem.CATALOG_ITEM_CODE), value(StudentExtractTypeCodes.MODULAR_ORDER)))
                .where(eq(property("me", ModularStudentExtract.entity()), value(model.getStudentModel().getStudent())));

        final EntityOrder orderRule = model.getProjectsModel().getDataSource().getEntityOrder();

        new DQLOrderDescriptionRegistry(ModularStudentExtract.class, "me").applyOrder(builder, orderRule);
        MoveStudentUtils.createExtractPage(getSession(), model.getProjectsModel().getDataSource(), builder, "me");
    }

    @Override
    public void prepareExtractsDataSource(Model model)
    {
        DynamicListDataSource<ModularStudentExtract> dataSource = model.getExtractsModel().getDataSource();

        MQBuilder builder = new MQBuilder(ModularStudentExtract.ENTITY_CLASS, "extract");
        builder.add(MQExpression.eq("extract", IAbstractExtract.L_ENTITY, model.getStudentModel().getStudent()));
        builder.add(MQExpression.isNotNull("extract", IAbstractExtract.L_PARAGRAPH));

        MoveStudentUtils.createExtractPage(getSession(), new OrderDescriptionRegistry("extract"), dataSource, builder, "extract");

        // inject view properties
        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(dataSource))
        {
            wrapper.setViewProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY, false);
        }
    }

    @Override
    public void prepareListExtractsDataSource(Model model)
    {
        DynamicListDataSource<AbstractStudentExtract> dataSource = model.getListextractsModel().getDataSource();

        MQBuilder builder = new MQBuilder(ListStudentExtract.ENTITY_CLASS, "extract");
        builder.add(MQExpression.eq("extract", IAbstractExtract.L_ENTITY, model.getStudentModel().getStudent()));
        builder.add(MQExpression.isNotNull("extract", IAbstractExtract.L_PARAGRAPH));

        MoveStudentUtils.createExtractPage(getSession(), new OrderDescriptionRegistry("extract"), dataSource, builder, "extract");

        // get statictics for view properties
        Set<Long> disabledExtractTypes = MoveStudentDaoFacade.getMoveStudentDao().getDisabledFormPrintingExtracts(dataSource.getEntityList(), MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        // inject view properties
        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(dataSource))
        {
            wrapper.setViewProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY, disabledExtractTypes.contains(wrapper.getId()));
        }
    }

    @Override
    public void prepareOtherOrdersDataSource(Model model)
    {
        DynamicListDataSource<OtherStudentExtract> dataSource = model.getOtherOrdersModel().getDataSource();

        List<StudentExtractType> types = model.getOtherOrdersModel().getSettings().get(IMoveStudentMQBuilder.TYPE);

        DQLSelectBuilder otherBuilder = new DQLSelectBuilder()
                .fromEntity(OtherStudentExtract.class, "se").column(property("se"))
                .where(eq(property(OtherStudentExtract.entity().fromAlias("se")), value(model.getStudentModel().getStudent())))
                //.order(property(OtherStudentExtract.paragraph().order().commitDate().fromAlias("se")), OrderDirection.desc)
                //.order(property(OtherStudentExtract.paragraph().order().createDate().fromAlias("se")), OrderDirection.desc);
                .order(property(OtherStudentExtract.paragraph().order().fromAlias("se").s(), dataSource.getEntityOrder().getColumnName()), dataSource.getEntityOrder().getDirection());

        if (types != null && !types.isEmpty())
            otherBuilder.where(in(property(OtherStudentExtract.type().fromAlias("se")), types));

        //new DQLOrderDescriptionRegistry(OtherStudentExtract.class, "e").applyOrder(otherBuilder, model.getOtherOrdersModel().getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getOtherOrdersModel().getDataSource(), otherBuilder, getSession());

        // get statistics for view properties
        Set<Long> disabledExtractTypes = MoveStudentDaoFacade.getMoveStudentDao().getPrintFormAttachedOtherOrders(dataSource.getEntityList());

        // inject view properties
        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(dataSource))
        {
            wrapper.setViewProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY, disabledExtractTypes.contains(wrapper.getId()));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    @Zlo(value = "see code under comment 'entrant moving support'")
    public void prepareOrdersDataSource(Model model)
    {
        List<StudentExtractType> types = model.getOrdersModel().getSettings().get(IMoveStudentMQBuilder.TYPE);
        List<StudentOrderReasons> reasons = model.getOrdersModel().getSettings().get(IMoveStudentMQBuilder.ORDER_REASON);
        DynamicListDataSource<AbstractStudentExtract> dataSource = model.getOrdersModel().getDataSource();

        /*if (types != null && !types.isEmpty())
            otherBuilder.where(in(property(OtherStudentExtract.type().fromAlias("se")), value(types)));*/

        final Session session = getSession();

        MQBuilder builder = getOrdersDataBuilder(model);
        builder.getSelectAliasList().clear();
        builder.addSelect("e", new String[]{IEntity.P_ID});

        List<Long> idsList = builder.getResultList(session);
        List<AbstractStudentExtract> preResult = MoveStudentDaoFacade.getMoveStudentDao().getExtractListByIds(idsList, AbstractStudentExtract.class);

        List result = new ArrayList();

        if ((types != null && !types.isEmpty()) || (reasons != null && !reasons.isEmpty()))
        {
            for (AbstractStudentExtract extract : preResult)
            {
                StudentExtractType type = null;
                StudentOrderReasons reason = null;
                if (extract instanceof ModularStudentExtract)
                {
                    type = extract.getType();
                    reason = ((ModularStudentExtract) extract).getReason();
                }
                else if (extract instanceof ListStudentExtract)
                {
                    type = ((StudentListOrder) extract.getParagraph().getOrder()).getType();
                    reason = ((StudentListOrder) extract.getParagraph().getOrder()).getReason();
                }
                else if (extract instanceof OtherStudentExtract)
                {
                    type = extract.getType();
                    reason = ((OtherStudentExtract) extract).getReason();
                }

                if (types != null && !types.isEmpty() && !types.contains(type)) continue;
                if (reasons != null && !reasons.isEmpty() && !reasons.contains(reason)) continue;

                result.add(extract);
            }
        }
        else
        {
            result.addAll(preResult);

            // entrant moving support
            if (EntityRuntime.getMeta("ru.tandemservice.uniec.entity.orders.EnrollmentExtract") != null)
            {
                builder = new MQBuilder("ru.tandemservice.uniec.entity.orders.EnrollmentExtract", "e");
                builder.addJoin("e", IAbstractExtract.L_STATE, "state");
                builder.add(MQExpression.eq("state", ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED));
                builder.add(MQExpression.eq("e", "studentNew", model.getStudentModel().getStudent()));
                result.addAll(builder.getResultList(session));
            }
        }

        Collections.sort(result, new EntityComparator(dataSource.getEntityOrder()));
        UniBaseUtils.createPage(dataSource, result);

        List<AbstractStudentExtract> listStudentExtracts = new ArrayList<>();
        for (IEntity extract : dataSource.getEntityList())
        {
            if (extract instanceof ListStudentExtract)
                listStudentExtracts.add((ListStudentExtract) extract);
        }
        Set<Long> disabledExtracts = MoveStudentDaoFacade.getMoveStudentDao().getDisabledFormPrintingExtracts(listStudentExtracts, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(dataSource))
        {
            IAbstractExtract extract = (IAbstractExtract) wrapper.getEntity();

            // entrant moving support - грязный-грязный хак для приказов о распределении
            if (extract.getType() instanceof StudentExtractType)
            {
                StudentExtractType sType = (StudentExtractType) extract.getType();
                if (sType.getCode().startsWith("2.ent"))
                    wrapper.setViewProperty("extractTitle", sType.getParent().getTitle());
                else
                    wrapper.setViewProperty("extractTitle", sType.getTitle());
            }
            else
            {
                wrapper.setViewProperty("extractTitle", extract.getType().getTitle());
            }

            boolean disabledPrint = !(extract instanceof IPrintableExtract && ((IPrintableExtract) extract).canBePrinted());

            if (extract instanceof ICommentableExtract)
            {
                if (extract instanceof AbstractStudentExtract)
                    wrapper.setViewProperty(IDAO.P_COMMENT, ((AbstractStudentExtract) extract).getComment() == null ? "" : ((AbstractStudentExtract) extract).getComment());
                else wrapper.setViewProperty(IDAO.P_COMMENT, ((ICommentableExtract) extract).getGeneratedComment(false));
            }
            else
                wrapper.setViewProperty(IDAO.P_COMMENT, "");

            if (extract instanceof AbstractStudentExtract)
                wrapper.setViewProperty(EXTRACT_COMMENT_EDIT_DISABLED, false);
            else wrapper.setViewProperty(EXTRACT_COMMENT_EDIT_DISABLED, true);

            wrapper.setViewProperty(ORDER_PRINT_DISABLED_VIEW_PROPERTY, false);

            if (extract instanceof ModularStudentExtract)
            {
                IAbstractParagraph paragraph = extract.getParagraph();
                wrapper.setViewProperty(IDAO.P_PARAGRAPH_NUMBER, "");
                wrapper.setViewProperty(IDAO.P_EXTRACT_NUMBER, paragraph == null ? "" : paragraph.getNumber());
                if (extract instanceof ModifyModularStuExtract)
                    wrapper.setViewProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY, true);
                else
                    wrapper.setViewProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY, ((ModularStudentExtract) extract).isIndividual());
            }
            else
            {
                IAbstractParagraph paragraph = extract == null ? null : extract.getParagraph();
                wrapper.setViewProperty(IDAO.P_PARAGRAPH_NUMBER, paragraph == null ? "" : paragraph.getNumber());
                wrapper.setViewProperty(IDAO.P_EXTRACT_NUMBER, extract == null ? "" : extract.getNumber());

                if (extract instanceof ListStudentExtract)
                    wrapper.setViewProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY, disabledExtracts.contains(wrapper.getId()));
                else if (extract instanceof OtherStudentExtract)
                {
                    wrapper.setViewProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY, true);

                    // Печать прочего приказ возможна, только если к нему прикреплена печатная форма
                    wrapper.setViewProperty(ORDER_PRINT_DISABLED_VIEW_PROPERTY, !((OtherStudentExtract) extract).hasOrderPrintForm());
                }
                else
                    wrapper.setViewProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY, disabledPrint);
            }
        }
    }

    protected MQBuilder getOrdersDataBuilder(Model model)
    {
        return new MQBuilder(AbstractStudentExtract.ENTITY_CLASS, "e")
                .addJoin("e", IAbstractExtract.L_STATE, "state")
                .add(MQExpression.eq("state", ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED))
                .add(MQExpression.eq("e", IAbstractExtract.L_ENTITY, model.getStudentModel().getStudent()));
    }

    @Override
    public void deleteRow(IBusinessComponent component)
    {
        IAbstractExtract extract = getNotNull((Long) component.getListenerParameter());
        if (extract.getParagraph() == null)
        {
            getSession().delete(extract);
            return;
        }
        if (extract.getParagraph().getOrder() instanceof StudentModularOrder)
        {
            MoveDaoFacade.getMoveDao().deleteExtractWithParagraph(extract);
            return;
        }
        if (extract.getParagraph().getOrder() instanceof StudentListOrder)
        {
            MoveStudentDaoFacade.getMoveStudentDao().deleteListExtract(extract.getId());
            return;
        }
        throw new RuntimeException("Unknown extract type");
    }
}
