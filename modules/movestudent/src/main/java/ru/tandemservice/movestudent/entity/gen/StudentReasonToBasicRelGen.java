package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.StudentReasonToBasicRel;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь причины и основания (приказы по студентам)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentReasonToBasicRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.StudentReasonToBasicRel";
    public static final String ENTITY_NAME = "studentReasonToBasicRel";
    public static final int VERSION_HASH = -935344842;
    private static IEntityMeta ENTITY_META;

    public static final String L_FIRST = "first";
    public static final String L_SECOND = "second";
    public static final String P_PRIORITY = "priority";

    private StudentOrderReasons _first;     // Причина приказа по студенту
    private StudentOrderBasics _second;     // Основание приказа по студенту
    private int _priority;     // Порядковый номер основания в рамках причины

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Причина приказа по студенту. Свойство не может быть null.
     */
    @NotNull
    public StudentOrderReasons getFirst()
    {
        return _first;
    }

    /**
     * @param first Причина приказа по студенту. Свойство не может быть null.
     */
    public void setFirst(StudentOrderReasons first)
    {
        dirty(_first, first);
        _first = first;
    }

    /**
     * @return Основание приказа по студенту. Свойство не может быть null.
     */
    @NotNull
    public StudentOrderBasics getSecond()
    {
        return _second;
    }

    /**
     * @param second Основание приказа по студенту. Свойство не может быть null.
     */
    public void setSecond(StudentOrderBasics second)
    {
        dirty(_second, second);
        _second = second;
    }

    /**
     * @return Порядковый номер основания в рамках причины. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Порядковый номер основания в рамках причины. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentReasonToBasicRelGen)
        {
            setFirst(((StudentReasonToBasicRel)another).getFirst());
            setSecond(((StudentReasonToBasicRel)another).getSecond());
            setPriority(((StudentReasonToBasicRel)another).getPriority());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentReasonToBasicRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentReasonToBasicRel.class;
        }

        public T newInstance()
        {
            return (T) new StudentReasonToBasicRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "first":
                    return obj.getFirst();
                case "second":
                    return obj.getSecond();
                case "priority":
                    return obj.getPriority();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "first":
                    obj.setFirst((StudentOrderReasons) value);
                    return;
                case "second":
                    obj.setSecond((StudentOrderBasics) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "first":
                        return true;
                case "second":
                        return true;
                case "priority":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "first":
                    return true;
                case "second":
                    return true;
                case "priority":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "first":
                    return StudentOrderReasons.class;
                case "second":
                    return StudentOrderBasics.class;
                case "priority":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentReasonToBasicRel> _dslPath = new Path<StudentReasonToBasicRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentReasonToBasicRel");
    }
            

    /**
     * @return Причина приказа по студенту. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentReasonToBasicRel#getFirst()
     */
    public static StudentOrderReasons.Path<StudentOrderReasons> first()
    {
        return _dslPath.first();
    }

    /**
     * @return Основание приказа по студенту. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentReasonToBasicRel#getSecond()
     */
    public static StudentOrderBasics.Path<StudentOrderBasics> second()
    {
        return _dslPath.second();
    }

    /**
     * @return Порядковый номер основания в рамках причины. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentReasonToBasicRel#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    public static class Path<E extends StudentReasonToBasicRel> extends EntityPath<E>
    {
        private StudentOrderReasons.Path<StudentOrderReasons> _first;
        private StudentOrderBasics.Path<StudentOrderBasics> _second;
        private PropertyPath<Integer> _priority;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Причина приказа по студенту. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentReasonToBasicRel#getFirst()
     */
        public StudentOrderReasons.Path<StudentOrderReasons> first()
        {
            if(_first == null )
                _first = new StudentOrderReasons.Path<StudentOrderReasons>(L_FIRST, this);
            return _first;
        }

    /**
     * @return Основание приказа по студенту. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentReasonToBasicRel#getSecond()
     */
        public StudentOrderBasics.Path<StudentOrderBasics> second()
        {
            if(_second == null )
                _second = new StudentOrderBasics.Path<StudentOrderBasics>(L_SECOND, this);
            return _second;
        }

    /**
     * @return Порядковый номер основания в рамках причины. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentReasonToBasicRel#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(StudentReasonToBasicRelGen.P_PRIORITY, this);
            return _priority;
        }

        public Class getEntityClass()
        {
            return StudentReasonToBasicRel.class;
        }

        public String getEntityName()
        {
            return "studentReasonToBasicRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
