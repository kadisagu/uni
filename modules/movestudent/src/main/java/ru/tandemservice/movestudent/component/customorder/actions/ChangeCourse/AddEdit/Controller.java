/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeCourse.AddEdit;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditController;
import ru.tandemservice.movestudent.entity.custom.ChangeCourseAction;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class Controller extends BaseCustomActionAddEditController<ChangeCourseAction, IDAO, Model>
{
}