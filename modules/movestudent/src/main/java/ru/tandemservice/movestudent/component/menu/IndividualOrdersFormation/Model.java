/* $Id$ */
package ru.tandemservice.movestudent.component.menu.IndividualOrdersFormation;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.commons.ExtractListModel;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 31.10.2012
 */
public class Model extends ExtractListModel
{
    private List<OrderStates> _ordersStateList;
    private ISelectModel employeePostModel;

    private ISelectModel _educationLevelTypeListModel;
    private List<StructureEducationLevels> _educationLevelTypeList;

    public ISelectModel getEducationLevelTypeListModel()
    {
        return _educationLevelTypeListModel;
    }

    public void setEducationLevelTypeListModel(ISelectModel educationLevelTypeListModel)
    {
        _educationLevelTypeListModel = educationLevelTypeListModel;
    }



    public List<OrderStates> getOrdersStateList()
    {
        return _ordersStateList;
    }

    public void setOrdersStateList(List<OrderStates> ordersStateList)
    {
        _ordersStateList = ordersStateList;
    }

    public ISelectModel getEmployeePostModel()
    {
        return employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        this.employeePostModel = employeePostModel;
    }
}