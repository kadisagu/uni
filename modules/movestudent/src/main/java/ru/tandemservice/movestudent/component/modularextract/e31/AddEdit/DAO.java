/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e31.AddEdit;

import org.tandemframework.core.info.ErrorCollector;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.GiveDiplAppDuplicateStuExtract;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 18.05.2009
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<GiveDiplAppDuplicateStuExtract, Model>
{
    @Override
    protected GiveDiplAppDuplicateStuExtract createNewInstance()
    {
        return new GiveDiplAppDuplicateStuExtract();
    }
    
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        if(model.isAddForm() && model.getExtract().getYear() == 0)
            model.getExtract().setYear(UniBaseUtils.getCurrentYear());
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);
        if(model.getExtract().getYear() < 1000)
            errors.add("Год выпуска студента должен быть четырехзначным числом", "year");
    }
}