package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.movestudent.entity.gen.GraduateSuccessStuListExtractGen;

import java.util.Date;

/**
 * Проект приказа «О выпуске студентов с отличием»
 */
public class GraduateSuccessStuListExtract extends GraduateSuccessStuListExtractGen implements IExcludeExtract
{
    @Override
    public Date getExcludeDate()
    {
        return getGraduateDate();
    }
}