/* $Id$ */
package ru.tandemservice.movestudent.component.customorder;

import org.hibernate.SessionFactory;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditModel;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.IBaseCustomActionAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.movestudent.entity.custom.OrderActionType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.entity.ICustomOrderAction;

import java.lang.reflect.Constructor;
import java.util.Date;

/**
 * @author Nikolay Fedorovskih
 * @since 08.10.2013
 */
public class CustomOrderUtils
{
    public static final String NEED_UPLOAD_PRINT_DOC = "Необходимо загрузить печатный документ.";

    public static boolean isRevertExtract(IStudentCustomOrderExtract extract)
    {
        return extract != null &&
                (StudentExtractTypeCodes.CUSTOM_REVERT_LIST_EXTRACT.equals(extract.getType().getCode()) ||
                        StudentExtractTypeCodes.CUSTOM_REVERT_MODULAR_ORDER.equals(extract.getType().getCode()));
    }

    public static void checkRevertAvailable(IStudentCustomOrderExtract extract)
    {
        if (extract.getOrderActionType() == OrderActionType.DO_REVERT)
        {
            if (!isRevertExtract(extract))
                throw new IllegalStateException("Revert action can be done only by revert order.");

            Long lastCommitedExtractId = MoveStudentDaoFacade.getMoveStudentDao().getLastCommittedExtractId(extract.getEntity().getId());
            if (!extract.getModifiableExtract().getId().equals(lastCommitedExtractId))
            {
                throw new ApplicationException(
                        "Нельзя откатить изменения по приказу " + CustomOrderUtils.getOrderTitle(extract.getModifiableExtract()) +
                                " для студента " + extract.getModifiableExtract().getEntity().getPerson().getFullFio() + ", т.к. это не последний проведенный приказ для данного студента.");
            }

        }
    }


    private static final String CANT_ROLLBACK_MESSAGE = "Нельзя откатить приказ, т.к. есть изменяющий, дополняющий или отменяющий приказ или проект приказа, ссылающийся на данный.";

    public static void checkRollbackSimpleOrderAvailable(AbstractStudentOrder simpleOrder)
    {
        if (ICustomOrderDao.instance.get().hasModifyExtract(simpleOrder))
            throw new ApplicationException(CANT_ROLLBACK_MESSAGE);
    }

    public static void checkRollbackSimpleExtractAvailable(AbstractStudentExtract simpleExtract)
    {
        if (ICustomOrderDao.instance.get().hasModifyExtract(simpleExtract))
            throw new ApplicationException(CANT_ROLLBACK_MESSAGE);
    }

    @SuppressWarnings("unchecked")
    public static <T extends ICustomOrderAction, M extends BaseCustomActionAddEditModel<T>> IBaseCustomActionAddEditDAO<T, M> getActionDao(T action, SessionFactory sessionFactory)
    {
        try
        {
            String componentDaoClassName = MoveStudentDaoFacade.getMoveStudentDao().getCustomActionComponent(action) + ".AddEdit.DAO";
            Class<?> daoClass = Class.forName(componentDaoClassName);
            Constructor<?> constructor = daoClass.getConstructor();
            UniDao dao = (UniDao) constructor.newInstance();
            dao.setSessionFactory(sessionFactory);
            return (IBaseCustomActionAddEditDAO<T, M>) dao;
        }
        catch (Exception e)
        {
            throw new IllegalStateException(e);
        }
    }

    public static String getModifiableOrderFieldName(String extractTypeCode)
    {
        switch (extractTypeCode)
        {
            case StudentExtractTypeCodes.CUSTOM_REVERT_MODULAR_ORDER:
            case StudentExtractTypeCodes.CUSTOM_REVERT_LIST_ORDER:
            case StudentExtractTypeCodes.CUSTOM_REVERT_LIST_EXTRACT:
                return "Отменяемый приказ";
            case StudentExtractTypeCodes.CUSTOM_EDIT_MODULAR_ORDER:
            case StudentExtractTypeCodes.CUSTOM_EDIT_LIST_ORDER:
            case StudentExtractTypeCodes.CUSTOM_EDIT_LIST_EXTRACT:
                return "Изменяемый приказ";
            case StudentExtractTypeCodes.CUSTOM_ADD_MODULAR_ORDER:
            case StudentExtractTypeCodes.CUSTOM_ADD_LIST_ORDER:
            case StudentExtractTypeCodes.CUSTOM_ADD_LIST_EXTRACT:
                return "Дополняемый приказ";
            default:
                throw new IllegalStateException();
        }
    }

    public static String getDescriptionPreamble(String extractTypeCode)
    {
        switch (extractTypeCode)
        {
            case StudentExtractTypeCodes.CUSTOM_REVERT_MODULAR_ORDER:
            case StudentExtractTypeCodes.CUSTOM_REVERT_LIST_ORDER:
            case StudentExtractTypeCodes.CUSTOM_REVERT_LIST_EXTRACT:
                return "Отмена приказа";
            case StudentExtractTypeCodes.CUSTOM_EDIT_MODULAR_ORDER:
            case StudentExtractTypeCodes.CUSTOM_EDIT_LIST_ORDER:
            case StudentExtractTypeCodes.CUSTOM_EDIT_LIST_EXTRACT:
                return "В частичное изменение приказа";
            case StudentExtractTypeCodes.CUSTOM_ADD_MODULAR_ORDER:
            case StudentExtractTypeCodes.CUSTOM_ADD_LIST_ORDER:
            case StudentExtractTypeCodes.CUSTOM_ADD_LIST_EXTRACT:
                return "В дополнение к приказу";
            default:
                throw new IllegalStateException();
        }
    }

    public static String getOrderTitle(String orderNumber, Date commitDate, String extractTypeTitle)
    {
        if (commitDate != null && orderNumber != null)
            return "№" + orderNumber + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(commitDate) + ". «" + extractTypeTitle + "»";
        else if (commitDate != null)
            return "№ [без номера] от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(commitDate) + ". «" + extractTypeTitle + "»";
        return "№" + orderNumber + ". «" + extractTypeTitle + "»";
    }

    public static String getOrderTitle(AbstractStudentExtract extract)
    {
        return getOrderTitle(extract.getParagraph().getOrder().getNumber(), extract.getParagraph().getOrder().getCommitDate(), extract.getType().getTitle());
    }

    public static String getOrderTitle(StudentListOrder order)
    {
        return getOrderTitle(order.getNumber(), order.getCommitDate(), order.getType().getTitle());
    }
}