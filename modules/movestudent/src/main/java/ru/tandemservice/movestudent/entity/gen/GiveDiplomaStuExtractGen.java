package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.GiveDiplomaStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О выдаче диплома и присвоении квалификации
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GiveDiplomaStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.GiveDiplomaStuExtract";
    public static final String ENTITY_NAME = "giveDiplomaStuExtract";
    public static final int VERSION_HASH = 1470084046;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";
    public static final String P_WITH_EXCELLENT = "withExcellent";
    public static final String P_PRINT_DIPLOMA_QUALIFICATION = "printDiplomaQualification";
    public static final String P_FINISHED_YEAR = "finishedYear";

    private StudentStatus _studentStatusOld;     // Состояние студента на момент проведения приказа
    private boolean _withExcellent;     // Диплом с отличием
    private boolean _printDiplomaQualification;     // Присвоить специальное звание
    private Integer _finishedYear;     // Год окончания на момент проведения приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Состояние студента на момент проведения приказа. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Состояние студента на момент проведения приказа. Свойство не может быть null.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    /**
     * @return Диплом с отличием. Свойство не может быть null.
     */
    @NotNull
    public boolean isWithExcellent()
    {
        return _withExcellent;
    }

    /**
     * @param withExcellent Диплом с отличием. Свойство не может быть null.
     */
    public void setWithExcellent(boolean withExcellent)
    {
        dirty(_withExcellent, withExcellent);
        _withExcellent = withExcellent;
    }

    /**
     * @return Присвоить специальное звание. Свойство не может быть null.
     */
    @NotNull
    public boolean isPrintDiplomaQualification()
    {
        return _printDiplomaQualification;
    }

    /**
     * @param printDiplomaQualification Присвоить специальное звание. Свойство не может быть null.
     */
    public void setPrintDiplomaQualification(boolean printDiplomaQualification)
    {
        dirty(_printDiplomaQualification, printDiplomaQualification);
        _printDiplomaQualification = printDiplomaQualification;
    }

    /**
     * @return Год окончания на момент проведения приказа.
     */
    public Integer getFinishedYear()
    {
        return _finishedYear;
    }

    /**
     * @param finishedYear Год окончания на момент проведения приказа.
     */
    public void setFinishedYear(Integer finishedYear)
    {
        dirty(_finishedYear, finishedYear);
        _finishedYear = finishedYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof GiveDiplomaStuExtractGen)
        {
            setStudentStatusOld(((GiveDiplomaStuExtract)another).getStudentStatusOld());
            setWithExcellent(((GiveDiplomaStuExtract)another).isWithExcellent());
            setPrintDiplomaQualification(((GiveDiplomaStuExtract)another).isPrintDiplomaQualification());
            setFinishedYear(((GiveDiplomaStuExtract)another).getFinishedYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GiveDiplomaStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GiveDiplomaStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new GiveDiplomaStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
                case "withExcellent":
                    return obj.isWithExcellent();
                case "printDiplomaQualification":
                    return obj.isPrintDiplomaQualification();
                case "finishedYear":
                    return obj.getFinishedYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
                case "withExcellent":
                    obj.setWithExcellent((Boolean) value);
                    return;
                case "printDiplomaQualification":
                    obj.setPrintDiplomaQualification((Boolean) value);
                    return;
                case "finishedYear":
                    obj.setFinishedYear((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                        return true;
                case "withExcellent":
                        return true;
                case "printDiplomaQualification":
                        return true;
                case "finishedYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                    return true;
                case "withExcellent":
                    return true;
                case "printDiplomaQualification":
                    return true;
                case "finishedYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                    return StudentStatus.class;
                case "withExcellent":
                    return Boolean.class;
                case "printDiplomaQualification":
                    return Boolean.class;
                case "finishedYear":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GiveDiplomaStuExtract> _dslPath = new Path<GiveDiplomaStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GiveDiplomaStuExtract");
    }
            

    /**
     * @return Состояние студента на момент проведения приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GiveDiplomaStuExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    /**
     * @return Диплом с отличием. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GiveDiplomaStuExtract#isWithExcellent()
     */
    public static PropertyPath<Boolean> withExcellent()
    {
        return _dslPath.withExcellent();
    }

    /**
     * @return Присвоить специальное звание. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GiveDiplomaStuExtract#isPrintDiplomaQualification()
     */
    public static PropertyPath<Boolean> printDiplomaQualification()
    {
        return _dslPath.printDiplomaQualification();
    }

    /**
     * @return Год окончания на момент проведения приказа.
     * @see ru.tandemservice.movestudent.entity.GiveDiplomaStuExtract#getFinishedYear()
     */
    public static PropertyPath<Integer> finishedYear()
    {
        return _dslPath.finishedYear();
    }

    public static class Path<E extends GiveDiplomaStuExtract> extends ModularStudentExtract.Path<E>
    {
        private StudentStatus.Path<StudentStatus> _studentStatusOld;
        private PropertyPath<Boolean> _withExcellent;
        private PropertyPath<Boolean> _printDiplomaQualification;
        private PropertyPath<Integer> _finishedYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Состояние студента на момент проведения приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GiveDiplomaStuExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

    /**
     * @return Диплом с отличием. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GiveDiplomaStuExtract#isWithExcellent()
     */
        public PropertyPath<Boolean> withExcellent()
        {
            if(_withExcellent == null )
                _withExcellent = new PropertyPath<Boolean>(GiveDiplomaStuExtractGen.P_WITH_EXCELLENT, this);
            return _withExcellent;
        }

    /**
     * @return Присвоить специальное звание. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GiveDiplomaStuExtract#isPrintDiplomaQualification()
     */
        public PropertyPath<Boolean> printDiplomaQualification()
        {
            if(_printDiplomaQualification == null )
                _printDiplomaQualification = new PropertyPath<Boolean>(GiveDiplomaStuExtractGen.P_PRINT_DIPLOMA_QUALIFICATION, this);
            return _printDiplomaQualification;
        }

    /**
     * @return Год окончания на момент проведения приказа.
     * @see ru.tandemservice.movestudent.entity.GiveDiplomaStuExtract#getFinishedYear()
     */
        public PropertyPath<Integer> finishedYear()
        {
            if(_finishedYear == null )
                _finishedYear = new PropertyPath<Integer>(GiveDiplomaStuExtractGen.P_FINISHED_YEAR, this);
            return _finishedYear;
        }

        public Class getEntityClass()
        {
            return GiveDiplomaStuExtract.class;
        }

        public String getEntityName()
        {
            return "giveDiplomaStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
