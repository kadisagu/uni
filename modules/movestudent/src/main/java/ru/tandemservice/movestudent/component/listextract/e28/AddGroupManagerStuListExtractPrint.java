/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e28;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.AddGroupManagerStuListExtract;
import ru.tandemservice.movestudent.entity.GroupToExtractRelation;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 14.09.2012
 */
public class AddGroupManagerStuListExtractPrint implements IPrintFormCreator<AddGroupManagerStuListExtract>, IListParagraphPrintFormCreator<AddGroupManagerStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, AddGroupManagerStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);

        Student student = extract.getEntity();
        Group group = student.getGroup();
        EducationOrgUnit educationOrgUnit = group.getEducationOrgUnit();

        CommonExtractPrint.modifyEducationStr(modifier, educationOrgUnit.getEducationLevelHighSchool().getEducationLevel());

        CommonExtractPrint.initOrgUnit(modifier, educationOrgUnit, "formativeOrgUnitStr", "");

        EducationYear educationYear = extract.getEducationYear();
        String educationYearStr = educationYear != null ? educationYear.getTitle() : "20__/20__";

        modifier.put("educationYear", educationYearStr);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GroupToExtractRelation.class, "ge");
        builder.column(DQLExpressions.property("ge", GroupToExtractRelation.group()));
        builder.where(DQLExpressions.eq(DQLExpressions.property("ge", GroupToExtractRelation.extract().id()), DQLExpressions.value(extract.getId())));
        List<Group> selectedGroups = ISharedBaseDao.instance.get().getList(builder);

        if (selectedGroups.isEmpty()) throw new ApplicationException("Невозможно напечатать выписку, поскольку не указана группа, в которую назначается старостой " + student.getFio() + ".");
        List<String> selectedGroupsTitles = Lists.newArrayList();
        for(Group selectedGroup : selectedGroups)
        {
            selectedGroupsTitles.add(selectedGroup.getTitle());
        }
        modifier.put("targetGroups", StringUtils.join(selectedGroupsTitles, ", "));

        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, educationOrgUnit, CommonListOrderPrint.getEducationBaseText(group), "fefuShortFastExtendedOptionalText");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, AddGroupManagerStuListExtract firstExtract)
    {
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, AddGroupManagerStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, AddGroupManagerStuListExtract firstExtract)
    {
    }
}