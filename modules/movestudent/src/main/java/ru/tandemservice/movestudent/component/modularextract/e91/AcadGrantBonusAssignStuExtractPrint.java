/* $Id: extractPrint.vm 23792 2012-08-10 10:47:17Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e91;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuExtract;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 11.01.2013
 */
public class AcadGrantBonusAssignStuExtractPrint implements IPrintFormCreator<AcadGrantBonusAssignStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, AcadGrantBonusAssignStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("grantBonusSize", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGrantBonusSize()));
        modifier.put("beginDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginDate()));
        modifier.put("endDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndDate()));


        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}