/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e84.Pub;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.movestudent.entity.RePassDiscComissStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToComissionMember;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 31.08.2012
 */
public class DAO extends ModularStudentExtractPubDAO<RePassDiscComissStuExtract, Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        List<StuExtractToComissionMember> disciplineList = new DQLSelectBuilder()
                .fromEntity(StuExtractToComissionMember.class, "d").column("d")
                .where(eq(property(StuExtractToComissionMember.extract().id().fromAlias("d")), value(model.getExtractId())))
                .order(property(StuExtractToComissionMember.priority().fromAlias("d")))
                .createStatement(getSession()).list();
        model.getDataSource().setCountRow(disciplineList.size());
        UniBaseUtils.createPage(model.getDataSource(), disciplineList);
    }
}