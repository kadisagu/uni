/* $Id: Model.java.vm 7834 2009-05-06 09:39:03Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e20.ParagraphAddEdit;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.SetReviewerStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 07.02.2011
 */
public class Model extends AbstractListParagraphAddEditModel<SetReviewerStuListExtract> implements IGroupModel
{
    private Course _course;
    private ISelectModel _groupListModel;
    private Group _group;
    private DevelopForm _developForm;
    private ISelectModel _developFormList;
    private String _reviewerName;
    private String _reviewerDegree;

    public String getCurrentNameId()
    {
        IEntity currentEntity = getDataSource().getCurrentEntity();
        return "name_id_" + currentEntity.getId();
    }

    public String getCurrentDegreeId()
    {
        IEntity currentEntity = getDataSource().getCurrentEntity();
        return "degree_id_" + currentEntity.getId();
    }

    public String getReviewerName() {
        return _reviewerName;
    }

    public void setReviewerName(String _reviewerName) {
        this._reviewerName = _reviewerName;
    }

    public String getReviewerDegree() {
        return _reviewerDegree;
    }

    public void setReviewerDegree(String _reviewerDegree) {
        this._reviewerDegree = _reviewerDegree;
    }

    public Course getCourse() {
        return _course;
    }

    public void setCourse(Course _course) {
        this._course = _course;
    }

    public ISelectModel getGroupListModel() {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel _groupListModel) {
        this._groupListModel = _groupListModel;
    }

    public Group getGroup() {
        return _group;
    }

    public void setGroup(Group _group) {
        this._group = _group;
    }

    public DevelopForm getDevelopForm() {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm _developForm) {
        this._developForm = _developForm;
    }

    public ISelectModel getDevelopFormList() { return _developFormList; }
    public void setDevelopFormList(ISelectModel _developFormList) { this._developFormList = _developFormList; }
}
