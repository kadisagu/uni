/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e47.Pub;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.movestudent.entity.AddGroupManagerStuExtract;

import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 10.08.2012
 */
public class Model extends ModularStudentExtractPubModel<AddGroupManagerStuExtract>
{
    private List<String> _groupsTitles;
    private List<String> _prevGroupManagers;


    public String getGroupsTitlesLabel()
    {
        if(_groupsTitles.size() > 1)
            return "Группы";
        else
            return "Группа";
    }

    public String getPrevGroupManagersLabel()
    {
        if(_prevGroupManagers.size() > 1)
            return "ФИО старост на момент создания приказа";
        else
            return "ФИО старосты на момент создания приказа";
    }

    public String getGroupsTitles()
    {
        return StringUtils.join(_groupsTitles, ", ");
    }

    public void setGroupsTitles(List<String> groupsTitles)
    {
        _groupsTitles = groupsTitles;
    }

    public String getPrevGroupManagers()
    {
        return StringUtils.join(_prevGroupManagers, "<br/>");
    }

    public void setPrevGroupManagers(List<String> prevGroupManagers)
    {
        _prevGroupManagers = prevGroupManagers;
    }
}
