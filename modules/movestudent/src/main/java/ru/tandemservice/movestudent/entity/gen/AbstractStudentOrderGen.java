package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.IAbstractDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приказ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AbstractStudentOrderGen extends EntityBase
 implements IAbstractDocument{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.AbstractStudentOrder";
    public static final String ENTITY_NAME = "abstractStudentOrder";
    public static final int VERSION_HASH = -2124053484;
    private static IEntityMeta ENTITY_META;

    public static final String P_CREATE_DATE = "createDate";
    public static final String P_COMMIT_DATE = "commitDate";
    public static final String P_COMMIT_DATE_SYSTEM = "commitDateSystem";
    public static final String P_NUMBER = "number";
    public static final String P_TEXT_PARAGRAPH = "textParagraph";
    public static final String P_RESPONSIBLE = "responsible";
    public static final String P_EXECUTOR = "executor";
    public static final String L_RESPONSIBLE_EMPL = "responsibleEmpl";
    public static final String L_EXECUTOR_EMPL = "executorEmpl";
    public static final String L_STATE = "state";
    public static final String L_ORG_UNIT = "orgUnit";

    private Date _createDate;     // Дата формирования
    private Date _commitDate;     // Дата вступления в силу
    private Date _commitDateSystem;     // Дата проведения
    private String _number;     // Номер
    private String _textParagraph;     // Текстовый параграф
    private String _responsible;     // Ответственный за исполнение
    private String _executor;     // Исполнитель
    private EmployeePost _responsibleEmpl;     // Ответственный за исполнение (должность)
    private EmployeePost _executorEmpl;     // Исполнитель (должность)
    private OrderStates _state;     // Состояние приказа
    private OrgUnit _orgUnit;     // Подразделение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Дата вступления в силу.
     */
    public Date getCommitDate()
    {
        return _commitDate;
    }

    /**
     * @param commitDate Дата вступления в силу.
     */
    public void setCommitDate(Date commitDate)
    {
        dirty(_commitDate, commitDate);
        _commitDate = commitDate;
    }

    /**
     * @return Дата проведения.
     */
    public Date getCommitDateSystem()
    {
        return _commitDateSystem;
    }

    /**
     * @param commitDateSystem Дата проведения.
     */
    public void setCommitDateSystem(Date commitDateSystem)
    {
        dirty(_commitDateSystem, commitDateSystem);
        _commitDateSystem = commitDateSystem;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Текстовый параграф.
     */
    public String getTextParagraph()
    {
        return _textParagraph;
    }

    /**
     * @param textParagraph Текстовый параграф.
     */
    public void setTextParagraph(String textParagraph)
    {
        dirty(_textParagraph, textParagraph);
        _textParagraph = textParagraph;
    }

    /**
     * @return Ответственный за исполнение.
     */
    @Length(max=255)
    public String getResponsible()
    {
        return _responsible;
    }

    /**
     * @param responsible Ответственный за исполнение.
     */
    public void setResponsible(String responsible)
    {
        dirty(_responsible, responsible);
        _responsible = responsible;
    }

    /**
     * @return Исполнитель.
     */
    @Length(max=255)
    public String getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Исполнитель.
     */
    public void setExecutor(String executor)
    {
        dirty(_executor, executor);
        _executor = executor;
    }

    /**
     * @return Ответственный за исполнение (должность).
     */
    public EmployeePost getResponsibleEmpl()
    {
        return _responsibleEmpl;
    }

    /**
     * @param responsibleEmpl Ответственный за исполнение (должность).
     */
    public void setResponsibleEmpl(EmployeePost responsibleEmpl)
    {
        dirty(_responsibleEmpl, responsibleEmpl);
        _responsibleEmpl = responsibleEmpl;
    }

    /**
     * @return Исполнитель (должность).
     */
    public EmployeePost getExecutorEmpl()
    {
        return _executorEmpl;
    }

    /**
     * @param executorEmpl Исполнитель (должность).
     */
    public void setExecutorEmpl(EmployeePost executorEmpl)
    {
        dirty(_executorEmpl, executorEmpl);
        _executorEmpl = executorEmpl;
    }

    /**
     * @return Состояние приказа. Свойство не может быть null.
     */
    @NotNull
    public OrderStates getState()
    {
        return _state;
    }

    /**
     * @param state Состояние приказа. Свойство не может быть null.
     */
    public void setState(OrderStates state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof AbstractStudentOrderGen)
        {
            setCreateDate(((AbstractStudentOrder)another).getCreateDate());
            setCommitDate(((AbstractStudentOrder)another).getCommitDate());
            setCommitDateSystem(((AbstractStudentOrder)another).getCommitDateSystem());
            setNumber(((AbstractStudentOrder)another).getNumber());
            setTextParagraph(((AbstractStudentOrder)another).getTextParagraph());
            setResponsible(((AbstractStudentOrder)another).getResponsible());
            setExecutor(((AbstractStudentOrder)another).getExecutor());
            setResponsibleEmpl(((AbstractStudentOrder)another).getResponsibleEmpl());
            setExecutorEmpl(((AbstractStudentOrder)another).getExecutorEmpl());
            setState(((AbstractStudentOrder)another).getState());
            setOrgUnit(((AbstractStudentOrder)another).getOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AbstractStudentOrderGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AbstractStudentOrder.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("AbstractStudentOrder is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "createDate":
                    return obj.getCreateDate();
                case "commitDate":
                    return obj.getCommitDate();
                case "commitDateSystem":
                    return obj.getCommitDateSystem();
                case "number":
                    return obj.getNumber();
                case "textParagraph":
                    return obj.getTextParagraph();
                case "responsible":
                    return obj.getResponsible();
                case "executor":
                    return obj.getExecutor();
                case "responsibleEmpl":
                    return obj.getResponsibleEmpl();
                case "executorEmpl":
                    return obj.getExecutorEmpl();
                case "state":
                    return obj.getState();
                case "orgUnit":
                    return obj.getOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "commitDate":
                    obj.setCommitDate((Date) value);
                    return;
                case "commitDateSystem":
                    obj.setCommitDateSystem((Date) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "textParagraph":
                    obj.setTextParagraph((String) value);
                    return;
                case "responsible":
                    obj.setResponsible((String) value);
                    return;
                case "executor":
                    obj.setExecutor((String) value);
                    return;
                case "responsibleEmpl":
                    obj.setResponsibleEmpl((EmployeePost) value);
                    return;
                case "executorEmpl":
                    obj.setExecutorEmpl((EmployeePost) value);
                    return;
                case "state":
                    obj.setState((OrderStates) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "createDate":
                        return true;
                case "commitDate":
                        return true;
                case "commitDateSystem":
                        return true;
                case "number":
                        return true;
                case "textParagraph":
                        return true;
                case "responsible":
                        return true;
                case "executor":
                        return true;
                case "responsibleEmpl":
                        return true;
                case "executorEmpl":
                        return true;
                case "state":
                        return true;
                case "orgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "createDate":
                    return true;
                case "commitDate":
                    return true;
                case "commitDateSystem":
                    return true;
                case "number":
                    return true;
                case "textParagraph":
                    return true;
                case "responsible":
                    return true;
                case "executor":
                    return true;
                case "responsibleEmpl":
                    return true;
                case "executorEmpl":
                    return true;
                case "state":
                    return true;
                case "orgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "createDate":
                    return Date.class;
                case "commitDate":
                    return Date.class;
                case "commitDateSystem":
                    return Date.class;
                case "number":
                    return String.class;
                case "textParagraph":
                    return String.class;
                case "responsible":
                    return String.class;
                case "executor":
                    return String.class;
                case "responsibleEmpl":
                    return EmployeePost.class;
                case "executorEmpl":
                    return EmployeePost.class;
                case "state":
                    return OrderStates.class;
                case "orgUnit":
                    return OrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AbstractStudentOrder> _dslPath = new Path<AbstractStudentOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AbstractStudentOrder");
    }
            

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Дата вступления в силу.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getCommitDate()
     */
    public static PropertyPath<Date> commitDate()
    {
        return _dslPath.commitDate();
    }

    /**
     * @return Дата проведения.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getCommitDateSystem()
     */
    public static PropertyPath<Date> commitDateSystem()
    {
        return _dslPath.commitDateSystem();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Текстовый параграф.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getTextParagraph()
     */
    public static PropertyPath<String> textParagraph()
    {
        return _dslPath.textParagraph();
    }

    /**
     * @return Ответственный за исполнение.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getResponsible()
     */
    public static PropertyPath<String> responsible()
    {
        return _dslPath.responsible();
    }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getExecutor()
     */
    public static PropertyPath<String> executor()
    {
        return _dslPath.executor();
    }

    /**
     * @return Ответственный за исполнение (должность).
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getResponsibleEmpl()
     */
    public static EmployeePost.Path<EmployeePost> responsibleEmpl()
    {
        return _dslPath.responsibleEmpl();
    }

    /**
     * @return Исполнитель (должность).
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getExecutorEmpl()
     */
    public static EmployeePost.Path<EmployeePost> executorEmpl()
    {
        return _dslPath.executorEmpl();
    }

    /**
     * @return Состояние приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getState()
     */
    public static OrderStates.Path<OrderStates> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    public static class Path<E extends AbstractStudentOrder> extends EntityPath<E>
    {
        private PropertyPath<Date> _createDate;
        private PropertyPath<Date> _commitDate;
        private PropertyPath<Date> _commitDateSystem;
        private PropertyPath<String> _number;
        private PropertyPath<String> _textParagraph;
        private PropertyPath<String> _responsible;
        private PropertyPath<String> _executor;
        private EmployeePost.Path<EmployeePost> _responsibleEmpl;
        private EmployeePost.Path<EmployeePost> _executorEmpl;
        private OrderStates.Path<OrderStates> _state;
        private OrgUnit.Path<OrgUnit> _orgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(AbstractStudentOrderGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Дата вступления в силу.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getCommitDate()
     */
        public PropertyPath<Date> commitDate()
        {
            if(_commitDate == null )
                _commitDate = new PropertyPath<Date>(AbstractStudentOrderGen.P_COMMIT_DATE, this);
            return _commitDate;
        }

    /**
     * @return Дата проведения.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getCommitDateSystem()
     */
        public PropertyPath<Date> commitDateSystem()
        {
            if(_commitDateSystem == null )
                _commitDateSystem = new PropertyPath<Date>(AbstractStudentOrderGen.P_COMMIT_DATE_SYSTEM, this);
            return _commitDateSystem;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(AbstractStudentOrderGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Текстовый параграф.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getTextParagraph()
     */
        public PropertyPath<String> textParagraph()
        {
            if(_textParagraph == null )
                _textParagraph = new PropertyPath<String>(AbstractStudentOrderGen.P_TEXT_PARAGRAPH, this);
            return _textParagraph;
        }

    /**
     * @return Ответственный за исполнение.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getResponsible()
     */
        public PropertyPath<String> responsible()
        {
            if(_responsible == null )
                _responsible = new PropertyPath<String>(AbstractStudentOrderGen.P_RESPONSIBLE, this);
            return _responsible;
        }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getExecutor()
     */
        public PropertyPath<String> executor()
        {
            if(_executor == null )
                _executor = new PropertyPath<String>(AbstractStudentOrderGen.P_EXECUTOR, this);
            return _executor;
        }

    /**
     * @return Ответственный за исполнение (должность).
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getResponsibleEmpl()
     */
        public EmployeePost.Path<EmployeePost> responsibleEmpl()
        {
            if(_responsibleEmpl == null )
                _responsibleEmpl = new EmployeePost.Path<EmployeePost>(L_RESPONSIBLE_EMPL, this);
            return _responsibleEmpl;
        }

    /**
     * @return Исполнитель (должность).
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getExecutorEmpl()
     */
        public EmployeePost.Path<EmployeePost> executorEmpl()
        {
            if(_executorEmpl == null )
                _executorEmpl = new EmployeePost.Path<EmployeePost>(L_EXECUTOR_EMPL, this);
            return _executorEmpl;
        }

    /**
     * @return Состояние приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getState()
     */
        public OrderStates.Path<OrderStates> state()
        {
            if(_state == null )
                _state = new OrderStates.Path<OrderStates>(L_STATE, this);
            return _state;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.movestudent.entity.AbstractStudentOrder#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

        public Class getEntityClass()
        {
            return AbstractStudentOrder.class;
        }

        public String getEntityName()
        {
            return "abstractStudentOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
