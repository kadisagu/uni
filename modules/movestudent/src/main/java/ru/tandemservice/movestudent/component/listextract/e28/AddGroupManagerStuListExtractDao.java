/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e28;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AddGroupManagerStuListExtract;
import ru.tandemservice.movestudent.entity.GroupToExtractRelation;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.GroupCaptainStudent;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.List;
import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 14.09.2012
 */
public class AddGroupManagerStuListExtractDao extends UniBaseDao implements IExtractComponentDao<AddGroupManagerStuListExtract>
{
    @Override
    public void doCommit(AddGroupManagerStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        Student student = extract.getEntity();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GroupToExtractRelation.class, "ge");
        builder.where(DQLExpressions.eq(DQLExpressions.property("ge", GroupToExtractRelation.extract().id()), DQLExpressions.value(extract.getId())));
        builder.column(DQLExpressions.property(GroupCaptainStudent.group().fromAlias("ge"))).distinct();

        List<Group> groupList = builder.createStatement(getSession()).list();

        if(groupList.isEmpty())
            throw new ApplicationException("Невозможно провести приказ, поскольку не указаны группы, в которую назначается старостой " + student.getFio() + ".");

        for(Group group : groupList)
        {
            UniDaoFacade.getGroupDao().addCaptainStudent(group, student);
        }

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            extract.setPrevOrderDate(orderData.getPublicOrderOrderDate());
            extract.setPrevOrderNumber(orderData.getPublicOrderOrderNumber());
        }
        orderData.setPublicOrderOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setPublicOrderOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);
    }

    @Override
    public void doRollback(AddGroupManagerStuListExtract extract, Map parameters)
    {
        Student student = extract.getEntity();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GroupToExtractRelation.class, "ge");
        builder.where(DQLExpressions.eq(DQLExpressions.property("ge", GroupToExtractRelation.extract().id()), DQLExpressions.value(extract.getId())));
        List<GroupToExtractRelation> groupToExtractRelations = builder.createStatement(getSession()).list();

        for(GroupToExtractRelation relation : groupToExtractRelations)
        {
            UniDaoFacade.getGroupDao().deleteCaptainStudent(relation.getGroup(), student);
        }

        for(GroupToExtractRelation relation : groupToExtractRelations)
        {
            if (null != relation.getPrevGroupManager())
            {
                UniDaoFacade.getGroupDao().addCaptainStudent(relation.getGroup(), relation.getPrevGroupManager());
            }
        }

        // возвращаем предыдущие номер и дату приказа
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setPublicOrderOrderDate(extract.getPrevOrderDate());
        orderData.setPublicOrderOrderNumber(extract.getPrevOrderNumber());
        getSession().saveOrUpdate(orderData);
    }
}