/* $Id$ */
package ru.tandemservice.movestudent.component.order.list.StudentListExtractPrint;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.utils.ooffice.OpenOfficeServiceUtil;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * @author dseleznev
 * @since 27.01.2012
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (null != model.getPrintPdf() && model.getPrintPdf())
            OpenOfficeServiceUtil.checkPdfConverterAvailable();

        getDao().prepare(model);

        try
        {
            BusinessComponentUtils.downloadDocument(buildDocumentRenderer(component), true);
        }
        catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public IDocumentRenderer buildDocumentRenderer(IBusinessComponent component)
    {
        Model model = getModel(component);

        deactivate(component);

        if (UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED.equals(model.getExtract().getState().getCode()))
        {
            // выписка уже проведена и печатная форма сохранена
            if (null != model.getPrintPdf() && model.getPrintPdf())
                return UniReportUtils.createConvertingToPdfRenderer("Extract.pdf", model.getData());
            else
                return new CommonBaseRenderer().rtf().document(model.getData()).fileName("Extract.rtf");
        }
        else
        {
            String printName = EntityRuntime.getMeta(model.getExtractId()).getName() + "_extractPrint";
            IPrintFormCreator<ListStudentExtract> componentPrint = CommonExtractPrint.getPrintFormCreator(printName);
            assert componentPrint != null;
            RtfDocument document = componentPrint.createPrintForm(model.getData(), model.getExtract());
            if (null != model.getPrintPdf() && model.getPrintPdf())
                return UniReportUtils.createConvertingToPdfRenderer("Extract.pdf", document);
            else
                return new CommonBaseRenderer().rtf().document(document).fileName("Extract.rtf");
        }
    }
}