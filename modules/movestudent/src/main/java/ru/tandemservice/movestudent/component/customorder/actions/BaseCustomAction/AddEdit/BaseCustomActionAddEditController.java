/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.unimove.entity.ICustomOrderAction;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public abstract class BaseCustomActionAddEditController<T extends ICustomOrderAction, IDAO extends IBaseCustomActionAddEditDAO<T, Model>, Model extends BaseCustomActionAddEditModel<T>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
    }
}