/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e36.utils;

import org.tandemframework.shared.person.base.entity.Person;

/**
 * @author Dmitry Seleznev
 * @since 22.11.2012
 */
public class GrantRiseExtractWrapper implements Comparable<GrantRiseExtractWrapper>
{
    private final Person _person;
    private final Double _grantRiseAmount;

    public GrantRiseExtractWrapper(Person person, Double grantRiseAmount)
    {
        _person = person;
        _grantRiseAmount = grantRiseAmount;
    }

    public Person getPerson()
    {
        return _person;
    }

    public Double getGrantRiseAmount()
    {
        return _grantRiseAmount;
    }

    @Override
    public int compareTo(GrantRiseExtractWrapper o)
    {
        return Person.FULL_FIO_AND_ID_COMPARATOR.compare(_person, o.getPerson());
    }
}