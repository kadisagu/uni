package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ChangeCategoryStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об изменении категории обучаемого
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChangeCategoryStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ChangeCategoryStuExtract";
    public static final String ENTITY_NAME = "changeCategoryStuExtract";
    public static final int VERSION_HASH = 298778428;
    private static IEntityMeta ENTITY_META;

    public static final String P_CATEGORY_CHANGE_DATE = "categoryChangeDate";
    public static final String L_STUDENT_CATEGORY_OLD = "studentCategoryOld";
    public static final String L_STUDENT_CATEGORY_NEW = "studentCategoryNew";

    private Date _categoryChangeDate;     // Дата изменения категории
    private StudentCategory _studentCategoryOld;     // Категория студента на момент формирования приказа
    private StudentCategory _studentCategoryNew;     // Новая категория студента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата изменения категории. Свойство не может быть null.
     */
    @NotNull
    public Date getCategoryChangeDate()
    {
        return _categoryChangeDate;
    }

    /**
     * @param categoryChangeDate Дата изменения категории. Свойство не может быть null.
     */
    public void setCategoryChangeDate(Date categoryChangeDate)
    {
        dirty(_categoryChangeDate, categoryChangeDate);
        _categoryChangeDate = categoryChangeDate;
    }

    /**
     * @return Категория студента на момент формирования приказа.
     */
    public StudentCategory getStudentCategoryOld()
    {
        return _studentCategoryOld;
    }

    /**
     * @param studentCategoryOld Категория студента на момент формирования приказа.
     */
    public void setStudentCategoryOld(StudentCategory studentCategoryOld)
    {
        dirty(_studentCategoryOld, studentCategoryOld);
        _studentCategoryOld = studentCategoryOld;
    }

    /**
     * @return Новая категория студента.
     */
    public StudentCategory getStudentCategoryNew()
    {
        return _studentCategoryNew;
    }

    /**
     * @param studentCategoryNew Новая категория студента.
     */
    public void setStudentCategoryNew(StudentCategory studentCategoryNew)
    {
        dirty(_studentCategoryNew, studentCategoryNew);
        _studentCategoryNew = studentCategoryNew;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ChangeCategoryStuExtractGen)
        {
            setCategoryChangeDate(((ChangeCategoryStuExtract)another).getCategoryChangeDate());
            setStudentCategoryOld(((ChangeCategoryStuExtract)another).getStudentCategoryOld());
            setStudentCategoryNew(((ChangeCategoryStuExtract)another).getStudentCategoryNew());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChangeCategoryStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChangeCategoryStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new ChangeCategoryStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "categoryChangeDate":
                    return obj.getCategoryChangeDate();
                case "studentCategoryOld":
                    return obj.getStudentCategoryOld();
                case "studentCategoryNew":
                    return obj.getStudentCategoryNew();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "categoryChangeDate":
                    obj.setCategoryChangeDate((Date) value);
                    return;
                case "studentCategoryOld":
                    obj.setStudentCategoryOld((StudentCategory) value);
                    return;
                case "studentCategoryNew":
                    obj.setStudentCategoryNew((StudentCategory) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "categoryChangeDate":
                        return true;
                case "studentCategoryOld":
                        return true;
                case "studentCategoryNew":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "categoryChangeDate":
                    return true;
                case "studentCategoryOld":
                    return true;
                case "studentCategoryNew":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "categoryChangeDate":
                    return Date.class;
                case "studentCategoryOld":
                    return StudentCategory.class;
                case "studentCategoryNew":
                    return StudentCategory.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChangeCategoryStuExtract> _dslPath = new Path<ChangeCategoryStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChangeCategoryStuExtract");
    }
            

    /**
     * @return Дата изменения категории. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeCategoryStuExtract#getCategoryChangeDate()
     */
    public static PropertyPath<Date> categoryChangeDate()
    {
        return _dslPath.categoryChangeDate();
    }

    /**
     * @return Категория студента на момент формирования приказа.
     * @see ru.tandemservice.movestudent.entity.ChangeCategoryStuExtract#getStudentCategoryOld()
     */
    public static StudentCategory.Path<StudentCategory> studentCategoryOld()
    {
        return _dslPath.studentCategoryOld();
    }

    /**
     * @return Новая категория студента.
     * @see ru.tandemservice.movestudent.entity.ChangeCategoryStuExtract#getStudentCategoryNew()
     */
    public static StudentCategory.Path<StudentCategory> studentCategoryNew()
    {
        return _dslPath.studentCategoryNew();
    }

    public static class Path<E extends ChangeCategoryStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _categoryChangeDate;
        private StudentCategory.Path<StudentCategory> _studentCategoryOld;
        private StudentCategory.Path<StudentCategory> _studentCategoryNew;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата изменения категории. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeCategoryStuExtract#getCategoryChangeDate()
     */
        public PropertyPath<Date> categoryChangeDate()
        {
            if(_categoryChangeDate == null )
                _categoryChangeDate = new PropertyPath<Date>(ChangeCategoryStuExtractGen.P_CATEGORY_CHANGE_DATE, this);
            return _categoryChangeDate;
        }

    /**
     * @return Категория студента на момент формирования приказа.
     * @see ru.tandemservice.movestudent.entity.ChangeCategoryStuExtract#getStudentCategoryOld()
     */
        public StudentCategory.Path<StudentCategory> studentCategoryOld()
        {
            if(_studentCategoryOld == null )
                _studentCategoryOld = new StudentCategory.Path<StudentCategory>(L_STUDENT_CATEGORY_OLD, this);
            return _studentCategoryOld;
        }

    /**
     * @return Новая категория студента.
     * @see ru.tandemservice.movestudent.entity.ChangeCategoryStuExtract#getStudentCategoryNew()
     */
        public StudentCategory.Path<StudentCategory> studentCategoryNew()
        {
            if(_studentCategoryNew == null )
                _studentCategoryNew = new StudentCategory.Path<StudentCategory>(L_STUDENT_CATEGORY_NEW, this);
            return _studentCategoryNew;
        }

        public Class getEntityClass()
        {
            return ChangeCategoryStuExtract.class;
        }

        public String getEntityName()
        {
            return "changeCategoryStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
