package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О предоставлении отпуска по беременности и родам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class WeekendPregnancyStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract";
    public static final String ENTITY_NAME = "weekendPregnancyStuExtract";
    public static final int VERSION_HASH = -1124086841;
    private static IEntityMeta ENTITY_META;

    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_PAY_BENEFIT = "payBenefit";
    public static final String P_PAY_ONETIME_BENEFIT = "payOnetimeBenefit";
    public static final String P_PAY_BENEFIT_TEXT = "payBenefitText";
    public static final String P_PAY_ONETIME_BENEFIT_TEXT = "payOnetimeBenefitText";
    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";
    public static final String P_PREV_BEGIN_DATE = "prevBeginDate";
    public static final String P_PREV_END_DATE = "prevEndDate";

    private Date _beginDate;     // Дата начала академ. отпуска
    private Date _endDate;     // Дата окончания академ. отпуска
    private boolean _payBenefit;     // Выплатить пособие по беременности и родам
    private boolean _payOnetimeBenefit;     // Выплатить единовременное пособие
    private String _payBenefitText;     // Текст выплаты пособия по беременности и родам
    private String _payOnetimeBenefitText;     // Текст выплаты единовременного пособия
    private StudentStatus _studentStatusOld;     // Предыдущее состояние студента
    private Date _prevBeginDate;     // Дата начала отпуска по предыдущему приказу
    private Date _prevEndDate;     // Дата окончания отпуска по предыдущему приказу

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата начала академ. отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала академ. отпуска. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания академ. отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания академ. отпуска. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Выплатить пособие по беременности и родам. Свойство не может быть null.
     */
    @NotNull
    public boolean isPayBenefit()
    {
        return _payBenefit;
    }

    /**
     * @param payBenefit Выплатить пособие по беременности и родам. Свойство не может быть null.
     */
    public void setPayBenefit(boolean payBenefit)
    {
        dirty(_payBenefit, payBenefit);
        _payBenefit = payBenefit;
    }

    /**
     * @return Выплатить единовременное пособие. Свойство не может быть null.
     */
    @NotNull
    public boolean isPayOnetimeBenefit()
    {
        return _payOnetimeBenefit;
    }

    /**
     * @param payOnetimeBenefit Выплатить единовременное пособие. Свойство не может быть null.
     */
    public void setPayOnetimeBenefit(boolean payOnetimeBenefit)
    {
        dirty(_payOnetimeBenefit, payOnetimeBenefit);
        _payOnetimeBenefit = payOnetimeBenefit;
    }

    /**
     * @return Текст выплаты пособия по беременности и родам.
     */
    @Length(max=600)
    public String getPayBenefitText()
    {
        return _payBenefitText;
    }

    /**
     * @param payBenefitText Текст выплаты пособия по беременности и родам.
     */
    public void setPayBenefitText(String payBenefitText)
    {
        dirty(_payBenefitText, payBenefitText);
        _payBenefitText = payBenefitText;
    }

    /**
     * @return Текст выплаты единовременного пособия.
     */
    @Length(max=600)
    public String getPayOnetimeBenefitText()
    {
        return _payOnetimeBenefitText;
    }

    /**
     * @param payOnetimeBenefitText Текст выплаты единовременного пособия.
     */
    public void setPayOnetimeBenefitText(String payOnetimeBenefitText)
    {
        dirty(_payOnetimeBenefitText, payOnetimeBenefitText);
        _payOnetimeBenefitText = payOnetimeBenefitText;
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Предыдущее состояние студента. Свойство не может быть null.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    /**
     * @return Дата начала отпуска по предыдущему приказу.
     */
    public Date getPrevBeginDate()
    {
        return _prevBeginDate;
    }

    /**
     * @param prevBeginDate Дата начала отпуска по предыдущему приказу.
     */
    public void setPrevBeginDate(Date prevBeginDate)
    {
        dirty(_prevBeginDate, prevBeginDate);
        _prevBeginDate = prevBeginDate;
    }

    /**
     * @return Дата окончания отпуска по предыдущему приказу.
     */
    public Date getPrevEndDate()
    {
        return _prevEndDate;
    }

    /**
     * @param prevEndDate Дата окончания отпуска по предыдущему приказу.
     */
    public void setPrevEndDate(Date prevEndDate)
    {
        dirty(_prevEndDate, prevEndDate);
        _prevEndDate = prevEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof WeekendPregnancyStuExtractGen)
        {
            setBeginDate(((WeekendPregnancyStuExtract)another).getBeginDate());
            setEndDate(((WeekendPregnancyStuExtract)another).getEndDate());
            setPayBenefit(((WeekendPregnancyStuExtract)another).isPayBenefit());
            setPayOnetimeBenefit(((WeekendPregnancyStuExtract)another).isPayOnetimeBenefit());
            setPayBenefitText(((WeekendPregnancyStuExtract)another).getPayBenefitText());
            setPayOnetimeBenefitText(((WeekendPregnancyStuExtract)another).getPayOnetimeBenefitText());
            setStudentStatusOld(((WeekendPregnancyStuExtract)another).getStudentStatusOld());
            setPrevBeginDate(((WeekendPregnancyStuExtract)another).getPrevBeginDate());
            setPrevEndDate(((WeekendPregnancyStuExtract)another).getPrevEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends WeekendPregnancyStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) WeekendPregnancyStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new WeekendPregnancyStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "payBenefit":
                    return obj.isPayBenefit();
                case "payOnetimeBenefit":
                    return obj.isPayOnetimeBenefit();
                case "payBenefitText":
                    return obj.getPayBenefitText();
                case "payOnetimeBenefitText":
                    return obj.getPayOnetimeBenefitText();
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
                case "prevBeginDate":
                    return obj.getPrevBeginDate();
                case "prevEndDate":
                    return obj.getPrevEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "payBenefit":
                    obj.setPayBenefit((Boolean) value);
                    return;
                case "payOnetimeBenefit":
                    obj.setPayOnetimeBenefit((Boolean) value);
                    return;
                case "payBenefitText":
                    obj.setPayBenefitText((String) value);
                    return;
                case "payOnetimeBenefitText":
                    obj.setPayOnetimeBenefitText((String) value);
                    return;
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
                case "prevBeginDate":
                    obj.setPrevBeginDate((Date) value);
                    return;
                case "prevEndDate":
                    obj.setPrevEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "payBenefit":
                        return true;
                case "payOnetimeBenefit":
                        return true;
                case "payBenefitText":
                        return true;
                case "payOnetimeBenefitText":
                        return true;
                case "studentStatusOld":
                        return true;
                case "prevBeginDate":
                        return true;
                case "prevEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "payBenefit":
                    return true;
                case "payOnetimeBenefit":
                    return true;
                case "payBenefitText":
                    return true;
                case "payOnetimeBenefitText":
                    return true;
                case "studentStatusOld":
                    return true;
                case "prevBeginDate":
                    return true;
                case "prevEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "payBenefit":
                    return Boolean.class;
                case "payOnetimeBenefit":
                    return Boolean.class;
                case "payBenefitText":
                    return String.class;
                case "payOnetimeBenefitText":
                    return String.class;
                case "studentStatusOld":
                    return StudentStatus.class;
                case "prevBeginDate":
                    return Date.class;
                case "prevEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<WeekendPregnancyStuExtract> _dslPath = new Path<WeekendPregnancyStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "WeekendPregnancyStuExtract");
    }
            

    /**
     * @return Дата начала академ. отпуска. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания академ. отпуска. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Выплатить пособие по беременности и родам. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract#isPayBenefit()
     */
    public static PropertyPath<Boolean> payBenefit()
    {
        return _dslPath.payBenefit();
    }

    /**
     * @return Выплатить единовременное пособие. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract#isPayOnetimeBenefit()
     */
    public static PropertyPath<Boolean> payOnetimeBenefit()
    {
        return _dslPath.payOnetimeBenefit();
    }

    /**
     * @return Текст выплаты пособия по беременности и родам.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract#getPayBenefitText()
     */
    public static PropertyPath<String> payBenefitText()
    {
        return _dslPath.payBenefitText();
    }

    /**
     * @return Текст выплаты единовременного пособия.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract#getPayOnetimeBenefitText()
     */
    public static PropertyPath<String> payOnetimeBenefitText()
    {
        return _dslPath.payOnetimeBenefitText();
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    /**
     * @return Дата начала отпуска по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract#getPrevBeginDate()
     */
    public static PropertyPath<Date> prevBeginDate()
    {
        return _dslPath.prevBeginDate();
    }

    /**
     * @return Дата окончания отпуска по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract#getPrevEndDate()
     */
    public static PropertyPath<Date> prevEndDate()
    {
        return _dslPath.prevEndDate();
    }

    public static class Path<E extends WeekendPregnancyStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Boolean> _payBenefit;
        private PropertyPath<Boolean> _payOnetimeBenefit;
        private PropertyPath<String> _payBenefitText;
        private PropertyPath<String> _payOnetimeBenefitText;
        private StudentStatus.Path<StudentStatus> _studentStatusOld;
        private PropertyPath<Date> _prevBeginDate;
        private PropertyPath<Date> _prevEndDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата начала академ. отпуска. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(WeekendPregnancyStuExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания академ. отпуска. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(WeekendPregnancyStuExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Выплатить пособие по беременности и родам. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract#isPayBenefit()
     */
        public PropertyPath<Boolean> payBenefit()
        {
            if(_payBenefit == null )
                _payBenefit = new PropertyPath<Boolean>(WeekendPregnancyStuExtractGen.P_PAY_BENEFIT, this);
            return _payBenefit;
        }

    /**
     * @return Выплатить единовременное пособие. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract#isPayOnetimeBenefit()
     */
        public PropertyPath<Boolean> payOnetimeBenefit()
        {
            if(_payOnetimeBenefit == null )
                _payOnetimeBenefit = new PropertyPath<Boolean>(WeekendPregnancyStuExtractGen.P_PAY_ONETIME_BENEFIT, this);
            return _payOnetimeBenefit;
        }

    /**
     * @return Текст выплаты пособия по беременности и родам.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract#getPayBenefitText()
     */
        public PropertyPath<String> payBenefitText()
        {
            if(_payBenefitText == null )
                _payBenefitText = new PropertyPath<String>(WeekendPregnancyStuExtractGen.P_PAY_BENEFIT_TEXT, this);
            return _payBenefitText;
        }

    /**
     * @return Текст выплаты единовременного пособия.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract#getPayOnetimeBenefitText()
     */
        public PropertyPath<String> payOnetimeBenefitText()
        {
            if(_payOnetimeBenefitText == null )
                _payOnetimeBenefitText = new PropertyPath<String>(WeekendPregnancyStuExtractGen.P_PAY_ONETIME_BENEFIT_TEXT, this);
            return _payOnetimeBenefitText;
        }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

    /**
     * @return Дата начала отпуска по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract#getPrevBeginDate()
     */
        public PropertyPath<Date> prevBeginDate()
        {
            if(_prevBeginDate == null )
                _prevBeginDate = new PropertyPath<Date>(WeekendPregnancyStuExtractGen.P_PREV_BEGIN_DATE, this);
            return _prevBeginDate;
        }

    /**
     * @return Дата окончания отпуска по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract#getPrevEndDate()
     */
        public PropertyPath<Date> prevEndDate()
        {
            if(_prevEndDate == null )
                _prevEndDate = new PropertyPath<Date>(WeekendPregnancyStuExtractGen.P_PREV_END_DATE, this);
            return _prevEndDate;
        }

        public Class getEntityClass()
        {
            return WeekendPregnancyStuExtract.class;
        }

        public String getEntityName()
        {
            return "weekendPregnancyStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
