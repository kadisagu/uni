/* $Id$ */
package ru.tandemservice.movestudent.component.settings.IndividualOrderSettings;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Dmitry Seleznev
 * @since 25.09.2012
 */
public interface IDAO extends IUniDao<Model>
{
    /**
     * Изменяет настройку для определенного типа сборного приказа.
     *
     * @param id - идентификатор типа сборного приказа.
     */
    public void updateExtractIndividual(long id);

    /**
     * Для уже существующих типов сборных приказов создаёт связанные объекты настройки, если их нет.<p>
     */
    public void updateForNewExtracts();
}