/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e47.ParagraphPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubController;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.TransferProgramSubjectStuListExtract;

/**
 * @author ListExtractComponentGenerator
 * @since 17.01.2017
 */
public class Controller extends AbstractListParagraphPubController<TransferProgramSubjectStuListExtract, IDAO, Model>
{

    @Override
    protected void prepareListDataSource(IBusinessComponent component)
    {
        super.prepareListDataSource(component);
        Model model = getModel(component);
        DynamicListDataSource dataSource = model.getDataSource();
        dataSource.getColumns().remove(9);
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", ListStudentExtract.entity().educationOrgUnit().educationLevelHighSchool().educationLevel().programSubjectWithCodeIndexAndGenTitle().s()).setClickable(false).setOrderable(false), 9);
        dataSource.addColumn(new SimpleColumn("Направленность", ListStudentExtract.entity().educationOrgUnit().educationLevelHighSchool().educationLevel().programSpecializationTitle().s()).setClickable(false).setOrderable(false), 10);
    }
}
