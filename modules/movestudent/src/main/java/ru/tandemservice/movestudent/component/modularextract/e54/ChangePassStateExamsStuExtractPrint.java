/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e54;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.ChangePassStateExamsStuExtract;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 15.08.2012
 */
public class ChangePassStateExamsStuExtractPrint implements IPrintFormCreator<ChangePassStateExamsStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ChangePassStateExamsStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        String speciality = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getTitle();
        String educationLevel = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getTitle();
        modifier.put("period", extract.getSeason() != null ? extract.getSeason() : "______");
        modifier.put("developCondition_G", CommonExtractPrint.getDevelopConditionStr2_G(extract.getEntity().getEducationOrgUnit().getDevelopCondition()));
        modifier.put("speciality", speciality);
        modifier.put("educationLevel", educationLevel);
        modifier.put("year", String.valueOf(extract.getYear()));
        modifier.put("newDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getNewDate()));

        modifier.put("newDateDay", RussianDateFormatUtils.getDayString(extract.getNewDate(), true));
        modifier.put("newDateMonthStr_N", RussianDateFormatUtils.getMonthName(extract.getNewDate(), true));
        modifier.put("newDateMonthStr_G", RussianDateFormatUtils.getMonthName(extract.getNewDate(), false));
        modifier.put("newDateYr", RussianDateFormatUtils.getYearString(extract.getNewDate(), true));
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}