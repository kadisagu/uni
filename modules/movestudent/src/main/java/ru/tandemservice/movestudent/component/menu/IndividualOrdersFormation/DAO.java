/* $Id$ */
package ru.tandemservice.movestudent.component.menu.IndividualOrdersFormation;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.component.commons.ExtractListUtil;
import ru.tandemservice.movestudent.component.commons.IMoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.MoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.OrderCategory;
import ru.tandemservice.movestudent.entity.ExtractCreationRule;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 31.10.2012
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry(IMoveStudentMQBuilder.EXTRACT_ALIAS);

    static
    {
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_FULL_FIO, new OrderDescription(IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS, IdentityCard.P_LAST_NAME), new OrderDescription(IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS, IdentityCard.P_FIRST_NAME), new OrderDescription(IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS, IdentityCard.P_MIDDLE_NAME));
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_EXTRACT_FORMATIVE_ORG_UNIT_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_TITLE}));
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_EXTRACT_TERRITORIAL_ORG_UNIT_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_TITLE}));
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_EXTRACT_PRODUCING_ORG_UNIT_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.L_ORG_UNIT, OrgUnit.P_TITLE}));
    }

    @Override
    public void prepare(Model model)
    {
        ExtractListUtil.initExtractListModel(model, "individualOrdersFormation", OrderCategory.INDIVIDUAL);
        model.setOrdersStateList(getCatalogItemList(OrderStates.class));
        model.setEmployeePostModel(new OrderExecutorSelectModel());

        model.setEducationLevelTypeListModel(new FullCheckSelectModel(StructureEducationLevels.P_SHORT_TITLE) {
            @Override
            public ListResult findValues(String filter) {
                Criteria c = getSession().createCriteria(StructureEducationLevels.class, "e");

                c.add(Restrictions.isNull(StructureEducationLevels.L_PARENT));
                c.setProjection(Projections.distinct(Projections.property("e." + StructureEducationLevels.P_ID)));

                final List<Long> ids = c.list();

                if (ids.isEmpty()) {
                    return ListResult.getEmpty();
                }

                c = getSession().createCriteria(StructureEducationLevels.class);
                c.add(Restrictions.in(StructureEducationLevels.P_ID, ids));


                return new ListResult<StructureEducationLevels>(c.list());
            }
        });
    }



    @Override
    public void prepareListDataSource(Model model)
    {
//        List<StructureEducationLevels> educationLevelsList = model.getEducationLevelTypeList();


        // init datasource
        IMoveStudentMQBuilder builder = MoveStudentMQBuilder.createExtractMQBuilder(ModularStudentExtract.class, model.getSettings());

        // apply user defined restrictions
        MQBuilder subBuilder = new MQBuilder(ExtractCreationRule.ENTITY_CLASS, "cr", new String[] {ExtractCreationRule.studentExtractType().id().s()});
        subBuilder.add(MQExpression.eq("cr", ExtractCreationRule.individualOrder().s(), Boolean.TRUE));

        builder.getMQBuilder().add(MQExpression.in(IMoveStudentMQBuilder.EXTRACT_ALIAS, ModularStudentExtract.type().id().s(), subBuilder));
        builder.getMQBuilder().add(MQExpression.isNotNull(IMoveStudentMQBuilder.EXTRACT_ALIAS, ModularStudentExtract.paragraph().s()));
        // apply filters
        builder.applyExtractEducationYear();
        builder.applyExtractCreateDate();
        builder.applyOrderCommitDate();
        builder.applyOrderNumber();
        builder.applyExtractLastName();
        builder.applyExtractFormativeOrgUnit();
        builder.applyExtractTerritorialOrgUnit();
        builder.applyExtractProducingOrgUnit();
        builder.applyExtractEducationLevelHighSchool();
        builder.applyExtractCourse();
        builder.applyExtractDevelopForm();
        builder.applyExtractCompensationType();
        builder.applyExtractType();
        builder.applyOrderState();
        builder.applyExtractReason();
        builder.applyOrderStudent(getSession());
        builder.applyExecutor();
        builder.applyEducationLevelIndividual(getSession());


        // create page
        builder.createPage(getSession(), _orderSettings, model.getDataSource());
    }
}