/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e38.ParagraphAddEdit;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.SplitStudentsExtStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.IExtEducationLevelModel;

import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 12.12.2012
 */
public class Model extends AbstractListParagraphAddEditModel<SplitStudentsExtStuListExtract> implements IEducationLevelModel, IExtEducationLevelModel, IGroupModel
{
    private Course _course;

    private Group _group;
    private ISelectModel _groupListModel;

    private ISelectModel _specializationsListModel;
    private EducationLevelsHighSchool _educationLevelsHighSchool;

    private List<CompensationType> _compensationTypeList;
    private CompensationType _compensationType;

    private EducationLevels _parentEduLevel;

    private ISelectModel _developFormModel;
    private ISelectModel _developTechModel;
    private ISelectModel _developConditionModel;
    private ISelectModel _developPeriodModel;

    private DevelopForm _developForm;
    private DevelopTech _developTech;
    private DevelopCondition _developCondition;
    private DevelopPeriod _developPeriod;


    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        this._course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        this._group = group;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        this._groupListModel = groupListModel;
    }

    public ISelectModel getSpecializationsListModel()
    {
        return _specializationsListModel;
    }

    public void setSpecializationsListModel(ISelectModel specializationsListModel)
    {
        this._specializationsListModel = specializationsListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

   public EducationLevels getParentEduLevel()
    {
        return _parentEduLevel;
    }

    public void setParentEduLevel(EducationLevels parentEduLevel)
    {
        _parentEduLevel = parentEduLevel;
    }

    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public ISelectModel getDevelopTechModel()
    {
        return _developTechModel;
    }

    public void setDevelopTechModel(ISelectModel developTechModel)
    {
        _developTechModel = developTechModel;
    }

    public ISelectModel getDevelopConditionModel()
    {
        return _developConditionModel;
    }

    public void setDevelopConditionModel(ISelectModel developConditionModel)
    {
        _developConditionModel = developConditionModel;
    }

    public ISelectModel getDevelopPeriodModel()
    {
        return _developPeriodModel;
    }

    public void setDevelopPeriodModel(ISelectModel developPeriodModel)
    {
        _developPeriodModel = developPeriodModel;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public void setDevelopTech(DevelopTech developTech)
    {
        _developTech = developTech;
    }

    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public void setDevelopCondition(DevelopCondition developCondition)
    {
        _developCondition = developCondition;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        _developPeriod = developPeriod;
    }

    public boolean isAnyOrderParametersCantBeChanged()
    {
        return !isParagraphOnlyOneInTheOrder();
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return getGroup() != null ? getGroup().getEducationOrgUnit().getFormativeOrgUnit() : null;
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return getGroup() != null ? getGroup().getEducationOrgUnit().getTerritorialOrgUnit() : null;
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }
}
