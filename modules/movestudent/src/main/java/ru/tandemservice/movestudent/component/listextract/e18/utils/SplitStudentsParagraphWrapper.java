/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e18.utils;

import com.google.common.collect.Lists;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: nikolay
 * Date: 3/12/13
 * Time: 4:49 PM
 */
public class SplitStudentsParagraphWrapper implements Comparable<SplitStudentsParagraphWrapper>
{
    private final Course _course;
    private final Group _group;
    private final EducationOrgUnit _educationOrgUnit;
    private final List<SplitStudentsParagraphPartWrapper> _paragraphPartWrapperList = Lists.newArrayList();
    private final ListStudentExtract _firstExtract;

    public SplitStudentsParagraphWrapper(Course course, Group group, EducationOrgUnit educationOrgUnit, ListStudentExtract firstExtract)
    {
        _course = course;
        _group = group;
        _educationOrgUnit = educationOrgUnit;
        _firstExtract = firstExtract;
    }

    public Course getCourse()
    {
        return _course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    public List<SplitStudentsParagraphPartWrapper> getParagraphPartWrapperList()
    {
        return _paragraphPartWrapperList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public int compareTo(SplitStudentsParagraphWrapper o)
    {
        int result = ((Integer) _course.getIntValue()).compareTo((Integer) o.getCourse().getIntValue());
        if (result ==0)
            result = _group.getTitle().compareTo(o.getGroup().getTitle());

        return result;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof SplitStudentsParagraphWrapper))
            return false;

        SplitStudentsParagraphWrapper that = (SplitStudentsParagraphWrapper) o;

        return (_course.equals(that.getCourse())
                && _group.equals(that.getGroup())
        );
    }
}
