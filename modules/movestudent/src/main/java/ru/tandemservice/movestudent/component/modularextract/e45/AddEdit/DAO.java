/* $Id: DAO.java.vm 11769 2010-03-05 07:11:49Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e45.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.RepeatPassStateExamStuExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 19.02.2011
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<RepeatPassStateExamStuExtract, Model>
{

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected RepeatPassStateExamStuExtract createNewInstance()
    {
        return new RepeatPassStateExamStuExtract();
    }

}
