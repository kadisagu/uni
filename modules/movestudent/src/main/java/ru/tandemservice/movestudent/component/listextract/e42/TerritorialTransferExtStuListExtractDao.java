/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.component.listextract.e42;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 28.08.2013
 */
public class TerritorialTransferExtStuListExtractDao extends UniBaseDao implements IExtractComponentDao<TerritorialTransferExtStuListExtract>
{
    @Override
    public void doCommit(TerritorialTransferExtStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        extract.setGroupOld(extract.getGroupOld());
        extract.getEntity().setGroup(extract.getGroupNew());

        extract.setCompensationTypeOld(extract.getEntity().getCompensationType());
        extract.getEntity().setCompensationType(extract.getCompensationTypeNew());

        extract.setEducationOrgUnitOld(extract.getEntity().getEducationOrgUnit());
        extract.getEntity().setEducationOrgUnit(extract.getEducationOrgUnitNew());
    }

    @Override
    public void doRollback(TerritorialTransferExtStuListExtract extract, Map parameters)
    {
        Student student = extract.getEntity();
        student.setGroup(extract.getGroupOld());
        student.setCompensationType(extract.getCompensationTypeOld());
        student.setEducationOrgUnit(extract.getEducationOrgUnitOld());
    }
}