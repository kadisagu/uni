/* $Id: DAO.java.vm 12068 2010-03-29 10:41:18Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e22.ParagraphAddEdit;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.ChangeReviewerStuListExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.IAbstractExtract;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 19.02.2011
 */
@SuppressWarnings("unchecked")
public class DAO extends AbstractListParagraphAddEditDAO<ChangeReviewerStuListExtract, Model> implements IDAO
{
    public static void prepareColumns(Model model)
    {
        if (model.getParagraphId() != null)
        {
            Map<Long, String> reviewerNameValueMap = new HashMap<>();
            Map<Long, String> reviewerDegreeValueMap = new HashMap<>();
            List<ChangeReviewerStuListExtract> extractList = DataAccessServices.dao().getList(ChangeReviewerStuListExtract.class, IAbstractExtract.L_PARAGRAPH, model.getParagraph());
            for (ChangeReviewerStuListExtract ext : extractList)
            {
                reviewerNameValueMap.put(ext.getEntity().getId(), ext.getReviewerName());
                reviewerDegreeValueMap.put(ext.getEntity().getId(), ext.getReviewerDegree());
            }
            ((BlockColumn) model.getDataSource().getColumn("reviewerName")).setValueMap(reviewerNameValueMap);
            ((BlockColumn) model.getDataSource().getColumn("reviewerDegree")).setValueMap(reviewerDegreeValueMap);
        }
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        // заполняем поля по умолчанию
        if (model.getParagraphId() != null)
        {
            ChangeReviewerStuListExtract extract = model.getFirstExtract();
            model.setCourse(extract.getCourse());
            model.setGroup(extract.getGroup());
            model.setDevelopForm(extract.getDevelopForm());
        }
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developForm(), model.getDevelopForm()));
    }

    @Override
    protected ChangeReviewerStuListExtract createNewInstance(Model model)
    {
        return new ChangeReviewerStuListExtract();
    }

    @Override
    protected void fillExtract(ChangeReviewerStuListExtract extract, Student student, Model model)
    {
        extract.setCourse(model.getCourse());
        extract.setDevelopForm(model.getDevelopForm());
        extract.setGroup(model.getGroup());
        ErrorCollector errs = UserContext.getInstance().getErrorCollector();

        final IValueMapHolder reviewerNameValueHolder = (IValueMapHolder) model.getDataSource().getColumn("reviewerName");
        final Map<Long, Object> reviewerNameValueMap = (null == reviewerNameValueHolder ? Collections.emptyMap() : reviewerNameValueHolder.getValueMap());

        if (reviewerNameValueMap.get(student.getId()) == null)
            errs.add("Поля «ФИО рецензента» у выбранных студентов обязательны для заполнения.", "name_id_" + student.getId());
        else
            extract.setReviewerName(reviewerNameValueMap.get(student.getId()).toString());

        final IValueMapHolder reviewerDegreeValueHolder = (IValueMapHolder) model.getDataSource().getColumn("reviewerDegree");
        final Map<Long, Object> reviewerDegreeValueMap = (null == reviewerDegreeValueHolder ? Collections.emptyMap() : reviewerDegreeValueHolder.getValueMap());

        if (reviewerDegreeValueMap.get(student.getId()) == null)
            errs.add("Поля «Степень, звание, должность» у выбранных студентов обязательны для заполнения.", "degree_id_" + student.getId());
        else
            extract.setReviewerDegree(reviewerDegreeValueMap.get(student.getId()).toString());
    }
}