/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e32.utils;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 02.10.2012
 */
public class AcadGrantAssignParagraphWrapper implements Comparable<AcadGrantAssignParagraphWrapper>
{
    private final EducationLevels _educationLevels;
    private final DevelopForm _developForm;
    private final DevelopCondition _developCondition;
    private final DevelopTech _developTech;
    private final DevelopPeriod _developPeriod;
    private final Date _beginDate;
    private final Date _endDate;
    private final OrgUnit _formativeOrgUnit;
    private final OrgUnit _territorialOrgUnit;
    private final String _eduBaseText;
    private final ListStudentExtract _firstExtract;

    public AcadGrantAssignParagraphWrapper(EducationLevels educationLevels, DevelopForm developForm, DevelopCondition developCondition, DevelopTech developTech, DevelopPeriod developPeriod, Date beginDate, Date endDate, OrgUnit formativeOrgUnit, OrgUnit territorialOrgUnit, String eduBaseText, ListStudentExtract firstExtract)
    {
        _educationLevels = educationLevels;
        _developForm = developForm;
        _developCondition = developCondition;
        _developTech = developTech;
        _developPeriod = developPeriod;
        _beginDate = beginDate;
        _endDate = endDate;
        _formativeOrgUnit = formativeOrgUnit;
        _territorialOrgUnit = territorialOrgUnit;
        _eduBaseText = eduBaseText;
        _firstExtract = firstExtract;
    }

    private final List<AcadGrantAssignParagraphPartWrapper> _paragraphPartWrapperList = new ArrayList<>();

    public EducationLevels getEducationLevels()
    {
        return _educationLevels;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public Date getBeginDate()
    {
        return _beginDate;
    }

    public Date getEndDate()
    {
        return _endDate;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public List<AcadGrantAssignParagraphPartWrapper> getParagraphPartWrapperList()
    {
        return _paragraphPartWrapperList;
    }

    public String getEduBaseText()
    {
        return _eduBaseText;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof AcadGrantAssignParagraphWrapper))
            return false;

        AcadGrantAssignParagraphWrapper that = (AcadGrantAssignParagraphWrapper) o;

        return
        _educationLevels.equals(that.getEducationLevels()) &&
        _developForm.equals(that.getDevelopForm()) &&
        _developCondition.equals(that.getDevelopCondition()) &&
        _developTech.equals(that.getDevelopTech()) &&
        _developPeriod.equals(that.getDevelopPeriod()) &&
        _beginDate.equals(that.getBeginDate()) &&
        _endDate.equals(that.getEndDate()) &&
        _eduBaseText.equals(that.getEduBaseText());
    }

    @Override
    public int hashCode()
    {
        return _educationLevels.hashCode() & _developForm.hashCode() & _developCondition.hashCode() & _developTech.hashCode() & _developPeriod.hashCode() & _beginDate.hashCode() & _endDate.hashCode() & _eduBaseText.hashCode();
    }

    @Override
    public int compareTo(AcadGrantAssignParagraphWrapper o)
    {
        StructureEducationLevels thisLevels = _educationLevels.getLevelType();
        StructureEducationLevels thatLevels = o.getEducationLevels().getLevelType();

        if (thisLevels == null || thatLevels == null)
            return thisLevels == null ? 1 : -1;

        String thisCode = _educationLevels.getTitleCodePrefix();
        String thatCode = o.getEducationLevels().getTitleCodePrefix();

        int result;

        if (thisCode == null || thatCode == null)
            result = thisCode == null ? (thatCode == null ? 0 : 1) : -1;
            else result = thisCode.compareTo(thatCode);

        boolean isThisChild = thisLevels.isSpecialty() || thisLevels.isProfile();
        boolean isThatChild = thatLevels.isSpecialty() || thatLevels.isProfile();

        if (isThisChild != isThatChild)
        {
            result = isThisChild ? 1 : -1;
        }

        if (result == 0)
            result = _educationLevels.getTitle().compareTo(o.getEducationLevels().getTitle());

        if (result == 0)
            result = _developForm.getCode().compareTo(o.getDevelopForm().getCode());

        if (result == 0)
            result = _developCondition.getCode().compareTo(o.getDevelopCondition().getCode());

        if (result == 0)
            result = _developTech.getCode().compareTo(o.getDevelopTech().getCode());

        if (result == 0)
            result = _developPeriod.getCode().compareTo(o.getDevelopPeriod().getCode());

        if (result == 0)
            result = _beginDate.compareTo(o.getBeginDate());

        if (result == 0)
            result = _endDate.compareTo(o.getEndDate());

        if (result == 0)
            result = _eduBaseText.compareTo(o.getEduBaseText());

        return result;
    }
}