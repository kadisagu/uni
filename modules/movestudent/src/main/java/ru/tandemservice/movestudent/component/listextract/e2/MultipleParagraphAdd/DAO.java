/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e2.MultipleParagraphAdd;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListMultipleParagraphAdd.AbstractListMultipleParagraphAddDAO;
import ru.tandemservice.movestudent.entity.CourseTransferStuListExtract;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 11.06.2009
 */
public class DAO extends AbstractListMultipleParagraphAddDAO<CourseTransferStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setCourseOldListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getCourse() == null) return ListResult.getEmpty();

                MQBuilder subBuilder = new MQBuilder(Group.ENTITY_CLASS, "g");
                subBuilder.add(MQExpression.eq("g", Group.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getOrder().getOrgUnit()));
                subBuilder.add(MQExpression.eq("g", Group.P_ARCHIVAL, Boolean.FALSE));
                subBuilder.add(MQExpression.eq("g", Group.L_COURSE, model.getCourse()));

                MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "s", new String[]{Student.L_COURSE});
                builder.add(MQExpression.eq("s", Student.L_STATUS + "." + StudentStatus.P_ACTIVE, Boolean.TRUE));
                builder.add(MQExpression.eq("s", Student.P_ARCHIVAL, Boolean.FALSE));
                builder.add(MQExpression.in("s", Student.L_GROUP, subBuilder));
                builder.addOrder("s", Student.L_COURSE + "." + Course.P_CODE);
                builder.setNeedDistinct(true);
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });
        model.setCourseNewListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getCourse() == null || model.getCourseOld() == null) return ListResult.getEmpty();
                int minCourse = Integer.parseInt(model.getCourseOld().getCode());
                List<Course> courseList = new ArrayList<>(model.getCourseList());
                for (Iterator<Course> iter = courseList.iterator(); iter.hasNext();)
                    if (Integer.parseInt(iter.next().getCode()) <= minCourse)
                        iter.remove();
                return new ListResult<>(courseList);
            }
        });
    }

    @Override
    protected MQBuilder getStudentsList(Model model, List<Group> groupsList)
    {
        return super.getStudentsList(model, groupsList)
        .add(MQExpression.eq("s", Student.L_COURSE, model.getCourseOld()));
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq("p", Group.L_COURSE, model.getCourse()));
    }

    @Override
    protected CourseTransferStuListExtract createNewInstance(Model model)
    {
        return new CourseTransferStuListExtract();
    }

    @Override
    protected void fillExtract(CourseTransferStuListExtract extract, Model model, Group group)
    {
        extract.setGroup(group);
        extract.setCourse(model.getCourse());
        extract.setCourseOld(model.getCourseOld());
        extract.setCourseNew(model.getCourseNew());
        extract.setGroupChangeCourse(model.isGroupChangeCourse());
    }
}