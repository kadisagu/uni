/* $Id$ */
package ru.tandemservice.movestudent.component.menu.OtherOrders;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.commons.ExtractListModel;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 11.04.2013
 */
public class Model extends ExtractListModel
{
    private List<OrderStates> _otherOrdersStateList;
    private ISelectModel employeePostModel;

    public List<OrderStates> getOtherOrdersStateList()
    {
        return _otherOrdersStateList;
    }

    public void setOtherOrdersStateList(List<OrderStates> otherOrdersStateList)
    {
        _otherOrdersStateList = otherOrdersStateList;
    }

    public ISelectModel getEmployeePostModel()
    {
        return employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        this.employeePostModel = employeePostModel;
    }
}