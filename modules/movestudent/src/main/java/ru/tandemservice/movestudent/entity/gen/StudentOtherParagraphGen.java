package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import ru.tandemservice.movestudent.entity.StudentOtherParagraph;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параграф прочего приказа по студентам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentOtherParagraphGen extends AbstractStudentParagraph
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.StudentOtherParagraph";
    public static final String ENTITY_NAME = "studentOtherParagraph";
    public static final int VERSION_HASH = 526590709;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof StudentOtherParagraphGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentOtherParagraphGen> extends AbstractStudentParagraph.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentOtherParagraph.class;
        }

        public T newInstance()
        {
            return (T) new StudentOtherParagraph();
        }
    }
    private static final Path<StudentOtherParagraph> _dslPath = new Path<StudentOtherParagraph>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentOtherParagraph");
    }
            

    public static class Path<E extends StudentOtherParagraph> extends AbstractStudentParagraph.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return StudentOtherParagraph.class;
        }

        public String getEntityName()
        {
            return "studentOtherParagraph";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
