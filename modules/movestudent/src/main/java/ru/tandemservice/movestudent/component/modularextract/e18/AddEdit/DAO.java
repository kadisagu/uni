/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e18.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.movestudent.entity.TransferStuExtractExt;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 05.05.2009
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<TransferStuExtractExt, Model>
{
    @Override
    protected TransferStuExtractExt createNewInstance()
    {
        return new TransferStuExtractExt();
    }
    
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }
    
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setEduModel(MoveStudentDaoFacade.getCommonExtractUtil().createEduModel(model.isAddForm(), model.getExtract(), true));
        
        if(!model.isAddForm())
            model.setDebtsList(MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(model.getExtract()));
        
        if(model.getDebtsList().isEmpty())
            model.getDebtsList().add(new StuExtractToDebtRelation());
    }

    @Override
    public void update(Model model)
    {
        //save rollback data
        MoveStudentDaoFacade.getCommonExtractUtil().update(model.getEduModel(), model.getExtract());
        
        if (!model.getExtract().isStopGrantsPaying())
            model.getExtract().setStopPayingDate(null);
        
        if(!model.getExtract().isHasDebts())
            model.getExtract().setDeadline(null);
        
        super.update(model);
        
        for (StuExtractToDebtRelation rel : model.getDebtsList())
        {
            if (model.getExtract().isHasDebts())
            {
                rel.setExtract(model.getExtract());
                getSession().saveOrUpdate(rel);
            }
            else if(null != rel.getId())
                getSession().delete(rel);
        }
        
        for(StuExtractToDebtRelation rel : model.getDebtsToDel())
        {
            if(null != rel.getId()) getSession().delete(rel);
        }
    }
}