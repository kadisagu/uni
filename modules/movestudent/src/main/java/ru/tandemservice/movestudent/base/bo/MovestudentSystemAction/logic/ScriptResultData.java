/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MovestudentSystemAction.logic;

/**
 * @author Ekaterina Zvereva
 * @since 07.03.2017
 */
public class ScriptResultData
{
    private byte[] content;
    private String fileName;

    public ScriptResultData(String name, byte[] array)
    {
        fileName = name;
        content = array;
    }

    public byte[] getContent()
    {
        return content;
    }

    public String getFileName()
    {
        return fileName;
    }
}