package ru.tandemservice.movestudent.entity.catalog;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.movestudent.entity.catalog.gen.MovestudentTemplateGen;

public class MovestudentTemplate extends MovestudentTemplateGen implements ITemplateDocument
{
    @Override
    public byte[] getContent()
    {
        return CommonBaseUtil.getTemplateContent(this);
    }

    @EntityDSLSupport(parts = {MovestudentTemplate.P_TITLE, MovestudentTemplate.L_TYPE + "." + StudentExtractType.P_CODE})
    @Override
    public String getTitleWithCode()
    {
        return getTitle() + " - (" + getType().getCode() + ")";
    }
}