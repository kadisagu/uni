/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e45.ParagraphAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AdmitToPassStatateExamStuListExtract;
import org.tandemframework.hibsupport.builder.MQBuilder;
import ru.tandemservice.movestudent.entity.ChangeProfileAndGroupStuListExtract;
import ru.tandemservice.movestudent.utils.ExtEducationLevelsHighSchoolSelectModel;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ListExtractComponentGenerator
 * @since 01.06.2016
 */
public class DAO extends AbstractListParagraphAddEditDAO<AdmitToPassStatateExamStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        model.setFormativeOrgUnit(model.getParagraph().getOrder().getOrgUnit());
        model.setCompensationTypeList(UniDaoFacade.getCoreDao().getCatalogItemList(CompensationType.class));
        model.setGroupNewListModel(new GroupSelectModel(model, model, null, model.getParagraph().getOrder().getOrgUnit())
        {
            @Override
            protected boolean isNeedRequest()
            {
                if (getGroupModel().getCourse() == null) return false;
                if (getEduLevelModel().getDevelopForm() == null) return false;
                return getOrgUnit() != null || getEduLevelModel().getFormativeOrgUnit() != null;
            }

            protected DQLSelectBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, G_ALIAS).column(G_ALIAS);

                if (o != null)
                {
                    if (o instanceof Long) builder.where(eq(property(G_ALIAS + ".id"), value((Long) o)));
                    else if (o instanceof Collection) builder.where(in(property(G_ALIAS + ".id"), (Collection) o));
                }

                builder.where(eq(property(G_ALIAS, Group.educationOrgUnit().formativeOrgUnit()), value(getEduLevelModel().getFormativeOrgUnit())));
                builder.where(eq(property(G_ALIAS, Group.course()), value(getGroupModel().getCourse())));
                builder.where(eq(property(G_ALIAS, Group.educationOrgUnit().developForm()), value(getEduLevelModel().getDevelopForm())));
                builder.where(eq(property(G_ALIAS, Group.archival()), value(Boolean.FALSE)));

                if(null != getEduLevelModel().getEducationLevelsHighSchool())
                {
                    DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(Student.class, "s").column(property("s", Student.group().id()))
                            .where(eq(property("s", Student.educationOrgUnit().educationLevelHighSchool().id()), value(getEduLevelModel().getEducationLevelsHighSchool().getId())));
                    builder.where(in(property(G_ALIAS, Group.id()), subBuilder.buildQuery()));
                }

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property(G_ALIAS, Group.P_TITLE), value(CoreStringUtils.escapeLike(filter, true))));

                builder.order(property(G_ALIAS, Group.P_TITLE), OrderDirection.asc);

                return builder;
            }
        });
        model.setDevelopFormModel(EducationOrgUnitUtil.getDevelopFormSelectModel(model, false));

        model.setSpecializationsListModel(new ExtEducationLevelsHighSchoolSelectModel(model, null)
        {
            @Override
            protected boolean isNeedRequest()
            {
                return getModel().getFormativeOrgUnit() != null;
            }

            @Override
            protected DQLSelectBuilder createEducationOrgUnitBuilder(String alias)
            {
                final DQLSelectBuilder dql = super.createEducationOrgUnitBuilder(alias);
                dql.where(eq(property(alias, EducationOrgUnit.formativeOrgUnit()), value(getModel().getFormativeOrgUnit())));

                dql.where(exists(
                        new DQLSelectBuilder().fromEntity(Student.class, "s")
                                .where(eq(property("s", Student.educationOrgUnit()), property(alias)))
                                .buildQuery()));

                return dql;
            }
        });

        if(null != model.getParagraphId())
        {
            AdmitToPassStatateExamStuListExtract firstExtract = model.getFirstExtract();
            if (null != firstExtract)
            {
                model.setEducationLevelsHighSchool(firstExtract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool());
                model.setDevelopForm(firstExtract.getEntity().getEducationOrgUnit().getDevelopForm());
                model.setCourse(firstExtract.getEntity().getCourse());
                model.setExamPeriodBegin(firstExtract.getBeginDate());
                model.setExamPeriodEnd(firstExtract.getEndDate());
            }
        }
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().formativeOrgUnit().s(), model.getFormativeOrgUnit()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().educationLevelHighSchool().s(), model.getEducationLevelsHighSchool()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developForm().s(), model.getDevelopForm()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.course().s(), model.getCourse()));

        if (null != model.getGroupList() && !model.getGroupList().isEmpty())
            builder.add(MQExpression.in(STUDENT_ALIAS, Student.L_GROUP, model.getGroupList()));

        if (null != model.getCompensationType())
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.compensationType().s(), model.getCompensationType()));

        builder.addOrder(STUDENT_ALIAS, Student.group().title().s());
        builder.addOrder(STUDENT_ALIAS, Student.course().title().s());
        builder.addOrder(STUDENT_ALIAS, Student.person().identityCard().fullFio().s());
    }

    @Override
    protected AdmitToPassStatateExamStuListExtract createNewInstance(Model model)
    {
        return new AdmitToPassStatateExamStuListExtract();
    }

    @Override
    protected void fillExtract(AdmitToPassStatateExamStuListExtract extract, Student student, Model model)
    {
        extract.setEducationLevelsHighSchool(model.getEducationLevelsHighSchool());
        extract.setDevelopForm(model.getDevelopForm());
        extract.setCourse(model.getCourse());
        extract.setBeginDate(model.getExamPeriodBegin());
        extract.setEndDate(model.getExamPeriodEnd());
    }
}