/* $Id: DAO.java.vm 16690 2011-02-19 08:39:38Z iolshvang $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e39.ParagraphAddEdit;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.movestudent.utils.SessionPartModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.*;

/**
 * @author ListExtractComponentGenerator
 * @since 21.01.2013
 */
public class DAO extends AbstractListParagraphAddEditDAO<AcadGrantBonusAssignStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));
        model.setPartModel(new SessionPartModel());

        AcadGrantBonusAssignStuListExtract extract = model.getFirstExtract();

        if (model.isParagraphOnlyOneInTheOrder() && !model.isEditForm())
        {
            model.setCompensationType(get(CompensationType.class, CompensationType.P_CODE, CompensationTypeCodes.COMPENSATION_TYPE_BUDGET));
        }

        if (!model.isParagraphOnlyOneInTheOrder())
        {
            model.setCompensationType(extract.getCompensationType());
            model.setCompensationTypeDisabled(true);
            model.setSessionType(extract.getSessionType());
            model.setSessionTypeDisabled(true);
        }

        if (model.isEditForm())
        {
            model.setCourse(extract.getCourse());
            model.setGroup(extract.getGroup());
            model.setCompensationType(extract.getCompensationType());
            model.setBeginDate(extract.getBeginDate());
            model.setEndDate(extract.getEndDate());
            model.setSessionType(extract.getSessionType());
        }
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        if (model.getCompensationType() != null)
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));
    }

    @Override
    protected AcadGrantBonusAssignStuListExtract createNewInstance(Model model)
    {
        return new AcadGrantBonusAssignStuListExtract();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void fillExtract(AcadGrantBonusAssignStuListExtract extract, Student student, Model model)
    {
        IValueMapHolder grantBonusSizeHolder = (IValueMapHolder) model.getDataSource().getColumn("grantBonusSize");
        Map<Long, Double> grantBonusSizeMap = (null == grantBonusSizeHolder ? Collections.emptyMap() : grantBonusSizeHolder.getValueMap());

        Number grantBonusSize = grantBonusSizeMap.get(student.getId());
        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setCompensationType(model.getCompensationType());
        extract.setBeginDate(model.getBeginDate());
        extract.setEndDate(model.getEndDate());
        extract.setSessionType(model.getSessionType());
        extract.setGrantBonusSize(grantBonusSize.doubleValue());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        IValueMapHolder finAidBonusSizeHolder = (IValueMapHolder) model.getDataSource().getColumn("grantBonusSize");
        Map<Long, Double> finAidBonusSizeMap = (null == finAidBonusSizeHolder ? Collections.emptyMap() : finAidBonusSizeHolder.getValueMap());

        List<ViewWrapper<Student>> viewlist = new ArrayList<ViewWrapper<Student>>((Collection) ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).getSelectedObjects());

        for (ViewWrapper<Student> wrapper : viewlist)
        {
            if (finAidBonusSizeMap.get(wrapper.getEntity().getId()) == null)
                errorCollector.add("Поле «Размер материальной помощи» обязательно для заполнения.", "grantBonusSizeId_" + wrapper.getEntity().getId());

        }

        if (!model.getBeginDate().before(model.getEndDate()))
            errorCollector.add("Дата окончания должна быть больше даты начала", "beginDate", "endDate");

        if (errorCollector.hasErrors())
            return;

        super.update(model);
    }

}