/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e32.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AdmissionStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 19.05.2009
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<AdmissionStuExtract, Model>
{
    @Override
    protected AdmissionStuExtract createNewInstance()
    {
        return new AdmissionStuExtract();
    }
    
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }
    
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        if(!model.isAddForm())
            model.setDebtsList(MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(model.getExtract()));
        
        if(model.getDebtsList().isEmpty())
            model.getDebtsList().add(new StuExtractToDebtRelation());
    }
    
    @Override
    public void update(Model model)
    {
        super.update(model);
        
        if(!model.getExtract().isHasDebts())
        {
            model.getExtract().setDeadline(null);
        }
        
        if(!model.getExtract().isPaymentsDebt())
        {
            model.getExtract().setPayingOffDate(null);
            model.getExtract().setDischargeOrderNumber(null);
            model.getExtract().setDischargeOrderDate(null);
        }
        
        for (StuExtractToDebtRelation rel : model.getDebtsList())
        {
            if (model.getExtract().isHasDebts())
            {
                rel.setExtract(model.getExtract());
                getSession().saveOrUpdate(rel);
            }
            else if(null != rel.getId())
                getSession().delete(rel);
        }
        
        for(StuExtractToDebtRelation rel : model.getDebtsToDel())
        {
            if(null != rel.getId()) getSession().delete(rel);
        }
    }
}