/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e37;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.IStudentListParagraphPrintFormatter;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author ListExtractComponentGenerator
 * @since 11.12.2012
 */
public class TerritorialCourseTransferStuListExtractPrint implements IPrintFormCreator<TerritorialCourseTransferStuListExtract>, IListParagraphPrintFormCreator<TerritorialCourseTransferStuListExtract>, IStudentListParagraphPrintFormatter
{
    @Override
    public RtfDocument createPrintForm(byte[] template, TerritorialCourseTransferStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public String formatSingleStudent(Student student, int extractNumber)
    {
        StringBuilder buffer = new StringBuilder();
        buffer.append("\\par ").append(extractNumber).append(".  ");
        buffer.append(student.getPerson().getFullFio());
        return buffer.toString();
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, TerritorialCourseTransferStuListExtract firstExtract)
    {
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);

        EducationOrgUnit educationOrgUnitOld = firstExtract.getEntity().getEducationOrgUnit();
        EducationOrgUnit educationOrgUnitNew = firstExtract.getEducationOrgUnitNew();

        CommonListExtractPrint.injectCommonListExtractData(injectModifier, firstExtract);
        CommonExtractPrint.initOrgUnit(injectModifier, educationOrgUnitOld, "formativeOrgUnitStr", "Old");
        CommonExtractPrint.initOrgUnit(injectModifier, educationOrgUnitNew, "formativeOrgUnitStr", "New");
        CommonExtractPrint.modifyEducationStr(injectModifier, educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel(), "Old");
        CommonExtractPrint.injectFefuEducationLevelNewText(injectModifier, new String[]{"educationStrNewOrSame_D", "fefuEducationStrNewOrSame_D"}, educationOrgUnitOld, educationOrgUnitNew);
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(injectModifier, firstExtract.getEducationOrgUnitOld(), CommonListOrderPrint.getEducationBaseText(firstExtract.getGroup()), "fefuShortFastExtendedOptionalText");

        return injectModifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, TerritorialCourseTransferStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, TerritorialCourseTransferStuListExtract firstExtract)
    {
    }
}