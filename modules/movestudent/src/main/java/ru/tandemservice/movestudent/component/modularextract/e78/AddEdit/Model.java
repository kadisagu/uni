/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e78.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.CommonExtractModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.TransferCompTypeExtStuExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.IExtEducationLevelModel;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 30.08.2012
 */
public class Model extends CommonModularStudentExtractAddEditModel<TransferCompTypeExtStuExtract> implements IExtEducationLevelModel
{
    private CommonExtractModel _eduModel;

    public CommonExtractModel getEduModel()
    {
        return _eduModel;
    }

    public void setEduModel(CommonExtractModel eduModel)
    {
        _eduModel = eduModel;
    }

    public boolean isEduLevelSpeciality()
    {
        StructureEducationLevels levelType = getParentEduLevel().getLevelType();
        return levelType.isSpecialty() || (levelType.isBachelor() && !levelType.isProfile()) || (levelType.isMaster() && !levelType.isProfile());
    }

    @Override
    public EducationLevels getParentEduLevel()
    {
        return EducationOrgUnitUtil.getParentLevel(getExtract().getEntity().getEducationOrgUnit().getEducationLevelHighSchool());
    }
}