/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.catalog.studentExtractType.StudentExtractTypePub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;

/**
 * @author AutoGenerator
 *         Created on 16.10.2008
 */
public class Controller extends DefaultCatalogPubController<StudentExtractType, Model, IDAO>
{
    @Override
    protected DynamicListDataSource<StudentExtractType> createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);
        boolean admin = UserContext.getInstance().getPrincipalContext().isAdmin();

        DynamicListDataSource<StudentExtractType> dataSource = new DynamicListDataSource<>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", admin ? StudentExtractType.titleWithCode().s() : StudentExtractType.title().s()).setTreeColumn(true));
        dataSource.addColumn(new SimpleColumn("Описание", StudentExtractType.description().s(), RawFormatter.INSTANCE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Группа приказов", "group").setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", StudentExtractType.P_TITLE).setDisabledProperty(StudentExtractType.P_DELETE_DISABLED).setPermissionKey(model.getCatalogItemDelete()));
        return dataSource;

    }
}
