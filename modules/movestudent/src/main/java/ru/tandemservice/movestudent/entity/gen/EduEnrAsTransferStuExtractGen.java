package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О зачислении в порядке перевода
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduEnrAsTransferStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract";
    public static final String ENTITY_NAME = "eduEnrAsTransferStuExtract";
    public static final int VERSION_HASH = 1549863337;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENTRY_DATE = "entryDate";
    public static final String P_UNIVERSITY = "university";
    public static final String P_COURSE_FROM = "courseFrom";
    public static final String P_FACULTY_FROM = "facultyFrom";
    public static final String P_DEVELOP_FORM_FROM = "developFormFrom";
    public static final String P_COMPENS_TYPE_FROM = "compensTypeFrom";
    public static final String P_APPLY_DATE = "applyDate";
    public static final String P_HAS_DEBTS = "hasDebts";
    public static final String P_DEADLINE = "deadline";
    public static final String P_PREV_ORDER_ENTRY_DATE = "prevOrderEntryDate";

    private Date _entryDate;     // Дата зачисления
    private String _university;     // Обучался (переводится) из ОУ
    private String _courseFrom;     // На каком курсе обучался
    private String _facultyFrom;     // На каком факультете (институте) обучался
    private String _developFormFrom;     // На какой форме обучения
    private String _compensTypeFrom;     // На какой основе обучения
    private Date _applyDate;     // Дата подачи заявления
    private boolean _hasDebts;     // Разница в учебных планах
    private Date _deadline;     // Срок ликвидации задолженности
    private Date _prevOrderEntryDate;     // Дата зачисления по предыдущему приказу о зачислении

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата зачисления.
     */
    public Date getEntryDate()
    {
        return _entryDate;
    }

    /**
     * @param entryDate Дата зачисления.
     */
    public void setEntryDate(Date entryDate)
    {
        dirty(_entryDate, entryDate);
        _entryDate = entryDate;
    }

    /**
     * @return Обучался (переводится) из ОУ. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getUniversity()
    {
        return _university;
    }

    /**
     * @param university Обучался (переводится) из ОУ. Свойство не может быть null.
     */
    public void setUniversity(String university)
    {
        dirty(_university, university);
        _university = university;
    }

    /**
     * @return На каком курсе обучался. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCourseFrom()
    {
        return _courseFrom;
    }

    /**
     * @param courseFrom На каком курсе обучался. Свойство не может быть null.
     */
    public void setCourseFrom(String courseFrom)
    {
        dirty(_courseFrom, courseFrom);
        _courseFrom = courseFrom;
    }

    /**
     * @return На каком факультете (институте) обучался. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFacultyFrom()
    {
        return _facultyFrom;
    }

    /**
     * @param facultyFrom На каком факультете (институте) обучался. Свойство не может быть null.
     */
    public void setFacultyFrom(String facultyFrom)
    {
        dirty(_facultyFrom, facultyFrom);
        _facultyFrom = facultyFrom;
    }

    /**
     * @return На какой форме обучения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDevelopFormFrom()
    {
        return _developFormFrom;
    }

    /**
     * @param developFormFrom На какой форме обучения. Свойство не может быть null.
     */
    public void setDevelopFormFrom(String developFormFrom)
    {
        dirty(_developFormFrom, developFormFrom);
        _developFormFrom = developFormFrom;
    }

    /**
     * @return На какой основе обучения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCompensTypeFrom()
    {
        return _compensTypeFrom;
    }

    /**
     * @param compensTypeFrom На какой основе обучения. Свойство не может быть null.
     */
    public void setCompensTypeFrom(String compensTypeFrom)
    {
        dirty(_compensTypeFrom, compensTypeFrom);
        _compensTypeFrom = compensTypeFrom;
    }

    /**
     * @return Дата подачи заявления. Свойство не может быть null.
     */
    @NotNull
    public Date getApplyDate()
    {
        return _applyDate;
    }

    /**
     * @param applyDate Дата подачи заявления. Свойство не может быть null.
     */
    public void setApplyDate(Date applyDate)
    {
        dirty(_applyDate, applyDate);
        _applyDate = applyDate;
    }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     */
    @NotNull
    public boolean isHasDebts()
    {
        return _hasDebts;
    }

    /**
     * @param hasDebts Разница в учебных планах. Свойство не может быть null.
     */
    public void setHasDebts(boolean hasDebts)
    {
        dirty(_hasDebts, hasDebts);
        _hasDebts = hasDebts;
    }

    /**
     * @return Срок ликвидации задолженности.
     */
    public Date getDeadline()
    {
        return _deadline;
    }

    /**
     * @param deadline Срок ликвидации задолженности.
     */
    public void setDeadline(Date deadline)
    {
        dirty(_deadline, deadline);
        _deadline = deadline;
    }

    /**
     * @return Дата зачисления по предыдущему приказу о зачислении.
     */
    public Date getPrevOrderEntryDate()
    {
        return _prevOrderEntryDate;
    }

    /**
     * @param prevOrderEntryDate Дата зачисления по предыдущему приказу о зачислении.
     */
    public void setPrevOrderEntryDate(Date prevOrderEntryDate)
    {
        dirty(_prevOrderEntryDate, prevOrderEntryDate);
        _prevOrderEntryDate = prevOrderEntryDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduEnrAsTransferStuExtractGen)
        {
            setEntryDate(((EduEnrAsTransferStuExtract)another).getEntryDate());
            setUniversity(((EduEnrAsTransferStuExtract)another).getUniversity());
            setCourseFrom(((EduEnrAsTransferStuExtract)another).getCourseFrom());
            setFacultyFrom(((EduEnrAsTransferStuExtract)another).getFacultyFrom());
            setDevelopFormFrom(((EduEnrAsTransferStuExtract)another).getDevelopFormFrom());
            setCompensTypeFrom(((EduEnrAsTransferStuExtract)another).getCompensTypeFrom());
            setApplyDate(((EduEnrAsTransferStuExtract)another).getApplyDate());
            setHasDebts(((EduEnrAsTransferStuExtract)another).isHasDebts());
            setDeadline(((EduEnrAsTransferStuExtract)another).getDeadline());
            setPrevOrderEntryDate(((EduEnrAsTransferStuExtract)another).getPrevOrderEntryDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduEnrAsTransferStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduEnrAsTransferStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new EduEnrAsTransferStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "entryDate":
                    return obj.getEntryDate();
                case "university":
                    return obj.getUniversity();
                case "courseFrom":
                    return obj.getCourseFrom();
                case "facultyFrom":
                    return obj.getFacultyFrom();
                case "developFormFrom":
                    return obj.getDevelopFormFrom();
                case "compensTypeFrom":
                    return obj.getCompensTypeFrom();
                case "applyDate":
                    return obj.getApplyDate();
                case "hasDebts":
                    return obj.isHasDebts();
                case "deadline":
                    return obj.getDeadline();
                case "prevOrderEntryDate":
                    return obj.getPrevOrderEntryDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "entryDate":
                    obj.setEntryDate((Date) value);
                    return;
                case "university":
                    obj.setUniversity((String) value);
                    return;
                case "courseFrom":
                    obj.setCourseFrom((String) value);
                    return;
                case "facultyFrom":
                    obj.setFacultyFrom((String) value);
                    return;
                case "developFormFrom":
                    obj.setDevelopFormFrom((String) value);
                    return;
                case "compensTypeFrom":
                    obj.setCompensTypeFrom((String) value);
                    return;
                case "applyDate":
                    obj.setApplyDate((Date) value);
                    return;
                case "hasDebts":
                    obj.setHasDebts((Boolean) value);
                    return;
                case "deadline":
                    obj.setDeadline((Date) value);
                    return;
                case "prevOrderEntryDate":
                    obj.setPrevOrderEntryDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entryDate":
                        return true;
                case "university":
                        return true;
                case "courseFrom":
                        return true;
                case "facultyFrom":
                        return true;
                case "developFormFrom":
                        return true;
                case "compensTypeFrom":
                        return true;
                case "applyDate":
                        return true;
                case "hasDebts":
                        return true;
                case "deadline":
                        return true;
                case "prevOrderEntryDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entryDate":
                    return true;
                case "university":
                    return true;
                case "courseFrom":
                    return true;
                case "facultyFrom":
                    return true;
                case "developFormFrom":
                    return true;
                case "compensTypeFrom":
                    return true;
                case "applyDate":
                    return true;
                case "hasDebts":
                    return true;
                case "deadline":
                    return true;
                case "prevOrderEntryDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "entryDate":
                    return Date.class;
                case "university":
                    return String.class;
                case "courseFrom":
                    return String.class;
                case "facultyFrom":
                    return String.class;
                case "developFormFrom":
                    return String.class;
                case "compensTypeFrom":
                    return String.class;
                case "applyDate":
                    return Date.class;
                case "hasDebts":
                    return Boolean.class;
                case "deadline":
                    return Date.class;
                case "prevOrderEntryDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduEnrAsTransferStuExtract> _dslPath = new Path<EduEnrAsTransferStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduEnrAsTransferStuExtract");
    }
            

    /**
     * @return Дата зачисления.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#getEntryDate()
     */
    public static PropertyPath<Date> entryDate()
    {
        return _dslPath.entryDate();
    }

    /**
     * @return Обучался (переводится) из ОУ. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#getUniversity()
     */
    public static PropertyPath<String> university()
    {
        return _dslPath.university();
    }

    /**
     * @return На каком курсе обучался. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#getCourseFrom()
     */
    public static PropertyPath<String> courseFrom()
    {
        return _dslPath.courseFrom();
    }

    /**
     * @return На каком факультете (институте) обучался. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#getFacultyFrom()
     */
    public static PropertyPath<String> facultyFrom()
    {
        return _dslPath.facultyFrom();
    }

    /**
     * @return На какой форме обучения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#getDevelopFormFrom()
     */
    public static PropertyPath<String> developFormFrom()
    {
        return _dslPath.developFormFrom();
    }

    /**
     * @return На какой основе обучения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#getCompensTypeFrom()
     */
    public static PropertyPath<String> compensTypeFrom()
    {
        return _dslPath.compensTypeFrom();
    }

    /**
     * @return Дата подачи заявления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#getApplyDate()
     */
    public static PropertyPath<Date> applyDate()
    {
        return _dslPath.applyDate();
    }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#isHasDebts()
     */
    public static PropertyPath<Boolean> hasDebts()
    {
        return _dslPath.hasDebts();
    }

    /**
     * @return Срок ликвидации задолженности.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#getDeadline()
     */
    public static PropertyPath<Date> deadline()
    {
        return _dslPath.deadline();
    }

    /**
     * @return Дата зачисления по предыдущему приказу о зачислении.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#getPrevOrderEntryDate()
     */
    public static PropertyPath<Date> prevOrderEntryDate()
    {
        return _dslPath.prevOrderEntryDate();
    }

    public static class Path<E extends EduEnrAsTransferStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<Date> _entryDate;
        private PropertyPath<String> _university;
        private PropertyPath<String> _courseFrom;
        private PropertyPath<String> _facultyFrom;
        private PropertyPath<String> _developFormFrom;
        private PropertyPath<String> _compensTypeFrom;
        private PropertyPath<Date> _applyDate;
        private PropertyPath<Boolean> _hasDebts;
        private PropertyPath<Date> _deadline;
        private PropertyPath<Date> _prevOrderEntryDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата зачисления.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#getEntryDate()
     */
        public PropertyPath<Date> entryDate()
        {
            if(_entryDate == null )
                _entryDate = new PropertyPath<Date>(EduEnrAsTransferStuExtractGen.P_ENTRY_DATE, this);
            return _entryDate;
        }

    /**
     * @return Обучался (переводится) из ОУ. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#getUniversity()
     */
        public PropertyPath<String> university()
        {
            if(_university == null )
                _university = new PropertyPath<String>(EduEnrAsTransferStuExtractGen.P_UNIVERSITY, this);
            return _university;
        }

    /**
     * @return На каком курсе обучался. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#getCourseFrom()
     */
        public PropertyPath<String> courseFrom()
        {
            if(_courseFrom == null )
                _courseFrom = new PropertyPath<String>(EduEnrAsTransferStuExtractGen.P_COURSE_FROM, this);
            return _courseFrom;
        }

    /**
     * @return На каком факультете (институте) обучался. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#getFacultyFrom()
     */
        public PropertyPath<String> facultyFrom()
        {
            if(_facultyFrom == null )
                _facultyFrom = new PropertyPath<String>(EduEnrAsTransferStuExtractGen.P_FACULTY_FROM, this);
            return _facultyFrom;
        }

    /**
     * @return На какой форме обучения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#getDevelopFormFrom()
     */
        public PropertyPath<String> developFormFrom()
        {
            if(_developFormFrom == null )
                _developFormFrom = new PropertyPath<String>(EduEnrAsTransferStuExtractGen.P_DEVELOP_FORM_FROM, this);
            return _developFormFrom;
        }

    /**
     * @return На какой основе обучения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#getCompensTypeFrom()
     */
        public PropertyPath<String> compensTypeFrom()
        {
            if(_compensTypeFrom == null )
                _compensTypeFrom = new PropertyPath<String>(EduEnrAsTransferStuExtractGen.P_COMPENS_TYPE_FROM, this);
            return _compensTypeFrom;
        }

    /**
     * @return Дата подачи заявления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#getApplyDate()
     */
        public PropertyPath<Date> applyDate()
        {
            if(_applyDate == null )
                _applyDate = new PropertyPath<Date>(EduEnrAsTransferStuExtractGen.P_APPLY_DATE, this);
            return _applyDate;
        }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#isHasDebts()
     */
        public PropertyPath<Boolean> hasDebts()
        {
            if(_hasDebts == null )
                _hasDebts = new PropertyPath<Boolean>(EduEnrAsTransferStuExtractGen.P_HAS_DEBTS, this);
            return _hasDebts;
        }

    /**
     * @return Срок ликвидации задолженности.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#getDeadline()
     */
        public PropertyPath<Date> deadline()
        {
            if(_deadline == null )
                _deadline = new PropertyPath<Date>(EduEnrAsTransferStuExtractGen.P_DEADLINE, this);
            return _deadline;
        }

    /**
     * @return Дата зачисления по предыдущему приказу о зачислении.
     * @see ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract#getPrevOrderEntryDate()
     */
        public PropertyPath<Date> prevOrderEntryDate()
        {
            if(_prevOrderEntryDate == null )
                _prevOrderEntryDate = new PropertyPath<Date>(EduEnrAsTransferStuExtractGen.P_PREV_ORDER_ENTRY_DATE, this);
            return _prevOrderEntryDate;
        }

        public Class getEntityClass()
        {
            return EduEnrAsTransferStuExtract.class;
        }

        public String getEntityName()
        {
            return "eduEnrAsTransferStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
