/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e82.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.RePassDiscStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToRePassDiscipline;
import ru.tandemservice.uni.entity.catalog.Term;

import java.util.Collection;
import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 31.08.2012
 */
public class Model extends CommonModularStudentExtractAddEditModel<RePassDiscStuExtract>
{
    List<StuExtractToRePassDiscipline> _disciplineList;
    StuExtractToRePassDiscipline _currentDiscipline;

    List<String> _controlActionsList;
    ISelectModel _eduYearList;
    Collection<Term> _termsList;

    public Integer getCurrentDisciplineNumber()
    {
        return _disciplineList.indexOf(getCurrentDiscipline());
    }

    public boolean isCurrentDisciplineLast()
    {
        return _disciplineList.size() - 1 == _disciplineList.indexOf(getCurrentDiscipline());
    }

    public List<StuExtractToRePassDiscipline> getDisciplineList()
    {
        return _disciplineList;
    }

    public void setDisciplineList(List<StuExtractToRePassDiscipline> disciplineList)
    {
        _disciplineList = disciplineList;
    }

    public StuExtractToRePassDiscipline getCurrentDiscipline()
    {
        return _currentDiscipline;
    }

    public void setCurrentDiscipline(StuExtractToRePassDiscipline currentDiscipline)
    {
        _currentDiscipline = currentDiscipline;
    }

    public List<String> getControlActionsList()
    {
        return _controlActionsList;
    }

    public void setControlActionsList(List<String> controlActionsList)
    {
        _controlActionsList = controlActionsList;
    }

    public ISelectModel getEduYearList()
    {
        return _eduYearList;
    }

    public void setEduYearList(ISelectModel eduYearList)
    {
        _eduYearList = eduYearList;
    }

    public Collection<Term> getTermsList()
    {
        return _termsList;
    }

    public void setTermsList(Collection<Term> termsList)
    {
        _termsList = termsList;
    }
}