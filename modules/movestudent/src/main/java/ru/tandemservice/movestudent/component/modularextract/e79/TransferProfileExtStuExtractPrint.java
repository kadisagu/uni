/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e79;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.TransferProfileExtStuExtract;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.*;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 30.08.2012
 */
public class TransferProfileExtStuExtractPrint implements IPrintFormCreator<TransferProfileExtStuExtract>
{
    private static final Map<String, String[]> PROFILE_STRING_CASES_ARRAY = new HashMap<>();
    private static final List<String> PROFILE_QUALIFICATION_CODES = new ArrayList<>();
    private static final List<String> SPEC_QUALIFICATION_CODES = new ArrayList<>();

    static
    {
        PROFILE_QUALIFICATION_CODES.add(QualificationsCodes.BAKALAVR);
        PROFILE_QUALIFICATION_CODES.add(QualificationsCodes.MAGISTR);
        SPEC_QUALIFICATION_CODES.add(QualificationsCodes.SPETSIALIST);
        SPEC_QUALIFICATION_CODES.add(QualificationsCodes.BAZOVYY_UROVEN_S_P_O);
        SPEC_QUALIFICATION_CODES.add(QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O);
        PROFILE_STRING_CASES_ARRAY.put(QualificationsCodes.BAKALAVR, CommonExtractPrint.PROFILE_CASES);
        PROFILE_STRING_CASES_ARRAY.put(QualificationsCodes.MAGISTR, CommonExtractPrint.MASTER_PROGRAM_CASES);
        PROFILE_STRING_CASES_ARRAY.put(QualificationsCodes.SPETSIALIST, CommonExtractPrint.SPECIALIZATION_CASES);
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, TransferProfileExtStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(extract.getEducationOrgUnitOld().getEducationLevelHighSchool().getEducationLevel());

        if (null != qualification && PROFILE_QUALIFICATION_CODES.contains(qualification.getCode()))
        {
            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
                modifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), PROFILE_STRING_CASES_ARRAY.get(qualification.getCode())[i]);
        } else if (null != qualification)
        {
            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
                modifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), PROFILE_STRING_CASES_ARRAY.get(QualificationsCodes.SPETSIALIST)[i]);
        } else
        {
            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
                modifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), "");
        }

        CommonExtractPrint.initEducationType(modifier, extract.getEducationOrgUnitOld(), "DirectionOld", true, false);
        CommonExtractPrint.initEducationType(modifier, extract.getEducationOrgUnitNew(), "ProfileNew", false, true);

        if (null == qualification || SPEC_QUALIFICATION_CODES.contains(qualification.getCode()))
        {
            modifier.put("eduLevelTextAlt", "вышеуказанной специальности");
            modifier.put("eduLevelTextAltOther", "со специализации на специализацию");
        } else
        {
            modifier.put("eduLevelTextAlt", "вышеуказанному направлению");
            if (QualificationsCodes.BAKALAVR.equals(qualification.getCode()))
                modifier.put("eduLevelTextAltOther", "с профиля на профиль");
            else if (QualificationsCodes.MAGISTR.equals(qualification.getCode()))
                modifier.put("eduLevelTextAltOther", "с программы на программу");
        }

        CommonExtractPrint.injectFefuEducationLevelNewText(modifier, "fefuEduLevelText", extract.getEducationOrgUnitOld(), extract.getEducationOrgUnitNew());

        if (extract.isLiquidateEduPlanDifference())
        {
            String liquidationDeadlineDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getLiquidationDeadlineDate());
            String liquidateEduPlanDifference = ApplicationRuntime.getProperty("liquidateEduPlanDifferenceDot").replaceAll("\\{deadlineDate\\}", liquidationDeadlineDate);
            modifier.put("liquidateEduPlanDifference", liquidateEduPlanDifference);

        }
        else
        {
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("liquidateEduPlanDifference"), true, false);
        }

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}