/* $Id$ */
package ru.tandemservice.movestudent.component.commons;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder.*;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.mq.UniDQLExpressions;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 28.11.2013
 */
public class OrderFlexBuilder
{
    public static final String COMMIT_DATE = "commitDate";                               // Дата приказа
    public static final String CREATE_DATE = "createDate";                               // Дата формирования приказа (выписки)
    public static final String CREATE_DATE_FROM = "createDateFrom";                      // Дата формирования приказа (выписки) с
    public static final String CREATE_DATE_TO = "createDateTo";                          // Дата формирования приказа (выписки) по
    public static final String COMMIT_DATE_FROM = "commitDateFrom";                      // Дата приказа с
    public static final String COMMIT_DATE_TO = "commitDateTo";                          // Дата приказа по
    public static final String COMMIT_DATE_SYSTEM = "commitDateSystem";                  // Дата проведения приказа
    public static final String COMMIT_DATE_SYSTEM_FROM = "commitDateSystemFrom";         // Дата проведения приказа с
    public static final String COMMIT_DATE_SYSTEM_TO = "commitDateSystemTo";             // Дата проведения приказа по
    public static final String EDUCATION_YEAR = "educationYear";                         // Учебный год
    public static final String ORDER_NUMBER = "number";                                  // № приказа
    public static final String EXECUTOR = "executor";
    public static final String ORDER_STATE = "state";

    public static final String DISABLE_PRINT = "disabledPrint";
    public static final String DISABLE_PRINT_PDF = "disabledPrintPdf";
    public static final String ORDER_DESCRIPTION = "orderDescription";
    public static final String EXTRACT_COUNT = "extractCount";
    public static final String PARAGRAPH_COUNT = "parCount";
    public static final String SINGLE_EXTRACT = "singleExtract";
    public static final String IS_OTHER_ORDER = "isOtherOrder";
    public static final String IS_INDIVIDUAL_ORDER = "isIndividualOrder";
    public static final String OTHER_ORDER_CAN_PRINT = "otherCanPrint";
    public static final String OTHER_ORDER_COMMENT = "otherComment";
    public static final String ORDER_CATEGORY = "orderCategory";
    public static final String EXTRACT_TYPE = "type";
    public static final String ORDER_REASON = "reason";

    public static final String STUDENTS = "students";
    public static final String FORMATIVE_ORGUNIT = "formativeOrgUnit";
    public static final String TERRITORIAL_ORGUNIT = "territorialOrgUnit";
    public static final String PRODUCING_ORGUNIT = "producingOrgUnit";
    public static final String EDUCATION_LEVEL_HIGH_SCHOOL = "educationLevelHighSchool";
    public static final String COURSE = "course";
    public static final String DEVELOP_FORM = "developForm";
    public static final String COMPENSATION_TYPE = "compensationType";

    public static final String ORDER_ALIAS = "ord";
    public static final String EXTRACT_ALIAS = "extr";
    public static final String PARAGRAPH_ALIAS = "par";
    public static final String STUDENT_ALIAS = "stu";
    public static final String EDUCATION_ORGUNIT_ALIAS = "eou";
    public static final String HIGH_SCHOOL_ALIAS = "hs";
    public static final String ORDER_DATA_JOIN_ALIAS = "ordAttr";

    public static final IFlexJoin STUDENT_JOIN = new SimpleFlexJoin(STUDENT_ALIAS, EXTRACT_ALIAS, IAbstractExtract.L_ENTITY);
    public static final IFlexJoin EDUCATION_ORGUNIT_JOIN = new SimpleFlexJoin(EDUCATION_ORGUNIT_ALIAS, STUDENT_ALIAS, Student.L_EDUCATION_ORG_UNIT);
    public static final IFlexJoin EDUCATION_LEVELS_HIGH_SCHOOL_JOIN = new SimpleFlexJoin(HIGH_SCHOOL_ALIAS, EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL);
    public static final IFlexFilter STUDENT_FILTER = new EntityFlexFilter(STUDENTS, EXTRACT_ALIAS, IAbstractExtract.L_ENTITY);
    public static final IFlexFilter FORMATIVE_ORG_UNIT_FILTER = new EntityFlexFilter(FORMATIVE_ORGUNIT, EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_FORMATIVE_ORG_UNIT);
    public static final IFlexFilter TERRITORIAL_ORG_UNIT_FILTER = new EntityFlexFilter(TERRITORIAL_ORGUNIT, EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT);
    public static final IFlexFilter PRODUCING_ORG_UNIT_FILTER = new EntityFlexFilter(PRODUCING_ORGUNIT, HIGH_SCHOOL_ALIAS, EducationLevelsHighSchool.L_ORG_UNIT);
    public static final IFlexFilter EDUCATION_LEVEL_HIGH_SCHOOL_FILTER = new EntityFlexFilter(EDUCATION_LEVEL_HIGH_SCHOOL, EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL);
    public static final IFlexFilter COURSE_FILTER = new EntityFlexFilter(COURSE, STUDENT_ALIAS, Student.L_COURSE);
    public static final IFlexFilter DEVELOP_FORM_FILTER = new EntityFlexFilter(DEVELOP_FORM, EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_DEVELOP_FORM);
    public static final IFlexFilter COMPENSATION_TYPE_FILTER = new EntityFlexFilter(COMPENSATION_TYPE, STUDENT_ALIAS, Student.L_COMPENSATION_TYPE);
    public static final IFlexFilter EXTRACT_TYPE_FILTER = new EntityFlexFilter(EXTRACT_TYPE, EXTRACT_ALIAS, IAbstractExtract.L_TYPE)
    {
        @Override
        protected IDQLExpression internalCreateCondition(Object o)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentExtractType.class, "t")
                    .column("t.id");

            if (o instanceof Collection)
                builder.where(or(in("t.id", (Collection) o), in("t." + StudentExtractType.L_PARENT, (Collection) o)));
            else
                builder.where(or(eq(property("t.id"), commonValue(o)), eq(property("t", StudentExtractType.L_PARENT), commonValue(o))));

            return in(property(getAlias(), getPath()), builder.buildQuery());
        }
    };
    public static final IFlexFilter EXECUTOR_FILTER = new EntityFlexFilter(EXECUTOR, ORDER_ALIAS, AbstractStudentOrder.L_EXECUTOR_EMPL)
    {
        @Override
        protected IDQLExpression internalCreateCondition(Object o)
        {
            IdentityCard card = ((EmployeePost) o).getPerson().getIdentityCard();
            String lastName = card.getLastName();
            String firstName = card.getFirstName();
            String middleName = card.getMiddleName();

            StringBuilder str = new StringBuilder();
            str.append(firstName.charAt(0)).append(".");
            if (StringUtils.isNotEmpty(middleName))
                str.append(middleName.charAt(0)).append(".");
            str.append(" ").append(lastName);

            return or(super.internalCreateCondition(o), likeUpper(property(getAlias(), AbstractStudentOrder.P_EXECUTOR), value(CoreStringUtils.escapeLike(str.toString()))));
        }
    };

    public static final IFlexColumn ORDER_CREATE_DATE_COLUMN = new SimpleFlexColumn(ORDER_ALIAS, IAbstractOrder.P_CREATE_DATE, CREATE_DATE);
    public static final IFlexColumn ORDER_COMMIT_DATE_COLUMN = new SimpleFlexColumn(ORDER_ALIAS, AbstractStudentOrder.P_COMMIT_DATE, COMMIT_DATE);
    public static final IFlexColumn ORDER_COMMIT_DATE_SYSTEM_COLUMN = new SimpleFlexColumn(ORDER_ALIAS, IAbstractOrder.P_COMMIT_DATE_SYSTEM, COMMIT_DATE_SYSTEM);
    public static final IFlexColumn ORDER_NUMBER_COLUMN = new SimpleFlexColumn(ORDER_ALIAS, IAbstractOrder.P_NUMBER, ORDER_NUMBER).addViewNames("delete");
    public static final IFlexColumn ORDER_STATE_COLUMN = new SimpleFlexColumn(ORDER_ALIAS, IAbstractOrder.L_STATE + "." + OrderStates.P_TITLE, ORDER_STATE);
    public static final IFlexColumn ORDER_EXECUTOR_COLUMN = new SimpleFlexColumn(ORDER_ALIAS, AbstractStudentOrder.P_EXECUTOR, EXECUTOR);
    public static final IFlexColumn EXTRACT_COUNT_COLUMN = new SimpleFlexColumn(ORDER_DATA_JOIN_ALIAS, EXTRACT_COUNT)
    {
        @Override
        public void deploy(DataWrapper wrapper, Object value)
        {
            // Для тех приказов, у которых нет выписок не будет приджойненого количества выписок, поэтому null заменяем на "0"
            wrapper.setProperty(getName(), value != null ? value : "0");
        }
    }.setCanBeHidden(false); // кол-во выписок иногда нужно не только для отображения в колонке, поэтому пусть всегда будет.

    public static final IFlexOrder ORDER_CREATE_DATE_SORT = new FlexOrder(ORDER_CREATE_DATE_COLUMN, OrderDirection.desc);
    public static final IFlexOrder ORDER_COMMIT_DATE_SORT = new FlexOrder(ORDER_COMMIT_DATE_COLUMN, OrderDirection.desc);
    public static final IFlexOrder ORDER_COMMIT_SYSTEM_DATE_SORT = new FlexOrder(ORDER_COMMIT_DATE_SYSTEM_COLUMN, OrderDirection.desc);
    public static final IFlexOrder ORDER_NUMBER_SORT = new FlexOrder(ORDER_NUMBER_COLUMN).addSubOrders(ORDER_CREATE_DATE_SORT);
    public static final IFlexOrder ORDER_EXECUTOR_SORT = new FlexOrder(ORDER_EXECUTOR_COLUMN).addSubOrders(ORDER_NUMBER_SORT);
    public static final IFlexOrder ORDER_EXTRACT_COUNT_SORT = new FlexOrder(EXTRACT_COUNT, ORDER_DATA_JOIN_ALIAS, EXTRACT_COUNT, OrderDirection.desc, ORDER_NUMBER_SORT);

    public static final IFlexFilter ORDER_FINISHED_FILTER = new StaticFlexFilter(AbstractStudentOrder.state().code().s(), ORDER_ALIAS, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED);
    public static final IFlexFilter ORDER_EDU_YEAR_FILTER = new BaseFlexFilter(EDUCATION_YEAR, ORDER_ALIAS, IAbstractOrder.P_CREATE_DATE)
    {
        @Override
        protected IDQLExpression internalCreateCondition(Object o)
        {
            return UniDQLExpressions.eqEduYear(getAlias(), getPath(), (EducationYear) o);
        }
    };
    public static final IFlexFilter ORDER_COMMIT_DATE_PERIOD_FILTER = new DatePeriodFlexFilter(COMMIT_DATE_FROM, COMMIT_DATE_TO, ORDER_ALIAS, AbstractStudentOrder.P_COMMIT_DATE);
    public static final IFlexFilter ORDER_COMMIT_DATE_SYSTEM_PERIOD_FILTER = new DatePeriodFlexFilter(COMMIT_DATE_SYSTEM_FROM, COMMIT_DATE_SYSTEM_TO, ORDER_ALIAS, IAbstractOrder.P_COMMIT_DATE_SYSTEM);
    public static final IFlexFilter ORDER_CREATE_DATE_PERIOD_FILTER = new DatePeriodFlexFilter(CREATE_DATE_FROM, CREATE_DATE_TO, ORDER_ALIAS, IAbstractOrder.P_CREATE_DATE);
    public static final IFlexFilter ORDER_NUMBER_FILTER = new StringFlexFilter(ORDER_NUMBER, ORDER_ALIAS, IAbstractOrder.P_NUMBER);
    public static final IFlexFilter ORDER_STATE_FILTER = new EntityFlexFilter(ORDER_STATE, ORDER_ALIAS, IAbstractOrder.L_STATE);

    public static class OrdersDataJoin extends DataSourceFlexJoin
    {
        private boolean modularOrders;
        private boolean individualOrders;
        private boolean listOrders;
        private boolean otherOrders;

        private boolean parCountColumn;
        private boolean singleExtractColumn;
        private boolean isOtherColumn;
        private boolean isIndividualColumn;

        public OrdersDataJoin(Collection<IEntity> orderCategories, DQLJoinType joinType, boolean includeParCountColumn, boolean includeSingleExtractColumn, boolean includeIsOtherColumn,
                              boolean includeIsIndividualColumn)
        {
            super(ORDER_DATA_JOIN_ALIAS, ORDER_ALIAS, joinType, true);

            boolean all = null == orderCategories || orderCategories.isEmpty();
            modularOrders = all || orderCategories.contains(OrderCategory.MODULAR_ORDER_CATEGORY);
            individualOrders = all || orderCategories.contains(OrderCategory.INDIVIDUAL_ORDER_CATEGORY);
            listOrders = all || orderCategories.contains(OrderCategory.LIST_ORDER_CATEGORY);
            otherOrders = all || orderCategories.contains(OrderCategory.OTHERS_ORDER_CATEGORY);

            parCountColumn = includeParCountColumn;
            singleExtractColumn = includeSingleExtractColumn;
            isOtherColumn = includeIsOtherColumn;
            isIndividualColumn = includeIsIndividualColumn;
        }

        @Override
        protected IDQLDataSource createDataSource()
        {
            Class<? extends IEntity> baseExtractClass;
            boolean all = false;
            boolean modular = modularOrders || individualOrders;
            if (modular && !listOrders && !otherOrders)
                baseExtractClass = ModularStudentExtract.class;
            else if (listOrders && !modular && !otherOrders)
                baseExtractClass = ListStudentExtract.class;
            else if (otherOrders && !modular && !listOrders)
                baseExtractClass = OtherStudentExtract.class;
            else
            {
                baseExtractClass = AbstractStudentExtract.class;
                all = true;
            }

            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(baseExtractClass, "e")
                    .column(property("p", IAbstractParagraph.L_ORDER), "id")
                    .joinPath(DQLJoinType.inner, "e." + IAbstractExtract.L_PARAGRAPH, "p")
                    .group(property("p", IAbstractParagraph.L_ORDER));

            // Количество выписок в приказе
            builder.column(DQLFunctions.countStar(), EXTRACT_COUNT);

            // Количество параграфов в приказе
            if (parCountColumn)
                builder.column(DQLFunctions.count(DQLPredicateType.distinct, property("p.id")), PARAGRAPH_COUNT);

            if (singleExtractColumn)
            {
                // Для колонки с примечанием, надо брать единственную выписку, если она в приказе единственная,
                // поэтому просто прицепляем минимальный идентификатор выписки в рамках группировки.
                builder.column(DQLFunctions.min(property("e.id")), SINGLE_EXTRACT);
            }

            if (all)
            {
                if (modular)
                    builder.joinEntity("e", DQLJoinType.left, ModularStudentExtract.class, "me", eq("e.id", "me.id"));
                else
                    builder.where(notIn("e.id", new DQLSelectBuilder().fromEntity(ModularStudentExtract.class, "me").column("me.id").buildQuery()));

                if (otherOrders)
                {
                    builder.joinEntity("e", DQLJoinType.left, OtherStudentExtract.class, "oe", eq("e.id", "oe.id"));

                    if (isOtherColumn)
                        builder.column(caseExpr(
                                new IDQLExpression[]{isNotNull(DQLFunctions.min(property("oe.id")))},
                                new IDQLExpression[]{value(Boolean.TRUE)},
                                value(Boolean.FALSE)
                        ), IS_OTHER_ORDER);

                    builder.column(property("oe", OtherStudentExtract.P_COMMENT), OTHER_ORDER_COMMENT);
                }
                else
                {
                    builder.where(notIn("e.id", new DQLSelectBuilder().fromEntity(OtherStudentExtract.class, "oe").column("oe.id").buildQuery()));

                    // Колонку с признаком прочего приказа заполняем полностью false
                    if (isOtherColumn)
                        builder.column(value(Boolean.FALSE), IS_OTHER_ORDER);
                }

                if (!listOrders)
                    builder.where(notIn("e.id", new DQLSelectBuilder().fromEntity(ListStudentExtract.class, "le").column("le.id").buildQuery()));
            }
            else
            {
                // Колонку с признаком прочего приказа заполняем полностью true или false
                if (isOtherColumn)
                    builder.column(value(otherOrders), IS_OTHER_ORDER);

                if (otherOrders)
                    builder.column(property("e", OtherStudentExtract.P_COMMENT), OTHER_ORDER_COMMENT);
            }

            if (otherOrders)
            {
                // Можно завязаться на то, что у прочего приказа всегда одна выписка, тогда MIN() вернет единственное значение при группировке по приказу
                builder.joinEntity("p", DQLJoinType.left, StudentOtherOrderTextRelation.class, "rel",
                                   eq("p." + IAbstractParagraph.L_ORDER, "rel." + StudentOtherOrderTextRelation.L_ORDER));

                // Есть ли печатная форма для прочего приказа
                builder.column(caseExpr(
                        new IDQLExpression[]{isNotNull(DQLFunctions.min(property("rel.id")))},
                        new IDQLExpression[]{value(Boolean.TRUE)},
                        value(Boolean.FALSE)
                ), OTHER_ORDER_CAN_PRINT);
            }

            if (modular && (individualOrders != modularOrders) || isIndividualColumn)
            {
                // Оставляем либо только сборные, либо только индивидуальные

                // Индивидуальные приказы - это сборные приказы с единственной выпиской, тип которой имеет признак индивидуальности
                // поэтому можно поступать также, как с колонкой достающей первую (единственную) выписку из приказа
                builder.joinEntity("e", DQLJoinType.left, ExtractCreationRule.class, "rule",
                                   eq("e." + IAbstractExtract.L_TYPE, "rule." + ExtractCreationRule.L_STUDENT_EXTRACT_TYPE));

                IDQLExpression expr;
                if (individualOrders)
                    expr = and(eq(DQLFunctions.countStar(), value(1)),
                               eq(DQLFunctions.min(DQLFunctions.cast(property("rule." + ExtractCreationRule.P_INDIVIDUAL_ORDER), PropertyType.INTEGER)), value(1)));
                else
                    expr = or(ne(DQLFunctions.countStar(), value(1)),
                              eq(DQLFunctions.coalesce(
                                      DQLFunctions.min(DQLFunctions.cast(property("rule." + ExtractCreationRule.P_INDIVIDUAL_ORDER), PropertyType.INTEGER)),
                                      value(0)
                              ), value(0)));

                if (all)
                    expr = or(isNull(DQLFunctions.min(property("me.id"))), expr);

                // Колонка isIndividualColumn нужна для, к примеру, списку сборных приказов на формировании - в этом списке есть
                // пустые приказы, поэтому просто приджойнить к Modular-приказам все только сборные приказы не получится,
                // т.к. нужен left join, который слева оставит и индивидуальные приказы. Для этого случая в приджойнином датасорсе
                // мы добавляем булевую колонку с признаком индивидуальность. Иначе же - просто ограничиваем выборку только сборными
                // или толко индивидуальными приказами.

                if (!isIndividualColumn)
                {
                    builder.having(expr);
                }
                else
                {
                    // Если нужна колонка c признаком индивидуальности, в выборке должны быть и сборные,
                    // и индивидуальные приказы. Иначе нет смысла в колонке.
                    if (individualOrders != modularOrders)
                        throw new IllegalStateException();

                    builder.column(caseExpr(
                            new IDQLExpression[]{expr},
                            new IDQLExpression[]{value(individualOrders)},
                            value(!individualOrders)
                    ), IS_INDIVIDUAL_ORDER);
                }
            }

            return builder.buildQuery();
        }
    }
}