/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.logic;


import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.entity.ExportMSRSettings;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 18.12.2014
 */
public interface IMovestudentMSRSettingsDao
{

    Map<OrgUnit, String> getMapCodesForFormativeOrgUnit();

    void saveOrUpdateCodesForFormativeOrgUnit(Map<OrgUnit, String> mapCodes);

    ExportMSRSettings getSettings();

    void saveSettings(ExportMSRSettings settings);

    Map<StudentStatus, Long> getMapStudentStatusCodes();

    void saveOrUpdateStudentStatusCodes(Map<StudentStatus, Long> mapCodes);

    Map<StudentExtractType, Integer> getMapOrdersCodeRelation();

    void saveOrUpdateOrdersCodesRelation(Map<StudentExtractType, Integer> mapCodes);

    Map<Long, Integer> getTerritorialOrgUnitAddresses(OrgUnit formativeOU);
}
