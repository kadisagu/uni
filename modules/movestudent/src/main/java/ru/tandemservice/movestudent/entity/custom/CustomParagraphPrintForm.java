package ru.tandemservice.movestudent.entity.custom;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.component.customorder.ICustomOrderDao;
import ru.tandemservice.movestudent.component.customorder.ICustomPrintFormContainer;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.custom.gen.CustomParagraphPrintFormGen;

/**
 * Печатная форма параграфа кастомного приказа, загруженная пользователем
 */
public class CustomParagraphPrintForm extends CustomParagraphPrintFormGen implements ICustomPrintFormContainer
{
    @Override
    public byte[] getBasicTemplate()
    {
        StudentExtractType type = getParagraph().getAnyExtractType();
        return MoveStudentDaoFacade.getMoveStudentDao().getTemplate(type, MoveStudentDefines.PARAGRAPH_TEXT_CODE);
    }

    @Override
    public Long getModifiableOrderId()
    {
        return ICustomOrderDao.instance.get().getModifiableOrderId(getParagraph());
    }
}