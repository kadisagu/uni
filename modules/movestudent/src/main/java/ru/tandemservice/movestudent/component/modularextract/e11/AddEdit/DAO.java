/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e11.AddEdit;

import org.tandemframework.core.exception.ApplicationException;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.RevertOrderStuExtract;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 21.11.2008
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<RevertOrderStuExtract, Model>
{
    @Override
    protected RevertOrderStuExtract createNewInstance()
    {
        return new RevertOrderStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.GENITIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        Long lastCommitedExtractId = MoveStudentDaoFacade.getMoveStudentDao().getLastCommittedExtractId(model.getExtract().getEntity().getId());
        if (lastCommitedExtractId == null)
            model.setErrorTitle("У студента нет проведенных выписок.");
        else
        {
            AbstractStudentExtract canceledExtract = getNotNull(lastCommitedExtractId);
            if (!canceledExtract.getType().getParent().getCode().equals(StudentExtractTypeCodes.MODULAR_ORDER))
                model.setErrorTitle("Последняя проведенная выписка не из сборного приказа.");
            else
                model.getExtract().setCanceledExtract(canceledExtract);
        }
    }

    @Override
    public void update(Model model)
    {
        if (model.isAddForm())
        {
            if (model.getExtract().getCanceledExtract() == null)
                throw new ApplicationException("Невозможно создать выписку «Об отмене приказа», так как изменяемая выписка не указана.");

            Long lastCommitedExtractId = MoveStudentDaoFacade.getMoveStudentDao().getLastCommittedExtractId(model.getExtract().getEntity().getId());
            if (lastCommitedExtractId == null)
                throw new ApplicationException("Невозможно создать выписку «Об отмене приказа», так как у объекта нет проведенных выписок.");

            if (!model.getExtract().getCanceledExtract().getId().equals(lastCommitedExtractId))
                throw new ApplicationException("Невозможно создать выписку «Об отмене приказа», так как изменяемая выписка не является последней проведенной.");
        }
        super.update(model);
    }
}
