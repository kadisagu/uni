package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.entity.gen.AttestationPracticWithServiceRecStuExtractGen;

/**
 * Выписка из сборного приказа по студенту. Об аттестации практик студентов имеющих стаж работы по профилю обучения
 */
public class AttestationPracticWithServiceRecStuExtract extends AttestationPracticWithServiceRecStuExtractGen
{
}