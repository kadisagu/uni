/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.EducationLevelDAO;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.Arrays;
import java.util.List;

/**
 * @author vip_delete
 * @since 27.10.2008
 */
public interface MoveStudentDefines
{
    // SearchList Fields
    String[] STUDENT_FULL_FIO = {IAbstractExtract.L_ENTITY, Student.L_PERSON, Person.P_FULLFIO};
    String[] EXTRACT_ORDER_TYPE_TITLE = {IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, IAbstractOrder.L_TYPE, StudentExtractType.P_TITLE};
    String[] STUDENT_EXTRACT_FORMATIVE_ORG_UNIT_FULL_TITLE = {IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_FULL_TITLE};
    String[] STUDENT_EXTRACT_TERRITORIAL_ORG_UNIT_FULL_TITLE = {IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_FULL_TITLE};
    String[] STUDENT_EXTRACT_PRODUCING_ORG_UNIT_FULL_TITLE = {IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.L_ORG_UNIT, OrgUnit.P_FULL_TITLE};
    String[] STUDENT_EXTRACT_EDUCATION_ORG_UNIT_DISPLAYABLE_TITLE = {IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY};

    List<String> CUSTOM_EXTRACT_TYPES = Arrays.asList(
			StudentExtractTypeCodes.CUSTOM_REVERT_MODULAR_ORDER,
            StudentExtractTypeCodes.CUSTOM_EDIT_MODULAR_ORDER,
            StudentExtractTypeCodes.CUSTOM_ADD_MODULAR_ORDER,
            StudentExtractTypeCodes.CUSTOM_REVERT_LIST_ORDER,
            StudentExtractTypeCodes.CUSTOM_EDIT_LIST_ORDER,
            StudentExtractTypeCodes.CUSTOM_ADD_LIST_ORDER,
            StudentExtractTypeCodes.CUSTOM_REVERT_LIST_EXTRACT,
            StudentExtractTypeCodes.CUSTOM_EDIT_LIST_EXTRACT,
            StudentExtractTypeCodes.CUSTOM_ADD_LIST_EXTRACT
    );

    String CUSTOM_ACTIONS_COMPONENT_MAP = "movestudentModifyExtractCustomActions";
    String MODULAR_CUSTOM_ACTIONS_ENTRY_SET = "movestudentModularOrderCustomActions";
    String LIST_CUSTOM_ACTIONS_ENTRY_SET = "movestudentListOrderCustomActions";

    // Extracts
    int EMPTY_MODULAR_STUDENT_EXTRACT_INDEX = 0;

    // Text Codes (for StudentExtractTextRelation and for MovestudentTemplate)
    int ORDER_TEXT_CODE = 1; // код для приказа
    int PARAGRAPH_TEXT_CODE = 1; // код для параграфа
    int EXTRACT_TEXT_CODE = 1; // код для текста выписки
    int EXTRACT_IN_ORDER_TEXT_CODE = 2; // код для текста выписки для текста приказа
    int LIST_EXTRACT_TEXT_CODE = 2; // код для текста выписки из списочного приказа
    int INDIVIDUAL_ORDER_TEXT_CODE = 3; // код для индивидуального приказа
}
