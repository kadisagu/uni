/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.movestudent.component.modularextract.ICommonExtractUtil;

/**
 * @author vip_delete
 * @since 18.10.2008
 */
public class MoveStudentDaoFacade
{
    private static IMoveStudentDao _moveStudentDao;
    private static ICommonExtractUtil _commonExtractUtil;
    private static IGeneratedCommentForStudentOrders _generatedComment;

    public static IMoveStudentDao getMoveStudentDao()
    {
        if (_moveStudentDao == null)
            _moveStudentDao = (IMoveStudentDao) ApplicationRuntime.getBean(IMoveStudentDao.MOVE_STUDENT_DAO_BEAN_NAME);
        return _moveStudentDao;
    }

    public static ICommonExtractUtil getCommonExtractUtil()
    {
        if (_commonExtractUtil == null)
            _commonExtractUtil = (ICommonExtractUtil) ApplicationRuntime.getBean(ICommonExtractUtil.class.getName());
        return _commonExtractUtil;
    }

    public static IGeneratedCommentForStudentOrders getGenerateComment()
    {
        if (_generatedComment == null)
            _generatedComment = (IGeneratedCommentForStudentOrders) ApplicationRuntime.getBean(IGeneratedCommentForStudentOrders.GENERATED_COMMENT_FOR_STUDENT_ORDERS_BEAN_NAME);
        return _generatedComment;
    }
}
