package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ChangePassDiplomaWorkPeriodStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О переносе сроков сдачи защиты дипломной работы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChangePassDiplomaWorkPeriodStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ChangePassDiplomaWorkPeriodStuExtract";
    public static final String ENTITY_NAME = "changePassDiplomaWorkPeriodStuExtract";
    public static final int VERSION_HASH = 912966485;
    private static IEntityMeta ENTITY_META;

    public static final String P_CURRENT_DATE = "currentDate";
    public static final String P_NEW_DATE = "newDate";

    private Date _currentDate;     // Текущая дата
    private Date _newDate;     // Новая дата

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Текущая дата. Свойство не может быть null.
     */
    @NotNull
    public Date getCurrentDate()
    {
        return _currentDate;
    }

    /**
     * @param currentDate Текущая дата. Свойство не может быть null.
     */
    public void setCurrentDate(Date currentDate)
    {
        dirty(_currentDate, currentDate);
        _currentDate = currentDate;
    }

    /**
     * @return Новая дата. Свойство не может быть null.
     */
    @NotNull
    public Date getNewDate()
    {
        return _newDate;
    }

    /**
     * @param newDate Новая дата. Свойство не может быть null.
     */
    public void setNewDate(Date newDate)
    {
        dirty(_newDate, newDate);
        _newDate = newDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ChangePassDiplomaWorkPeriodStuExtractGen)
        {
            setCurrentDate(((ChangePassDiplomaWorkPeriodStuExtract)another).getCurrentDate());
            setNewDate(((ChangePassDiplomaWorkPeriodStuExtract)another).getNewDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChangePassDiplomaWorkPeriodStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChangePassDiplomaWorkPeriodStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new ChangePassDiplomaWorkPeriodStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "currentDate":
                    return obj.getCurrentDate();
                case "newDate":
                    return obj.getNewDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "currentDate":
                    obj.setCurrentDate((Date) value);
                    return;
                case "newDate":
                    obj.setNewDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "currentDate":
                        return true;
                case "newDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "currentDate":
                    return true;
                case "newDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "currentDate":
                    return Date.class;
                case "newDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChangePassDiplomaWorkPeriodStuExtract> _dslPath = new Path<ChangePassDiplomaWorkPeriodStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChangePassDiplomaWorkPeriodStuExtract");
    }
            

    /**
     * @return Текущая дата. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangePassDiplomaWorkPeriodStuExtract#getCurrentDate()
     */
    public static PropertyPath<Date> currentDate()
    {
        return _dslPath.currentDate();
    }

    /**
     * @return Новая дата. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangePassDiplomaWorkPeriodStuExtract#getNewDate()
     */
    public static PropertyPath<Date> newDate()
    {
        return _dslPath.newDate();
    }

    public static class Path<E extends ChangePassDiplomaWorkPeriodStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _currentDate;
        private PropertyPath<Date> _newDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Текущая дата. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangePassDiplomaWorkPeriodStuExtract#getCurrentDate()
     */
        public PropertyPath<Date> currentDate()
        {
            if(_currentDate == null )
                _currentDate = new PropertyPath<Date>(ChangePassDiplomaWorkPeriodStuExtractGen.P_CURRENT_DATE, this);
            return _currentDate;
        }

    /**
     * @return Новая дата. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangePassDiplomaWorkPeriodStuExtract#getNewDate()
     */
        public PropertyPath<Date> newDate()
        {
            if(_newDate == null )
                _newDate = new PropertyPath<Date>(ChangePassDiplomaWorkPeriodStuExtractGen.P_NEW_DATE, this);
            return _newDate;
        }

        public Class getEntityClass()
        {
            return ChangePassDiplomaWorkPeriodStuExtract.class;
        }

        public String getEntityName()
        {
            return "changePassDiplomaWorkPeriodStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
