/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeGroupCaptain.AddEdit;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.IBaseCustomActionAddEditDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeGroupCaptainAction;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public interface IDAO extends IBaseCustomActionAddEditDAO<ChangeGroupCaptainAction, Model>
{
}