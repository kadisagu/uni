package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.TransferCompTypeStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О переводе на другую основу оплаты обучения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TransferCompTypeStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.TransferCompTypeStuExtract";
    public static final String ENTITY_NAME = "transferCompTypeStuExtract";
    public static final int VERSION_HASH = -558930952;
    private static IEntityMeta ENTITY_META;

    public static final String P_TRANSFER_DATE = "transferDate";
    public static final String P_PREV_CHANGE_COMP_TYPE_ORDER_DATE = "prevChangeCompTypeOrderDate";
    public static final String P_PREV_CHANGE_COMP_TYPE_ORDER_NUMBER = "prevChangeCompTypeOrderNumber";

    private Date _transferDate;     // Дата перевода
    private Date _prevChangeCompTypeOrderDate;     // Дата предыдущего приказа об изменении основы оплаты обучения
    private String _prevChangeCompTypeOrderNumber;     // Номер предыдущего приказа об изменении основы оплаты обучения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата перевода.
     */
    public Date getTransferDate()
    {
        return _transferDate;
    }

    /**
     * @param transferDate Дата перевода.
     */
    public void setTransferDate(Date transferDate)
    {
        dirty(_transferDate, transferDate);
        _transferDate = transferDate;
    }

    /**
     * @return Дата предыдущего приказа об изменении основы оплаты обучения.
     */
    public Date getPrevChangeCompTypeOrderDate()
    {
        return _prevChangeCompTypeOrderDate;
    }

    /**
     * @param prevChangeCompTypeOrderDate Дата предыдущего приказа об изменении основы оплаты обучения.
     */
    public void setPrevChangeCompTypeOrderDate(Date prevChangeCompTypeOrderDate)
    {
        dirty(_prevChangeCompTypeOrderDate, prevChangeCompTypeOrderDate);
        _prevChangeCompTypeOrderDate = prevChangeCompTypeOrderDate;
    }

    /**
     * @return Номер предыдущего приказа об изменении основы оплаты обучения.
     */
    @Length(max=255)
    public String getPrevChangeCompTypeOrderNumber()
    {
        return _prevChangeCompTypeOrderNumber;
    }

    /**
     * @param prevChangeCompTypeOrderNumber Номер предыдущего приказа об изменении основы оплаты обучения.
     */
    public void setPrevChangeCompTypeOrderNumber(String prevChangeCompTypeOrderNumber)
    {
        dirty(_prevChangeCompTypeOrderNumber, prevChangeCompTypeOrderNumber);
        _prevChangeCompTypeOrderNumber = prevChangeCompTypeOrderNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TransferCompTypeStuExtractGen)
        {
            setTransferDate(((TransferCompTypeStuExtract)another).getTransferDate());
            setPrevChangeCompTypeOrderDate(((TransferCompTypeStuExtract)another).getPrevChangeCompTypeOrderDate());
            setPrevChangeCompTypeOrderNumber(((TransferCompTypeStuExtract)another).getPrevChangeCompTypeOrderNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TransferCompTypeStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TransferCompTypeStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new TransferCompTypeStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return obj.getTransferDate();
                case "prevChangeCompTypeOrderDate":
                    return obj.getPrevChangeCompTypeOrderDate();
                case "prevChangeCompTypeOrderNumber":
                    return obj.getPrevChangeCompTypeOrderNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "transferDate":
                    obj.setTransferDate((Date) value);
                    return;
                case "prevChangeCompTypeOrderDate":
                    obj.setPrevChangeCompTypeOrderDate((Date) value);
                    return;
                case "prevChangeCompTypeOrderNumber":
                    obj.setPrevChangeCompTypeOrderNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                        return true;
                case "prevChangeCompTypeOrderDate":
                        return true;
                case "prevChangeCompTypeOrderNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return true;
                case "prevChangeCompTypeOrderDate":
                    return true;
                case "prevChangeCompTypeOrderNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return Date.class;
                case "prevChangeCompTypeOrderDate":
                    return Date.class;
                case "prevChangeCompTypeOrderNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TransferCompTypeStuExtract> _dslPath = new Path<TransferCompTypeStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TransferCompTypeStuExtract");
    }
            

    /**
     * @return Дата перевода.
     * @see ru.tandemservice.movestudent.entity.TransferCompTypeStuExtract#getTransferDate()
     */
    public static PropertyPath<Date> transferDate()
    {
        return _dslPath.transferDate();
    }

    /**
     * @return Дата предыдущего приказа об изменении основы оплаты обучения.
     * @see ru.tandemservice.movestudent.entity.TransferCompTypeStuExtract#getPrevChangeCompTypeOrderDate()
     */
    public static PropertyPath<Date> prevChangeCompTypeOrderDate()
    {
        return _dslPath.prevChangeCompTypeOrderDate();
    }

    /**
     * @return Номер предыдущего приказа об изменении основы оплаты обучения.
     * @see ru.tandemservice.movestudent.entity.TransferCompTypeStuExtract#getPrevChangeCompTypeOrderNumber()
     */
    public static PropertyPath<String> prevChangeCompTypeOrderNumber()
    {
        return _dslPath.prevChangeCompTypeOrderNumber();
    }

    public static class Path<E extends TransferCompTypeStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<Date> _transferDate;
        private PropertyPath<Date> _prevChangeCompTypeOrderDate;
        private PropertyPath<String> _prevChangeCompTypeOrderNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата перевода.
     * @see ru.tandemservice.movestudent.entity.TransferCompTypeStuExtract#getTransferDate()
     */
        public PropertyPath<Date> transferDate()
        {
            if(_transferDate == null )
                _transferDate = new PropertyPath<Date>(TransferCompTypeStuExtractGen.P_TRANSFER_DATE, this);
            return _transferDate;
        }

    /**
     * @return Дата предыдущего приказа об изменении основы оплаты обучения.
     * @see ru.tandemservice.movestudent.entity.TransferCompTypeStuExtract#getPrevChangeCompTypeOrderDate()
     */
        public PropertyPath<Date> prevChangeCompTypeOrderDate()
        {
            if(_prevChangeCompTypeOrderDate == null )
                _prevChangeCompTypeOrderDate = new PropertyPath<Date>(TransferCompTypeStuExtractGen.P_PREV_CHANGE_COMP_TYPE_ORDER_DATE, this);
            return _prevChangeCompTypeOrderDate;
        }

    /**
     * @return Номер предыдущего приказа об изменении основы оплаты обучения.
     * @see ru.tandemservice.movestudent.entity.TransferCompTypeStuExtract#getPrevChangeCompTypeOrderNumber()
     */
        public PropertyPath<String> prevChangeCompTypeOrderNumber()
        {
            if(_prevChangeCompTypeOrderNumber == null )
                _prevChangeCompTypeOrderNumber = new PropertyPath<String>(TransferCompTypeStuExtractGen.P_PREV_CHANGE_COMP_TYPE_ORDER_NUMBER, this);
            return _prevChangeCompTypeOrderNumber;
        }

        public Class getEntityClass()
        {
            return TransferCompTypeStuExtract.class;
        }

        public String getEntityName()
        {
            return "transferCompTypeStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
