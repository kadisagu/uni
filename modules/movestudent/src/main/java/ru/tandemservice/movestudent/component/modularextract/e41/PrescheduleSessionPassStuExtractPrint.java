/* $Id: extractPrint.vm 6933 2009-03-07 05:41:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e41;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.PrescheduleSessionPassStuExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 08.02.2011
 */
public class PrescheduleSessionPassStuExtractPrint implements IPrintFormCreator<PrescheduleSessionPassStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, PrescheduleSessionPassStuExtract extract)
    {
        String sessionType_G;
        if (UniDefines.SESSION_TYPE_WINTER.equals(extract.getSessionType().getCode()))
            sessionType_G = "зимней";
        else if (UniDefines.SESSION_TYPE_SUMMER.equals(extract.getSessionType().getCode()))
            sessionType_G = "летней";
        else if (UniDefines.SESSION_TYPE_SPRING.equals(extract.getSessionType().getCode()))
            sessionType_G = "весенней";
        else sessionType_G = "";

        EducationLevels eduLevel = EducationOrgUnitUtil.getParentLevel(extract.getEntity().getGroup().getEducationOrgUnit().getEducationLevelHighSchool());
        String specialityTitle = null != eduLevel ? eduLevel.getTitle() : "";
        String extractTitle = extract.getType().getTitle();
        String sessionYear = extract.getSessionYear().getTitle();
        String educationLevel = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getTitle();

        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("educationLevel", educationLevel);
        modifier.put("extractTitle", extractTitle);
        modifier.put("sessionType_G", sessionType_G);
        modifier.put("compensationType_G", CommonExtractPrint.getCompensationTypeStr_G(extract.getEntity().getCompensationType()));
        modifier.put("studentFIO_G", extract.getStudentTitle());
        modifier.put("speciality", specialityTitle);
        modifier.put("developForm_G", CommonExtractPrint.getDevelopFormStr_G(extract.getEntity().getEducationOrgUnit().getDevelopForm()));
        modifier.put("developCondition_G", CommonExtractPrint.getDevelopConditionStr2_G(extract.getEntity().getEducationOrgUnit().getDevelopCondition()));
        modifier.put("sessionYear", sessionYear);

        CommonExtractPrint.initOrgUnit(modifier, extract.getEntity().getEducationOrgUnit(), "orgUnit", "");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}
