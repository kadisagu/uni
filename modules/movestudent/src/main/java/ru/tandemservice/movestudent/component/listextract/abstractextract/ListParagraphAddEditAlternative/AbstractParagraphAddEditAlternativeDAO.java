/*$Id$*/
package ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEditAlternative;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSearchListUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 30.05.2014
 */
public abstract class AbstractParagraphAddEditAlternativeDAO<T extends ListStudentExtract, Model extends AbstractParagraphAddEditAlternativeModel<T>> extends AbstractListParagraphAddEditDAO<T, Model> implements IAbstractParagraphAddEditAlternativeDAO<T, Model>
{
	private static final String STUDENT_STATUS_ALIAS = "studentStatus";
	private static final String STUDENT_GROUP_ALIAS = "group";
	private static final OrderDescription LAST_NAME_SORT = new OrderDescription(STUDENT_ALIAS, Student.STUDENT_LAST_NAME);
	private static final OrderDescription FIRST_NAME_SORT = new OrderDescription(STUDENT_ALIAS, Student.STUDENT_FIRST_NAME);
	private static final OrderDescription MIDDLE_NAME_SORT = new OrderDescription(STUDENT_ALIAS, Student.STUDENT_MIDDLE_NAME);

	protected static final DQLOrderDescriptionRegistry _orderDescriptionRegistry = new DQLOrderDescriptionRegistry(Student.class, STUDENT_ALIAS);
	static
	{
		_orderDescriptionRegistry.setOrders(Student.person().fullFio(), LAST_NAME_SORT, FIRST_NAME_SORT, MIDDLE_NAME_SORT);
		_orderDescriptionRegistry.setOrders(Student.educationOrgUnit().educationLevelHighSchool().orgUnit().fullTitle(), new OrderDescription(STUDENT_ALIAS, new String[] { Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.L_ORG_UNIT, OrgUnit.P_TITLE }), LAST_NAME_SORT, FIRST_NAME_SORT, MIDDLE_NAME_SORT);
		_orderDescriptionRegistry.setOrders(Student.status().title(), new OrderDescription(STUDENT_STATUS_ALIAS, StudentStatus.P_PRIORITY), LAST_NAME_SORT, FIRST_NAME_SORT, MIDDLE_NAME_SORT);
		_orderDescriptionRegistry.setOrders(Student.group().title(), new OrderDescription(STUDENT_GROUP_ALIAS, Group.P_TITLE), LAST_NAME_SORT, FIRST_NAME_SORT, MIDDLE_NAME_SORT);
		_orderDescriptionRegistry.setOrders(Student.course().title(), new OrderDescription(STUDENT_ALIAS, new String[]{Student.L_COURSE, Course.P_TITLE}), LAST_NAME_SORT, FIRST_NAME_SORT, MIDDLE_NAME_SORT);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void prepare(Model model)
	{
		super.prepare(model);
		if (model.isEditForm() && !model.isInitialized())
			model.getAddedStudentsList().addAll((Collection) model.getSelectedItemList());
		model.setInitialized(true);
	}

	@Override
	public void prepareListDataSource(Model model)
	{
		//prepare ListDataSource
		List<Student> list = model.getAddedStudentsList();
		model.getDataSource().setCountRow(list.isEmpty() ? 3 : list.size());
		CommonBaseSearchListUtil.createPage(model.getDataSource(), list);
		setAdditionalViewProperties(model, list, true);

		//prepare SearchDataSource
		DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, STUDENT_ALIAS)
				.column(STUDENT_ALIAS)
				.where(eq(property(STUDENT_ALIAS, Student.educationOrgUnit().formativeOrgUnit()), value(model.getOrgUnit())))
				.where(eq(property(STUDENT_ALIAS, Student.archival()), value(Boolean.FALSE)));

		builder.joinPath(DQLJoinType.inner, Student.group().fromAlias(STUDENT_ALIAS), STUDENT_GROUP_ALIAS);
		builder.joinPath(DQLJoinType.left, Student.status().fromAlias(STUDENT_ALIAS), STUDENT_STATUS_ALIAS);

		if (!model.getAddedStudentsList().isEmpty())
		{
			builder.where(notIn(property(STUDENT_ALIAS, Student.id()), model.getAddedStudentsList()));
		}

		patchSearchDataSource(builder, model);

		_orderDescriptionRegistry.applyOrder(builder, model.getSearchDataSource().getEntityOrder());

		CommonBaseSearchListUtil.createPage(model.getSearchDataSource(), builder, getSession());
		setAdditionalViewProperties(model, model.getSearchDataSource(), model.getSearchDataSource().getEntityList(), true);
	}

	protected abstract void patchSearchDataSource(DQLSelectBuilder builder, Model model);

	@Override
	public void update(Model model)
	{
		updateParagraph(model, model.getAddedStudentsList());
	}

	@Override
	protected void patchListDataSource(MQBuilder builder, Model model)
	{
	}
}
