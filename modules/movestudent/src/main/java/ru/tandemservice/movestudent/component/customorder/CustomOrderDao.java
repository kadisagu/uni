/* $Id$ */
package ru.tandemservice.movestudent.component.customorder;

import com.google.common.collect.Lists;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.movestudent.entity.custom.ModifyListStuExtract;
import ru.tandemservice.movestudent.entity.custom.ModifyModularStuExtract;
import ru.tandemservice.movestudent.entity.custom.OrderActionType;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.ICustomOrderAction;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 28.10.2013
 */
public class CustomOrderDao extends UniBaseDao implements ICustomOrderDao
{
    @Override
    public void doCommitCustomExtract(IStudentCustomOrderExtract extract)
    {
        if (extract.getOrderActionType() == OrderActionType.DO_REVERT)
        {
            CustomOrderUtils.checkRevertAvailable(extract);

            // Откатываем выписку
            MoveDaoFacade.getMoveDao().doRollbackExtract(extract.getModifiableExtract(), extract.getModifiableExtract().getState(), null);
        }
        else
        {
            if (CustomOrderUtils.isRevertExtract(extract))
            {
                // Если это приказ об отмене приказа, снимаем флаг "проведено" у отменяемой выписки
                extract.getModifiableExtract().setCommitted(false);
                update(extract.getModifiableExtract());
            }

            if (extract.getOrderActionType() == OrderActionType.DO_CUSTOM_ACTION)
            {
                // Применяем все выбранные кастомные действия
                for (ICustomOrderAction action : MoveStudentDaoFacade.getMoveStudentDao().getCustomActions(extract.getId(), extract instanceof ModifyListStuExtract).keySet())
                {
                    CustomOrderUtils.getActionDao(action, getSessionFactory()).doCommit(action);
                }
            }
        }
    }

    @Override
    public void doRollbackCustomExtract(IStudentCustomOrderExtract extract)
    {
        if (extract.getOrderActionType() == OrderActionType.DO_REVERT)
        {
            // обратно проводим откаченную выписку
            MoveDaoFacade.getMoveDao().doCommitExtract(extract.getModifiableExtract(), extract.getModifiableExtract().getState(), null);
        }
        else
        {
            String code = extract.getType().getCode();
            if (StudentExtractTypeCodes.CUSTOM_REVERT_LIST_EXTRACT.equals(code) || StudentExtractTypeCodes.CUSTOM_REVERT_MODULAR_ORDER.equals(code))
            {
                // Если это приказ об отмене приказа, ставим обратно флаг "проведено" у отменяемой выписки
                extract.getModifiableExtract().setCommitted(true);
                update(extract.getModifiableExtract());
            }

            if (extract.getOrderActionType() == OrderActionType.DO_CUSTOM_ACTION)
            {
                List<ICustomOrderAction> actions = new ArrayList<>(MoveStudentDaoFacade.getMoveStudentDao().getCustomActions(extract.getId(), extract instanceof ModifyListStuExtract).keySet());
                // Откатывать будем порядке, обратном порядку проведения
                for (ICustomOrderAction action : Lists.reverse(actions))
                {
                    CustomOrderUtils.getActionDao(action, getSessionFactory()).doRollback(action);
                }
            }
        }
    }

    @Override
    public Long getModifiableOrderId(AbstractStudentParagraph customListOrderParagraph)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ModifyListStuExtract.class, "e")
                .predicate(DQLPredicateType.distinct)
                .column(property("e", ModifyListStuExtract.modifiableExtract().paragraph().order().id()))
                .where(eq(property("e", ModifyListStuExtract.paragraph()), value(customListOrderParagraph)));
        return builder.createStatement(getSession()).uniqueResult();
    }

    @Override
    public Long getModifiableOrderId(AbstractStudentOrder customListOrder)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ModifyListStuExtract.class, "e")
                .top(2).predicate(DQLPredicateType.distinct)
                .column(property("e", ModifyListStuExtract.modifiableExtract().paragraph().order().id()))
                .where(eq(property("e", ModifyListStuExtract.paragraph().order()), value(customListOrder)));
        List<Long> result = builder.createStatement(getSession()).list();
        if (result.size() == 1)
            return result.get(0);
        return null;
    }

    @Override
    public boolean hasModifyExtract(AbstractStudentExtract extract)
    {
        return existsEntity(ModifyModularStuExtract.class, ModifyModularStuExtract.L_MODIFIABLE_EXTRACT, extract) ||
                existsEntity(ModifyListStuExtract.class, ModifyListStuExtract.L_MODIFIABLE_EXTRACT, extract);
    }

    @Override
    public boolean hasModifyExtract(AbstractStudentOrder order)
    {
        return existsEntity(ModifyModularStuExtract.class, ModifyModularStuExtract.modifiableExtract().paragraph().order().s(), order) ||
                existsEntity(ModifyListStuExtract.class, ModifyListStuExtract.modifiableExtract().paragraph().order().s(), order);
    }
}