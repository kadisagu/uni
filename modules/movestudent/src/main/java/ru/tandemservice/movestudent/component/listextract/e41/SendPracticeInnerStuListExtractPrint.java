/* $Id: extractPrint.vm 24048 2012-09-06 05:21:10Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e41;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 13.02.2013
 */
public class SendPracticeInnerStuListExtractPrint implements IPrintFormCreator<SendPracticeInnerStuListExtract>, IListParagraphPrintFormCreator<SendPracticeInnerStuListExtract>
{

    @Override
    public RtfDocument createPrintForm(byte[] template, SendPracticeInnerStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);

        Sex sex = extract.getEntity().getPerson().getIdentityCard().getSex();
        if (extract.isDoneEduPlan())
            modifier.put("doneEduPlan", " полностью выполнивш" + (sex.isMale() ? "его" : "ую") + " учебный план " + extract.getPracticeCourse().getTitle() + " курса,");
        else
            modifier.put("doneEduPlan", "");

        if (extract.isDonePractice())
            modifier.put("donePractice", "считать прошедш" + (sex.isMale() ? "им" : "ей"));
        else
            modifier.put("donePractice", "направить на");

        if (extract.isOutClassTime())
            modifier.put("outClassTime", "в свободное от аудиторных занятий время ");
        else
            modifier.put("outClassTime", "");

        if (StringUtils.isEmpty(extract.getPracticeHeaderStr()))
        {
            modifier.put("practiceHeader_N", getPracticeHeaderStr(extract.getPracticeHeader(), extract.getPracticeHeaderDegree()));
        }
        else
        {
            modifier.put("practiceHeader_N", extract.getPracticeHeaderStr());
        }

        modifier.put("practiceOrgUnit", extract.getPracticeOrgUnitStr());

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, SendPracticeInnerStuListExtract firstExtract)
    {
        EducationOrgUnit educationOrgUnit = firstExtract.getEntity().getEducationOrgUnit();

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
        CommonListExtractPrint.injectCommonListExtractData(injectModifier, firstExtract);

        if (firstExtract.isDoneEduPlan())
            injectModifier.put("doneEduPlan", " полностью выполнивших учебный план " + firstExtract.getPracticeCourse().getTitle() + " курса,");
        else
            injectModifier.put("doneEduPlan", "");

        if (firstExtract.isDonePractice())
            injectModifier.put("donePractice", "считать прошедшими");
        else
            injectModifier.put("donePractice", "направить на");

        String practiceKind = MoveStudentDaoFacade.getMoveStudentDao().getDeclinableFullPracticeKind(firstExtract.getPracticeKind(), GrammaCase.ACCUSATIVE);
        injectModifier.put("practiceKind", practiceKind != null ? practiceKind : firstExtract.getPracticeKind());

        injectModifier.put("practiceCourse", firstExtract.getPracticeCourse().getTitle());
        if (firstExtract.isOutClassTime())
        {
            injectModifier.put("outClassTime", "в свободное от аудиторных занятий время ");
        }
        else
            injectModifier.put("outClassTime", "");

        injectModifier.put("practiceDuration", firstExtract.getPracticeDurationStr() + " нед.");

        injectModifier.put("practiceBeginDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(firstExtract.getPracticeBeginDate()));
        injectModifier.put("practiceEndDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(firstExtract.getPracticeEndDate()));
        injectModifier.put("attestationDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(firstExtract.getAttestationDate()));

        CommonExtractPrint.modifyEducationStr(injectModifier, educationOrgUnit);
        CommonExtractPrint.initOrgUnit(injectModifier, educationOrgUnit, "formativeOrgUnitStr", "");

        EducationLevels speciality = EducationOrgUnitUtil.getParentLevel(educationOrgUnit.getEducationLevelHighSchool());
        CommonExtractPrint.modifyEducationStr(injectModifier, speciality, new String[]{"fefuEducationStrDirection"}, false);

        if (StringUtils.isEmpty(firstExtract.getPreventAccidentsICStr()))
        {
            injectModifier.put("preventAccidentsIC_A", getPreventAccidentsICStr(firstExtract.getPreventAccidentsIC(), firstExtract.getPreventAccidentsICDegree()));
        }
        else
        {
            injectModifier.put("preventAccidentsIC_A", firstExtract.getPreventAccidentsICStr());
        }

        if (firstExtract.isProvideFundsAccordingToEstimates())
        {
            RtfString provFndsAccToEstimRtfString = new RtfString();
            provFndsAccToEstimRtfString.append(ApplicationRuntime.getProperty("sendPracticeOut.provFndsAccToEstim")
                    .replaceAll("\\{estimateNum\\}", firstExtract.getEstimateNum())).par();
            injectModifier.put("provFndsAccToEstim", provFndsAccToEstimRtfString);

            RtfString respForRecCashRtfString = new RtfString();

            if(!StringUtils.isEmpty(firstExtract.getResponsForRecieveCashStr()))
            {
                respForRecCashRtfString.append(ApplicationRuntime.getProperty("sendPracticeOut.respForRecCash")
                        .replaceAll("\\{responsForRecieveCash_A\\}", firstExtract.getResponsForRecieveCashStr())).par();
            }
            else
            {
                EmployeePost respForRecCash = firstExtract.getResponsForRecieveCash();

                String respForRecCashStr = "";
                respForRecCashStr += getDegreeShortTitleWithDots(firstExtract.getResponsForRecieveCashDegree());

                String respForRecCashAT = respForRecCash.getPostRelation().getPostBoundedWithQGandQL().getPost().getAccusativeCaseTitle();
                String respForRecCashAccustive = !StringUtils.isEmpty(respForRecCashAT) ? respForRecCashAT :
                        respForRecCash.getPostRelation().getPostBoundedWithQGandQL().getTitle();

                respForRecCashStr += " " + respForRecCashAccustive.toLowerCase();
                respForRecCashStr += " " + CommonListExtractPrint.getModifiedFioInitials(respForRecCash.getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE);

                respForRecCashRtfString.append(ApplicationRuntime.getProperty("sendPracticeOut.respForRecCash")
                        .replaceAll("\\{responsForRecieveCash_A\\}", respForRecCashStr)).par();
            }
            injectModifier.put("respForRecCash", respForRecCashRtfString);
        }
        else
        {
            injectModifier.put("provFndsAccToEstim", "");
            injectModifier.put("respForRecCash", "");
        }

        Group group = firstExtract.getGroup();
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(injectModifier, group.getEducationOrgUnit(), CommonListOrderPrint.getEducationBaseText(group), "fefuShortFastExtendedOptionalText");
        CommonExtractPrint.initFefuGroup(injectModifier, "groupInternal_G", group, group.getEducationOrgUnit().getDevelopForm(), " группы ");

        injectModifier.put("ifCompensType", CommonExtractPrint.getFefuCompTypeStr(firstExtract.getEntity().getCompensationType().isBudget(), true));

        return injectModifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, SendPracticeInnerStuListExtract firstExtract)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();

        int cnt = 1;
        List<String[]> paragraphDataLines = new ArrayList<>();
        for (Object ext : paragraph.getExtractList())
        {
            SendPracticeInnerStuListExtract extract = (SendPracticeInnerStuListExtract) ext;

            String practiceHeaderStr;

            if (StringUtils.isEmpty(extract.getPracticeHeaderStr()))
            {
                practiceHeaderStr = getPracticeHeaderStr(extract.getPracticeHeader(), extract.getPracticeHeaderDegree());
            }
            else
            {
                practiceHeaderStr = extract.getPracticeHeaderStr();
            }

            paragraphDataLines.add(new String[]{String.valueOf(cnt++), extract.getEntity().getPerson().getFullFio(), extract.getPracticeOrgUnitStr(), practiceHeaderStr});
        }

        tableModifier.put("T", paragraphDataLines.toArray(new String[][]{}));

        return tableModifier;
    }

    public static void injectTextPars(RtfInjectModifier modifier, IAbstractOrder order)
    {
        String textParagraphs = (String) order.getProperty(AbstractStudentOrder.P_TEXT_PARAGRAPH);
        if (!StringUtils.isEmpty(textParagraphs))
        {
            List<String> textParagraphsList = Lists.newArrayList(textParagraphs.split("\n"));
            RtfString textParagraphsRtfString = new RtfString();

            for (String textParagraph : textParagraphsList)
            {
                textParagraphsRtfString.append(textParagraph.trim()).par();
            }
            modifier.put("textParagraphs", textParagraphsRtfString.par());
        }
        else
        {
            modifier.put("textParagraphs", "");
        }
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, SendPracticeInnerStuListExtract firstExtract)
    {
        injectTextPars(modifier, order);
    }

    private String getPreventAccidentsICStr(EmployeePost employeePost, PersonAcademicDegree degree)
    {
        StringBuilder preventAccidentsICStrBuilder = new StringBuilder();

        preventAccidentsICStrBuilder.append(getDegreeShortTitleWithDots(degree));

        String preventPostAT = employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().getAccusativeCaseTitle();
        String preventAccidentsICAccustive = !StringUtils.isEmpty(preventPostAT) ? preventPostAT :
                employeePost.getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if (!StringUtils.isEmpty(preventAccidentsICAccustive))
            preventAccidentsICStrBuilder.append((StringUtils.isEmpty(preventAccidentsICStrBuilder.toString()) ? "" : " ")).append(preventAccidentsICAccustive.toLowerCase());

        preventAccidentsICStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedIofInitials(employeePost.getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE));

        return preventAccidentsICStrBuilder.toString();
    }


    private String getPracticeHeaderStr(EmployeePost employeePost, PersonAcademicDegree degree)
    {
        StringBuilder practiceHeaderStrBuilder = new StringBuilder();

        practiceHeaderStrBuilder.append(getDegreeShortTitleWithDots(degree));

        String practiceHeaderPostNT = employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().getAccusativeCaseTitle();
        String practiceHeaderNominative = !org.apache.cxf.common.util.StringUtils.isEmpty(practiceHeaderPostNT) ? practiceHeaderPostNT :
                employeePost.getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if (!StringUtils.isEmpty(practiceHeaderNominative))
            practiceHeaderStrBuilder.append((StringUtils.isEmpty(practiceHeaderStrBuilder.toString()) ? "" : " ")).append(practiceHeaderNominative.toLowerCase());

        practiceHeaderStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedIofInitials(employeePost.getPerson().getIdentityCard(), GrammaCase.NOMINATIVE));

        return practiceHeaderStrBuilder.toString();
    }

    private String getDegreeShortTitleWithDots(PersonAcademicDegree prevDegree)
    {
        String preventAccidentsICStr = "";
        if (null != prevDegree)
        {
            String[] degreeTitle = prevDegree.getAcademicDegree().getTitle().toLowerCase().split(" ");
            StringBuilder shortDegreeTitle = new StringBuilder();
            for (String deg : Lists.newArrayList(degreeTitle))
            {
                shortDegreeTitle.append(deg.charAt(0)).append(".");
            }
            preventAccidentsICStr += shortDegreeTitle;
        }
        return preventAccidentsICStr;
    }
}