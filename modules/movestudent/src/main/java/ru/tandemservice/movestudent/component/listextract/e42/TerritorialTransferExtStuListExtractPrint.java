/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.component.listextract.e42;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author Alexey Lopatin
 * @since 28.08.2013
 */
public class TerritorialTransferExtStuListExtractPrint implements IPrintFormCreator<TerritorialTransferExtStuListExtract>, IListParagraphPrintFormCreator<TerritorialTransferExtStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, TerritorialTransferExtStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, TerritorialTransferExtStuListExtract firstExtract)
    {
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);

        CompensationType compensationTypeOld = firstExtract.getCompensationTypeOld();
        CompensationType compensationTypeNew = firstExtract.getCompensationTypeNew();

        EducationOrgUnit educationOrgUnitOld = firstExtract.getEducationOrgUnitOld();
        EducationOrgUnit educationOrgUnitNew = firstExtract.getEducationOrgUnitNew();

        injectModifier.put("groupOld", firstExtract.getGroupOld().getTitle());
        injectModifier.put("groupNew", firstExtract.getGroupNew().getTitle());

        CommonListOrderPrint.injectFefuDevelopConditionAndTech(injectModifier, educationOrgUnitOld, CommonListOrderPrint.getEducationBaseText(firstExtract.getGroupOld()), "fefuShortFastExtendedOptionalTextOld");
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(injectModifier, educationOrgUnitNew, CommonListOrderPrint.getEducationBaseText(firstExtract.getGroupNew()), "fefuShortFastExtendedOptionalTextNew");
        CommonExtractPrint.initFefuGroup(injectModifier, "intoFefuGroupOld", firstExtract.getGroupOld(), educationOrgUnitOld.getDevelopForm(), " группы ");
        CommonExtractPrint.initFefuGroup(injectModifier, "intoFefuGroupNew", firstExtract.getGroupNew(), educationOrgUnitNew.getDevelopForm(), " в группу ");

        injectModifier.put("fefuCompensationTypeStrOld", compensationTypeOld != null ? compensationTypeOld.isBudget() ? "за счет средств федерального бюджета" : "на договорной основе" : "");
        injectModifier.put("compensationTypeStrOld", compensationTypeOld != null ? compensationTypeOld.isBudget() ? "на бюджетной основе" : "по договору, предусматривающему оплату обучения" : "");

        injectModifier.put("fefuCompensationTypeStrNew", compensationTypeNew != null ? compensationTypeNew.isBudget() ? "за счет средств федерального бюджета" : "на договорной основе" : "");
        injectModifier.put("compensationTypeStrNew", compensationTypeNew != null ? compensationTypeNew.isBudget() ? "на бюджетной основе" : "по договору, предусматривающему оплату обучения" : "");

        injectModifier.put("formativeOrgUnitOld_P", educationOrgUnitOld.getFormativeOrgUnit().getPrepositionalCaseTitle() != null ? educationOrgUnitOld.getFormativeOrgUnit().getPrepositionalCaseTitle() : educationOrgUnitOld.getFormativeOrgUnit().getPrintTitle());
        injectModifier.put("formativeOrgUnitNew_P", educationOrgUnitNew.getFormativeOrgUnit().getPrepositionalCaseTitle() != null ? educationOrgUnitNew.getFormativeOrgUnit().getPrepositionalCaseTitle() : educationOrgUnitNew.getFormativeOrgUnit().getPrintTitle());

        injectModifier.put("territorialOrgUnitOld_P", educationOrgUnitOld.getTerritorialOrgUnit().getTerritorialTitle().equals(TopOrgUnit.getInstance().getTerritorialTitle()) ? "" :" (" + educationOrgUnitOld.getTerritorialOrgUnit().getPrintTitle() + ")");
        injectModifier.put("territorialOrgUnitNew_P", educationOrgUnitNew.getTerritorialOrgUnit().getTerritorialTitle().equals(TopOrgUnit.getInstance().getTerritorialTitle()) ? "" :" (" + educationOrgUnitNew.getTerritorialOrgUnit().getPrintTitle() + ")");

        CommonExtractPrint.modifyEducationStr(injectModifier, educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel(), new String[]{"educationStrOld"});
        StructureEducationLevels levelOld = educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel().getLevelType();

        String educationStrNew = levelOld.isProfile() ? levelOld.isBachelor() ? "указанному направлению и профилю" : "указанному магистерскому направлению и магистерской программе" : "указанной специальности и специализации";

        if (!educationOrgUnitOld.getEducationLevelHighSchool().equals(educationOrgUnitNew.getEducationLevelHighSchool()))
        {
            educationStrNew += " «" + educationOrgUnitNew.getEducationLevelHighSchool().getTitle() + "»";
        }

        //Кастомизация для ДВФУ
        if (educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel().isProfileOrSpecialization())
        {
            levelOld = (EducationOrgUnitUtil.getParentLevel(educationOrgUnitOld.getEducationLevelHighSchool())).getLevelType();
        }

        String fefuEducationStrNew = levelOld.isSpecialty() ? "указанной специальности" : levelOld.isMaster() || levelOld.isBachelor() ? "указанному направлению" : "";

        // если нет профиля или специализации у старого НП
        if (!educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel().isProfileOrSpecialization())
        {
            // если есть профиль или специализации у нового НП
            if (educationOrgUnitNew.getEducationLevelHighSchool().getEducationLevel().isProfileOrSpecialization())
                fefuEducationStrNew += (levelOld.isSpecialty() ? ", специализации" : levelOld.isMaster() ? ", магистерской программе" : levelOld.isBachelor() ? ", профилю" : "") + " «" + educationOrgUnitNew.getEducationLevelHighSchool().getTitle() + "»";
        }
        else
        {
            EducationLevels parentLevelOld = educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel();

            // если новое НП является направлением или специализацией (когда новое НП не выбрано)
            if (!educationOrgUnitNew.getEducationLevelHighSchool().getEducationLevel().isProfileOrSpecialization())
            {
                parentLevelOld = educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel().getParentLevel();
            }

            if (!parentLevelOld.equals(educationOrgUnitNew.getEducationLevelHighSchool().getEducationLevel()))
                fefuEducationStrNew += (levelOld.isSpecialty() ? ", специализации" : levelOld.isMaster() ? ", магистерской программе" : levelOld.isBachelor() ? ", профилю" : "") + " «" + educationOrgUnitNew.getEducationLevelHighSchool().getTitle() + "»";
            else
                fefuEducationStrNew += parentLevelOld.isProfileOrSpecialization()? (levelOld.isSpecialty() ? ", специализации" : levelOld.isMaster() ? ", магистерской программе" : levelOld.isBachelor() ? ", профилю" : "") : "";
        }

        injectModifier.put("educationStrNew_D", educationStrNew);
        injectModifier.put("fefuEducationStrNew_D", fefuEducationStrNew);

        CommonExtractPrint.initEducationType(injectModifier, educationOrgUnitOld, "Old");

        return injectModifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, TerritorialTransferExtStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, TerritorialTransferExtStuListExtract firstExtract)
    {
    }
}