/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractAddEdit.ModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 06.11.2008
 */
public abstract class CommonModularStudentExtractAddEditModel<T extends ModularStudentExtract> extends ModularStudentExtractAddEditModel<T>
{
    private List<StudentOrderReasons> _reasonList;
    private List<StudentOrderBasics> _selectedBasicList = new ArrayList<>();
    private IMultiSelectModel _basicListModel;
    private StudentOrderBasics _currentBasic;
    private Map<Long, String> _currentBasicMap = new HashMap<>();
    private boolean _individualOrder;
    private boolean _changesNothing;
    private boolean _changeBookNumber;
    private boolean _changeTargetAdmission;

    private StudentModularOrder _order = new StudentModularOrder();

    private ISelectModel _employeePostModel;
    private EmployeePost _employeePost;

    public String getCommonPage()
    {
        return CommonModularStudentExtractAddEditModel.class.getPackage().getName() + ".CommonPageAddEdit";
    }

    // метод под этим названием оставлен для совместимости с клиентскими доработками
    public String getCommentsPage()
    {
        return getAdditionalDataPage();
    }

    public String getAdditionalDataPage()
    {
        return CommonModularStudentExtractAddEditModel.class.getPackage().getName() + ".AdditionalDataAddEdit";
    }

    public boolean isNeedComments()
    {
        for (StudentOrderBasics basic : _selectedBasicList)
            if (basic.isCommentable()) return true;
        return false;
    }

    public boolean isCanChangeBookNumberAndTargetAdmission()
    {
        return getExtract() instanceof CommonStuExtract;
    }

    public String getContextEntityTitleGenitive()
    {
        if(isIndividualOrder()) return "индивидуального приказа";
        return "проекта приказа";
    }

    public String getStickerTitle()
    {
        return (isAddForm() ? "Добавление " : "Редактирование ") + getContextEntityTitleGenitive() + " «" + (null != getExtractType() ? getExtractType().getTitle() : getExtract().getType().getTitle()) + "»";
    }

    public boolean isShowExecutor()
    {
        if(isAddForm() || !isIndividualOrder())
            return true;

        return false;
    }

    public String getCommonBlockTitle()
    {
        return "Общие данные " + getContextEntityTitleGenitive();
    }

    public String getStudentBlockTitle()
    {
        return "Данные студента на момент создания " + getContextEntityTitleGenitive();
    }

    public String getCurrentBasicTitle()
    {
        return _currentBasicMap.get(_currentBasic.getId());
    }

    public void setCurrentBasicTitle(String title)
    {
        _currentBasicMap.put(_currentBasic.getId(), title);
    }

    public String getCurrentBasicId()
    {
        return "basicId_" + _currentBasic.getId();
    }

    public boolean isThereAnyChanges()
    {
        return !isChangesNothing();
    }

    public boolean isReasonCommentVisible()
    {
        return null != getExtract().getReason() && getExtract().getReason().isCommentable();
    }

    // Getters & Setters

    public List<StudentOrderReasons> getReasonList()
    {
        return _reasonList;
    }

    public void setReasonList(List<StudentOrderReasons> reasonList)
    {
        _reasonList = reasonList;
    }

    public List<StudentOrderBasics> getSelectedBasicList()
    {
        return _selectedBasicList;
    }

    public void setSelectedBasicList(List<StudentOrderBasics> selectedBasicList)
    {
        _selectedBasicList = selectedBasicList;
    }

    public IMultiSelectModel getBasicListModel()
    {
        return _basicListModel;
    }

    public void setBasicListModel(IMultiSelectModel basicListModel)
    {
        _basicListModel = basicListModel;
    }

    public StudentOrderBasics getCurrentBasic()
    {
        return _currentBasic;
    }

    public void setCurrentBasic(StudentOrderBasics currentBasic)
    {
        _currentBasic = currentBasic;
    }

    public Map<Long, String> getCurrentBasicMap()
    {
        return _currentBasicMap;
    }

    public void setCurrentBasicMap(Map<Long, String> currentBasicMap)
    {
        _currentBasicMap = currentBasicMap;
    }

    public boolean isIndividualOrder()
    {
        return _individualOrder;
    }

    public void setIndividualOrder(boolean individualOrder)
    {
        _individualOrder = individualOrder;
    }

    public StudentModularOrder getOrder()
    {
        return _order;
    }

    public void setOrder(StudentModularOrder order)
    {
        _order = order;
    }

    public ISelectModel getEmployeePostModel()
    {
        return _employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        _employeePostModel = employeePostModel;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        _employeePost = employeePost;
    }

    public boolean isChangesNothing()
    {
        return _changesNothing;
    }

    public void setChangesNothing(boolean changesNothing)
    {
        _changesNothing = changesNothing;
    }

    public boolean isChangeBookNumber()
    {
        return _changeBookNumber;
    }

    public void setChangeBookNumber(boolean changeBookNumber)
    {
        _changeBookNumber = changeBookNumber;
    }

    public boolean isChangeTargetAdmission()
    {
        return _changeTargetAdmission;
    }

    public void setChangeTargetAdmission(boolean changeTargetAdmission)
    {
        _changeTargetAdmission = changeTargetAdmission;
    }

}