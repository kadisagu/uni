/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeCompensationType.AddEdit;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditController;
import ru.tandemservice.movestudent.entity.custom.ChangeCompensationTypeAction;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class Controller extends BaseCustomActionAddEditController<ChangeCompensationTypeAction, IDAO, Model>
{
}