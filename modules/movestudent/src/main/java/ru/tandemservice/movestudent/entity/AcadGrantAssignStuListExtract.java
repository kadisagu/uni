/* $Id$ */
package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IAssignPaymentExtract;
import ru.tandemservice.movestudent.component.commons.gradation.IGroupManagerPaymentExtract;
import ru.tandemservice.movestudent.entity.gen.AcadGrantAssignStuListExtractGen;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Проект приказа «О назначении академической стипендии»
 */
public class AcadGrantAssignStuListExtract extends AcadGrantAssignStuListExtractGen implements IAssignPaymentExtract, IGroupManagerPaymentExtract
{
    @Override
    public Date getGroupManagerBonusBeginDate()
    {
        return getBeginDate();
    }

    @Override
    public Date getGroupManagerBonusEndDate()
    {
        return getEndDate();
    }

    @Override
    public boolean hasGroupManagerBonus()
    {
        return getGroupManagerBonusSizeRuble() != null;
    }

    @Override
    public BigDecimal getPaymentAmount()
    {
        return getGrantSizeRuble() != null ? new BigDecimal(getGrantSizeRuble()) : null;
    }

    @Override
    public BigDecimal getGroupManagerPaymentAmount()
    {
        return getGroupManagerBonusSizeRuble() != null ? new BigDecimal(getGroupManagerBonusSizeRuble()) : null;
    }

    @Override
    public Date getPaymentBeginDate()
    {
        return getBeginDate();
    }

    @Override
    public Date getPaymentEndDate()
    {
        return getEndDate();
    }
}