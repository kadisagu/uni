/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.list.StudentListParagraphAdd;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDao;

/**
 * @author vip_delete
 * @since 07.05.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setOrder((StudentListOrder) getNotNull(model.getOrderId()));

        MQBuilder builder = new MQBuilder(StudentExtractType.ENTITY_CLASS, "t");
        builder.add(MQExpression.eq("t", StudentExtractType.L_PARENT, model.getOrder().getType()));
        builder.add(MQExpression.eq("t", StudentExtractType.P_ACTIVE, Boolean.TRUE));

        model.setTypeList(builder.<StudentExtractType>getResultList(getSession()));
    }
}
