/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e84.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.movestudent.entity.RePassDiscComissStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToComissionMember;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 31.08.2012
 */
public class Controller extends CommonModularStudentExtractAddEditController<RePassDiscComissStuExtract, IDAO, Model>
{
    public void onSelectComissionMembers(IBusinessComponent component)
    {
        Model model = getModel(component);
        for (PpsEntry pps : model.getSelectedPPSList())
        {
            StuExtractToComissionMember member = new StuExtractToComissionMember();
            member.setFio(pps.getFio());

            if (pps instanceof PpsEntryByEmployeePost)
            {
                PpsEntryByEmployeePost post = (PpsEntryByEmployeePost) pps;
                StringBuilder postStr = new StringBuilder();

                OrgUnit orgUnit = post.getOrgUnit();

                postStr.append(StringUtils.uncapitalize(post.getPost().getTitle())).append(" ");
                postStr.append(null != orgUnit.getGenitiveCaseTitle() ? orgUnit.getGenitiveCaseTitle() : orgUnit.getFullTitle());
                member.setPost(postStr.toString());
            } else member.setPost(pps.getFullTitle());
            model.getMembersList().add(member);
        }
        model.getSelectedPPSList().clear();
    }

    public void onClickDeleteMember(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getMembersList().size() == 1)
        {
            model.getMembersList().clear();
            return;
        }

        Integer memberNumber = (Integer) component.getListenerParameter();
        StuExtractToComissionMember member = model.getMembersList().get(memberNumber);
        model.getMembersList().remove(member);
    }
}