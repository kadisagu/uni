package ru.tandemservice.movestudent.entity.custom;

import ru.tandemservice.movestudent.entity.custom.gen.*;

/**
 * Назначение старостой/Снятие с должности старосты
 *
 * Если стоит признак "Назначить старостой", значит после проведения студент будет старостой его группы, иначе - с него снимут эту должность.
 */
public class ChangeGroupCaptainAction extends ChangeGroupCaptainActionGen
{
}