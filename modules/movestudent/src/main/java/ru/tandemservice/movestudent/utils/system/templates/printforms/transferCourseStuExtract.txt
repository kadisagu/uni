\keep\keepn\qj\b {extractNumber}. ПЕРЕВЕСТИ:\b0\par
{fio} — {student_A} {courseOld} курса {developFormOld_G} отделения группы {groupOld} {orgUnitOld_G}, {learned_A} {compensationTypeStrOld}, по {educationTypeOld_D} «{educationOrgUnitOld}», на {courseNew} курс в группу {groupNew} как {executed_A} содержание учебного плана {courseOld} курса.\par
  Основание: {listBasics}.