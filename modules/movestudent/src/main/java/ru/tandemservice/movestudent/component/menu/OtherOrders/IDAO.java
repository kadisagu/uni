/* $Id$ */
package ru.tandemservice.movestudent.component.menu.OtherOrders;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Dmitry Seleznev
 * @since 11.04.2013
 */
public interface IDAO extends IUniDao<Model>
{
}