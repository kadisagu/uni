package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.WeekendChildThreeStuExtractGen;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О предоставлении отпуска по уходу за ребенком до 3 лет
 */
public class WeekendChildThreeStuExtract extends WeekendChildThreeStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getBeginDate();
    }

    @Override
    public Group getGroupOld()
    {
        return getEntity().getGroup();
    }

    @Override
    public Group getGroupNew()
    {
        return null;
    }
}