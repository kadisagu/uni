/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e33.utils;

import org.tandemframework.shared.person.base.entity.Person;

/**
 * @author Alexander Zhebko
 * @since 03.10.2012
 */
public class SocGrantAssignExtractWrapper implements Comparable<SocGrantAssignExtractWrapper>
{
    private final Person _person;
    private final Double _grantSize;

    public SocGrantAssignExtractWrapper(Person person, Double grantSize)
    {
        _person = person;
        _grantSize = grantSize;
    }

    public Person getPerson()
    {
        return _person;
    }

    public Double getGrantSize()
    {
        return _grantSize;
    }

    @Override
    public int compareTo(SocGrantAssignExtractWrapper o)
    {
        return Person.FULL_FIO_AND_ID_COMPARATOR.compare(_person, o.getPerson());
    }
}