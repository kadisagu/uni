/* $Id: Controller.java.vm 7834 2009-05-06 09:39:03Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e40.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.movestudent.entity.GroupTransferExtStuListExtract;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;

/**
 * @author ListExtractComponentGenerator
 * @since 30.01.2013
 */
public class Controller extends AbstractListParagraphAddEditController<GroupTransferExtStuListExtract, IDAO, Model>
{
    @Override
    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дополнительный статус", Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Пол", Student.person().identityCard().sex().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("status", "Состояние", ru.tandemservice.movestudent.component.listextract.e18.ParagraphAddEdit.Model.STATUS_KEY, SimpleFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn(EDU_LVL_HS_COLUMN_TITLE, Student.educationOrgUnit().educationLevelHighSchool().configurableTitle().s()).setClickable(false));
        dataSource.setOrder("status", OrderDirection.asc);
    }

    public void onChangeGroup(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (null != model.getGroup())
        {
            model.setParentEduLevel(EducationOrgUnitUtil.getParentLevel(model.getGroup().getEducationOrgUnit().getEducationLevelHighSchool()));
            model.setDevelopForm(model.getGroup().getEducationOrgUnit().getDevelopForm());
            model.setDevelopCondition(model.getGroup().getEducationOrgUnit().getDevelopCondition());
            model.setDevelopTech(model.getGroup().getEducationOrgUnit().getDevelopTech());
            model.setDevelopPeriod(model.getGroup().getEducationOrgUnit().getDevelopPeriod());
        }
    }
}
