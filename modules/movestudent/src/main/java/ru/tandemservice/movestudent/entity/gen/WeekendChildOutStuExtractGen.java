package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.WeekendChildOutStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О выходе из отпуска по уходу за ребенком
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class WeekendChildOutStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.WeekendChildOutStuExtract";
    public static final String ENTITY_NAME = "weekendChildOutStuExtract";
    public static final int VERSION_HASH = -592093721;
    private static IEntityMeta ENTITY_META;

    public static final String P_EARLY_OUT = "earlyOut";
    public static final String P_ALLOW_INDEPENDENT_SCHEDULE = "allowIndependentSchedule";
    public static final String P_INDEP_SCHED_BEGIN_DATE = "indepSchedBeginDate";
    public static final String P_INDEP_SCHED_END_DATE = "indepSchedEndDate";

    private boolean _earlyOut;     // Досрочное окончание отпуска
    private boolean _allowIndependentSchedule;     // Предоставить свободное посещение занятий
    private Date _indepSchedBeginDate;     // Дата начала свободного посещения
    private Date _indepSchedEndDate;     // Дата окончания свободного посещения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Досрочное окончание отпуска. Свойство не может быть null.
     */
    @NotNull
    public boolean isEarlyOut()
    {
        return _earlyOut;
    }

    /**
     * @param earlyOut Досрочное окончание отпуска. Свойство не может быть null.
     */
    public void setEarlyOut(boolean earlyOut)
    {
        dirty(_earlyOut, earlyOut);
        _earlyOut = earlyOut;
    }

    /**
     * @return Предоставить свободное посещение занятий. Свойство не может быть null.
     */
    @NotNull
    public boolean isAllowIndependentSchedule()
    {
        return _allowIndependentSchedule;
    }

    /**
     * @param allowIndependentSchedule Предоставить свободное посещение занятий. Свойство не может быть null.
     */
    public void setAllowIndependentSchedule(boolean allowIndependentSchedule)
    {
        dirty(_allowIndependentSchedule, allowIndependentSchedule);
        _allowIndependentSchedule = allowIndependentSchedule;
    }

    /**
     * @return Дата начала свободного посещения.
     */
    public Date getIndepSchedBeginDate()
    {
        return _indepSchedBeginDate;
    }

    /**
     * @param indepSchedBeginDate Дата начала свободного посещения.
     */
    public void setIndepSchedBeginDate(Date indepSchedBeginDate)
    {
        dirty(_indepSchedBeginDate, indepSchedBeginDate);
        _indepSchedBeginDate = indepSchedBeginDate;
    }

    /**
     * @return Дата окончания свободного посещения.
     */
    public Date getIndepSchedEndDate()
    {
        return _indepSchedEndDate;
    }

    /**
     * @param indepSchedEndDate Дата окончания свободного посещения.
     */
    public void setIndepSchedEndDate(Date indepSchedEndDate)
    {
        dirty(_indepSchedEndDate, indepSchedEndDate);
        _indepSchedEndDate = indepSchedEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof WeekendChildOutStuExtractGen)
        {
            setEarlyOut(((WeekendChildOutStuExtract)another).isEarlyOut());
            setAllowIndependentSchedule(((WeekendChildOutStuExtract)another).isAllowIndependentSchedule());
            setIndepSchedBeginDate(((WeekendChildOutStuExtract)another).getIndepSchedBeginDate());
            setIndepSchedEndDate(((WeekendChildOutStuExtract)another).getIndepSchedEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends WeekendChildOutStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) WeekendChildOutStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new WeekendChildOutStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "earlyOut":
                    return obj.isEarlyOut();
                case "allowIndependentSchedule":
                    return obj.isAllowIndependentSchedule();
                case "indepSchedBeginDate":
                    return obj.getIndepSchedBeginDate();
                case "indepSchedEndDate":
                    return obj.getIndepSchedEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "earlyOut":
                    obj.setEarlyOut((Boolean) value);
                    return;
                case "allowIndependentSchedule":
                    obj.setAllowIndependentSchedule((Boolean) value);
                    return;
                case "indepSchedBeginDate":
                    obj.setIndepSchedBeginDate((Date) value);
                    return;
                case "indepSchedEndDate":
                    obj.setIndepSchedEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "earlyOut":
                        return true;
                case "allowIndependentSchedule":
                        return true;
                case "indepSchedBeginDate":
                        return true;
                case "indepSchedEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "earlyOut":
                    return true;
                case "allowIndependentSchedule":
                    return true;
                case "indepSchedBeginDate":
                    return true;
                case "indepSchedEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "earlyOut":
                    return Boolean.class;
                case "allowIndependentSchedule":
                    return Boolean.class;
                case "indepSchedBeginDate":
                    return Date.class;
                case "indepSchedEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<WeekendChildOutStuExtract> _dslPath = new Path<WeekendChildOutStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "WeekendChildOutStuExtract");
    }
            

    /**
     * @return Досрочное окончание отпуска. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOutStuExtract#isEarlyOut()
     */
    public static PropertyPath<Boolean> earlyOut()
    {
        return _dslPath.earlyOut();
    }

    /**
     * @return Предоставить свободное посещение занятий. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOutStuExtract#isAllowIndependentSchedule()
     */
    public static PropertyPath<Boolean> allowIndependentSchedule()
    {
        return _dslPath.allowIndependentSchedule();
    }

    /**
     * @return Дата начала свободного посещения.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOutStuExtract#getIndepSchedBeginDate()
     */
    public static PropertyPath<Date> indepSchedBeginDate()
    {
        return _dslPath.indepSchedBeginDate();
    }

    /**
     * @return Дата окончания свободного посещения.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOutStuExtract#getIndepSchedEndDate()
     */
    public static PropertyPath<Date> indepSchedEndDate()
    {
        return _dslPath.indepSchedEndDate();
    }

    public static class Path<E extends WeekendChildOutStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<Boolean> _earlyOut;
        private PropertyPath<Boolean> _allowIndependentSchedule;
        private PropertyPath<Date> _indepSchedBeginDate;
        private PropertyPath<Date> _indepSchedEndDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Досрочное окончание отпуска. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOutStuExtract#isEarlyOut()
     */
        public PropertyPath<Boolean> earlyOut()
        {
            if(_earlyOut == null )
                _earlyOut = new PropertyPath<Boolean>(WeekendChildOutStuExtractGen.P_EARLY_OUT, this);
            return _earlyOut;
        }

    /**
     * @return Предоставить свободное посещение занятий. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOutStuExtract#isAllowIndependentSchedule()
     */
        public PropertyPath<Boolean> allowIndependentSchedule()
        {
            if(_allowIndependentSchedule == null )
                _allowIndependentSchedule = new PropertyPath<Boolean>(WeekendChildOutStuExtractGen.P_ALLOW_INDEPENDENT_SCHEDULE, this);
            return _allowIndependentSchedule;
        }

    /**
     * @return Дата начала свободного посещения.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOutStuExtract#getIndepSchedBeginDate()
     */
        public PropertyPath<Date> indepSchedBeginDate()
        {
            if(_indepSchedBeginDate == null )
                _indepSchedBeginDate = new PropertyPath<Date>(WeekendChildOutStuExtractGen.P_INDEP_SCHED_BEGIN_DATE, this);
            return _indepSchedBeginDate;
        }

    /**
     * @return Дата окончания свободного посещения.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOutStuExtract#getIndepSchedEndDate()
     */
        public PropertyPath<Date> indepSchedEndDate()
        {
            if(_indepSchedEndDate == null )
                _indepSchedEndDate = new PropertyPath<Date>(WeekendChildOutStuExtractGen.P_INDEP_SCHED_END_DATE, this);
            return _indepSchedEndDate;
        }

        public Class getEntityClass()
        {
            return WeekendChildOutStuExtract.class;
        }

        public String getEntityName()
        {
            return "weekendChildOutStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
