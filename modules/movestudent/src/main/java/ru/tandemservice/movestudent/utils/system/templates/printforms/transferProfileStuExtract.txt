\keep\keepn\qj\b {extractNumber}. ЗАКРЕПИТЬ:\b0\par
{fio} — {student_A} {courseOld} курса {developFormOld_G} отделения группы {groupOld} {orgUnitOld_G}, {learned_A} {compensationTypeStrOld}, по {fefuEducationStrDirectionOld_D}, за {fefuEducationStrProfileNew_I} и перераспределить в группу {groupNew} на {courseNew} курс {developFormNew_G} отделения {orgUnitNew_G}.\par
  Основание: {listBasics}.