/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeCustomStates.Pub;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.BaseCustomActionPubDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeCustomStateActionRelation;
import ru.tandemservice.movestudent.entity.custom.ChangeCustomStatesAction;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class DAO extends BaseCustomActionPubDAO<ChangeCustomStatesAction, Model>
{
    @Override
    protected Class<ChangeCustomStatesAction> getActionClass()
    {
        return ChangeCustomStatesAction.class;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        if (model.getCustomAction() == null)
            return;

        String oldStates = "", newStates = "";
        for (ChangeCustomStateActionRelation rel : getList(ChangeCustomStateActionRelation.class, ChangeCustomStateActionRelation.L_ACTION, model.getCustomAction().getId()))
        {
            if (rel.isNewState())
            {
                if (!newStates.isEmpty())
                    newStates += "; ";
                newStates += rel.getState().getTitle();
            }
            else
            {
                if (!oldStates.isEmpty())
                    oldStates += "; ";
                oldStates += rel.getState().getTitle();
            }
        }
        model.setNewStates(newStates);
        model.setOldStatesToDelete(oldStates);
    }
}