/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e15;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.ExcludeSingGrpStuListExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.text.SimpleDateFormat;

/**
 * @author ListExtractComponentGenerator
 * @since 28.07.2009
 */
public class ExcludeSingGrpStuListExtractPrint implements IPrintFormCreator<ExcludeSingGrpStuListExtract>, IListParagraphPrintFormCreator<ExcludeSingGrpStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ExcludeSingGrpStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, ExcludeSingGrpStuListExtract firstExtract)
    {
        CommonExtractPrint.initDevelopForm(modifier, firstExtract.getEntity().getGroup().getEducationOrgUnit(), "");
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, ExcludeSingGrpStuListExtract currentExtract)
    {
        EducationLevelsHighSchool hs = currentExtract.getEntity().getGroup().getEducationOrgUnit().getEducationLevelHighSchool();
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, currentExtract)
                .put("excludingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(currentExtract.getExcludeDate()))
                .put("excludingDateStr", new SimpleDateFormat("«dd» MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(currentExtract.getExcludeDate()))
                .put("diplomaQualification", hs.getQualificationTitleNullSafe())
                .put("educationOrgUnit", hs.getPrintTitleWithoutOrgUnit())
                .put("stateExams", currentExtract.isProvidedExam() ? "сдавших итоговый междисциплинарный экзамен, " : "");
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, ExcludeSingGrpStuListExtract firstExtract)
    {
        return null;
    }
}