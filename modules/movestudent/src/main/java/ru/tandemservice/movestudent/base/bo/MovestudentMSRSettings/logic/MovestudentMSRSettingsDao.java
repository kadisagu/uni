/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.logic;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.gen.OrgUnitGen;
import ru.tandemservice.movestudent.entity.CodesMSRForFormativeOrgUnit;
import ru.tandemservice.movestudent.entity.CodesMSRForStudentOrders;
import ru.tandemservice.movestudent.entity.CodesMSRForStudentStatus;
import ru.tandemservice.movestudent.entity.ExportMSRSettings;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Ekaterina Zvereva
 * @since 18.12.2014
 */
public class MovestudentMSRSettingsDao extends UniBaseDao implements IMovestudentMSRSettingsDao
{
    private static final String MOSCOW_KLADR_CODE = "7700000000000";
    private static final Integer LOCATION_IN_MOSCOW = 1;
    private static final Integer LOCATION_NOT_IN_MOSCOW = 2;

    @Override
    public Map<OrgUnit, String> getMapCodesForFormativeOrgUnit()
    {
        List<CodesMSRForFormativeOrgUnit> list = getList(CodesMSRForFormativeOrgUnit.class);
        Map<OrgUnit, String> result = new HashMap<>(list.size());
        for(CodesMSRForFormativeOrgUnit item : list)
        {
            result.put(item.getOrgUnit(), item.getCode());
        }

        return result;
    }

    @Override
    public void saveOrUpdateCodesForFormativeOrgUnit(Map<OrgUnit, String> mapCodes)
    {
        new DQLDeleteBuilder(CodesMSRForFormativeOrgUnit.class).createStatement(getSession()).execute();

        for (Map.Entry<OrgUnit, String> item : mapCodes.entrySet())
        {
            CodesMSRForFormativeOrgUnit code = new CodesMSRForFormativeOrgUnit();
            code.setOrgUnit(item.getKey());
            code.setCode(item.getValue());
            saveOrUpdate(code);
        }
    }

    @Override
    public ExportMSRSettings getSettings()
    {
        return new DQLSelectBuilder().fromEntity(ExportMSRSettings.class, "s", true)
                .column(property("s"))
                .createStatement(getSession())
                .uniqueResult();
    }

    @Override
    public void saveSettings(ExportMSRSettings settings)
    {
        saveOrUpdate(settings);
    }

    @Override
    public Map<StudentStatus, Long> getMapStudentStatusCodes()
    {
        List<CodesMSRForStudentStatus> list = getList(CodesMSRForStudentStatus.class);
        Map<StudentStatus, Long> result = new HashMap<>(list.size());
        for(CodesMSRForStudentStatus item : list)
        {
            result.put(item.getStudentStatus(), item.getCode());
        }

        return result;
    }

    @Override
    public void saveOrUpdateStudentStatusCodes(Map<StudentStatus, Long> mapCodes)
    {
        new DQLDeleteBuilder(CodesMSRForStudentStatus.class).createStatement(getSession()).execute();

        for (Map.Entry<StudentStatus, Long> item : mapCodes.entrySet())
        {
            if (item.getValue() != null)
            {
                CodesMSRForStudentStatus code = new CodesMSRForStudentStatus();
                code.setStudentStatus(item.getKey());
                code.setCode(item.getValue());
                saveOrUpdate(code);
            }
        }
    }

    @Override
    public Map<StudentExtractType, Integer> getMapOrdersCodeRelation()
    {
        List<CodesMSRForStudentOrders> list = getList(CodesMSRForStudentOrders.class);
        Map<StudentExtractType, Integer> result = new HashMap<>(list.size());
        for(CodesMSRForStudentOrders item : list)
        {
            result.put(item.getExtractType(), item.getCode());
        }

        return result;
    }

    @Override
    public void saveOrUpdateOrdersCodesRelation(Map<StudentExtractType, Integer> mapCodes)
    {
        new DQLDeleteBuilder(CodesMSRForStudentOrders.class).createStatement(getSession()).execute();

        for (Map.Entry<StudentExtractType, Integer> item : mapCodes.entrySet())
        {
            if (item.getValue() != null)
            {
                CodesMSRForStudentOrders code = new CodesMSRForStudentOrders();
                code.setExtractType(item.getKey());
                code.setCode(item.getValue());
                saveOrUpdate(code);
            }
        }
    }


    /**
     * Заполняем мап адресов территориальных подразделений
     */
    @Override
    public Map<Long, Integer> getTerritorialOrgUnitAddresses(OrgUnit formativeOU)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "st")
                .joinPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias("st"), "eou")
                .column(property("eou", EducationOrgUnit.territorialOrgUnit().id()))
                .where(eq(property("eou", EducationOrgUnit.formativeOrgUnit()), value(formativeOU)))
                .distinct();
        List<Long> ids = getList(builder);
        List<OrgUnit> orgUnits = getList(OrgUnit.class, ids);

        List<String> errorOU = orgUnits.stream()
                .filter(entry -> (entry.getAddress() == null || entry.getAddress().getSettlement() == null))
                .map(OrgUnitGen::getFullTitle)
                .collect(Collectors.toList());
        if (!errorOU.isEmpty())
            throw new ApplicationException("Отчет не может быть построен, т.к. не задан населенный пункт в фактическом адресе для подразделений: " + CoreStringUtils.join(errorOU, ", "));

        Map<Long, Integer> resultMap = new HashMap<>();
        for (OrgUnit orgUnit : orgUnits)
        {
            AddressItem settlement = orgUnit.getAddress().getSettlement();
            resultMap.put(orgUnit.getId(), LOCATION_NOT_IN_MOSCOW);
            while (settlement != null)
            {
                if (settlement.getCode().equals(MOSCOW_KLADR_CODE))
                {
                    resultMap.put(orgUnit.getId(), LOCATION_IN_MOSCOW);
                    break;
                }
                settlement = settlement.getParent();
            }
        }
        return resultMap;
    }
}
