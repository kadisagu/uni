/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e48.ParagraphAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.CourseTransferCustomStuListExtract;
import org.tandemframework.hibsupport.builder.MQBuilder;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.unimove.IAbstractExtract;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ListExtractComponentGenerator
 * @since 03.02.2017
 */
public class DAO extends AbstractListParagraphAddEditDAO<CourseTransferCustomStuListExtract, Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));

        model.setGroupListModel(new GroupSelectModel(model, model, null, model.getParagraph().getOrder().getOrgUnit())
        {
            @Override
            protected boolean isNeedRequest()
            {
                if (getGroupModel().getCourse() == null) return false;
                if (getEduLevelModel().getDevelopForm() == null) return false;
                return getOrgUnit() != null || getEduLevelModel().getFormativeOrgUnit() != null;
            }

            protected DQLSelectBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, G_ALIAS).column(G_ALIAS);

                if (o != null)
                {
                    if (o instanceof Long) builder.where(eq(property(G_ALIAS + ".id"), value((Long) o)));
                    else if (o instanceof Collection) builder.where(in(property(G_ALIAS + ".id"), (Collection) o));
                }

                builder.where(eq(property(G_ALIAS, Group.educationOrgUnit().formativeOrgUnit()), value(getEduLevelModel().getFormativeOrgUnit())));
                builder.where(eq(property(G_ALIAS, Group.course()), value(getGroupModel().getCourse())));
                builder.where(eq(property(G_ALIAS, Group.educationOrgUnit().developForm()), value(getEduLevelModel().getDevelopForm())));
                builder.where(eq(property(G_ALIAS, Group.archival()), value(Boolean.FALSE)));

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property(G_ALIAS, Group.P_TITLE), value(CoreStringUtils.escapeLike(filter, true))));

                builder.order(property(G_ALIAS, Group.P_TITLE), OrderDirection.asc);

                return builder;
            }
        });
        model.setDevelopFormModel(EducationOrgUnitUtil.getDevelopFormSelectModel(model, false));

        model.setCourseNewListModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getCourse() == null) return ListResult.getEmpty();

                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder
                        .fromEntity(Course.class, "c")
                        .where(eq(property(Course.intValue().fromAlias("c")), value(model.getCourse().getIntValue() + 1)));
                List<Course> resultList =builder.createStatement(getSession()).list();
                if (resultList.isEmpty())
                    throw new ApplicationException("Для указанного курса нет следующего.");

                return new ListResult<>(resultList);
            }
        });

        if (model.getParagraphId() != null)
        {
            List<CourseTransferCustomStuListExtract> extracts = (List<CourseTransferCustomStuListExtract>) model.getParagraph().getExtractList();
            List<Group> groups = extracts.stream().map(e->e.getEntity().getGroup()).distinct().collect(Collectors.toList());
            model.setGroupList(groups);
            CourseTransferCustomStuListExtract firstExtract = model.getFirstExtract();
            model.setCourse(firstExtract.getCourseOld());
            model.setDevelopForm(firstExtract.getEntity().getGroup().getEducationOrgUnit().getDevelopForm());
            model.setDeadlineDate(firstExtract.getDeadlineDate());
        }

    }



    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.course().s(), model.getCourse()));
        if (null != model.getGroupList() && !model.getGroupList().isEmpty())
            builder.add(MQExpression.in(STUDENT_ALIAS, Student.L_GROUP, model.getGroupList()));

        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developForm().s(), model.getDevelopForm()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().formativeOrgUnit(), model.getParagraph().getOrder().getOrgUnit()));

        if (model.getCompensationType() != null)
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.compensationType().s(), model.getCompensationType()));

        //Не включать в список тех, кто уже имеется в данном приказе
        MQBuilder subBuilder = new MQBuilder(CourseTransferCustomStuListExtract.ENTITY_CLASS, "st", new String[]{CourseTransferCustomStuListExtract.entity().id().s()})
                .add(MQExpression.eq("st", CourseTransferCustomStuListExtract.paragraph().order().id().s(), model.getParagraph().getOrder().getId()))
                .add(MQExpression.notEq("st", CourseTransferCustomStuListExtract.paragraph().id().s(), model.getParagraphId()));
        builder.add(MQExpression.notIn(STUDENT_ALIAS, Student.P_ID, subBuilder));

        builder.addOrder(STUDENT_ALIAS, Student.group().s());
        builder.addOrder(STUDENT_ALIAS, Student.person().identityCard().fullFio().s());
    }
    
    @Override
    protected CourseTransferCustomStuListExtract createNewInstance(Model model)
    {
        return new CourseTransferCustomStuListExtract();
    }

    @Override
    protected void fillExtract(CourseTransferCustomStuListExtract extract, Student student, Model model)
    {
        extract.setCourseNew(model.getCourseNew());
        extract.setCourseOld(model.getCourse());

        final IValueMapHolder conditionalTransferValueHolder = (IValueMapHolder) model.getDataSource().getColumn("conditionalTransfer");
        final Map<Long, Boolean> conditionalTransferMap = (null == conditionalTransferValueHolder ? Collections.emptyMap() : conditionalTransferValueHolder.getValueMap());
        extract.setDeadlineDate(model.getDeadlineDate());
        extract.setConditionalTransfer(conditionalTransferMap.get(student.getId()));

    }

    @Override
    public void prepareColumns(Model model)
    {
        if (model.getParagraphId() != null)
        {
            Map<Long, Boolean> conditionalTransfMap = new HashMap<>();
            List<CourseTransferCustomStuListExtract> extractList = getList(CourseTransferCustomStuListExtract.class, IAbstractExtract.L_PARAGRAPH, model.getParagraph());

            for (CourseTransferCustomStuListExtract ext : extractList)
            {
                conditionalTransfMap.put(ext.getEntity().getId(), ext.isConditionalTransfer());
            }
            ((BlockColumn) model.getDataSource().getColumn("conditionalTransfer")).setValueMap(conditionalTransfMap);
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);
        ErrorCollector errs = UserContext.getInstance().getErrorCollector();

        final IValueMapHolder conditionalTransferValueHolder = (IValueMapHolder) model.getDataSource().getColumn("conditionalTransfer");
        final Map<Long, Boolean> conditionalTransferMap = (null == conditionalTransferValueHolder ? Collections.emptyMap() : conditionalTransferValueHolder.getValueMap());
        if (conditionalTransferMap.values().contains(Boolean.TRUE) && model.getDeadlineDate() == null)
            errs.add("Необходимо указать дату ликвидации задолженности для студентов, переводимых условно", "deadline");
    }
}