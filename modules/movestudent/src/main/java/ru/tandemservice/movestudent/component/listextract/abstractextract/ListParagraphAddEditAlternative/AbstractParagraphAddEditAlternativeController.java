/*$Id$*/
package ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEditAlternative;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author DMITRY KNYAZEV
 * @since 30.05.2014
 */
public abstract class AbstractParagraphAddEditAlternativeController<T extends ListStudentExtract, IDAO extends IAbstractParagraphAddEditAlternativeDAO<T, Model>, Model extends AbstractParagraphAddEditAlternativeModel<T>> extends AbstractListParagraphAddEditController<T, IDAO, Model>
{
	@Override
	public void onRefreshComponent(IBusinessComponent component)
	{
		Model model = getModel(component);
		getDao().prepare(model);
		model.setFilterPage(getClass().getPackage().getName() + ".Filters");
		model.setAttributesPage(getClass().getPackage().getName() + ".Attributes");

		model.setSearchStuListSettingsKey("StudentListParagraphAltAddEdit." + model.getParagraphType().getCode() + ".");
		model.setSearchListSettingsKey("StudentListParagraphAddEdit." + model.getParagraphType().getCode() + ".");

		prepareListDataSource(component);
		prepareSearchDataSource(component);
	}

	private void prepareSearchDataSource(IBusinessComponent component)
	{
		Model model = getModel(component);
		if (model.getSearchDataSource() != null) return;
		DynamicListDataSource<Student> dataSource = UniBaseUtils.createDataSource(component, getDao());

		CheckboxColumn checkboxColumn = new CheckboxColumn(Model.P_CHECKBOX_SELECTED_COLUMN, "", true);
		checkboxColumn.setDisabledProperty(AbstractListParagraphAddEditModel.P_STUDENT_DISABLED);
		checkboxColumn.setDisplayTotal(true);
		checkboxColumn.setParametersResolver((entity, valueEntity) -> entity);
		dataSource.addColumn(checkboxColumn);

		prepareSearchDataSource(component, dataSource);
		model.setSearchDataSource(dataSource);
	}

	protected void prepareSearchDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
	{
		dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false));
		dataSource.addColumn(new SimpleColumn("Пол", Student.person().identityCard().sex().shortTitle().s()).setClickable(false).setOrderable(false));
		dataSource.addColumn(new SimpleColumn("Дополнительный статус", Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
		dataSource.addColumn(new SimpleColumn("status", "Состояние", Model.STATUS_KEY, SimpleFormatter.INSTANCE).setClickable(false));
		dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false));
		dataSource.addColumn(new SimpleColumn("Группа", Student.group().title().s()).setClickable(false));
		dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
		dataSource.addColumn(new SimpleColumn("Форма освоения", Student.educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
		dataSource.addColumn(new SimpleColumn("Формирующее подр.", new String[]{Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_FULL_TITLE}).setClickable(false).setOrderable(false));
		dataSource.addColumn(new SimpleColumn("Территориальное подр.", new String[]{Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_FULL_TITLE}).setClickable(false).setOrderable(false));
		dataSource.addColumn(new SimpleColumn(EducationOrgUnit.EDU_LVL_HS_COLUMN_TITLE, Student.educationOrgUnit().educationLevelHighSchool().configurableTitle().s()).setClickable(false));
		dataSource.setOrder("status", OrderDirection.asc);
	}

	public void onClickAdd(IBusinessComponent component)
	{
		final Model model = getModel(component);
		final List<Student> selectedList = ((CheckboxColumn) model.getSearchDataSource().getColumn(Model.P_CHECKBOX_SELECTED_COLUMN)).getSelectedObjects().stream()
            .map(o -> (Student) ((ViewWrapper) o).getEntity())
            .collect(Collectors.toList());

        model.getAddedStudentsList().addAll(selectedList);
        model.getAddedStudentsList().sort(Student.FULL_FIO_AND_ID_COMPARATOR);
	}

	public void onClickDeleteStudent(IBusinessComponent component)
	{
		Student student = getDao().getNotNull(Student.class, (Long) component.getListenerParameter());
		getModel(component).getAddedStudentsList().remove(student);
	}

	private void prepareListDataSource(IBusinessComponent component)
	{
		Model model = getModel(component);
		if (model.getDataSource() != null) return;
		DynamicListDataSource<Student> dataSource = UniBaseUtils.createDataSource(component, getDao());
		prepareListDataSource(component, dataSource);
		model.setDataSource(dataSource);
	}

	@Override
	protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
	{
		super.prepareListDataSource(component, dataSource);
		dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteStudent", "Удалить «{0}»?", Student.FIO_KEY));
	}
}
