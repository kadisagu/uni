/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractAddEdit;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.component.commons.ExtractTypeSelectModel;
import ru.tandemservice.movestudent.component.commons.OrderCategory;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.EducationLevelDAO;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.Date;

/**
 * @author vip_delete
 * @since 05.11.2008
 */
public abstract class ModularStudentExtractAddEditDAO<T extends ModularStudentExtract, Model extends ModularStudentExtractAddEditModel<T>> extends UniDao<Model> implements IModularStudentExtractAddEditDAO<T, Model>
{
    @Override
    public void prepare(final Model model)
    {
        model.setAddForm(model.getExtractId() == null);
        model.setEditForm(!model.isAddForm());

        model.setExtractTypeList(MoveStudentDaoFacade.getMoveStudentDao().getModularStudentExtractTypeList());

        model.setExtractTypeListModel(new ExtractTypeSelectModel(null != model.getIndividual() && model.getIndividual() ? OrderCategory.INDIVIDUAL : OrderCategory.MODULAR));

        if (model.getExtractType() != null && model.getExtractType().getId() != null)
            model.setExtractType(getNotNull(StudentExtractType.class, model.getExtractType().getId()));

        if (null != model.getFormingOrgUnitId())
            model.setFormingOrgUnit(get(OrgUnit.class, model.getFormingOrgUnitId()));

        if (model.isEditForm())
        {
            model.setExtract(refresh(this.<T>getNotNull(model.getExtractId())));
            model.setExtractType(model.getExtract().getType());
        }
        else
        {
            model.setExtract(createNewInstance());
        }

        if (model.getExtract() != null)
        {
            if (model.getExtract().getEntity() == null)
                model.getExtract().setEntity(refresh(getNotNull(Student.class, model.getStudentId())));
            syncPreCommitData(model);

            if (model.isAddForm())
            {
                model.getExtract().setCreateDate(new Date());
                GrammaCase rusCase = getStudentTitleCase();
                if (rusCase != null)
                {
                    IdentityCard identityCard = model.getExtract().getEntity().getPerson().getIdentityCard();
                    InflectorVariant variant = DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(rusCase);
                    model.getExtract().setStudentTitle(PersonManager.instance().declinationDao().getCalculatedFIODeclination(identityCard, variant.getCode()));
                }
                else
                    model.getExtract().setStudentTitle(model.getExtract().getEntity().getPerson().getFullFio());
            }
        }
    }

    protected abstract GrammaCase getStudentTitleCase();

    @Override
    public void update(Model model)
    {
        T extract = model.getExtract();

        if (model.isEditForm())
        {
            if (extract.getEntity().isArchival())
                throw new ApplicationException("Редактирование выписки возможно только только у неархивных студентов.");

            if (extract.isIndividual())
            {
                final String orderStateCode = getProperty(AbstractStudentOrder.class, AbstractStudentOrder.state().code().s(), "id", extract.getParagraph().getOrder().getId());
                if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(orderStateCode))
                    throw new ApplicationException("Редактирование приказа возможно только на этапе формирования.");
            }
            else if (!UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE.equals(MoveStudentDaoFacade.getMoveStudentDao().getExtractStateCode(extract.getId())))
            {
                throw new ApplicationException("Редактирование выписки возможно только на этапе формирования.");
            }
        }
        else
        {
            if (extract.getEntity().isArchival())
                throw new ApplicationException("Создание выписки возможно только только для неархивных студентов.");

            if (!MoveStudentDaoFacade.getMoveStudentDao().isMoveAccessible(extract.getEntity()))
                throw new ApplicationException("Нельзя добавить выписку, так как у студента уже есть непроведенные выписки.");

            extract.setType(model.getExtractType());
            extract.setOrgUnit(model.getFormingOrgUnit());
        }

        //еще раз обновляем текстовые значения старых атрибутов
        syncPreCommitData(model);

        saveOrUpdate(extract);
    }

    private void syncPreCommitData(Model model)
    {
        T extract = model.getExtract();
        Student student = extract.getEntity();

        extract.setStudentTitleStr(student.getPerson().getFullFio());
        extract.setStudentStatusStr(student.getStatus().getTitle());
        extract.setPersonalNumberStr(student.getPersonalNumber());
        extract.setCourseStr(student.getCourse().getTitle());
        extract.setGroupStr(student.getGroup() == null ? "" : student.getGroup().getTitle());
        extract.setCompensationTypeStr(student.getCompensationType().getShortTitle());
        extract.setFormativeOrgUnitStr(student.getEducationOrgUnit().getFormativeOrgUnit().getFullTitle());
        extract.setTerritorialOrgUnitStr(student.getEducationOrgUnit().getTerritorialOrgUnit().getTerritorialFullTitle());
        extract.setEducationLevelHighSchoolStr((String) student.getEducationOrgUnit().getEducationLevelHighSchool().getProperty(EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY));
        extract.setDevelopFormStr(student.getEducationOrgUnit().getDevelopForm().getTitle());
        extract.setDevelopConditionStr(student.getEducationOrgUnit().getDevelopCondition().getTitle());
        extract.setDevelopTechStr(student.getEducationOrgUnit().getDevelopTech().getTitle());
        extract.setDevelopPeriodStr(student.getEducationOrgUnit().getDevelopPeriod().getTitle());
    }

    protected abstract T createNewInstance();
}