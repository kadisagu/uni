/* $Id$ */
package ru.tandemservice.movestudent.component.catalog.studentOrderBasics.StudentOrderBasicsPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics;

/**
 * @author AutoGenerator
 *         Created on 04.12.2012
 */
public class Controller extends DefaultCatalogPubController<StudentOrderBasics, Model, IDAO>
{
    @Override
    protected DynamicListDataSource<StudentOrderBasics> createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<StudentOrderBasics> dataSource = new DynamicListDataSource<>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", StudentOrderBasics.P_TITLE));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", StudentOrderBasics.P_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        return dataSource;
    }
}