/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.modular.StudentModularOrderAdd;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.component.commons.ExtractListUtil;
import ru.tandemservice.movestudent.component.commons.IMoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.MoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.OrderCategory;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StudentModularParagraph;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.Date;

/**
 * @author vip_delete
 * @since 20.10.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry(IMoveStudentMQBuilder.EXTRACT_ALIAS);

    static
    {
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_FULL_FIO, new OrderDescription(IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS, IdentityCard.P_LAST_NAME), new OrderDescription(IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS, IdentityCard.P_FIRST_NAME), new OrderDescription(IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS, IdentityCard.P_MIDDLE_NAME));
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_EXTRACT_FORMATIVE_ORG_UNIT_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_TITLE}));
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_EXTRACT_TERRITORIAL_ORG_UNIT_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_TITLE}));
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_EXTRACT_PRODUCING_ORG_UNIT_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.L_ORG_UNIT, OrgUnit.P_TITLE}));
    }

    @Override
    public void prepare(Model model)
    {
        // init main params
        model.getOrder().setCreateDate(new Date());
        if (model.getOrgUnitId() != null)
            model.getOrder().setOrgUnit((OrgUnit) getNotNull(model.getOrgUnitId()));
        ExtractListUtil.initExtractListModel(model, "studentModularOrderAdd", OrderCategory.MODULAR);
        model.setEmployeePostModel(new OrderExecutorSelectModel());
        if (null == model.getOrder().getId())
        {
            IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
            if (principalContext instanceof EmployeePost)
                model.setEmployeePost((EmployeePost) principalContext);
        }
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        // init datasource
        IMoveStudentMQBuilder builder = MoveStudentMQBuilder.createExtractMQBuilder(ModularStudentExtract.class, model.getSettings());

        // apply user defined restrictions
        builder.getMQBuilder().add(MQExpression.eq(IMoveStudentMQBuilder.STATE_ALIAS, ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED));

        if (model.getOrder().getOrgUnit() != null)
        {
            // условие для сборных приказов созданных на подразделении
            AbstractExpression eqFormative = MQExpression.eq(IMoveStudentMQBuilder.EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getOrder().getOrgUnit());
            AbstractExpression eqTerritorial = MQExpression.eq(IMoveStudentMQBuilder.EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getOrder().getOrgUnit());
            AbstractExpression eqOrgUnit = MQExpression.eq(IMoveStudentMQBuilder.EXTRACT_ALIAS, AbstractStudentExtract.L_ORG_UNIT, model.getOrder().getOrgUnit());
            AbstractExpression nullOrgUnit = MQExpression.isNull(IMoveStudentMQBuilder.EXTRACT_ALIAS, AbstractStudentExtract.L_ORG_UNIT);
            builder.getMQBuilder().add(MQExpression.or(eqOrgUnit, MQExpression.and(MQExpression.or(eqFormative, eqTerritorial), nullOrgUnit)));
        }

        // apply filters
        builder.applyExtractCreateDate();
        builder.applyExtractLastName();
        builder.applyExtractFormativeOrgUnit();
        builder.applyExtractTerritorialOrgUnit();
        builder.applyExtractProducingOrgUnit();
        builder.applyExtractEducationLevelHighSchool();
        builder.applyExtractCourse();
        builder.applyExtractDevelopForm();
        builder.applyExtractCompensationType();
        builder.applyExtractType();

        // create page
        builder.createPage(getSession(), _orderSettings, model.getDataSource());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        MoveStudentDaoFacade.getMoveStudentDao().checkOrderNumber(model.getOrder());

        // save order
        save(model.getOrder());

        // save executor
        String responsible = OrderExecutorSelectModel.getExecutor(model.getResponsibleEmployee());
        if (responsible != null) model.getOrder().setResponsible(responsible);
        model.getOrder().setResponsibleEmpl(model.getResponsibleEmployee());

        String executor = OrderExecutorSelectModel.getExecutor(model.getEmployeePost());
        if (executor != null) model.getOrder().setExecutor(executor);
        model.getOrder().setExecutorEmpl(model.getEmployeePost());

        // save extracts
        int count = 1;
        for (IEntity entity : ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).getSelectedObjects())
        {
            IAbstractExtract extract = (IAbstractExtract) entity;
            if (extract.getParagraph() != null)
                throw new ApplicationException(extract.getTitle() + " уже добавлена в приказ №" + extract.getParagraph().getOrder().getNumber()+".");

            if (!UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED.equals(extract.getState().getCode()))
                throw new ApplicationException(extract.getTitle() + " не согласован.");

            //create paragraph
            StudentModularParagraph paragraph = new StudentModularParagraph();
            paragraph.setNumber(count++);
            paragraph.setOrder(model.getOrder());
            save(paragraph);

            //update extract
            extract.setNumber(1);
            extract.setParagraph(paragraph);
            extract.setState(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
            update(extract);
        }
    }
}
