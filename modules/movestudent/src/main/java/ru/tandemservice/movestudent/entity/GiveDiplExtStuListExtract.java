package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.movestudent.entity.gen.GiveDiplExtStuListExtractGen;

import java.util.Date;

/**
 * Проект приказа «О выдаче диплома и присвоении квалификации»
 */
public class GiveDiplExtStuListExtract extends GiveDiplExtStuListExtractGen implements IExcludeExtract
{
    @Override
    public Date getExcludeDate()
    {
        return getBeginDate();
    }
}