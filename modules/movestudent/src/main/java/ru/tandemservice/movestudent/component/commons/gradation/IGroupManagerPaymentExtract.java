/* $Id$ */
package ru.tandemservice.movestudent.component.commons.gradation;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Интерфейс для выписок, где начисляются надбавки старостам.
 *
 * @author Nikolay Fedorovskih
 * @since 04.07.2013
 */
public interface IGroupManagerPaymentExtract
{
    /**
     * Дата начала выплаты надбавки для старосты
     * @return дата
     */
    Date getGroupManagerBonusBeginDate();

    /**
     * Дата окончания выплаты надбавки для старосты
     * @return дата
     */
    Date getGroupManagerBonusEndDate();

    /**
     * Есть ли надбавка для старосты
     * @return да или нет
     */
    boolean hasGroupManagerBonus();

    /**
     * Сумма надбавки для старосты.
     */
    BigDecimal getGroupManagerPaymentAmount();
}