/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.movestudent.entity.*;

/**
 * Класс для наследования в скрипте (чтобы добавление метода в интерфейс не ломало скрипт).
 *
 * !!! В тела методов ничего не писать !!!
 *
 * @author Nikolay Fedorovskih
 * @since 23.07.2015
 */
public class EmptyBaseMoveStudentInjectModifier implements IMoveStudentInjectModifier
{
    @Override
    public void modularExtractModifier(RtfInjectModifier modifier, ModularStudentExtract extract)
    {
       // оставить пустым
    }

    @Override
    public void modularExtractTableModifier(RtfTableModifier modifier, ModularStudentExtract extract)
    {
        // оставить пустым
    }

    @Override
    public void modularOrderModifier(RtfInjectModifier modifier, StudentModularOrder order)
    {
        // оставить пустым
    }

    @Override
    public void modularOrderTableModifier(RtfTableModifier modifier, StudentModularOrder order)
    {
        // оставить пустым
    }

    @Override
    public void individualOrderModifier(RtfInjectModifier modifier, StudentModularOrder order, ModularStudentExtract extract)
    {
        // оставить пустым
    }

    @Override
    public void individualOrderTableModifier(RtfTableModifier modifier, StudentModularOrder order, ModularStudentExtract extract)
    {
        // оставить пустым
    }

    @Override
    public void listExtractModifier(RtfInjectModifier modifier, ListStudentExtract extract)
    {
        // оставить пустым
    }

    @Override
    public void listExtractTableModifier(RtfTableModifier modifier, ListStudentExtract extract)
    {
        // оставить пустым
    }

    @Override
    public void listOrderModifier(RtfInjectModifier modifier, StudentListOrder order, ListStudentExtract firstExtract)
    {
        // оставить пустым
    }

    @Override
    public void listOrderTableModifier(RtfTableModifier modifier, StudentListOrder order)
    {
        // оставить пустым
    }

    @Override
    public void listParagraphModifier(RtfInjectModifier modifier, StudentListParagraph paragraph, ListStudentExtract firstExtract)
    {
        // оставить пустым
    }

    @Override
    public void listParagraphTableModifier(RtfTableModifier modifier, StudentListParagraph paragraph, ListStudentExtract firstExtract)
    {
        // оставить пустым
    }

}