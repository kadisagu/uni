/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e40.AddEdit;

import java.util.Date;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.GiveDiplIncompleteStuListExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 09.02.2011
 */
public class Model extends CommonModularStudentExtractAddEditModel<GiveDiplIncompleteStuListExtract>
{

    private String _dischargeOrderNumber;
    private Date _dismissDate;
    private Date _beginYear;
    private Date _endYear;

    public String getDischargeOrderNumber()
    {
        return _dischargeOrderNumber;
    }

    public void setDischargeOrderNumber(String dischargeOrderNumber)
    {
        _dischargeOrderNumber = dischargeOrderNumber;
    }

    public Date getDismissDate()
    {
        return _dismissDate;
    }

    public void setDismissDate(Date dismissDate)
    {
        _dismissDate = dismissDate;
    }

    public Date getBeginYear()
    {
        return _beginYear;
    }

    public void setBeginYear(Date beginYear)
    {
        _beginYear = beginYear;
    }

    public Date getEndYear()
    {
        return _endYear;
    }

    public void setEndYear(Date endYear)
    {
        _endYear = endYear;
    }
}
