/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e6;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.TransferCompTypeStuExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 24.12.2008
 */
public class TransferCompTypeStuExtractPrint implements IPrintFormCreator<TransferCompTypeStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, TransferCompTypeStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        //TODO: выбрать одну из меток - transferDate или entryIntoForceDate (в шаблоне сейчас transferDate). Связанные задачи: DEV-664, DEV-1319
        CommonExtractPrint.addEntryDateToModifier(modifier, extract.getTransferDate());
        modifier.put("transferDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getTransferDate()));

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}