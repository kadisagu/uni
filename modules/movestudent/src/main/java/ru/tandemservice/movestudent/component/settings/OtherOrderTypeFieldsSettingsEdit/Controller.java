/* $Id$ */
package ru.tandemservice.movestudent.component.settings.OtherOrderTypeFieldsSettingsEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author Dmitry Seleznev
 * @since 11.04.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
        deactivate(component);
    }
}