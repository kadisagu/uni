\keep\keepn\qj\b {extractNumber}.\par\b0\par
\fi709\qj В связи {reasonStr} \par\fi0\par
приказываю:\par
\fi709\qj 
1. Предоставить {student_D} {fio} {birthDate} дата рождения, {course} курса {orgUnit_G} {developForm_GF} формы обучения {compensationTypeStr_G} академический отпуск {reason} с {beginDate} г. по {endDate} г. \par
{stopGrantPayingStr}\par
{assignPaymentStr}\par\par 
Основание: {listBasics}\par\fi0