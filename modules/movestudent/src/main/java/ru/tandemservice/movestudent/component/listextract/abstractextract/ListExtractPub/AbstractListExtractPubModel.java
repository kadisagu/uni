/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * @author vip_delete
 * @since 12.05.2009
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "extractId")
})
public class AbstractListExtractPubModel<T extends ListStudentExtract>
{
    private Long _extractId;
    private T _extract;
    private CommonPostfixPermissionModel _secModel;
    public static final String _numberEmptyName = "не задан";
    private boolean _numberEmpty;


    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        _extractId = extractId;
    }

    public T getExtract()
    {
        return _extract;
    }

    public void setExtract(T extract)
    {
        _extract = extract;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public boolean isExtractDeletable()
    {
        return UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(getExtract().getParagraph().getOrder().getState().getCode());
    }


    public void setNumberEmpty(boolean numberEmpty)
    {
        _numberEmpty=numberEmpty;
    }
    public boolean getNumberEmpty()
    {
        return _numberEmpty;
    }

    public String getNumberEmptyName()
    {
        return _numberEmptyName;
    }
}
