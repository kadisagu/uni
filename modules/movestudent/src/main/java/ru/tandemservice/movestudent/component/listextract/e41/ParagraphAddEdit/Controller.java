/* $Id: Controller.java.vm 7834 2009-05-06 09:39:03Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e41.ParagraphAddEdit;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressInter;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.component.listextract.e41.utils.EmployeePostVO;
import ru.tandemservice.movestudent.component.listextract.e41.utils.OrgUnitVO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 13.02.2013
 */
@SuppressWarnings("unchecked")
public class Controller extends AbstractListParagraphAddEditController<SendPracticeInnerStuListExtract, IDAO, Model>
{
    private GrammaCase grammaCase;

    @Override
    public void onActivateComponent(IBusinessComponent component)
    {
        setGrammaCase(GrammaCase.ACCUSATIVE);
    }

    @Override
    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дополнительный статус", Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("practiceHeader", "Руководитель практики").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("practiceHeaderStr", "Руководитель практики(печать)").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("practiceOrgUnit", "Подразделение").setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Заполнить по образцу", "clone", "onClickCopyHeaderWithOu").setDisabledProperty(AbstractListParagraphAddEditModel.P_STUDENT_DISABLED));

        Model model = getModel(component);
        if (model.isEditForm())
        {
            IValueMapHolder practiceOrgUnitHolder = (IValueMapHolder) dataSource.getColumn("practiceOrgUnit");
            Map<Long, OrgUnitVO> practiceOrgUnitsMap = (null == practiceOrgUnitHolder ? Collections.emptyMap() : practiceOrgUnitHolder.getValueMap());

            IValueMapHolder practiceHeaderHolder = (IValueMapHolder) dataSource.getColumn("practiceHeader");
            Map<Long, EmployeePostVO> practiceHeaderMap = (null == practiceHeaderHolder ? Collections.emptyMap() : practiceHeaderHolder.getValueMap());

            IValueMapHolder practiceHeaderStrHolder = (IValueMapHolder) dataSource.getColumn("practiceHeaderStr");
            Map<Long, String> practiceHeaderStrMap = (null == practiceHeaderStrHolder ? Collections.emptyMap() : practiceHeaderStrHolder.getValueMap());

            for (SendPracticeInnerStuListExtract extract : MoveStudentDaoFacade.getMoveStudentDao().<SendPracticeInnerStuListExtract>getExtractListByParagraph(model.getParagraph().getId(), false))
            {
                OrgUnitVO orgUnitVO = new OrgUnitVO(extract.getPracticeOrgUnit());
                orgUnitVO.setOrgUnitStr(extract.getPracticeOrgUnitStr());
                practiceOrgUnitsMap.put(extract.getEntity().getId(), orgUnitVO);

                if(null != extract.getPracticeHeader())
                    practiceHeaderMap.put(extract.getEntity().getId(), new EmployeePostVO(extract.getPracticeHeader(), extract.getPracticeHeaderDegree()));
                practiceHeaderStrMap.put(extract.getEntity().getId(), extract.getPracticeHeaderStr());
            }
        }
    }

    public void onChangeCourse(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setPracticeCourse(model.getCourse());
    }

    public void onClickCopyHeaderWithOu(IBusinessComponent component)
    {
        Model model = getModel(component);
        DynamicListDataSource dataSource = model.getDataSource();

        // Структурное подразделение
        final IValueMapHolder practiceOrgUnitHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceOrgUnit");
        final Map<Long, OrgUnitVO> practiceOrgUnitsMap = (null == practiceOrgUnitHolder ? Collections.emptyMap() : practiceOrgUnitHolder.getValueMap());

        OrgUnitVO currentOrgUnit = practiceOrgUnitsMap.get(component.getListenerParameter());
        if (null != currentOrgUnit)
        {
            for (ViewWrapper<Student> wrapper : (List<ViewWrapper<Student>>) dataSource.getSelectedEntities())
            {
                if (practiceOrgUnitsMap.get(wrapper.getEntity().getId()) == null)
                {
                    practiceOrgUnitsMap.put(wrapper.getEntity().getId(), currentOrgUnit);
                }
                else if (practiceOrgUnitsMap.get(wrapper.getEntity().getId()).getId().equals(currentOrgUnit.getId()))
                {
                    practiceOrgUnitsMap.get(wrapper.getEntity().getId()).setOrgUnitStr(currentOrgUnit.getOrgUnitStr());
                }
            }
        }

        // Руководитель практики
        final IValueMapHolder practiceHeaderHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceHeader");
        final Map<Long, EmployeePostVO> practiceHeaderMap = (null == practiceHeaderHolder ? Collections.emptyMap() : practiceHeaderHolder.getValueMap());

        EmployeePostVO currentPracticeHeader = practiceHeaderMap.get(component.getListenerParameter());

        if (null != currentPracticeHeader)
        {
            for (ViewWrapper<Student> wrapper : (List<ViewWrapper<Student>>) dataSource.getSelectedEntities())
            {
                if (practiceHeaderMap.get(wrapper.getEntity().getId()) == null)
                {
                    practiceHeaderMap.put(wrapper.getEntity().getId(), currentPracticeHeader);
                }
            }
        }

        // Руководитель практики(печать)
        final IValueMapHolder practiceHeaderStrHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceHeaderStr");
        final Map<Long, String> practiceHeaderStrMap = (null == practiceHeaderStrHolder ? Collections.emptyMap() : practiceHeaderStrHolder.getValueMap());

        String currentPracticeHeaderStr = practiceHeaderStrMap.get(component.getListenerParameter());

        if (!StringUtils.isEmpty(currentPracticeHeaderStr))
        {
            for (ViewWrapper<Student> wrapper : (List<ViewWrapper<Student>>) dataSource.getSelectedEntities())
            {
                if (practiceHeaderStrMap.get(wrapper.getEntity().getId()) == null)
                {
                    practiceHeaderStrMap.put(wrapper.getEntity().getId(), currentPracticeHeaderStr);
                }
            }
        }

        model.getDataSource().refresh();
    }

    public void onOrgUnitChange(IBusinessComponent component)
    {
        Model model = getModel(component);

        // Структурное подразделение
        final IValueMapHolder practiceOrgUnitHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceOrgUnit");
        final Map<Long, OrgUnitVO> practiceOrgUnitsMap = (null == practiceOrgUnitHolder ? Collections.emptyMap() : practiceOrgUnitHolder.getValueMap());

        OrgUnitVO currentOrgUnit = practiceOrgUnitsMap.get(component.getListenerParameter());

        if (null != currentOrgUnit)
        {
            StringBuilder orgUnitStr = new StringBuilder(currentOrgUnit.getOrgUnit().getTitle());

            AddressDetailed address = currentOrgUnit.getOrgUnit().getAddress();

            if (null != address)
            {
                if (null != address.getSettlement())
                {
                    orgUnitStr.append(", ").append(address.getSettlement().getTitleWithType());
                    if(address instanceof AddressRu && null != ((AddressRu)address).getStreet())
                    {
                        orgUnitStr.append(", ").append(((AddressRu)address).getStreet().getTitleWithType());
                        if (!StringUtils.isEmpty(((AddressRu)address).getHouseNumber()))
                        {
                            orgUnitStr.append(", д.").append(((AddressRu)address).getHouseNumber());
                            if (!StringUtils.isEmpty(((AddressRu)address).getHouseUnitNumber()))
                            {
                                orgUnitStr.append("-").append(((AddressRu)address).getHouseUnitNumber());
                            }
                        }
                    }
                    else if(address instanceof AddressInter && !StringUtils.isEmpty(((AddressInter) address).getAddressLocation()))
                    {
                        orgUnitStr.append(", ").append(((AddressInter) address).getAddressLocation());
                    }
                }

            }

            if (!StringUtils.isEmpty(currentOrgUnit.getOrgUnit().getPhone()))
                orgUnitStr.append(", тел. ").append(currentOrgUnit.getOrgUnit().getPhone());

            if (!StringUtils.isEmpty(currentOrgUnit.getOrgUnit().getEmail()))
                orgUnitStr.append(", ").append(currentOrgUnit.getOrgUnit().getEmail());

            currentOrgUnit.setOrgUnitStr(orgUnitStr.toString());
        }
    }


    public void onPreventAccidentsICChange(IBusinessComponent component)
    {
        Model model = getModel(component);

        if(null != model.getPreventAccidentsIC())
        {
            model.setPreventAccidentsICStr(getPreventAccidentsICStr(model.getPreventAccidentsIC()));
        }
    }

    public void onResponsForRecieveCashChange(IBusinessComponent component)
    {
        Model model = getModel(component);

        if(null != model.getResponsForRecieveCash())
        {
            model.setResponsForRecieveCashStr(getFioStr(model.getResponsForRecieveCash()));
        }
    }

    public void onPracticeHeaderChange(IBusinessComponent component)
    {
        Model model = getModel(component);

        // Руководитель практики
        final IValueMapHolder practiceHeaderHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceHeader");
        final Map<Long, EmployeePostVO> practiceHeaderMap = (null == practiceHeaderHolder ? Collections.emptyMap() : practiceHeaderHolder.getValueMap());

        EmployeePostVO currentPracticeHeader = practiceHeaderMap.get((Long)component.getListenerParameter());

        // Руководитель практики(печать)
        final IValueMapHolder practiceHeaderStrHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceHeaderStr");
        final Map<Long, String> practiceHeaderStrMap = (null == practiceHeaderStrHolder ? Collections.emptyMap() : practiceHeaderStrHolder.getValueMap());

//        String currentPracticeHeaderStr = practiceHeaderStrMap.get(component.getListenerParameter());

        if(null != currentPracticeHeader)
        {
            practiceHeaderStrMap.put(component.<Long>getListenerParameter(), getPracticeHeaderStr(currentPracticeHeader));
        }

    }

    protected String getPreventAccidentsICStr(EmployeePostVO employeePostVO)
    {
        StringBuilder preventAccidentsICStrBuilder = new StringBuilder();

        preventAccidentsICStrBuilder.append(getDegreeShortTitleWithDots(employeePostVO.getAcademicDegree()));
        String preventPostAT;
        if (getGrammaCase().equals(GrammaCase.ACCUSATIVE)) {
            preventPostAT = employeePostVO.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getPost().getAccusativeCaseTitle();
        } else {
            preventPostAT = employeePostVO.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle();
        }
        String preventAccidentsIC = !StringUtils.isEmpty(preventPostAT) ? preventPostAT :
                employeePostVO.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if(!StringUtils.isEmpty(preventAccidentsIC))
            preventAccidentsICStrBuilder.append(StringUtils.isEmpty(preventAccidentsICStrBuilder.toString()) ? "" : " ").append(preventAccidentsIC.toLowerCase());

        preventAccidentsICStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedIofInitials(employeePostVO.getEmployeePost().getPerson().getIdentityCard(), getGrammaCase()));

        return preventAccidentsICStrBuilder.toString();
    }


    private String getPracticeHeaderStr(EmployeePostVO employeePostVO)
    {
        EmployeePost practiceHeader = employeePostVO.getEmployeePost();

        StringBuilder practiceHeaderStrBuilder = new StringBuilder();

        practiceHeaderStrBuilder.append(getDegreeShortTitleWithDots(employeePostVO.getAcademicDegree()));

        String practiceHeaderPostNT = practiceHeader.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle();
        String practiceHeaderNominative = !StringUtils.isEmpty(practiceHeaderPostNT) ? practiceHeaderPostNT :
                practiceHeader.getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if(!StringUtils.isEmpty(practiceHeaderNominative))
            practiceHeaderStrBuilder.append(StringUtils.isEmpty(practiceHeaderStrBuilder.toString()) ? "" : " ").append(practiceHeaderNominative.toLowerCase());

        practiceHeaderStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedFioInitials(practiceHeader.getPerson().getIdentityCard(), GrammaCase.NOMINATIVE));

        return practiceHeaderStrBuilder.toString();
    }

    protected String getFioStr(EmployeePostVO employeePostVO)
    {
        EmployeePost practiceHeader = employeePostVO.getEmployeePost();

        StringBuilder fioStrBuilder = new StringBuilder();

        fioStrBuilder.append(getDegreeShortTitleWithDots(employeePostVO.getAcademicDegree()));

        String practiceHeaderPostAT;
        if (getGrammaCase().equals(GrammaCase.ACCUSATIVE)) {
            practiceHeaderPostAT = practiceHeader.getPostRelation().getPostBoundedWithQGandQL().getPost().getAccusativeCaseTitle();
        } else {
            practiceHeaderPostAT = practiceHeader.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle();
        }
        String practiceHeaderStr = !StringUtils.isEmpty(practiceHeaderPostAT) ? practiceHeaderPostAT :
                practiceHeader.getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if(!StringUtils.isEmpty(practiceHeaderStr))
            fioStrBuilder.append(StringUtils.isEmpty(fioStrBuilder.toString()) ? "" : " ").append(practiceHeaderStr.toLowerCase());

        fioStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedFioInitials(practiceHeader.getPerson().getIdentityCard(), getGrammaCase()));

        return fioStrBuilder.toString();
    }

    protected String getDegreeShortTitleWithDots(PersonAcademicDegree prevDegree)
    {
        String degreeShortTitleWithDots = "";
        if (null != prevDegree)
        {
            String[] degreeTitle = prevDegree.getAcademicDegree().getTitle().toLowerCase().split(" ");
            StringBuilder shortDegreeTitle = new StringBuilder();
            for (String deg : Lists.newArrayList(degreeTitle))
            {
                shortDegreeTitle.append(deg.charAt(0)).append(".");
            }
            degreeShortTitleWithDots += shortDegreeTitle;
        }
        return degreeShortTitleWithDots;
    }

    protected void setGrammaCase(GrammaCase grammaCase){
        this.grammaCase = grammaCase;
    }

    protected GrammaCase getGrammaCase(){
        return grammaCase;
    }
}
