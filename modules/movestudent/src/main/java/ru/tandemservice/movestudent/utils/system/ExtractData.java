/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.utils.system;

/**
 * @author vip_delete
 * @since 21.11.2008
 */
public class ExtractData
{
    //список всех entity выписок из сборного приказа
    public static final String[][] EXTRACT_LIST = new String[][]{
        /* 1*/{"weekendStuExtract", "О предоставлении академического отпуска"},
        /* 2*/{"restorationStuExtract", "О восстановлении"},
        /* 3*/{"weekendOutStuExtract", "О выходе из академического отпуска"},
        /* 4*/{"eduEnrolmentStuExtract", "О зачислении"},
        /* 5*/{"transferDevFormStuExtract", "О переводе на другую форму освоения"},
        /* 6*/{"transferCompTypeStuExtract", "О переводе на другую основу оплаты обучения"},
        /* 7*/{"transferEduTypeStuExtract", "О переводе на другое направление подготовки (специальность)"},
        /* 8*/{"transferStuExtract", "Об изменении пункта обучения"},
        /* 9*/{"excludeStuExtract", "Об отчислении"},
        /*10*/{"changeFioStuExtract", "О смене фамилии (имени)"},
        /*11*/{"revertOrderStuExtract", "Об отмене приказа"},
        /*12*/{"weekendPregnancyStuExtract", "О предоставлении отпуска по беременности и родам"},
        /*13*/{"weekendChildStuExtract", "О предоставлении отпуска по уходу за ребенком"},
        /*14*/{"eduEnrolmentStuExtractExt", "О зачислении"},
        /*15*/{"eduEnrAsTransferStuExtract", "О зачислении в порядке перевода"},
        /*16*/{"weekendStuExtractExt", "О предоставлении академического отпуска"},
        /*17*/{"restorationStuExtractExt", "О восстановлении"},
        /*18*/{"transferStuExtractExt", "О переводе"},
        /*19*/{"dischargingStuExtract", "Об отстранении от занятий"},
        /*20*/{"sessionProlongStuExtract", "О продлении экзаменационной сессии"},
        /*21*/{"prolongWeekndChildStuExtract", "О продлении отпуска по уходу за ребенком"},
        /*22*/{"acadGrantAssignStuExtract", "О назначении академической стипендии"},
        /*23*/{"socGrantAssignStuExtract", "О назначении социальной стипендии"},
        /*24*/{"socGrantStopStuExtract", "О приостановлении выплаты социальной стипендии"},
        /*25*/{"socGrantResumptionStuExtract", "О возобновлении выплаты социальной стипендии"},
        /*26*/{"excludeStuExtractExt", "Об отчислении"},
        /*27*/{"excludeTransfStuExtract", "Об отчислении в связи с переводом"},
        /*28*/{"giveBookDuplicateStuExtract", "О выдаче дубликата зачетной книжки"},
        /*29*/{"giveCardDuplicateStuExtract", "О выдаче дубликата студенческого билета"},
        /*30*/{"giveDiplomaDuplicateStuExtract", "О выдаче дубликата диплома"},
        /*31*/{"giveDiplAppDuplicateStuExtract", "О выдаче дубликата приложения к диплому"},
        /*32*/{"admissionStuExtract", "О допуске к занятиям"},
        /*33*/{"assignBenefitStuExtract", "О назначении выплаты пособий"},
        /*34*/{"changeFirstNameStuExtract", "О смене имени"},
        /*35*/{"changeMiddleNameStuExtract", "О смене отчества"},
        /*36*/{"reEducationStuExtract", "О повторном обучении"},
        /*37*/{"prolongWeekendStuExtract", "О продлении академического отпуска"},
        /*38*/{"weekendChildOutStuExtract", "О выходе из отпуска по уходу за ребенком"},
        /*39*/{"paymentDiscountStuExtract", "О скидке по оплате за обучение"},
        /*40*/{"giveDiplIncompleteStuListExtract","О выдаче диплома о неполном высшем профессиональном образовании"},
        /*41*/{"prescheduleSessionPassStuExtract", "О досрочной сдаче сессии"},
        /*42*/{"changePassDiplomaWorkPeriodStuExtract", "О переносе сроков сдачи защиты дипломной работы"},
        /*43*/{"changeDateStateExaminationStuExtract", "О переносе сроков сдачи государственного экзамена"},
        /*44*/{"repeatProtectDiplomaWorkStuExtract", "О повторной защите дипломной работы"},
        /*45*/{"repeatPassStateExamStuExtract", "О повторной сдаче государственного экзамена"},
        /*46*/{"freeStudiesAttendanceStuExtract", "О свободном посещении занятий"},
        /*105*/{"acceleratedLearningStuExtract", "О переводе на ускоренное обучение"}
    };
}