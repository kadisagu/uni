/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e6.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.TransferCompTypeStuExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 21.11.2008
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<TransferCompTypeStuExtract, Model>
{
    @Override
    protected TransferCompTypeStuExtract createNewInstance()
    {
        return new TransferCompTypeStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setEduModel(MoveStudentDaoFacade.getCommonExtractUtil().createEduModel(model.isAddForm(), model.getExtract(), true));
        //из-за специфики выписки надо отображать только "бюджет"
        
        if(model.isAddForm())
        {
            Student student = model.getExtract().getEntity();
            model.getEduModel().setFormativeOrgUnit(student.getEducationOrgUnit().getFormativeOrgUnit());
            model.getEduModel().setTerritorialOrgUnit(student.getEducationOrgUnit().getTerritorialOrgUnit());
            model.getEduModel().setCourse(student.getCourse());
            model.getEduModel().setGroup(student.getGroup());
            model.getEduModel().setEducationLevelsHighSchool(student.getEducationOrgUnit().getEducationLevelHighSchool());
            model.getEduModel().setDevelopForm(student.getEducationOrgUnit().getDevelopForm());
            model.getEduModel().setDevelopCondition(student.getEducationOrgUnit().getDevelopCondition());
            model.getEduModel().setDevelopTech(student.getEducationOrgUnit().getDevelopTech());
            model.getEduModel().setDevelopPeriod(student.getEducationOrgUnit().getDevelopPeriod());

            CompensationType compensationType = student.getCompensationType();
            if(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(compensationType.getCode()))
            {
                model.getEduModel().setCompensationType(get(CompensationType.class, CompensationType.code(), CompensationTypeCodes.COMPENSATION_TYPE_BUDGET));
            }
            else
            {
                model.getEduModel().setCompensationType(get(CompensationType.class, CompensationType.code(), CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT));
            }
        }


    }

    @Override
    public void update(Model model)
    {
        //save rollback data
        MoveStudentDaoFacade.getCommonExtractUtil().update(model.getEduModel(), model.getExtract());
        super.update(model);
    }
}
