/* $Id$ */
package ru.tandemservice.movestudent.utils.system.advgenerators;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Namespace;
import org.dom4j.dom.DOMComment;
import org.dom4j.dom.DOMDocument;
import org.dom4j.dom.DOMElement;
import org.dom4j.dom.DOMText;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudent.utils.system.TemplatesGenerator;
import ru.tandemservice.uni.util.system.UniSystemUtils;
import ru.tandemservice.unimove.service.AcceptVisaExtractService;
import ru.tandemservice.unimove.service.InitCoordinationVisaExtractService;
import ru.tandemservice.unimove.service.RejectVisaExtractService;
import ru.tandemservice.unimv.UnimvDefines;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Date;

/**
 * @author Dmitry Seleznev
 * @since 10.08.2012
 */
public class ModularExtractAdvancedGenerator
{
    private static String _tandemUniPath = UniSystemUtils.getModulesDir().getAbsolutePath();
    private static String _templatePath = _tandemUniPath + "/modules/movestudent/src/main/java/ru/tandemservice/movestudent/utils/system/templates";
    private static String _resourcesPath = _tandemUniPath + "/modules/movestudent/src/main/resources/movestudent";
    //private static String _srcCodesPath = _tandemUniPath + "/movestudent/src/main/java";

    public static void main(String[] args) throws Exception
    {
        String moduleOwner = System.getProperty("moduleOwner");
        if (StringUtils.isEmpty(moduleOwner))
            throw new RuntimeException("Plz specify printforms moduleOwner: for example 'unifefu'!");

        if (args.length == 0) throw new ApplicationException("There is no any extract description number specified!");

        ModularExtractMetaDescription description = ModularExtractAdvancedData.EXTRACT_DESCRIPTIONS_MAP.get(Integer.parseInt(args[0]));
        registerNewExtractTypeCatalogItems(description);
        registerNewExtractEntity(description);
        createExtractComponent(description);
        generateSpringDescription();
        generateComponentDescription();

        TemplatesGenerator.main(null);
        System.setProperty("moduleOwner", "movestudent");
        TemplatesGenerator.main(null);
    }

    private static void registerNewExtractTypeCatalogItems(ModularExtractMetaDescription metaDescription)
    {
        try
        {
            File file = new File(_resourcesPath + "/init/" + "movestudent.data.xml");
            String data = IOUtils.toString(new FileInputStream(file));

            if (data.indexOf(metaDescription.getMetaDescriptor().getEntityName()) > 0)
            {
                System.out.println("It seems you've already added that extract! Skipping catalog items adding.");
                return;
            }

			// Новый тип сборного приказа вставляем перед списочными. В случае, если метку все-таки кто-то удалит, добавляем непосредственно перед первым списочным приказом.
            int placeToInsertExtractType = data.indexOf("        <!-- СПИСОЧНЫЕ ПРИКАЗЫ -->");
			if (placeToInsertExtractType == -1)
				placeToInsertExtractType = data.indexOf("        <item id=\"2\" synchronize=\"system\" constant-name=\"list order\">");

			String constantName = metaDescription.getTypeConstantName() == null ? "" : " constant-name=\"" + metaDescription.getTypeConstantName() + "\"";

            StringBuilder result = new StringBuilder(data.substring(0, placeToInsertExtractType));
            result.append("        <item id=\"" + metaDescription.getTypeCode() + "\" synchronize=\"system\"" + constantName + ">\n");
            result.append("            <property name=\"title\" value=\"" + metaDescription.getTypeTitle() + "\" synchronize=\"user\"/>\n");
            if (null != metaDescription.getTypeDescription())
                result.append("            <property name=\"description\" value=\"" + metaDescription.getTypeDescription() + "\" synchronize=\"user\"/>\n");
            result.append("            <itemref name=\"parent\" id=\"1\" entity=\"studentExtractType\"/>\n");
            result.append("            <property name=\"active\" value=\"false\" synchronize=\"user\"/>\n");
            result.append("            <property name=\"businessObjectName\" value=\"" + metaDescription.getMetaDescriptor().getEntityName() + "\"/>\n");
            result.append("            <property name=\"index\" value=\"" + metaDescription.getTypeIndex() + "\"/>\n");
            result.append("        </item>\n");

            int placeToInsertFirstTemplate = data.indexOf("        <item id=\"1.1\">", placeToInsertExtractType);
            result.append(data.substring(placeToInsertExtractType, placeToInsertFirstTemplate));
            result.append("        <item id=\"" + metaDescription.getTemplateCode() + "\">\n");
            result.append("            <property name=\"title\" value=\"Выписка из сборного приказа. " + metaDescription.getTypeTitle() + "\" synchronize=\"user\"/>\n");
            result.append("            <itemref name=\"type\" id=\"" + metaDescription.getTypeCode() + "\" entity=\"studentExtractType\"/>\n");
            result.append("            <property name=\"index\" value=\"1\"/>\n");
            result.append("            <property name=\"path\" value=\"movestudent/templates/modularorder/" + metaDescription.getFileName() + ".rtf\"/>\n");
            result.append("        </item>\n");

            int placeToInsertSecondTemplate = data.indexOf("        <item id=\"0.0\">", placeToInsertFirstTemplate);
            result.append(data.substring(placeToInsertFirstTemplate, placeToInsertSecondTemplate));
            result.append("        <item id=\"" + metaDescription.getTemplateCode() + ".1\">\n");
            result.append("            <property name=\"title\" value=\"Шаблон для сборного приказа. " + metaDescription.getTypeTitle() + "\" synchronize=\"user\"/>\n");
            result.append("            <itemref name=\"type\" id=\"" + metaDescription.getTypeCode() + "\" entity=\"studentExtractType\"/>\n");
            result.append("            <property name=\"index\" value=\"2\"/>\n");
            result.append("            <property name=\"path\" value=\"movestudent/templates/modularorder/modular_" + metaDescription.getFileName() + ".rtf\"/>\n");
            result.append("        </item>\n");

            result.append(data.substring(placeToInsertSecondTemplate));

            OutputStream outputStream = new FileOutputStream(file);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, Charset.defaultCharset()));
            writer.write(result.toString());
            writer.close();

        } catch (final Exception e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private static void registerNewExtractEntity(ModularExtractMetaDescription metaDesecription)
    {
        EntityMetaDescriptor entityDescriptor = metaDesecription.getMetaDescriptor();
        try
        {
            File file = new File(_resourcesPath + "/entity/" + "modularextract.entity.xml");
            String data = IOUtils.toString(new FileInputStream(file));

            if (data.indexOf(metaDesecription.getMetaDescriptor().getEntityName()) > 0)
            {
                System.out.println("It seems you've already added that extract! Skipping entity meta data adding.");
                return;
            }

            int placeToInsertExtractType = data.indexOf("</entity-config>");

            StringBuilder result = new StringBuilder(data.substring(0, placeToInsertExtractType));
            result.append("    <!-- " + metaDesecription.getTypeIndex() + ". " + metaDesecription.getTypeTitle() + " -->\n");
            result.append("    <child-entity name=\"" + entityDescriptor.getEntityName() + "\" parent-ref=\"" + entityDescriptor.getParentRef() + "\" title=\"" + entityDescriptor.getEntityTitle() + "\"");
            if (null != entityDescriptor.getTableName())
                result.append(" table-name=\"" + entityDescriptor.getTableName() + "\"");
            if (null == entityDescriptor.getFieldDescriptorsList() || entityDescriptor.getFieldDescriptorsList().isEmpty())
                result.append("/>\n");
            else
            {
                result.append(">\n");

                for (EntityFieldBaseMetaDescriptor fieldDecriptor : entityDescriptor.getFieldDescriptorsList())
                {
                    if (fieldDecriptor instanceof EntityFieldPropertyMetaDescriptor)
                    {
                        result.append("        <property name=\"" + fieldDecriptor.getFieldName() + "\" type=\"" + ((EntityFieldPropertyMetaDescriptor) fieldDecriptor).getFieldType() + "\"");
                        if (fieldDecriptor.isRequired()) result.append(" required=\"true\"");
                        if (null != fieldDecriptor.getUnique())
                            result.append(" unique=\"" + fieldDecriptor.getUnique() + "\"");
                        result.append(" title=\"" + fieldDecriptor.getFieldTitle() + "\"/>\n");
                    } else if (fieldDecriptor instanceof EntityFieldManyToOneMetaDescriptor)
                    {
                        result.append("        <many-to-one name=\"" + fieldDecriptor.getFieldName() + "\" entity-ref=\"" + ((EntityFieldManyToOneMetaDescriptor) fieldDecriptor).getEntityRef() + "\"");
                        if (fieldDecriptor.isRequired()) result.append(" required=\"true\"");
                        if (null != fieldDecriptor.getUnique())
                            result.append(" unique=\"" + fieldDecriptor.getUnique() + "\"");
                        result.append(" title=\"" + fieldDecriptor.getFieldTitle() + "\"/>\n");
                    }
                }

                result.append("    </child-entity>\n\n");
            }

            result.append(data.substring(placeToInsertExtractType));

            OutputStream outputStream = new FileOutputStream(file);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, Charset.defaultCharset()));
            writer.write(result.toString());
            writer.close();

        } catch (final Exception e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private static void createExtractComponent(ModularExtractMetaDescription metaDesecription) throws Exception
    {
        EntityMetaDescriptor entityDescriptor = metaDesecription.getMetaDescriptor();
        String extractSimpleClass = StringUtils.capitalize(entityDescriptor.getEntityName());

        String componentPath = _tandemUniPath + "/modules/movestudent/src/main/java/ru/tandemservice/movestudent/component/modularextract/e" + metaDesecription.getTypeIndex();

        UniSystemUtils.initVelocity();

        VelocityContext context = new VelocityContext();
        context.put("entityName", entityDescriptor.getEntityName());
        context.put("creationDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        context.put("entityClassName", extractSimpleClass);
        context.put("extractIndex", metaDesecription.getTypeIndex());

        UniSystemUtils.createComponentByTemplate(componentPath + "/AddEdit", _templatePath + "/AddEdit", context, false);
        UniSystemUtils.createComponentByTemplate(componentPath + "/Pub", _templatePath + "/Pub", context, false);
        UniSystemUtils.createFileByTemplate(componentPath + "/" + extractSimpleClass + "Dao.java", _templatePath + "/extractDao.vm", context, false);
        UniSystemUtils.createFileByTemplate(componentPath + "/" + extractSimpleClass + "Print.java", _templatePath + "/extractPrint.vm", context, false);
    }

    private static void generateSpringDescription()
    {
        String fileName = "movestudent.modularextract.spring.xml";
        Document document = createDaoDocument("ru.tandemservice.movestudent.component.modularextract.e");
        UniSystemUtils.writeDocument(document, _resourcesPath, fileName);
    }

    private static void generateComponentDescription()
    {
        Document document = createComponentDocument();
        String fileName = "movestudent.modularextract.components.xml";
        UniSystemUtils.writeDocument(document, _resourcesPath, fileName);
    }

    private static Document createDaoDocument(String extractPackage)
    {
        Document document = new DOMDocument();
        document.setXMLEncoding("UTF-8");
        document.addComment("DO NOT EDIT");
        document.addComment("Autogenerated by " + ModularExtractAdvancedGenerator.class.getName());
        Namespace namespace = Namespace.get("http://www.springframework.org/schema/beans");
        Namespace tandemUtil = Namespace.get("tandem-util", "http://www.tandemframework.org/schema/tandem-util");

        Element rootElement = new DOMElement("beans");
        rootElement.add(namespace);
        rootElement.add(Namespace.get("xsi", "http://www.w3.org/2001/XMLSchema-instance"));
        rootElement.add(tandemUtil);
        rootElement.add(Namespace.get("util", "http://www.springframework.org/schema/util"));
        rootElement.addAttribute("xsi:schemaLocation", "http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.5.xsd http://www.tandemframework.org/schema/tandem-util http://www.tandemframework.org/schema/tandem-util/spring-tandem-util.xsd http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-2.5.xsd");
        rootElement.addAttribute("default-init-method", "init");
        document.add(rootElement);

        // create dao descriptions

        for (ModularExtractMetaDescription description : ModularExtractAdvancedData.EXTRACT_DESCRIPTIONS_MAP.values())
        {
            EntityMetaDescriptor metaDescriptor = description.getMetaDescriptor();
            DOMComment comment = new DOMComment(description.getTypeIndex() + ". " + description.getTypeTitle());
            rootElement.add(comment);

            // add print

            Element bean = new DOMElement("bean", namespace);
            bean.addAttribute("id", metaDescriptor.getEntityName() + "_extractPrint");
            bean.addAttribute("class", "ru.tandemservice.movestudent.component.modularextract.e" + description.getTypeIndex() + "." + StringUtils.capitalize(metaDescriptor.getEntityName()) + "Print");
            rootElement.add(bean);

            // add dao

            bean = new DOMElement("bean", namespace);
            bean.addAttribute("id", metaDescriptor.getEntityName() + "_extractDao");
            bean.addAttribute("parent", "baseTransactionProxy");

            //add proxyInterfaces
            {
                Element property = new DOMElement("property", namespace);
                property.addAttribute("name", "proxyInterfaces");

                Element list = new DOMElement("list", namespace);

                Element value = new DOMElement("value", namespace);
                value.add(new DOMText("ru.tandemservice.unimove.dao.IExtractComponentDao"));

                list.add(value);
                property.add(list);
                bean.add(property);
            }

            //add target
            {
                Element property = new DOMElement("property", namespace);
                property.addAttribute("name", "target");

                Element beanTarget = new DOMElement("bean", namespace);
                beanTarget.addAttribute("class", extractPackage + (description.getTypeIndex()) + "." + StringUtils.capitalize(metaDescriptor.getEntityName()) + "Dao");

                Element beanProperty = new DOMElement("property", namespace);
                beanProperty.addAttribute("name", "sessionFactory");
                beanProperty.addAttribute("ref", "sessionFactory");

                beanTarget.add(beanProperty);
                property.add(beanTarget);
                bean.add(property);
            }
            rootElement.add(bean);
        }

        // components

        rootElement.add(new DOMText("\n"));
        rootElement.add(new DOMComment("Регистрация компонентов для последующей подмены"));
        rootElement.add(new DOMText("\n"));

        Element componentMap = new DOMElement("tandem-util:map", namespace);
        componentMap.addAttribute("name", "movestudentComponentsMap");

        for (ModularExtractMetaDescription description : ModularExtractAdvancedData.EXTRACT_DESCRIPTIONS_MAP.values())
        {
            EntityMetaDescriptor metaDescriptor = description.getMetaDescriptor();

            Element PubComponentEntry = new DOMElement("entry", namespace);
            PubComponentEntry.addAttribute("key", metaDescriptor.getEntityName() + "Pub");
            PubComponentEntry.addAttribute("value", "ru.tandemservice.movestudent.component.modularextract.e" + description.getTypeIndex() + ".Pub");
            componentMap.add(PubComponentEntry);

            Element addEditComponentEntry = new DOMElement("entry", namespace);
            addEditComponentEntry.addAttribute("key", metaDescriptor.getEntityName() + "AddEdit");
            addEditComponentEntry.addAttribute("value", "ru.tandemservice.movestudent.component.modularextract.e" + description.getTypeIndex() + ".AddEdit");
            componentMap.add(addEditComponentEntry);
        }

        rootElement.add(componentMap);

        // visa service description
        DOMComment servRegComment = new DOMComment("Регистрация сервисов");
        //DOMText servRegSpace = new DOMText("");
        rootElement.add(servRegComment);
        //rootElement.add(servRegSpace);

        rootElement.add(createSetElement(UnimvDefines.VISING_ENTITY_SET, tandemUtil, namespace));
        rootElement.add(createMapElement(UnimvDefines.VISA_INIT_SERVICE_MAP, InitCoordinationVisaExtractService.class.getName(), tandemUtil, namespace));
        rootElement.add(createMapElement(UnimvDefines.VISA_ACCEPT_SERVICE_MAP, AcceptVisaExtractService.class.getName(), tandemUtil, namespace));
        rootElement.add(createMapElement(UnimvDefines.VISA_REJECT_SERVICE_MAP, RejectVisaExtractService.class.getName(), tandemUtil, namespace));

        return document;
    }

    private static Element createSetElement(String setName, Namespace tandemUtil, Namespace namespace)
    {
        Element element = new DOMElement("set", tandemUtil);
        element.addAttribute("name", setName);

        for (ModularExtractMetaDescription description : ModularExtractAdvancedData.EXTRACT_DESCRIPTIONS_MAP.values())
        {
            EntityMetaDescriptor metaDescriptor = description.getMetaDescriptor();
            Element entry = new DOMElement("value", namespace);
            entry.addText(metaDescriptor.getEntityName());
            element.add(entry);
        }

        return element;
    }

    private static Element createMapElement(String mapName, String className, Namespace tandemUtil, Namespace namespace)
    {
        Element element = new DOMElement("map", tandemUtil);
        element.addAttribute("name", mapName);

        for (ModularExtractMetaDescription description : ModularExtractAdvancedData.EXTRACT_DESCRIPTIONS_MAP.values())
        {
            EntityMetaDescriptor metaDescriptor = description.getMetaDescriptor();

            Element entry = new DOMElement("entry", namespace);
            entry.addAttribute("key", metaDescriptor.getEntityName());

            Element bean = new DOMElement("bean", namespace);
            bean.addAttribute("class", className);

            entry.add(bean);
            element.add(entry);
        }

        return element;
    }

    private static Document createComponentDocument()
    {
        Document document = new DOMDocument();
        document.setXMLEncoding("UTF-8");
        document.addComment("DO NOT EDIT");
        document.addComment("Autogenerated by " + ModularExtractAdvancedGenerator.class.getName());
        Namespace namespace = Namespace.get("http://www.tandemframework.org/meta/components");

        Element rootElement = new DOMElement("components-config");
        rootElement.add(namespace);
        rootElement.add(Namespace.get("xsi", "http://www.w3.org/2001/XMLSchema-instance"));
        rootElement.add(Namespace.get("components", "http://www.tandemframework.org/meta/components"));
        rootElement.addAttribute("xsi:schemaLocation", "http://www.tandemframework.org/meta/components http://www.tandemframework.org/schema/meta/meta-components.xsd");
        rootElement.addAttribute("name", "movestudent-modularextract-components");
        rootElement.addAttribute("label", "Компоненты выписок из сборного приказа");
        document.add(rootElement);

        for (ModularExtractMetaDescription description : ModularExtractAdvancedData.EXTRACT_DESCRIPTIONS_MAP.values())
        {
            EntityMetaDescriptor metaDescriptor = description.getMetaDescriptor();
            DOMComment comment = new DOMComment(description.getTypeIndex() + ". " + description.getTypeTitle());
            rootElement.add(comment);

            Element entityView = new DOMElement("publisher", namespace);
            entityView.addAttribute("name", metaDescriptor.getEntityName());
            entityView.addAttribute("component", "ru.tandemservice.movestudent.component.modularextract.e" + description.getTypeIndex() + ".Pub");
            rootElement.add(entityView);
        }

        for (ModularExtractMetaDescription description : ModularExtractAdvancedData.EXTRACT_DESCRIPTIONS_MAP.values())
        {
            EntityMetaDescriptor metaDescriptor = description.getMetaDescriptor();

            // AddEdit
            {
                Element component = new DOMElement("component", namespace);
                component.addAttribute("package", "ru.tandemservice.movestudent.component.modularextract.e" + description.getTypeIndex() + ".AddEdit");
                component.addAttribute("title", "Форма добавления и редактирования выписки «" + description.getTypeTitle() + "»");

                Element elemDescription = new DOMElement("description", namespace);
                elemDescription.addText(description.getTypeIndex() + ". " + description.getTypeTitle() + " " + metaDescriptor.getEntityName());
                component.add(elemDescription);

                rootElement.add(component);
            }
            // Pub
            {
                Element component = new DOMElement("component", namespace);
                component.addAttribute("package", "ru.tandemservice.movestudent.component.modularextract.e" + description.getTypeIndex() + ".Pub");
                component.addAttribute("title", "Публикатор выписки «" + description.getTypeTitle() + "»");

                Element elemDescription = new DOMElement("description", namespace);
                elemDescription.addText(description.getTypeIndex() + ". " + description.getTypeTitle() + " " + metaDescriptor.getEntityName());
                component.add(elemDescription);

                Element extpoint = new DOMElement("ext-point-def", namespace);
                extpoint.addAttribute("name", "extractTabList");
                extpoint.addAttribute("type", "tabList");

                component.add(extpoint);
                rootElement.add(component);
            }
        }

        return document;
    }
}