/* $Id$ */
package ru.tandemservice.movestudent.component.commons;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.OtherStudentExtract;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;

import java.util.*;

/**
 * @author Nikolay Fedorovskih
 * @since 23.12.2013
 */
public enum OrderCategory
{
    LIST(StudentExtractTypeCodes.LIST_ORDER, ListStudentExtract.class),
    INDIVIDUAL(StudentExtractTypeCodes.MODULAR_ORDER, ModularStudentExtract.class),
    MODULAR(StudentExtractTypeCodes.MODULAR_ORDER, ModularStudentExtract.class),
    OTHER(StudentExtractTypeCodes.OTHER_ORDER, OtherStudentExtract.class);

    private String _baseExtractTypeCode;
    private Class<? extends AbstractStudentExtract> _baseExtractClass;

    OrderCategory(String baseExtractTypeCode, Class<? extends AbstractStudentExtract> baseExtractClass)
    {
        _baseExtractTypeCode = baseExtractTypeCode;
        _baseExtractClass = baseExtractClass;
    }

    public static Collection<OrderCategory> getCategoriesFromFilter(Collection<IEntity> selectedItems, boolean isRequired)
    {
        if (selectedItems == null || selectedItems.isEmpty())
        {
            if (isRequired)
                return Collections.emptyList();
            return Arrays.asList(OrderCategory.values());
        }

        List<OrderCategory> result = new ArrayList<>(selectedItems.size());
        for (IEntity entity : selectedItems)
        {
            switch (entity.getId().intValue())
            {
                case (int) LIST_ORDER_CATEGORY_ID:
                    result.add(LIST);
                    break;
                case (int) MODULAR_ORDER_CATEGORY_ID:
                    result.add(MODULAR);
                    break;
                case (int) INDIVIDUAL_ORDER_CATEGORY_ID:
                    result.add(INDIVIDUAL);
                    break;
                case (int) OTHER_ORDER_CATEGORY_ID:
                    result.add(OTHER);
                    break;
            }
        }
        return result;
    }

    public static final long LIST_ORDER_CATEGORY_ID = 0L;
    public static final long MODULAR_ORDER_CATEGORY_ID = 1L;
    public static final long INDIVIDUAL_ORDER_CATEGORY_ID = 2L;
    public static final long OTHER_ORDER_CATEGORY_ID = 3L;

    public static final IEntity LIST_ORDER_CATEGORY = new DataWrapper(LIST_ORDER_CATEGORY_ID, "Списочные");
    public static final IEntity MODULAR_ORDER_CATEGORY = new DataWrapper(MODULAR_ORDER_CATEGORY_ID, "Сборные");
    public static final IEntity INDIVIDUAL_ORDER_CATEGORY = new DataWrapper(INDIVIDUAL_ORDER_CATEGORY_ID, "Индивидуальные");
    public static final IEntity OTHERS_ORDER_CATEGORY = new DataWrapper(OTHER_ORDER_CATEGORY_ID, "Прочие");

    public String getBaseExtractTypeCode()
    {
        return _baseExtractTypeCode;
    }

    public Class<? extends AbstractStudentExtract> getBaseExtractClass()
    {
        return _baseExtractClass;
    }
}