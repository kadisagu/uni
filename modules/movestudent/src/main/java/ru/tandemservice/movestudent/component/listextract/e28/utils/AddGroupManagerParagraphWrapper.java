/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e28.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 14.09.2012
 */
public class AddGroupManagerParagraphWrapper implements Comparable<AddGroupManagerParagraphWrapper>
{
    private CompensationType _compensationType;
    private EducationOrgUnit _educationOrgUnit;
    private EducationYear _educationYear;
    private String _eduBaseText;
    private final ListStudentExtract _firstExtract;

    public AddGroupManagerParagraphWrapper(CompensationType compensationType, EducationOrgUnit educationOrgUnit, EducationYear educationYear, String eduBaseText, ListStudentExtract firstExtract)
    {
        _compensationType = compensationType;
        _educationOrgUnit = educationOrgUnit;
        _educationYear = educationYear;
        _eduBaseText = eduBaseText;
        _firstExtract = firstExtract;
    }

    private List<AddGroupManagerParagraphPartWrapper> _paragraphPartWrapperList = new ArrayList<AddGroupManagerParagraphPartWrapper>();

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        _educationOrgUnit = educationOrgUnit;
    }

    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    public void setEducationYear(EducationYear educationYear)
    {
        _educationYear = educationYear;
    }

    public List<AddGroupManagerParagraphPartWrapper> getParagraphPartWrapperList()
    {
        return _paragraphPartWrapperList;
    }

    public void setParagraphPartWrapperList(List<AddGroupManagerParagraphPartWrapper> paragraphPartWrapperList)
    {
        _paragraphPartWrapperList = paragraphPartWrapperList;
    }

    public String getEduBaseText()
    {
        return _eduBaseText;
    }

    public void setEduBaseText(String eduBaseText)
    {
        _eduBaseText = eduBaseText;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof AddGroupManagerParagraphWrapper))
            return false;

        AddGroupManagerParagraphWrapper that = (AddGroupManagerParagraphWrapper) o;

        return (_compensationType.equals(that.getCompensationType()) &&
        _educationOrgUnit.equals(that.getEducationOrgUnit()) &&
        (_educationYear == null ? (that.getEducationYear() == null) : _educationYear.equals(that.getEducationYear())) &&
        _eduBaseText.equals(that.getEduBaseText()));
    }

    @Override
    public int hashCode()
    {
        return _compensationType.hashCode() & _educationOrgUnit.hashCode() & _educationYear.hashCode() & _eduBaseText.hashCode();
    }

    @Override
    public int compareTo(AddGroupManagerParagraphWrapper o)
    {
        int result = _compensationType.getCode().compareTo(o.getCompensationType().getCode());

        String thisCode = _educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix();
        String thatCode = o.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix();

        if (result == 0)
            result = thisCode == null ? 1 : thisCode.compareTo(thatCode);

        if (result == 0)
            result = _educationOrgUnit.getTitle().compareTo(o.getEducationOrgUnit().getTitle());

        if (result == 0)
            result = _eduBaseText.compareTo(o.getEduBaseText());

        return result;
    }
}