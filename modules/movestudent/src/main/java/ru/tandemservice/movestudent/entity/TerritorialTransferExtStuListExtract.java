package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.TerritorialTransferExtStuListExtractGen;

import java.util.Date;

/**
 * Проект приказа «О переводе студентов территориального подразделения (расширенный)»
 */
public class TerritorialTransferExtStuListExtract extends TerritorialTransferExtStuListExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getBeginDate();
    }
}