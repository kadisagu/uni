/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e35;

import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.e35.utils.SplitFirstCourseStudentsGroupParagraphPartWrapper;
import ru.tandemservice.movestudent.component.listextract.e35.utils.SplitFirstCourseStudentsGroupParagraphWrapper;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 14.10.2013
 */
public class SplitFirstCourseStudentsGroupStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        final RtfDocument document = new RtfReader().read(template);

        List<SplitFirstCourseStudentsGroupStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        List<SplitFirstCourseStudentsGroupParagraphWrapper> preparedParagraphsStructure = prepareParagraphsStructure(extracts);

        injectModifier.put("orderType", order.getType().getTitle());
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, order, preparedParagraphsStructure);
        RtfUtil.optimizeFontAndColorTable(document);
        return document;
    }

    protected List<SplitFirstCourseStudentsGroupParagraphWrapper> prepareParagraphsStructure(List<SplitFirstCourseStudentsGroupStuListExtract> extracts)
    {
        int ind;
        List<SplitFirstCourseStudentsGroupParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        for (SplitFirstCourseStudentsGroupStuListExtract extract : extracts)
        {
            Student student = extract.getEntity();
            Person person = extract.getEntity().getPerson();

            SplitFirstCourseStudentsGroupParagraphWrapper paragraphWrapper = new SplitFirstCourseStudentsGroupParagraphWrapper(
                    student.getStudentCategory(),
                    student.getCompensationType(),
                    extract.getDevelopForm(),
                    extract.getDevelopCondition(),
                    extract.getDevelopTech(),
                    extract.getDevelopPeriod(),
                    extract);

            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else paragraphWrapper = paragraphWrapperList.get(ind);

            SplitFirstCourseStudentsGroupParagraphPartWrapper paragraphPartWrapper = new SplitFirstCourseStudentsGroupParagraphPartWrapper(
                    extract.getEducationLevelsHighSchool(),
                    extract.getNewGroup(),
                    extract);

            ind = paragraphWrapper.getParagraphPartWrapperList().indexOf(paragraphPartWrapper);
            if (ind == -1)
                paragraphWrapper.getParagraphPartWrapperList().add(paragraphPartWrapper);
            else paragraphPartWrapper = paragraphWrapper.getParagraphPartWrapperList().get(ind);
            paragraphPartWrapper.getPersonList().add(person);
        }
        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, StudentListOrder order, List<SplitFirstCourseStudentsGroupParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            int parNumber = 0;
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (SplitFirstCourseStudentsGroupParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.ASSIGN_TO_GROUPS_FIRST_COURSE_STUDENTS_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphWrapper.getFirstExtract();
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(firstExtract.getParagraph(), firstExtract);

                SplitFirstCourseStudentsGroupStuListExtract extract = (SplitFirstCourseStudentsGroupStuListExtract) paragraphWrapper.getFirstExtract();

                CommonListExtractPrint.injectCommonListExtractData(paragraphInjectModifier, extract);
                CommonListExtractPrint.injectStudentsCategory(paragraphInjectModifier, paragraphWrapper.getStudentCategory());
                CommonExtractPrint.initEducationType(paragraphInjectModifier, extract.getEntity().getEducationOrgUnit(), "", true, false);
                CommonExtractPrint.initDevelopForm(paragraphInjectModifier, extract.getDevelopForm(), "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, extract.getEntity().getEducationOrgUnit(), "formativeOrgUnitStr", "");
                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, extract.getDevelopCondition(), extract.getDevelopTech(), extract.getDevelopPeriod(), CommonListOrderPrint.getEducationBaseText(extract.getNewGroup()), "fefuShortFastExtendedOptionalText");

                paragraphInjectModifier.put("parNumber", paragraphWrappers.size() == 1 ? "" : String.valueOf(++parNumber) + ". ");
                paragraphInjectModifier.put("paragraphNumber", String.valueOf(parNumber));
                paragraphInjectModifier.put("course", "1");

                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, EducationOrgUnitUtil.getParentLevel(extract.getEducationLevelsHighSchool()));
                customModifier.listParagraphModifier(paragraphInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                paragraphInjectModifier.modify(paragraph);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getParagraphPartWrapperList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }
            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraph, List<SplitFirstCourseStudentsGroupParagraphPartWrapper> paragraphPartWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartWrappers);
            List<IRtfElement> parList = new ArrayList<>();

            for (SplitFirstCourseStudentsGroupParagraphPartWrapper paragraphPartWrapper : paragraphPartWrappers)
            {
                // Получаем шаблон подпараграфа
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.ASSIGN_TO_GROUPS_FIRST_COURSE_STUDENTS_LIST_EXTRACT), 3);
                RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphPartWrapper.getFirstExtract();
                RtfInjectModifier paragraphPartInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, firstExtract);

                paragraphPartInjectModifier.put("newGroup", "Группа " + paragraphPartWrapper.getNewGroup().getTitle());

                List<Person> personList = paragraphPartWrapper.getPersonList();
                personList.sort(Person.FULL_FIO_AND_ID_COMPARATOR);
                RtfString rtfString = new RtfString();

                int j = 0;
                for (; j < personList.size() - 1; j++)
                {
                    rtfString.append(IRtfData.TAB).append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio()).par();
                }

                rtfString.append(IRtfData.TAB).append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio());
                paragraphPartInjectModifier.put("STUDENT_LIST", rtfString);
                additionalParagraphPartInjectModifier(paragraphPartInjectModifier, paragraphPartWrapper);
                customModifier.listParagraphModifier(paragraphPartInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphPartInjectModifier.modify(paragraphPart);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraphPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    public void additionalParagraphPartInjectModifier(RtfInjectModifier paragraphPartInjectModifier, SplitFirstCourseStudentsGroupParagraphPartWrapper paragraphPartWrapper)
    {
    }
}
