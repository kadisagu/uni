package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IAssignPaymentExtract;
import ru.tandemservice.movestudent.entity.gen.MatAidAssignStuExtractGen;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. Об оказании материальной помощи
 */
public class MatAidAssignStuExtract extends MatAidAssignStuExtractGen implements IAssignPaymentExtract
{
    @Override
    public BigDecimal getPaymentAmount()
    {
        return new BigDecimal(getGrantSize());
    }

    @Override
    public Date getPaymentBeginDate()
    {
        // Первый день указанного месяца, указанного года
        Calendar calendar = Calendar.getInstance();
        //noinspection MagicConstant
        calendar.set(getYear(), getMonth() - 1, 1, 0, 0, 0);
        return calendar.getTime();
    }

    @Override
    public Date getPaymentEndDate()
    {
        // Последний день указанного месяца, указанного года
        Calendar calendar = Calendar.getInstance();
        //noinspection MagicConstant
        calendar.set(getYear(), getMonth() - 1, 1, 0, 0, 0);
        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        return calendar.getTime();
    }
}