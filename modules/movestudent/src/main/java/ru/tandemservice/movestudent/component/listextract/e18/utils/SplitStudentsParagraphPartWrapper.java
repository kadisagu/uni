/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e18.utils;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: nikolay
 * Date: 3/12/13
 * Time: 4:49 PM
 */
public class SplitStudentsParagraphPartWrapper implements Comparable<SplitStudentsParagraphPartWrapper>
{
    private final EducationLevelsHighSchool _educationLevelsHighSchoolNew;
    private final ListStudentExtract _firstExtract;
    private final List<Person> _personList = new ArrayList<>();

    public SplitStudentsParagraphPartWrapper(EducationLevelsHighSchool educationLevelsHighSchoolNew, ListStudentExtract firstExtract)
    {
        _educationLevelsHighSchoolNew = educationLevelsHighSchoolNew;
        _firstExtract = firstExtract;
    }

    public EducationLevelsHighSchool getEducationLevelsHighSchoolNew()
    {
        return _educationLevelsHighSchoolNew;
    }

    public List<Person> getPersonList()
    {
        return _personList;
    }

    public ListStudentExtract getFirstExtract() {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof SplitStudentsParagraphPartWrapper))
            return false;

        SplitStudentsParagraphPartWrapper that = (SplitStudentsParagraphPartWrapper) o;

        return (_educationLevelsHighSchoolNew.equals(that.getEducationLevelsHighSchoolNew()));
    }

    @Override
    public int hashCode()
    {
        return _educationLevelsHighSchoolNew.hashCode();
    }

    @Override
    public int compareTo(SplitStudentsParagraphPartWrapper o)
    {
        String thisCode = _educationLevelsHighSchoolNew.getEducationLevel().getTitleCodePrefix();
        String thatCode = o.getEducationLevelsHighSchoolNew().getEducationLevel().getTitleCodePrefix();
        int result = thisCode.compareTo(thatCode);
        return result;
    }
}
