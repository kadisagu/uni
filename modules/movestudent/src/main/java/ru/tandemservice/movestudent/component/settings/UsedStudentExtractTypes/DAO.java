/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.settings.UsedStudentExtractTypes;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;

import java.util.*;

/**
 * @author dseleznev
 *         Created on: 12.12.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        List<StudentExtractType> result = getList(StudentExtractType.class);
        //Collections.sort(result, UniBaseUtils.CATALOG_CODE_COMPARATOR);

        Collections.sort(result, new StudentExtractType.ItemsComparator());

        model.getDataSource().setCountRow(result.size());
        UniBaseUtils.createPage(model.getDataSource(), result);

        Map<StudentExtractType, String> groupMap = new HashMap<>();
        for (StudentExtractTypeToGroup rel : getList(StudentExtractTypeToGroup.class))
        {
            String group = groupMap.get(rel.getType());
            if (null == group)
                group = rel.getGroup().getTitle();
            else
                group = group + ", " + rel.getGroup().getTitle();
            groupMap.put(rel.getType(), group);
        }
        for (ViewWrapper<StudentExtractType> wrapper : ViewWrapper.<StudentExtractType>getPatchedList(model.getDataSource()))
        {
            final String groupTitle = groupMap.get(wrapper.getEntity());
            wrapper.setViewProperty("group", groupTitle == null ? "" : groupTitle);
            wrapper.setViewProperty(Model.ACTIVATION_DISABLED, isActiveParent(wrapper.getEntity()));
        }
    }

    @Override
    public void updateActive(long extractTypeId)
    {
        StudentExtractType parentExtractType = get(StudentExtractType.class, extractTypeId);
        StudentExtractType elderParentExtractType = null;
        if (parentExtractType != null)
        {
            elderParentExtractType = parentExtractType.getParent();
            parentExtractType.setActive(!parentExtractType.isActive());
            update(parentExtractType);
            for (StudentExtractType child : getChildren(parentExtractType, new ArrayList<StudentExtractType>()))
            {
                if (child != null && !parentExtractType.isActive() && child.isActive())
                {
                    child.setActive(false);
                    update(child);
                }
            }
            if (getChildren(parentExtractType, new ArrayList<StudentExtractType>()).size() == 1 && !getChildren(parentExtractType, new ArrayList<StudentExtractType>()).get(0).isActive() && parentExtractType.isActive())
            {
                getChildren(parentExtractType, new ArrayList<StudentExtractType>()).get(0).setActive(true);
                update(getChildren(parentExtractType, new ArrayList<StudentExtractType>()).get(0));
            }
        }
        int activeAmount = 0;
        if (elderParentExtractType != null)
        {
            if (!elderParentExtractType.isActive() && parentExtractType.isActive())
                elderParentExtractType.setActive(true);
            for (StudentExtractType child : getChildren(elderParentExtractType, new ArrayList<StudentExtractType>()))
            {
                if (child.isActive()) activeAmount++;
            }
            if (activeAmount == 0) elderParentExtractType.setActive(false);
            update(elderParentExtractType);
        }
    }

    private List<StudentExtractType> getChildren(StudentExtractType extractType, List<StudentExtractType> children)
    {
        DQLSelectBuilder childBuild = new DQLSelectBuilder().fromEntity(StudentExtractType.ENTITY_CLASS, "stext")
                .column(DQLExpressions.property("stext"))
                .where(DQLExpressions.eq(DQLExpressions.property(StudentExtractType.parent().id().fromAlias("stext")), DQLExpressions.value(extractType.getId())));
        List<StudentExtractType> result = childBuild.createStatement(getSession()).list();
        children.addAll(result);
        for (StudentExtractType child : result)
            getChildren(child, children);
        return children;
    }

    private boolean isActiveParent(StudentExtractType extractType)
    {
        boolean flag = false;
        if (null == extractType.getParent())
            flag = true;
        return flag;
    }

}