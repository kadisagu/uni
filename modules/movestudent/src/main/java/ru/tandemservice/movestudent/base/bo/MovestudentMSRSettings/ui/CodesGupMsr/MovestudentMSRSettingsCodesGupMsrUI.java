/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.ui.CodesGupMsr;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.shared.commonbase.base.bo.Common.ui.ScriptEdit.CommonScriptEdit;
import org.tandemframework.shared.commonbase.base.bo.Common.ui.ScriptEdit.CommonScriptEditUI;
import org.tandemframework.shared.commonbase.base.bo.Common.ui.ScriptEdit.ScriptHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.MovestudentMSRSettingsManager;
import ru.tandemservice.movestudent.entity.ExportMSRSettings;
import ru.tandemservice.uni.dao.IUniScriptDao;
import ru.tandemservice.uni.dao.UniScriptDao;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 18.12.2014
 */
public class MovestudentMSRSettingsCodesGupMsrUI extends UIPresenter
{
    private ExportMSRSettings _settingsMSR;
    private Map<OrgUnit, String> _codesMap;

    public ExportMSRSettings getSettingsMSR()
    {
        return _settingsMSR;
    }

    public void setSettingsMSR(ExportMSRSettings settings)
    {
        _settingsMSR = settings;
    }

    public Map<OrgUnit, String> getCodesMap()
    {
        return _codesMap;
    }

    public void setCodesMap(Map<OrgUnit, String> codesMap)
    {
        _codesMap = codesMap;
    }

    public String getCodesFacultyId()
    {
        return "codesFacultyId_" + getConfig().getDataSource(MovestudentMSRSettingsCodesGupMsr.FORMATIVE_ORG_UNIT_DS).getCurrentId();
    }

    @Override
    public void onComponentRefresh()
    {
        _codesMap = MovestudentMSRSettingsManager.instance().dao().getMapCodesForFormativeOrgUnit();
        _settingsMSR = MovestudentMSRSettingsManager.instance().dao().getSettings();
        if (_settingsMSR == null)
            _settingsMSR = new ExportMSRSettings();
    }

    public void onClickApply()
    {
        MovestudentMSRSettingsManager.instance().dao().saveOrUpdateCodesForFormativeOrgUnit(_codesMap);
        MovestudentMSRSettingsManager.instance().dao().saveSettings(_settingsMSR);
        deactivate();

    }

    public String getCurrentCodeFaculty()
    {
        OrgUnit current = getConfig().getDataSource(MovestudentMSRSettingsCodesGupMsr.FORMATIVE_ORG_UNIT_DS).getCurrent();
        return _codesMap.get(current);
    }

    public void setCurrentCodeFaculty(String code)
    {
        OrgUnit current = getConfig().getDataSource(MovestudentMSRSettingsCodesGupMsr.FORMATIVE_ORG_UNIT_DS).getCurrent();
        _codesMap.put(current, code);
    }


    public void onEditScript()
    {
        _uiActivation.asDesktopRoot(CommonScriptEdit.class)
                .parameter(CommonScriptEditUI.PARAM_HOLDER, new ScriptHolder<ExportMSRSettings>(_settingsMSR) {

                    @Override public String getScriptText() {
                        final String script = StringUtils.trimToNull(getValue().getUserScriptExportStudent());
                        if (null != script) { return script; }
                        return getDefaultScriptText();
                    }

                    @Override public void setScriptText(String userScript) {
                        userScript = StringUtils.trimToNull(userScript);
                        if (null != userScript && scriptEquals(userScript, getDefaultScriptText()))
                            userScript = null;
                        getValue().setUserScriptExportStudent(userScript);
                    }

                    @Override public String getDefaultScriptText() {
                        return StringUtils.trimToEmpty(UniScriptDao.getScriptText(ExportMSRSettings.getDefScriptPath()));
                    }

                    @Override protected void internalSave() {
                        IUniScriptDao.instance.get().doSaveUserScript(getValue());
                    }

                })
                .activate();
    }

}
