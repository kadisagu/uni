/* $Id$ */
package ru.tandemservice.movestudent.component.order.other.StudentOtherOrderAddEdit;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 11.04.2013
 */
@Input({
        @Bind(key = "studentId", binding = "studentId"),
        @Bind(key = "extractId", binding = "extractId"),
        @Bind(key = "orgUnitId", binding = "orgUnitId")
})
public class Model
{
    private Long _studentId;
    private Long _extractId;
    private Long _orgUnitId;

    private Student _student;
    private OrgUnit _orgUnit;
    private OtherStudentExtract _extract;
    private StudentOtherParagraph _paragraph;
    private StudentOtherOrder _order;

    private ISelectModel _extractParentTypeListModel;
    private ISelectModel _extractTypeListModel;
    private StudentExtractType _parentExtractType;
    private StudentExtractType _extractType;

    private List<OrderStates> _orderStatesList;
    private OrderStates _orderState;

    private IMultiSelectModel _reasonListModel;
    private List<StudentOrderBasics> _selectedBasicList = new ArrayList<>();
    private IMultiSelectModel _basicListModel;
    private StudentOrderBasics _currentBasic;
    private Map<Long, String> _currentBasicMap = new HashMap<>();

    private List<Course> _courseList;
    private Collection<Term> _termList;

    private OtherStudentFieldsSettings _fieldSettings;

    private boolean _otherTypesThreeLevel = false;
    private boolean _addForm;

    private ISelectModel _employeePostModel;
    private EmployeePost _employeePost;
    private EmployeePost _responsibleEmployee;

    private IUploadFile _uploadFile;
    private StudentOtherOrderTextRelation _printForm;

    public boolean isEditForm()
    {
        return !isAddForm();
    }

    public boolean isReasonCommentVisible()
    {
        return null != getExtract().getReason() && getExtract().getReason().isCommentable();
    }

    public boolean isNeedComments()
    {
        for (StudentOrderBasics basic : _selectedBasicList)
            if (basic.isCommentable()) return true;
        return false;
    }

    public String getCurrentBasicTitle()
    {
        return _currentBasicMap.get(_currentBasic.getId());
    }

    public void setCurrentBasicTitle(String title)
    {
        _currentBasicMap.put(_currentBasic.getId(), title);
    }

    public String getCurrentBasicId()
    {
        return "basicId_" + _currentBasic.getId();
    }

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        _studentId = studentId;
    }

    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        _extractId = extractId;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public OtherStudentExtract getExtract()
    {
        return _extract;
    }

    public void setExtract(OtherStudentExtract extract)
    {
        _extract = extract;
    }

    public StudentOtherParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(StudentOtherParagraph paragraph)
    {
        _paragraph = paragraph;
    }

    public StudentOtherOrder getOrder()
    {
        return _order;
    }

    public void setOrder(StudentOtherOrder order)
    {
        _order = order;
    }

    public ISelectModel getExtractParentTypeListModel()
    {
        return _extractParentTypeListModel;
    }

    public void setExtractParentTypeListModel(ISelectModel extractParentTypeListModel)
    {
        _extractParentTypeListModel = extractParentTypeListModel;
    }

    public ISelectModel getExtractTypeListModel()
    {
        return _extractTypeListModel;
    }

    public void setExtractTypeListModel(ISelectModel extractTypeListModel)
    {
        _extractTypeListModel = extractTypeListModel;
    }

    public StudentExtractType getParentExtractType()
    {
        return _parentExtractType;
    }

    public void setParentExtractType(StudentExtractType parentExtractType)
    {
        _parentExtractType = parentExtractType;
    }

    public StudentExtractType getExtractType()
    {
        return _extractType;
    }

    public void setExtractType(StudentExtractType extractType)
    {
        _extractType = extractType;
    }

    public List<OrderStates> getOrderStatesList()
    {
        return _orderStatesList;
    }

    public void setOrderStatesList(List<OrderStates> orderStatesList)
    {
        _orderStatesList = orderStatesList;
    }

    public OrderStates getOrderState()
    {
        return _orderState;
    }

    public void setOrderState(OrderStates orderState)
    {
        _orderState = orderState;
    }

    public IMultiSelectModel getReasonListModel()
    {
        return _reasonListModel;
    }

    public void setReasonListModel(IMultiSelectModel reasonListModel)
    {
        _reasonListModel = reasonListModel;
    }

    public List<StudentOrderBasics> getSelectedBasicList()
    {
        return _selectedBasicList;
    }

    public void setSelectedBasicList(List<StudentOrderBasics> selectedBasicList)
    {
        _selectedBasicList = selectedBasicList;
    }

    public IMultiSelectModel getBasicListModel()
    {
        return _basicListModel;
    }

    public void setBasicListModel(IMultiSelectModel basicListModel)
    {
        _basicListModel = basicListModel;
    }

    public StudentOrderBasics getCurrentBasic()
    {
        return _currentBasic;
    }

    public void setCurrentBasic(StudentOrderBasics currentBasic)
    {
        _currentBasic = currentBasic;
    }

    public Map<Long, String> getCurrentBasicMap()
    {
        return _currentBasicMap;
    }

    public void setCurrentBasicMap(Map<Long, String> currentBasicMap)
    {
        _currentBasicMap = currentBasicMap;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public Collection<Term> getTermList()
    {
        return _termList;
    }

    public void setTermList(Collection<Term> termList)
    {
        _termList = termList;
    }

    public OtherStudentFieldsSettings getFieldSettings()
    {
        return _fieldSettings;
    }

    public void setFieldSettings(OtherStudentFieldsSettings fieldSettings)
    {
        _fieldSettings = fieldSettings;
    }

    public boolean isOtherTypesThreeLevel()
    {
        return _otherTypesThreeLevel;
    }

    public void setOtherTypesThreeLevel(boolean otherTypesThreeLevel)
    {
        _otherTypesThreeLevel = otherTypesThreeLevel;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        _addForm = addForm;
    }

    public ISelectModel getEmployeePostModel()
    {
        return _employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        _employeePostModel = employeePostModel;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        _employeePost = employeePost;
    }

    public EmployeePost getResponsibleEmployee()
    {
        return _responsibleEmployee;
    }

    public void setResponsibleEmployee(EmployeePost responsibleEmployee)
    {
        _responsibleEmployee = responsibleEmployee;
    }

    public IUploadFile getUploadFile()
    {
        return _uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile)
    {
        _uploadFile = uploadFile;
    }

    public StudentOtherOrderTextRelation getPrintForm()
    {
        return _printForm;
    }

    public void setPrintForm(StudentOtherOrderTextRelation printForm)
    {
        _printForm = printForm;
    }
}