/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e40.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.GiveDiplIncompleteStuListExtract;
import ru.tandemservice.uni.entity.employee.OrderData;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 09.02.2011
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<GiveDiplIncompleteStuListExtract, Model>
{
    @Override
    protected GiveDiplIncompleteStuListExtract createNewInstance()
    {
        return new GiveDiplIncompleteStuListExtract();
    }
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void prepare(Model model) {
        super.prepare(model);

        if (model.isEditForm())
        {
            model.setDischargeOrderNumber(model.getExtract().getDischargeOrderNumber());
            model.setDismissDate(model.getExtract().getDismissDate());
            model.setBeginYear(model.getExtract().getBeginYear());
            model.setEndYear(model.getExtract().getEndYear());
        }

        if (model.isAddForm())
        {
            OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, model.getExtract().getEntity());
            if (orderData != null)
            {
                model.setDischargeOrderNumber(orderData.getExcludeOrderNumber());
                model.setDismissDate(orderData.getExcludeOrderDate());
                model.setBeginYear(orderData.getEduEnrollmentOrderDate());
                model.setEndYear(orderData.getExcludeOrderDate());
            }
        }
    }
    @Override
    public void update(Model model)
    {
        model.getExtract().setDischargeOrderNumber(model.getDischargeOrderNumber());
        model.getExtract().setDismissDate(model.getDismissDate());
        model.getExtract().setBeginYear(model.getBeginYear());
        model.getExtract().setEndYear(model.getEndYear());
        super.update(model);
    }
}
