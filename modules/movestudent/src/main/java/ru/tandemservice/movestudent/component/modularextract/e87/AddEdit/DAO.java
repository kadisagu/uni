/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e87.AddEdit;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.GrantRiseStuExtract;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 20.11.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<GrantRiseStuExtract, Model> implements IDAO
{
    @Override
    protected GrantRiseStuExtract createNewInstance()
    {
        return new GrantRiseStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        List<IdentifiableWrapper> months = new ArrayList<IdentifiableWrapper>();
        for (int i = 1; i <= 12; i++)
            months.add(new IdentifiableWrapper((long) i, CommonBaseDateUtil.getMonthNameDeclined(i, GrammaCase.NOMINATIVE)));

        model.setMonthList(months);

        if (model.isEditForm())
        {
            model.setMonth(model.getMonthList().get(model.getExtract().getMonth() - 1));
            model.setYear(model.getExtract().getYear());
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if(model.getExtract().getGrantRiseAmount() <= 0)
            errors.add("Значение поля «Размер повышения стипендии» должно быть больше нуля.", "grantRiseAmount");
        super.validate(model, errors);
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setMonth(model.getMonth().getId().intValue());
        model.getExtract().setYear(model.getYear());
        super.update(model);
    }
}