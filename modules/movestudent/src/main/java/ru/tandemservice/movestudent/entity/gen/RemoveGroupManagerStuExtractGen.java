package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.RemoveGroupManagerStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об освобождении от обязанностей старосты учебной группы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RemoveGroupManagerStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.RemoveGroupManagerStuExtract";
    public static final String ENTITY_NAME = "removeGroupManagerStuExtract";
    public static final int VERSION_HASH = 1668684255;
    private static IEntityMeta ENTITY_META;

    public static final String P_REMOVAL_DATE = "removalDate";

    private Date _removalDate;     // Дата освобождения от обязанностей старосты

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата освобождения от обязанностей старосты. Свойство не может быть null.
     */
    @NotNull
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата освобождения от обязанностей старосты. Свойство не может быть null.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RemoveGroupManagerStuExtractGen)
        {
            setRemovalDate(((RemoveGroupManagerStuExtract)another).getRemovalDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RemoveGroupManagerStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RemoveGroupManagerStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new RemoveGroupManagerStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "removalDate":
                    return obj.getRemovalDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "removalDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "removalDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "removalDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RemoveGroupManagerStuExtract> _dslPath = new Path<RemoveGroupManagerStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RemoveGroupManagerStuExtract");
    }
            

    /**
     * @return Дата освобождения от обязанностей старосты. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RemoveGroupManagerStuExtract#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    public static class Path<E extends RemoveGroupManagerStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _removalDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата освобождения от обязанностей старосты. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RemoveGroupManagerStuExtract#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(RemoveGroupManagerStuExtractGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

        public Class getEntityClass()
        {
            return RemoveGroupManagerStuExtract.class;
        }

        public String getEntityName()
        {
            return "removeGroupManagerStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
