/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e16;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.WeekendStuExtractExt;

import java.util.Collections;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 30.04.2009
 */
public class WeekendStuExtractExtPrint implements IPrintFormCreator<WeekendStuExtractExt>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, WeekendStuExtractExt extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        modifier.put("reasonStr", extract.getReasonStr());

        if (extract.isStopGrantsPaying())
        {
            StringBuilder builder = new StringBuilder("2. ");
            builder.append(modifier.getStringValue("student_D")).append(" ").append(modifier.getStringValue("fio")).append(" прекратить выплату стипендии с ");
            builder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStopPayingFrom())).append(" г. по ");
            builder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStopPayingTo())).append(" г.");
            modifier.put("stopGrantPayingStr", builder.toString());
            String stopGrantsPayingParagraphText = ApplicationRuntime.getProperty("print.stopGrantsPayingParagraph");
            if (null != stopGrantsPayingParagraphText)
            {
                StringBuilder datesPeriod = new StringBuilder("с ");
                if (null != extract.getStopPayingFrom())
                    datesPeriod.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStopPayingFrom()));
                else
                    datesPeriod.append("__.__.____");
                if (null != extract.getStopPayingTo())
                    datesPeriod.append(" по ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStopPayingTo()));
                modifier.put("stopGrantsPayingParagraph", stopGrantsPayingParagraphText.replaceAll("\\{stopGrantPayingPeriod\\}", datesPeriod.append(".").toString()));
            } else
                SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("stopGrantsPayingParagraph"), false, false);
        } else
        {
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("stopGrantPayingStr"), false, false);
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("stopGrantsPayingParagraph"), false, false);
        }

        if (extract.isAssignPayment())
        {
            StringBuilder builder = new StringBuilder(extract.isStopGrantsPaying() ? "3. " : "2. ");
            builder.append(modifier.getStringValue("student_D")).append(" ").append(modifier.getStringValue("fio")).append(" назначить выплату компенсации в размере ");
            builder.append(extract.getPaymentValue()).append(" c ");
            builder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPaymentAssignDate())).append(" г.");
            modifier.put("assignPaymentStr", builder.toString());
        } else
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("assignPaymentStr"), false, false);

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}