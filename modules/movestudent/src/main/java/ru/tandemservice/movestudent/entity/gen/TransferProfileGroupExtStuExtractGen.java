package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.TransferProfileGroupExtStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О переводе со специализации на специализацию (с профиля на профиль) и перераспределении в группу
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TransferProfileGroupExtStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.TransferProfileGroupExtStuExtract";
    public static final String ENTITY_NAME = "transferProfileGroupExtStuExtract";
    public static final int VERSION_HASH = -427759302;
    private static IEntityMeta ENTITY_META;

    public static final String P_PREV_SPLIT_ORDER_DATE = "prevSplitOrderDate";
    public static final String P_PREV_SPLIT_ORDER_NUMBER = "prevSplitOrderNumber";
    public static final String P_LIQUIDATE_EDU_PLAN_DIFFERENCE = "liquidateEduPlanDifference";
    public static final String P_LIQUIDATION_DEADLINE_DATE = "liquidationDeadlineDate";

    private Date _prevSplitOrderDate;     // Дата предыдущего приказа о распределении студентов по профилям
    private String _prevSplitOrderNumber;     // Номер предыдущего приказа о распределении студентов по профилям
    private boolean _liquidateEduPlanDifference;     // Ликвидировать разницу в учебных планах
    private Date _liquidationDeadlineDate;     // Крайняя дата ликвидации разницы в учебных планах

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата предыдущего приказа о распределении студентов по профилям.
     */
    public Date getPrevSplitOrderDate()
    {
        return _prevSplitOrderDate;
    }

    /**
     * @param prevSplitOrderDate Дата предыдущего приказа о распределении студентов по профилям.
     */
    public void setPrevSplitOrderDate(Date prevSplitOrderDate)
    {
        dirty(_prevSplitOrderDate, prevSplitOrderDate);
        _prevSplitOrderDate = prevSplitOrderDate;
    }

    /**
     * @return Номер предыдущего приказа о распределении студентов по профилям.
     */
    @Length(max=255)
    public String getPrevSplitOrderNumber()
    {
        return _prevSplitOrderNumber;
    }

    /**
     * @param prevSplitOrderNumber Номер предыдущего приказа о распределении студентов по профилям.
     */
    public void setPrevSplitOrderNumber(String prevSplitOrderNumber)
    {
        dirty(_prevSplitOrderNumber, prevSplitOrderNumber);
        _prevSplitOrderNumber = prevSplitOrderNumber;
    }

    /**
     * @return Ликвидировать разницу в учебных планах. Свойство не может быть null.
     */
    @NotNull
    public boolean isLiquidateEduPlanDifference()
    {
        return _liquidateEduPlanDifference;
    }

    /**
     * @param liquidateEduPlanDifference Ликвидировать разницу в учебных планах. Свойство не может быть null.
     */
    public void setLiquidateEduPlanDifference(boolean liquidateEduPlanDifference)
    {
        dirty(_liquidateEduPlanDifference, liquidateEduPlanDifference);
        _liquidateEduPlanDifference = liquidateEduPlanDifference;
    }

    /**
     * @return Крайняя дата ликвидации разницы в учебных планах.
     */
    public Date getLiquidationDeadlineDate()
    {
        return _liquidationDeadlineDate;
    }

    /**
     * @param liquidationDeadlineDate Крайняя дата ликвидации разницы в учебных планах.
     */
    public void setLiquidationDeadlineDate(Date liquidationDeadlineDate)
    {
        dirty(_liquidationDeadlineDate, liquidationDeadlineDate);
        _liquidationDeadlineDate = liquidationDeadlineDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TransferProfileGroupExtStuExtractGen)
        {
            setPrevSplitOrderDate(((TransferProfileGroupExtStuExtract)another).getPrevSplitOrderDate());
            setPrevSplitOrderNumber(((TransferProfileGroupExtStuExtract)another).getPrevSplitOrderNumber());
            setLiquidateEduPlanDifference(((TransferProfileGroupExtStuExtract)another).isLiquidateEduPlanDifference());
            setLiquidationDeadlineDate(((TransferProfileGroupExtStuExtract)another).getLiquidationDeadlineDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TransferProfileGroupExtStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TransferProfileGroupExtStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new TransferProfileGroupExtStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "prevSplitOrderDate":
                    return obj.getPrevSplitOrderDate();
                case "prevSplitOrderNumber":
                    return obj.getPrevSplitOrderNumber();
                case "liquidateEduPlanDifference":
                    return obj.isLiquidateEduPlanDifference();
                case "liquidationDeadlineDate":
                    return obj.getLiquidationDeadlineDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "prevSplitOrderDate":
                    obj.setPrevSplitOrderDate((Date) value);
                    return;
                case "prevSplitOrderNumber":
                    obj.setPrevSplitOrderNumber((String) value);
                    return;
                case "liquidateEduPlanDifference":
                    obj.setLiquidateEduPlanDifference((Boolean) value);
                    return;
                case "liquidationDeadlineDate":
                    obj.setLiquidationDeadlineDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "prevSplitOrderDate":
                        return true;
                case "prevSplitOrderNumber":
                        return true;
                case "liquidateEduPlanDifference":
                        return true;
                case "liquidationDeadlineDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "prevSplitOrderDate":
                    return true;
                case "prevSplitOrderNumber":
                    return true;
                case "liquidateEduPlanDifference":
                    return true;
                case "liquidationDeadlineDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "prevSplitOrderDate":
                    return Date.class;
                case "prevSplitOrderNumber":
                    return String.class;
                case "liquidateEduPlanDifference":
                    return Boolean.class;
                case "liquidationDeadlineDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TransferProfileGroupExtStuExtract> _dslPath = new Path<TransferProfileGroupExtStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TransferProfileGroupExtStuExtract");
    }
            

    /**
     * @return Дата предыдущего приказа о распределении студентов по профилям.
     * @see ru.tandemservice.movestudent.entity.TransferProfileGroupExtStuExtract#getPrevSplitOrderDate()
     */
    public static PropertyPath<Date> prevSplitOrderDate()
    {
        return _dslPath.prevSplitOrderDate();
    }

    /**
     * @return Номер предыдущего приказа о распределении студентов по профилям.
     * @see ru.tandemservice.movestudent.entity.TransferProfileGroupExtStuExtract#getPrevSplitOrderNumber()
     */
    public static PropertyPath<String> prevSplitOrderNumber()
    {
        return _dslPath.prevSplitOrderNumber();
    }

    /**
     * @return Ликвидировать разницу в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferProfileGroupExtStuExtract#isLiquidateEduPlanDifference()
     */
    public static PropertyPath<Boolean> liquidateEduPlanDifference()
    {
        return _dslPath.liquidateEduPlanDifference();
    }

    /**
     * @return Крайняя дата ликвидации разницы в учебных планах.
     * @see ru.tandemservice.movestudent.entity.TransferProfileGroupExtStuExtract#getLiquidationDeadlineDate()
     */
    public static PropertyPath<Date> liquidationDeadlineDate()
    {
        return _dslPath.liquidationDeadlineDate();
    }

    public static class Path<E extends TransferProfileGroupExtStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<Date> _prevSplitOrderDate;
        private PropertyPath<String> _prevSplitOrderNumber;
        private PropertyPath<Boolean> _liquidateEduPlanDifference;
        private PropertyPath<Date> _liquidationDeadlineDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата предыдущего приказа о распределении студентов по профилям.
     * @see ru.tandemservice.movestudent.entity.TransferProfileGroupExtStuExtract#getPrevSplitOrderDate()
     */
        public PropertyPath<Date> prevSplitOrderDate()
        {
            if(_prevSplitOrderDate == null )
                _prevSplitOrderDate = new PropertyPath<Date>(TransferProfileGroupExtStuExtractGen.P_PREV_SPLIT_ORDER_DATE, this);
            return _prevSplitOrderDate;
        }

    /**
     * @return Номер предыдущего приказа о распределении студентов по профилям.
     * @see ru.tandemservice.movestudent.entity.TransferProfileGroupExtStuExtract#getPrevSplitOrderNumber()
     */
        public PropertyPath<String> prevSplitOrderNumber()
        {
            if(_prevSplitOrderNumber == null )
                _prevSplitOrderNumber = new PropertyPath<String>(TransferProfileGroupExtStuExtractGen.P_PREV_SPLIT_ORDER_NUMBER, this);
            return _prevSplitOrderNumber;
        }

    /**
     * @return Ликвидировать разницу в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferProfileGroupExtStuExtract#isLiquidateEduPlanDifference()
     */
        public PropertyPath<Boolean> liquidateEduPlanDifference()
        {
            if(_liquidateEduPlanDifference == null )
                _liquidateEduPlanDifference = new PropertyPath<Boolean>(TransferProfileGroupExtStuExtractGen.P_LIQUIDATE_EDU_PLAN_DIFFERENCE, this);
            return _liquidateEduPlanDifference;
        }

    /**
     * @return Крайняя дата ликвидации разницы в учебных планах.
     * @see ru.tandemservice.movestudent.entity.TransferProfileGroupExtStuExtract#getLiquidationDeadlineDate()
     */
        public PropertyPath<Date> liquidationDeadlineDate()
        {
            if(_liquidationDeadlineDate == null )
                _liquidationDeadlineDate = new PropertyPath<Date>(TransferProfileGroupExtStuExtractGen.P_LIQUIDATION_DEADLINE_DATE, this);
            return _liquidationDeadlineDate;
        }

        public Class getEntityClass()
        {
            return TransferProfileGroupExtStuExtract.class;
        }

        public String getEntityName()
        {
            return "transferProfileGroupExtStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
