/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.NumberConvertingUtil;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author vip_delete
 * @since 06.05.2009
 */
public class CommonListExtractPrint
{
    private static final Map<String, String[]> DIFFERENT_WORD_MALE_SEX_CASES_ARRAY = new HashMap<>();
    private static final Map<String, String[]> DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY = new HashMap<>();
    private static final Map<String, String[]> STUDENT_CATEGORY_MALE_SEX_CASES_ARRAY = new HashMap<>();
    private static final Map<String, String[]> STUDENT_CATEGORY_FEMALE_SEX_CASES_ARRAY = new HashMap<>();


    static
    {
        // male sex cases
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("gender", new String[]{"он", "его", "ему", "его", "им", "нем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("student", new String[]{"студент", "студента", "студенту", "студента", "студентом", "студенте"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("Student", new String[]{"Студент", "Студента", "Студенту", "Студента", "Студентом", "Студенте"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("learned", new String[]{"обучающийся", "обучающегося", "обучающемуся", "обучающегося", "обучающимся", "обучающимся"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("learned_past", new String[]{"обучался", "обучавшемуся", "обучавшемуся", "обучавшегося", "обучавшимся", "обучавшемуся"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("toBegin", new String[]{"приступивший", "приступившего", "приступившему", "приступившего", "приступившим", "приступившем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("developed", new String[]{"освоивший", "освоившего", "освоившему", "освоившего", "освоившим", "освоившем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("proved", new String[]{"проявивший", "проявившего", "проявившему", "проявившего", "проявившим", "проявившем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("tested", new String[]{"прошедший", "прошедшего", "прошедшему", "прошедшего", "прошедшим", "прошедшем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("finished", new String[]{"окончивший", "окончившего", "окончившему", "окончившего", "окончившим", "окончившем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("finishedAlt", new String[]{"закончивший", "закончившего", "закончившему", "закончившего", "закончившим", "закончившем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("executed", new String[]{"выполнивший", "выполнившего", "выполнившему", "выполнившего", "выполнившим", "выполнившем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("defended", new String[]{"защитивший", "защитившего", "защитившему", "защитившего", "защитившим", "защитившем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("passed", new String[]{"сдавший", "сдавшего", "сдавшему", "сдавшего", "сдавшим", "сдавшем"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("accepted", new String[]{"зачиленный", "зачисленного", "зачисленному", "зачисленного", "зачисленным", "зачисленном"});
        DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.put("transferred", new String[]{"переведённый", "переведённого", "переведённому", "переведённого", "переведённым", "переведённом"});
        STUDENT_CATEGORY_MALE_SEX_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_STUDENT, new String[]{"студент", "студента", "студенту", "студента", "студентом", "студенте"});
        STUDENT_CATEGORY_MALE_SEX_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY, new String[]{"студент", "студента", "студенту", "студента", "студентом", "студенте"});
        STUDENT_CATEGORY_MALE_SEX_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_LISTENER, new String[]{"слушатель", "слушателя", "слушателю", "слушателя", "слушателем", "слушателе"});
        STUDENT_CATEGORY_MALE_SEX_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_DPP, new String[]{"слушатель", "слушателя", "слушателю", "слушателя", "слушателем", "слушателе"});

        // female sex cases
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("gender", new String[]{"она", "ее", "ей", "ее", "ей", "ней"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("student", new String[]{"студентка", "студентки", "студентке", "студентку", "студенткой", "студентке"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("Student", new String[]{"Студентка", "Студентки", "Студентке", "Студентку", "Студенткой", "Студентке"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("learned", new String[]{"обучающаяся", "обучающейся", "обучающейся", "обучающуюся", "обучающейся", "обучающейся"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("learned_past", new String[]{"обучалась", "обучавшейся", "обучавшейся", "обучавшуюся", "обучавшейся", "обучавшейся"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("toBegin", new String[]{"приступившая", "приступившей", "приступившей", "приступившую", "приступившей", "приступившем"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("developed", new String[]{"освоившая", "освоившей", "освоившей", "освоившую", "освоившей", "освоившей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("proved", new String[]{"проявившая", "проявившей", "проявившей", "проявившую", "проявившей", "проявившей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("tested", new String[]{"прошедшая", "прошедшей", "прошедшей", "прошедшую", "прошедшей", "прошедшей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("finished", new String[]{"окончившая", "окончившей", "окончившей", "окончившую", "окончившей", "окончившей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("finishedAlt", new String[]{"закончившая", "закончившей", "закончившей", "закончившую", "закончившей", "закончившей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("executed", new String[]{"выполнившая", "выполнившей", "выполнившей", "выполнившую", "выполнившей", "выполнившей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("defended", new String[]{"защитившая", "защитившей", "защитившей", "защитившую", "защитившей", "защитившей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("passed", new String[]{"сдавшая", "сдавшей", "сдавшей", "сдавшую", "сдавшей", "сдавшей"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("accepted", new String[]{"зачисленная", "зачисленной", "зачисленной", "зачисленную", "зачисленной", "зачисленной"});
        DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.put("transferred", new String[]{"переведённая", "переведённой", "переведённой", "переведённую", "переведённой", "переведённой"});
        STUDENT_CATEGORY_FEMALE_SEX_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_STUDENT, new String[]{"студентка", "студентки", "студентке", "студентку", "студенткой", "студентке"});
        STUDENT_CATEGORY_FEMALE_SEX_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY, new String[]{"студентка", "студентки", "студентке", "студентку", "студенткой", "студентке"});
        STUDENT_CATEGORY_FEMALE_SEX_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_LISTENER, new String[]{"слушатель", "слушателя", "слушателю", "слушателя", "слушателем", "слушателе"});
        STUDENT_CATEGORY_FEMALE_SEX_CASES_ARRAY.put(StudentCategoryCodes.STUDENT_CATEGORY_DPP, new String[]{"слушатель", "слушателя", "слушателю", "слушателя", "слушателем", "слушателе"});


    }

    public static final Map<String, String[]> STUDENTS_CATEGORY_CASES_ARRAY = new HashMap<String, String[]>()
    {
        {
            // plural cases
            put(StudentCategoryCodes.STUDENT_CATEGORY_STUDENT, new String[]{"студенты", "студентов", "студентам", "студентов", "студентами", "студентах"});
            put(StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY, new String[]{"студенты", "студентов", "студентам", "студентов", "студентами", "студентах"});
            put(StudentCategoryCodes.STUDENT_CATEGORY_LISTENER, new String[]{"слушатели", "слушателей", "слушателям", "слушателей", "слушателями", "слушателях"});
            put(StudentCategoryCodes.STUDENT_CATEGORY_DPP, new String[]{"слушатели", "слушателей", "слушателям", "слушателей", "слушателями", "слушателях"});
        }
    };

    public static void injectCommonListExtractData(RtfInjectModifier modifier, ListStudentExtract extract)
    {
        IEntityMeta meta = EntityRuntime.getMeta(extract.getId());

        final String[] postfixList = new String[]{"Old", "New"};
        injectAcademy(modifier);

        Student student = extract.getEntity();
        IdentityCard identityCard = student.getPerson().getIdentityCard();

        modifier.put("studentPersonalNumber", student.getPersonalNumber());
        modifier.put("studentPersonalFileNumber", null != student.getPersonalFileNumber() ? student.getPersonalFileNumber() : "");
        modifier.put("studentBookNumber", null != student.getBookNumber() ? student.getBookNumber() : "");

        boolean maleSex = identityCard.getSex().isMale();
        injectDifferentWords(modifier, maleSex ? DIFFERENT_WORD_MALE_SEX_CASES_ARRAY : DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY);

        // extractType
        modifier.put("extractType", extract.getParagraph() == null ? "проект приказа" : "выписка");
        // DEV-4953
        modifier.put("extractTypeAlt", extract.getParagraph() == null ? "Проект" : "Выписка");

        Map<String, String[]> studentCategoryCasesMap = maleSex ? STUDENT_CATEGORY_MALE_SEX_CASES_ARRAY : STUDENT_CATEGORY_FEMALE_SEX_CASES_ARRAY;
        String[] categoryCase = studentCategoryCasesMap.get(student.getStudentCategory().getCode());
        if (null != categoryCase)
        {
            for (String casePostfix : UniRtfUtil.CASE_POSTFIX)
            {
                modifier.put("studentCategory" + casePostfix, categoryCase[UniRtfUtil.CASE_POSTFIX.indexOf(casePostfix)]);
            }
        }


        // orgUnit (+*)
        /*
         * При печати выписки, если территориальное подр. не указано, то в () должно выводиться название населенного
         * пункта из фактического адреса объекта "ОУ", в офрмате "г. Екатеринбург", если фактический адрес не заполнен,
         * то в () будет пусто. Сейчас в () пусто всегда, когда не указано территориальное подр.. Если же территориальное
         * подр. указано, то в скобочках выводить название населенного пункта из фактического адреса территориального
         * подр., в формате "г. Саратов", если в фактическом адресе не указано населенного пункта, то выводим
         * в () название территориального подр. в именительном падеже, если и в именительном падеже название не задано,
         * то выводим просто в стандартном формате "название подразделения (тип подразделения)".
         */
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();

        CommonExtractPrint.initOrgUnit(modifier, educationOrgUnit, "orgUnit", "");
        CommonExtractPrint.initOrgUnit(modifier, educationOrgUnit, "formativeOrgUnit", "");
        initTerritorialOrgUnit(modifier, educationOrgUnit.getTerritorialOrgUnit(), "territorialOrgUnit", "");

        for (String postfix : postfixList)
        {
            String property = "course" + postfix;
            if (meta.getProperty(property) != null)
            {
                modifier.put(property, ((Course) extract.getProperty(property)).getTitle());
            }

            property = "group" + postfix;
            if (meta.getProperty(property) != null)
            {
                modifier.put(property, ((Group) extract.getProperty(property)).getTitle());
            }

            property = "compensationType" + postfix;
            if (meta.getProperty(property) != null)
            {
	            injectCompensationType(modifier, ((CompensationType) (extract.getProperty(property))), postfix, student.isTargetAdmission());
            }

            property = "educationOrgUnit" + postfix;
            if (meta.getProperty(property) != null)
            {
                EducationOrgUnit eduOU = (EducationOrgUnit) extract.getProperty(property);

                CommonExtractPrint.initOrgUnit(modifier, eduOU, "orgUnit", postfix);

                initTerritorialOrgUnit(modifier, eduOU.getTerritorialOrgUnit(), "territorialOrgUnit", postfix);

                UniRtfUtil.initEducationType(modifier, eduOU.getEducationLevelHighSchool(), postfix);

                CommonExtractPrint.initDevelopForm(modifier, eduOU, postfix);

                initDevelopCondition(modifier, eduOU, postfix);

                modifier.put(property, eduOU.getEducationLevelHighSchool().getPrintTitleWithoutOrgUnit());

                // Новые направления
                EducationLevels educationLevels = eduOU.getEducationLevelHighSchool().getEducationLevel();
                CommonExtractPrint.initProgramSubjectLabels(modifier, educationLevels.getEduProgramSubject(), educationLevels.getEduProgramSpecialization(), postfix);
            }
        }
        UniRtfUtil.initEducationType(modifier, educationOrgUnit.getEducationLevelHighSchool(), "");

        modifier.put("course", student.getCourse().getTitle());

        modifier.put("group", student.getGroup() == null ? "" : student.getGroup().getTitle());

        CommonExtractPrint.initDevelopForm(modifier, educationOrgUnit, "");

        initDevelopCondition(modifier, educationOrgUnit, "");

        modifier.put("educationOrgUnit", educationOrgUnit.getEducationLevelHighSchool().getPrintTitleWithoutOrgUnit());

        // Новые направления
        EducationLevels educationLevels = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel();
        CommonExtractPrint.initProgramSubjectLabels(modifier, educationLevels.getEduProgramSubject(), educationLevels.getEduProgramSpecialization(), "");

        injectCompensationType(modifier, student.getCompensationType(), "", student.isTargetAdmission());

        // fio
        modifier.put("fio", extract.getStudentTitle());
        modifier.put("FIO", extract.getStudentTitle().toUpperCase());

        // fio in Nominative
        String fullFio = identityCard.getFullFio();
        modifier.put("fio_N", fullFio);
        modifier.put("FIO_N", fullFio.toUpperCase());
        injectFio(modifier, identityCard);


        EducationLevelsHighSchool eduHS = educationOrgUnit.getEducationLevelHighSchool();
        Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(eduHS.getEducationLevel());
        if (null == qualification)
        {
            modifier.put("educationOrgUnitWithLevel", "по направлению \"" + eduHS.getTitle() + "\"");
        }
        //направление магистров
        else if (QualificationsCodes.MAGISTR.equals(qualification.getCode()))
        {
            modifier.put("educationOrgUnitWithLevel", "по направлению магистратуры \"" + eduHS.getTitle() + "\"");
        }
        //направление бакалавров
        else if (QualificationsCodes.BAKALAVR.equals(qualification.getCode()))
        {
            modifier.put("educationOrgUnitWithLevel", "по направлению бакалавриата \"" + eduHS.getTitle() + "\"");
        }
        //направление специальностей
        else if (QualificationsCodes.SPETSIALIST.equals(qualification.getCode()))
        {
            modifier.put("educationOrgUnitWithLevel", "по специальности \"" + eduHS.getTitle() + "\"");
        }
        //специальность (базовый уровень)
        else if (QualificationsCodes.BAZOVYY_UROVEN_S_P_O.equals(qualification.getCode()))
        {
            modifier.put("educationOrgUnitWithLevel", "по специальности \"" + eduHS.getTitle() + " (базовый уровень)\"");
        }
        //специальность (повышенный уровень)
        else if (QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O.equals(qualification.getCode()))
        {
            modifier.put("educationOrgUnitWithLevel", "по специальности \"" + eduHS.getTitle() + " (повышенный уровень)\"");
        }
        else
            modifier.put("educationOrgUnitWithLevel", "по специальности \"" + eduHS.getTitle() + "\"");


        StudentListParagraph paragraph = (StudentListParagraph) extract.getParagraph();
        StudentListOrder order = paragraph != null ? (StudentListOrder) paragraph.getOrder() : null;

        // orderType
        if (null != order)
            modifier.put("orderType", order.getType().getTitle());
        else
            modifier.put("orderType", extract.getType().getTitle());

        // date
        if (meta.getProperty("date") != null)
            modifier.put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) extract.getProperty("date")));

        // createDate
        modifier.put("createDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getCreateDate()));

        // commitDate
        Date commitDate = order != null ? order.getCommitDate() : null;
        if (commitDate == null)
            modifier.put("commitDate", "        ");
        else
            modifier.put("commitDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(commitDate));

        // order number
        modifier.put("orderNumber", order == null ? "     " : order.getNumber());

        // extractNumber
        modifier.put("extractNumber", extract.getNumber() == null ? "     " : Integer.toString(extract.getNumber()));

        // paragraphNumber
        modifier.put("parNumber", paragraph == null ? "     " : Integer.toString(paragraph.getNumber()));
        modifier.put("parNumberRoman", paragraph == null ? "     " : NumberConvertingUtil.convertDecimal2Roman(paragraph.getNumber()));
        modifier.put("eduYear", order != null ? order.getEducationYear().getTitle() : "");

        // listBasics
        String basicsStr = order != null ? order.getBasicListStr() : null;
        if (null != basicsStr && !basicsStr.isEmpty())
        {
            modifier.put("basicsWord", "Основание: ");
            modifier.put("listBasics", basicsStr);
            modifier.put("basicsPoint", ".");
        }
        else
        {
            modifier.put("basicsWord", "");
            modifier.put("listBasics", "");
            modifier.put("basicsPoint", "");
        }

        // reason
        if (order != null)
            CommonExtractPrint.injectReason(modifier, order.getReason(), order.getReasonComment());
        else
            CommonExtractPrint.injectReason(modifier, null, null);

        // SPECIAL CODES
        if (meta.getProperty("firstNameNew") != null)
        {
            String firstNameNew = (String) extract.getProperty("firstNameNew");
            String lastNameNew = (String) extract.getProperty("lastNameNew");
            String middleNameNew = (String) extract.getProperty("middleNameNew");
            StringBuilder str = new StringBuilder(lastNameNew);
            if (StringUtils.isNotEmpty(firstNameNew))
                str.append(" ").append(firstNameNew);
            if (StringUtils.isNotEmpty(middleNameNew))
                str.append(" ").append(middleNameNew);
            modifier.put("fullFioNew", str.toString());
            modifier.put("firstNameNew", firstNameNew);
            modifier.put("lastNameNew", lastNameNew);
            modifier.put("middleNameNew", middleNameNew);
        }

        modifier.put("birthDate", extract.getEntity().getPerson().getBirthDateStr());

        if (meta.getProperty("university") != null)
            modifier.put("university", (String) extract.getProperty("university"));

        if (meta.getProperty("deadline") != null)
            modifier.put("deadline", DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) extract.getProperty("deadline")));

        if (meta.getProperty("canceledExtract") != null)
        {
            ModularStudentExtract canceledExtract = (ModularStudentExtract) extract.getProperty("canceledExtract");
            modifier.put("revertedOrderNumber", canceledExtract.getParagraph() == null ? "" : canceledExtract.getParagraph().getOrder().getNumber());
            modifier.put("revertedExtractNumber", canceledExtract.getParagraph() == null ? "" : Integer.toString(canceledExtract.getParagraph().getNumber()));
            modifier.put("revertedCommitDate", canceledExtract.getParagraph() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(canceledExtract.getParagraph().getOrder().getCommitDate()) + " г.");
            modifier.put("revertedOrderType", canceledExtract.getType().getTitle());
        }

        if (meta.getProperty("beginDate") != null)
        {
            Date beginDate = (Date) extract.getProperty("beginDate");
            modifier.put("beginDate", beginDate == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(beginDate));
        }

        if (meta.getProperty("endDate") != null)
        {
            Date endDate = (Date) extract.getProperty("endDate");
            modifier.put("endDate", endDate == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(endDate));
        }


        // Точка расширения для проектных модулей
        MoveStudentPrintModifierManager.instance().modifier().listExtractModifier(modifier, extract);
    }

    public static void injectFio(RtfInjectModifier modifier, IdentityCard identityCard)
    {
        int indexOfSpace;
        Map<InflectorVariant, String> fioDeclinations = PersonManager.instance().declinationDao().getCalculatedFIODeclinations(identityCard);

        for (InflectorVariant variant : DeclinationDao.INFLECTOR_VARIANT_LIST)
        {
            GrammaCase grammaCase = DeclinationDao.INFLECTOR_VARIANT_CODES_TO_GRAMMA_CASE_MAP.get(variant.getCode());
            if (grammaCase == null)
                continue;

            String postfix = CommonExtractPrint.GRAMMA_CASE_POSTFIX.get(grammaCase);

            String fioDeclinated = fioDeclinations.get(variant);
            String fioDeclinatedUpper = fioDeclinated.toUpperCase();
            modifier.put("fio_" + postfix, fioDeclinated);

            indexOfSpace = fioDeclinated.indexOf(" ");
            modifier.put("Fio_" + postfix, indexOfSpace <= 0 ? fioDeclinatedUpper : (fioDeclinated.substring(0, indexOfSpace).toUpperCase() + fioDeclinated.substring(indexOfSpace)));
            modifier.put("FIO_" + postfix, fioDeclinatedUpper);

            String[] fioArr = StringUtils.split(fioDeclinated, " ");
            String fioInitials = fioArr[0] + " " + fioArr[1].charAt(0) + "." + (fioArr.length > 2 ? (fioArr[2].charAt(0) + ".") : "");
            modifier.put("fio_" + postfix + "_initials", fioInitials);

            String fioInitialsUpper = fioInitials.toUpperCase();
            modifier.put("Fio_" + postfix + "_initials", fioInitialsUpper);
            modifier.put("FIO_" + postfix + "_initials", fioInitialsUpper);
        }
    }


    /**
     * @return Возвращает строку формата И.О. Фамилия.
     */
    public static String getModifiedIofInitials(IdentityCard identityCard, GrammaCase rusCase)
    {
        if (null == rusCase) return identityCard.getIof();

        //boolean isMaleSex = identityCard.getSex().isMale();

        CoreCollectionUtils.Pair<String, InflectorVariant> lastNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.lastName().s(), DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(rusCase));

        StringBuilder str = new StringBuilder();
        str.append(identityCard.getFirstName().toCharArray()[0]).append(".");
        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            str.append(identityCard.getMiddleName().toCharArray()[0]).append(".");
        str.append(" ");
        str.append(PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(lastNamePairKey));
        return str.toString();
    }

    /**
     * @return Возвращает строку формата Фамилия И.О.
     */
    public static String getModifiedFioInitials(IdentityCard identityCard, GrammaCase rusCase)
    {
        if (null == rusCase) return identityCard.getFullFio();

        //boolean isMaleSex = identityCard.getSex().isMale();

        CoreCollectionUtils.Pair<String, InflectorVariant> lastNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.lastName().s(), DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(rusCase));

        StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(lastNamePairKey));
        str.append(" ").append(identityCard.getFirstName().toCharArray()[0]).append(".");
        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            str.append(identityCard.getMiddleName().toCharArray()[0]).append(".");
        return str.toString();
    }

    private static void initDevelopCondition(RtfInjectModifier modifier, EducationOrgUnit educationOrgUnit, String postfix)
    {
        DevelopCondition developCondition = educationOrgUnit.getDevelopCondition();

        if (UniDefines.DEVELOP_CONDITION_SHORT.equals(developCondition.getCode()))
        {
            modifier.put("developCondition" + postfix, "сокращенная");
            modifier.put("developCondition" + postfix + "_G", "сокращенной");
            modifier.put("developCondition" + postfix + "_D", "сокращенной");
            modifier.put("developCondition" + postfix + "_A", "сокращенную");
            modifier.put("developCondition" + postfix + "_I", "сокращенной");
            modifier.put("developCondition" + postfix + "_P", "сокращенной");
        }
        else if (UniDefines.DEVELOP_CONDITION_FAST.equals(developCondition.getCode()))
        {
            modifier.put("developCondition" + postfix, "ускоренная");
            modifier.put("developCondition" + postfix + "_G", "ускоренной");
            modifier.put("developCondition" + postfix + "_D", "ускоренной");
            modifier.put("developCondition" + postfix + "_A", "ускоренную");
            modifier.put("developCondition" + postfix + "_I", "ускоренной");
            modifier.put("developCondition" + postfix + "_P", "ускоренной");
        }
        else if (UniDefines.DEVELOP_CONDITION_SHORT_FAST.equals(developCondition.getCode()))
        {
            modifier.put("developCondition" + postfix, "сокращенная ускоренная");
            modifier.put("developCondition" + postfix + "_G", "сокращенной ускоренной");
            modifier.put("developCondition" + postfix + "_D", "сокращенной ускоренной");
            modifier.put("developCondition" + postfix + "_A", "сокращенную ускоренную");
            modifier.put("developCondition" + postfix + "_I", "сокращенной ускоренной");
            modifier.put("developCondition" + postfix + "_P", "сокращенной ускоренной");
        }
        else
        {
            modifier.put("developCondition" + postfix, "полная");
            modifier.put("developCondition" + postfix + "_G", "полной");
            modifier.put("developCondition" + postfix + "_D", "полной");
            modifier.put("developCondition" + postfix + "_A", "полную");
            modifier.put("developCondition" + postfix + "_I", "полной");
            modifier.put("developCondition" + postfix + "_P", "полной");
        }
    }

    private static void initTerritorialOrgUnit(RtfInjectModifier modifier, OrgUnit orgUnit, String prefix, String postfix)
    {
        AddressDetailed address = orgUnit.getAddress();

        if (address == null || address.getSettlement() == null)
            modifier.put(prefix + postfix, "");
        else
            modifier.put(prefix + postfix, address.getSettlement().getAddressType().getShortTitle() + " " + address.getSettlement().getTitle());
    }

    private static void injectAcademy(RtfInjectModifier injectModifier)
    {
        TopOrgUnit academy = TopOrgUnit.getInstance(true);
        injectModifier.put("academy", null != academy.getNominativeCaseTitle() ? academy.getNominativeCaseTitle() : academy.getFullTitle());
        injectModifier.put("academy_N", null != academy.getNominativeCaseTitle() ? academy.getNominativeCaseTitle() : academy.getFullTitle());
        injectModifier.put("academy_G", null != academy.getGenitiveCaseTitle() ? academy.getGenitiveCaseTitle() : academy.getFullTitle());
        injectModifier.put("academy_D", null != academy.getDativeCaseTitle() ? academy.getDativeCaseTitle() : academy.getFullTitle());
        injectModifier.put("academy_A", null != academy.getAccusativeCaseTitle() ? academy.getAccusativeCaseTitle() : academy.getFullTitle());
        injectModifier.put("academy_P", null != academy.getPrepositionalCaseTitle() ? academy.getPrepositionalCaseTitle() : academy.getFullTitle());

        injectModifier.put("topOrgUnitFullTitle", academy.getTitle());
	    injectModifier.put("topOrgUnitCityTitle",
	                       (null != academy.getPostalAddress() && null != academy.getPostalAddress().getSettlement()) ? academy.getPostalAddress().getSettlement().getTitle() :
			                       (null != academy.getLegalAddress() && null != academy.getLegalAddress().getSettlement()) ? academy.getLegalAddress().getSettlement().getTitle() :
					                       (null != academy.getAddress() && null != academy.getAddress().getSettlement()) ? academy.getAddress().getSettlement().getTitle() : "");
    }

    public static void injectCompensationType(RtfInjectModifier modifier, CompensationType compensationType, String postfix, boolean isTargetAdmission)
    {
	    boolean budget = compensationType.isBudget();
	    modifier.put("compensationTypeStr" + postfix, budget ? "на бюджетной основе" : "по договору, предусматривающему оплату обучения");
	    modifier.put("compensationTypeStr" + postfix + "_A", budget ? "на бюджетную основу" : "на контрактную основу");
	    modifier.put("compensationTypeStr" + postfix + "_G", budget ? "бюджетной основы" : "контрактной основы");
	    modifier.put("compensationTypeStr" + postfix + "_D_Alt", budget ? "бюджетной основе" : "внебюджетной основе");
	    modifier.put("compensationTypeStr" + postfix + "_G_Alt", budget ? "бюджетной основы" : "внебюджетной основы");
	    modifier.put("compensationTypeStr" + postfix + "_A_Alt", budget ? "бюджетную основу" : "внебюджетную основу");
	    modifier.put("fefuCompensationTypeStr" + postfix, budget ?
                "за счет бюджетных ассигнований федерального бюджета" :
                "на основе договора об оказании платных образовательных услуг");

	    String admissionType;
	    String admissionType_A;
	    String admissionType_Other;
	    if (isTargetAdmission)
	    {
		    admissionType = budget ? "целевой " : "целевому ";
		    admissionType_A = "целевую ";
		    admissionType_Other = "целевой ";
	    }
	    else admissionType = admissionType_A = admissionType_Other = "";
	    modifier.put("usmaCompensationTypeStr" + postfix, budget ? "на " + admissionType + "бюджетной основе" : admissionType + "по " + admissionType + "договору, предусматривающему оплату обучения");
	    modifier.put("usmaCompensationTypeStr" + postfix + "_A", budget ? "на " + admissionType_A + "бюджетную основу" : admissionType + "на " + admissionType_A + "контрактную основу");
	    modifier.put("usmaCompensationTypeStr" + postfix + "_G", budget ? admissionType_Other + "бюджетной основы" : admissionType_Other + "контрактной основы");
	    modifier.put("usmaCompensationTypeStr" + postfix + "_D_Alt", budget ? admissionType_Other + "бюджетной основе" : admissionType_Other + "внебюджетной основе");
	    modifier.put("usmaCompensationTypeStr" + postfix + "_G_Alt", budget ? admissionType_Other + "бюджетной основы" : admissionType_Other + "внебюджетной основы");
	    modifier.put("usmaCompensationTypeStr" + postfix + "_A_Alt", budget ? admissionType_A + "бюджетную основу" : admissionType_A + "внебюджетную основу");
    }

    private static void injectDifferentWords(RtfInjectModifier modifier, Map<String, String[]> differentWords)
    {
        for (Map.Entry<String, String[]> entry : differentWords.entrySet())
        {
            for (String casePostfix : UniRtfUtil.CASE_POSTFIX)
            {
                modifier.put(entry.getKey() + casePostfix, entry.getValue()[UniRtfUtil.CASE_POSTFIX.indexOf(casePostfix)]);
            }
        }
    }

    public static void injectStudentsCategory(RtfInjectModifier modifier, StudentCategory studentsCategory)
    {
        Map<String, String[]> studentsCategoryCasesMap = STUDENTS_CATEGORY_CASES_ARRAY;
        String[] catCase = studentsCategoryCasesMap.get(studentsCategory.getCode());
        if (null != catCase)
        {
            for (String casePostfix : UniRtfUtil.CASE_POSTFIX)
            {
                modifier.put("studentsCategory" + casePostfix, catCase[UniRtfUtil.CASE_POSTFIX.indexOf(casePostfix)]);
                modifier.put("StudentsCategory" + casePostfix, StringUtils.capitalize(catCase[UniRtfUtil.CASE_POSTFIX.indexOf(casePostfix)]));
            }
        }
    }
}