package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IEnrollmentExtract;
import ru.tandemservice.movestudent.entity.gen.RestorationAdmitToDiplomaDefStuExtractGen;

import java.util.Date;

/**
 * О восстановлении и допуске к защите ВКР (неявка на защиту)
 */
public class RestorationAdmitToDiplomaDefStuExtract extends RestorationAdmitToDiplomaDefStuExtractGen implements IEnrollmentExtract
{
    @Override
    public Date getEntranceDate()
    {
        return getRestorationDate();
    }
}