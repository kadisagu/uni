package ru.tandemservice.movestudent.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_movestudent_2x11x0_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность transferProgramSubjectStuListExtract

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("trnsfrprgrmsbjctstlstextrct_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_144114e2"), 
				new DBColumn("educationorgunitold_id", DBType.LONG),
				new DBColumn("educationorgunitnew_id", DBType.LONG).setNullable(false), 
				new DBColumn("course_id", DBType.LONG).setNullable(false), 
				new DBColumn("group_id", DBType.LONG).setNullable(false), 
				new DBColumn("transferdate_p", DBType.DATE), 
				new DBColumn("changegroupeducationou_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("groupeducationouold_id", DBType.LONG),
				new DBColumn("groupeducationounew_id", DBType.LONG),
                new DBColumn("eduprogramsubjectold_id", DBType.LONG).setNullable(false),
                new DBColumn("eduprogramsubjectnew_id", DBType.LONG).setNullable(false)

			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("transferProgramSubjectStuListExtract");

		}


    }
}