/* $Id$ */
package ru.tandemservice.movestudent.dao;

import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.custom.ModifyListStuExtract;
import ru.tandemservice.movestudent.entity.custom.ModifyModularStuExtract;

/**
 * @author Alexey Lopatin
 * @since 04.10.2013
 */
public interface IGeneratedCommentForStudentOrders
{
    String GENERATED_COMMENT_FOR_STUDENT_ORDERS_BEAN_NAME = "generatedCommentForStudentOrders";

    ///// Выписки из кастомных приказов по студентам /////

    // Сборные приказы
    String getComment(ModifyModularStuExtract extract, boolean isPrintOrder);

    // Списочные приказы
    String getComment(ModifyListStuExtract extract, boolean isPrintOrder);

    ///// Выписки из прочих приказов по студентам /////

    String getComment(OtherStudentExtract extract, boolean isPrintOrder);

    ///// Выписки из сборных приказов по студентам /////

    // 1.1  Об отмене приказа
    String getComment(RevertOrderStuExtract extract, boolean isPrintOrder);

    // 1.2  О предоставлении академического отпуска (Вариант 1)
    String getComment(WeekendStuExtract extract, boolean isPrintOrder);

    // 1.3  О переводе на другую форму освоения
    String getComment(TransferDevFormStuExtract extract, boolean isPrintOrder);

    // 1.4  О переводе на другую основу оплаты обучения
    String getComment(TransferCompTypeStuExtract extract, boolean isPrintOrder);

    // 1.5  О переводе на другое направление подготовки (специальность)
    String getComment(TransferEduTypeStuExtract extract, boolean isPrintOrder);

    // 1.6  Об изменении пункта обучения
    String getComment(TransferStuExtract extract, boolean isPrintOrder);

    // 1.7  Об отчислении (Вариант 1)
    String getComment(ExcludeStuExtract extract, boolean isPrintOrder);

    // 1.8  О смене фамилии (имени)
    String getComment(ChangeFioStuExtract extract, boolean isPrintOrder);

    // 1.9  О выходе из академического отпуска
    String getComment(WeekendOutStuExtract extract, boolean isPrintOrder);

    // 1.10  О восстановлении (Вариант 1)
    String getComment(EduEnrolmentStuExtract extract, boolean isPrintOrder);

    // 1.11  О зачислении (Вариант 1)
    String getComment(RestorationStuExtract extract, boolean isPrintOrder);

    // 1.12  О предоставлении отпуска по беременности и родам
    String getComment(WeekendPregnancyStuExtract extract, boolean isPrintOrder);

    // 1.13  О предоставлении отпуска по уходу за ребенком
    String getComment(WeekendChildStuExtract extract, boolean isPrintOrder);

    // 1.14  О зачислении (Вариант 2)
    String getComment(EduEnrolmentStuExtractExt extract, boolean isPrintOrder);

    // 1.15  О зачислении в порядке перевода
    String getComment(EduEnrAsTransferStuExtract extract, boolean isPrintOrder);

    // 1.16  О предоставлении академического отпуска (Вариант 2)
    String getComment(WeekendStuExtractExt extract, boolean isPrintOrder);

    // 1.17  О восстановлении (Вариант 2)
    String getComment(RestorationStuExtractExt extract, boolean isPrintOrder);

    // 1.18  О переводе
    String getComment(TransferStuExtractExt extract, boolean isPrintOrder);

    // 1.19  Об отстранении от занятий
    String getComment(DischargingStuExtract extract, boolean isPrintOrder);

    // 1.20  О продлении экзаменационной сессии
    String getComment(SessionProlongStuExtract extract, boolean isPrintOrder);

    // 1.21  О продлении отпуска по уходу за ребенком
    String getComment(ProlongWeekndChildStuExtract extract, boolean isPrintOrder);

    // 1.22  О назначении академической стипендии
    String getComment(AcadGrantAssignStuExtract extract, boolean isPrintOrder);

    // 1.23  О назначении социальной стипендии
    String getComment(SocGrantAssignStuExtract extract, boolean isPrintOrder);

    // 1.24  О приостановлении выплаты социальной стипендии
    String getComment(SocGrantStopStuExtract extract, boolean isPrintOrder);

    // 1.25  О возобновлении выплаты социальной стипендии
    String getComment(SocGrantResumptionStuExtract extract, boolean isPrintOrder);

    // 1.26  Об отчислении (Вариант 2)
    String getComment(ExcludeStuExtractExt extract, boolean isPrintOrder);

    // 1.27  Об отчислении в связи с переводом
    String getComment(ExcludeTransfStuExtract extract, boolean isPrintOrder);

    // 1.28  О выдаче дубликата зачетной книжки
    String getComment(GiveBookDuplicateStuExtract extract, boolean isPrintOrder);

    // 1.29  О выдаче дубликата студенческого билета
    String getComment(GiveCardDuplicateStuExtract extract, boolean isPrintOrder);

    // 1.30  О выдаче дубликата диплома
    String getComment(GiveDiplomaDuplicateStuExtract extract, boolean isPrintOrder);

    // 1.31  О выдаче дубликата приложения к диплому
    String getComment(GiveDiplAppDuplicateStuExtract extract, boolean isPrintOrder);

    // 1.32  О допуске к занятиям
    String getComment(AdmissionStuExtract extract, boolean isPrintOrder);

    // 1.33  О назначении выплаты пособий
    String getComment(AssignBenefitStuExtract extract, boolean isPrintOrder);

    // 1.34  О смене имени
    String getComment(ChangeFirstNameStuExtract extract, boolean isPrintOrder);

    // 1.35  О смене отчества
    String getComment(ChangeMiddleNameStuExtract extract, boolean isPrintOrder);

    // 1.36  О повторном обучении
    String getComment(ReEducationStuExtract extract, boolean isPrintOrder);

    // 1.37  О продлении академического отпуска
    String getComment(ProlongWeekendStuExtract extract, boolean isPrintOrder);

    // 1.38  О выходе из отпуска по уходу за ребенком
    String getComment(WeekendChildOutStuExtract extract, boolean isPrintOrder);

    // 1.39  О скидке по оплате за обучение
    String getComment(PaymentDiscountStuExtract extract, boolean isPrintOrder);

    // 1.40  О выдаче диплома о неполном высшем профессиональном образовании
    String getComment(GiveDiplIncompleteStuListExtract extract, boolean isPrintOrder);

    // 1.41  О досрочной сдаче сессии
    String getComment(PrescheduleSessionPassStuExtract extract, boolean isPrintOrder);

    // 1.42  О переносе сроков сдачи защиты дипломной работы
    String getComment(ChangePassDiplomaWorkPeriodStuExtract extract, boolean isPrintOrder);

    // 1.43  О переносе сроков сдачи государственного экзамена
    String getComment(ChangeDateStateExaminationStuExtract extract, boolean isPrintOrder);

    // 1.44  О повторной защите дипломной работы
    String getComment(RepeatProtectDiplomaWorkStuExtract extract, boolean isPrintOrder);

    // 1.45  О повторной сдаче государственного экзамена
    String getComment(RepeatPassStateExamStuExtract extract, boolean isPrintOrder);

    // 1.46  О свободном посещении занятий
    String getComment(FreeStudiesAttendanceStuExtract extract, boolean isPrintOrder);

    // 1.47  О назначении старостой учебной группы
    String getComment(AddGroupManagerStuExtract extract, boolean isPrintOrder);

    // 1.48  Об освобождении от обязанностей старосты учебной группы
    String getComment(RemoveGroupManagerStuExtract extract, boolean isPrintOrder);

    // 1.49  О дисциплинарном взыскании
    String getComment(DisciplinaryPenaltyStuExtract extract, boolean isPrintOrder);

    // 1.50  О наложении взыскания за утерю зачетной книжки
    String getComment(RecordBookLossPenaltyStuExtract extract, boolean isPrintOrder);

    // 1.51  О наложении взыскания за утерю студенческого билета
    String getComment(StudentCardLossPenaltyStuExtract extract, boolean isPrintOrder);

    // 1.52  О допуске к сдаче государственного экзамена
    String getComment(AdmitToStateExamsStuExtract extract, boolean isPrintOrder);

    // 1.53  О предоставлении каникул
    String getComment(HolidayStuExtract extract, boolean isPrintOrder);

    // 1.54  О переносе итоговой государственной аттестации
    String getComment(ChangePassStateExamsStuExtract extract, boolean isPrintOrder);

    // 1.55  Об установлении индивидуального срока сдачи сессии (продление сессии)
    String getComment(SessionIndividualStuExtract extract, boolean isPrintOrder);

    // 1.56  О переводе на индивидуальный план обучения
    String getComment(TransferIndPlanStuExtract extract, boolean isPrintOrder);

    // 1.57  О допуске к защите выпускной квалификационной работы (после ГЭ)
    String getComment(AdmitToDiplomaStuExtract extract, boolean isPrintOrder);

    // 1.58  О переводе на индивидуальный график обучения
    String getComment(TransferIndGraphStuExtract extract, boolean isPrintOrder);

    // 1.59  О предоставлении отпуска по уходу за ребенком до 1,5 лет
    String getComment(WeekendChildOneAndHalfStuExtract extract, boolean isPrintOrder);

    // 1.60  О предоставлении повторного года обучения с переходом на ФГОС
    String getComment(ReEducationFGOSStuExtract extract, boolean isPrintOrder);

    // 1.61  О восстановлении
    String getComment(RestorationCourseStuExtract extract, boolean isPrintOrder);

    // 1.62  Об отчислении (государственный экзамен)
    String getComment(ExcludeStateExamStuExtract extract, boolean isPrintOrder);

    // 1.63  Об отчислении (недопуск к ВКР)
    String getComment(ExcludeUnacceptedToGPDefenceStuExtract extract, boolean isPrintOrder);

    // 1.64  Об отчислении (ВКР)
    String getComment(ExcludeGPDefenceFailStuExtract extract, boolean isPrintOrder);

    // 1.65  О переводе из группы в группу
    String getComment(TransferGroupStuExtract extract, boolean isPrintOrder);

    // 1.66  О переводе с курса на следующий курс
    String getComment(TransferCourseStuExtract extract, boolean isPrintOrder);

    // 1.67  О переводе со специальности на специальность (с направления на направление)
    String getComment(TransferEduLevelStuExtract extract, boolean isPrintOrder);

    // 1.68  О переводе со специальности на специальность (с направления на направление) со сменой формирующего подразделения
    String getComment(TransferFormativeStuExtract extract, boolean isPrintOrder);

    // 1.69  О переводе со специальности на специальность (с направления на направление) со сменой территориального подразделения
    String getComment(TransferTerritorialStuExtract extract, boolean isPrintOrder);

    // 1.70  О переводе со специальности на специальность (с направления на направление) между территориальными подразделениями
    String getComment(TransferTerritorialExtStuExtract extract, boolean isPrintOrder);

    // 1.71  О переводе на другие условия освоения и срок освоения
    String getComment(TransferDevCondExtStuExtract extract, boolean isPrintOrder);

    // 1.72  О переводе на другие условия освоения
    String getComment(TransferDevCondStuExtract extract, boolean isPrintOrder);

    // 1.73  О восстановлении и повторном допуске к сдаче государственного экзамена (пересдача ГЭ)
    String getComment(RestorationAdmitStateExamsExtStuExtract extract, boolean isPrintOrder);

    // 1.74  О закреплении за специализацией (профилем) и перераспределении в группу
    String getComment(TransferProfileStuExtract extract, boolean isPrintOrder);

    // 1.75  О восстановлении и повторном допуске к сдаче государственного экзамена (неявка на ГЭ)
    String getComment(RestorationAdmitStateExamsStuExtract extract, boolean isPrintOrder);

    // 1.76  О переводе со специализации на специализацию (с профиля на профиль) и перераспределении в группу
    String getComment(TransferProfileGroupExtStuExtract extract, boolean isPrintOrder);

    // 1.77  О восстановлении и допуске к подготовке ВКР
    String getComment(RestorationAdmitToDiplomaStuExtract extract, boolean isPrintOrder);

    // 1.78  О переводе на вакантное бюджетное место
    String getComment(TransferCompTypeExtStuExtract extract, boolean isPrintOrder);

    // 1.79  О переводе со специализации на специализацию (с профиля на профиль)
    String getComment(TransferProfileExtStuExtract extract, boolean isPrintOrder);

    // 1.80  Об изменении категории обучаемого
    String getComment(ChangeCategoryStuExtract extract, boolean isPrintOrder);

    // 1.81  О восстановлении и допуске к защите ВКР (неявка на защиту)
    String getComment(RestorationAdmitToDiplomaDefStuExtract extract, boolean isPrintOrder);

    // 1.82  О пересдаче дисциплин учебного плана
    String getComment(RePassDiscStuExtract extract, boolean isPrintOrder);

    // 1.83  О восстановлении и допуске к защите ВКР (повторная защита)
    String getComment(RestorationAdmitToDiplomaDefExtStuExtract extract, boolean isPrintOrder);

    // 1.84  О пересдаче дисциплин учебного плана комиссии
    String getComment(RePassDiscComissStuExtract extract, boolean isPrintOrder);

    // 1.85  О присвоении степени, выдаче диплома и отчислении в связи с окончанием университета
    String getComment(GiveDiplomaStuExtract extract, boolean isPrintOrder);

    // 1.86  Об оказании материальной помощи
    String getComment(MatAidAssignStuExtract extract, boolean isPrintOrder);

    // 1.87  Об однократном повышении государственной академической стипендии
    String getComment(GrantRiseStuExtract extract, boolean isPrintOrder);

    // 1.88  О закреплении за специализацией (профилем)
    String getComment(ProfileAssignStuExtract extract, boolean isPrintOrder);

    // 1.89  О выходе из отпуска по беременности и родам
    String getComment(WeekendPregnancyOutStuExtract extract, boolean isPrintOrder);

    // 1.90  О выходе из отпуска по уходу за ребенком
    String getComment(WeekendChildCareOutStuExtract extract, boolean isPrintOrder);

    // 1.91  О назначении надбавки к государственной академической стипендии
    String getComment(AcadGrantBonusAssignStuExtract extract, boolean isPrintOrder);

    // 1.92  О переводе с курса на следующий курс со сменой территориального подразделения
    String getComment(TransferCourseWithChangeTerrOuStuExtract extract, boolean isPrintOrder);

    // 1.93  О распределении в группу
    String getComment(GroupAssignStuExtract extract, boolean isPrintOrder);

    // 1.94  О предоставлении отпуска по уходу за ребенком до 3 лет
    String getComment(WeekendChildThreeStuExtract extract, boolean isPrintOrder);

    // 1.95  Об установлении индивидуального срока сдачи сессии (досрочная сдача сессии)
    String getComment(SessionIndividualAheadStuExtract extract, boolean isPrintOrder);

    // 1.96  О допуске к защите выпускной квалификационной работы
    String getComment(AdmitToDiplomaWithoutExamStuExtract extract, boolean isPrintOrder);

    // 1.97  О приеме гражданства Российской Федерации
    String getComment(AdoptRussianCitizenshipStuExtract extract, boolean isPrintOrder);

    // 1.98  Об аттестации практик студентов на основании академической справки
    String getComment(AttestationPracticStuExtract extract, boolean isPrintOrder);

    // 1.99  О прекращении выплаты академической стипендии
    String getComment(AcadGrantPaymentStopStuExtract extract, boolean isPrintOrder);

    // 1.100  О прекращении выплаты социальной стипендии
    String getComment(SocGrantPaymentStopStuExtract extract, boolean isPrintOrder);

    // 1.101  О прекращении выплаты надбавки к государственной академической стипендии
    String getComment(AcadGrantBonusPaymentStopStuExtract extract, boolean isPrintOrder);

    // 1.102  Об аттестации практик студентов имеющих стаж работы по профилю обучения
    String getComment(AttestationPracticWithServiceRecStuExtract extract, boolean isPrintOrder);

    // 1.103  Об аттестации практик студентов, прошедших стажировку
    String getComment(AttestationPracticWithProbationStuExtract extract, boolean isPrintOrder);

    // 1.104  О переводе между территориальными подразделениями
    String getComment(TransferBetweenTerritorialStuExtract extract, boolean isPrintOrder);

    ///// Выписки из списочных приказов по студентам /////

    // 2.1.1  О переводе с курса на курс
    String getComment(CourseTransferStuListExtract extract, boolean isPrintOrder);

    // 2.1.2  Считать на прежнем курсе
    String getComment(CourseNoChangeStuListExtract extract, boolean isPrintOrder);

    // 2.1.3  О переводе с курса на курс студентов, имеющих задолженности
    String getComment(CourseTransferDebtorStuListExtract extract, boolean isPrintOrder);

    // 2.2.1  О выпуске студентов с отличием
    String getComment(GraduateSuccessStuListExtract extract, boolean isPrintOrder);

    // 2.2.2  О выпуске студентов
    String getComment(GraduateStuListExtract extract, boolean isPrintOrder);

    // 2.2.3  Об исключении студентов, не выполнивших учебный план
    String getComment(GradExcludeStuListExtract extract, boolean isPrintOrder);

    // 2.4.1  Об отчислении (Вариант 1)
    String getComment(ExcludeStuListExtract extract, boolean isPrintOrder);

    // 2.5.1  О допуске к государственной итоговой аттестации
    String getComment(AdmitToStateExamsStuListExtract extract, boolean isPrintOrder);

    // 2.6.1  О выдаче диплома и присвоении квалификации (Вариант 1)
    String getComment(GiveDiplStuListExtract extract, boolean isPrintOrder);

    // 2.6.2  О выдаче диплома с отличием и присвоении квалификации
    String getComment(GiveDiplSuccessStuListExtract extract, boolean isPrintOrder);

    // 2.7.1  О предоставлении каникул
    String getComment(HolidayStuListExtract extract, boolean isPrintOrder);

    // 2.8.1  Об отчислении для одной группы, диплом с отличием (Вариант 2)
    String getComment(ExcludeSingGrpSuccStuListExtract extract, boolean isPrintOrder);

    // 2.8.2  Об отчислении для одной группы (Вариант 2)
    String getComment(ExcludeSingGrpStuListExtract extract, boolean isPrintOrder);

    // 2.9.1  Об отчислении для одной группы (кафедральный)
    String getComment(ExcludeSingGrpCathStuListExtract extract, boolean isPrintOrder);

    // 2.10.1  О разделении студентов по специализациям
    String getComment(SplitStudentsStuListExtract extract, boolean isPrintOrder);

    // 2.11.1  Об изменении условия освоения
    String getComment(TransferDevConditionStuListExtract extract, boolean isPrintOrder);

    // 2.12.1  О переводе студентов территориального подразделения
    String getComment(TerritorialTransferStuListExtract extract, boolean isPrintOrder);

    // 2.17.1  О допуске к защите дипломной работы
    String getComment(AdmitToPassDiplomaWorkStuListExtract extract, boolean isPrintOrder);

    // 2.20.1  О назначении рецензентов
    String getComment(SetReviewerStuListExtract extract, boolean isPrintOrder);

    // 2.21.1  О смене научных руководителей
    String getComment(ChangeScientificAdviserExtract extract, boolean isPrintOrder);

    // 2.22.1  О смене рецензентов
    String getComment(ChangeReviewerStuListExtract extract, boolean isPrintOrder);

    // 2.23.1  Об изменении темы дипломный работы
    String getComment(ChangeDiplomaWorkTopicStuListExtract extract, boolean isPrintOrder);

    // 2.24.1  Об утверждении тем дипломных работ и назначении научных руководителей
    String getComment(SetDiplomaWorkTopicAndScientificAdviserStuListExtract extract, boolean isPrintOrder);

    // 2.25.1  О закреплении за специализациями (профилями) и перераспределении в группу
    String getComment(SplitStudentsGroupStuListExtract extract, boolean isPrintOrder);

    // 2.26.1  О допуске к сдаче государственного экзамена
    String getComment(AdmitToStateExamsExtStuListExtract extract, boolean isPrintOrder);

    // 2.27.1  О допуске к защите выпускной квалификационной работы
    String getComment(AdmitToPassDiplomaWorkExtStuListExtract extract, boolean isPrintOrder);

    // 2.28.1  О назначении старостами учебных групп
    String getComment(AddGroupManagerStuListExtract extract, boolean isPrintOrder);

    // 2.29.1  О выдаче диплома и присвоении квалификации (Вариант 2)
    String getComment(GiveDiplExtStuListExtract extract, boolean isPrintOrder);

    // 2.29.2  О выдаче диплома с отличием и присвоении квалификации (Вариант 2)
    String getComment(GiveDiplSuccessExtStuListExtract extract, boolean isPrintOrder);

    // 2.30.1  О переводе с курса на следующий курс (по направлениям подготовки)
    String getComment(CourseTransferExtStuListExtract extract, boolean isPrintOrder);

    // 2.31.1  О назначении академической стипендии
    String getComment(AcadGrantAssignStuListExtract extract, boolean isPrintOrder);

    // 2.32.1  О назначении социальной стипендии
    String getComment(SocGrantAssignStuListExtract extract, boolean isPrintOrder);

    // 2.33.1  Об оказании материальной помощи
    String getComment(FinAidAssignStuListExtract extract, boolean isPrintOrder);

    // 2.34.1  О распределении студентов 1 курса в группы
    String getComment(SplitFirstCourseStudentsGroupStuListExtract extract, boolean isPrintOrder);

    // 2.35.1  Об однократном повышении государственной академической стипендии
    String getComment(GrantRiseStuListExtract extract, boolean isPrintOrder);

    // 2.36.1  О переводе с курса на следующий курс (по направлениям подготовки) со сменой территориального подразделения
    String getComment(TerritorialCourseTransferStuListExtract extract, boolean isPrintOrder);

    // 2.37.1  О закреплении за специализациями (профилями) (Вариант 2)
    String getComment(SplitStudentsExtStuListExtract extract, boolean isPrintOrder);

    // 2.38.1  О назначении надбавки к государственной академической стипендии
    String getComment(AcadGrantBonusAssignStuListExtract extract, boolean isPrintOrder);

    // 2.39.1  О переводе из группы в группу
    String getComment(GroupTransferExtStuListExtract extract, boolean isPrintOrder);

    // 2.40.1  О направлении на практику в пределах ОУ
    String getComment(SendPracticeInnerStuListExtract extract, boolean isPrintOrder);

    // 2.41.1  О переводе студентов территориального подразделения (расширенный) (Вариант 2)
    String getComment(TerritorialTransferExtStuListExtract extract, boolean isPrintOrder);

    //2.43.1 О переводе со специализации на специализацию (с профиля на профиль)
    String getComment(ChangeProfileStuListExtract extract, boolean isPrintOrder);

    //2.44.1 О переводе со специализации на специализацию (с профиля на профиль) и перераспределении в группу
    String getComment(ChangeProfileAndGroupStuListExtract extract, boolean isPrintOrder);

    // Базовый метод для вывода даты и номера приказа
    String getDefaultOrder(AbstractStudentExtract extract, boolean isPrintOrder, boolean isComma);
}