/* $Id$ */
package ru.tandemservice.movestudent.component.movestudentScriptItem.MovestudentScriptItemPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogPub.DefaultScriptCatalogPubDAO;

/**
 * @author Vasily Zhukov
 * @since 22.12.2011
 */
public class DAO<T extends ICatalogItem & IScriptItem, Model extends ru.tandemservice.movestudent.component.movestudentScriptItem.MovestudentScriptItemPub.Model<T>> extends DefaultScriptCatalogPubDAO<T, Model> implements IDAO<T, Model>
{
}