package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.SessionIndividualStuExtract;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об установлении индивидуального срока сдачи сессии(продление сессии)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionIndividualStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.SessionIndividualStuExtract";
    public static final String ENTITY_NAME = "sessionIndividualStuExtract";
    public static final int VERSION_HASH = -1025814400;
    private static IEntityMeta ENTITY_META;

    public static final String P_DEADLINE_DATE = "deadlineDate";
    public static final String L_SESSION_TYPE = "sessionType";

    private Date _deadlineDate;     // Срок сдачи
    private YearDistributionPart _sessionType;     // Вид сессии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Срок сдачи. Свойство не может быть null.
     */
    @NotNull
    public Date getDeadlineDate()
    {
        return _deadlineDate;
    }

    /**
     * @param deadlineDate Срок сдачи. Свойство не может быть null.
     */
    public void setDeadlineDate(Date deadlineDate)
    {
        dirty(_deadlineDate, deadlineDate);
        _deadlineDate = deadlineDate;
    }

    /**
     * @return Вид сессии. Свойство не может быть null.
     */
    @NotNull
    public YearDistributionPart getSessionType()
    {
        return _sessionType;
    }

    /**
     * @param sessionType Вид сессии. Свойство не может быть null.
     */
    public void setSessionType(YearDistributionPart sessionType)
    {
        dirty(_sessionType, sessionType);
        _sessionType = sessionType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionIndividualStuExtractGen)
        {
            setDeadlineDate(((SessionIndividualStuExtract)another).getDeadlineDate());
            setSessionType(((SessionIndividualStuExtract)another).getSessionType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionIndividualStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionIndividualStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new SessionIndividualStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "deadlineDate":
                    return obj.getDeadlineDate();
                case "sessionType":
                    return obj.getSessionType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "deadlineDate":
                    obj.setDeadlineDate((Date) value);
                    return;
                case "sessionType":
                    obj.setSessionType((YearDistributionPart) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "deadlineDate":
                        return true;
                case "sessionType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "deadlineDate":
                    return true;
                case "sessionType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "deadlineDate":
                    return Date.class;
                case "sessionType":
                    return YearDistributionPart.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionIndividualStuExtract> _dslPath = new Path<SessionIndividualStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionIndividualStuExtract");
    }
            

    /**
     * @return Срок сдачи. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionIndividualStuExtract#getDeadlineDate()
     */
    public static PropertyPath<Date> deadlineDate()
    {
        return _dslPath.deadlineDate();
    }

    /**
     * @return Вид сессии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionIndividualStuExtract#getSessionType()
     */
    public static YearDistributionPart.Path<YearDistributionPart> sessionType()
    {
        return _dslPath.sessionType();
    }

    public static class Path<E extends SessionIndividualStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _deadlineDate;
        private YearDistributionPart.Path<YearDistributionPart> _sessionType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Срок сдачи. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionIndividualStuExtract#getDeadlineDate()
     */
        public PropertyPath<Date> deadlineDate()
        {
            if(_deadlineDate == null )
                _deadlineDate = new PropertyPath<Date>(SessionIndividualStuExtractGen.P_DEADLINE_DATE, this);
            return _deadlineDate;
        }

    /**
     * @return Вид сессии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionIndividualStuExtract#getSessionType()
     */
        public YearDistributionPart.Path<YearDistributionPart> sessionType()
        {
            if(_sessionType == null )
                _sessionType = new YearDistributionPart.Path<YearDistributionPart>(L_SESSION_TYPE, this);
            return _sessionType;
        }

        public Class getEntityClass()
        {
            return SessionIndividualStuExtract.class;
        }

        public String getEntityName()
        {
            return "sessionIndividualStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
