/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.unimove.entity.ICustomOrderAction;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public interface IBaseCustomActionPubDAO<T extends ICustomOrderAction, Model extends BaseCustomActionPubModel<T>> extends IUniDao<Model>
{
}