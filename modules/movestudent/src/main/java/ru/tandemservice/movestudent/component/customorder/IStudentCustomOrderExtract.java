/* $Id$ */
package ru.tandemservice.movestudent.component.customorder;

import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.custom.OrderActionType;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.ICustomOrderExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 10.10.2013
 */
public interface IStudentCustomOrderExtract extends ICustomOrderExtract
{
    OrderActionType getOrderActionType();

    void setOrderActionType(OrderActionType orderActionType);

    AbstractStudentExtract getModifiableExtract();

    void setModifiableExtract(AbstractStudentExtract modifiableExtract);

    AbstractStudentParagraph getParagraph();

    StudentExtractType getType();

    ExtractStates getState();

    DatabaseFile getUserPrintForm();

    void setUserPrintForm(DatabaseFile userPrintForm);
}