/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeEducationOrgUnit.Pub;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.BaseCustomActionPubDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeEducationOrgUnitAction;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class DAO extends BaseCustomActionPubDAO<ChangeEducationOrgUnitAction, Model>
{
    @Override
    protected Class<ChangeEducationOrgUnitAction> getActionClass()
    {
        return ChangeEducationOrgUnitAction.class;
    }
}