package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О переводе студентов территориального подразделения (расширенный)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TerritorialTransferExtStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract";
    public static final String ENTITY_NAME = "territorialTransferExtStuListExtract";
    public static final int VERSION_HASH = -265676028;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP_NEW = "groupNew";
    public static final String L_GROUP_OLD = "groupOld";
    public static final String L_COMPENSATION_TYPE_NEW = "compensationTypeNew";
    public static final String L_COMPENSATION_TYPE_OLD = "compensationTypeOld";
    public static final String L_EDUCATION_ORG_UNIT_NEW = "educationOrgUnitNew";
    public static final String L_EDUCATION_ORG_UNIT_OLD = "educationOrgUnitOld";
    public static final String P_CHANGE_EDU_PLAN = "changeEduPlan";
    public static final String P_CHANGE_EDU_PLAN_DEADLINE_DATE = "changeEduPlanDeadlineDate";

    private Course _course;     // Курс
    private Group _groupNew;     // Новая группа
    private Group _groupOld;     // Старая группа
    private CompensationType _compensationTypeNew;     // Новый вид возмещения затрат
    private CompensationType _compensationTypeOld;     // Старый вид возмещения затрат
    private EducationOrgUnit _educationOrgUnitNew;     // Новое направление подготовки
    private EducationOrgUnit _educationOrgUnitOld;     // Старое направление подготовки
    private boolean _changeEduPlan;     // Разница в учебных планах
    private Date _changeEduPlanDeadlineDate;     // Дата ликвидации

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroupNew()
    {
        return _groupNew;
    }

    /**
     * @param groupNew Новая группа. Свойство не может быть null.
     */
    public void setGroupNew(Group groupNew)
    {
        dirty(_groupNew, groupNew);
        _groupNew = groupNew;
    }

    /**
     * @return Старая группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroupOld()
    {
        return _groupOld;
    }

    /**
     * @param groupOld Старая группа. Свойство не может быть null.
     */
    public void setGroupOld(Group groupOld)
    {
        dirty(_groupOld, groupOld);
        _groupOld = groupOld;
    }

    /**
     * @return Новый вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationTypeNew()
    {
        return _compensationTypeNew;
    }

    /**
     * @param compensationTypeNew Новый вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationTypeNew(CompensationType compensationTypeNew)
    {
        dirty(_compensationTypeNew, compensationTypeNew);
        _compensationTypeNew = compensationTypeNew;
    }

    /**
     * @return Старый вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationTypeOld()
    {
        return _compensationTypeOld;
    }

    /**
     * @param compensationTypeOld Старый вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationTypeOld(CompensationType compensationTypeOld)
    {
        dirty(_compensationTypeOld, compensationTypeOld);
        _compensationTypeOld = compensationTypeOld;
    }

    /**
     * @return Новое направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitNew()
    {
        return _educationOrgUnitNew;
    }

    /**
     * @param educationOrgUnitNew Новое направление подготовки. Свойство не может быть null.
     */
    public void setEducationOrgUnitNew(EducationOrgUnit educationOrgUnitNew)
    {
        dirty(_educationOrgUnitNew, educationOrgUnitNew);
        _educationOrgUnitNew = educationOrgUnitNew;
    }

    /**
     * @return Старое направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitOld()
    {
        return _educationOrgUnitOld;
    }

    /**
     * @param educationOrgUnitOld Старое направление подготовки. Свойство не может быть null.
     */
    public void setEducationOrgUnitOld(EducationOrgUnit educationOrgUnitOld)
    {
        dirty(_educationOrgUnitOld, educationOrgUnitOld);
        _educationOrgUnitOld = educationOrgUnitOld;
    }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     */
    @NotNull
    public boolean isChangeEduPlan()
    {
        return _changeEduPlan;
    }

    /**
     * @param changeEduPlan Разница в учебных планах. Свойство не может быть null.
     */
    public void setChangeEduPlan(boolean changeEduPlan)
    {
        dirty(_changeEduPlan, changeEduPlan);
        _changeEduPlan = changeEduPlan;
    }

    /**
     * @return Дата ликвидации.
     */
    public Date getChangeEduPlanDeadlineDate()
    {
        return _changeEduPlanDeadlineDate;
    }

    /**
     * @param changeEduPlanDeadlineDate Дата ликвидации.
     */
    public void setChangeEduPlanDeadlineDate(Date changeEduPlanDeadlineDate)
    {
        dirty(_changeEduPlanDeadlineDate, changeEduPlanDeadlineDate);
        _changeEduPlanDeadlineDate = changeEduPlanDeadlineDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TerritorialTransferExtStuListExtractGen)
        {
            setCourse(((TerritorialTransferExtStuListExtract)another).getCourse());
            setGroupNew(((TerritorialTransferExtStuListExtract)another).getGroupNew());
            setGroupOld(((TerritorialTransferExtStuListExtract)another).getGroupOld());
            setCompensationTypeNew(((TerritorialTransferExtStuListExtract)another).getCompensationTypeNew());
            setCompensationTypeOld(((TerritorialTransferExtStuListExtract)another).getCompensationTypeOld());
            setEducationOrgUnitNew(((TerritorialTransferExtStuListExtract)another).getEducationOrgUnitNew());
            setEducationOrgUnitOld(((TerritorialTransferExtStuListExtract)another).getEducationOrgUnitOld());
            setChangeEduPlan(((TerritorialTransferExtStuListExtract)another).isChangeEduPlan());
            setChangeEduPlanDeadlineDate(((TerritorialTransferExtStuListExtract)another).getChangeEduPlanDeadlineDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TerritorialTransferExtStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TerritorialTransferExtStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new TerritorialTransferExtStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "groupNew":
                    return obj.getGroupNew();
                case "groupOld":
                    return obj.getGroupOld();
                case "compensationTypeNew":
                    return obj.getCompensationTypeNew();
                case "compensationTypeOld":
                    return obj.getCompensationTypeOld();
                case "educationOrgUnitNew":
                    return obj.getEducationOrgUnitNew();
                case "educationOrgUnitOld":
                    return obj.getEducationOrgUnitOld();
                case "changeEduPlan":
                    return obj.isChangeEduPlan();
                case "changeEduPlanDeadlineDate":
                    return obj.getChangeEduPlanDeadlineDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "groupNew":
                    obj.setGroupNew((Group) value);
                    return;
                case "groupOld":
                    obj.setGroupOld((Group) value);
                    return;
                case "compensationTypeNew":
                    obj.setCompensationTypeNew((CompensationType) value);
                    return;
                case "compensationTypeOld":
                    obj.setCompensationTypeOld((CompensationType) value);
                    return;
                case "educationOrgUnitNew":
                    obj.setEducationOrgUnitNew((EducationOrgUnit) value);
                    return;
                case "educationOrgUnitOld":
                    obj.setEducationOrgUnitOld((EducationOrgUnit) value);
                    return;
                case "changeEduPlan":
                    obj.setChangeEduPlan((Boolean) value);
                    return;
                case "changeEduPlanDeadlineDate":
                    obj.setChangeEduPlanDeadlineDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "groupNew":
                        return true;
                case "groupOld":
                        return true;
                case "compensationTypeNew":
                        return true;
                case "compensationTypeOld":
                        return true;
                case "educationOrgUnitNew":
                        return true;
                case "educationOrgUnitOld":
                        return true;
                case "changeEduPlan":
                        return true;
                case "changeEduPlanDeadlineDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "groupNew":
                    return true;
                case "groupOld":
                    return true;
                case "compensationTypeNew":
                    return true;
                case "compensationTypeOld":
                    return true;
                case "educationOrgUnitNew":
                    return true;
                case "educationOrgUnitOld":
                    return true;
                case "changeEduPlan":
                    return true;
                case "changeEduPlanDeadlineDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "groupNew":
                    return Group.class;
                case "groupOld":
                    return Group.class;
                case "compensationTypeNew":
                    return CompensationType.class;
                case "compensationTypeOld":
                    return CompensationType.class;
                case "educationOrgUnitNew":
                    return EducationOrgUnit.class;
                case "educationOrgUnitOld":
                    return EducationOrgUnit.class;
                case "changeEduPlan":
                    return Boolean.class;
                case "changeEduPlanDeadlineDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TerritorialTransferExtStuListExtract> _dslPath = new Path<TerritorialTransferExtStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TerritorialTransferExtStuListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract#getGroupNew()
     */
    public static Group.Path<Group> groupNew()
    {
        return _dslPath.groupNew();
    }

    /**
     * @return Старая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract#getGroupOld()
     */
    public static Group.Path<Group> groupOld()
    {
        return _dslPath.groupOld();
    }

    /**
     * @return Новый вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract#getCompensationTypeNew()
     */
    public static CompensationType.Path<CompensationType> compensationTypeNew()
    {
        return _dslPath.compensationTypeNew();
    }

    /**
     * @return Старый вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract#getCompensationTypeOld()
     */
    public static CompensationType.Path<CompensationType> compensationTypeOld()
    {
        return _dslPath.compensationTypeOld();
    }

    /**
     * @return Новое направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract#getEducationOrgUnitNew()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
    {
        return _dslPath.educationOrgUnitNew();
    }

    /**
     * @return Старое направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract#getEducationOrgUnitOld()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
    {
        return _dslPath.educationOrgUnitOld();
    }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract#isChangeEduPlan()
     */
    public static PropertyPath<Boolean> changeEduPlan()
    {
        return _dslPath.changeEduPlan();
    }

    /**
     * @return Дата ликвидации.
     * @see ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract#getChangeEduPlanDeadlineDate()
     */
    public static PropertyPath<Date> changeEduPlanDeadlineDate()
    {
        return _dslPath.changeEduPlanDeadlineDate();
    }

    public static class Path<E extends TerritorialTransferExtStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _groupNew;
        private Group.Path<Group> _groupOld;
        private CompensationType.Path<CompensationType> _compensationTypeNew;
        private CompensationType.Path<CompensationType> _compensationTypeOld;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitNew;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitOld;
        private PropertyPath<Boolean> _changeEduPlan;
        private PropertyPath<Date> _changeEduPlanDeadlineDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract#getGroupNew()
     */
        public Group.Path<Group> groupNew()
        {
            if(_groupNew == null )
                _groupNew = new Group.Path<Group>(L_GROUP_NEW, this);
            return _groupNew;
        }

    /**
     * @return Старая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract#getGroupOld()
     */
        public Group.Path<Group> groupOld()
        {
            if(_groupOld == null )
                _groupOld = new Group.Path<Group>(L_GROUP_OLD, this);
            return _groupOld;
        }

    /**
     * @return Новый вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract#getCompensationTypeNew()
     */
        public CompensationType.Path<CompensationType> compensationTypeNew()
        {
            if(_compensationTypeNew == null )
                _compensationTypeNew = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE_NEW, this);
            return _compensationTypeNew;
        }

    /**
     * @return Старый вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract#getCompensationTypeOld()
     */
        public CompensationType.Path<CompensationType> compensationTypeOld()
        {
            if(_compensationTypeOld == null )
                _compensationTypeOld = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE_OLD, this);
            return _compensationTypeOld;
        }

    /**
     * @return Новое направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract#getEducationOrgUnitNew()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
        {
            if(_educationOrgUnitNew == null )
                _educationOrgUnitNew = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_NEW, this);
            return _educationOrgUnitNew;
        }

    /**
     * @return Старое направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract#getEducationOrgUnitOld()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
        {
            if(_educationOrgUnitOld == null )
                _educationOrgUnitOld = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_OLD, this);
            return _educationOrgUnitOld;
        }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract#isChangeEduPlan()
     */
        public PropertyPath<Boolean> changeEduPlan()
        {
            if(_changeEduPlan == null )
                _changeEduPlan = new PropertyPath<Boolean>(TerritorialTransferExtStuListExtractGen.P_CHANGE_EDU_PLAN, this);
            return _changeEduPlan;
        }

    /**
     * @return Дата ликвидации.
     * @see ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract#getChangeEduPlanDeadlineDate()
     */
        public PropertyPath<Date> changeEduPlanDeadlineDate()
        {
            if(_changeEduPlanDeadlineDate == null )
                _changeEduPlanDeadlineDate = new PropertyPath<Date>(TerritorialTransferExtStuListExtractGen.P_CHANGE_EDU_PLAN_DEADLINE_DATE, this);
            return _changeEduPlanDeadlineDate;
        }

        public Class getEntityClass()
        {
            return TerritorialTransferExtStuListExtract.class;
        }

        public String getEntityName()
        {
            return "territorialTransferExtStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
