/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.component.commons;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.EducationLevelDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

/**
 * @author vip_delete
 * @since 01.11.2008
 */
public class MoveStudentColumns
{
    public static final String COMMIT_DATE_COLUMN = "commitDate";
    public static final String COMMIT_DATE_SYSTEM_COLUMN = "commitDateSystem";
    public static final String CREATE_DATE_COLUMN = "createDate";

    public static AbstractColumn getCreateDateColumn()
    {
        return new SimpleColumn(IAbstractExtract.P_CREATE_DATE, "Дата формирования", IAbstractExtract.P_CREATE_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME);
    }

    public static AbstractColumn getFullFioColumn()
    {
        return new SimpleColumn("ФИО студента", MoveStudentDefines.STUDENT_FULL_FIO).setClickable(false);
    }

    public static AbstractColumn getFullFioPublisherLinkColumn()
    {
        return new PublisherLinkColumn("ФИО студента", MoveStudentDefines.STUDENT_FULL_FIO).setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                ModularStudentExtract extract = (ModularStudentExtract) ((ViewWrapper) entity).getEntity();
                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, extract.getEntity().getId()).add("selectedStudentTab", "studentTab");
            }
        });
    }

    public static AbstractColumn getFormativeOrgUnitColumn()
    {
        return new SimpleColumn("Формирующее подр.", MoveStudentDefines.STUDENT_EXTRACT_FORMATIVE_ORG_UNIT_FULL_TITLE).setClickable(false);
    }

    public static AbstractColumn getTerritorialOrgUnitColumn()
    {
        return new SimpleColumn("Территориальное подр.", MoveStudentDefines.STUDENT_EXTRACT_TERRITORIAL_ORG_UNIT_FULL_TITLE).setClickable(false);
    }

    public static AbstractColumn getProducingOrgUnitColumn()
    {
        return new SimpleColumn("Выпускающее подр.", MoveStudentDefines.STUDENT_EXTRACT_PRODUCING_ORG_UNIT_FULL_TITLE).setClickable(false);
    }

    public static void addEduLevelTitleColumns(DynamicListDataSource ds) {

        // После DEV-9009 вместо одной колонки с fullTitle НПв выводим 2 колонки с направлением и направленность из нового перечня
        if (!EducationLevelsHighSchool.P_FULL_TITLE.equals(EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY)) {
            // Но для ДВФУ пока оставляем еще старый вариант колонки - у них там fullTitleExtended (уже и направление, и направленность)
            ds.addColumn(new SimpleColumn(EducationOrgUnit.EDU_LVL_HS_COLUMN_TITLE, MoveStudentDefines.STUDENT_EXTRACT_EDUCATION_ORG_UNIT_DISPLAYABLE_TITLE).setClickable(false));
        }
        ds.addColumn(new SimpleColumn("Направление подготовки (специальность)", AbstractStudentExtract.entity().educationOrgUnit().educationLevelHighSchool().educationLevel().programSubjectWithCodeIndexAndGenTitle().s()).setClickable(false));
        ds.addColumn(new SimpleColumn("Направленность", AbstractStudentExtract.entity().educationOrgUnit().educationLevelHighSchool().educationLevel().programSpecializationTitle().s()).setClickable(false));
    }

    public static AbstractColumn getCourseColumn()
    {
        return new SimpleColumn("Курс", new String[]{IAbstractExtract.L_ENTITY, Student.L_COURSE, Course.P_TITLE}).setClickable(false);
    }

    public static AbstractColumn getGroupColumn()
    {
        return new SimpleColumn("Группа", new String[]{IAbstractExtract.L_ENTITY, Student.L_GROUP, Group.P_TITLE}).setClickable(false);
    }

    public static AbstractColumn getDevelopFormColumn()
    {
        return new SimpleColumn("Форма освоения", new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_DEVELOP_FORM, DevelopForm.P_TITLE}).setClickable(false);
    }

    public static AbstractColumn getCompensationTypeColumn()
    {
        return new SimpleColumn("Контракт", new String[]{IAbstractExtract.L_ENTITY, Student.L_COMPENSATION_TYPE, CompensationType.P_SHORT_TITLE}).setClickable(false);
    }

    public static AbstractColumn getExtractOrderNumberColumn()
    {
        return new SimpleColumn("№ приказа", new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, IAbstractOrder.P_NUMBER}).setClickable(false);
    }

    public static AbstractColumn getExtractOrderTypeColumn()
    {
        if (UserContext.getInstance().getPrincipalContext().isAdmin())
            return new SimpleColumn("Тип приказа", new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, StudentListOrder.L_TYPE, StudentExtractType.P_TITLE_WITH_CODE}).setClickable(false).setOrderable(false);
        return new SimpleColumn("Тип приказа", new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, StudentListOrder.L_TYPE, StudentExtractType.P_TITLE}).setClickable(false).setOrderable(false);
    }

    public static AbstractColumn getExtractOrderStateColumn()
    {
        return new SimpleColumn("Состояние приказа", new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, IAbstractOrder.L_STATE, OrderStates.P_TITLE}).setClickable(false);
    }

    public static AbstractColumn getParagraphNumberColumn()
    {
        return new SimpleColumn("№ параграфа", new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.P_NUMBER}).setClickable(false).setOrderable(false);
    }

    public static AbstractColumn getExtractCommitDateColumn()
    {
        return new SimpleColumn(COMMIT_DATE_COLUMN, "Дата приказа", new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, AbstractStudentOrder.P_COMMIT_DATE}, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false);
    }

    public static AbstractColumn getExtractCreateDateColumn()
    {
        return new SimpleColumn(CREATE_DATE_COLUMN, "Дата формирования", new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, IAbstractOrder.P_CREATE_DATE}, DateFormatter.DATE_FORMATTER_WITH_TIME);
    }

    public static AbstractColumn getExtractCommitDateSystemColumn()
    {
        return new SimpleColumn(COMMIT_DATE_SYSTEM_COLUMN, "Дата проведения", new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, IAbstractOrder.P_COMMIT_DATE_SYSTEM}, DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false);
    }

    public static AbstractColumn getExtractTypeColumn(String title)
    {
        if (UserContext.getInstance().getPrincipalContext().isAdmin())
            return new SimpleColumn(title, new String[]{IAbstractExtract.L_TYPE, StudentExtractType.P_TITLE_WITH_CODE}).setClickable(false);
        return new SimpleColumn(title, new String[]{IAbstractExtract.L_TYPE, ICatalogItem.CATALOG_ITEM_TITLE}).setClickable(false);
    }

    public static AbstractColumn getModularExtractReasonColumn(String title)
    {
        return new SimpleColumn(title, new String[]{ModularStudentExtract.P_REASON_FULL_TITLE}).setClickable(false).setOrderable(false);
    }

    public static AbstractColumn getListExtractReasonColumn(String title)
    {
        return new SimpleColumn(title, new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, StudentListOrder.P_REASON_FULL_TITLE}).setClickable(false).setOrderable(false);
    }

    public static AbstractColumn getExtractStateColumn(String title)
    {
        return new SimpleColumn(title, new String[]{IAbstractExtract.L_STATE, ICatalogItem.CATALOG_ITEM_TITLE}).setClickable(false);
    }

    public static AbstractColumn getExtractPrintColumn(Object handler, String permissionKey)
    {
        return getExtractPrintColumn(handler, permissionKey, "Печать");
    }

    public static AbstractColumn getExtractPrintPdfColumn(Object handler, String permissionKey)
    {
        return getExtractPrintPdfColumn(handler, permissionKey, "Печать PDF");
    }

    public static AbstractColumn getExtractPrintColumn(Object handler, String permissionKey, String title)
    {
        return CommonBaseUtil.getPrintColumn(handler.getClass().getPackage().getName() + ":onClickExtractPrint", title).setPermissionKey(permissionKey);
    }

    public static AbstractColumn getExtractPrintPdfColumn(Object handler, String permissionKey, String title)
    {
        return CommonBaseUtil.getPrintPdfColumn(handler.getClass().getPackage().getName() + ":onClickExtractPrintPdf", title).setPermissionKey(permissionKey);
    }

    public static AbstractColumn getOrderPrintColumn(Object handler, String permissionKey, String title)
    {
        final IndicatorColumn column = new IndicatorColumn("orderPrint", title, null, handler.getClass().getPackage().getName() + ":onClickOrderPrint", (String) null);
        column.setOrderable(false);
        column.setImageHeader(false);
        column.defaultIndicator(new IndicatorColumn.Item("printer", title));
        column.setDisableSecondSubmit(false);
        return column.setPermissionKey(permissionKey);
    }

    public static AbstractColumn getOrderPrintPdfColumn(Object handler, String permissionKey, String title)
    {
        final IndicatorColumn column = new IndicatorColumn("orderPrintPdf", title, null, handler.getClass().getPackage().getName() + ":onClickOrderPrintPdf", (String) null);
        column.setOrderable(false);
        column.setImageHeader(false);
        column.defaultIndicator(new IndicatorColumn.Item("printer_pdf", title));
        column.setDisableSecondSubmit(false);
        return column.setPermissionKey(permissionKey);
    }

    public static AbstractColumn getExtractEditColumn(Object handler, String permissionKey)
    {
        return new ActionColumn("Редактировать", ActionColumn.EDIT, handler.getClass().getPackage().getName() + ":onClickExtractEdit").setPermissionKey(permissionKey).setDisabledProperty(AbstractStudentExtract.P_NO_EDIT);
    }

    public static AbstractColumn getExtractDeleteColumn(Object handler, String permissionKey)
    {
        return new ActionColumn("Удалить", ActionColumn.DELETE, handler.getClass().getPackage().getName() + ":onClickExtractDelete", "Удалить «{0}»?", IAbstractExtract.P_TITLE).setPermissionKey(permissionKey).setDisabledProperty(AbstractStudentExtract.P_NO_DELETE);
    }

    public static AbstractColumn getIndividualOrderPrintColumn(Object handler, String permissionKey)
    {
        return CommonBaseUtil.getPrintColumn(handler.getClass().getPackage().getName() + ":onClickIndividualOrderPrint", "Печать").setPermissionKey(permissionKey);
    }

    public static AbstractColumn getIndividualOrderPrintPdfColumn(Object handler, String permissionKey)
    {
        return CommonBaseUtil.getPrintPdfColumn(handler.getClass().getPackage().getName() + ":onClickIndividualOrderPrintPdf", "Печать PDF").setPermissionKey(permissionKey);
    }

    public static AbstractColumn getIndividualOrderEditColumn(Object handler, String permissionKey)
    {
        return new ActionColumn("Редактировать", ActionColumn.EDIT, handler.getClass().getPackage().getName() + ":onClickIndividualOrderEdit").setPermissionKey(permissionKey)
                .setDisabledProperty(AbstractStudentExtract.paragraph().order().s() + "." + StudentModularOrder.P_READONLY);
    }

    public static AbstractColumn getIndividualOrderDeleteColumn(Object handler, String permissionKey)
    {
        return new ActionColumn("Удалить", ActionColumn.DELETE, handler.getClass().getPackage().getName() + ":onClickIndividualOrderDelete", "Удалить «{0}»?", ModularStudentExtract.P_INDIVIDUAL_ORDER_TITLE)
                .setPermissionKey(permissionKey).setDisabledProperty(AbstractStudentExtract.paragraph().order().s() + "." + AbstractStudentOrder.P_NO_DELETE);
    }

    public static AbstractColumn getOtherOrderEditColumn(Object handler, String permissionKey)
    {
        return new ActionColumn("Редактировать", ActionColumn.EDIT, handler.getClass().getPackage().getName() + ":onClickOtherOrderEdit").setPermissionKey(permissionKey);
    }

    public static AbstractColumn getOtherOrderPrintColumn(Object handler, String permissionKey)
    {
        return CommonBaseUtil.getPrintColumn(handler.getClass().getPackage().getName() + ":onClickOtherOrderPrint", "Печать").setPermissionKey(permissionKey);
    }

    public static AbstractColumn getOtherOrderDeleteColumn(Object handler, String permissionKey)
    {
        return new ActionColumn("Удалить", ActionColumn.DELETE, handler.getClass().getPackage().getName() + ":onClickOtherOrderDelete", "Удалить «{0}»?", IAbstractExtract.P_TITLE).setPermissionKey(permissionKey);
    }

    // composite

    public static AbstractColumn getModularExtractNumberColumn()
    {
        return new SimpleColumn("№ пункта приказа", new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.P_NUMBER}).setClickable(false).setOrderable(false);
    }

    public static AbstractColumn getListExtractNumberColumn()
    {
        return new SimpleColumn("№ пункта в параграфе", new String[]{IAbstractExtract.P_NUMBER}).setClickable(false).setOrderable(false);
    }

    public static AbstractColumn getExecutorColumn()
    {
        return new SimpleColumn("Исполнитель", new String[]{AbstractStudentExtract.L_EXECUTOR, EmployeePost.P_TITLE_WITH_POST}).setClickable(false).setOrderable(false);
    }
}
