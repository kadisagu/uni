package ru.tandemservice.movestudent.entity.custom.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.custom.ChangeCompensationTypeAction;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.unimove.entity.ICustomOrderAction;
import ru.tandemservice.unimove.entity.ICustomOrderExtract;
import ru.tandemservice.unimove.entity.gen.ICustomOrderExtractGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Изменение вида возмещения затрат
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChangeCompensationTypeActionGen extends EntityBase
 implements ICustomOrderAction{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.custom.ChangeCompensationTypeAction";
    public static final String ENTITY_NAME = "changeCompensationTypeAction";
    public static final int VERSION_HASH = -825219836;
    private static IEntityMeta ENTITY_META;

    public static final String L_OLD_COMPENSATION_TYPE = "oldCompensationType";
    public static final String L_NEW_COMPENSATION_TYPE = "newCompensationType";
    public static final String L_CUSTOM_EXTRACT = "customExtract";

    private CompensationType _oldCompensationType;     // Старый вид возмещения затрат
    private CompensationType _newCompensationType;     // Новый вид возмещения затрат
    private ICustomOrderExtract _customExtract;     // Выписка из кастомного приказа, с набором действий, определенных пользователем

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Старый вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getOldCompensationType()
    {
        return _oldCompensationType;
    }

    /**
     * @param oldCompensationType Старый вид возмещения затрат. Свойство не может быть null.
     */
    public void setOldCompensationType(CompensationType oldCompensationType)
    {
        dirty(_oldCompensationType, oldCompensationType);
        _oldCompensationType = oldCompensationType;
    }

    /**
     * @return Новый вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getNewCompensationType()
    {
        return _newCompensationType;
    }

    /**
     * @param newCompensationType Новый вид возмещения затрат. Свойство не может быть null.
     */
    public void setNewCompensationType(CompensationType newCompensationType)
    {
        dirty(_newCompensationType, newCompensationType);
        _newCompensationType = newCompensationType;
    }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public ICustomOrderExtract getCustomExtract()
    {
        return _customExtract;
    }

    /**
     * @param customExtract Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     */
    public void setCustomExtract(ICustomOrderExtract customExtract)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && customExtract!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(ICustomOrderExtract.class);
            IEntityMeta actual =  customExtract instanceof IEntity ? EntityRuntime.getMeta((IEntity) customExtract) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_customExtract, customExtract);
        _customExtract = customExtract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ChangeCompensationTypeActionGen)
        {
            setOldCompensationType(((ChangeCompensationTypeAction)another).getOldCompensationType());
            setNewCompensationType(((ChangeCompensationTypeAction)another).getNewCompensationType());
            setCustomExtract(((ChangeCompensationTypeAction)another).getCustomExtract());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChangeCompensationTypeActionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChangeCompensationTypeAction.class;
        }

        public T newInstance()
        {
            return (T) new ChangeCompensationTypeAction();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "oldCompensationType":
                    return obj.getOldCompensationType();
                case "newCompensationType":
                    return obj.getNewCompensationType();
                case "customExtract":
                    return obj.getCustomExtract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "oldCompensationType":
                    obj.setOldCompensationType((CompensationType) value);
                    return;
                case "newCompensationType":
                    obj.setNewCompensationType((CompensationType) value);
                    return;
                case "customExtract":
                    obj.setCustomExtract((ICustomOrderExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "oldCompensationType":
                        return true;
                case "newCompensationType":
                        return true;
                case "customExtract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "oldCompensationType":
                    return true;
                case "newCompensationType":
                    return true;
                case "customExtract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "oldCompensationType":
                    return CompensationType.class;
                case "newCompensationType":
                    return CompensationType.class;
                case "customExtract":
                    return ICustomOrderExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChangeCompensationTypeAction> _dslPath = new Path<ChangeCompensationTypeAction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChangeCompensationTypeAction");
    }
            

    /**
     * @return Старый вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeCompensationTypeAction#getOldCompensationType()
     */
    public static CompensationType.Path<CompensationType> oldCompensationType()
    {
        return _dslPath.oldCompensationType();
    }

    /**
     * @return Новый вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeCompensationTypeAction#getNewCompensationType()
     */
    public static CompensationType.Path<CompensationType> newCompensationType()
    {
        return _dslPath.newCompensationType();
    }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeCompensationTypeAction#getCustomExtract()
     */
    public static ICustomOrderExtractGen.Path<ICustomOrderExtract> customExtract()
    {
        return _dslPath.customExtract();
    }

    public static class Path<E extends ChangeCompensationTypeAction> extends EntityPath<E>
    {
        private CompensationType.Path<CompensationType> _oldCompensationType;
        private CompensationType.Path<CompensationType> _newCompensationType;
        private ICustomOrderExtractGen.Path<ICustomOrderExtract> _customExtract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Старый вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeCompensationTypeAction#getOldCompensationType()
     */
        public CompensationType.Path<CompensationType> oldCompensationType()
        {
            if(_oldCompensationType == null )
                _oldCompensationType = new CompensationType.Path<CompensationType>(L_OLD_COMPENSATION_TYPE, this);
            return _oldCompensationType;
        }

    /**
     * @return Новый вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeCompensationTypeAction#getNewCompensationType()
     */
        public CompensationType.Path<CompensationType> newCompensationType()
        {
            if(_newCompensationType == null )
                _newCompensationType = new CompensationType.Path<CompensationType>(L_NEW_COMPENSATION_TYPE, this);
            return _newCompensationType;
        }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeCompensationTypeAction#getCustomExtract()
     */
        public ICustomOrderExtractGen.Path<ICustomOrderExtract> customExtract()
        {
            if(_customExtract == null )
                _customExtract = new ICustomOrderExtractGen.Path<ICustomOrderExtract>(L_CUSTOM_EXTRACT, this);
            return _customExtract;
        }

        public Class getEntityClass()
        {
            return ChangeCompensationTypeAction.class;
        }

        public String getEntityName()
        {
            return "changeCompensationTypeAction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
