package ru.tandemservice.movestudent.entity.custom.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.custom.ChangeCourseAction;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unimove.entity.ICustomOrderAction;
import ru.tandemservice.unimove.entity.ICustomOrderExtract;
import ru.tandemservice.unimove.entity.gen.ICustomOrderExtractGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Смена курса
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChangeCourseActionGen extends EntityBase
 implements ICustomOrderAction{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.custom.ChangeCourseAction";
    public static final String ENTITY_NAME = "changeCourseAction";
    public static final int VERSION_HASH = 892384996;
    private static IEntityMeta ENTITY_META;

    public static final String L_OLD_COURSE = "oldCourse";
    public static final String L_NEW_COURSE = "newCourse";
    public static final String L_CUSTOM_EXTRACT = "customExtract";

    private Course _oldCourse;     // Старый курс
    private Course _newCourse;     // Новый курс
    private ICustomOrderExtract _customExtract;     // Выписка из кастомного приказа, с набором действий, определенных пользователем

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Старый курс. Свойство не может быть null.
     */
    @NotNull
    public Course getOldCourse()
    {
        return _oldCourse;
    }

    /**
     * @param oldCourse Старый курс. Свойство не может быть null.
     */
    public void setOldCourse(Course oldCourse)
    {
        dirty(_oldCourse, oldCourse);
        _oldCourse = oldCourse;
    }

    /**
     * @return Новый курс. Свойство не может быть null.
     */
    @NotNull
    public Course getNewCourse()
    {
        return _newCourse;
    }

    /**
     * @param newCourse Новый курс. Свойство не может быть null.
     */
    public void setNewCourse(Course newCourse)
    {
        dirty(_newCourse, newCourse);
        _newCourse = newCourse;
    }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public ICustomOrderExtract getCustomExtract()
    {
        return _customExtract;
    }

    /**
     * @param customExtract Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     */
    public void setCustomExtract(ICustomOrderExtract customExtract)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && customExtract!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(ICustomOrderExtract.class);
            IEntityMeta actual =  customExtract instanceof IEntity ? EntityRuntime.getMeta((IEntity) customExtract) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_customExtract, customExtract);
        _customExtract = customExtract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ChangeCourseActionGen)
        {
            setOldCourse(((ChangeCourseAction)another).getOldCourse());
            setNewCourse(((ChangeCourseAction)another).getNewCourse());
            setCustomExtract(((ChangeCourseAction)another).getCustomExtract());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChangeCourseActionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChangeCourseAction.class;
        }

        public T newInstance()
        {
            return (T) new ChangeCourseAction();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "oldCourse":
                    return obj.getOldCourse();
                case "newCourse":
                    return obj.getNewCourse();
                case "customExtract":
                    return obj.getCustomExtract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "oldCourse":
                    obj.setOldCourse((Course) value);
                    return;
                case "newCourse":
                    obj.setNewCourse((Course) value);
                    return;
                case "customExtract":
                    obj.setCustomExtract((ICustomOrderExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "oldCourse":
                        return true;
                case "newCourse":
                        return true;
                case "customExtract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "oldCourse":
                    return true;
                case "newCourse":
                    return true;
                case "customExtract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "oldCourse":
                    return Course.class;
                case "newCourse":
                    return Course.class;
                case "customExtract":
                    return ICustomOrderExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChangeCourseAction> _dslPath = new Path<ChangeCourseAction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChangeCourseAction");
    }
            

    /**
     * @return Старый курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeCourseAction#getOldCourse()
     */
    public static Course.Path<Course> oldCourse()
    {
        return _dslPath.oldCourse();
    }

    /**
     * @return Новый курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeCourseAction#getNewCourse()
     */
    public static Course.Path<Course> newCourse()
    {
        return _dslPath.newCourse();
    }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeCourseAction#getCustomExtract()
     */
    public static ICustomOrderExtractGen.Path<ICustomOrderExtract> customExtract()
    {
        return _dslPath.customExtract();
    }

    public static class Path<E extends ChangeCourseAction> extends EntityPath<E>
    {
        private Course.Path<Course> _oldCourse;
        private Course.Path<Course> _newCourse;
        private ICustomOrderExtractGen.Path<ICustomOrderExtract> _customExtract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Старый курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeCourseAction#getOldCourse()
     */
        public Course.Path<Course> oldCourse()
        {
            if(_oldCourse == null )
                _oldCourse = new Course.Path<Course>(L_OLD_COURSE, this);
            return _oldCourse;
        }

    /**
     * @return Новый курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeCourseAction#getNewCourse()
     */
        public Course.Path<Course> newCourse()
        {
            if(_newCourse == null )
                _newCourse = new Course.Path<Course>(L_NEW_COURSE, this);
            return _newCourse;
        }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeCourseAction#getCustomExtract()
     */
        public ICustomOrderExtractGen.Path<ICustomOrderExtract> customExtract()
        {
            if(_customExtract == null )
                _customExtract = new ICustomOrderExtractGen.Path<ICustomOrderExtract>(L_CUSTOM_EXTRACT, this);
            return _customExtract;
        }

        public Class getEntityClass()
        {
            return ChangeCourseAction.class;
        }

        public String getEntityName()
        {
            return "changeCourseAction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
