/* $Id:$ */
package ru.tandemservice.movestudent.component.listextract.e41.utils;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 2/18/13
 * Time: 4:51 PM
 */
public class OrgUnitVO extends DataWrapper
{
    private OrgUnit _orgUnit;
    private String _orgUnitStr;

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public String getOrgUnitStr()
    {
        return _orgUnitStr;
    }

    public void setOrgUnitStr(String orgUnitStr)
    {
        _orgUnitStr = orgUnitStr;
    }

    public OrgUnitVO(OrgUnit orgUnit)
    {
        _id = orgUnit.getId();
        _title = orgUnit.getTitleWithType();
        _orgUnit = orgUnit;

        List<String> parentsTitles = Lists.newArrayList();
        OrgUnit parent = orgUnit.getParent();
        while(null != parent)
        {
            if(!(parent instanceof TopOrgUnit))
                parentsTitles.add(parent.getShortTitle());
            parent = parent.getParent();
        }

        if(!parentsTitles.isEmpty())
        {
            _title += " (" + StringUtils.join(parentsTitles, ";") + ")";
        }
    }
}
