/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.menu.OrdersAll;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.base.bo.OrderComment.ui.Edit.OrderCommentEdit;
import ru.tandemservice.movestudent.component.commons.OrderFlexBuilder;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.movestudent.entity.StudentOtherOrderTextRelation;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractOrder;

import java.util.List;

import static ru.tandemservice.movestudent.entity.gen.AbstractStudentExtractGen.P_NUMBER;

/**
 * @author vip_delete
 * @since 22.12.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setSettings(UniBaseUtils.getDataSettings(component, "OrdersAll.filter"));

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<DataWrapper> dataSource = UniBaseUtils.createDataSource(component, getDao());

        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Дата формирования", OrderFlexBuilder.CREATE_DATE);
        linkColumn.setName(OrderFlexBuilder.CREATE_DATE);
        linkColumn.setFormatter(DateFormatter.DATE_FORMATTER_WITH_TIME);
        dataSource.addColumn(linkColumn);

        dataSource.addColumn(new SimpleColumn("Дата приказа", OrderFlexBuilder.COMMIT_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ приказа", OrderFlexBuilder.ORDER_NUMBER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата проведения", OrderFlexBuilder.COMMIT_DATE_SYSTEM).setFormatter(DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во выписок", OrderFlexBuilder.EXTRACT_COUNT).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Примечание", OrderFlexBuilder.ORDER_DESCRIPTION).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать примечание", ActionColumn.EDIT, "onClickEditComment").setPermissionKey("orderCommentEdit").setDisabledProperty(Model.EDIT_DISABLED));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrint", "Печать").setPermissionKey("print_menuList_ordersAll").setDisabledProperty(OrderFlexBuilder.DISABLE_PRINT));
        if (MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
            dataSource.addColumn(CommonBaseUtil.getPrintPdfColumn("onClickPrintPdf", "Печать PDF").setPermissionKey("printPdf_menuList_ordersAll").setDisabledProperty(OrderFlexBuilder.DISABLE_PRINT_PDF));

        dataSource.setOrder(OrderFlexBuilder.CREATE_DATE, OrderDirection.desc);
        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);

        UniBaseUtils.validateDatesPeriod(model.getSettings(), "commitDateFrom", "commitDateTo");
        UniBaseUtils.validateDatesPeriod(model.getSettings(), "createDateFrom", "createDateTo");
        UniBaseUtils.validateDatesPeriod(model.getSettings(), "commitDateSystemFrom", "commitDateSystemTo");

        if (!UserContext.getInstance().getErrorCollector().isHasFieldErrors())
        {
            DataSettingsFacade.saveSettings(model.getSettings());
            model.getDataSource().refresh();
        }
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }

    public void onClickPrint(IBusinessComponent component)
    {
        IEntity object = getDao().getNotNull(component.getListenerParameter());
        if (object instanceof StudentModularOrder)
        {
            activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_PRINT, new ParametersMap()
                    .add("orderId", object.getId())
            ));
        }
        else if (object instanceof StudentListOrder)
        {
            activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_ORDER_PRINT, new ParametersMap()
                    .add("orderId", object.getId())
            ));
        }
        else
        {
            // Иначе прочий приказ
            StudentOtherOrderTextRelation rel = UniDaoFacade.getCoreDao().get(StudentOtherOrderTextRelation.class, StudentOtherOrderTextRelation.order().s(), object.getId());
            if (rel == null)
                throw new ApplicationException("К данному приказу не прикреплена печатная форма.");
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(rel.getFileType()).fileName(rel.getFileName()).document(rel.getContent()), false);
        }
    }

    public void onClickPrintPdf(IBusinessComponent component)
    {
        IAbstractOrder order = getDao().getNotNull((Long) component.getListenerParameter());
        if (order instanceof StudentModularOrder)
        {
            activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_PRINT, new ParametersMap()
                    .add("orderId", order.getId())
                    .add("printPdf", Boolean.TRUE)
            ));
        }
        else if (order instanceof StudentListOrder)
        {
            activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_ORDER_PRINT, new ParametersMap()
                    .add("orderId", order.getId())
                    .add("printPdf", Boolean.TRUE)
            ));
        }
        else
        {
            throw new RuntimeException("Other orders not support print to pdf.");
        }
    }

    public void onClickDelete(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }

    public void onClickEditComment(IBusinessComponent component)
    {
        IAbstractOrder order = getDao().getNotNull((Long) component.getListenerParameter());
        List<AbstractStudentExtract> extractList = getDao().getList(AbstractStudentExtract.class, AbstractStudentExtract.paragraph().order().s(), order, P_NUMBER);
        if (!extractList.isEmpty() && extractList.size() == 1)
        {
            ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(OrderCommentEdit.class.getSimpleName(),
                  new ParametersMap().add("extractId",  extractList.get(0).getId())));
        }

    }

}