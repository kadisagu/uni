package ru.tandemservice.movestudent.entity;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.gen.AbstractStudentOrderGen;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.List;

public abstract class AbstractStudentOrder extends AbstractStudentOrderGen implements IAbstractOrder
{
    public static final String P_PARAGRAPH_LIST = "paragraphList";
    public static final String P_READONLY = "readonly";
    public static final String P_NO_DELETE = "noDelete";
    public static final String P_COUNT_EXTRACT = "countExtract";     // view-property
    public static final String P_COUNT_PARAGRAPH = "countParagraph"; // view-property

    @Override
    public List<? extends IAbstractParagraph> getParagraphList()
    {
        return UniDaoFacade.getCoreDao().getList(AbstractStudentParagraph.class, IAbstractParagraph.L_ORDER, this, IAbstractParagraph.P_NUMBER);
    }

    @Override
    public int getParagraphCount()
    {
        return UniDaoFacade.getCoreDao().getCount(AbstractStudentParagraph.class, IAbstractParagraph.L_ORDER, this);
    }

    public int getExtractCount()
    {
        return MoveStudentDaoFacade.getMoveStudentDao().getExtractCount(getId());
    }

    public boolean isReadonly()
    {
        return !UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(getState().getCode());
    }

    public boolean isNoDelete()
    {
        return UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(getState().getCode());
    }

    @Override
    public void setState(ICatalogItem state)
    {
        setState((OrderStates) state);
    }

    public String getCreateDateFormatted()
    {
        return DateFormatter.DATE_FORMATTER_WITH_TIME.format(getCreateDate());
    }
}