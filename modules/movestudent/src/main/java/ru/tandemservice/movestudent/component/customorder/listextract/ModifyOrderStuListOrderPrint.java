/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.listextract;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.custom.CustomOrderPrintForm;
import ru.tandemservice.movestudent.entity.custom.CustomParagraphPrintForm;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 07.10.2013
 */
public class ModifyOrderStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        // Если юзер изменил шаблон приказа - берем его, иначе шаблон по умолчанию
        CustomOrderPrintForm rel = DataAccessServices.dao().get(CustomOrderPrintForm.class, CustomOrderPrintForm.L_ORDER, order);
        if (rel != null && rel.getFile().getContent() != null)
            template = rel.getFile().getContent();

        RtfDocument document = new RtfReader().read(template);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order);
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, order);

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }

    protected void injectParagraphs(RtfDocument document, StudentListOrder order)
    {
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);
        if (rtfSearchResult.isFound())
        {
            List<IRtfElement> parList = new ArrayList<>();
            for (IAbstractParagraph par : order.getParagraphList())
            {
                CustomParagraphPrintForm rel = DataAccessServices.dao().get(CustomParagraphPrintForm.class, CustomParagraphPrintForm.L_PARAGRAPH, par);
                if (rel == null || rel.getFile() == null || rel.getFile().getContent() == null)
                    throw new ApplicationException("Необходимо загрузить пользовательский документ для параграфа №" + par.getNumber() + ".");

                RtfDocument paragraph = new RtfReader().read(rel.getFile().getContent());

                RtfInjectModifier paragraphInjectModifier = new RtfInjectModifier();
                // Различные метки для кастомных приказов сделать отдельной задачей

                // Список студентов
                RtfString students = new RtfString();
                for (String fullFio : MoveStudentDaoFacade.getMoveStudentDao().getStudentsFioByParagraph(par.getId()))
                {
                    students.append(fullFio);
                    students.par();
                }
                paragraphInjectModifier.put("STUDENT_LIST", students);
                paragraphInjectModifier.modify(paragraph);


                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}