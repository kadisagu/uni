/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e90;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.movestudent.entity.WeekendChildCareOutStuExtract;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;

import java.util.Collections;
import java.util.List;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 26.12.2012
 */
public class WeekendChildCareOutStuExtractPrint implements IPrintFormCreator<WeekendChildCareOutStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, WeekendChildCareOutStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        if (extract.isResumeGrantPay())
        {
            modifier.put("resumeGrantPay", ApplicationRuntime.getProperty("weekendChildCareOut.grantPaymentResumption")
                    .replaceAll("\\{grantSum\\}", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGrantSum()))
                    .replaceAll("\\{beginDate\\}", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getGrantPayBeginDate()))
                    .replaceAll("\\{endDate\\}", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getGrantPayEndDate())));

        } else
        {
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("resumeGrantPay"), true, false);
        }

        modifier.put("weekendOutDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getWeekendOutDate()));
        CommonExtractPrint.initEducationType(modifier, extract.getEntity().getEducationOrgUnit(), "Direction", true, false);

        StudentOrderReasons reason =  extract.getReason();
        if (null != reason)
        {
            modifier.put("reasonWithSpace", " " + reason.getTitle());
        }
        else
        {
            modifier.put("reasonWithSpace", "");
        }

        modifier.put("groupNew", extract.getGroupNew().getTitle());

        if (!extract.isHasDebts())
        {
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("debts"), false, false);
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("debtsList"), false, false);
        }
        else
        {
            if(null != extract.getDeadlineDate())
            {
                String liquidationDeadlineDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDeadlineDate());
                String liquidateEduPlanDifference = ApplicationRuntime.getProperty("liquidateEduPlanDifferenceColon").replaceAll("\\{deadlineDate\\}", liquidationDeadlineDate);
                modifier.put("debts", liquidateEduPlanDifference);
            }

            RtfString debtsList = new RtfString();
            int i = 0;
            List<StuExtractToDebtRelation> relsList = MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(extract);
            for(StuExtractToDebtRelation rel : relsList)
            {
                debtsList.append(String.valueOf(++i)).append(". «").append(rel.getDiscipline()).append("» - ").append(String.valueOf(rel.getHours())).append(" ч. - ").append(rel.getControlAction());
                if(relsList.indexOf(rel) < relsList.size() - 1) debtsList.par();
                else debtsList.append(".");
            }
            modifier.put("debtsList", debtsList);
        }

        CommonExtractPrint.initFefuGroup(modifier, "intoGroupNew", extract.getGroupNew(), extract.getEntity().getEducationOrgUnit().getDevelopForm(), " в группу ");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}