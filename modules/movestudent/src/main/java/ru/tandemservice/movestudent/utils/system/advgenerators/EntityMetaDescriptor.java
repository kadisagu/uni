/* $Id$ */
package ru.tandemservice.movestudent.utils.system.advgenerators;

import java.util.Arrays;
import java.util.List;

/**
 * Содержит описание сущности
 *
 * @author Dmitry Seleznev
 * @since 10.08.2012
 */
public class EntityMetaDescriptor
{
    private String _entityName; // Имя сущности (по сути, имя класса), в описанни сущности соответствует атрибуту "name"
    private String _parentRef; // Родительский тип, указывается, если выписка наследуется от чего-то кроме modularStudentExtract, в описанни сущности соответствует атрибуту "parent-ref"
    private String _entityTitle; // Человеко-читаемое название сущности, в описанни сущности соответствует атрибуту "title"
    private String _tableName; // Название таблицы, в описанни сущности соответствует атрибуту "table-name"
    private List<EntityFieldBaseMetaDescriptor> _fieldDescriptorsList; // Список описаний полей сущности

    /**
     * Конструктор
     *
     * @param entityName           - Имя сущности (по сути, имя класса), в описанни сущности соответствует атрибуту "name"
     * @param entityTitle          - Человеко-читаемое название сущности, в описанни сущности соответствует атрибуту "title"
     * @param fieldDescriptorsList - Список описаний полей сущности
     */
    public EntityMetaDescriptor(String entityName, String entityTitle, EntityFieldBaseMetaDescriptor... fieldDescriptorsList)
    {
        _entityName = entityName;
        _parentRef = "modularStudentExtract";
        _entityTitle = "Выписка из сборного приказа по студенту. " + entityTitle;
        _fieldDescriptorsList = Arrays.asList(fieldDescriptorsList);
    }

    /**
     * Конструктор
     *
     * @param entityName           - Имя сущности (по сути, имя класса), в описанни сущности соответствует атрибуту "name"
     * @param entityTitle          - Человеко-читаемое название сущности, в описанни сущности соответствует атрибуту "title"
     * @param parentRef            - Родительский тип, указывается, если выписка наследуется от чего-то кроме modularStudentExtract, в описанни сущности соответствует атрибуту "parent-ref"
     * @param fieldDescriptorsList - Список описаний полей сущности
     */
    public EntityMetaDescriptor(String entityName, String entityTitle, String parentRef, EntityFieldBaseMetaDescriptor... fieldDescriptorsList)
    {
        _entityName = entityName;
        _entityTitle = entityTitle;
        _parentRef = parentRef;
        _fieldDescriptorsList = Arrays.asList(fieldDescriptorsList);
    }

    /**
     * Конструктор
     *
     * @param entityName           - Имя сущности (по сути, имя класса), в описанни сущности соответствует атрибуту "name"
     * @param entityTitle          - Человеко-читаемое название сущности, в описанни сущности соответствует атрибуту "title"
     * @param parentRef            - Родительский тип, указывается, если выписка наследуется от чего-то кроме modularStudentExtract, в описанни сущности соответствует атрибуту "parent-ref"
     * @param tableName            - Название таблицы, в описанни сущности соответствует атрибуту "table-name"
     * @param fieldDescriptorsList - Список описаний полей сущности
     */
    public EntityMetaDescriptor(String entityName, String entityTitle, String parentRef, String tableName, EntityFieldBaseMetaDescriptor... fieldDescriptorsList)
    {
        _entityName = entityName;
        _entityTitle = entityTitle;
        _parentRef = parentRef;
        _tableName = tableName;
        _fieldDescriptorsList = Arrays.asList(fieldDescriptorsList);
    }

    /**
     * Имя сущности (по сути, имя класса), в описанни сущности соответствует атрибуту "name"
     *
     * @return - имя сущности
     */
    public String getEntityName()
    {
        return _entityName;
    }

    /**
     * Родительский тип, указывается, если выписка наследуется от чего-то кроме modularStudentExtract, в описанни сущности соответствует атрибуту "parent-ref"
     *
     * @return - родительский тип
     */
    public String getParentRef()
    {
        return _parentRef;
    }

    /**
     * Человеко-читаемое название сущности, в описанни сущности соответствует атрибуту "title"
     *
     * @return - человеко-читаемое название сущности
     */
    public String getEntityTitle()
    {
        return _entityTitle;
    }

    /**
     * Название таблицы, в описанни сущности соответствует атрибуту "table-name"
     *
     * @return - название таблицы
     */
    public String getTableName()
    {
        return _tableName;
    }

    /**
     * Список описаний полей сущности
     *
     * @return - список описаний полей
     */
    public List<EntityFieldBaseMetaDescriptor> getFieldDescriptorsList()
    {
        return _fieldDescriptorsList;
    }
}