/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

/**
 * @author Nikolay Fedorovskih
 * @since 07.12.2013
 */
public interface IJoinCreator
{
    void checkAlias(String alias);
}