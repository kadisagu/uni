package ru.tandemservice.movestudent.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.gen.StudentListParagraphGen;
import ru.tandemservice.unimove.IAbstractExtract;

import java.util.List;

/**
 * Параграф списочного приказа по студенту
 */
public class StudentListParagraph extends StudentListParagraphGen implements ITitled
{
    public static final String P_NO_EDIT = "noEdit";       // нельзя редактировать

    public static final String P_TITLE = "title";

    public String getEditableTitle()
    {
        return (getOrder().getNumber() != null ? "№ " + getOrder().getNumber() : "") + (getOrder().getCommitDate() == null ? "" : " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getOrder().getCommitDate()));
    }
    
    @Override
    public String getTitle()
    {
        return "Параграф №" + getNumber();
    }

    public static final String P_FIRST_EXTRACT = "firstExtract";

    public boolean isNoEdit()
    {
        return getOrder().isReadonly();
    }
}