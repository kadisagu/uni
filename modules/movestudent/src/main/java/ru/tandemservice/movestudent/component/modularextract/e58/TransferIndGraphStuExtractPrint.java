/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e58;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.TransferIndGraphStuExtract;
import ru.tandemservice.uni.entity.catalog.Term;

import java.util.List;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 21.08.2012
 */
public class TransferIndGraphStuExtractPrint implements IPrintFormCreator<TransferIndGraphStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, TransferIndGraphStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        List<Term> termList = MoveStudentDaoFacade.getMoveStudentDao().getExtractTerms(extract);
        StringBuilder builder = new StringBuilder();
        for (Term term: termList)
            builder.append(term.getTitle()).append(", ");
        String terms = builder.length() > 1 ? builder.substring(0, builder.length() - 2) : "";

        modifier.put("termslist", terms);
        modifier.put("educationYear", extract.getEducationYear().getTitle());
        modifier.put("s", termList.size() > 1 ? "ы" : "");
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}