package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.SocGrantResumptionStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О возобновлении выплаты социальной стипендии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SocGrantResumptionStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.SocGrantResumptionStuExtract";
    public static final String ENTITY_NAME = "socGrantResumptionStuExtract";
    public static final int VERSION_HASH = 1159402973;
    private static IEntityMeta ENTITY_META;

    public static final String P_START_DATE = "startDate";
    public static final String P_END_DATE = "endDate";

    private Date _startDate;     // Дата возобновления выплаты стипендии
    private Date _endDate;     // Дата окончания выплаты стипендии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата возобновления выплаты стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Дата возобновления выплаты стипендии. Свойство не может быть null.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Дата окончания выплаты стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания выплаты стипендии. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SocGrantResumptionStuExtractGen)
        {
            setStartDate(((SocGrantResumptionStuExtract)another).getStartDate());
            setEndDate(((SocGrantResumptionStuExtract)another).getEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SocGrantResumptionStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SocGrantResumptionStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new SocGrantResumptionStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "startDate":
                    return obj.getStartDate();
                case "endDate":
                    return obj.getEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "startDate":
                        return true;
                case "endDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "startDate":
                    return true;
                case "endDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "startDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SocGrantResumptionStuExtract> _dslPath = new Path<SocGrantResumptionStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SocGrantResumptionStuExtract");
    }
            

    /**
     * @return Дата возобновления выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantResumptionStuExtract#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Дата окончания выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantResumptionStuExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    public static class Path<E extends SocGrantResumptionStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _startDate;
        private PropertyPath<Date> _endDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата возобновления выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantResumptionStuExtract#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(SocGrantResumptionStuExtractGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Дата окончания выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantResumptionStuExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(SocGrantResumptionStuExtractGen.P_END_DATE, this);
            return _endDate;
        }

        public Class getEntityClass()
        {
            return SocGrantResumptionStuExtract.class;
        }

        public String getEntityName()
        {
            return "socGrantResumptionStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
