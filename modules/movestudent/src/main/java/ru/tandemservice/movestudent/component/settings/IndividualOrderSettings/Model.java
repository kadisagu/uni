/* $Id$ */
package ru.tandemservice.movestudent.component.settings.IndividualOrderSettings;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.entity.ExtractCreationRule;

/**
 * @author Dmitry Seleznev
 * @since 25.09.2012
 */
public class Model
{
    private DynamicListDataSource<ExtractCreationRule> _dataSource;

    public DynamicListDataSource<ExtractCreationRule> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<ExtractCreationRule> dataSource)
    {
        _dataSource = dataSource;
    }
}