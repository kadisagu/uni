/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderPub;

import org.apache.commons.lang.StringUtils;
import org.hibernate.ObjectNotFoundException;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.movestudent.component.customorder.CustomOrderUtils;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.StuListOrderToBasicRelation;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.services.UniServiceFacade;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.UnimvDefines;
import ru.tandemservice.unimv.UnimvUtil;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.VisaTask;
import ru.tandemservice.unimv.services.visatask.ISendToCoordinationService;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskHandler;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskService;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 06.05.2009
 */
public abstract class AbstractListOrderPubDAO<T extends StudentListOrder, Model extends AbstractListOrderPubModel<T>> extends UniDao<Model> implements IAbstractListOrderPubDAO<T, Model>
{
    @Override
    public T getOrder(Long id)
    {
        T order = new DQLSelectBuilder().fromEntity(StudentListOrder.class, "o")
                .where(eq(property("o.id"), value(id)))
                .createStatement(getSession()).uniqueResult();

        if (order == null)
            throw new ObjectNotFoundException(id, EntityRuntime.getMeta(id).getClassName());

        return order;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepare(Model model)
    {
        model.setSecModel(new CommonPostfixPermissionModel(model.getSecPostfix()));
        model.setVisingStatus(UnimvDaoFacade.getVisaDao().getVisingStatus(model.getOrder()));

        // Список оснований
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(StuListOrderToBasicRelation.class, "e")
                .column(property("e", StuListOrderToBasicRelation.basic().title()))
                .where(eq(property("e", StuListOrderToBasicRelation.L_ORDER), value(model.getOrderId())))
                .order(property("e", StuListOrderToBasicRelation.basic().title()));
        model.setBasicsTitle(StringUtils.join(dql.createStatement(getSession()).list(), ", "));

        prepareItems(model);

        model.setMultipleParagraphAddEnabled(MoveStudentDaoFacade.getMoveStudentDao().isMultipleAddParagraphsOrder(model.getOrder().getType()));
    }

    private void prepareItems(Model model)
    {
        DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                        .joinPath(DQLJoinType.inner, AbstractStudentExtract.paragraph().fromAlias("e"), "p")
                        .where(eq(property("p", IAbstractParagraph.L_ORDER), value(model.getOrderId())))
                        .group(property("p.id"))
                        .group(property("p", IAbstractParagraph.P_NUMBER))
        );

        int parIdCol = dql.column("p.id");
        int parNumCol = dql.column(property("p", IAbstractParagraph.P_NUMBER));
        int countCol = dql.column(DQLFunctions.countStar());
        int firstExtractCol = dql.column(
                new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "ee")
                        .top(1).column("ee.id")
                        .where(eq(property("ee", IAbstractExtract.L_PARAGRAPH), property("p.id")))
                        .where(eq(property("ee", IAbstractExtract.P_NUMBER), DQLFunctions.min(property("e", IAbstractExtract.P_NUMBER))))
                        .buildQuery()
        );

        List<Object[]> items = dql.getDql().createStatement(getSession()).list();
        Map<Long, DataWrapper> map = new HashMap<>(items.size());
        for (Object[] item : items)
        {
            Long id = (Long) item[parIdCol];
            DataWrapper wrapper = new DataWrapper(id, "Параграф №" + item[parNumCol]);
            wrapper.setProperty(AbstractListOrderPubModel.EXTRACT_COUNT, item[countCol]);
            wrapper.setProperty("number", item[parNumCol]);
            map.put((Long) item[firstExtractCol], wrapper);
        }


        Map<Long, String[]> typeMap = new HashMap<>();

        if (!items.isEmpty())
        {
            dql = new DQLSelectColumnNumerator(
                    new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                            .joinPath(DQLJoinType.inner, AbstractStudentExtract.entity().fromAlias("e"), "s")
                            .joinPath(DQLJoinType.inner, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().fromAlias("s"), "el")
                            .joinPath(DQLJoinType.left, EducationLevels.eduProgramSubject().fromAlias("el"), "eps")
                            .joinPath(DQLJoinType.left, EducationLevels.eduProgramSpecialization().fromAlias("el"), "epsp")
                            .where(in("e.id", map.keySet()))
            );

            int extractIdCol = dql.column("e.id");
            int groupTitleCol = dql.column(property("e", AbstractStudentExtract.P_GROUP_STR));
            int eduTitle = dql.column(property("e", AbstractStudentExtract.P_EDUCATION_LEVEL_HIGH_SCHOOL_STR));
            int extractTypeCol = dql.column(property("e", AbstractStudentExtract.type().id()));
            int eduProgSpec = dql.column(property("epsp"));
            int eduProgSubj = dql.column(property("eps"));
            int courseTitleCol = dql.column(property("s", Student.course().title()));
            int developFormCol = dql.column(property("s", Student.educationOrgUnit().developForm().title()));

            boolean readOnly = model.getOrder().isReadonly();
            for (Object[] item : dql.getDql().createStatement(getSession()).<Object[]>list())
            {
                Long id = (Long) item[extractIdCol];
                Long typeId = (Long) item[extractTypeCol];
                DataWrapper wrapper = map.get(id);

                String[] typeItem = typeMap.get(typeId);
                if (typeItem == null)
                {
                    StudentExtractType type = getNotNull(typeId);
                    String compName = MoveStudentUtils.getListParagraphPubComponent(type);
                    String typeTitle = UserContext.getInstance().getPrincipalContext().isAdmin() ? type.getTitleWithCode() : type.getTitle();
                    typeMap.put(typeId, typeItem = new String[]{compName, typeTitle});
                }

                wrapper.setProperty(AbstractListOrderPubModel.FIRST_EXTRACT, proxy(id));
                wrapper.setProperty(AbstractListOrderPubModel.EDU_LEVEL_HS, item[eduTitle]);
                wrapper.setProperty(AbstractListOrderPubModel.GROUP_TITLE, item[groupTitleCol]);
                wrapper.setProperty(AbstractListOrderPubModel.PAR_COMPONENT, typeItem[0]);
                wrapper.setProperty(AbstractListOrderPubModel.EXTRACT_TYPE_TITLE, typeItem[1]);
                wrapper.setProperty(AbstractListOrderPubModel.READ_ONLY, readOnly);
                wrapper.setProperty(AbstractListOrderPubModel.EDU_PROGRAM_SUBJECT, item[eduProgSubj] != null? ((EduProgramSubject)item[eduProgSubj]).getTitleWithCodeIndexAndGen(): "");
                wrapper.setProperty(AbstractListOrderPubModel.EDU_PROGRAM_SPEC, item[eduProgSpec]!=null? ((EduProgramSpecialization)item[eduProgSpec]).getDisplayableTitle():"");
                wrapper.setProperty(AbstractListOrderPubModel.COURSE_TITLE, item[courseTitleCol]);
                wrapper.setProperty(AbstractListOrderPubModel.DEVELOP_FORM_TITLE, item[developFormCol]);
            }
        }

        List<DataWrapper> wrappers = new ArrayList<>(map.values());
        Collections.sort(wrappers, new Comparator<DataWrapper>()
        {
            @Override
            public int compare(DataWrapper o1, DataWrapper o2)
            {
                return (Integer) o1.get("number") - (Integer) o2.get("number");
            }
        });

        model.setWrappers(wrappers);
        model.setParagraphCount(wrappers.size());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        model.getDataSource().setCountRow(Math.max(model.getWrappers().size(), 3));
        UniBaseUtils.createPage(model.getDataSource(), new ArrayList<>(model.getWrappers()));
    }

    @Override
    public void doSendToCoordination(Model model, IPersistentPersonable initiator)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getOrder());
        if (visaTask != null)
            errCollector.add("Нельзя отправить приказ на согласование, так как он уже на согласовании.");

        if (StringUtils.isEmpty(model.getOrder().getNumber()))
            errCollector.add("Нельзя отправить приказ на согласование без номера.");

        if (model.getOrder().getCommitDate() == null)
            errCollector.add("Нельзя отправить приказ на согласование без даты приказа.");

        if (!errCollector.hasErrors())
        {
            //2. надо сохранить печатную форму приказа
            MoveStudentDaoFacade.getMoveStudentDao().saveListOrderText(model.getOrder());

            //3. отправляем на согласование
            ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
            sendToCoordinationService.init(model.getOrder(), initiator);
            sendToCoordinationService.execute();
        }
    }

    @Override
    public void doSendToFormative(Model model)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getOrder());
        if (visaTask != null)
            errCollector.add("Нельзя отправить приказ на формирование, так как он на согласовании.");

        if (!errCollector.hasErrors())
        {
            //2. надо сменить состояние на формируется
            model.getOrder().setState(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
            update(model.getOrder());
        }
    }

    @Override
    public void doReject(Model model)
    {
        ITouchVisaTaskService rejectService = UniServiceFacade.getService(ITouchVisaTaskService.REJECT_VISA_TASK_SERVICE_NAME);
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getOrder());
        if (visaTask != null)
        {
            rejectService.init(visaTask.getId(), "SYSTEM: Отменено пользователем с публикатора");
            rejectService.execute();
        }
        else
        {
            //задачи нет => документ уже согласован,
            //в историю согласования ничего не попадет. так как нет задачи согласования
            //короче запускаем процедуру отрицательного завершения процедуры согласования
            ITouchVisaTaskHandler touchService = UnimvUtil.findTouchService(model.getOrder(), UnimvDefines.VISA_REJECT_SERVICE_MAP);
            touchService.init(model.getOrder());
            touchService.execute();
        }
    }

    @Override
    public void doCommit(Model model)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        if (model.getOrder().getNumber() == null)
            errCollector.add("Нельзя проводить приказ без номера.");

        if (model.getOrder().getCommitDate() == null)
            errCollector.add("Нельзя проводить приказ без даты приказа.");

        if (!errCollector.hasErrors())
        {
            //обновляем печатную форму
            MoveStudentDaoFacade.getMoveStudentDao().saveListOrderText(model.getOrder());

            MoveDaoFacade.getMoveDao().doCommitOrder(model.getOrder(), UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED), null);
        }
    }

    @Override
    public void doRollback(Model model)
    {
        // у каждого студента из выписок данного приказа смотрим, нет ли у него более поздней проведенной выписки.
        // если такую находим - ругаемся

        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();
        final DQLSelectBuilder extractDQL = new DQLSelectBuilder()
                .fromEntity(AbstractStudentExtract.class, "ext")
                .column("ext.entity.id")
                .where(in(
                        property("ext.entity"),
                        new DQLSelectBuilder()
                                .fromEntity(AbstractStudentExtract.class, "stu")
                                .column("stu.entity")
                                .where(eq(property(AbstractStudentExtract.paragraph().order().fromAlias("stu")), value(model.getOrder())))
                                .buildQuery()
                ))
                .where(gt(property("ext.paragraph.order.commitDateSystem"), valueTimestamp(model.getOrder().getCommitDateSystem())));

        List<Student> students = getList(Student.class, extractDQL.createStatement(getSession()).<Long>list());
        if (students.size() > 0)
            errCollector.add("Нельзя откатить приказ, т.к. для студент" + (students.size() == 1 ? "ов " : "а ") +
                                     getStudentsList(students) +
                                     " есть приказы, которые вступили в силу после данного приказа");

        CustomOrderUtils.checkRollbackSimpleOrderAvailable(model.getOrder());

        if (!errCollector.hasErrors())
            MoveDaoFacade.getMoveDao().doRollbackOrder(model.getOrder(), UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER), null);
    }

    private String getStudentsList(List<Student> students)
    {
        String resultList = "";
        for (Student student : students)
            resultList += student.getPerson().getFio() + ", ";
        return resultList.length() > 0 ? resultList.substring(0, resultList.length() - 3) : "";
    }

    @Override
    public void deleteParagraph(Model model, Long paragraphId)
    {
        StudentListParagraph paragraph = getNotNull(StudentListParagraph.class, paragraphId);
        getSession().refresh(model.getOrder());
        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getOrder().getState().getCode()))
            throw new ApplicationException("Нельзя изменять приказ. Изменение приказа возможно только в состоянии «Формируется».");
        MoveDaoFacade.getMoveDao().deleteParagraph(paragraph, null);
    }
}
