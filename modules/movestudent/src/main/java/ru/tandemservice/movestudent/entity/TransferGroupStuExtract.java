package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.TransferGroupStuExtractGen;

import java.util.Date;

/**
 * О переводе из группы в группу
 */
public class TransferGroupStuExtract extends TransferGroupStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}