/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e32.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 01.10.2012
 */
@SuppressWarnings("unchecked")
public class Controller extends AbstractListParagraphAddEditController<AcadGrantAssignStuListExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        Model model = getModel(component);
        if (model.isEditForm())
            model.setFillDefaults(false);
    }

    @Override
    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дополнительный статус", Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Пол", Student.person().identityCard().sex().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("status", "Состояние", Model.STATUS_KEY, SimpleFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("grantSizeRuble","Размер стипендии, руб").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("groupManagerBonusSizeRuble","Размер выплаты старосте, руб").setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Заполнить по образцу", "clone", "onClickCopyGrantSize"));
        dataSource.setOrder("status", OrderDirection.asc);

        Model model = getModel(component);
        if (model.isEditForm())
        {
            IValueMapHolder grantSizeRubleHolder = (IValueMapHolder) dataSource.getColumn("grantSizeRuble");
            Map<Long, Double> grantSizeRubleMap = (null == grantSizeRubleHolder ? Collections.emptyMap() : grantSizeRubleHolder.getValueMap());

            IValueMapHolder groupManagerBonusSizeRubleHolder = (IValueMapHolder) dataSource.getColumn("groupManagerBonusSizeRuble");
            Map<Long, Double> groupManagerBonusSizeRubleMap = (null == grantSizeRubleHolder ? Collections.emptyMap() : groupManagerBonusSizeRubleHolder.getValueMap());

            for (AcadGrantAssignStuListExtract grantExtract: (List<AcadGrantAssignStuListExtract>) model.getParagraph().getExtractList())
            {
                grantSizeRubleMap.put(grantExtract.getEntity().getId(), grantExtract.getGrantSizeRuble());
                groupManagerBonusSizeRubleMap.put(grantExtract.getEntity().getId(), grantExtract.getGroupManagerBonusSizeRuble());
            }
        }
    }

    public void onClickCopyGrantSize(IBusinessComponent component)
    {
        Model model = getModel(component);
        List<Long> groupManagerIds = model.getGroupManagerIds();
        DynamicListDataSource dataSource = model.getDataSource();

        IValueMapHolder grantSizeRubleHolder = (IValueMapHolder) model.getDataSource().getColumn("grantSizeRuble");
        Map<Long, Double> grantSizeRubleMap = (null == grantSizeRubleHolder ? Collections.emptyMap() : grantSizeRubleHolder.getValueMap());

        IValueMapHolder groupManagerBonusSizeRubleHolder = (IValueMapHolder) model.getDataSource().getColumn("groupManagerBonusSizeRuble");
        Map<Long, Double> groupManagerBonusSizeRubleMap = (null == grantSizeRubleHolder ? Collections.emptyMap() : groupManagerBonusSizeRubleHolder.getValueMap());

        Double currentGrantSizeRuble = getValue(grantSizeRubleMap.get(component.getListenerParameter()));
        Double currentGroupManagerBonusSizeRuble = getValue(groupManagerBonusSizeRubleMap.get(component.getListenerParameter()));

        for (ViewWrapper<Student> wrapper: (List<ViewWrapper<Student>>) dataSource.getSelectedEntities())
        {
            Long id = wrapper.getId();

            if (grantSizeRubleMap.get(id) == null)
            {
                grantSizeRubleMap.put(id, currentGrantSizeRuble);
            }

            if (groupManagerBonusSizeRubleMap.get(id) == null && groupManagerIds.contains(id))
            {
                groupManagerBonusSizeRubleMap.put(id, currentGroupManagerBonusSizeRuble);
            }
        }

        model.setFillDefaults(false);
        model.getDataSource().refresh();
    }

    private Double getValue(Object object)
    {
        if (object instanceof Long)
            return ((Long) object).doubleValue();
        else
            return (Double) object;
    }
}
