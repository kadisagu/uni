/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.list.StudentListParagraphAdd;

import java.util.List;

import org.tandemframework.core.component.Input;

import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;

/**
 * @author vip_delete
 * @since 07.05.2009
 */
@Input(keys = {"orderId"}, bindings = {"orderId"})
public class Model
{
    private Long _orderId;
    private StudentListOrder _order;
    private List<StudentExtractType> _typeList;
    private StudentExtractType _type;

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public StudentListOrder getOrder()
    {
        return _order;
    }

    public void setOrder(StudentListOrder order)
    {
        _order = order;
    }

    public List<StudentExtractType> getTypeList()
    {
        return _typeList;
    }

    public void setTypeList(List<StudentExtractType> typeList)
    {
        _typeList = typeList;
    }

    public StudentExtractType getType()
    {
        return _type;
    }

    public void setType(StudentExtractType type)
    {
        _type = type;
    }
}
