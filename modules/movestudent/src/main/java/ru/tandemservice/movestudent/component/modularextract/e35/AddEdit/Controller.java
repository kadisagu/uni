/* $Id: Controller.java.vm 6933 2009-03-07 05:41:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e35.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.IBusinessComponent;

import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 22.12.2009
 */
public class Controller extends CommonModularStudentExtractAddEditController<ChangeMiddleNameStuExtract, IDAO, Model>
{
    public void onRefresh(final IBusinessComponent component)
    {
        final ChangeMiddleNameStuExtract extract = getModel(component).getExtract();
        final IdentityCardType cardType = extract.getCardType();
        if ((cardType != null) && cardType.isSeriaRequired() && isNeedChange(extract.getCardSeria(), cardType.getMaxSeriaLength()))
        {
            extract.setCardSeria(getDefaultValue(cardType.getMaxSeriaLength()));
        }
        if ((cardType != null) && cardType.isNumberRequired() && isNeedChange(extract.getCardNumber(), cardType.getMaxNumberLength()))
        {
            extract.setCardNumber(getDefaultValue(cardType.getMaxNumberLength()));
        }
        if ((cardType == null) && StringUtils.containsOnly(extract.getCardNumber(), "0"))
        {
            extract.setCardNumber(null);
        }
        if ((null != cardType) && !cardType.isShowSeria())
        {
            extract.setCardSeria(null);
        }
        if (null != cardType)
        {
            extract.setCitizenship(cardType.getCitizenshipDefault());
        }
    }

    private static boolean isNeedChange(final String value, final int maxLength)
    {
        return StringUtils.isEmpty(value) || StringUtils.containsOnly(value, "0") || (value.length() > maxLength);
    }

    private static String getDefaultValue(final int maxLength)
    {
        return String.format("%" + maxLength + "s", "").replaceAll(" ", "0");
    }

}
