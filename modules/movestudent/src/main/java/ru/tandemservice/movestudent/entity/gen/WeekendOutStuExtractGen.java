package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.WeekendOutStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О выходе из академического отпуска
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class WeekendOutStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.WeekendOutStuExtract";
    public static final String ENTITY_NAME = "weekendOutStuExtract";
    public static final int VERSION_HASH = 1195248556;
    private static IEntityMeta ENTITY_META;

    public static final String P_REASON_OLD = "reasonOld";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_GRANT_SUM = "grantSum";
    public static final String P_PAYMENT_RESUME = "paymentResume";

    private String _reasonOld;     // Причина ухода в академический отпуск
    private Date _beginDate;     // Дата начала выплаты стипендии
    private Date _endDate;     // Дата окончания выплаты стипендии
    private Double _grantSum;     // Размер стипендии
    private boolean _paymentResume;     // Возобновить выплату стипендии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Причина ухода в академический отпуск.
     */
    @Length(max=255)
    public String getReasonOld()
    {
        return _reasonOld;
    }

    /**
     * @param reasonOld Причина ухода в академический отпуск.
     */
    public void setReasonOld(String reasonOld)
    {
        dirty(_reasonOld, reasonOld);
        _reasonOld = reasonOld;
    }

    /**
     * @return Дата начала выплаты стипендии.
     */
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала выплаты стипендии.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания выплаты стипендии.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания выплаты стипендии.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Размер стипендии.
     */
    public Double getGrantSum()
    {
        return _grantSum;
    }

    /**
     * @param grantSum Размер стипендии.
     */
    public void setGrantSum(Double grantSum)
    {
        dirty(_grantSum, grantSum);
        _grantSum = grantSum;
    }

    /**
     * @return Возобновить выплату стипендии. Свойство не может быть null.
     */
    @NotNull
    public boolean isPaymentResume()
    {
        return _paymentResume;
    }

    /**
     * @param paymentResume Возобновить выплату стипендии. Свойство не может быть null.
     */
    public void setPaymentResume(boolean paymentResume)
    {
        dirty(_paymentResume, paymentResume);
        _paymentResume = paymentResume;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof WeekendOutStuExtractGen)
        {
            setReasonOld(((WeekendOutStuExtract)another).getReasonOld());
            setBeginDate(((WeekendOutStuExtract)another).getBeginDate());
            setEndDate(((WeekendOutStuExtract)another).getEndDate());
            setGrantSum(((WeekendOutStuExtract)another).getGrantSum());
            setPaymentResume(((WeekendOutStuExtract)another).isPaymentResume());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends WeekendOutStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) WeekendOutStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new WeekendOutStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "reasonOld":
                    return obj.getReasonOld();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "grantSum":
                    return obj.getGrantSum();
                case "paymentResume":
                    return obj.isPaymentResume();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "reasonOld":
                    obj.setReasonOld((String) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "grantSum":
                    obj.setGrantSum((Double) value);
                    return;
                case "paymentResume":
                    obj.setPaymentResume((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reasonOld":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "grantSum":
                        return true;
                case "paymentResume":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reasonOld":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "grantSum":
                    return true;
                case "paymentResume":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "reasonOld":
                    return String.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "grantSum":
                    return Double.class;
                case "paymentResume":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<WeekendOutStuExtract> _dslPath = new Path<WeekendOutStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "WeekendOutStuExtract");
    }
            

    /**
     * @return Причина ухода в академический отпуск.
     * @see ru.tandemservice.movestudent.entity.WeekendOutStuExtract#getReasonOld()
     */
    public static PropertyPath<String> reasonOld()
    {
        return _dslPath.reasonOld();
    }

    /**
     * @return Дата начала выплаты стипендии.
     * @see ru.tandemservice.movestudent.entity.WeekendOutStuExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания выплаты стипендии.
     * @see ru.tandemservice.movestudent.entity.WeekendOutStuExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Размер стипендии.
     * @see ru.tandemservice.movestudent.entity.WeekendOutStuExtract#getGrantSum()
     */
    public static PropertyPath<Double> grantSum()
    {
        return _dslPath.grantSum();
    }

    /**
     * @return Возобновить выплату стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendOutStuExtract#isPaymentResume()
     */
    public static PropertyPath<Boolean> paymentResume()
    {
        return _dslPath.paymentResume();
    }

    public static class Path<E extends WeekendOutStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<String> _reasonOld;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Double> _grantSum;
        private PropertyPath<Boolean> _paymentResume;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Причина ухода в академический отпуск.
     * @see ru.tandemservice.movestudent.entity.WeekendOutStuExtract#getReasonOld()
     */
        public PropertyPath<String> reasonOld()
        {
            if(_reasonOld == null )
                _reasonOld = new PropertyPath<String>(WeekendOutStuExtractGen.P_REASON_OLD, this);
            return _reasonOld;
        }

    /**
     * @return Дата начала выплаты стипендии.
     * @see ru.tandemservice.movestudent.entity.WeekendOutStuExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(WeekendOutStuExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания выплаты стипендии.
     * @see ru.tandemservice.movestudent.entity.WeekendOutStuExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(WeekendOutStuExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Размер стипендии.
     * @see ru.tandemservice.movestudent.entity.WeekendOutStuExtract#getGrantSum()
     */
        public PropertyPath<Double> grantSum()
        {
            if(_grantSum == null )
                _grantSum = new PropertyPath<Double>(WeekendOutStuExtractGen.P_GRANT_SUM, this);
            return _grantSum;
        }

    /**
     * @return Возобновить выплату стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendOutStuExtract#isPaymentResume()
     */
        public PropertyPath<Boolean> paymentResume()
        {
            if(_paymentResume == null )
                _paymentResume = new PropertyPath<Boolean>(WeekendOutStuExtractGen.P_PAYMENT_RESUME, this);
            return _paymentResume;
        }

        public Class getEntityClass()
        {
            return WeekendOutStuExtract.class;
        }

        public String getEntityName()
        {
            return "weekendOutStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
