package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.SessionProlongStuExtract;
import ru.tandemservice.uni.entity.catalog.SessionType;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О продлении экзаменационной сессии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionProlongStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.SessionProlongStuExtract";
    public static final String ENTITY_NAME = "sessionProlongStuExtract";
    public static final int VERSION_HASH = 1462969727;
    private static IEntityMeta ENTITY_META;

    public static final String P_PROLONG_REASON = "prolongReason";
    public static final String L_SESSION_YEAR = "sessionYear";
    public static final String L_SESSION_TYPE = "sessionType";
    public static final String P_PROLONG_DATE_FROM = "prolongDateFrom";
    public static final String P_PROLONG_DATE_TO = "prolongDateTo";
    public static final String P_PREV_PROLONG_DATE_TO = "prevProlongDateTo";

    private String _prolongReason;     // В связи с чем продлевается сессия
    private EducationYear _sessionYear;     // Учебный год сессии
    private SessionType _sessionType;     // Вид сессии
    private Date _prolongDateFrom;     // Продлить сессию с
    private Date _prolongDateTo;     // Продлить сессию по
    private Date _prevProlongDateTo;     // Дата продления сессии по предыдущему приказу

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return В связи с чем продлевается сессия. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getProlongReason()
    {
        return _prolongReason;
    }

    /**
     * @param prolongReason В связи с чем продлевается сессия. Свойство не может быть null.
     */
    public void setProlongReason(String prolongReason)
    {
        dirty(_prolongReason, prolongReason);
        _prolongReason = prolongReason;
    }

    /**
     * @return Учебный год сессии. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getSessionYear()
    {
        return _sessionYear;
    }

    /**
     * @param sessionYear Учебный год сессии. Свойство не может быть null.
     */
    public void setSessionYear(EducationYear sessionYear)
    {
        dirty(_sessionYear, sessionYear);
        _sessionYear = sessionYear;
    }

    /**
     * @return Вид сессии. Свойство не может быть null.
     */
    @NotNull
    public SessionType getSessionType()
    {
        return _sessionType;
    }

    /**
     * @param sessionType Вид сессии. Свойство не может быть null.
     */
    public void setSessionType(SessionType sessionType)
    {
        dirty(_sessionType, sessionType);
        _sessionType = sessionType;
    }

    /**
     * @return Продлить сессию с. Свойство не может быть null.
     */
    @NotNull
    public Date getProlongDateFrom()
    {
        return _prolongDateFrom;
    }

    /**
     * @param prolongDateFrom Продлить сессию с. Свойство не может быть null.
     */
    public void setProlongDateFrom(Date prolongDateFrom)
    {
        dirty(_prolongDateFrom, prolongDateFrom);
        _prolongDateFrom = prolongDateFrom;
    }

    /**
     * @return Продлить сессию по. Свойство не может быть null.
     */
    @NotNull
    public Date getProlongDateTo()
    {
        return _prolongDateTo;
    }

    /**
     * @param prolongDateTo Продлить сессию по. Свойство не может быть null.
     */
    public void setProlongDateTo(Date prolongDateTo)
    {
        dirty(_prolongDateTo, prolongDateTo);
        _prolongDateTo = prolongDateTo;
    }

    /**
     * @return Дата продления сессии по предыдущему приказу.
     */
    public Date getPrevProlongDateTo()
    {
        return _prevProlongDateTo;
    }

    /**
     * @param prevProlongDateTo Дата продления сессии по предыдущему приказу.
     */
    public void setPrevProlongDateTo(Date prevProlongDateTo)
    {
        dirty(_prevProlongDateTo, prevProlongDateTo);
        _prevProlongDateTo = prevProlongDateTo;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionProlongStuExtractGen)
        {
            setProlongReason(((SessionProlongStuExtract)another).getProlongReason());
            setSessionYear(((SessionProlongStuExtract)another).getSessionYear());
            setSessionType(((SessionProlongStuExtract)another).getSessionType());
            setProlongDateFrom(((SessionProlongStuExtract)another).getProlongDateFrom());
            setProlongDateTo(((SessionProlongStuExtract)another).getProlongDateTo());
            setPrevProlongDateTo(((SessionProlongStuExtract)another).getPrevProlongDateTo());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionProlongStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionProlongStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new SessionProlongStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "prolongReason":
                    return obj.getProlongReason();
                case "sessionYear":
                    return obj.getSessionYear();
                case "sessionType":
                    return obj.getSessionType();
                case "prolongDateFrom":
                    return obj.getProlongDateFrom();
                case "prolongDateTo":
                    return obj.getProlongDateTo();
                case "prevProlongDateTo":
                    return obj.getPrevProlongDateTo();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "prolongReason":
                    obj.setProlongReason((String) value);
                    return;
                case "sessionYear":
                    obj.setSessionYear((EducationYear) value);
                    return;
                case "sessionType":
                    obj.setSessionType((SessionType) value);
                    return;
                case "prolongDateFrom":
                    obj.setProlongDateFrom((Date) value);
                    return;
                case "prolongDateTo":
                    obj.setProlongDateTo((Date) value);
                    return;
                case "prevProlongDateTo":
                    obj.setPrevProlongDateTo((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "prolongReason":
                        return true;
                case "sessionYear":
                        return true;
                case "sessionType":
                        return true;
                case "prolongDateFrom":
                        return true;
                case "prolongDateTo":
                        return true;
                case "prevProlongDateTo":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "prolongReason":
                    return true;
                case "sessionYear":
                    return true;
                case "sessionType":
                    return true;
                case "prolongDateFrom":
                    return true;
                case "prolongDateTo":
                    return true;
                case "prevProlongDateTo":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "prolongReason":
                    return String.class;
                case "sessionYear":
                    return EducationYear.class;
                case "sessionType":
                    return SessionType.class;
                case "prolongDateFrom":
                    return Date.class;
                case "prolongDateTo":
                    return Date.class;
                case "prevProlongDateTo":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionProlongStuExtract> _dslPath = new Path<SessionProlongStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionProlongStuExtract");
    }
            

    /**
     * @return В связи с чем продлевается сессия. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionProlongStuExtract#getProlongReason()
     */
    public static PropertyPath<String> prolongReason()
    {
        return _dslPath.prolongReason();
    }

    /**
     * @return Учебный год сессии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionProlongStuExtract#getSessionYear()
     */
    public static EducationYear.Path<EducationYear> sessionYear()
    {
        return _dslPath.sessionYear();
    }

    /**
     * @return Вид сессии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionProlongStuExtract#getSessionType()
     */
    public static SessionType.Path<SessionType> sessionType()
    {
        return _dslPath.sessionType();
    }

    /**
     * @return Продлить сессию с. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionProlongStuExtract#getProlongDateFrom()
     */
    public static PropertyPath<Date> prolongDateFrom()
    {
        return _dslPath.prolongDateFrom();
    }

    /**
     * @return Продлить сессию по. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionProlongStuExtract#getProlongDateTo()
     */
    public static PropertyPath<Date> prolongDateTo()
    {
        return _dslPath.prolongDateTo();
    }

    /**
     * @return Дата продления сессии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.SessionProlongStuExtract#getPrevProlongDateTo()
     */
    public static PropertyPath<Date> prevProlongDateTo()
    {
        return _dslPath.prevProlongDateTo();
    }

    public static class Path<E extends SessionProlongStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<String> _prolongReason;
        private EducationYear.Path<EducationYear> _sessionYear;
        private SessionType.Path<SessionType> _sessionType;
        private PropertyPath<Date> _prolongDateFrom;
        private PropertyPath<Date> _prolongDateTo;
        private PropertyPath<Date> _prevProlongDateTo;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return В связи с чем продлевается сессия. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionProlongStuExtract#getProlongReason()
     */
        public PropertyPath<String> prolongReason()
        {
            if(_prolongReason == null )
                _prolongReason = new PropertyPath<String>(SessionProlongStuExtractGen.P_PROLONG_REASON, this);
            return _prolongReason;
        }

    /**
     * @return Учебный год сессии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionProlongStuExtract#getSessionYear()
     */
        public EducationYear.Path<EducationYear> sessionYear()
        {
            if(_sessionYear == null )
                _sessionYear = new EducationYear.Path<EducationYear>(L_SESSION_YEAR, this);
            return _sessionYear;
        }

    /**
     * @return Вид сессии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionProlongStuExtract#getSessionType()
     */
        public SessionType.Path<SessionType> sessionType()
        {
            if(_sessionType == null )
                _sessionType = new SessionType.Path<SessionType>(L_SESSION_TYPE, this);
            return _sessionType;
        }

    /**
     * @return Продлить сессию с. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionProlongStuExtract#getProlongDateFrom()
     */
        public PropertyPath<Date> prolongDateFrom()
        {
            if(_prolongDateFrom == null )
                _prolongDateFrom = new PropertyPath<Date>(SessionProlongStuExtractGen.P_PROLONG_DATE_FROM, this);
            return _prolongDateFrom;
        }

    /**
     * @return Продлить сессию по. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionProlongStuExtract#getProlongDateTo()
     */
        public PropertyPath<Date> prolongDateTo()
        {
            if(_prolongDateTo == null )
                _prolongDateTo = new PropertyPath<Date>(SessionProlongStuExtractGen.P_PROLONG_DATE_TO, this);
            return _prolongDateTo;
        }

    /**
     * @return Дата продления сессии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.SessionProlongStuExtract#getPrevProlongDateTo()
     */
        public PropertyPath<Date> prevProlongDateTo()
        {
            if(_prevProlongDateTo == null )
                _prevProlongDateTo = new PropertyPath<Date>(SessionProlongStuExtractGen.P_PREV_PROLONG_DATE_TO, this);
            return _prevProlongDateTo;
        }

        public Class getEntityClass()
        {
            return SessionProlongStuExtract.class;
        }

        public String getEntityName()
        {
            return "sessionProlongStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
