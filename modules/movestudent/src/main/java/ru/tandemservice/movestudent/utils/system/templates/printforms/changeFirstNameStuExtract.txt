\keep\keepn\qj\b {extractNumber}. ИЗМЕНИТЬ имя во всех учетных документах:\b0\par
{fio} — {student_A} {course} курса {developForm_G} отделения группы {group} {orgUnit_G}, {learned_A} {compensationTypeStr}, по {educationType_D} «{educationOrgUnit}», считать {fullFioNew} с {entryIntoForceDate}.\par
  Основание: {listBasics}.