package ru.tandemservice.movestudent.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.gen.ModularStudentExtractGen;
import ru.tandemservice.unimove.IPrintableExtract;

import java.util.List;

/**
 * Выписка из сборного (индивидуального) приказа по студентам
 */
public abstract class ModularStudentExtract extends ModularStudentExtractGen implements IPrintableExtract
{
    public static final String P_REASON_FULL_TITLE = "reasonFullTitle";

    public static final String P_INDIVIDUAL_ORDER_TITLE = "individualOrderTitle";

    /**
     * Получение названия приказа для заведомо индивидуального приказа.
     * Медот добавлен для оптимизации списка костыльных индивидуальных приказов.
     * @return название
     */
    public String getIndividualOrderTitle()
    {
        if (getParagraph() == null)
            throw new IllegalStateException(); // у выписки индивидуального приказ по определению есть параграф и приказ. Для неиндивидуальных использовать этот метод нельзя.

        String orderNumber = getParagraph().getOrder().getNumber();
        orderNumber = (orderNumber != null) ? " №" + orderNumber : "";
        return "Индивидуальный приказ «" + getType().getTitle() + "»" + orderNumber + " | " + getEntity().getPerson().getFullFio();
    }

    public String getBasicListStr()
    {
        List<StuExtractToBasicRelation> list = MoveStudentDaoFacade.getMoveStudentDao().getPrioritySortedExtractBasicsList(this);
        String[] title = new String[list.size()];
        for (int i = 0; i < list.size(); i++)
            title[i] = list.get(i).getBasic().getTitle() + (StringUtils.isEmpty(list.get(i).getComment()) ? "" : " " + list.get(i).getComment());
        return StringUtils.join(title, ", ");
    }

    public String getReasonFullTitle()
    {
        if (null == getReason()) return null;
        return getReason().getTitle() + (null != getReasonComment() ? (" " + getReasonComment()) : "");
    }

    @Override
    public boolean canBePrinted()
    {
        return true;
    }

    @Override
    public void doExtractPrint(boolean printPdf)
    {
        if (canBePrinted())
        {
            if (getParagraph() != null && MoveStudentDaoFacade.getMoveStudentDao().isModularOrderIndividual(getType()))
                ContextLocal.createDesktop(new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_PRINT, new ParametersMap().add("orderId", getParagraph().getOrder().getId()).add("printPdf", printPdf)), null);
            else
                ContextLocal.createDesktop(new ComponentActivator(IMoveStudentComponents.MODULAR_STUDENT_EXTRACT_PRINT, new ParametersMap().add("extractId", getId()).add("printPdf", printPdf)), null);
        }
        else
            throw new ApplicationException("Выписка не может быть распечатана.");
    }

    public boolean isIndividual()
    {
        return getParagraph() != null && MoveStudentDaoFacade.getMoveStudentDao().isModularOrderIndividual((StudentModularOrder) getParagraph().getOrder());
    }

    @Override
    public void doOrderPrint(boolean printPdf)
    {
        if (getParagraph() != null)
            ContextLocal.createDesktop(new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_PRINT, new ParametersMap().add("orderId", getParagraph().getOrder().getId()).add("printPdf", printPdf)), null);
        else
            throw new ApplicationException("Приказ не может быть распечатан.");
    }
}

