package ru.tandemservice.movestudent.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudent_2x6x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность finAidAssignStuListExtract

		// создано свойство protocolNumber
		{
			// создать колонку
			tool.createColumn("finaidassignstulistextract_t", new DBColumn("protocolnumber_p", DBType.createVarchar(255)));

		}

		// создано свойство responsibleForPayments
		{
			// создать колонку
			tool.createColumn("finaidassignstulistextract_t", new DBColumn("responsibleforpayments_id", DBType.LONG));

		}

		// создано свойство responsibleForAgreement
		{
			// создать колонку
			tool.createColumn("finaidassignstulistextract_t", new DBColumn("responsibleforagreement_id", DBType.LONG));

		}

		// создано свойство matchingDate
		{
			// создать колонку
			tool.createColumn("finaidassignstulistextract_t", new DBColumn("matchingdate_p", DBType.DATE));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность matAidAssignStuExtract

		// создано свойство protocolNumber
		{
			// создать колонку
			tool.createColumn("mataidassignstuextract_t", new DBColumn("protocolnumber_p", DBType.createVarchar(255)));

		}

		// создано свойство responsibleForPaymentsStr
		{
			// создать колонку
			tool.createColumn("mataidassignstuextract_t", new DBColumn("responsibleforpaymentsstr_p", DBType.createVarchar(255)));

		}

		// создано свойство responsibleForAgreementStr
		{
			// создать колонку
			tool.createColumn("mataidassignstuextract_t", new DBColumn("responsibleforagreementstr_p", DBType.createVarchar(255)));

		}

		// создано свойство responsibleForPayments
		{
			// создать колонку
			tool.createColumn("mataidassignstuextract_t", new DBColumn("responsibleforpayments_id", DBType.LONG));

		}

		// создано свойство responsibleForAgreement
		{
			// создать колонку
			tool.createColumn("mataidassignstuextract_t", new DBColumn("responsibleforagreement_id", DBType.LONG));

		}

		// создано свойство matchingDate
		{
			// создать колонку
			tool.createColumn("mataidassignstuextract_t", new DBColumn("matchingdate_p", DBType.DATE));

		}


    }
}