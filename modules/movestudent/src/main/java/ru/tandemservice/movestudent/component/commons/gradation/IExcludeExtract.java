/* $Id$ */
package ru.tandemservice.movestudent.component.commons.gradation;

import java.util.Date;

/**
 * Выписка об отчислении студента.
 *
 * @author Nikolay Fedorovskih
 * @since 15.12.2013
 */
public interface IExcludeExtract
{
    /**
     * Дата отчисления.
     */
    Date getExcludeDate();
}