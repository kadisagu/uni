/* $Id: DAO.java.vm 24710 2012-10-30 11:06:12Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e103.AddEdit;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.IMoveStudentDao;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AttestationPracticWithProbationStuExtract;
import ru.tandemservice.movestudent.entity.ISendPracticeExtract;
import ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup;
import ru.tandemservice.movestudent.utils.CachedSingleSelectTextModel;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.codes.StudentExtractGroupCodes;
import ru.tandemservice.unimove.IAbstractExtract;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 25.02.2013
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<AttestationPracticWithProbationStuExtract, Model> implements IDAO
{
    @Override
    protected AttestationPracticWithProbationStuExtract createNewInstance()
    {
        return new AttestationPracticWithProbationStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        IMoveStudentDao mvDao = MoveStudentDaoFacade.getMoveStudentDao();

        model.setPracticeTypeList(mvDao.getStudentPracticeTypes());
        model.setPracticeKindModel(new CachedSingleSelectTextModel(mvDao.getStudentPracticeKinds()));
        model.setCourseList(DevelopGridDAO.getCourseList());

        if (model.isAddForm())
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                    .fromEntity(StudentExtractTypeToGroup.class, "ttg")
                    .column(property("ttg", StudentExtractTypeToGroup.type().id()))
                    .where(eq(property("ttg", StudentExtractTypeToGroup.group().code()), value(StudentExtractGroupCodes.SEND_PRACTICE)));

            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(AbstractStudentExtract.class, "ext")
                    .top(1).column("ext.id")
                    .where(eq(property("ext", IAbstractExtract.P_COMMITTED), value(Boolean.TRUE)))
                    .where(eq(property("ext", IAbstractExtract.L_ENTITY), value(model.getExtract().getEntity())))
                    .where(in(property("ext", IAbstractExtract.L_TYPE), subBuilder.buildQuery()))
                    .order(property("ext", AbstractStudentExtract.paragraph().order().commitDate()), OrderDirection.desc);

            Long lastExtractId = builder.createStatement(getSession()).uniqueResult();

            ISendPracticeExtract lastExtract = null;
            if (lastExtractId != null)
            {
                AbstractStudentExtract extract = getNotNull(lastExtractId);
                if (extract instanceof ISendPracticeExtract)
                {
                    lastExtract = (ISendPracticeExtract) extract;
                }
            }

            if (null != lastExtract)
            {
                model.getExtract().setPracticeBeginDate(lastExtract.getPracticeBeginDateFromExtr());
                model.getExtract().setPracticeEndDate(lastExtract.getPracticeEndDateFromExtr());
                model.getExtract().setPracticeCourse(lastExtract.getPracticeCourseFromExtr());
                model.getExtract().setPracticeDurationStr(lastExtract.getPracticeDurationFromExtr());
                model.getExtract().setPracticePlace(lastExtract.getPracticePlaceFromExtr());
                model.getExtract().setPracticeOrderNum(((AbstractStudentExtract) lastExtract).getParagraph().getOrder().getNumber());
                model.getExtract().setPracticeOrderDate(((AbstractStudentExtract) lastExtract).getParagraph().getOrder().getCommitDate());
            }
            else
            {
                model.getExtract().setPracticeCourse(model.getExtract().getEntity().getCourse());
            }
        }
        else
        {
            model.setPracticeType(mvDao.extractPracticeType(model.getExtract().getPracticeType()));
            model.setPracticeKind(mvDao.extractPracticeKind(model.getExtract().getPracticeType()));
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        Date beginDate = model.getExtract().getPracticeBeginDate();
        Date endDate = model.getExtract().getPracticeEndDate();

        if (beginDate.after(endDate))
            errors.add("Дата окончания практики должна быть позже даты начала", "practiceBeginDate", "practiceEndDate");
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setPracticeType(MoveStudentDaoFacade.getMoveStudentDao().getFullPracticeKind(model.getPracticeType(), model.getPracticeKind()));
        super.update(model);
    }
}