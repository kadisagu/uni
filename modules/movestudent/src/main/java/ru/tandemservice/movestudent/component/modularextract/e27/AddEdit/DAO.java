/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e27.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 18.05.2009
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<ExcludeTransfStuExtract, Model>
{
    @Override
    protected ExcludeTransfStuExtract createNewInstance()
    {
        return new ExcludeTransfStuExtract();
    }
    
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }
    
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setStudentStatusNewStr(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED).getTitle());
    }

    @Override
    public void update(Model model)
    {
        if (!model.getExtract().isStopGrantsPaying())
            model.getExtract().setStopPayingDate(null);
        
        model.getExtract().setStudentStatusOld(model.getExtract().getEntity().getStatus());
        
        super.update(model);
    }
}