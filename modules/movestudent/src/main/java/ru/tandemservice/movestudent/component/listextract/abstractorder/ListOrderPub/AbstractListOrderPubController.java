/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderPub;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.sec.entity.Admin;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.dao.MoveDaoFacade;

import java.util.List;

/**
 * @author vip_delete
 * @since 06.05.2009
 */
public abstract class AbstractListOrderPubController<T extends StudentListOrder, IDAO extends IAbstractListOrderPubDAO<T, Model>, Model extends AbstractListOrderPubModel<T>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setActionsPage("ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderPub.ListOrderPubActions");
        model.setParamsPage("ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderPub.ListOrderPubParams");

        model.setOrder(getDao().getOrder(model.getOrderId()));

        String needComponent = MoveStudentUtils.getListOrderPubComponent(model.getOrder().getType());
        if (!component.getName().equals(needComponent))
        {
            deactivate(component);
            activateInRoot(component, new ComponentActivator(
                    needComponent,
                    new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getOrderId()).add("selectedTab", model.getSelectedTab())));
        }
        else
        {
            getDao().prepare(model);

            // настройка действует в рамках типа приказа
            model.setSearchListSettingsKey("StudentListOrder." + model.getOrder().getType().getCode() + ".");

            prepareListDataSource(component);
        }
    }

    // titleColumn

    protected static PublisherLinkColumn getTitleColumn()
    {
        return (PublisherLinkColumn) new PublisherLinkColumn("Название параграфа", AbstractListOrderPubModel.PAR_TITLE).setResolver(
                new DefaultPublisherLinkResolver()
                {
                    @Override
                    public String getComponentName(IEntity entity)
                    {
                        return ((DataWrapper) entity).get(AbstractListOrderPubModel.PAR_COMPONENT);
                    }
                }
        ).setFormatter(NoWrapFormatter.INSTANCE).setOrderable(false);
    }

    // paragraphTypeColumn

    protected static AbstractColumn getParagraphTypeColumn()
    {
        return new SimpleColumn("Тип параграфа", AbstractListOrderPubModel.PAR_TITLE).setClickable(false).setOrderable(false);
    }

    private void rebuildListDataSource(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        DynamicListDataSource<DataWrapper> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(getTitleColumn());
        dataSource.addColumn(getParagraphTypeColumn());

        prepareListDataSource(dataSource, component);

        dataSource.addColumn(new SimpleColumn("Кол-во студентов", AbstractListOrderPubModel.EXTRACT_COUNT).setClickable(false).setOrderable(false));
        if (!model.getOrder().isReadonly())
        {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditParagraph").setPermissionKey(model.getSecModel().getPermission("editParagraph")).setDisabledProperty(AbstractListOrderPubModel.READ_ONLY));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteParagraph", "Удалить «{0}»?", AbstractListOrderPubModel.PAR_TITLE).setPermissionKey(model.getSecModel().getPermission("deleteParagraph")).setDisabledProperty(AbstractListOrderPubModel.READ_ONLY));
        }
        model.setDataSource(dataSource);
    }

    // extend point

    protected abstract void prepareListDataSource(DynamicListDataSource dataSource, IBusinessComponent component);

    // Event Listeners

    public void onClickSendToCoordination(IBusinessComponent component)
    {
        IPrincipalContext principalContext = component.getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable) && !(principalContext instanceof Admin && Debug.isEnabled()))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");

        if (principalContext instanceof Admin)
            getDao().doSendToCoordination(getModel(component), null);
        else
            getDao().doSendToCoordination(getModel(component), (IPersistentPersonable) principalContext);
      rebuildListDataSource(component);
    }

    @SuppressWarnings("unchecked")
    public void onClickSendOrderToCoordinationExtended(IBusinessComponent component)
    {
        for (IListStudentOrderPubControllerExtension ext : (List<IListStudentOrderPubControllerExtension>) ApplicationRuntime.getBean(IListStudentOrderPubControllerExtension.LIST_STUDENT_ORDER_PUB_CONTROLLER_EXTENSIONS_BEAN_NAME))
        {
            ext.doSendToCoordinationAdditionalAction(component, getModel(component).getOrder());
        }
    }

    @SuppressWarnings("unchecked")
    public void onClickExecuteAdditionalAction(IBusinessComponent component)
    {
        for (IListStudentOrderPubControllerExtension ext : (List<IListStudentOrderPubControllerExtension>) ApplicationRuntime.getBean(IListStudentOrderPubControllerExtension.LIST_STUDENT_ORDER_PUB_CONTROLLER_EXTENSIONS_BEAN_NAME))
        {
            ext.doExecuteAdditionalAction(component, getModel(component).getOrder());
        }
    }

    public boolean isPdfEnable()
    {
        return MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED;
    }

    public void onClickEdit(IBusinessComponent component)
    {
        T order = getModel(component).getOrder();

        component.createDefaultChildRegion(new ComponentActivator(MoveStudentUtils.getListOrderAddEditComponent(order.getType()), new ParametersMap()
                .add("orgUnitId", null)
                .add("orderId", getModel(component).getOrder().getId())
        ));
    }

    public void onClickSetExecutor(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveStudentComponents.STUDENT_ORDER_SET_EXECUTOR, new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())
        ));
    }

    public void onClickAddParagraph(IBusinessComponent component)
    {
        T order = getModel(component).getOrder();
        component.createDefaultChildRegion(new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_PARAGRAPH_ADD, new ParametersMap()
                .add("orderId", order.getId())
        ));
    }

    public void onClickAddMultipleParagraph(IBusinessComponent component)
    {
        T order = getModel(component).getOrder();
        component.createDefaultChildRegion(new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_MULTIPLE_PARAGRAPH_ADD, new ParametersMap()
                .add("orderId", order.getId())
        ));
    }

    public void onClickEditParagraph(IBusinessComponent component)
    {
        StudentListParagraph paragraph = getDao().getNotNull(StudentListParagraph.class, (Long) component.getListenerParameter());

        component.createDefaultChildRegion(new ComponentActivator(MoveStudentUtils.getListParagraphAddEditComponent(paragraph.getAnyExtractType()), new ParametersMap()
                .add(AbstractListParagraphAddEditModel.PARAMETER_PARAGRAPH_ID, paragraph.getId())
        ));
    }

    public void onClickReject(IBusinessComponent component)
    {
        getDao().doReject(getModel(component));
        rebuildListDataSource(component);
    }

    public void onClickCommit(IBusinessComponent component)
    {
        getDao().doCommit(getModel(component));
        rebuildListDataSource(component);
    }

    public void onClickSendToFormative(IBusinessComponent component)
    {
        getDao().doSendToFormative(getModel(component));
        rebuildListDataSource(component);
    }

    public void onClickRollback(IBusinessComponent component)
    {
        getDao().doRollback(getModel(component));
        rebuildListDataSource(component);
    }

    public void onClickDeleteOrder(IBusinessComponent component)
    {
        deactivate(component);
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(getModel(component).getOrder());
    }

    public void onClickPrintOrder(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_ORDER_PRINT, new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())
        ));
    }

    public void onClickPrintOrderPdf(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_ORDER_PRINT, new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())
                .add("printPdf", Boolean.TRUE)
        ));
    }

    //печать выписок
    public void onClickPrintExtracts(IBusinessComponent component)
    {
        BusinessComponentUtils.downloadDocument(MoveStudentPrintModifierManager.instance().printDao().printMassOrder(getModel(component).getOrder()), true);
    }


    public void onClickDeleteParagraph(IBusinessComponent component)
    {
        getDao().deleteParagraph(getModel(component), (Long) component.getListenerParameter());
        rebuildListDataSource(component);
    }

    public void onClickEditTemplate(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.isCustom())
        {
            activateInRoot(component, new ComponentActivator(IMoveStudentComponents.CUSTOM_ORDER_TEMPLATE_EDIT, new ParametersMap()
                    .add(ru.tandemservice.movestudent.component.customorder.TemplateEdit.Model.OBJECT_ID_BIND, model.getOrderId())
            ));
        }
    }
}