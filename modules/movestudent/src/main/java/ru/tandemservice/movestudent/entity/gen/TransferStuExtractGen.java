package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.TransferStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об изменении пункта обучения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TransferStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.TransferStuExtract";
    public static final String ENTITY_NAME = "transferStuExtract";
    public static final int VERSION_HASH = 1252834983;
    private static IEntityMeta ENTITY_META;

    public static final String P_TRANSFER_DATE = "transferDate";
    public static final String P_DEADLINE = "deadline";

    private Date _transferDate;     // Дата изменения
    private Date _deadline;     // График сдачи разницы в учебных планах до

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата изменения.
     */
    public Date getTransferDate()
    {
        return _transferDate;
    }

    /**
     * @param transferDate Дата изменения.
     */
    public void setTransferDate(Date transferDate)
    {
        dirty(_transferDate, transferDate);
        _transferDate = transferDate;
    }

    /**
     * @return График сдачи разницы в учебных планах до.
     */
    public Date getDeadline()
    {
        return _deadline;
    }

    /**
     * @param deadline График сдачи разницы в учебных планах до.
     */
    public void setDeadline(Date deadline)
    {
        dirty(_deadline, deadline);
        _deadline = deadline;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TransferStuExtractGen)
        {
            setTransferDate(((TransferStuExtract)another).getTransferDate());
            setDeadline(((TransferStuExtract)another).getDeadline());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TransferStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TransferStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new TransferStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return obj.getTransferDate();
                case "deadline":
                    return obj.getDeadline();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "transferDate":
                    obj.setTransferDate((Date) value);
                    return;
                case "deadline":
                    obj.setDeadline((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                        return true;
                case "deadline":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return true;
                case "deadline":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return Date.class;
                case "deadline":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TransferStuExtract> _dslPath = new Path<TransferStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TransferStuExtract");
    }
            

    /**
     * @return Дата изменения.
     * @see ru.tandemservice.movestudent.entity.TransferStuExtract#getTransferDate()
     */
    public static PropertyPath<Date> transferDate()
    {
        return _dslPath.transferDate();
    }

    /**
     * @return График сдачи разницы в учебных планах до.
     * @see ru.tandemservice.movestudent.entity.TransferStuExtract#getDeadline()
     */
    public static PropertyPath<Date> deadline()
    {
        return _dslPath.deadline();
    }

    public static class Path<E extends TransferStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<Date> _transferDate;
        private PropertyPath<Date> _deadline;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата изменения.
     * @see ru.tandemservice.movestudent.entity.TransferStuExtract#getTransferDate()
     */
        public PropertyPath<Date> transferDate()
        {
            if(_transferDate == null )
                _transferDate = new PropertyPath<Date>(TransferStuExtractGen.P_TRANSFER_DATE, this);
            return _transferDate;
        }

    /**
     * @return График сдачи разницы в учебных планах до.
     * @see ru.tandemservice.movestudent.entity.TransferStuExtract#getDeadline()
     */
        public PropertyPath<Date> deadline()
        {
            if(_deadline == null )
                _deadline = new PropertyPath<Date>(TransferStuExtractGen.P_DEADLINE, this);
            return _deadline;
        }

        public Class getEntityClass()
        {
            return TransferStuExtract.class;
        }

        public String getEntityName()
        {
            return "transferStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
