package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.TransferBetweenTerritorialStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О переводе между территориальными подразделениями
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TransferBetweenTerritorialStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.TransferBetweenTerritorialStuExtract";
    public static final String ENTITY_NAME = "transferBetweenTerritorialStuExtract";
    public static final int VERSION_HASH = 1238707929;
    private static IEntityMeta ENTITY_META;

    public static final String P_CHANGE_EDU_PLAN = "changeEduPlan";
    public static final String P_CHANGE_EDU_PLAN_DEADLINE_DATE = "changeEduPlanDeadlineDate";

    private boolean _changeEduPlan;     // Разница в учебных планах
    private Date _changeEduPlanDeadlineDate;     // Дата ликвидации

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     */
    @NotNull
    public boolean isChangeEduPlan()
    {
        return _changeEduPlan;
    }

    /**
     * @param changeEduPlan Разница в учебных планах. Свойство не может быть null.
     */
    public void setChangeEduPlan(boolean changeEduPlan)
    {
        dirty(_changeEduPlan, changeEduPlan);
        _changeEduPlan = changeEduPlan;
    }

    /**
     * @return Дата ликвидации.
     */
    public Date getChangeEduPlanDeadlineDate()
    {
        return _changeEduPlanDeadlineDate;
    }

    /**
     * @param changeEduPlanDeadlineDate Дата ликвидации.
     */
    public void setChangeEduPlanDeadlineDate(Date changeEduPlanDeadlineDate)
    {
        dirty(_changeEduPlanDeadlineDate, changeEduPlanDeadlineDate);
        _changeEduPlanDeadlineDate = changeEduPlanDeadlineDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TransferBetweenTerritorialStuExtractGen)
        {
            setChangeEduPlan(((TransferBetweenTerritorialStuExtract)another).isChangeEduPlan());
            setChangeEduPlanDeadlineDate(((TransferBetweenTerritorialStuExtract)another).getChangeEduPlanDeadlineDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TransferBetweenTerritorialStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TransferBetweenTerritorialStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new TransferBetweenTerritorialStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "changeEduPlan":
                    return obj.isChangeEduPlan();
                case "changeEduPlanDeadlineDate":
                    return obj.getChangeEduPlanDeadlineDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "changeEduPlan":
                    obj.setChangeEduPlan((Boolean) value);
                    return;
                case "changeEduPlanDeadlineDate":
                    obj.setChangeEduPlanDeadlineDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "changeEduPlan":
                        return true;
                case "changeEduPlanDeadlineDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "changeEduPlan":
                    return true;
                case "changeEduPlanDeadlineDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "changeEduPlan":
                    return Boolean.class;
                case "changeEduPlanDeadlineDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TransferBetweenTerritorialStuExtract> _dslPath = new Path<TransferBetweenTerritorialStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TransferBetweenTerritorialStuExtract");
    }
            

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferBetweenTerritorialStuExtract#isChangeEduPlan()
     */
    public static PropertyPath<Boolean> changeEduPlan()
    {
        return _dslPath.changeEduPlan();
    }

    /**
     * @return Дата ликвидации.
     * @see ru.tandemservice.movestudent.entity.TransferBetweenTerritorialStuExtract#getChangeEduPlanDeadlineDate()
     */
    public static PropertyPath<Date> changeEduPlanDeadlineDate()
    {
        return _dslPath.changeEduPlanDeadlineDate();
    }

    public static class Path<E extends TransferBetweenTerritorialStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<Boolean> _changeEduPlan;
        private PropertyPath<Date> _changeEduPlanDeadlineDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferBetweenTerritorialStuExtract#isChangeEduPlan()
     */
        public PropertyPath<Boolean> changeEduPlan()
        {
            if(_changeEduPlan == null )
                _changeEduPlan = new PropertyPath<Boolean>(TransferBetweenTerritorialStuExtractGen.P_CHANGE_EDU_PLAN, this);
            return _changeEduPlan;
        }

    /**
     * @return Дата ликвидации.
     * @see ru.tandemservice.movestudent.entity.TransferBetweenTerritorialStuExtract#getChangeEduPlanDeadlineDate()
     */
        public PropertyPath<Date> changeEduPlanDeadlineDate()
        {
            if(_changeEduPlanDeadlineDate == null )
                _changeEduPlanDeadlineDate = new PropertyPath<Date>(TransferBetweenTerritorialStuExtractGen.P_CHANGE_EDU_PLAN_DEADLINE_DATE, this);
            return _changeEduPlanDeadlineDate;
        }

        public Class getEntityClass()
        {
            return TransferBetweenTerritorialStuExtract.class;
        }

        public String getEntityName()
        {
            return "transferBetweenTerritorialStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
