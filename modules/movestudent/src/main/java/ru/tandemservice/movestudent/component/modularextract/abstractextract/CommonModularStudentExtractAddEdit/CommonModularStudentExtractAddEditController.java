/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit;

import org.tandemframework.core.component.IBusinessComponent;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractAddEdit.ModularStudentExtractAddEditController;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;

/**
 * @author vip_delete
 * @since 06.11.2008
 */
public abstract class CommonModularStudentExtractAddEditController<T extends ModularStudentExtract, IDAO extends ICommonModularStudentExtractAddEditDAO<T, Model>, Model extends CommonModularStudentExtractAddEditModel<T>> extends ModularStudentExtractAddEditController<T, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
    }

    public void onChangeTargetAdmissionVisible(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.isChangeTargetAdmission()) {
            ((CommonStuExtract) model.getExtract()).setTargetAdmissionNew(!model.getExtract().getEntity().isTargetAdmission());
        }
    }
}
