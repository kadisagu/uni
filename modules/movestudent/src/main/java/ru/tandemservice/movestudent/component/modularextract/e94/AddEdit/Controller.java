/* $Id: Controller.java.vm 11769 2010-03-05 07:11:49Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e94.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.movestudent.entity.WeekendChildThreeStuExtract;
import ru.tandemservice.uni.UniDefines;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 16.01.2013
 */
public class Controller extends CommonModularStudentExtractAddEditController<WeekendChildThreeStuExtract, IDAO, Model>
{
    public void onChangeDate(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getExtract().setPayBenefitText(DAO.getBenefitTextThreeYear(model.getExtract().getPayBenefitBeginDate(), model.getExtract().getPayBenefitEndDate()));
    }

    public void onChangeStudentStatus(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (null != model.getExtract().getStudentStatusNew())
        {
            if (UniDefines.CATALOG_STUDENT_STATUS_CHILD_WITHOUT_ATTENDANCE.equals(model.getExtract().getStudentStatusNew().getCode()))
            {
                model.getExtract().setFreeAttendance(false);
            }
        }
    }
}
