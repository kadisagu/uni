/* $Id$ */
package ru.tandemservice.movestudent.utils.system.advgenerators;

/**
 * Виды каскадных удалений
 *
 * @author Dmitry Seleznev
 * @since 10.08.2012
 */
public enum CascadeTypes
{
    DELETE, // Удаление объекта, ссылающегося на тип, помеченный в поле на каскадное удаление этого типа
    DELETE_ORPHAN // Зануление соответствующего поля у объекта, ссылающегося на тип, помеченный в поле на каскадное удаление этого типа
}