\keep\keepn\qj\b {extractNumber}. ВЫДАТЬ:\b0\par
{student_D} {fio} {birthDate} дата рождения, {course} курса {orgUnit_G} {developForm_GF} формы обучения {compensationTypeStr_G}, выдать дубликат зачетной книжки № {bookNumber} взамен утерянной.\par
  Основание: {listBasics}.