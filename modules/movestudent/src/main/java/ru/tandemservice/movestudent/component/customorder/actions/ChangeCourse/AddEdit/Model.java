/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeCourse.AddEdit;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditModel;
import ru.tandemservice.movestudent.entity.custom.ChangeCourseAction;
import ru.tandemservice.uni.entity.catalog.Course;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public class Model extends BaseCustomActionAddEditModel<ChangeCourseAction>
{
    private List<Course> _newCourseList;

    public List<Course> getNewCourseList()
    {
        return _newCourseList;
    }

    public void setNewCourseList(List<Course> newCourseList)
    {
        _newCourseList = newCourseList;
    }
}