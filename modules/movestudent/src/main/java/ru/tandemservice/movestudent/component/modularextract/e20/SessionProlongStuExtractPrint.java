/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e20;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.SessionProlongStuExtract;
import ru.tandemservice.uni.UniDefines;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 07.05.2009
 */
public class SessionProlongStuExtractPrint implements IPrintFormCreator<SessionProlongStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, SessionProlongStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        if (extract.getEntity().getPerson().getIdentityCard().getSex().isMale())
        {
            modifier.put("applyed", "обратился");
        }
        else
        {
            modifier.put("applyed", "обратилась");
        }

        modifier.put("textReason", extract.getProlongReason());
        if (UniDefines.SESSION_TYPE_WINTER.equals(extract.getSessionType().getCode()))
        {
            modifier.put("sessionType_I", "зимней");
            modifier.put("sessionType_A", "зимнюю");
        }
        else if(UniDefines.SESSION_TYPE_SUMMER.equals(extract.getSessionType().getCode()))
        {
            modifier.put("sessionType_I", "летней");
            modifier.put("sessionType_A", "летнюю");
        }
        else
        {
            modifier.put("sessionType_I", "весенней");
            modifier.put("sessionType_A", "весеннюю");
        }
            
        modifier.put("dateStart", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getProlongDateFrom()));
        modifier.put("dateFinish", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getProlongDateTo()));

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}