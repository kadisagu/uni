/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub;

import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.sec.entity.Admin;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.base.bo.OrderComment.ui.Edit.OrderCommentEdit;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.ui.formatters.RuMoneyFormatter;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.List;

/**
 * @author vip_delete
 * @since 18.11.2008
 */
public abstract class ModularStudentExtractPubController<T extends ModularStudentExtract, IDAO extends IModularStudentExtractPubDAO<T, Model>, Model extends ModularStudentExtractPubModel<T>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setDebugMode(Boolean.parseBoolean(ApplicationRuntime.getProperty("uni.god.mode")));

        model.setAttributesPage(getClass().getPackage().getName() + ".Attributes");
    }

    public boolean isPdfEnable()
    {
        return MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED;
    }

    // event listeners

    public final synchronized void onClickCommit(IBusinessComponent component)
    {
        Model model = getModel(component);
        MoveDaoFacade.getMoveDao().doCommitExtract(model.getExtract(), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED), null);
    }

    public final synchronized void onClickRollback(IBusinessComponent component)
    {
        getDao().doRollback(getModel(component));
    }

    public void onClickEdit(IBusinessComponent component)
    {
        Long extractId = getModel(component).getExtractId();
        component.createChildRegion("extractTabPanel", new ComponentActivator(MoveStudentUtils.getModularExtractAddEditComponentNew(extractId), new ParametersMap()
                .add("extractId", extractId)
        ));
    }

    public void onClickEditOrder(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_EDIT, new ParametersMap()
                .add("orderId", getModel(component).getExtract().getParagraph().getOrder().getId()))
        );
    }

    public void onClickSetExecutor(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveStudentComponents.STUDENT_ORDER_SET_EXECUTOR, new ParametersMap()
                .add("orderId", getModel(component).getExtract().getParagraph().getOrder().getId())
        ));
    }

    public void onClickSendToCoordination(IBusinessComponent component)
    {
        IPrincipalContext principalContext = component.getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable) && !(principalContext instanceof Admin && Debug.isEnabled()))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");
        if (principalContext instanceof Admin)
            getDao().doSendToCoordination(getModel(component), null);
        else
            getDao().doSendToCoordination(getModel(component), (IPersistentPersonable) principalContext);
    }

    public void onClickSendToFormative(IBusinessComponent component)
    {
        getDao().doSendToFormative(getModel(component));
    }

    public void onClickSendOrderToCoordination(IBusinessComponent component)
    {
        IPrincipalContext principalContext = component.getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable) && !(principalContext instanceof Admin && Debug.isEnabled()))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");
        if (principalContext instanceof Admin)
            getDao().doSendOrderToCoordination(getModel(component), null);
        else
            getDao().doSendOrderToCoordination(getModel(component), (IPersistentPersonable) principalContext);
    }

    @SuppressWarnings("unchecked")
    public void onClickSendOrderToCoordinationExtended(IBusinessComponent component)
    {
        if (null != getModel(component).getExtract().getParagraph())
        {
            for (IModularStudentExtractPubControllerExtension ext : (List<IModularStudentExtractPubControllerExtension>) ApplicationRuntime.getBean(IModularStudentExtractPubControllerExtension.MODULAR_STUDENT_EXTRACT_PUB_CONTROLLER_EXTENSIONS_BEAN_NAME))
            {
                ext.doSendToCoordinationAdditionalAction(component, getModel(component).getExtract().getParagraph().getOrder());
            }
        }
    }

    @SuppressWarnings("unchecked")
    public void onClickExecuteAdditionalAction(IBusinessComponent component)
    {
        if (null != getModel(component).getExtract().getParagraph())
        {
            for (IModularStudentExtractPubControllerExtension ext : (List<IModularStudentExtractPubControllerExtension>) ApplicationRuntime.getBean(IModularStudentExtractPubControllerExtension.MODULAR_STUDENT_EXTRACT_PUB_CONTROLLER_EXTENSIONS_BEAN_NAME))
            {
                ext.doExecuteAdditionalAction(component, getModel(component).getExtract().getParagraph().getOrder());
            }
        }
    }

    public void onClickSendOrderToFormative(IBusinessComponent component)
    {
        getDao().doSendOrderToFormative(getModel(component));
    }

    public void onClickRejectOrder(IBusinessComponent component)
    {
        getDao().doRejectOrder(getModel(component));
    }

    public final synchronized void onClickCommitOrder(IBusinessComponent component)
    {
        getDao().doCommitOrder(getModel(component));
    }

    public final synchronized void onClickRollbackOrder(IBusinessComponent component)
    {
        getDao().doRollbackOrder(getModel(component));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.isIndividualOrder() && null != model.getExtract().getParagraph())
            MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(model.getExtract().getParagraph().getOrder());
        else
            MoveDaoFacade.getMoveDao().deleteExtractWithParagraph(getModel(component).getExtract());
        deactivate(component);
    }

    public void onClickPrint(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.isIndividualOrder())
        {
            activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_PRINT, new ParametersMap()
                    .add("orderId", getModel(component).getExtract().getParagraph().getOrder().getId())
            ));
        }
        else
        {
            activateInRoot(component, new ComponentActivator(IMoveStudentComponents.MODULAR_STUDENT_EXTRACT_PRINT, new ParametersMap()
                    .add("extractId", getModel(component).getExtractId())
            ));
        }
    }

    public void onClickPrintPdf(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.isIndividualOrder())
        {
            activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_PRINT, new ParametersMap()
                    .add("orderId", model.getExtract().getParagraph().getOrder().getId())
                    .add("printPdf", Boolean.TRUE)
            ));
        }
        else
        {
            activateInRoot(component, new ComponentActivator(IMoveStudentComponents.MODULAR_STUDENT_EXTRACT_PRINT, new ParametersMap()
                    .add("extractId", model.getExtractId())
                    .add("printPdf", Boolean.TRUE)
            ));
        }
    }

    public void onClickEditTemplate(IBusinessComponent component)
    {
        Model model = getModel(component);
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.CUSTOM_ORDER_TEMPLATE_EDIT, new ParametersMap()
                .add(ru.tandemservice.movestudent.component.customorder.TemplateEdit.Model.OBJECT_ID_BIND, model.getExtract().getId())
        ));
    }

    public void onClickEditComment(IBusinessComponent component)
    {
        Model model = getModel(component);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(OrderCommentEdit.class.getSimpleName(),
            new ParametersMap().add("extractId",  model.getExtractId())));
    }

    /**
     * Перевод денежных сумм из double в формат 00 руб. 00 коп
     *
     * @param value - значение суммы
     */
    public String getMoneyFromDouble(Double value)
    {
        RuMoneyFormatter formatter = new RuMoneyFormatter();
        return formatter.format(value);
    }
}