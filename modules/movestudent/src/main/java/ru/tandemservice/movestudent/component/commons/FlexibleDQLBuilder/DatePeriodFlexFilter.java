/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.util.mq.UniDQLExpressions;

import java.util.Date;

/**
 * @author Nikolay Fedorovskih
 * @since 27.11.2013
 */
public class DatePeriodFlexFilter implements IFlexFilter
{
    private String _dateFromSettingName;
    private String _dateToSettingName;
    private String _propertyPath;
    private String _alias;

    public DatePeriodFlexFilter(String dateFromSettingName, String dateToSettingName, String alias, String propertyPath)
    {
        _dateFromSettingName = dateFromSettingName;
        _dateToSettingName = dateToSettingName;
        _propertyPath = propertyPath;
        _alias = alias;
    }

    public String getAlias()
    {
        return _alias;
    }

    @Override
    public boolean addToBuilder(IDataSettings dataSettings, DQLSelectBuilder builder, IJoinCreator joinCreator)
    {
        Date dateFrom = dataSettings.get(_dateFromSettingName);
        Date dateTo = dataSettings.get(_dateToSettingName);

        if (dateFrom == null && dateTo == null)
            return false;

        joinCreator.checkAlias(_alias);
        if (dateFrom != null)
        {
            if (dateTo != null)
                builder.where(DQLExpressions.betweenDays(_alias + "." + _propertyPath, dateFrom, dateTo));
            else
                builder.where(UniDQLExpressions.greatOrEq(_alias, _propertyPath, dateFrom));
        }
        else
        {
            builder.where(UniDQLExpressions.lessOrEq(_alias, _propertyPath, dateTo));
        }
        return true;
    }
}