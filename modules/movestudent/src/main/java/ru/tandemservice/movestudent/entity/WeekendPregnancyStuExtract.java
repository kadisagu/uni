package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IAssignPaymentExtract;
import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.WeekendPregnancyStuExtractGen;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.math.BigDecimal;
import java.util.Date;

public class WeekendPregnancyStuExtract extends WeekendPregnancyStuExtractGen implements ITransferExtract, IAssignPaymentExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getBeginDate();
    }

    @Override
    public Group getGroupOld()
    {
        return getEntity().getGroup();
    }

    @Override
    public Group getGroupNew()
    {
        return null;
    }

    @Override
    public Date getPaymentBeginDate()
    {
        return getParagraph() != null ? getParagraph().getOrder().getCommitDate() : null;
    }

    @Override
    public Date getPaymentEndDate()
    {
        return null;
    }

    @Override
    public BigDecimal getPaymentAmount()
    {
        return null;
    }
}