/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.modularextract.AddEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.IComponentRegion;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.customorder.CustomOrderUtils;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditModel;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.IBaseCustomActionAddEditDAO;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDao;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.movestudent.entity.custom.ModifyModularStuExtract;
import ru.tandemservice.movestudent.entity.custom.OrderActionType;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;
import ru.tandemservice.unimove.entity.ICustomOrderAction;
import ru.tandemservice.unimove.entity.gen.ICustomOrderActionGen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 28.08.2013
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<ModifyModularStuExtract, Model>
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        // Получаем список возможных к выбору классов действий
        model.setPossibleActionComponents(MoveStudentDaoFacade.getMoveStudentDao().getPossibleCustomActionComponents(false));

        List<OrderActionType> actionTypes = new ArrayList<>();
        actionTypes.addAll(Arrays.asList(OrderActionType.values()));
        if (!StudentExtractTypeCodes.CUSTOM_REVERT_MODULAR_ORDER.equals(model.getExtractType().getCode()))
            actionTypes.remove(OrderActionType.DO_REVERT);
        model.setActionTypeList(actionTypes);

        model.setModifiedExtractModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                        new DQLSelectBuilder()
                                .fromEntity(AbstractStudentExtract.class, "e")
                                .joinPath(DQLJoinType.inner, AbstractStudentExtract.paragraph().fromAlias("e"), "p")
                                .joinPath(DQLJoinType.inner, AbstractStudentParagraph.order().fromAlias("p"), "o")
                                .where(eq(property("e", AbstractStudentExtract.entity()), value(model.getExtract().getEntity())))
                                .where(eq(property("e", AbstractStudentExtract.committed()), value(Boolean.TRUE)))
                                .order(property("o", AbstractStudentOrder.commitDateSystem()), OrderDirection.desc)
                );

                int extract_id_col = dql.column(property("e.id"));
                int order_number_col = dql.column(property("o", AbstractStudentOrder.number()));
                int commit_date_col = dql.column(property("o", AbstractStudentOrder.commitDate()));
                int type_title_col = dql.column(property("e", AbstractStudentExtract.type().title()));

                if (filter != null && !filter.isEmpty())
                {
                    dql.getDql().where(likeUpper(DQLFunctions.concat(property("o", AbstractStudentOrder.number()),
                                                                     property("e", AbstractStudentExtract.type().title())),
                                                 value(CoreStringUtils.escapeLike(filter, true))
                    ));
                }

                dql.getDql().top(MoveStudentDao.MAX_ROWS);

                List<Object[]> items = dql.getDql().createStatement(getSession()).list();
                List<DataWrapper> resultList = new ArrayList<>(items.size());
                for (Object[] item : items)
                {
                    Long id = (Long) item[extract_id_col];
                    String title = CustomOrderUtils.getOrderTitle((String) item[order_number_col], (Date) item[commit_date_col], (String) item[type_title_col]);
                    resultList.add(new DataWrapper(id, title));
                }

                return new ListResult<>(resultList);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                if (value instanceof AbstractStudentExtract)
                    return CustomOrderUtils.getOrderTitle((AbstractStudentExtract) value);
                return super.getLabelFor(value, columnIndex);
            }
        });
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(final Model model)
    {
        super.update(model);
        if (OrderActionType.DO_CUSTOM_ACTION == model.getExtract().getOrderActionType())
        {
            boolean hasSelectedActions = false;
            for (String regionName : model.getPossibleActionComponents().keySet())
            {
                IComponentRegion region = model.getContext().getChildRegion(regionName);
                IBusinessComponent component = region.getActiveComponent();
                IBaseCustomActionAddEditDAO dao = (IBaseCustomActionAddEditDAO) component.getController().getDao();
                BaseCustomActionAddEditModel actionModel = component.getModel();
                dao.updateForExtract(actionModel, model.getExtract(), model.isEditForm());
                if (actionModel.isActionActive())
                    hasSelectedActions = true;
            }
            if (!hasSelectedActions)
            {
                throw new ApplicationException("Не выбрано ни одного действия для актуализации данных студента.");
            }
        }
        else
        {
            if (model.isEditForm())
            {
                for (ICustomOrderAction action : getList(ICustomOrderAction.class, ICustomOrderActionGen.customExtract().id(), model.getExtract().getId()))
                {
                    delete(action);
                }
            }
            CustomOrderUtils.checkRevertAvailable(model.getExtract());
        }
    }

    @Override
    protected ModifyModularStuExtract createNewInstance()
    {
        return new ModifyModularStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }
}