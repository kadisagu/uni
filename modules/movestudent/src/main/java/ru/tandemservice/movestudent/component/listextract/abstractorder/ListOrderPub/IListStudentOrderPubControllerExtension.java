/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderPub;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;

/**
 * @author Dmitry Seleznev
 * @since 18.07.2013
 */
public interface IListStudentOrderPubControllerExtension
{
    String LIST_STUDENT_ORDER_PUB_CONTROLLER_EXTENSIONS_BEAN_NAME = "listStudentOrderPubControllerExtensions";

    void doSendToCoordinationAdditionalAction(IBusinessComponent component, AbstractStudentOrder order);

    void doExecuteAdditionalAction(IBusinessComponent component, AbstractStudentOrder order);
}