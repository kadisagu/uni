package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.TransferBetweenTerritorialStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О переводе между территориальными подразделениями
 */
public class TransferBetweenTerritorialStuExtract extends TransferBetweenTerritorialStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}