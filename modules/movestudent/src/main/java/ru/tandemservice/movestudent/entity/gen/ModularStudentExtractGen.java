package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного (индивидуального) приказа по студентам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ModularStudentExtractGen extends AbstractStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ModularStudentExtract";
    public static final String ENTITY_NAME = "modularStudentExtract";
    public static final int VERSION_HASH = -317492509;
    private static IEntityMeta ENTITY_META;

    public static final String L_REASON = "reason";
    public static final String P_REASON_COMMENT = "reasonComment";

    private StudentOrderReasons _reason;     // Причина выписки
    private String _reasonComment;     // Примечание к причине

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Причина выписки. Свойство не может быть null.
     */
    @NotNull
    public StudentOrderReasons getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина выписки. Свойство не может быть null.
     */
    public void setReason(StudentOrderReasons reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Примечание к причине.
     */
    @Length(max=255)
    public String getReasonComment()
    {
        return _reasonComment;
    }

    /**
     * @param reasonComment Примечание к причине.
     */
    public void setReasonComment(String reasonComment)
    {
        dirty(_reasonComment, reasonComment);
        _reasonComment = reasonComment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ModularStudentExtractGen)
        {
            setReason(((ModularStudentExtract)another).getReason());
            setReasonComment(((ModularStudentExtract)another).getReasonComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ModularStudentExtractGen> extends AbstractStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ModularStudentExtract.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("ModularStudentExtract is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                    return obj.getReason();
                case "reasonComment":
                    return obj.getReasonComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "reason":
                    obj.setReason((StudentOrderReasons) value);
                    return;
                case "reasonComment":
                    obj.setReasonComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                        return true;
                case "reasonComment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                    return true;
                case "reasonComment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                    return StudentOrderReasons.class;
                case "reasonComment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ModularStudentExtract> _dslPath = new Path<ModularStudentExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ModularStudentExtract");
    }
            

    /**
     * @return Причина выписки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ModularStudentExtract#getReason()
     */
    public static StudentOrderReasons.Path<StudentOrderReasons> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Примечание к причине.
     * @see ru.tandemservice.movestudent.entity.ModularStudentExtract#getReasonComment()
     */
    public static PropertyPath<String> reasonComment()
    {
        return _dslPath.reasonComment();
    }

    public static class Path<E extends ModularStudentExtract> extends AbstractStudentExtract.Path<E>
    {
        private StudentOrderReasons.Path<StudentOrderReasons> _reason;
        private PropertyPath<String> _reasonComment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Причина выписки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ModularStudentExtract#getReason()
     */
        public StudentOrderReasons.Path<StudentOrderReasons> reason()
        {
            if(_reason == null )
                _reason = new StudentOrderReasons.Path<StudentOrderReasons>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Примечание к причине.
     * @see ru.tandemservice.movestudent.entity.ModularStudentExtract#getReasonComment()
     */
        public PropertyPath<String> reasonComment()
        {
            if(_reasonComment == null )
                _reasonComment = new PropertyPath<String>(ModularStudentExtractGen.P_REASON_COMMENT, this);
            return _reasonComment;
        }

        public Class getEntityClass()
        {
            return ModularStudentExtract.class;
        }

        public String getEntityName()
        {
            return "modularStudentExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
