/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.ui.CodesGupMsr;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.MovestudentMSRSettingsManager;


/**
 * @author Ekaterina Zvereva
 * @since 18.12.2014
 */
@Configuration
public class MovestudentMSRSettingsCodesGupMsr extends BusinessComponentManager
{

    public static String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(FORMATIVE_ORG_UNIT_DS, formativeDSColumn(), MovestudentMSRSettingsManager.instance().createOrgUnitDataSource()))
                .create();
    }

    @Bean
    public ColumnListExtPoint formativeDSColumn()
    {
        return columnListExtPointBuilder(FORMATIVE_ORG_UNIT_DS)
                .addColumn(textColumn("orgUnit", OrgUnit.titleWithType()))
                .addColumn(blockColumn("codesFaculty", "codesFaculty"))
                .create();
    }




}
