/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e10;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.GraduateStuListExtract;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author ListExtractComponentGenerator
 * @since 17.06.2009
 */
public class GraduateStuListExtractPrint implements IPrintFormCreator<GraduateStuListExtract>, IListParagraphPrintFormCreator<GraduateStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, GraduateStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, GraduateStuListExtract firstExtract)
    {

    }

    public static String getDevelopFormFullStr_G(DevelopForm developForm)
    {
        switch (developForm.getCode())
        {
            case DevelopFormCodes.FULL_TIME_FORM:
                return "очного отделения";
            case DevelopFormCodes.CORESP_FORM:
                return "заочного отделения";
            case DevelopFormCodes.PART_TIME_FORM:
                return "очно-заочного отделения";
            case DevelopFormCodes.EXTERNAL_FORM:
                return "отделения экстернат";
            case DevelopFormCodes.APPLICANT_FORM:
                return "самостоятельного обучения";
            default:
                return "";
        }
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, GraduateStuListExtract firstExtract)
    {
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract)
                .put("developForm_G", getDevelopFormFullStr_G(firstExtract.getEntity().getEducationOrgUnit().getDevelopForm()));
        return CommonExtractPrint.addEntryDateToModifier(injectModifier, firstExtract.getGraduateDate());
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, GraduateStuListExtract firstExtract)
    {
        return null;
    }
}