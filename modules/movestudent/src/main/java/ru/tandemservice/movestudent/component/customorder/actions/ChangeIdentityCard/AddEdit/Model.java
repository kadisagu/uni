/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeIdentityCard.AddEdit;

import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditModel;
import ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public class Model extends BaseCustomActionAddEditModel<ChangeIdentityCardAction>
{
    private ISelectModel _citizenshipModel;
    private List<IdentityCardType> _identityCardTypeList;
    private ISelectModel _nationalityModel;
    private List<Sex> _sexList;

    public ISelectModel getCitizenshipModel()
    {
        return _citizenshipModel;
    }

    public void setCitizenshipModel(ISelectModel citizenshipModel)
    {
        _citizenshipModel = citizenshipModel;
    }

    public List<IdentityCardType> getIdentityCardTypeList()
    {
        return _identityCardTypeList;
    }

    public void setIdentityCardTypeList(List<IdentityCardType> identityCardTypeList)
    {
        _identityCardTypeList = identityCardTypeList;
    }

    public ISelectModel getNationalityModel()
    {
        return _nationalityModel;
    }

    public void setNationalityModel(ISelectModel nationalityModel)
    {
        _nationalityModel = nationalityModel;
    }

    public List<Sex> getSexList()
    {
        return _sexList;
    }

    public void setSexList(List<Sex> sexList)
    {
        _sexList = sexList;
    }
}