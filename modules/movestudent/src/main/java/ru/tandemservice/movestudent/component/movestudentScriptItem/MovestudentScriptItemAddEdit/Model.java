/* $Id$ */
package ru.tandemservice.movestudent.component.movestudentScriptItem.MovestudentScriptItemAddEdit;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogAddEdit.DefaultScriptCatalogAddEditModel;

/**
 * @author Vasily Zhukov
 * @since 22.12.2011
 */
@Input({
        @Bind(key = "useUserTemplate", binding = "useUserTemplate"),
        @Bind(key = "useUserScript", binding = "useUserScript")
})
public class Model<T extends ICatalogItem & IScriptItem> extends DefaultScriptCatalogAddEditModel<T>
{
}