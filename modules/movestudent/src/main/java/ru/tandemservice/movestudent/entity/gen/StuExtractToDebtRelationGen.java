package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь выписки с задолженностью, подлежащей ликвидации
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StuExtractToDebtRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.StuExtractToDebtRelation";
    public static final String ENTITY_NAME = "stuExtractToDebtRelation";
    public static final int VERSION_HASH = -1243276540;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String P_DISCIPLINE = "discipline";
    public static final String P_HOURS = "hours";
    public static final String P_CONTROL_ACTION = "controlAction";

    private ModularStudentExtract _extract;     // Выписка из сборного (индивидуального) приказа по студентам
    private String _discipline;     // Предмет
    private int _hours;     // Часы
    private String _controlAction;     // Контрольное мероприятие

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного (индивидуального) приказа по студентам.
     */
    public ModularStudentExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка из сборного (индивидуального) приказа по студентам.
     */
    public void setExtract(ModularStudentExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Предмет. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Предмет. Свойство не может быть null.
     */
    public void setDiscipline(String discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Часы. Свойство не может быть null.
     */
    @NotNull
    public int getHours()
    {
        return _hours;
    }

    /**
     * @param hours Часы. Свойство не может быть null.
     */
    public void setHours(int hours)
    {
        dirty(_hours, hours);
        _hours = hours;
    }

    /**
     * @return Контрольное мероприятие. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getControlAction()
    {
        return _controlAction;
    }

    /**
     * @param controlAction Контрольное мероприятие. Свойство не может быть null.
     */
    public void setControlAction(String controlAction)
    {
        dirty(_controlAction, controlAction);
        _controlAction = controlAction;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StuExtractToDebtRelationGen)
        {
            setExtract(((StuExtractToDebtRelation)another).getExtract());
            setDiscipline(((StuExtractToDebtRelation)another).getDiscipline());
            setHours(((StuExtractToDebtRelation)another).getHours());
            setControlAction(((StuExtractToDebtRelation)another).getControlAction());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StuExtractToDebtRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StuExtractToDebtRelation.class;
        }

        public T newInstance()
        {
            return (T) new StuExtractToDebtRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "discipline":
                    return obj.getDiscipline();
                case "hours":
                    return obj.getHours();
                case "controlAction":
                    return obj.getControlAction();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((ModularStudentExtract) value);
                    return;
                case "discipline":
                    obj.setDiscipline((String) value);
                    return;
                case "hours":
                    obj.setHours((Integer) value);
                    return;
                case "controlAction":
                    obj.setControlAction((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "discipline":
                        return true;
                case "hours":
                        return true;
                case "controlAction":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "discipline":
                    return true;
                case "hours":
                    return true;
                case "controlAction":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return ModularStudentExtract.class;
                case "discipline":
                    return String.class;
                case "hours":
                    return Integer.class;
                case "controlAction":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StuExtractToDebtRelation> _dslPath = new Path<StuExtractToDebtRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StuExtractToDebtRelation");
    }
            

    /**
     * @return Выписка из сборного (индивидуального) приказа по студентам.
     * @see ru.tandemservice.movestudent.entity.StuExtractToDebtRelation#getExtract()
     */
    public static ModularStudentExtract.Path<ModularStudentExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Предмет. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToDebtRelation#getDiscipline()
     */
    public static PropertyPath<String> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Часы. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToDebtRelation#getHours()
     */
    public static PropertyPath<Integer> hours()
    {
        return _dslPath.hours();
    }

    /**
     * @return Контрольное мероприятие. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToDebtRelation#getControlAction()
     */
    public static PropertyPath<String> controlAction()
    {
        return _dslPath.controlAction();
    }

    public static class Path<E extends StuExtractToDebtRelation> extends EntityPath<E>
    {
        private ModularStudentExtract.Path<ModularStudentExtract> _extract;
        private PropertyPath<String> _discipline;
        private PropertyPath<Integer> _hours;
        private PropertyPath<String> _controlAction;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного (индивидуального) приказа по студентам.
     * @see ru.tandemservice.movestudent.entity.StuExtractToDebtRelation#getExtract()
     */
        public ModularStudentExtract.Path<ModularStudentExtract> extract()
        {
            if(_extract == null )
                _extract = new ModularStudentExtract.Path<ModularStudentExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Предмет. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToDebtRelation#getDiscipline()
     */
        public PropertyPath<String> discipline()
        {
            if(_discipline == null )
                _discipline = new PropertyPath<String>(StuExtractToDebtRelationGen.P_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Часы. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToDebtRelation#getHours()
     */
        public PropertyPath<Integer> hours()
        {
            if(_hours == null )
                _hours = new PropertyPath<Integer>(StuExtractToDebtRelationGen.P_HOURS, this);
            return _hours;
        }

    /**
     * @return Контрольное мероприятие. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToDebtRelation#getControlAction()
     */
        public PropertyPath<String> controlAction()
        {
            if(_controlAction == null )
                _controlAction = new PropertyPath<String>(StuExtractToDebtRelationGen.P_CONTROL_ACTION, this);
            return _controlAction;
        }

        public Class getEntityClass()
        {
            return StuExtractToDebtRelation.class;
        }

        public String getEntityName()
        {
            return "stuExtractToDebtRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
