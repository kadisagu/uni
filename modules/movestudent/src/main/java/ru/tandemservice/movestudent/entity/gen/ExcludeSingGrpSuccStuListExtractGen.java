package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ExcludeSingGrpSuccStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «Об отчислении для одной группы, диплом с отличием»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExcludeSingGrpSuccStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ExcludeSingGrpSuccStuListExtract";
    public static final String ENTITY_NAME = "excludeSingGrpSuccStuListExtract";
    public static final int VERSION_HASH = 488562270;
    private static IEntityMeta ENTITY_META;

    public static final String L_STATUS_OLD = "statusOld";
    public static final String L_STATUS_NEW = "statusNew";
    public static final String P_EXCLUDE_DATE = "excludeDate";
    public static final String P_PREV_GRADUATE_SUCC_ORDER_DATE = "prevGraduateSuccOrderDate";
    public static final String P_PREV_GRADUATE_SUCC_ORDER_NUMBER = "prevGraduateSuccOrderNumber";
    public static final String P_PROVIDED_EXAM = "providedExam";
    public static final String P_FINISHED_YEAR = "finishedYear";

    private StudentStatus _statusOld;     // Состояние студента
    private StudentStatus _statusNew;     // Состояние студента
    private Date _excludeDate;     // Дата отчисления
    private Date _prevGraduateSuccOrderDate;     // Дата предыдущего приказа о выпуске (с отличием)
    private String _prevGraduateSuccOrderNumber;     // Номер предыдущего приказа о выпуске (с отличием)
    private boolean _providedExam = true;     // Сдача ГЭ предусмотрена
    private Integer _finishedYear;     // Год окончания на момент проведения приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStatusOld()
    {
        return _statusOld;
    }

    /**
     * @param statusOld Состояние студента. Свойство не может быть null.
     */
    public void setStatusOld(StudentStatus statusOld)
    {
        dirty(_statusOld, statusOld);
        _statusOld = statusOld;
    }

    /**
     * @return Состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStatusNew()
    {
        return _statusNew;
    }

    /**
     * @param statusNew Состояние студента. Свойство не может быть null.
     */
    public void setStatusNew(StudentStatus statusNew)
    {
        dirty(_statusNew, statusNew);
        _statusNew = statusNew;
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     */
    @NotNull
    public Date getExcludeDate()
    {
        return _excludeDate;
    }

    /**
     * @param excludeDate Дата отчисления. Свойство не может быть null.
     */
    public void setExcludeDate(Date excludeDate)
    {
        dirty(_excludeDate, excludeDate);
        _excludeDate = excludeDate;
    }

    /**
     * @return Дата предыдущего приказа о выпуске (с отличием).
     */
    public Date getPrevGraduateSuccOrderDate()
    {
        initLazyForGet("prevGraduateSuccOrderDate");
        return _prevGraduateSuccOrderDate;
    }

    /**
     * @param prevGraduateSuccOrderDate Дата предыдущего приказа о выпуске (с отличием).
     */
    public void setPrevGraduateSuccOrderDate(Date prevGraduateSuccOrderDate)
    {
        initLazyForSet("prevGraduateSuccOrderDate");
        dirty(_prevGraduateSuccOrderDate, prevGraduateSuccOrderDate);
        _prevGraduateSuccOrderDate = prevGraduateSuccOrderDate;
    }

    /**
     * @return Номер предыдущего приказа о выпуске (с отличием).
     */
    @Length(max=255)
    public String getPrevGraduateSuccOrderNumber()
    {
        initLazyForGet("prevGraduateSuccOrderNumber");
        return _prevGraduateSuccOrderNumber;
    }

    /**
     * @param prevGraduateSuccOrderNumber Номер предыдущего приказа о выпуске (с отличием).
     */
    public void setPrevGraduateSuccOrderNumber(String prevGraduateSuccOrderNumber)
    {
        initLazyForSet("prevGraduateSuccOrderNumber");
        dirty(_prevGraduateSuccOrderNumber, prevGraduateSuccOrderNumber);
        _prevGraduateSuccOrderNumber = prevGraduateSuccOrderNumber;
    }

    /**
     * @return Сдача ГЭ предусмотрена. Свойство не может быть null.
     */
    @NotNull
    public boolean isProvidedExam()
    {
        return _providedExam;
    }

    /**
     * @param providedExam Сдача ГЭ предусмотрена. Свойство не может быть null.
     */
    public void setProvidedExam(boolean providedExam)
    {
        dirty(_providedExam, providedExam);
        _providedExam = providedExam;
    }

    /**
     * @return Год окончания на момент проведения приказа.
     */
    public Integer getFinishedYear()
    {
        return _finishedYear;
    }

    /**
     * @param finishedYear Год окончания на момент проведения приказа.
     */
    public void setFinishedYear(Integer finishedYear)
    {
        dirty(_finishedYear, finishedYear);
        _finishedYear = finishedYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ExcludeSingGrpSuccStuListExtractGen)
        {
            setStatusOld(((ExcludeSingGrpSuccStuListExtract)another).getStatusOld());
            setStatusNew(((ExcludeSingGrpSuccStuListExtract)another).getStatusNew());
            setExcludeDate(((ExcludeSingGrpSuccStuListExtract)another).getExcludeDate());
            setPrevGraduateSuccOrderDate(((ExcludeSingGrpSuccStuListExtract)another).getPrevGraduateSuccOrderDate());
            setPrevGraduateSuccOrderNumber(((ExcludeSingGrpSuccStuListExtract)another).getPrevGraduateSuccOrderNumber());
            setProvidedExam(((ExcludeSingGrpSuccStuListExtract)another).isProvidedExam());
            setFinishedYear(((ExcludeSingGrpSuccStuListExtract)another).getFinishedYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExcludeSingGrpSuccStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExcludeSingGrpSuccStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new ExcludeSingGrpSuccStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "statusOld":
                    return obj.getStatusOld();
                case "statusNew":
                    return obj.getStatusNew();
                case "excludeDate":
                    return obj.getExcludeDate();
                case "prevGraduateSuccOrderDate":
                    return obj.getPrevGraduateSuccOrderDate();
                case "prevGraduateSuccOrderNumber":
                    return obj.getPrevGraduateSuccOrderNumber();
                case "providedExam":
                    return obj.isProvidedExam();
                case "finishedYear":
                    return obj.getFinishedYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "statusOld":
                    obj.setStatusOld((StudentStatus) value);
                    return;
                case "statusNew":
                    obj.setStatusNew((StudentStatus) value);
                    return;
                case "excludeDate":
                    obj.setExcludeDate((Date) value);
                    return;
                case "prevGraduateSuccOrderDate":
                    obj.setPrevGraduateSuccOrderDate((Date) value);
                    return;
                case "prevGraduateSuccOrderNumber":
                    obj.setPrevGraduateSuccOrderNumber((String) value);
                    return;
                case "providedExam":
                    obj.setProvidedExam((Boolean) value);
                    return;
                case "finishedYear":
                    obj.setFinishedYear((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "statusOld":
                        return true;
                case "statusNew":
                        return true;
                case "excludeDate":
                        return true;
                case "prevGraduateSuccOrderDate":
                        return true;
                case "prevGraduateSuccOrderNumber":
                        return true;
                case "providedExam":
                        return true;
                case "finishedYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "statusOld":
                    return true;
                case "statusNew":
                    return true;
                case "excludeDate":
                    return true;
                case "prevGraduateSuccOrderDate":
                    return true;
                case "prevGraduateSuccOrderNumber":
                    return true;
                case "providedExam":
                    return true;
                case "finishedYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "statusOld":
                    return StudentStatus.class;
                case "statusNew":
                    return StudentStatus.class;
                case "excludeDate":
                    return Date.class;
                case "prevGraduateSuccOrderDate":
                    return Date.class;
                case "prevGraduateSuccOrderNumber":
                    return String.class;
                case "providedExam":
                    return Boolean.class;
                case "finishedYear":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExcludeSingGrpSuccStuListExtract> _dslPath = new Path<ExcludeSingGrpSuccStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExcludeSingGrpSuccStuListExtract");
    }
            

    /**
     * @return Состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpSuccStuListExtract#getStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> statusOld()
    {
        return _dslPath.statusOld();
    }

    /**
     * @return Состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpSuccStuListExtract#getStatusNew()
     */
    public static StudentStatus.Path<StudentStatus> statusNew()
    {
        return _dslPath.statusNew();
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpSuccStuListExtract#getExcludeDate()
     */
    public static PropertyPath<Date> excludeDate()
    {
        return _dslPath.excludeDate();
    }

    /**
     * @return Дата предыдущего приказа о выпуске (с отличием).
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpSuccStuListExtract#getPrevGraduateSuccOrderDate()
     */
    public static PropertyPath<Date> prevGraduateSuccOrderDate()
    {
        return _dslPath.prevGraduateSuccOrderDate();
    }

    /**
     * @return Номер предыдущего приказа о выпуске (с отличием).
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpSuccStuListExtract#getPrevGraduateSuccOrderNumber()
     */
    public static PropertyPath<String> prevGraduateSuccOrderNumber()
    {
        return _dslPath.prevGraduateSuccOrderNumber();
    }

    /**
     * @return Сдача ГЭ предусмотрена. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpSuccStuListExtract#isProvidedExam()
     */
    public static PropertyPath<Boolean> providedExam()
    {
        return _dslPath.providedExam();
    }

    /**
     * @return Год окончания на момент проведения приказа.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpSuccStuListExtract#getFinishedYear()
     */
    public static PropertyPath<Integer> finishedYear()
    {
        return _dslPath.finishedYear();
    }

    public static class Path<E extends ExcludeSingGrpSuccStuListExtract> extends ListStudentExtract.Path<E>
    {
        private StudentStatus.Path<StudentStatus> _statusOld;
        private StudentStatus.Path<StudentStatus> _statusNew;
        private PropertyPath<Date> _excludeDate;
        private PropertyPath<Date> _prevGraduateSuccOrderDate;
        private PropertyPath<String> _prevGraduateSuccOrderNumber;
        private PropertyPath<Boolean> _providedExam;
        private PropertyPath<Integer> _finishedYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpSuccStuListExtract#getStatusOld()
     */
        public StudentStatus.Path<StudentStatus> statusOld()
        {
            if(_statusOld == null )
                _statusOld = new StudentStatus.Path<StudentStatus>(L_STATUS_OLD, this);
            return _statusOld;
        }

    /**
     * @return Состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpSuccStuListExtract#getStatusNew()
     */
        public StudentStatus.Path<StudentStatus> statusNew()
        {
            if(_statusNew == null )
                _statusNew = new StudentStatus.Path<StudentStatus>(L_STATUS_NEW, this);
            return _statusNew;
        }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpSuccStuListExtract#getExcludeDate()
     */
        public PropertyPath<Date> excludeDate()
        {
            if(_excludeDate == null )
                _excludeDate = new PropertyPath<Date>(ExcludeSingGrpSuccStuListExtractGen.P_EXCLUDE_DATE, this);
            return _excludeDate;
        }

    /**
     * @return Дата предыдущего приказа о выпуске (с отличием).
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpSuccStuListExtract#getPrevGraduateSuccOrderDate()
     */
        public PropertyPath<Date> prevGraduateSuccOrderDate()
        {
            if(_prevGraduateSuccOrderDate == null )
                _prevGraduateSuccOrderDate = new PropertyPath<Date>(ExcludeSingGrpSuccStuListExtractGen.P_PREV_GRADUATE_SUCC_ORDER_DATE, this);
            return _prevGraduateSuccOrderDate;
        }

    /**
     * @return Номер предыдущего приказа о выпуске (с отличием).
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpSuccStuListExtract#getPrevGraduateSuccOrderNumber()
     */
        public PropertyPath<String> prevGraduateSuccOrderNumber()
        {
            if(_prevGraduateSuccOrderNumber == null )
                _prevGraduateSuccOrderNumber = new PropertyPath<String>(ExcludeSingGrpSuccStuListExtractGen.P_PREV_GRADUATE_SUCC_ORDER_NUMBER, this);
            return _prevGraduateSuccOrderNumber;
        }

    /**
     * @return Сдача ГЭ предусмотрена. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpSuccStuListExtract#isProvidedExam()
     */
        public PropertyPath<Boolean> providedExam()
        {
            if(_providedExam == null )
                _providedExam = new PropertyPath<Boolean>(ExcludeSingGrpSuccStuListExtractGen.P_PROVIDED_EXAM, this);
            return _providedExam;
        }

    /**
     * @return Год окончания на момент проведения приказа.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpSuccStuListExtract#getFinishedYear()
     */
        public PropertyPath<Integer> finishedYear()
        {
            if(_finishedYear == null )
                _finishedYear = new PropertyPath<Integer>(ExcludeSingGrpSuccStuListExtractGen.P_FINISHED_YEAR, this);
            return _finishedYear;
        }

        public Class getEntityClass()
        {
            return ExcludeSingGrpSuccStuListExtract.class;
        }

        public String getEntityName()
        {
            return "excludeSingGrpSuccStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
