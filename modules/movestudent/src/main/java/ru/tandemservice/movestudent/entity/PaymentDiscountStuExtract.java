package ru.tandemservice.movestudent.entity;

import java.util.Date;

import ru.tandemservice.movestudent.entity.gen.PaymentDiscountStuExtractGen;

/**
 * Выписка из сборного приказа по студенту. О скидке по оплате за обучение
 */
public class PaymentDiscountStuExtract extends PaymentDiscountStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getDiscountBeginDate();
    }

    @Override
    public Date getEndDate()
    {
        return getDiscountEndDate();
    }
}