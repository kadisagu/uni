/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e58.AddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StuExtractToTermRelation;
import ru.tandemservice.movestudent.entity.TransferIndGraphStuExtract;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 21.08.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<TransferIndGraphStuExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setEducationYearsList(new EducationYearModel());
        if (model.isAddForm())
            model.getExtract().setEducationYear(UniDaoFacade.getCoreDao().get(EducationYear.class, EducationYear.P_CURRENT, Boolean.TRUE));
        model.setTermListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getExtract() == null)
                    return ListResult.getEmpty();
                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder
                        .fromEntity(Term.class, "term")
                        .where(DQLExpressions.like(DQLExpressions.property(Term.title().fromAlias("term")), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                return new ListResult<>(builder.createStatement(getSession()).list());
            }
        });


        if (model.isEditForm())
        {
            model.getSelectedTermList().clear();
            for (Term term: MoveStudentDaoFacade.getMoveStudentDao().getExtractTerms(model.getExtract()))
                model.getSelectedTermList().add(term);
        }
    }

    @Override
    public void update(Model model)
    {
        super.update(model);
        updateTermList(model);
    }

    private void updateTermList(Model model)
    {
        if (model.isEditForm())
        {
            for (StuExtractToTermRelation relation : getList(StuExtractToTermRelation.class, StuExtractToTermRelation.L_EXTRACT, model.getExtract()))
                delete(relation);
            getSession().flush();
        }

        for (Term term : model.getSelectedTermList())
        {
            StuExtractToTermRelation relation = new StuExtractToTermRelation();
            relation.setTerm(term);
            relation.setExtract(model.getExtract());
            save(relation);
        }
    }

    @Override
    protected TransferIndGraphStuExtract createNewInstance()
    {
        return new TransferIndGraphStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }
}