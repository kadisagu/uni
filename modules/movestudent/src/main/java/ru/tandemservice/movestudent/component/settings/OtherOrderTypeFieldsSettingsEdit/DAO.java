/* $Id$ */
package ru.tandemservice.movestudent.component.settings.OtherOrderTypeFieldsSettingsEdit;

import ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDao;

/**
 * @author Dmitry Seleznev
 * @since 11.04.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setStudentExtractType(this.<StudentExtractType>getNotNull(model.getStudentExtractTypeId()));

        OtherStudentFieldsSettings settings = get(OtherStudentFieldsSettings.class, OtherStudentFieldsSettings.studentExtractType().id(), model.getStudentExtractTypeId());
        model.setFieldSettings(null != settings ? settings : new OtherStudentFieldsSettings(model.getStudentExtractType()));

        if (null == settings)
        {
            StudentExtractType parentType = model.getStudentExtractType().getParent();

            if (null != parentType)
            {
                OtherStudentFieldsSettings parentSettings = get(OtherStudentFieldsSettings.class, OtherStudentFieldsSettings.studentExtractType().id(), parentType.getId());
                while (null == parentSettings && parentType.getParent() != null)
                {
                    parentType = parentType.getParent();
                    parentSettings = get(OtherStudentFieldsSettings.class, OtherStudentFieldsSettings.studentExtractType().id(), parentType.getId());
                }

                if (null != parentSettings)
                {
                    model.getFieldSettings().update(parentSettings);
                    model.getFieldSettings().setStudentExtractType(model.getStudentExtractType());
                }
            }
        }
    }

    @Override
    public void update(Model model)
    {
        getSession().saveOrUpdate(model.getFieldSettings());
    }
}