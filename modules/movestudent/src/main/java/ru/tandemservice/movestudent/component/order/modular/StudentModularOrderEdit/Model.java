/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.modular.StudentModularOrderEdit;

import org.tandemframework.core.component.Input;

import ru.tandemservice.movestudent.entity.StudentModularOrder;

/**
 * @author vip_delete
 * @since 20.10.2008
 */
@Input(keys = "orderId", bindings = "order.id")
public class Model
{
    private StudentModularOrder _order = new StudentModularOrder();
    private boolean _disableCommitDate;
    private boolean _disableOrderNumber;

    public StudentModularOrder getOrder()
    {
        return _order;
    }

    public void setOrder(StudentModularOrder order)
    {
        _order = order;
    }

    public boolean isDisableCommitDate()
    {
        return _disableCommitDate;
    }

    public void setDisableCommitDate(boolean disableCommitDate)
    {
        _disableCommitDate = disableCommitDate;
    }

    public boolean isDisableOrderNumber()
    {
        return _disableOrderNumber;
    }

    public void setDisableOrderNumber(boolean disableOrderNumber)
    {
        _disableOrderNumber = disableOrderNumber;
    }
}
