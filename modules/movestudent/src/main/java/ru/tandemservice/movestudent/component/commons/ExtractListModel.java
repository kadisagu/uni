/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.commons;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.List;

/**
 * @author vip_delete
 * @since 01.11.2008
 */
public class ExtractListModel implements IEducationLevelModel
{
    public static final String EDUCATION_YEAR = "educationYear";
    public static final String FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String PRODUCING_ORG_UNIT = "producingOrgUnit";
    public static final String EDUCATION_LEVEL_HIGH_SCHOOL = "educationLevelHighSchool";
    public static final String COURSE = "course";
    public static final String DEVELOP_FORM = "developForm";
    public static final String COMPENSATION_TYPE = "compensationType";
    public static final String EXTRACT_TYPE = "extractType";
    public static final String EXTRACT_STATE = "extractState";
    public static final String EXTRACT_REASON = "extractReason";

    private DynamicListDataSource _dataSource;
    private IDataSettings _settings;
    private CommonPostfixPermissionModel _secModel;

    // filters
    private ISelectModel _educationYearList;
    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _producingOrgUnitModel;
    private ISelectModel _educationLevelHighSchool;
    private ISelectModel _studentList;
    private IMultiSelectModel _extractReasonModel;
    private List<Course> _courseList;
    private ISelectModel _developFormList;
    private List<CompensationType> _compensationTypeList;
    private IMultiSelectModel _extractTypesListModel;
    private List<ExtractStates> _extractStateList;
    private ISelectModel _executorModel;

    // IEducationLevelModel

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return _settings.get(FORMATIVE_ORG_UNIT);
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return _settings.get(TERRITORIAL_ORG_UNIT);
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _settings.get(EDUCATION_LEVEL_HIGH_SCHOOL);
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        // unused
        throw new UnsupportedOperationException();
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        // unused
        throw new UnsupportedOperationException();
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        // unused
        throw new UnsupportedOperationException();
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        // unused
        throw new UnsupportedOperationException();
    }

    // Getters & Setters
    @SuppressWarnings("unchecked")
    public <T extends IEntity> DynamicListDataSource<T> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public ISelectModel getEducationYearList()
    {
        return _educationYearList;
    }

    public void setEducationYearList(ISelectModel educationYearList)
    {
        _educationYearList = educationYearList;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getProducingOrgUnitModel()
    {
        return _producingOrgUnitModel;
    }

    public void setProducingOrgUnitModel(ISelectModel producingOrgUnitModel)
    {
        _producingOrgUnitModel = producingOrgUnitModel;
    }

    public ISelectModel getEducationLevelHighSchool()
    {
        return _educationLevelHighSchool;
    }

    public void setEducationLevelHighSchool(ISelectModel educationLevelHighSchool)
    {
        _educationLevelHighSchool = educationLevelHighSchool;
    }

    public ISelectModel getStudentList()
    {
        return _studentList;
    }

    public void setStudentList(ISelectModel studentList)
    {
        _studentList = studentList;
    }

    public IMultiSelectModel getExtractReasonModel()
    {
        return _extractReasonModel;
    }

    public void setExtractReasonModel(IMultiSelectModel extractReasonModel)
    {
        this._extractReasonModel = extractReasonModel;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public ISelectModel getDevelopFormList() { return _developFormList; }
    public void setDevelopFormList(ISelectModel developFormList) { _developFormList = developFormList; }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public IMultiSelectModel getExtractTypesListModel()
    {
        return _extractTypesListModel;
    }

    public void setExtractTypesListModel(IMultiSelectModel extractTypesListModel)
    {
        this._extractTypesListModel = extractTypesListModel;
    }

    public List<ExtractStates> getExtractStateList()
    {
        return _extractStateList;
    }

    public void setExtractStateList(List<ExtractStates> extractStateList)
    {
        _extractStateList = extractStateList;
    }

    public ISelectModel getExecutorModel()
    {
        return _executorModel;
    }

    public void setExecutorModel(ISelectModel executorModel)
    {
        _executorModel = executorModel;
    }
}