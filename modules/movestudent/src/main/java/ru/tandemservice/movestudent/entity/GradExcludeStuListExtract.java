package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.movestudent.entity.gen.GradExcludeStuListExtractGen;

/**
 * Проект приказа «Об исключении студентов, не выполнивших учебный план»
 */
public class GradExcludeStuListExtract extends GradExcludeStuListExtractGen implements IExcludeExtract
{
}