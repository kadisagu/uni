/* $Id$ */
package ru.tandemservice.movestudent.component.movestudentScriptItem.MovestudentScriptItemItemPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogItemPub.DefaultScriptCatalogItemPubController;

/**
 * @author Vasily Zhukov
 * @since 22.12.2011
 */
public class Controller<T extends ICatalogItem & IScriptItem, Model extends ru.tandemservice.movestudent.component.movestudentScriptItem.MovestudentScriptItemItemPub.Model<T>, IDAO extends ru.tandemservice.movestudent.component.movestudentScriptItem.MovestudentScriptItemItemPub.IDAO<T, Model>> extends DefaultScriptCatalogItemPubController
{
}