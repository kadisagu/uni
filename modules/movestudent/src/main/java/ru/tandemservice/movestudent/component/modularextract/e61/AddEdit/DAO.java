/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e61.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.RestorationCourseStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.movestudent.utils.system.StudentExcludeOrderData;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 28.08.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<RestorationCourseStuExtract, Model> implements IDAO
{
    @Override
    protected RestorationCourseStuExtract createNewInstance()
    {
        return new RestorationCourseStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setEduModel(MoveStudentDaoFacade.getCommonExtractUtil().createEduModel(model.isAddForm(), model.getExtract(), true));
        model.setStudentStatusNewStr(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE).getTitle());
        model.setTermsList(DevelopGridDAO.getTermMap().values());

        if(!model.isAddForm())
            model.setDebtsList(MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(model.getExtract()));

        if(model.getDebtsList().isEmpty())
            model.getDebtsList().add(new StuExtractToDebtRelation());

        if (model.isAddForm())
        {
            model.getExtract().setDismissedFrom(TopOrgUnit.getInstance().getShortTitle());

            StudentExcludeOrderData data = MoveStudentDaoFacade.getMoveStudentDao().getStudentExcludeOrderData(model.getExtract().getEntity());
            if (data != null)
            {
                model.getExtract().setDismissOrder(data.getDismissOrderNum());
                model.getExtract().setDismissOrderDate(data.getDismissOrderDate());
                model.getExtract().setDismissDate(data.getDismissDate());
                model.getExtract().setDismissReason(data.getDismissOrderReason());
            }
        }
    }

    @Override
    public void update(Model model)
    {
        //save rollback data
        MoveStudentDaoFacade.getCommonExtractUtil().update(model.getEduModel(), model.getExtract());
        super.update(model);

        for (StuExtractToDebtRelation rel : model.getDebtsList())
        {
            if (model.getExtract().isHasDebts())
            {
                rel.setExtract(model.getExtract());
                getSession().saveOrUpdate(rel);
            }
            else if(null != rel.getId())
                getSession().delete(rel);
        }

        for(StuExtractToDebtRelation rel : model.getDebtsToDel())
        {
            if(null != rel.getId()) getSession().delete(rel);
        }
    }
}