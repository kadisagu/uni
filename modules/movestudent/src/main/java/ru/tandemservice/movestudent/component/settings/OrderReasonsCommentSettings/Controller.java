/* $Id$ */
package ru.tandemservice.movestudent.component.settings.OrderReasonsCommentSettings;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author Dmitry Seleznev
 * @since 05.12.2012
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<StudentOrderReasons> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Название", StudentOrderReasons.P_TITLE).setClickable(false));
        dataSource.addColumn(new ToggleColumn("Требует примечания", StudentOrderReasons.commentable().s()).setListener("onClickCommentable"));
        dataSource.addColumn(new ToggleColumn("Примечание обязательно", StudentOrderReasons.commentRequired().s()).setListener("onClickRequired").setDisabledProperty(StudentOrderReasons.P_COMMENT_REQUIRED_DISABLED));
        model.setDataSource(dataSource);
    }

    public void onClickCommentable(IBusinessComponent component)
    {
        getDao().updateCommentable((Long) component.getListenerParameter());
    }

    public void onClickRequired(IBusinessComponent component)
    {
        getDao().updateRequired((Long) component.getListenerParameter());
    }
}