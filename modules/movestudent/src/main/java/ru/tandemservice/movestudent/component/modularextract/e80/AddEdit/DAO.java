/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e80.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.ChangeCategoryStuExtract;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentCategory;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 31.08.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<ChangeCategoryStuExtract, Model> implements IDAO
{
    @Override
    protected ChangeCategoryStuExtract createNewInstance()
    {
        return new ChangeCategoryStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setStudentCategoryList(UniDaoFacade.getCoreDao().getCatalogItemList(StudentCategory.class));

        if (model.isAddForm())
        {
            model.getExtract().setStudentCategoryOld(model.getExtract().getEntity().getStudentCategory());
        }
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setStudentCategoryOld(model.getExtract().getEntity().getStudentCategory());
        super.update(model);
    }
}