/* $Id$ */
package ru.tandemservice.movestudent.component.order.other.StudentOtherOrderPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.movestudent.entity.OtherStudentExtract;
import ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings;
import ru.tandemservice.movestudent.entity.StudentOtherOrderTextRelation;

/**
 * @author Dmitry Seleznev
 * @since 15.04.2013
 */
@State({
        @Bind(key = "publisherId", binding = "publisherId"),
        @Bind(key = "extractId", binding = "extractId")
})
public class Model
{
    private Long _extractId;
    private Long _publisherId;
    private OtherStudentExtract _extract;

    private CommonPostfixPermissionModel _secModel;
    private OtherStudentFieldsSettings _fieldSettings;
    private boolean _otherTypesThreeLevel = false;

    private boolean _hasFileAttached = false;
    private StudentOtherOrderTextRelation _printForm;

    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        _extractId = extractId;
    }

    public Long getPublisherId()
    {
        return _publisherId;
    }

    public void setPublisherId(Long publisherId)
    {
        _publisherId = publisherId;
    }

    public OtherStudentExtract getExtract()
    {
        return _extract;
    }

    public void setExtract(OtherStudentExtract extract)
    {
        _extract = extract;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public OtherStudentFieldsSettings getFieldSettings()
    {
        return _fieldSettings;
    }

    public void setFieldSettings(OtherStudentFieldsSettings fieldSettings)
    {
        _fieldSettings = fieldSettings;
    }

    public boolean isOtherTypesThreeLevel()
    {
        return _otherTypesThreeLevel;
    }

    public void setOtherTypesThreeLevel(boolean otherTypesThreeLevel)
    {
        _otherTypesThreeLevel = otherTypesThreeLevel;
    }

    public boolean isHasFileAttached()
    {
        return _hasFileAttached;
    }

    public void setHasFileAttached(boolean hasFileAttached)
    {
        _hasFileAttached = hasFileAttached;
    }

    public StudentOtherOrderTextRelation getPrintForm()
    {
        return _printForm;
    }

    public void setPrintForm(StudentOtherOrderTextRelation printForm)
    {
        _printForm = printForm;
    }
}