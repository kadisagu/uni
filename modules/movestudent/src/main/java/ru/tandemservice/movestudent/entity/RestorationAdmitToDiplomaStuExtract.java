package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IEnrollmentExtract;
import ru.tandemservice.movestudent.entity.gen.RestorationAdmitToDiplomaStuExtractGen;

import java.util.Date;

/**
 * О восстановлении и допуске к подготовке ВКР
 */
public class RestorationAdmitToDiplomaStuExtract extends RestorationAdmitToDiplomaStuExtractGen implements IEnrollmentExtract
{
    @Override
    public Date getEntranceDate()
    {
        return getRestorationDate();
    }
}