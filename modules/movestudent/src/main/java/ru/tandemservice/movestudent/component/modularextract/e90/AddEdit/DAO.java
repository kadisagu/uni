/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e90.AddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.movestudent.entity.WeekendChildCareOutStuExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 26.12.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<WeekendChildCareOutStuExtract, Model> implements IDAO
{
    @Override
    protected WeekendChildCareOutStuExtract createNewInstance()
    {
        return new WeekendChildCareOutStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        model.setGroupModel(new GroupSelectModel(model, model).extModel(model).showChildLevels(true));


        if(model.isAddForm())
        {
            if(null != model.getExtract().getEntity().getGroup())
            {
                model.getExtract().setGroupOld(model.getExtract().getEntity().getGroup());
            }
            model.setGroup(model.getExtract().getEntity().getGroup());
        }
        else
        {
            model.setGroup(model.getExtract().getGroupNew());
        }

        if(!model.isAddForm())
            model.setDebtsList(MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(model.getExtract()));

        if(model.getDebtsList().isEmpty())
            model.getDebtsList().add(new StuExtractToDebtRelation());
    }

    @Override
    public void update(Model model)
    {
        if (!model.getExtract().isResumeGrantPay())
        {
            model.getExtract().setGrantSum(null);
            model.getExtract().setGrantPayBeginDate(null);
            model.getExtract().setGrantPayEndDate(null);
        }
        //save rollback data
        model.getExtract().setGroupOld(model.getExtract().getEntity().getGroup());

        //save new data
        model.getExtract().setGroupNew(model.getGroup());

        model.getExtract().setStudentStatusOld(model.getExtract().getEntity().getStatus());

        if(!model.getExtract().isHasDebts())
        {
            model.getExtract().setDeadlineDate(null);
        }

        super.update(model);

        for (StuExtractToDebtRelation rel : model.getDebtsList())
        {
            if (model.getExtract().isHasDebts())
            {
                rel.setExtract(model.getExtract());
                getSession().saveOrUpdate(rel);
            }
            else if(null != rel.getId())
                getSession().delete(rel);
        }

        for(StuExtractToDebtRelation rel : model.getDebtsToDel())
        {
            if(null != rel.getId()) getSession().delete(rel);
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errorCollector)
    {
        super.validate(model, errorCollector);
        if (model.getExtract().isResumeGrantPay() && model.getExtract().getGrantPayBeginDate().after(model.getExtract().getGrantPayEndDate()))
            errorCollector.add("Дата окончания выплаты стипендии должна быть позже даты начала", "beginDate", "endDate");
    }
}