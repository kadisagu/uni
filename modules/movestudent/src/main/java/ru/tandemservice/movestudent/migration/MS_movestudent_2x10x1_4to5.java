package ru.tandemservice.movestudent.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_movestudent_2x10x1_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность admitToPassQualificationWorkStuListExtract

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("dmttpssqlfctnwrkstlstextrct_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_20b103ce"), 
				new DBColumn("educationorgunit_id", DBType.LONG).setNullable(false), 
				new DBColumn("developform_id", DBType.LONG).setNullable(false), 
				new DBColumn("course_id", DBType.LONG).setNullable(false), 
				new DBColumn("begindate_p", DBType.DATE), 
				new DBColumn("enddate_p", DBType.DATE)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("admitToPassQualificationWorkStuListExtract");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность admitToPassStatateExamStuListExtract

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("dmttpssstttexmstlstextrct_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_f38a67ab"), 
				new DBColumn("educationorgunit_id", DBType.LONG).setNullable(false), 
				new DBColumn("developform_id", DBType.LONG).setNullable(false), 
				new DBColumn("course_id", DBType.LONG).setNullable(false), 
				new DBColumn("begindate_p", DBType.DATE), 
				new DBColumn("enddate_p", DBType.DATE)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("admitToPassStatateExamStuListExtract");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность setDiplomaWorkTopicAndScientificAdviserStuListExtract

		// создано свойство scientificAdviserNameOld
		{
			// создать колонку
			tool.createColumn("stdplmwrktpcandscntfcadvsrst_t", new DBColumn("scientificadvisernameold_id", DBType.LONG));

		}


    }
}