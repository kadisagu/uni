/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e32.ListExtractPub;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubModel;
import ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract;
import ru.tandemservice.movestudent.utils.SessionPartModel;

/**
 * @author ListExtractComponentGenerator
 * @since 01.10.2012
 */
public class Model extends AbstractListExtractPubModel<AcadGrantAssignStuListExtract>
{
    public String getSessionTitle()
    {
        return SessionPartModel.getSessionShortTitle(getExtract().getSessionType(), GrammaCase.NOMINATIVE);
    }
}
