package ru.tandemservice.movestudent.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_movestudent_2x10x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность changeDiplomaWorkTopicStuListExtract

		if(tool.tableExists("chngdplmwrktpcstlstextrct_t"))
		{
			// создано свойство scientificAdviserNameStrOld
			if(!tool.columnExists("chngdplmwrktpcstlstextrct_t", "scientificadvisernamestrold_p"))
			{
				// создать колонку
				tool.createColumn("chngdplmwrktpcstlstextrct_t", new DBColumn("scientificadvisernamestrold_p", DBType.createVarchar(1024)));

			}

			// создано свойство scientificAdviserNameOld
			if(!tool.columnExists("chngdplmwrktpcstlstextrct_t", "scientificadvisernameold_id"))
			{
				// создать колонку
				tool.createColumn("chngdplmwrktpcstlstextrct_t", new DBColumn("scientificadvisernameold_id", DBType.LONG));

			}
		}
    }
}