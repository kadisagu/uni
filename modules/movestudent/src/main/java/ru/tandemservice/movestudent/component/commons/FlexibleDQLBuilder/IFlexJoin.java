/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

/**
 * @author Nikolay Fedorovskih
 * @since 29.11.2013
 */
public interface IFlexJoin
{
    String getAlias();

    String getLeftAlias();

    boolean isRequired();

    void addToBuilder(DQLSelectBuilder builder);
}