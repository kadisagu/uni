/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e45.utils;

import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;

import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 19.05.2016
 */
public class SetDiplomaWorkTopicAndScientificAdviserStudParagraphWrapper implements Comparable<SetDiplomaWorkTopicAndScientificAdviserStudParagraphWrapper>
{
    private final String _studentFio;
    private final String _finalQualifyingWorkTheme;
    private final String _scientificAdvisor;
    private SetDiplomaWorkTopicAndScientificAdviserStuListExtract _extract;

    public SetDiplomaWorkTopicAndScientificAdviserStudParagraphWrapper(String studentFio, String finalQualifyingWorkTheme, String scientificAdvisor, SetDiplomaWorkTopicAndScientificAdviserStuListExtract extract)
    {
        _studentFio = studentFio;
        _finalQualifyingWorkTheme = finalQualifyingWorkTheme;
        _scientificAdvisor = scientificAdvisor;
        _extract = extract;
    }

    public SetDiplomaWorkTopicAndScientificAdviserStudParagraphWrapper(SetDiplomaWorkTopicAndScientificAdviserStuListExtract extract, Map<Long, List<ScienceDegree>> scDegreeListMap, Map<Long, List<ScienceStatus>> scStatusListMap)
    {
        _extract = extract;
        _studentFio = extract.getEntity().getPerson().getFullFio();
        _finalQualifyingWorkTheme = extract.getDiplomaWorkTopicNew();

        StringBuilder statusAndPost = new StringBuilder();

        if (null != scDegreeListMap.get(extract.getId()))
        {
            for (ScienceDegree degree : scDegreeListMap.get(extract.getId()))
                statusAndPost.append(statusAndPost.length() > 0 ? ", " : "").append(degree.getShortTitle());
        }

        if (null != scStatusListMap.get(extract.getId()))
        {
            for (ScienceStatus status : scStatusListMap.get(extract.getId()))
                statusAndPost.append(statusAndPost.length() > 0 ? ", " : "").append(status.getShortTitle());
        }

        statusAndPost.append(statusAndPost.length() > 0 ? ", " : "");
        if (extract.getScientificAdviserName() instanceof PpsEntryByEmployeePost)
            statusAndPost.append(((PpsEntryByEmployeePost) extract.getScientificAdviserName()).getPost().getTitle()).append(" ");
        if (null != extract.getScientificAdviserName().getOrgUnit().getGenitiveCaseTitle())
            statusAndPost.append(extract.getScientificAdviserName().getOrgUnit().getGenitiveCaseTitle());
        else statusAndPost.append(extract.getScientificAdviserName().getOrgUnit().getTitle());

        statusAndPost.append(" ").append(extract.getScientificAdviserName().getPerson().getFio());
        _scientificAdvisor = statusAndPost.toString();
    }

    public SetDiplomaWorkTopicAndScientificAdviserStuListExtract getExtract()
    {
        return _extract;
    }

    public void setExtract(SetDiplomaWorkTopicAndScientificAdviserStuListExtract extract)
    {
        _extract = extract;
    }

    public String getStudentFio()
    {
        return _studentFio;
    }

    public String getFinalQualifyingWorkTheme()
    {
        return _finalQualifyingWorkTheme;
    }

    public String getScientificAdvisor()
    {
        return _scientificAdvisor;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof SetDiplomaWorkTopicAndScientificAdviserStudParagraphWrapper)) return false;

        SetDiplomaWorkTopicAndScientificAdviserStudParagraphWrapper that = (SetDiplomaWorkTopicAndScientificAdviserStudParagraphWrapper) o;

        return _studentFio.equals(that.getStudentFio())
                && _finalQualifyingWorkTheme.equals(that.getFinalQualifyingWorkTheme())
                && _scientificAdvisor.equals(that.getScientificAdvisor());
    }

    @Override
    public int hashCode()
    {
        return _studentFio.hashCode() & _finalQualifyingWorkTheme.hashCode() & _scientificAdvisor.hashCode();
    }

    @Override
    public int compareTo(SetDiplomaWorkTopicAndScientificAdviserStudParagraphWrapper o)
    {
        return _studentFio.compareTo(o.getStudentFio());
    }
}