/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e13.ParagraphAddEdit;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.TerritorialTransferStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Date;

/**
 * @author ListExtractComponentGenerator
 * @since 06.09.2010
 */
public class Model extends AbstractListParagraphAddEditModel<TerritorialTransferStuListExtract> implements IGroupModel
{
    private Course _course;

    private Group _group;
    private ISelectModel _groupListModel;

    private OrgUnit _formativeOrgUnitNew;
    private OrgUnit _territorialOrgUnitNew;
    private DevelopCondition _developConditionNew;
    private DevelopPeriod _developPeriodNew;

    private ISelectModel _formativeOrgUnitsListModel;
    private ISelectModel _territorialOrgUnitsListModel;
    private ISelectModel _developConditionsListModel;
    private ISelectModel _developPeriodsListModel;
    private Date _transferDate;

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        this._course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        this._group = group;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        this._groupListModel = groupListModel;
    }

    public OrgUnit getFormativeOrgUnitNew()
    {
        return _formativeOrgUnitNew;
    }

    public void setFormativeOrgUnitNew(OrgUnit formativeOrgUnitNew)
    {
        this._formativeOrgUnitNew = formativeOrgUnitNew;
    }

    public OrgUnit getTerritorialOrgUnitNew()
    {
        return _territorialOrgUnitNew;
    }

    public void setTerritorialOrgUnitNew(OrgUnit territorialOrgUnitNew)
    {
        this._territorialOrgUnitNew = territorialOrgUnitNew;
    }

    public DevelopCondition getDevelopConditionNew()
    {
        return _developConditionNew;
    }

    public void setDevelopConditionNew(DevelopCondition developConditionNew)
    {
        this._developConditionNew = developConditionNew;
    }

    public DevelopPeriod getDevelopPeriodNew()
    {
        return _developPeriodNew;
    }

    public void setDevelopPeriodNew(DevelopPeriod developPeriodNew)
    {
        this._developPeriodNew = developPeriodNew;
    }

    public ISelectModel getFormativeOrgUnitsListModel()
    {
        return _formativeOrgUnitsListModel;
    }

    public void setFormativeOrgUnitsListModel(ISelectModel formativeOrgUnitsListModel)
    {
        this._formativeOrgUnitsListModel = formativeOrgUnitsListModel;
    }

    public ISelectModel getTerritorialOrgUnitsListModel()
    {
        return _territorialOrgUnitsListModel;
    }

    public void setTerritorialOrgUnitsListModel(ISelectModel territorialOrgUnitsListModel)
    {
        this._territorialOrgUnitsListModel = territorialOrgUnitsListModel;
    }

    public ISelectModel getDevelopConditionsListModel()
    {
        return _developConditionsListModel;
    }

    public void setDevelopConditionsListModel(ISelectModel developConditionsListModel)
    {
        this._developConditionsListModel = developConditionsListModel;
    }

    public ISelectModel getDevelopPeriodsListModel()
    {
        return _developPeriodsListModel;
    }

    public void setDevelopPeriodsListModel(ISelectModel developPeriodsListModel)
    {
        this._developPeriodsListModel = developPeriodsListModel;
    }

    public boolean isAnyOrderParametersCantBeChanged()
    {
        return !isParagraphOnlyOneInTheOrder();
    }

    public Date getTransferDate()
    {
        return _transferDate;
    }

    public void setTransferDate(Date transferDate)
    {
        this._transferDate = transferDate;
    }
}