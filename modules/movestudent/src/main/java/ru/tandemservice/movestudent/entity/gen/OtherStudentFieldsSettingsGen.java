package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Используемые поля для типов «прочих» приказов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OtherStudentFieldsSettingsGen extends EntityBase
 implements INaturalIdentifiable<OtherStudentFieldsSettingsGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings";
    public static final String ENTITY_NAME = "otherStudentFieldsSettings";
    public static final int VERSION_HASH = 178179576;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_EXTRACT_TYPE = "studentExtractType";
    public static final String P_USE_REASON_FIELD = "useReasonField";
    public static final String P_USE_COURSE_FIELD = "useCourseField";
    public static final String P_USE_TERM_FIELD = "useTermField";
    public static final String P_USE_INSTITUTE_FIELD = "useInstituteField";
    public static final String P_USE_FACULTY_FIELD = "useFacultyField";
    public static final String P_USE_SPECIALITY_FIELD = "useSpecialityField";
    public static final String P_USE_PROFILE_FIELD = "useProfileField";
    public static final String P_USE_DOP_EDU_LEVEL_FIELD = "useDopEduLevelField";
    public static final String P_USE_ENCOURAGELTY_FIELD = "useEncourageltyField";
    public static final String P_USE_ENCOURAGELTY_END_DATE_FIELD = "useEncourageltyEndDateField";
    public static final String P_USE_GRANT_SIZE_FIELD = "useGrantSizeField";
    public static final String P_USE_GRANT_CATEGORY_FIELD = "useGrantCategoryField";
    public static final String P_USE_PRACTICE_TYPE_FIELD = "usePracticeTypeField";
    public static final String P_USE_PRACTICE_PLACE_FIELD = "usePracticePlaceField";
    public static final String P_USE_PRACTICE_OWNERSHIP_TYPE_FIELD = "usePracticeOwnershipTypeField";
    public static final String P_USE_PRACTICE_SETTLEMENT_FIELD = "usePracticeSettlementField";
    public static final String P_USE_PRACTICE_PERIOD_FIELD = "usePracticePeriodField";
    public static final String P_USE_PRACTICE_SUPERVISOR_FIELD = "usePracticeSupervisorField";

    private StudentExtractType _studentExtractType;     // Тип приказа
    private boolean _useReasonField;     // Использовать поле «Причина»
    private boolean _useCourseField;     // Использовать поле «Курс»
    private boolean _useTermField;     // Использовать поле «Семестр»
    private boolean _useInstituteField;     // Использовать поле «Институт»
    private boolean _useFacultyField;     // Использовать поле «Факультет / Школа»
    private boolean _useSpecialityField;     // Использовать поле «Направление (специальность)»
    private boolean _useProfileField;     // Использовать поле «Профиль (специализация)»
    private boolean _useDopEduLevelField;     // Использовать поле «Дополнительная образовательная программа / Дополнительная квалификация»
    private boolean _useEncourageltyField;     // Использовать поле «Поощрение / взыскание»
    private boolean _useEncourageltyEndDateField;     // Использовать поле «Дата снятия поощрения / взыскания»
    private boolean _useGrantSizeField;     // Использовать поле «Размер стипендии / выплаты»
    private boolean _useGrantCategoryField;     // Использовать поле «Категория стипендии / выплаты»
    private boolean _usePracticeTypeField;     // Использовать поле «Вид практики»
    private boolean _usePracticePlaceField;     // Использовать поле «Место прохождения практики»
    private boolean _usePracticeOwnershipTypeField;     // Использовать поле «Форма собственности предприятия прохождения практики»
    private boolean _usePracticeSettlementField;     // Использовать поле «Город прохождения практики»
    private boolean _usePracticePeriodField;     // Использовать поле «Сроки практики»
    private boolean _usePracticeSupervisorField;     // Использовать поле «Руководитель от вуза»

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип приказа. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public StudentExtractType getStudentExtractType()
    {
        return _studentExtractType;
    }

    /**
     * @param studentExtractType Тип приказа. Свойство не может быть null и должно быть уникальным.
     */
    public void setStudentExtractType(StudentExtractType studentExtractType)
    {
        dirty(_studentExtractType, studentExtractType);
        _studentExtractType = studentExtractType;
    }

    /**
     * @return Использовать поле «Причина». Свойство не может быть null.
     */
    @NotNull
    public boolean isUseReasonField()
    {
        return _useReasonField;
    }

    /**
     * @param useReasonField Использовать поле «Причина». Свойство не может быть null.
     */
    public void setUseReasonField(boolean useReasonField)
    {
        dirty(_useReasonField, useReasonField);
        _useReasonField = useReasonField;
    }

    /**
     * @return Использовать поле «Курс». Свойство не может быть null.
     */
    @NotNull
    public boolean isUseCourseField()
    {
        return _useCourseField;
    }

    /**
     * @param useCourseField Использовать поле «Курс». Свойство не может быть null.
     */
    public void setUseCourseField(boolean useCourseField)
    {
        dirty(_useCourseField, useCourseField);
        _useCourseField = useCourseField;
    }

    /**
     * @return Использовать поле «Семестр». Свойство не может быть null.
     */
    @NotNull
    public boolean isUseTermField()
    {
        return _useTermField;
    }

    /**
     * @param useTermField Использовать поле «Семестр». Свойство не может быть null.
     */
    public void setUseTermField(boolean useTermField)
    {
        dirty(_useTermField, useTermField);
        _useTermField = useTermField;
    }

    /**
     * @return Использовать поле «Институт». Свойство не может быть null.
     */
    @NotNull
    public boolean isUseInstituteField()
    {
        return _useInstituteField;
    }

    /**
     * @param useInstituteField Использовать поле «Институт». Свойство не может быть null.
     */
    public void setUseInstituteField(boolean useInstituteField)
    {
        dirty(_useInstituteField, useInstituteField);
        _useInstituteField = useInstituteField;
    }

    /**
     * @return Использовать поле «Факультет / Школа». Свойство не может быть null.
     */
    @NotNull
    public boolean isUseFacultyField()
    {
        return _useFacultyField;
    }

    /**
     * @param useFacultyField Использовать поле «Факультет / Школа». Свойство не может быть null.
     */
    public void setUseFacultyField(boolean useFacultyField)
    {
        dirty(_useFacultyField, useFacultyField);
        _useFacultyField = useFacultyField;
    }

    /**
     * @return Использовать поле «Направление (специальность)». Свойство не может быть null.
     */
    @NotNull
    public boolean isUseSpecialityField()
    {
        return _useSpecialityField;
    }

    /**
     * @param useSpecialityField Использовать поле «Направление (специальность)». Свойство не может быть null.
     */
    public void setUseSpecialityField(boolean useSpecialityField)
    {
        dirty(_useSpecialityField, useSpecialityField);
        _useSpecialityField = useSpecialityField;
    }

    /**
     * @return Использовать поле «Профиль (специализация)». Свойство не может быть null.
     */
    @NotNull
    public boolean isUseProfileField()
    {
        return _useProfileField;
    }

    /**
     * @param useProfileField Использовать поле «Профиль (специализация)». Свойство не может быть null.
     */
    public void setUseProfileField(boolean useProfileField)
    {
        dirty(_useProfileField, useProfileField);
        _useProfileField = useProfileField;
    }

    /**
     * @return Использовать поле «Дополнительная образовательная программа / Дополнительная квалификация». Свойство не может быть null.
     */
    @NotNull
    public boolean isUseDopEduLevelField()
    {
        return _useDopEduLevelField;
    }

    /**
     * @param useDopEduLevelField Использовать поле «Дополнительная образовательная программа / Дополнительная квалификация». Свойство не может быть null.
     */
    public void setUseDopEduLevelField(boolean useDopEduLevelField)
    {
        dirty(_useDopEduLevelField, useDopEduLevelField);
        _useDopEduLevelField = useDopEduLevelField;
    }

    /**
     * @return Использовать поле «Поощрение / взыскание». Свойство не может быть null.
     */
    @NotNull
    public boolean isUseEncourageltyField()
    {
        return _useEncourageltyField;
    }

    /**
     * @param useEncourageltyField Использовать поле «Поощрение / взыскание». Свойство не может быть null.
     */
    public void setUseEncourageltyField(boolean useEncourageltyField)
    {
        dirty(_useEncourageltyField, useEncourageltyField);
        _useEncourageltyField = useEncourageltyField;
    }

    /**
     * @return Использовать поле «Дата снятия поощрения / взыскания». Свойство не может быть null.
     */
    @NotNull
    public boolean isUseEncourageltyEndDateField()
    {
        return _useEncourageltyEndDateField;
    }

    /**
     * @param useEncourageltyEndDateField Использовать поле «Дата снятия поощрения / взыскания». Свойство не может быть null.
     */
    public void setUseEncourageltyEndDateField(boolean useEncourageltyEndDateField)
    {
        dirty(_useEncourageltyEndDateField, useEncourageltyEndDateField);
        _useEncourageltyEndDateField = useEncourageltyEndDateField;
    }

    /**
     * @return Использовать поле «Размер стипендии / выплаты». Свойство не может быть null.
     */
    @NotNull
    public boolean isUseGrantSizeField()
    {
        return _useGrantSizeField;
    }

    /**
     * @param useGrantSizeField Использовать поле «Размер стипендии / выплаты». Свойство не может быть null.
     */
    public void setUseGrantSizeField(boolean useGrantSizeField)
    {
        dirty(_useGrantSizeField, useGrantSizeField);
        _useGrantSizeField = useGrantSizeField;
    }

    /**
     * @return Использовать поле «Категория стипендии / выплаты». Свойство не может быть null.
     */
    @NotNull
    public boolean isUseGrantCategoryField()
    {
        return _useGrantCategoryField;
    }

    /**
     * @param useGrantCategoryField Использовать поле «Категория стипендии / выплаты». Свойство не может быть null.
     */
    public void setUseGrantCategoryField(boolean useGrantCategoryField)
    {
        dirty(_useGrantCategoryField, useGrantCategoryField);
        _useGrantCategoryField = useGrantCategoryField;
    }

    /**
     * @return Использовать поле «Вид практики». Свойство не может быть null.
     */
    @NotNull
    public boolean isUsePracticeTypeField()
    {
        return _usePracticeTypeField;
    }

    /**
     * @param usePracticeTypeField Использовать поле «Вид практики». Свойство не может быть null.
     */
    public void setUsePracticeTypeField(boolean usePracticeTypeField)
    {
        dirty(_usePracticeTypeField, usePracticeTypeField);
        _usePracticeTypeField = usePracticeTypeField;
    }

    /**
     * @return Использовать поле «Место прохождения практики». Свойство не может быть null.
     */
    @NotNull
    public boolean isUsePracticePlaceField()
    {
        return _usePracticePlaceField;
    }

    /**
     * @param usePracticePlaceField Использовать поле «Место прохождения практики». Свойство не может быть null.
     */
    public void setUsePracticePlaceField(boolean usePracticePlaceField)
    {
        dirty(_usePracticePlaceField, usePracticePlaceField);
        _usePracticePlaceField = usePracticePlaceField;
    }

    /**
     * @return Использовать поле «Форма собственности предприятия прохождения практики». Свойство не может быть null.
     */
    @NotNull
    public boolean isUsePracticeOwnershipTypeField()
    {
        return _usePracticeOwnershipTypeField;
    }

    /**
     * @param usePracticeOwnershipTypeField Использовать поле «Форма собственности предприятия прохождения практики». Свойство не может быть null.
     */
    public void setUsePracticeOwnershipTypeField(boolean usePracticeOwnershipTypeField)
    {
        dirty(_usePracticeOwnershipTypeField, usePracticeOwnershipTypeField);
        _usePracticeOwnershipTypeField = usePracticeOwnershipTypeField;
    }

    /**
     * @return Использовать поле «Город прохождения практики». Свойство не может быть null.
     */
    @NotNull
    public boolean isUsePracticeSettlementField()
    {
        return _usePracticeSettlementField;
    }

    /**
     * @param usePracticeSettlementField Использовать поле «Город прохождения практики». Свойство не может быть null.
     */
    public void setUsePracticeSettlementField(boolean usePracticeSettlementField)
    {
        dirty(_usePracticeSettlementField, usePracticeSettlementField);
        _usePracticeSettlementField = usePracticeSettlementField;
    }

    /**
     * @return Использовать поле «Сроки практики». Свойство не может быть null.
     */
    @NotNull
    public boolean isUsePracticePeriodField()
    {
        return _usePracticePeriodField;
    }

    /**
     * @param usePracticePeriodField Использовать поле «Сроки практики». Свойство не может быть null.
     */
    public void setUsePracticePeriodField(boolean usePracticePeriodField)
    {
        dirty(_usePracticePeriodField, usePracticePeriodField);
        _usePracticePeriodField = usePracticePeriodField;
    }

    /**
     * @return Использовать поле «Руководитель от вуза». Свойство не может быть null.
     */
    @NotNull
    public boolean isUsePracticeSupervisorField()
    {
        return _usePracticeSupervisorField;
    }

    /**
     * @param usePracticeSupervisorField Использовать поле «Руководитель от вуза». Свойство не может быть null.
     */
    public void setUsePracticeSupervisorField(boolean usePracticeSupervisorField)
    {
        dirty(_usePracticeSupervisorField, usePracticeSupervisorField);
        _usePracticeSupervisorField = usePracticeSupervisorField;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OtherStudentFieldsSettingsGen)
        {
            if (withNaturalIdProperties)
            {
                setStudentExtractType(((OtherStudentFieldsSettings)another).getStudentExtractType());
            }
            setUseReasonField(((OtherStudentFieldsSettings)another).isUseReasonField());
            setUseCourseField(((OtherStudentFieldsSettings)another).isUseCourseField());
            setUseTermField(((OtherStudentFieldsSettings)another).isUseTermField());
            setUseInstituteField(((OtherStudentFieldsSettings)another).isUseInstituteField());
            setUseFacultyField(((OtherStudentFieldsSettings)another).isUseFacultyField());
            setUseSpecialityField(((OtherStudentFieldsSettings)another).isUseSpecialityField());
            setUseProfileField(((OtherStudentFieldsSettings)another).isUseProfileField());
            setUseDopEduLevelField(((OtherStudentFieldsSettings)another).isUseDopEduLevelField());
            setUseEncourageltyField(((OtherStudentFieldsSettings)another).isUseEncourageltyField());
            setUseEncourageltyEndDateField(((OtherStudentFieldsSettings)another).isUseEncourageltyEndDateField());
            setUseGrantSizeField(((OtherStudentFieldsSettings)another).isUseGrantSizeField());
            setUseGrantCategoryField(((OtherStudentFieldsSettings)another).isUseGrantCategoryField());
            setUsePracticeTypeField(((OtherStudentFieldsSettings)another).isUsePracticeTypeField());
            setUsePracticePlaceField(((OtherStudentFieldsSettings)another).isUsePracticePlaceField());
            setUsePracticeOwnershipTypeField(((OtherStudentFieldsSettings)another).isUsePracticeOwnershipTypeField());
            setUsePracticeSettlementField(((OtherStudentFieldsSettings)another).isUsePracticeSettlementField());
            setUsePracticePeriodField(((OtherStudentFieldsSettings)another).isUsePracticePeriodField());
            setUsePracticeSupervisorField(((OtherStudentFieldsSettings)another).isUsePracticeSupervisorField());
        }
    }

    public INaturalId<OtherStudentFieldsSettingsGen> getNaturalId()
    {
        return new NaturalId(getStudentExtractType());
    }

    public static class NaturalId extends NaturalIdBase<OtherStudentFieldsSettingsGen>
    {
        private static final String PROXY_NAME = "OtherStudentFieldsSettingsNaturalProxy";

        private Long _studentExtractType;

        public NaturalId()
        {}

        public NaturalId(StudentExtractType studentExtractType)
        {
            _studentExtractType = ((IEntity) studentExtractType).getId();
        }

        public Long getStudentExtractType()
        {
            return _studentExtractType;
        }

        public void setStudentExtractType(Long studentExtractType)
        {
            _studentExtractType = studentExtractType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof OtherStudentFieldsSettingsGen.NaturalId) ) return false;

            OtherStudentFieldsSettingsGen.NaturalId that = (NaturalId) o;

            if( !equals(getStudentExtractType(), that.getStudentExtractType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStudentExtractType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStudentExtractType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OtherStudentFieldsSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OtherStudentFieldsSettings.class;
        }

        public T newInstance()
        {
            return (T) new OtherStudentFieldsSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentExtractType":
                    return obj.getStudentExtractType();
                case "useReasonField":
                    return obj.isUseReasonField();
                case "useCourseField":
                    return obj.isUseCourseField();
                case "useTermField":
                    return obj.isUseTermField();
                case "useInstituteField":
                    return obj.isUseInstituteField();
                case "useFacultyField":
                    return obj.isUseFacultyField();
                case "useSpecialityField":
                    return obj.isUseSpecialityField();
                case "useProfileField":
                    return obj.isUseProfileField();
                case "useDopEduLevelField":
                    return obj.isUseDopEduLevelField();
                case "useEncourageltyField":
                    return obj.isUseEncourageltyField();
                case "useEncourageltyEndDateField":
                    return obj.isUseEncourageltyEndDateField();
                case "useGrantSizeField":
                    return obj.isUseGrantSizeField();
                case "useGrantCategoryField":
                    return obj.isUseGrantCategoryField();
                case "usePracticeTypeField":
                    return obj.isUsePracticeTypeField();
                case "usePracticePlaceField":
                    return obj.isUsePracticePlaceField();
                case "usePracticeOwnershipTypeField":
                    return obj.isUsePracticeOwnershipTypeField();
                case "usePracticeSettlementField":
                    return obj.isUsePracticeSettlementField();
                case "usePracticePeriodField":
                    return obj.isUsePracticePeriodField();
                case "usePracticeSupervisorField":
                    return obj.isUsePracticeSupervisorField();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentExtractType":
                    obj.setStudentExtractType((StudentExtractType) value);
                    return;
                case "useReasonField":
                    obj.setUseReasonField((Boolean) value);
                    return;
                case "useCourseField":
                    obj.setUseCourseField((Boolean) value);
                    return;
                case "useTermField":
                    obj.setUseTermField((Boolean) value);
                    return;
                case "useInstituteField":
                    obj.setUseInstituteField((Boolean) value);
                    return;
                case "useFacultyField":
                    obj.setUseFacultyField((Boolean) value);
                    return;
                case "useSpecialityField":
                    obj.setUseSpecialityField((Boolean) value);
                    return;
                case "useProfileField":
                    obj.setUseProfileField((Boolean) value);
                    return;
                case "useDopEduLevelField":
                    obj.setUseDopEduLevelField((Boolean) value);
                    return;
                case "useEncourageltyField":
                    obj.setUseEncourageltyField((Boolean) value);
                    return;
                case "useEncourageltyEndDateField":
                    obj.setUseEncourageltyEndDateField((Boolean) value);
                    return;
                case "useGrantSizeField":
                    obj.setUseGrantSizeField((Boolean) value);
                    return;
                case "useGrantCategoryField":
                    obj.setUseGrantCategoryField((Boolean) value);
                    return;
                case "usePracticeTypeField":
                    obj.setUsePracticeTypeField((Boolean) value);
                    return;
                case "usePracticePlaceField":
                    obj.setUsePracticePlaceField((Boolean) value);
                    return;
                case "usePracticeOwnershipTypeField":
                    obj.setUsePracticeOwnershipTypeField((Boolean) value);
                    return;
                case "usePracticeSettlementField":
                    obj.setUsePracticeSettlementField((Boolean) value);
                    return;
                case "usePracticePeriodField":
                    obj.setUsePracticePeriodField((Boolean) value);
                    return;
                case "usePracticeSupervisorField":
                    obj.setUsePracticeSupervisorField((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentExtractType":
                        return true;
                case "useReasonField":
                        return true;
                case "useCourseField":
                        return true;
                case "useTermField":
                        return true;
                case "useInstituteField":
                        return true;
                case "useFacultyField":
                        return true;
                case "useSpecialityField":
                        return true;
                case "useProfileField":
                        return true;
                case "useDopEduLevelField":
                        return true;
                case "useEncourageltyField":
                        return true;
                case "useEncourageltyEndDateField":
                        return true;
                case "useGrantSizeField":
                        return true;
                case "useGrantCategoryField":
                        return true;
                case "usePracticeTypeField":
                        return true;
                case "usePracticePlaceField":
                        return true;
                case "usePracticeOwnershipTypeField":
                        return true;
                case "usePracticeSettlementField":
                        return true;
                case "usePracticePeriodField":
                        return true;
                case "usePracticeSupervisorField":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentExtractType":
                    return true;
                case "useReasonField":
                    return true;
                case "useCourseField":
                    return true;
                case "useTermField":
                    return true;
                case "useInstituteField":
                    return true;
                case "useFacultyField":
                    return true;
                case "useSpecialityField":
                    return true;
                case "useProfileField":
                    return true;
                case "useDopEduLevelField":
                    return true;
                case "useEncourageltyField":
                    return true;
                case "useEncourageltyEndDateField":
                    return true;
                case "useGrantSizeField":
                    return true;
                case "useGrantCategoryField":
                    return true;
                case "usePracticeTypeField":
                    return true;
                case "usePracticePlaceField":
                    return true;
                case "usePracticeOwnershipTypeField":
                    return true;
                case "usePracticeSettlementField":
                    return true;
                case "usePracticePeriodField":
                    return true;
                case "usePracticeSupervisorField":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentExtractType":
                    return StudentExtractType.class;
                case "useReasonField":
                    return Boolean.class;
                case "useCourseField":
                    return Boolean.class;
                case "useTermField":
                    return Boolean.class;
                case "useInstituteField":
                    return Boolean.class;
                case "useFacultyField":
                    return Boolean.class;
                case "useSpecialityField":
                    return Boolean.class;
                case "useProfileField":
                    return Boolean.class;
                case "useDopEduLevelField":
                    return Boolean.class;
                case "useEncourageltyField":
                    return Boolean.class;
                case "useEncourageltyEndDateField":
                    return Boolean.class;
                case "useGrantSizeField":
                    return Boolean.class;
                case "useGrantCategoryField":
                    return Boolean.class;
                case "usePracticeTypeField":
                    return Boolean.class;
                case "usePracticePlaceField":
                    return Boolean.class;
                case "usePracticeOwnershipTypeField":
                    return Boolean.class;
                case "usePracticeSettlementField":
                    return Boolean.class;
                case "usePracticePeriodField":
                    return Boolean.class;
                case "usePracticeSupervisorField":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OtherStudentFieldsSettings> _dslPath = new Path<OtherStudentFieldsSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OtherStudentFieldsSettings");
    }
            

    /**
     * @return Тип приказа. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#getStudentExtractType()
     */
    public static StudentExtractType.Path<StudentExtractType> studentExtractType()
    {
        return _dslPath.studentExtractType();
    }

    /**
     * @return Использовать поле «Причина». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseReasonField()
     */
    public static PropertyPath<Boolean> useReasonField()
    {
        return _dslPath.useReasonField();
    }

    /**
     * @return Использовать поле «Курс». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseCourseField()
     */
    public static PropertyPath<Boolean> useCourseField()
    {
        return _dslPath.useCourseField();
    }

    /**
     * @return Использовать поле «Семестр». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseTermField()
     */
    public static PropertyPath<Boolean> useTermField()
    {
        return _dslPath.useTermField();
    }

    /**
     * @return Использовать поле «Институт». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseInstituteField()
     */
    public static PropertyPath<Boolean> useInstituteField()
    {
        return _dslPath.useInstituteField();
    }

    /**
     * @return Использовать поле «Факультет / Школа». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseFacultyField()
     */
    public static PropertyPath<Boolean> useFacultyField()
    {
        return _dslPath.useFacultyField();
    }

    /**
     * @return Использовать поле «Направление (специальность)». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseSpecialityField()
     */
    public static PropertyPath<Boolean> useSpecialityField()
    {
        return _dslPath.useSpecialityField();
    }

    /**
     * @return Использовать поле «Профиль (специализация)». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseProfileField()
     */
    public static PropertyPath<Boolean> useProfileField()
    {
        return _dslPath.useProfileField();
    }

    /**
     * @return Использовать поле «Дополнительная образовательная программа / Дополнительная квалификация». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseDopEduLevelField()
     */
    public static PropertyPath<Boolean> useDopEduLevelField()
    {
        return _dslPath.useDopEduLevelField();
    }

    /**
     * @return Использовать поле «Поощрение / взыскание». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseEncourageltyField()
     */
    public static PropertyPath<Boolean> useEncourageltyField()
    {
        return _dslPath.useEncourageltyField();
    }

    /**
     * @return Использовать поле «Дата снятия поощрения / взыскания». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseEncourageltyEndDateField()
     */
    public static PropertyPath<Boolean> useEncourageltyEndDateField()
    {
        return _dslPath.useEncourageltyEndDateField();
    }

    /**
     * @return Использовать поле «Размер стипендии / выплаты». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseGrantSizeField()
     */
    public static PropertyPath<Boolean> useGrantSizeField()
    {
        return _dslPath.useGrantSizeField();
    }

    /**
     * @return Использовать поле «Категория стипендии / выплаты». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseGrantCategoryField()
     */
    public static PropertyPath<Boolean> useGrantCategoryField()
    {
        return _dslPath.useGrantCategoryField();
    }

    /**
     * @return Использовать поле «Вид практики». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUsePracticeTypeField()
     */
    public static PropertyPath<Boolean> usePracticeTypeField()
    {
        return _dslPath.usePracticeTypeField();
    }

    /**
     * @return Использовать поле «Место прохождения практики». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUsePracticePlaceField()
     */
    public static PropertyPath<Boolean> usePracticePlaceField()
    {
        return _dslPath.usePracticePlaceField();
    }

    /**
     * @return Использовать поле «Форма собственности предприятия прохождения практики». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUsePracticeOwnershipTypeField()
     */
    public static PropertyPath<Boolean> usePracticeOwnershipTypeField()
    {
        return _dslPath.usePracticeOwnershipTypeField();
    }

    /**
     * @return Использовать поле «Город прохождения практики». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUsePracticeSettlementField()
     */
    public static PropertyPath<Boolean> usePracticeSettlementField()
    {
        return _dslPath.usePracticeSettlementField();
    }

    /**
     * @return Использовать поле «Сроки практики». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUsePracticePeriodField()
     */
    public static PropertyPath<Boolean> usePracticePeriodField()
    {
        return _dslPath.usePracticePeriodField();
    }

    /**
     * @return Использовать поле «Руководитель от вуза». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUsePracticeSupervisorField()
     */
    public static PropertyPath<Boolean> usePracticeSupervisorField()
    {
        return _dslPath.usePracticeSupervisorField();
    }

    public static class Path<E extends OtherStudentFieldsSettings> extends EntityPath<E>
    {
        private StudentExtractType.Path<StudentExtractType> _studentExtractType;
        private PropertyPath<Boolean> _useReasonField;
        private PropertyPath<Boolean> _useCourseField;
        private PropertyPath<Boolean> _useTermField;
        private PropertyPath<Boolean> _useInstituteField;
        private PropertyPath<Boolean> _useFacultyField;
        private PropertyPath<Boolean> _useSpecialityField;
        private PropertyPath<Boolean> _useProfileField;
        private PropertyPath<Boolean> _useDopEduLevelField;
        private PropertyPath<Boolean> _useEncourageltyField;
        private PropertyPath<Boolean> _useEncourageltyEndDateField;
        private PropertyPath<Boolean> _useGrantSizeField;
        private PropertyPath<Boolean> _useGrantCategoryField;
        private PropertyPath<Boolean> _usePracticeTypeField;
        private PropertyPath<Boolean> _usePracticePlaceField;
        private PropertyPath<Boolean> _usePracticeOwnershipTypeField;
        private PropertyPath<Boolean> _usePracticeSettlementField;
        private PropertyPath<Boolean> _usePracticePeriodField;
        private PropertyPath<Boolean> _usePracticeSupervisorField;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип приказа. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#getStudentExtractType()
     */
        public StudentExtractType.Path<StudentExtractType> studentExtractType()
        {
            if(_studentExtractType == null )
                _studentExtractType = new StudentExtractType.Path<StudentExtractType>(L_STUDENT_EXTRACT_TYPE, this);
            return _studentExtractType;
        }

    /**
     * @return Использовать поле «Причина». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseReasonField()
     */
        public PropertyPath<Boolean> useReasonField()
        {
            if(_useReasonField == null )
                _useReasonField = new PropertyPath<Boolean>(OtherStudentFieldsSettingsGen.P_USE_REASON_FIELD, this);
            return _useReasonField;
        }

    /**
     * @return Использовать поле «Курс». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseCourseField()
     */
        public PropertyPath<Boolean> useCourseField()
        {
            if(_useCourseField == null )
                _useCourseField = new PropertyPath<Boolean>(OtherStudentFieldsSettingsGen.P_USE_COURSE_FIELD, this);
            return _useCourseField;
        }

    /**
     * @return Использовать поле «Семестр». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseTermField()
     */
        public PropertyPath<Boolean> useTermField()
        {
            if(_useTermField == null )
                _useTermField = new PropertyPath<Boolean>(OtherStudentFieldsSettingsGen.P_USE_TERM_FIELD, this);
            return _useTermField;
        }

    /**
     * @return Использовать поле «Институт». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseInstituteField()
     */
        public PropertyPath<Boolean> useInstituteField()
        {
            if(_useInstituteField == null )
                _useInstituteField = new PropertyPath<Boolean>(OtherStudentFieldsSettingsGen.P_USE_INSTITUTE_FIELD, this);
            return _useInstituteField;
        }

    /**
     * @return Использовать поле «Факультет / Школа». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseFacultyField()
     */
        public PropertyPath<Boolean> useFacultyField()
        {
            if(_useFacultyField == null )
                _useFacultyField = new PropertyPath<Boolean>(OtherStudentFieldsSettingsGen.P_USE_FACULTY_FIELD, this);
            return _useFacultyField;
        }

    /**
     * @return Использовать поле «Направление (специальность)». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseSpecialityField()
     */
        public PropertyPath<Boolean> useSpecialityField()
        {
            if(_useSpecialityField == null )
                _useSpecialityField = new PropertyPath<Boolean>(OtherStudentFieldsSettingsGen.P_USE_SPECIALITY_FIELD, this);
            return _useSpecialityField;
        }

    /**
     * @return Использовать поле «Профиль (специализация)». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseProfileField()
     */
        public PropertyPath<Boolean> useProfileField()
        {
            if(_useProfileField == null )
                _useProfileField = new PropertyPath<Boolean>(OtherStudentFieldsSettingsGen.P_USE_PROFILE_FIELD, this);
            return _useProfileField;
        }

    /**
     * @return Использовать поле «Дополнительная образовательная программа / Дополнительная квалификация». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseDopEduLevelField()
     */
        public PropertyPath<Boolean> useDopEduLevelField()
        {
            if(_useDopEduLevelField == null )
                _useDopEduLevelField = new PropertyPath<Boolean>(OtherStudentFieldsSettingsGen.P_USE_DOP_EDU_LEVEL_FIELD, this);
            return _useDopEduLevelField;
        }

    /**
     * @return Использовать поле «Поощрение / взыскание». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseEncourageltyField()
     */
        public PropertyPath<Boolean> useEncourageltyField()
        {
            if(_useEncourageltyField == null )
                _useEncourageltyField = new PropertyPath<Boolean>(OtherStudentFieldsSettingsGen.P_USE_ENCOURAGELTY_FIELD, this);
            return _useEncourageltyField;
        }

    /**
     * @return Использовать поле «Дата снятия поощрения / взыскания». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseEncourageltyEndDateField()
     */
        public PropertyPath<Boolean> useEncourageltyEndDateField()
        {
            if(_useEncourageltyEndDateField == null )
                _useEncourageltyEndDateField = new PropertyPath<Boolean>(OtherStudentFieldsSettingsGen.P_USE_ENCOURAGELTY_END_DATE_FIELD, this);
            return _useEncourageltyEndDateField;
        }

    /**
     * @return Использовать поле «Размер стипендии / выплаты». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseGrantSizeField()
     */
        public PropertyPath<Boolean> useGrantSizeField()
        {
            if(_useGrantSizeField == null )
                _useGrantSizeField = new PropertyPath<Boolean>(OtherStudentFieldsSettingsGen.P_USE_GRANT_SIZE_FIELD, this);
            return _useGrantSizeField;
        }

    /**
     * @return Использовать поле «Категория стипендии / выплаты». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUseGrantCategoryField()
     */
        public PropertyPath<Boolean> useGrantCategoryField()
        {
            if(_useGrantCategoryField == null )
                _useGrantCategoryField = new PropertyPath<Boolean>(OtherStudentFieldsSettingsGen.P_USE_GRANT_CATEGORY_FIELD, this);
            return _useGrantCategoryField;
        }

    /**
     * @return Использовать поле «Вид практики». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUsePracticeTypeField()
     */
        public PropertyPath<Boolean> usePracticeTypeField()
        {
            if(_usePracticeTypeField == null )
                _usePracticeTypeField = new PropertyPath<Boolean>(OtherStudentFieldsSettingsGen.P_USE_PRACTICE_TYPE_FIELD, this);
            return _usePracticeTypeField;
        }

    /**
     * @return Использовать поле «Место прохождения практики». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUsePracticePlaceField()
     */
        public PropertyPath<Boolean> usePracticePlaceField()
        {
            if(_usePracticePlaceField == null )
                _usePracticePlaceField = new PropertyPath<Boolean>(OtherStudentFieldsSettingsGen.P_USE_PRACTICE_PLACE_FIELD, this);
            return _usePracticePlaceField;
        }

    /**
     * @return Использовать поле «Форма собственности предприятия прохождения практики». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUsePracticeOwnershipTypeField()
     */
        public PropertyPath<Boolean> usePracticeOwnershipTypeField()
        {
            if(_usePracticeOwnershipTypeField == null )
                _usePracticeOwnershipTypeField = new PropertyPath<Boolean>(OtherStudentFieldsSettingsGen.P_USE_PRACTICE_OWNERSHIP_TYPE_FIELD, this);
            return _usePracticeOwnershipTypeField;
        }

    /**
     * @return Использовать поле «Город прохождения практики». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUsePracticeSettlementField()
     */
        public PropertyPath<Boolean> usePracticeSettlementField()
        {
            if(_usePracticeSettlementField == null )
                _usePracticeSettlementField = new PropertyPath<Boolean>(OtherStudentFieldsSettingsGen.P_USE_PRACTICE_SETTLEMENT_FIELD, this);
            return _usePracticeSettlementField;
        }

    /**
     * @return Использовать поле «Сроки практики». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUsePracticePeriodField()
     */
        public PropertyPath<Boolean> usePracticePeriodField()
        {
            if(_usePracticePeriodField == null )
                _usePracticePeriodField = new PropertyPath<Boolean>(OtherStudentFieldsSettingsGen.P_USE_PRACTICE_PERIOD_FIELD, this);
            return _usePracticePeriodField;
        }

    /**
     * @return Использовать поле «Руководитель от вуза». Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings#isUsePracticeSupervisorField()
     */
        public PropertyPath<Boolean> usePracticeSupervisorField()
        {
            if(_usePracticeSupervisorField == null )
                _usePracticeSupervisorField = new PropertyPath<Boolean>(OtherStudentFieldsSettingsGen.P_USE_PRACTICE_SUPERVISOR_FIELD, this);
            return _usePracticeSupervisorField;
        }

        public Class getEntityClass()
        {
            return OtherStudentFieldsSettings.class;
        }

        public String getEntityName()
        {
            return "otherStudentFieldsSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
