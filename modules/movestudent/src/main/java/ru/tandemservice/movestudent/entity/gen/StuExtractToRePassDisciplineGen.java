package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StuExtractToRePassDiscipline;
import ru.tandemservice.uni.entity.catalog.Term;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные о пересдаваемой дисциплине для выписки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StuExtractToRePassDisciplineGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.StuExtractToRePassDiscipline";
    public static final String ENTITY_NAME = "stuExtractToRePassDiscipline";
    public static final int VERSION_HASH = -1747142943;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String P_PRIORITY = "priority";
    public static final String P_DISCIPLINE = "discipline";
    public static final String P_CONTROL_ACTION = "controlAction";
    public static final String L_SRC_TERM = "srcTerm";

    private ModularStudentExtract _extract;     // Выписка из сборного приказа
    private int _priority;     // Порядок следования в тексте
    private String _discipline;     // Наименование дисциплины
    private String _controlAction;     // Форма промежуточного контроля
    private Term _srcTerm;     // Семестр чтения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа. Свойство не может быть null.
     */
    @NotNull
    public ModularStudentExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка из сборного приказа. Свойство не может быть null.
     */
    public void setExtract(ModularStudentExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Порядок следования в тексте. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Порядок следования в тексте. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Наименование дисциплины. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Наименование дисциплины. Свойство не может быть null.
     */
    public void setDiscipline(String discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Форма промежуточного контроля. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getControlAction()
    {
        return _controlAction;
    }

    /**
     * @param controlAction Форма промежуточного контроля. Свойство не может быть null.
     */
    public void setControlAction(String controlAction)
    {
        dirty(_controlAction, controlAction);
        _controlAction = controlAction;
    }

    /**
     * @return Семестр чтения. Свойство не может быть null.
     */
    @NotNull
    public Term getSrcTerm()
    {
        return _srcTerm;
    }

    /**
     * @param srcTerm Семестр чтения. Свойство не может быть null.
     */
    public void setSrcTerm(Term srcTerm)
    {
        dirty(_srcTerm, srcTerm);
        _srcTerm = srcTerm;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StuExtractToRePassDisciplineGen)
        {
            setExtract(((StuExtractToRePassDiscipline)another).getExtract());
            setPriority(((StuExtractToRePassDiscipline)another).getPriority());
            setDiscipline(((StuExtractToRePassDiscipline)another).getDiscipline());
            setControlAction(((StuExtractToRePassDiscipline)another).getControlAction());
            setSrcTerm(((StuExtractToRePassDiscipline)another).getSrcTerm());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StuExtractToRePassDisciplineGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StuExtractToRePassDiscipline.class;
        }

        public T newInstance()
        {
            return (T) new StuExtractToRePassDiscipline();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "priority":
                    return obj.getPriority();
                case "discipline":
                    return obj.getDiscipline();
                case "controlAction":
                    return obj.getControlAction();
                case "srcTerm":
                    return obj.getSrcTerm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((ModularStudentExtract) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "discipline":
                    obj.setDiscipline((String) value);
                    return;
                case "controlAction":
                    obj.setControlAction((String) value);
                    return;
                case "srcTerm":
                    obj.setSrcTerm((Term) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "priority":
                        return true;
                case "discipline":
                        return true;
                case "controlAction":
                        return true;
                case "srcTerm":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "priority":
                    return true;
                case "discipline":
                    return true;
                case "controlAction":
                    return true;
                case "srcTerm":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return ModularStudentExtract.class;
                case "priority":
                    return Integer.class;
                case "discipline":
                    return String.class;
                case "controlAction":
                    return String.class;
                case "srcTerm":
                    return Term.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StuExtractToRePassDiscipline> _dslPath = new Path<StuExtractToRePassDiscipline>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StuExtractToRePassDiscipline");
    }
            

    /**
     * @return Выписка из сборного приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToRePassDiscipline#getExtract()
     */
    public static ModularStudentExtract.Path<ModularStudentExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Порядок следования в тексте. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToRePassDiscipline#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Наименование дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToRePassDiscipline#getDiscipline()
     */
    public static PropertyPath<String> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Форма промежуточного контроля. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToRePassDiscipline#getControlAction()
     */
    public static PropertyPath<String> controlAction()
    {
        return _dslPath.controlAction();
    }

    /**
     * @return Семестр чтения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToRePassDiscipline#getSrcTerm()
     */
    public static Term.Path<Term> srcTerm()
    {
        return _dslPath.srcTerm();
    }

    public static class Path<E extends StuExtractToRePassDiscipline> extends EntityPath<E>
    {
        private ModularStudentExtract.Path<ModularStudentExtract> _extract;
        private PropertyPath<Integer> _priority;
        private PropertyPath<String> _discipline;
        private PropertyPath<String> _controlAction;
        private Term.Path<Term> _srcTerm;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToRePassDiscipline#getExtract()
     */
        public ModularStudentExtract.Path<ModularStudentExtract> extract()
        {
            if(_extract == null )
                _extract = new ModularStudentExtract.Path<ModularStudentExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Порядок следования в тексте. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToRePassDiscipline#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(StuExtractToRePassDisciplineGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Наименование дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToRePassDiscipline#getDiscipline()
     */
        public PropertyPath<String> discipline()
        {
            if(_discipline == null )
                _discipline = new PropertyPath<String>(StuExtractToRePassDisciplineGen.P_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Форма промежуточного контроля. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToRePassDiscipline#getControlAction()
     */
        public PropertyPath<String> controlAction()
        {
            if(_controlAction == null )
                _controlAction = new PropertyPath<String>(StuExtractToRePassDisciplineGen.P_CONTROL_ACTION, this);
            return _controlAction;
        }

    /**
     * @return Семестр чтения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StuExtractToRePassDiscipline#getSrcTerm()
     */
        public Term.Path<Term> srcTerm()
        {
            if(_srcTerm == null )
                _srcTerm = new Term.Path<Term>(L_SRC_TERM, this);
            return _srcTerm;
        }

        public Class getEntityClass()
        {
            return StuExtractToRePassDiscipline.class;
        }

        public String getEntityName()
        {
            return "stuExtractToRePassDiscipline";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
