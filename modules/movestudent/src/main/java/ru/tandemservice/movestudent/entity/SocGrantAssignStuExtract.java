package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IAssignPaymentExtract;
import ru.tandemservice.movestudent.entity.gen.SocGrantAssignStuExtractGen;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О назначении социальной стипендии
 */
public class SocGrantAssignStuExtract extends SocGrantAssignStuExtractGen implements IAssignPaymentExtract
{
    @Override
    public Date getBeginDate()
    {
        return getStartDate();
    }

    @Override
    public BigDecimal getPaymentAmount()
    {
        return getGrantSize() != null ? new BigDecimal(getGrantSize()) : null;
    }

    @Override
    public Date getPaymentBeginDate()
    {
        return getStartDate();
    }

    @Override
    public Date getPaymentEndDate()
    {
        return getEndDate();
    }
}