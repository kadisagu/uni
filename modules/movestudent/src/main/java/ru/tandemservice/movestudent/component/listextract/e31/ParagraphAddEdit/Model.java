/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e31.ParagraphAddEdit;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.CourseTransferExtStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.IExtEducationLevelModel;

import java.util.Date;
import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 24.09.2012
 */
public class Model extends AbstractListParagraphAddEditModel<CourseTransferExtStuListExtract> implements IGroupModel, IExtEducationLevelModel, IEducationLevelModel
{
    private Date _transferDate;
    private Course _course;
    private Group _group;
    private CompensationType _compensationType;
    private Course _courseOld;
    private Course _courseNew;
    private Group _groupNew;
    private EducationLevels _parentEduLevel;

    private ISelectModel _groupListModel;
    private List<CompensationType> _compensationTypeList;
    private ISelectModel _courseOldListModel;
    private ISelectModel _courseNewListModel;
    private ISelectModel _groupNewListModel;

    private boolean _compensationTypeDisabled;

    public Date getTransferDate()
    {
        return _transferDate;
    }

    public void setTransferDate(Date transferDate)
    {
        _transferDate = transferDate;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public Course getCourseOld()
    {
        return _courseOld;
    }

    public void setCourseOld(Course courseOld)
    {
        _courseOld = courseOld;
    }

    public Course getCourseNew()
    {
        return _courseNew;
    }

    public void setCourseNew(Course courseNew)
    {
        _courseNew = courseNew;
    }

    public Group getGroupNew()
    {
        return _groupNew;
    }

    public void setGroupNew(Group groupNew)
    {
        _groupNew = groupNew;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public ISelectModel getCourseOldListModel()
    {
        return _courseOldListModel;
    }

    public void setCourseOldListModel(ISelectModel courseOldListModel)
    {
        _courseOldListModel = courseOldListModel;
    }

    public ISelectModel getCourseNewListModel()
    {
        return _courseNewListModel;
    }

    public void setCourseNewListModel(ISelectModel courseNewListModel)
    {
        _courseNewListModel = courseNewListModel;
    }

    public ISelectModel getGroupNewListModel()
    {
        return _groupNewListModel;
    }

    public void setGroupNewListModel(ISelectModel groupNewListModel)
    {
        _groupNewListModel = groupNewListModel;
    }

    public boolean isCompensationTypeDisabled()
    {
        return _compensationTypeDisabled;
    }

    public void setCompensationTypeDisabled(boolean compensationTypeDisabled)
    {
        _compensationTypeDisabled = compensationTypeDisabled;
    }
    @Override
    public EducationLevels getParentEduLevel()
    {
        return _parentEduLevel;
    }

    public void setParentEduLevel(EducationLevels parentEduLevel)
    {
       _parentEduLevel=parentEduLevel;
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return getParagraph().getOrder().getOrgUnit();
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return null;
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return null;
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return null;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return null;
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return null;
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return null;
    }
}
