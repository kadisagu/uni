package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.fias.base.entity.ICitizenship;
import org.tandemframework.shared.fias.base.entity.gen.ICitizenshipGen;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Nationality;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О смене отчества
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChangeMiddleNameStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract";
    public static final String ENTITY_NAME = "changeMiddleNameStuExtract";
    public static final int VERSION_HASH = 555378187;
    private static IEntityMeta ENTITY_META;

    public static final String P_CHANGE_MIDDLE_NAME_DATE = "changeMiddleNameDate";
    public static final String P_FIRST_NAME_NEW = "firstNameNew";
    public static final String P_LAST_NAME_NEW = "lastNameNew";
    public static final String P_MIDDLE_NAME_NEW = "middleNameNew";
    public static final String P_CARD_SERIA = "cardSeria";
    public static final String P_CARD_NUMBER = "cardNumber";
    public static final String P_BIRTH_DATE = "birthDate";
    public static final String P_BIRTH_PLACE = "birthPlace";
    public static final String P_ISSUANCE_DATE = "issuanceDate";
    public static final String P_ISSUANCE_PLACE = "issuancePlace";
    public static final String P_REASON_STR = "reasonStr";
    public static final String L_NATIONALITY = "nationality";
    public static final String L_SEX = "sex";
    public static final String L_CITIZENSHIP = "citizenship";
    public static final String L_CARD_TYPE = "cardType";
    public static final String L_LAST_ACTIVE_IDENTITY_CARD = "lastActiveIdentityCard";
    public static final String L_IDENTITY_CARD_NEW = "identityCardNew";

    private Date _changeMiddleNameDate;     // Дата смены
    private String _firstNameNew;     // Новое имя
    private String _lastNameNew;     // Новая фамилия
    private String _middleNameNew;     // Новое отчество
    private String _cardSeria;     // Серия
    private String _cardNumber;     // Номер
    private Date _birthDate;     // Дата рождения
    private String _birthPlace;     // Место рождения
    private Date _issuanceDate;     // Дата выдачи удостоверения
    private String _issuancePlace;     // Кем выдано удостоверение
    private String _reasonStr;     // Причина изменения
    private Nationality _nationality;     // Национальные группы
    private Sex _sex;     // Пол
    private ICitizenship _citizenship;     // Гражданство
    private IdentityCardType _cardType;     // Тип удостоверения личности
    private IdentityCard _lastActiveIdentityCard;     // Тип последнего активного удостоверения личности
    private IdentityCard _identityCardNew;     // Новое удостоверение личности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата смены.
     */
    public Date getChangeMiddleNameDate()
    {
        return _changeMiddleNameDate;
    }

    /**
     * @param changeMiddleNameDate Дата смены.
     */
    public void setChangeMiddleNameDate(Date changeMiddleNameDate)
    {
        dirty(_changeMiddleNameDate, changeMiddleNameDate);
        _changeMiddleNameDate = changeMiddleNameDate;
    }

    /**
     * @return Новое имя. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFirstNameNew()
    {
        return _firstNameNew;
    }

    /**
     * @param firstNameNew Новое имя. Свойство не может быть null.
     */
    public void setFirstNameNew(String firstNameNew)
    {
        dirty(_firstNameNew, firstNameNew);
        _firstNameNew = firstNameNew;
    }

    /**
     * @return Новая фамилия. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLastNameNew()
    {
        return _lastNameNew;
    }

    /**
     * @param lastNameNew Новая фамилия. Свойство не может быть null.
     */
    public void setLastNameNew(String lastNameNew)
    {
        dirty(_lastNameNew, lastNameNew);
        _lastNameNew = lastNameNew;
    }

    /**
     * @return Новое отчество.
     */
    @Length(max=255)
    public String getMiddleNameNew()
    {
        return _middleNameNew;
    }

    /**
     * @param middleNameNew Новое отчество.
     */
    public void setMiddleNameNew(String middleNameNew)
    {
        dirty(_middleNameNew, middleNameNew);
        _middleNameNew = middleNameNew;
    }

    /**
     * @return Серия.
     */
    @Length(max=255)
    public String getCardSeria()
    {
        return _cardSeria;
    }

    /**
     * @param cardSeria Серия.
     */
    public void setCardSeria(String cardSeria)
    {
        dirty(_cardSeria, cardSeria);
        _cardSeria = cardSeria;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getCardNumber()
    {
        return _cardNumber;
    }

    /**
     * @param cardNumber Номер.
     */
    public void setCardNumber(String cardNumber)
    {
        dirty(_cardNumber, cardNumber);
        _cardNumber = cardNumber;
    }

    /**
     * @return Дата рождения.
     */
    public Date getBirthDate()
    {
        return _birthDate;
    }

    /**
     * @param birthDate Дата рождения.
     */
    public void setBirthDate(Date birthDate)
    {
        dirty(_birthDate, birthDate);
        _birthDate = birthDate;
    }

    /**
     * @return Место рождения.
     */
    @Length(max=255)
    public String getBirthPlace()
    {
        return _birthPlace;
    }

    /**
     * @param birthPlace Место рождения.
     */
    public void setBirthPlace(String birthPlace)
    {
        dirty(_birthPlace, birthPlace);
        _birthPlace = birthPlace;
    }

    /**
     * @return Дата выдачи удостоверения.
     */
    public Date getIssuanceDate()
    {
        return _issuanceDate;
    }

    /**
     * @param issuanceDate Дата выдачи удостоверения.
     */
    public void setIssuanceDate(Date issuanceDate)
    {
        dirty(_issuanceDate, issuanceDate);
        _issuanceDate = issuanceDate;
    }

    /**
     * @return Кем выдано удостоверение.
     */
    @Length(max=255)
    public String getIssuancePlace()
    {
        return _issuancePlace;
    }

    /**
     * @param issuancePlace Кем выдано удостоверение.
     */
    public void setIssuancePlace(String issuancePlace)
    {
        dirty(_issuancePlace, issuancePlace);
        _issuancePlace = issuancePlace;
    }

    /**
     * @return Причина изменения.
     */
    @Length(max=255)
    public String getReasonStr()
    {
        return _reasonStr;
    }

    /**
     * @param reasonStr Причина изменения.
     */
    public void setReasonStr(String reasonStr)
    {
        dirty(_reasonStr, reasonStr);
        _reasonStr = reasonStr;
    }

    /**
     * @return Национальные группы.
     */
    public Nationality getNationality()
    {
        return _nationality;
    }

    /**
     * @param nationality Национальные группы.
     */
    public void setNationality(Nationality nationality)
    {
        dirty(_nationality, nationality);
        _nationality = nationality;
    }

    /**
     * @return Пол. Свойство не может быть null.
     */
    @NotNull
    public Sex getSex()
    {
        return _sex;
    }

    /**
     * @param sex Пол. Свойство не может быть null.
     */
    public void setSex(Sex sex)
    {
        dirty(_sex, sex);
        _sex = sex;
    }

    /**
     * @return Гражданство. Свойство не может быть null.
     */
    @NotNull
    public ICitizenship getCitizenship()
    {
        return _citizenship;
    }

    /**
     * @param citizenship Гражданство. Свойство не может быть null.
     */
    public void setCitizenship(ICitizenship citizenship)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && citizenship!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(ICitizenship.class);
            IEntityMeta actual =  citizenship instanceof IEntity ? EntityRuntime.getMeta((IEntity) citizenship) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_citizenship, citizenship);
        _citizenship = citizenship;
    }

    /**
     * @return Тип удостоверения личности. Свойство не может быть null.
     */
    @NotNull
    public IdentityCardType getCardType()
    {
        return _cardType;
    }

    /**
     * @param cardType Тип удостоверения личности. Свойство не может быть null.
     */
    public void setCardType(IdentityCardType cardType)
    {
        dirty(_cardType, cardType);
        _cardType = cardType;
    }

    /**
     * @return Тип последнего активного удостоверения личности. Свойство не может быть null.
     */
    @NotNull
    public IdentityCard getLastActiveIdentityCard()
    {
        return _lastActiveIdentityCard;
    }

    /**
     * @param lastActiveIdentityCard Тип последнего активного удостоверения личности. Свойство не может быть null.
     */
    public void setLastActiveIdentityCard(IdentityCard lastActiveIdentityCard)
    {
        dirty(_lastActiveIdentityCard, lastActiveIdentityCard);
        _lastActiveIdentityCard = lastActiveIdentityCard;
    }

    /**
     * @return Новое удостоверение личности.
     */
    public IdentityCard getIdentityCardNew()
    {
        return _identityCardNew;
    }

    /**
     * @param identityCardNew Новое удостоверение личности.
     */
    public void setIdentityCardNew(IdentityCard identityCardNew)
    {
        dirty(_identityCardNew, identityCardNew);
        _identityCardNew = identityCardNew;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ChangeMiddleNameStuExtractGen)
        {
            setChangeMiddleNameDate(((ChangeMiddleNameStuExtract)another).getChangeMiddleNameDate());
            setFirstNameNew(((ChangeMiddleNameStuExtract)another).getFirstNameNew());
            setLastNameNew(((ChangeMiddleNameStuExtract)another).getLastNameNew());
            setMiddleNameNew(((ChangeMiddleNameStuExtract)another).getMiddleNameNew());
            setCardSeria(((ChangeMiddleNameStuExtract)another).getCardSeria());
            setCardNumber(((ChangeMiddleNameStuExtract)another).getCardNumber());
            setBirthDate(((ChangeMiddleNameStuExtract)another).getBirthDate());
            setBirthPlace(((ChangeMiddleNameStuExtract)another).getBirthPlace());
            setIssuanceDate(((ChangeMiddleNameStuExtract)another).getIssuanceDate());
            setIssuancePlace(((ChangeMiddleNameStuExtract)another).getIssuancePlace());
            setReasonStr(((ChangeMiddleNameStuExtract)another).getReasonStr());
            setNationality(((ChangeMiddleNameStuExtract)another).getNationality());
            setSex(((ChangeMiddleNameStuExtract)another).getSex());
            setCitizenship(((ChangeMiddleNameStuExtract)another).getCitizenship());
            setCardType(((ChangeMiddleNameStuExtract)another).getCardType());
            setLastActiveIdentityCard(((ChangeMiddleNameStuExtract)another).getLastActiveIdentityCard());
            setIdentityCardNew(((ChangeMiddleNameStuExtract)another).getIdentityCardNew());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChangeMiddleNameStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChangeMiddleNameStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new ChangeMiddleNameStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "changeMiddleNameDate":
                    return obj.getChangeMiddleNameDate();
                case "firstNameNew":
                    return obj.getFirstNameNew();
                case "lastNameNew":
                    return obj.getLastNameNew();
                case "middleNameNew":
                    return obj.getMiddleNameNew();
                case "cardSeria":
                    return obj.getCardSeria();
                case "cardNumber":
                    return obj.getCardNumber();
                case "birthDate":
                    return obj.getBirthDate();
                case "birthPlace":
                    return obj.getBirthPlace();
                case "issuanceDate":
                    return obj.getIssuanceDate();
                case "issuancePlace":
                    return obj.getIssuancePlace();
                case "reasonStr":
                    return obj.getReasonStr();
                case "nationality":
                    return obj.getNationality();
                case "sex":
                    return obj.getSex();
                case "citizenship":
                    return obj.getCitizenship();
                case "cardType":
                    return obj.getCardType();
                case "lastActiveIdentityCard":
                    return obj.getLastActiveIdentityCard();
                case "identityCardNew":
                    return obj.getIdentityCardNew();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "changeMiddleNameDate":
                    obj.setChangeMiddleNameDate((Date) value);
                    return;
                case "firstNameNew":
                    obj.setFirstNameNew((String) value);
                    return;
                case "lastNameNew":
                    obj.setLastNameNew((String) value);
                    return;
                case "middleNameNew":
                    obj.setMiddleNameNew((String) value);
                    return;
                case "cardSeria":
                    obj.setCardSeria((String) value);
                    return;
                case "cardNumber":
                    obj.setCardNumber((String) value);
                    return;
                case "birthDate":
                    obj.setBirthDate((Date) value);
                    return;
                case "birthPlace":
                    obj.setBirthPlace((String) value);
                    return;
                case "issuanceDate":
                    obj.setIssuanceDate((Date) value);
                    return;
                case "issuancePlace":
                    obj.setIssuancePlace((String) value);
                    return;
                case "reasonStr":
                    obj.setReasonStr((String) value);
                    return;
                case "nationality":
                    obj.setNationality((Nationality) value);
                    return;
                case "sex":
                    obj.setSex((Sex) value);
                    return;
                case "citizenship":
                    obj.setCitizenship((ICitizenship) value);
                    return;
                case "cardType":
                    obj.setCardType((IdentityCardType) value);
                    return;
                case "lastActiveIdentityCard":
                    obj.setLastActiveIdentityCard((IdentityCard) value);
                    return;
                case "identityCardNew":
                    obj.setIdentityCardNew((IdentityCard) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "changeMiddleNameDate":
                        return true;
                case "firstNameNew":
                        return true;
                case "lastNameNew":
                        return true;
                case "middleNameNew":
                        return true;
                case "cardSeria":
                        return true;
                case "cardNumber":
                        return true;
                case "birthDate":
                        return true;
                case "birthPlace":
                        return true;
                case "issuanceDate":
                        return true;
                case "issuancePlace":
                        return true;
                case "reasonStr":
                        return true;
                case "nationality":
                        return true;
                case "sex":
                        return true;
                case "citizenship":
                        return true;
                case "cardType":
                        return true;
                case "lastActiveIdentityCard":
                        return true;
                case "identityCardNew":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "changeMiddleNameDate":
                    return true;
                case "firstNameNew":
                    return true;
                case "lastNameNew":
                    return true;
                case "middleNameNew":
                    return true;
                case "cardSeria":
                    return true;
                case "cardNumber":
                    return true;
                case "birthDate":
                    return true;
                case "birthPlace":
                    return true;
                case "issuanceDate":
                    return true;
                case "issuancePlace":
                    return true;
                case "reasonStr":
                    return true;
                case "nationality":
                    return true;
                case "sex":
                    return true;
                case "citizenship":
                    return true;
                case "cardType":
                    return true;
                case "lastActiveIdentityCard":
                    return true;
                case "identityCardNew":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "changeMiddleNameDate":
                    return Date.class;
                case "firstNameNew":
                    return String.class;
                case "lastNameNew":
                    return String.class;
                case "middleNameNew":
                    return String.class;
                case "cardSeria":
                    return String.class;
                case "cardNumber":
                    return String.class;
                case "birthDate":
                    return Date.class;
                case "birthPlace":
                    return String.class;
                case "issuanceDate":
                    return Date.class;
                case "issuancePlace":
                    return String.class;
                case "reasonStr":
                    return String.class;
                case "nationality":
                    return Nationality.class;
                case "sex":
                    return Sex.class;
                case "citizenship":
                    return ICitizenship.class;
                case "cardType":
                    return IdentityCardType.class;
                case "lastActiveIdentityCard":
                    return IdentityCard.class;
                case "identityCardNew":
                    return IdentityCard.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChangeMiddleNameStuExtract> _dslPath = new Path<ChangeMiddleNameStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChangeMiddleNameStuExtract");
    }
            

    /**
     * @return Дата смены.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getChangeMiddleNameDate()
     */
    public static PropertyPath<Date> changeMiddleNameDate()
    {
        return _dslPath.changeMiddleNameDate();
    }

    /**
     * @return Новое имя. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getFirstNameNew()
     */
    public static PropertyPath<String> firstNameNew()
    {
        return _dslPath.firstNameNew();
    }

    /**
     * @return Новая фамилия. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getLastNameNew()
     */
    public static PropertyPath<String> lastNameNew()
    {
        return _dslPath.lastNameNew();
    }

    /**
     * @return Новое отчество.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getMiddleNameNew()
     */
    public static PropertyPath<String> middleNameNew()
    {
        return _dslPath.middleNameNew();
    }

    /**
     * @return Серия.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getCardSeria()
     */
    public static PropertyPath<String> cardSeria()
    {
        return _dslPath.cardSeria();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getCardNumber()
     */
    public static PropertyPath<String> cardNumber()
    {
        return _dslPath.cardNumber();
    }

    /**
     * @return Дата рождения.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getBirthDate()
     */
    public static PropertyPath<Date> birthDate()
    {
        return _dslPath.birthDate();
    }

    /**
     * @return Место рождения.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getBirthPlace()
     */
    public static PropertyPath<String> birthPlace()
    {
        return _dslPath.birthPlace();
    }

    /**
     * @return Дата выдачи удостоверения.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getIssuanceDate()
     */
    public static PropertyPath<Date> issuanceDate()
    {
        return _dslPath.issuanceDate();
    }

    /**
     * @return Кем выдано удостоверение.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getIssuancePlace()
     */
    public static PropertyPath<String> issuancePlace()
    {
        return _dslPath.issuancePlace();
    }

    /**
     * @return Причина изменения.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getReasonStr()
     */
    public static PropertyPath<String> reasonStr()
    {
        return _dslPath.reasonStr();
    }

    /**
     * @return Национальные группы.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getNationality()
     */
    public static Nationality.Path<Nationality> nationality()
    {
        return _dslPath.nationality();
    }

    /**
     * @return Пол. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getSex()
     */
    public static Sex.Path<Sex> sex()
    {
        return _dslPath.sex();
    }

    /**
     * @return Гражданство. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getCitizenship()
     */
    public static ICitizenshipGen.Path<ICitizenship> citizenship()
    {
        return _dslPath.citizenship();
    }

    /**
     * @return Тип удостоверения личности. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getCardType()
     */
    public static IdentityCardType.Path<IdentityCardType> cardType()
    {
        return _dslPath.cardType();
    }

    /**
     * @return Тип последнего активного удостоверения личности. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getLastActiveIdentityCard()
     */
    public static IdentityCard.Path<IdentityCard> lastActiveIdentityCard()
    {
        return _dslPath.lastActiveIdentityCard();
    }

    /**
     * @return Новое удостоверение личности.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getIdentityCardNew()
     */
    public static IdentityCard.Path<IdentityCard> identityCardNew()
    {
        return _dslPath.identityCardNew();
    }

    public static class Path<E extends ChangeMiddleNameStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _changeMiddleNameDate;
        private PropertyPath<String> _firstNameNew;
        private PropertyPath<String> _lastNameNew;
        private PropertyPath<String> _middleNameNew;
        private PropertyPath<String> _cardSeria;
        private PropertyPath<String> _cardNumber;
        private PropertyPath<Date> _birthDate;
        private PropertyPath<String> _birthPlace;
        private PropertyPath<Date> _issuanceDate;
        private PropertyPath<String> _issuancePlace;
        private PropertyPath<String> _reasonStr;
        private Nationality.Path<Nationality> _nationality;
        private Sex.Path<Sex> _sex;
        private ICitizenshipGen.Path<ICitizenship> _citizenship;
        private IdentityCardType.Path<IdentityCardType> _cardType;
        private IdentityCard.Path<IdentityCard> _lastActiveIdentityCard;
        private IdentityCard.Path<IdentityCard> _identityCardNew;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата смены.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getChangeMiddleNameDate()
     */
        public PropertyPath<Date> changeMiddleNameDate()
        {
            if(_changeMiddleNameDate == null )
                _changeMiddleNameDate = new PropertyPath<Date>(ChangeMiddleNameStuExtractGen.P_CHANGE_MIDDLE_NAME_DATE, this);
            return _changeMiddleNameDate;
        }

    /**
     * @return Новое имя. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getFirstNameNew()
     */
        public PropertyPath<String> firstNameNew()
        {
            if(_firstNameNew == null )
                _firstNameNew = new PropertyPath<String>(ChangeMiddleNameStuExtractGen.P_FIRST_NAME_NEW, this);
            return _firstNameNew;
        }

    /**
     * @return Новая фамилия. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getLastNameNew()
     */
        public PropertyPath<String> lastNameNew()
        {
            if(_lastNameNew == null )
                _lastNameNew = new PropertyPath<String>(ChangeMiddleNameStuExtractGen.P_LAST_NAME_NEW, this);
            return _lastNameNew;
        }

    /**
     * @return Новое отчество.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getMiddleNameNew()
     */
        public PropertyPath<String> middleNameNew()
        {
            if(_middleNameNew == null )
                _middleNameNew = new PropertyPath<String>(ChangeMiddleNameStuExtractGen.P_MIDDLE_NAME_NEW, this);
            return _middleNameNew;
        }

    /**
     * @return Серия.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getCardSeria()
     */
        public PropertyPath<String> cardSeria()
        {
            if(_cardSeria == null )
                _cardSeria = new PropertyPath<String>(ChangeMiddleNameStuExtractGen.P_CARD_SERIA, this);
            return _cardSeria;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getCardNumber()
     */
        public PropertyPath<String> cardNumber()
        {
            if(_cardNumber == null )
                _cardNumber = new PropertyPath<String>(ChangeMiddleNameStuExtractGen.P_CARD_NUMBER, this);
            return _cardNumber;
        }

    /**
     * @return Дата рождения.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getBirthDate()
     */
        public PropertyPath<Date> birthDate()
        {
            if(_birthDate == null )
                _birthDate = new PropertyPath<Date>(ChangeMiddleNameStuExtractGen.P_BIRTH_DATE, this);
            return _birthDate;
        }

    /**
     * @return Место рождения.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getBirthPlace()
     */
        public PropertyPath<String> birthPlace()
        {
            if(_birthPlace == null )
                _birthPlace = new PropertyPath<String>(ChangeMiddleNameStuExtractGen.P_BIRTH_PLACE, this);
            return _birthPlace;
        }

    /**
     * @return Дата выдачи удостоверения.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getIssuanceDate()
     */
        public PropertyPath<Date> issuanceDate()
        {
            if(_issuanceDate == null )
                _issuanceDate = new PropertyPath<Date>(ChangeMiddleNameStuExtractGen.P_ISSUANCE_DATE, this);
            return _issuanceDate;
        }

    /**
     * @return Кем выдано удостоверение.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getIssuancePlace()
     */
        public PropertyPath<String> issuancePlace()
        {
            if(_issuancePlace == null )
                _issuancePlace = new PropertyPath<String>(ChangeMiddleNameStuExtractGen.P_ISSUANCE_PLACE, this);
            return _issuancePlace;
        }

    /**
     * @return Причина изменения.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getReasonStr()
     */
        public PropertyPath<String> reasonStr()
        {
            if(_reasonStr == null )
                _reasonStr = new PropertyPath<String>(ChangeMiddleNameStuExtractGen.P_REASON_STR, this);
            return _reasonStr;
        }

    /**
     * @return Национальные группы.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getNationality()
     */
        public Nationality.Path<Nationality> nationality()
        {
            if(_nationality == null )
                _nationality = new Nationality.Path<Nationality>(L_NATIONALITY, this);
            return _nationality;
        }

    /**
     * @return Пол. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getSex()
     */
        public Sex.Path<Sex> sex()
        {
            if(_sex == null )
                _sex = new Sex.Path<Sex>(L_SEX, this);
            return _sex;
        }

    /**
     * @return Гражданство. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getCitizenship()
     */
        public ICitizenshipGen.Path<ICitizenship> citizenship()
        {
            if(_citizenship == null )
                _citizenship = new ICitizenshipGen.Path<ICitizenship>(L_CITIZENSHIP, this);
            return _citizenship;
        }

    /**
     * @return Тип удостоверения личности. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getCardType()
     */
        public IdentityCardType.Path<IdentityCardType> cardType()
        {
            if(_cardType == null )
                _cardType = new IdentityCardType.Path<IdentityCardType>(L_CARD_TYPE, this);
            return _cardType;
        }

    /**
     * @return Тип последнего активного удостоверения личности. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getLastActiveIdentityCard()
     */
        public IdentityCard.Path<IdentityCard> lastActiveIdentityCard()
        {
            if(_lastActiveIdentityCard == null )
                _lastActiveIdentityCard = new IdentityCard.Path<IdentityCard>(L_LAST_ACTIVE_IDENTITY_CARD, this);
            return _lastActiveIdentityCard;
        }

    /**
     * @return Новое удостоверение личности.
     * @see ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract#getIdentityCardNew()
     */
        public IdentityCard.Path<IdentityCard> identityCardNew()
        {
            if(_identityCardNew == null )
                _identityCardNew = new IdentityCard.Path<IdentityCard>(L_IDENTITY_CARD_NEW, this);
            return _identityCardNew;
        }

        public Class getEntityClass()
        {
            return ChangeMiddleNameStuExtract.class;
        }

        public String getEntityName()
        {
            return "changeMiddleNameStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
