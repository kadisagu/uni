package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «Об утверждении тем дипломных работ и назначении научных руководителей»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SetDiplomaWorkTopicAndScientificAdviserStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract";
    public static final String ENTITY_NAME = "setDiplomaWorkTopicAndScientificAdviserStuListExtract";
    public static final int VERSION_HASH = 1234484144;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_DEVELOP_CONDITION = "developCondition";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_DIPLOMA_WORK_TOPIC_OLD = "diplomaWorkTopicOld";
    public static final String P_DIPLOMA_WORK_TOPIC_NEW = "diplomaWorkTopicNew";
    public static final String P_SCIENTIFIC_ADVISER_NAME_STR_OLD = "scientificAdviserNameStrOld";
    public static final String L_SCIENTIFIC_ADVISER_NAME = "scientificAdviserName";
    public static final String L_SCIENTIFIC_ADVISER_NAME_OLD = "scientificAdviserNameOld";

    private Course _course;     // Курс
    private Group _group;     // Группа
    private DevelopForm _developForm;     // Форма освоения
    private DevelopCondition _developCondition;     // Условие освоения
    private CompensationType _compensationType;     // Вид возмещения затрат
    private String _diplomaWorkTopicOld;     // Старая тема дипломной работы
    private String _diplomaWorkTopicNew;     // Новая тема дипломной работы
    private String _scientificAdviserNameStrOld;     // Прежний научный руководитель (текстом)
    private PpsEntry _scientificAdviserName;     // Новый научный руководитель
    private PpsEntry _scientificAdviserNameOld;     // Прежний научный руководитель (ППС)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения.
     */
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения.
     */
    public void setDevelopCondition(DevelopCondition developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Старая тема дипломной работы.
     */
    @Length(max=1024)
    public String getDiplomaWorkTopicOld()
    {
        return _diplomaWorkTopicOld;
    }

    /**
     * @param diplomaWorkTopicOld Старая тема дипломной работы.
     */
    public void setDiplomaWorkTopicOld(String diplomaWorkTopicOld)
    {
        dirty(_diplomaWorkTopicOld, diplomaWorkTopicOld);
        _diplomaWorkTopicOld = diplomaWorkTopicOld;
    }

    /**
     * @return Новая тема дипломной работы. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1024)
    public String getDiplomaWorkTopicNew()
    {
        return _diplomaWorkTopicNew;
    }

    /**
     * @param diplomaWorkTopicNew Новая тема дипломной работы. Свойство не может быть null.
     */
    public void setDiplomaWorkTopicNew(String diplomaWorkTopicNew)
    {
        dirty(_diplomaWorkTopicNew, diplomaWorkTopicNew);
        _diplomaWorkTopicNew = diplomaWorkTopicNew;
    }

    /**
     * @return Прежний научный руководитель (текстом).
     */
    @Length(max=1024)
    public String getScientificAdviserNameStrOld()
    {
        return _scientificAdviserNameStrOld;
    }

    /**
     * @param scientificAdviserNameStrOld Прежний научный руководитель (текстом).
     */
    public void setScientificAdviserNameStrOld(String scientificAdviserNameStrOld)
    {
        dirty(_scientificAdviserNameStrOld, scientificAdviserNameStrOld);
        _scientificAdviserNameStrOld = scientificAdviserNameStrOld;
    }

    /**
     * @return Новый научный руководитель. Свойство не может быть null.
     */
    @NotNull
    public PpsEntry getScientificAdviserName()
    {
        return _scientificAdviserName;
    }

    /**
     * @param scientificAdviserName Новый научный руководитель. Свойство не может быть null.
     */
    public void setScientificAdviserName(PpsEntry scientificAdviserName)
    {
        dirty(_scientificAdviserName, scientificAdviserName);
        _scientificAdviserName = scientificAdviserName;
    }

    /**
     * @return Прежний научный руководитель (ППС).
     */
    public PpsEntry getScientificAdviserNameOld()
    {
        return _scientificAdviserNameOld;
    }

    /**
     * @param scientificAdviserNameOld Прежний научный руководитель (ППС).
     */
    public void setScientificAdviserNameOld(PpsEntry scientificAdviserNameOld)
    {
        dirty(_scientificAdviserNameOld, scientificAdviserNameOld);
        _scientificAdviserNameOld = scientificAdviserNameOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SetDiplomaWorkTopicAndScientificAdviserStuListExtractGen)
        {
            setCourse(((SetDiplomaWorkTopicAndScientificAdviserStuListExtract)another).getCourse());
            setGroup(((SetDiplomaWorkTopicAndScientificAdviserStuListExtract)another).getGroup());
            setDevelopForm(((SetDiplomaWorkTopicAndScientificAdviserStuListExtract)another).getDevelopForm());
            setDevelopCondition(((SetDiplomaWorkTopicAndScientificAdviserStuListExtract)another).getDevelopCondition());
            setCompensationType(((SetDiplomaWorkTopicAndScientificAdviserStuListExtract)another).getCompensationType());
            setDiplomaWorkTopicOld(((SetDiplomaWorkTopicAndScientificAdviserStuListExtract)another).getDiplomaWorkTopicOld());
            setDiplomaWorkTopicNew(((SetDiplomaWorkTopicAndScientificAdviserStuListExtract)another).getDiplomaWorkTopicNew());
            setScientificAdviserNameStrOld(((SetDiplomaWorkTopicAndScientificAdviserStuListExtract)another).getScientificAdviserNameStrOld());
            setScientificAdviserName(((SetDiplomaWorkTopicAndScientificAdviserStuListExtract)another).getScientificAdviserName());
            setScientificAdviserNameOld(((SetDiplomaWorkTopicAndScientificAdviserStuListExtract)another).getScientificAdviserNameOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SetDiplomaWorkTopicAndScientificAdviserStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SetDiplomaWorkTopicAndScientificAdviserStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new SetDiplomaWorkTopicAndScientificAdviserStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "compensationType":
                    return obj.getCompensationType();
                case "diplomaWorkTopicOld":
                    return obj.getDiplomaWorkTopicOld();
                case "diplomaWorkTopicNew":
                    return obj.getDiplomaWorkTopicNew();
                case "scientificAdviserNameStrOld":
                    return obj.getScientificAdviserNameStrOld();
                case "scientificAdviserName":
                    return obj.getScientificAdviserName();
                case "scientificAdviserNameOld":
                    return obj.getScientificAdviserNameOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((DevelopCondition) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "diplomaWorkTopicOld":
                    obj.setDiplomaWorkTopicOld((String) value);
                    return;
                case "diplomaWorkTopicNew":
                    obj.setDiplomaWorkTopicNew((String) value);
                    return;
                case "scientificAdviserNameStrOld":
                    obj.setScientificAdviserNameStrOld((String) value);
                    return;
                case "scientificAdviserName":
                    obj.setScientificAdviserName((PpsEntry) value);
                    return;
                case "scientificAdviserNameOld":
                    obj.setScientificAdviserNameOld((PpsEntry) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "group":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "compensationType":
                        return true;
                case "diplomaWorkTopicOld":
                        return true;
                case "diplomaWorkTopicNew":
                        return true;
                case "scientificAdviserNameStrOld":
                        return true;
                case "scientificAdviserName":
                        return true;
                case "scientificAdviserNameOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "group":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "compensationType":
                    return true;
                case "diplomaWorkTopicOld":
                    return true;
                case "diplomaWorkTopicNew":
                    return true;
                case "scientificAdviserNameStrOld":
                    return true;
                case "scientificAdviserName":
                    return true;
                case "scientificAdviserNameOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "developForm":
                    return DevelopForm.class;
                case "developCondition":
                    return DevelopCondition.class;
                case "compensationType":
                    return CompensationType.class;
                case "diplomaWorkTopicOld":
                    return String.class;
                case "diplomaWorkTopicNew":
                    return String.class;
                case "scientificAdviserNameStrOld":
                    return String.class;
                case "scientificAdviserName":
                    return PpsEntry.class;
                case "scientificAdviserNameOld":
                    return PpsEntry.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SetDiplomaWorkTopicAndScientificAdviserStuListExtract> _dslPath = new Path<SetDiplomaWorkTopicAndScientificAdviserStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SetDiplomaWorkTopicAndScientificAdviserStuListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getDevelopCondition()
     */
    public static DevelopCondition.Path<DevelopCondition> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Старая тема дипломной работы.
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getDiplomaWorkTopicOld()
     */
    public static PropertyPath<String> diplomaWorkTopicOld()
    {
        return _dslPath.diplomaWorkTopicOld();
    }

    /**
     * @return Новая тема дипломной работы. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getDiplomaWorkTopicNew()
     */
    public static PropertyPath<String> diplomaWorkTopicNew()
    {
        return _dslPath.diplomaWorkTopicNew();
    }

    /**
     * @return Прежний научный руководитель (текстом).
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getScientificAdviserNameStrOld()
     */
    public static PropertyPath<String> scientificAdviserNameStrOld()
    {
        return _dslPath.scientificAdviserNameStrOld();
    }

    /**
     * @return Новый научный руководитель. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getScientificAdviserName()
     */
    public static PpsEntry.Path<PpsEntry> scientificAdviserName()
    {
        return _dslPath.scientificAdviserName();
    }

    /**
     * @return Прежний научный руководитель (ППС).
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getScientificAdviserNameOld()
     */
    public static PpsEntry.Path<PpsEntry> scientificAdviserNameOld()
    {
        return _dslPath.scientificAdviserNameOld();
    }

    public static class Path<E extends SetDiplomaWorkTopicAndScientificAdviserStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private DevelopForm.Path<DevelopForm> _developForm;
        private DevelopCondition.Path<DevelopCondition> _developCondition;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<String> _diplomaWorkTopicOld;
        private PropertyPath<String> _diplomaWorkTopicNew;
        private PropertyPath<String> _scientificAdviserNameStrOld;
        private PpsEntry.Path<PpsEntry> _scientificAdviserName;
        private PpsEntry.Path<PpsEntry> _scientificAdviserNameOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getDevelopCondition()
     */
        public DevelopCondition.Path<DevelopCondition> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new DevelopCondition.Path<DevelopCondition>(L_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Старая тема дипломной работы.
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getDiplomaWorkTopicOld()
     */
        public PropertyPath<String> diplomaWorkTopicOld()
        {
            if(_diplomaWorkTopicOld == null )
                _diplomaWorkTopicOld = new PropertyPath<String>(SetDiplomaWorkTopicAndScientificAdviserStuListExtractGen.P_DIPLOMA_WORK_TOPIC_OLD, this);
            return _diplomaWorkTopicOld;
        }

    /**
     * @return Новая тема дипломной работы. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getDiplomaWorkTopicNew()
     */
        public PropertyPath<String> diplomaWorkTopicNew()
        {
            if(_diplomaWorkTopicNew == null )
                _diplomaWorkTopicNew = new PropertyPath<String>(SetDiplomaWorkTopicAndScientificAdviserStuListExtractGen.P_DIPLOMA_WORK_TOPIC_NEW, this);
            return _diplomaWorkTopicNew;
        }

    /**
     * @return Прежний научный руководитель (текстом).
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getScientificAdviserNameStrOld()
     */
        public PropertyPath<String> scientificAdviserNameStrOld()
        {
            if(_scientificAdviserNameStrOld == null )
                _scientificAdviserNameStrOld = new PropertyPath<String>(SetDiplomaWorkTopicAndScientificAdviserStuListExtractGen.P_SCIENTIFIC_ADVISER_NAME_STR_OLD, this);
            return _scientificAdviserNameStrOld;
        }

    /**
     * @return Новый научный руководитель. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getScientificAdviserName()
     */
        public PpsEntry.Path<PpsEntry> scientificAdviserName()
        {
            if(_scientificAdviserName == null )
                _scientificAdviserName = new PpsEntry.Path<PpsEntry>(L_SCIENTIFIC_ADVISER_NAME, this);
            return _scientificAdviserName;
        }

    /**
     * @return Прежний научный руководитель (ППС).
     * @see ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract#getScientificAdviserNameOld()
     */
        public PpsEntry.Path<PpsEntry> scientificAdviserNameOld()
        {
            if(_scientificAdviserNameOld == null )
                _scientificAdviserNameOld = new PpsEntry.Path<PpsEntry>(L_SCIENTIFIC_ADVISER_NAME_OLD, this);
            return _scientificAdviserNameOld;
        }

        public Class getEntityClass()
        {
            return SetDiplomaWorkTopicAndScientificAdviserStuListExtract.class;
        }

        public String getEntityName()
        {
            return "setDiplomaWorkTopicAndScientificAdviserStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
