/**
 *$Id$
 */
package ru.tandemservice.movestudent.base.bo.MovestudentSystemAction;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.movestudent.base.bo.MovestudentSystemAction.logic.IMovestudentSystemActionDao;
import ru.tandemservice.movestudent.base.bo.MovestudentSystemAction.logic.MovestudentSystemActionDao;

/**
 * @author Alexander Shaburov
 * @since 13.02.13
 */
@Configuration
public class MovestudentSystemActionManager extends BusinessObjectManager
{
    public static MovestudentSystemActionManager instance()
    {
        return instance(MovestudentSystemActionManager.class);
    }

    @Bean
    public IMovestudentSystemActionDao dao()
    {
        return new MovestudentSystemActionDao();
    }
}
