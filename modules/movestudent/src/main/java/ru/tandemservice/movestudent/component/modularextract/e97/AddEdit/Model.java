/* $Id: Model.java.vm 11769 2010-03-05 07:11:49Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e97.AddEdit;

import org.apache.tapestry.form.validator.BaseValidator;
import org.apache.tapestry.form.validator.Required;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.AdoptRussianCitizenshipStuExtract;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 18.01.2013
 */
public class Model extends CommonModularStudentExtractAddEditModel<AdoptRussianCitizenshipStuExtract>
{
    private ISelectModel _citizenshipModel;
	private List<IdentityCardType> _identityCardTypeList;
	private ISelectModel _nationalityModel;
	private List<Sex> _sexList;

    public ISelectModel getCitizenshipModel()
    {
        return _citizenshipModel;
    }

    public void setCitizenshipModel(ISelectModel citizenshipModel)
    {
        _citizenshipModel = citizenshipModel;
    }

    public List<IdentityCardType> getIdentityCardTypeList()
	{
		return _identityCardTypeList;
	}

	public void setIdentityCardTypeList(final List<IdentityCardType> identityCardTypeList)
	{
		_identityCardTypeList = identityCardTypeList;
	}

    public ISelectModel getNationalityModel()
    {
        return _nationalityModel;
    }

    public void setNationalityModel(ISelectModel nationalityModel)
    {
        _nationalityModel = nationalityModel;
    }

    public List<Sex> getSexList()
	{
		return _sexList;
	}

	public void setSexList(final List<Sex> sexList)
	{
		_sexList = sexList;
	}

    public List<BaseValidator> getRequiredMiddleNameValidators ()
    {
        return getExtract().getCardType().getMiddleNameValidators();
    }


}
