/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.orgunit.OrgUnitMoveStudent;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.commons.ExtractListModel;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.List;

/**
 * @author vip_delete
 * @since 18.10.2008
 */
@State({
        @Bind(key = "orgUnitId", binding = "orgUnitId"),
        @Bind(key = "orgUnitMoveStudentSelectedTab", binding = "orgUnitMoveStudentSelectedTab")
})
public class Model
{
    private long _orgUnitId;
    private String _orgUnitMoveStudentSelectedTab;
    private OrgUnit _orgUnit;
    private ISelectModel _educationYearList;
    private ISelectModel _studentList;
    private ISelectModel employeePostModel;
    private List<OrderStates> _ordersStateList;
    private List<OrderStates> _otherOrdersStateList;
    private IMultiSelectModel _extractTypesListModel;

    // individual orders tab
    private ExtractListModel _individualOrdersModel = new ExtractListModel();

    // projects tab
    private ExtractListModel _projectsModel = new ExtractListModel();

    // modular order tab
    private CommonPostfixPermissionModel _modularOrdersSecModel;
    private DynamicListDataSource _modularOrdersdataSource;
    private IDataSettings _modularOrdersSettings;

    // modular extracts tab
    private ExtractListModel _extractsModel = new ExtractListModel();

    // list orders tab
    private CommonPostfixPermissionModel _listOrdersSecModel;
    private IMultiSelectModel _listOrderTypesListModel;
    private IMultiSelectModel _listOrderReasonModel;
    private DynamicListDataSource _listOrdersdataSource;
    private IDataSettings _listOrdersSettings;

    // list extracts tab
    private ExtractListModel _listextractsModel = new ExtractListModel();

    // other orders tab
    private ExtractListModel _otherOrdersModel = new ExtractListModel();

    // Getters & Setters

    public long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public String getOrgUnitMoveStudentSelectedTab()
    {
        return _orgUnitMoveStudentSelectedTab;
    }

    public void setOrgUnitMoveStudentSelectedTab(String orgUnitMoveStudentSelectedTab)
    {
        _orgUnitMoveStudentSelectedTab = orgUnitMoveStudentSelectedTab;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public ISelectModel getEducationYearList()
    {
        return _educationYearList;
    }

    public void setEducationYearList(ISelectModel educationYearList)
    {
        _educationYearList = educationYearList;
    }

    public List<OrderStates> getOrdersStateList()
    {
        return _ordersStateList;
    }

    public void setOrdersStateList(List<OrderStates> ordersStateList)
    {
        _ordersStateList = ordersStateList;
    }

    public List<OrderStates> getOtherOrdersStateList()
    {
        return _otherOrdersStateList;
    }

    public void setOtherOrdersStateList(List<OrderStates> otherOrdersStateList)
    {
        _otherOrdersStateList = otherOrdersStateList;
    }

    public ExtractListModel getIndividualOrdersModel()
    {
        return _individualOrdersModel;
    }

    public void setIndividualOrdersModel(ExtractListModel individualOrdersModel)
    {
        _individualOrdersModel = individualOrdersModel;
    }

    public ExtractListModel getProjectsModel()
    {
        return _projectsModel;
    }

    public void setProjectsModel(ExtractListModel projectsModel)
    {
        _projectsModel = projectsModel;
    }

    public CommonPostfixPermissionModel getModularOrdersSecModel()
    {
        return _modularOrdersSecModel;
    }

    public void setModularOrdersSecModel(CommonPostfixPermissionModel modularOrdersSecModel)
    {
        _modularOrdersSecModel = modularOrdersSecModel;
    }

    @SuppressWarnings("unchecked")
    public <T extends IEntity> DynamicListDataSource<T> getModularOrdersdataSource()
    {
        return _modularOrdersdataSource;
    }

    public void setModularOrdersdataSource(DynamicListDataSource modularOrdersdataSource)
    {
        _modularOrdersdataSource = modularOrdersdataSource;
    }

    public IDataSettings getModularOrdersSettings()
    {
        return _modularOrdersSettings;
    }

    public void setModularOrdersSettings(IDataSettings modularOrdersSettings)
    {
        _modularOrdersSettings = modularOrdersSettings;
    }

    public ExtractListModel getExtractsModel()
    {
        return _extractsModel;
    }

    public void setExtractsModel(ExtractListModel extractsModel)
    {
        _extractsModel = extractsModel;
    }

    public CommonPostfixPermissionModel getListOrdersSecModel()
    {
        return _listOrdersSecModel;
    }

    public void setListOrdersSecModel(CommonPostfixPermissionModel listOrdersSecModel)
    {
        _listOrdersSecModel = listOrdersSecModel;
    }

    public IMultiSelectModel getListOrderTypesListModel()
    {
        return _listOrderTypesListModel;
    }

    public void setListOrderTypesListModel(IMultiSelectModel listOrderTypesListModel)
    {
        _listOrderTypesListModel = listOrderTypesListModel;
    }

    public IMultiSelectModel getListOrderReasonModel()
    {
        return _listOrderReasonModel;
    }

    public void setListOrderReasonModel(IMultiSelectModel listOrderReasonModel)
    {
        _listOrderReasonModel = listOrderReasonModel;
    }

    @SuppressWarnings("unchecked")
    public <T extends IEntity> DynamicListDataSource<T> getListOrdersdataSource()
    {
        return _listOrdersdataSource;
    }

    public void setListOrdersdataSource(DynamicListDataSource listOrdersdataSource)
    {
        _listOrdersdataSource = listOrdersdataSource;
    }

    public IDataSettings getListOrdersSettings()
    {
        return _listOrdersSettings;
    }

    public void setListOrdersSettings(IDataSettings listOrdersSettings)
    {
        _listOrdersSettings = listOrdersSettings;
    }

    public ISelectModel getStudentList()
    {
        return _studentList;
    }

    public void setStudentList(ISelectModel studentList)
    {
        _studentList = studentList;
    }

    public ExtractListModel getListextractsModel()
    {
        return _listextractsModel;
    }

    public void setListextractsModel(ExtractListModel listextractsModel)
    {
        _listextractsModel = listextractsModel;
    }

    public ExtractListModel getOtherOrdersModel()
    {
        return _otherOrdersModel;
    }

    public void setOtherOrdersModel(ExtractListModel otherOrdersModel)
    {
        _otherOrdersModel = otherOrdersModel;
    }

    public ISelectModel getEmployeePostModel()
    {
        return employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        this.employeePostModel = employeePostModel;
    }
    public IMultiSelectModel getExtractTypesListModel()
    {
        return _extractTypesListModel;
    }

    public void setExtractTypesListModel(IMultiSelectModel extractTypesListModel)
    {
        this._extractTypesListModel = extractTypesListModel;
    }
}