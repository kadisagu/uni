/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.settings.BasicsCommentSettings;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author vip_delete
 * @since 17.10.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<StudentOrderBasics> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Название", StudentOrderBasics.P_TITLE).setClickable(false));
        dataSource.addColumn(new ToggleColumn("Требует примечания", StudentOrderBasics.commentable().s()).setListener("onClickCommentable"));
        dataSource.addColumn(new ToggleColumn("Примечание обязательно", StudentOrderBasics.required().s()).setListener("onClickRequired").setDisabledProperty(StudentOrderBasics.P_REQUIRED_DISABLED));
        model.setDataSource(dataSource);
    }

    public void onClickCommentable(IBusinessComponent component)
    {
        getDao().updateCommentable((Long) component.getListenerParameter());
    }

    public void onClickRequired(IBusinessComponent component)
    {
        getDao().updateRequired((Long) component.getListenerParameter());
    }
}