/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e27;

import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.e27.utils.AdmitToPassDiplomaWorkExtParagraphPartWrapper;
import ru.tandemservice.movestudent.component.listextract.e27.utils.AdmitToPassDiplomaWorkExtParagraphWrapper;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AdmitToPassDiplomaWorkExtStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 13.09.2012
 */
public class AdmitToPassDiplomaWorkExtStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);
        List<AdmitToPassDiplomaWorkExtStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        EducationOrgUnit eo = extracts.get(0).getEntity().getEducationOrgUnit();
        EducationLevels educationLevels = eo.getEducationLevelHighSchool().getEducationLevel();
        CommonExtractPrint.initProgramSubjectLabels(injectModifier, educationLevels.getEduProgramSubject(), educationLevels.getEduProgramSpecialization(), "");

        String workType;
        String qCode = extracts.get(0).getEntity().getGroup().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getSafeQCode();

        switch (qCode)
        {
            case QualificationsCodes.BAKALAVR:
                workType = "квалификационной работы (бакалаврской работы)";
                break;
            case QualificationsCodes.MAGISTR:
                workType = "квалификационной работы (магистерской диссертации)";
                break;
            default:
                workType = "квалификационной работы";
                break;
        }

        injectModifier.put("workType", workType);
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, prepareParagraphsStructure(extracts));

        RtfUtil.optimizeFontAndColorTable(document);
        return document;
    }

    protected List<AdmitToPassDiplomaWorkExtParagraphWrapper> prepareParagraphsStructure(List<AdmitToPassDiplomaWorkExtStuListExtract> extracts)
    {
        List<AdmitToPassDiplomaWorkExtParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        int ind;

        for (AdmitToPassDiplomaWorkExtStuListExtract extract: extracts)
        {
            Student student = extract.getEntity();
            Group group = student.getGroup();
            EducationOrgUnit educationOrgUnit = group.getEducationOrgUnit();

            AdmitToPassDiplomaWorkExtParagraphWrapper paragraphWrapper = new AdmitToPassDiplomaWorkExtParagraphWrapper(group.getCourse(), student.getCompensationType(),educationOrgUnit, CommonListOrderPrint.getEducationBaseText(group), extract);
            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else paragraphWrapper = paragraphWrapperList.get(ind);

            AdmitToPassDiplomaWorkExtParagraphPartWrapper paragraphPartWrapper = new AdmitToPassDiplomaWorkExtParagraphPartWrapper(group, extract);
            ind = paragraphWrapper.getParagraphPartWrapperList().indexOf(paragraphPartWrapper);
            if (ind == -1)
                paragraphWrapper.getParagraphPartWrapperList().add(paragraphPartWrapper);
            else paragraphPartWrapper = paragraphWrapper.getParagraphPartWrapperList().get(ind);

            Person person = student.getPerson();

            if (!paragraphPartWrapper.getPersonList().contains(person))
                paragraphPartWrapper.getPersonList().add(person);
        }

        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<AdmitToPassDiplomaWorkExtParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            int parNumber = 0;
            for (AdmitToPassDiplomaWorkExtParagraphWrapper paragraphWrapper: paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.ADMIT_TO_PASS_DIPLOMA_WORK_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphWrapper.getFirstExtract();
                RtfInjectModifier paragraphInjectModifier = new RtfInjectModifier();

                String courseStr = paragraphWrapper.getCourse().getTitle();

                paragraphInjectModifier.put("course", courseStr);

                String code = paragraphWrapper.getEducationOrgUnit().getDevelopForm().getCode();
                switch (code)
                {
                    case DevelopFormCodes.FULL_TIME_FORM:
                        paragraphInjectModifier.put("developForm_DF", "очной");
                        break;
                    case DevelopFormCodes.CORESP_FORM:
                        paragraphInjectModifier.put("developForm_DF", "заочной");
                        break;
                    case DevelopFormCodes.PART_TIME_FORM:
                        paragraphInjectModifier.put("developForm_DF", "очно-заочной");
                        break;
                    case DevelopFormCodes.EXTERNAL_FORM:
                        paragraphInjectModifier.put("developForm_DF", "экстернату");
                        break;
                    case DevelopFormCodes.APPLICANT_FORM:
                        paragraphInjectModifier.put("developForm_DF", "самостоятельному обучению и итоговой аттестации");
                        break;
                }

                CommonListExtractPrint.injectCompensationType(paragraphInjectModifier, paragraphWrapper.getCompensationType(), "", false);
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, paragraphWrapper.getEducationOrgUnit(), "formativeOrgUnitStr", "");

                EducationLevelsHighSchool educationLevelsHighSchool = paragraphWrapper.getEducationOrgUnit().getEducationLevelHighSchool();

                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, educationLevelsHighSchool.getEducationLevel());

                String qCode = educationLevelsHighSchool.getEducationLevel().getSafeQCode();
                String workType;

                switch (qCode)
                {
                    case QualificationsCodes.BAKALAVR:
                        workType = "бакалаврской работы";
                        break;
                    case QualificationsCodes.MAGISTR:
                        workType = "квалификационной работы (магистерской диссертации)";
                        break;
                    default:
                        workType = "квалификационной работы";
                        break;
                }

                paragraphInjectModifier.put("workType", workType);
                paragraphInjectModifier.put("parNumber", String.valueOf(++parNumber));
                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");
                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, paragraphWrapper.getEducationOrgUnit(), paragraphWrapper.getEduBaseText(), "fefuShortFastExtendedOptionalText");
                CommonExtractPrint.initProgramSubjectLabels(paragraphInjectModifier, educationLevelsHighSchool.getEducationLevel().getEduProgramSubject(), educationLevelsHighSchool.getEducationLevel().getEduProgramSpecialization(), "");
                customModifier.listParagraphModifier(paragraphInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphInjectModifier.modify(paragraph);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getParagraphPartWrapperList(), paragraphWrapper.getEducationOrgUnit().getDevelopForm());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraph, List<AdmitToPassDiplomaWorkExtParagraphPartWrapper> paragraphPartWrappers, DevelopForm developForm)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (AdmitToPassDiplomaWorkExtParagraphPartWrapper paragraphPartWrapper: paragraphPartWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.ADMIT_TO_PASS_DIPLOMA_WORK_LIST_EXTRACT), 3);
                RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);
                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphPartWrapper.getFirstExtract();
                RtfInjectModifier paragraphPartInjectModifier = new RtfInjectModifier();

                paragraphPartInjectModifier.put("group", paragraphPartWrapper.getGroup().getTitle());
                CommonListOrderPrint.injectFefuGroupPar(paragraphPartInjectModifier, paragraphPartWrapper.getGroup(), developForm, false, true);

                List<Person> personList = paragraphPartWrapper.getPersonList();
                personList.sort(Person.FULL_FIO_AND_ID_COMPARATOR);
                RtfString rtfString = new RtfString();

                int i = 0;

                for (; i < personList.size() - 1; i++)
                {
                    rtfString.append(String.valueOf(i + 1)).append(". ").append(personList.get(i).getFullFio()).par();
                }

                rtfString.append(String.valueOf(i + 1)).append(". ").append(personList.get(i).getFullFio());

                paragraphPartInjectModifier.put("STUDENT_LIST", rtfString);
                customModifier.listParagraphModifier(paragraphPartInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphPartInjectModifier.modify(paragraphPart);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraphPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

}