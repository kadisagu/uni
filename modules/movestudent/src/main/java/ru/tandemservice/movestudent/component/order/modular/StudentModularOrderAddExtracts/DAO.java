/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.modular.StudentModularOrderAddExtracts;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.component.commons.IMoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.MoveStudentMQBuilder;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.movestudent.entity.StudentModularParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.EducationLevelsHighSchoolSelectModel;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

/**
 * @author vip_delete
 * @since 27.10.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry(IMoveStudentMQBuilder.EXTRACT_ALIAS);

    static
    {
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_FULL_FIO, new OrderDescription(IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS, IdentityCard.P_LAST_NAME), new OrderDescription(IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS, IdentityCard.P_FIRST_NAME), new OrderDescription(IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS, IdentityCard.P_MIDDLE_NAME));
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_EXTRACT_FORMATIVE_ORG_UNIT_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_TITLE}));
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_EXTRACT_TERRITORIAL_ORG_UNIT_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_TITLE}));
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_EXTRACT_PRODUCING_ORG_UNIT_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.L_ORG_UNIT, OrgUnit.P_TITLE}));
    }

    @Override
    public void prepare(Model model)
    {
        model.setOrder(getNotNull(StudentModularOrder.class, model.getOrder().getId()));
        boolean admin = UserContext.getInstance().getPrincipalContext().isAdmin();

        // init filters
        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setProducingOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING));
        model.setEducationLevelHighSchool(new EducationLevelsHighSchoolSelectModel(model));
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setExtractTypesListModel(new FullCheckSelectModel(admin ? StudentExtractType.P_TITLE_WITH_CODE : StudentExtractType.P_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                return MoveStudentDaoFacade.getMoveStudentDao().getModularStudentExtractTypeList(filter, StudentExtractTypeCodes.MODULAR_ORDER);
            }
        });
        model.setExecutorModel(new OrderExecutorSelectModel());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        // init datasource
        IMoveStudentMQBuilder builder = MoveStudentMQBuilder.createExtractMQBuilder(ModularStudentExtract.class, model.getSettings());

        // apply user defined restrictions
        builder.getMQBuilder().add(MQExpression.eq(IMoveStudentMQBuilder.STATE_ALIAS, ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED));

        if (model.getOrder().getOrgUnit() != null)
        {
            // условие для сборных приказов созданных на подразделении
            AbstractExpression eqFormative = MQExpression.eq(IMoveStudentMQBuilder.EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getOrder().getOrgUnit());
            AbstractExpression eqTerritorial = MQExpression.eq(IMoveStudentMQBuilder.EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getOrder().getOrgUnit());
            AbstractExpression eqOrgUnit = MQExpression.eq(IMoveStudentMQBuilder.EXTRACT_ALIAS, AbstractStudentExtract.L_ORG_UNIT, model.getOrder().getOrgUnit());
            AbstractExpression nullOrgUnit = MQExpression.isNull(IMoveStudentMQBuilder.EXTRACT_ALIAS, AbstractStudentExtract.L_ORG_UNIT);
            builder.getMQBuilder().add(MQExpression.or(eqOrgUnit, MQExpression.and(MQExpression.or(eqFormative, eqTerritorial), nullOrgUnit)));
        }

        // apply filters
        builder.applyExtractCreateDate();
        builder.applyExtractLastName();
        builder.applyExtractFormativeOrgUnit();
        builder.applyExtractTerritorialOrgUnit();
        builder.applyExtractProducingOrgUnit();
        builder.applyExtractEducationLevelHighSchool();
        builder.applyExtractCourse();
        builder.applyExtractDevelopForm();
        builder.applyExtractCompensationType();
        builder.applyExtractType();
        builder.applyExtractExecutor();

        // create page
        builder.createPage(getSession(), _orderSettings, model.getDataSource());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        getSession().refresh(model.getOrder());
        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getOrder().getState().getCode()))
            throw new ApplicationException("Нельзя изменять приказ. Изменение приказа возможно только в состоянии «Формируется».");

        int count = model.getOrder().getParagraphCount() + 1;
        for (IEntity entity : ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).getSelectedObjects())
        {
            IAbstractExtract extract = (IAbstractExtract) entity;
            if (extract.getParagraph() != null)
                throw new ApplicationException(extract.getTitle() + " уже добавлена в приказ №" + extract.getParagraph().getOrder().getNumber() + ".");

            if (!UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED.equals(extract.getState().getCode()))
                throw new ApplicationException(extract.getTitle() + " не согласован.");

            if (extract.isCommitted())
                throw new ApplicationException("Выписка " + extract.getTitle() + " уже проведена.");

            //create paragraph
            StudentModularParagraph paragraph = new StudentModularParagraph();
            paragraph.setNumber(count++);
            paragraph.setOrder(model.getOrder());
            save(paragraph);

            //update extract
            extract.setNumber(1);
            extract.setParagraph(paragraph);
            extract.setState(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
            update(extract);
        }
    }
}
