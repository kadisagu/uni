/* $Id:$ */
package ru.tandemservice.movestudent.component.listextract.e39.utils;

import org.tandemframework.shared.person.base.entity.Person;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 1/21/13
 * Time: 4:31 PM
 */
public class AcadGrantBonusAssignExtractWrapper implements Comparable<AcadGrantBonusAssignExtractWrapper>
{
    private final Person _person;
    private final Double _grantBonusSize;

    public AcadGrantBonusAssignExtractWrapper(Person person, Double grantBonusSize)
    {
        _person = person;
        _grantBonusSize = grantBonusSize;
    }

    public Person getPerson()
    {
        return _person;
    }

    public Double getGrantBonusSize()
    {
        return _grantBonusSize;
    }

    @Override
    public int compareTo(AcadGrantBonusAssignExtractWrapper o)
    {
        return Person.FULL_FIO_AND_ID_COMPARATOR.compare(_person, o.getPerson());
    }
}
