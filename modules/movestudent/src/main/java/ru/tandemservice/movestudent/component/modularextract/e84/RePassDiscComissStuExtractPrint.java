/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e84;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.RePassDiscComissStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToComissionMember;

import java.util.Arrays;
import java.util.List;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 31.08.2012
 */
public class RePassDiscComissStuExtractPrint implements IPrintFormCreator<RePassDiscComissStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, RePassDiscComissStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        List<String> controlActionsList = Arrays.asList((ApplicationRuntime.getProperty("commonextract.controlActionsList")).split(";"));
        List<String> controlActionsAccusativeList = Arrays.asList((ApplicationRuntime.getProperty("commonextract.controlActionsList_A")).split(";"));

        String controlActionStr = extract.getActionType();
        if(controlActionsList.contains(controlActionStr))
        {
            int idx = controlActionsList.indexOf(controlActionStr);
            if(idx > 0 && controlActionsAccusativeList.size() > idx)
                controlActionStr = controlActionsAccusativeList.get(idx);
        }

        modifier.put("actionType", extract.getActionType());
        modifier.put("actionType_A", controlActionStr);
        modifier.put("discipline", extract.getDiscipline());
        modifier.put("actionPlace", extract.getActionPlace());
        modifier.put("actionDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getActionDateTime()));
        modifier.put("actionTime", DateFormatter.DATE_FORMATTER_TIME.format(extract.getActionDateTime()));

        StringBuilder commissionListStr = new StringBuilder();
        List<StuExtractToComissionMember> disciplineList = DataAccessServices.dao().getList(StuExtractToComissionMember.class, StuExtractToComissionMember.extract().s(), extract, StuExtractToComissionMember.priority().s());

        for (StuExtractToComissionMember member : disciplineList)
        {
            commissionListStr.append(commissionListStr.length() > 0 ? ", " : "");
            commissionListStr.append(member.getFio()).append(", ");
            commissionListStr.append(member.getPost());
        }
        modifier.put("comissionStr", commissionListStr.toString());

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}