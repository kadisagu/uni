/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractAddEdit.ModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 06.11.2008
 */
public abstract class CommonModularStudentExtractAddEditDAO<T extends ModularStudentExtract, Model extends CommonModularStudentExtractAddEditModel<T>> extends ModularStudentExtractAddEditDAO<T, Model>
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setIndividualOrder(MoveStudentDaoFacade.getMoveStudentDao().isModularOrderIndividual(model.getExtractType()));

        model.setReasonList(getFirstList(StudentReasonToTypeRel.class, model.getExtractType()));
        model.setBasicListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getExtract() == null || model.getExtract().getReason() == null)
                    return ListResult.getEmpty();

                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(StudentOrderBasics.class, "b");
                dql.column(property("b"));
                dql.where(exists(StudentReasonToBasicRel.class,
                                 StudentReasonToBasicRel.first().s(), model.getExtract().getReason(),
                                 StudentReasonToBasicRel.second().s(), property("b")));
                dql.order(property("b", StudentOrderBasics.title()));

                FilterUtils.applySimpleLikeFilter(dql, "b", StudentOrderBasics.title(), filter);

                return new DQLListResultBuilder(dql).findOptions();
            }
        });

        model.setEmployeePostModel(new OrderExecutorSelectModel());
        if (model.isAddForm())
        {
            IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
            if (principalContext instanceof EmployeePost)
                model.setEmployeePost((EmployeePost) principalContext);
        }

        if (model.isEditForm())
        {
            model.getCurrentBasicMap().clear();
            final List<StudentOrderBasics> basicsList = new ArrayList<>();
            final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(StuExtractToBasicRelation.class, "r");
            dql.column(property("r", StuExtractToBasicRelation.basic()));
            dql.column(property("r", StuExtractToBasicRelation.comment()));
            dql.where(eq(property("r", StuExtractToBasicRelation.extract()), value(model.getExtract())));
            dql.order(property("r", StuExtractToBasicRelation.basic().title()));
            this.<Object[]>getList(dql).forEach(rel -> {
                final StudentOrderBasics basic = (StudentOrderBasics) rel[0];
                final String comment = (String) rel[1];
                basicsList.add(basic);
                model.getCurrentBasicMap().put(basic.getId(), comment);
            });
            model.setSelectedBasicList(basicsList);
            model.setEmployeePost(model.getExtract().getExecutor());

            if (null != model.getExtract().getParagraph())
            {
                model.setOrder(this.<StudentModularOrder>getNotNull(model.getExtract().getParagraph().getOrder().getId()));
            }
        }

        if (model.getExtract() instanceof CommonStuExtract) {
            final CommonStuExtract extract = (CommonStuExtract) model.getExtract();
            if (model.isEditForm()) {
                model.setChangeBookNumber(extract.getBookNumberNew() != null);
                model.setChangeTargetAdmission(extract.getTargetAdmissionNew() != null);
            }
        }
    }

    @Override
    public void update(Model model)
    {
        super.update(model);
        model.getExtract().setExecutor(model.getEmployeePost());
        if (model.getExtract() instanceof CommonStuExtract) {
            final CommonStuExtract extract = (CommonStuExtract) model.getExtract();
            if (!model.isChangeBookNumber()) {
                extract.setBookNumberNew(null);
                extract.setBookNumberOld(null);
            } else {
                extract.setBookNumberOld(extract.getEntity().getBookNumber());
            }

            if (!model.isChangeTargetAdmission()) {
                extract.setTargetAdmissionNew(null);
                extract.setTargetAdmissionOld(null);
            } else {
                extract.setTargetAdmissionOld(extract.getEntity().isTargetAdmission());
            }
        }

        updateBasicList(model);

        if (model.isIndividualOrder())
        {
            StudentModularOrder order = model.getOrder();
            T extract = model.getExtract();

            order.setCreateDate(extract.getCreateDate());
            if (extract.getEntity().getEducationOrgUnit().getTerritorialOrgUnit() instanceof TopOrgUnit)
                order.setOrgUnit(extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit());
            else
                order.setOrgUnit(extract.getEntity().getEducationOrgUnit().getTerritorialOrgUnit());
            MoveStudentDaoFacade.getMoveStudentDao().checkOrderNumber(order);

            // save order
            saveOrUpdate(order);

            // save executor
            String executor = OrderExecutorSelectModel.getExecutor(model.getEmployeePost());
            if (executor != null) model.getOrder().setExecutor(executor);

            // save paragraph
            AbstractStudentParagraph paragraph = extract.getParagraph();
            if (null == paragraph)
                paragraph = new StudentModularParagraph();

            paragraph.setOrder(order);
            paragraph.setNumber(1);
            saveOrUpdate(paragraph);

            // update extract
            extract.setNumber(1);
            extract.setParagraph(paragraph);
            extract.setState(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
            update(extract);
        }
    }

    private void updateBasicList(Model model)
    {
        if (model.isEditForm())
        {
            for (StuExtractToBasicRelation relation : getList(StuExtractToBasicRelation.class, StuExtractToBasicRelation.L_EXTRACT, model.getExtract()))
                delete(relation);
            getSession().flush();
        }

        for (StudentOrderBasics basic : model.getSelectedBasicList())
        {
            StuExtractToBasicRelation relation = new StuExtractToBasicRelation();
            relation.setBasic(basic);
            relation.setExtract(model.getExtract());
            relation.setComment(model.getCurrentBasicMap().get(basic.getId()));
            save(relation);
        }
    }
}