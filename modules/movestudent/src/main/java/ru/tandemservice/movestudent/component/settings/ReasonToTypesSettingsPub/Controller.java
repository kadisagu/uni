/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.settings.ReasonToTypesSettingsPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.entity.StudentReasonToTypeRel;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationPub.AbstractRelationPubController;

/**
 * @author vip_delete
 * @since 16.10.2008
 */
public class Controller extends AbstractRelationPubController<IDAO, Model>
{
    @Override
    protected String getRelationAddingComponentName()
    {
        return IMoveStudentComponents.REASON_TO_TYPES_SETTINGS_ADD;
    }

    @Override
    protected String getDeleteRowPermissionKey()
    {
        return null;
    }

    @Override
    protected void prepareListDataSource(IBusinessComponent component)
    {
        super.prepareListDataSource(component);
        if (UserContext.getInstance().getPrincipalContext().isAdmin())
        {
            DynamicListDataSource dataSource = getModel(component).getDataSource();
            AbstractColumn titleColumn = dataSource.getColumn(0);
            titleColumn.setKey(StudentReasonToTypeRel.second().titleWithCode().s());
        }
    }
}
