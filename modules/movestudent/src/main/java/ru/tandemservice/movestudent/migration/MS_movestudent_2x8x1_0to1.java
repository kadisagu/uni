package ru.tandemservice.movestudent.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudent_2x8x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {

        ////////////////////////////////////////////////////////////////////////////////
        // сущность codesMSRForFormativeOrgUnit

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("codesmsrforformativeorgunit_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_codesmsrforformativeorgunit"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("orgunit_id", DBType.LONG).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("codesMSRForFormativeOrgUnit");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность codesMSRForStudentOrders

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("codesmsrforstudentorders_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_codesmsrforstudentorders"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("extracttype_id", DBType.LONG).setNullable(false),
                                      new DBColumn("code_p", DBType.INTEGER).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("codesMSRForStudentOrders");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность codesMSRForStudentStatus

        // создана новая сущность
         {
            // создать таблицу
            DBTable dbt = new DBTable("codesmsrforstudentstatus_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_codesmsrforstudentstatus"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("studentstatus_id", DBType.LONG).setNullable(false),
                                      new DBColumn("code_p", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("codesMSRForStudentStatus");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность counterMSRForExportFile

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("countermsrforexportfile_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_countermsrforexportfile"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("currentdate_p", DBType.DATE).setNullable(false),
                                      new DBColumn("counter_p", DBType.INTEGER).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("counterMSRForExportFile");

        }


		////////////////////////////////////////////////////////////////////////////////
		// сущность exportMSRSettings

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("exportmsrsettings_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_exportmsrsettings"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("codehighschool_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("codesender_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("fileversion_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("userscriptexportstudent_p", DBType.TEXT)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("exportMSRSettings");

		}


    }
}
