package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.TransferCourseStuExtractGen;

import java.util.Date;

/**
 * О переводе с курса на следующий курс
 */
public class TransferCourseStuExtract extends TransferCourseStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}