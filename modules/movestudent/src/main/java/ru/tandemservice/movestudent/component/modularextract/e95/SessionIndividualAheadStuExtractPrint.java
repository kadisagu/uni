/* $Id: extractPrint.vm 23792 2012-08-10 10:47:17Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e95;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.SessionIndividualAheadStuExtract;
import ru.tandemservice.movestudent.utils.SessionPartModel;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 17.01.2013
 */
public class SessionIndividualAheadStuExtractPrint implements IPrintFormCreator<SessionIndividualAheadStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, SessionIndividualAheadStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        modifier.put("sessionType_G", SessionPartModel.getSessionShortTitle(extract.getSessionType(), GrammaCase.GENITIVE));

        modifier.put("sessionBeginDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getSessionBeginDate()));
        modifier.put("sessionEndDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getSessionEndDate()));
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}