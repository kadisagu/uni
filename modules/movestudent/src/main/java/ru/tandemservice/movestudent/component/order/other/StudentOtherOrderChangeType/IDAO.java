/* $Id$ */
package ru.tandemservice.movestudent.component.order.other.StudentOtherOrderChangeType;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Dmitry Seleznev
 * @since 18.04.2013
 */
public interface IDAO extends IUniDao<Model>
{
}