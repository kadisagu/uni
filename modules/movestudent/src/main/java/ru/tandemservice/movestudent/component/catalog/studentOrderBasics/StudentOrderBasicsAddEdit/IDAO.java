/* $Id$ */
package ru.tandemservice.movestudent.component.catalog.studentOrderBasics.StudentOrderBasicsAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics;

/**
 * @author AutoGenerator
 *         Created on 04.12.2012
 */
public interface IDAO extends IDefaultCatalogAddEditDAO<StudentOrderBasics, Model>
{
}