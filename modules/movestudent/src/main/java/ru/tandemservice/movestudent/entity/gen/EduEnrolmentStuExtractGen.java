package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.EduEnrolmentStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О зачислении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduEnrolmentStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.EduEnrolmentStuExtract";
    public static final String ENTITY_NAME = "eduEnrolmentStuExtract";
    public static final int VERSION_HASH = -1941938422;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENTRY_DATE = "entryDate";
    public static final String P_UNIVERSITY = "university";
    public static final String P_DEADLINE = "deadline";
    public static final String P_PREV_ORDER_ENTRY_DATE = "prevOrderEntryDate";

    private Date _entryDate;     // Дата зачисления
    private String _university;     // Зачислен из ОУ
    private Date _deadline;     // График сдачи разницы в учебных планах до
    private Date _prevOrderEntryDate;     // Дата зачисления по предыдущему приказу о зачислении

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата зачисления.
     */
    public Date getEntryDate()
    {
        return _entryDate;
    }

    /**
     * @param entryDate Дата зачисления.
     */
    public void setEntryDate(Date entryDate)
    {
        dirty(_entryDate, entryDate);
        _entryDate = entryDate;
    }

    /**
     * @return Зачислен из ОУ. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getUniversity()
    {
        return _university;
    }

    /**
     * @param university Зачислен из ОУ. Свойство не может быть null.
     */
    public void setUniversity(String university)
    {
        dirty(_university, university);
        _university = university;
    }

    /**
     * @return График сдачи разницы в учебных планах до.
     */
    public Date getDeadline()
    {
        return _deadline;
    }

    /**
     * @param deadline График сдачи разницы в учебных планах до.
     */
    public void setDeadline(Date deadline)
    {
        dirty(_deadline, deadline);
        _deadline = deadline;
    }

    /**
     * @return Дата зачисления по предыдущему приказу о зачислении.
     */
    public Date getPrevOrderEntryDate()
    {
        return _prevOrderEntryDate;
    }

    /**
     * @param prevOrderEntryDate Дата зачисления по предыдущему приказу о зачислении.
     */
    public void setPrevOrderEntryDate(Date prevOrderEntryDate)
    {
        dirty(_prevOrderEntryDate, prevOrderEntryDate);
        _prevOrderEntryDate = prevOrderEntryDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduEnrolmentStuExtractGen)
        {
            setEntryDate(((EduEnrolmentStuExtract)another).getEntryDate());
            setUniversity(((EduEnrolmentStuExtract)another).getUniversity());
            setDeadline(((EduEnrolmentStuExtract)another).getDeadline());
            setPrevOrderEntryDate(((EduEnrolmentStuExtract)another).getPrevOrderEntryDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduEnrolmentStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduEnrolmentStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new EduEnrolmentStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "entryDate":
                    return obj.getEntryDate();
                case "university":
                    return obj.getUniversity();
                case "deadline":
                    return obj.getDeadline();
                case "prevOrderEntryDate":
                    return obj.getPrevOrderEntryDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "entryDate":
                    obj.setEntryDate((Date) value);
                    return;
                case "university":
                    obj.setUniversity((String) value);
                    return;
                case "deadline":
                    obj.setDeadline((Date) value);
                    return;
                case "prevOrderEntryDate":
                    obj.setPrevOrderEntryDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entryDate":
                        return true;
                case "university":
                        return true;
                case "deadline":
                        return true;
                case "prevOrderEntryDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entryDate":
                    return true;
                case "university":
                    return true;
                case "deadline":
                    return true;
                case "prevOrderEntryDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "entryDate":
                    return Date.class;
                case "university":
                    return String.class;
                case "deadline":
                    return Date.class;
                case "prevOrderEntryDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduEnrolmentStuExtract> _dslPath = new Path<EduEnrolmentStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduEnrolmentStuExtract");
    }
            

    /**
     * @return Дата зачисления.
     * @see ru.tandemservice.movestudent.entity.EduEnrolmentStuExtract#getEntryDate()
     */
    public static PropertyPath<Date> entryDate()
    {
        return _dslPath.entryDate();
    }

    /**
     * @return Зачислен из ОУ. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.EduEnrolmentStuExtract#getUniversity()
     */
    public static PropertyPath<String> university()
    {
        return _dslPath.university();
    }

    /**
     * @return График сдачи разницы в учебных планах до.
     * @see ru.tandemservice.movestudent.entity.EduEnrolmentStuExtract#getDeadline()
     */
    public static PropertyPath<Date> deadline()
    {
        return _dslPath.deadline();
    }

    /**
     * @return Дата зачисления по предыдущему приказу о зачислении.
     * @see ru.tandemservice.movestudent.entity.EduEnrolmentStuExtract#getPrevOrderEntryDate()
     */
    public static PropertyPath<Date> prevOrderEntryDate()
    {
        return _dslPath.prevOrderEntryDate();
    }

    public static class Path<E extends EduEnrolmentStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<Date> _entryDate;
        private PropertyPath<String> _university;
        private PropertyPath<Date> _deadline;
        private PropertyPath<Date> _prevOrderEntryDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата зачисления.
     * @see ru.tandemservice.movestudent.entity.EduEnrolmentStuExtract#getEntryDate()
     */
        public PropertyPath<Date> entryDate()
        {
            if(_entryDate == null )
                _entryDate = new PropertyPath<Date>(EduEnrolmentStuExtractGen.P_ENTRY_DATE, this);
            return _entryDate;
        }

    /**
     * @return Зачислен из ОУ. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.EduEnrolmentStuExtract#getUniversity()
     */
        public PropertyPath<String> university()
        {
            if(_university == null )
                _university = new PropertyPath<String>(EduEnrolmentStuExtractGen.P_UNIVERSITY, this);
            return _university;
        }

    /**
     * @return График сдачи разницы в учебных планах до.
     * @see ru.tandemservice.movestudent.entity.EduEnrolmentStuExtract#getDeadline()
     */
        public PropertyPath<Date> deadline()
        {
            if(_deadline == null )
                _deadline = new PropertyPath<Date>(EduEnrolmentStuExtractGen.P_DEADLINE, this);
            return _deadline;
        }

    /**
     * @return Дата зачисления по предыдущему приказу о зачислении.
     * @see ru.tandemservice.movestudent.entity.EduEnrolmentStuExtract#getPrevOrderEntryDate()
     */
        public PropertyPath<Date> prevOrderEntryDate()
        {
            if(_prevOrderEntryDate == null )
                _prevOrderEntryDate = new PropertyPath<Date>(EduEnrolmentStuExtractGen.P_PREV_ORDER_ENTRY_DATE, this);
            return _prevOrderEntryDate;
        }

        public Class getEntityClass()
        {
            return EduEnrolmentStuExtract.class;
        }

        public String getEntityName()
        {
            return "eduEnrolmentStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
