/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e99.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.movestudent.entity.AcadGrantPaymentStopStuExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 20.02.2013
 */
public class Controller extends CommonModularStudentExtractAddEditController<AcadGrantPaymentStopStuExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        if(getModel(component).isAddForm())
        {
            onClickChangeOrderDateAndNumber(component);
        }
    }

    public void onClickChangeOrderDateAndNumber(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getAcadGrantExtract() != null)
        {
            model.setAcadGrantOrderDate(model.getAcadGrantExtract().getParagraph().getOrder().getCommitDate());
            model.setAcadGrantOrderNumber(model.getAcadGrantExtract().getParagraph().getOrder().getNumber());
        }
    }
}
