/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e38.utils;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 15.10.2013
 */
public class SplitStudentsExtParagraphWrapper implements Comparable<SplitStudentsExtParagraphWrapper>
{
    private final StudentCategory _studentCategory;
    private final CompensationType _compensationType;
    private final EducationLevelsHighSchool _educationLevelHighSchool;

    private final ListStudentExtract _firstExtract;

    private final List<Person> _personList = new ArrayList<>();

    public SplitStudentsExtParagraphWrapper(StudentCategory studentCategory, CompensationType compensationType, EducationLevelsHighSchool educationLevelHighSchool, ListStudentExtract firstExtract)
    {
        _studentCategory = studentCategory;
        _compensationType = compensationType;
        _educationLevelHighSchool = educationLevelHighSchool;
        _firstExtract = firstExtract;
    }

    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public EducationLevelsHighSchool getEducationLevelHighSchool()
    {
        return _educationLevelHighSchool;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    public List<Person> getPersonList()
    {
        return _personList;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof SplitStudentsExtParagraphWrapper))
            return false;

        SplitStudentsExtParagraphWrapper that = (SplitStudentsExtParagraphWrapper) o;

        return _studentCategory.equals(that.getStudentCategory())
                && _compensationType.equals(that.getCompensationType())
                && _educationLevelHighSchool.equals(that.getEducationLevelHighSchool());
    }

    @Override
    public int compareTo(SplitStudentsExtParagraphWrapper o)
    {
        if (StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(_studentCategory.getCode()) && StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(o.getStudentCategory().getCode()))
            return -1;
        else if (StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(_studentCategory.getCode()) && StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(o.getStudentCategory().getCode()))
            return 1;

        if(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(_compensationType.getCode()) && CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(o.getCompensationType().getCode()))
            return -1;
        else if(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(_compensationType.getCode()) && CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(o.getCompensationType().getCode()))
            return 1;

        return _educationLevelHighSchool.getTitle().compareTo(o.getEducationLevelHighSchool().getTitle());
    }
}
