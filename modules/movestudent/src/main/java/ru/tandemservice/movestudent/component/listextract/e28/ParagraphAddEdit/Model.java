/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e28.ParagraphAddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.AddGroupManagerStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 14.09.2012
 */
public class Model extends AbstractListParagraphAddEditModel<AddGroupManagerStuListExtract> implements IGroupModel
{
    private EducationYear _educationYear;
    private Course _course;
    private Group _group;
    private CompensationType _compensationType;
    private List<EducationYear> _educationYearList;
    private ISelectModel _groupListModel;
    private List<CompensationType> _compensationTypeList;
    private IMultiSelectModel _selectedGroupListModel;

    public boolean getStudentDisabled()
    {
        Boolean disabled = (Boolean) getDataSource().getCurrentValueEntity().getProperty(AbstractListParagraphAddEditModel.P_STUDENT_DISABLED);
        return null != disabled ? disabled : false ;
    }

    public String getSelectedGroupId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "selectedGroupId_" + dataSource.getCurrentEntity().getId();
    }

    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    public void setEducationYear(EducationYear educationYear)
    {
        _educationYear = educationYear;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public List<EducationYear> getEducationYearList()
    {
        return _educationYearList;
    }

    public void setEducationYearList(List<EducationYear> educationYearList)
    {
        _educationYearList = educationYearList;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public IMultiSelectModel getSelectedGroupListModel()
    {
        return _selectedGroupListModel;
    }

    public void setSelectedGroupListModel(IMultiSelectModel selectedGroupListModel)
    {
        _selectedGroupListModel = selectedGroupListModel;
    }
}
