/* $Id$ */
package ru.tandemservice.movestudent.base.bo.OrderComment.ui.Edit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;

/**
 * @author Ekaterina Zvereva
 * @since 14.01.2015
 */
@Input(keys = {"extractId"}, bindings = {"extractId"})
public class OrderCommentEditUI extends UIPresenter
{
    private Long _extractId;
    private AbstractStudentExtract _extract;

    public AbstractStudentExtract getExtract()
    {
        return _extract;
    }

    public void setExtract(AbstractStudentExtract extract)
    {
        _extract = extract;
    }

    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        _extractId = extractId;
    }

    public void onClickApply()
    {
        _extract.setCommentEdited(true);
        DataAccessServices.dao().update(_extract);

        deactivate();
    }

    @Override
    public void onComponentRefresh()
    {
        _extract = DataAccessServices.dao().get(AbstractStudentExtract.class, _extractId);
        if (!_extract.isCommentEdited() && StringUtils.isEmpty(_extract.getComment()))
            _extract.setComment(_extract.getGeneratedComment(false));
    }
}