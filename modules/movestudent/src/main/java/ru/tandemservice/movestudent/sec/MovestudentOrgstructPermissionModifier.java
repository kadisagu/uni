/* $Id$ */
package ru.tandemservice.movestudent.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

/**
 * @author dseleznev
 * @since 31.01.2012
 */
public class MovestudentOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(final Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        final SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("movestudent");
        config.setName("movestudent-orgstruct-sec-config");
        config.setTitle("");

        final ModuleGlobalGroupMeta mggOrgstruct = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
        final ModuleLocalGroupMeta mlgOrgstruct = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");

        for (final OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            final String code = description.getCode();
            final ClassGroupMeta gcg = PermissionMetaUtil.createClassGroup(mggOrgstruct, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
            final ClassGroupMeta lcg = PermissionMetaUtil.createClassGroup(mlgOrgstruct, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

            final PermissionGroupMeta pgMoveStudentTab = PermissionMetaUtil.createPermissionGroup(config, code + "MoveStudentPermissionGroup", "Вкладка «Движение студентов»");
            PermissionMetaUtil.createGroupRelation(config, pgMoveStudentTab.getName(), gcg.getName());
            PermissionMetaUtil.createGroupRelation(config, pgMoveStudentTab.getName(), lcg.getName());
            PermissionMetaUtil.createPermission(pgMoveStudentTab, "orgUnit_viewMoveStudent_" + code, "Просмотр");

            final PermissionGroupMeta pgIndividualOrdersTab = PermissionMetaUtil.createPermissionGroup(pgMoveStudentTab, code + "MoveStudentPermissionGroup_individualOrders", "Вкладка «Индивидуальные приказы»");
            PermissionMetaUtil.createPermission(pgIndividualOrdersTab, "orgUnit_viewMoveStudent_" + code + "_individualOrders", "Просмотр");
            PermissionMetaUtil.createPermission(pgIndividualOrdersTab, "orgUnit_print_" + code + "_individualOrders", "Печать индивидуального приказа");
            PermissionMetaUtil.createPermission(pgIndividualOrdersTab, "orgUnit_printPdf_" + code + "_individualOrders", "Печать индивидуального приказа в PDF");
            PermissionMetaUtil.createPermission(pgIndividualOrdersTab, "orgUnit_edit_" + code + "_individualOrders", "Редактирование индивидуального приказа");
            PermissionMetaUtil.createPermission(pgIndividualOrdersTab, "orgUnit_delete_" + code + "_individualOrders", "Удаление индивидуального приказа");

            final PermissionGroupMeta pgProjectsTab = PermissionMetaUtil.createPermissionGroup(pgMoveStudentTab, code + "MoveStudentPermissionGroup_projects", "Вкладка «Проекты сборного приказа»");
            PermissionMetaUtil.createPermission(pgProjectsTab, "orgUnit_viewMoveStudent_" + code + "_projects", "Просмотр");
            PermissionMetaUtil.createPermission(pgProjectsTab, "orgUnit_add_" + code + "_projects", "Добавление проекта приказа");
            PermissionMetaUtil.createPermission(pgProjectsTab, "orgUnit_print_" + code + "_projects", "Печать проекта приказа");
            PermissionMetaUtil.createPermission(pgProjectsTab, "orgUnit_printPdf_" + code + "_projects", "Печать проекта приказа в PDF");
            PermissionMetaUtil.createPermission(pgProjectsTab, "orgUnit_edit_" + code + "_projects", "Редактирование проекта приказа");
            PermissionMetaUtil.createPermission(pgProjectsTab, "orgUnit_delete_" + code + "_projects", "Удаление проекта приказа");

            final PermissionGroupMeta pgModularOrdersTab = PermissionMetaUtil.createPermissionGroup(pgMoveStudentTab, code + "academyMoveStudentPermissionGroup_modularorders", "Вкладка «Сборные приказы»");
            PermissionMetaUtil.createPermission(pgModularOrdersTab, "orgUnit_viewMoveStudent_" + code + "_modularorders", "Просмотр");
            PermissionMetaUtil.createPermission(pgModularOrdersTab, "orgUnit_add_" + code + "_modularorders", "Добавление сборного приказа");
            PermissionMetaUtil.createPermission(pgModularOrdersTab, "orgUnit_print_" + code + "_modularorders", "Печать сборного приказа");
            PermissionMetaUtil.createPermission(pgModularOrdersTab, "orgUnit_printPdf_" + code + "_modularorders", "Печать сборного приказа в PDF");
            PermissionMetaUtil.createPermission(pgModularOrdersTab, "orgUnit_edit_" + code + "_modularorders", "Редактирование сборного приказа");
            PermissionMetaUtil.createPermission(pgModularOrdersTab, "orgUnit_delete_" + code + "_modularorders", "Удаление сборного приказа");

            final PermissionGroupMeta pgModularExtractsTab = PermissionMetaUtil.createPermissionGroup(pgMoveStudentTab, code + "academyMoveStudentPermissionGroup_extracts", "Вкладка «Выписки из сборного приказа»");
            PermissionMetaUtil.createPermission(pgModularExtractsTab, "orgUnit_viewMoveStudent_" + code + "_extracts", "Просмотр");
            PermissionMetaUtil.createPermission(pgModularExtractsTab, "orgUnit_print_" + code + "_extracts", "Печать выписки");
            PermissionMetaUtil.createPermission(pgModularExtractsTab, "orgUnit_printPdf_" + code + "_extracts", "Печать выписки в PDF");

            final PermissionGroupMeta pgListOrdersTab = PermissionMetaUtil.createPermissionGroup(pgMoveStudentTab, code + "academyMoveStudentPermissionGroup_listorders", "Вкладка «Списочные приказы»");
            PermissionMetaUtil.createPermission(pgListOrdersTab, "orgUnit_viewMoveStudent_" + code + "_listorders", "Просмотр");
            PermissionMetaUtil.createPermission(pgListOrdersTab, "orgUnit_add_" + code + "_listorders", "Добавление списочного приказа");
            PermissionMetaUtil.createPermission(pgListOrdersTab, "orgUnit_print_" + code + "_listorders", "Печать списочного приказа");
            PermissionMetaUtil.createPermission(pgListOrdersTab, "orgUnit_printPdf_" + code + "_listorders", "Печать списочного приказа в PDF");
            PermissionMetaUtil.createPermission(pgListOrdersTab, "orgUnit_edit_" + code + "_listorders", "Редактирование списочного приказа");
            PermissionMetaUtil.createPermission(pgListOrdersTab, "orgUnit_delete_" + code + "_listorders", "Удаление списочного приказа");

            final PermissionGroupMeta pgListExtractsTab = PermissionMetaUtil.createPermissionGroup(pgMoveStudentTab, code + "academyMoveStudentPermissionGroup_listextracts", "Вкладка «Выписки из списочных приказов»");
            PermissionMetaUtil.createPermission(pgListExtractsTab, "orgUnit_viewMoveStudent_" + code + "_listextracts", "Просмотр");
            PermissionMetaUtil.createPermission(pgListExtractsTab, "orgUnit_printExtract_" + code + "_listextracts", "Печать выписки");
            PermissionMetaUtil.createPermission(pgListExtractsTab, "orgUnit_printExtractPdf_" + code + "_listextracts", "Печать выписки в PDF");

            final PermissionGroupMeta pgOtherOrdersTab = PermissionMetaUtil.createPermissionGroup(pgMoveStudentTab, code + "MoveStudentPermissionGroup_otherOrders", "Вкладка «Прочие приказы»");
            PermissionMetaUtil.createPermission(pgOtherOrdersTab, "orgUnit_viewMoveStudent_" + code + "_otherOrders", "Просмотр");
            PermissionMetaUtil.createPermission(pgOtherOrdersTab, "orgUnit_print_" + code + "_otherOrders", "Печать индивидуального приказа");
            PermissionMetaUtil.createPermission(pgOtherOrdersTab, "orgUnit_edit_" + code + "_otherOrders", "Редактирование индивидуального приказа");
            PermissionMetaUtil.createPermission(pgOtherOrdersTab, "orgUnit_delete_" + code + "_otherOrders", "Удаление индивидуального приказа");
        }

        securityConfigMetaMap.put(config.getName(), config);
    }
}