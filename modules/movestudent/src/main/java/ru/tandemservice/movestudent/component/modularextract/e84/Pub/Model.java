/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e84.Pub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.movestudent.entity.RePassDiscComissStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToComissionMember;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 31.08.2012
 */
public class Model extends ModularStudentExtractPubModel<RePassDiscComissStuExtract>
{
    private DynamicListDataSource<StuExtractToComissionMember> _dataSource;

    public DynamicListDataSource<StuExtractToComissionMember> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<StuExtractToComissionMember> dataSource)
    {
        _dataSource = dataSource;
    }
}