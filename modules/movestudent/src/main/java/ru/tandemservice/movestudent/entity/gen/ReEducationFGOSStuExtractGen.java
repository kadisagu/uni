package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.ReEducationFGOSStuExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О предоставлении повторного года обучения с переходом на ФГОС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ReEducationFGOSStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ReEducationFGOSStuExtract";
    public static final String ENTITY_NAME = "reEducationFGOSStuExtract";
    public static final int VERSION_HASH = 1494441116;
    private static IEntityMeta ENTITY_META;

    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_ANNUL_ATTESTATION_RESULTS = "annulAttestationResults";
    public static final String L_PREV_COURSE = "prevCourse";
    public static final String L_EDU_YEAR = "eduYear";
    public static final String P_LIQUIDATE_G_O_S_DIFFERENCE = "liquidateGOSDifference";
    public static final String P_LIQUIDATION_DEADLINE_DATE = "liquidationDeadlineDate";

    private Date _beginDate;     // Дата начала повторного обучения
    private boolean _annulAttestationResults;     // Аннулировать аттестации по прослушанным за прошедший курс дисциплинам
    private Course _prevCourse;     // Курс, за который требуется аннулировать аттестации по дисциплинам
    private EducationYear _eduYear;     // Учебный год, за который требуется аннулировать аттестации по дисциплинам
    private boolean _liquidateGOSDifference;     // Ликвидировать разницу в учебных планах
    private Date _liquidationDeadlineDate;     // Крайняя дата ликвидации разницы в учебных планах

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата начала повторного обучения. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала повторного обучения. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Аннулировать аттестации по прослушанным за прошедший курс дисциплинам. Свойство не может быть null.
     */
    @NotNull
    public boolean isAnnulAttestationResults()
    {
        return _annulAttestationResults;
    }

    /**
     * @param annulAttestationResults Аннулировать аттестации по прослушанным за прошедший курс дисциплинам. Свойство не может быть null.
     */
    public void setAnnulAttestationResults(boolean annulAttestationResults)
    {
        dirty(_annulAttestationResults, annulAttestationResults);
        _annulAttestationResults = annulAttestationResults;
    }

    /**
     * @return Курс, за который требуется аннулировать аттестации по дисциплинам.
     */
    public Course getPrevCourse()
    {
        return _prevCourse;
    }

    /**
     * @param prevCourse Курс, за который требуется аннулировать аттестации по дисциплинам.
     */
    public void setPrevCourse(Course prevCourse)
    {
        dirty(_prevCourse, prevCourse);
        _prevCourse = prevCourse;
    }

    /**
     * @return Учебный год, за который требуется аннулировать аттестации по дисциплинам.
     */
    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    /**
     * @param eduYear Учебный год, за который требуется аннулировать аттестации по дисциплинам.
     */
    public void setEduYear(EducationYear eduYear)
    {
        dirty(_eduYear, eduYear);
        _eduYear = eduYear;
    }

    /**
     * @return Ликвидировать разницу в учебных планах. Свойство не может быть null.
     */
    @NotNull
    public boolean isLiquidateGOSDifference()
    {
        return _liquidateGOSDifference;
    }

    /**
     * @param liquidateGOSDifference Ликвидировать разницу в учебных планах. Свойство не может быть null.
     */
    public void setLiquidateGOSDifference(boolean liquidateGOSDifference)
    {
        dirty(_liquidateGOSDifference, liquidateGOSDifference);
        _liquidateGOSDifference = liquidateGOSDifference;
    }

    /**
     * @return Крайняя дата ликвидации разницы в учебных планах.
     */
    public Date getLiquidationDeadlineDate()
    {
        return _liquidationDeadlineDate;
    }

    /**
     * @param liquidationDeadlineDate Крайняя дата ликвидации разницы в учебных планах.
     */
    public void setLiquidationDeadlineDate(Date liquidationDeadlineDate)
    {
        dirty(_liquidationDeadlineDate, liquidationDeadlineDate);
        _liquidationDeadlineDate = liquidationDeadlineDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ReEducationFGOSStuExtractGen)
        {
            setBeginDate(((ReEducationFGOSStuExtract)another).getBeginDate());
            setAnnulAttestationResults(((ReEducationFGOSStuExtract)another).isAnnulAttestationResults());
            setPrevCourse(((ReEducationFGOSStuExtract)another).getPrevCourse());
            setEduYear(((ReEducationFGOSStuExtract)another).getEduYear());
            setLiquidateGOSDifference(((ReEducationFGOSStuExtract)another).isLiquidateGOSDifference());
            setLiquidationDeadlineDate(((ReEducationFGOSStuExtract)another).getLiquidationDeadlineDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ReEducationFGOSStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ReEducationFGOSStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new ReEducationFGOSStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                    return obj.getBeginDate();
                case "annulAttestationResults":
                    return obj.isAnnulAttestationResults();
                case "prevCourse":
                    return obj.getPrevCourse();
                case "eduYear":
                    return obj.getEduYear();
                case "liquidateGOSDifference":
                    return obj.isLiquidateGOSDifference();
                case "liquidationDeadlineDate":
                    return obj.getLiquidationDeadlineDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "annulAttestationResults":
                    obj.setAnnulAttestationResults((Boolean) value);
                    return;
                case "prevCourse":
                    obj.setPrevCourse((Course) value);
                    return;
                case "eduYear":
                    obj.setEduYear((EducationYear) value);
                    return;
                case "liquidateGOSDifference":
                    obj.setLiquidateGOSDifference((Boolean) value);
                    return;
                case "liquidationDeadlineDate":
                    obj.setLiquidationDeadlineDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                        return true;
                case "annulAttestationResults":
                        return true;
                case "prevCourse":
                        return true;
                case "eduYear":
                        return true;
                case "liquidateGOSDifference":
                        return true;
                case "liquidationDeadlineDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                    return true;
                case "annulAttestationResults":
                    return true;
                case "prevCourse":
                    return true;
                case "eduYear":
                    return true;
                case "liquidateGOSDifference":
                    return true;
                case "liquidationDeadlineDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                    return Date.class;
                case "annulAttestationResults":
                    return Boolean.class;
                case "prevCourse":
                    return Course.class;
                case "eduYear":
                    return EducationYear.class;
                case "liquidateGOSDifference":
                    return Boolean.class;
                case "liquidationDeadlineDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ReEducationFGOSStuExtract> _dslPath = new Path<ReEducationFGOSStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ReEducationFGOSStuExtract");
    }
            

    /**
     * @return Дата начала повторного обучения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ReEducationFGOSStuExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Аннулировать аттестации по прослушанным за прошедший курс дисциплинам. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ReEducationFGOSStuExtract#isAnnulAttestationResults()
     */
    public static PropertyPath<Boolean> annulAttestationResults()
    {
        return _dslPath.annulAttestationResults();
    }

    /**
     * @return Курс, за который требуется аннулировать аттестации по дисциплинам.
     * @see ru.tandemservice.movestudent.entity.ReEducationFGOSStuExtract#getPrevCourse()
     */
    public static Course.Path<Course> prevCourse()
    {
        return _dslPath.prevCourse();
    }

    /**
     * @return Учебный год, за который требуется аннулировать аттестации по дисциплинам.
     * @see ru.tandemservice.movestudent.entity.ReEducationFGOSStuExtract#getEduYear()
     */
    public static EducationYear.Path<EducationYear> eduYear()
    {
        return _dslPath.eduYear();
    }

    /**
     * @return Ликвидировать разницу в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ReEducationFGOSStuExtract#isLiquidateGOSDifference()
     */
    public static PropertyPath<Boolean> liquidateGOSDifference()
    {
        return _dslPath.liquidateGOSDifference();
    }

    /**
     * @return Крайняя дата ликвидации разницы в учебных планах.
     * @see ru.tandemservice.movestudent.entity.ReEducationFGOSStuExtract#getLiquidationDeadlineDate()
     */
    public static PropertyPath<Date> liquidationDeadlineDate()
    {
        return _dslPath.liquidationDeadlineDate();
    }

    public static class Path<E extends ReEducationFGOSStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Boolean> _annulAttestationResults;
        private Course.Path<Course> _prevCourse;
        private EducationYear.Path<EducationYear> _eduYear;
        private PropertyPath<Boolean> _liquidateGOSDifference;
        private PropertyPath<Date> _liquidationDeadlineDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата начала повторного обучения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ReEducationFGOSStuExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(ReEducationFGOSStuExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Аннулировать аттестации по прослушанным за прошедший курс дисциплинам. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ReEducationFGOSStuExtract#isAnnulAttestationResults()
     */
        public PropertyPath<Boolean> annulAttestationResults()
        {
            if(_annulAttestationResults == null )
                _annulAttestationResults = new PropertyPath<Boolean>(ReEducationFGOSStuExtractGen.P_ANNUL_ATTESTATION_RESULTS, this);
            return _annulAttestationResults;
        }

    /**
     * @return Курс, за который требуется аннулировать аттестации по дисциплинам.
     * @see ru.tandemservice.movestudent.entity.ReEducationFGOSStuExtract#getPrevCourse()
     */
        public Course.Path<Course> prevCourse()
        {
            if(_prevCourse == null )
                _prevCourse = new Course.Path<Course>(L_PREV_COURSE, this);
            return _prevCourse;
        }

    /**
     * @return Учебный год, за который требуется аннулировать аттестации по дисциплинам.
     * @see ru.tandemservice.movestudent.entity.ReEducationFGOSStuExtract#getEduYear()
     */
        public EducationYear.Path<EducationYear> eduYear()
        {
            if(_eduYear == null )
                _eduYear = new EducationYear.Path<EducationYear>(L_EDU_YEAR, this);
            return _eduYear;
        }

    /**
     * @return Ликвидировать разницу в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ReEducationFGOSStuExtract#isLiquidateGOSDifference()
     */
        public PropertyPath<Boolean> liquidateGOSDifference()
        {
            if(_liquidateGOSDifference == null )
                _liquidateGOSDifference = new PropertyPath<Boolean>(ReEducationFGOSStuExtractGen.P_LIQUIDATE_G_O_S_DIFFERENCE, this);
            return _liquidateGOSDifference;
        }

    /**
     * @return Крайняя дата ликвидации разницы в учебных планах.
     * @see ru.tandemservice.movestudent.entity.ReEducationFGOSStuExtract#getLiquidationDeadlineDate()
     */
        public PropertyPath<Date> liquidationDeadlineDate()
        {
            if(_liquidationDeadlineDate == null )
                _liquidationDeadlineDate = new PropertyPath<Date>(ReEducationFGOSStuExtractGen.P_LIQUIDATION_DEADLINE_DATE, this);
            return _liquidationDeadlineDate;
        }

        public Class getEntityClass()
        {
            return ReEducationFGOSStuExtract.class;
        }

        public String getEntityName()
        {
            return "reEducationFGOSStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
