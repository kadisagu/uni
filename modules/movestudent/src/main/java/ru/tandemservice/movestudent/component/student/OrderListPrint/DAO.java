// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.student.OrderListPrint;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 24.03.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setStudent(getNotNull(Student.class, model.getStudent().getId()));
    }

    @Override
    public RtfDocument print(Model model)
    {
        Student student = model.getStudent();
        EducationOrgUnit eduOu = student.getEducationOrgUnit();

        ITemplateDocument template = getCatalogItem(MovestudentTemplate.class, "studentOrderList");
        RtfDocument document = new RtfReader().read(template.getContent());

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("printDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        injectModifier.put("number", student.getPersonalFileNumber());
        injectModifier.put("fio", student.getPerson().getFullFio());
        injectModifier.put("birthYear", DateFormatter.DEFAULT_DATE_FORMATTER.format(student.getPerson().getIdentityCard().getBirthDate()));
        injectModifier.put("developForm", eduOu.getDevelopForm().getTitle() + " ("+student.getCompensationType().getShortTitle()+")");
        injectModifier.put("fmOrgUnitTypeTitle", eduOu.getFormativeOrgUnit().getOrgUnitType().getTitle());
        injectModifier.put("fmOrgUnitTitle", eduOu.getFormativeOrgUnit().getTitle());
        injectModifier.put("eduLevelTypeTitle", EducationOrgUnit.EDU_LVL_HS_COLUMN_TITLE);
        injectModifier.put("eduLevelTitle", eduOu.getEducationLevelHighSchool().getPrintTitle());
        injectModifier.put("group", student.getGroup() == null ? "без группы" : student.getGroup().getTitle());

        AddressBase address = student.getPerson().getIdentityCard().getAddress();
        injectModifier.put("region", address instanceof AddressDetailed && ((AddressDetailed) address).getSettlement() != null ? ((AddressDetailed) address).getSettlement().getTitle() : "");
        injectModifier.put("citizenship", student.getPerson().getIdentityCard().getCitizenship().getTitle());
        injectModifier.modify(document);

        List<Row> rows = new ArrayList<>();

        MQBuilder builder = new MQBuilder(AbstractStudentExtract.ENTITY_CLASS, "e");
        builder.addJoin("e", IAbstractExtract.L_STATE, "state");
        builder.add(MQExpression.eq("state", ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED));
        builder.add(MQExpression.eq("e", IAbstractExtract.L_ENTITY, model.getStudent()));
        for (AbstractStudentExtract extract : builder.<AbstractStudentExtract>getResultList(getSession()))
        {
            if (extract.getParagraph() == null || extract.getParagraph().getOrder() == null) continue;
            rows.add(new Row(extract, extract.getBeginDate(), extract.getEndDate()));
        }

        // этот модуль не зависит от uniec, и наоборот
        // поэтому вот такой вот хак по выбору данных
        if (EntityRuntime.getMeta("ru.tandemservice.uniec.entity.orders.EnrollmentExtract") != null)
        {
            builder = new MQBuilder("ru.tandemservice.uniec.entity.orders.EnrollmentExtract", "e");
            builder.addJoin("e", IAbstractExtract.L_STATE, "state");
            builder.add(MQExpression.eq("state", ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED));
            builder.add(MQExpression.eq("e", "studentNew", model.getStudent()));
            for (IAbstractExtract extract : builder.<IAbstractExtract>getResultList(getSession()))
            {
                if (extract.getParagraph() == null || extract.getParagraph().getOrder() == null) continue;
                rows.add(new Row(extract, (Date) extract.getParagraph().getOrder().getProperty("enrollmentDate"), null));
            }
        }

        Collections.sort(rows);
        List<String[]> table = new ArrayList<>();
        int num = 1;
        for (Row row : rows)
            table.add(row.print(num++));
        RtfTableModifier tableModifier = new RtfTableModifier().put("T", table.toArray(new String[table.size()][]));
        tableModifier.modify(document);

        return document;
    }

    private static class Row implements Comparable
    {
        Date commitDate;
        List<String> data = new ArrayList<>();

        Row(IAbstractExtract extract, Date begin, Date end)
        {
            IAbstractOrder order = extract.getParagraph().getOrder();
            // у всех наших приказов такая дата есть
            commitDate = (Date) order.getProperty("commitDate");
            // для тех приказов, где есть тип, берем тип приказа
            String orderType = null;
            try
            {
                orderType = (String) order.getProperty("type.title");
            } catch (Exception e) { /* do nothing */ }

            // не нашли - берем тип выписки
            if (null == orderType)
                orderType = (String) extract.getProperty("type.title");

            data.add(orderType);

            data.add(order.getNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(commitDate));
            data.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(begin));
            data.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(end));
        }

        String[] print(int num)
        {
            data.add(0, String.valueOf(num));
            return data.toArray(new String[data.size()]);
        }

        @Override
        public int compareTo(Object other)
        {
            if(null == ((Row) other).commitDate && null == commitDate) return 0;
            if(null == ((Row) other).commitDate) return -1;
            if(null == commitDate) return 1;
            return commitDate.compareTo(((Row) other).commitDate);
        }
    }
}
