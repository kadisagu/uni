package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.TransferFormativeStuExtractGen;

import java.util.Date;

/**
 * О переводе со специальности на специальность (с направления на направление) со сменой формирующего подразделения
 */
public class TransferFormativeStuExtract extends TransferFormativeStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}