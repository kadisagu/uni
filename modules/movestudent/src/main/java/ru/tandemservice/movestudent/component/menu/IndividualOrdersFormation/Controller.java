/* $Id$ */
package ru.tandemservice.movestudent.component.menu.IndividualOrdersFormation;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.component.commons.IMoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.MoveStudentColumns;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.dao.MoveDaoFacade;

/**
 * @author Dmitry Seleznev
 * @since 31.10.2012
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setSettings(UniBaseUtils.getDataSettings(component, "IndividualOrdersFormation.filter"));
        model.getSettings().set(IMoveStudentMQBuilder.LAST_NAME, null);

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<ModularStudentExtract> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(MoveStudentColumns.getCreateDateColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractOrderNumberColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractCommitDateColumn());
        dataSource.addColumn(MoveStudentColumns.getFullFioColumn());
        dataSource.addColumn(MoveStudentColumns.getFormativeOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getTerritorialOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getProducingOrgUnitColumn());
        MoveStudentColumns.addEduLevelTitleColumns(dataSource);
        dataSource.addColumn(MoveStudentColumns.getCourseColumn());
        dataSource.addColumn(MoveStudentColumns.getGroupColumn());
        dataSource.addColumn(MoveStudentColumns.getDevelopFormColumn());
        dataSource.addColumn(MoveStudentColumns.getCompensationTypeColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractTypeColumn("Тип приказа"));
        dataSource.addColumn(MoveStudentColumns.getModularExtractReasonColumn("Причина приказа"));
        dataSource.addColumn(MoveStudentColumns.getExtractOrderStateColumn());
        dataSource.addColumn(MoveStudentColumns.getIndividualOrderPrintColumn(this, getModel(component).getSecModel().getPermission("print_menuList")));
        if(MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
            dataSource.addColumn(MoveStudentColumns.getIndividualOrderPrintPdfColumn(this, getModel(component).getSecModel().getPermission("printPdf_menuList")));
        dataSource.addColumn(MoveStudentColumns.getIndividualOrderEditColumn(this, getModel(component).getSecModel().getPermission("edit_menuList")));
        dataSource.addColumn(MoveStudentColumns.getIndividualOrderDeleteColumn(this, getModel(component).getSecModel().getPermission("delete_menuList")));
        dataSource.setOrder(IAbstractExtract.P_CREATE_DATE, OrderDirection.desc);
        getModel(component).setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);
        UniBaseUtils.validateDatesPeriod(model.getSettings(), "createDateFrom", "createDateTo");

        if (!UserContext.getInstance().getErrorCollector().isHasFieldErrors())
        {
            DataSettingsFacade.saveSettings(model.getSettings());
            model.getDataSource().refresh();
        }
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }

    public void onClickIndividualOrderPrint(IBusinessComponent component)
    {
        ModularStudentExtract extract = DataAccessServices.dao().get((Long) component.getListenerParameter());
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_PRINT, new ParametersMap()
                .add("orderId", extract.getParagraph().getOrder().getId())
        ));
    }

    public void onClickIndividualOrderPrintPdf(IBusinessComponent component)
    {
        ModularStudentExtract extract = DataAccessServices.dao().get((Long) component.getListenerParameter());
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_PRINT, new ParametersMap()
                .add("orderId", extract.getParagraph().getOrder().getId())
                .add("printPdf", Boolean.TRUE)
        ));
    }

    public void onClickIndividualOrderEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(MoveStudentUtils.getModularExtractAddEditComponentNew((Long) component.getListenerParameter()), new ParametersMap()
                .add("extractId", component.getListenerParameter())));
    }

    public void onClickIndividualOrderDelete(IBusinessComponent component)
    {
        ModularStudentExtract extract = getDao().get(ModularStudentExtract.class, (Long) component.getListenerParameter());
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(extract.getParagraph().getOrder());
    }
}