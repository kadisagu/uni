package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.WeekendChildOutStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О выходе из отпуска по уходу за ребенком
 */
public class WeekendChildOutStuExtract extends WeekendChildOutStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}