package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.movestudent.entity.gen.GiveDiplomaStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О выдаче диплома и присвоении квалификации
 */
public class GiveDiplomaStuExtract extends GiveDiplomaStuExtractGen implements IExcludeExtract
{
    @Override
    public Date getExcludeDate()
    {
        return getBeginDate();
    }
}