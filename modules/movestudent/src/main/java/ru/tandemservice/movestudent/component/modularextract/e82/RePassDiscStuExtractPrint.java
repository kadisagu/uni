/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e82;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.RePassDiscStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToRePassDiscipline;

import java.util.Arrays;
import java.util.List;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 31.08.2012
 */
public class RePassDiscStuExtractPrint implements IPrintFormCreator<RePassDiscStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, RePassDiscStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        StringBuilder disciplineListStr = new StringBuilder();
        List<StuExtractToRePassDiscipline> disciplineList = DataAccessServices.dao().getList(StuExtractToRePassDiscipline.class, StuExtractToRePassDiscipline.extract().s(), extract, StuExtractToRePassDiscipline.priority().s());
        List<String> controlActionsList = Arrays.asList((ApplicationRuntime.getProperty("commonextract.controlActionsList")).split(";"));
        List<String> controlActionsAccusativeList = Arrays.asList((ApplicationRuntime.getProperty("commonextract.controlActionsList_A")).split(";"));

        for (StuExtractToRePassDiscipline disc : disciplineList)
        {
            String controlActionStr = disc.getControlAction();
            if(controlActionsList.contains(controlActionStr))
            {
                int idx = controlActionsList.indexOf(controlActionStr);
                if(idx > 0 && controlActionsAccusativeList.size() > idx)
                    controlActionStr = controlActionsAccusativeList.get(idx);
            }

            disciplineListStr.append(disciplineListStr.length() > 0 ? ", " : "");
            disciplineListStr.append(controlActionStr).append(" по дисциплине «");
            disciplineListStr.append(disc.getDiscipline()).append("» (");
            disciplineListStr.append(disc.getSrcTerm().getTitle()).append(" семестр)");
        }
        disciplineListStr.append(" в ");
        disciplineListStr.append(extract.getDestTerm().getTitle()).append(" семестре ");
        disciplineListStr.append(extract.getDestEduYear().getTitle()).append(" учебного года");
        modifier.put("discList", disciplineListStr.toString());

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}