/* $Id$ */
package ru.tandemservice.movestudent.component.commons.gradation;

/**
 * Интерфейс для выписок об однокртной выплате (или повышении выплаты)
 *
 * @author Nikolay Fedorovskih
 * @since 04.07.2013
 */
public interface ISinglePayment
{
    int getYear();

    int getMonth();
}