package ru.tandemservice.movestudent.entity;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.movestudent.entity.gen.StudentModularOrderGen;

public class StudentModularOrder extends StudentModularOrderGen
{
    @Override
    public String getTitle()
    {
        return "Сборный приказ" + (getNumber() == null ? "" : " №" + getNumber()) + (getCommitDate() == null ? "" : " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCommitDate()));
    }

    @Override
    public ICatalogItem getType()
    {
        return null;
    }
}