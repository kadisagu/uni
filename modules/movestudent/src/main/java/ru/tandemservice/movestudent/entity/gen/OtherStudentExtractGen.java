package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.OtherStudentExtract;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка (прочие приказы по студентам)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OtherStudentExtractGen extends AbstractStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.OtherStudentExtract";
    public static final String ENTITY_NAME = "otherStudentExtract";
    public static final int VERSION_HASH = 23808146;
    private static IEntityMeta ENTITY_META;

    public static final String L_REASON = "reason";
    public static final String P_REASON_COMMENT = "reasonComment";
    public static final String L_COURSE = "course";
    public static final String L_TERM = "term";
    public static final String P_INSTITUTE = "institute";
    public static final String P_FACULTY = "faculty";
    public static final String P_SPECIALITY = "speciality";
    public static final String P_PROFILE = "profile";
    public static final String P_DOP_EDU_LEVEL = "dopEduLevel";
    public static final String P_ENCOURAGELTY = "encouragelty";
    public static final String P_ENCOURAGELTY_END_DATE = "encourageltyEndDate";
    public static final String P_GRANT_SIZE = "grantSize";
    public static final String P_GRANT_CATEGORY = "grantCategory";
    public static final String P_PRACTICE_TYPE = "practiceType";
    public static final String P_PRACTICE_PLACE = "practicePlace";
    public static final String P_PRACTICE_OWNERSHIP_TYPE = "practiceOwnershipType";
    public static final String P_PRACTICE_SETTLEMENT = "practiceSettlement";
    public static final String P_PRACTICE_PERIOD = "practicePeriod";
    public static final String P_PRACTICE_SUPERVISOR = "practiceSupervisor";

    private StudentOrderReasons _reason;     // Причина выписки
    private String _reasonComment;     // Примечание к причине
    private Course _course;     // Курс
    private Term _term;     // Семестр
    private String _institute;     // Институт
    private String _faculty;     // Факультет/Школа
    private String _speciality;     // Направление (специальность)
    private String _profile;     // Профиль (специализация)
    private String _dopEduLevel;     // Дополнительная образовательная программа / Дополнительная квалификация
    private String _encouragelty;     // Поощрение / взыскание
    private Date _encourageltyEndDate;     // Дата снятия поощрения / взыскания
    private String _grantSize;     // Размер стипендии/выплаты
    private String _grantCategory;     // Категория стипендии/выплаты
    private String _practiceType;     // Вид практики
    private String _practicePlace;     // Место прохождения практики
    private String _practiceOwnershipType;     // Форма собственности предприятия прохождения практики
    private String _practiceSettlement;     // Город прохождения практики
    private String _practicePeriod;     // Сроки практики
    private String _practiceSupervisor;     // Руководитель от вуза

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Причина выписки.
     */
    public StudentOrderReasons getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина выписки.
     */
    public void setReason(StudentOrderReasons reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Примечание к причине.
     */
    @Length(max=255)
    public String getReasonComment()
    {
        return _reasonComment;
    }

    /**
     * @param reasonComment Примечание к причине.
     */
    public void setReasonComment(String reasonComment)
    {
        dirty(_reasonComment, reasonComment);
        _reasonComment = reasonComment;
    }

    /**
     * @return Курс.
     */
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Семестр.
     */
    public Term getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр.
     */
    public void setTerm(Term term)
    {
        dirty(_term, term);
        _term = term;
    }

    /**
     * @return Институт.
     */
    @Length(max=255)
    public String getInstitute()
    {
        return _institute;
    }

    /**
     * @param institute Институт.
     */
    public void setInstitute(String institute)
    {
        dirty(_institute, institute);
        _institute = institute;
    }

    /**
     * @return Факультет/Школа.
     */
    @Length(max=255)
    public String getFaculty()
    {
        return _faculty;
    }

    /**
     * @param faculty Факультет/Школа.
     */
    public void setFaculty(String faculty)
    {
        dirty(_faculty, faculty);
        _faculty = faculty;
    }

    /**
     * @return Направление (специальность).
     */
    @Length(max=255)
    public String getSpeciality()
    {
        return _speciality;
    }

    /**
     * @param speciality Направление (специальность).
     */
    public void setSpeciality(String speciality)
    {
        dirty(_speciality, speciality);
        _speciality = speciality;
    }

    /**
     * @return Профиль (специализация).
     */
    @Length(max=255)
    public String getProfile()
    {
        return _profile;
    }

    /**
     * @param profile Профиль (специализация).
     */
    public void setProfile(String profile)
    {
        dirty(_profile, profile);
        _profile = profile;
    }

    /**
     * @return Дополнительная образовательная программа / Дополнительная квалификация.
     */
    @Length(max=255)
    public String getDopEduLevel()
    {
        return _dopEduLevel;
    }

    /**
     * @param dopEduLevel Дополнительная образовательная программа / Дополнительная квалификация.
     */
    public void setDopEduLevel(String dopEduLevel)
    {
        dirty(_dopEduLevel, dopEduLevel);
        _dopEduLevel = dopEduLevel;
    }

    /**
     * @return Поощрение / взыскание.
     */
    @Length(max=255)
    public String getEncouragelty()
    {
        return _encouragelty;
    }

    /**
     * @param encouragelty Поощрение / взыскание.
     */
    public void setEncouragelty(String encouragelty)
    {
        dirty(_encouragelty, encouragelty);
        _encouragelty = encouragelty;
    }

    /**
     * @return Дата снятия поощрения / взыскания.
     */
    public Date getEncourageltyEndDate()
    {
        return _encourageltyEndDate;
    }

    /**
     * @param encourageltyEndDate Дата снятия поощрения / взыскания.
     */
    public void setEncourageltyEndDate(Date encourageltyEndDate)
    {
        dirty(_encourageltyEndDate, encourageltyEndDate);
        _encourageltyEndDate = encourageltyEndDate;
    }

    /**
     * @return Размер стипендии/выплаты.
     */
    @Length(max=255)
    public String getGrantSize()
    {
        return _grantSize;
    }

    /**
     * @param grantSize Размер стипендии/выплаты.
     */
    public void setGrantSize(String grantSize)
    {
        dirty(_grantSize, grantSize);
        _grantSize = grantSize;
    }

    /**
     * @return Категория стипендии/выплаты.
     */
    @Length(max=255)
    public String getGrantCategory()
    {
        return _grantCategory;
    }

    /**
     * @param grantCategory Категория стипендии/выплаты.
     */
    public void setGrantCategory(String grantCategory)
    {
        dirty(_grantCategory, grantCategory);
        _grantCategory = grantCategory;
    }

    /**
     * @return Вид практики.
     */
    @Length(max=255)
    public String getPracticeType()
    {
        return _practiceType;
    }

    /**
     * @param practiceType Вид практики.
     */
    public void setPracticeType(String practiceType)
    {
        dirty(_practiceType, practiceType);
        _practiceType = practiceType;
    }

    /**
     * @return Место прохождения практики.
     */
    @Length(max=255)
    public String getPracticePlace()
    {
        return _practicePlace;
    }

    /**
     * @param practicePlace Место прохождения практики.
     */
    public void setPracticePlace(String practicePlace)
    {
        dirty(_practicePlace, practicePlace);
        _practicePlace = practicePlace;
    }

    /**
     * @return Форма собственности предприятия прохождения практики.
     */
    @Length(max=255)
    public String getPracticeOwnershipType()
    {
        return _practiceOwnershipType;
    }

    /**
     * @param practiceOwnershipType Форма собственности предприятия прохождения практики.
     */
    public void setPracticeOwnershipType(String practiceOwnershipType)
    {
        dirty(_practiceOwnershipType, practiceOwnershipType);
        _practiceOwnershipType = practiceOwnershipType;
    }

    /**
     * @return Город прохождения практики.
     */
    @Length(max=255)
    public String getPracticeSettlement()
    {
        return _practiceSettlement;
    }

    /**
     * @param practiceSettlement Город прохождения практики.
     */
    public void setPracticeSettlement(String practiceSettlement)
    {
        dirty(_practiceSettlement, practiceSettlement);
        _practiceSettlement = practiceSettlement;
    }

    /**
     * @return Сроки практики.
     */
    @Length(max=255)
    public String getPracticePeriod()
    {
        return _practicePeriod;
    }

    /**
     * @param practicePeriod Сроки практики.
     */
    public void setPracticePeriod(String practicePeriod)
    {
        dirty(_practicePeriod, practicePeriod);
        _practicePeriod = practicePeriod;
    }

    /**
     * @return Руководитель от вуза.
     */
    @Length(max=255)
    public String getPracticeSupervisor()
    {
        return _practiceSupervisor;
    }

    /**
     * @param practiceSupervisor Руководитель от вуза.
     */
    public void setPracticeSupervisor(String practiceSupervisor)
    {
        dirty(_practiceSupervisor, practiceSupervisor);
        _practiceSupervisor = practiceSupervisor;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof OtherStudentExtractGen)
        {
            setReason(((OtherStudentExtract)another).getReason());
            setReasonComment(((OtherStudentExtract)another).getReasonComment());
            setCourse(((OtherStudentExtract)another).getCourse());
            setTerm(((OtherStudentExtract)another).getTerm());
            setInstitute(((OtherStudentExtract)another).getInstitute());
            setFaculty(((OtherStudentExtract)another).getFaculty());
            setSpeciality(((OtherStudentExtract)another).getSpeciality());
            setProfile(((OtherStudentExtract)another).getProfile());
            setDopEduLevel(((OtherStudentExtract)another).getDopEduLevel());
            setEncouragelty(((OtherStudentExtract)another).getEncouragelty());
            setEncourageltyEndDate(((OtherStudentExtract)another).getEncourageltyEndDate());
            setGrantSize(((OtherStudentExtract)another).getGrantSize());
            setGrantCategory(((OtherStudentExtract)another).getGrantCategory());
            setPracticeType(((OtherStudentExtract)another).getPracticeType());
            setPracticePlace(((OtherStudentExtract)another).getPracticePlace());
            setPracticeOwnershipType(((OtherStudentExtract)another).getPracticeOwnershipType());
            setPracticeSettlement(((OtherStudentExtract)another).getPracticeSettlement());
            setPracticePeriod(((OtherStudentExtract)another).getPracticePeriod());
            setPracticeSupervisor(((OtherStudentExtract)another).getPracticeSupervisor());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OtherStudentExtractGen> extends AbstractStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OtherStudentExtract.class;
        }

        public T newInstance()
        {
            return (T) new OtherStudentExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                    return obj.getReason();
                case "reasonComment":
                    return obj.getReasonComment();
                case "course":
                    return obj.getCourse();
                case "term":
                    return obj.getTerm();
                case "institute":
                    return obj.getInstitute();
                case "faculty":
                    return obj.getFaculty();
                case "speciality":
                    return obj.getSpeciality();
                case "profile":
                    return obj.getProfile();
                case "dopEduLevel":
                    return obj.getDopEduLevel();
                case "encouragelty":
                    return obj.getEncouragelty();
                case "encourageltyEndDate":
                    return obj.getEncourageltyEndDate();
                case "grantSize":
                    return obj.getGrantSize();
                case "grantCategory":
                    return obj.getGrantCategory();
                case "practiceType":
                    return obj.getPracticeType();
                case "practicePlace":
                    return obj.getPracticePlace();
                case "practiceOwnershipType":
                    return obj.getPracticeOwnershipType();
                case "practiceSettlement":
                    return obj.getPracticeSettlement();
                case "practicePeriod":
                    return obj.getPracticePeriod();
                case "practiceSupervisor":
                    return obj.getPracticeSupervisor();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "reason":
                    obj.setReason((StudentOrderReasons) value);
                    return;
                case "reasonComment":
                    obj.setReasonComment((String) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "term":
                    obj.setTerm((Term) value);
                    return;
                case "institute":
                    obj.setInstitute((String) value);
                    return;
                case "faculty":
                    obj.setFaculty((String) value);
                    return;
                case "speciality":
                    obj.setSpeciality((String) value);
                    return;
                case "profile":
                    obj.setProfile((String) value);
                    return;
                case "dopEduLevel":
                    obj.setDopEduLevel((String) value);
                    return;
                case "encouragelty":
                    obj.setEncouragelty((String) value);
                    return;
                case "encourageltyEndDate":
                    obj.setEncourageltyEndDate((Date) value);
                    return;
                case "grantSize":
                    obj.setGrantSize((String) value);
                    return;
                case "grantCategory":
                    obj.setGrantCategory((String) value);
                    return;
                case "practiceType":
                    obj.setPracticeType((String) value);
                    return;
                case "practicePlace":
                    obj.setPracticePlace((String) value);
                    return;
                case "practiceOwnershipType":
                    obj.setPracticeOwnershipType((String) value);
                    return;
                case "practiceSettlement":
                    obj.setPracticeSettlement((String) value);
                    return;
                case "practicePeriod":
                    obj.setPracticePeriod((String) value);
                    return;
                case "practiceSupervisor":
                    obj.setPracticeSupervisor((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                        return true;
                case "reasonComment":
                        return true;
                case "course":
                        return true;
                case "term":
                        return true;
                case "institute":
                        return true;
                case "faculty":
                        return true;
                case "speciality":
                        return true;
                case "profile":
                        return true;
                case "dopEduLevel":
                        return true;
                case "encouragelty":
                        return true;
                case "encourageltyEndDate":
                        return true;
                case "grantSize":
                        return true;
                case "grantCategory":
                        return true;
                case "practiceType":
                        return true;
                case "practicePlace":
                        return true;
                case "practiceOwnershipType":
                        return true;
                case "practiceSettlement":
                        return true;
                case "practicePeriod":
                        return true;
                case "practiceSupervisor":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                    return true;
                case "reasonComment":
                    return true;
                case "course":
                    return true;
                case "term":
                    return true;
                case "institute":
                    return true;
                case "faculty":
                    return true;
                case "speciality":
                    return true;
                case "profile":
                    return true;
                case "dopEduLevel":
                    return true;
                case "encouragelty":
                    return true;
                case "encourageltyEndDate":
                    return true;
                case "grantSize":
                    return true;
                case "grantCategory":
                    return true;
                case "practiceType":
                    return true;
                case "practicePlace":
                    return true;
                case "practiceOwnershipType":
                    return true;
                case "practiceSettlement":
                    return true;
                case "practicePeriod":
                    return true;
                case "practiceSupervisor":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                    return StudentOrderReasons.class;
                case "reasonComment":
                    return String.class;
                case "course":
                    return Course.class;
                case "term":
                    return Term.class;
                case "institute":
                    return String.class;
                case "faculty":
                    return String.class;
                case "speciality":
                    return String.class;
                case "profile":
                    return String.class;
                case "dopEduLevel":
                    return String.class;
                case "encouragelty":
                    return String.class;
                case "encourageltyEndDate":
                    return Date.class;
                case "grantSize":
                    return String.class;
                case "grantCategory":
                    return String.class;
                case "practiceType":
                    return String.class;
                case "practicePlace":
                    return String.class;
                case "practiceOwnershipType":
                    return String.class;
                case "practiceSettlement":
                    return String.class;
                case "practicePeriod":
                    return String.class;
                case "practiceSupervisor":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OtherStudentExtract> _dslPath = new Path<OtherStudentExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OtherStudentExtract");
    }
            

    /**
     * @return Причина выписки.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getReason()
     */
    public static StudentOrderReasons.Path<StudentOrderReasons> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Примечание к причине.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getReasonComment()
     */
    public static PropertyPath<String> reasonComment()
    {
        return _dslPath.reasonComment();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Семестр.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getTerm()
     */
    public static Term.Path<Term> term()
    {
        return _dslPath.term();
    }

    /**
     * @return Институт.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getInstitute()
     */
    public static PropertyPath<String> institute()
    {
        return _dslPath.institute();
    }

    /**
     * @return Факультет/Школа.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getFaculty()
     */
    public static PropertyPath<String> faculty()
    {
        return _dslPath.faculty();
    }

    /**
     * @return Направление (специальность).
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getSpeciality()
     */
    public static PropertyPath<String> speciality()
    {
        return _dslPath.speciality();
    }

    /**
     * @return Профиль (специализация).
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getProfile()
     */
    public static PropertyPath<String> profile()
    {
        return _dslPath.profile();
    }

    /**
     * @return Дополнительная образовательная программа / Дополнительная квалификация.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getDopEduLevel()
     */
    public static PropertyPath<String> dopEduLevel()
    {
        return _dslPath.dopEduLevel();
    }

    /**
     * @return Поощрение / взыскание.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getEncouragelty()
     */
    public static PropertyPath<String> encouragelty()
    {
        return _dslPath.encouragelty();
    }

    /**
     * @return Дата снятия поощрения / взыскания.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getEncourageltyEndDate()
     */
    public static PropertyPath<Date> encourageltyEndDate()
    {
        return _dslPath.encourageltyEndDate();
    }

    /**
     * @return Размер стипендии/выплаты.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getGrantSize()
     */
    public static PropertyPath<String> grantSize()
    {
        return _dslPath.grantSize();
    }

    /**
     * @return Категория стипендии/выплаты.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getGrantCategory()
     */
    public static PropertyPath<String> grantCategory()
    {
        return _dslPath.grantCategory();
    }

    /**
     * @return Вид практики.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getPracticeType()
     */
    public static PropertyPath<String> practiceType()
    {
        return _dslPath.practiceType();
    }

    /**
     * @return Место прохождения практики.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getPracticePlace()
     */
    public static PropertyPath<String> practicePlace()
    {
        return _dslPath.practicePlace();
    }

    /**
     * @return Форма собственности предприятия прохождения практики.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getPracticeOwnershipType()
     */
    public static PropertyPath<String> practiceOwnershipType()
    {
        return _dslPath.practiceOwnershipType();
    }

    /**
     * @return Город прохождения практики.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getPracticeSettlement()
     */
    public static PropertyPath<String> practiceSettlement()
    {
        return _dslPath.practiceSettlement();
    }

    /**
     * @return Сроки практики.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getPracticePeriod()
     */
    public static PropertyPath<String> practicePeriod()
    {
        return _dslPath.practicePeriod();
    }

    /**
     * @return Руководитель от вуза.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getPracticeSupervisor()
     */
    public static PropertyPath<String> practiceSupervisor()
    {
        return _dslPath.practiceSupervisor();
    }

    public static class Path<E extends OtherStudentExtract> extends AbstractStudentExtract.Path<E>
    {
        private StudentOrderReasons.Path<StudentOrderReasons> _reason;
        private PropertyPath<String> _reasonComment;
        private Course.Path<Course> _course;
        private Term.Path<Term> _term;
        private PropertyPath<String> _institute;
        private PropertyPath<String> _faculty;
        private PropertyPath<String> _speciality;
        private PropertyPath<String> _profile;
        private PropertyPath<String> _dopEduLevel;
        private PropertyPath<String> _encouragelty;
        private PropertyPath<Date> _encourageltyEndDate;
        private PropertyPath<String> _grantSize;
        private PropertyPath<String> _grantCategory;
        private PropertyPath<String> _practiceType;
        private PropertyPath<String> _practicePlace;
        private PropertyPath<String> _practiceOwnershipType;
        private PropertyPath<String> _practiceSettlement;
        private PropertyPath<String> _practicePeriod;
        private PropertyPath<String> _practiceSupervisor;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Причина выписки.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getReason()
     */
        public StudentOrderReasons.Path<StudentOrderReasons> reason()
        {
            if(_reason == null )
                _reason = new StudentOrderReasons.Path<StudentOrderReasons>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Примечание к причине.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getReasonComment()
     */
        public PropertyPath<String> reasonComment()
        {
            if(_reasonComment == null )
                _reasonComment = new PropertyPath<String>(OtherStudentExtractGen.P_REASON_COMMENT, this);
            return _reasonComment;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Семестр.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getTerm()
     */
        public Term.Path<Term> term()
        {
            if(_term == null )
                _term = new Term.Path<Term>(L_TERM, this);
            return _term;
        }

    /**
     * @return Институт.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getInstitute()
     */
        public PropertyPath<String> institute()
        {
            if(_institute == null )
                _institute = new PropertyPath<String>(OtherStudentExtractGen.P_INSTITUTE, this);
            return _institute;
        }

    /**
     * @return Факультет/Школа.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getFaculty()
     */
        public PropertyPath<String> faculty()
        {
            if(_faculty == null )
                _faculty = new PropertyPath<String>(OtherStudentExtractGen.P_FACULTY, this);
            return _faculty;
        }

    /**
     * @return Направление (специальность).
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getSpeciality()
     */
        public PropertyPath<String> speciality()
        {
            if(_speciality == null )
                _speciality = new PropertyPath<String>(OtherStudentExtractGen.P_SPECIALITY, this);
            return _speciality;
        }

    /**
     * @return Профиль (специализация).
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getProfile()
     */
        public PropertyPath<String> profile()
        {
            if(_profile == null )
                _profile = new PropertyPath<String>(OtherStudentExtractGen.P_PROFILE, this);
            return _profile;
        }

    /**
     * @return Дополнительная образовательная программа / Дополнительная квалификация.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getDopEduLevel()
     */
        public PropertyPath<String> dopEduLevel()
        {
            if(_dopEduLevel == null )
                _dopEduLevel = new PropertyPath<String>(OtherStudentExtractGen.P_DOP_EDU_LEVEL, this);
            return _dopEduLevel;
        }

    /**
     * @return Поощрение / взыскание.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getEncouragelty()
     */
        public PropertyPath<String> encouragelty()
        {
            if(_encouragelty == null )
                _encouragelty = new PropertyPath<String>(OtherStudentExtractGen.P_ENCOURAGELTY, this);
            return _encouragelty;
        }

    /**
     * @return Дата снятия поощрения / взыскания.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getEncourageltyEndDate()
     */
        public PropertyPath<Date> encourageltyEndDate()
        {
            if(_encourageltyEndDate == null )
                _encourageltyEndDate = new PropertyPath<Date>(OtherStudentExtractGen.P_ENCOURAGELTY_END_DATE, this);
            return _encourageltyEndDate;
        }

    /**
     * @return Размер стипендии/выплаты.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getGrantSize()
     */
        public PropertyPath<String> grantSize()
        {
            if(_grantSize == null )
                _grantSize = new PropertyPath<String>(OtherStudentExtractGen.P_GRANT_SIZE, this);
            return _grantSize;
        }

    /**
     * @return Категория стипендии/выплаты.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getGrantCategory()
     */
        public PropertyPath<String> grantCategory()
        {
            if(_grantCategory == null )
                _grantCategory = new PropertyPath<String>(OtherStudentExtractGen.P_GRANT_CATEGORY, this);
            return _grantCategory;
        }

    /**
     * @return Вид практики.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getPracticeType()
     */
        public PropertyPath<String> practiceType()
        {
            if(_practiceType == null )
                _practiceType = new PropertyPath<String>(OtherStudentExtractGen.P_PRACTICE_TYPE, this);
            return _practiceType;
        }

    /**
     * @return Место прохождения практики.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getPracticePlace()
     */
        public PropertyPath<String> practicePlace()
        {
            if(_practicePlace == null )
                _practicePlace = new PropertyPath<String>(OtherStudentExtractGen.P_PRACTICE_PLACE, this);
            return _practicePlace;
        }

    /**
     * @return Форма собственности предприятия прохождения практики.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getPracticeOwnershipType()
     */
        public PropertyPath<String> practiceOwnershipType()
        {
            if(_practiceOwnershipType == null )
                _practiceOwnershipType = new PropertyPath<String>(OtherStudentExtractGen.P_PRACTICE_OWNERSHIP_TYPE, this);
            return _practiceOwnershipType;
        }

    /**
     * @return Город прохождения практики.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getPracticeSettlement()
     */
        public PropertyPath<String> practiceSettlement()
        {
            if(_practiceSettlement == null )
                _practiceSettlement = new PropertyPath<String>(OtherStudentExtractGen.P_PRACTICE_SETTLEMENT, this);
            return _practiceSettlement;
        }

    /**
     * @return Сроки практики.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getPracticePeriod()
     */
        public PropertyPath<String> practicePeriod()
        {
            if(_practicePeriod == null )
                _practicePeriod = new PropertyPath<String>(OtherStudentExtractGen.P_PRACTICE_PERIOD, this);
            return _practicePeriod;
        }

    /**
     * @return Руководитель от вуза.
     * @see ru.tandemservice.movestudent.entity.OtherStudentExtract#getPracticeSupervisor()
     */
        public PropertyPath<String> practiceSupervisor()
        {
            if(_practiceSupervisor == null )
                _practiceSupervisor = new PropertyPath<String>(OtherStudentExtractGen.P_PRACTICE_SUPERVISOR, this);
            return _practiceSupervisor;
        }

        public Class getEntityClass()
        {
            return OtherStudentExtract.class;
        }

        public String getEntityName()
        {
            return "otherStudentExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
