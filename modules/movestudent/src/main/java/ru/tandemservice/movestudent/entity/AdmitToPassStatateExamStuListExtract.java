package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.listextract.e45.utils.AdmitToPassSomething;
import ru.tandemservice.movestudent.entity.gen.*;

/** @see ru.tandemservice.movestudent.entity.gen.AdmitToPassStatateExamStuListExtractGen */
public class AdmitToPassStatateExamStuListExtract extends AdmitToPassStatateExamStuListExtractGen implements AdmitToPassSomething
{
    @Override
    public boolean isVKR()
    {
        return false;
    }
}