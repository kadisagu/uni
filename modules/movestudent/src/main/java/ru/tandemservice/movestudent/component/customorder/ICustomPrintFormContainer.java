/* $Id$ */
package ru.tandemservice.movestudent.component.customorder;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;

/**
 * @author Nikolay Fedorovskih
 * @since 12.10.2013
 */
public interface ICustomPrintFormContainer extends IEntity
{
    DatabaseFile getFile();

    void setFile(DatabaseFile file);

    byte[] getBasicTemplate();

    Long getModifiableOrderId();
}