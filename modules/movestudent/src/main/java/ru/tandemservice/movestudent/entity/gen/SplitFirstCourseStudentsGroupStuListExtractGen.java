package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О распределении студентов 1 курса в группы»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SplitFirstCourseStudentsGroupStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract";
    public static final String ENTITY_NAME = "splitFirstCourseStudentsGroupStuListExtract";
    public static final int VERSION_HASH = 218104851;
    private static IEntityMeta ENTITY_META;

    public static final String L_NEW_GROUP = "newGroup";
    public static final String L_EDUCATION_LEVELS_HIGH_SCHOOL = "educationLevelsHighSchool";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_DEVELOP_CONDITION = "developCondition";
    public static final String L_DEVELOP_TECH = "developTech";
    public static final String L_DEVELOP_PERIOD = "developPeriod";

    private Group _newGroup;     // Новая группа
    private EducationLevelsHighSchool _educationLevelsHighSchool;     // Направление подготовки
    private DevelopForm _developForm;     // Форма освоения
    private DevelopCondition _developCondition;     // Условие освоения
    private DevelopTech _developTech;     // Технология освоения
    private DevelopPeriod _developPeriod;     // Период освоения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     */
    @NotNull
    public Group getNewGroup()
    {
        return _newGroup;
    }

    /**
     * @param newGroup Новая группа. Свойство не может быть null.
     */
    public void setNewGroup(Group newGroup)
    {
        dirty(_newGroup, newGroup);
        _newGroup = newGroup;
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    /**
     * @param educationLevelsHighSchool Направление подготовки. Свойство не может быть null.
     */
    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        dirty(_educationLevelsHighSchool, educationLevelsHighSchool);
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения. Свойство не может быть null.
     */
    public void setDevelopCondition(DevelopCondition developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Технология освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения. Свойство не может быть null.
     */
    public void setDevelopTech(DevelopTech developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Период освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Период освоения. Свойство не может быть null.
     */
    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SplitFirstCourseStudentsGroupStuListExtractGen)
        {
            setNewGroup(((SplitFirstCourseStudentsGroupStuListExtract)another).getNewGroup());
            setEducationLevelsHighSchool(((SplitFirstCourseStudentsGroupStuListExtract)another).getEducationLevelsHighSchool());
            setDevelopForm(((SplitFirstCourseStudentsGroupStuListExtract)another).getDevelopForm());
            setDevelopCondition(((SplitFirstCourseStudentsGroupStuListExtract)another).getDevelopCondition());
            setDevelopTech(((SplitFirstCourseStudentsGroupStuListExtract)another).getDevelopTech());
            setDevelopPeriod(((SplitFirstCourseStudentsGroupStuListExtract)another).getDevelopPeriod());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SplitFirstCourseStudentsGroupStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SplitFirstCourseStudentsGroupStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new SplitFirstCourseStudentsGroupStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "newGroup":
                    return obj.getNewGroup();
                case "educationLevelsHighSchool":
                    return obj.getEducationLevelsHighSchool();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developTech":
                    return obj.getDevelopTech();
                case "developPeriod":
                    return obj.getDevelopPeriod();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "newGroup":
                    obj.setNewGroup((Group) value);
                    return;
                case "educationLevelsHighSchool":
                    obj.setEducationLevelsHighSchool((EducationLevelsHighSchool) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((DevelopCondition) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((DevelopTech) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((DevelopPeriod) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "newGroup":
                        return true;
                case "educationLevelsHighSchool":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "developTech":
                        return true;
                case "developPeriod":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "newGroup":
                    return true;
                case "educationLevelsHighSchool":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "developTech":
                    return true;
                case "developPeriod":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "newGroup":
                    return Group.class;
                case "educationLevelsHighSchool":
                    return EducationLevelsHighSchool.class;
                case "developForm":
                    return DevelopForm.class;
                case "developCondition":
                    return DevelopCondition.class;
                case "developTech":
                    return DevelopTech.class;
                case "developPeriod":
                    return DevelopPeriod.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SplitFirstCourseStudentsGroupStuListExtract> _dslPath = new Path<SplitFirstCourseStudentsGroupStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SplitFirstCourseStudentsGroupStuListExtract");
    }
            

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract#getNewGroup()
     */
    public static Group.Path<Group> newGroup()
    {
        return _dslPath.newGroup();
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract#getEducationLevelsHighSchool()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
    {
        return _dslPath.educationLevelsHighSchool();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract#getDevelopCondition()
     */
    public static DevelopCondition.Path<DevelopCondition> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract#getDevelopTech()
     */
    public static DevelopTech.Path<DevelopTech> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Период освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract#getDevelopPeriod()
     */
    public static DevelopPeriod.Path<DevelopPeriod> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    public static class Path<E extends SplitFirstCourseStudentsGroupStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Group.Path<Group> _newGroup;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _educationLevelsHighSchool;
        private DevelopForm.Path<DevelopForm> _developForm;
        private DevelopCondition.Path<DevelopCondition> _developCondition;
        private DevelopTech.Path<DevelopTech> _developTech;
        private DevelopPeriod.Path<DevelopPeriod> _developPeriod;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract#getNewGroup()
     */
        public Group.Path<Group> newGroup()
        {
            if(_newGroup == null )
                _newGroup = new Group.Path<Group>(L_NEW_GROUP, this);
            return _newGroup;
        }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract#getEducationLevelsHighSchool()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
        {
            if(_educationLevelsHighSchool == null )
                _educationLevelsHighSchool = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_EDUCATION_LEVELS_HIGH_SCHOOL, this);
            return _educationLevelsHighSchool;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract#getDevelopCondition()
     */
        public DevelopCondition.Path<DevelopCondition> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new DevelopCondition.Path<DevelopCondition>(L_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract#getDevelopTech()
     */
        public DevelopTech.Path<DevelopTech> developTech()
        {
            if(_developTech == null )
                _developTech = new DevelopTech.Path<DevelopTech>(L_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Период освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract#getDevelopPeriod()
     */
        public DevelopPeriod.Path<DevelopPeriod> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new DevelopPeriod.Path<DevelopPeriod>(L_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

        public Class getEntityClass()
        {
            return SplitFirstCourseStudentsGroupStuListExtract.class;
        }

        public String getEntityName()
        {
            return "splitFirstCourseStudentsGroupStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
