/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.catalog.movestudentTemplate.MovestudentTemplatePub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultPrintCatalogPub.DefaultPrintCatalogPubDAO;
import ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;

/**
 * @author AutoGenerator
 * Created on 13.01.2009
 */
public class DAO extends DefaultPrintCatalogPubDAO<MovestudentTemplate, Model> implements IDAO
{
    @Override
    protected void applyFilters(Model model, MQBuilder builder)
    {
        String title = (String) model.getSettings().get("title");
        builder.add(MQExpression.eq("ci", MovestudentTemplate.L_TYPE + "." + StudentExtractType.P_ACTIVE, Boolean.TRUE));
        if(!StringUtils.isEmpty(title))
        {
            builder.add(MQExpression.like("ci", MovestudentTemplate.P_TITLE, CoreStringUtils.escapeLike(title)));
        }
    }
}