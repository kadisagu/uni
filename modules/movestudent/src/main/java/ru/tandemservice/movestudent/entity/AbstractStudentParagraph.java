package ru.tandemservice.movestudent.entity;

import java.util.List;

import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.gen.AbstractStudentParagraphGen;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;

public abstract class AbstractStudentParagraph extends AbstractStudentParagraphGen implements IAbstractParagraph<AbstractStudentOrder>
{
    @Override
    public List<? extends IAbstractExtract> getExtractList()
    {
        return MoveStudentDaoFacade.getMoveStudentDao().getExtractListByParagraph(this.getId(), true);
    }

    @Override
    public int getExtractCount()
    {
        return UniDaoFacade.getCoreDao().getCount(AbstractStudentExtract.class, IAbstractExtract.L_PARAGRAPH, this);
    }

    public AbstractStudentExtract getFirstExtract()
    {
        return MoveStudentDaoFacade.getMoveStudentDao().getParagraphFirstExtract(getId(), false);
    }

    public StudentExtractType getAnyExtractType()
    {
        return MoveStudentDaoFacade.getMoveStudentDao().getAnyExtractTypeByParagraph(getId());
    }
}