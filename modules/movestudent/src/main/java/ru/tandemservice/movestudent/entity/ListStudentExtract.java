package ru.tandemservice.movestudent.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.entity.gen.ListStudentExtractGen;
import ru.tandemservice.unimove.IPrintableExtract;

/**
 * Абстрактная выписка из списочного приказа по студенту
 */
public abstract class ListStudentExtract extends ListStudentExtractGen implements IPrintableExtract
{
    @Override
    public String getTitle()
    {
        if (getEntity() == null) {
            return this.getClass().getSimpleName();
        }

        String studentWord = SexCodes.FEMALE.equals(getEntity().getPerson().getIdentityCard().getSex().getCode()) ? "студентка" : "студент";
        StudentListOrder order = (StudentListOrder) getParagraph().getOrder();

        StringBuilder title = new StringBuilder("Выписка «" + getType().getTitle() + "» №" + getNumber() + " из параграфа №" + getParagraph().getNumber());
        if (StringUtils.isNotEmpty(order.getNumber()))
            title.append(" приказа №").append(order.getNumber()).append(order.getCommitDate() == null ? "" : " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()));
        else if (order.getCommitDate() != null)
            title.append(" приказа от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()));

        title.append(" | ").append(studentWord).append(" ").append(getEntity().getPerson().getFullFio());

        return title.toString();
    }

    @Override
    public boolean canBePrinted()
    {
        return true;
    }

    @Override
    public void doExtractPrint(boolean printPdf)
    {
        if (canBePrinted())
            ContextLocal.createDesktop(new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_EXTRACT_PRINT, new ParametersMap().add("extractId", getId()).add("printPdf", printPdf)), null);
        else
            throw new ApplicationException("Выписка не может быть распечатана.");
    }

    @Override
    public void doOrderPrint(boolean printPdf)
    {
        ContextLocal.createDesktop(new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_ORDER_PRINT, new ParametersMap().add("orderId", getParagraph().getOrder().getId()).add("printPdf", printPdf)), null);
    }
}