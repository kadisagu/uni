/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e88;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ProfileAssignStuExtract;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 10.12.2012
 */
public class ProfileAssignStuExtractPrint implements IPrintFormCreator<ProfileAssignStuExtract>
{
    private static final Map<String, String[]> PROFILE_STRING_CASES_ARRAY = new HashMap<>();
    private static final List<String> PROFILE_QUALIFICATION_CODES = new ArrayList<>();

    static
    {
        PROFILE_QUALIFICATION_CODES.add(QualificationsCodes.BAKALAVR);
        PROFILE_QUALIFICATION_CODES.add(QualificationsCodes.MAGISTR);
        PROFILE_STRING_CASES_ARRAY.put(QualificationsCodes.BAKALAVR, CommonExtractPrint.PROFILE_CASES);
        PROFILE_STRING_CASES_ARRAY.put(QualificationsCodes.MAGISTR, CommonExtractPrint.MASTER_PROGRAM_CASES);
        PROFILE_STRING_CASES_ARRAY.put(QualificationsCodes.SPETSIALIST, CommonExtractPrint.SPECIALIZATION_CASES);
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, ProfileAssignStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(extract.getEducationOrgUnitOld().getEducationLevelHighSchool().getEducationLevel());

        if (null != qualification && PROFILE_QUALIFICATION_CODES.contains(qualification.getCode()))
        {
            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
                modifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), PROFILE_STRING_CASES_ARRAY.get(qualification.getCode())[i]);
        } else if (null != qualification)
        {
            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
                modifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), PROFILE_STRING_CASES_ARRAY.get(QualificationsCodes.SPETSIALIST)[i]);
        } else
        {
            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
                modifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), "");
        }

        CommonExtractPrint.initEducationType(modifier, extract.getEducationOrgUnitOld(), "DirectionOld", true, false);
        CommonExtractPrint.initEducationType(modifier, extract.getEducationOrgUnitNew(), "ProfileNew", false, true);

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}