/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e78;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.TransferCompTypeExtStuExtract;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.HashMap;
import java.util.Map;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 30.08.2012
 */
public class TransferCompTypeExtStuExtractPrint implements IPrintFormCreator<TransferCompTypeExtStuExtract>
{
    private static final Map<String, String[]> COMPENSATION_TYPE_STRING_CASES_ARRAY = new HashMap<>();

    static
    {
        COMPENSATION_TYPE_STRING_CASES_ARRAY.put(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET, new String[]{"вакантное бюджетное место", "вакантного бюджетного места", "вакантному бюджетному месту", "вакантное бюджетное место", "вакантным бюджетным местом", "вакантном бюджетном месте"});
        COMPENSATION_TYPE_STRING_CASES_ARRAY.put(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT, new String[]{"договорная основа", "договорной основы", "договорной основе", "договорную основу", "договорной основой", "договорной основе"});
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, TransferCompTypeExtStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("transferDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getTransferDate()));

        for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
        {
            modifier.put("compensationTypeStrAlt" + UniRtfUtil.CASE_POSTFIX.get(i), COMPENSATION_TYPE_STRING_CASES_ARRAY.get(extract.getCompensationTypeNew().getCode())[i]);
        }

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}