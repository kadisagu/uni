/* $Id$ */
package ru.tandemservice.movestudent.component.settings.OrderReasonsCommentSettings;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Dmitry Seleznev
 * @since 05.12.2012
 */
public interface IDAO extends IUniDao<Model>
{
    void updateCommentable(long reasonId);

    void updateRequired(long reasonId);
}