package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.ProlongWeekndChildStuExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О продлении отпуска по уходу за ребенком
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ProlongWeekndChildStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ProlongWeekndChildStuExtract";
    public static final String ENTITY_NAME = "prolongWeekndChildStuExtract";
    public static final int VERSION_HASH = -1377785926;
    private static IEntityMeta ENTITY_META;

    public static final String P_PROLONG_DATE_FROM = "prolongDateFrom";
    public static final String P_PROLONG_DATE_TO = "prolongDateTo";
    public static final String L_STUDENT_STATUS_NEW = "studentStatusNew";
    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";
    public static final String P_PREV_END_DATE = "prevEndDate";

    private Date _prolongDateFrom;     // Продлить отпуск с
    private Date _prolongDateTo;     // Продлить отпуск по
    private StudentStatus _studentStatusNew;     // Новое состояние студента
    private StudentStatus _studentStatusOld;     // Предыдущее состояние студента
    private Date _prevEndDate;     // Дата окончания отпуска по предыдущему приказу

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Продлить отпуск с. Свойство не может быть null.
     */
    @NotNull
    public Date getProlongDateFrom()
    {
        return _prolongDateFrom;
    }

    /**
     * @param prolongDateFrom Продлить отпуск с. Свойство не может быть null.
     */
    public void setProlongDateFrom(Date prolongDateFrom)
    {
        dirty(_prolongDateFrom, prolongDateFrom);
        _prolongDateFrom = prolongDateFrom;
    }

    /**
     * @return Продлить отпуск по. Свойство не может быть null.
     */
    @NotNull
    public Date getProlongDateTo()
    {
        return _prolongDateTo;
    }

    /**
     * @param prolongDateTo Продлить отпуск по. Свойство не может быть null.
     */
    public void setProlongDateTo(Date prolongDateTo)
    {
        dirty(_prolongDateTo, prolongDateTo);
        _prolongDateTo = prolongDateTo;
    }

    /**
     * @return Новое состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    /**
     * @param studentStatusNew Новое состояние студента. Свойство не может быть null.
     */
    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        dirty(_studentStatusNew, studentStatusNew);
        _studentStatusNew = studentStatusNew;
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Предыдущее состояние студента. Свойство не может быть null.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    /**
     * @return Дата окончания отпуска по предыдущему приказу.
     */
    public Date getPrevEndDate()
    {
        return _prevEndDate;
    }

    /**
     * @param prevEndDate Дата окончания отпуска по предыдущему приказу.
     */
    public void setPrevEndDate(Date prevEndDate)
    {
        dirty(_prevEndDate, prevEndDate);
        _prevEndDate = prevEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ProlongWeekndChildStuExtractGen)
        {
            setProlongDateFrom(((ProlongWeekndChildStuExtract)another).getProlongDateFrom());
            setProlongDateTo(((ProlongWeekndChildStuExtract)another).getProlongDateTo());
            setStudentStatusNew(((ProlongWeekndChildStuExtract)another).getStudentStatusNew());
            setStudentStatusOld(((ProlongWeekndChildStuExtract)another).getStudentStatusOld());
            setPrevEndDate(((ProlongWeekndChildStuExtract)another).getPrevEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ProlongWeekndChildStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ProlongWeekndChildStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new ProlongWeekndChildStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "prolongDateFrom":
                    return obj.getProlongDateFrom();
                case "prolongDateTo":
                    return obj.getProlongDateTo();
                case "studentStatusNew":
                    return obj.getStudentStatusNew();
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
                case "prevEndDate":
                    return obj.getPrevEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "prolongDateFrom":
                    obj.setProlongDateFrom((Date) value);
                    return;
                case "prolongDateTo":
                    obj.setProlongDateTo((Date) value);
                    return;
                case "studentStatusNew":
                    obj.setStudentStatusNew((StudentStatus) value);
                    return;
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
                case "prevEndDate":
                    obj.setPrevEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "prolongDateFrom":
                        return true;
                case "prolongDateTo":
                        return true;
                case "studentStatusNew":
                        return true;
                case "studentStatusOld":
                        return true;
                case "prevEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "prolongDateFrom":
                    return true;
                case "prolongDateTo":
                    return true;
                case "studentStatusNew":
                    return true;
                case "studentStatusOld":
                    return true;
                case "prevEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "prolongDateFrom":
                    return Date.class;
                case "prolongDateTo":
                    return Date.class;
                case "studentStatusNew":
                    return StudentStatus.class;
                case "studentStatusOld":
                    return StudentStatus.class;
                case "prevEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ProlongWeekndChildStuExtract> _dslPath = new Path<ProlongWeekndChildStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ProlongWeekndChildStuExtract");
    }
            

    /**
     * @return Продлить отпуск с. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ProlongWeekndChildStuExtract#getProlongDateFrom()
     */
    public static PropertyPath<Date> prolongDateFrom()
    {
        return _dslPath.prolongDateFrom();
    }

    /**
     * @return Продлить отпуск по. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ProlongWeekndChildStuExtract#getProlongDateTo()
     */
    public static PropertyPath<Date> prolongDateTo()
    {
        return _dslPath.prolongDateTo();
    }

    /**
     * @return Новое состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ProlongWeekndChildStuExtract#getStudentStatusNew()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusNew()
    {
        return _dslPath.studentStatusNew();
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ProlongWeekndChildStuExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    /**
     * @return Дата окончания отпуска по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.ProlongWeekndChildStuExtract#getPrevEndDate()
     */
    public static PropertyPath<Date> prevEndDate()
    {
        return _dslPath.prevEndDate();
    }

    public static class Path<E extends ProlongWeekndChildStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _prolongDateFrom;
        private PropertyPath<Date> _prolongDateTo;
        private StudentStatus.Path<StudentStatus> _studentStatusNew;
        private StudentStatus.Path<StudentStatus> _studentStatusOld;
        private PropertyPath<Date> _prevEndDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Продлить отпуск с. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ProlongWeekndChildStuExtract#getProlongDateFrom()
     */
        public PropertyPath<Date> prolongDateFrom()
        {
            if(_prolongDateFrom == null )
                _prolongDateFrom = new PropertyPath<Date>(ProlongWeekndChildStuExtractGen.P_PROLONG_DATE_FROM, this);
            return _prolongDateFrom;
        }

    /**
     * @return Продлить отпуск по. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ProlongWeekndChildStuExtract#getProlongDateTo()
     */
        public PropertyPath<Date> prolongDateTo()
        {
            if(_prolongDateTo == null )
                _prolongDateTo = new PropertyPath<Date>(ProlongWeekndChildStuExtractGen.P_PROLONG_DATE_TO, this);
            return _prolongDateTo;
        }

    /**
     * @return Новое состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ProlongWeekndChildStuExtract#getStudentStatusNew()
     */
        public StudentStatus.Path<StudentStatus> studentStatusNew()
        {
            if(_studentStatusNew == null )
                _studentStatusNew = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_NEW, this);
            return _studentStatusNew;
        }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ProlongWeekndChildStuExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

    /**
     * @return Дата окончания отпуска по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.ProlongWeekndChildStuExtract#getPrevEndDate()
     */
        public PropertyPath<Date> prevEndDate()
        {
            if(_prevEndDate == null )
                _prevEndDate = new PropertyPath<Date>(ProlongWeekndChildStuExtractGen.P_PREV_END_DATE, this);
            return _prevEndDate;
        }

        public Class getEntityClass()
        {
            return ProlongWeekndChildStuExtract.class;
        }

        public String getEntityName()
        {
            return "prolongWeekndChildStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
