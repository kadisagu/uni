package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.TransferProfileStuExtractGen;

import java.util.Date;

/**
 * О закреплении за специализацией (профилем) и перераспеределнии в группу
 */
public class TransferProfileStuExtract extends TransferProfileStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}