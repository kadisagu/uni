package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import ru.tandemservice.movestudent.entity.DegreeToExtractRelation;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь выписки и ученых степеней ППС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DegreeToExtractRelationGen extends EntityBase
 implements INaturalIdentifiable<DegreeToExtractRelationGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.DegreeToExtractRelation";
    public static final String ENTITY_NAME = "degreeToExtractRelation";
    public static final int VERSION_HASH = 1895590023;
    private static IEntityMeta ENTITY_META;

    public static final String L_DEGREE = "degree";
    public static final String L_EXTRACT = "extract";

    private ScienceDegree _degree;     // Ученая степень
    private ListStudentExtract _extract;     // Абстрактная выписка из списочного приказа по студенту

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Ученая степень. Свойство не может быть null.
     */
    @NotNull
    public ScienceDegree getDegree()
    {
        return _degree;
    }

    /**
     * @param degree Ученая степень. Свойство не может быть null.
     */
    public void setDegree(ScienceDegree degree)
    {
        dirty(_degree, degree);
        _degree = degree;
    }

    /**
     * @return Абстрактная выписка из списочного приказа по студенту. Свойство не может быть null.
     */
    @NotNull
    public ListStudentExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Абстрактная выписка из списочного приказа по студенту. Свойство не может быть null.
     */
    public void setExtract(ListStudentExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DegreeToExtractRelationGen)
        {
            if (withNaturalIdProperties)
            {
                setDegree(((DegreeToExtractRelation)another).getDegree());
                setExtract(((DegreeToExtractRelation)another).getExtract());
            }
        }
    }

    public INaturalId<DegreeToExtractRelationGen> getNaturalId()
    {
        return new NaturalId(getDegree(), getExtract());
    }

    public static class NaturalId extends NaturalIdBase<DegreeToExtractRelationGen>
    {
        private static final String PROXY_NAME = "DegreeToExtractRelationNaturalProxy";

        private Long _degree;
        private Long _extract;

        public NaturalId()
        {}

        public NaturalId(ScienceDegree degree, ListStudentExtract extract)
        {
            _degree = ((IEntity) degree).getId();
            _extract = ((IEntity) extract).getId();
        }

        public Long getDegree()
        {
            return _degree;
        }

        public void setDegree(Long degree)
        {
            _degree = degree;
        }

        public Long getExtract()
        {
            return _extract;
        }

        public void setExtract(Long extract)
        {
            _extract = extract;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof DegreeToExtractRelationGen.NaturalId) ) return false;

            DegreeToExtractRelationGen.NaturalId that = (NaturalId) o;

            if( !equals(getDegree(), that.getDegree()) ) return false;
            if( !equals(getExtract(), that.getExtract()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDegree());
            result = hashCode(result, getExtract());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDegree());
            sb.append("/");
            sb.append(getExtract());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DegreeToExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DegreeToExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new DegreeToExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "degree":
                    return obj.getDegree();
                case "extract":
                    return obj.getExtract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "degree":
                    obj.setDegree((ScienceDegree) value);
                    return;
                case "extract":
                    obj.setExtract((ListStudentExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "degree":
                        return true;
                case "extract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "degree":
                    return true;
                case "extract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "degree":
                    return ScienceDegree.class;
                case "extract":
                    return ListStudentExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DegreeToExtractRelation> _dslPath = new Path<DegreeToExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DegreeToExtractRelation");
    }
            

    /**
     * @return Ученая степень. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.DegreeToExtractRelation#getDegree()
     */
    public static ScienceDegree.Path<ScienceDegree> degree()
    {
        return _dslPath.degree();
    }

    /**
     * @return Абстрактная выписка из списочного приказа по студенту. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.DegreeToExtractRelation#getExtract()
     */
    public static ListStudentExtract.Path<ListStudentExtract> extract()
    {
        return _dslPath.extract();
    }

    public static class Path<E extends DegreeToExtractRelation> extends EntityPath<E>
    {
        private ScienceDegree.Path<ScienceDegree> _degree;
        private ListStudentExtract.Path<ListStudentExtract> _extract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Ученая степень. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.DegreeToExtractRelation#getDegree()
     */
        public ScienceDegree.Path<ScienceDegree> degree()
        {
            if(_degree == null )
                _degree = new ScienceDegree.Path<ScienceDegree>(L_DEGREE, this);
            return _degree;
        }

    /**
     * @return Абстрактная выписка из списочного приказа по студенту. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.DegreeToExtractRelation#getExtract()
     */
        public ListStudentExtract.Path<ListStudentExtract> extract()
        {
            if(_extract == null )
                _extract = new ListStudentExtract.Path<ListStudentExtract>(L_EXTRACT, this);
            return _extract;
        }

        public Class getEntityClass()
        {
            return DegreeToExtractRelation.class;
        }

        public String getEntityName()
        {
            return "degreeToExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
