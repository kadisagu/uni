package ru.tandemservice.movestudent.utils.system;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 8/29/12
 * Time: 5:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class StudentExcludeOrderData
{
    private String _dismissOrderNum;
    private Date _dismissOrderDate;
    private Date _dismissDate;
    private String _dismissOrderReason;

    public StudentExcludeOrderData(String excludeOrderNum, Date excludeOrderDate, Date excludeDate, String excludeOrderReason)
    {
        _dismissOrderNum = excludeOrderNum;
        _dismissOrderDate = excludeOrderDate;
        _dismissDate = excludeDate;
        _dismissOrderReason = excludeOrderReason;
    }

    public String getDismissOrderNum()
    {
        return _dismissOrderNum;
    }

    public void setDismissOrderNum(String dismissOrderNum)
    {
        _dismissOrderNum = dismissOrderNum;
    }

    public Date getDismissOrderDate()
    {
        return _dismissOrderDate;
    }

    public void setDismissOrderDate(Date dismissOrderDate)
    {
        _dismissOrderDate = dismissOrderDate;
    }

    public Date getDismissDate()
    {
        return _dismissDate;
    }

    public void setDismissDate(Date dismissDate)
    {
        _dismissDate = dismissDate;
    }

    public String getDismissOrderReason()
    {
        return _dismissOrderReason;
    }

    public void setDismissOrderReason(String dismissOrderReason)
    {
        _dismissOrderReason = dismissOrderReason;
    }
}
