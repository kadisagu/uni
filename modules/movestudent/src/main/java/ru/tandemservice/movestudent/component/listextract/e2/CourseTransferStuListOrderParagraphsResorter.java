/**
 * $Id$
 */
package ru.tandemservice.movestudent.component.listextract.e2;

import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.dao.IOrderParagraphsResorter;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 31.03.2010
 */
public class CourseTransferStuListOrderParagraphsResorter implements IOrderParagraphsResorter
{
    private class ListOrderComparator implements Comparator<StudentListParagraph>
    {
        private boolean _orderAsc;

        public ListOrderComparator()
        {
            _orderAsc = true;
        }

        public ListOrderComparator(boolean orderAsc)
        {
            _orderAsc = orderAsc;
        }

        @Override
        public int compare(StudentListParagraph paragraph1, StudentListParagraph paragraph2)
        {
            int cnt1 = 0;
            int cnt2 = 0;

            // FIXME это жесть. Нельзя в компараторе лезть в базу!

            List<Student> extrList1 = MoveStudentDaoFacade.getMoveStudentDao().getStudentsByParagraph(paragraph1.getId());
            List<Student> extrList2 = MoveStudentDaoFacade.getMoveStudentDao().getStudentsByParagraph(paragraph2.getId());

            Group group1 = extrList1.get(0).getGroup();
            while (group1 == null && extrList1.size() > ++cnt1)
                group1 = extrList1.get(cnt1).getGroup();

            Group group2 = extrList2.get(0).getGroup();
            while (group2 == null && extrList2.size() > ++cnt2)
                group2 = extrList2.get(cnt2).getGroup();

            if (null == group1 && null == group2) return 0;
            else if (null == group1) return -1;
            else if (null == group2) return 1;

            Integer grp1Course = group1.getCourse().getIntValue();
            Integer grp2Course = group2.getCourse().getIntValue();
            return _orderAsc ? grp1Course.compareTo(grp2Course) : grp2Course.compareTo(grp1Course);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<IAbstractParagraph<IAbstractOrder>> resortParagraphsBeforeCommit(IAbstractOrder order)
    {
        List list = order.getParagraphList();
        Collections.sort(list, new ListOrderComparator(false));
        return list;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<IAbstractParagraph<IAbstractOrder>> resortParagraphsBeforeRollback(IAbstractOrder order)
    {
        List list = order.getParagraphList();
        Collections.sort(list, new ListOrderComparator());
        return list;
    }
}