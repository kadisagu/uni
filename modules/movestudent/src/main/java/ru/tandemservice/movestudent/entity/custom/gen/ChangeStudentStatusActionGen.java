package ru.tandemservice.movestudent.entity.custom.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.custom.ChangeStudentStatusAction;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unimove.entity.ICustomOrderAction;
import ru.tandemservice.unimove.entity.ICustomOrderExtract;
import ru.tandemservice.unimove.entity.gen.ICustomOrderExtractGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Изменение состояния студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChangeStudentStatusActionGen extends EntityBase
 implements ICustomOrderAction{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.custom.ChangeStudentStatusAction";
    public static final String ENTITY_NAME = "changeStudentStatusAction";
    public static final int VERSION_HASH = 1115627166;
    private static IEntityMeta ENTITY_META;

    public static final String L_OLD_STATUS = "oldStatus";
    public static final String L_NEW_STATUS = "newStatus";
    public static final String L_CUSTOM_EXTRACT = "customExtract";

    private StudentStatus _oldStatus;     // Старое состояние студента
    private StudentStatus _newStatus;     // Новое состояние студента
    private ICustomOrderExtract _customExtract;     // Выписка из кастомного приказа, с набором действий, определенных пользователем

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Старое состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getOldStatus()
    {
        return _oldStatus;
    }

    /**
     * @param oldStatus Старое состояние студента. Свойство не может быть null.
     */
    public void setOldStatus(StudentStatus oldStatus)
    {
        dirty(_oldStatus, oldStatus);
        _oldStatus = oldStatus;
    }

    /**
     * @return Новое состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getNewStatus()
    {
        return _newStatus;
    }

    /**
     * @param newStatus Новое состояние студента. Свойство не может быть null.
     */
    public void setNewStatus(StudentStatus newStatus)
    {
        dirty(_newStatus, newStatus);
        _newStatus = newStatus;
    }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public ICustomOrderExtract getCustomExtract()
    {
        return _customExtract;
    }

    /**
     * @param customExtract Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     */
    public void setCustomExtract(ICustomOrderExtract customExtract)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && customExtract!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(ICustomOrderExtract.class);
            IEntityMeta actual =  customExtract instanceof IEntity ? EntityRuntime.getMeta((IEntity) customExtract) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_customExtract, customExtract);
        _customExtract = customExtract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ChangeStudentStatusActionGen)
        {
            setOldStatus(((ChangeStudentStatusAction)another).getOldStatus());
            setNewStatus(((ChangeStudentStatusAction)another).getNewStatus());
            setCustomExtract(((ChangeStudentStatusAction)another).getCustomExtract());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChangeStudentStatusActionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChangeStudentStatusAction.class;
        }

        public T newInstance()
        {
            return (T) new ChangeStudentStatusAction();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "oldStatus":
                    return obj.getOldStatus();
                case "newStatus":
                    return obj.getNewStatus();
                case "customExtract":
                    return obj.getCustomExtract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "oldStatus":
                    obj.setOldStatus((StudentStatus) value);
                    return;
                case "newStatus":
                    obj.setNewStatus((StudentStatus) value);
                    return;
                case "customExtract":
                    obj.setCustomExtract((ICustomOrderExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "oldStatus":
                        return true;
                case "newStatus":
                        return true;
                case "customExtract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "oldStatus":
                    return true;
                case "newStatus":
                    return true;
                case "customExtract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "oldStatus":
                    return StudentStatus.class;
                case "newStatus":
                    return StudentStatus.class;
                case "customExtract":
                    return ICustomOrderExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChangeStudentStatusAction> _dslPath = new Path<ChangeStudentStatusAction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChangeStudentStatusAction");
    }
            

    /**
     * @return Старое состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeStudentStatusAction#getOldStatus()
     */
    public static StudentStatus.Path<StudentStatus> oldStatus()
    {
        return _dslPath.oldStatus();
    }

    /**
     * @return Новое состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeStudentStatusAction#getNewStatus()
     */
    public static StudentStatus.Path<StudentStatus> newStatus()
    {
        return _dslPath.newStatus();
    }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeStudentStatusAction#getCustomExtract()
     */
    public static ICustomOrderExtractGen.Path<ICustomOrderExtract> customExtract()
    {
        return _dslPath.customExtract();
    }

    public static class Path<E extends ChangeStudentStatusAction> extends EntityPath<E>
    {
        private StudentStatus.Path<StudentStatus> _oldStatus;
        private StudentStatus.Path<StudentStatus> _newStatus;
        private ICustomOrderExtractGen.Path<ICustomOrderExtract> _customExtract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Старое состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeStudentStatusAction#getOldStatus()
     */
        public StudentStatus.Path<StudentStatus> oldStatus()
        {
            if(_oldStatus == null )
                _oldStatus = new StudentStatus.Path<StudentStatus>(L_OLD_STATUS, this);
            return _oldStatus;
        }

    /**
     * @return Новое состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeStudentStatusAction#getNewStatus()
     */
        public StudentStatus.Path<StudentStatus> newStatus()
        {
            if(_newStatus == null )
                _newStatus = new StudentStatus.Path<StudentStatus>(L_NEW_STATUS, this);
            return _newStatus;
        }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeStudentStatusAction#getCustomExtract()
     */
        public ICustomOrderExtractGen.Path<ICustomOrderExtract> customExtract()
        {
            if(_customExtract == null )
                _customExtract = new ICustomOrderExtractGen.Path<ICustomOrderExtract>(L_CUSTOM_EXTRACT, this);
            return _customExtract;
        }

        public Class getEntityClass()
        {
            return ChangeStudentStatusAction.class;
        }

        public String getEntityName()
        {
            return "changeStudentStatusAction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
