/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.customorder.listextract.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.custom.ModifyListStuExtract;
import ru.tandemservice.movestudent.entity.custom.OrderActionType;
import ru.tandemservice.unimove.entity.ICustomOrderAction;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 07.10.2013
 */
public class DAO extends AbstractListExtractPubDAO<ModifyListStuExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        if (model.getExtract().getOrderActionType() == OrderActionType.DO_CUSTOM_ACTION)
        {
            Map<String, String> compNames = new LinkedHashMap<>();
            for (Map.Entry<ICustomOrderAction, String> entry : MoveStudentDaoFacade.getMoveStudentDao().getCustomActions(model.getExtract().getId(), true).entrySet())
            {
                compNames.put(entry.getKey().getClass().getSimpleName(), entry.getValue());
            }
            model.setActionMap(compNames);
        }
        else
            model.setActionMap(Collections.<String, String>emptyMap());
    }
}
