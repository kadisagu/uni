/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e33;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.e33.utils.SocGrantAssignExtractWrapper;
import ru.tandemservice.movestudent.component.listextract.e33.utils.SocGrantAssignParagraphPartWrapper;
import ru.tandemservice.movestudent.component.listextract.e33.utils.SocGrantAssignParagraphWrapper;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 03.10.2012
 */
public class SocGrantAssignStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<SocGrantAssignStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, prepareParagraphsStructure(extracts));

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }

    protected List<SocGrantAssignParagraphWrapper> prepareParagraphsStructure(List<SocGrantAssignStuListExtract> extracts)
    {
        List<SocGrantAssignParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        int ind;
        for (SocGrantAssignStuListExtract extract : extracts)
        {
            Student student = extract.getEntity();
            Person person = student.getPerson();

            Group group = student.getGroup();
            EducationOrgUnit educationOrgUnit = group.getEducationOrgUnit();

            SocGrantAssignParagraphWrapper paragraphWrapper = new SocGrantAssignParagraphWrapper(extract.getCompensationType(), educationOrgUnit.getEducationLevelHighSchool().getEducationLevel(), educationOrgUnit.getDevelopForm(), educationOrgUnit.getDevelopCondition(), educationOrgUnit.getDevelopTech(), educationOrgUnit.getDevelopPeriod(), extract.getBeginDate(), extract.getEndDate(), educationOrgUnit.getFormativeOrgUnit(), educationOrgUnit.getTerritorialOrgUnit(), CommonListOrderPrint.getEducationBaseText(group), extract);

            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else
                paragraphWrapper = paragraphWrapperList.get(ind);

            SocGrantAssignParagraphPartWrapper paragraphPartWrapper = new SocGrantAssignParagraphPartWrapper(extract.getCourse(), extract.getGroup(), extract);

            ind = paragraphWrapper.getParagraphPartWrapperList().indexOf(paragraphPartWrapper);
            if (ind == -1)
                paragraphWrapper.getParagraphPartWrapperList().add(paragraphPartWrapper);
            else
                paragraphPartWrapper = paragraphWrapper.getParagraphPartWrapperList().get(ind);

            paragraphPartWrapper.getExtractWrapperList().add(new SocGrantAssignExtractWrapper(person, extract.getGrantSize()));
        }

        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<SocGrantAssignParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            int parNumber = 0;
            for (SocGrantAssignParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.SOC_GRANT_ASSIGN_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphWrapper.getFirstExtract();
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(firstExtract.getParagraph(), firstExtract);

                CommonExtractPrint.initDevelopForm(paragraphInjectModifier, paragraphWrapper.getDevelopForm(), "");
                CommonListExtractPrint.injectCompensationType(paragraphInjectModifier, paragraphWrapper.getCompensationType(), "", false);

                OrgUnit formativeOrgUnit = paragraphWrapper.getFormativeOrgUnit();
                OrgUnit territorialOrgUnit = paragraphWrapper.getTerritorialOrgUnit();
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, formativeOrgUnit, "", "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, paragraphWrapper.getEducationLevels(), formativeOrgUnit, territorialOrgUnit, "formativeOrgUnitStr", "");

                paragraphInjectModifier.put("dateBegin", DateFormatter.DEFAULT_DATE_FORMATTER.format(paragraphWrapper.getBeginDate()));
                paragraphInjectModifier.put("dateEnd", DateFormatter.DEFAULT_DATE_FORMATTER.format(paragraphWrapper.getEndDate()));

                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, paragraphWrapper.getEducationLevels());
                paragraphInjectModifier.put("parNumber", String.valueOf(++parNumber));
                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");
                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, paragraphWrapper.getDevelopCondition(), paragraphWrapper.getDevelopTech(), paragraphWrapper.getDevelopPeriod(), paragraphWrapper.getEduBaseText(), "fefuShortFastExtendedOptionalText");
                customModifier.listParagraphModifier(paragraphInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                paragraphInjectModifier.modify(paragraph);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getParagraphPartWrapperList(), paragraphWrapper.getDevelopForm());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraph, List<SocGrantAssignParagraphPartWrapper> paragraphPartWrappers, DevelopForm developForm)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartWrappers);
            List<IRtfElement> parList = new ArrayList<>();

            for (SocGrantAssignParagraphPartWrapper paragraphPartWrapper : paragraphPartWrappers)
            {
                // Получаем шаблон подпараграфа
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.SOC_GRANT_ASSIGN_LIST_EXTRACT), 3);
                RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphPartWrapper.getFirstExtract();
                RtfInjectModifier paragraphPartInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, firstExtract);

                paragraphPartInjectModifier.put("course", paragraphPartWrapper.getCourse().getTitle());
                paragraphPartInjectModifier.put("group", paragraphPartWrapper.getGroup().getTitle());
                CommonListOrderPrint.injectFefuGroupPar(paragraphPartInjectModifier, paragraphPartWrapper.getGroup(), developForm);
                customModifier.listParagraphModifier(paragraphPartInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                paragraphPartInjectModifier.modify(paragraphPart);

                RtfTableModifier paragraphPartTableModifier = new RtfTableModifier();
                List<SocGrantAssignExtractWrapper> extractWrapperList = paragraphPartWrapper.getExtractWrapperList();

                int j = 0;
                String[][] tableData = new String[extractWrapperList.size()][];
                for (SocGrantAssignExtractWrapper extractWrapper : extractWrapperList)
                {
                    tableData[j++] = new String[]{String.valueOf(j) + ".", extractWrapper.getPerson().getFullFio(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extractWrapper.getGrantSize()) + " руб."};
                }

                paragraphPartTableModifier.put("STUDENTS_TABLE", tableData);
                customModifier.listParagraphTableModifier(paragraphPartTableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                paragraphPartTableModifier.modify(paragraphPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}