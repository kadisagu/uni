/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e33.ParagraphAddEdit;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.SocGrantAssignStuListExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.dao.student.IAcademicGrantSizeDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.*;

/**
 * @author ListExtractComponentGenerator
 * @since 03.10.2012
 */
public class DAO extends AbstractListParagraphAddEditDAO<SocGrantAssignStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));

        if (model.getParagraphId() == null)
        {
            model.setCompensationType(get(CompensationType.class, CompensationType.P_CODE, CompensationTypeCodes.COMPENSATION_TYPE_BUDGET));
        }

        if (model.isEditForm())
        {
            SocGrantAssignStuListExtract extract = model.getFirstExtract();
            model.setCourse(extract.getCourse());
            model.setGroup(extract.getGroup());
            model.setCompensationType(extract.getCompensationType());
            model.setBeginDate(extract.getBeginDate());
            model.setEndDate(extract.getEndDate());
        }
    }


    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        if (model.getCompensationType() != null)
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));
    }

    @Override
    protected SocGrantAssignStuListExtract createNewInstance(Model model)
    {
        return new SocGrantAssignStuListExtract();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void fillExtract(SocGrantAssignStuListExtract extract, Student student, Model model)
    {
        IValueMapHolder<Double> grantSizeHolder = (IValueMapHolder<Double>) model.getDataSource().getColumn("grantSize");
        Map<Long, Double> grantSizeMap = (null == grantSizeHolder ? Collections.<Long, Double>emptyMap() : grantSizeHolder.getValueMap());

        Number grantSize = grantSizeMap.get(student.getId());

        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setCompensationType(model.getCompensationType());
        extract.setBeginDate(model.getBeginDate());
        extract.setEndDate(model.getEndDate());
        extract.setGrantSize(grantSize.doubleValue());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {

        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        IValueMapHolder<Double> finAidSizeHolder = (IValueMapHolder<Double>) model.getDataSource().getColumn("grantSize");
        Map<Long, Double> finAidSizeMap = (null == finAidSizeHolder ? Collections.<Long, Double>emptyMap() : finAidSizeHolder.getValueMap());

        List<ViewWrapper<Student>> viewlist = new ArrayList<ViewWrapper<Student>>((Collection) ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).getSelectedObjects());

        for (ViewWrapper<Student> wrapper : viewlist)
        {
            if (finAidSizeMap.get(wrapper.getEntity().getId()) == null)
                errorCollector.add("Поле «Размер стипендии» обязательно для заполнения.", "grantSizeId_" + wrapper.getEntity().getId());

        }

        if (!model.getBeginDate().before(model.getEndDate()))
            errorCollector.add("Дата окончания должна быть больше даты начала", "beginDate", "endDate");

        if (errorCollector.hasErrors())
            return;

        super.update(model);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(ru.tandemservice.movestudent.component.listextract.e33.ParagraphAddEdit.Model model)
    {
        super.prepareListDataSource(model);

        IValueMapHolder<Double> grantSizeHolder = (IValueMapHolder<Double>) model.getDataSource().getColumn("grantSize");
        Map<Long, Double> socialGrantSizeMap = (grantSizeHolder == null) ? Collections.<Long, Double>emptyMap() : grantSizeHolder.getValueMap();

        Integer grantSize = IAcademicGrantSizeDAO.instance.get().getDefaultSocialGrantSize();
        if (grantSize != null)
        {
            Double defSocialGrandSize = grantSize.doubleValue();
            for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
            {
                if (!socialGrantSizeMap.containsKey(wrapper.getId()))
                    socialGrantSizeMap.put(wrapper.getId(), defSocialGrandSize);
            }
        }
    }
}