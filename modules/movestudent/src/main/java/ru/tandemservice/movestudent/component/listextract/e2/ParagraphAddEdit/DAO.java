/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e2.ParagraphAddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.CourseTransferStuListExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 06.05.2009
 */
public class DAO extends AbstractListParagraphAddEditDAO<CourseTransferStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));
        model.setCourseOldListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getGroup() == null) return ListResult.getEmpty();

                MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "s", new String[]{Student.L_COURSE});
                builder.add(MQExpression.eq("s", Student.P_ARCHIVAL, Boolean.FALSE));
                builder.add(MQExpression.eq("s", Student.L_GROUP, model.getGroup()));
                builder.addOrder("s", Student.L_COURSE + "." + Course.P_CODE);
                builder.setNeedDistinct(true);
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });
        model.setCourseNewListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getGroup() == null || model.getCourseOld() == null) return ListResult.getEmpty();
                int minCourse = Integer.parseInt(model.getCourseOld().getCode());
                List<Course> courseList = new ArrayList<>(model.getCourseList());
                for (Iterator<Course> iter = courseList.iterator(); iter.hasNext();)
                    if (Integer.parseInt(iter.next().getCode()) <= minCourse)
                        iter.remove();
                return new ListResult<>(courseList);
            }
        });
        // заполняем поля по умолчанию
        if (model.getParagraphId() != null)
        {
            CourseTransferStuListExtract extract = model.getFirstExtract();
            model.setCourse(extract.getCourse());
            model.setGroup(extract.getGroup());
            model.setCourseOld(extract.getCourseOld());
            model.setCourseNew(extract.getCourseNew());
            model.setGroupChangeCourse(extract.isGroupChangeCourse());
            model.setTransferDate(extract.getTransferDate());
        }
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COURSE, model.getCourseOld()));
    }

    @Override
    protected CourseTransferStuListExtract createNewInstance(Model model)
    {
        return new CourseTransferStuListExtract();
    }

    @Override
    protected void fillExtract(CourseTransferStuListExtract extract, Student student, Model model)
    {
        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setCourseOld(model.getCourseOld());
        extract.setCourseNew(model.getCourseNew());
        extract.setGroupChangeCourse(model.isGroupChangeCourse());
        extract.setTransferDate(model.getTransferDate());
    }
}