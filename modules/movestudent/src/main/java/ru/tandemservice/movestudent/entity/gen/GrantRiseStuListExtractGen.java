package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.GrantRiseStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «Об однократном повышении государственной академической стипендии»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GrantRiseStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.GrantRiseStuListExtract";
    public static final String ENTITY_NAME = "grantRiseStuListExtract";
    public static final int VERSION_HASH = -1310064619;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_MONTH = "month";
    public static final String P_YEAR = "year";
    public static final String P_GRANT_RISE_AMOUNT = "grantRiseAmount";

    private Course _course;     // Курс
    private Group _group;     // Группа
    private CompensationType _compensationType;     // Вид возмещения затрат
    private int _month;     // Месяц
    private int _year;     // Год
    private double _grantRiseAmount;     // Размер повышения государственной академической стипендии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Месяц. Свойство не может быть null.
     */
    @NotNull
    public int getMonth()
    {
        return _month;
    }

    /**
     * @param month Месяц. Свойство не может быть null.
     */
    public void setMonth(int month)
    {
        dirty(_month, month);
        _month = month;
    }

    /**
     * @return Год. Свойство не может быть null.
     */
    @NotNull
    public int getYear()
    {
        return _year;
    }

    /**
     * @param year Год. Свойство не может быть null.
     */
    public void setYear(int year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Размер повышения государственной академической стипендии. Свойство не может быть null.
     */
    @NotNull
    public double getGrantRiseAmount()
    {
        return _grantRiseAmount;
    }

    /**
     * @param grantRiseAmount Размер повышения государственной академической стипендии. Свойство не может быть null.
     */
    public void setGrantRiseAmount(double grantRiseAmount)
    {
        dirty(_grantRiseAmount, grantRiseAmount);
        _grantRiseAmount = grantRiseAmount;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof GrantRiseStuListExtractGen)
        {
            setCourse(((GrantRiseStuListExtract)another).getCourse());
            setGroup(((GrantRiseStuListExtract)another).getGroup());
            setCompensationType(((GrantRiseStuListExtract)another).getCompensationType());
            setMonth(((GrantRiseStuListExtract)another).getMonth());
            setYear(((GrantRiseStuListExtract)another).getYear());
            setGrantRiseAmount(((GrantRiseStuListExtract)another).getGrantRiseAmount());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GrantRiseStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GrantRiseStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new GrantRiseStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "compensationType":
                    return obj.getCompensationType();
                case "month":
                    return obj.getMonth();
                case "year":
                    return obj.getYear();
                case "grantRiseAmount":
                    return obj.getGrantRiseAmount();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "month":
                    obj.setMonth((Integer) value);
                    return;
                case "year":
                    obj.setYear((Integer) value);
                    return;
                case "grantRiseAmount":
                    obj.setGrantRiseAmount((Double) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "group":
                        return true;
                case "compensationType":
                        return true;
                case "month":
                        return true;
                case "year":
                        return true;
                case "grantRiseAmount":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "group":
                    return true;
                case "compensationType":
                    return true;
                case "month":
                    return true;
                case "year":
                    return true;
                case "grantRiseAmount":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "compensationType":
                    return CompensationType.class;
                case "month":
                    return Integer.class;
                case "year":
                    return Integer.class;
                case "grantRiseAmount":
                    return Double.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GrantRiseStuListExtract> _dslPath = new Path<GrantRiseStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GrantRiseStuListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GrantRiseStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GrantRiseStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.movestudent.entity.GrantRiseStuListExtract#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Месяц. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GrantRiseStuListExtract#getMonth()
     */
    public static PropertyPath<Integer> month()
    {
        return _dslPath.month();
    }

    /**
     * @return Год. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GrantRiseStuListExtract#getYear()
     */
    public static PropertyPath<Integer> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Размер повышения государственной академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GrantRiseStuListExtract#getGrantRiseAmount()
     */
    public static PropertyPath<Double> grantRiseAmount()
    {
        return _dslPath.grantRiseAmount();
    }

    public static class Path<E extends GrantRiseStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<Integer> _month;
        private PropertyPath<Integer> _year;
        private PropertyPath<Double> _grantRiseAmount;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GrantRiseStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GrantRiseStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.movestudent.entity.GrantRiseStuListExtract#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Месяц. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GrantRiseStuListExtract#getMonth()
     */
        public PropertyPath<Integer> month()
        {
            if(_month == null )
                _month = new PropertyPath<Integer>(GrantRiseStuListExtractGen.P_MONTH, this);
            return _month;
        }

    /**
     * @return Год. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GrantRiseStuListExtract#getYear()
     */
        public PropertyPath<Integer> year()
        {
            if(_year == null )
                _year = new PropertyPath<Integer>(GrantRiseStuListExtractGen.P_YEAR, this);
            return _year;
        }

    /**
     * @return Размер повышения государственной академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GrantRiseStuListExtract#getGrantRiseAmount()
     */
        public PropertyPath<Double> grantRiseAmount()
        {
            if(_grantRiseAmount == null )
                _grantRiseAmount = new PropertyPath<Double>(GrantRiseStuListExtractGen.P_GRANT_RISE_AMOUNT, this);
            return _grantRiseAmount;
        }

        public Class getEntityClass()
        {
            return GrantRiseStuListExtract.class;
        }

        public String getEntityName()
        {
            return "grantRiseStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
