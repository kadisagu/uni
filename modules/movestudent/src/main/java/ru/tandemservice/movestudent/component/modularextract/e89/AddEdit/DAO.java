/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e89.AddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 10.12.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<WeekendPregnancyOutStuExtract, Model> implements IDAO
{
    @Override
    protected WeekendPregnancyOutStuExtract createNewInstance()
    {
        return new WeekendPregnancyOutStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        model.setGroupModel(new GroupSelectModel(model, model).extModel(model).showChildLevels(true));


        if(model.isAddForm())
        {
            if(null != model.getExtract().getEntity().getGroup())
            {
                model.getExtract().setGroupOld(model.getExtract().getEntity().getGroup());
            }
            model.setGroup(model.getExtract().getEntity().getGroup());
        }
        else
        {
            model.setGroup(model.getExtract().getGroupNew());
        }
    }

    @Override
    public void update(Model model)
    {
        if (!model.getExtract().isResumeGrantPay())
        {
            model.getExtract().setGrantSum(null);
            model.getExtract().setGrantPayBeginDate(null);
            model.getExtract().setGrantPayEndDate(null);
        }
        model.getExtract().setGroupOld(model.getExtract().getEntity().getGroup());
        model.getExtract().setGroupNew(model.getGroup());
        model.getExtract().setStudentStatusOld(model.getExtract().getEntity().getStatus());
        super.update(model);
    }

    @Override
    public void validate(Model model, ErrorCollector errorCollector)
    {
        super.validate(model, errorCollector);
        if (model.getExtract().isResumeGrantPay() && model.getExtract().getGrantPayBeginDate().after(model.getExtract().getGrantPayEndDate()))
            errorCollector.add("Дата окончания выплаты стипендии должна быть позже даты начала", "beginDate", "endDate");
    }
}