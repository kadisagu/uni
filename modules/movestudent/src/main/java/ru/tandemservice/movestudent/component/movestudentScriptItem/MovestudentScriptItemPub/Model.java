/* $Id$ */
package ru.tandemservice.movestudent.component.movestudentScriptItem.MovestudentScriptItemPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogPub.DefaultScriptCatalogPubModel;

/**
 * @author Vasily Zhukov
 * @since 22.12.2011
 */
public class Model<T extends ICatalogItem & IScriptItem> extends DefaultScriptCatalogPubModel<T>
{
}