/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 27.11.2013
 */
public abstract class BaseFlexFilter implements IFlexFilter
{
    private String _path;
    private String _name;
    private String _alias;

    public BaseFlexFilter(String name, String alias, String path)
    {
        _name = name;
        _path = path;
        _alias = alias;
    }

    @Override
    public String getAlias()
    {
        return _alias;
    }

    public String getPath()
    {
        return _path;
    }

    public String getName()
    {
        return _name;
    }

    protected abstract IDQLExpression internalCreateCondition(Object o);

    @Override
    public boolean addToBuilder(IDataSettings dataSettings, DQLSelectBuilder builder, IJoinCreator joinCreator)
    {
        Object o = dataSettings.get(_name);
        if (o != null && (!(o instanceof Collection) || !((Collection)o).isEmpty()))
        {
            joinCreator.checkAlias(_alias);
            builder.where(internalCreateCondition(o));
            return true;
        }
        return false;
    }
}