package ru.tandemservice.movestudent.entity;

import java.util.Date;

import ru.tandemservice.movestudent.entity.gen.AssignBenefitStuExtractGen;

/**
 * Выписка из сборного приказа по студенту. О назначении выплаты пособий
 */
public class AssignBenefitStuExtract extends AssignBenefitStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getBenefitBeginDate();
    }

    @Override
    public Date getEndDate()
    {
        return getBenefitEndDate();
    }
}