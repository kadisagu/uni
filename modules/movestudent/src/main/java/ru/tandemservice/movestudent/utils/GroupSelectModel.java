/* $Id$ */
package ru.tandemservice.movestudent.utils;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.IExtEducationLevelModel;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 21.11.2013
 */
public class GroupSelectModel extends BaseSelectModel implements ISingleSelectModel, IMultiSelectModel
{
    private OrgUnit _orgUnit;
    private IGroupModel _groupModel;
    private IEducationLevelModel _eduLevelModel;
    private IExtEducationLevelModel _extModel;
    private boolean _dependFromFUTS;
    private boolean _showChildLevels;
    private boolean _hideInactiveProgramSubject;

    public GroupSelectModel(IGroupModel groupModel, OrgUnit orgUnit)
    {
        this(null, groupModel, null, orgUnit);
    }

    public GroupSelectModel(IEducationLevelModel eduLevelModel, IGroupModel groupModel)
    {
        this(eduLevelModel, groupModel, null, null);
    }

    public GroupSelectModel(IEducationLevelModel eduLevelModel, IGroupModel groupModel, IExtEducationLevelModel extModel, OrgUnit orgUnit)
    {
        super();
        _eduLevelModel = eduLevelModel;
        _groupModel = groupModel;
        _extModel = extModel;
        _orgUnit = orgUnit;
    }

    public GroupSelectModel dependFromFUTS(boolean value)
    {
        _dependFromFUTS = value;
        return this;
    }

    public GroupSelectModel extModel(IExtEducationLevelModel extModel)
    {
        _extModel = extModel;
        return this;
    }

    public GroupSelectModel showChildLevels(boolean value)
    {
        _showChildLevels = value;
        return this;
    }

    public GroupSelectModel hideInactiveProgramSubject(boolean value)
    {
        _hideInactiveProgramSubject = value;
        return this;
    }

    protected static final String G_ALIAS = "g";

    protected DQLSelectBuilder createBuilder(String filter, Object o)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .column(G_ALIAS)
                .fromEntity(Group.class, G_ALIAS);

        if (o != null)
        {
            if (o instanceof Long)
                builder.where(eq(property(G_ALIAS + ".id"), value((Long) o)));
            else if (o instanceof Collection)
                builder.where(in(property(G_ALIAS + ".id"), (Collection) o));
        }

        if (_groupModel.getCourse() != null)
            builder.where(eq(property(G_ALIAS, Group.course()), value(_groupModel.getCourse())));

        if (_orgUnit != null)
        {
            builder.where(or(
                    eq(property(G_ALIAS, Group.educationOrgUnit().formativeOrgUnit()), value(_orgUnit)),
                    eq(property(G_ALIAS, Group.educationOrgUnit().territorialOrgUnit()), value(_orgUnit))
            ));
        }

        if (_eduLevelModel != null)
        {
            if (_eduLevelModel.getFormativeOrgUnit() != null)
                builder.where(eq(property(G_ALIAS, Group.educationOrgUnit().formativeOrgUnit()), value(_eduLevelModel.getFormativeOrgUnit())));

            if (_eduLevelModel.getTerritorialOrgUnit() != null)
                builder.where(eq(property(G_ALIAS, Group.educationOrgUnit().territorialOrgUnit()), value(_eduLevelModel.getTerritorialOrgUnit())));

            if (_dependFromFUTS)
            {
                builder
                        .where(eq(property(G_ALIAS, Group.educationOrgUnit().developForm()), value(_eduLevelModel.getDevelopForm())))
                        .where(eq(property(G_ALIAS, Group.educationOrgUnit().developCondition()), value(_eduLevelModel.getDevelopCondition())))
                        .where(eq(property(G_ALIAS, Group.educationOrgUnit().developTech()), value(_eduLevelModel.getDevelopTech())))
                        .where(eq(property(G_ALIAS, Group.educationOrgUnit().developPeriod()), value(_eduLevelModel.getDevelopPeriod())));
            }
        }

        if (_extModel != null)
        {
            EducationLevels parentLevel = _extModel.getParentEduLevel();
            if (parentLevel != null)
            {
                if (_showChildLevels)
                {
                    builder.where(or(
                            eq(property(G_ALIAS, Group.educationOrgUnit().educationLevelHighSchool().educationLevel()), value(parentLevel)),
                            eq(property(G_ALIAS, Group.educationOrgUnit().educationLevelHighSchool().educationLevel().parentLevel()), value(parentLevel))));
                }
                else
                    builder.where(eq(property(G_ALIAS, Group.educationOrgUnit().educationLevelHighSchool().educationLevel().parentLevel()), value(parentLevel)));
            }
        }

        builder.where(eq(property(G_ALIAS, Group.archival()), value(Boolean.FALSE)));

        if (StringUtils.isNotEmpty(filter))
            builder.where(likeUpper(property(G_ALIAS, Group.P_TITLE), value(CoreStringUtils.escapeLike(filter, true))));

        if (_hideInactiveProgramSubject)
            MoveStudentDaoFacade.getMoveStudentDao().addCustomConditionToGroupSelectBuilder(builder, G_ALIAS);

        builder.order(property(G_ALIAS, Group.P_TITLE), OrderDirection.asc);

        return builder;
    }

    protected boolean isNeedRequest()
    {
        if (_groupModel.getCourse() == null)
            return false;

        if (_orgUnit == null && _eduLevelModel == null)
            return true;

        if (_eduLevelModel != null && _dependFromFUTS && (_eduLevelModel.getDevelopForm() == null || _eduLevelModel.getDevelopCondition() == null || _eduLevelModel.getDevelopTech() == null || _eduLevelModel.getDevelopPeriod() == null))
            return false;

        return _orgUnit != null || _eduLevelModel.getFormativeOrgUnit() != null;
    }

    @Override
    public ListResult findValues(String filter)
    {
        return isNeedRequest() ? new DQLListResultBuilder(createBuilder(filter, null)).findOptions() :
                new SimpleListResultBuilder<>(Collections.<Group>emptyList()).findOptions();
    }

    @Override
    public Object getValue(Object primaryKey)
    {
        return isNeedRequest() ? new DQLListResultBuilder(createBuilder(null, primaryKey)).getSingleValue() : null;
    }

    @Override
    public List getValues(Set primaryKeys)
    {
        return isNeedRequest() ? new DQLListResultBuilder(createBuilder(null, primaryKeys)).getValues() : Collections.emptyList();
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public IGroupModel getGroupModel()
    {
        return _groupModel;
    }

    public IEducationLevelModel getEduLevelModel()
    {
        return _eduLevelModel;
    }

    public IExtEducationLevelModel getExtModel()
    {
        return _extModel;
    }
}
