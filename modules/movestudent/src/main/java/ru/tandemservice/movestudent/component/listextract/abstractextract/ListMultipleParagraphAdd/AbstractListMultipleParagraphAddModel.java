/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.abstractextract.ListMultipleParagraphAdd;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 10.06.2009
 */
@Input({
    @Bind(key = AbstractListMultipleParagraphAddModel.PARAMETER_ORDER_ID, binding = "orderId"),
    @Bind(key = AbstractListMultipleParagraphAddModel.PARAMETER_PARAGRAPH_TYPE_ID, binding = "paragraphType.id")
})
public abstract class AbstractListMultipleParagraphAddModel<T extends ListStudentExtract>
{
    public static final String PARAMETER_ORDER_ID = "orderId";
    public static final String PARAMETER_PARAGRAPH_TYPE_ID = "paragraphTypeId";

    private Long _orderId;
    private StudentListOrder _order;
    private StudentListParagraph _paragraph;
    private StudentExtractType _paragraphType = new StudentExtractType();
    private T _extract;
    private DynamicListDataSource<Group> _dataSource;
    private String _searchListSettingsKey;
    private List<IEntity> _selectedItemList;

    private List<StudentListParagraph> _addedParagraphs;

    // Getters & Setters

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public StudentListOrder getOrder()
    {
        return _order;
    }

    public void setOrder(StudentListOrder order)
    {
        this._order = order;
    }

    public StudentListParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(StudentListParagraph paragraph)
    {
        this._paragraph = paragraph;
    }

    public StudentExtractType getParagraphType()
    {
        return _paragraphType;
    }

    public void setParagraphType(StudentExtractType paragraphType)
    {
        _paragraphType = paragraphType;
    }

    public T getExtract()
    {
        return _extract;
    }

    public void setExtract(T extract)
    {
        this._extract = extract;
    }

    public DynamicListDataSource<Group> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<Group> dataSource)
    {
        _dataSource = dataSource;
    }

    public String getSearchListSettingsKey()
    {
        return _searchListSettingsKey;
    }

    public void setSearchListSettingsKey(String searchListSettingsKey)
    {
        _searchListSettingsKey = searchListSettingsKey;
    }

    public List<IEntity> getSelectedItemList()
    {
        return _selectedItemList;
    }

    public void setSelectedItemList(List<IEntity> selectedItemList)
    {
        _selectedItemList = selectedItemList;
    }

    public List<StudentListParagraph> getAddedParagraphs()
    {
        return _addedParagraphs;
    }

    public void setAddedParagraphs(List<StudentListParagraph> addedParagraphs)
    {
        this._addedParagraphs = addedParagraphs;
    }
}