/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e27.ParagraphAddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.AdmitToPassDiplomaWorkExtStuListExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author ListExtractComponentGenerator
 * @since 13.09.2012
 */
public class DAO extends AbstractListParagraphAddEditDAO<AdmitToPassDiplomaWorkExtStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionList(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));

        //заполняем поля сохраненными данными
        if (model.getParagraphId() != null)
        {
            AdmitToPassDiplomaWorkExtStuListExtract extract = model.getFirstExtract();
            model.setCourse(extract.getCourse());
            model.setGroup(extract.getGroup());
            model.setDevelopForm(extract.getDevelopForm());
            model.setDevelopCondition(extract.getDevelopCondition());
            model.setCompensationType(extract.getCompensationType());
        }
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopForm()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopCondition()));
        if (model.getCompensationType() != null)
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));

    }
    
    @Override
    protected AdmitToPassDiplomaWorkExtStuListExtract createNewInstance(Model model)
    {
        return new AdmitToPassDiplomaWorkExtStuListExtract();
    }

    @Override
    protected void fillExtract(AdmitToPassDiplomaWorkExtStuListExtract extract, Student student, Model model)
    {
        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setDevelopForm(model.getDevelopForm());
        extract.setDevelopCondition(model.getDevelopCondition());
        extract.setCompensationType(model.getCompensationType());
    }
}