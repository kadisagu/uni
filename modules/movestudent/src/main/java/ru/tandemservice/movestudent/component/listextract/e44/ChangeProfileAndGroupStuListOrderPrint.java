/*$Id$*/
package ru.tandemservice.movestudent.component.listextract.e44;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.e44.utils.ChangeProfileAndGroupParagraphWrapper;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ChangeProfileAndGroupStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2014
 */
public class ChangeProfileAndGroupStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

	    List<ChangeProfileAndGroupStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        List<ChangeProfileAndGroupParagraphWrapper> preparedParagraphsStructure = prepareParagraphsStructure(extracts);
	    if(!preparedParagraphsStructure.isEmpty())
	    {
		    Qualifications qualification = preparedParagraphsStructure.get(0).getQualification();
		    String qualificationCode = null != qualification ? qualification.getCode() : null;
		    if (null != qualificationCode && !ChangeProfileAndGroupStuListExtractPrint.PROCESSED_QUALIFICATION_CODES.contains(qualificationCode))
			    qualificationCode = null;

		    for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
		    {
			    injectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), ChangeProfileAndGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[i]);
			    injectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i) + "P", ChangeProfileAndGroupStuListExtractPrint.PROFILE_STRING_PLURAL_CASES_ARRAY.get(qualificationCode)[i]);
		    }
	    }
	    if(!extracts.isEmpty())
	    {
		    ChangeProfileAndGroupStuListExtract extract = extracts.get(0);
		    if (extract.isChangeEduPlan())
		    {
			    RtfString changeEduPlan = new RtfString();
			    changeEduPlan.append("Разницу в учебных планах ликвидировать в срок до ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getChangeEduPlanDeadlineDate())).append(".").par().par();
			    injectModifier.put("changeEduPlan", changeEduPlan);
		    }
		    else
		    {
			    injectModifier.put("changeEduPlan", "");
		    }

            injectModifier.put("transferDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getTransferOrOrderDate()));
	    }
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, preparedParagraphsStructure);

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }

    protected List<ChangeProfileAndGroupParagraphWrapper> prepareParagraphsStructure(List<ChangeProfileAndGroupStuListExtract> extracts)
    {
        int ind;
        List<ChangeProfileAndGroupParagraphWrapper> paragraphWrapperList = new ArrayList<>();

        if (extracts.isEmpty())
            return paragraphWrapperList;

        for (ChangeProfileAndGroupStuListExtract extract : extracts)
        {
            Group groupNew = extract.getGroupNew();
            EducationOrgUnit educationOrgUnitNew = extract.getEducationOrgUnitNew();
            EducationOrgUnit educationOrgUnitOld = extract.getEducationOrgUnitOld();

            EducationLevels eduLevelsNew = educationOrgUnitNew.getEducationLevelHighSchool().getEducationLevel();
            Qualifications qualification = eduLevelsNew.getQualification();

            if (null == qualification)
            {
                EducationLevels tmpLevel = eduLevelsNew.getParentLevel();
                while (null != tmpLevel && null == qualification)
                {
                    qualification = tmpLevel.getQualification();
                    tmpLevel = tmpLevel.getParentLevel();
                }
            }

            ChangeProfileAndGroupParagraphWrapper paragraphWrapper = new ChangeProfileAndGroupParagraphWrapper(
                    extract.getEntity().getCourse(),
                    extract.getEntity().getGroup(),
                    extract.getEntity().getCompensationType(),
                    educationOrgUnitOld.getEducationLevelHighSchool(),
                    educationOrgUnitOld.getFormativeOrgUnit(),
                    educationOrgUnitOld.getTerritorialOrgUnit(),
                    educationOrgUnitOld.getDevelopForm(),
                    educationOrgUnitOld.getDevelopCondition(),
                    educationOrgUnitOld.getDevelopTech(),
                    educationOrgUnitOld.getDevelopPeriod(),
                    qualification,
                    extract,
                    educationOrgUnitNew.getEducationLevelHighSchool().getEducationLevel(),
                    groupNew,
                    extract.getEntity().getStudentCategory());

            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else paragraphWrapper = paragraphWrapperList.get(ind);

            paragraphWrapper.getPersonList().add(extract.getEntity().getPerson());
        }
        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<ChangeProfileAndGroupParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            int parNumber = 0;
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (ChangeProfileAndGroupParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.TRANSFER_PROFILE_AND_REASSIGN_GROUP_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphWrapper.getFirstExtract();
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(firstExtract.getParagraph(), firstExtract);

                parNumber++;
                paragraphInjectModifier.put("parNumber", String.valueOf(parNumber));
                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");
                paragraphInjectModifier.put("paragraphNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");

                String code = paragraphWrapper.getDevelopForm().getCode();
                switch (code)
                {
                    case DevelopFormCodes.FULL_TIME_FORM:
                        paragraphInjectModifier.put("developForm_DF", "очной");
                        break;
                    case DevelopFormCodes.CORESP_FORM:
                        paragraphInjectModifier.put("developForm_DF", "заочной");
                        break;
                    case DevelopFormCodes.PART_TIME_FORM:
                        paragraphInjectModifier.put("developForm_DF", "очно-заочной");
                        break;
                    case DevelopFormCodes.EXTERNAL_FORM:
                        paragraphInjectModifier.put("developForm_DF", "экстернату");
                        break;
                    case DevelopFormCodes.APPLICANT_FORM:
                        paragraphInjectModifier.put("developForm_DF", "самостоятельному обучению и итоговой аттестации");
                        break;
                }

                Student firstStudent = firstExtract.getEntity();

                CommonListExtractPrint.injectStudentsCategory(paragraphInjectModifier, paragraphWrapper.getStudentCategory());
                CommonListExtractPrint.injectCompensationType(paragraphInjectModifier, firstStudent.getCompensationType(), "", false);
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstStudent.getEducationOrgUnit().getFormativeOrgUnit(), "", "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstStudent.getEducationOrgUnit(), "formativeOrgUnitStr", "");

                Qualifications qualification = paragraphWrapper.getQualification();
                String qualificationCode = null != qualification ? qualification.getCode() : null;
                if (null != qualificationCode && !ChangeProfileAndGroupStuListExtractPrint.PROCESSED_QUALIFICATION_CODES.contains(qualificationCode))
                {
                    qualificationCode = null;
                }

                for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
                {
                    paragraphInjectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), ChangeProfileAndGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[i]);
                    paragraphInjectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i) + "P", ChangeProfileAndGroupStuListExtractPrint.PROFILE_STRING_PLURAL_CASES_ARRAY.get(qualificationCode)[i]);
                }

                paragraphInjectModifier.put("course", paragraphWrapper.getCourse().getTitle());
	            CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, paragraphWrapper.getDevelopCondition(), paragraphWrapper.getDevelopTech(), paragraphWrapper.getDevelopPeriod(), CommonListOrderPrint.getEducationBaseText(paragraphWrapper.getGroup()), "fefuShortFastExtendedOptionalText");
                CommonExtractPrint.initFefuGroup(paragraphInjectModifier, "groupInternal_G", paragraphWrapper.getGroup(), paragraphWrapper.getDevelopForm(), " группы ");
                UniRtfUtil.initEducationType(paragraphInjectModifier, paragraphWrapper.getEducationLevels(), "");
	            CommonExtractPrint.initEducationType(paragraphInjectModifier, paragraphWrapper.getFirstExtract().getEducationOrgUnitOld(), "Old");

                paragraphInjectModifier.put("specializationNew", paragraphWrapper.getEducationLevelNew().getTitle());
	            paragraphInjectModifier.put("eduTypeString", paragraphWrapper.getEducationLevels().isSpeciality() ? "вышеуказанной специальности" : "вышеуказанному направлению подготовки");
                paragraphInjectModifier.put("groupNew", paragraphWrapper.getGroupNew().getTitle());

                paragraphInjectModifier.put("transferDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(paragraphWrapper.getFirstExtract().getTransferOrOrderDate()));

                // Получаем список студентов
                List<Person> personList = paragraphWrapper.getPersonList();
                personList.sort(Person.FULL_FIO_AND_ID_COMPARATOR);
                RtfString rtfString = new RtfString();

                int j = 0;
                for (; j < personList.size() - 1; j++)
                {
                    rtfString.append(IRtfData.TAB).append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio()).par();
                }
                // Вставляем список студентов
                rtfString.append(IRtfData.TAB).append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio());
                paragraphInjectModifier.put("STUDENT_LIST", rtfString);
                customModifier.listParagraphModifier(paragraphInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphInjectModifier.modify(paragraph);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraph);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);

            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}
