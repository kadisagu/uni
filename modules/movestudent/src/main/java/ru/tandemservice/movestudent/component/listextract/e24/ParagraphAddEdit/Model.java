/* $Id: Model.java.vm 7834 2009-05-06 09:39:03Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e24.ParagraphAddEdit;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 19.02.2011
 */
public class Model extends AbstractListParagraphAddEditModel<SetDiplomaWorkTopicAndScientificAdviserStuListExtract> implements IGroupModel
{
    private Course _course;
    private ISelectModel _groupListModel;
    private Group _group;
    private DevelopForm _developForm;
    private ISelectModel _developFormList;
    private DevelopCondition _developCondition;
    private ISelectModel _developConditionList;
    private CompensationType _compensationType;
    private List<CompensationType> _compensationTypeList;
    private String _diplomaWorkTopic;
    private ISelectModel _ppsListModel;
    private ISelectModel _degreeListModel;
    private ISelectModel _statusListModel;

    public boolean isDevelopConditionRequired()
    {
        return true;
    }

    public ISelectModel getStatusListModel() {
        return _statusListModel;
    }

    public void setStatusListModel(ISelectModel statusListModel) {
        _statusListModel = statusListModel;
    }

    public ISelectModel getDegreeListModel() {
        return _degreeListModel;
    }

    public void setDegreeListModel(ISelectModel degreeListModel) {
        _degreeListModel = degreeListModel;
    }

    public CompensationType getCompensationType() {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType) {
        _compensationType = compensationType;
    }

    public List<CompensationType> getCompensationTypeList() {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList) {
        _compensationTypeList = compensationTypeList;
    }

    public String getCurrentDiplomaWorkTopicId()
    {
        IEntity currentEntity = getDataSource().getCurrentEntity();
        return "topic_id_" + currentEntity.getId();
    }

    public String getCurrentNameId()
    {
        IEntity currentEntity = getDataSource().getCurrentEntity();
        return "name_id_" + currentEntity.getId();
    }

    public String getCurrentDegreeId()
    {
        IEntity currentEntity = getDataSource().getCurrentEntity();
        return "degree_id_" + currentEntity.getId();
    }

    public String getCurrentStatusId()
    {
        IEntity currentEntity = getDataSource().getCurrentEntity();
        return "status_id_" + currentEntity.getId();
    }

    public String getAdviserDependentFields()
    {
        IEntity currentEntity = getDataSource().getCurrentEntity();
        return "status_id_" + currentEntity.getId() + ", " + "degree_id_" + currentEntity.getId();
    }

    public String getDiplomaWorkTopic() {
        return _diplomaWorkTopic;
    }

    public void setDiplomaWorkTopic(String diplomaWorkTopic) {
        _diplomaWorkTopic = diplomaWorkTopic;
    }

    public ISelectModel getPpsListModel() {
        return _ppsListModel;
    }

    public void setPpsListModel(ISelectModel ppsListModel) {
        _ppsListModel = ppsListModel;
    }

    public Course getCourse() {
        return _course;
    }

    public void setCourse(Course course) {
        _course = course;
    }

    public ISelectModel getGroupListModel() {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel) {
        _groupListModel = groupListModel;
    }

    public Group getGroup() {
        return _group;
    }

    public void setGroup(Group group) {
        _group = group;
    }

    public DevelopForm getDevelopForm() {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm) {
        _developForm = developForm;
    }

    public ISelectModel getDevelopFormList() { return _developFormList; }
    public void setDevelopFormList(ISelectModel developFormList) { _developFormList = developFormList; }

    public DevelopCondition getDevelopCondition() {
        return _developCondition;
    }

    public void setDevelopCondition(DevelopCondition developCondition) {
        _developCondition = developCondition;
    }

    public ISelectModel getDevelopConditionList() { return _developConditionList; }
    public void setDevelopConditionList(ISelectModel developConditionList) { _developConditionList = developConditionList; }

}
