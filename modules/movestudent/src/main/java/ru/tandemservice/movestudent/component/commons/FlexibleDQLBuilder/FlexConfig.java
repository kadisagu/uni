/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

import org.tandemframework.core.entity.IEntity;

import java.util.*;

/**
 * @author Nikolay Fedorovskih
 * @since 29.11.2013
 */
public class FlexConfig implements IFlexConfig
{
    private Map<String, IFlexColumn> _columns = new LinkedHashMap<>();
    private Map<String, IFlexJoin> _joins = new LinkedHashMap<>();
    private List<IFlexFilter> _filters = new ArrayList<>();
    private List<IFlexOrder> _orders = new ArrayList<>();
    private Class<? extends IEntity> _clazz;
    private String _alias;

    public FlexConfig(Class<? extends IEntity> clazz, String alias)
    {
        _clazz = clazz;
        _alias = alias;
    }

    public Class<? extends IEntity> getClazz()
    {
        return _clazz;
    }

    public String getAlias()
    {
        return _alias;
    }

    public IFlexConfig addOrders(IFlexOrder... orders)
    {
        Collections.addAll(_orders, orders);
        return this;
    }

    @Override
    public IFlexConfig addFilters(IFlexFilter... filters)
    {
        Collections.addAll(_filters, filters);
        return this;
    }

    @Override
    public IFlexConfig addColumns(IFlexColumn... columns)
    {
        for (IFlexColumn column : columns)
        {
            String colName = column.getName();
            if (_columns.containsKey(colName))
                throw new RuntimeException("Column name mast bu unique.");

            _columns.put(column.getName(), column);
        }
        return this;
    }

    @Override
    public IFlexConfig addJoins(IFlexJoin... joins)
    {
        for (IFlexJoin join : joins)
        {
            String joinAlias = join.getAlias();
            if (_joins.containsKey(joinAlias))
                throw new RuntimeException("Join alias must be unique.");

            _joins.put(joinAlias, join);
        }
        return this;
    }

    @Override
    public IFlexColumn getColumn(String name)
    {
        return _columns.get(name);
    }

    @Override
    public Collection<IFlexColumn> getColumns()
    {
        return _columns.values();
    }

    @Override
    public IFlexOrder getOrder(String name)
    {
        for (IFlexOrder order : _orders)
        {
            if (order.getName().equals(name))
                return order;
        }
        return null;
    }

    @Override
    public Collection<IFlexFilter> getFilters()
    {
        return _filters;
    }

    @Override
    public Collection<IFlexJoin> getJoins()
    {
        return _joins.values();
    }

    @Override
    public IFlexJoin getJoin(String alias)
    {
        return _joins.get(alias);
    }
}