/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeGroupCaptain.AddEdit;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditController;
import ru.tandemservice.movestudent.entity.custom.ChangeGroupCaptainAction;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class Controller extends BaseCustomActionAddEditController<ChangeGroupCaptainAction, IDAO, Model>
{
}