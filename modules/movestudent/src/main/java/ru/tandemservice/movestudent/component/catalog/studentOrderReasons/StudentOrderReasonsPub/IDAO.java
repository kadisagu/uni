/* $Id$ */
package ru.tandemservice.movestudent.component.catalog.studentOrderReasons.StudentOrderReasonsPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;

/**
 * @author AutoGenerator
 *         Created on 05.12.2012
 */
public interface IDAO extends IDefaultCatalogPubDAO<StudentOrderReasons, Model>
{
}