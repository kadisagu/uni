/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e58.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.TransferIndGraphStuExtract;
import ru.tandemservice.uni.entity.catalog.Term;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 21.08.2012
 */
public class DAO extends ModularStudentExtractPubDAO<TransferIndGraphStuExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        StringBuilder builder = new StringBuilder();
        for (Term term: MoveStudentDaoFacade.getMoveStudentDao().getExtractTerms(model.getExtract()))
            builder.append(term.getTitle()).append(", ");
        model.setTerms(builder.length() > 1 ? builder.substring(0, builder.length() - 2) : "");
    }
}
