/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.component.order.modular.StudentModularOrderPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.component.visa.VisaList.IVisaOwnerModel;

import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 20.10.2008
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "order.id"),
        @Bind(key = "selectedTab", binding = "selectedTab")
})
public class Model implements IVisaOwnerModel, IEducationLevelModel
{
    private IDataSettings _settings;
    private CommonPostfixPermissionModel _secModel;
    private StudentModularOrder _order = new StudentModularOrder();
    private String _selectedTab;
    private Long _visingStatus;
    private DynamicListDataSource<IAbstractExtract> _dataSource;

    private Map<String, Object> _visaListParameters = ParametersMap.createWith("visaOwnerModel", this);

    // filters
    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _producingOrgUnitModel;
    private ISelectModel _educationLevelHighSchool;
    private List<Course> _courseList;
    private ISelectModel _developFormList;
    private List<CompensationType> _compensationTypeList;
    private IMultiSelectModel _extractTypesListModel;

    public boolean isNeedVising()
    {
        return _visingStatus != null;
    }

    // IVisaOwnerModel

    @Override
    public IAbstractDocument getDocument()
    {
        return _order;
    }

    @Override
    public boolean isVisaListModificable()
    {
        return UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(_order.getState().getCode());
    }

    @Override
    public String getSecPostfix()
    {
        return "studentModularOrder";
    }

    // IEducationLevelModel

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return (OrgUnit) _settings.get("formativeOrgUnit");
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return (OrgUnit) _settings.get("territorialOrgUnit");
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return (EducationLevelsHighSchool) _settings.get("educationLevelHighSchool");
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        throw new UnsupportedOperationException();
    }

    // Getters & Setters

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public StudentModularOrder getOrder()
    {
        return _order;
    }

    public void setOrder(StudentModularOrder order)
    {
        _order = order;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public Long getVisingStatus()
    {
        return _visingStatus;
    }

    public void setVisingStatus(Long visingStatus)
    {
        _visingStatus = visingStatus;
    }

    public DynamicListDataSource<IAbstractExtract> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<IAbstractExtract> dataSource)
    {
        _dataSource = dataSource;
    }

    public Map<String, Object> getVisaListParameters()
    {
        return _visaListParameters;
    }

    public void setVisaListParameters(Map<String, Object> visaListParameters)
    {
        _visaListParameters = visaListParameters;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getProducingOrgUnitModel()
    {
        return _producingOrgUnitModel;
    }

    public void setProducingOrgUnitModel(ISelectModel producingOrgUnitModel)
    {
        _producingOrgUnitModel = producingOrgUnitModel;
    }

    public ISelectModel getEducationLevelHighSchool()
    {
        return _educationLevelHighSchool;
    }

    public void setEducationLevelHighSchool(ISelectModel educationLevelHighSchool)
    {
        _educationLevelHighSchool = educationLevelHighSchool;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public ISelectModel getDevelopFormList() { return _developFormList; }
    public void setDevelopFormList(ISelectModel developFormList) { _developFormList = developFormList; }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public IMultiSelectModel getExtractTypesListModel()
    {
        return _extractTypesListModel;
    }

    public void setExtractTypesListModel(IMultiSelectModel extractTypesListModel)
    {
        this._extractTypesListModel = extractTypesListModel;
    }
}