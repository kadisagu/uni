/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e53;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.HolidayStuExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 15.08.2012
 */
public class HolidayStuExtractDao extends UniBaseDao implements IExtractComponentDao<HolidayStuExtract>
{
    public void doCommit(HolidayStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            extract.setPrevOrderDate(orderData.getHolidaysOrderDate());
            extract.setPrevOrderNumber(orderData.getHolidaysOrderNumber());
            extract.setPrevBeginDate(orderData.getHolidaysBeginDate());
            extract.setPrevEndDate(orderData.getHolidaysEndDate());
        }
        orderData.setHolidaysOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setHolidaysOrderNumber(extract.getParagraph().getOrder().getNumber());
        orderData.setHolidaysBeginDate(extract.getBeginDate());
        orderData.setHolidaysEndDate(extract.getEndDate());
        getSession().saveOrUpdate(orderData);
    }

    public void doRollback(HolidayStuExtract extract, Map parameters)
    {
        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setHolidaysOrderDate(extract.getPrevOrderDate());
        orderData.setHolidaysOrderNumber(extract.getPrevOrderNumber());
        orderData.setHolidaysBeginDate(extract.getPrevBeginDate());
        orderData.setHolidaysEndDate(extract.getPrevEndDate());
        getSession().saveOrUpdate(orderData);
    }
}