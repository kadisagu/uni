/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.customorder.listextract.AddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.custom.ModifyListStuExtract;
import ru.tandemservice.movestudent.entity.custom.OrderActionType;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 07.10.2013
 */
public class Controller extends AbstractListParagraphAddEditController<ModifyListStuExtract, IDAO, Model>
{
    @Override
    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Пол", Student.person().identityCard().sex().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("status", "Состояние", Model.STATUS_KEY, SimpleFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn(EDU_LVL_HS_COLUMN_TITLE, Student.educationOrgUnit().educationLevelHighSchool().configurableTitle().s()).setClickable(false));
        dataSource.setOrder("status", OrderDirection.asc);
    }

    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        final Model model = this.getModel(component);
        model.setContext(component);
        activateCustomComponents(component);
    }

    public void onChangeActionType(IBusinessComponent component)
    {
        activateCustomComponents(component);
    }

    private void activateCustomComponents(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (OrderActionType.DO_CUSTOM_ACTION == model.getOrderActionType())
        {
            for (Map.Entry<String, String> entry : model.getPossibleActionComponents().entrySet())
            {
                if (component.getChildRegion(entry.getKey()) == null)
                {
                    Long extractId = model.isEditForm() ? MoveStudentDaoFacade.getMoveStudentDao().getParagraphFirstExtract(model.getParagraph().getId(), true).getId() : null;
                    component.createChildRegion(entry.getKey(), new ComponentActivator(
                            entry.getValue() + ".AddEdit",
                            new ParametersMap().add(BaseCustomActionAddEditModel.EXTRACT_ID_BIND, extractId)
                    ));
                }
            }
        }
    }
}