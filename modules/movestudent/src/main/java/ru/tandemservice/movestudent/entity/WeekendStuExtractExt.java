package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IAcadWeekendExtract;
import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.WeekendStuExtractExtGen;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О предоставлении академического отпуска
 */
public class WeekendStuExtractExt extends WeekendStuExtractExtGen implements ITransferExtract, IAcadWeekendExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getBeginDate();
    }

    @Override
    public Group getGroupOld()
    {
        return getEntity().getGroup();
    }

    @Override
    public Group getGroupNew()
    {
        return null;
    }

    @Override
    public Date getBeginAcadWeekendDate()
    {
        return getBeginDate();
    }

    @Override
    public Date getEndAcadWeekendDate()
    {
        return getEndDate();
    }
}