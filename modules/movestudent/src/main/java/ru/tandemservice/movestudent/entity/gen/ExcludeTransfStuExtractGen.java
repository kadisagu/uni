package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об отчислении в связи с переводом
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExcludeTransfStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract";
    public static final String ENTITY_NAME = "excludeTransfStuExtract";
    public static final int VERSION_HASH = -145952491;
    private static IEntityMeta ENTITY_META;

    public static final String P_APPLY_DATE = "applyDate";
    public static final String P_TRANSFER_TO = "transferTo";
    public static final String P_DATE = "date";
    public static final String P_STOP_GRANTS_PAYING = "stopGrantsPaying";
    public static final String P_STOP_PAYING_DATE = "stopPayingDate";
    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";

    private Date _applyDate;     // Дата заявления
    private String _transferTo;     // Переводится в
    private Date _date;     // Дата отчисления
    private boolean _stopGrantsPaying;     // Отменить выплату стипендии
    private Date _stopPayingDate;     // Дата прекращения выплаты стипендии
    private StudentStatus _studentStatusOld;     // Предыдущее состояние студента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата заявления. Свойство не может быть null.
     */
    @NotNull
    public Date getApplyDate()
    {
        return _applyDate;
    }

    /**
     * @param applyDate Дата заявления. Свойство не может быть null.
     */
    public void setApplyDate(Date applyDate)
    {
        dirty(_applyDate, applyDate);
        _applyDate = applyDate;
    }

    /**
     * @return Переводится в.
     */
    @Length(max=255)
    public String getTransferTo()
    {
        return _transferTo;
    }

    /**
     * @param transferTo Переводится в.
     */
    public void setTransferTo(String transferTo)
    {
        dirty(_transferTo, transferTo);
        _transferTo = transferTo;
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата отчисления. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Отменить выплату стипендии. Свойство не может быть null.
     */
    @NotNull
    public boolean isStopGrantsPaying()
    {
        return _stopGrantsPaying;
    }

    /**
     * @param stopGrantsPaying Отменить выплату стипендии. Свойство не может быть null.
     */
    public void setStopGrantsPaying(boolean stopGrantsPaying)
    {
        dirty(_stopGrantsPaying, stopGrantsPaying);
        _stopGrantsPaying = stopGrantsPaying;
    }

    /**
     * @return Дата прекращения выплаты стипендии.
     */
    public Date getStopPayingDate()
    {
        return _stopPayingDate;
    }

    /**
     * @param stopPayingDate Дата прекращения выплаты стипендии.
     */
    public void setStopPayingDate(Date stopPayingDate)
    {
        dirty(_stopPayingDate, stopPayingDate);
        _stopPayingDate = stopPayingDate;
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Предыдущее состояние студента. Свойство не может быть null.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ExcludeTransfStuExtractGen)
        {
            setApplyDate(((ExcludeTransfStuExtract)another).getApplyDate());
            setTransferTo(((ExcludeTransfStuExtract)another).getTransferTo());
            setDate(((ExcludeTransfStuExtract)another).getDate());
            setStopGrantsPaying(((ExcludeTransfStuExtract)another).isStopGrantsPaying());
            setStopPayingDate(((ExcludeTransfStuExtract)another).getStopPayingDate());
            setStudentStatusOld(((ExcludeTransfStuExtract)another).getStudentStatusOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExcludeTransfStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExcludeTransfStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new ExcludeTransfStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "applyDate":
                    return obj.getApplyDate();
                case "transferTo":
                    return obj.getTransferTo();
                case "date":
                    return obj.getDate();
                case "stopGrantsPaying":
                    return obj.isStopGrantsPaying();
                case "stopPayingDate":
                    return obj.getStopPayingDate();
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "applyDate":
                    obj.setApplyDate((Date) value);
                    return;
                case "transferTo":
                    obj.setTransferTo((String) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "stopGrantsPaying":
                    obj.setStopGrantsPaying((Boolean) value);
                    return;
                case "stopPayingDate":
                    obj.setStopPayingDate((Date) value);
                    return;
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "applyDate":
                        return true;
                case "transferTo":
                        return true;
                case "date":
                        return true;
                case "stopGrantsPaying":
                        return true;
                case "stopPayingDate":
                        return true;
                case "studentStatusOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "applyDate":
                    return true;
                case "transferTo":
                    return true;
                case "date":
                    return true;
                case "stopGrantsPaying":
                    return true;
                case "stopPayingDate":
                    return true;
                case "studentStatusOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "applyDate":
                    return Date.class;
                case "transferTo":
                    return String.class;
                case "date":
                    return Date.class;
                case "stopGrantsPaying":
                    return Boolean.class;
                case "stopPayingDate":
                    return Date.class;
                case "studentStatusOld":
                    return StudentStatus.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExcludeTransfStuExtract> _dslPath = new Path<ExcludeTransfStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExcludeTransfStuExtract");
    }
            

    /**
     * @return Дата заявления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract#getApplyDate()
     */
    public static PropertyPath<Date> applyDate()
    {
        return _dslPath.applyDate();
    }

    /**
     * @return Переводится в.
     * @see ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract#getTransferTo()
     */
    public static PropertyPath<String> transferTo()
    {
        return _dslPath.transferTo();
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Отменить выплату стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract#isStopGrantsPaying()
     */
    public static PropertyPath<Boolean> stopGrantsPaying()
    {
        return _dslPath.stopGrantsPaying();
    }

    /**
     * @return Дата прекращения выплаты стипендии.
     * @see ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract#getStopPayingDate()
     */
    public static PropertyPath<Date> stopPayingDate()
    {
        return _dslPath.stopPayingDate();
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    public static class Path<E extends ExcludeTransfStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _applyDate;
        private PropertyPath<String> _transferTo;
        private PropertyPath<Date> _date;
        private PropertyPath<Boolean> _stopGrantsPaying;
        private PropertyPath<Date> _stopPayingDate;
        private StudentStatus.Path<StudentStatus> _studentStatusOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата заявления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract#getApplyDate()
     */
        public PropertyPath<Date> applyDate()
        {
            if(_applyDate == null )
                _applyDate = new PropertyPath<Date>(ExcludeTransfStuExtractGen.P_APPLY_DATE, this);
            return _applyDate;
        }

    /**
     * @return Переводится в.
     * @see ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract#getTransferTo()
     */
        public PropertyPath<String> transferTo()
        {
            if(_transferTo == null )
                _transferTo = new PropertyPath<String>(ExcludeTransfStuExtractGen.P_TRANSFER_TO, this);
            return _transferTo;
        }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(ExcludeTransfStuExtractGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Отменить выплату стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract#isStopGrantsPaying()
     */
        public PropertyPath<Boolean> stopGrantsPaying()
        {
            if(_stopGrantsPaying == null )
                _stopGrantsPaying = new PropertyPath<Boolean>(ExcludeTransfStuExtractGen.P_STOP_GRANTS_PAYING, this);
            return _stopGrantsPaying;
        }

    /**
     * @return Дата прекращения выплаты стипендии.
     * @see ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract#getStopPayingDate()
     */
        public PropertyPath<Date> stopPayingDate()
        {
            if(_stopPayingDate == null )
                _stopPayingDate = new PropertyPath<Date>(ExcludeTransfStuExtractGen.P_STOP_PAYING_DATE, this);
            return _stopPayingDate;
        }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

        public Class getEntityClass()
        {
            return ExcludeTransfStuExtract.class;
        }

        public String getEntityName()
        {
            return "excludeTransfStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
