package ru.tandemservice.movestudent.entity.catalog;

import ru.tandemservice.movestudent.entity.catalog.gen.StudentOrderBasicsGen;

public class StudentOrderBasics extends StudentOrderBasicsGen
{
    public static final String P_REQUIRED_DISABLED = "requiredDisabled";

    public boolean isRequiredDisabled()
    {
        return !isCommentable();
    }
}