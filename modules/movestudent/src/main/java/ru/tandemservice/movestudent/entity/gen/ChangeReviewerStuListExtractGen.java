package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ChangeReviewerStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О смене рецензентов»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChangeReviewerStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ChangeReviewerStuListExtract";
    public static final String ENTITY_NAME = "changeReviewerStuListExtract";
    public static final int VERSION_HASH = 1859701985;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String P_REVIEWER_NAME = "reviewerName";
    public static final String P_REVIEWER_DEGREE = "reviewerDegree";
    public static final String L_GROUP = "group";

    private Course _course;     // Курс
    private DevelopForm _developForm;     // Форма освоения
    private String _reviewerName;     // ФИО рецензента
    private String _reviewerDegree;     // Ученая степень рецензента
    private Group _group;     // Группа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return ФИО рецензента. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getReviewerName()
    {
        return _reviewerName;
    }

    /**
     * @param reviewerName ФИО рецензента. Свойство не может быть null.
     */
    public void setReviewerName(String reviewerName)
    {
        dirty(_reviewerName, reviewerName);
        _reviewerName = reviewerName;
    }

    /**
     * @return Ученая степень рецензента. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getReviewerDegree()
    {
        return _reviewerDegree;
    }

    /**
     * @param reviewerDegree Ученая степень рецензента. Свойство не может быть null.
     */
    public void setReviewerDegree(String reviewerDegree)
    {
        dirty(_reviewerDegree, reviewerDegree);
        _reviewerDegree = reviewerDegree;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ChangeReviewerStuListExtractGen)
        {
            setCourse(((ChangeReviewerStuListExtract)another).getCourse());
            setDevelopForm(((ChangeReviewerStuListExtract)another).getDevelopForm());
            setReviewerName(((ChangeReviewerStuListExtract)another).getReviewerName());
            setReviewerDegree(((ChangeReviewerStuListExtract)another).getReviewerDegree());
            setGroup(((ChangeReviewerStuListExtract)another).getGroup());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChangeReviewerStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChangeReviewerStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new ChangeReviewerStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "developForm":
                    return obj.getDevelopForm();
                case "reviewerName":
                    return obj.getReviewerName();
                case "reviewerDegree":
                    return obj.getReviewerDegree();
                case "group":
                    return obj.getGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "reviewerName":
                    obj.setReviewerName((String) value);
                    return;
                case "reviewerDegree":
                    obj.setReviewerDegree((String) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "developForm":
                        return true;
                case "reviewerName":
                        return true;
                case "reviewerDegree":
                        return true;
                case "group":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "developForm":
                    return true;
                case "reviewerName":
                    return true;
                case "reviewerDegree":
                    return true;
                case "group":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "developForm":
                    return DevelopForm.class;
                case "reviewerName":
                    return String.class;
                case "reviewerDegree":
                    return String.class;
                case "group":
                    return Group.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChangeReviewerStuListExtract> _dslPath = new Path<ChangeReviewerStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChangeReviewerStuListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeReviewerStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeReviewerStuListExtract#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return ФИО рецензента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeReviewerStuListExtract#getReviewerName()
     */
    public static PropertyPath<String> reviewerName()
    {
        return _dslPath.reviewerName();
    }

    /**
     * @return Ученая степень рецензента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeReviewerStuListExtract#getReviewerDegree()
     */
    public static PropertyPath<String> reviewerDegree()
    {
        return _dslPath.reviewerDegree();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeReviewerStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    public static class Path<E extends ChangeReviewerStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private DevelopForm.Path<DevelopForm> _developForm;
        private PropertyPath<String> _reviewerName;
        private PropertyPath<String> _reviewerDegree;
        private Group.Path<Group> _group;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeReviewerStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeReviewerStuListExtract#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return ФИО рецензента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeReviewerStuListExtract#getReviewerName()
     */
        public PropertyPath<String> reviewerName()
        {
            if(_reviewerName == null )
                _reviewerName = new PropertyPath<String>(ChangeReviewerStuListExtractGen.P_REVIEWER_NAME, this);
            return _reviewerName;
        }

    /**
     * @return Ученая степень рецензента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeReviewerStuListExtract#getReviewerDegree()
     */
        public PropertyPath<String> reviewerDegree()
        {
            if(_reviewerDegree == null )
                _reviewerDegree = new PropertyPath<String>(ChangeReviewerStuListExtractGen.P_REVIEWER_DEGREE, this);
            return _reviewerDegree;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeReviewerStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

        public Class getEntityClass()
        {
            return ChangeReviewerStuListExtract.class;
        }

        public String getEntityName()
        {
            return "changeReviewerStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
