/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.component.menu.ModularExtracts;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.component.commons.IMoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.MoveStudentColumns;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;

/**
 * @author agolubenko
 * @since 21.11.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setSettings(component.getSettings());
        model.getSettings().set(IMoveStudentMQBuilder.LAST_NAME, null);

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<ModularStudentExtract> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(MoveStudentColumns.getCreateDateColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractCommitDateColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractOrderNumberColumn());
        dataSource.addColumn(MoveStudentColumns.getModularExtractNumberColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractCommitDateSystemColumn());
        dataSource.addColumn(MoveStudentColumns.getFullFioColumn());
        dataSource.addColumn(MoveStudentColumns.getFormativeOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getTerritorialOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getProducingOrgUnitColumn());
        MoveStudentColumns.addEduLevelTitleColumns(dataSource);
        dataSource.addColumn(MoveStudentColumns.getCourseColumn());
        dataSource.addColumn(MoveStudentColumns.getGroupColumn());
        dataSource.addColumn(MoveStudentColumns.getDevelopFormColumn());
        dataSource.addColumn(MoveStudentColumns.getCompensationTypeColumn());
        dataSource.addColumn(MoveStudentColumns.getExecutorColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractTypeColumn("Тип приказа"));
        dataSource.addColumn(MoveStudentColumns.getExtractStateColumn("Состояние выписки"));
        dataSource.addColumn(MoveStudentColumns.getExtractPrintColumn(this, getModel(component).getSecModel().getPermission("printPdf_menuList")));
        if(MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
            dataSource.addColumn(MoveStudentColumns.getExtractPrintPdfColumn(this, getModel(component).getSecModel().getPermission("printPdf_menuList")));
        dataSource.setOrder(IAbstractExtract.P_CREATE_DATE, OrderDirection.desc);
        getModel(component).setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);

        UniBaseUtils.validateDatesPeriod(model.getSettings(), "commitDateFrom", "commitDateTo");
        UniBaseUtils.validateDatesPeriod(model.getSettings(), "createDateFrom", "createDateTo");
        UniBaseUtils.validateDatesPeriod(model.getSettings(), "commitDateSystemFrom", "commitDateSystemTo");

        if (!UserContext.getInstance().getErrorCollector().isHasFieldErrors())
        {
            component.saveSettings();
            model.getDataSource().refresh();
        }
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }

    public void onClickExtractPrint(IBusinessComponent component)
    {
        ParametersMap params = new ParametersMap().add("extractId", component.getListenerParameter());
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.MODULAR_STUDENT_EXTRACT_PRINT, params));
    }

    public void onClickExtractPrintPdf(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.MODULAR_STUDENT_EXTRACT_PRINT, new ParametersMap()
                .add("extractId", component.getListenerParameter())
                .add("printPdf", Boolean.TRUE)
        ));
    }
}