package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.GiveDiplAppDuplicateStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О выдаче дубликата приложения к диплому
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GiveDiplAppDuplicateStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.GiveDiplAppDuplicateStuExtract";
    public static final String ENTITY_NAME = "giveDiplAppDuplicateStuExtract";
    public static final int VERSION_HASH = 1741071209;
    private static IEntityMeta ENTITY_META;

    public static final String P_YEAR = "year";
    public static final String P_EXCELLENT_DIPLOMA = "excellentDiploma";
    public static final String P_DIPLOMA_NUMBER = "diplomaNumber";

    private int _year;     // Год выпуска
    private boolean _excellentDiploma;     // Диплом с отличием
    private String _diplomaNumber;     // Номер выдаваемого документа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Год выпуска. Свойство не может быть null.
     */
    @NotNull
    public int getYear()
    {
        return _year;
    }

    /**
     * @param year Год выпуска. Свойство не может быть null.
     */
    public void setYear(int year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Диплом с отличием. Свойство не может быть null.
     */
    @NotNull
    public boolean isExcellentDiploma()
    {
        return _excellentDiploma;
    }

    /**
     * @param excellentDiploma Диплом с отличием. Свойство не может быть null.
     */
    public void setExcellentDiploma(boolean excellentDiploma)
    {
        dirty(_excellentDiploma, excellentDiploma);
        _excellentDiploma = excellentDiploma;
    }

    /**
     * @return Номер выдаваемого документа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDiplomaNumber()
    {
        return _diplomaNumber;
    }

    /**
     * @param diplomaNumber Номер выдаваемого документа. Свойство не может быть null.
     */
    public void setDiplomaNumber(String diplomaNumber)
    {
        dirty(_diplomaNumber, diplomaNumber);
        _diplomaNumber = diplomaNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof GiveDiplAppDuplicateStuExtractGen)
        {
            setYear(((GiveDiplAppDuplicateStuExtract)another).getYear());
            setExcellentDiploma(((GiveDiplAppDuplicateStuExtract)another).isExcellentDiploma());
            setDiplomaNumber(((GiveDiplAppDuplicateStuExtract)another).getDiplomaNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GiveDiplAppDuplicateStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GiveDiplAppDuplicateStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new GiveDiplAppDuplicateStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "year":
                    return obj.getYear();
                case "excellentDiploma":
                    return obj.isExcellentDiploma();
                case "diplomaNumber":
                    return obj.getDiplomaNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "year":
                    obj.setYear((Integer) value);
                    return;
                case "excellentDiploma":
                    obj.setExcellentDiploma((Boolean) value);
                    return;
                case "diplomaNumber":
                    obj.setDiplomaNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "year":
                        return true;
                case "excellentDiploma":
                        return true;
                case "diplomaNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "year":
                    return true;
                case "excellentDiploma":
                    return true;
                case "diplomaNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "year":
                    return Integer.class;
                case "excellentDiploma":
                    return Boolean.class;
                case "diplomaNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GiveDiplAppDuplicateStuExtract> _dslPath = new Path<GiveDiplAppDuplicateStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GiveDiplAppDuplicateStuExtract");
    }
            

    /**
     * @return Год выпуска. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GiveDiplAppDuplicateStuExtract#getYear()
     */
    public static PropertyPath<Integer> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Диплом с отличием. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GiveDiplAppDuplicateStuExtract#isExcellentDiploma()
     */
    public static PropertyPath<Boolean> excellentDiploma()
    {
        return _dslPath.excellentDiploma();
    }

    /**
     * @return Номер выдаваемого документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GiveDiplAppDuplicateStuExtract#getDiplomaNumber()
     */
    public static PropertyPath<String> diplomaNumber()
    {
        return _dslPath.diplomaNumber();
    }

    public static class Path<E extends GiveDiplAppDuplicateStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Integer> _year;
        private PropertyPath<Boolean> _excellentDiploma;
        private PropertyPath<String> _diplomaNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Год выпуска. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GiveDiplAppDuplicateStuExtract#getYear()
     */
        public PropertyPath<Integer> year()
        {
            if(_year == null )
                _year = new PropertyPath<Integer>(GiveDiplAppDuplicateStuExtractGen.P_YEAR, this);
            return _year;
        }

    /**
     * @return Диплом с отличием. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GiveDiplAppDuplicateStuExtract#isExcellentDiploma()
     */
        public PropertyPath<Boolean> excellentDiploma()
        {
            if(_excellentDiploma == null )
                _excellentDiploma = new PropertyPath<Boolean>(GiveDiplAppDuplicateStuExtractGen.P_EXCELLENT_DIPLOMA, this);
            return _excellentDiploma;
        }

    /**
     * @return Номер выдаваемого документа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GiveDiplAppDuplicateStuExtract#getDiplomaNumber()
     */
        public PropertyPath<String> diplomaNumber()
        {
            if(_diplomaNumber == null )
                _diplomaNumber = new PropertyPath<String>(GiveDiplAppDuplicateStuExtractGen.P_DIPLOMA_NUMBER, this);
            return _diplomaNumber;
        }

        public Class getEntityClass()
        {
            return GiveDiplAppDuplicateStuExtract.class;
        }

        public String getEntityName()
        {
            return "giveDiplAppDuplicateStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
