/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeIdentityCard.Pub;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.BaseCustomActionPubModel;
import ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public class Model extends BaseCustomActionPubModel<ChangeIdentityCardAction>
{
}