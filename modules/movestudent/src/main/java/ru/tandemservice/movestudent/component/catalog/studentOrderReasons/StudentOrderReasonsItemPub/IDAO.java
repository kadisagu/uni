/* $Id$ */
package ru.tandemservice.movestudent.component.catalog.studentOrderReasons.StudentOrderReasonsItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.IDefaultCatalogItemPubDAO;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;

/**
 * @author AutoGenerator
 *         Created on 05.12.2012
 */
public interface IDAO extends IDefaultCatalogItemPubDAO<StudentOrderReasons, Model>
{
}