package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ExcludeSingGrpCathStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «Об отчислении для одной группы (кафедральный)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExcludeSingGrpCathStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ExcludeSingGrpCathStuListExtract";
    public static final String ENTITY_NAME = "excludeSingGrpCathStuListExtract";
    public static final int VERSION_HASH = -226898575;
    private static IEntityMeta ENTITY_META;

    public static final String P_EXCLUDE_DATE = "excludeDate";
    public static final String P_PROVIDED_EXAM = "providedExam";

    private Date _excludeDate;     // Дата отчисления
    private boolean _providedExam = true;     // Сдача ГЭ предусмотрена

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     */
    @NotNull
    public Date getExcludeDate()
    {
        return _excludeDate;
    }

    /**
     * @param excludeDate Дата отчисления. Свойство не может быть null.
     */
    public void setExcludeDate(Date excludeDate)
    {
        dirty(_excludeDate, excludeDate);
        _excludeDate = excludeDate;
    }

    /**
     * @return Сдача ГЭ предусмотрена. Свойство не может быть null.
     */
    @NotNull
    public boolean isProvidedExam()
    {
        return _providedExam;
    }

    /**
     * @param providedExam Сдача ГЭ предусмотрена. Свойство не может быть null.
     */
    public void setProvidedExam(boolean providedExam)
    {
        dirty(_providedExam, providedExam);
        _providedExam = providedExam;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ExcludeSingGrpCathStuListExtractGen)
        {
            setExcludeDate(((ExcludeSingGrpCathStuListExtract)another).getExcludeDate());
            setProvidedExam(((ExcludeSingGrpCathStuListExtract)another).isProvidedExam());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExcludeSingGrpCathStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExcludeSingGrpCathStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new ExcludeSingGrpCathStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    return obj.getExcludeDate();
                case "providedExam":
                    return obj.isProvidedExam();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    obj.setExcludeDate((Date) value);
                    return;
                case "providedExam":
                    obj.setProvidedExam((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                        return true;
                case "providedExam":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    return true;
                case "providedExam":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    return Date.class;
                case "providedExam":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExcludeSingGrpCathStuListExtract> _dslPath = new Path<ExcludeSingGrpCathStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExcludeSingGrpCathStuListExtract");
    }
            

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpCathStuListExtract#getExcludeDate()
     */
    public static PropertyPath<Date> excludeDate()
    {
        return _dslPath.excludeDate();
    }

    /**
     * @return Сдача ГЭ предусмотрена. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpCathStuListExtract#isProvidedExam()
     */
    public static PropertyPath<Boolean> providedExam()
    {
        return _dslPath.providedExam();
    }

    public static class Path<E extends ExcludeSingGrpCathStuListExtract> extends ListStudentExtract.Path<E>
    {
        private PropertyPath<Date> _excludeDate;
        private PropertyPath<Boolean> _providedExam;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpCathStuListExtract#getExcludeDate()
     */
        public PropertyPath<Date> excludeDate()
        {
            if(_excludeDate == null )
                _excludeDate = new PropertyPath<Date>(ExcludeSingGrpCathStuListExtractGen.P_EXCLUDE_DATE, this);
            return _excludeDate;
        }

    /**
     * @return Сдача ГЭ предусмотрена. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeSingGrpCathStuListExtract#isProvidedExam()
     */
        public PropertyPath<Boolean> providedExam()
        {
            if(_providedExam == null )
                _providedExam = new PropertyPath<Boolean>(ExcludeSingGrpCathStuListExtractGen.P_PROVIDED_EXAM, this);
            return _providedExam;
        }

        public Class getEntityClass()
        {
            return ExcludeSingGrpCathStuListExtract.class;
        }

        public String getEntityName()
        {
            return "excludeSingGrpCathStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
