/* $Id$ */
package ru.tandemservice.movestudent.component.catalog.studentOrderReasons.StudentOrderReasonsAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;

/**
 * @author AutoGenerator
 *         Created on 05.12.2012
 */
public class DAO extends DefaultCatalogAddEditDAO<StudentOrderReasons, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
    }

    @Override
    public void update(Model model)
    {
        super.update(model);
    }
}