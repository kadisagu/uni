package ru.tandemservice.movestudent.entity;

import org.tandemframework.shared.commonbase.base.util.IScriptOwner;
import ru.tandemservice.movestudent.entity.gen.*;
import ru.tandemservice.uni.dao.UniScriptDao;

/** @see ru.tandemservice.movestudent.entity.gen.ExportMSRSettingsGen */
public class ExportMSRSettings extends ExportMSRSettingsGen implements IScriptOwner
{
    public static String getDefScriptPath()
    {
        return UniScriptDao.findScriptPath("scripts/UnloadStudentGupMsrScript.groovy");
    }

    @Override
    public String getScriptPath()
    {
        return getDefScriptPath();
    }

    @Override
    public String getUserScript()
    {
        return getUserScriptExportStudent();
    }
}
