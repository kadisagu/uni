package ru.tandemservice.movestudent.entity;

import java.util.Date;

import ru.tandemservice.movestudent.component.commons.gradation.IAcadWeekendExtract;
import ru.tandemservice.movestudent.entity.gen.ProlongWeekendStuExtractGen;

/**
 * Выписка из сборного приказа по студенту. О продлении академического отпуска
 */
public class ProlongWeekendStuExtract extends ProlongWeekendStuExtractGen implements IAcadWeekendExtract
{
    @Override
    public Date getBeginDate()
    {
        return getProlongDateFrom();
    }

    @Override
    public Date getEndDate()
    {
        return getProlongDateTo();
    }

    @Override
    public Date getBeginAcadWeekendDate()
    {
        return getProlongDateFrom();
    }

    @Override
    public Date getEndAcadWeekendDate()
    {
        return getProlongDateTo();
    }
}