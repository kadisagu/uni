package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.entity.gen.ChangePassStateExamsStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О переносе итоговой государственной аттестации
 */
public class ChangePassStateExamsStuExtract extends ChangePassStateExamsStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getNewDate();
    }
}