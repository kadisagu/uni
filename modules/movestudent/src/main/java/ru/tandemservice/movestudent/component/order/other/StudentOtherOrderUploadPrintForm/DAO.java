/* $Id$ */
package ru.tandemservice.movestudent.component.order.other.StudentOtherOrderUploadPrintForm;

import org.apache.commons.io.IOUtils;
import org.apache.tapestry.request.IUploadFile;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.movestudent.entity.StudentOtherOrder;
import ru.tandemservice.movestudent.entity.StudentOtherOrderTextRelation;
import ru.tandemservice.uni.dao.UniDao;

/**
 * @author Dmitry Seleznev
 * @since 15.04.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setOrder(this.getNotNull(StudentOtherOrder.class, model.getOrderId()));
        Criteria c = getSession().createCriteria(StudentOtherOrderTextRelation.class);
        c.add(Restrictions.eq(StudentOtherOrderTextRelation.L_ORDER, model.getOrder()));
        model.setPrintForm((StudentOtherOrderTextRelation) c.uniqueResult());
    }

    @Override
    public void update(Model model)
    {
        IUploadFile uploadFile = model.getUploadFile();

        if (uploadFile.getSize() > 5 * 1024 * 1024)
            throw new ApplicationException("Размер загружаемого файла не должен превышать 5Mb.");

        try
        {
            byte[] content = IOUtils.toByteArray(uploadFile.getStream());
            if ((content == null) || (content.length == 0)) content = new byte[]{};

            if (null != model.getPrintForm())
            {
                model.getPrintForm().setFileType(CommonBaseUtil.getContentType(uploadFile));
                model.getPrintForm().setFileName(uploadFile.getFileName());
                model.getPrintForm().setContent(content);
                update(model.getPrintForm());
            } else
            {
                StudentOtherOrderTextRelation rel = new StudentOtherOrderTextRelation();
                rel.setOrder(model.getOrder());
                rel.setFileType(uploadFile.getContentType());
                rel.setFileName(uploadFile.getFileName());
                rel.setContent(content);
                save(rel);
            }
        } catch (final Throwable e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}