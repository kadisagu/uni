/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.component.orgunit.OrgUnitMoveStudent;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.component.commons.ExtractListModel;
import ru.tandemservice.movestudent.component.commons.MoveStudentColumns;
import ru.tandemservice.movestudent.component.commons.OrderFlexBuilder;
import ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderAddEdit.AbstractListOrderAddEditModel;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.dao.MoveDaoFacade;

import java.util.Date;

/**
 * @author vip_delete
 * @since 18.10.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.getIndividualOrdersModel().setSettings(UniBaseUtils.getDataSettings(component, "OrgUnitMoveStudent_IndividualOrdersTab_" + model.getOrgUnitId() + ".filter"));
        model.getProjectsModel().setSettings(UniBaseUtils.getDataSettings(component, "OrgUnitMoveStudent_ProjectsTab_" + model.getOrgUnitId() + ".filter"));
        model.setModularOrdersSettings(UniBaseUtils.getDataSettings(component, "OrgUnitMoveStudent_ModularOrdersTab_" + model.getOrgUnitId() + ".filter"));
        model.getExtractsModel().setSettings(UniBaseUtils.getDataSettings(component, "OrgUnitMoveStudent_ModularExtractsTab_" + model.getOrgUnitId() + ".filter"));
        model.setListOrdersSettings(UniBaseUtils.getDataSettings(component, "OrgUnitMoveStudent_ListOrdersTab_" + model.getOrgUnitId() + ".filter"));
        model.getListextractsModel().setSettings(UniBaseUtils.getDataSettings(component, "OrgUnitMoveStudent_ListExtractsTab_" + model.getOrgUnitId() + ".filter"));
        model.getOtherOrdersModel().setSettings(UniBaseUtils.getDataSettings(component, "OrgUnitMoveStudent_OtherOrdersTab_" + model.getOrgUnitId() + ".filter"));

        getDao().prepare(model);

        prepareIndividualOrdersListDataSource(model, component);
        prepareProjectsListDataSource(model, component);
        prepareModularOrdersListDataSource(model, component);
        prepareExtractsListDataSource(model, component);
        prepareListOrdersListDataSource(model, component);
        prepareListExtractsListDataSource(model, component);
        prepareOtherOrdersListDataSource(model, component);
    }

    // prepare data source

    private void prepareIndividualOrdersListDataSource(final Model model, IBusinessComponent component)
    {
        if (model.getIndividualOrdersModel().getDataSource() != null) return;

        DynamicListDataSource<ModularStudentExtract> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareIndividualOrdersDataSource(model);
        });
        dataSource.addColumn(MoveStudentColumns.getCreateDateColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractOrderNumberColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractCommitDateColumn());
        dataSource.addColumn(MoveStudentColumns.getFullFioColumn());
        dataSource.addColumn(MoveStudentColumns.getFormativeOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getTerritorialOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getProducingOrgUnitColumn());
        MoveStudentColumns.addEduLevelTitleColumns(dataSource);
        dataSource.addColumn(MoveStudentColumns.getCourseColumn());
        dataSource.addColumn(MoveStudentColumns.getGroupColumn());
        dataSource.addColumn(MoveStudentColumns.getDevelopFormColumn());
        dataSource.addColumn(MoveStudentColumns.getCompensationTypeColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractTypeColumn("Тип приказа"));
        dataSource.addColumn(MoveStudentColumns.getModularExtractReasonColumn("Причина приказа"));
        dataSource.addColumn(MoveStudentColumns.getExtractOrderStateColumn());
        dataSource.addColumn(MoveStudentColumns.getIndividualOrderPrintColumn(this, model.getIndividualOrdersModel().getSecModel().getPermission("orgUnit_print")));
        if (MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
            dataSource.addColumn(MoveStudentColumns.getIndividualOrderPrintPdfColumn(this, model.getIndividualOrdersModel().getSecModel().getPermission("orgUnit_printPdf")));

        if (!model.getOrgUnit().isArchival())
        {
            dataSource.addColumn(MoveStudentColumns.getIndividualOrderEditColumn(this, model.getIndividualOrdersModel().getSecModel().getPermission("orgUnit_edit")));
            dataSource.addColumn(MoveStudentColumns.getIndividualOrderDeleteColumn(this, model.getIndividualOrdersModel().getSecModel().getPermission("orgUnit_delete")));
        }

        dataSource.setOrder(IAbstractExtract.P_CREATE_DATE, OrderDirection.desc);
        model.getIndividualOrdersModel().setDataSource(dataSource);
    }

    private void prepareProjectsListDataSource(final Model model, IBusinessComponent component)
    {
        if (model.getProjectsModel().getDataSource() != null) return;

        DynamicListDataSource<ModularStudentExtract> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareProjectsDataSource(model);
        });
        dataSource.addColumn(MoveStudentColumns.getCreateDateColumn());
        dataSource.addColumn(MoveStudentColumns.getFullFioColumn());
        dataSource.addColumn(MoveStudentColumns.getFormativeOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getTerritorialOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getProducingOrgUnitColumn());
        MoveStudentColumns.addEduLevelTitleColumns(dataSource);
        dataSource.addColumn(MoveStudentColumns.getCourseColumn());
        dataSource.addColumn(MoveStudentColumns.getGroupColumn());
        dataSource.addColumn(MoveStudentColumns.getDevelopFormColumn());
        dataSource.addColumn(MoveStudentColumns.getCompensationTypeColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractTypeColumn("Тип приказа"));
        dataSource.addColumn(MoveStudentColumns.getExtractStateColumn("Состояние проекта приказа"));
        dataSource.addColumn(MoveStudentColumns.getExtractPrintColumn(this, model.getProjectsModel().getSecModel().getPermission("orgUnit_print")));
        if (MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
            dataSource.addColumn(MoveStudentColumns.getExtractPrintPdfColumn(this, model.getProjectsModel().getSecModel().getPermission("orgUnit_printPdf")));

        if (!model.getOrgUnit().isArchival())
        {
            dataSource.addColumn(MoveStudentColumns.getExtractEditColumn(this, model.getProjectsModel().getSecModel().getPermission("orgUnit_edit")));
            dataSource.addColumn(MoveStudentColumns.getExtractDeleteColumn(this, model.getProjectsModel().getSecModel().getPermission("orgUnit_delete")));
        }

        dataSource.setOrder(IAbstractExtract.P_CREATE_DATE, OrderDirection.desc);
        model.getProjectsModel().setDataSource(dataSource);
    }

    private void prepareModularOrdersListDataSource(final Model model, IBusinessComponent component)
    {
        if (model.getModularOrdersdataSource() != null) return;

        DynamicListDataSource dataSource = new DynamicListDataSource(component, component1 -> {
            getDao().prepareModularOrdersDataSource(model);
        });

        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Дата формирования", OrderFlexBuilder.CREATE_DATE);
        linkColumn.setName(OrderFlexBuilder.CREATE_DATE);
        linkColumn.setFormatter(DateFormatter.DATE_FORMATTER_WITH_TIME);
        dataSource.addColumn(linkColumn);

        dataSource.addColumn(new SimpleColumn("Дата приказа", OrderFlexBuilder.COMMIT_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ приказа", OrderFlexBuilder.ORDER_NUMBER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", OrderFlexBuilder.ORDER_STATE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата проведения", OrderFlexBuilder.COMMIT_DATE_SYSTEM).setFormatter(DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во выписок", OrderFlexBuilder.EXTRACT_COUNT).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Исполнитель", OrderFlexBuilder.EXECUTOR).setClickable(false));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrintModularOrder", "Печать").setPermissionKey(model.getModularOrdersSecModel().getPermission("orgUnit_print")).setDisabledProperty("disabledPrint"));
        if (MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
            dataSource.addColumn(CommonBaseUtil.getPrintPdfColumn("onClickPrintModularOrderPdf", "Печать PDF").setPermissionKey(model.getModularOrdersSecModel().getPermission("orgUnit_printPdf")).setDisabledProperty("disabledPrint"));

        if (!model.getOrgUnit().isArchival())
        {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditModularOrder").setPermissionKey(model.getModularOrdersSecModel().getPermission("orgUnit_edit")).setDisabledProperty(AbstractStudentOrder.P_READONLY));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteModularOrder", "Удалить «{0}»?", IAbstractOrder.P_TITLE).setPermissionKey(model.getModularOrdersSecModel().getPermission("orgUnit_delete")).setDisabledProperty(AbstractStudentOrder.P_READONLY));
        }

        dataSource.setOrder(IAbstractOrder.P_COMMIT_DATE_SYSTEM, OrderDirection.desc);
        model.setModularOrdersdataSource(dataSource);
    }

    private void prepareExtractsListDataSource(final Model model, IBusinessComponent component)
    {
        if (model.getExtractsModel().getDataSource() != null) return;

        DynamicListDataSource<ModularStudentExtract> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareExtractsDataSource(model);
        });
        dataSource.addColumn(MoveStudentColumns.getCreateDateColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractOrderNumberColumn());
        dataSource.addColumn(MoveStudentColumns.getModularExtractNumberColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractCommitDateColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractCommitDateSystemColumn());
        dataSource.addColumn(MoveStudentColumns.getFullFioColumn());
        dataSource.addColumn(MoveStudentColumns.getFormativeOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getTerritorialOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getProducingOrgUnitColumn());
        MoveStudentColumns.addEduLevelTitleColumns(dataSource);
        dataSource.addColumn(MoveStudentColumns.getCourseColumn());
        dataSource.addColumn(MoveStudentColumns.getGroupColumn());
        dataSource.addColumn(MoveStudentColumns.getDevelopFormColumn());
        dataSource.addColumn(MoveStudentColumns.getCompensationTypeColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractTypeColumn("Тип приказа"));
        dataSource.addColumn(MoveStudentColumns.getExtractStateColumn("Состояние выписки"));
        dataSource.addColumn(MoveStudentColumns.getExtractPrintColumn(this, model.getExtractsModel().getSecModel().getPermission("orgUnit_print")));
        if (MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
            dataSource.addColumn(MoveStudentColumns.getExtractPrintPdfColumn(this, model.getExtractsModel().getSecModel().getPermission("orgUnit_printPdf")));
        dataSource.setOrder(IAbstractExtract.P_CREATE_DATE, OrderDirection.desc);
        model.getExtractsModel().setDataSource(dataSource);
    }

    private void prepareListOrdersListDataSource(final Model model, IBusinessComponent component)
    {
        if (model.getListOrdersdataSource() != null) return;
        boolean admin = UserContext.getInstance().getPrincipalContext().isAdmin();

        DynamicListDataSource dataSource = new DynamicListDataSource(component, component1 -> {
            getDao().prepareListOrdersDataSource(model);
        });
        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Дата формирования", IAbstractOrder.P_CREATE_DATE);
        linkColumn.setFormatter(DateFormatter.DATE_FORMATTER_WITH_TIME);
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Дата приказа", AbstractStudentOrder.P_COMMIT_DATE).setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ приказа", IAbstractOrder.P_NUMBER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", new String[]{IAbstractOrder.L_STATE, ICatalogItem.CATALOG_ITEM_TITLE}).setClickable(false));
        dataSource.addColumn(new SimpleColumn(IAbstractOrder.P_COMMIT_DATE_SYSTEM, "Дата проведения", IAbstractOrder.P_COMMIT_DATE_SYSTEM, DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип приказа", admin ? StudentListOrder.type().titleWithCode().s() : StudentListOrder.type().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Причина приказа", StudentListOrder.P_REASON_FULL_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во параграфов", AbstractStudentOrder.P_COUNT_PARAGRAPH).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Исполнитель", AbstractStudentOrder.P_EXECUTOR).setClickable(false));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrintListOrder", "Печать").setPermissionKey(model.getListOrdersSecModel().getPermission("orgUnit_print")).setDisabledProperty("disabledPrint"));
        if (MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
            dataSource.addColumn(CommonBaseUtil.getPrintPdfColumn("onClickPrintListOrderPdf", "Печать PDF").setPermissionKey(model.getListOrdersSecModel().getPermission("orgUnit_printPdf")).setDisabledProperty("disabledPrint"));

        if (!model.getOrgUnit().isArchival())
        {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditListOrder").setPermissionKey(model.getListOrdersSecModel().getPermission("orgUnit_edit")).setDisabledProperty(AbstractStudentOrder.P_READONLY));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteListOrder", "Удалить «{0}»?", IAbstractOrder.P_TITLE).setPermissionKey(model.getListOrdersSecModel().getPermission("orgUnit_delete")).setDisabledProperty(AbstractStudentOrder.P_READONLY));
        }

        dataSource.setOrder(IAbstractOrder.P_COMMIT_DATE_SYSTEM, OrderDirection.desc);
        model.setListOrdersdataSource(dataSource);
    }

    private void prepareListExtractsListDataSource(final Model model, IBusinessComponent component)
    {
        if (model.getListextractsModel().getDataSource() != null) return;

        DynamicListDataSource<ListStudentExtract> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListExtractsDataSource(model);
        });
        AbstractColumn linkColumn = new PublisherLinkColumn("Дата приказа", new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, AbstractStudentOrder.P_COMMIT_DATE}).setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public String getComponentName(IEntity entity)
            {
                StudentExtractType type = (StudentExtractType) ((IAbstractExtract) ((ViewWrapper) entity).getEntity()).getType();
                return MoveStudentUtils.getListExtractPubComponent(type);
            }
        }).setFormatter(new DateFormatter()
        {
            @Override
            public String format(Date date) {
                return date == null ? "не задана" : super.format(date);
            }
        });
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(MoveStudentColumns.getExtractOrderNumberColumn());
        dataSource.addColumn(MoveStudentColumns.getParagraphNumberColumn());
        dataSource.addColumn(MoveStudentColumns.getListExtractNumberColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractCommitDateSystemColumn());
        dataSource.addColumn(MoveStudentColumns.getFullFioColumn());
        dataSource.addColumn(MoveStudentColumns.getFormativeOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getTerritorialOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getProducingOrgUnitColumn());
        MoveStudentColumns.addEduLevelTitleColumns(dataSource);
        dataSource.addColumn(MoveStudentColumns.getCourseColumn());
        dataSource.addColumn(MoveStudentColumns.getGroupColumn());
        dataSource.addColumn(MoveStudentColumns.getDevelopFormColumn());
        dataSource.addColumn(MoveStudentColumns.getCompensationTypeColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractOrderTypeColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractTypeColumn("Тип выписки"));
        dataSource.addColumn(MoveStudentColumns.getListExtractReasonColumn("Причина приказа"));
        dataSource.addColumn(MoveStudentColumns.getExtractStateColumn("Состояние выписки"));

        IndicatorColumn printColumn = (IndicatorColumn) MoveStudentColumns.getExtractPrintColumn(this, model.getListextractsModel().getSecModel().getPermission("orgUnit_printExtract"));
        printColumn.setDisabledProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY);
        printColumn.setListener(getClass().getPackage().getName() + ":onClickListExtractPrint");
        dataSource.addColumn(printColumn);

        if (MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
        {
            IndicatorColumn printPdfColumn = (IndicatorColumn) MoveStudentColumns.getExtractPrintPdfColumn(this, model.getListextractsModel().getSecModel().getPermission("orgUnit_printExtractPdf"));
            printPdfColumn.setDisabledProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY);
            printPdfColumn.setListener(getClass().getPackage().getName() + ":onClickListExtractPrintPdf");
            dataSource.addColumn(printPdfColumn);
        }

        dataSource.setOrder(IAbstractExtract.P_CREATE_DATE, OrderDirection.desc);
        model.getListextractsModel().setDataSource(dataSource);
    }

    private void prepareOtherOrdersListDataSource(final Model model, IBusinessComponent component)
    {
        if (model.getOtherOrdersModel().getDataSource() != null) return;

        DynamicListDataSource<OtherStudentExtract> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareOtherOrdersDataSource(model);
        });
        dataSource.addColumn(MoveStudentColumns.getCreateDateColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractOrderNumberColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractCommitDateColumn());
        dataSource.addColumn(MoveStudentColumns.getFullFioColumn());
        dataSource.addColumn(MoveStudentColumns.getFormativeOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getTerritorialOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getProducingOrgUnitColumn());
        MoveStudentColumns.addEduLevelTitleColumns(dataSource);
        dataSource.addColumn(MoveStudentColumns.getCourseColumn());
        dataSource.addColumn(MoveStudentColumns.getGroupColumn());
        dataSource.addColumn(MoveStudentColumns.getDevelopFormColumn());
        dataSource.addColumn(MoveStudentColumns.getCompensationTypeColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractTypeColumn("Тип приказа"));
        dataSource.addColumn(MoveStudentColumns.getExtractOrderStateColumn());
        dataSource.addColumn(new SimpleColumn("Примечание", OtherStudentExtract.comment().s()).setClickable(false));

        IndicatorColumn printColumn = (IndicatorColumn) MoveStudentColumns.getOtherOrderPrintColumn(this, model.getOtherOrdersModel().getSecModel().getPermission("orgUnit_print"));
        printColumn.setDisabledProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY);
        dataSource.addColumn(printColumn);

        if (!model.getOrgUnit().isArchival())
        {
            dataSource.addColumn(MoveStudentColumns.getOtherOrderEditColumn(this, model.getOtherOrdersModel().getSecModel().getPermission("orgUnit_edit")));
            dataSource.addColumn(MoveStudentColumns.getOtherOrderDeleteColumn(this, model.getOtherOrdersModel().getSecModel().getPermission("orgUnit_delete")));
        }

        dataSource.setOrder(IAbstractExtract.P_CREATE_DATE, OrderDirection.desc);
        model.getOtherOrdersModel().setDataSource(dataSource);
    }


    // filter events

    public void onClickIndividualOrdersSearch(IBusinessComponent component)
    {
        ExtractListModel model = getModel(component).getIndividualOrdersModel();

        UniBaseUtils.validateDatesPeriod(model.getSettings(), "createDateFrom", "createDateTo");
        UniBaseUtils.validateDatesPeriod(model.getSettings(), "commitDateFrom", "commitDateTo");

        if (!UserContext.getInstance().getErrorCollector().isHasFieldErrors())
        {
            DataSettingsFacade.saveSettings(model.getSettings());
            model.getDataSource().refresh();
        }
    }

    public void onClickIndividualOrdersClear(IBusinessComponent component)
    {
        getModel(component).getIndividualOrdersModel().getSettings().clear();
        onClickIndividualOrdersSearch(component);
    }

    public void onClickProjectsSearch(IBusinessComponent component)
    {
        ExtractListModel model = getModel(component).getProjectsModel();

        UniBaseUtils.validateDatesPeriod(model.getSettings(), "createDateFrom", "createDateTo");

        if (!UserContext.getInstance().getErrorCollector().isHasFieldErrors())
        {
            DataSettingsFacade.saveSettings(model.getSettings());
            model.getDataSource().refresh();
        }
    }

    public void onClickProjectsClear(IBusinessComponent component)
    {
        getModel(component).getProjectsModel().getSettings().clear();
        onClickProjectsSearch(component);
    }

    public void onClickModularOrdersSearch(IBusinessComponent component)
    {
        Model model = getModel(component);

        UniBaseUtils.validateDatesPeriod(model.getModularOrdersSettings(), "commitDateFrom", "commitDateTo");
        UniBaseUtils.validateDatesPeriod(model.getModularOrdersSettings(), "createDateFrom", "createDateTo");
        UniBaseUtils.validateDatesPeriod(model.getModularOrdersSettings(), "commitDateSystemFrom", "commitDateSystemTo");

        if (!UserContext.getInstance().getErrorCollector().isHasFieldErrors())
        {
            DataSettingsFacade.saveSettings(model.getModularOrdersSettings());
            model.getModularOrdersdataSource().refresh();
        }
    }

    public void onClickModularOrdersClear(IBusinessComponent component)
    {
        getModel(component).getModularOrdersSettings().clear();
        onClickModularOrdersSearch(component);
    }

    public void onClickExtractsSearch(IBusinessComponent component)
    {
        ExtractListModel model = getModel(component).getExtractsModel();

        UniBaseUtils.validateDatesPeriod(model.getSettings(), "commitDateFrom", "commitDateTo");
        UniBaseUtils.validateDatesPeriod(model.getSettings(), "createDateFrom", "createDateTo");
        UniBaseUtils.validateDatesPeriod(model.getSettings(), "commitDateSystemFrom", "commitDateSystemTo");

        if (!UserContext.getInstance().getErrorCollector().isHasFieldErrors())
        {
            DataSettingsFacade.saveSettings(model.getSettings());
            model.getDataSource().refresh();
        }
    }

    public void onClickExtractsClear(IBusinessComponent component)
    {
        getModel(component).getExtractsModel().getSettings().clear();
        onClickExtractsSearch(component);
    }

    public void onClickListOrdersSearch(IBusinessComponent component)
    {
        Model model = getModel(component);

        UniBaseUtils.validateDatesPeriod(model.getListOrdersSettings(), "commitDateFrom", "commitDateTo");
        UniBaseUtils.validateDatesPeriod(model.getListOrdersSettings(), "createDateFrom", "createDateTo");
        UniBaseUtils.validateDatesPeriod(model.getListOrdersSettings(), "commitDateSystemFrom", "commitDateSystemTo");

        if (!UserContext.getInstance().getErrorCollector().isHasFieldErrors())
        {
            DataSettingsFacade.saveSettings(model.getListOrdersSettings());
            model.getListOrdersdataSource().refresh();
        }
    }

    public void onClickListOrdersClear(IBusinessComponent component)
    {
        getModel(component).getListOrdersSettings().clear();
        onClickListOrdersSearch(component);
    }

    public void onClickListextractsSearch(IBusinessComponent component)
    {
        ExtractListModel model = getModel(component).getListextractsModel();

        UniBaseUtils.validateDatesPeriod(model.getSettings(), "commitDateFrom", "commitDateTo");
        UniBaseUtils.validateDatesPeriod(model.getSettings(), "createDateFrom", "createDateTo");
        UniBaseUtils.validateDatesPeriod(model.getSettings(), "commitDateSystemFrom", "commitDateSystemTo");

        if (!UserContext.getInstance().getErrorCollector().isHasFieldErrors())
        {
            DataSettingsFacade.saveSettings(model.getSettings());
            model.getDataSource().refresh();
        }
    }

    public void onClickListextractsClear(IBusinessComponent component)
    {
        getModel(component).getListextractsModel().getSettings().clear();
        onClickListextractsSearch(component);
    }

    public void onClickOtherOrdersSearch(IBusinessComponent component)
    {
        ExtractListModel model = getModel(component).getOtherOrdersModel();

        UniBaseUtils.validateDatesPeriod(model.getSettings(), "createDateFrom", "createDateTo");
        UniBaseUtils.validateDatesPeriod(model.getSettings(), "commitDateFrom", "commitDateTo");

        if (!UserContext.getInstance().getErrorCollector().isHasFieldErrors())
        {
            DataSettingsFacade.saveSettings(model.getSettings());
            model.getDataSource().refresh();
        }
    }

    public void onClickOtherOrdersClear(IBusinessComponent component)
    {
        getModel(component).getOtherOrdersModel().getSettings().clear();
        onClickOtherOrdersSearch(component);
    }

    // event listeners

    public void onClickIndividualOrderPrint(IBusinessComponent component)
    {
        ModularStudentExtract extract = DataAccessServices.dao().get((Long) component.getListenerParameter());
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_PRINT, new ParametersMap()
                .add("orderId", extract.getParagraph().getOrder().getId())
        ));
    }

    public void onClickIndividualOrderPrintPdf(IBusinessComponent component)
    {
        ModularStudentExtract extract = DataAccessServices.dao().get((Long) component.getListenerParameter());
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_PRINT, new ParametersMap()
                .add("orderId", extract.getParagraph().getOrder().getId())
                .add("printPdf", Boolean.TRUE)
        ));
    }

    public void onClickExtractPrint(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.MODULAR_STUDENT_EXTRACT_PRINT, new ParametersMap()
                .add("extractId", component.getListenerParameter())
        ));
    }

    public void onClickExtractPrintPdf(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.MODULAR_STUDENT_EXTRACT_PRINT, new ParametersMap()
                .add("extractId", component.getListenerParameter())
                .add("printPdf", Boolean.TRUE)
        ));
    }

    public void onClickIndividualOrderEdit(IBusinessComponent component)
    {
        Long extractId = component.getListenerParameter();
        component.createChildRegion("moveStudent", new ComponentActivator(MoveStudentUtils.getModularExtractAddEditComponentNew(extractId), new ParametersMap().add("extractId", extractId)));
    }

    public void onClickExtractEdit(IBusinessComponent component)
    {
        component.createChildRegion("projectsScope", new ComponentActivator(MoveStudentUtils.getModularExtractAddEditComponentNew((Long) component.getListenerParameter()), new ParametersMap()
                .add("extractId", component.getListenerParameter())
        ));
    }

    public void onClickOtherOrderEdit(IBusinessComponent component)
    {
        Long extractId = component.getListenerParameter();
        component.createChildRegion("moveStudent", new ComponentActivator(IMoveStudentComponents.STUDENT_OTHER_ORDER_ADD_EDIT, new ParametersMap().add("extractId", extractId)));
    }

    public void onClickIndividualOrderDelete(IBusinessComponent component)
    {
        ModularStudentExtract extract = getDao().get(ModularStudentExtract.class, (Long) component.getListenerParameter());
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(extract.getParagraph().getOrder());
    }

    public void onClickExtractDelete(IBusinessComponent component)
    {
        getDao().deleteModularExtract(component);
    }

    public void onClickOtherOrderDelete(IBusinessComponent component)
    {
        OtherStudentExtract extract = getDao().get(OtherStudentExtract.class, (Long) component.getListenerParameter());
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(extract.getParagraph().getOrder());
    }

    public void onClickPrintModularOrder(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_PRINT, new ParametersMap()
                .add("orderId", component.getListenerParameter())
        ));
    }

    public void onClickPrintModularOrderPdf(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_PRINT, new ParametersMap()
                .add("orderId", component.getListenerParameter())
                .add("printPdf", Boolean.TRUE)
        ));
    }

    public void onClickEditModularOrder(IBusinessComponent component)
    {
        StudentModularOrder order = DataAccessServices.dao().get((Long) component.getListenerParameter());
        if (MoveStudentDaoFacade.getMoveStudentDao().isModularOrderIndividual(order))
        {
            AbstractStudentExtract extract = MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(order.getId());
            component.createChildRegion("modularOrdersScope", new ComponentActivator(MoveStudentUtils.getModularExtractAddEditComponent(extract.getType()), new ParametersMap()
                    .add("extractId", extract.getId())
            ));
            return;
        }

        component.createChildRegion("modularOrdersScope", new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_EDIT, new ParametersMap()
                .add("orderId", component.getListenerParameter())
        ));
    }

    public void onClickDeleteModularOrder(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }

    public void onClickAddModularOrder(IBusinessComponent component)
    {
        component.createChildRegion("modularOrdersScope", new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_ADD, new ParametersMap()
                .add("orgUnitId", getModel(component).getOrgUnitId())
        ));
    }

    public void onClickAddListOrder(IBusinessComponent component)
    {
        component.createChildRegion("listOrdersScope", new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_ORDER_ADD, new ParametersMap()
                .add("orgUnitId", getModel(component).getOrgUnitId())
                .add("orderId", null)
        ));
    }

    public void onClickAddExtract(IBusinessComponent component)
    {
        component.createChildRegion("projectsScope", new ComponentActivator(MoveStudentUtils.getOrgUnitModularExtractAddEditComponent(), new ParametersMap()
                .add("orgUnitId", getModel(component).getOrgUnitId())
        ));
    }

    public void onClickEditListOrder(IBusinessComponent component)
    {
        StudentListOrder order = getDao().getNotNull((Long) component.getListenerParameter());

        component.createChildRegion("listOrdersScope", new ComponentActivator(MoveStudentUtils.getListOrderAddEditComponent(order.getType()), new ParametersMap()
                .add(AbstractListOrderAddEditModel.ORG_UNIT_ID, null)
                .add(AbstractListOrderAddEditModel.ORDER_ID, order.getId())
                .add(AbstractListOrderAddEditModel.ORDER_TYPE_ID, null)
        ));
    }

    public void onClickPrintListOrder(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_ORDER_PRINT, new ParametersMap()
                .add("orderId", component.getListenerParameter())
        ));
    }

    public void onClickPrintListOrderPdf(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_ORDER_PRINT, new ParametersMap()
                .add("orderId", component.getListenerParameter())
                .add("printPdf", Boolean.TRUE)
        ));
    }

    public void onClickDeleteListOrder(IBusinessComponent component)
    {
        getDao().deleteListOrder((Long) component.getListenerParameter());
    }

    public void onClickListExtractPrint(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_EXTRACT_PRINT, new ParametersMap()
                .add("extractId", component.getListenerParameter())
        ));
    }

    public void onClickListExtractPrintPdf(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_EXTRACT_PRINT, new ParametersMap()
                .add("extractId", component.getListenerParameter())
                .add("printPdf", Boolean.TRUE)
        ));
    }

    public void onClickOtherOrderPrint(IBusinessComponent component)
    {
        OtherStudentExtract extract = getDao().getNotNull(OtherStudentExtract.class, component.<Long>getListenerParameter());
        StudentOtherOrderTextRelation rel = UniDaoFacade.getCoreDao().get(StudentOtherOrderTextRelation.class, StudentOtherOrderTextRelation.order().s(), extract.getParagraph().getOrder().getId());
        if (null == rel) throw new ApplicationException("К данному приказу не прикреплена печатная форма.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(rel.getFileType()).fileName(rel.getFileName()).document(rel.getContent()), false);
    }
}