/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.utils.system;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.SystemUtil;

import ru.tandemservice.uni.util.system.UniSystemUtils;

/**
 * Генератор печатных форм
 * <p/>
 * WORD разрывает метки! ХЗ как править в общем случае поэтому сгенерим все печ. формы (:
 *
 * @author vip_delete
 * @since 09.12.2008
 */
public class TemplatesGenerator
{
    private final static String EXTRACT_HEADER = "@extractHeader.txt";
    private final static String EXTRACT_TOP = "@extractTop.txt";
    private final static String EXTRACT_BOTTOM = "@extractBottom.txt";
    private final static String MODULAR_ORDER = "@modularOrder.txt";

    public static void main(String[] args) throws IOException
    {
        File modules = UniSystemUtils.getModulesDir();
        String moduleOwner = System.getProperty("moduleOwner");
        if (StringUtils.isEmpty(moduleOwner))
            throw new RuntimeException("Plz specify printforms moduleOwner: for example 'movestudent'");

        String printformsPath = modules.getAbsolutePath() + ("movestudent".equals(moduleOwner) ? "/modules/" : ("/projects/" + moduleOwner + "/")) + moduleOwner + "/src/main/java/ru/tandemservice/" + moduleOwner + "/utils/system/templates/printforms/";
        String templatesPath = modules.getAbsolutePath() + ("movestudent".equals(moduleOwner) ? "/modules/" : ("/projects/" + moduleOwner + "/")) + moduleOwner + "/src/main/resources/" + moduleOwner + "/templates/modularorder/";

        byte[] extractHeaderUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + EXTRACT_HEADER));
        byte[] extractTopUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + EXTRACT_TOP));
        byte[] extractBottomUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + EXTRACT_BOTTOM));
        byte[] modularOrderUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + MODULAR_ORDER));

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // create modularorder

        FileUtils.writeByteArrayToFile(new File(templatesPath + "/modularOrder.rtf"), getRtfData(modularOrderUTF8));

        // create extracts
        File path = new File(printformsPath);
        for (File file : path.listFiles())
        {
            if (file.getName().endsWith(".txt") && !file.getName().startsWith("@"))
            {
                // create extract printform
                out.reset();
                out.write(getRtfData(extractHeaderUTF8));
                out.write(getRtfData(extractTopUTF8));
                out.write(getRtfData(FileUtils.readFileToByteArray(file)));
                out.write(getRtfData(extractBottomUTF8));
                out.write("\\par}".getBytes());
                File newFile = new File(templatesPath + "/" + FilenameUtils.getBaseName(file.getName()) + ".rtf");
                FileUtils.writeByteArrayToFile(newFile, out.toByteArray());
                 System.out.println("[+] " + newFile.getAbsolutePath());
                // create extract in modular order print form

                out.reset();
                out.write(getRtfData(extractHeaderUTF8));
                out.write(getRtfData(FileUtils.readFileToByteArray(file)));
                out.write("\\par}".getBytes());
                newFile = new File(templatesPath + "/modular_" + FilenameUtils.getBaseName(file.getName()) + ".rtf");
                FileUtils.writeByteArrayToFile(newFile, out.toByteArray());
                System.out.println("[+] " + newFile.getAbsolutePath());
            }
        }
    }

    /**
     * My_Param} -> {\field{\*\fldinst{COMMENTS My_Param \\* MERGEFORMAT}}{\fldrslt{My_Param}}}
     * \n -> \par
     *
     * @param readableDataUTF - массив байт в кодировке UTF-8 шаблона выписки
     * @return массив получившихся байт после подстановки параметров
     * @throws IOException если будет ошибка записи в ByteArrayOutputStream
     */
    private static byte[] getRtfData(byte[] readableDataUTF) throws IOException
    {
        byte[] readableData = new String(readableDataUTF, "UTF-8").getBytes("CP1251");

        ByteArrayOutputStream result = new ByteArrayOutputStream();
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        for (int i = 0; i < readableData.length; i++)
        {
            byte c = readableData[i];
            if (c < 0)
            {
                // русские символы превращаются в rtf-escape-последовательность
                result.write(new byte[]{'\\', '\''});
                result.write(SystemUtil.getBytes(c));
            } else if (c == '{')
            {
                i++;
                boolean isParameter = true;
                for (; i < readableData.length && readableData[i] != '}'; i++)
                {
                    byte b = readableData[i];
                    isParameter = isParameter && ((b >= 'A' && b <= 'Z') || (b >= 'a' && b <= 'z') || b == '_');
                    buffer.write(b);
                }
                if (isParameter)
                    result.write(("{\\field{\\*\\fldinst{COMMENTS " + buffer.toString() + "}}{\\fldrslt{" + buffer.toString() + "}}}").getBytes());
                else
                {
                    result.write('{');
                    result.write(buffer.toByteArray());
                    result.write('}');
                }
                buffer.reset();
            } else
                result.write(c);
        }
        return result.toByteArray();
    }
}
