package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.TransferProfileGroupExtStuExtractGen;

import java.util.Date;

/**
 * О переводе со специализации на специализацию (с профиля на профиль) и перераспределении в группу
 */
public class TransferProfileGroupExtStuExtract extends TransferProfileGroupExtStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}