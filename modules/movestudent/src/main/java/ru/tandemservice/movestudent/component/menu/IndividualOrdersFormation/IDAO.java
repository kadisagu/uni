/* $Id$ */
package ru.tandemservice.movestudent.component.menu.IndividualOrdersFormation;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Dmitry Seleznev
 * @since 31.10.2012
 */
public interface IDAO extends IUniDao<Model>
{
}