package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.RePassDiscComissStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О пересдаче дисциплин учебного плана комиссии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RePassDiscComissStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.RePassDiscComissStuExtract";
    public static final String ENTITY_NAME = "rePassDiscComissStuExtract";
    public static final int VERSION_HASH = -480663384;
    private static IEntityMeta ENTITY_META;

    public static final String P_DISCIPLINE = "discipline";
    public static final String P_ACTION_TYPE = "actionType";
    public static final String P_ACTION_DATE_TIME = "actionDateTime";
    public static final String P_ACTION_PLACE = "actionPlace";

    private String _discipline;     // Наименование дисциплины
    private String _actionType;     // Форма промежуточного контроля
    private Date _actionDateTime;     // Дата и время пересдачи
    private String _actionPlace;     // Номер аудитории

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Наименование дисциплины. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Наименование дисциплины. Свойство не может быть null.
     */
    public void setDiscipline(String discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Форма промежуточного контроля. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getActionType()
    {
        return _actionType;
    }

    /**
     * @param actionType Форма промежуточного контроля. Свойство не может быть null.
     */
    public void setActionType(String actionType)
    {
        dirty(_actionType, actionType);
        _actionType = actionType;
    }

    /**
     * @return Дата и время пересдачи. Свойство не может быть null.
     */
    @NotNull
    public Date getActionDateTime()
    {
        return _actionDateTime;
    }

    /**
     * @param actionDateTime Дата и время пересдачи. Свойство не может быть null.
     */
    public void setActionDateTime(Date actionDateTime)
    {
        dirty(_actionDateTime, actionDateTime);
        _actionDateTime = actionDateTime;
    }

    /**
     * @return Номер аудитории. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getActionPlace()
    {
        return _actionPlace;
    }

    /**
     * @param actionPlace Номер аудитории. Свойство не может быть null.
     */
    public void setActionPlace(String actionPlace)
    {
        dirty(_actionPlace, actionPlace);
        _actionPlace = actionPlace;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RePassDiscComissStuExtractGen)
        {
            setDiscipline(((RePassDiscComissStuExtract)another).getDiscipline());
            setActionType(((RePassDiscComissStuExtract)another).getActionType());
            setActionDateTime(((RePassDiscComissStuExtract)another).getActionDateTime());
            setActionPlace(((RePassDiscComissStuExtract)another).getActionPlace());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RePassDiscComissStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RePassDiscComissStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new RePassDiscComissStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "discipline":
                    return obj.getDiscipline();
                case "actionType":
                    return obj.getActionType();
                case "actionDateTime":
                    return obj.getActionDateTime();
                case "actionPlace":
                    return obj.getActionPlace();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "discipline":
                    obj.setDiscipline((String) value);
                    return;
                case "actionType":
                    obj.setActionType((String) value);
                    return;
                case "actionDateTime":
                    obj.setActionDateTime((Date) value);
                    return;
                case "actionPlace":
                    obj.setActionPlace((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "discipline":
                        return true;
                case "actionType":
                        return true;
                case "actionDateTime":
                        return true;
                case "actionPlace":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "discipline":
                    return true;
                case "actionType":
                    return true;
                case "actionDateTime":
                    return true;
                case "actionPlace":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "discipline":
                    return String.class;
                case "actionType":
                    return String.class;
                case "actionDateTime":
                    return Date.class;
                case "actionPlace":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RePassDiscComissStuExtract> _dslPath = new Path<RePassDiscComissStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RePassDiscComissStuExtract");
    }
            

    /**
     * @return Наименование дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RePassDiscComissStuExtract#getDiscipline()
     */
    public static PropertyPath<String> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Форма промежуточного контроля. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RePassDiscComissStuExtract#getActionType()
     */
    public static PropertyPath<String> actionType()
    {
        return _dslPath.actionType();
    }

    /**
     * @return Дата и время пересдачи. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RePassDiscComissStuExtract#getActionDateTime()
     */
    public static PropertyPath<Date> actionDateTime()
    {
        return _dslPath.actionDateTime();
    }

    /**
     * @return Номер аудитории. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RePassDiscComissStuExtract#getActionPlace()
     */
    public static PropertyPath<String> actionPlace()
    {
        return _dslPath.actionPlace();
    }

    public static class Path<E extends RePassDiscComissStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<String> _discipline;
        private PropertyPath<String> _actionType;
        private PropertyPath<Date> _actionDateTime;
        private PropertyPath<String> _actionPlace;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Наименование дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RePassDiscComissStuExtract#getDiscipline()
     */
        public PropertyPath<String> discipline()
        {
            if(_discipline == null )
                _discipline = new PropertyPath<String>(RePassDiscComissStuExtractGen.P_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Форма промежуточного контроля. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RePassDiscComissStuExtract#getActionType()
     */
        public PropertyPath<String> actionType()
        {
            if(_actionType == null )
                _actionType = new PropertyPath<String>(RePassDiscComissStuExtractGen.P_ACTION_TYPE, this);
            return _actionType;
        }

    /**
     * @return Дата и время пересдачи. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RePassDiscComissStuExtract#getActionDateTime()
     */
        public PropertyPath<Date> actionDateTime()
        {
            if(_actionDateTime == null )
                _actionDateTime = new PropertyPath<Date>(RePassDiscComissStuExtractGen.P_ACTION_DATE_TIME, this);
            return _actionDateTime;
        }

    /**
     * @return Номер аудитории. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RePassDiscComissStuExtract#getActionPlace()
     */
        public PropertyPath<String> actionPlace()
        {
            if(_actionPlace == null )
                _actionPlace = new PropertyPath<String>(RePassDiscComissStuExtractGen.P_ACTION_PLACE, this);
            return _actionPlace;
        }

        public Class getEntityClass()
        {
            return RePassDiscComissStuExtract.class;
        }

        public String getEntityName()
        {
            return "rePassDiscComissStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
