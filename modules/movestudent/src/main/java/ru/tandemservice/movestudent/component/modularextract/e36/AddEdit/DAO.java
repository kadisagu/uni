/* $Id: DAO.java.vm 6933 2009-03-07 05:41:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e36.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ReEducationStuExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 23.12.2009
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<ReEducationStuExtract, Model>
{
    @Override
    protected ReEducationStuExtract createNewInstance()
    {
        return new ReEducationStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setEduModel(MoveStudentDaoFacade.getCommonExtractUtil().createEduModel(model.isAddForm(), model.getExtract(), true));
        model.getEduModel().setCompensationType(model.getExtract().getEntity().getCompensationType());
    }

    @Override
    public void update(Model model)
    {
        //save rollback data
        MoveStudentDaoFacade.getCommonExtractUtil().update(model.getEduModel(), model.getExtract());
        super.update(model);
    }

}
