/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e60.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.ReEducationFGOSStuExtract;
import ru.tandemservice.uni.entity.catalog.Course;

import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 24.08.2012
 */
public class Model extends CommonModularStudentExtractAddEditModel<ReEducationFGOSStuExtract>
{
    private CommonExtractModel _eduModel;
    private List<Course> _prevCourseList;
    private ISelectModel _educationYearsList;

    public CommonExtractModel getEduModel()
    {
        return _eduModel;
    }

    public void setEduModel(CommonExtractModel eduModel)
    {
        _eduModel = eduModel;
    }

    public List<Course> getPrevCourseList()
    {
        return _prevCourseList;
    }

    public void setPrevCourseList(List<Course> prevCourseList)
    {
        _prevCourseList = prevCourseList;
    }

    public ISelectModel getEducationYearsList()
    {
        return _educationYearsList;
    }

    public void setEducationYearsList(ISelectModel educationYearsList)
    {
        _educationYearsList = educationYearsList;
    }
}