package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.PrescheduleSessionPassStuExtract;
import ru.tandemservice.uni.entity.catalog.SessionType;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О досрочной сдаче сессии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PrescheduleSessionPassStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.PrescheduleSessionPassStuExtract";
    public static final String ENTITY_NAME = "prescheduleSessionPassStuExtract";
    public static final int VERSION_HASH = 1997282203;
    private static IEntityMeta ENTITY_META;

    public static final String L_SESSION_TYPE = "sessionType";
    public static final String L_SESSION_YEAR = "sessionYear";

    private SessionType _sessionType;     // Виды сессий студентов
    private EducationYear _sessionYear;     // Учебный год сессии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Виды сессий студентов. Свойство не может быть null.
     */
    @NotNull
    public SessionType getSessionType()
    {
        return _sessionType;
    }

    /**
     * @param sessionType Виды сессий студентов. Свойство не может быть null.
     */
    public void setSessionType(SessionType sessionType)
    {
        dirty(_sessionType, sessionType);
        _sessionType = sessionType;
    }

    /**
     * @return Учебный год сессии. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getSessionYear()
    {
        return _sessionYear;
    }

    /**
     * @param sessionYear Учебный год сессии. Свойство не может быть null.
     */
    public void setSessionYear(EducationYear sessionYear)
    {
        dirty(_sessionYear, sessionYear);
        _sessionYear = sessionYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof PrescheduleSessionPassStuExtractGen)
        {
            setSessionType(((PrescheduleSessionPassStuExtract)another).getSessionType());
            setSessionYear(((PrescheduleSessionPassStuExtract)another).getSessionYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PrescheduleSessionPassStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PrescheduleSessionPassStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new PrescheduleSessionPassStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "sessionType":
                    return obj.getSessionType();
                case "sessionYear":
                    return obj.getSessionYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "sessionType":
                    obj.setSessionType((SessionType) value);
                    return;
                case "sessionYear":
                    obj.setSessionYear((EducationYear) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "sessionType":
                        return true;
                case "sessionYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "sessionType":
                    return true;
                case "sessionYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "sessionType":
                    return SessionType.class;
                case "sessionYear":
                    return EducationYear.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PrescheduleSessionPassStuExtract> _dslPath = new Path<PrescheduleSessionPassStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PrescheduleSessionPassStuExtract");
    }
            

    /**
     * @return Виды сессий студентов. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.PrescheduleSessionPassStuExtract#getSessionType()
     */
    public static SessionType.Path<SessionType> sessionType()
    {
        return _dslPath.sessionType();
    }

    /**
     * @return Учебный год сессии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.PrescheduleSessionPassStuExtract#getSessionYear()
     */
    public static EducationYear.Path<EducationYear> sessionYear()
    {
        return _dslPath.sessionYear();
    }

    public static class Path<E extends PrescheduleSessionPassStuExtract> extends ModularStudentExtract.Path<E>
    {
        private SessionType.Path<SessionType> _sessionType;
        private EducationYear.Path<EducationYear> _sessionYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Виды сессий студентов. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.PrescheduleSessionPassStuExtract#getSessionType()
     */
        public SessionType.Path<SessionType> sessionType()
        {
            if(_sessionType == null )
                _sessionType = new SessionType.Path<SessionType>(L_SESSION_TYPE, this);
            return _sessionType;
        }

    /**
     * @return Учебный год сессии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.PrescheduleSessionPassStuExtract#getSessionYear()
     */
        public EducationYear.Path<EducationYear> sessionYear()
        {
            if(_sessionYear == null )
                _sessionYear = new EducationYear.Path<EducationYear>(L_SESSION_YEAR, this);
            return _sessionYear;
        }

        public Class getEntityClass()
        {
            return PrescheduleSessionPassStuExtract.class;
        }

        public String getEntityName()
        {
            return "prescheduleSessionPassStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
