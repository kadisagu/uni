/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MovestudentSystemAction.logic.script;

import org.hibernate.Session;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.base.bo.MovestudentSystemAction.logic.ScriptResultData;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.util.groovy.IScriptInstance;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 03.07.2015
 */
public interface IMovestudentUnloadScript extends IScriptInstance
{
    ScriptResultData exportStudentsData(Session session, OrgUnit formativeOrgUnit, boolean isAllStudents, boolean onlyFullData, List<StudentCustomStateCI> studentCustomStates, int counter);
}
