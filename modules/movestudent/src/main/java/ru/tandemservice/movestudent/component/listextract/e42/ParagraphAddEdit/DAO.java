/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e42.ParagraphAddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.TerritorialTransferExtStuListExtract;
import ru.tandemservice.movestudent.utils.ExtEducationLevelsHighSchoolSelectModel;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 28.08.2013
 */
public class DAO extends AbstractListParagraphAddEditDAO<TerritorialTransferExtStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        TerritorialTransferExtStuListExtract firstExtract = model.getFirstExtract();
        if (null != firstExtract)
        {
            if (null != model.getParagraphId())
            {
                model.setCompensationTypeNew(firstExtract.getCompensationTypeNew());
                model.setEducationLevelsHighSchool(firstExtract.getEducationOrgUnitNew().getEducationLevelHighSchool());
            }
            model.setCourse(firstExtract.getCourse());
            model.setGroupOld(firstExtract.getGroupOld());
            model.setGroupNew(firstExtract.getGroupNew());
            model.setTerritorialOrgUnitNew(firstExtract.getGroupNew().getEducationOrgUnit().getTerritorialOrgUnit());
            model.setCompensationTypeOld(firstExtract.getCompensationTypeOld());
            model.setChangeEduPlan(firstExtract.isChangeEduPlan());
            if (firstExtract.isChangeEduPlan())
                model.setChangeEduPlanDeadlineDate(firstExtract.getChangeEduPlanDeadlineDate());

        }
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));

        model.setGroupOldListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));
        model.setTerritorialOrgUnitsListModel(new UniSimpleAutocompleteModel()
        {
            private DQLSelectBuilder createBuilder()
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, "g")
                        .joinPath(DQLJoinType.inner, Group.educationOrgUnit().fromAlias("g"), "eou")
                        .column(property(EducationOrgUnit.territorialOrgUnit().fromAlias("eou")));

                if (model.getCourse() == null || model.getGroupOld() == null)
                {
                    return builder.where(nothing());
                }

                EducationLevels educationLevels = EducationOrgUnitUtil.getParentLevel(model.getGroupOld().getEducationOrgUnit().getEducationLevelHighSchool());

                UniDaoFacade.getEducationLevelDao().applyUsedFilterForEducationOrgUnit(builder, "eou");
                MoveStudentDaoFacade.getMoveStudentDao().addFormativeOrgUnitToBuilder(builder, model.getFormativeOrgUnit());

                return builder
                        .where(or(
                                eq(property(EducationOrgUnit.educationLevelHighSchool().educationLevel().fromAlias("eou")), value(educationLevels)),
                                eq(property(EducationOrgUnit.educationLevelHighSchool().educationLevel().parentLevel().fromAlias("eou")), value(educationLevels))))
//                        .where(eq(property(EducationOrgUnit.formativeOrgUnit().fromAlias("eou")), value(model.getFormativeOrgUnit())))
                        .where(eq(property(EducationOrgUnit.developForm().fromAlias("eou")), value(model.getDevelopForm())))
                        .where(eq(property(EducationOrgUnit.developCondition().fromAlias("eou")), value(model.getDevelopCondition())))
                        .where(eq(property(EducationOrgUnit.developTech().fromAlias("eou")), value(model.getDevelopTech())))
                        .where(eq(property(EducationOrgUnit.developPeriod().fromAlias("eou")), value(model.getDevelopPeriod())))
                        .where(ne(property(EducationOrgUnit.territorialOrgUnit().fromAlias("eou")), value(model.getGroupOld().getEducationOrgUnit().getTerritorialOrgUnit())));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                DQLSelectBuilder builder = createBuilder();
                builder.where(eq(property(EducationOrgUnit.territorialOrgUnit().id().fromAlias("eou")), commonValue(primaryKey)));
                return builder.createStatement(getSession()).uniqueResult();
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                OrgUnit orgUnit = (OrgUnit) value;
                return orgUnit.getTerritorialTitle();
            }

            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = createBuilder().distinct()
                        .where(like(DQLFunctions.upper(property(EducationOrgUnit.territorialOrgUnit().territorialFullTitle().fromAlias("eou"))), value(CoreStringUtils.escapeLike(filter))))
                        .order(property(EducationOrgUnit.territorialOrgUnit().territorialFullTitle().fromAlias("eou")));

                int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                if (count > MAX_ROWS)
                {
                    builder.top(MAX_ROWS);
                }

                return new ListResult<>(builder.createStatement(getSession()).list(), count);
            }
        });

        model.setGroupNewListModel(
                new GroupSelectModel(model, model)
                        .extModel(new OldParentEduLevelExtModel(model))
                        .showChildLevels(true)
                        .dependFromFUTS(true)
        );

        model.setSpecializationsListModel(new ExtEducationLevelsHighSchoolSelectModel(model, model)
        {
            @Override
            protected boolean isNeedRequest()
            {
                return super.isNeedRequest() && model.getCourse() != null && model.getGroupOld() != null && model.getTerritorialOrgUnitNew() != null && model.getGroupNew() != null;
            }
        }.dependFromFUTS(true).showParentLevel(true));
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroupOld()));
    }

    @Override
    protected TerritorialTransferExtStuListExtract createNewInstance(Model model)
    {
        return new TerritorialTransferExtStuListExtract();
    }

    @Override
    protected void fillExtract(TerritorialTransferExtStuListExtract extract, Student student, Model model)
    {
        extract.setCourse(student.getCourse());
        extract.setGroupOld(student.getGroup());
        extract.setGroupNew(model.getGroupNew());
        extract.setEducationOrgUnitOld(student.getEducationOrgUnit());

        if (model.getEducationLevelsHighSchool() != null)
        {
            extract.setEducationOrgUnitNew(UniDaoFacade.getEducationLevelDao().getEducationOrgUnit(model));
        }
        else
        {
            extract.setEducationOrgUnitNew(model.getGroupNew().getEducationOrgUnit());
        }

        extract.setCompensationTypeOld(student.getCompensationType());
        extract.setCompensationTypeNew(model.getCompensationTypeNew());
        extract.setChangeEduPlan(model.isChangeEduPlan());
        if (model.isChangeEduPlan())
            extract.setChangeEduPlanDeadlineDate(model.getChangeEduPlanDeadlineDate());
        else
            model.getExtract().setChangeEduPlanDeadlineDate(null);
    }
}