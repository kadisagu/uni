/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e26;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.AdmitToStateExamsExtStuListExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author ListExtractComponentGenerator
 * @since 06.09.2012
 */
public class AdmitToStateExamsExtStuListExtractPrint implements IPrintFormCreator<AdmitToStateExamsExtStuListExtract>, IListParagraphPrintFormCreator<AdmitToStateExamsExtStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, AdmitToStateExamsExtStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);

        Group group = extract.getEntity().getGroup();
        EducationOrgUnit educationOrgUnit = group.getEducationOrgUnit();
        String qCode = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getSafeQCode();
        boolean isDirection = QualificationsCodes.BAKALAVR.equals(qCode) || QualificationsCodes.MAGISTR.equals(qCode);


        StudentOrderReasons reason = ((StudentListOrder) extract.getParagraph().getOrder()).getReason();
        if (null != reason)
        {
            modifier.put("reasonWithSpace", " " + reason.getTitle());
        }
        else
        {
            modifier.put("reasonWithSpace", "");
        }

        CommonExtractPrint.initOrgUnit(modifier, educationOrgUnit, "formativeOrgUnitStr", "");
        modifier.put("directionString_D", isDirection ? "направлению" : "специальности");
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, educationOrgUnit, CommonListOrderPrint.getEducationBaseText(group), "fefuShortFastExtendedOptionalText");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, AdmitToStateExamsExtStuListExtract firstExtract)
    {
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, AdmitToStateExamsExtStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, AdmitToStateExamsExtStuListExtract firstExtract)
    {
    }
}