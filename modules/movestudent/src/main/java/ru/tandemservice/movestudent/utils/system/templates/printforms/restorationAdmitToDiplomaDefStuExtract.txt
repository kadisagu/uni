\keep\keepn\qj\b {extractNumber}. ВОССТАНОВИТЬ:\b0\par
{fio} — {studentCategory_A} {dismissed_A} из {vuzTitle}  приказом № {disOrderNum} от {disOrderDate}г. как не{appeared_A} на защиту выпускной квалификационной работы, в число студентов {orgUnitNew_G} {developFormNew_GF} формы обучения и повторно допустить к защите выпускной квалификационной работы{diplomaEnd_G}.\par
  Основание: {listBasics}.