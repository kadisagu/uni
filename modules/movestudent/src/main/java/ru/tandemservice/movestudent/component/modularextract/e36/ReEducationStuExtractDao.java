/* $Id: extractDao.vm 6933 2009-03-07 05:41:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e36;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ReEducationStuExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 23.12.2009
 */
public class ReEducationStuExtractDao extends UniBaseDao implements IExtractComponentDao<ReEducationStuExtract>
{
    @Override
    public void doCommit(ReEducationStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        MoveStudentDaoFacade.getCommonExtractUtil().doCommitWithoutChangeStatus(extract);

        // заполняем номер и дату приказа
        if ((null == extract.getParagraph()) || (null == extract.getParagraph().getOrder()))
        {
            return;
        }
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            extract.setPrevOrderDate(orderData.getTransferOrderDate());
            extract.setPrevOrderNumber(orderData.getTransferOrderNumber());
            extract.setPrevChangeCompTypeOrderDate(orderData.getChangeCompensationTypeOrderDate());
            extract.setPrevChangeCompTypeOrderNumber(orderData.getChangeCompensationTypeOrderNumber());
        }
        orderData.setTransferOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setTransferOrderNumber(extract.getParagraph().getOrder().getNumber());
        orderData.setChangeCompensationTypeOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setChangeCompensationTypeOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);
    }

    @Override
    public void doRollback(ReEducationStuExtract extract, Map parameters)
    {
        MoveStudentDaoFacade.getCommonExtractUtil().doRollbackWithoutChangeStatus(extract);

        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setTransferOrderDate(extract.getPrevOrderDate());
        orderData.setTransferOrderNumber(extract.getPrevOrderNumber());
        orderData.setChangeCompensationTypeOrderDate(extract.getPrevChangeCompTypeOrderDate());
        orderData.setChangeFioOrderNumber(extract.getPrevChangeCompTypeOrderNumber());
        getSession().saveOrUpdate(orderData);
    }
}