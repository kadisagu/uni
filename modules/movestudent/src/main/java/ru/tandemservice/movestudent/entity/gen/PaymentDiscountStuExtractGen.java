package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.PaymentDiscountStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О скидке по оплате за обучение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PaymentDiscountStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.PaymentDiscountStuExtract";
    public static final String ENTITY_NAME = "paymentDiscountStuExtract";
    public static final int VERSION_HASH = -207130111;
    private static IEntityMeta ENTITY_META;

    public static final String P_DISCOUNT_SIZE = "discountSize";
    public static final String P_DISCOUNT_BEGIN_DATE = "discountBeginDate";
    public static final String P_DISCOUNT_END_DATE = "discountEndDate";

    private double _discountSize;     // Размер скидки
    private Date _discountBeginDate;     // Дата начала предоставления скидки
    private Date _discountEndDate;     // Дата окончания предоставления скидки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Размер скидки. Свойство не может быть null.
     */
    @NotNull
    public double getDiscountSize()
    {
        return _discountSize;
    }

    /**
     * @param discountSize Размер скидки. Свойство не может быть null.
     */
    public void setDiscountSize(double discountSize)
    {
        dirty(_discountSize, discountSize);
        _discountSize = discountSize;
    }

    /**
     * @return Дата начала предоставления скидки. Свойство не может быть null.
     */
    @NotNull
    public Date getDiscountBeginDate()
    {
        return _discountBeginDate;
    }

    /**
     * @param discountBeginDate Дата начала предоставления скидки. Свойство не может быть null.
     */
    public void setDiscountBeginDate(Date discountBeginDate)
    {
        dirty(_discountBeginDate, discountBeginDate);
        _discountBeginDate = discountBeginDate;
    }

    /**
     * @return Дата окончания предоставления скидки. Свойство не может быть null.
     */
    @NotNull
    public Date getDiscountEndDate()
    {
        return _discountEndDate;
    }

    /**
     * @param discountEndDate Дата окончания предоставления скидки. Свойство не может быть null.
     */
    public void setDiscountEndDate(Date discountEndDate)
    {
        dirty(_discountEndDate, discountEndDate);
        _discountEndDate = discountEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof PaymentDiscountStuExtractGen)
        {
            setDiscountSize(((PaymentDiscountStuExtract)another).getDiscountSize());
            setDiscountBeginDate(((PaymentDiscountStuExtract)another).getDiscountBeginDate());
            setDiscountEndDate(((PaymentDiscountStuExtract)another).getDiscountEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PaymentDiscountStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PaymentDiscountStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new PaymentDiscountStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "discountSize":
                    return obj.getDiscountSize();
                case "discountBeginDate":
                    return obj.getDiscountBeginDate();
                case "discountEndDate":
                    return obj.getDiscountEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "discountSize":
                    obj.setDiscountSize((Double) value);
                    return;
                case "discountBeginDate":
                    obj.setDiscountBeginDate((Date) value);
                    return;
                case "discountEndDate":
                    obj.setDiscountEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "discountSize":
                        return true;
                case "discountBeginDate":
                        return true;
                case "discountEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "discountSize":
                    return true;
                case "discountBeginDate":
                    return true;
                case "discountEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "discountSize":
                    return Double.class;
                case "discountBeginDate":
                    return Date.class;
                case "discountEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PaymentDiscountStuExtract> _dslPath = new Path<PaymentDiscountStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PaymentDiscountStuExtract");
    }
            

    /**
     * @return Размер скидки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.PaymentDiscountStuExtract#getDiscountSize()
     */
    public static PropertyPath<Double> discountSize()
    {
        return _dslPath.discountSize();
    }

    /**
     * @return Дата начала предоставления скидки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.PaymentDiscountStuExtract#getDiscountBeginDate()
     */
    public static PropertyPath<Date> discountBeginDate()
    {
        return _dslPath.discountBeginDate();
    }

    /**
     * @return Дата окончания предоставления скидки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.PaymentDiscountStuExtract#getDiscountEndDate()
     */
    public static PropertyPath<Date> discountEndDate()
    {
        return _dslPath.discountEndDate();
    }

    public static class Path<E extends PaymentDiscountStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Double> _discountSize;
        private PropertyPath<Date> _discountBeginDate;
        private PropertyPath<Date> _discountEndDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Размер скидки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.PaymentDiscountStuExtract#getDiscountSize()
     */
        public PropertyPath<Double> discountSize()
        {
            if(_discountSize == null )
                _discountSize = new PropertyPath<Double>(PaymentDiscountStuExtractGen.P_DISCOUNT_SIZE, this);
            return _discountSize;
        }

    /**
     * @return Дата начала предоставления скидки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.PaymentDiscountStuExtract#getDiscountBeginDate()
     */
        public PropertyPath<Date> discountBeginDate()
        {
            if(_discountBeginDate == null )
                _discountBeginDate = new PropertyPath<Date>(PaymentDiscountStuExtractGen.P_DISCOUNT_BEGIN_DATE, this);
            return _discountBeginDate;
        }

    /**
     * @return Дата окончания предоставления скидки. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.PaymentDiscountStuExtract#getDiscountEndDate()
     */
        public PropertyPath<Date> discountEndDate()
        {
            if(_discountEndDate == null )
                _discountEndDate = new PropertyPath<Date>(PaymentDiscountStuExtractGen.P_DISCOUNT_END_DATE, this);
            return _discountEndDate;
        }

        public Class getEntityClass()
        {
            return PaymentDiscountStuExtract.class;
        }

        public String getEntityName()
        {
            return "paymentDiscountStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
