/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeCustomStates.AddEdit;

import org.hibernate.Session;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import ru.tandemservice.movestudent.component.customorder.IStudentCustomOrderExtract;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeCustomStateActionRelation;
import ru.tandemservice.movestudent.entity.custom.ChangeCustomStatesAction;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class DAO extends BaseCustomActionAddEditDAO<ChangeCustomStatesAction, Model>
{
    private List<StudentCustomStateCI> getStudentCustomStates(Student student)
    {
        return new DQLSelectBuilder().fromEntity(StudentCustomState.class, "s")
                .column(property("s", StudentCustomState.customState()))
                .where(eq(property("s", StudentCustomState.student().id()), value(student.getId())))
                .createStatement(getSession()).list();
    }

    private List<StudentCustomStateCI> getStates(ChangeCustomStatesAction action, boolean newStates)
    {
        return new DQLSelectBuilder().fromEntity(ChangeCustomStateActionRelation.class, "rel")
                .column(property("rel", ChangeCustomStateActionRelation.state()))
                .where(eq(property("rel", ChangeCustomStateActionRelation.action().id()), value(action.getId())))
                .where(eq(property("rel", ChangeCustomStateActionRelation.newState()), value(newStates)))
                .createStatement(getSession()).list();
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        if (model.isActionActive())
        {
            List<StudentCustomStateCI> allStates = new DQLSelectBuilder().fromEntity(StudentCustomStateCI.class, "s")
                    .order(property("s", StudentCustomStateCI.title()))
                    .createStatement(getSession()).list();

            if (model.getStudent() != null)
            {
                List<StudentCustomStateCI> studentCustomStateCIs = getStudentCustomStates(model.getStudent());
                model.setOldStatesToDeleteModel(new StaticSelectModel("id", "title", studentCustomStateCIs));
                allStates.removeAll(studentCustomStateCIs);
                model.setNewStatesModel(new StaticSelectModel("id", "title", allStates));
            }
            else
            {
                model.setOldStatesToDeleteModel(new StaticSelectModel("id", "title", allStates));
                model.setNewStatesModel(new StaticSelectModel("id", "title", allStates));
            }

            if (model.isEdit())
            {
                model.setOldStatesToDelete(getStates(model.getCustomAction(), false));
                model.setNewStates(getStates(model.getCustomAction(), true));
            }
        }
    }

    @Override
    protected Class<ChangeCustomStatesAction> getActionClass()
    {
        return ChangeCustomStatesAction.class;
    }

    @Override
    public void updatePrepare(Model model, ChangeCustomStatesAction action, IStudentCustomOrderExtract extract)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();
        if (model.getNewStates().isEmpty() && model.getOldStatesToDelete().isEmpty())
        {
            errCollector.add("Не выбраны удаляемые или добавляемые дополнительные статусы студента.", "newStates", "oldStatesToDelete");
            return;
        }

        for (StudentCustomStateCI oldState : model.getOldStatesToDelete())
        {
            if (model.getNewStates().contains(oldState))
            {
                errCollector.add("Один из удаляемых дополнительных статусов выбран также как добавляемый.", "newStates", "oldStatesToDelete");
                return;
            }
        }

        Session session = getSession();

        if (action.getId() != null)
        {
            for (ChangeCustomStateActionRelation rel : getList(ChangeCustomStateActionRelation.class, ChangeCustomStateActionRelation.action(), action))
                session.delete(rel);
            session.flush();
        }
        else
            session.saveOrUpdate(action);

        for (StudentCustomStateCI state : model.getOldStatesToDelete())
        {
            ChangeCustomStateActionRelation rel = new ChangeCustomStateActionRelation();
            rel.setAction(action);
            rel.setState(state);
            rel.setNewState(false);
            session.save(rel);
        }

        for (StudentCustomStateCI state : model.getNewStates())
        {
            ChangeCustomStateActionRelation rel = new ChangeCustomStateActionRelation();
            rel.setAction(action);
            rel.setState(state);
            rel.setNewState(true);
            session.save(rel);
        }
    }

    @Override
    public void doCommit(ChangeCustomStatesAction action)
    {
        Student student = (Student) action.getCustomExtract().getEntity();
        //student.setCourse(action.getNewCourse());
        update(student);
    }

    @Override
    public void doRollback(ChangeCustomStatesAction action)
    {
        Student student = (Student) action.getCustomExtract().getEntity();
        //student.setCourse(action.getOldCourse());
        update(student);
    }
}