/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeGroup.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.movestudent.component.customorder.IStudentCustomOrderExtract;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.custom.ChangeGroupAction;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class DAO extends BaseCustomActionAddEditDAO<ChangeGroupAction, Model>
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        if (model.isActionActive())
        {
            if (model.isEdit())
            {
                Group group = model.getCustomAction().getNewGroup();
                model.setCourse(group.getCourse());
                model.setFormativeOrgUnit(group.getEducationOrgUnit().getFormativeOrgUnit());
                model.setTerritorialOrgUnit(group.getEducationOrgUnit().getTerritorialOrgUnit());
            }
            else if (model.getStudent() != null)
            {
                model.setCourse(model.getStudent().getCourse());
                model.setFormativeOrgUnit(model.getStudent().getEducationOrgUnit().getFormativeOrgUnit());
                model.setTerritorialOrgUnit(model.getStudent().getEducationOrgUnit().getTerritorialOrgUnit());
            }

            model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
            model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
            model.setCourseList(DevelopGridDAO.getCourseList());
            model.setGroupModel(new CommonSingleSelectModel()
            {
                protected IListResultBuilder createBuilder(String filter, Object id)
                {
                    DQLSelectBuilder builder = new DQLSelectBuilder()
                            .fromEntity(Group.class, "g")
                            .column("g")
                            .where(eq(property("g", Group.educationOrgUnit().formativeOrgUnit()), value(model.getFormativeOrgUnit())))
                            .where(eq(property("g", Group.educationOrgUnit().territorialOrgUnit()), value(model.getTerritorialOrgUnit())))
                            .where(eq(property("g", Group.course()), value(model.getCourse())))
                            .where(eq(property("g", Group.archival()), value(Boolean.FALSE)));

                    if (id != null)
                        builder.where(eq(property("g.id"), commonValue(id, PropertyType.LONG)));

                    if (model.getStudent() != null && model.getStudent().getGroup() != null)
                        builder.where(ne(property("g", Group.id()), value(model.getStudent().getGroup().getId())));

                    if (StringUtils.isNotEmpty(filter))
                        builder.where(likeUpper(property("g", Group.title()), value(CoreStringUtils.escapeLike(filter, true))));

                    MoveStudentDaoFacade.getMoveStudentDao().addCustomConditionToGroupSelectBuilder(builder, "g");
                    builder.order(property("g", Group.title()));

                    return new DQLListResultBuilder(builder, 100);
                }
            });
        }
    }

    @Override
    protected Class<ChangeGroupAction> getActionClass()
    {
        return ChangeGroupAction.class;
    }

    @Override
    public void updatePrepare(Model model, ChangeGroupAction action, IStudentCustomOrderExtract extract)
    {
        Student student = (Student) extract.getEntity();
        action.setOldGroup(student.getGroup());
    }

    @Override
    public void doCommit(ChangeGroupAction action)
    {
        Student student = (Student) action.getCustomExtract().getEntity();

        action.setOldGroup(student.getGroup());
        update(action);

        student.setGroup(action.getNewGroup());
        update(student);
    }

    @Override
    public void doRollback(ChangeGroupAction action)
    {
        Student student = (Student) action.getCustomExtract().getEntity();
        student.setGroup(action.getOldGroup());
        update(student);
    }
}