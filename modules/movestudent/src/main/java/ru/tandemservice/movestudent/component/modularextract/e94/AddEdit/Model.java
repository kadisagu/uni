/* $Id: Model.java.vm 11769 2010-03-05 07:11:49Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e94.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.WeekendChildThreeStuExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 16.01.2013
 */
public class Model extends CommonModularStudentExtractAddEditModel<WeekendChildThreeStuExtract>
{
    private List<StudentStatus> _studentStatusList;

    public List<StudentStatus> getStudentStatusList()
    {
        return _studentStatusList;
    }

    public void setStudentStatusList(List<StudentStatus> studentStatusList)
    {
        _studentStatusList = studentStatusList;
    }

    public boolean getDisplayStudentStatusSelect()
    {
        return !_studentStatusList.isEmpty();
    }

    public Boolean getDisplayFreeAttend()
    {
        if(null == getExtract().getStudentStatusNew())
        {
            return Boolean.FALSE;
        }
        else if(UniDefines.CATALOG_STUDENT_STATUS_CHILD_WITHOUT_ATTENDANCE.equals(getExtract().getStudentStatusNew().getCode()))
        {
            return Boolean.FALSE;
        }
        else
        {
            return Boolean.TRUE;
        }
    }

    public String getDisplayFreeAttendDisplayName()
    {
        String freeAttendanceText = ApplicationRuntime.getProperty("threeYear_freeAttendanceText");
        return "Выводить в приказе - «" + (!StringUtils.isEmpty(freeAttendanceText) ? freeAttendanceText : "") + "»";
    }
}
