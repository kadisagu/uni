/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e16.AddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.WeekendStuExtractExt;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

import java.util.Date;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 30.04.2009
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<WeekendStuExtractExt, Model>
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setStudentStatusNewStr(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACADEM).getTitle());
        if (model.isAddForm())
        {
            model.getExtract().setBeginDate(new Date());
            String defaultReasonStr = ApplicationRuntime.getProperty("defaultReasonStr");
            if (null != defaultReasonStr) model.getExtract().setReasonStr(defaultReasonStr);
        }
    }

    @Override
    protected WeekendStuExtractExt createNewInstance()
    {
        return new WeekendStuExtractExt();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        Date beginDate = model.getExtract().getBeginDate();
        Date endDate = model.getExtract().getEndDate();

        Date stopDateFrom = model.getExtract().getStopPayingFrom();
        Date stopDateTo = model.getExtract().getStopPayingTo();

        if (beginDate.getTime() >= endDate.getTime())
            errors.add("Дата окончания отпуска должна быть позже даты начала", "endDate");

        if (null != stopDateFrom && null != stopDateTo && stopDateFrom.getTime() >= stopDateTo.getTime())
            errors.add("Дата окончания приостановки выплаты стипендии должна быть позже даты ее начала", "stopPayingTo");
    }

    @Override
    public void update(Model model)
    {
        //save rollback data
        model.getExtract().setStudentStatusOld(model.getExtract().getEntity().getStatus());
        if (!model.getExtract().isStopGrantsPaying())
        {
            model.getExtract().setStopPayingFrom(null);
            model.getExtract().setStopPayingTo(null);
        }
        if (!model.getExtract().isAssignPayment())
        {
            model.getExtract().setPaymentAssignDate(null);
            model.getExtract().setPaymentValue(null);
        }
        super.update(model);
    }
}