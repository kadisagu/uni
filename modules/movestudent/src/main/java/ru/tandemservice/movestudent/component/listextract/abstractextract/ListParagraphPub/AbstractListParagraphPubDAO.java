/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 09.04.2009
 */
public abstract class AbstractListParagraphPubDAO<T extends ListStudentExtract, Model extends AbstractListParagraphPubModel<T>> extends UniDao<Model> implements IAbstractListParagraphPubDAO<T, Model>
{
    private final Class<T> _persistentClass;
    private final boolean _printable;

    @SuppressWarnings("unchecked")
    protected AbstractListParagraphPubDAO()
    {
        // Ищем базовый класс дао, чтобы достать тип выписки параграфа из параметров параметризации
        Class baseClass = getClass();
        while (!(baseClass.getGenericSuperclass() instanceof ParameterizedType) && baseClass.getSuperclass() != null)
        {
            baseClass = baseClass.getSuperclass();
        }

        if (!(baseClass.getGenericSuperclass() instanceof ParameterizedType)) {
            throw new IllegalStateException("Base dao class must be parametrized.");
        }

        _persistentClass = (Class<T>) ((ParameterizedType) baseClass.getGenericSuperclass()).getActualTypeArguments()[0];
        _printable = CommonExtractPrint.getPrintFormCreator(EntityRuntime.getMeta(_persistentClass).getName() + "_extractPrint") != null;
    }

    @Override
    public void prepare(Model model)
    {
        model.setParagraph(getNotNull(StudentListParagraph.class, model.getParagraph().getId()));
        model.setFormTitle(getFormTitle(model));
        model.setSecModel(new CommonPostfixPermissionModel("studentListOrder"));

        List<T> extractList = new DQLSelectBuilder().fromEntity(_persistentClass, "e", true)
                .column("e")
                .where(eq(property("e", IAbstractExtract.L_PARAGRAPH), value(model.getParagraph())))
                .order(property("e", IAbstractExtract.P_NUMBER))
                .createStatement(getSession()).list();

        model.setExtractList(extractList);
        model.setExtract(!extractList.isEmpty() ? extractList.get(0) : null);

        // настройка действует в рамках типа параграфа
        model.setSearchListSettingsKey(model.getExtract() != null ? "StudentListParagraph." + model.getExtract().getType().getCode() + "." : null);
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        List<T> extractList = model.getExtractList();
        model.getDataSource().setCountRow(extractList.size());
        UniBaseUtils.createPage(model.getDataSource(), extractList);
        boolean readOnly = extractList.size() == 1 || model.getParagraph().getOrder().isReadonly();
        for (ViewWrapper viewWrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            viewWrapper.setViewProperty(Model.NO_DELETE, readOnly);
            viewWrapper.setViewProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY, !_printable);
        }
    }

    private String getFormTitle(Model model)
    {
        StringBuilder title = new StringBuilder();
        title.append("Параграф №");
        title.append(model.getParagraph().getNumber());
        title.append(" списочного приказа");

        if (StringUtils.isNotEmpty(model.getParagraph().getOrder().getNumber()))
            title.append(" №").append(model.getParagraph().getOrder().getNumber());
        if (model.getParagraph().getOrder().getCommitDate() != null)
            title.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getParagraph().getOrder().getCommitDate()));
        return title.toString();
    }
}
