/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.modular.StudentModularOrderPub;

import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.BusinessComponentRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.sec.entity.Admin;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.component.commons.MoveStudentColumns;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;

/**
 * @author vip_delete
 * @since 20.10.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    public static final String DELETE_COLUMN_NAME = "excludeColumn";

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        if (!MoveStudentDaoFacade.getMoveStudentDao().isModularOrderIndividual(model.getOrder()))
        {
            prepareListDataSource(component);

            model.setSettings(UniBaseUtils.getDataSettings(component, "StudentModularOrderPub.filter"));
        }
        else
        {
            AbstractStudentExtract extract = MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(model.getOrder().getId());
            deactivate(component);
            activateInRoot(component, new ComponentActivator(BusinessComponentRuntime.getInstance().getPublisher(extract.getEntityMeta().getName()),
                                                             new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, extract.getId())));
        }
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<IAbstractExtract> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(MoveStudentColumns.getCreateDateColumn());
        dataSource.addColumn(new PublisherLinkColumn("ФИО студента", MoveStudentDefines.STUDENT_FULL_FIO)
                                     .setResolver(new SimplePublisherLinkResolver(ModularStudentExtract.entity().id()).param("selectedStudentTab", "studentTab")));
        dataSource.addColumn(MoveStudentColumns.getFormativeOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getTerritorialOrgUnitColumn());
        dataSource.addColumn(MoveStudentColumns.getProducingOrgUnitColumn());
        MoveStudentColumns.addEduLevelTitleColumns(dataSource);
        dataSource.addColumn(MoveStudentColumns.getCourseColumn());
        dataSource.addColumn(MoveStudentColumns.getGroupColumn());
        dataSource.addColumn(MoveStudentColumns.getDevelopFormColumn());
        dataSource.addColumn(MoveStudentColumns.getCompensationTypeColumn());
        dataSource.addColumn(MoveStudentColumns.getExtractTypeColumn("Тип приказа"));
        dataSource.addColumn(MoveStudentColumns.getModularExtractNumberColumn().setOrderable(true));
        dataSource.addColumn(MoveStudentColumns.getExtractPrintColumn(this, model.getSecModel().getPermission("printExtract")));
        if (MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
            dataSource.addColumn(MoveStudentColumns.getExtractPrintPdfColumn(this, model.getSecModel().getPermission("printExtractPdf")));

        final ActionColumn deleteColumn = new ActionColumn("Исключить", ActionColumn.DELETE, "onClickExtractDelete", "Исключить «{0}» из приказа?", IAbstractExtract.P_TITLE);
        deleteColumn.setName(DELETE_COLUMN_NAME);
        dataSource.addColumn(deleteColumn);

        model.setDataSource(dataSource);
    }

    public boolean isPdfEnable()
    {
        return MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED;
    }

    // Event Listeners

    public void onClickSendToCoordination(IBusinessComponent component)
    {
        IPrincipalContext principalContext = component.getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable) && !(principalContext instanceof Admin && Debug.isEnabled()))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");
        if (principalContext instanceof Admin)
            getDao().doSendToCoordination(getModel(component), null);
        else
            getDao().doSendToCoordination(getModel(component), (IPersistentPersonable) principalContext);
    }

    public void onClickSendToFormative(IBusinessComponent component)
    {
        getDao().doSendToFormative(getModel(component));
    }

    public void onClickReject(IBusinessComponent component)
    {
        getDao().doReject(getModel(component));
    }

    public void onClickEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_EDIT, new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())
        ));
    }

    public void onClickDeleteOrderWithExtracts(IBusinessComponent component)
    {
        getDao().deleteOrderWithExtracts(getModel(component));
        deactivate(component);
    }

    public void onClickAddExtract(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_ADD_EXTRACTS, new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())
        ));

    }

    public final synchronized void onClickCommit(IBusinessComponent component)
    {
        getDao().doCommit(getModel(component));
    }

    public final synchronized void onClickRollback(IBusinessComponent component)
    {
        getDao().doRollback(getModel(component));
    }

    public void onClickDeleteOrder(IBusinessComponent component)
    {
        getDao().deleteOrder(getModel(component));
        deactivate(component);
    }

    public void onClickPrintOrder(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_PRINT, new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())
        ));
    }

    public void onClickPrintOrderPdf(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_PRINT, new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())
                .add("printPdf", Boolean.TRUE)
        ));
    }

    //печать выписок
    public void onClickPrintExtracts(IBusinessComponent component)
    {
        BusinessComponentUtils.downloadDocument(MoveStudentPrintModifierManager.instance().printDao().printMassOrder(getModel(component).getOrder()), true);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        DataSettingsFacade.saveSettings(getModel(component).getSettings());
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }

    public void onClickExtractDelete(IBusinessComponent component)
    {
        getDao().deleteExtract(getModel(component), (Long) component.getListenerParameter());
        MoveStudentDaoFacade.getMoveStudentDao().updateParagraphsNumber(getModel(component).getOrder());
    }

    public void onClickExtractPrint(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.MODULAR_STUDENT_EXTRACT_PRINT, new ParametersMap()
                .add("extractId", component.getListenerParameter())
        ));
    }

    public void onClickExtractPrintPdf(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.MODULAR_STUDENT_EXTRACT_PRINT, new ParametersMap()
                .add("extractId", component.getListenerParameter())
                .add("printPdf", Boolean.TRUE)
        ));
    }

    public void onClickSetExecutor(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveStudentComponents.STUDENT_ORDER_SET_EXECUTOR, new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())
        ));
    }
}