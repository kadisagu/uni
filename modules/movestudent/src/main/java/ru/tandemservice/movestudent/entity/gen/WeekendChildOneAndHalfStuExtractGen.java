package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О предоставлении отпуска по уходу за ребенком до 1,5 лет
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class WeekendChildOneAndHalfStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract";
    public static final String ENTITY_NAME = "weekendChildOneAndHalfStuExtract";
    public static final int VERSION_HASH = 2056813289;
    private static IEntityMeta ENTITY_META;

    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String L_STUDENT_STATUS_NEW = "studentStatusNew";
    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";
    public static final String P_PAY_BENEFIT = "payBenefit";
    public static final String P_PAY_BENEFIT_BEGIN_DATE = "payBenefitBeginDate";
    public static final String P_PAY_BENEFIT_END_DATE = "payBenefitEndDate";
    public static final String P_PAY_ONETIME_BENEFIT = "payOnetimeBenefit";
    public static final String P_PAY_BENEFIT_TEXT = "payBenefitText";
    public static final String P_PAY_ONETIME_BENEFIT_TEXT = "payOnetimeBenefitText";
    public static final String P_PREV_BEGIN_DATE = "prevBeginDate";
    public static final String P_PREV_END_DATE = "prevEndDate";
    public static final String P_FREE_ATTENDANCE = "freeAttendance";

    private Date _beginDate;     // Дата начала отпуска
    private Date _endDate;     // Дата окончания отпуска
    private StudentStatus _studentStatusNew;     // Новое состояние студента
    private StudentStatus _studentStatusOld;     // Предыдущее состояние студента
    private boolean _payBenefit;     // Выплатить пособие по беременности и родам
    private Date _payBenefitBeginDate;     // Дата начала выплаты пособия по беременности и родам
    private Date _payBenefitEndDate;     // Дата окончания выплаты пособия по беременности и родам
    private boolean _payOnetimeBenefit;     // Выплатить единовременное пособие
    private String _payBenefitText;     // Текст выплаты пособия по беременности и родам
    private String _payOnetimeBenefitText;     // Текст выплаты единовременного пособия
    private Date _prevBeginDate;     // Дата начала отпуска по предыдущему приказу
    private Date _prevEndDate;     // Дата окончания отпуска по предыдущему приказу
    private boolean _freeAttendance;     // Свободное посещение занятий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата начала отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала отпуска. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания отпуска. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Новое состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    /**
     * @param studentStatusNew Новое состояние студента. Свойство не может быть null.
     */
    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        dirty(_studentStatusNew, studentStatusNew);
        _studentStatusNew = studentStatusNew;
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Предыдущее состояние студента. Свойство не может быть null.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    /**
     * @return Выплатить пособие по беременности и родам. Свойство не может быть null.
     */
    @NotNull
    public boolean isPayBenefit()
    {
        return _payBenefit;
    }

    /**
     * @param payBenefit Выплатить пособие по беременности и родам. Свойство не может быть null.
     */
    public void setPayBenefit(boolean payBenefit)
    {
        dirty(_payBenefit, payBenefit);
        _payBenefit = payBenefit;
    }

    /**
     * @return Дата начала выплаты пособия по беременности и родам.
     */
    public Date getPayBenefitBeginDate()
    {
        return _payBenefitBeginDate;
    }

    /**
     * @param payBenefitBeginDate Дата начала выплаты пособия по беременности и родам.
     */
    public void setPayBenefitBeginDate(Date payBenefitBeginDate)
    {
        dirty(_payBenefitBeginDate, payBenefitBeginDate);
        _payBenefitBeginDate = payBenefitBeginDate;
    }

    /**
     * @return Дата окончания выплаты пособия по беременности и родам.
     */
    public Date getPayBenefitEndDate()
    {
        return _payBenefitEndDate;
    }

    /**
     * @param payBenefitEndDate Дата окончания выплаты пособия по беременности и родам.
     */
    public void setPayBenefitEndDate(Date payBenefitEndDate)
    {
        dirty(_payBenefitEndDate, payBenefitEndDate);
        _payBenefitEndDate = payBenefitEndDate;
    }

    /**
     * @return Выплатить единовременное пособие. Свойство не может быть null.
     */
    @NotNull
    public boolean isPayOnetimeBenefit()
    {
        return _payOnetimeBenefit;
    }

    /**
     * @param payOnetimeBenefit Выплатить единовременное пособие. Свойство не может быть null.
     */
    public void setPayOnetimeBenefit(boolean payOnetimeBenefit)
    {
        dirty(_payOnetimeBenefit, payOnetimeBenefit);
        _payOnetimeBenefit = payOnetimeBenefit;
    }

    /**
     * @return Текст выплаты пособия по беременности и родам.
     */
    @Length(max=600)
    public String getPayBenefitText()
    {
        return _payBenefitText;
    }

    /**
     * @param payBenefitText Текст выплаты пособия по беременности и родам.
     */
    public void setPayBenefitText(String payBenefitText)
    {
        dirty(_payBenefitText, payBenefitText);
        _payBenefitText = payBenefitText;
    }

    /**
     * @return Текст выплаты единовременного пособия.
     */
    @Length(max=600)
    public String getPayOnetimeBenefitText()
    {
        return _payOnetimeBenefitText;
    }

    /**
     * @param payOnetimeBenefitText Текст выплаты единовременного пособия.
     */
    public void setPayOnetimeBenefitText(String payOnetimeBenefitText)
    {
        dirty(_payOnetimeBenefitText, payOnetimeBenefitText);
        _payOnetimeBenefitText = payOnetimeBenefitText;
    }

    /**
     * @return Дата начала отпуска по предыдущему приказу.
     */
    public Date getPrevBeginDate()
    {
        return _prevBeginDate;
    }

    /**
     * @param prevBeginDate Дата начала отпуска по предыдущему приказу.
     */
    public void setPrevBeginDate(Date prevBeginDate)
    {
        dirty(_prevBeginDate, prevBeginDate);
        _prevBeginDate = prevBeginDate;
    }

    /**
     * @return Дата окончания отпуска по предыдущему приказу.
     */
    public Date getPrevEndDate()
    {
        return _prevEndDate;
    }

    /**
     * @param prevEndDate Дата окончания отпуска по предыдущему приказу.
     */
    public void setPrevEndDate(Date prevEndDate)
    {
        dirty(_prevEndDate, prevEndDate);
        _prevEndDate = prevEndDate;
    }

    /**
     * @return Свободное посещение занятий. Свойство не может быть null.
     */
    @NotNull
    public boolean isFreeAttendance()
    {
        return _freeAttendance;
    }

    /**
     * @param freeAttendance Свободное посещение занятий. Свойство не может быть null.
     */
    public void setFreeAttendance(boolean freeAttendance)
    {
        dirty(_freeAttendance, freeAttendance);
        _freeAttendance = freeAttendance;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof WeekendChildOneAndHalfStuExtractGen)
        {
            setBeginDate(((WeekendChildOneAndHalfStuExtract)another).getBeginDate());
            setEndDate(((WeekendChildOneAndHalfStuExtract)another).getEndDate());
            setStudentStatusNew(((WeekendChildOneAndHalfStuExtract)another).getStudentStatusNew());
            setStudentStatusOld(((WeekendChildOneAndHalfStuExtract)another).getStudentStatusOld());
            setPayBenefit(((WeekendChildOneAndHalfStuExtract)another).isPayBenefit());
            setPayBenefitBeginDate(((WeekendChildOneAndHalfStuExtract)another).getPayBenefitBeginDate());
            setPayBenefitEndDate(((WeekendChildOneAndHalfStuExtract)another).getPayBenefitEndDate());
            setPayOnetimeBenefit(((WeekendChildOneAndHalfStuExtract)another).isPayOnetimeBenefit());
            setPayBenefitText(((WeekendChildOneAndHalfStuExtract)another).getPayBenefitText());
            setPayOnetimeBenefitText(((WeekendChildOneAndHalfStuExtract)another).getPayOnetimeBenefitText());
            setPrevBeginDate(((WeekendChildOneAndHalfStuExtract)another).getPrevBeginDate());
            setPrevEndDate(((WeekendChildOneAndHalfStuExtract)another).getPrevEndDate());
            setFreeAttendance(((WeekendChildOneAndHalfStuExtract)another).isFreeAttendance());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends WeekendChildOneAndHalfStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) WeekendChildOneAndHalfStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new WeekendChildOneAndHalfStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "studentStatusNew":
                    return obj.getStudentStatusNew();
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
                case "payBenefit":
                    return obj.isPayBenefit();
                case "payBenefitBeginDate":
                    return obj.getPayBenefitBeginDate();
                case "payBenefitEndDate":
                    return obj.getPayBenefitEndDate();
                case "payOnetimeBenefit":
                    return obj.isPayOnetimeBenefit();
                case "payBenefitText":
                    return obj.getPayBenefitText();
                case "payOnetimeBenefitText":
                    return obj.getPayOnetimeBenefitText();
                case "prevBeginDate":
                    return obj.getPrevBeginDate();
                case "prevEndDate":
                    return obj.getPrevEndDate();
                case "freeAttendance":
                    return obj.isFreeAttendance();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "studentStatusNew":
                    obj.setStudentStatusNew((StudentStatus) value);
                    return;
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
                case "payBenefit":
                    obj.setPayBenefit((Boolean) value);
                    return;
                case "payBenefitBeginDate":
                    obj.setPayBenefitBeginDate((Date) value);
                    return;
                case "payBenefitEndDate":
                    obj.setPayBenefitEndDate((Date) value);
                    return;
                case "payOnetimeBenefit":
                    obj.setPayOnetimeBenefit((Boolean) value);
                    return;
                case "payBenefitText":
                    obj.setPayBenefitText((String) value);
                    return;
                case "payOnetimeBenefitText":
                    obj.setPayOnetimeBenefitText((String) value);
                    return;
                case "prevBeginDate":
                    obj.setPrevBeginDate((Date) value);
                    return;
                case "prevEndDate":
                    obj.setPrevEndDate((Date) value);
                    return;
                case "freeAttendance":
                    obj.setFreeAttendance((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "studentStatusNew":
                        return true;
                case "studentStatusOld":
                        return true;
                case "payBenefit":
                        return true;
                case "payBenefitBeginDate":
                        return true;
                case "payBenefitEndDate":
                        return true;
                case "payOnetimeBenefit":
                        return true;
                case "payBenefitText":
                        return true;
                case "payOnetimeBenefitText":
                        return true;
                case "prevBeginDate":
                        return true;
                case "prevEndDate":
                        return true;
                case "freeAttendance":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "studentStatusNew":
                    return true;
                case "studentStatusOld":
                    return true;
                case "payBenefit":
                    return true;
                case "payBenefitBeginDate":
                    return true;
                case "payBenefitEndDate":
                    return true;
                case "payOnetimeBenefit":
                    return true;
                case "payBenefitText":
                    return true;
                case "payOnetimeBenefitText":
                    return true;
                case "prevBeginDate":
                    return true;
                case "prevEndDate":
                    return true;
                case "freeAttendance":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "studentStatusNew":
                    return StudentStatus.class;
                case "studentStatusOld":
                    return StudentStatus.class;
                case "payBenefit":
                    return Boolean.class;
                case "payBenefitBeginDate":
                    return Date.class;
                case "payBenefitEndDate":
                    return Date.class;
                case "payOnetimeBenefit":
                    return Boolean.class;
                case "payBenefitText":
                    return String.class;
                case "payOnetimeBenefitText":
                    return String.class;
                case "prevBeginDate":
                    return Date.class;
                case "prevEndDate":
                    return Date.class;
                case "freeAttendance":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<WeekendChildOneAndHalfStuExtract> _dslPath = new Path<WeekendChildOneAndHalfStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "WeekendChildOneAndHalfStuExtract");
    }
            

    /**
     * @return Дата начала отпуска. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания отпуска. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Новое состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getStudentStatusNew()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusNew()
    {
        return _dslPath.studentStatusNew();
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    /**
     * @return Выплатить пособие по беременности и родам. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#isPayBenefit()
     */
    public static PropertyPath<Boolean> payBenefit()
    {
        return _dslPath.payBenefit();
    }

    /**
     * @return Дата начала выплаты пособия по беременности и родам.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getPayBenefitBeginDate()
     */
    public static PropertyPath<Date> payBenefitBeginDate()
    {
        return _dslPath.payBenefitBeginDate();
    }

    /**
     * @return Дата окончания выплаты пособия по беременности и родам.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getPayBenefitEndDate()
     */
    public static PropertyPath<Date> payBenefitEndDate()
    {
        return _dslPath.payBenefitEndDate();
    }

    /**
     * @return Выплатить единовременное пособие. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#isPayOnetimeBenefit()
     */
    public static PropertyPath<Boolean> payOnetimeBenefit()
    {
        return _dslPath.payOnetimeBenefit();
    }

    /**
     * @return Текст выплаты пособия по беременности и родам.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getPayBenefitText()
     */
    public static PropertyPath<String> payBenefitText()
    {
        return _dslPath.payBenefitText();
    }

    /**
     * @return Текст выплаты единовременного пособия.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getPayOnetimeBenefitText()
     */
    public static PropertyPath<String> payOnetimeBenefitText()
    {
        return _dslPath.payOnetimeBenefitText();
    }

    /**
     * @return Дата начала отпуска по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getPrevBeginDate()
     */
    public static PropertyPath<Date> prevBeginDate()
    {
        return _dslPath.prevBeginDate();
    }

    /**
     * @return Дата окончания отпуска по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getPrevEndDate()
     */
    public static PropertyPath<Date> prevEndDate()
    {
        return _dslPath.prevEndDate();
    }

    /**
     * @return Свободное посещение занятий. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#isFreeAttendance()
     */
    public static PropertyPath<Boolean> freeAttendance()
    {
        return _dslPath.freeAttendance();
    }

    public static class Path<E extends WeekendChildOneAndHalfStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private StudentStatus.Path<StudentStatus> _studentStatusNew;
        private StudentStatus.Path<StudentStatus> _studentStatusOld;
        private PropertyPath<Boolean> _payBenefit;
        private PropertyPath<Date> _payBenefitBeginDate;
        private PropertyPath<Date> _payBenefitEndDate;
        private PropertyPath<Boolean> _payOnetimeBenefit;
        private PropertyPath<String> _payBenefitText;
        private PropertyPath<String> _payOnetimeBenefitText;
        private PropertyPath<Date> _prevBeginDate;
        private PropertyPath<Date> _prevEndDate;
        private PropertyPath<Boolean> _freeAttendance;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата начала отпуска. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(WeekendChildOneAndHalfStuExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания отпуска. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(WeekendChildOneAndHalfStuExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Новое состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getStudentStatusNew()
     */
        public StudentStatus.Path<StudentStatus> studentStatusNew()
        {
            if(_studentStatusNew == null )
                _studentStatusNew = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_NEW, this);
            return _studentStatusNew;
        }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

    /**
     * @return Выплатить пособие по беременности и родам. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#isPayBenefit()
     */
        public PropertyPath<Boolean> payBenefit()
        {
            if(_payBenefit == null )
                _payBenefit = new PropertyPath<Boolean>(WeekendChildOneAndHalfStuExtractGen.P_PAY_BENEFIT, this);
            return _payBenefit;
        }

    /**
     * @return Дата начала выплаты пособия по беременности и родам.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getPayBenefitBeginDate()
     */
        public PropertyPath<Date> payBenefitBeginDate()
        {
            if(_payBenefitBeginDate == null )
                _payBenefitBeginDate = new PropertyPath<Date>(WeekendChildOneAndHalfStuExtractGen.P_PAY_BENEFIT_BEGIN_DATE, this);
            return _payBenefitBeginDate;
        }

    /**
     * @return Дата окончания выплаты пособия по беременности и родам.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getPayBenefitEndDate()
     */
        public PropertyPath<Date> payBenefitEndDate()
        {
            if(_payBenefitEndDate == null )
                _payBenefitEndDate = new PropertyPath<Date>(WeekendChildOneAndHalfStuExtractGen.P_PAY_BENEFIT_END_DATE, this);
            return _payBenefitEndDate;
        }

    /**
     * @return Выплатить единовременное пособие. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#isPayOnetimeBenefit()
     */
        public PropertyPath<Boolean> payOnetimeBenefit()
        {
            if(_payOnetimeBenefit == null )
                _payOnetimeBenefit = new PropertyPath<Boolean>(WeekendChildOneAndHalfStuExtractGen.P_PAY_ONETIME_BENEFIT, this);
            return _payOnetimeBenefit;
        }

    /**
     * @return Текст выплаты пособия по беременности и родам.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getPayBenefitText()
     */
        public PropertyPath<String> payBenefitText()
        {
            if(_payBenefitText == null )
                _payBenefitText = new PropertyPath<String>(WeekendChildOneAndHalfStuExtractGen.P_PAY_BENEFIT_TEXT, this);
            return _payBenefitText;
        }

    /**
     * @return Текст выплаты единовременного пособия.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getPayOnetimeBenefitText()
     */
        public PropertyPath<String> payOnetimeBenefitText()
        {
            if(_payOnetimeBenefitText == null )
                _payOnetimeBenefitText = new PropertyPath<String>(WeekendChildOneAndHalfStuExtractGen.P_PAY_ONETIME_BENEFIT_TEXT, this);
            return _payOnetimeBenefitText;
        }

    /**
     * @return Дата начала отпуска по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getPrevBeginDate()
     */
        public PropertyPath<Date> prevBeginDate()
        {
            if(_prevBeginDate == null )
                _prevBeginDate = new PropertyPath<Date>(WeekendChildOneAndHalfStuExtractGen.P_PREV_BEGIN_DATE, this);
            return _prevBeginDate;
        }

    /**
     * @return Дата окончания отпуска по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#getPrevEndDate()
     */
        public PropertyPath<Date> prevEndDate()
        {
            if(_prevEndDate == null )
                _prevEndDate = new PropertyPath<Date>(WeekendChildOneAndHalfStuExtractGen.P_PREV_END_DATE, this);
            return _prevEndDate;
        }

    /**
     * @return Свободное посещение занятий. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract#isFreeAttendance()
     */
        public PropertyPath<Boolean> freeAttendance()
        {
            if(_freeAttendance == null )
                _freeAttendance = new PropertyPath<Boolean>(WeekendChildOneAndHalfStuExtractGen.P_FREE_ATTENDANCE, this);
            return _freeAttendance;
        }

        public Class getEntityClass()
        {
            return WeekendChildOneAndHalfStuExtract.class;
        }

        public String getEntityName()
        {
            return "weekendChildOneAndHalfStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
