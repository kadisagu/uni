/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e93.AddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDao;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.GroupAssignStuExtract;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 11.01.2013
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<GroupAssignStuExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        final Student student = model.getExtract().getEntity();
        final EducationOrgUnit studentEduOU = student.getEducationOrgUnit();

        if(model.isAddForm())
            model.setGroupNew(model.getExtract().getEntity().getGroup());
        else
            model.setGroupNew(model.getExtract().getGroupNew());

        model.setGroupModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder
                        .fromEntity(Group.class, "g")
                        .column("g")
                        .where(DQLExpressions.eq(DQLExpressions.property(Group.course().fromAlias("g")), DQLExpressions.value(student.getCourse())))
                        .where(DQLExpressions.eq(DQLExpressions.property(Group.educationOrgUnit().formativeOrgUnit().fromAlias("g")), DQLExpressions.value(studentEduOU.getFormativeOrgUnit())))
                        .where(DQLExpressions.eq(DQLExpressions.property(Group.educationOrgUnit().territorialOrgUnit().fromAlias("g")), DQLExpressions.value(studentEduOU.getTerritorialOrgUnit())))
                        .where(DQLExpressions.eq(DQLExpressions.property(Group.educationOrgUnit().developForm().fromAlias("g")), DQLExpressions.value(studentEduOU.getDevelopForm())))
                        .where(DQLExpressions.eq(DQLExpressions.property(Group.educationOrgUnit().developCondition().fromAlias("g")), DQLExpressions.value(studentEduOU.getDevelopCondition())))
                        .where(DQLExpressions.eq(DQLExpressions.property(Group.educationOrgUnit().developTech().fromAlias("g")), DQLExpressions.value(studentEduOU.getDevelopTech())))
                        .where(DQLExpressions.eq(DQLExpressions.property(Group.educationOrgUnit().developPeriod().fromAlias("g")), DQLExpressions.value(studentEduOU.getDevelopPeriod())))
                        .where(DQLExpressions.eq(DQLExpressions.property(Group.archival().fromAlias("g")), DQLExpressions.value(Boolean.FALSE)))
                        .where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(Group.title().fromAlias("g"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))))
                       .order(DQLExpressions.property(Group.title().fromAlias("g")));

                MoveStudentDaoFacade.getMoveStudentDao().addCustomConditionToGroupSelectBuilder(builder, "g");

                int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                if (count > MoveStudentDao.MAX_ROWS)
                    builder.top(MoveStudentDao.MAX_ROWS);

                return new ListResult<>(builder.createStatement(getSession()).list(), count);
            }
        });
    }

    @Override
    protected GroupAssignStuExtract createNewInstance()
    {
        return new GroupAssignStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setGroupOld(model.getExtract().getEntity().getGroup());
        model.getExtract().setGroupNew(model.getGroupNew());
        super.update(model);
    }
}