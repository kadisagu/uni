/* $Id: DAO.java.vm 24710 2012-10-30 11:06:12Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e91.AddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 11.01.2013
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<AcadGrantBonusAssignStuExtract, Model> implements IDAO
{
    @Override
    protected AcadGrantBonusAssignStuExtract createNewInstance()
    {
        return new AcadGrantBonusAssignStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void validate(Model model, ErrorCollector errorCollector)
    {
        super.validate(model, errorCollector);
        if (model.getExtract().getBeginDate().after(model.getExtract().getEndDate()))
            errorCollector.add("Дата окончания выплаты надбавки к стипендии должна быть позже даты начала", "beginDate", "endDate");
    }
}