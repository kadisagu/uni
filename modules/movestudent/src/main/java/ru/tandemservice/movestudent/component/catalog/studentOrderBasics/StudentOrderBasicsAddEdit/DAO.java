/* $Id$ */
package ru.tandemservice.movestudent.component.catalog.studentOrderBasics.StudentOrderBasicsAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics;

/**
 * @author AutoGenerator
 *         Created on 04.12.2012
 */
public class DAO extends DefaultCatalogAddEditDAO<StudentOrderBasics, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
    }

    @Override
    public void update(Model model)
    {
        super.update(model);
    }
}