/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.modular.StudentModularOrderPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.component.commons.IMoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.MoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.customorder.CustomOrderUtils;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.services.UniServiceFacade;
import ru.tandemservice.uni.ui.EducationLevelsHighSchoolSelectModel;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.UnimvDefines;
import ru.tandemservice.unimv.UnimvUtil;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.VisaTask;
import ru.tandemservice.unimv.services.visatask.ISendToCoordinationService;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskHandler;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskService;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 20.10.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry(IMoveStudentMQBuilder.EXTRACT_ALIAS);

    static
    {
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_FULL_FIO, new OrderDescription(IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS, IdentityCard.P_LAST_NAME), new OrderDescription(IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS, IdentityCard.P_FIRST_NAME), new OrderDescription(IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS, IdentityCard.P_MIDDLE_NAME));
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_EXTRACT_FORMATIVE_ORG_UNIT_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_TITLE}));
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_EXTRACT_TERRITORIAL_ORG_UNIT_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_TITLE}));
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_EXTRACT_PRODUCING_ORG_UNIT_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.L_ORG_UNIT, OrgUnit.P_TITLE}));
    }

    @Override
    public void prepare(Model model)
    {
        model.setOrder(getNotNull(StudentModularOrder.class, model.getOrder().getId()));
        model.setSecModel(new CommonPostfixPermissionModel("studentModularOrder"));
        model.setVisingStatus(UnimvDaoFacade.getVisaDao().getVisingStatus(model.getOrder()));

        // init filters
        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setProducingOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING));
        model.setEducationLevelHighSchool(new EducationLevelsHighSchoolSelectModel(model));
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setExtractTypesListModel(new FullCheckSelectModel(UserContext.getInstance().getPrincipalContext().isAdmin() ? StudentExtractType.titleWithCode().s() : StudentExtractType.title().s())
        {
            @Override
            public ListResult findValues(String filter)
            {
                return MoveStudentDaoFacade.getMoveStudentDao().getModularStudentExtractTypeList(filter, StudentExtractTypeCodes.MODULAR_ORDER);
            }
        });
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        // init datasource
        IMoveStudentMQBuilder builder = MoveStudentMQBuilder.createExtractMQBuilder(ModularStudentExtract.class, model.getSettings());

        // apply user defined restrictions
        builder.getMQBuilder().add(MQExpression.eq(IMoveStudentMQBuilder.PARAGRAPH_ALIAS, IAbstractParagraph.L_ORDER, model.getOrder()));

        // apply filters
        builder.applyExtractCreateDate();
        builder.applyExtractLastName();
        builder.applyExtractFormativeOrgUnit();
        builder.applyExtractTerritorialOrgUnit();
        builder.applyExtractProducingOrgUnit();
        builder.applyExtractEducationLevelHighSchool();
        builder.applyExtractCourse();
        builder.applyExtractDevelopForm();
        builder.applyExtractCompensationType();
        builder.applyExtractType();

        // create page
        builder.createPage(getSession(), _orderSettings, model.getDataSource());

        // Обновляем дизабл на колонке с кнопками удаления выписок из приказа
        boolean canDelete = UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getOrder().getState().getCode());
        if (canDelete) {
            final String permissionKey = model.getSecModel().getPermission("deleteExtract");
            final UserContext userContext = ContextLocal.getUserContext();
            canDelete = userContext != null && CoreServices.securityService().check(model.getOrder(), userContext.getPrincipalContext(), permissionKey);
        }
        model.getDataSource().getColumn(Controller.DELETE_COLUMN_NAME).setDisabled(!canDelete);
    }

    @Override
    public void doSendToCoordination(Model model, IPersistentPersonable initiator)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getOrder());
        if (visaTask != null)
            errCollector.add("Нельзя отправить приказ на согласование, так как он уже на согласовании.");

        if (StringUtils.isEmpty(model.getOrder().getNumber()))
            errCollector.add("Нельзя отправить приказ на согласование без номера.");

        if (model.getOrder().getCommitDate() == null)
            errCollector.add("Нельзя отправить приказ на согласование без даты приказа.");

        if (!errCollector.hasErrors())
        {
            //save print form
            MoveStudentDaoFacade.getMoveStudentDao().saveModularOrderText(model.getOrder());

            ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
            sendToCoordinationService.init(model.getOrder(), initiator);
            sendToCoordinationService.execute();
        }
    }

    @Override
    public void doSendToFormative(Model model)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getOrder());
        if (visaTask != null)
            errCollector.add("Нельзя отправить приказ на формирование, так как он на согласовании.");

        if (!errCollector.hasErrors())
        {
            //2. надо сменить состояние на формируется
            model.getOrder().setState(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
            update(model.getOrder());
        }
    }

    @Override
    public void doReject(Model model)
    {
        ITouchVisaTaskService rejectService = UniServiceFacade.getService(ITouchVisaTaskService.REJECT_VISA_TASK_SERVICE_NAME);
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getOrder());
        if (visaTask != null)
        {
            rejectService.init(visaTask.getId(), "SYSTEM: Отменено пользователем с публикатора");
            rejectService.execute();
        } else
        {
            //задачи нет => документ уже согласован,
            //в историю согласования ничего не попадет. так как нет задачи согласования
            //короче запускаем процедуру отрицательного завершения процедуры согласования
            ITouchVisaTaskHandler touchService = UnimvUtil.findTouchService(model.getOrder(), UnimvDefines.VISA_REJECT_SERVICE_MAP);
            touchService.init(model.getOrder());
            touchService.execute();
        }
    }

    @Override
    public void doCommit(Model model)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        if (model.getOrder().getNumber() == null)
            errCollector.add("Нельзя проводить приказ без номера.");

        if (model.getOrder().getCommitDate() == null)
            errCollector.add("Нельзя проводить приказ без даты приказа.");

        if (!errCollector.hasErrors())
        {
            //save print form
            MoveStudentDaoFacade.getMoveStudentDao().saveModularOrderText(model.getOrder());

            MoveDaoFacade.getMoveDao().doCommitOrder(model.getOrder(), getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED), getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED), null);
        }
    }

    @Override
    public void doRollback(Model model)
    {
        // у каждого студента из выписок данного приказа смотрим, нет ли у него более поздней проведенной выписки.
        // если такую находим - ругаемся

        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();
        final DQLSelectBuilder extractDQL = new DQLSelectBuilder()
                .fromEntity(AbstractStudentExtract.class, "ext")
                .column("ext.entity.id")
                .where(in(
                        property("ext.entity"),
                        new DQLSelectBuilder()
                                .fromEntity(AbstractStudentExtract.class, "stu")
                                .column("stu.entity")
                                .where(eq(property(AbstractStudentExtract.paragraph().order().fromAlias("stu")), value(model.getOrder())))
                                .buildQuery()
                ))
                .where(gt(property("ext.paragraph.order.commitDateSystem"), valueTimestamp(model.getOrder().getCommitDateSystem())));

        List<Student> students = getList(Student.class, extractDQL.createStatement(getSession()).<Long>list());
        if (students.size() > 0)
            errCollector.add("Нельзя откатить приказ, т.к. для студент" + (students.size() == 1 ? "ов " : "а ") +
                                     getStudentsList(students) +
                                     " есть приказы, которые вступили в силу после данного приказа");

        CustomOrderUtils.checkRollbackSimpleOrderAvailable(model.getOrder());

        if (!errCollector.hasErrors())
            MoveDaoFacade.getMoveDao().doRollbackOrder(model.getOrder(), UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER), null);
    }

    private String getStudentsList(List<Student> students)
    {
        String resultList = "";
        for (Student student : students)
            resultList += student.getPerson().getFio() + ", ";
        return resultList.length() > 0 ? resultList.substring(0, resultList.length() - 3) : "";
    }

    @Override
    public void deleteExtract(Model model, Long extractId)
    {
        ModularStudentExtract extract = getNotNull(extractId);
        getSession().refresh(model.getOrder());
        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getOrder().getState().getCode()))
            throw new ApplicationException("Нельзя изменять приказ. Изменение приказа возможно только в состоянии «Формируется».");
        MoveDaoFacade.getMoveDao().deleteParagraph(extract.getParagraph(), getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED));
    }

    @Override
    public void deleteOrder(Model model)
    {
        getSession().refresh(model.getOrder());
        if (UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(model.getOrder().getState().getCode()))
            MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(model.getOrder());
        else
            MoveDaoFacade.getMoveDao().deleteOrder(model.getOrder(), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED));
    }

    @Override
    public void deleteOrderWithExtracts(Model model)
    {
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(model.getOrder());
    }
}