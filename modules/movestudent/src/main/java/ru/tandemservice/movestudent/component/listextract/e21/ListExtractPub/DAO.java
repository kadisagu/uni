/* $Id: DAO.java.vm 7906 2009-05-12 11:40:06Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e21.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.movestudent.entity.ChangeScientificAdviserExtract;

/**
 * @author ListExtractComponentGenerator
 * @since 10.02.2011
 */
public class DAO extends AbstractListExtractPubDAO<ChangeScientificAdviserExtract, Model> implements IDAO
{
}
