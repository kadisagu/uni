package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.SessionIndividualAheadStuExtract;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об установлении индивидуального срока сдачи сессии(досрочная сдача сессии)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionIndividualAheadStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.SessionIndividualAheadStuExtract";
    public static final String ENTITY_NAME = "sessionIndividualAheadStuExtract";
    public static final int VERSION_HASH = 1408998306;
    private static IEntityMeta ENTITY_META;

    public static final String P_SESSION_BEGIN_DATE = "sessionBeginDate";
    public static final String P_SESSION_END_DATE = "sessionEndDate";
    public static final String L_SESSION_TYPE = "sessionType";

    private Date _sessionBeginDate;     // Дата начала сессии
    private Date _sessionEndDate;     // Дата окончания сессии
    private YearDistributionPart _sessionType;     // Вид сессии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата начала сессии. Свойство не может быть null.
     */
    @NotNull
    public Date getSessionBeginDate()
    {
        return _sessionBeginDate;
    }

    /**
     * @param sessionBeginDate Дата начала сессии. Свойство не может быть null.
     */
    public void setSessionBeginDate(Date sessionBeginDate)
    {
        dirty(_sessionBeginDate, sessionBeginDate);
        _sessionBeginDate = sessionBeginDate;
    }

    /**
     * @return Дата окончания сессии. Свойство не может быть null.
     */
    @NotNull
    public Date getSessionEndDate()
    {
        return _sessionEndDate;
    }

    /**
     * @param sessionEndDate Дата окончания сессии. Свойство не может быть null.
     */
    public void setSessionEndDate(Date sessionEndDate)
    {
        dirty(_sessionEndDate, sessionEndDate);
        _sessionEndDate = sessionEndDate;
    }

    /**
     * @return Вид сессии. Свойство не может быть null.
     */
    @NotNull
    public YearDistributionPart getSessionType()
    {
        return _sessionType;
    }

    /**
     * @param sessionType Вид сессии. Свойство не может быть null.
     */
    public void setSessionType(YearDistributionPart sessionType)
    {
        dirty(_sessionType, sessionType);
        _sessionType = sessionType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionIndividualAheadStuExtractGen)
        {
            setSessionBeginDate(((SessionIndividualAheadStuExtract)another).getSessionBeginDate());
            setSessionEndDate(((SessionIndividualAheadStuExtract)another).getSessionEndDate());
            setSessionType(((SessionIndividualAheadStuExtract)another).getSessionType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionIndividualAheadStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionIndividualAheadStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new SessionIndividualAheadStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "sessionBeginDate":
                    return obj.getSessionBeginDate();
                case "sessionEndDate":
                    return obj.getSessionEndDate();
                case "sessionType":
                    return obj.getSessionType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "sessionBeginDate":
                    obj.setSessionBeginDate((Date) value);
                    return;
                case "sessionEndDate":
                    obj.setSessionEndDate((Date) value);
                    return;
                case "sessionType":
                    obj.setSessionType((YearDistributionPart) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "sessionBeginDate":
                        return true;
                case "sessionEndDate":
                        return true;
                case "sessionType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "sessionBeginDate":
                    return true;
                case "sessionEndDate":
                    return true;
                case "sessionType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "sessionBeginDate":
                    return Date.class;
                case "sessionEndDate":
                    return Date.class;
                case "sessionType":
                    return YearDistributionPart.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionIndividualAheadStuExtract> _dslPath = new Path<SessionIndividualAheadStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionIndividualAheadStuExtract");
    }
            

    /**
     * @return Дата начала сессии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionIndividualAheadStuExtract#getSessionBeginDate()
     */
    public static PropertyPath<Date> sessionBeginDate()
    {
        return _dslPath.sessionBeginDate();
    }

    /**
     * @return Дата окончания сессии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionIndividualAheadStuExtract#getSessionEndDate()
     */
    public static PropertyPath<Date> sessionEndDate()
    {
        return _dslPath.sessionEndDate();
    }

    /**
     * @return Вид сессии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionIndividualAheadStuExtract#getSessionType()
     */
    public static YearDistributionPart.Path<YearDistributionPart> sessionType()
    {
        return _dslPath.sessionType();
    }

    public static class Path<E extends SessionIndividualAheadStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _sessionBeginDate;
        private PropertyPath<Date> _sessionEndDate;
        private YearDistributionPart.Path<YearDistributionPart> _sessionType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата начала сессии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionIndividualAheadStuExtract#getSessionBeginDate()
     */
        public PropertyPath<Date> sessionBeginDate()
        {
            if(_sessionBeginDate == null )
                _sessionBeginDate = new PropertyPath<Date>(SessionIndividualAheadStuExtractGen.P_SESSION_BEGIN_DATE, this);
            return _sessionBeginDate;
        }

    /**
     * @return Дата окончания сессии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionIndividualAheadStuExtract#getSessionEndDate()
     */
        public PropertyPath<Date> sessionEndDate()
        {
            if(_sessionEndDate == null )
                _sessionEndDate = new PropertyPath<Date>(SessionIndividualAheadStuExtractGen.P_SESSION_END_DATE, this);
            return _sessionEndDate;
        }

    /**
     * @return Вид сессии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SessionIndividualAheadStuExtract#getSessionType()
     */
        public YearDistributionPart.Path<YearDistributionPart> sessionType()
        {
            if(_sessionType == null )
                _sessionType = new YearDistributionPart.Path<YearDistributionPart>(L_SESSION_TYPE, this);
            return _sessionType;
        }

        public Class getEntityClass()
        {
            return SessionIndividualAheadStuExtract.class;
        }

        public String getEntityName()
        {
            return "sessionIndividualAheadStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
