package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.movestudent.entity.gen.GiveDiplSuccessStuListExtractGen;

import java.util.Date;

/**
 * Проект приказа «О выдаче диплома с отличием и присвоении квалификации»
 */
public class GiveDiplSuccessStuListExtract extends GiveDiplSuccessStuListExtractGen implements IExcludeExtract
{
    @Override
    public Date getExcludeDate()
    {
        return getBeginDate();
    }
}