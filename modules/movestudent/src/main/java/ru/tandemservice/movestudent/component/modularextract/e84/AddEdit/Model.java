/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e84.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.RePassDiscComissStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToComissionMember;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;

import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 31.08.2012
 */
public class Model extends CommonModularStudentExtractAddEditModel<RePassDiscComissStuExtract>
{
    private List<StuExtractToComissionMember> _membersList;
    private StuExtractToComissionMember _currentMember;
    List<String> _controlActionsList;

    private ISelectModel _membersPPSMultiSelectModel;
    private List<PpsEntry> _selectedPPSList;

    public Integer getCurrentMemberNumber()
    {
        return _membersList.indexOf(getCurrentMember());
    }

    public boolean isAnyMemberIncluded()
    {
        return !getMembersList().isEmpty();
    }

    public List<StuExtractToComissionMember> getMembersList()
    {
        return _membersList;
    }

    public void setMembersList(List<StuExtractToComissionMember> membersList)
    {
        _membersList = membersList;
    }

    public StuExtractToComissionMember getCurrentMember()
    {
        return _currentMember;
    }

    public void setCurrentMember(StuExtractToComissionMember currentMember)
    {
        _currentMember = currentMember;
    }

    public List<String> getControlActionsList()
    {
        return _controlActionsList;
    }

    public void setControlActionsList(List<String> controlActionsList)
    {
        _controlActionsList = controlActionsList;
    }

    public ISelectModel getMembersPPSMultiSelectModel()
    {
        return _membersPPSMultiSelectModel;
    }

    public void setMembersPPSMultiSelectModel(ISelectModel membersPPSMultiSelectModel)
    {
        _membersPPSMultiSelectModel = membersPPSMultiSelectModel;
    }

    public List<PpsEntry> getSelectedPPSList()
    {
        return _selectedPPSList;
    }

    public void setSelectedPPSList(List<PpsEntry> selectedPPSList)
    {
        _selectedPPSList = selectedPPSList;
    }
}