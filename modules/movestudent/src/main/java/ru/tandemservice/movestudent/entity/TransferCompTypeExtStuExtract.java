package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IEnrollmentExtract;
import ru.tandemservice.movestudent.entity.gen.TransferCompTypeExtStuExtractGen;

import java.util.Date;

/**
 * О переводе на другую основу оплаты обучения (вакантное бюджетное место)
 */
public class TransferCompTypeExtStuExtract extends TransferCompTypeExtStuExtractGen implements IEnrollmentExtract
{
    @Override
    public Date getEntranceDate()
    {
        return getTransferDate();
    }
}