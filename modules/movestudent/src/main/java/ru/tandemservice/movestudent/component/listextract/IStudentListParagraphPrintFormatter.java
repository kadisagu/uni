/**
 * $Id$
 */
package ru.tandemservice.movestudent.component.listextract;

import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author dseleznev
 * Created on: 29.07.2009
 */
public interface IStudentListParagraphPrintFormatter
{
    String formatSingleStudent(Student student, int extractNumber);
}