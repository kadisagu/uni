package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.StudentOrderTextRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь приказа и текста приказа (приказы по студентам)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentOrderTextRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.StudentOrderTextRelation";
    public static final String ENTITY_NAME = "studentOrderTextRelation";
    public static final int VERSION_HASH = 1537620887;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String P_TEXT = "text";

    private AbstractStudentOrder _order;     // Приказ
    private byte[] _text;     // Сохраненная печатная форма

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public AbstractStudentOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ. Свойство не может быть null и должно быть уникальным.
     */
    public void setOrder(AbstractStudentOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     */
    @NotNull
    public byte[] getText()
    {
        return _text;
    }

    /**
     * @param text Сохраненная печатная форма. Свойство не может быть null.
     */
    public void setText(byte[] text)
    {
        dirty(_text, text);
        _text = text;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentOrderTextRelationGen)
        {
            setOrder(((StudentOrderTextRelation)another).getOrder());
            setText(((StudentOrderTextRelation)another).getText());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentOrderTextRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentOrderTextRelation.class;
        }

        public T newInstance()
        {
            return (T) new StudentOrderTextRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "text":
                    return obj.getText();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((AbstractStudentOrder) value);
                    return;
                case "text":
                    obj.setText((byte[]) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "text":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "text":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return AbstractStudentOrder.class;
                case "text":
                    return byte[].class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentOrderTextRelation> _dslPath = new Path<StudentOrderTextRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentOrderTextRelation");
    }
            

    /**
     * @return Приказ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.StudentOrderTextRelation#getOrder()
     */
    public static AbstractStudentOrder.Path<AbstractStudentOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentOrderTextRelation#getText()
     */
    public static PropertyPath<byte[]> text()
    {
        return _dslPath.text();
    }

    public static class Path<E extends StudentOrderTextRelation> extends EntityPath<E>
    {
        private AbstractStudentOrder.Path<AbstractStudentOrder> _order;
        private PropertyPath<byte[]> _text;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.StudentOrderTextRelation#getOrder()
     */
        public AbstractStudentOrder.Path<AbstractStudentOrder> order()
        {
            if(_order == null )
                _order = new AbstractStudentOrder.Path<AbstractStudentOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StudentOrderTextRelation#getText()
     */
        public PropertyPath<byte[]> text()
        {
            if(_text == null )
                _text = new PropertyPath<byte[]>(StudentOrderTextRelationGen.P_TEXT, this);
            return _text;
        }

        public Class getEntityClass()
        {
            return StudentOrderTextRelation.class;
        }

        public String getEntityName()
        {
            return "studentOrderTextRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
