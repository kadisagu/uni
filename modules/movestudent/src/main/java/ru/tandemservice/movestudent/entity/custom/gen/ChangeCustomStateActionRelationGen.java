package ru.tandemservice.movestudent.entity.custom.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.custom.ChangeCustomStateActionRelation;
import ru.tandemservice.movestudent.entity.custom.ChangeCustomStatesAction;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь действия изменения доп. статусов студента с доп. статусом
 *
 * Если признак "Новый статус" установлен, данный статус будет добавлен студенту, иначе - снят со студента.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChangeCustomStateActionRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.custom.ChangeCustomStateActionRelation";
    public static final String ENTITY_NAME = "changeCustomStateActionRelation";
    public static final int VERSION_HASH = 953706339;
    private static IEntityMeta ENTITY_META;

    public static final String L_STATE = "state";
    public static final String L_ACTION = "action";
    public static final String P_NEW_STATE = "newState";

    private StudentCustomStateCI _state;     // Дополнительный статус студента (справочник)
    private ChangeCustomStatesAction _action;     // Изменение набора дополнительных статусов
    private boolean _newState;     // Новый статус

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дополнительный статус студента (справочник). Свойство не может быть null.
     */
    @NotNull
    public StudentCustomStateCI getState()
    {
        return _state;
    }

    /**
     * @param state Дополнительный статус студента (справочник). Свойство не может быть null.
     */
    public void setState(StudentCustomStateCI state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Изменение набора дополнительных статусов. Свойство не может быть null.
     */
    @NotNull
    public ChangeCustomStatesAction getAction()
    {
        return _action;
    }

    /**
     * @param action Изменение набора дополнительных статусов. Свойство не может быть null.
     */
    public void setAction(ChangeCustomStatesAction action)
    {
        dirty(_action, action);
        _action = action;
    }

    /**
     * @return Новый статус. Свойство не может быть null.
     */
    @NotNull
    public boolean isNewState()
    {
        return _newState;
    }

    /**
     * @param newState Новый статус. Свойство не может быть null.
     */
    public void setNewState(boolean newState)
    {
        dirty(_newState, newState);
        _newState = newState;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ChangeCustomStateActionRelationGen)
        {
            setState(((ChangeCustomStateActionRelation)another).getState());
            setAction(((ChangeCustomStateActionRelation)another).getAction());
            setNewState(((ChangeCustomStateActionRelation)another).isNewState());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChangeCustomStateActionRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChangeCustomStateActionRelation.class;
        }

        public T newInstance()
        {
            return (T) new ChangeCustomStateActionRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "state":
                    return obj.getState();
                case "action":
                    return obj.getAction();
                case "newState":
                    return obj.isNewState();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "state":
                    obj.setState((StudentCustomStateCI) value);
                    return;
                case "action":
                    obj.setAction((ChangeCustomStatesAction) value);
                    return;
                case "newState":
                    obj.setNewState((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "state":
                        return true;
                case "action":
                        return true;
                case "newState":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "state":
                    return true;
                case "action":
                    return true;
                case "newState":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "state":
                    return StudentCustomStateCI.class;
                case "action":
                    return ChangeCustomStatesAction.class;
                case "newState":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChangeCustomStateActionRelation> _dslPath = new Path<ChangeCustomStateActionRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChangeCustomStateActionRelation");
    }
            

    /**
     * @return Дополнительный статус студента (справочник). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeCustomStateActionRelation#getState()
     */
    public static StudentCustomStateCI.Path<StudentCustomStateCI> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Изменение набора дополнительных статусов. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeCustomStateActionRelation#getAction()
     */
    public static ChangeCustomStatesAction.Path<ChangeCustomStatesAction> action()
    {
        return _dslPath.action();
    }

    /**
     * @return Новый статус. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeCustomStateActionRelation#isNewState()
     */
    public static PropertyPath<Boolean> newState()
    {
        return _dslPath.newState();
    }

    public static class Path<E extends ChangeCustomStateActionRelation> extends EntityPath<E>
    {
        private StudentCustomStateCI.Path<StudentCustomStateCI> _state;
        private ChangeCustomStatesAction.Path<ChangeCustomStatesAction> _action;
        private PropertyPath<Boolean> _newState;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дополнительный статус студента (справочник). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeCustomStateActionRelation#getState()
     */
        public StudentCustomStateCI.Path<StudentCustomStateCI> state()
        {
            if(_state == null )
                _state = new StudentCustomStateCI.Path<StudentCustomStateCI>(L_STATE, this);
            return _state;
        }

    /**
     * @return Изменение набора дополнительных статусов. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeCustomStateActionRelation#getAction()
     */
        public ChangeCustomStatesAction.Path<ChangeCustomStatesAction> action()
        {
            if(_action == null )
                _action = new ChangeCustomStatesAction.Path<ChangeCustomStatesAction>(L_ACTION, this);
            return _action;
        }

    /**
     * @return Новый статус. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ChangeCustomStateActionRelation#isNewState()
     */
        public PropertyPath<Boolean> newState()
        {
            if(_newState == null )
                _newState = new PropertyPath<Boolean>(ChangeCustomStateActionRelationGen.P_NEW_STATE, this);
            return _newState;
        }

        public Class getEntityClass()
        {
            return ChangeCustomStateActionRelation.class;
        }

        public String getEntityName()
        {
            return "changeCustomStateActionRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
