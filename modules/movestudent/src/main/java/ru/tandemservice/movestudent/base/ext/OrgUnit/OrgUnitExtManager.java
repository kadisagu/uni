/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.movestudent.base.ext.OrgUnit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Vasily Zhukov
 * @since 02.04.2012
 */
@Configuration
public class OrgUnitExtManager extends BusinessObjectExtensionManager
{
}
