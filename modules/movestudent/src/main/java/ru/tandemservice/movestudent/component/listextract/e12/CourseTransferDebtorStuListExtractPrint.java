/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e12;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.CourseTransferDebtorStuListExtract;
import ru.tandemservice.uni.util.NumberConvertingUtil;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author ListExtractComponentGenerator
 * @since 23.07.2009
 */
public class CourseTransferDebtorStuListExtractPrint implements IPrintFormCreator<CourseTransferDebtorStuListExtract>, IListParagraphPrintFormCreator<CourseTransferDebtorStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, CourseTransferDebtorStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, CourseTransferDebtorStuListExtract firstExtract)
    {

    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, CourseTransferDebtorStuListExtract firstExtract)
    {
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract)
                .put("courseOld", firstExtract.getCourseOld().getTitle())
                .put("courseNew", firstExtract.getCourseNew().getTitle())
                .put("courseOldRoman", NumberConvertingUtil.convertDecimal2Roman(Integer.valueOf(firstExtract.getCourseOld().getCode())))
                .put("courseNewRoman", NumberConvertingUtil.convertDecimal2Roman(Integer.valueOf(firstExtract.getCourseNew().getCode())));
        return CommonExtractPrint.addEntryDateToModifier(injectModifier, firstExtract.getTransferDate());
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, CourseTransferDebtorStuListExtract firstExtract)
    {
        return null;
    }
}