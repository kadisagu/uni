\qc\b\'a7 {parNumber}\b0
\par Присвоить квалификацию \'ab{diplomaQualification}\'bb и выдать диплом 
следующим студентам {course} курса {orgUnit_G} {educationType} \'ab{educationOrgUnit}\'bb 
форма обучения \'ab{developForm}\'bb группа \'ab{group}\'bb согласно списку:
\par \par \ql{STUDENT_LIST}
Всего {extractCount} ({extractCountStr}) {people}.\par\par