package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О направлении на практику в пределах ОУ»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SendPracticeInnerStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract";
    public static final String ENTITY_NAME = "sendPracticeInnerStuListExtract";
    public static final int VERSION_HASH = -1099021112;
    private static IEntityMeta ENTITY_META;

    public static final String P_PRACTICE_KIND = "practiceKind";
    public static final String P_PRACTICE_DURATION_STR = "practiceDurationStr";
    public static final String P_PRACTICE_BEGIN_DATE = "practiceBeginDate";
    public static final String P_PRACTICE_END_DATE = "practiceEndDate";
    public static final String P_ATTESTATION_DATE = "attestationDate";
    public static final String P_OUT_CLASS_TIME = "outClassTime";
    public static final String P_DONE_EDU_PLAN = "doneEduPlan";
    public static final String P_DONE_PRACTICE = "donePractice";
    public static final String P_PROVIDE_FUNDS_ACCORDING_TO_ESTIMATES = "provideFundsAccordingToEstimates";
    public static final String P_ESTIMATE_NUM = "estimateNum";
    public static final String P_RESPONS_FOR_RECIEVE_CASH_STR = "responsForRecieveCashStr";
    public static final String P_PRACTICE_ORG_UNIT_STR = "practiceOrgUnitStr";
    public static final String P_PRACTICE_HEADER_STR = "practiceHeaderStr";
    public static final String P_PREVENT_ACCIDENTS_I_C_STR = "preventAccidentsICStr";
    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_PRACTICE_COURSE = "practiceCourse";
    public static final String L_PREVENT_ACCIDENTS_I_C = "preventAccidentsIC";
    public static final String L_PREVENT_ACCIDENTS_I_C_DEGREE = "preventAccidentsICDegree";
    public static final String L_RESPONS_FOR_RECIEVE_CASH = "responsForRecieveCash";
    public static final String L_RESPONS_FOR_RECIEVE_CASH_DEGREE = "responsForRecieveCashDegree";
    public static final String L_PRACTICE_ORG_UNIT = "practiceOrgUnit";
    public static final String L_PRACTICE_HEADER = "practiceHeader";
    public static final String L_PRACTICE_HEADER_DEGREE = "practiceHeaderDegree";

    private String _practiceKind;     // Вид практики
    private String _practiceDurationStr;     // Объем(недель)
    private Date _practiceBeginDate;     // Дата начала практики
    private Date _practiceEndDate;     // Дата окончания практики
    private Date _attestationDate;     // Дата аттестации по итогам практики
    private boolean _outClassTime;     // В свободное от аудиторных занятий время
    private boolean _doneEduPlan;     // Полное выполнение учебного плана
    private boolean _donePractice;     // Считать прошедшими практику
    private boolean _provideFundsAccordingToEstimates = false;     // Выделить средства согласно сметам
    private String _estimateNum;     // Номер приложения(сметы расходов)
    private String _responsForRecieveCashStr;     // Ответственный за получение денежных средств(печать)
    private String _practiceOrgUnitStr;     // Подразделение(печать)
    private String _practiceHeaderStr;     // Руководитель практики(печать)
    private String _preventAccidentsICStr;     // Ответственный за соблюдение техники безопасности (в вин.падеже для печати)
    private Course _course;     // Курс
    private Group _group;     // Группа
    private Course _practiceCourse;     // Курс прохождения практики
    private EmployeePost _preventAccidentsIC;     // Ответственный за соблюдение техники безопасности
    private PersonAcademicDegree _preventAccidentsICDegree;     // Ответственный за соблюдение техники безопасности(уч. степень)
    private EmployeePost _responsForRecieveCash;     // Ответственный за получение денежных средств
    private PersonAcademicDegree _responsForRecieveCashDegree;     // Ответственный за получение денежных средств(уч. степень)
    private OrgUnit _practiceOrgUnit;     // Подразделение
    private EmployeePost _practiceHeader;     // Руководитель практики
    private PersonAcademicDegree _practiceHeaderDegree;     // Руководитель практики(уч. степень)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид практики. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPracticeKind()
    {
        return _practiceKind;
    }

    /**
     * @param practiceKind Вид практики. Свойство не может быть null.
     */
    public void setPracticeKind(String practiceKind)
    {
        dirty(_practiceKind, practiceKind);
        _practiceKind = practiceKind;
    }

    /**
     * @return Объем(недель). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPracticeDurationStr()
    {
        return _practiceDurationStr;
    }

    /**
     * @param practiceDurationStr Объем(недель). Свойство не может быть null.
     */
    public void setPracticeDurationStr(String practiceDurationStr)
    {
        dirty(_practiceDurationStr, practiceDurationStr);
        _practiceDurationStr = practiceDurationStr;
    }

    /**
     * @return Дата начала практики. Свойство не может быть null.
     */
    @NotNull
    public Date getPracticeBeginDate()
    {
        return _practiceBeginDate;
    }

    /**
     * @param practiceBeginDate Дата начала практики. Свойство не может быть null.
     */
    public void setPracticeBeginDate(Date practiceBeginDate)
    {
        dirty(_practiceBeginDate, practiceBeginDate);
        _practiceBeginDate = practiceBeginDate;
    }

    /**
     * @return Дата окончания практики. Свойство не может быть null.
     */
    @NotNull
    public Date getPracticeEndDate()
    {
        return _practiceEndDate;
    }

    /**
     * @param practiceEndDate Дата окончания практики. Свойство не может быть null.
     */
    public void setPracticeEndDate(Date practiceEndDate)
    {
        dirty(_practiceEndDate, practiceEndDate);
        _practiceEndDate = practiceEndDate;
    }

    /**
     * @return Дата аттестации по итогам практики. Свойство не может быть null.
     */
    @NotNull
    public Date getAttestationDate()
    {
        return _attestationDate;
    }

    /**
     * @param attestationDate Дата аттестации по итогам практики. Свойство не может быть null.
     */
    public void setAttestationDate(Date attestationDate)
    {
        dirty(_attestationDate, attestationDate);
        _attestationDate = attestationDate;
    }

    /**
     * @return В свободное от аудиторных занятий время. Свойство не может быть null.
     */
    @NotNull
    public boolean isOutClassTime()
    {
        return _outClassTime;
    }

    /**
     * @param outClassTime В свободное от аудиторных занятий время. Свойство не может быть null.
     */
    public void setOutClassTime(boolean outClassTime)
    {
        dirty(_outClassTime, outClassTime);
        _outClassTime = outClassTime;
    }

    /**
     * @return Полное выполнение учебного плана. Свойство не может быть null.
     */
    @NotNull
    public boolean isDoneEduPlan()
    {
        return _doneEduPlan;
    }

    /**
     * @param doneEduPlan Полное выполнение учебного плана. Свойство не может быть null.
     */
    public void setDoneEduPlan(boolean doneEduPlan)
    {
        dirty(_doneEduPlan, doneEduPlan);
        _doneEduPlan = doneEduPlan;
    }

    /**
     * @return Считать прошедшими практику. Свойство не может быть null.
     */
    @NotNull
    public boolean isDonePractice()
    {
        return _donePractice;
    }

    /**
     * @param donePractice Считать прошедшими практику. Свойство не может быть null.
     */
    public void setDonePractice(boolean donePractice)
    {
        dirty(_donePractice, donePractice);
        _donePractice = donePractice;
    }

    /**
     * @return Выделить средства согласно сметам. Свойство не может быть null.
     */
    @NotNull
    public boolean isProvideFundsAccordingToEstimates()
    {
        return _provideFundsAccordingToEstimates;
    }

    /**
     * @param provideFundsAccordingToEstimates Выделить средства согласно сметам. Свойство не может быть null.
     */
    public void setProvideFundsAccordingToEstimates(boolean provideFundsAccordingToEstimates)
    {
        dirty(_provideFundsAccordingToEstimates, provideFundsAccordingToEstimates);
        _provideFundsAccordingToEstimates = provideFundsAccordingToEstimates;
    }

    /**
     * @return Номер приложения(сметы расходов).
     */
    @Length(max=255)
    public String getEstimateNum()
    {
        return _estimateNum;
    }

    /**
     * @param estimateNum Номер приложения(сметы расходов).
     */
    public void setEstimateNum(String estimateNum)
    {
        dirty(_estimateNum, estimateNum);
        _estimateNum = estimateNum;
    }

    /**
     * @return Ответственный за получение денежных средств(печать).
     */
    @Length(max=255)
    public String getResponsForRecieveCashStr()
    {
        return _responsForRecieveCashStr;
    }

    /**
     * @param responsForRecieveCashStr Ответственный за получение денежных средств(печать).
     */
    public void setResponsForRecieveCashStr(String responsForRecieveCashStr)
    {
        dirty(_responsForRecieveCashStr, responsForRecieveCashStr);
        _responsForRecieveCashStr = responsForRecieveCashStr;
    }

    /**
     * @return Подразделение(печать). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPracticeOrgUnitStr()
    {
        return _practiceOrgUnitStr;
    }

    /**
     * @param practiceOrgUnitStr Подразделение(печать). Свойство не может быть null.
     */
    public void setPracticeOrgUnitStr(String practiceOrgUnitStr)
    {
        dirty(_practiceOrgUnitStr, practiceOrgUnitStr);
        _practiceOrgUnitStr = practiceOrgUnitStr;
    }

    /**
     * @return Руководитель практики(печать).
     */
    @Length(max=255)
    public String getPracticeHeaderStr()
    {
        return _practiceHeaderStr;
    }

    /**
     * @param practiceHeaderStr Руководитель практики(печать).
     */
    public void setPracticeHeaderStr(String practiceHeaderStr)
    {
        dirty(_practiceHeaderStr, practiceHeaderStr);
        _practiceHeaderStr = practiceHeaderStr;
    }

    /**
     * @return Ответственный за соблюдение техники безопасности (в вин.падеже для печати).
     */
    @Length(max=255)
    public String getPreventAccidentsICStr()
    {
        return _preventAccidentsICStr;
    }

    /**
     * @param preventAccidentsICStr Ответственный за соблюдение техники безопасности (в вин.падеже для печати).
     */
    public void setPreventAccidentsICStr(String preventAccidentsICStr)
    {
        dirty(_preventAccidentsICStr, preventAccidentsICStr);
        _preventAccidentsICStr = preventAccidentsICStr;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Курс прохождения практики. Свойство не может быть null.
     */
    @NotNull
    public Course getPracticeCourse()
    {
        return _practiceCourse;
    }

    /**
     * @param practiceCourse Курс прохождения практики. Свойство не может быть null.
     */
    public void setPracticeCourse(Course practiceCourse)
    {
        dirty(_practiceCourse, practiceCourse);
        _practiceCourse = practiceCourse;
    }

    /**
     * @return Ответственный за соблюдение техники безопасности.
     */
    public EmployeePost getPreventAccidentsIC()
    {
        return _preventAccidentsIC;
    }

    /**
     * @param preventAccidentsIC Ответственный за соблюдение техники безопасности.
     */
    public void setPreventAccidentsIC(EmployeePost preventAccidentsIC)
    {
        dirty(_preventAccidentsIC, preventAccidentsIC);
        _preventAccidentsIC = preventAccidentsIC;
    }

    /**
     * @return Ответственный за соблюдение техники безопасности(уч. степень).
     */
    public PersonAcademicDegree getPreventAccidentsICDegree()
    {
        return _preventAccidentsICDegree;
    }

    /**
     * @param preventAccidentsICDegree Ответственный за соблюдение техники безопасности(уч. степень).
     */
    public void setPreventAccidentsICDegree(PersonAcademicDegree preventAccidentsICDegree)
    {
        dirty(_preventAccidentsICDegree, preventAccidentsICDegree);
        _preventAccidentsICDegree = preventAccidentsICDegree;
    }

    /**
     * @return Ответственный за получение денежных средств.
     */
    public EmployeePost getResponsForRecieveCash()
    {
        return _responsForRecieveCash;
    }

    /**
     * @param responsForRecieveCash Ответственный за получение денежных средств.
     */
    public void setResponsForRecieveCash(EmployeePost responsForRecieveCash)
    {
        dirty(_responsForRecieveCash, responsForRecieveCash);
        _responsForRecieveCash = responsForRecieveCash;
    }

    /**
     * @return Ответственный за получение денежных средств(уч. степень).
     */
    public PersonAcademicDegree getResponsForRecieveCashDegree()
    {
        return _responsForRecieveCashDegree;
    }

    /**
     * @param responsForRecieveCashDegree Ответственный за получение денежных средств(уч. степень).
     */
    public void setResponsForRecieveCashDegree(PersonAcademicDegree responsForRecieveCashDegree)
    {
        dirty(_responsForRecieveCashDegree, responsForRecieveCashDegree);
        _responsForRecieveCashDegree = responsForRecieveCashDegree;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getPracticeOrgUnit()
    {
        return _practiceOrgUnit;
    }

    /**
     * @param practiceOrgUnit Подразделение. Свойство не может быть null.
     */
    public void setPracticeOrgUnit(OrgUnit practiceOrgUnit)
    {
        dirty(_practiceOrgUnit, practiceOrgUnit);
        _practiceOrgUnit = practiceOrgUnit;
    }

    /**
     * @return Руководитель практики.
     */
    public EmployeePost getPracticeHeader()
    {
        return _practiceHeader;
    }

    /**
     * @param practiceHeader Руководитель практики.
     */
    public void setPracticeHeader(EmployeePost practiceHeader)
    {
        dirty(_practiceHeader, practiceHeader);
        _practiceHeader = practiceHeader;
    }

    /**
     * @return Руководитель практики(уч. степень).
     */
    public PersonAcademicDegree getPracticeHeaderDegree()
    {
        return _practiceHeaderDegree;
    }

    /**
     * @param practiceHeaderDegree Руководитель практики(уч. степень).
     */
    public void setPracticeHeaderDegree(PersonAcademicDegree practiceHeaderDegree)
    {
        dirty(_practiceHeaderDegree, practiceHeaderDegree);
        _practiceHeaderDegree = practiceHeaderDegree;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SendPracticeInnerStuListExtractGen)
        {
            setPracticeKind(((SendPracticeInnerStuListExtract)another).getPracticeKind());
            setPracticeDurationStr(((SendPracticeInnerStuListExtract)another).getPracticeDurationStr());
            setPracticeBeginDate(((SendPracticeInnerStuListExtract)another).getPracticeBeginDate());
            setPracticeEndDate(((SendPracticeInnerStuListExtract)another).getPracticeEndDate());
            setAttestationDate(((SendPracticeInnerStuListExtract)another).getAttestationDate());
            setOutClassTime(((SendPracticeInnerStuListExtract)another).isOutClassTime());
            setDoneEduPlan(((SendPracticeInnerStuListExtract)another).isDoneEduPlan());
            setDonePractice(((SendPracticeInnerStuListExtract)another).isDonePractice());
            setProvideFundsAccordingToEstimates(((SendPracticeInnerStuListExtract)another).isProvideFundsAccordingToEstimates());
            setEstimateNum(((SendPracticeInnerStuListExtract)another).getEstimateNum());
            setResponsForRecieveCashStr(((SendPracticeInnerStuListExtract)another).getResponsForRecieveCashStr());
            setPracticeOrgUnitStr(((SendPracticeInnerStuListExtract)another).getPracticeOrgUnitStr());
            setPracticeHeaderStr(((SendPracticeInnerStuListExtract)another).getPracticeHeaderStr());
            setPreventAccidentsICStr(((SendPracticeInnerStuListExtract)another).getPreventAccidentsICStr());
            setCourse(((SendPracticeInnerStuListExtract)another).getCourse());
            setGroup(((SendPracticeInnerStuListExtract)another).getGroup());
            setPracticeCourse(((SendPracticeInnerStuListExtract)another).getPracticeCourse());
            setPreventAccidentsIC(((SendPracticeInnerStuListExtract)another).getPreventAccidentsIC());
            setPreventAccidentsICDegree(((SendPracticeInnerStuListExtract)another).getPreventAccidentsICDegree());
            setResponsForRecieveCash(((SendPracticeInnerStuListExtract)another).getResponsForRecieveCash());
            setResponsForRecieveCashDegree(((SendPracticeInnerStuListExtract)another).getResponsForRecieveCashDegree());
            setPracticeOrgUnit(((SendPracticeInnerStuListExtract)another).getPracticeOrgUnit());
            setPracticeHeader(((SendPracticeInnerStuListExtract)another).getPracticeHeader());
            setPracticeHeaderDegree(((SendPracticeInnerStuListExtract)another).getPracticeHeaderDegree());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SendPracticeInnerStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SendPracticeInnerStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new SendPracticeInnerStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "practiceKind":
                    return obj.getPracticeKind();
                case "practiceDurationStr":
                    return obj.getPracticeDurationStr();
                case "practiceBeginDate":
                    return obj.getPracticeBeginDate();
                case "practiceEndDate":
                    return obj.getPracticeEndDate();
                case "attestationDate":
                    return obj.getAttestationDate();
                case "outClassTime":
                    return obj.isOutClassTime();
                case "doneEduPlan":
                    return obj.isDoneEduPlan();
                case "donePractice":
                    return obj.isDonePractice();
                case "provideFundsAccordingToEstimates":
                    return obj.isProvideFundsAccordingToEstimates();
                case "estimateNum":
                    return obj.getEstimateNum();
                case "responsForRecieveCashStr":
                    return obj.getResponsForRecieveCashStr();
                case "practiceOrgUnitStr":
                    return obj.getPracticeOrgUnitStr();
                case "practiceHeaderStr":
                    return obj.getPracticeHeaderStr();
                case "preventAccidentsICStr":
                    return obj.getPreventAccidentsICStr();
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "practiceCourse":
                    return obj.getPracticeCourse();
                case "preventAccidentsIC":
                    return obj.getPreventAccidentsIC();
                case "preventAccidentsICDegree":
                    return obj.getPreventAccidentsICDegree();
                case "responsForRecieveCash":
                    return obj.getResponsForRecieveCash();
                case "responsForRecieveCashDegree":
                    return obj.getResponsForRecieveCashDegree();
                case "practiceOrgUnit":
                    return obj.getPracticeOrgUnit();
                case "practiceHeader":
                    return obj.getPracticeHeader();
                case "practiceHeaderDegree":
                    return obj.getPracticeHeaderDegree();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "practiceKind":
                    obj.setPracticeKind((String) value);
                    return;
                case "practiceDurationStr":
                    obj.setPracticeDurationStr((String) value);
                    return;
                case "practiceBeginDate":
                    obj.setPracticeBeginDate((Date) value);
                    return;
                case "practiceEndDate":
                    obj.setPracticeEndDate((Date) value);
                    return;
                case "attestationDate":
                    obj.setAttestationDate((Date) value);
                    return;
                case "outClassTime":
                    obj.setOutClassTime((Boolean) value);
                    return;
                case "doneEduPlan":
                    obj.setDoneEduPlan((Boolean) value);
                    return;
                case "donePractice":
                    obj.setDonePractice((Boolean) value);
                    return;
                case "provideFundsAccordingToEstimates":
                    obj.setProvideFundsAccordingToEstimates((Boolean) value);
                    return;
                case "estimateNum":
                    obj.setEstimateNum((String) value);
                    return;
                case "responsForRecieveCashStr":
                    obj.setResponsForRecieveCashStr((String) value);
                    return;
                case "practiceOrgUnitStr":
                    obj.setPracticeOrgUnitStr((String) value);
                    return;
                case "practiceHeaderStr":
                    obj.setPracticeHeaderStr((String) value);
                    return;
                case "preventAccidentsICStr":
                    obj.setPreventAccidentsICStr((String) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "practiceCourse":
                    obj.setPracticeCourse((Course) value);
                    return;
                case "preventAccidentsIC":
                    obj.setPreventAccidentsIC((EmployeePost) value);
                    return;
                case "preventAccidentsICDegree":
                    obj.setPreventAccidentsICDegree((PersonAcademicDegree) value);
                    return;
                case "responsForRecieveCash":
                    obj.setResponsForRecieveCash((EmployeePost) value);
                    return;
                case "responsForRecieveCashDegree":
                    obj.setResponsForRecieveCashDegree((PersonAcademicDegree) value);
                    return;
                case "practiceOrgUnit":
                    obj.setPracticeOrgUnit((OrgUnit) value);
                    return;
                case "practiceHeader":
                    obj.setPracticeHeader((EmployeePost) value);
                    return;
                case "practiceHeaderDegree":
                    obj.setPracticeHeaderDegree((PersonAcademicDegree) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "practiceKind":
                        return true;
                case "practiceDurationStr":
                        return true;
                case "practiceBeginDate":
                        return true;
                case "practiceEndDate":
                        return true;
                case "attestationDate":
                        return true;
                case "outClassTime":
                        return true;
                case "doneEduPlan":
                        return true;
                case "donePractice":
                        return true;
                case "provideFundsAccordingToEstimates":
                        return true;
                case "estimateNum":
                        return true;
                case "responsForRecieveCashStr":
                        return true;
                case "practiceOrgUnitStr":
                        return true;
                case "practiceHeaderStr":
                        return true;
                case "preventAccidentsICStr":
                        return true;
                case "course":
                        return true;
                case "group":
                        return true;
                case "practiceCourse":
                        return true;
                case "preventAccidentsIC":
                        return true;
                case "preventAccidentsICDegree":
                        return true;
                case "responsForRecieveCash":
                        return true;
                case "responsForRecieveCashDegree":
                        return true;
                case "practiceOrgUnit":
                        return true;
                case "practiceHeader":
                        return true;
                case "practiceHeaderDegree":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "practiceKind":
                    return true;
                case "practiceDurationStr":
                    return true;
                case "practiceBeginDate":
                    return true;
                case "practiceEndDate":
                    return true;
                case "attestationDate":
                    return true;
                case "outClassTime":
                    return true;
                case "doneEduPlan":
                    return true;
                case "donePractice":
                    return true;
                case "provideFundsAccordingToEstimates":
                    return true;
                case "estimateNum":
                    return true;
                case "responsForRecieveCashStr":
                    return true;
                case "practiceOrgUnitStr":
                    return true;
                case "practiceHeaderStr":
                    return true;
                case "preventAccidentsICStr":
                    return true;
                case "course":
                    return true;
                case "group":
                    return true;
                case "practiceCourse":
                    return true;
                case "preventAccidentsIC":
                    return true;
                case "preventAccidentsICDegree":
                    return true;
                case "responsForRecieveCash":
                    return true;
                case "responsForRecieveCashDegree":
                    return true;
                case "practiceOrgUnit":
                    return true;
                case "practiceHeader":
                    return true;
                case "practiceHeaderDegree":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "practiceKind":
                    return String.class;
                case "practiceDurationStr":
                    return String.class;
                case "practiceBeginDate":
                    return Date.class;
                case "practiceEndDate":
                    return Date.class;
                case "attestationDate":
                    return Date.class;
                case "outClassTime":
                    return Boolean.class;
                case "doneEduPlan":
                    return Boolean.class;
                case "donePractice":
                    return Boolean.class;
                case "provideFundsAccordingToEstimates":
                    return Boolean.class;
                case "estimateNum":
                    return String.class;
                case "responsForRecieveCashStr":
                    return String.class;
                case "practiceOrgUnitStr":
                    return String.class;
                case "practiceHeaderStr":
                    return String.class;
                case "preventAccidentsICStr":
                    return String.class;
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "practiceCourse":
                    return Course.class;
                case "preventAccidentsIC":
                    return EmployeePost.class;
                case "preventAccidentsICDegree":
                    return PersonAcademicDegree.class;
                case "responsForRecieveCash":
                    return EmployeePost.class;
                case "responsForRecieveCashDegree":
                    return PersonAcademicDegree.class;
                case "practiceOrgUnit":
                    return OrgUnit.class;
                case "practiceHeader":
                    return EmployeePost.class;
                case "practiceHeaderDegree":
                    return PersonAcademicDegree.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SendPracticeInnerStuListExtract> _dslPath = new Path<SendPracticeInnerStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SendPracticeInnerStuListExtract");
    }
            

    /**
     * @return Вид практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeKind()
     */
    public static PropertyPath<String> practiceKind()
    {
        return _dslPath.practiceKind();
    }

    /**
     * @return Объем(недель). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeDurationStr()
     */
    public static PropertyPath<String> practiceDurationStr()
    {
        return _dslPath.practiceDurationStr();
    }

    /**
     * @return Дата начала практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeBeginDate()
     */
    public static PropertyPath<Date> practiceBeginDate()
    {
        return _dslPath.practiceBeginDate();
    }

    /**
     * @return Дата окончания практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeEndDate()
     */
    public static PropertyPath<Date> practiceEndDate()
    {
        return _dslPath.practiceEndDate();
    }

    /**
     * @return Дата аттестации по итогам практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getAttestationDate()
     */
    public static PropertyPath<Date> attestationDate()
    {
        return _dslPath.attestationDate();
    }

    /**
     * @return В свободное от аудиторных занятий время. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#isOutClassTime()
     */
    public static PropertyPath<Boolean> outClassTime()
    {
        return _dslPath.outClassTime();
    }

    /**
     * @return Полное выполнение учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#isDoneEduPlan()
     */
    public static PropertyPath<Boolean> doneEduPlan()
    {
        return _dslPath.doneEduPlan();
    }

    /**
     * @return Считать прошедшими практику. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#isDonePractice()
     */
    public static PropertyPath<Boolean> donePractice()
    {
        return _dslPath.donePractice();
    }

    /**
     * @return Выделить средства согласно сметам. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#isProvideFundsAccordingToEstimates()
     */
    public static PropertyPath<Boolean> provideFundsAccordingToEstimates()
    {
        return _dslPath.provideFundsAccordingToEstimates();
    }

    /**
     * @return Номер приложения(сметы расходов).
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getEstimateNum()
     */
    public static PropertyPath<String> estimateNum()
    {
        return _dslPath.estimateNum();
    }

    /**
     * @return Ответственный за получение денежных средств(печать).
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getResponsForRecieveCashStr()
     */
    public static PropertyPath<String> responsForRecieveCashStr()
    {
        return _dslPath.responsForRecieveCashStr();
    }

    /**
     * @return Подразделение(печать). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeOrgUnitStr()
     */
    public static PropertyPath<String> practiceOrgUnitStr()
    {
        return _dslPath.practiceOrgUnitStr();
    }

    /**
     * @return Руководитель практики(печать).
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeHeaderStr()
     */
    public static PropertyPath<String> practiceHeaderStr()
    {
        return _dslPath.practiceHeaderStr();
    }

    /**
     * @return Ответственный за соблюдение техники безопасности (в вин.падеже для печати).
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPreventAccidentsICStr()
     */
    public static PropertyPath<String> preventAccidentsICStr()
    {
        return _dslPath.preventAccidentsICStr();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Курс прохождения практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeCourse()
     */
    public static Course.Path<Course> practiceCourse()
    {
        return _dslPath.practiceCourse();
    }

    /**
     * @return Ответственный за соблюдение техники безопасности.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPreventAccidentsIC()
     */
    public static EmployeePost.Path<EmployeePost> preventAccidentsIC()
    {
        return _dslPath.preventAccidentsIC();
    }

    /**
     * @return Ответственный за соблюдение техники безопасности(уч. степень).
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPreventAccidentsICDegree()
     */
    public static PersonAcademicDegree.Path<PersonAcademicDegree> preventAccidentsICDegree()
    {
        return _dslPath.preventAccidentsICDegree();
    }

    /**
     * @return Ответственный за получение денежных средств.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getResponsForRecieveCash()
     */
    public static EmployeePost.Path<EmployeePost> responsForRecieveCash()
    {
        return _dslPath.responsForRecieveCash();
    }

    /**
     * @return Ответственный за получение денежных средств(уч. степень).
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getResponsForRecieveCashDegree()
     */
    public static PersonAcademicDegree.Path<PersonAcademicDegree> responsForRecieveCashDegree()
    {
        return _dslPath.responsForRecieveCashDegree();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> practiceOrgUnit()
    {
        return _dslPath.practiceOrgUnit();
    }

    /**
     * @return Руководитель практики.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeHeader()
     */
    public static EmployeePost.Path<EmployeePost> practiceHeader()
    {
        return _dslPath.practiceHeader();
    }

    /**
     * @return Руководитель практики(уч. степень).
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeHeaderDegree()
     */
    public static PersonAcademicDegree.Path<PersonAcademicDegree> practiceHeaderDegree()
    {
        return _dslPath.practiceHeaderDegree();
    }

    public static class Path<E extends SendPracticeInnerStuListExtract> extends ListStudentExtract.Path<E>
    {
        private PropertyPath<String> _practiceKind;
        private PropertyPath<String> _practiceDurationStr;
        private PropertyPath<Date> _practiceBeginDate;
        private PropertyPath<Date> _practiceEndDate;
        private PropertyPath<Date> _attestationDate;
        private PropertyPath<Boolean> _outClassTime;
        private PropertyPath<Boolean> _doneEduPlan;
        private PropertyPath<Boolean> _donePractice;
        private PropertyPath<Boolean> _provideFundsAccordingToEstimates;
        private PropertyPath<String> _estimateNum;
        private PropertyPath<String> _responsForRecieveCashStr;
        private PropertyPath<String> _practiceOrgUnitStr;
        private PropertyPath<String> _practiceHeaderStr;
        private PropertyPath<String> _preventAccidentsICStr;
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private Course.Path<Course> _practiceCourse;
        private EmployeePost.Path<EmployeePost> _preventAccidentsIC;
        private PersonAcademicDegree.Path<PersonAcademicDegree> _preventAccidentsICDegree;
        private EmployeePost.Path<EmployeePost> _responsForRecieveCash;
        private PersonAcademicDegree.Path<PersonAcademicDegree> _responsForRecieveCashDegree;
        private OrgUnit.Path<OrgUnit> _practiceOrgUnit;
        private EmployeePost.Path<EmployeePost> _practiceHeader;
        private PersonAcademicDegree.Path<PersonAcademicDegree> _practiceHeaderDegree;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeKind()
     */
        public PropertyPath<String> practiceKind()
        {
            if(_practiceKind == null )
                _practiceKind = new PropertyPath<String>(SendPracticeInnerStuListExtractGen.P_PRACTICE_KIND, this);
            return _practiceKind;
        }

    /**
     * @return Объем(недель). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeDurationStr()
     */
        public PropertyPath<String> practiceDurationStr()
        {
            if(_practiceDurationStr == null )
                _practiceDurationStr = new PropertyPath<String>(SendPracticeInnerStuListExtractGen.P_PRACTICE_DURATION_STR, this);
            return _practiceDurationStr;
        }

    /**
     * @return Дата начала практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeBeginDate()
     */
        public PropertyPath<Date> practiceBeginDate()
        {
            if(_practiceBeginDate == null )
                _practiceBeginDate = new PropertyPath<Date>(SendPracticeInnerStuListExtractGen.P_PRACTICE_BEGIN_DATE, this);
            return _practiceBeginDate;
        }

    /**
     * @return Дата окончания практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeEndDate()
     */
        public PropertyPath<Date> practiceEndDate()
        {
            if(_practiceEndDate == null )
                _practiceEndDate = new PropertyPath<Date>(SendPracticeInnerStuListExtractGen.P_PRACTICE_END_DATE, this);
            return _practiceEndDate;
        }

    /**
     * @return Дата аттестации по итогам практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getAttestationDate()
     */
        public PropertyPath<Date> attestationDate()
        {
            if(_attestationDate == null )
                _attestationDate = new PropertyPath<Date>(SendPracticeInnerStuListExtractGen.P_ATTESTATION_DATE, this);
            return _attestationDate;
        }

    /**
     * @return В свободное от аудиторных занятий время. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#isOutClassTime()
     */
        public PropertyPath<Boolean> outClassTime()
        {
            if(_outClassTime == null )
                _outClassTime = new PropertyPath<Boolean>(SendPracticeInnerStuListExtractGen.P_OUT_CLASS_TIME, this);
            return _outClassTime;
        }

    /**
     * @return Полное выполнение учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#isDoneEduPlan()
     */
        public PropertyPath<Boolean> doneEduPlan()
        {
            if(_doneEduPlan == null )
                _doneEduPlan = new PropertyPath<Boolean>(SendPracticeInnerStuListExtractGen.P_DONE_EDU_PLAN, this);
            return _doneEduPlan;
        }

    /**
     * @return Считать прошедшими практику. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#isDonePractice()
     */
        public PropertyPath<Boolean> donePractice()
        {
            if(_donePractice == null )
                _donePractice = new PropertyPath<Boolean>(SendPracticeInnerStuListExtractGen.P_DONE_PRACTICE, this);
            return _donePractice;
        }

    /**
     * @return Выделить средства согласно сметам. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#isProvideFundsAccordingToEstimates()
     */
        public PropertyPath<Boolean> provideFundsAccordingToEstimates()
        {
            if(_provideFundsAccordingToEstimates == null )
                _provideFundsAccordingToEstimates = new PropertyPath<Boolean>(SendPracticeInnerStuListExtractGen.P_PROVIDE_FUNDS_ACCORDING_TO_ESTIMATES, this);
            return _provideFundsAccordingToEstimates;
        }

    /**
     * @return Номер приложения(сметы расходов).
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getEstimateNum()
     */
        public PropertyPath<String> estimateNum()
        {
            if(_estimateNum == null )
                _estimateNum = new PropertyPath<String>(SendPracticeInnerStuListExtractGen.P_ESTIMATE_NUM, this);
            return _estimateNum;
        }

    /**
     * @return Ответственный за получение денежных средств(печать).
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getResponsForRecieveCashStr()
     */
        public PropertyPath<String> responsForRecieveCashStr()
        {
            if(_responsForRecieveCashStr == null )
                _responsForRecieveCashStr = new PropertyPath<String>(SendPracticeInnerStuListExtractGen.P_RESPONS_FOR_RECIEVE_CASH_STR, this);
            return _responsForRecieveCashStr;
        }

    /**
     * @return Подразделение(печать). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeOrgUnitStr()
     */
        public PropertyPath<String> practiceOrgUnitStr()
        {
            if(_practiceOrgUnitStr == null )
                _practiceOrgUnitStr = new PropertyPath<String>(SendPracticeInnerStuListExtractGen.P_PRACTICE_ORG_UNIT_STR, this);
            return _practiceOrgUnitStr;
        }

    /**
     * @return Руководитель практики(печать).
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeHeaderStr()
     */
        public PropertyPath<String> practiceHeaderStr()
        {
            if(_practiceHeaderStr == null )
                _practiceHeaderStr = new PropertyPath<String>(SendPracticeInnerStuListExtractGen.P_PRACTICE_HEADER_STR, this);
            return _practiceHeaderStr;
        }

    /**
     * @return Ответственный за соблюдение техники безопасности (в вин.падеже для печати).
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPreventAccidentsICStr()
     */
        public PropertyPath<String> preventAccidentsICStr()
        {
            if(_preventAccidentsICStr == null )
                _preventAccidentsICStr = new PropertyPath<String>(SendPracticeInnerStuListExtractGen.P_PREVENT_ACCIDENTS_I_C_STR, this);
            return _preventAccidentsICStr;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Курс прохождения практики. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeCourse()
     */
        public Course.Path<Course> practiceCourse()
        {
            if(_practiceCourse == null )
                _practiceCourse = new Course.Path<Course>(L_PRACTICE_COURSE, this);
            return _practiceCourse;
        }

    /**
     * @return Ответственный за соблюдение техники безопасности.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPreventAccidentsIC()
     */
        public EmployeePost.Path<EmployeePost> preventAccidentsIC()
        {
            if(_preventAccidentsIC == null )
                _preventAccidentsIC = new EmployeePost.Path<EmployeePost>(L_PREVENT_ACCIDENTS_I_C, this);
            return _preventAccidentsIC;
        }

    /**
     * @return Ответственный за соблюдение техники безопасности(уч. степень).
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPreventAccidentsICDegree()
     */
        public PersonAcademicDegree.Path<PersonAcademicDegree> preventAccidentsICDegree()
        {
            if(_preventAccidentsICDegree == null )
                _preventAccidentsICDegree = new PersonAcademicDegree.Path<PersonAcademicDegree>(L_PREVENT_ACCIDENTS_I_C_DEGREE, this);
            return _preventAccidentsICDegree;
        }

    /**
     * @return Ответственный за получение денежных средств.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getResponsForRecieveCash()
     */
        public EmployeePost.Path<EmployeePost> responsForRecieveCash()
        {
            if(_responsForRecieveCash == null )
                _responsForRecieveCash = new EmployeePost.Path<EmployeePost>(L_RESPONS_FOR_RECIEVE_CASH, this);
            return _responsForRecieveCash;
        }

    /**
     * @return Ответственный за получение денежных средств(уч. степень).
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getResponsForRecieveCashDegree()
     */
        public PersonAcademicDegree.Path<PersonAcademicDegree> responsForRecieveCashDegree()
        {
            if(_responsForRecieveCashDegree == null )
                _responsForRecieveCashDegree = new PersonAcademicDegree.Path<PersonAcademicDegree>(L_RESPONS_FOR_RECIEVE_CASH_DEGREE, this);
            return _responsForRecieveCashDegree;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> practiceOrgUnit()
        {
            if(_practiceOrgUnit == null )
                _practiceOrgUnit = new OrgUnit.Path<OrgUnit>(L_PRACTICE_ORG_UNIT, this);
            return _practiceOrgUnit;
        }

    /**
     * @return Руководитель практики.
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeHeader()
     */
        public EmployeePost.Path<EmployeePost> practiceHeader()
        {
            if(_practiceHeader == null )
                _practiceHeader = new EmployeePost.Path<EmployeePost>(L_PRACTICE_HEADER, this);
            return _practiceHeader;
        }

    /**
     * @return Руководитель практики(уч. степень).
     * @see ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract#getPracticeHeaderDegree()
     */
        public PersonAcademicDegree.Path<PersonAcademicDegree> practiceHeaderDegree()
        {
            if(_practiceHeaderDegree == null )
                _practiceHeaderDegree = new PersonAcademicDegree.Path<PersonAcademicDegree>(L_PRACTICE_HEADER_DEGREE, this);
            return _practiceHeaderDegree;
        }

        public Class getEntityClass()
        {
            return SendPracticeInnerStuListExtract.class;
        }

        public String getEntityName()
        {
            return "sendPracticeInnerStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
