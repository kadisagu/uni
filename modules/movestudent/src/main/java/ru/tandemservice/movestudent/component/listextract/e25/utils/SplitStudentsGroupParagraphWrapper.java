/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e25.utils;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 27.11.2012
 */
public class SplitStudentsGroupParagraphWrapper implements Comparable<SplitStudentsGroupParagraphWrapper>
{
    /**
     * course, group, compensationType, EducationLevels, formativeOrgUnit, territorialOrgUnit, developForm, специализации (профили и т.д.)
     */
    private final Course _course;
    private final Group _group;
    private final CompensationType _compensationType;
    private final EducationLevelsHighSchool _educationLevels;
    private final OrgUnit _formativeOrgUnit;
    private final OrgUnit _territorialOrgUnit;
    private final DevelopForm _developForm;
    private final DevelopCondition _developCondition;
    private final DevelopTech _developTech;
    private final DevelopPeriod _developPeriod;
    private final Qualifications _qualification;
    private final ListStudentExtract _firstExtract;

    public SplitStudentsGroupParagraphWrapper(Course course, Group group, CompensationType compensationType, EducationLevelsHighSchool educationLevels, OrgUnit formativeOrgUnit, OrgUnit territorialOrgUnit, DevelopForm developForm, DevelopCondition developCondition, DevelopTech developTech, DevelopPeriod developPeriod, Qualifications qualification, ListStudentExtract firstExtract)
    {
        _course = course;
        _group = group;
        _compensationType = compensationType;
        _educationLevels = educationLevels;
        _formativeOrgUnit = formativeOrgUnit;
        _territorialOrgUnit = territorialOrgUnit;
        _developForm = developForm;
        _developCondition = developCondition;
        _developTech = developTech;
        _developPeriod = developPeriod;
        _qualification = qualification;
        _firstExtract = firstExtract;
    }

    private List<SplitStudentsGroupParagraphPartWrapper> _paragraphPartWrapperList = new ArrayList<SplitStudentsGroupParagraphPartWrapper>();

    public Course getCourse()
    {
        return _course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public EducationLevelsHighSchool getEducationLevels()
    {
        return _educationLevels;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public Qualifications getQualification()
    {
        return _qualification;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    public List<SplitStudentsGroupParagraphPartWrapper> getParagraphPartWrapperList()
    {
        return _paragraphPartWrapperList;
    }

    public void setParagraphPartWrapperList(List<SplitStudentsGroupParagraphPartWrapper> paragraphPartWrapperList)
    {
        _paragraphPartWrapperList = paragraphPartWrapperList;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof SplitStudentsGroupParagraphWrapper))
            return false;

        SplitStudentsGroupParagraphWrapper that = (SplitStudentsGroupParagraphWrapper) o;

        return _course.equals(that.getCourse())
                && _group.equals(that.getGroup())
                && _compensationType.equals(that.getCompensationType())
                && _educationLevels.equals(that.getEducationLevels())
                && _formativeOrgUnit.equals(that.getFormativeOrgUnit())
                && _territorialOrgUnit.equals(that.getTerritorialOrgUnit())
                && _developForm.equals(that.getDevelopForm())
                && _developCondition.equals(that.getDevelopCondition())
                && _developTech.equals(that.getDevelopTech())
                && _developPeriod.equals(that.getDevelopPeriod());
    }

    @Override
    public int hashCode()
    {
        return _course.hashCode() & _group.hashCode() & _compensationType.hashCode() & _educationLevels.hashCode() & _formativeOrgUnit.hashCode() & _territorialOrgUnit.hashCode() & _developForm.hashCode() & _developCondition.hashCode() & _developTech.hashCode() & _developPeriod.hashCode();
    }

    @Override
    public int compareTo(SplitStudentsGroupParagraphWrapper o)
    {
        if (_course.getIntValue() > o.getCourse().getIntValue()) return 1;
        else if (_course.getIntValue() < o.getCourse().getIntValue()) return -1;

        int result = _group.getTitle().compareTo(o.getGroup().getTitle());

        if (result == 0)
            result = _compensationType.getCode().compareTo(o.getCompensationType().getCode());

        StructureEducationLevels thisLevels = _educationLevels.getEducationLevel().getLevelType();
        StructureEducationLevels thatLevels = o.getEducationLevels().getEducationLevel().getLevelType();

        if (thisLevels == null || thatLevels == null)
            return thisLevels == null ? (thatLevels == null ? 0 : 1) : -1;

        boolean isThisChild = thisLevels.isSpecialty() || thisLevels.isProfile();
        boolean isThatChild = thatLevels.isSpecialty() || thatLevels.isProfile();

        if (isThisChild != isThatChild)
            return isThisChild ? 1 : -1;

        result = _educationLevels.getTitle().compareTo(o.getEducationLevels().getTitle());

        String formativeOrgUnitStr1 = null != _formativeOrgUnit.getNominativeCaseTitle() ? _formativeOrgUnit.getNominativeCaseTitle() : _formativeOrgUnit.getFullTitle();
        String formativeOrgUnitStr2 = null != o.getFormativeOrgUnit().getNominativeCaseTitle() ? o.getFormativeOrgUnit().getNominativeCaseTitle() : o.getFormativeOrgUnit().getFullTitle();

        if (result == 0)
            result = formativeOrgUnitStr1.compareTo(formativeOrgUnitStr2);

        String territorialOrgUnitStr1 = null != _territorialOrgUnit.getNominativeCaseTitle() ? _territorialOrgUnit.getNominativeCaseTitle() : _territorialOrgUnit.getFullTitle();
        String territorialOrgUnitStr2 = null != o.getTerritorialOrgUnit().getNominativeCaseTitle() ? o.getTerritorialOrgUnit().getNominativeCaseTitle() : o.getTerritorialOrgUnit().getFullTitle();

        if (result == 0)
            result = territorialOrgUnitStr1.compareTo(territorialOrgUnitStr2);

        if (result == 0)
            result = _developForm.getCode().compareTo(o.getDevelopForm().getCode());

        if (result == 0)
            result = _developCondition.getCode().compareTo(o.getDevelopCondition().getCode());

        if (result == 0)
            result = _developTech.getCode().compareTo(o.getDevelopTech().getCode());

        if (result == 0)
            result = _developPeriod.getCode().compareTo(o.getDevelopPeriod().getCode());

        return result;
    }
}