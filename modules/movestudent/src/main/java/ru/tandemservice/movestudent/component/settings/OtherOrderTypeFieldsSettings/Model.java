/* $Id$ */
package ru.tandemservice.movestudent.component.settings.OtherOrderTypeFieldsSettings;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;

/**
 * @author Dmitry Seleznev
 * @since 05.04.2013
 */
public class Model
{
    public static final String DISABLE_REVERT_PROPERTY = "disableRevert";

    private DynamicListDataSource<StudentExtractType> _dataSource;

    public DynamicListDataSource<StudentExtractType> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<StudentExtractType> dataSource)
    {
        this._dataSource = dataSource;
    }
}