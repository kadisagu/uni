package ru.tandemservice.movestudent.entity.custom.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.custom.ModifyListStuExtract;
import ru.tandemservice.movestudent.entity.custom.OrderActionType;
import ru.tandemservice.unimove.entity.ICustomOrderExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Универсальная выписка из списочного приказа по студенту
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ModifyListStuExtractGen extends ListStudentExtract
 implements ICustomOrderExtract{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.custom.ModifyListStuExtract";
    public static final String ENTITY_NAME = "modifyListStuExtract";
    public static final int VERSION_HASH = -1494524040;
    private static IEntityMeta ENTITY_META;

    public static final String L_MODIFIABLE_EXTRACT = "modifiableExtract";
    public static final String P_ORDER_ACTION_TYPE = "orderActionType";
    public static final String L_USER_PRINT_FORM = "userPrintForm";

    private AbstractStudentExtract _modifiableExtract;     // Выписка для изменения, дополнения или отмены
    private OrderActionType _orderActionType;     // Выполняемое действие
    private DatabaseFile _userPrintForm;     // Печатная форма, загруженная пользователем

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка для изменения, дополнения или отмены. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public AbstractStudentExtract getModifiableExtract()
    {
        return _modifiableExtract;
    }

    /**
     * @param modifiableExtract Выписка для изменения, дополнения или отмены. Свойство не может быть null и должно быть уникальным.
     */
    public void setModifiableExtract(AbstractStudentExtract modifiableExtract)
    {
        dirty(_modifiableExtract, modifiableExtract);
        _modifiableExtract = modifiableExtract;
    }

    /**
     * @return Выполняемое действие. Свойство не может быть null.
     */
    @NotNull
    public OrderActionType getOrderActionType()
    {
        return _orderActionType;
    }

    /**
     * @param orderActionType Выполняемое действие. Свойство не может быть null.
     */
    public void setOrderActionType(OrderActionType orderActionType)
    {
        dirty(_orderActionType, orderActionType);
        _orderActionType = orderActionType;
    }

    /**
     * @return Печатная форма, загруженная пользователем.
     */
    public DatabaseFile getUserPrintForm()
    {
        return _userPrintForm;
    }

    /**
     * @param userPrintForm Печатная форма, загруженная пользователем.
     */
    public void setUserPrintForm(DatabaseFile userPrintForm)
    {
        dirty(_userPrintForm, userPrintForm);
        _userPrintForm = userPrintForm;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ModifyListStuExtractGen)
        {
            setModifiableExtract(((ModifyListStuExtract)another).getModifiableExtract());
            setOrderActionType(((ModifyListStuExtract)another).getOrderActionType());
            setUserPrintForm(((ModifyListStuExtract)another).getUserPrintForm());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ModifyListStuExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ModifyListStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new ModifyListStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "modifiableExtract":
                    return obj.getModifiableExtract();
                case "orderActionType":
                    return obj.getOrderActionType();
                case "userPrintForm":
                    return obj.getUserPrintForm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "modifiableExtract":
                    obj.setModifiableExtract((AbstractStudentExtract) value);
                    return;
                case "orderActionType":
                    obj.setOrderActionType((OrderActionType) value);
                    return;
                case "userPrintForm":
                    obj.setUserPrintForm((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "modifiableExtract":
                        return true;
                case "orderActionType":
                        return true;
                case "userPrintForm":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "modifiableExtract":
                    return true;
                case "orderActionType":
                    return true;
                case "userPrintForm":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "modifiableExtract":
                    return AbstractStudentExtract.class;
                case "orderActionType":
                    return OrderActionType.class;
                case "userPrintForm":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ModifyListStuExtract> _dslPath = new Path<ModifyListStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ModifyListStuExtract");
    }
            

    /**
     * @return Выписка для изменения, дополнения или отмены. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.custom.ModifyListStuExtract#getModifiableExtract()
     */
    public static AbstractStudentExtract.Path<AbstractStudentExtract> modifiableExtract()
    {
        return _dslPath.modifiableExtract();
    }

    /**
     * @return Выполняемое действие. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ModifyListStuExtract#getOrderActionType()
     */
    public static PropertyPath<OrderActionType> orderActionType()
    {
        return _dslPath.orderActionType();
    }

    /**
     * @return Печатная форма, загруженная пользователем.
     * @see ru.tandemservice.movestudent.entity.custom.ModifyListStuExtract#getUserPrintForm()
     */
    public static DatabaseFile.Path<DatabaseFile> userPrintForm()
    {
        return _dslPath.userPrintForm();
    }

    public static class Path<E extends ModifyListStuExtract> extends ListStudentExtract.Path<E>
    {
        private AbstractStudentExtract.Path<AbstractStudentExtract> _modifiableExtract;
        private PropertyPath<OrderActionType> _orderActionType;
        private DatabaseFile.Path<DatabaseFile> _userPrintForm;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка для изменения, дополнения или отмены. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.custom.ModifyListStuExtract#getModifiableExtract()
     */
        public AbstractStudentExtract.Path<AbstractStudentExtract> modifiableExtract()
        {
            if(_modifiableExtract == null )
                _modifiableExtract = new AbstractStudentExtract.Path<AbstractStudentExtract>(L_MODIFIABLE_EXTRACT, this);
            return _modifiableExtract;
        }

    /**
     * @return Выполняемое действие. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.ModifyListStuExtract#getOrderActionType()
     */
        public PropertyPath<OrderActionType> orderActionType()
        {
            if(_orderActionType == null )
                _orderActionType = new PropertyPath<OrderActionType>(ModifyListStuExtractGen.P_ORDER_ACTION_TYPE, this);
            return _orderActionType;
        }

    /**
     * @return Печатная форма, загруженная пользователем.
     * @see ru.tandemservice.movestudent.entity.custom.ModifyListStuExtract#getUserPrintForm()
     */
        public DatabaseFile.Path<DatabaseFile> userPrintForm()
        {
            if(_userPrintForm == null )
                _userPrintForm = new DatabaseFile.Path<DatabaseFile>(L_USER_PRINT_FORM, this);
            return _userPrintForm;
        }

        public Class getEntityClass()
        {
            return ModifyListStuExtract.class;
        }

        public String getEntityName()
        {
            return "modifyListStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
