/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e36.ParagraphAddEdit;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.GrantRiseStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 22.11.2012
 */
public class Model extends AbstractListParagraphAddEditModel<GrantRiseStuListExtract> implements IGroupModel
{
    private Course _course;
    private Group _group;
    private CompensationType _compensationType;
    private IdentifiableWrapper _month;
    private Integer _year;

    private ISelectModel _groupListModel;
    private List<CompensationType> _compensationTypeList;
    private List<IdentifiableWrapper> _monthList;

    //Calculate method
    public String getGrantRiseAmountId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "grantRiseAmountId_" + dataSource.getCurrentEntity().getId();
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public IdentifiableWrapper getMonth()
    {
        return _month;
    }

    public void setMonth(IdentifiableWrapper month)
    {
        _month = month;
    }

    public Integer getYear()
    {
        return _year;
    }

    public void setYear(Integer year)
    {
        _year = year;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public List<IdentifiableWrapper> getMonthList()
    {
        return _monthList;
    }

    public void setMonthList(List<IdentifiableWrapper> monthList)
    {
        _monthList = monthList;
    }
}