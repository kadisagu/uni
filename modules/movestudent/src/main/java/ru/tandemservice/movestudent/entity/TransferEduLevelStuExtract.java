package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.TransferEduLevelStuExtractGen;

import java.util.Date;

/**
 * О переводе со специальности на специальность (с направления на направление)
 */
public class TransferEduLevelStuExtract extends TransferEduLevelStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}