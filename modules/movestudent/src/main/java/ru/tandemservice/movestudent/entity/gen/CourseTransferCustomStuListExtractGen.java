package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CourseTransferCustomStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О переводе с курса на курс(вариант 2)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CourseTransferCustomStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.CourseTransferCustomStuListExtract";
    public static final String ENTITY_NAME = "courseTransferCustomStuListExtract";
    public static final int VERSION_HASH = -965396323;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE_OLD = "courseOld";
    public static final String L_COURSE_NEW = "courseNew";
    public static final String P_DEADLINE_DATE = "deadlineDate";
    public static final String P_CONDITIONAL_TRANSFER = "conditionalTransfer";

    private Course _courseOld;     // Курс (старый)
    private Course _courseNew;     // Курс (новый)
    private Date _deadlineDate;     // Дата ликвидации задолженности
    private boolean _conditionalTransfer;     // Перевести условно

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс (старый). Свойство не может быть null.
     */
    @NotNull
    public Course getCourseOld()
    {
        return _courseOld;
    }

    /**
     * @param courseOld Курс (старый). Свойство не может быть null.
     */
    public void setCourseOld(Course courseOld)
    {
        dirty(_courseOld, courseOld);
        _courseOld = courseOld;
    }

    /**
     * @return Курс (новый). Свойство не может быть null.
     */
    @NotNull
    public Course getCourseNew()
    {
        return _courseNew;
    }

    /**
     * @param courseNew Курс (новый). Свойство не может быть null.
     */
    public void setCourseNew(Course courseNew)
    {
        dirty(_courseNew, courseNew);
        _courseNew = courseNew;
    }

    /**
     * @return Дата ликвидации задолженности.
     */
    public Date getDeadlineDate()
    {
        return _deadlineDate;
    }

    /**
     * @param deadlineDate Дата ликвидации задолженности.
     */
    public void setDeadlineDate(Date deadlineDate)
    {
        dirty(_deadlineDate, deadlineDate);
        _deadlineDate = deadlineDate;
    }

    /**
     * @return Перевести условно. Свойство не может быть null.
     */
    @NotNull
    public boolean isConditionalTransfer()
    {
        return _conditionalTransfer;
    }

    /**
     * @param conditionalTransfer Перевести условно. Свойство не может быть null.
     */
    public void setConditionalTransfer(boolean conditionalTransfer)
    {
        dirty(_conditionalTransfer, conditionalTransfer);
        _conditionalTransfer = conditionalTransfer;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof CourseTransferCustomStuListExtractGen)
        {
            setCourseOld(((CourseTransferCustomStuListExtract)another).getCourseOld());
            setCourseNew(((CourseTransferCustomStuListExtract)another).getCourseNew());
            setDeadlineDate(((CourseTransferCustomStuListExtract)another).getDeadlineDate());
            setConditionalTransfer(((CourseTransferCustomStuListExtract)another).isConditionalTransfer());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CourseTransferCustomStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CourseTransferCustomStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new CourseTransferCustomStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "courseOld":
                    return obj.getCourseOld();
                case "courseNew":
                    return obj.getCourseNew();
                case "deadlineDate":
                    return obj.getDeadlineDate();
                case "conditionalTransfer":
                    return obj.isConditionalTransfer();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "courseOld":
                    obj.setCourseOld((Course) value);
                    return;
                case "courseNew":
                    obj.setCourseNew((Course) value);
                    return;
                case "deadlineDate":
                    obj.setDeadlineDate((Date) value);
                    return;
                case "conditionalTransfer":
                    obj.setConditionalTransfer((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "courseOld":
                        return true;
                case "courseNew":
                        return true;
                case "deadlineDate":
                        return true;
                case "conditionalTransfer":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "courseOld":
                    return true;
                case "courseNew":
                    return true;
                case "deadlineDate":
                    return true;
                case "conditionalTransfer":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "courseOld":
                    return Course.class;
                case "courseNew":
                    return Course.class;
                case "deadlineDate":
                    return Date.class;
                case "conditionalTransfer":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CourseTransferCustomStuListExtract> _dslPath = new Path<CourseTransferCustomStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CourseTransferCustomStuListExtract");
    }
            

    /**
     * @return Курс (старый). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseTransferCustomStuListExtract#getCourseOld()
     */
    public static Course.Path<Course> courseOld()
    {
        return _dslPath.courseOld();
    }

    /**
     * @return Курс (новый). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseTransferCustomStuListExtract#getCourseNew()
     */
    public static Course.Path<Course> courseNew()
    {
        return _dslPath.courseNew();
    }

    /**
     * @return Дата ликвидации задолженности.
     * @see ru.tandemservice.movestudent.entity.CourseTransferCustomStuListExtract#getDeadlineDate()
     */
    public static PropertyPath<Date> deadlineDate()
    {
        return _dslPath.deadlineDate();
    }

    /**
     * @return Перевести условно. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseTransferCustomStuListExtract#isConditionalTransfer()
     */
    public static PropertyPath<Boolean> conditionalTransfer()
    {
        return _dslPath.conditionalTransfer();
    }

    public static class Path<E extends CourseTransferCustomStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _courseOld;
        private Course.Path<Course> _courseNew;
        private PropertyPath<Date> _deadlineDate;
        private PropertyPath<Boolean> _conditionalTransfer;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс (старый). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseTransferCustomStuListExtract#getCourseOld()
     */
        public Course.Path<Course> courseOld()
        {
            if(_courseOld == null )
                _courseOld = new Course.Path<Course>(L_COURSE_OLD, this);
            return _courseOld;
        }

    /**
     * @return Курс (новый). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseTransferCustomStuListExtract#getCourseNew()
     */
        public Course.Path<Course> courseNew()
        {
            if(_courseNew == null )
                _courseNew = new Course.Path<Course>(L_COURSE_NEW, this);
            return _courseNew;
        }

    /**
     * @return Дата ликвидации задолженности.
     * @see ru.tandemservice.movestudent.entity.CourseTransferCustomStuListExtract#getDeadlineDate()
     */
        public PropertyPath<Date> deadlineDate()
        {
            if(_deadlineDate == null )
                _deadlineDate = new PropertyPath<Date>(CourseTransferCustomStuListExtractGen.P_DEADLINE_DATE, this);
            return _deadlineDate;
        }

    /**
     * @return Перевести условно. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseTransferCustomStuListExtract#isConditionalTransfer()
     */
        public PropertyPath<Boolean> conditionalTransfer()
        {
            if(_conditionalTransfer == null )
                _conditionalTransfer = new PropertyPath<Boolean>(CourseTransferCustomStuListExtractGen.P_CONDITIONAL_TRANSFER, this);
            return _conditionalTransfer;
        }

        public Class getEntityClass()
        {
            return CourseTransferCustomStuListExtract.class;
        }

        public String getEntityName()
        {
            return "courseTransferCustomStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
