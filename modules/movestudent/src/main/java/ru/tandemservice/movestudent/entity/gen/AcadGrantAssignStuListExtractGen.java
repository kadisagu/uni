package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О назначении академической стипендии»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AcadGrantAssignStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract";
    public static final String ENTITY_NAME = "acadGrantAssignStuListExtract";
    public static final int VERSION_HASH = -944813095;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_GRANT_SIZE_RUBLE = "grantSizeRuble";
    public static final String P_GROUP_MANAGER_BONUS_SIZE_RUBLE = "groupManagerBonusSizeRuble";
    public static final String L_SESSION_TYPE = "sessionType";
    public static final String P_PREV_BEGIN_DATE = "prevBeginDate";
    public static final String P_PREV_END_DATE = "prevEndDate";

    private Course _course;     // Курс
    private Group _group;     // Группа
    private CompensationType _compensationType;     // Вид возмещения затрат
    private Date _beginDate;     // Дата начала выплаты стипендии
    private Date _endDate;     // Дата окончания выплаты стипендии
    private Double _grantSizeRuble;     // Размер стипендии, руб
    private Double _groupManagerBonusSizeRuble;     // Размер выплаты старосте, руб
    private YearDistributionPart _sessionType;     // Вид сессии
    private Date _prevBeginDate;     // Дата начала выплаты академической стипендии по предыдущему приказу
    private Date _prevEndDate;     // Дата окончания выплаты академической стипендии по предыдущему приказу

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Дата начала выплаты стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала выплаты стипендии. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания выплаты стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания выплаты стипендии. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Размер стипендии, руб.
     */
    public Double getGrantSizeRuble()
    {
        return _grantSizeRuble;
    }

    /**
     * @param grantSizeRuble Размер стипендии, руб.
     */
    public void setGrantSizeRuble(Double grantSizeRuble)
    {
        dirty(_grantSizeRuble, grantSizeRuble);
        _grantSizeRuble = grantSizeRuble;
    }

    /**
     * @return Размер выплаты старосте, руб.
     */
    public Double getGroupManagerBonusSizeRuble()
    {
        return _groupManagerBonusSizeRuble;
    }

    /**
     * @param groupManagerBonusSizeRuble Размер выплаты старосте, руб.
     */
    public void setGroupManagerBonusSizeRuble(Double groupManagerBonusSizeRuble)
    {
        dirty(_groupManagerBonusSizeRuble, groupManagerBonusSizeRuble);
        _groupManagerBonusSizeRuble = groupManagerBonusSizeRuble;
    }

    /**
     * @return Вид сессии. Свойство не может быть null.
     */
    @NotNull
    public YearDistributionPart getSessionType()
    {
        return _sessionType;
    }

    /**
     * @param sessionType Вид сессии. Свойство не может быть null.
     */
    public void setSessionType(YearDistributionPart sessionType)
    {
        dirty(_sessionType, sessionType);
        _sessionType = sessionType;
    }

    /**
     * @return Дата начала выплаты академической стипендии по предыдущему приказу.
     */
    public Date getPrevBeginDate()
    {
        return _prevBeginDate;
    }

    /**
     * @param prevBeginDate Дата начала выплаты академической стипендии по предыдущему приказу.
     */
    public void setPrevBeginDate(Date prevBeginDate)
    {
        dirty(_prevBeginDate, prevBeginDate);
        _prevBeginDate = prevBeginDate;
    }

    /**
     * @return Дата окончания выплаты академической стипендии по предыдущему приказу.
     */
    public Date getPrevEndDate()
    {
        return _prevEndDate;
    }

    /**
     * @param prevEndDate Дата окончания выплаты академической стипендии по предыдущему приказу.
     */
    public void setPrevEndDate(Date prevEndDate)
    {
        dirty(_prevEndDate, prevEndDate);
        _prevEndDate = prevEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof AcadGrantAssignStuListExtractGen)
        {
            setCourse(((AcadGrantAssignStuListExtract)another).getCourse());
            setGroup(((AcadGrantAssignStuListExtract)another).getGroup());
            setCompensationType(((AcadGrantAssignStuListExtract)another).getCompensationType());
            setBeginDate(((AcadGrantAssignStuListExtract)another).getBeginDate());
            setEndDate(((AcadGrantAssignStuListExtract)another).getEndDate());
            setGrantSizeRuble(((AcadGrantAssignStuListExtract)another).getGrantSizeRuble());
            setGroupManagerBonusSizeRuble(((AcadGrantAssignStuListExtract)another).getGroupManagerBonusSizeRuble());
            setSessionType(((AcadGrantAssignStuListExtract)another).getSessionType());
            setPrevBeginDate(((AcadGrantAssignStuListExtract)another).getPrevBeginDate());
            setPrevEndDate(((AcadGrantAssignStuListExtract)another).getPrevEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AcadGrantAssignStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AcadGrantAssignStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new AcadGrantAssignStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "compensationType":
                    return obj.getCompensationType();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "grantSizeRuble":
                    return obj.getGrantSizeRuble();
                case "groupManagerBonusSizeRuble":
                    return obj.getGroupManagerBonusSizeRuble();
                case "sessionType":
                    return obj.getSessionType();
                case "prevBeginDate":
                    return obj.getPrevBeginDate();
                case "prevEndDate":
                    return obj.getPrevEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "grantSizeRuble":
                    obj.setGrantSizeRuble((Double) value);
                    return;
                case "groupManagerBonusSizeRuble":
                    obj.setGroupManagerBonusSizeRuble((Double) value);
                    return;
                case "sessionType":
                    obj.setSessionType((YearDistributionPart) value);
                    return;
                case "prevBeginDate":
                    obj.setPrevBeginDate((Date) value);
                    return;
                case "prevEndDate":
                    obj.setPrevEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "group":
                        return true;
                case "compensationType":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "grantSizeRuble":
                        return true;
                case "groupManagerBonusSizeRuble":
                        return true;
                case "sessionType":
                        return true;
                case "prevBeginDate":
                        return true;
                case "prevEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "group":
                    return true;
                case "compensationType":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "grantSizeRuble":
                    return true;
                case "groupManagerBonusSizeRuble":
                    return true;
                case "sessionType":
                    return true;
                case "prevBeginDate":
                    return true;
                case "prevEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "compensationType":
                    return CompensationType.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "grantSizeRuble":
                    return Double.class;
                case "groupManagerBonusSizeRuble":
                    return Double.class;
                case "sessionType":
                    return YearDistributionPart.class;
                case "prevBeginDate":
                    return Date.class;
                case "prevEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AcadGrantAssignStuListExtract> _dslPath = new Path<AcadGrantAssignStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AcadGrantAssignStuListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Дата начала выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Размер стипендии, руб.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getGrantSizeRuble()
     */
    public static PropertyPath<Double> grantSizeRuble()
    {
        return _dslPath.grantSizeRuble();
    }

    /**
     * @return Размер выплаты старосте, руб.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getGroupManagerBonusSizeRuble()
     */
    public static PropertyPath<Double> groupManagerBonusSizeRuble()
    {
        return _dslPath.groupManagerBonusSizeRuble();
    }

    /**
     * @return Вид сессии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getSessionType()
     */
    public static YearDistributionPart.Path<YearDistributionPart> sessionType()
    {
        return _dslPath.sessionType();
    }

    /**
     * @return Дата начала выплаты академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getPrevBeginDate()
     */
    public static PropertyPath<Date> prevBeginDate()
    {
        return _dslPath.prevBeginDate();
    }

    /**
     * @return Дата окончания выплаты академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getPrevEndDate()
     */
    public static PropertyPath<Date> prevEndDate()
    {
        return _dslPath.prevEndDate();
    }

    public static class Path<E extends AcadGrantAssignStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Double> _grantSizeRuble;
        private PropertyPath<Double> _groupManagerBonusSizeRuble;
        private YearDistributionPart.Path<YearDistributionPart> _sessionType;
        private PropertyPath<Date> _prevBeginDate;
        private PropertyPath<Date> _prevEndDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Дата начала выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(AcadGrantAssignStuListExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(AcadGrantAssignStuListExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Размер стипендии, руб.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getGrantSizeRuble()
     */
        public PropertyPath<Double> grantSizeRuble()
        {
            if(_grantSizeRuble == null )
                _grantSizeRuble = new PropertyPath<Double>(AcadGrantAssignStuListExtractGen.P_GRANT_SIZE_RUBLE, this);
            return _grantSizeRuble;
        }

    /**
     * @return Размер выплаты старосте, руб.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getGroupManagerBonusSizeRuble()
     */
        public PropertyPath<Double> groupManagerBonusSizeRuble()
        {
            if(_groupManagerBonusSizeRuble == null )
                _groupManagerBonusSizeRuble = new PropertyPath<Double>(AcadGrantAssignStuListExtractGen.P_GROUP_MANAGER_BONUS_SIZE_RUBLE, this);
            return _groupManagerBonusSizeRuble;
        }

    /**
     * @return Вид сессии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getSessionType()
     */
        public YearDistributionPart.Path<YearDistributionPart> sessionType()
        {
            if(_sessionType == null )
                _sessionType = new YearDistributionPart.Path<YearDistributionPart>(L_SESSION_TYPE, this);
            return _sessionType;
        }

    /**
     * @return Дата начала выплаты академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getPrevBeginDate()
     */
        public PropertyPath<Date> prevBeginDate()
        {
            if(_prevBeginDate == null )
                _prevBeginDate = new PropertyPath<Date>(AcadGrantAssignStuListExtractGen.P_PREV_BEGIN_DATE, this);
            return _prevBeginDate;
        }

    /**
     * @return Дата окончания выплаты академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract#getPrevEndDate()
     */
        public PropertyPath<Date> prevEndDate()
        {
            if(_prevEndDate == null )
                _prevEndDate = new PropertyPath<Date>(AcadGrantAssignStuListExtractGen.P_PREV_END_DATE, this);
            return _prevEndDate;
        }

        public Class getEntityClass()
        {
            return AcadGrantAssignStuListExtract.class;
        }

        public String getEntityName()
        {
            return "acadGrantAssignStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
