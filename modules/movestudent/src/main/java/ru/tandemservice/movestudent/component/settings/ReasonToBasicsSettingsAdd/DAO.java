/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.settings.ReasonToBasicsSettingsAdd;

import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.entity.StudentReasonToBasicRel;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import ru.tandemservice.uni.dao.UniDao;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 16.10.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings({"unchecked"})
    public void prepare(Model model)
    {
        model.setFirst(this.<StudentOrderReasons>getNotNull(model.getFirstId()));
        String hql = "from " + StudentOrderBasics.class.getName() + " b where b not in (select " + IEntityRelation.L_SECOND + " from " + StudentReasonToBasicRel.class.getName() + " where " + IEntityRelation.L_FIRST + "=:first) order by b.title";
        List list = getSession().createQuery(hql).setParameter("first", model.getFirst()).list();
        model.setSecondList(list);
    }

    @Override
    public void update(Model model)
    {
        Number number = (Number) getSession().createCriteria(StudentReasonToBasicRel.class)
                .add(Restrictions.eq(IEntityRelation.L_FIRST, model.getFirst()))
                .add(Restrictions.eq(IEntityRelation.L_SECOND, model.getSecond()))
                .setProjection(Projections.rowCount()).uniqueResult();
        int count = number == null ? 0 : number.intValue();

        if (count == 0)
        {
            List<StudentReasonToBasicRel> basicsList = new DQLSelectBuilder()
                    .fromEntity(StudentReasonToBasicRel.class, "rel").column("rel")
                    .where(eq(property(StudentReasonToBasicRel.first().id().fromAlias("rel")), value(model.getFirstId())))
                    .order(property(StudentReasonToBasicRel.priority().fromAlias("rel")))
                    .createStatement(getSession()).list();

            StudentReasonToBasicRel rel = new StudentReasonToBasicRel();
            rel.setFirst(model.getFirst());
            rel.setSecond(model.getSecond());
            basicsList.add(rel);

            try
            {
                int priority = 1;
                for (StudentReasonToBasicRel relItem : basicsList)
                {
                    if (null != relItem.getId() && relItem.getPriority() != priority)
                    {
                        relItem.setPriority(priority);
                        update(relItem);
                    } else if (null == relItem.getId())
                    {
                        relItem.setPriority(priority);
                        save(rel);
                    }
                    priority++;
                }

            } catch (Exception e)
            {
                throw new RuntimeException(e.getMessage(), e);
            }
        } else
            throw new ApplicationException("Такая связь уже существует.");
    }
}
