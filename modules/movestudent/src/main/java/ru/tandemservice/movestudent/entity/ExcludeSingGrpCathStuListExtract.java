package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.movestudent.entity.gen.ExcludeSingGrpCathStuListExtractGen;

import java.util.Date;

/**
 * Проект приказа «Об отчислении для одной группы (кафедральный)»
 */
public class ExcludeSingGrpCathStuListExtract extends ExcludeSingGrpCathStuListExtractGen implements IExcludeExtract
{
    @Override
    public Date getBeginDate()
    {
        return getExcludeDate();
    }
}