/* $Id$ */
package ru.tandemservice.movestudent.component.modularextract;

import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;

/**
 * @author Nikolay Fedorovskih
 * @since 05.10.2013
 */
public interface ICommonExtractUtil
{
    CommonExtractModel createEduModel(boolean isAddForm, CommonStuExtract extract);

    CommonExtractModel createEduModel(boolean isAddForm, CommonStuExtract extract, boolean hideInactiveProgramSubject);

    void initEduModel(CommonExtractModel model, CommonStuExtract extract);

    void update(CommonExtractModel model, CommonStuExtract extract);

    void handleGroupChange(CommonExtractModel model);

    void doCommitWithoutChangeStatus(CommonStuExtract extract);

    void doCommit(CommonStuExtract extract, IUniBaseDao coreDao);

    void doRollbackWithoutChangeStatus(CommonStuExtract extract);

    void doRollback(CommonStuExtract extract);

    Qualifications getQualification(EducationLevels eduLevel);
}