package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.TransferProfileExtStuExtractGen;

import java.util.Date;

/**
 * О переводе со специализации на специализации (с профиля на профиль)
 */
public class TransferProfileExtStuExtract extends TransferProfileExtStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}