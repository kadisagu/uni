/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e48;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.CourseTransferCustomStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author ListExtractComponentGenerator
 * @since 03.02.2017
 */
public class CourseTransferCustomStuListExtractPrint implements IPrintFormCreator<CourseTransferCustomStuListExtract>, IListParagraphPrintFormCreator<CourseTransferCustomStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, CourseTransferCustomStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, CourseTransferCustomStuListExtract firstExtract)
    {
        RtfInjectModifier modifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
        modifier.put("compensationTypeStr_P", firstExtract.getEntity().getCompensationType().isBudget()?
                "местах, финансируемых за счет бюджетных ассигнований федерального бюджета":
                "местах с оплатой стоимости обучения физическими лицами");

        modifier.put("conditionalTransferAlt", firstExtract.isConditionalTransfer()?
                "Установить срок ликвидации академических задолженностей до ":
                 "Студент полностью выполнил учебный план");
        modifier.put("deadlineDateStr", firstExtract.isConditionalTransfer()? DateFormatter.DEFAULT_DATE_FORMATTER.format(firstExtract.getDeadlineDate()): "");
        return modifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, CourseTransferCustomStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, CourseTransferCustomStuListExtract firstExtract)
    {
    }
}