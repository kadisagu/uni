\keep\keepn\qj\b {extractNumber}.\par\b0\par
\fi709\qj {reason} \par\fi0\par
приказываю:\par
\fi709\qj 
1. {student_A} {fio} {birthDate} дата рождения, {course} курса {orgUnit_G} {developForm_GF} формы обучения {compensationTypeStr_G} с {entryIntoForceDate} не допускать к учебному процессу на {course} курсе {orgUnit_G} {developForm_GF} формы освоения до ликвидации {textCause}.\par\par
\fi709\qj Основание: {listBasics}\par\fi0