package ru.tandemservice.movestudent.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.dialect.MSSqlDBDialect;
import org.tandemframework.dbsupport.ddl.dialect.PostgresDBDialect;
import org.tandemframework.dbsupport.ddl.dialect.OracleDBDialect;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import ru.tandemservice.uni.entity.catalog.codes.StudentExtractGroupCodes;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudent_2x7x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность excludeSingGrpStuListExtract

		// создано свойство prevGraduateOrderDate
		{
			// создать колонку
            if (!tool.columnExists("excludesinggrpstulistextract_t", "prevgraduateorderdate_p"))
            {
                tool.createColumn("excludesinggrpstulistextract_t", new DBColumn("prevgraduateorderdate_p", DBType.DATE));
            }
		}

		// создано свойство prevGraduateOrderNumber
		{
			// создать колонку
            if (!tool.columnExists("excludesinggrpstulistextract_t", "prevgraduateordernumber_p"))
            {
                tool.createColumn("excludesinggrpstulistextract_t", new DBColumn("prevgraduateordernumber_p", DBType.createVarchar(255)));
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность excludeSingGrpSuccStuListExtract

		// создано свойство prevGraduateSuccOrderDate
		{
			// создать колонку
            if (!tool.columnExists("xcldsnggrpsccstlstextrct_t", "prevgraduatesuccorderdate_p"))
            {
                tool.createColumn("xcldsnggrpsccstlstextrct_t", new DBColumn("prevgraduatesuccorderdate_p", DBType.DATE));
            }
		}

		// создано свойство prevGraduateSuccOrderNumber
		{
			// создать колонку
            if (!tool.columnExists("xcldsnggrpsccstlstextrct_t", "prevgraduatesuccordernumber_p"))
            {
                tool.createColumn("xcldsnggrpsccstlstextrct_t", new DBColumn("prevgraduatesuccordernumber_p", DBType.createVarchar(255)));
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность graduateStuListExtract

		// создано свойство prevGraduateOrderDate
		{
			// создать колонку
            if (!tool.columnExists("graduatestulistextract_t", "prevgraduateorderdate_p"))
            {
                tool.createColumn("graduatestulistextract_t", new DBColumn("prevgraduateorderdate_p", DBType.DATE));
            }
		}

		// создано свойство prevGraduateOrderNumber
		{
			// создать колонку
            if (!tool.columnExists("graduatestulistextract_t", "prevgraduateordernumber_p"))
            {
                tool.createColumn("graduatestulistextract_t", new DBColumn("prevgraduateordernumber_p", DBType.createVarchar(255)));
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность graduateSuccessStuListExtract

		// создано свойство prevGraduateSuccOrderDate
		{
			// создать колонку
            if (!tool.columnExists("grdtsccssstlstextrct_t", "prevgraduatesuccorderdate_p"))
            {
                tool.createColumn("grdtsccssstlstextrct_t", new DBColumn("prevgraduatesuccorderdate_p", DBType.DATE));
            }
		}

		// создано свойство prevGraduateSuccOrderNumber
		{
			// создать колонку
            if (!tool.columnExists("grdtsccssstlstextrct_t", "prevgraduatesuccordernumber_p"))
            {
                tool.createColumn("grdtsccssstlstextrct_t", new DBColumn("prevgraduatesuccordernumber_p", DBType.createVarchar(255)));
            }
		}

        //заполняем данные о предыдущих приказах в выписках приказов "О выпуске студентов" и "О выпуске студентов с отличием"
        fillPrevOrderData(tool);
        //заполянем данные о выпуске студентов и о выпуске студенов с отличием в "Данных о последних приказах"
        fillLastOrderData(tool);
    }


    private void fillPrevOrderData(DBTool tool) throws Exception
    {
        //находим все выписки, относящиеся к приказам "выпуске" и "выпуске с отличием"
        PreparedStatement prstm = tool.prepareStatement("SELECT extr.id, extr.entity_id, extrGroupType.code_p, ord.commitdate_p \n" +
                                                                "FROM abstractstudentextract_t extr\n" +
                                                                "inner join studentextracttypetogroup_t extrType on extrType.type_id=extr.type_id\n" +
                                                                "inner join studentextractgroup_t extrGroupType on extrGroupType.id=extrType.group_id\n" +
                                                                "inner join abstractstudentparagraph_t par on par.id = extr.paragraph_id\n" +
                                                                "inner join abstractstudentorder_t ord on ord.id = par.order_id\n" +
                                                                "WHERE\n" +
                                                                "(extrGroupType.code_p like ? or extrGroupType.code_p like ?)\n" +
                                                                "and extr.committed_p=?");

        prstm.setString(1, StudentExtractGroupCodes.GRADUATION); //код группы приказов  "О выпуске"
        prstm.setString(2, StudentExtractGroupCodes.GRADUATION_HONOURS); //код группы приказов  "О выпуске (диплом с отличием)"
        prstm.setBoolean(3, true);

        ResultSet rs = prstm.executeQuery();
        while (rs.next())
        {
            Long extractId = rs.getLong(1);
            Long studentId = rs.getLong(2);
            String extractTypeCode = rs.getString(3);
            Date commitDate = rs.getDate(4);
            //заполняем для каждой выписки даннеы о последних приказах
            findPreviousExtract(tool, extractId, studentId, extractTypeCode, commitDate);
        }
    }

    private void findPreviousExtract(DBTool tool, Long extractId, Long studentId, String extractTypeCode, Date commitDate) throws Exception
    {


        //берем предыдущую выписку приказа данного типа, для данного студента
        String query = "";
        if (tool.getDialect() instanceof MSSqlDBDialect)
        {
            query = "SELECT TOP 1 ord.commitdate_p, ord.number_p\n" +
                    "FROM abstractstudentextract_t extr\n" +
                    "left join studentextracttypetogroup_t extrType on extrType.type_id=extr.type_id\n" +
                    "left join studentextractgroup_t extrGroupType on extrGroupType.id=extrType.group_id\n" +
                    "inner join abstractstudentparagraph_t par on par.id = extr.paragraph_id\n" +
                    "inner join abstractstudentorder_t ord on ord.id = par.order_id\n" +
                    "WHERE\n" +
                    "extrGroupType.code_p = ?\n" +
                    "and ord.commitdate_p < ?\n" +
                    "and extr.committed_p=?\n" +
                    "and extr.entity_id=?\n" +
                    "ORDER BY  ord.commitdate_p desc";
        }
        else if (tool.getDialect() instanceof PostgresDBDialect)
        {
            query = "SELECT ord.commitdate_p, ord.number_p\n" +
                    "FROM abstractstudentextract_t extr\n" +
                    "left join studentextracttypetogroup_t extrType on extrType.type_id=extr.type_id\n" +
                    "left join studentextractgroup_t extrGroupType on extrGroupType.id=extrType.group_id\n" +
                    "inner join abstractstudentparagraph_t par on par.id = extr.paragraph_id\n" +
                    "inner join abstractstudentorder_t ord on ord.id = par.order_id\n" +
                    "WHERE\n" +
                    "extrGroupType.code_p = ?\n" +
                    "and ord.commitdate_p < ?\n" +
                    "and extr.committed_p=?\n" +
                    "and extr.entity_id=?\n" +
                    "ORDER BY  ord.commitdate_p desc\n" +
                    "LIMIT 1";
        }
        else if (tool.getDialect() instanceof OracleDBDialect)
        {
            query = "SELECT ord.commitdate_p, ord.number_p\n" +
                    "FROM abstractstudentextract_t extr\n" +
                    "left join studentextracttypetogroup_t extrType on extrType.type_id=extr.type_id\n" +
                    "left join studentextractgroup_t extrGroupType on extrGroupType.id=extrType.group_id\n" +
                    "inner join abstractstudentparagraph_t par on par.id = extr.paragraph_id\n" +
                    "inner join abstractstudentorder_t ord on ord.id = par.order_id\n" +
                    "WHERE\n" +
                    "extrGroupType.code_p = ?\n" +
                    "and ord.commitdate_p < ?\n" +
                    "and extr.committed_p=?\n" +
                    "and extr.entity_id=?\n" +
                    "and rownum = 1\n " +
                    "ORDER BY  ord.commitdate_p desc";
        }
        PreparedStatement prstm = tool.prepareStatement(query);
        if (extractTypeCode.equals(StudentExtractGroupCodes.GRADUATION))
        {
            prstm.setString(1, StudentExtractGroupCodes.GRADUATION);
        }
        else if (extractTypeCode.equals(StudentExtractGroupCodes.GRADUATION_HONOURS))
        {
            prstm.setString(1, StudentExtractGroupCodes.GRADUATION_HONOURS);
        }
        prstm.setDate(2, commitDate);
        prstm.setBoolean(3, true);
        prstm.setLong(4, studentId);

        ResultSet rs = prstm.executeQuery();

        //Заполняем данне о предыдущем приказе для выписок "О выпуске" и "О выпуске с отличием"
        Date prevOrderDate = null;
        String prevOrderNumber = null;
        while (rs.next())
        {

            prevOrderDate =rs.getDate(1);
            prevOrderNumber = rs.getString(2);
        }
        //апдэйтим данные о предыдущем приказе в выписке

        if (extractTypeCode.equals(StudentExtractGroupCodes.GRADUATION))
        {
            PreparedStatement updateEx = tool.prepareStatement("UPDATE excludesinggrpstulistextract_t " +
                                                                     "SET " +
                                                                     "prevgraduateorderdate_p=?, prevgraduateordernumber_p=? " +
                                                                     "WHERE id=?");
            updateEx.setDate(1, prevOrderDate);
            updateEx.setString(2, prevOrderNumber);
            updateEx.setLong(3, extractId);
            updateEx.execute();

            PreparedStatement updateGr = tool.prepareStatement("UPDATE graduatestulistextract_t " +
                                                                       "SET " +
                                                                       "prevgraduateorderdate_p=?, prevgraduateordernumber_p=? " +
                                                                       "WHERE id=?");
            updateGr.setDate(1, prevOrderDate);
            updateGr.setString(2, prevOrderNumber);
            updateGr.setLong(3, extractId);
            updateGr.execute();
        }
        else if (extractTypeCode.equals(StudentExtractGroupCodes.GRADUATION_HONOURS))
        {
            PreparedStatement updateEx = tool.prepareStatement("UPDATE xcldsnggrpsccstlstextrct_t " +
                                                                     "SET " +
                                                                     "prevgraduatesuccorderdate_p=?, prevgraduatesuccordernumber_p=? " +
                                                                     "WHERE id=?");
            updateEx.setDate(1, prevOrderDate);
            updateEx.setString(2, prevOrderNumber);
            updateEx.setLong(3, extractId);
            updateEx.execute();

            PreparedStatement updateGr = tool.prepareStatement("UPDATE grdtsccssstlstextrct_t " +
                                                                       "SET " +
                                                                       "prevgraduatesuccorderdate_p=?, prevgraduatesuccordernumber_p=? " +
                                                                       "WHERE id=?");
            updateGr.setDate(1, prevOrderDate);
            updateGr.setString(2, prevOrderNumber);
            updateGr.setLong(3, extractId);
            updateGr.execute();
        }

    }


    private void fillLastOrderData(DBTool tool) throws Exception
    {
        //Находим студентов по которым проводились приказы об отчислении (или о выпуске, но из-за DEV-6395, приказы были учтены тольок как приказы "об отчислении")
        PreparedStatement prstm = tool.prepareStatement(
                "SELECT student_id " +
                        "FROM orderdata_t WHERE excludeordernumber_p  is not null"
        );

        ResultSet rs = prstm.executeQuery();

        while (rs.next())
        {
            //находим последнюю выписку каждого типа ("о выпуске (диплом)", "о выпуске (диплом с отличием)") для каждого студента
            //и апдэйтим получеными данными "Данные о последних приказах"
            findLastExtract(tool, rs.getLong(1));
        }

    }


    private void findLastExtract(DBTool tool, Long studentId) throws Exception
    {

        String graduateOrderNumber = null;
        String graduateSuccessOrderNumber = null;
        Date graduateOrderDate = null;
        Date graduateSuccessOrderDate = null;

        String queryGrad = "";
        if (tool.getDialect() instanceof MSSqlDBDialect)
        {
            queryGrad = "SELECT TOP 1 ord.commitdate_p, ord.number_p\n" +
                    "FROM abstractstudentextract_t extr\n" +
                    "left join studentextracttypetogroup_t extrType on extrType.type_id=extr.type_id\n" +
                    "left join studentextractgroup_t extrGroupType on extrGroupType.id=extrType.group_id\n" +
                    "inner join abstractstudentparagraph_t par on par.id = extr.paragraph_id\n" +
                    "inner join abstractstudentorder_t ord on ord.id = par.order_id\n" +
                    "WHERE\n" +
                    "extrGroupType.code_p = ?\n" +
                    "and extr.committed_p=?\n" +
                    "and extr.entity_id=?\n" +
                    "ORDER BY ord.commitdate_p desc";
        }
        else if (tool.getDialect() instanceof PostgresDBDialect)
        {
            queryGrad ="SELECT ord.commitdate_p, ord.number_p\n" +
                    "FROM abstractstudentextract_t extr\n" +
                    "left join studentextracttypetogroup_t extrType on extrType.type_id=extr.type_id\n" +
                    "left join studentextractgroup_t extrGroupType on extrGroupType.id=extrType.group_id\n" +
                    "inner join abstractstudentparagraph_t par on par.id = extr.paragraph_id\n" +
                    "inner join abstractstudentorder_t ord on ord.id = par.order_id\n" +
                    "WHERE\n" +
                    "extrGroupType.code_p = ?\n" +
                    "and extr.committed_p=?\n" +
                    "and extr.entity_id=?\n" +
                    "ORDER BY ord.commitdate_p desc\n" +
                    "LIMIT 1";
        }
        else if (tool.getDialect() instanceof OracleDBDialect)
        {
            queryGrad = "SELECT ord.commitdate_p, ord.number_p\n" +
                    "FROM abstractstudentextract_t extr\n" +
                    "left join studentextracttypetogroup_t extrType on extrType.type_id=extr.type_id\n" +
                    "left join studentextractgroup_t extrGroupType on extrGroupType.id=extrType.group_id\n" +
                    "inner join abstractstudentparagraph_t par on par.id = extr.paragraph_id\n" +
                    "inner join abstractstudentorder_t ord on ord.id = par.order_id\n" +
                    "WHERE\n" +
                    "extrGroupType.code_p = ?\n" +
                    "and extr.committed_p=?\n" +
                    "and extr.entity_id=?\n" +
                    "and ROWNUM = 1\n " +
                    "ORDER BY ord.commitdate_p desc";
        }

        //находим последнююю выписку "О выпуске (диплом)"
        PreparedStatement graduatePrstm = tool.prepareStatement(queryGrad);

        graduatePrstm.setString(1, StudentExtractGroupCodes.GRADUATION); //код группы приказов "о выпуске (диплом)"
        graduatePrstm.setBoolean(2, true); // выписка должна быть проведена
        graduatePrstm.setLong(3, studentId); //id студента


        ResultSet graduateRs = graduatePrstm.executeQuery();
        while (graduateRs.next())
        {
            graduateOrderDate = graduateRs.getDate(1);
            graduateOrderNumber = graduateRs.getString(2);
        }

        //находим последнююю выписку "О выпуске (диплом с отличием)"
        PreparedStatement graduateSuccessPrstm = tool.prepareStatement(queryGrad);

        graduateSuccessPrstm.setString(1, StudentExtractGroupCodes.GRADUATION_HONOURS); //код группы приказов "о выпуске (диплом с отличием)"
        graduateSuccessPrstm.setBoolean(2, true); // выписка должна быть проведена
        graduateSuccessPrstm.setLong(3, studentId); //id студента


        ResultSet graduateSuccessRs = graduateSuccessPrstm.executeQuery();
        while (graduateSuccessRs.next())
        {
            graduateSuccessOrderDate = graduateSuccessRs.getDate(1);
            graduateSuccessOrderNumber = graduateSuccessRs.getString(2);
        }




        //апдэйтим данные о последних приказах
        PreparedStatement update = tool.prepareStatement("UPDATE orderdata_t " +
                                                                 "SET " +
                                                                 "graduateDiplomaOrderDate_p=?, graduateDiplomaOrderNumber_p=?, " +
                                                                 "grdtsccssdplmordrdt_p=?, grdtsccssdplmordrnmbr_p=?" +
                                                                 "WHERE student_id=?");

        update.setDate(1, graduateOrderDate);
        update.setString(2, graduateOrderNumber);
        update.setDate(3, graduateSuccessOrderDate);
        update.setString(4, graduateSuccessOrderNumber);
        update.setLong(5, studentId);

        update.execute();

    }


}