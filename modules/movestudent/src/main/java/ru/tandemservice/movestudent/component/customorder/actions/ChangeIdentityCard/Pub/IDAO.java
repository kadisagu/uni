/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeIdentityCard.Pub;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.IBaseCustomActionPubDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public interface IDAO extends IBaseCustomActionPubDAO<ChangeIdentityCardAction, Model>
{
}