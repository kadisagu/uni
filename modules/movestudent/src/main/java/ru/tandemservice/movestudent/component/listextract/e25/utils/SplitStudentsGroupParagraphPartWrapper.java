/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e25.utils;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 27.11.2012
 */
public class SplitStudentsGroupParagraphPartWrapper implements Comparable<SplitStudentsGroupParagraphPartWrapper>
{
    private final EducationLevelsHighSchool _educationLevel;
    private final Qualifications _qualification;
    private final Group _groupNew;
    private final ListStudentExtract _firstExtract;

    public SplitStudentsGroupParagraphPartWrapper(EducationLevelsHighSchool educationLevel, Group groupNew, Qualifications qualification, ListStudentExtract firstExtract)
    {
        _educationLevel = educationLevel;
        _qualification = qualification;
        _groupNew = groupNew;
        _firstExtract = firstExtract;
    }

    private List<Person> _personList = new ArrayList<>();

    public EducationLevelsHighSchool getEducationLevel()
    {
        return _educationLevel;
    }

    public Group getGroupNew()
    {
        return _groupNew;
    }

    public Qualifications getQualification()
    {
        return _qualification;
    }

    public List<Person> getPersonList()
    {
        return _personList;
    }

    public void setPersonList(List<Person> personList)
    {
        _personList = personList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof SplitStudentsGroupParagraphPartWrapper))
            return false;

        SplitStudentsGroupParagraphPartWrapper that = (SplitStudentsGroupParagraphPartWrapper) o;

        return _educationLevel.equals(that.getEducationLevel()) && _groupNew.equals(that.getGroupNew());
    }

    @Override
    public int hashCode()
    {
        return _educationLevel.hashCode() & _groupNew.hashCode();
    }

    @Override
    public int compareTo(SplitStudentsGroupParagraphPartWrapper o)
    {
        int result = _educationLevel.getFullTitle().compareTo(o.getEducationLevel().getFullTitle());
        if (result == 0)
            result = _groupNew.getTitle().compareTo(o.getGroupNew().getTitle());

        return result;
    }
}