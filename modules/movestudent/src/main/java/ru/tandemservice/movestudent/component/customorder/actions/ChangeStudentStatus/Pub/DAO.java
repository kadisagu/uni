/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeStudentStatus.Pub;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.BaseCustomActionPubDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeStudentStatusAction;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class DAO extends BaseCustomActionPubDAO<ChangeStudentStatusAction, Model>
{
    @Override
    protected Class<ChangeStudentStatusAction> getActionClass()
    {
        return ChangeStudentStatusAction.class;
    }
}