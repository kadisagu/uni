/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e48;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.GroupToExtractRelation;
import ru.tandemservice.movestudent.entity.RemoveGroupManagerStuExtract;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.GroupCaptainStudent;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.List;
import java.util.Map;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 13.08.2012
 */
public class RemoveGroupManagerStuExtractDao extends UniBaseDao implements IExtractComponentDao<RemoveGroupManagerStuExtract>
{
    public void doCommit(RemoveGroupManagerStuExtract extract, Map parameters)
    {
         //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GroupToExtractRelation.class, "ge");
        builder.where(DQLExpressions.eq(DQLExpressions.property("ge", GroupToExtractRelation.extract().id()), DQLExpressions.value(extract.getId())));
        builder.column(DQLExpressions.property(GroupCaptainStudent.group().fromAlias("ge"))).distinct();

        Student student = extract.getEntity();
        List<Group> groupList = builder.createStatement(getSession()).list();
        List<Group> errorGroups = Lists.newArrayList();

        for(Group group : groupList)
        {
            List<Student> studentList = UniDaoFacade.getGroupDao().getCaptainStudentList(group);
            if(studentList.isEmpty() || !studentList.contains(extract.getEntity()))
                errorGroups.add(group);
        }

        if(!errorGroups.isEmpty())
        {
            List<String> errorGroupsTitles = Lists.newArrayList();
            for(Group errorGroup : errorGroups)
            {
                errorGroupsTitles.add(errorGroup.getTitle());
            }
            throw new ApplicationException("Невозможно провести приказ, поскольку студент " +extract.getEntity().getFio() + " не является старостой " + (errorGroupsTitles.size() > 1 ? "групп " : " группы ") + StringUtils.join(errorGroupsTitles, ", ") + ".");
        }

        for(Group group : groupList)
            UniDaoFacade.getGroupDao().deleteCaptainStudent(group, student);
    }

    public void doRollback(RemoveGroupManagerStuExtract extract, Map parameters)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GroupToExtractRelation.class, "ge");
        builder.where(DQLExpressions.eq(DQLExpressions.property("ge", GroupToExtractRelation.extract().id()), DQLExpressions.value(extract.getId())));
        builder.column(DQLExpressions.property(GroupCaptainStudent.group().fromAlias("ge"))).distinct();

        List<Group> groupList = builder.createStatement(getSession()).list();

        for(Group group : groupList)
        {
            if (!UniDaoFacade.getGroupDao().isCaptainStudent(group, extract.getEntity()))
            {
                UniDaoFacade.getGroupDao().addCaptainStudent(group, extract.getEntity());
            }
        }
    }
}