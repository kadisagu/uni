package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ChangeProfileStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О переводе со специализации на специализацию (с профиля на профиль)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChangeProfileStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ChangeProfileStuListExtract";
    public static final String ENTITY_NAME = "changeProfileStuListExtract";
    public static final int VERSION_HASH = -945980322;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_ORG_UNIT_NEW = "educationOrgUnitNew";
    public static final String L_EDUCATION_ORG_UNIT_OLD = "educationOrgUnitOld";
    public static final String P_CHANGE_EDU_PLAN = "changeEduPlan";
    public static final String P_CHANGE_EDU_PLAN_DEADLINE_DATE = "changeEduPlanDeadlineDate";

    private EducationOrgUnit _educationOrgUnitNew;     // Новый профиль (специализация)
    private EducationOrgUnit _educationOrgUnitOld;     // Старый профиль (специализация)
    private boolean _changeEduPlan;     // Разница в учебных планах
    private Date _changeEduPlanDeadlineDate;     // Дата ликвидации

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Новый профиль (специализация). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitNew()
    {
        return _educationOrgUnitNew;
    }

    /**
     * @param educationOrgUnitNew Новый профиль (специализация). Свойство не может быть null.
     */
    public void setEducationOrgUnitNew(EducationOrgUnit educationOrgUnitNew)
    {
        dirty(_educationOrgUnitNew, educationOrgUnitNew);
        _educationOrgUnitNew = educationOrgUnitNew;
    }

    /**
     * @return Старый профиль (специализация). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitOld()
    {
        return _educationOrgUnitOld;
    }

    /**
     * @param educationOrgUnitOld Старый профиль (специализация). Свойство не может быть null.
     */
    public void setEducationOrgUnitOld(EducationOrgUnit educationOrgUnitOld)
    {
        dirty(_educationOrgUnitOld, educationOrgUnitOld);
        _educationOrgUnitOld = educationOrgUnitOld;
    }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     */
    @NotNull
    public boolean isChangeEduPlan()
    {
        return _changeEduPlan;
    }

    /**
     * @param changeEduPlan Разница в учебных планах. Свойство не может быть null.
     */
    public void setChangeEduPlan(boolean changeEduPlan)
    {
        dirty(_changeEduPlan, changeEduPlan);
        _changeEduPlan = changeEduPlan;
    }

    /**
     * @return Дата ликвидации.
     */
    public Date getChangeEduPlanDeadlineDate()
    {
        return _changeEduPlanDeadlineDate;
    }

    /**
     * @param changeEduPlanDeadlineDate Дата ликвидации.
     */
    public void setChangeEduPlanDeadlineDate(Date changeEduPlanDeadlineDate)
    {
        dirty(_changeEduPlanDeadlineDate, changeEduPlanDeadlineDate);
        _changeEduPlanDeadlineDate = changeEduPlanDeadlineDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ChangeProfileStuListExtractGen)
        {
            setEducationOrgUnitNew(((ChangeProfileStuListExtract)another).getEducationOrgUnitNew());
            setEducationOrgUnitOld(((ChangeProfileStuListExtract)another).getEducationOrgUnitOld());
            setChangeEduPlan(((ChangeProfileStuListExtract)another).isChangeEduPlan());
            setChangeEduPlanDeadlineDate(((ChangeProfileStuListExtract)another).getChangeEduPlanDeadlineDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChangeProfileStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChangeProfileStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new ChangeProfileStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "educationOrgUnitNew":
                    return obj.getEducationOrgUnitNew();
                case "educationOrgUnitOld":
                    return obj.getEducationOrgUnitOld();
                case "changeEduPlan":
                    return obj.isChangeEduPlan();
                case "changeEduPlanDeadlineDate":
                    return obj.getChangeEduPlanDeadlineDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "educationOrgUnitNew":
                    obj.setEducationOrgUnitNew((EducationOrgUnit) value);
                    return;
                case "educationOrgUnitOld":
                    obj.setEducationOrgUnitOld((EducationOrgUnit) value);
                    return;
                case "changeEduPlan":
                    obj.setChangeEduPlan((Boolean) value);
                    return;
                case "changeEduPlanDeadlineDate":
                    obj.setChangeEduPlanDeadlineDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationOrgUnitNew":
                        return true;
                case "educationOrgUnitOld":
                        return true;
                case "changeEduPlan":
                        return true;
                case "changeEduPlanDeadlineDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationOrgUnitNew":
                    return true;
                case "educationOrgUnitOld":
                    return true;
                case "changeEduPlan":
                    return true;
                case "changeEduPlanDeadlineDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "educationOrgUnitNew":
                    return EducationOrgUnit.class;
                case "educationOrgUnitOld":
                    return EducationOrgUnit.class;
                case "changeEduPlan":
                    return Boolean.class;
                case "changeEduPlanDeadlineDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChangeProfileStuListExtract> _dslPath = new Path<ChangeProfileStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChangeProfileStuListExtract");
    }
            

    /**
     * @return Новый профиль (специализация). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeProfileStuListExtract#getEducationOrgUnitNew()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
    {
        return _dslPath.educationOrgUnitNew();
    }

    /**
     * @return Старый профиль (специализация). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeProfileStuListExtract#getEducationOrgUnitOld()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
    {
        return _dslPath.educationOrgUnitOld();
    }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeProfileStuListExtract#isChangeEduPlan()
     */
    public static PropertyPath<Boolean> changeEduPlan()
    {
        return _dslPath.changeEduPlan();
    }

    /**
     * @return Дата ликвидации.
     * @see ru.tandemservice.movestudent.entity.ChangeProfileStuListExtract#getChangeEduPlanDeadlineDate()
     */
    public static PropertyPath<Date> changeEduPlanDeadlineDate()
    {
        return _dslPath.changeEduPlanDeadlineDate();
    }

    public static class Path<E extends ChangeProfileStuListExtract> extends ListStudentExtract.Path<E>
    {
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitNew;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitOld;
        private PropertyPath<Boolean> _changeEduPlan;
        private PropertyPath<Date> _changeEduPlanDeadlineDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Новый профиль (специализация). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeProfileStuListExtract#getEducationOrgUnitNew()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
        {
            if(_educationOrgUnitNew == null )
                _educationOrgUnitNew = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_NEW, this);
            return _educationOrgUnitNew;
        }

    /**
     * @return Старый профиль (специализация). Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeProfileStuListExtract#getEducationOrgUnitOld()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
        {
            if(_educationOrgUnitOld == null )
                _educationOrgUnitOld = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_OLD, this);
            return _educationOrgUnitOld;
        }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeProfileStuListExtract#isChangeEduPlan()
     */
        public PropertyPath<Boolean> changeEduPlan()
        {
            if(_changeEduPlan == null )
                _changeEduPlan = new PropertyPath<Boolean>(ChangeProfileStuListExtractGen.P_CHANGE_EDU_PLAN, this);
            return _changeEduPlan;
        }

    /**
     * @return Дата ликвидации.
     * @see ru.tandemservice.movestudent.entity.ChangeProfileStuListExtract#getChangeEduPlanDeadlineDate()
     */
        public PropertyPath<Date> changeEduPlanDeadlineDate()
        {
            if(_changeEduPlanDeadlineDate == null )
                _changeEduPlanDeadlineDate = new PropertyPath<Date>(ChangeProfileStuListExtractGen.P_CHANGE_EDU_PLAN_DEADLINE_DATE, this);
            return _changeEduPlanDeadlineDate;
        }

        public Class getEntityClass()
        {
            return ChangeProfileStuListExtract.class;
        }

        public String getEntityName()
        {
            return "changeProfileStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
