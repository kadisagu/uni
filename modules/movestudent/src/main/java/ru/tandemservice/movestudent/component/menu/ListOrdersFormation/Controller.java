/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.menu.ListOrdersFormation;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractOrder;

/**
 * @author vip_delete
 * @since 18.10.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setSettings(UniBaseUtils.getDataSettings(component, "ListOrdersFormation.filter"));

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;
        boolean admin = UserContext.getInstance().getPrincipalContext().isAdmin();

        DynamicListDataSource<IAbstractOrder> dataSource = UniBaseUtils.createDataSource(component, getDao());

        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Дата формирования", IAbstractOrder.P_CREATE_DATE);
        linkColumn.setFormatter(DateFormatter.DATE_FORMATTER_WITH_TIME);
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Дата приказа", AbstractStudentOrder.P_COMMIT_DATE).setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ приказа", IAbstractOrder.P_NUMBER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Подразделение", Model.ORG_UNI_FULL_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип приказа", admin ? StudentListOrder.type().titleWithCode().s() : StudentListOrder.type().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Причина приказа", StudentListOrder.reason().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", new String[]{IAbstractOrder.L_STATE, ICatalogItem.CATALOG_ITEM_TITLE}).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во параграфов", AbstractStudentOrder.P_COUNT_PARAGRAPH).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Исполнитель", AbstractStudentOrder.P_EXECUTOR).setClickable(false));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrint", "Печать").setPermissionKey("print_menuList_listOrdersFormation").setDisabledProperty("disabledPrint"));
        if(MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
            dataSource.addColumn(CommonBaseUtil.getPrintPdfColumn("onClickPrintPdf", "Печать PDF").setPermissionKey("printPdf_menuList_listOrdersFormation").setDisabledProperty("disabledPrint"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey("edit_menuList_listOrdersFormation").setDisabledProperty(AbstractStudentOrder.P_READONLY));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить «{0}»?", IAbstractOrder.P_TITLE).setPermissionKey("delete_menuList_listOrdersFormation"));

        dataSource.setOrder(0, OrderDirection.desc);
        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);

        UniBaseUtils.validateDatesPeriod(model.getSettings(), "commitDateFrom", "commitDateTo");
        UniBaseUtils.validateDatesPeriod(model.getSettings(), "createDateFrom", "createDateTo");

        if (!UserContext.getInstance().getErrorCollector().isHasFieldErrors())
        {
            DataSettingsFacade.saveSettings(model.getSettings());
            model.getDataSource().refresh();
        }
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }

    public void onClickAdd(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveStudentComponents.STUDENT_MODULAR_ORDER_ADD, new ParametersMap()
                .add("orgUnitId", null)
        ));
    }

    public void onClickPrint(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_ORDER_PRINT, new ParametersMap()
                .add("orderId", component.getListenerParameter())
        ));
    }

    public void onClickPrintPdf(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_LIST_ORDER_PRINT, new ParametersMap()
                .add("orderId", component.getListenerParameter())
                .add("printPdf", Boolean.TRUE)
        ));
    }

    public void onClickEdit(IBusinessComponent component)
    {
        StudentListOrder order = getDao().getNotNull((Long) component.getListenerParameter());
        component.createDefaultChildRegion(new ComponentActivator(MoveStudentUtils.getListOrderAddEditComponent(order.getType()), new ParametersMap()
                .add("orderId", component.getListenerParameter())
        ));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }
}