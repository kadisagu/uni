package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import ru.tandemservice.movestudent.entity.StudentModularParagraph;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параграф сборного приказа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentModularParagraphGen extends AbstractStudentParagraph
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.StudentModularParagraph";
    public static final String ENTITY_NAME = "studentModularParagraph";
    public static final int VERSION_HASH = 89362363;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof StudentModularParagraphGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentModularParagraphGen> extends AbstractStudentParagraph.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentModularParagraph.class;
        }

        public T newInstance()
        {
            return (T) new StudentModularParagraph();
        }
    }
    private static final Path<StudentModularParagraph> _dslPath = new Path<StudentModularParagraph>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentModularParagraph");
    }
            

    public static class Path<E extends StudentModularParagraph> extends AbstractStudentParagraph.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return StudentModularParagraph.class;
        }

        public String getEntityName()
        {
            return "studentModularParagraph";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
