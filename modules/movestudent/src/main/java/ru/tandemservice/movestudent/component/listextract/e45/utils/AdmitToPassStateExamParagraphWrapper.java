/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e45.utils;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 03.06.2016
 */
public class AdmitToPassStateExamParagraphWrapper implements Comparable<AdmitToPassStateExamParagraphWrapper>
{
    private final OrgUnit _formativeOrgUnit;
    private final EducationLevelsHighSchool _educationLevels;
    private final DevelopForm _developForm;
    private final Course _course;
    private final CompensationType _compensationType;
    private final StudentListParagraph _paragraph;
    private final List<Group> _groups = new ArrayList<>();

    private List<AdmitToPassSomething> _studentsList;

    public AdmitToPassStateExamParagraphWrapper(OrgUnit formativeOrgUnit, EducationLevelsHighSchool educationLevels, DevelopForm developForm,
                                                Course course, CompensationType compensationType, StudentListParagraph paragraph)
    {
        _formativeOrgUnit = formativeOrgUnit;
        _educationLevels = educationLevels;
        _developForm = developForm;
        _course = course;
        _compensationType = compensationType;
        _paragraph = paragraph;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public EducationLevelsHighSchool getEducationLevels()
    {
        return _educationLevels;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public Course getCourse()
    {
        return _course;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public StudentListParagraph getParagraph()
    {
        return _paragraph;
    }

    public List<Group> getGroups()
    {
        if (_groups.isEmpty())
        {
            for (AdmitToPassSomething extract : _studentsList)
            {
                if (!_groups.contains(extract.getEntity().getGroup()))
                    _groups.add(extract.getEntity().getGroup());
            }
        }

        Collections.sort(_groups, new Comparator<Group>()
        {
            @Override
            public int compare(Group o1, Group o2)
            {
                return o1.getTitle().compareTo(o2.getTitle());
            }
        });

        return _groups;
    }

    public List<AdmitToPassSomething> getStudentsList()
    {
        if (null == _studentsList) _studentsList = new ArrayList<>();
        return _studentsList;
    }

    public AdmitToPassSomething getFirstExtract()
    {
        return getStudentsList().isEmpty() || null == getStudentsList().get(0) ? null : getStudentsList().get(0);
    }

    public String getGroupsStr()
    {
        StringBuilder groupStrBuilder = new StringBuilder();
        for (Group group : getGroups())
        {
            groupStrBuilder.append(groupStrBuilder.length() > 0 ? ", " : "").append(group.getTitle());
        }
        return groupStrBuilder.toString();
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof AdmitToPassStateExamParagraphWrapper)) return false;

        AdmitToPassStateExamParagraphWrapper that = (AdmitToPassStateExamParagraphWrapper) o;

        return _formativeOrgUnit.equals(that.getFormativeOrgUnit())
                && _educationLevels.equals(that.getEducationLevels())
                && _developForm.equals(that.getDevelopForm())
                && _course.equals(that.getCourse())
                && _compensationType.equals(that.getCompensationType())
                && _paragraph.equals(that.getParagraph());
    }

    @Override
    public int hashCode()
    {
        return _formativeOrgUnit.hashCode() & _educationLevels.hashCode() & _developForm.hashCode() & _course.hashCode() & _compensationType.hashCode() & _paragraph.hashCode();
    }

    @Override
    public int compareTo(AdmitToPassStateExamParagraphWrapper o)
    {
        int result = 0;
        int paragraphNumber1 = _paragraph.getNumber();
        int paragraphNumber2 = o.getParagraph().getNumber();
        result = Integer.compare(paragraphNumber1, paragraphNumber2);
        if (0 != result) return result;

        String formativeOrgUnitStr1 = null != _formativeOrgUnit.getNominativeCaseTitle() ? _formativeOrgUnit.getNominativeCaseTitle() : _formativeOrgUnit.getFullTitle();
        String formativeOrgUnitStr2 = null != o.getFormativeOrgUnit().getNominativeCaseTitle() ? o.getFormativeOrgUnit().getNominativeCaseTitle() : o.getFormativeOrgUnit().getFullTitle();
        result = formativeOrgUnitStr1.compareTo(formativeOrgUnitStr2);
        if (0 != result) return result;

        StructureEducationLevels thisLevels = _educationLevels.getEducationLevel().getLevelType();
        StructureEducationLevels thatLevels = o.getEducationLevels().getEducationLevel().getLevelType();

        if (thisLevels == null || thatLevels == null)
            return thisLevels == null ? (thatLevels == null ? 0 : 1) : -1;

        boolean isThisChild = thisLevels.isSpecialty() || thisLevels.isProfile();
        boolean isThatChild = thatLevels.isSpecialty() || thatLevels.isProfile();

        if (isThisChild != isThatChild) return isThisChild ? 1 : -1;

        result = _educationLevels.getTitle().compareTo(o.getEducationLevels().getTitle());
        if (result != 0) return result;

        result = _developForm.getCode().compareTo(o.getDevelopForm().getCode());
        if (result != 0) return result;

        if (_course.getIntValue() > o.getCourse().getIntValue()) return 1;
        else if (_course.getIntValue() < o.getCourse().getIntValue()) return -1;

        return _compensationType.getTitle().compareTo(o.getCompensationType().getTitle());
    }
}