/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.commons;

import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;

/**
 * @author vip_delete
 * @since 23.12.2008
 */
public interface IMoveStudentMQBuilder
{
    // filter names ordered by name

    String COMMIT_DATE = "commitDate";                               // Дата приказа
    String COMMIT_DATE_FROM = "commitDateFrom";                      // Дата приказа с
    String COMMIT_DATE_TO = "commitDateTo";                          // Дата приказа по
    String COMMIT_DATE_SYSTEM = "commitDateSystem";                  // Дата проведения приказа
    String COMMIT_DATE_SYSTEM_FROM = "commitDateSystemFrom";         // Дата проведения приказа с
    String COMMIT_DATE_SYSTEM_TO = "commitDateSystemTo";             // Дата проведения приказа по
    String COMPENSATION_TYPE = "compensationType";                   // Вид возмещения затрат
    String COURSE = "course";                                        // Курс
    String CREATE_DATE = "createDate";                               // Дата формирования приказа (выписки)
    String CREATE_DATE_FROM = "createDateFrom";                      // Дата формирования приказа (выписки) с
    String CREATE_DATE_TO = "createDateTo";                          // Дата формирования приказа (выписки) по
    String DEVELOP_FORM ="developForm";                              // Форма освоения
    String EDUCATION_LEVEL_HIGH_SCHOOL = "educationLevelHighSchool"; // Уровень образования ОУ
    String EDUCATION_YEAR = "educationYear";                         // Учебный год
    String FORMATIVE_ORGUNIT = "formativeOrgUnit";                   // Формирующее подр.
    String EXTRACT_REASON = "extractReason";                         // Причина выписки
    String ORDER_REASON = "orderReason";                             // Причина приказа
    String LAST_NAME = "lastName";                                   // Фамилия студента
    String NUMBER = "number";                                        // № приказа
    String PRODUCING_ORGUNIT = "producingOrgUnit";                   // Выпускающее подразделение
    String STATE = "state";                                          // Состояние приказа (выписки)
    String STUDENTS = "students";                                    // Студенты
    String ORG_UNIT = "orgUnit";                                     // Подразделение
    String TYPE = "type";                                            // Тип приказа (выписки)
    String TERRITORIAL_ORGUNIT = "territorialOrgUnit";               // Территориальное подр.
    String EXECUTOR = "executor";
    String EDUCATION_LEVEL_TYPE_LIST = "educationLevelTypeList";
    String EXTRACT_EXECUTOR = "extractExecutor";                      // Исполнитель выписки


    // builder alias ordered by name

    String EDUCATION_LEVEL_HIGHSCHOOL_ALIAS = "_educationLevelHighSchool";
    String EDUCATION_ORGUNIT_ALIAS = "_educationOrgUnit";
    String EXTRACT_ALIAS = "_extract";
    String IDENTITY_CARD_ALIAS = "_idCard";
    String ORDER_ALIAS = "_order";
    String PARAGRAPH_ALIAS = "_paragraph";
    String STATE_ALIAS = "_state";
    String STUDENT_ALIAS = "_student";

    // MQBuilder delegate method

    MQBuilder getMQBuilder();

    // order filters

    void applyListOrderType();

    void applyListOrderTypes();

    void applyListOrderReason();

    void applyOrderCommitDate();

    void applyOrderCommitDateSystem();

    void applyOrderCreateDate();

    void applyOrderEducationYear();

    void applyListOrderEducationYear();

    void applyOrderNumber();

    void applyOrderOrgUnit();

    void applyOrderState();

    void applyOrderStudent(Session session);

    // extract filters ordered by name

    void applyExtractCompensationType();

    void applyExtractCourse();

    void applyExtractDevelopForm();

    void applyExtractCreateDate();

    void applyExtractOrderDate();

    void applyExtractEducationLevelHighSchool();

    void applyExtractReason();

    void applyOrderReason();

    void applyExtractEducationYear();

    void applyExtractOrderEducationYear();

    void applyExtractFormativeOrgUnit();

    void applyExtractLastName();

    void applyExtractProducingOrgUnit();

    void applyExtractState();

    void applyExtractStudent(Session session);

    void applyExtractTerritorialOrgUnit();

    void applyExtractType();

    void applyListExtractType();

    void applyExecutor();

    void applyExtractExecutor();

    // create page

    void createPage(Session session, OrderDescriptionRegistry orderDescriptionRegistry, DynamicListDataSource<? extends IEntity> dataSource);

    //typeOrderFormationOrFinished -  true для фильтрации ListOrderFinished
    //false для фильтрации ListOrdersFormation
    public void applyEducationLevel(Session session, boolean typeOrderFormationOrFinished);

    public void applyEducationLevelIndividual(Session session);
}