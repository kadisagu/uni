/* $Id: extractPrint.vm 23792 2012-08-10 10:47:17Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e96;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AdmitToDiplomaWithoutExamStuExtract;
import ru.tandemservice.uni.entity.catalog.Qualifications;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 17.01.2013
 */
public class AdmitToDiplomaWithoutExamStuExtractPrint implements IPrintFormCreator<AdmitToDiplomaWithoutExamStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, AdmitToDiplomaWithoutExamStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());
        String qualificationCode = null != qualification ? qualification.getCode() : null;

        String workType;
        if (null != qualificationCode && qualificationCode.equals("62"))
            workType = "квалификационной работы (бакалаврской работы)";
        else if (null != qualificationCode && qualificationCode.equals("68"))
            workType = "квалификационной работы (магистерской диссертации)";
        else workType = "квалификационной работы";

        modifier.put("workType", workType);

        additionalModify(modifier, extract);

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    public void additionalModify(RtfInjectModifier modifier, AdmitToDiplomaWithoutExamStuExtract extract) {}
}