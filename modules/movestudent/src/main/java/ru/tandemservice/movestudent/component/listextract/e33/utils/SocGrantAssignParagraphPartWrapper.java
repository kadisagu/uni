/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e33.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 03.10.2012
 */
public class SocGrantAssignParagraphPartWrapper implements Comparable<SocGrantAssignParagraphPartWrapper>
{
    private final Course _course;
    private final Group _group;
    private final ListStudentExtract _firstExtract;

    public SocGrantAssignParagraphPartWrapper(Course course, Group group, ListStudentExtract firstExtract)
    {
        _course = course;
        _group = group;
        _firstExtract = firstExtract;
    }

    private final List<SocGrantAssignExtractWrapper> _extractWrapperList = new ArrayList<>();

    public Course getCourse()
    {
        return _course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public List<SocGrantAssignExtractWrapper> getExtractWrapperList()
    {
        return _extractWrapperList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof SocGrantAssignParagraphPartWrapper))
            return false;

        SocGrantAssignParagraphPartWrapper that = (SocGrantAssignParagraphPartWrapper) o;

        return _course.equals(that.getCourse()) && _group.equals(that.getGroup());
    }

    @Override
    public int hashCode()
    {
        return _course.hashCode() & _group.hashCode();
    }

    @Override
    public int compareTo(SocGrantAssignParagraphPartWrapper o)
    {
        int result = ((Integer) _course.getIntValue()).compareTo((Integer) o.getCourse().getIntValue());
        if (result == 0)
            result = _group.getTitle().compareTo(o.getGroup().getTitle());

        return result;
    }
}