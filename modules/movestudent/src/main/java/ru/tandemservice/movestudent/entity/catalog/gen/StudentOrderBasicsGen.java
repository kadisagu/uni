package ru.tandemservice.movestudent.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Основание приказа по студенту
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentOrderBasicsGen extends EntityBase
 implements INaturalIdentifiable<StudentOrderBasicsGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics";
    public static final String ENTITY_NAME = "studentOrderBasics";
    public static final int VERSION_HASH = 826994891;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_COMMENTABLE = "commentable";
    public static final String P_REQUIRED = "required";
    public static final String P_HELP = "help";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private boolean _commentable;     // Комментируемо
    private boolean _required;     // Примечание обязательно
    private String _help;     // Разъяснение
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Комментируемо. Свойство не может быть null.
     */
    @NotNull
    public boolean isCommentable()
    {
        return _commentable;
    }

    /**
     * @param commentable Комментируемо. Свойство не может быть null.
     */
    public void setCommentable(boolean commentable)
    {
        dirty(_commentable, commentable);
        _commentable = commentable;
    }

    /**
     * @return Примечание обязательно. Свойство не может быть null.
     */
    @NotNull
    public boolean isRequired()
    {
        return _required;
    }

    /**
     * @param required Примечание обязательно. Свойство не может быть null.
     */
    public void setRequired(boolean required)
    {
        dirty(_required, required);
        _required = required;
    }

    /**
     * @return Разъяснение.
     */
    public String getHelp()
    {
        return _help;
    }

    /**
     * @param help Разъяснение.
     */
    public void setHelp(String help)
    {
        dirty(_help, help);
        _help = help;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentOrderBasicsGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((StudentOrderBasics)another).getCode());
            }
            setCommentable(((StudentOrderBasics)another).isCommentable());
            setRequired(((StudentOrderBasics)another).isRequired());
            setHelp(((StudentOrderBasics)another).getHelp());
            setTitle(((StudentOrderBasics)another).getTitle());
        }
    }

    public INaturalId<StudentOrderBasicsGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<StudentOrderBasicsGen>
    {
        private static final String PROXY_NAME = "StudentOrderBasicsNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof StudentOrderBasicsGen.NaturalId) ) return false;

            StudentOrderBasicsGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentOrderBasicsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentOrderBasics.class;
        }

        public T newInstance()
        {
            return (T) new StudentOrderBasics();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "commentable":
                    return obj.isCommentable();
                case "required":
                    return obj.isRequired();
                case "help":
                    return obj.getHelp();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "commentable":
                    obj.setCommentable((Boolean) value);
                    return;
                case "required":
                    obj.setRequired((Boolean) value);
                    return;
                case "help":
                    obj.setHelp((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "commentable":
                        return true;
                case "required":
                        return true;
                case "help":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "commentable":
                    return true;
                case "required":
                    return true;
                case "help":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "commentable":
                    return Boolean.class;
                case "required":
                    return Boolean.class;
                case "help":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentOrderBasics> _dslPath = new Path<StudentOrderBasics>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentOrderBasics");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Комментируемо. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics#isCommentable()
     */
    public static PropertyPath<Boolean> commentable()
    {
        return _dslPath.commentable();
    }

    /**
     * @return Примечание обязательно. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics#isRequired()
     */
    public static PropertyPath<Boolean> required()
    {
        return _dslPath.required();
    }

    /**
     * @return Разъяснение.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics#getHelp()
     */
    public static PropertyPath<String> help()
    {
        return _dslPath.help();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends StudentOrderBasics> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Boolean> _commentable;
        private PropertyPath<Boolean> _required;
        private PropertyPath<String> _help;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(StudentOrderBasicsGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Комментируемо. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics#isCommentable()
     */
        public PropertyPath<Boolean> commentable()
        {
            if(_commentable == null )
                _commentable = new PropertyPath<Boolean>(StudentOrderBasicsGen.P_COMMENTABLE, this);
            return _commentable;
        }

    /**
     * @return Примечание обязательно. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics#isRequired()
     */
        public PropertyPath<Boolean> required()
        {
            if(_required == null )
                _required = new PropertyPath<Boolean>(StudentOrderBasicsGen.P_REQUIRED, this);
            return _required;
        }

    /**
     * @return Разъяснение.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics#getHelp()
     */
        public PropertyPath<String> help()
        {
            if(_help == null )
                _help = new PropertyPath<String>(StudentOrderBasicsGen.P_HELP, this);
            return _help;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(StudentOrderBasicsGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return StudentOrderBasics.class;
        }

        public String getEntityName()
        {
            return "studentOrderBasics";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
