/* $Id$ */
package ru.tandemservice.movestudent.component.order.other.StudentOtherOrderPub;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.entity.StudentOtherOrderTextRelation;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.dao.MoveDaoFacade;

/**
 * @author Dmitry Seleznev
 * @since 15.04.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickEdit(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveStudentComponents.STUDENT_OTHER_ORDER_ADD_EDIT, new ParametersMap()
                .add("extractId", getModel(component).getExtract().getId()))
        );
    }

    public void onClickChangeType(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(IMoveStudentComponents.STUDENT_OTHER_ORDER_CHANGE_TYPE, new ParametersMap()
                .add("extractId", getModel(component).getExtract().getId())
        ));
    }

    public void onClickSetExecutor(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(IMoveStudentComponents.STUDENT_ORDER_SET_EXECUTOR, new ParametersMap()
                .add("orderId", getModel(component).getExtract().getParagraph().getOrder().getId())
        ));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(getModel(component).getExtract().getParagraph().getOrder());
        deactivate(component);
    }

    public void onClickUploadPrintForm(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(ru.tandemservice.movestudent.component.order.other.StudentOtherOrderUploadPrintForm.Controller.class.getPackage().getName(), new ParametersMap()
                .add("orderId", getModel(component).getExtract().getParagraph().getOrder().getId())
        ));
    }

    public void onClickDownloadPrintForm(IBusinessComponent component)
    {
        StudentOtherOrderTextRelation rel = UniDaoFacade.getCoreDao().get(StudentOtherOrderTextRelation.class, StudentOtherOrderTextRelation.order().s(), getModel(component).getExtract().getParagraph().getOrder().getId());
        if (null == rel) throw new ApplicationException("К данному приказу не прикреплена печатная форма.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(rel.getFileType()).fileName(rel.getFileName()).document(rel.getContent()), false);
    }

    public void onClickDeletePrintForm(IBusinessComponent component)
    {
        getDao().deleteAttachedPrintForm(getModel(component));
        component.refresh();
    }
}