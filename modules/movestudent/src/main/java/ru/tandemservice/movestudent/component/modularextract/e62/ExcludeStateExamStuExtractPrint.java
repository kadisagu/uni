/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e62;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.ExcludeStateExamStuExtract;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;

import java.util.Collections;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 27.08.2012
 */
public class ExcludeStateExamStuExtractPrint implements IPrintFormCreator<ExcludeStateExamStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ExcludeStateExamStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        String contractTerminationStr = ApplicationRuntime.getProperty("commonextract.contractTerminationStr");
        if (null != contractTerminationStr && CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(extract.getEntity().getCompensationType().getCode()))
        {
            modifier.put("contractTerminationStr", contractTerminationStr);
        }
        else
        {
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("contractTerminationStr"), true, false);
        }
        modifier.put("pluralForStateExam", extract.isPluralForStateExam() ? "государственных экзаменов" : "государственного экзамена");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}