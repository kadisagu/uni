/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e39.AddEdit;

import java.util.Date;

import org.tandemframework.core.info.ErrorCollector;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.PaymentDiscountStuExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 04.06.2010
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<PaymentDiscountStuExtract, Model>
{
    @Override
    protected PaymentDiscountStuExtract createNewInstance()
    {
        return new PaymentDiscountStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void validate(ru.tandemservice.movestudent.component.modularextract.e39.AddEdit.Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        Date beginDate = model.getExtract().getBeginDate();
        Date endDate = model.getExtract().getEndDate();

        if (beginDate.getTime() >= endDate.getTime())
            errors.add("Дата окончания предоставления скидки должна быть позже даты начала", "endDate", "beginDate");
    }
}
