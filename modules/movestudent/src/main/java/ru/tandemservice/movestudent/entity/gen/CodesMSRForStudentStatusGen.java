package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.CodesMSRForStudentStatus;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Соответствие статусов студента кодам ГУП МСР
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CodesMSRForStudentStatusGen extends EntityBase
 implements INaturalIdentifiable<CodesMSRForStudentStatusGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.CodesMSRForStudentStatus";
    public static final String ENTITY_NAME = "codesMSRForStudentStatus";
    public static final int VERSION_HASH = 527805385;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_STATUS = "studentStatus";
    public static final String P_CODE = "code";

    private StudentStatus _studentStatus;     // Статус студента
    private long _code;     // Код МСР

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Статус студента. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public StudentStatus getStudentStatus()
    {
        return _studentStatus;
    }

    /**
     * @param studentStatus Статус студента. Свойство не может быть null и должно быть уникальным.
     */
    public void setStudentStatus(StudentStatus studentStatus)
    {
        dirty(_studentStatus, studentStatus);
        _studentStatus = studentStatus;
    }

    /**
     * @return Код МСР. Свойство не может быть null.
     */
    @NotNull
    public long getCode()
    {
        return _code;
    }

    /**
     * @param code Код МСР. Свойство не может быть null.
     */
    public void setCode(long code)
    {
        dirty(_code, code);
        _code = code;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof CodesMSRForStudentStatusGen)
        {
            if (withNaturalIdProperties)
            {
                setStudentStatus(((CodesMSRForStudentStatus)another).getStudentStatus());
            }
            setCode(((CodesMSRForStudentStatus)another).getCode());
        }
    }

    public INaturalId<CodesMSRForStudentStatusGen> getNaturalId()
    {
        return new NaturalId(getStudentStatus());
    }

    public static class NaturalId extends NaturalIdBase<CodesMSRForStudentStatusGen>
    {
        private static final String PROXY_NAME = "CodesMSRForStudentStatusNaturalProxy";

        private Long _studentStatus;

        public NaturalId()
        {}

        public NaturalId(StudentStatus studentStatus)
        {
            _studentStatus = ((IEntity) studentStatus).getId();
        }

        public Long getStudentStatus()
        {
            return _studentStatus;
        }

        public void setStudentStatus(Long studentStatus)
        {
            _studentStatus = studentStatus;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof CodesMSRForStudentStatusGen.NaturalId) ) return false;

            CodesMSRForStudentStatusGen.NaturalId that = (NaturalId) o;

            if( !equals(getStudentStatus(), that.getStudentStatus()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStudentStatus());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStudentStatus());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CodesMSRForStudentStatusGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CodesMSRForStudentStatus.class;
        }

        public T newInstance()
        {
            return (T) new CodesMSRForStudentStatus();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentStatus":
                    return obj.getStudentStatus();
                case "code":
                    return obj.getCode();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentStatus":
                    obj.setStudentStatus((StudentStatus) value);
                    return;
                case "code":
                    obj.setCode((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentStatus":
                        return true;
                case "code":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentStatus":
                    return true;
                case "code":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentStatus":
                    return StudentStatus.class;
                case "code":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CodesMSRForStudentStatus> _dslPath = new Path<CodesMSRForStudentStatus>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CodesMSRForStudentStatus");
    }
            

    /**
     * @return Статус студента. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.CodesMSRForStudentStatus#getStudentStatus()
     */
    public static StudentStatus.Path<StudentStatus> studentStatus()
    {
        return _dslPath.studentStatus();
    }

    /**
     * @return Код МСР. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CodesMSRForStudentStatus#getCode()
     */
    public static PropertyPath<Long> code()
    {
        return _dslPath.code();
    }

    public static class Path<E extends CodesMSRForStudentStatus> extends EntityPath<E>
    {
        private StudentStatus.Path<StudentStatus> _studentStatus;
        private PropertyPath<Long> _code;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Статус студента. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.CodesMSRForStudentStatus#getStudentStatus()
     */
        public StudentStatus.Path<StudentStatus> studentStatus()
        {
            if(_studentStatus == null )
                _studentStatus = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS, this);
            return _studentStatus;
        }

    /**
     * @return Код МСР. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CodesMSRForStudentStatus#getCode()
     */
        public PropertyPath<Long> code()
        {
            if(_code == null )
                _code = new PropertyPath<Long>(CodesMSRForStudentStatusGen.P_CODE, this);
            return _code;
        }

        public Class getEntityClass()
        {
            return CodesMSRForStudentStatus.class;
        }

        public String getEntityName()
        {
            return "codesMSRForStudentStatus";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
