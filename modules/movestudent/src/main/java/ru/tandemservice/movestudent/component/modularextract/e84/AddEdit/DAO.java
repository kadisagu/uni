/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e84.AddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.RePassDiscComissStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToComissionMember;
import ru.tandemservice.uni.ui.selectModel.PpsEntryModel;

import java.util.ArrayList;
import java.util.Arrays;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 31.08.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<RePassDiscComissStuExtract, Model> implements IDAO
{
    @Override
    protected RePassDiscComissStuExtract createNewInstance()
    {
        return new RePassDiscComissStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setMembersPPSMultiSelectModel(new PpsEntryModel());
        model.setControlActionsList(Arrays.asList((ApplicationRuntime.getProperty("commonextract.controlActionsList")).split(";")));

        if (model.isEditForm())
        {
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(StuExtractToComissionMember.class, "d").column("d")
                    .where(eq(property(StuExtractToComissionMember.extract().id().fromAlias("d")), value(model.getExtractId())))
                    .order(property(StuExtractToComissionMember.priority().fromAlias("d")));
            model.setMembersList(builder.createStatement(getSession()).<StuExtractToComissionMember>list());
        } else
        {
            model.setMembersList(new ArrayList<StuExtractToComissionMember>());
        }
    }

    @Override
    public void update(Model model)
    {
        super.update(model);

        new DQLDeleteBuilder(StuExtractToComissionMember.class)
                .where(eq(property(StuExtractToComissionMember.extract().id()), value(model.getExtract().getId())))
                .createStatement(getSession()).execute();

        int i = 0;
        for (StuExtractToComissionMember member : model.getMembersList())
        {
            StuExtractToComissionMember newMember = new StuExtractToComissionMember();
            newMember.update(member);

            newMember.setPriority(i++);
            newMember.setExtract(model.getExtract());
            getSession().save(newMember);
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);
        if (model.getMembersList().isEmpty())
            errors.add("Необходимо указать хотя бы одного члена комиссии.", "members");
    }
}