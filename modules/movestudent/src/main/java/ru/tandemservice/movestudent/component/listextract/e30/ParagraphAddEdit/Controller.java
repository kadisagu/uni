/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e30.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.movestudent.entity.GiveDiplSuccessExtStuListExtract;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

/**
 * @author ListExtractComponentGenerator
 * @since 17.09.2012
 */
public class Controller extends AbstractListParagraphAddEditController<GiveDiplSuccessExtStuListExtract, IDAO, Model>
{
    public void onChangeGroup(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getGroup() != null)
        {
            EducationOrgUnit educationOrgUnit = model.getGroup().getEducationOrgUnit();
            model.setDevelopForm(educationOrgUnit.getDevelopForm());
            model.setDevelopCondition(educationOrgUnit.getDevelopCondition());
            model.setDevelopTech(educationOrgUnit.getDevelopTech());
            model.setDevelopPeriod(educationOrgUnit.getDevelopPeriod());
        }
    }
}
