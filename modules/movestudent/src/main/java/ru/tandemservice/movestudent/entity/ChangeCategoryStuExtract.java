package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.entity.gen.ChangeCategoryStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. Об изменении категории обучаемого
 */
public class ChangeCategoryStuExtract extends ChangeCategoryStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getCategoryChangeDate();
    }
}