/* $Id: DAO.java.vm 16690 2011-02-19 08:39:38Z iolshvang $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e41.ParagraphAddEdit;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.EntityUtils;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.component.listextract.e41.utils.EmployeePostVO;
import ru.tandemservice.movestudent.component.listextract.e41.utils.OrgUnitVO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract;
import ru.tandemservice.movestudent.utils.CachedSingleSelectTextModel;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;

import java.util.*;

/**
 * @author ListExtractComponentGenerator
 * @since 13.02.2013
 */
public class DAO extends AbstractListParagraphAddEditDAO<SendPracticeInnerStuListExtract, Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setPracticeTypeList(MoveStudentDaoFacade.getMoveStudentDao().getStudentPracticeTypes());
        model.setPracticeKindModel(new CachedSingleSelectTextModel(MoveStudentDaoFacade.getMoveStudentDao().getStudentPracticeKinds()));
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));
        model.setEmployeePostModel(new UniSimpleAutocompleteModel()
        {
            {
                setColumnTitles(new String[] {"ФИО", "Должность", "Подразделение", "Ученая степень"});
                setLabelProperties(new String[] {"a", "b", "c", "d"});
//                setLabelProperties(new String[] {"employeePost.person.fio", "employee.postRelation.postBoundedWithQGandQL.title", "employeePost.orgUnit.titleWithType", "academicDegree.fullTitle"});
            }

            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "e");

                builder.add(MQExpression.eq("e", EmployeePost.L_POST_STATUS + "." + EmployeePostStatus.P_ACTIVE, Boolean.TRUE));
                builder.add(MQExpression.like("e", EmployeePost.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FULL_FIO, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("e", EmployeePost.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FULL_FIO, OrderDirection.asc);

                List<EmployeePost> employeePosts = builder.getResultList(getSession(), 0, 50);

                List<EmployeePostVO> employeePostVOs = Lists.newArrayList();

                Map<Long, PersonAcademicDegree> personsDegrees = getAcademicDegreesMap(employeePosts);

                for(EmployeePost employeePost : employeePosts)
                {
                    employeePostVOs.add(new EmployeePostVO(employeePost, personsDegrees.get(employeePost.getPerson().getId())));
                }

                return new ListResult<>(employeePostVOs, builder.getResultCount(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                EmployeePost empl = get((Long) primaryKey);
                if(null != empl)
                {
                    return new EmployeePostVO(empl, getAcademicDegree(empl));
                }
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                if(0 == columnIndex)
                {
                    return ((EmployeePostVO) value).getEmployeePost().getPerson().getFio();
                }
                else if(1 == columnIndex)
                {
                    return ((EmployeePostVO) value).getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getTitle();
                }
                else if(2 == columnIndex)
                {
                    return ((EmployeePostVO) value).getEmployeePost().getOrgUnit().getTitleWithType();
                }
                else if(3 == columnIndex)
                {
                    PersonAcademicDegree academicDegree = ((EmployeePostVO) value).getAcademicDegree();
                    return null != academicDegree ? academicDegree.getAcademicDegree().getTitle() : null;
                }
                else
                    return super.getLabelFor(value, columnIndex);
            }
        });
        model.setOrgUnitModel(new UniSimpleAutocompleteModel()
        {

            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");

                builder.add(MQExpression.eq("ou", OrgUnit.P_ARCHIVAL, Boolean.FALSE));
                builder.add(MQExpression.like("ou", OrgUnit.P_TITLE , CoreStringUtils.escapeLike(filter)));
                builder.addOrder("ou", OrgUnit.P_TITLE, OrderDirection.asc);

                List<OrgUnit> orgUnits = builder.getResultList(getSession(), 0, 50);
                List<OrgUnitVO> orgUnitVOs = Lists.newArrayList();
                for(OrgUnit orgUnit : orgUnits)
                {
                    orgUnitVOs.add(new OrgUnitVO(orgUnit));
                }

                return new ListResult<>(orgUnitVOs, builder.getResultCount(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                OrgUnit ou = get((Long) primaryKey);
                if(null != ou)
                {
                    return new OrgUnitVO(ou);
                }
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((OrgUnitVO) value).getTitle();
            }


        });

        if(model.isEditForm())
        {
            SendPracticeInnerStuListExtract extract = model.getFirstExtract();
            model.setCourse(extract.getCourse());
            model.setGroup(extract.getGroup());
            model.setPracticeType(MoveStudentDaoFacade.getMoveStudentDao().extractPracticeType(extract.getPracticeKind()));
            model.setPracticeKind(MoveStudentDaoFacade.getMoveStudentDao().extractPracticeKind(extract.getPracticeKind()));
            model.setPracticeDuration(extract.getPracticeDurationStr());
            model.setPracticeBeginDate(extract.getPracticeBeginDate());
            model.setPracticeEndDate(extract.getPracticeEndDate());
            model.setAttestationDate(extract.getAttestationDate());
            model.setOutClassTime(extract.isOutClassTime());
            model.setDoneEduPlan(extract.isDoneEduPlan());
            model.setDonePractice(extract.isDonePractice());
            model.setPracticeCourse(extract.getPracticeCourse());
            if(null != extract.getPreventAccidentsIC())
            {
                model.setPreventAccidentsIC(new EmployeePostVO(extract.getPreventAccidentsIC(), extract.getPreventAccidentsICDegree()));
            }
            model.setPreventAccidentsICStr(extract.getPreventAccidentsICStr());
            model.setProvideFundsAccordingToEstimates(extract.isProvideFundsAccordingToEstimates());
            if (model.getProvideFundsAccordingToEstimates())
            {
                model.setEstimateNum(extract.getEstimateNum());
                if(null != extract.getResponsForRecieveCash())
                    model.setResponsForRecieveCash(new EmployeePostVO(extract.getResponsForRecieveCash(), extract.getResponsForRecieveCashDegree()));
                model.setResponsForRecieveCashStr(extract.getResponsForRecieveCashStr());
            }
        }
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        if (model.getCompensationType() != null)
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));
    }
    
    @Override
    protected SendPracticeInnerStuListExtract createNewInstance(Model model)
    {
        return new SendPracticeInnerStuListExtract();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void fillExtract(SendPracticeInnerStuListExtract extract, Student student, Model model)
    {
        IValueMapHolder practiceOrgUnitHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceOrgUnit");
        Map<Long, OrgUnitVO> practiceOrgUnitMap = (null == practiceOrgUnitHolder ? Collections.emptyMap() : practiceOrgUnitHolder.getValueMap());

        IValueMapHolder practiceHeaderHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceHeader");
        Map<Long, EmployeePostVO> practiceHeaderMap = (null == practiceHeaderHolder ? Collections.emptyMap() : practiceHeaderHolder.getValueMap());

        IValueMapHolder practiceHeaderStrHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceHeaderStr");
        Map<Long, String> practiceHeaderStrMap = (null == practiceHeaderStrHolder ? Collections.emptyMap() : practiceHeaderStrHolder.getValueMap());

        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setPracticeKind(MoveStudentDaoFacade.getMoveStudentDao().getFullPracticeKind(model.getPracticeType(), model.getPracticeKind()));
        extract.setPracticeDurationStr(model.getPracticeDuration());
        extract.setPracticeBeginDate(model.getPracticeBeginDate());
        extract.setPracticeEndDate(model.getPracticeEndDate());
        extract.setAttestationDate(model.getAttestationDate());
        extract.setOutClassTime(model.isOutClassTime());
        extract.setDoneEduPlan(model.isDoneEduPlan());
        extract.setDonePractice(model.isDonePractice());
        extract.setPracticeCourse(model.getPracticeCourse());
        if(null != model.getPreventAccidentsIC())
        {
            extract.setPreventAccidentsIC(model.getPreventAccidentsIC().getEmployeePost());
            extract.setPreventAccidentsICDegree(model.getPreventAccidentsIC().getAcademicDegree());
        }
        else
        {
            extract.setPreventAccidentsIC(null);
            extract.setPreventAccidentsICDegree(null);
        }
        extract.setPreventAccidentsICStr(model.getPreventAccidentsICStr());

        extract.setProvideFundsAccordingToEstimates(model.getProvideFundsAccordingToEstimates());
        if (extract.isProvideFundsAccordingToEstimates())
        {
            extract.setEstimateNum(model.getEstimateNum());
            if(null != model.getResponsForRecieveCash())
            {
                extract.setResponsForRecieveCash(model.getResponsForRecieveCash().getEmployeePost());
                extract.setResponsForRecieveCashDegree(model.getResponsForRecieveCash().getAcademicDegree());
            }
            extract.setResponsForRecieveCashStr(model.getResponsForRecieveCashStr());
        }
        else
        {
            extract.setEstimateNum(null);
            extract.setResponsForRecieveCash(null);
            extract.setResponsForRecieveCashDegree(null);
            extract.setResponsForRecieveCashStr(null);
        }

        if(null != practiceHeaderMap.get(student.getId()))
        {
            extract.setPracticeHeader(practiceHeaderMap.get(student.getId()).getEmployeePost());
            extract.setPracticeHeaderDegree(practiceHeaderMap.get(student.getId()).getAcademicDegree());
        }
        else
        {
            extract.setPracticeHeader(null);
            extract.setPracticeHeaderDegree(null);
        }
        extract.setPracticeHeaderStr(practiceHeaderStrMap.get(student.getId()));
        if(null != practiceOrgUnitMap.get(student.getId()))
        {
            extract.setPracticeOrgUnit(practiceOrgUnitMap.get(student.getId()).getOrgUnit());
            extract.setPracticeOrgUnitStr(practiceOrgUnitMap.get(student.getId()).getOrgUnitStr());
        }
        else
        {
            extract.setPracticeOrgUnit(null);
            extract.setPracticeOrgUnitStr(null);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        IValueMapHolder practiceOrgUnitHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceOrgUnit");
        Map<Long, OrgUnitVO> practiceOrgUnitMap = (null == practiceOrgUnitHolder ? Collections.emptyMap() : practiceOrgUnitHolder.getValueMap());

        IValueMapHolder practiceHeaderHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceHeader");
        Map<Long, EmployeePostVO> practiceHeaderMap = (null == practiceHeaderHolder ? Collections.emptyMap() : practiceHeaderHolder.getValueMap());

        IValueMapHolder practiceHeaderStrHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceHeaderStr");
        Map<Long, String> practiceHeaderStrMap = (null == practiceHeaderStrHolder ? Collections.emptyMap() : practiceHeaderStrHolder.getValueMap());

        List<ViewWrapper<Student>> viewlist = new ArrayList<ViewWrapper<Student>>((Collection) ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).getSelectedObjects());

        for (ViewWrapper<Student> wrapper: viewlist)
        {
            if (practiceOrgUnitMap.get(wrapper.getEntity().getId()) == null)
            {
                errorCollector.add("Поле «Подразделение» обязательно для заполнения.", "practiceOrgUnitId_" + wrapper.getEntity().getId());
                errorCollector.add("Поле «Подразделение(печать)» обязательно для заполнения.", "practiceOrgUnitStrId_" + wrapper.getEntity().getId());
            }
            else if(StringUtils.isEmpty(practiceOrgUnitMap.get(wrapper.getEntity().getId()).getOrgUnitStr()))
            {
                errorCollector.add("Поле «Подразделение(печать)» обязательно для заполнения.", "practiceOrgUnitStrId_" + wrapper.getEntity().getId());
            }
            if (practiceHeaderMap.get(wrapper.getEntity().getId()) == null && StringUtils.isEmpty(practiceHeaderStrMap.get(wrapper.getEntity().getId())))
                errorCollector.add("Поле «Руководитель практики» или поле «Руководитель практики(печать)» обязательно для заполнения.", "practiceHeaderInnerId_" + wrapper.getEntity().getId(), "practiceHeaderInnerStrId_" + wrapper.getEntity().getId());
        }

        if(null == model.getPreventAccidentsIC() && StringUtils.isEmpty(model.getPreventAccidentsICStr()))
            errorCollector.add("Поле «Ответственный за соблюдение техники безопасности» или поле «Ответственный за соблюдение техники безопасности (в вин.падеже для печати)» обязательно для заполнения.", "preventAccidentsICStr", "preventAccidentsIC");

        if(model.getProvideFundsAccordingToEstimates())
        {
            if (null == model.getResponsForRecieveCash() && StringUtils.isEmpty(model.getResponsForRecieveCashStr()))
                errorCollector.add("Поле «Ответственный за получение денежных средств» или поле «Ответственный за получение денежных средств(печать)» обязательно для заполнения.", "responsForRecieveCash", "responsForRecieveCashStr");
        }

        if (!model.getPracticeBeginDate().before(model.getPracticeEndDate()))
            errorCollector.add("Дата окончания должна быть больше даты начала", "practiceBeginDate", "practiceEndDate");

        if (errorCollector.hasErrors())
            return;

        super.update(model);
    }

    private Map<Long, PersonAcademicDegree> getAcademicDegreesMap(List<EmployeePost> employeePosts)
    {
        Map<Long, PersonAcademicDegree> academicDegreesMap = Maps.newHashMap();

        Map<Long, List<PersonAcademicDegree>> academicDegreesListMap = Maps.newHashMap();

        List<Long> personsIds = Lists.newArrayList();

        for(EmployeePost employeePost : employeePosts)
        {
            personsIds.add(employeePost.getPerson().getId());
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PersonAcademicDegree.class, "d");
        builder.where(DQLExpressions.in(DQLExpressions.property("d", PersonAcademicDegree.person().id()), personsIds));

        List<PersonAcademicDegree> academicDegreeList = builder.createStatement(getSession()).list();

        for(PersonAcademicDegree degree : academicDegreeList)
        {
            if(null == academicDegreesListMap.get(degree.getPerson().getId()))
            {
                List<PersonAcademicDegree> degrees = Lists.newArrayList();
                degrees.add(degree);
                academicDegreesListMap.put(degree.getPerson().getId(), degrees);
            }
            else
            {
                List<PersonAcademicDegree> degrees = academicDegreesListMap.get(degree.getPerson().getId());
                List<Long> degreesIds = EntityUtils.getIdsFromEntityList(degrees);
                if(!degreesIds.contains(degree.getId()))
                {
                    degrees.add(degree);
                }
            }
        }

        for(Long key : academicDegreesListMap.keySet())
        {
            PersonAcademicDegree personAcademicDegree = null;

            List<PersonAcademicDegree> degrees = academicDegreesListMap.get(key);
            if(!degrees.isEmpty())
            {
                personAcademicDegree = degrees.get(degrees.size() - 1);

                boolean containsWithoutDate = true;
                for (PersonAcademicDegree academicDegree : degrees)
                {
                    if(null == academicDegree.getIssuanceDate())
                    {
                        containsWithoutDate = false;
                        break;
                    }
                }

                if(!containsWithoutDate)
                {
                    EntityComparator<PersonAcademicDegree> comparator = new EntityComparator<>(new EntityOrder(PersonAcademicDegree.P_ISSUANCE_DATE));
                    Collections.sort(degrees, comparator);
                    personAcademicDegree = degrees.get(degrees.size() - 1);
                }
            }
            if(null != personAcademicDegree)
            {
                academicDegreesMap.put(key, personAcademicDegree);
            }
        }
        return academicDegreesMap;
    }

    @Override
    public PersonAcademicDegree getAcademicDegree(EmployeePost employeePost)
    {
        DQLSelectBuilder degreeBuilder = new DQLSelectBuilder().fromEntity(PersonAcademicDegree.class, "d");
        degreeBuilder.where(DQLExpressions.eq(DQLExpressions.property("d", PersonAcademicDegree.person().id()), DQLExpressions.value(employeePost.getPerson().getId())));
        degreeBuilder.order(DQLExpressions.property("d", PersonAcademicDegree.id()));

        List<PersonAcademicDegree> academicDegrees = degreeBuilder.createStatement(getSession()).list();

        PersonAcademicDegree personAcademicDegree = null;

        if(!academicDegrees.isEmpty())
        {
            personAcademicDegree = academicDegrees.get(academicDegrees.size() - 1);

            boolean containsWithoutDate = true;
            for (PersonAcademicDegree academicDegree : academicDegrees)
            {
                if(null == academicDegree.getIssuanceDate())
                {
                    containsWithoutDate = false;
                    break;
                }
            }

            if(!containsWithoutDate)
            {
                EntityComparator<PersonAcademicDegree> comparator = new EntityComparator<>(new EntityOrder(PersonAcademicDegree.P_ISSUANCE_DATE));
                Collections.sort(academicDegrees, comparator);
                personAcademicDegree = academicDegrees.get(academicDegrees.size() - 1);
            }
        }
        return personAcademicDegree;
    }

}