/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e27;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 18.05.2009
 */
public class ExcludeTransfStuExtractPrint implements IPrintFormCreator<ExcludeTransfStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ExcludeTransfStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        
        List<String> tagsToDel = new ArrayList<>();
        modifier.put("dateApp", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getApplyDate()));
        modifier.put("excludeDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDate()));
        modifier.put("transferTo", extract.getTransferTo());

        if (extract.getEntity().getPerson().getIdentityCard().getSex().isMale())
        {
            modifier.put("applied", "подал");
        }
        else
        {
            modifier.put("applied", "подала");
        }
        
        GrammaCase rusCase = GrammaCase.DATIVE;
        IdentityCard identityCard = extract.getEntity().getPerson().getIdentityCard();

        InflectorVariant variant = DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(rusCase);

        CoreCollectionUtils.Pair<String, InflectorVariant> lastNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.lastName().s(), variant);
        CoreCollectionUtils.Pair<String, InflectorVariant> firstNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.firstName().s(), variant);
        CoreCollectionUtils.Pair<String, InflectorVariant> middleNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.middleName().s(), variant);

        String lastName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(lastNamePairKey);
        String firstName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(firstNamePairKey);
        String middleName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(middleNamePairKey);

        StringBuilder fio_D = new StringBuilder(lastName);
        fio_D.append(" ").append(firstName);
        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            fio_D.append(" ").append(middleName);
        
        if(extract.isStopGrantsPaying())
        {
            StringBuilder builder = new StringBuilder();
            builder.append("2. ").append(modifier.getStringValue("Student_D")).append(" ").append(fio_D.toString());
            builder.append(" прекратить выплату стипендии с ");
            builder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStopPayingDate()));
            builder.append(" г.");
            modifier.put("stopPayments", builder.toString());
        }
        else
            tagsToDel.add("stopPayments");
        
        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, tagsToDel, false, false);
        
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}