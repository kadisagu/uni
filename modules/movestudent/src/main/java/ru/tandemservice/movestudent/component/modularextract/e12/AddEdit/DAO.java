/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e12.AddEdit;

import java.util.Date;

import org.tandemframework.core.info.ErrorCollector;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 11.01.2009
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<WeekendPregnancyStuExtract, Model>
{
    public static String getBenefitText(Date beginDate, Date endDate)
    {
        return ApplicationRuntime.getProperty("benefitText")
                .replaceAll("\\{(beginDate|payBenefitBeginDate)\\}", DateFormatter.DEFAULT_DATE_FORMATTER.format(beginDate))
                .replaceAll("\\{(endDate|payBenefitEndDate)\\}", DateFormatter.DEFAULT_DATE_FORMATTER.format(endDate));
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        if (model.isAddForm())
            model.getExtract().setBeginDate(new Date());
        if (model.getExtract().getPayBenefitText() == null)
            model.getExtract().setPayBenefitText(getBenefitText(model.getExtract().getBeginDate(), model.getExtract().getEndDate()));
        if (model.getExtract().getPayOnetimeBenefitText() == null)
            model.getExtract().setPayOnetimeBenefitText(ApplicationRuntime.getProperty("onetimeBenefitText"));

        model.setStudentStatusNewStr(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_PREGNANCY).getTitle());
    }

    @Override
    protected WeekendPregnancyStuExtract createNewInstance()
    {
        return new WeekendPregnancyStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        Date beginDate = model.getExtract().getBeginDate();
        Date endDate = model.getExtract().getEndDate();

        if (beginDate.getTime() >= endDate.getTime())
            errors.add("Дата окончания отпуска должна быть позже даты начала", "endDate");
    }

    @Override
    public void update(Model model)
    {
        //save rollback data
        model.getExtract().setStudentStatusOld(model.getExtract().getEntity().getStatus());
        if (!model.getExtract().isPayBenefit())
            model.getExtract().setPayBenefitText(null);
        if (!model.getExtract().isPayOnetimeBenefit())
            model.getExtract().setPayOnetimeBenefitText(null);
        super.update(model);
    }
}
