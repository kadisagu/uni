/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.modular.StudentModularOrderEdit;

import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * @author vip_delete
 * @since 20.10.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setOrder(refresh(getNotNull(StudentModularOrder.class, model.getOrder().getId())));
    }

    @Override
    public void update(Model model)
    {
        StudentModularOrder order = model.getOrder();
        MoveStudentDaoFacade.getMoveStudentDao().checkOrderNumber(order);
        String currentOrderStateCode = MoveStudentDaoFacade.getMoveStudentDao().getOrderStateCode(order.getId());
        if (MoveStudentDaoFacade.getMoveStudentDao().isModularOrderIndividual(order))
        {
            if (UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(currentOrderStateCode))
                throw new ApplicationException("Нельзя редактировать проведенный приказ.");

            // Для индивидуальных приказов даты формирования выписки и приказа должны совпадать
            AbstractStudentExtract extract = MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(order.getId());
            extract.setCreateDate(order.getCreateDate());
            update(extract);
        }
        else if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(currentOrderStateCode))
            throw new ApplicationException("Редактирование приказа возможно только на этапе формирования.");

        update(order);
    }
}
