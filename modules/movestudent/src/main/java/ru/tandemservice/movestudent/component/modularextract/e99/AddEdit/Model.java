/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e99.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AcadGrantPaymentStopStuExtract;

import java.util.Date;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 20.02.2013
 */
public class Model extends CommonModularStudentExtractAddEditModel<AcadGrantPaymentStopStuExtract>
{
    private ISelectModel _grantOrderModel;
    private AbstractStudentExtract _acadGrantExtract;
    private Date _acadGrantOrderDate;
    private String _acadGrantOrderNumber;

    public ISelectModel getGrantOrderModel()
    {
        return _grantOrderModel;
    }

    public void setGrantOrderModel(ISelectModel grantOrderModel)
    {
        _grantOrderModel = grantOrderModel;
    }

    public Date getAcadGrantOrderDate()
    {
        return _acadGrantOrderDate;
    }

    public void setAcadGrantOrderDate(Date acadGrantOrderDate)
    {
        _acadGrantOrderDate = acadGrantOrderDate;
    }

    public String getAcadGrantOrderNumber()
    {
        return _acadGrantOrderNumber;
    }

    public void setAcadGrantOrderNumber(String acadGrantOrderNumber)
    {
        _acadGrantOrderNumber = acadGrantOrderNumber;
    }

    public AbstractStudentExtract getAcadGrantExtract()
    {
        return _acadGrantExtract;
    }

    public void setAcadGrantExtract(AbstractStudentExtract acadGrantExtract)
    {
        _acadGrantExtract = acadGrantExtract;
    }
}
