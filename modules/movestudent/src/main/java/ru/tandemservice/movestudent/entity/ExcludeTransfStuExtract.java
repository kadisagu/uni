package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.movestudent.entity.gen.ExcludeTransfStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. Об отчислении в связи с переводом
 */
public class ExcludeTransfStuExtract extends ExcludeTransfStuExtractGen implements IExcludeExtract
{
    @Override
    public Date getBeginDate()
    {
        return getDate();
    }

    @Override
    public Date getExcludeDate()
    {
        return getDate();
    }
}