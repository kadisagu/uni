package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О выходе из отпуска по беременности и родам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class WeekendPregnancyOutStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract";
    public static final String ENTITY_NAME = "weekendPregnancyOutStuExtract";
    public static final int VERSION_HASH = -72573735;
    private static IEntityMeta ENTITY_META;

    public static final String P_WEEKEND_OUT_DATE = "weekendOutDate";
    public static final String P_RESUME_GRANT_PAY = "resumeGrantPay";
    public static final String P_GRANT_SUM = "grantSum";
    public static final String P_GRANT_PAY_BEGIN_DATE = "grantPayBeginDate";
    public static final String P_GRANT_PAY_END_DATE = "grantPayEndDate";
    public static final String L_GROUP_NEW = "groupNew";
    public static final String L_GROUP_OLD = "groupOld";
    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";

    private Date _weekendOutDate;     // Дата выхода из отпуска
    private boolean _resumeGrantPay;     // Возобновить выплату стипендии
    private Double _grantSum;     // Размер стипендии
    private Date _grantPayBeginDate;     // Дата начала выплаты стипендии
    private Date _grantPayEndDate;     // Дата окончания выплаты стипендии
    private Group _groupNew;     // Новая группа
    private Group _groupOld;     // Предыдущая группа студента
    private StudentStatus _studentStatusOld;     // Состояние студента на момент проведения приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата выхода из отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getWeekendOutDate()
    {
        return _weekendOutDate;
    }

    /**
     * @param weekendOutDate Дата выхода из отпуска. Свойство не может быть null.
     */
    public void setWeekendOutDate(Date weekendOutDate)
    {
        dirty(_weekendOutDate, weekendOutDate);
        _weekendOutDate = weekendOutDate;
    }

    /**
     * @return Возобновить выплату стипендии. Свойство не может быть null.
     */
    @NotNull
    public boolean isResumeGrantPay()
    {
        return _resumeGrantPay;
    }

    /**
     * @param resumeGrantPay Возобновить выплату стипендии. Свойство не может быть null.
     */
    public void setResumeGrantPay(boolean resumeGrantPay)
    {
        dirty(_resumeGrantPay, resumeGrantPay);
        _resumeGrantPay = resumeGrantPay;
    }

    /**
     * @return Размер стипендии.
     */
    public Double getGrantSum()
    {
        return _grantSum;
    }

    /**
     * @param grantSum Размер стипендии.
     */
    public void setGrantSum(Double grantSum)
    {
        dirty(_grantSum, grantSum);
        _grantSum = grantSum;
    }

    /**
     * @return Дата начала выплаты стипендии.
     */
    public Date getGrantPayBeginDate()
    {
        return _grantPayBeginDate;
    }

    /**
     * @param grantPayBeginDate Дата начала выплаты стипендии.
     */
    public void setGrantPayBeginDate(Date grantPayBeginDate)
    {
        dirty(_grantPayBeginDate, grantPayBeginDate);
        _grantPayBeginDate = grantPayBeginDate;
    }

    /**
     * @return Дата окончания выплаты стипендии.
     */
    public Date getGrantPayEndDate()
    {
        return _grantPayEndDate;
    }

    /**
     * @param grantPayEndDate Дата окончания выплаты стипендии.
     */
    public void setGrantPayEndDate(Date grantPayEndDate)
    {
        dirty(_grantPayEndDate, grantPayEndDate);
        _grantPayEndDate = grantPayEndDate;
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroupNew()
    {
        return _groupNew;
    }

    /**
     * @param groupNew Новая группа. Свойство не может быть null.
     */
    public void setGroupNew(Group groupNew)
    {
        dirty(_groupNew, groupNew);
        _groupNew = groupNew;
    }

    /**
     * @return Предыдущая группа студента. Свойство не может быть null.
     */
    @NotNull
    public Group getGroupOld()
    {
        return _groupOld;
    }

    /**
     * @param groupOld Предыдущая группа студента. Свойство не может быть null.
     */
    public void setGroupOld(Group groupOld)
    {
        dirty(_groupOld, groupOld);
        _groupOld = groupOld;
    }

    /**
     * @return Состояние студента на момент проведения приказа. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Состояние студента на момент проведения приказа. Свойство не может быть null.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof WeekendPregnancyOutStuExtractGen)
        {
            setWeekendOutDate(((WeekendPregnancyOutStuExtract)another).getWeekendOutDate());
            setResumeGrantPay(((WeekendPregnancyOutStuExtract)another).isResumeGrantPay());
            setGrantSum(((WeekendPregnancyOutStuExtract)another).getGrantSum());
            setGrantPayBeginDate(((WeekendPregnancyOutStuExtract)another).getGrantPayBeginDate());
            setGrantPayEndDate(((WeekendPregnancyOutStuExtract)another).getGrantPayEndDate());
            setGroupNew(((WeekendPregnancyOutStuExtract)another).getGroupNew());
            setGroupOld(((WeekendPregnancyOutStuExtract)another).getGroupOld());
            setStudentStatusOld(((WeekendPregnancyOutStuExtract)another).getStudentStatusOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends WeekendPregnancyOutStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) WeekendPregnancyOutStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new WeekendPregnancyOutStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "weekendOutDate":
                    return obj.getWeekendOutDate();
                case "resumeGrantPay":
                    return obj.isResumeGrantPay();
                case "grantSum":
                    return obj.getGrantSum();
                case "grantPayBeginDate":
                    return obj.getGrantPayBeginDate();
                case "grantPayEndDate":
                    return obj.getGrantPayEndDate();
                case "groupNew":
                    return obj.getGroupNew();
                case "groupOld":
                    return obj.getGroupOld();
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "weekendOutDate":
                    obj.setWeekendOutDate((Date) value);
                    return;
                case "resumeGrantPay":
                    obj.setResumeGrantPay((Boolean) value);
                    return;
                case "grantSum":
                    obj.setGrantSum((Double) value);
                    return;
                case "grantPayBeginDate":
                    obj.setGrantPayBeginDate((Date) value);
                    return;
                case "grantPayEndDate":
                    obj.setGrantPayEndDate((Date) value);
                    return;
                case "groupNew":
                    obj.setGroupNew((Group) value);
                    return;
                case "groupOld":
                    obj.setGroupOld((Group) value);
                    return;
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "weekendOutDate":
                        return true;
                case "resumeGrantPay":
                        return true;
                case "grantSum":
                        return true;
                case "grantPayBeginDate":
                        return true;
                case "grantPayEndDate":
                        return true;
                case "groupNew":
                        return true;
                case "groupOld":
                        return true;
                case "studentStatusOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "weekendOutDate":
                    return true;
                case "resumeGrantPay":
                    return true;
                case "grantSum":
                    return true;
                case "grantPayBeginDate":
                    return true;
                case "grantPayEndDate":
                    return true;
                case "groupNew":
                    return true;
                case "groupOld":
                    return true;
                case "studentStatusOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "weekendOutDate":
                    return Date.class;
                case "resumeGrantPay":
                    return Boolean.class;
                case "grantSum":
                    return Double.class;
                case "grantPayBeginDate":
                    return Date.class;
                case "grantPayEndDate":
                    return Date.class;
                case "groupNew":
                    return Group.class;
                case "groupOld":
                    return Group.class;
                case "studentStatusOld":
                    return StudentStatus.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<WeekendPregnancyOutStuExtract> _dslPath = new Path<WeekendPregnancyOutStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "WeekendPregnancyOutStuExtract");
    }
            

    /**
     * @return Дата выхода из отпуска. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract#getWeekendOutDate()
     */
    public static PropertyPath<Date> weekendOutDate()
    {
        return _dslPath.weekendOutDate();
    }

    /**
     * @return Возобновить выплату стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract#isResumeGrantPay()
     */
    public static PropertyPath<Boolean> resumeGrantPay()
    {
        return _dslPath.resumeGrantPay();
    }

    /**
     * @return Размер стипендии.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract#getGrantSum()
     */
    public static PropertyPath<Double> grantSum()
    {
        return _dslPath.grantSum();
    }

    /**
     * @return Дата начала выплаты стипендии.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract#getGrantPayBeginDate()
     */
    public static PropertyPath<Date> grantPayBeginDate()
    {
        return _dslPath.grantPayBeginDate();
    }

    /**
     * @return Дата окончания выплаты стипендии.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract#getGrantPayEndDate()
     */
    public static PropertyPath<Date> grantPayEndDate()
    {
        return _dslPath.grantPayEndDate();
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract#getGroupNew()
     */
    public static Group.Path<Group> groupNew()
    {
        return _dslPath.groupNew();
    }

    /**
     * @return Предыдущая группа студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract#getGroupOld()
     */
    public static Group.Path<Group> groupOld()
    {
        return _dslPath.groupOld();
    }

    /**
     * @return Состояние студента на момент проведения приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    public static class Path<E extends WeekendPregnancyOutStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _weekendOutDate;
        private PropertyPath<Boolean> _resumeGrantPay;
        private PropertyPath<Double> _grantSum;
        private PropertyPath<Date> _grantPayBeginDate;
        private PropertyPath<Date> _grantPayEndDate;
        private Group.Path<Group> _groupNew;
        private Group.Path<Group> _groupOld;
        private StudentStatus.Path<StudentStatus> _studentStatusOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата выхода из отпуска. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract#getWeekendOutDate()
     */
        public PropertyPath<Date> weekendOutDate()
        {
            if(_weekendOutDate == null )
                _weekendOutDate = new PropertyPath<Date>(WeekendPregnancyOutStuExtractGen.P_WEEKEND_OUT_DATE, this);
            return _weekendOutDate;
        }

    /**
     * @return Возобновить выплату стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract#isResumeGrantPay()
     */
        public PropertyPath<Boolean> resumeGrantPay()
        {
            if(_resumeGrantPay == null )
                _resumeGrantPay = new PropertyPath<Boolean>(WeekendPregnancyOutStuExtractGen.P_RESUME_GRANT_PAY, this);
            return _resumeGrantPay;
        }

    /**
     * @return Размер стипендии.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract#getGrantSum()
     */
        public PropertyPath<Double> grantSum()
        {
            if(_grantSum == null )
                _grantSum = new PropertyPath<Double>(WeekendPregnancyOutStuExtractGen.P_GRANT_SUM, this);
            return _grantSum;
        }

    /**
     * @return Дата начала выплаты стипендии.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract#getGrantPayBeginDate()
     */
        public PropertyPath<Date> grantPayBeginDate()
        {
            if(_grantPayBeginDate == null )
                _grantPayBeginDate = new PropertyPath<Date>(WeekendPregnancyOutStuExtractGen.P_GRANT_PAY_BEGIN_DATE, this);
            return _grantPayBeginDate;
        }

    /**
     * @return Дата окончания выплаты стипендии.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract#getGrantPayEndDate()
     */
        public PropertyPath<Date> grantPayEndDate()
        {
            if(_grantPayEndDate == null )
                _grantPayEndDate = new PropertyPath<Date>(WeekendPregnancyOutStuExtractGen.P_GRANT_PAY_END_DATE, this);
            return _grantPayEndDate;
        }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract#getGroupNew()
     */
        public Group.Path<Group> groupNew()
        {
            if(_groupNew == null )
                _groupNew = new Group.Path<Group>(L_GROUP_NEW, this);
            return _groupNew;
        }

    /**
     * @return Предыдущая группа студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract#getGroupOld()
     */
        public Group.Path<Group> groupOld()
        {
            if(_groupOld == null )
                _groupOld = new Group.Path<Group>(L_GROUP_OLD, this);
            return _groupOld;
        }

    /**
     * @return Состояние студента на момент проведения приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

        public Class getEntityClass()
        {
            return WeekendPregnancyOutStuExtract.class;
        }

        public String getEntityName()
        {
            return "weekendPregnancyOutStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
