package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IAcadWeekendExtract;
import ru.tandemservice.movestudent.entity.gen.WeekendStuExtractGen;

import java.util.Date;

public class WeekendStuExtract extends WeekendStuExtractGen implements IAcadWeekendExtract
{
    @Override
    public Date getBeginAcadWeekendDate()
    {
        return getBeginDate();
    }

    @Override
    public Date getEndAcadWeekendDate()
    {
        return getEndDate();
    }
}