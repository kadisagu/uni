package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StatusToExtractRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь выписки и ученых званий ППС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StatusToExtractRelationGen extends EntityBase
 implements INaturalIdentifiable<StatusToExtractRelationGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.StatusToExtractRelation";
    public static final String ENTITY_NAME = "statusToExtractRelation";
    public static final int VERSION_HASH = -437071941;
    private static IEntityMeta ENTITY_META;

    public static final String L_STATUS = "status";
    public static final String L_EXTRACT = "extract";

    private ScienceStatus _status;     // Ученое звание
    private ListStudentExtract _extract;     // Абстрактная выписка из списочного приказа по студенту

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Ученое звание. Свойство не может быть null.
     */
    @NotNull
    public ScienceStatus getStatus()
    {
        return _status;
    }

    /**
     * @param status Ученое звание. Свойство не может быть null.
     */
    public void setStatus(ScienceStatus status)
    {
        dirty(_status, status);
        _status = status;
    }

    /**
     * @return Абстрактная выписка из списочного приказа по студенту. Свойство не может быть null.
     */
    @NotNull
    public ListStudentExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Абстрактная выписка из списочного приказа по студенту. Свойство не может быть null.
     */
    public void setExtract(ListStudentExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StatusToExtractRelationGen)
        {
            if (withNaturalIdProperties)
            {
                setStatus(((StatusToExtractRelation)another).getStatus());
                setExtract(((StatusToExtractRelation)another).getExtract());
            }
        }
    }

    public INaturalId<StatusToExtractRelationGen> getNaturalId()
    {
        return new NaturalId(getStatus(), getExtract());
    }

    public static class NaturalId extends NaturalIdBase<StatusToExtractRelationGen>
    {
        private static final String PROXY_NAME = "StatusToExtractRelationNaturalProxy";

        private Long _status;
        private Long _extract;

        public NaturalId()
        {}

        public NaturalId(ScienceStatus status, ListStudentExtract extract)
        {
            _status = ((IEntity) status).getId();
            _extract = ((IEntity) extract).getId();
        }

        public Long getStatus()
        {
            return _status;
        }

        public void setStatus(Long status)
        {
            _status = status;
        }

        public Long getExtract()
        {
            return _extract;
        }

        public void setExtract(Long extract)
        {
            _extract = extract;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof StatusToExtractRelationGen.NaturalId) ) return false;

            StatusToExtractRelationGen.NaturalId that = (NaturalId) o;

            if( !equals(getStatus(), that.getStatus()) ) return false;
            if( !equals(getExtract(), that.getExtract()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStatus());
            result = hashCode(result, getExtract());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStatus());
            sb.append("/");
            sb.append(getExtract());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StatusToExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StatusToExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new StatusToExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "status":
                    return obj.getStatus();
                case "extract":
                    return obj.getExtract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "status":
                    obj.setStatus((ScienceStatus) value);
                    return;
                case "extract":
                    obj.setExtract((ListStudentExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "status":
                        return true;
                case "extract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "status":
                    return true;
                case "extract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "status":
                    return ScienceStatus.class;
                case "extract":
                    return ListStudentExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StatusToExtractRelation> _dslPath = new Path<StatusToExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StatusToExtractRelation");
    }
            

    /**
     * @return Ученое звание. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StatusToExtractRelation#getStatus()
     */
    public static ScienceStatus.Path<ScienceStatus> status()
    {
        return _dslPath.status();
    }

    /**
     * @return Абстрактная выписка из списочного приказа по студенту. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StatusToExtractRelation#getExtract()
     */
    public static ListStudentExtract.Path<ListStudentExtract> extract()
    {
        return _dslPath.extract();
    }

    public static class Path<E extends StatusToExtractRelation> extends EntityPath<E>
    {
        private ScienceStatus.Path<ScienceStatus> _status;
        private ListStudentExtract.Path<ListStudentExtract> _extract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Ученое звание. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StatusToExtractRelation#getStatus()
     */
        public ScienceStatus.Path<ScienceStatus> status()
        {
            if(_status == null )
                _status = new ScienceStatus.Path<ScienceStatus>(L_STATUS, this);
            return _status;
        }

    /**
     * @return Абстрактная выписка из списочного приказа по студенту. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.StatusToExtractRelation#getExtract()
     */
        public ListStudentExtract.Path<ListStudentExtract> extract()
        {
            if(_extract == null )
                _extract = new ListStudentExtract.Path<ListStudentExtract>(L_EXTRACT, this);
            return _extract;
        }

        public Class getEntityClass()
        {
            return StatusToExtractRelation.class;
        }

        public String getEntityName()
        {
            return "statusToExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
