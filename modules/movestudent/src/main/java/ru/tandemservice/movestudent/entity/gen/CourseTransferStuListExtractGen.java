package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CourseTransferStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О переводе с курса на курс»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CourseTransferStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.CourseTransferStuListExtract";
    public static final String ENTITY_NAME = "courseTransferStuListExtract";
    public static final int VERSION_HASH = -1207451797;
    private static IEntityMeta ENTITY_META;

    public static final String P_TRANSFER_DATE = "transferDate";
    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_COURSE_OLD = "courseOld";
    public static final String L_COURSE_NEW = "courseNew";
    public static final String P_GROUP_CHANGE_COURSE = "groupChangeCourse";

    private Date _transferDate;     // Дата перевода
    private Course _course;     // Курс
    private Group _group;     // Группа
    private Course _courseOld;     // Курс
    private Course _courseNew;     // Курс
    private boolean _groupChangeCourse; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата перевода.
     */
    public Date getTransferDate()
    {
        return _transferDate;
    }

    /**
     * @param transferDate Дата перевода.
     */
    public void setTransferDate(Date transferDate)
    {
        dirty(_transferDate, transferDate);
        _transferDate = transferDate;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourseOld()
    {
        return _courseOld;
    }

    /**
     * @param courseOld Курс. Свойство не может быть null.
     */
    public void setCourseOld(Course courseOld)
    {
        dirty(_courseOld, courseOld);
        _courseOld = courseOld;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourseNew()
    {
        return _courseNew;
    }

    /**
     * @param courseNew Курс. Свойство не может быть null.
     */
    public void setCourseNew(Course courseNew)
    {
        dirty(_courseNew, courseNew);
        _courseNew = courseNew;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isGroupChangeCourse()
    {
        return _groupChangeCourse;
    }

    /**
     * @param groupChangeCourse  Свойство не может быть null.
     */
    public void setGroupChangeCourse(boolean groupChangeCourse)
    {
        dirty(_groupChangeCourse, groupChangeCourse);
        _groupChangeCourse = groupChangeCourse;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof CourseTransferStuListExtractGen)
        {
            setTransferDate(((CourseTransferStuListExtract)another).getTransferDate());
            setCourse(((CourseTransferStuListExtract)another).getCourse());
            setGroup(((CourseTransferStuListExtract)another).getGroup());
            setCourseOld(((CourseTransferStuListExtract)another).getCourseOld());
            setCourseNew(((CourseTransferStuListExtract)another).getCourseNew());
            setGroupChangeCourse(((CourseTransferStuListExtract)another).isGroupChangeCourse());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CourseTransferStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CourseTransferStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new CourseTransferStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return obj.getTransferDate();
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "courseOld":
                    return obj.getCourseOld();
                case "courseNew":
                    return obj.getCourseNew();
                case "groupChangeCourse":
                    return obj.isGroupChangeCourse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "transferDate":
                    obj.setTransferDate((Date) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "courseOld":
                    obj.setCourseOld((Course) value);
                    return;
                case "courseNew":
                    obj.setCourseNew((Course) value);
                    return;
                case "groupChangeCourse":
                    obj.setGroupChangeCourse((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                        return true;
                case "course":
                        return true;
                case "group":
                        return true;
                case "courseOld":
                        return true;
                case "courseNew":
                        return true;
                case "groupChangeCourse":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return true;
                case "course":
                    return true;
                case "group":
                    return true;
                case "courseOld":
                    return true;
                case "courseNew":
                    return true;
                case "groupChangeCourse":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return Date.class;
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "courseOld":
                    return Course.class;
                case "courseNew":
                    return Course.class;
                case "groupChangeCourse":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CourseTransferStuListExtract> _dslPath = new Path<CourseTransferStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CourseTransferStuListExtract");
    }
            

    /**
     * @return Дата перевода.
     * @see ru.tandemservice.movestudent.entity.CourseTransferStuListExtract#getTransferDate()
     */
    public static PropertyPath<Date> transferDate()
    {
        return _dslPath.transferDate();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseTransferStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseTransferStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseTransferStuListExtract#getCourseOld()
     */
    public static Course.Path<Course> courseOld()
    {
        return _dslPath.courseOld();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseTransferStuListExtract#getCourseNew()
     */
    public static Course.Path<Course> courseNew()
    {
        return _dslPath.courseNew();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseTransferStuListExtract#isGroupChangeCourse()
     */
    public static PropertyPath<Boolean> groupChangeCourse()
    {
        return _dslPath.groupChangeCourse();
    }

    public static class Path<E extends CourseTransferStuListExtract> extends ListStudentExtract.Path<E>
    {
        private PropertyPath<Date> _transferDate;
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private Course.Path<Course> _courseOld;
        private Course.Path<Course> _courseNew;
        private PropertyPath<Boolean> _groupChangeCourse;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата перевода.
     * @see ru.tandemservice.movestudent.entity.CourseTransferStuListExtract#getTransferDate()
     */
        public PropertyPath<Date> transferDate()
        {
            if(_transferDate == null )
                _transferDate = new PropertyPath<Date>(CourseTransferStuListExtractGen.P_TRANSFER_DATE, this);
            return _transferDate;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseTransferStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseTransferStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseTransferStuListExtract#getCourseOld()
     */
        public Course.Path<Course> courseOld()
        {
            if(_courseOld == null )
                _courseOld = new Course.Path<Course>(L_COURSE_OLD, this);
            return _courseOld;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseTransferStuListExtract#getCourseNew()
     */
        public Course.Path<Course> courseNew()
        {
            if(_courseNew == null )
                _courseNew = new Course.Path<Course>(L_COURSE_NEW, this);
            return _courseNew;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CourseTransferStuListExtract#isGroupChangeCourse()
     */
        public PropertyPath<Boolean> groupChangeCourse()
        {
            if(_groupChangeCourse == null )
                _groupChangeCourse = new PropertyPath<Boolean>(CourseTransferStuListExtractGen.P_GROUP_CHANGE_COURSE, this);
            return _groupChangeCourse;
        }

        public Class getEntityClass()
        {
            return CourseTransferStuListExtract.class;
        }

        public String getEntityName()
        {
            return "courseTransferStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
