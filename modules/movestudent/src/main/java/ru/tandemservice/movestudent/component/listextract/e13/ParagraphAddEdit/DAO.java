/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e13.ParagraphAddEdit;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractUtil;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.TerritorialTransferStuListExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 06.09.2010
 */
public class DAO extends AbstractListParagraphAddEditDAO<TerritorialTransferStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));

        TerritorialTransferStuListExtract firstExtract = model.getFirstExtract();

        if(null != firstExtract)
        {
            if (null != model.getParagraphId())
            {
                model.setCourse(firstExtract.getCourse());
                model.setGroup(firstExtract.getGroup());
            }
            model.setFormativeOrgUnitNew(firstExtract.getEducationOrgUnitNew().getFormativeOrgUnit());
            model.setTerritorialOrgUnitNew(firstExtract.getEducationOrgUnitNew().getTerritorialOrgUnit());
            model.setDevelopConditionNew(firstExtract.getEducationOrgUnitNew().getDevelopCondition());
            model.setDevelopPeriodNew(firstExtract.getEducationOrgUnitNew().getDevelopPeriod());
            model.setTransferDate(firstExtract.getTransferDate());
        }


        final TerritorialTransferStuListExtract finalExtract = firstExtract;

        model.setFormativeOrgUnitsListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (null == model.getGroup() && (null == finalExtract || model.isParagraphOnlyOneInTheOrder()))
                    return ListResult.getEmpty();

                if (!model.isParagraphOnlyOneInTheOrder() && null != finalExtract)
                {
                    List<OrgUnit> result = new ArrayList<>();
                    result.add(finalExtract.getEducationOrgUnitNew().getFormativeOrgUnit());
                    return new ListResult<>(result);
                }

                MQBuilder subBuilder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou", new String[]{EducationOrgUnit.L_FORMATIVE_ORG_UNIT + "." + OrgUnit.P_ID});
                subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getGroup().getEducationOrgUnit().getEducationLevelHighSchool()));
                subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.P_USED, Boolean.TRUE));
                subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.P_ALLOW_STUDENTS, Boolean.TRUE));
                subBuilder.setNeedDistinct(true);

                MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
                builder.add(MQExpression.in("ou", OrgUnit.P_ID, subBuilder));
                builder.addOrder("ou", OrgUnit.P_TITLE);

                return new ListResult<>(builder.getResultList(getSession()));
            }
        });

        model.setTerritorialOrgUnitsListModel(new FullCheckSelectModel(OrgUnit.P_TERRITORIAL_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (null == model.getGroup() && (null == finalExtract || model.isParagraphOnlyOneInTheOrder()))
                    return ListResult.getEmpty();

                if (!model.isParagraphOnlyOneInTheOrder() && null != finalExtract)
                {
                    List<OrgUnit> result = new ArrayList<>();
                    result.add(finalExtract.getEducationOrgUnitNew().getTerritorialOrgUnit());
                    return new ListResult<>(result);
                }

                MQBuilder subBuilder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou", new String[]{EducationOrgUnit.L_TERRITORIAL_ORG_UNIT + "." + OrgUnit.P_ID});
                subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getGroup().getEducationOrgUnit().getEducationLevelHighSchool()));
                subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnitNew()));
                subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.P_USED, Boolean.TRUE));
                subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.P_ALLOW_STUDENTS, Boolean.TRUE));
                subBuilder.setNeedDistinct(true);

                MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
                builder.add(MQExpression.in("ou", OrgUnit.P_ID, subBuilder));
                builder.addOrder("ou", OrgUnit.P_TERRITORIAL_TITLE);

                return new ListResult<>(builder.getResultList(getSession()));
            }
        });

        model.setDevelopConditionsListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (null == model.getGroup() && (null == finalExtract || model.isParagraphOnlyOneInTheOrder()))
                    return ListResult.getEmpty();

                if (!model.isParagraphOnlyOneInTheOrder() && null != finalExtract)
                {
                    List<DevelopCondition> result = new ArrayList<>();
                    result.add(finalExtract.getEducationOrgUnitNew().getDevelopCondition());
                    return new ListResult<>(result);
                }

                MQBuilder subBuilder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou", new String[]{EducationOrgUnit.L_DEVELOP_CONDITION + "." + DevelopCondition.P_ID});
                subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getGroup().getEducationOrgUnit().getEducationLevelHighSchool()));
                subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnitNew()));
                subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnitNew()));
                subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.P_USED, Boolean.TRUE));
                subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.P_ALLOW_STUDENTS, Boolean.TRUE));
                subBuilder.setNeedDistinct(true);

                MQBuilder builder = new MQBuilder(DevelopCondition.ENTITY_CLASS, "df");
                builder.add(MQExpression.in("df", DevelopCondition.P_ID, subBuilder));
                builder.addOrder("df", DevelopCondition.P_CODE);

                return new ListResult<>(builder.getResultList(getSession()));
            }
        });

        model.setDevelopPeriodsListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if ((null == model.getGroup() || null == model.getDevelopConditionNew()) && (null == finalExtract || model.isParagraphOnlyOneInTheOrder()))
                    return ListResult.getEmpty();

                if (!model.isParagraphOnlyOneInTheOrder() && null != finalExtract)
                {
                    List<DevelopPeriod> result = new ArrayList<>();
                    result.add(finalExtract.getEducationOrgUnitNew().getDevelopPeriod());
                    return new ListResult<>(result);
                }

                MQBuilder subBuilder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou", new String[]{EducationOrgUnit.L_DEVELOP_PERIOD + "." + DevelopPeriod.P_ID});
                subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getGroup().getEducationOrgUnit().getEducationLevelHighSchool()));
                subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnitNew()));
                subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnitNew()));
                subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopConditionNew()));
                subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.P_USED, Boolean.TRUE));
                subBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.P_ALLOW_STUDENTS, Boolean.TRUE));
                subBuilder.setNeedDistinct(true);

                MQBuilder builder = new MQBuilder(DevelopPeriod.ENTITY_CLASS, "dp");
                builder.add(MQExpression.in("dp", DevelopPeriod.P_ID, subBuilder));
                builder.addOrder("dp", DevelopPeriod.P_CODE);

                return new ListResult<>(builder.getResultList(getSession()));
            }
        });


    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
    }

    @Override
    protected TerritorialTransferStuListExtract createNewInstance(Model model)
    {
        return new TerritorialTransferStuListExtract();
    }

    @Override
    protected void fillExtract(TerritorialTransferStuListExtract extract, Student student, Model model)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        if (student.getEducationOrgUnit().getFormativeOrgUnit().getId().equals(model.getFormativeOrgUnitNew().getId()))
        {
            if ((student.getEducationOrgUnit().getTerritorialOrgUnit() != null) && (model.getTerritorialOrgUnitNew() != null))
            {
                if (student.getEducationOrgUnit().getTerritorialOrgUnit().getId().equals(model.getTerritorialOrgUnitNew().getId()))
                {
                    errCollector.add("Нельзя перевести студентов на указанные формирующее и территориальное подразделение, т.к. они совпадают с их текущими данными.", "territorialOrgUnit");
                    throw new ApplicationException("Нельзя перевести студентов на указанные формирующее и территориальное подразделение, т.к. они совпадают с их текущими данными.");
                }
            }
            else
            {
                if ((student.getEducationOrgUnit().getTerritorialOrgUnit() == null) && (model.getTerritorialOrgUnitNew() == null))
                {
                    errCollector.add("Нельзя перевести студентов на указанные формирующее и территориальное подразделение, т.к. они совпадают с их текущими данными.", "territorialOrgUnit");
                    throw new ApplicationException("Нельзя перевести студентов на указанные формирующее и территориальное подразделение, т.к. они совпадают с их текущими данными.");
                }
            }
        }

        MQBuilder builder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou");
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnitNew()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnitNew()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, student.getEducationOrgUnit().getEducationLevelHighSchool()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_FORM, student.getEducationOrgUnit().getDevelopForm()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_TECH, student.getEducationOrgUnit().getDevelopTech()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopConditionNew()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_PERIOD, model.getDevelopPeriodNew()));
        List<EducationOrgUnit> acceptableEduOrgUnitsList = builder.getResultList(getSession());

        if (acceptableEduOrgUnitsList.isEmpty())
        {
            EducationOrgUnit educationOrgUnit = new EducationOrgUnit();
            educationOrgUnit.update(student.getEducationOrgUnit());
            educationOrgUnit.setFormativeOrgUnit(model.getFormativeOrgUnitNew());
            educationOrgUnit.setTerritorialOrgUnit(model.getTerritorialOrgUnitNew());
            educationOrgUnit.setDevelopCondition(model.getDevelopConditionNew());
            educationOrgUnit.setDevelopPeriod(model.getDevelopPeriodNew());
            errCollector.add(CommonListExtractUtil.buildStudentTransferError("Студент " + student.getPerson().getFullFio() + " не может быть переведен", educationOrgUnit));
        }
        else extract.setEducationOrgUnitNew(acceptableEduOrgUnitsList.get(0));

        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setEducationOrgUnitOld(student.getEducationOrgUnit());
        extract.setTransferDate(model.getTransferDate());

        // TODO: только если группа переводится, а не создается заново
        Group group = model.getGroup();
        MQBuilder grpBuilder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou");
        grpBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnitNew()));
        grpBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnitNew()));
        grpBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, group.getEducationOrgUnit().getEducationLevelHighSchool()));
        grpBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_FORM, group.getEducationOrgUnit().getDevelopForm()));
        grpBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_TECH, group.getEducationOrgUnit().getDevelopTech()));
        grpBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopConditionNew()));
        grpBuilder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_PERIOD, model.getDevelopPeriodNew()));
        List<EducationOrgUnit> acceptableGrpEduOrgUnitsList = grpBuilder.getResultList(getSession());

        if (acceptableGrpEduOrgUnitsList.isEmpty())
        {
            EducationOrgUnit educationOrgUnit = new EducationOrgUnit();
            educationOrgUnit.update(group.getEducationOrgUnit());
            educationOrgUnit.setFormativeOrgUnit(model.getFormativeOrgUnitNew());
            educationOrgUnit.setTerritorialOrgUnit(model.getTerritorialOrgUnitNew());
            educationOrgUnit.setDevelopCondition(model.getDevelopConditionNew());
            educationOrgUnit.setDevelopPeriod(model.getDevelopPeriodNew());
            errCollector.add(CommonListExtractUtil.buildStudentTransferError("Группа " + group.getTitle() + " не может быть переведена", educationOrgUnit));
        }
        else
        {
            extract.setGroupEducationOrgUnitOld(group.getEducationOrgUnit());
            extract.setGroupEducationOrgUnitNew(acceptableGrpEduOrgUnitsList.get(0));
        }
    }
}