package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StuExtractToTermRelation;
import ru.tandemservice.uni.entity.catalog.Term;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь выписки с семестром
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StuExtractToTermRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.StuExtractToTermRelation";
    public static final String ENTITY_NAME = "stuExtractToTermRelation";
    public static final int VERSION_HASH = -193515664;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String L_TERM = "term";

    private ModularStudentExtract _extract;     // Выписка из сборного (индивидуального) приказа по студентам
    private Term _term;     // Семестр

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного (индивидуального) приказа по студентам.
     */
    public ModularStudentExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка из сборного (индивидуального) приказа по студентам.
     */
    public void setExtract(ModularStudentExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Семестр.
     */
    public Term getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр.
     */
    public void setTerm(Term term)
    {
        dirty(_term, term);
        _term = term;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StuExtractToTermRelationGen)
        {
            setExtract(((StuExtractToTermRelation)another).getExtract());
            setTerm(((StuExtractToTermRelation)another).getTerm());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StuExtractToTermRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StuExtractToTermRelation.class;
        }

        public T newInstance()
        {
            return (T) new StuExtractToTermRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "term":
                    return obj.getTerm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((ModularStudentExtract) value);
                    return;
                case "term":
                    obj.setTerm((Term) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "term":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "term":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return ModularStudentExtract.class;
                case "term":
                    return Term.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StuExtractToTermRelation> _dslPath = new Path<StuExtractToTermRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StuExtractToTermRelation");
    }
            

    /**
     * @return Выписка из сборного (индивидуального) приказа по студентам.
     * @see ru.tandemservice.movestudent.entity.StuExtractToTermRelation#getExtract()
     */
    public static ModularStudentExtract.Path<ModularStudentExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Семестр.
     * @see ru.tandemservice.movestudent.entity.StuExtractToTermRelation#getTerm()
     */
    public static Term.Path<Term> term()
    {
        return _dslPath.term();
    }

    public static class Path<E extends StuExtractToTermRelation> extends EntityPath<E>
    {
        private ModularStudentExtract.Path<ModularStudentExtract> _extract;
        private Term.Path<Term> _term;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного (индивидуального) приказа по студентам.
     * @see ru.tandemservice.movestudent.entity.StuExtractToTermRelation#getExtract()
     */
        public ModularStudentExtract.Path<ModularStudentExtract> extract()
        {
            if(_extract == null )
                _extract = new ModularStudentExtract.Path<ModularStudentExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Семестр.
     * @see ru.tandemservice.movestudent.entity.StuExtractToTermRelation#getTerm()
     */
        public Term.Path<Term> term()
        {
            if(_term == null )
                _term = new Term.Path<Term>(L_TERM, this);
            return _term;
        }

        public Class getEntityClass()
        {
            return StuExtractToTermRelation.class;
        }

        public String getEntityName()
        {
            return "stuExtractToTermRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
