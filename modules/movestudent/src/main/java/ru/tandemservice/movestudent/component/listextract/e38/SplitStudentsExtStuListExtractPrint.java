/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e38;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.IStudentListParagraphPrintFormatter;
import ru.tandemservice.movestudent.component.listextract.e25.SplitStudentsGroupStuListExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.SplitStudentsExtStuListExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author ListExtractComponentGenerator
 * @since 12.12.2012
 */
public class SplitStudentsExtStuListExtractPrint implements IPrintFormCreator<SplitStudentsExtStuListExtract>, IListParagraphPrintFormCreator<SplitStudentsExtStuListExtract>, IStudentListParagraphPrintFormatter
{
    @Override
    public RtfDocument createPrintForm(byte[] template, SplitStudentsExtStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, SplitStudentsExtStuListExtract firstExtract)
    {
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract)
                .put("compensationTypeStr", firstExtract.getCompensationTypeStr());

        CommonExtractPrint.initDevelopForm(injectModifier, firstExtract.getEducationOrgUnitOld().getDevelopForm(), "");

        injectModifier.put("paragraphNumber", String.valueOf(paragraph.getNumber()));
        injectModifier.put("parNumberConditional", paragraph.getOrder().getParagraphCount() > 1 ? String.valueOf(paragraph.getNumber()) + ". " : "");
        injectModifier.put("paragraphNumberConditional", paragraph.getOrder().getParagraphCount() > 1 ? String.valueOf(paragraph.getNumber()) + ". " : "");

        Student firstStudent = firstExtract.getEntity();

        CommonListExtractPrint.injectCompensationType(injectModifier, firstStudent.getCompensationType(), "", false);
        CommonExtractPrint.initOrgUnit(injectModifier, firstStudent.getEducationOrgUnit().getFormativeOrgUnit(), "", "");
        CommonExtractPrint.initOrgUnit(injectModifier, firstStudent.getEducationOrgUnit(), "formativeOrgUnitStr", "");

        Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(firstExtract.getEducationOrgUnitOld().getEducationLevelHighSchool().getEducationLevel());
        String qualificationCode = null != qualification ? qualification.getCode() : null;
        if (null != qualificationCode && !SplitStudentsGroupStuListExtractPrint.PROCESSED_QUALIFICATION_CODES.contains(qualificationCode))
            qualificationCode = null;

        for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
        {
            injectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[i]);
            injectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i) + "P", SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_PLURAL_CASES_ARRAY.get(qualificationCode)[i]);
        }
        injectModifier.put("specialization", firstExtract.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle());
        injectModifier.put("specializationNew", firstExtract.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle());
        injectModifier.put("profileNew", firstExtract.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle());

        StringBuilder fefuEducationStrProfileNew = new StringBuilder();
        StringBuilder fefuEducationStrProfileNew_I = new StringBuilder();

        fefuEducationStrProfileNew.append(SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[0]).append(" «");
        fefuEducationStrProfileNew.append(firstExtract.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle()).append("»");
        injectModifier.put("fefuEducationStrProfileNew", StringUtils.capitalize(fefuEducationStrProfileNew.toString()));

        fefuEducationStrProfileNew_I.append(SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[4]).append(" «");
        fefuEducationStrProfileNew_I.append(firstExtract.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle()).append("»");
        injectModifier.put("fefuEducationStrProfileNew_I", fefuEducationStrProfileNew_I.toString());

        EducationLevels speciality = EducationOrgUnitUtil.getParentLevel(firstExtract.getEducationOrgUnitOld().getEducationLevelHighSchool());
        CommonExtractPrint.modifyEducationStr(injectModifier, speciality, new String[]{"fefuEducationStrDirectionOld"}, false);

        CommonExtractPrint.initFefuGroup(injectModifier, "groupInternal_G", firstExtract.getEntity().getGroup(), firstExtract.getEducationOrgUnitOld().getDevelopForm(), " группы ");

        UniRtfUtil.initEducationType(injectModifier, firstExtract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool(), "");
        CommonExtractPrint.initOrgUnit(injectModifier, firstExtract.getEntity().getEducationOrgUnit().getFormativeOrgUnit(), "orgUnit", "");
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(injectModifier, firstExtract.getEducationOrgUnitOld(), CommonListOrderPrint.getEducationBaseText(firstExtract.getEntity().getGroup()), "fefuShortFastExtendedOptionalText");

        return injectModifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, SplitStudentsExtStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, SplitStudentsExtStuListExtract firstExtract)
    {
        Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(firstExtract.getEducationOrgUnitOld().getEducationLevelHighSchool().getEducationLevel());
        String qualificationCode = null != qualification ? qualification.getCode() : null;
        if (null != qualificationCode && !SplitStudentsGroupStuListExtractPrint.PROCESSED_QUALIFICATION_CODES.contains(qualificationCode))
        {
            //qualification = null;
            qualificationCode = null;
        }

        for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
        {
            modifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[i]);
            modifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i) + "P", SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_PLURAL_CASES_ARRAY.get(qualificationCode)[i]);
        }
    }

    @Override
    public String formatSingleStudent(Student student, int extractNumber)
    {
        StringBuilder buffer = new StringBuilder();
        buffer.append("\\par ").append(extractNumber).append(".  ");
        buffer.append(student.getPerson().getFullFio());
        return buffer.toString();
    }
}