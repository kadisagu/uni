/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.dao;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.utils.system.StudentExcludeOrderData;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unimove.entity.ICustomOrderAction;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author vip_delete
 * @since 18.10.2008
 */
public interface IMoveStudentDao
{
    String MOVE_STUDENT_DAO_BEAN_NAME = "moveStudentDao";

    SpringBeanCache<IMoveStudentDao> instance = new SpringBeanCache<>(MOVE_STUDENT_DAO_BEAN_NAME);

    /**
     * Получение кода состояния для какого-то объекта. Метод нужен, чтобы не делать refresh приказа или выписки при сабмите формы редактирования,
     * к примеру, чтобы новые данные не потерялись, но и актуальное состояние проверить.
     *
     * @param objectId объект
     * @return код состояния
     */
    String getObjectStateCode(Class<? extends IEntity> clazz, Long objectId);

    /**
     * Получение кода состояния приказа. Метод нужен, чтобы не делать refresh приказа при сабмите формы редактирования, к примеру, чтобы новые данные не потерялись,
     * но и актуальное состояние проверить.
     *
     * @param orderId приказ
     * @return код состояния приказа
     */
    String getOrderStateCode(Long orderId);

    /**
     * Получение кода состояния выписки. Метод нужен, чтобы не делать refresh выписки при сабмите формы редактирования, к примеру, чтобы новые данные не потерялись,
     * но и актуальное состояние проверить.
     *
     * @param extractId выписка
     * @return код состояния выписки
     */
    String getExtractStateCode(Long extractId);

    /**
     * Получение списка типов практик студентов (учебная/производственная).
     * Для продукта берется из проперти-файла. Для ДВФУ - из справочника.
     */
    List<String> getStudentPracticeTypes();

    /**
     * Метод определяет, можно ли для указанного типа выписки создавать выписку на студента вне группы.
     */
    boolean canBeCreatedExtractForStudentWithoutGroup(StudentExtractType extractType);

    /**
     * Получение списка видов (или подвидов) практик студентов.
     * Для продукта берется из проперти-файла. Для ДВФУ - из справочника.
     */
    List<String> getStudentPracticeKinds();

    String getFullPracticeKind(String practiceType, String practiceKind);

    String extractPracticeKind(String fullPracticeKind);

    String extractPracticeType(String fullPracticeKind);

    String getDeclinablePracticeType(String type, GrammaCase grammaCase);

    String getDeclinableFullPracticeKind(String fullPracticeKind, GrammaCase grammaCase);

    boolean isOrgUnitMoveStudentTabVisible(OrgUnit orgUnit);

    /**
     * Определяет, может ли содержать списочный приказ указанного типа параграфы разных типов
     *
     * @param orderType тип приказа
     * @return есть ли
     */
    boolean isVariousExtractTypeInOrder(StudentExtractType orderType);

    /**
     * Определяет, есть ли для указанного типа списочного приказа комнонент множественного добавления параграфов
     *
     * @param orderType тип списочного приказа
     * @return есть ли
     */
    boolean isMultipleAddParagraphsOrder(StudentExtractType orderType);

    /**
     * Проверка того, можно ли создавать новые приказы на студента.
     * Если у студента есть проекты приказа, не включенные в приказ, либо студент включен в какой-либо приказ, находящийся в
     * состоянии отличном от "Проведено" или "Отклонен", то студента создавать новые приказы нельзя и метод вернет false.
     * Иначе вернет true.
     *
     * @param student студент
     * @return true, если студенту можно создавать новые выписки. Иначе - false.
     */
    boolean isMoveAccessible(Student student);

    /**
     * Проверка того, можно ли создавать новые приказы на студента.
     * Если у студента есть проекты приказа, не включенные в приказ, либо студент включен в какой-либо приказ, находящийся в
     * состоянии отличном от "Проведено" или "Отклонен", то студента создавать новые приказы нельзя и метод вернет false.
     * Иначе вернет true.
     *
     * @param student            студент
     * @param excludeParagraphId идентификатор параграфа, который нужно исключить из проверки. Необязательный параметр.
     * @return true, если студенту можно создавать новые выписки. Иначе - false.
     */
    boolean isMoveAccessible(@NotNull Student student, @Nullable Long excludeParagraphId);

    // список всех выписок из сборного приказа. сортировка по названию
    List<StudentExtractType> getModularStudentExtractTypeList();

    // список выписок по заданному фильтру из сборного приказа. сортировка по названию
    ListResult<StudentExtractType> getModularStudentExtractTypeList(String filter, String parentExtractTypeCode);

    // получает идентификатор последней выписки студента в проведенном состоянии
    Long getLastCommittedExtractId(Long studentId);

    // получает печатную форму выписки и сохраняет ее в базу.
    // предыдущая сохраненная форма, если она была, переписывается
    void saveModularExtractText(ModularStudentExtract extract, int textCode);

    // получает печатную форму приказа и сохраняет ее в базу.
    // предыдущая сохраненная форма, если она была, переписывается
    void saveModularOrderText(StudentModularOrder order);

    // получает печатную форму выписки и сохраняет ее в базу.
    // предыдущая сохраненная форма, если она была, переписывается
    void saveListExtractText(ListStudentExtract extract, int textCode);

    // получает печатную форму приказа и сохраняет ее в базу.
    // предыдущая сохраненная форма, если она была, переписывается
    void saveListOrderText(StudentListOrder order);

    boolean isCustomOrderType(StudentExtractType extractType);

    StudentExtractTextRelation getExtractTextRelation(Long extractId, int textCode);

    StudentOrderTextRelation getOrderTextRelation(Long orderId);

    /**
     * Генерация печатного документа выписки или приказа по шаблону
     *
     * @param object        объект печати (выписка или приказ)
     * @param extractType   тип выписки/приказа
     * @param templateIndex индекс элемента справочника (1 - приказ, 2 - выписка)
     * @param printBeanName имя бина контроллера печати
     * @return сгенерированный документ
     */
    <T> byte[] createPrintDocumentFromTemplate(T object, StudentExtractType extractType, int templateIndex, String printBeanName);

    // удаляет выписку из параграфа списочного приказа
    void deleteListExtract(Long extractId);

    // получает шаблон выписки указанного типа и индекса.
    // Индекс - это MoveStudentDefines.EXTRACT_TEXT_CODE или MoveStudentDefines.EXTRACT_IN_ORDER_TEXT_CODE
    // то есть указываем брать шаблон для печати выписки или шаблон для создания сборного приказа
    // текст выписки в сборном приказе может отличаться
    byte[] getTemplate(final StudentExtractType extractType, final int templateIndex);

    // возвращает список задолженностей студента, подлежащих ликвидации
    // для указанной выписки
    List<StuExtractToDebtRelation> getStudentExtractDebtsList(ModularStudentExtract extract);

    /**
     * Возвращает родительский уровень образования ОУ для указанной специализации,
     * или сам уровень указанный уровень образования, в зависимости от его места в иерархии
     *
     * @deprecated В следующих версиях метод будет удален, вместо него необходимо использовать EducationOrgUnitUtil.getParentLevel
     */
    @Deprecated
    EducationLevels getSpeciality(EducationLevelsHighSchool eduLevel);

    // проверка на уникальность номера приказа в рамках календарного года
    void checkOrderNumber(AbstractStudentOrder order);

    /**
     * Возвращает тип первой попавшейся выписки в параграфе. По парадигме модуля в параграфе могут быть выписки только одного типа,
     * а значит все выписки этого типа.
     *
     * @param paragraphId идентификатор параграфа
     * @return тип первой попавшейся выписки данного параграфа
     */
    StudentExtractType getAnyExtractTypeByParagraph(Long paragraphId);

    /**
     * Получение списка выписок по идентификаторам. Метод работает быстрее, чем простой запрос к абстрактной выписке,
     * т.к. разбивает выписки на типы и запросы делает к конечным классам.
     *
     * @param extractIds идентификаторы выписок
     * @param checkClass базовый класс для выписок
     * @return список выписок
     */
    <T extends AbstractStudentExtract> List<T> getExtractListByIds(Collection<Long> extractIds, Class<T> checkClass);

    /**
     * Возвращает первую выписку из первого параграфа приказа в виде прокси.
     *
     * @param orderId идентификатор приказа
     * @return выписка (в виде прокси)
     */
    <T extends AbstractStudentExtract> T getOrderFirstExtract(Long orderId);

    /**
     * Возвращает первую выписку из первого параграфа приказа.
     *
     * @param orderId   идентификатор приказа
     * @param onlyProxy вернуть в виде прокси
     * @return выписка или прокси
     */
    <T extends AbstractStudentExtract> T getOrderFirstExtract(Long orderId, boolean onlyProxy);

    /**
     * Возвращает первую (сортировка по номеру) выписку в параграфе
     *
     * @param paragraphId идентификатор параграфа
     * @param onlyProxy   вернуть сущность целиком, а не в виде прокси
     * @return выписка в виде прокси
     */
    <T extends AbstractStudentExtract> T getParagraphFirstExtract(Long paragraphId, boolean onlyProxy);

    /**
     * Количество выписок в приказе. Если orderId == null, то вернет 0.
     *
     * @param orderId идентификатор приказа
     * @return количество выписок в приказе
     */
    int getExtractCount(Long orderId);

    /**
     * Берутся все выписки приказа (<tt>order</tt>), {@link ru.tandemservice.movestudent.utils.MoveStudentUtils.ModularStudentExtractComparator сортируются}<p>
     * и в соответствии с сортировкой соответствующим параграфам выставляются порядковые номера.
     *
     * @param order - приказ  в котором производится нумерация выписок
     */
    void updateParagraphsNumber(StudentModularOrder order);

    /**
     * Возвращает полный список выписок, включенных в приказ
     *
     * @param order приказ, для которого требуется получить список выписок
     * @param sort сортировать ли по номеру выписки и номеру параграфа
     * @return список выписок
     */
    <T extends AbstractStudentExtract> List<T> getOrderExtractList(AbstractStudentOrder order, boolean sort);

    /**
     * Получение списка выписок параграфа
     *
     * @param paragraphId идентификатор параграфа
     * @param sort        сортировать ли выписки по номеру
     * @return список выписок
     */
    <T extends AbstractStudentExtract> List<T> getExtractListByParagraph(Long paragraphId, boolean sort);

    /**
     * Получение списка студентов из параграфа приказа. Сортировка по номеру выписки
     *
     * @param paragraphId идентификатор параграфа
     * @return список студентов
     */
    List<Student> getStudentsByParagraph(Long paragraphId);

    /**
     * Получение списка ФИО студентов из параграфа приказа. Сортировка по номеру выписки
     *
     * @param paragraphId идентификатор параграфа
     * @return список ФИО студентов
     */
    List<String> getStudentsFioByParagraph(Long paragraphId);

    /**
     * Возвращает сет идентификаторов выписок, печать которых невозможна по одной из следующих причин:
     * 1. для проведенной выписки не сохранена печатная форма
     * 2. для непроведенной выписки не определен шаблон печатной формы выписки
     * 3. для непроведенной выписки не определен контроллер печати выписки
     *
     * @param extractsList         - список выписок, для которых проверяется возможность печати
     * @param extractTemplateIndex - индекс печатного шаблона выписки
     * @return - список идентификаторов выписок, печать которых неевозможна.
     */
    Set<Long> getDisabledFormPrintingExtracts(List<AbstractStudentExtract> extractsList, int extractTemplateIndex);

    /**
     * Возвращает список семестров, сохраненных в выписке
     *
     * @param extract Выписка
     * @return семестры
     */
    List<Term> getExtractTerms(ModularStudentExtract extract);

    /**
     * Возвращает информацию по последнему приказу об отчислении студента
     *
     * @param student Студент
     * @return Информация по приказу
     */
    StudentExcludeOrderData getStudentExcludeOrderData(Student student);

    /**
     * Возвращает список идентификаторов специализаций (профилей) для указанной специальности.
     * Если передаётся не специальность, а специализация, то берется родительский элемент и ищётся дочерние для него.
     *
     * @param eduLevelsHighSchool - НП
     * @param formativeOrgUnit    - формиирующее подразделение для фильтрации
     * @param territorialOrgUnit  - территориальное подразделение для фильтрации
     * @return - список идентификаторов специализаций (профилей).
     * @deprecated use DQL
     */
    Collection<Long> getChildSpecializationIDs(EducationLevelsHighSchool eduLevelsHighSchool, OrgUnit formativeOrgUnit, OrgUnit territorialOrgUnit);

    /**
     * Возвращает true, если данный тип приказов помечен в настройке "Правила формированяи сборных приказов"
     * как формируемый индивидуально.
     *
     * @param extractType - тип выписки
     * @return - true, если приказ является индивидуальным
     */
    boolean isModularOrderIndividual(StudentExtractType extractType);

    /**
     * Возвращает true, если сборный приказ содержит всего одну выписку и тип этой выписки
     * помечен в настройке "Правила формированяи сборных приказов" как формируемый индивидуально.
     *
     * @param order - сборный приказ по студентам
     * @return - true, если приказ является индивидуальным (содержит одну выписку и тип этой выписки помечен как индивидуальный).
     */
    boolean isModularOrderIndividual(StudentModularOrder order);

    /**
     * Возвращает список оснований выписки
     *
     * @param extract - выписка из сборного приказа
     * @return - список связанных с выпиской оснований.
     */
    List<StuExtractToBasicRelation> getPrioritySortedExtractBasicsList(ModularStudentExtract extract);

    /**
     * Возвращает список оснований списочного приказа по студентам
     *
     * @param order - списочный приказ по студентам
     * @return - список связанных с выпиской оснований.
     */
    List<StuListOrderToBasicRelation> getPrioritySortedListOrderBasicsList(StudentListOrder order);

    /**
     * Возвращает список оснований выписки
     *
     * @param extract - выписка из прочего приказа
     * @return - список связанных с выпиской оснований.
     */
    List<StuOtherOrderToBasicRelation> getPrioritySortedExtractBasicsList(OtherStudentExtract extract);

    /**
     * Возвращает набор идентификаторов выписок из прочих приказов, для которых невозможна печать документа.
     *
     * @param extractsList - список выписок из прочих приказов
     * @return - набор идентификаторов непечатных выписок.
     */
    Set<Long> getPrintFormAttachedOtherOrders(List<OtherStudentExtract> extractsList);

    /**
     * Получение названия компонента для кастомного действия
     *
     * @param action катомной действие
     * @return пакет компонента (без AddEdit или Pub - их надо дописывать)
     */
    String getCustomActionComponent(ICustomOrderAction action);

    /**
     * Получение из спринга списка компонентов кастомных действий.
     *
     * @param isListOrder true - для списочных приказов, false - для сборных
     * @return список компонентов в том порядке, который задан в спринге
     */
    Map<String, String> getPossibleCustomActionComponents(boolean isListOrder);

    /**
     * Получение списка примененных к выписке кастомных действий. Сортировка в порядке перечисления в спринге.
     *
     * @param extractId идентификатор выписки
     * @return мапа сущностей и соответствующих названий компонентов
     */
    Map<ICustomOrderAction, String> getCustomActions(Long extractId, boolean isListOrder);

    /**
     * Определяет, есть ли у типов прочих приказов третий уровень вложенности
     */
    boolean hasThirdLevelOtherOrders();

    /**
     * Добавление кастомных условий в DQLSelectBuilder
     * для выбора групп
     */
    void addCustomConditionToGroupSelectBuilder(DQLSelectBuilder builder, String alias);

    /**
     *  Добавление кастомных условий в DQLSelectBuilder для выбора территориального подразделения
     */
    void addFormativeOrgUnitToBuilder(DQLSelectBuilder builder, OrgUnit formativeOrgUnit);

    IMoveStudentInjectModifier getScriptInjectModifier();

    /**
     * Возвращает форматированный вывод научного руководителя, с указанными в выписке регалиями (степени и звания)
     *
     * @param extract - выписка
     * @param advisor - научный руководитель
     * @return - форматированный вывод научного руководителя.
     */
    String getStuListExtractScientificAdvisorStr(AbstractStudentExtract extract, PpsEntry advisor);
}