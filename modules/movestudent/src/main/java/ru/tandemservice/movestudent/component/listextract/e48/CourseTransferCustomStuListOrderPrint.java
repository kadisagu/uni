/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e48;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractUtil;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.CourseTransferCustomStuListExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.movestudent.entity.gen.AbstractStudentExtractGen;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Ekaterina Zvereva
 * @since 06.02.2017
 */
public class CourseTransferCustomStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    public static final String SUBPARAGRAPH_LABEL = "SUBPARAGRAPHS";

    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<CourseTransferCustomStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));

        if (!extracts.isEmpty())
        {
            injectModifier.put("lastParNumber", String.valueOf(order.getParagraphCount() + 1));
        }

        if (null != order.getResponsibleEmpl())
        {
            OrgUnit orgUnit = order.getResponsibleEmpl().getOrgUnit();
            Post post = order.getResponsibleEmpl().getPostRelation().getPostBoundedWithQGandQL().getPost();
            String respFioA = CommonExtractUtil.getModifiedFio(order.getResponsibleEmpl().getPerson(), GrammaCase.ACCUSATIVE, true);

            injectModifier.put("controllerPost_A", null != post.getAccusativeCaseTitle() ? post.getAccusativeCaseTitle().toLowerCase() : post.getTitle().toLowerCase());
            injectModifier.put("controllerOrgUnit_G", null != orgUnit.getGenitiveCaseTitle() ? orgUnit.getGenitiveCaseTitle() : orgUnit.getTitle());
            injectModifier.put("controllerFio_A", respFioA);
        } else
        {
            injectModifier.put("controllerPost_A", "");
            injectModifier.put("controllerOrgUnit_G", "");
            injectModifier.put("controllerFio_A", "__________________________________________________________________________________________________________________");
        }

        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, order);

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }


    protected void injectParagraphs(RtfDocument document, StudentListOrder order)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            int parNumber = 0;
            List<IRtfElement> parList = new ArrayList<>();
            // Получаем шаблон параграфа
            byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.COURSE_TRANSF_CUSTOM_LIST_EXTRACT), 1);
            RtfDocument template = new RtfReader().read(paragraphTemplate);
            // Шаблон подпараграфа
            byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.COURSE_TRANSF_CUSTOM_LIST_EXTRACT), 3);
            RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

            for (StudentListParagraph paragraph : (List<StudentListParagraph>) order.getParagraphList())
            {
                // Вносим необходимые метки
                final CourseTransferCustomStuListExtract firstExtract = (CourseTransferCustomStuListExtract) paragraph.getFirstExtract();
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(firstExtract.getParagraph(), firstExtract);

                Student firstStudent = firstExtract.getEntity();
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstStudent.getEducationOrgUnit().getFormativeOrgUnit(), "orderOrgUnit", "");

                paragraphInjectModifier.put("courseOld", firstExtract.getCourseOld().getTitle());
                paragraphInjectModifier.put("courseNew", firstExtract.getCourseNew().getTitle());
                customModifier.listParagraphModifier(paragraphInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                RtfDocument paragraphRtfTemplate = template.getClone();
                paragraphInjectModifier.modify(paragraphRtfTemplate);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraphRtfTemplate);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraphRtfTemplate, (List<CourseTransferCustomStuListExtract>) paragraph.getExtractList(), paragraphPart.getClone());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphRtfTemplate.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraph, List<CourseTransferCustomStuListExtract> extracts, RtfDocument subParagraphTemplate)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, SUBPARAGRAPH_LABEL);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Map<CoreCollectionUtils.Pair<CompensationType, Boolean>, List<CourseTransferCustomStuListExtract>> subParagraphsMap = new HashMap<>();
            extracts.stream().forEach(e->
                          SafeMap.safeGet(subParagraphsMap, new CoreCollectionUtils.Pair<>(e.getEntity().getCompensationType(), e.isConditionalTransfer()),
                                          key->new ArrayList<>()).add(e));

            List<IRtfElement> parList = new ArrayList<>();
            List<CoreCollectionUtils.Pair<CompensationType, Boolean>> keySet = subParagraphsMap.keySet().stream()
                    .sorted(Comparator.comparing((CoreCollectionUtils.Pair<CompensationType, Boolean>e)->e.getX().getCode()).thenComparing(CoreCollectionUtils.Pair::getY)).collect(Collectors.toList());

            int subParCount=0;
            for (CoreCollectionUtils.Pair<CompensationType, Boolean> item: keySet)
            {
                RtfDocument subParagraph = subParagraphTemplate.getClone();

                // Вносим необходимые метки
                final CourseTransferCustomStuListExtract firstExtract = subParagraphsMap.get(item).get(0);
                RtfInjectModifier paragraphPartInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, firstExtract);
                CommonListExtractPrint.injectCompensationType(paragraphPartInjectModifier, firstExtract.getEntity().getCompensationType(), "", false);
                paragraphPartInjectModifier.put("subParNumber", String.valueOf(firstExtract.getParagraph().getNumber()) + "." + String.valueOf(++subParCount) + ". ");
                paragraphPartInjectModifier.put("compensationTypeMirea", firstExtract.getEntity().getCompensationType().isBudget()?
                        "на местах, финансируемых за счет бюджетных ассигнований федерального бюджета"
                        : "на местах с оплатой стоимости обучения физическими лицами");

                paragraphPartInjectModifier.put("conditionalTransfer", firstExtract.isConditionalTransfer()?
                                                "установить срок ликвидации академических задолженностей до ":
                                                "полностью выполнивших учебный план");
                paragraphPartInjectModifier.put("deadlineDateStr", firstExtract.isConditionalTransfer()? DateFormatter.DEFAULT_DATE_FORMATTER.format(firstExtract.getDeadlineDate()): "");


                List<Student> studentList = subParagraphsMap.get(item).stream().map(AbstractStudentExtractGen::getEntity).collect(Collectors.toList());
                ArrayList<String[]> resultList = new ArrayList<>();
                studentList.stream().sorted(Comparator.comparing((Student s) -> s.getGroup().getTitle()).thenComparing(Student.FULL_FIO_AND_ID_COMPARATOR))
                        .forEach(e-> resultList.add(new String[] {e.getFullFio(), e.getPersonalNumber(), e.getGroup().getTitle()}));


                customModifier.listParagraphModifier(paragraphPartInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                paragraphPartInjectModifier.modify(subParagraph);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                tableModifier.put("STUDENT_LIST_T", resultList.toArray(new String[resultList.size()][]));
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(subParagraph);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(subParagraph.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}