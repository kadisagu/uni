package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.SocGrantAssignStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О назначении социальной стипендии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SocGrantAssignStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.SocGrantAssignStuExtract";
    public static final String ENTITY_NAME = "socGrantAssignStuExtract";
    public static final int VERSION_HASH = 467669114;
    private static IEntityMeta ENTITY_META;

    public static final String P_START_DATE = "startDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_GRANT_SIZE = "grantSize";
    public static final String P_PREV_BEGIN_DATE = "prevBeginDate";
    public static final String P_PREV_END_DATE = "prevEndDate";

    private Date _startDate;     // Дата начала выплаты стипендии
    private Date _endDate;     // Дата окончания выплаты стипендии
    private Double _grantSize;     // Размер стипендии
    private Date _prevBeginDate;     // Дата начала выплаты социальной стипендии по предыдущему приказу
    private Date _prevEndDate;     // Дата окончания выплаты социальной стипендии по предыдущему приказу

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата начала выплаты стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Дата начала выплаты стипендии. Свойство не может быть null.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Дата окончания выплаты стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания выплаты стипендии. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Размер стипендии.
     */
    public Double getGrantSize()
    {
        return _grantSize;
    }

    /**
     * @param grantSize Размер стипендии.
     */
    public void setGrantSize(Double grantSize)
    {
        dirty(_grantSize, grantSize);
        _grantSize = grantSize;
    }

    /**
     * @return Дата начала выплаты социальной стипендии по предыдущему приказу.
     */
    public Date getPrevBeginDate()
    {
        return _prevBeginDate;
    }

    /**
     * @param prevBeginDate Дата начала выплаты социальной стипендии по предыдущему приказу.
     */
    public void setPrevBeginDate(Date prevBeginDate)
    {
        dirty(_prevBeginDate, prevBeginDate);
        _prevBeginDate = prevBeginDate;
    }

    /**
     * @return Дата окончания выплаты социальной стипендии по предыдущему приказу.
     */
    public Date getPrevEndDate()
    {
        return _prevEndDate;
    }

    /**
     * @param prevEndDate Дата окончания выплаты социальной стипендии по предыдущему приказу.
     */
    public void setPrevEndDate(Date prevEndDate)
    {
        dirty(_prevEndDate, prevEndDate);
        _prevEndDate = prevEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SocGrantAssignStuExtractGen)
        {
            setStartDate(((SocGrantAssignStuExtract)another).getStartDate());
            setEndDate(((SocGrantAssignStuExtract)another).getEndDate());
            setGrantSize(((SocGrantAssignStuExtract)another).getGrantSize());
            setPrevBeginDate(((SocGrantAssignStuExtract)another).getPrevBeginDate());
            setPrevEndDate(((SocGrantAssignStuExtract)another).getPrevEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SocGrantAssignStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SocGrantAssignStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new SocGrantAssignStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "startDate":
                    return obj.getStartDate();
                case "endDate":
                    return obj.getEndDate();
                case "grantSize":
                    return obj.getGrantSize();
                case "prevBeginDate":
                    return obj.getPrevBeginDate();
                case "prevEndDate":
                    return obj.getPrevEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "grantSize":
                    obj.setGrantSize((Double) value);
                    return;
                case "prevBeginDate":
                    obj.setPrevBeginDate((Date) value);
                    return;
                case "prevEndDate":
                    obj.setPrevEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "startDate":
                        return true;
                case "endDate":
                        return true;
                case "grantSize":
                        return true;
                case "prevBeginDate":
                        return true;
                case "prevEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "startDate":
                    return true;
                case "endDate":
                    return true;
                case "grantSize":
                    return true;
                case "prevBeginDate":
                    return true;
                case "prevEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "startDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "grantSize":
                    return Double.class;
                case "prevBeginDate":
                    return Date.class;
                case "prevEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SocGrantAssignStuExtract> _dslPath = new Path<SocGrantAssignStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SocGrantAssignStuExtract");
    }
            

    /**
     * @return Дата начала выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuExtract#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Дата окончания выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Размер стипендии.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuExtract#getGrantSize()
     */
    public static PropertyPath<Double> grantSize()
    {
        return _dslPath.grantSize();
    }

    /**
     * @return Дата начала выплаты социальной стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuExtract#getPrevBeginDate()
     */
    public static PropertyPath<Date> prevBeginDate()
    {
        return _dslPath.prevBeginDate();
    }

    /**
     * @return Дата окончания выплаты социальной стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuExtract#getPrevEndDate()
     */
    public static PropertyPath<Date> prevEndDate()
    {
        return _dslPath.prevEndDate();
    }

    public static class Path<E extends SocGrantAssignStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _startDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Double> _grantSize;
        private PropertyPath<Date> _prevBeginDate;
        private PropertyPath<Date> _prevEndDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата начала выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuExtract#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(SocGrantAssignStuExtractGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Дата окончания выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(SocGrantAssignStuExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Размер стипендии.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuExtract#getGrantSize()
     */
        public PropertyPath<Double> grantSize()
        {
            if(_grantSize == null )
                _grantSize = new PropertyPath<Double>(SocGrantAssignStuExtractGen.P_GRANT_SIZE, this);
            return _grantSize;
        }

    /**
     * @return Дата начала выплаты социальной стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuExtract#getPrevBeginDate()
     */
        public PropertyPath<Date> prevBeginDate()
        {
            if(_prevBeginDate == null )
                _prevBeginDate = new PropertyPath<Date>(SocGrantAssignStuExtractGen.P_PREV_BEGIN_DATE, this);
            return _prevBeginDate;
        }

    /**
     * @return Дата окончания выплаты социальной стипендии по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.SocGrantAssignStuExtract#getPrevEndDate()
     */
        public PropertyPath<Date> prevEndDate()
        {
            if(_prevEndDate == null )
                _prevEndDate = new PropertyPath<Date>(SocGrantAssignStuExtractGen.P_PREV_END_DATE, this);
            return _prevEndDate;
        }

        public Class getEntityClass()
        {
            return SocGrantAssignStuExtract.class;
        }

        public String getEntityName()
        {
            return "socGrantAssignStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
