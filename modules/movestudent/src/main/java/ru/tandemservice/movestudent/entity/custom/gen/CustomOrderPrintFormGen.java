package ru.tandemservice.movestudent.entity.custom.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.custom.CustomOrderPrintForm;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Печатная форма кастомного приказа, загруженная пользователем
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CustomOrderPrintFormGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.custom.CustomOrderPrintForm";
    public static final String ENTITY_NAME = "customOrderPrintForm";
    public static final int VERSION_HASH = -759698325;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String L_FILE = "file";

    private AbstractStudentOrder _order;     // Приказ
    private DatabaseFile _file;     // Пользовательская печатная форма

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public AbstractStudentOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ. Свойство не может быть null и должно быть уникальным.
     */
    public void setOrder(AbstractStudentOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Пользовательская печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getFile()
    {
        return _file;
    }

    /**
     * @param file Пользовательская печатная форма. Свойство не может быть null.
     */
    public void setFile(DatabaseFile file)
    {
        dirty(_file, file);
        _file = file;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof CustomOrderPrintFormGen)
        {
            setOrder(((CustomOrderPrintForm)another).getOrder());
            setFile(((CustomOrderPrintForm)another).getFile());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CustomOrderPrintFormGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CustomOrderPrintForm.class;
        }

        public T newInstance()
        {
            return (T) new CustomOrderPrintForm();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "file":
                    return obj.getFile();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((AbstractStudentOrder) value);
                    return;
                case "file":
                    obj.setFile((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "file":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "file":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return AbstractStudentOrder.class;
                case "file":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CustomOrderPrintForm> _dslPath = new Path<CustomOrderPrintForm>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CustomOrderPrintForm");
    }
            

    /**
     * @return Приказ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.custom.CustomOrderPrintForm#getOrder()
     */
    public static AbstractStudentOrder.Path<AbstractStudentOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Пользовательская печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.CustomOrderPrintForm#getFile()
     */
    public static DatabaseFile.Path<DatabaseFile> file()
    {
        return _dslPath.file();
    }

    public static class Path<E extends CustomOrderPrintForm> extends EntityPath<E>
    {
        private AbstractStudentOrder.Path<AbstractStudentOrder> _order;
        private DatabaseFile.Path<DatabaseFile> _file;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.custom.CustomOrderPrintForm#getOrder()
     */
        public AbstractStudentOrder.Path<AbstractStudentOrder> order()
        {
            if(_order == null )
                _order = new AbstractStudentOrder.Path<AbstractStudentOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Пользовательская печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.custom.CustomOrderPrintForm#getFile()
     */
        public DatabaseFile.Path<DatabaseFile> file()
        {
            if(_file == null )
                _file = new DatabaseFile.Path<DatabaseFile>(L_FILE, this);
            return _file;
        }

        public Class getEntityClass()
        {
            return CustomOrderPrintForm.class;
        }

        public String getEntityName()
        {
            return "customOrderPrintForm";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
