/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.catalog.studentExtractType.StudentExtractTypeItemPub;

import java.util.List;

import ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubDAO;

/**
 * @author AutoGenerator
 * Created on 11.12.2010
 */
public class DAO extends DefaultCatalogItemPubDAO<StudentExtractType, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        List<StudentExtractTypeToGroup> grpRelsList = getList(StudentExtractTypeToGroup.class, StudentExtractTypeToGroup.L_TYPE, model.getCatalogItem());
        if (null != grpRelsList && grpRelsList.size() > 0)
        {
            StringBuilder builder = new StringBuilder();
            for (StudentExtractTypeToGroup grp : grpRelsList)
            {
                if (builder.length() > 0) builder.append(", ");
                builder.append(grp.getGroup().getTitle());
            }
            model.setGroupsTitle(builder.toString());
        } else
            model.setGroupsTitle("");
    }
}