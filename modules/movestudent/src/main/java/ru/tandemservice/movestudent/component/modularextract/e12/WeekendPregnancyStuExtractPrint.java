/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e12;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 11.01.2009
 */
public class WeekendPregnancyStuExtractPrint implements IPrintFormCreator<WeekendPregnancyStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, WeekendPregnancyStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        if (extract.isPayBenefit())
            modifier.put("payBenefit", new RtfString().par().append(extract.getPayBenefitText()).par());
        else modifier.put("payBenefit", "");
        if (extract.isPayOnetimeBenefit())
            modifier.put("payOnetimeBenefit", new RtfString().par().append(extract.getPayOnetimeBenefitText()).par());
        else modifier.put("payOnetimeBenefit", "");
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}
