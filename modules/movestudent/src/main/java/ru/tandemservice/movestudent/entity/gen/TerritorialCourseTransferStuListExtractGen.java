package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О переводе с курса на следующий курс (по направлениям подготовки) со сменой территориального подразделения»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TerritorialCourseTransferStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract";
    public static final String ENTITY_NAME = "territorialCourseTransferStuListExtract";
    public static final int VERSION_HASH = 1310939357;
    private static IEntityMeta ENTITY_META;

    public static final String P_TRANSFER_DATE = "transferDate";
    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String L_COURSE_OLD = "courseOld";
    public static final String L_COURSE_NEW = "courseNew";
    public static final String L_GROUP_NEW = "groupNew";
    public static final String L_EDUCATION_ORG_UNIT_NEW = "educationOrgUnitNew";
    public static final String L_EDUCATION_ORG_UNIT_OLD = "educationOrgUnitOld";

    private Date _transferDate;     // Дата перевода
    private Course _course;     // Курс
    private Group _group;     // Группа
    private CompensationType _compensationType;     // Вид возмещения затрат
    private Course _courseOld;     // Перевести с курса
    private Course _courseNew;     // Перевести на курс
    private Group _groupNew;     // Новая группа
    private EducationOrgUnit _educationOrgUnitNew;     // Новое направление подготовки студента
    private EducationOrgUnit _educationOrgUnitOld;     // Направление подготовки студента на момент формирования приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата перевода. Свойство не может быть null.
     */
    @NotNull
    public Date getTransferDate()
    {
        return _transferDate;
    }

    /**
     * @param transferDate Дата перевода. Свойство не может быть null.
     */
    public void setTransferDate(Date transferDate)
    {
        dirty(_transferDate, transferDate);
        _transferDate = transferDate;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Перевести с курса. Свойство не может быть null.
     */
    @NotNull
    public Course getCourseOld()
    {
        return _courseOld;
    }

    /**
     * @param courseOld Перевести с курса. Свойство не может быть null.
     */
    public void setCourseOld(Course courseOld)
    {
        dirty(_courseOld, courseOld);
        _courseOld = courseOld;
    }

    /**
     * @return Перевести на курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourseNew()
    {
        return _courseNew;
    }

    /**
     * @param courseNew Перевести на курс. Свойство не может быть null.
     */
    public void setCourseNew(Course courseNew)
    {
        dirty(_courseNew, courseNew);
        _courseNew = courseNew;
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroupNew()
    {
        return _groupNew;
    }

    /**
     * @param groupNew Новая группа. Свойство не может быть null.
     */
    public void setGroupNew(Group groupNew)
    {
        dirty(_groupNew, groupNew);
        _groupNew = groupNew;
    }

    /**
     * @return Новое направление подготовки студента. Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitNew()
    {
        return _educationOrgUnitNew;
    }

    /**
     * @param educationOrgUnitNew Новое направление подготовки студента. Свойство не может быть null.
     */
    public void setEducationOrgUnitNew(EducationOrgUnit educationOrgUnitNew)
    {
        dirty(_educationOrgUnitNew, educationOrgUnitNew);
        _educationOrgUnitNew = educationOrgUnitNew;
    }

    /**
     * @return Направление подготовки студента на момент формирования приказа. Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitOld()
    {
        return _educationOrgUnitOld;
    }

    /**
     * @param educationOrgUnitOld Направление подготовки студента на момент формирования приказа. Свойство не может быть null.
     */
    public void setEducationOrgUnitOld(EducationOrgUnit educationOrgUnitOld)
    {
        dirty(_educationOrgUnitOld, educationOrgUnitOld);
        _educationOrgUnitOld = educationOrgUnitOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TerritorialCourseTransferStuListExtractGen)
        {
            setTransferDate(((TerritorialCourseTransferStuListExtract)another).getTransferDate());
            setCourse(((TerritorialCourseTransferStuListExtract)another).getCourse());
            setGroup(((TerritorialCourseTransferStuListExtract)another).getGroup());
            setCompensationType(((TerritorialCourseTransferStuListExtract)another).getCompensationType());
            setCourseOld(((TerritorialCourseTransferStuListExtract)another).getCourseOld());
            setCourseNew(((TerritorialCourseTransferStuListExtract)another).getCourseNew());
            setGroupNew(((TerritorialCourseTransferStuListExtract)another).getGroupNew());
            setEducationOrgUnitNew(((TerritorialCourseTransferStuListExtract)another).getEducationOrgUnitNew());
            setEducationOrgUnitOld(((TerritorialCourseTransferStuListExtract)another).getEducationOrgUnitOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TerritorialCourseTransferStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TerritorialCourseTransferStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new TerritorialCourseTransferStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return obj.getTransferDate();
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "compensationType":
                    return obj.getCompensationType();
                case "courseOld":
                    return obj.getCourseOld();
                case "courseNew":
                    return obj.getCourseNew();
                case "groupNew":
                    return obj.getGroupNew();
                case "educationOrgUnitNew":
                    return obj.getEducationOrgUnitNew();
                case "educationOrgUnitOld":
                    return obj.getEducationOrgUnitOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "transferDate":
                    obj.setTransferDate((Date) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "courseOld":
                    obj.setCourseOld((Course) value);
                    return;
                case "courseNew":
                    obj.setCourseNew((Course) value);
                    return;
                case "groupNew":
                    obj.setGroupNew((Group) value);
                    return;
                case "educationOrgUnitNew":
                    obj.setEducationOrgUnitNew((EducationOrgUnit) value);
                    return;
                case "educationOrgUnitOld":
                    obj.setEducationOrgUnitOld((EducationOrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                        return true;
                case "course":
                        return true;
                case "group":
                        return true;
                case "compensationType":
                        return true;
                case "courseOld":
                        return true;
                case "courseNew":
                        return true;
                case "groupNew":
                        return true;
                case "educationOrgUnitNew":
                        return true;
                case "educationOrgUnitOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return true;
                case "course":
                    return true;
                case "group":
                    return true;
                case "compensationType":
                    return true;
                case "courseOld":
                    return true;
                case "courseNew":
                    return true;
                case "groupNew":
                    return true;
                case "educationOrgUnitNew":
                    return true;
                case "educationOrgUnitOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return Date.class;
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "compensationType":
                    return CompensationType.class;
                case "courseOld":
                    return Course.class;
                case "courseNew":
                    return Course.class;
                case "groupNew":
                    return Group.class;
                case "educationOrgUnitNew":
                    return EducationOrgUnit.class;
                case "educationOrgUnitOld":
                    return EducationOrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TerritorialCourseTransferStuListExtract> _dslPath = new Path<TerritorialCourseTransferStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TerritorialCourseTransferStuListExtract");
    }
            

    /**
     * @return Дата перевода. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract#getTransferDate()
     */
    public static PropertyPath<Date> transferDate()
    {
        return _dslPath.transferDate();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Перевести с курса. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract#getCourseOld()
     */
    public static Course.Path<Course> courseOld()
    {
        return _dslPath.courseOld();
    }

    /**
     * @return Перевести на курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract#getCourseNew()
     */
    public static Course.Path<Course> courseNew()
    {
        return _dslPath.courseNew();
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract#getGroupNew()
     */
    public static Group.Path<Group> groupNew()
    {
        return _dslPath.groupNew();
    }

    /**
     * @return Новое направление подготовки студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract#getEducationOrgUnitNew()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
    {
        return _dslPath.educationOrgUnitNew();
    }

    /**
     * @return Направление подготовки студента на момент формирования приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract#getEducationOrgUnitOld()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
    {
        return _dslPath.educationOrgUnitOld();
    }

    public static class Path<E extends TerritorialCourseTransferStuListExtract> extends ListStudentExtract.Path<E>
    {
        private PropertyPath<Date> _transferDate;
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private CompensationType.Path<CompensationType> _compensationType;
        private Course.Path<Course> _courseOld;
        private Course.Path<Course> _courseNew;
        private Group.Path<Group> _groupNew;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitNew;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата перевода. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract#getTransferDate()
     */
        public PropertyPath<Date> transferDate()
        {
            if(_transferDate == null )
                _transferDate = new PropertyPath<Date>(TerritorialCourseTransferStuListExtractGen.P_TRANSFER_DATE, this);
            return _transferDate;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Перевести с курса. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract#getCourseOld()
     */
        public Course.Path<Course> courseOld()
        {
            if(_courseOld == null )
                _courseOld = new Course.Path<Course>(L_COURSE_OLD, this);
            return _courseOld;
        }

    /**
     * @return Перевести на курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract#getCourseNew()
     */
        public Course.Path<Course> courseNew()
        {
            if(_courseNew == null )
                _courseNew = new Course.Path<Course>(L_COURSE_NEW, this);
            return _courseNew;
        }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract#getGroupNew()
     */
        public Group.Path<Group> groupNew()
        {
            if(_groupNew == null )
                _groupNew = new Group.Path<Group>(L_GROUP_NEW, this);
            return _groupNew;
        }

    /**
     * @return Новое направление подготовки студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract#getEducationOrgUnitNew()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
        {
            if(_educationOrgUnitNew == null )
                _educationOrgUnitNew = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_NEW, this);
            return _educationOrgUnitNew;
        }

    /**
     * @return Направление подготовки студента на момент формирования приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract#getEducationOrgUnitOld()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
        {
            if(_educationOrgUnitOld == null )
                _educationOrgUnitOld = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_OLD, this);
            return _educationOrgUnitOld;
        }

        public Class getEntityClass()
        {
            return TerritorialCourseTransferStuListExtract.class;
        }

        public String getEntityName()
        {
            return "territorialCourseTransferStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
