/* $Id$ */
package ru.tandemservice.movestudent.component.movestudentScriptItem.MovestudentScriptItemItemPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogItemPub.DefaultScriptCatalogItemPubDAO;

/**
 * @author Vasily Zhukov
 * @since 22.12.2011
 */
public class DAO<T extends ICatalogItem & IScriptItem, Model extends ru.tandemservice.movestudent.component.movestudentScriptItem.MovestudentScriptItemItemPub.Model<T>> extends DefaultScriptCatalogItemPubDAO
{
}