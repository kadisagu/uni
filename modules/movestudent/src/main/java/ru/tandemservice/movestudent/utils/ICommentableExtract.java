/* $Id$ */
package ru.tandemservice.movestudent.utils;

/**
 * @author Alexey Lopatin
 * @since 14.11.2013
 */
public interface ICommentableExtract
{
    String getGeneratedComment(boolean isPrintOrderData);
}
