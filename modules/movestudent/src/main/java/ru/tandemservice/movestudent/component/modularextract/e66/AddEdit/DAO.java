/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e66.AddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.TransferCourseStuExtract;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;

import java.util.Arrays;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 28.08.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<TransferCourseStuExtract, Model> implements IDAO
{
    @Override
    protected TransferCourseStuExtract createNewInstance()
    {
        return new TransferCourseStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setEduModel(MoveStudentDaoFacade.getCommonExtractUtil().createEduModel(model.isAddForm(), model.getExtract()));

        model.getEduModel().setGroupModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                EducationOrgUnit studentEduOrgUnit = model.getExtract().getEntity().getEducationOrgUnit();
                if (model.getEduModel().getCourse() == null || studentEduOrgUnit.getFormativeOrgUnit() == null)
                    return ListResult.getEmpty();

                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder
                        .fromEntity(Group.class, "g")
                        .where(eq(property(Group.archival().fromAlias("g")), value(Boolean.FALSE)))
                        .where(in(
                                property(Group.educationOrgUnit().fromAlias("g")),
                                Arrays.asList(studentEduOrgUnit, model.getExtract().getEntity().getGroup().getEducationOrgUnit())
                        ))
                        .where(eq(property(Group.course().fromAlias("g")), value(model.getEduModel().getCourse())))
                        .where(like(DQLFunctions.upper(property(Group.title().fromAlias("g"))), value(CoreStringUtils.escapeLike(filter))))
                        .order(property(Group.title().fromAlias("g")));

                int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                if (count > 50) builder.top(50);

                return new ListResult<>(builder.createStatement(getSession()).list(), count);
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                Group grp = get((Long) primaryKey);
                if (null != grp && findValues(null).getObjects().contains(grp))
                    return grp;
                return null;
            }
        });

        if (model.isAddForm())
        {
            model.getEduModel().setFormativeOrgUnit(model.getExtract().getEntity().getEducationOrgUnit().getFormativeOrgUnit());
            model.getEduModel().setTerritorialOrgUnit(model.getExtract().getEntity().getEducationOrgUnit().getTerritorialOrgUnit());
            model.getEduModel().setCompensationType(model.getExtract().getEntity().getCompensationType());
            model.getEduModel().setEducationLevelsHighSchool(model.getExtract().getEntity().getEducationOrgUnit().getEducationLevelHighSchool());
            model.getEduModel().setDevelopForm(model.getExtract().getEntity().getEducationOrgUnit().getDevelopForm());
            model.getEduModel().setDevelopCondition(model.getExtract().getEntity().getEducationOrgUnit().getDevelopCondition());
            model.getEduModel().setDevelopTech(model.getExtract().getEntity().getEducationOrgUnit().getDevelopTech());
            model.getEduModel().setDevelopPeriod(model.getExtract().getEntity().getEducationOrgUnit().getDevelopPeriod());
            model.getExtract().setCourseOld(model.getExtract().getEntity().getCourse());
            if (null != model.getExtract().getEntity().getGroup())
            {
                model.getExtract().setGroupOld(model.getExtract().getEntity().getGroup());
                model.getExtract().setOldGroupNumber(model.getExtract().getGroupOld() == null ? "-" : model.getExtract().getGroupOld().getTitle());
                model.getEduModel().setCourse(model.getExtract().getEntity().getGroup().getCourse());
            }
        }
    }

    @Override
    public void update(Model model)
    {
        MoveStudentDaoFacade.getCommonExtractUtil().update(model.getEduModel(), model.getExtract());
        super.update(model);
        //model.getExtract().setGroupStr(model.getExtract().getOldGroupNumber());
        //getSession().update(model.getExtract());
    }
}