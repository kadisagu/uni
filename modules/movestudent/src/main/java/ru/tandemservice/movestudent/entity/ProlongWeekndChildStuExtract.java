package ru.tandemservice.movestudent.entity;

import java.util.Date;

import ru.tandemservice.movestudent.entity.gen.ProlongWeekndChildStuExtractGen;

/**
 * Выписка из сборного приказа по студенту. О продлении отпуска по уходу за ребенком
 */
public class ProlongWeekndChildStuExtract extends ProlongWeekndChildStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getProlongDateFrom();
    }

    @Override
    public Date getEndDate()
    {
        return getProlongDateTo();
    }
}