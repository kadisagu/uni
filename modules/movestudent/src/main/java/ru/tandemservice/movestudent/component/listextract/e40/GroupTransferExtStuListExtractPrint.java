/* $Id: extractPrint.vm 24048 2012-09-06 05:21:10Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e40;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.e25.SplitStudentsGroupStuListExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.GroupTransferExtStuListExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author ListExtractComponentGenerator
 * @since 30.01.2013
 */
public class GroupTransferExtStuListExtractPrint implements IPrintFormCreator<GroupTransferExtStuListExtract>, IListParagraphPrintFormCreator<GroupTransferExtStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, GroupTransferExtStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, GroupTransferExtStuListExtract firstExtract)
    {
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
        CommonExtractPrint.initOrgUnit(injectModifier, firstExtract.getEntity().getEducationOrgUnit(), "formativeOrgUnitStr", "");

        EducationLevels speciality = EducationOrgUnitUtil.getParentLevel(firstExtract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool());
        CommonExtractPrint.modifyEducationStr(injectModifier, speciality);
        CommonExtractPrint.modifyEducationStr(injectModifier, speciality, new String[]{"fefuEducationStrDirection"}, false);

        injectModifier.put("groupNew", firstExtract.getGroupNew().getTitle());
        injectModifier.put("specialization", firstExtract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getTitle());
        injectModifier.put("transferDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(firstExtract.getTransferOrOrderDate()));

        StringBuilder fefuEducationStrProfile = new StringBuilder();
        StringBuilder fefuEducationStrProfile_I = new StringBuilder();

        Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(firstExtract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());

        String qualificationCode = null != qualification ? qualification.getCode() : null;
        if (null != qualificationCode && !SplitStudentsGroupStuListExtractPrint.PROCESSED_QUALIFICATION_CODES.contains(qualificationCode))
        {
            qualificationCode = null;
        }

        for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
        {
            injectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i), SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[i]);
            injectModifier.put("profileString" + UniRtfUtil.CASE_POSTFIX.get(i) + "P", SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_PLURAL_CASES_ARRAY.get(qualificationCode)[i]);
        }

        fefuEducationStrProfile.append(SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[0]).append(" «");
        fefuEducationStrProfile.append(firstExtract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getTitle()).append("»");
        injectModifier.put("fefuEducationStrProfile", StringUtils.capitalize(fefuEducationStrProfile.toString()));

        fefuEducationStrProfile_I.append(SplitStudentsGroupStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[4]).append(" «");
        fefuEducationStrProfile_I.append(firstExtract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getTitle()).append("»");
        injectModifier.put("fefuEducationStrProfile_I", fefuEducationStrProfile_I.toString());
        EducationOrgUnit educationOrgUnit = firstExtract.getEntity().getEducationOrgUnit();
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(injectModifier, educationOrgUnit, CommonListOrderPrint.getEducationBaseText(firstExtract.getGroupOld()), "fefuShortFastExtendedOptionalText");

        return injectModifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, GroupTransferExtStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, GroupTransferExtStuListExtract firstExtract)
    {
    }
}