package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.entity.gen.RemoveGroupManagerStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. Об освобождении от обязанностей старосты учебной группы
 */
public class RemoveGroupManagerStuExtract extends RemoveGroupManagerStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getRemovalDate();
    }
}