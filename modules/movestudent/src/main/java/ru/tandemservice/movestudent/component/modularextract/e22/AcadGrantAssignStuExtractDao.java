/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e22;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AcadGrantAssignStuExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 08.05.2009
 */
public class AcadGrantAssignStuExtractDao extends UniBaseDao implements IExtractComponentDao<AcadGrantAssignStuExtract>
{
    @Override
    public void doCommit(AcadGrantAssignStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);

        } else
        {
            extract.setPrevOrderDate(orderData.getAcadGrantAssignmentOrderDate());
            extract.setPrevOrderNumber(orderData.getAcadGrantAssignmentOrderNumber());
            extract.setPrevBeginDate(orderData.getAcadGrantPaymentDateFrom());
            extract.setPrevEndDate(orderData.getAcadGrantPaymentDateTo());
        }

        orderData.setAcadGrantAssignmentOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setAcadGrantAssignmentOrderNumber(extract.getParagraph().getOrder().getNumber());
        orderData.setAcadGrantPaymentDateFrom(extract.getBeginDate());
        orderData.setAcadGrantPaymentDateTo(extract.getEndDate());
        getSession().saveOrUpdate(orderData);
    }

    @Override
    public void doRollback(AcadGrantAssignStuExtract extract, Map parameters)
    {
        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }

        orderData.setAcadGrantAssignmentOrderDate(extract.getPrevOrderDate());
        orderData.setAcadGrantAssignmentOrderNumber(extract.getPrevOrderNumber());
        orderData.setAcadGrantPaymentDateFrom(extract.getPrevBeginDate());
        orderData.setAcadGrantPaymentDateTo(extract.getPrevEndDate());
        getSession().saveOrUpdate(orderData);
    }
}