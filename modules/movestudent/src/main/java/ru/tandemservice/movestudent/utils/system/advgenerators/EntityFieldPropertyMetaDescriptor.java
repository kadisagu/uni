/* $Id$ */
package ru.tandemservice.movestudent.utils.system.advgenerators;

/**
 * Описание поля, соответствующего property в файлах *.entity.xml
 *
 * @author Dmitry Seleznev
 * @since 10.08.2012
 */
public class EntityFieldPropertyMetaDescriptor extends EntityFieldBaseMetaDescriptor
{
    private String _fieldType; // Тип поля, в описании сущности соответствует атрибуту "type"

    /**
     * Конструктор
     *
     * @param fieldName  - Имя поля, в описании сущности соответствует атрибуту "name"
     * @param fieldType  - Тип поля, в описании сущности соответствует атрибуту "type"
     * @param fieldTitle - Человеко-читаемое имя поля, в описании сущности соответствует атрибуту "title"
     */
    public EntityFieldPropertyMetaDescriptor(String fieldName, String fieldType, String fieldTitle)
    {
        super(fieldName, fieldTitle);
        _fieldType = fieldType;
    }

    /**
     * Конструктор
     *
     * @param fieldName  - Имя поля, в описании сущности соответствует атрибуту "name"
     * @param fieldType  - Тип поля, в описании сущности соответствует атрибуту "type"
     * @param fieldTitle - Человеко-читаемое имя поля, в описании сущности соответствует атрибуту "title"
     * @param required   - Признак обязательности заполнения, в описании сущности соответствует атрибуту "required". "false" воспринимается как отсутствие явного указание данного атрибута.
     */
    public EntityFieldPropertyMetaDescriptor(String fieldName, String fieldType, String fieldTitle, boolean required)
    {
        super(fieldName, fieldTitle, required);
        _fieldType = fieldType;
    }

    /**
     * Конструктор
     *
     * @param fieldName  - Имя поля, в описании сущности соответствует атрибуту "name"
     * @param fieldType  - Тип поля, в описании сущности соответствует атрибуту "type"
     * @param fieldTitle - Человеко-читаемое имя поля, в описании сущности соответствует атрибуту "title"
     * @param required   - Признак обязательности заполнения, в описании сущности соответствует атрибуту "required". "false" воспринимается как отсутствие явного указание данного атрибута.
     * @param unique     - Ключ уникальности, в описании сущности соответствует атрибуту "unique". Может принимать строковые значения. Если пусто, или "true", то воспринимается как "true"
     */
    public EntityFieldPropertyMetaDescriptor(String fieldName, String fieldType, String fieldTitle, boolean required, String unique)
    {
        super(fieldName, fieldTitle, required, unique);
        _fieldType = fieldType;
    }

    /**
     * Тип поля, в описании сущности соответствует атрибуту "type"
     *
     * @return - тип поля
     */
    public String getFieldType()
    {
        return _fieldType;
    }
}