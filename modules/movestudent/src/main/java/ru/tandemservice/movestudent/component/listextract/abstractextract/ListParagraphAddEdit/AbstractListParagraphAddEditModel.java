/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

/**
 * @author vip_delete
 * @since 09.04.2009
 */
@Input({
        @Bind(key = AbstractListParagraphAddEditModel.PARAMETER_ORDER_ID, binding = "orderId"),
        @Bind(key = AbstractListParagraphAddEditModel.PARAMETER_PARAGRAPH_TYPE_ID, binding = "paragraphType.id"),
        @Bind(key = AbstractListParagraphAddEditModel.PARAMETER_PARAGRAPH_ID, binding = "paragraphId")
})
public abstract class AbstractListParagraphAddEditModel<T extends ListStudentExtract>
{
    public static final String PARAMETER_ORDER_ID = "orderId";
    public static final String PARAMETER_PARAGRAPH_TYPE_ID = "paragraphTypeId";
    public static final String PARAMETER_PARAGRAPH_ID = "paragraphId";
    public static final Object STATUS_KEY = new String[]{Student.L_STATUS, StudentStatus.P_TITLE};

    public static final String P_STUDENT_DISABLED = "studentDisabled";
    public static final String P_STUDENT_ACTIVE_CUSTOME_STATES = "studentActiveCustomStates";

    private Long _orderId;
    private Long _paragraphId;
    private StudentListParagraph _paragraph;
    private StudentExtractType _paragraphType = new StudentExtractType();
    private T _extract;
    private DynamicListDataSource<Student> _dataSource;
    private boolean _editForm;
    private String _searchListSettingsKey;
    private List<IEntity> _selectedItemList;
    private IMultiSelectModel _studentCustomStateCIModel;
    private List<StudentCustomStateCI> _studentCustomStateCIs;
    private T _firstExtract;
    private boolean _paragraphOnlyOneInTheOrder;
    private List<Course> _courseList;

    public boolean isParagraphOnlyOneInTheOrder()
    {
        return _paragraphOnlyOneInTheOrder;
    }

    public void setParagraphOnlyOneInTheOrder(boolean paragraphOnlyOneInTheOrder)
    {
        _paragraphOnlyOneInTheOrder = paragraphOnlyOneInTheOrder;
    }

    public T getFirstExtract()
    {
        return _firstExtract;
    }

    public void setFirstExtract(T firstExtract)
    {
        _firstExtract = firstExtract;
    }
    // Getters & Setters

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public StudentExtractType getParagraphType()
    {
        return _paragraphType;
    }

    public void setParagraphType(StudentExtractType paragraphType)
    {
        _paragraphType = paragraphType;
    }

    public Long getParagraphId()
    {
        return _paragraphId;
    }

    public void setParagraphId(Long paragraphId)
    {
        _paragraphId = paragraphId;
    }

    public StudentListParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(StudentListParagraph paragraph)
    {
        _paragraph = paragraph;
    }

    public T getExtract()
    {
        return _extract;
    }

    public void setExtract(T extract)
    {
        _extract = extract;
    }

    public DynamicListDataSource<Student> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<Student> dataSource)
    {
        _dataSource = dataSource;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public String getSearchListSettingsKey()
    {
        return _searchListSettingsKey;
    }

    public void setSearchListSettingsKey(String searchListSettingsKey)
    {
        _searchListSettingsKey = searchListSettingsKey;
    }

    public List<IEntity> getSelectedItemList()
    {
        return _selectedItemList;
    }

    public void setSelectedItemList(List<IEntity> selectedItemList)
    {
        _selectedItemList = selectedItemList;
    }

    public IMultiSelectModel getStudentCustomStateCIModel()
    {
        return _studentCustomStateCIModel;
    }

    public void setStudentCustomStateCIModel(IMultiSelectModel studentCustomStateCIModel)
    {
        _studentCustomStateCIModel = studentCustomStateCIModel;
    }

    public List<StudentCustomStateCI> getStudentCustomStateCIs()
    {
        return _studentCustomStateCIs;
    }

    public void setStudentCustomStateCIs(List<StudentCustomStateCI> studentCustomStateCIs)
    {
        _studentCustomStateCIs = studentCustomStateCIs;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }
}
