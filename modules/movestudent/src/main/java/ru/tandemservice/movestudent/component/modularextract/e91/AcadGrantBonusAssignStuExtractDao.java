/* $Id: extractDao.vm 23805 2012-08-13 07:42:13Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e91;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 11.01.2013
 */
public class AcadGrantBonusAssignStuExtractDao extends UniBaseDao implements IExtractComponentDao<AcadGrantBonusAssignStuExtract>
{
    public void doCommit(AcadGrantBonusAssignStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);

        } else
        {
            extract.setPrevOrderDate(orderData.getAcadGrantBonusAssignmentOrderDate());
            extract.setPrevOrderNumber(orderData.getAcadGrantBonusAssignmentOrderNumber());
            extract.setPrevBeginDate(orderData.getAcadGrantBonusPaymentDateFrom());
            extract.setPrevEndDate(orderData.getAcadGrantBonusPaymentDateTo());
        }

        orderData.setAcadGrantBonusAssignmentOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setAcadGrantBonusAssignmentOrderNumber(extract.getParagraph().getOrder().getNumber());
        orderData.setAcadGrantBonusPaymentDateFrom(extract.getBeginDate());
        orderData.setAcadGrantBonusPaymentDateTo(extract.getEndDate());
        getSession().saveOrUpdate(orderData);
    }

    public void doRollback(AcadGrantBonusAssignStuExtract extract, Map parameters)
    {
        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }

        orderData.setAcadGrantBonusAssignmentOrderDate(extract.getPrevOrderDate());
        orderData.setAcadGrantBonusAssignmentOrderNumber(extract.getPrevOrderNumber());
        orderData.setAcadGrantBonusPaymentDateFrom(extract.getPrevBeginDate());
        orderData.setAcadGrantBonusPaymentDateTo(extract.getPrevEndDate());
        getSession().saveOrUpdate(orderData);
    }
}
