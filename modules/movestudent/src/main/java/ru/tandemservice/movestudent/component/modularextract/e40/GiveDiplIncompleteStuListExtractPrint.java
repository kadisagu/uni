/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e40;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.GiveDiplIncompleteStuListExtract;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 09.02.2011
 */
public class GiveDiplIncompleteStuListExtractPrint implements IPrintFormCreator<GiveDiplIncompleteStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, GiveDiplIncompleteStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        EducationOrgUnit eou = extract.getEntity().getEducationOrgUnit();
        String educationLevel = eou.getEducationLevelHighSchool().getEducationLevel().getLevelType().getTitle();
        String speciality = eou.getEducationLevelHighSchool().getTitle();
        String extractTitle = extract.getType().getTitle();

        modifier.put("extractTitle", extractTitle);
        modifier.put("educationLevel", educationLevel);
        modifier.put("speciality", speciality);
        modifier.put("dischargeOrderNumber", extract.getDischargeOrderNumber());
        modifier.put("beginYear", extract.getBeginYear() != null ? RussianDateFormatUtils.getYearString(extract.getBeginYear(), false) : null);
        modifier.put("endYear", extract.getEndYear() != null ? RussianDateFormatUtils.getYearString(extract.getEndYear(), false) : null);
        modifier.put("disOrderDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDismissDate()));
        modifier.put("developCondition_G", CommonExtractPrint.getDevelopConditionStr2_G(eou.getDevelopCondition()));
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}
