/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.movestudent.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;

/**
 * @author Vasily Zhukov
 * @since 02.04.2012
 */
@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager
{
    @Autowired
    public OrgUnitView _orgUnitView;

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        // <component-tab name="abstractOrgUnit_MoveStudent" label="Движение студентов" after="abstractOrgUnit_OrgUnitEmployeeList" component-name="ru.tandemservice.movestudent.component.orgunit.OrgUnitMoveStudent" permission-key="ognl:model.getPermissionKey('orgUnit_viewMoveStudent_')" secured-object="fast:model.orgUnit" visible="ognl:@ru.tandemservice.movestudent.dao.MoveStudentDaoFacade@getMoveStudentDao().isOrgUnitMoveStudentTabVisible(model.getOrgUnit())"/>
        return tabPanelExtensionBuilder(_orgUnitView.orgUnitTabPanelExtPoint())
                .addTab(componentTab("abstractOrgUnit_MoveStudent", "ru.tandemservice.movestudent.component.orgunit.OrgUnitMoveStudent").permissionKey("ui:secModel.orgUnit_viewMoveStudent").visible("ognl:@ru.tandemservice.movestudent.dao.MoveStudentDaoFacade@getMoveStudentDao().isOrgUnitMoveStudentTabVisible(presenter.orgUnit)").parameters("mvel:['orgUnitId':presenter.orgUnit.id]"))
                .create();
    }
}
