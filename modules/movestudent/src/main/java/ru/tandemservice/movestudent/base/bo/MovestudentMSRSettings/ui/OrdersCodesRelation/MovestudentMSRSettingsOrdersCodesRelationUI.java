/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.ui.OrdersCodesRelation;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.MovestudentMSRSettingsManager;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 19.12.2014
 */
public class MovestudentMSRSettingsOrdersCodesRelationUI extends UIPresenter
{

    private Map<StudentExtractType, Integer> _codesMap;
    private DynamicListDataSource<StudentExtractType> _dataSource;

    public DynamicListDataSource<StudentExtractType> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<StudentExtractType> dataSource)
    {
        _dataSource = dataSource;
    }

    public Map<StudentExtractType, Integer> getCodesMap()
    {
        return _codesMap;
    }

    public void setCodesMap(Map<StudentExtractType, Integer> codesMap)
    {
        _codesMap = codesMap;
    }

    public String getOrderCodeId()
    {
        return "orderCodeId_" + getDataSource().getCurrentEntity().getId();
    }

    @Override
    public void onComponentRefresh()
    {

        _codesMap = MovestudentMSRSettingsManager.instance().dao().getMapOrdersCodeRelation();
        createListDataSource();

    }

    private void createListDataSource()
    {
        final DynamicListDataSource<StudentExtractType> dataSource = new DynamicListDataSource<>(getConfig().getBusinessComponent(), component -> {
            List<StudentExtractType> list = new DQLSelectBuilder().fromEntity(StudentExtractType.class, "se").createStatement(_uiSupport.getSession()).list();
            Collections.sort(list, new StudentExtractType.ItemsComparator());
            getDataSource().setTotalSize(list.size());
            getDataSource().setCountRow(list.size());
            UniBaseUtils.createPage(getDataSource(), list);
        });

        dataSource.addColumn(new SimpleColumn("Тип приказа", StudentExtractType.titleWithCode()).setClickable(false).setOrderable(false).setTreeColumn(true));
        dataSource.addColumn(new BlockColumn("orderCode", "Код приказа"));

        setDataSource(dataSource);
    }

    public void onClickApply()
    {
        MovestudentMSRSettingsManager.instance().dao().saveOrUpdateOrdersCodesRelation(_codesMap);
        deactivate();

    }

    public Integer getCurrentOrderCode()
    {
        StudentExtractType type = getDataSource().getCurrentEntity();
        return  _codesMap.get(type);
    }

    public void setCurrentOrderCode(Integer code)
    {
        StudentExtractType type = getDataSource().getCurrentEntity();
        _codesMap.put(type, code);
    }

    public boolean isVisible()
    {
        if (getDataSource().getCurrentEntity().getParent() == null)
            return false;
        if (getDataSource().getCurrentEntity().getParent().getParent() != null)
            return false;
        return true;
    }
}
