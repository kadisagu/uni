/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 05.05.2009
 */
@Input({
        @Bind(key = AbstractListOrderAddEditModel.ORG_UNIT_ID, binding = "orgUnitId"),
        @Bind(key = AbstractListOrderAddEditModel.ORDER_ID, binding = "orderId"),
        @Bind(key = AbstractListOrderAddEditModel.ORDER_TYPE_ID, binding = "orderTypeId")
})
public abstract class AbstractListOrderAddEditModel<T extends StudentListOrder>
{
    public static final String ORG_UNIT_ID = "orgUnitId";
    public static final String ORDER_ID = "orderId";
    public static final String ORDER_TYPE_ID = "orderTypeId";

    private T _order;
    private Long _orgUnitId;
    private Long _orderId;
    private Long _orderTypeId;
    private ISelectModel _reasonListModel;
    private ISelectModel _basicListModel;
    private ISelectModel _educationYearsList;
    private List<StudentOrderBasics> _basicList = new ArrayList<StudentOrderBasics>();
    private ISelectModel _employeePostModel;
    private EmployeePost _employeePost;
    private EmployeePost _responsibleEmployee;
    private String _fieldsPage;
    private String _commentsPage;
    
    private StudentOrderBasics _currentBasic;
    private Map<Long, String> _currentBasicMap = new HashMap<Long, String>();
    
    public boolean isNeedComments()
    {
        for (StudentOrderBasics basic : _basicList)
            if (basic.isCommentable()) return true;
        return false;
    }
    
    public String getCurrentBasicTitle()
    {
	    return _currentBasicMap.get(_currentBasic.getId());
    }

    public void setCurrentBasicTitle(String title)
    {
        _currentBasicMap.put(_currentBasic.getId(), title);
    }

    public String getCurrentBasicId()
    {
        return "basicId_" + _currentBasic.getId();
    }

    public boolean isReasonCommentVisible()
    {
        return null != getOrder().getReason() && getOrder().getReason().isCommentable();
    }

    // Getters & Setters

    public T getOrder()
    {
        return _order;
    }

    public void setOrder(T order)
    {
        _order = order;
        this.setEmployeePost(order.getExecutorEmpl());
        this.setResponsibleEmployee(order.getResponsibleEmpl());
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public Long getOrderTypeId()
    {
        return _orderTypeId;
    }

    public void setOrderTypeId(Long orderTypeId)
    {
        _orderTypeId = orderTypeId;
    }

    public ISelectModel getReasonListModel()
    {
        return _reasonListModel;
    }

    public void setReasonListModel(ISelectModel reasonListModel)
    {
        _reasonListModel = reasonListModel;
    }

    public ISelectModel getBasicListModel()
    {
        return _basicListModel;
    }

    public void setBasicListModel(ISelectModel basicListModel)
    {
        _basicListModel = basicListModel;
    }

    public ISelectModel getEducationYearsList()
    {
        return _educationYearsList;
    }

    public void setEducationYearsList(ISelectModel educationYearsList)
    {
        this._educationYearsList = educationYearsList;
    }

    public List<StudentOrderBasics> getBasicList()
    {
        return _basicList;
    }

    public void setBasicList(List<StudentOrderBasics> basicList)
    {
        _basicList = basicList;
    }

    public ISelectModel getEmployeePostModel()
    {
        return _employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        _employeePostModel = employeePostModel;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        _employeePost = employeePost;
    }

    public EmployeePost getResponsibleEmployee()
    {
        return _responsibleEmployee;
    }

    public void setResponsibleEmployee(EmployeePost responsibleEmployee)
    {
        _responsibleEmployee = responsibleEmployee;
    }

    public String getFieldsPage()
    {
        return _fieldsPage;
    }

    public void setFieldsPage(String fieldsPage)
    {
        _fieldsPage = fieldsPage;
    }

    public String getCommentsPage()
    {
        return _commentsPage;
    }

    public void setCommentsPage(String commentsPage)
    {
        this._commentsPage = commentsPage;
    }
    
    public StudentOrderBasics getCurrentBasic()
    {
        return _currentBasic;
    }

    public void setCurrentBasic(StudentOrderBasics currentBasic)
    {
        _currentBasic = currentBasic;
    }
    
    public Map<Long, String> getCurrentBasicMap()
    {
        return _currentBasicMap;
    }

    public void setCurrentBasicMap(Map<Long, String> currentBasicMap)
    {
        _currentBasicMap = currentBasicMap;
    }
}