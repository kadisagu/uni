/* $Id$ */
package ru.tandemservice.movestudent.utils.system.advgenerators;

/**
 * Описание поля, соответствующего many-to-one в файлах *.entity.xml
 *
 * @author Dmitry Seleznev
 * @since 10.08.2012
 */
public class EntityFieldManyToOneMetaDescriptor extends EntityFieldBaseMetaDescriptor
{
    private String _entityRef; // Тип поля, в описании сущности соответствует атрибуту "entity-ref"
    private CascadeTypes _cascade; // Сценарий обработки ссылок на объекты при их удалении, в описании сущности соответствует атрибуту "cascade"

    /**
     * Конструктор
     *
     * @param fieldName  - Имя поля, в описании сущности соответствует атрибуту "name"
     * @param entityRef  - Тип поля, в описании сущности соответствует атрибуту "entity-ref"
     * @param fieldTitle - Человеко-читаемое имя поля, в описании сущности соответствует атрибуту "title"
     */
    public EntityFieldManyToOneMetaDescriptor(String fieldName, String entityRef, String fieldTitle)
    {
        super(fieldName, fieldTitle);
        _entityRef = entityRef;
    }

    /**
     * Конструктор
     *
     * @param fieldName  - Имя поля, в описании сущности соответствует атрибуту "name"
     * @param entityRef  - Тип поля, в описании сущности соответствует атрибуту "entity-ref"
     * @param fieldTitle - Человеко-читаемое имя поля, в описании сущности соответствует атрибуту "title"
     * @param required   - Признак обязательности заполнения, в описании сущности соответствует атрибуту "required". "false" воспринимается как отсутствие явного указание данного атрибута.
     */
    public EntityFieldManyToOneMetaDescriptor(String fieldName, String entityRef, String fieldTitle, boolean required)
    {
        super(fieldName, fieldTitle, required);
        _entityRef = entityRef;
    }

    /**
     * Конструктор
     *
     * @param fieldName  - Имя поля, в описании сущности соответствует атрибуту "name"
     * @param entityRef  - Тип поля, в описании сущности соответствует атрибуту "entity-ref"
     * @param fieldTitle - Человеко-читаемое имя поля, в описании сущности соответствует атрибуту "title"
     * @param required   - Признак обязательности заполнения, в описании сущности соответствует атрибуту "required". "false" воспринимается как отсутствие явного указание данного атрибута.
     * @param unique     - Ключ уникальности, в описании сущности соответствует атрибуту "unique". Может принимать строковые значения. Если пусто, или "true", то воспринимается как "true"
     */
    public EntityFieldManyToOneMetaDescriptor(String fieldName, String entityRef, String fieldTitle, boolean required, String unique)
    {
        super(fieldName, fieldTitle, required, unique);
        _entityRef = entityRef;
    }

    /**
     * Конструктор
     *
     * @param fieldName  - Имя поля, в описании сущности соответствует атрибуту "name"
     * @param entityRef  - Тип поля, в описании сущности соответствует атрибуту "entity-ref"
     * @param fieldTitle - Человеко-читаемое имя поля, в описании сущности соответствует атрибуту "title"
     * @param required   - Признак обязательности заполнения, в описании сущности соответствует атрибуту "required". "false" воспринимается как отсутствие явного указание данного атрибута.
     * @param unique     - Ключ уникальности, в описании сущности соответствует атрибуту "unique". Может принимать строковые значения. Если пусто, или "true", то воспринимается как "true"
     * @param cascade    - Сценарий обработки ссылок на объекты при их удалении, в описании сущности соответствует атрибуту "cascade"
     */
    public EntityFieldManyToOneMetaDescriptor(String fieldName, String entityRef, String fieldTitle, boolean required, String unique, CascadeTypes cascade)
    {
        super(fieldName, fieldTitle, required, unique);
        _entityRef = entityRef;
        _cascade = cascade;
    }

    /**
     * Тип поля, в описании сущности соответствует атрибуту "entity-ref"
     *
     * @return - тип поля
     */
    public String getEntityRef()
    {
        return _entityRef;
    }

    /**
     * Сценарий обработки ссылок на объекты при их удалении, в описании сущности соответствует атрибуту "cascade"
     *
     * @return - сценарий обработки ссылок на объекты при их удалении
     */
    public CascadeTypes getCascade()
    {
        return _cascade;
    }
}