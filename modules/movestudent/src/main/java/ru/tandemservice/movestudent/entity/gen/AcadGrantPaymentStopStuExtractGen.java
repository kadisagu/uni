package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AcadGrantPaymentStopStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О прекращении выплаты академической стипендии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AcadGrantPaymentStopStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.AcadGrantPaymentStopStuExtract";
    public static final String ENTITY_NAME = "acadGrantPaymentStopStuExtract";
    public static final int VERSION_HASH = -1693361700;
    private static IEntityMeta ENTITY_META;

    public static final String L_ACAD_GRANT_EXTRACT = "acadGrantExtract";
    public static final String P_ACAD_GRANT_ORDER_NUMBER = "acadGrantOrderNumber";
    public static final String P_ACAD_GRANT_ORDER_DATE = "acadGrantOrderDate";
    public static final String P_ACAD_GRANT_PAYMENT_STOP_DATE = "acadGrantPaymentStopDate";

    private AbstractStudentExtract _acadGrantExtract;     // Выписка о назначении академической стипендии
    private String _acadGrantOrderNumber;     // Номер приказа о назначении академической стипендии
    private Date _acadGrantOrderDate;     // Дата приказа о назначении академической стипендии
    private Date _acadGrantPaymentStopDate;     // Дата прекращения выплаты стипендии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка о назначении академической стипендии.
     */
    public AbstractStudentExtract getAcadGrantExtract()
    {
        return _acadGrantExtract;
    }

    /**
     * @param acadGrantExtract Выписка о назначении академической стипендии.
     */
    public void setAcadGrantExtract(AbstractStudentExtract acadGrantExtract)
    {
        dirty(_acadGrantExtract, acadGrantExtract);
        _acadGrantExtract = acadGrantExtract;
    }

    /**
     * @return Номер приказа о назначении академической стипендии. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getAcadGrantOrderNumber()
    {
        return _acadGrantOrderNumber;
    }

    /**
     * @param acadGrantOrderNumber Номер приказа о назначении академической стипендии. Свойство не может быть null.
     */
    public void setAcadGrantOrderNumber(String acadGrantOrderNumber)
    {
        dirty(_acadGrantOrderNumber, acadGrantOrderNumber);
        _acadGrantOrderNumber = acadGrantOrderNumber;
    }

    /**
     * @return Дата приказа о назначении академической стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getAcadGrantOrderDate()
    {
        return _acadGrantOrderDate;
    }

    /**
     * @param acadGrantOrderDate Дата приказа о назначении академической стипендии. Свойство не может быть null.
     */
    public void setAcadGrantOrderDate(Date acadGrantOrderDate)
    {
        dirty(_acadGrantOrderDate, acadGrantOrderDate);
        _acadGrantOrderDate = acadGrantOrderDate;
    }

    /**
     * @return Дата прекращения выплаты стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getAcadGrantPaymentStopDate()
    {
        return _acadGrantPaymentStopDate;
    }

    /**
     * @param acadGrantPaymentStopDate Дата прекращения выплаты стипендии. Свойство не может быть null.
     */
    public void setAcadGrantPaymentStopDate(Date acadGrantPaymentStopDate)
    {
        dirty(_acadGrantPaymentStopDate, acadGrantPaymentStopDate);
        _acadGrantPaymentStopDate = acadGrantPaymentStopDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof AcadGrantPaymentStopStuExtractGen)
        {
            setAcadGrantExtract(((AcadGrantPaymentStopStuExtract)another).getAcadGrantExtract());
            setAcadGrantOrderNumber(((AcadGrantPaymentStopStuExtract)another).getAcadGrantOrderNumber());
            setAcadGrantOrderDate(((AcadGrantPaymentStopStuExtract)another).getAcadGrantOrderDate());
            setAcadGrantPaymentStopDate(((AcadGrantPaymentStopStuExtract)another).getAcadGrantPaymentStopDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AcadGrantPaymentStopStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AcadGrantPaymentStopStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new AcadGrantPaymentStopStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "acadGrantExtract":
                    return obj.getAcadGrantExtract();
                case "acadGrantOrderNumber":
                    return obj.getAcadGrantOrderNumber();
                case "acadGrantOrderDate":
                    return obj.getAcadGrantOrderDate();
                case "acadGrantPaymentStopDate":
                    return obj.getAcadGrantPaymentStopDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "acadGrantExtract":
                    obj.setAcadGrantExtract((AbstractStudentExtract) value);
                    return;
                case "acadGrantOrderNumber":
                    obj.setAcadGrantOrderNumber((String) value);
                    return;
                case "acadGrantOrderDate":
                    obj.setAcadGrantOrderDate((Date) value);
                    return;
                case "acadGrantPaymentStopDate":
                    obj.setAcadGrantPaymentStopDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "acadGrantExtract":
                        return true;
                case "acadGrantOrderNumber":
                        return true;
                case "acadGrantOrderDate":
                        return true;
                case "acadGrantPaymentStopDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "acadGrantExtract":
                    return true;
                case "acadGrantOrderNumber":
                    return true;
                case "acadGrantOrderDate":
                    return true;
                case "acadGrantPaymentStopDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "acadGrantExtract":
                    return AbstractStudentExtract.class;
                case "acadGrantOrderNumber":
                    return String.class;
                case "acadGrantOrderDate":
                    return Date.class;
                case "acadGrantPaymentStopDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AcadGrantPaymentStopStuExtract> _dslPath = new Path<AcadGrantPaymentStopStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AcadGrantPaymentStopStuExtract");
    }
            

    /**
     * @return Выписка о назначении академической стипендии.
     * @see ru.tandemservice.movestudent.entity.AcadGrantPaymentStopStuExtract#getAcadGrantExtract()
     */
    public static AbstractStudentExtract.Path<AbstractStudentExtract> acadGrantExtract()
    {
        return _dslPath.acadGrantExtract();
    }

    /**
     * @return Номер приказа о назначении академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantPaymentStopStuExtract#getAcadGrantOrderNumber()
     */
    public static PropertyPath<String> acadGrantOrderNumber()
    {
        return _dslPath.acadGrantOrderNumber();
    }

    /**
     * @return Дата приказа о назначении академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantPaymentStopStuExtract#getAcadGrantOrderDate()
     */
    public static PropertyPath<Date> acadGrantOrderDate()
    {
        return _dslPath.acadGrantOrderDate();
    }

    /**
     * @return Дата прекращения выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantPaymentStopStuExtract#getAcadGrantPaymentStopDate()
     */
    public static PropertyPath<Date> acadGrantPaymentStopDate()
    {
        return _dslPath.acadGrantPaymentStopDate();
    }

    public static class Path<E extends AcadGrantPaymentStopStuExtract> extends ModularStudentExtract.Path<E>
    {
        private AbstractStudentExtract.Path<AbstractStudentExtract> _acadGrantExtract;
        private PropertyPath<String> _acadGrantOrderNumber;
        private PropertyPath<Date> _acadGrantOrderDate;
        private PropertyPath<Date> _acadGrantPaymentStopDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка о назначении академической стипендии.
     * @see ru.tandemservice.movestudent.entity.AcadGrantPaymentStopStuExtract#getAcadGrantExtract()
     */
        public AbstractStudentExtract.Path<AbstractStudentExtract> acadGrantExtract()
        {
            if(_acadGrantExtract == null )
                _acadGrantExtract = new AbstractStudentExtract.Path<AbstractStudentExtract>(L_ACAD_GRANT_EXTRACT, this);
            return _acadGrantExtract;
        }

    /**
     * @return Номер приказа о назначении академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantPaymentStopStuExtract#getAcadGrantOrderNumber()
     */
        public PropertyPath<String> acadGrantOrderNumber()
        {
            if(_acadGrantOrderNumber == null )
                _acadGrantOrderNumber = new PropertyPath<String>(AcadGrantPaymentStopStuExtractGen.P_ACAD_GRANT_ORDER_NUMBER, this);
            return _acadGrantOrderNumber;
        }

    /**
     * @return Дата приказа о назначении академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantPaymentStopStuExtract#getAcadGrantOrderDate()
     */
        public PropertyPath<Date> acadGrantOrderDate()
        {
            if(_acadGrantOrderDate == null )
                _acadGrantOrderDate = new PropertyPath<Date>(AcadGrantPaymentStopStuExtractGen.P_ACAD_GRANT_ORDER_DATE, this);
            return _acadGrantOrderDate;
        }

    /**
     * @return Дата прекращения выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcadGrantPaymentStopStuExtract#getAcadGrantPaymentStopDate()
     */
        public PropertyPath<Date> acadGrantPaymentStopDate()
        {
            if(_acadGrantPaymentStopDate == null )
                _acadGrantPaymentStopDate = new PropertyPath<Date>(AcadGrantPaymentStopStuExtractGen.P_ACAD_GRANT_PAYMENT_STOP_DATE, this);
            return _acadGrantPaymentStopDate;
        }

        public Class getEntityClass()
        {
            return AcadGrantPaymentStopStuExtract.class;
        }

        public String getEntityName()
        {
            return "acadGrantPaymentStopStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
