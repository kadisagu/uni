/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.component.menu.ModularOrdersFinished;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

/**
 * @author vip_delete
 * @since 18.10.2008
 */
public class Model
{
    private ISelectModel _educationYearList;
    private DynamicListDataSource _dataSource;
    private IDataSettings _settings;
    private ISelectModel _studentList;
    private ISelectModel employeePostModel;
    private IMultiSelectModel _extractTypesListModel;

    public ISelectModel getEducationYearList()
    {
        return _educationYearList;
    }

    public void setEducationYearList(ISelectModel educationYearList)
    {
        _educationYearList = educationYearList;
    }

    @SuppressWarnings("unchecked")
    public <T extends IEntity> DynamicListDataSource<T> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public ISelectModel getStudentList()
    {
        return _studentList;
    }

    public void setStudentList(ISelectModel studentList)
    {
        _studentList = studentList;
    }

    public ISelectModel getEmployeePostModel()
    {
        return employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        this.employeePostModel = employeePostModel;
    }

    public IMultiSelectModel getExtractTypesListModel()
    {
        return _extractTypesListModel;
    }

    public void setExtractTypesListModel(IMultiSelectModel extractTypesListModel)
    {
        this._extractTypesListModel = extractTypesListModel;
    }
}
