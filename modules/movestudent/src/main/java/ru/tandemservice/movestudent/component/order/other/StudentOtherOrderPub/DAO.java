/* $Id$ */
package ru.tandemservice.movestudent.component.order.other.StudentOtherOrderPub;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.OtherStudentExtract;
import ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings;
import ru.tandemservice.movestudent.entity.StudentOtherOrderTextRelation;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 15.04.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        // В компонент может прийти идентификатор как выписки, так и приказа
        IEntity object = getNotNull(model.getPublisherId() != null ? model.getPublisherId() : model.getExtractId());
        IEntity order;
        if (object instanceof OtherStudentExtract)
        {
            model.setExtract((OtherStudentExtract) object);
            order = ((OtherStudentExtract) object).getParagraph().getOrder();
        }
        else
        {
            order = object;
            model.setExtract(MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(order.getId()));
        }

        model.setSecModel(new CommonPostfixPermissionModel("otherStudentOrder"));

        initFieldSettings(model);
        model.setOtherTypesThreeLevel(isOtherTypesHasThreeLevelStructure());

        StudentOtherOrderTextRelation rel = get(StudentOtherOrderTextRelation.class, StudentOtherOrderTextRelation.order().s(), order.getId());
        model.setHasFileAttached(null != rel);
        model.setPrintForm(rel);
    }

    public void initFieldSettings(Model model)
    {
        if (null == model.getExtract().getType()) model.setFieldSettings(null);

        OtherStudentFieldsSettings settings = get(OtherStudentFieldsSettings.class, OtherStudentFieldsSettings.studentExtractType(), model.getExtract().getType());
        if (null == settings && null != model.getExtract().getType())
        {
            StudentExtractType parentType = model.getExtract().getType().getParent();
            settings = get(OtherStudentFieldsSettings.class, OtherStudentFieldsSettings.studentExtractType(), parentType);

            while (null != parentType && null == settings)
            {
                settings = get(OtherStudentFieldsSettings.class, OtherStudentFieldsSettings.studentExtractType(), parentType);
                parentType = parentType.getParent();
            }
        }
        model.setFieldSettings(settings);
    }

    public void deleteAttachedPrintForm(Model model)
    {
        StudentOtherOrderTextRelation rel = UniDaoFacade.getCoreDao().get(StudentOtherOrderTextRelation.class, StudentOtherOrderTextRelation.order().s(), model.getExtract().getParagraph().getOrder().getId());
        if (null == rel) throw new ApplicationException("К данному приказу не прикреплена печатная форма.");
        delete(rel);
    }

    private boolean isOtherTypesHasThreeLevelStructure()
    {
        List<StudentExtractType> extractTypesList = getList(StudentExtractType.class);

        for (StudentExtractType type : extractTypesList)
        {
            int level = 0;
            boolean otherType = false;
            StudentExtractType parentType = type;

            while (null != parentType)
            {
                level++;
                if (null == parentType.getParent() && !StudentExtractTypeCodes.OTHER_ORDER.equals(parentType.getCode()))
                    otherType = true;
                parentType = parentType.getParent();
            }
            if (otherType && level > 2) return true;
        }
        return false;
    }
}