/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.menu.ListExtracts;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.component.commons.ExtractListUtil;
import ru.tandemservice.movestudent.component.commons.IMoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.MoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.OrderCategory;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.ui.UnimoveStudentMultiSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author vip_delete
 * @since 18.10.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry(IMoveStudentMQBuilder.EXTRACT_ALIAS);

    static
    {
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_FULL_FIO, new OrderDescription(IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS, IdentityCard.P_LAST_NAME), new OrderDescription(IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS, IdentityCard.P_FIRST_NAME), new OrderDescription(IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS, IdentityCard.P_MIDDLE_NAME));
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_EXTRACT_FORMATIVE_ORG_UNIT_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_TITLE}));
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_EXTRACT_TERRITORIAL_ORG_UNIT_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_TITLE}));
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_EXTRACT_PRODUCING_ORG_UNIT_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.L_ORG_UNIT, OrgUnit.P_TITLE}));
        _orderSettings.setOrders(MoveStudentDefines.EXTRACT_ORDER_TYPE_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, IAbstractOrder.L_TYPE, StudentExtractType.P_TITLE}));
    }

    @Override
    public void prepare(Model model)
    {
        ExtractListUtil.initExtractListModel(model, "listExtracts", OrderCategory.LIST);

        List<ExtractStates> extractStateList = new ArrayList<>();
        extractStateList.add(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
        extractStateList.add(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED));
        model.setExtractStateList(extractStateList);
        model.setStudentList(new UnimoveStudentMultiSelectModel());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        // init dataSource
        IMoveStudentMQBuilder builder = MoveStudentMQBuilder.createExtractMQBuilder(ListStudentExtract.class, model.getSettings());

        // apply user defined restrictions
        builder.getMQBuilder().add(MQExpression.isNotNull(IMoveStudentMQBuilder.EXTRACT_ALIAS, IAbstractExtract.L_PARAGRAPH));

        // apply filters
        builder.applyExtractEducationYear();
        builder.applyOrderCommitDate();
        builder.applyExtractCreateDate();
        builder.applyOrderNumber();
        builder.applyOrderCommitDateSystem();
        builder.applyExtractLastName();
        builder.applyExtractFormativeOrgUnit();
        builder.applyExtractTerritorialOrgUnit();
        builder.applyExtractProducingOrgUnit();
        builder.applyExtractEducationLevelHighSchool();
        builder.applyExtractCourse();
        builder.applyExtractDevelopForm();
        builder.applyExtractCompensationType();
        builder.applyListExtractType();
        builder.applyListOrderReason();
        builder.applyExtractState();
        builder.applyExtractStudent(getSession());

        // create page
        builder.createPage(getSession(), _orderSettings, model.getDataSource());

        // get statistics for view properties
        DynamicListDataSource<AbstractStudentExtract> ds = model.getDataSource();
        Set<Long> disabledExtractTypes = MoveStudentDaoFacade.getMoveStudentDao().getDisabledFormPrintingExtracts(ds.getEntityList(), MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        // inject view properties
        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            wrapper.setViewProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY, disabledExtractTypes.contains(wrapper.getId()));
        }
    }
}
