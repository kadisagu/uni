package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.TransferTerritorialExtStuExtractGen;

import java.util.Date;

/**
 * О переводе со специальности на специальность (с направления на направление) между территориальными подразделениями
 */
public class TransferTerritorialExtStuExtract extends TransferTerritorialExtStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}