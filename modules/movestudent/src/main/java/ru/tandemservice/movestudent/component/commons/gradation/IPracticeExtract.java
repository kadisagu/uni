/* $Id$ */
package ru.tandemservice.movestudent.component.commons.gradation;

import java.util.Date;

/**
 * Интерфейс для выписок о направлении на практику.
 *
 * @author Nikolay Fedorovskih
 * @since 04.07.2013
 */
public interface IPracticeExtract
{
    String getPracticeKind();

    String getPracticePlace();

    Date getPracticeBeginDate();

    Date getPracticeEndDate();

    String getPracticeHeaderInnerStr();
}