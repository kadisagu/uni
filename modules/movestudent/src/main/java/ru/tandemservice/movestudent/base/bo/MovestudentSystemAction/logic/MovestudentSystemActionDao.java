/**
 *$Id$
 */
package ru.tandemservice.movestudent.base.bo.MovestudentSystemAction.logic;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.MovestudentMSRSettingsManager;
import ru.tandemservice.movestudent.base.bo.MovestudentSystemAction.MovestudentSystemActionManager;
import ru.tandemservice.movestudent.base.bo.MovestudentSystemAction.logic.script.IMovestudentUnloadScript;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.CounterMSRForExportFile;
import ru.tandemservice.movestudent.entity.ExportMSRSettings;
import ru.tandemservice.uni.dao.IUniScriptDao;
import ru.tandemservice.uni.dao.IUniScriptDao.IScriptWrapper;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.Date;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 13.02.13
 */
public class MovestudentSystemActionDao extends UniBaseDao implements IMovestudentSystemActionDao
{

    private final ReentrantLock lock = new ReentrantLock();


    @Override
    public void doCorrectNonCommitedOrdersWithCommitedExtracts()
    {
        // Получаем список непроведенных приказов, у которых проведены все выписки
        List<Long> orderIdsList = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e").column("o.id")
                .joinPath(DQLJoinType.inner, AbstractStudentExtract.paragraph().order().fromAlias("e"), "o")
                .where(ne(property("o", AbstractStudentOrder.state().code()), value(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)))
                .group(property("o.id"))
                .having(eq(DQLFunctions.count(DQLPredicateType.distinct, property("e", IAbstractExtract.L_STATE)), value(1)))
                .having(eq(DQLFunctions.min(property("e", AbstractStudentExtract.state().code())), value(UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED)))
                .createStatement(getSession()).list();

        for (Long orderId : orderIdsList)
        {
            AbstractStudentOrder order = refresh(this.<AbstractStudentOrder>getNotNull(orderId));
            order.setState(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED));
            update(order);
        }
    }

    @Override
    public void deleteEmptyOrders()
    {
        DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                .fromEntity(AbstractStudentExtract.class, "e")
                .column(property(AbstractStudentExtract.paragraph().order().id().fromAlias("e")));

        new DQLDeleteBuilder(AbstractStudentOrder.class)
                .where(notIn(property(AbstractStudentOrder.id()), subBuilder.buildQuery()))
                .createStatement(getSession()).execute();
    }

    //Счетчик выгруженных файлов за день
    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    public int updateCounter()
    {
        getSession().clear();
        CounterMSRForExportFile counter = new DQLSelectBuilder().fromEntity(CounterMSRForExportFile.class, "c").createStatement(getSession()).uniqueResult();
        Date currentDate = CoreDateUtils.getDayFirstTimeMoment(new Date());
        int result;
        if (counter == null)
        {
            counter = new CounterMSRForExportFile();
            counter.setCurrentDate(currentDate);
        }

        if (counter.getCurrentDate().equals(currentDate))
        {
            result = counter.getCounter();
            counter.setCounter(result + 1);
        }
        else
        {
            result = 1;
            counter.setCurrentDate(currentDate);
            counter.setCounter(result);
        }

        saveOrUpdate(counter);
        getSession().flush();
        return result;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public ScriptResultData generateUnloadingStudentsData(OrgUnit formativeOrgUnit, boolean isAllStudent, boolean onlyFullData, List<StudentCustomStateCI> studentCustomStates)
    {
        final ExportMSRSettings settings = MovestudentMSRSettingsManager.instance().dao().getSettings();
        if (settings != null) {
            refresh(settings);
        }

        int counter = 0;
        lock.lock();  // block until condition holds
        try
        {
           counter = MovestudentSystemActionManager.instance().dao().updateCounter();
        }
        finally
        {
            lock.unlock();
        }


        final String configKey = "movestudent_unload_export_script_" + (null == settings || settings.getUserScript() == null ? "null" : Hashing.crc32().newHasher().putString(settings.getUserScript(), Charsets.UTF_8).hash().toString());
        final String scriptTitle = null == settings ? "Скрипт по умолчанию (без настроек)" : "Скрипт пользовательский";
        final String defScriptPath = ExportMSRSettings.getDefScriptPath();

        IScriptWrapper<IMovestudentUnloadScript, ExportMSRSettings> scriptWrapper = IUniScriptDao.instance.get().buildScriptWrapper(settings, IMovestudentUnloadScript.class, configKey, defScriptPath, scriptTitle);

        return scriptWrapper.getScript().exportStudentsData(getSession(), formativeOrgUnit, isAllStudent, onlyFullData, studentCustomStates, counter);
    }
}