package ru.tandemservice.movestudent.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;



/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_movestudent_2x11x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность excludeSingGrpStuListExtract
		// создано свойство finishedYear
		if (!tool.columnExists("excludesinggrpstulistextract_t", "finishedyear_p"))
		{
			// создать колонку
			tool.createColumn("excludesinggrpstulistextract_t", new DBColumn("finishedyear_p", DBType.INTEGER));
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность excludeSingGrpSuccStuListExtract
		// создано свойство finishedYear
		if (!tool.columnExists("xcldsnggrpsccstlstextrct_t", "finishedyear_p"))
		{
			// создать колонку
			tool.createColumn("xcldsnggrpsccstlstextrct_t", new DBColumn("finishedyear_p", DBType.INTEGER));
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность excludeStuListExtract
		// создано свойство finishedYear
		if (!tool.columnExists("excludestulistextract_t", "finishedyear_p"))
		{
			// создать колонку
			tool.createColumn("excludestulistextract_t", new DBColumn("finishedyear_p", DBType.INTEGER));
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность giveDiplExtStuListExtract
		// создано свойство finishedYear
		if (!tool.columnExists("givediplextstulistextract_t", "finishedyear_p"))
		{
			// создать колонку
			tool.createColumn("givediplextstulistextract_t", new DBColumn("finishedyear_p", DBType.INTEGER));
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность giveDiplSuccessExtStuListExtract
		// создано свойство finishedYear
		if (!tool.columnExists("gvdplsccssextstlstextrct_t", "finishedyear_p"))
		{
			// создать колонку
			tool.createColumn("gvdplsccssextstlstextrct_t", new DBColumn("finishedyear_p", DBType.INTEGER));
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность graduateStuListExtract
		// создано свойство finishedYear
		if (!tool.columnExists("graduatestulistextract_t", "finishedyear_p"))
		{
			// создать колонку
			tool.createColumn("graduatestulistextract_t", new DBColumn("finishedyear_p", DBType.INTEGER));
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность graduateSuccessStuListExtract
		// создано свойство finishedYear
		if (!tool.columnExists("grdtsccssstlstextrct_t", "finishedyear_p"))
		{
			// создать колонку
			tool.createColumn("grdtsccssstlstextrct_t", new DBColumn("finishedyear_p", DBType.INTEGER));
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность giveDiplomaStuExtract
		// создано свойство finishedYear
		if (!tool.columnExists("givediplomastuextract_t", "finishedyear_p"))
		{
			// создать колонку
			tool.createColumn("givediplomastuextract_t", new DBColumn("finishedyear_p", DBType.INTEGER));
		}
    }

}