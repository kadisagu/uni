/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic;

import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.shared.archive.base.bo.Archive.ui.AbstractList.AbstractArchiveList;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentModularOrder;

/**
 * @author Ekaterina Zvereva
 * @since 24.11.2016
 */
public interface IExtractsMassPrintDao
{
    IDocumentRenderer printMassOrder(AbstractStudentOrder order);
}
