/* $Id: extractDao.vm 23805 2012-08-13 07:42:13Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e92;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.TransferCourseWithChangeTerrOuStuExtract;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 14.01.2013
 */
public class TransferCourseWithChangeTerrOuStuExtractDao extends UniBaseDao implements IExtractComponentDao<TransferCourseWithChangeTerrOuStuExtract>
{
    public void doCommit(TransferCourseWithChangeTerrOuStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);
        MoveStudentDaoFacade.getCommonExtractUtil().doCommitWithoutChangeStatus(extract);

        doCaptainStudent(extract.getEntity(), extract.getGroupOld(), extract.getGroupNew());
    }

    public void doRollback(TransferCourseWithChangeTerrOuStuExtract extract, Map parameters)
    {
        MoveStudentDaoFacade.getCommonExtractUtil().doRollbackWithoutChangeStatus(extract);

        doCaptainStudent(extract.getEntity(), extract.getGroupNew(), extract.getGroupOld());
    }

    private void doCaptainStudent(Student student, Group groupOld, Group groupNew)
    {
        if ((UniDaoFacade.getGroupDao().isCaptainStudent(groupOld, student)) && !UniDaoFacade.getGroupDao().isCaptainStudent(groupNew, student))
        {
            UniDaoFacade.getGroupDao().deleteCaptainStudent(groupOld, student);
            UniDaoFacade.getGroupDao().addCaptainStudent(groupNew, student);
        }
    }
}
