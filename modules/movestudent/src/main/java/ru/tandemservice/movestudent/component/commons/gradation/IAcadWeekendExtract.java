/* $Id:$ */
package ru.tandemservice.movestudent.component.commons.gradation;

import java.util.Date;

/**
 * @author Denis Perminov
 * @since 30.04.2014
 */
public interface IAcadWeekendExtract
{
    /**
     * Дата начала академического отпуска
     */
    Date getBeginAcadWeekendDate();
    /**
     * Дата окончания академического отпуска
     */
    Date getEndAcadWeekendDate();
}
