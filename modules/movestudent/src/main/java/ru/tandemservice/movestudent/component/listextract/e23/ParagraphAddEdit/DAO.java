/* $Id: DAO.java.vm 12068 2010-03-29 10:41:18Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e23.ParagraphAddEdit;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract;
import ru.tandemservice.movestudent.entity.DegreeToExtractRelation;
import ru.tandemservice.movestudent.entity.StatusToExtractRelation;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.ui.selectModel.PpsEntryModel;
import ru.tandemservice.unimove.IAbstractExtract;

import java.util.*;

/**
 * @author ListExtractComponentGenerator
 * @since 19.02.2011
 */
@SuppressWarnings("unchecked")
public class DAO extends AbstractListParagraphAddEditDAO<ChangeDiplomaWorkTopicStuListExtract, Model> implements IDAO
{
    @Override
    public void prepareColumns(final Model model)
    {
        if (model.getParagraphId() != null)
        {
            Map<Long, String> diplomaWorkTopicValueMap = new HashMap<>();
            Map<Long, PpsEntry> reviewerNameValueMap = new HashMap<>();
            Map<Long, List<ScienceDegree>> reviewerDegreeValueMap = new HashMap<>();
            Map<Long, List<ScienceStatus>> scientificAdviserStatusValueMap = new HashMap<>();

            List<ChangeDiplomaWorkTopicStuListExtract> extractList = getList(ChangeDiplomaWorkTopicStuListExtract.class, IAbstractExtract.L_PARAGRAPH, model.getParagraph());
            for (ChangeDiplomaWorkTopicStuListExtract ext : extractList)
            {
                diplomaWorkTopicValueMap.put(ext.getEntity().getId(), ext.getDiplomaWorkTopicNew());
                reviewerNameValueMap.put(ext.getEntity().getId(), ext.getScientificAdviserName());

                List<ScienceDegree> scienceDegreeList = new ArrayList<>();
                List<DegreeToExtractRelation> degreeToExtractRelationList = getList(DegreeToExtractRelation.class, DegreeToExtractRelation.extract(), ext);
                for (DegreeToExtractRelation degreeToExtractRelation : degreeToExtractRelationList)
                    scienceDegreeList.add(degreeToExtractRelation.getDegree());
                reviewerDegreeValueMap.put(ext.getEntity().getId(), scienceDegreeList);

                List<ScienceStatus> scienceStatusList = new ArrayList<>();
                List<StatusToExtractRelation> statusToExtractRelationList = getList(StatusToExtractRelation.class, StatusToExtractRelation.extract(), ext);
                for (StatusToExtractRelation statusToExtractRelation : statusToExtractRelationList)
                    scienceStatusList.add(statusToExtractRelation.getStatus());
                scientificAdviserStatusValueMap.put(ext.getEntity().getId(), scienceStatusList);

            }
            ((BlockColumn) model.getDataSource().getColumn("diplomaWorkTopic")).setValueMap(diplomaWorkTopicValueMap);
            ((BlockColumn) model.getDataSource().getColumn("scientificAdviserName")).setValueMap(reviewerNameValueMap);
            ((BlockColumn) model.getDataSource().getColumn("scientificAdviserDegree")).setValueMap(reviewerDegreeValueMap);
            ((BlockColumn) model.getDataSource().getColumn("scientificAdviserStatus")).setValueMap(scientificAdviserStatusValueMap);
        }
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setPpsListModel(new PpsEntryModel());

        model.setDegreeListModel(new LazySimpleSelectModel<>(ScienceDegree.class).setSearchFromStart(false));
        model.setStatusListModel(new LazySimpleSelectModel<>(ScienceStatus.class).setSearchFromStart(false));
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionList(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));

        ChangeDiplomaWorkTopicStuListExtract firstExtract = model.getFirstExtract();
        if(null != firstExtract)
        {
            if(null != model.getParagraphId())
            {
                model.setCourse(firstExtract.getCourse());
                model.setGroup(firstExtract.getGroup());
                model.setDevelopForm(firstExtract.getDevelopForm());
                model.setDevelopCondition(firstExtract.getDevelopCondition());
                //model.setCompensationType(firstExtract.getCompensationType());
            }
            model.setPreviousOrderNumber(firstExtract.getPreviousOrderNumber());
            model.setPreviousOrderDate(firstExtract.getPreviousOrderDate());
        }
        model.setNotFirstParagraph(!model.isParagraphOnlyOneInTheOrder());


    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developForm(), model.getDevelopForm()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developCondition(), model.getDevelopCondition()));
        if(null != model.getCompensationType()) builder.add(MQExpression.eq(STUDENT_ALIAS, Student.compensationType(), model.getCompensationType()));
    }

    @Override
    protected ChangeDiplomaWorkTopicStuListExtract createNewInstance(Model model)
    {
        return new ChangeDiplomaWorkTopicStuListExtract();
    }

    @Override
    public void update(Model model)
    {
        super.update(model);
        List<ChangeDiplomaWorkTopicStuListExtract> extractList = getList(ChangeDiplomaWorkTopicStuListExtract.class, IAbstractExtract.L_PARAGRAPH, model.getParagraph());
        for (ChangeDiplomaWorkTopicStuListExtract extract : extractList)
        {
            afterFillExtract(extract, model);
        }
    }

    @Override
    protected void fillExtract(ChangeDiplomaWorkTopicStuListExtract extract, Student student, Model model)
    {
        extract.setCourse(model.getCourse());
        extract.setDevelopForm(model.getDevelopForm());
        extract.setDevelopCondition(model.getDevelopCondition());
        extract.setGroup(model.getGroup());
        extract.setPreviousOrderNumber(model.getPreviousOrderNumber());
        extract.setPreviousOrderDate(model.getPreviousOrderDate());

        if(null == model.getCompensationType()) extract.setCompensationType(extract.getEntity().getCompensationType());
        else extract.setCompensationType(model.getCompensationType());

        ErrorCollector errs = UserContext.getInstance().getErrorCollector();

        final IValueMapHolder diplomaWorkTopicValueHolder = (IValueMapHolder) model.getDataSource().getColumn("diplomaWorkTopic");
        final Map<Long, Object> diplomaWorkTopicValueMap = (null == diplomaWorkTopicValueHolder ? Collections.emptyMap() : diplomaWorkTopicValueHolder.getValueMap());
        if (diplomaWorkTopicValueMap.get(student.getId()) == null)
            errs.add("Поля «Тема дипломной работы» у выбранных студентов обязательны для заполнения.", "topic_id_" + student.getId());
        else
        {
            extract.setDiplomaWorkTopicNew(diplomaWorkTopicValueMap.get(student.getId()).toString());
            extract.setDiplomaWorkTopicOld(student.getFinalQualifyingWorkTheme());
        }

        final IValueMapHolder scientificAdviserNameValueHolder = (IValueMapHolder) model.getDataSource().getColumn("scientificAdviserName");
        final Map<Long, Object> scientificAdviserNameValueMap = (null == scientificAdviserNameValueHolder ? Collections.emptyMap() : scientificAdviserNameValueHolder.getValueMap());
        if (scientificAdviserNameValueMap.get(student.getId()) == null)
            errs.add("Поля «ФИО научного руководителя» у выбранных студентов обязательны для заполнения.", "name_id_" + student.getId());
        else
        {
            extract.setScientificAdviserName((PpsEntry) scientificAdviserNameValueMap.get(student.getId()));
            extract.setScientificAdviserNameStrOld(student.getFinalQualifyingWorkAdvisor());
            extract.setScientificAdviserNameOld(student.getPpsEntry());
        }
    }

    public void afterFillExtract(ChangeDiplomaWorkTopicStuListExtract extract, Model model)
    {
        Student student = extract.getEntity();
        UserContext.getInstance().getErrorCollector();

        final IValueMapHolder scientificAdviserDegreeValueHolder = (IValueMapHolder) model.getDataSource().getColumn("scientificAdviserDegree");
        final Map<Long, Object> scientificAdviserDegreeValueMap = (null == scientificAdviserDegreeValueHolder ? Collections.emptyMap() : scientificAdviserDegreeValueHolder.getValueMap());
        if (scientificAdviserDegreeValueMap.get(student.getId()) != null)
        {
            MQBuilder builder = new MQBuilder(DegreeToExtractRelation.ENTITY_CLASS, "g");
            builder.add(MQExpression.eq("g", DegreeToExtractRelation.extract(), extract));
            List<DegreeToExtractRelation> degreeToExtractRelationList = builder.getResultList(getSession());
            List<ScienceDegree> academicDegreeList = (List<ScienceDegree>) scientificAdviserDegreeValueMap.get(student.getId());

            for (DegreeToExtractRelation degreeToExtractRelation : degreeToExtractRelationList)
                if (!academicDegreeList.contains(degreeToExtractRelation.getDegree()))
                    delete(degreeToExtractRelation);
                else
                    academicDegreeList.remove(degreeToExtractRelation.getDegree());

            getSession().flush();

            for (ScienceDegree scienceDegree : academicDegreeList)
            {
                DegreeToExtractRelation degreeToExtractRelation = new DegreeToExtractRelation();
                degreeToExtractRelation.setExtract(extract);
                degreeToExtractRelation.setDegree(scienceDegree);
                save(degreeToExtractRelation);
            }
        }

        final IValueMapHolder scientificAdviserStatusValueHolder = (IValueMapHolder) model.getDataSource().getColumn("scientificAdviserStatus");
        final Map<Long, Object> scientificAdviserStatusValueMap = (null == scientificAdviserStatusValueHolder ? Collections.emptyMap() : scientificAdviserStatusValueHolder.getValueMap());
        if (scientificAdviserStatusValueMap.get(student.getId()) != null)
        {
            MQBuilder builder = new MQBuilder(StatusToExtractRelation.ENTITY_CLASS, "g");
            builder.add(MQExpression.eq("g", StatusToExtractRelation.extract(), extract));
            List<StatusToExtractRelation> statusToExtractRelationList = builder.getResultList(getSession());
            List<ScienceStatus> statusList = (List<ScienceStatus>) scientificAdviserStatusValueMap.get(student.getId());

            for (StatusToExtractRelation statusToExtractRelation : statusToExtractRelationList)
                if (!statusList.contains(statusToExtractRelation.getStatus()))
                    delete(statusToExtractRelation);
                else
                    statusList.remove(statusToExtractRelation.getStatus());

            getSession().flush();

            for (ScienceStatus scienceStatus : statusList)
            {
                StatusToExtractRelation statusToExtractRelation = new StatusToExtractRelation();
                statusToExtractRelation.setExtract(extract);
                statusToExtractRelation.setStatus(scienceStatus);
                save(statusToExtractRelation);
            }
        }

    }
}