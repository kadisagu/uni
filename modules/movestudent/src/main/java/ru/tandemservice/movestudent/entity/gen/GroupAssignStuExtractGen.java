package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.GroupAssignStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О распределении в группу
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GroupAssignStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.GroupAssignStuExtract";
    public static final String ENTITY_NAME = "groupAssignStuExtract";
    public static final int VERSION_HASH = 1238407234;
    private static IEntityMeta ENTITY_META;

    public static final String L_GROUP_OLD = "groupOld";
    public static final String L_GROUP_NEW = "groupNew";

    private Group _groupOld;     // Группа на момент проведения приказа
    private Group _groupNew;     // Новая группа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Группа на момент проведения приказа.
     */
    public Group getGroupOld()
    {
        return _groupOld;
    }

    /**
     * @param groupOld Группа на момент проведения приказа.
     */
    public void setGroupOld(Group groupOld)
    {
        dirty(_groupOld, groupOld);
        _groupOld = groupOld;
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroupNew()
    {
        return _groupNew;
    }

    /**
     * @param groupNew Новая группа. Свойство не может быть null.
     */
    public void setGroupNew(Group groupNew)
    {
        dirty(_groupNew, groupNew);
        _groupNew = groupNew;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof GroupAssignStuExtractGen)
        {
            setGroupOld(((GroupAssignStuExtract)another).getGroupOld());
            setGroupNew(((GroupAssignStuExtract)another).getGroupNew());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GroupAssignStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GroupAssignStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new GroupAssignStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "groupOld":
                    return obj.getGroupOld();
                case "groupNew":
                    return obj.getGroupNew();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "groupOld":
                    obj.setGroupOld((Group) value);
                    return;
                case "groupNew":
                    obj.setGroupNew((Group) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "groupOld":
                        return true;
                case "groupNew":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "groupOld":
                    return true;
                case "groupNew":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "groupOld":
                    return Group.class;
                case "groupNew":
                    return Group.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GroupAssignStuExtract> _dslPath = new Path<GroupAssignStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GroupAssignStuExtract");
    }
            

    /**
     * @return Группа на момент проведения приказа.
     * @see ru.tandemservice.movestudent.entity.GroupAssignStuExtract#getGroupOld()
     */
    public static Group.Path<Group> groupOld()
    {
        return _dslPath.groupOld();
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GroupAssignStuExtract#getGroupNew()
     */
    public static Group.Path<Group> groupNew()
    {
        return _dslPath.groupNew();
    }

    public static class Path<E extends GroupAssignStuExtract> extends ModularStudentExtract.Path<E>
    {
        private Group.Path<Group> _groupOld;
        private Group.Path<Group> _groupNew;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Группа на момент проведения приказа.
     * @see ru.tandemservice.movestudent.entity.GroupAssignStuExtract#getGroupOld()
     */
        public Group.Path<Group> groupOld()
        {
            if(_groupOld == null )
                _groupOld = new Group.Path<Group>(L_GROUP_OLD, this);
            return _groupOld;
        }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.GroupAssignStuExtract#getGroupNew()
     */
        public Group.Path<Group> groupNew()
        {
            if(_groupNew == null )
                _groupNew = new Group.Path<Group>(L_GROUP_NEW, this);
            return _groupNew;
        }

        public Class getEntityClass()
        {
            return GroupAssignStuExtract.class;
        }

        public String getEntityName()
        {
            return "groupAssignStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
