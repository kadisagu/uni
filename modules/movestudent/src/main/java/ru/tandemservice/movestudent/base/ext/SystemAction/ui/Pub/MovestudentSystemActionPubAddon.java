/**
 *$Id$
 */
package ru.tandemservice.movestudent.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.movestudent.base.bo.MovestudentSystemAction.MovestudentSystemActionManager;
import ru.tandemservice.movestudent.base.bo.MovestudentSystemAction.ui.ExportStudentDataGupMsr.MovestudentSystemActionExportStudentDataGupMsr;

/**
 * @author Alexander Shaburov
 * @since 13.02.13
 */
public class MovestudentSystemActionPubAddon extends UIAddon
{
    public MovestudentSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onСorrectNonCommitedOrdersWithCommitedExtracts() throws Exception
    {
        MovestudentSystemActionManager.instance().dao().doCorrectNonCommitedOrdersWithCommitedExtracts();
    }

    public void onDeleteEmptyOrders()
    {
        MovestudentSystemActionManager.instance().dao().deleteEmptyOrders();
    }

    public void onClickImportGupMsrStudents()
    {
        getActivationBuilder().asRegion(MovestudentSystemActionExportStudentDataGupMsr.class).activate();
    }
}
