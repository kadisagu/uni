\keep\keepn\qj\b {extractNumber}. ПРИОСТАНОВИТЬ:\par\b0
{student_D} {course} курса {orgUnit_G} {developForm_GF} формы обучения {fio} {birthDate} дата рождения, выплату социальной стипендии с {stopPayingDate} г. до ликвидации {beforeLiquidation}.\par
  Основание: {listBasics}\par\fi0