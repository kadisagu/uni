/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e70;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.TransferTerritorialExtStuExtract;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;

import java.util.Collections;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 29.08.2012
 */
public class TransferTerritorialExtStuExtractPrint implements IPrintFormCreator<TransferTerritorialExtStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, TransferTerritorialExtStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        CommonExtractPrint.injectSpecializationTransferOrderTitlePart(modifier, "eduLevelText", extract.getEducationOrgUnitOld(), extract.getEducationOrgUnitNew());

        if (extract.isLiquidateEduPlanDifference())
        {
            String liquidationDeadlineDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getLiquidationDeadlineDate());
            String liquidateEduPlanDifference = ApplicationRuntime.getProperty("liquidateEduPlanDifferenceDot").replaceAll("\\{deadlineDate\\}", liquidationDeadlineDate);
            modifier.put("liquidateEduPlanDifference", liquidateEduPlanDifference);

        }
        else
        {
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("liquidateEduPlanDifference"), true, false);
        }

        if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(extract.getCompensationTypeNew().getCode()))
            modifier.put("fefuCompensationTypeStrNewAlt", "с сохранением бюджетного места");
        else
            modifier.put("fefuCompensationTypeStrNewAlt", "на договорной основе");

        CommonExtractPrint.initFefuGroup(modifier, "intoGroupNew", extract.getGroupNew(), extract.getEducationOrgUnitNew().getDevelopForm(), " в группу ");

        // Добавлена метка в шаблон для ДВФУ "для прохождения ГИА"
        modifier.put("transferAim", ApplicationRuntime.getProperty(extract.isForStateFinalAttestation() ? "forStateFinalAttestation" : "forEducation"));

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}