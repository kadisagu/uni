/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e90.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.movestudent.entity.WeekendChildCareOutStuExtract;

import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 26.12.2012
 */
public class Model extends ModularStudentExtractPubModel<WeekendChildCareOutStuExtract>
{
    private List<StuExtractToDebtRelation> _debtsList;

    public List<StuExtractToDebtRelation> getDebtsList()
    {
        return _debtsList;
    }

    public void setDebtsList(List<StuExtractToDebtRelation> debtsList)
    {
        this._debtsList = debtsList;
    }

    public String getDebtsListStr()
    {
        StringBuilder builder = new StringBuilder();
        for (StuExtractToDebtRelation rel : getDebtsList())
        {
            builder.append(builder.length() > 0 ? ",</li>" : "<ol> ").append("<li type=\"1\">");
            builder.append(rel.getDiscipline()).append(" (").append(rel.getHours()).append(" ч.) - ");
            builder.append(rel.getControlAction());
        }
        builder.append(builder.length() > 0 ? "</li> </ol>" : "");
        return builder.toString();
    }
}
