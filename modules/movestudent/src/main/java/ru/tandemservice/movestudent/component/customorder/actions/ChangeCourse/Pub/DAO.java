/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeCourse.Pub;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.BaseCustomActionPubDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeCourseAction;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class DAO extends BaseCustomActionPubDAO<ChangeCourseAction, Model>
{
    @Override
    protected Class<ChangeCourseAction> getActionClass()
    {
        return ChangeCourseAction.class;
    }
}