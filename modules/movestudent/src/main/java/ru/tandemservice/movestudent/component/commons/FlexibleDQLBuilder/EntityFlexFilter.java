/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.IDQLExpression;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author Nikolay Fedorovskih
 * @since 27.11.2013
 */
public class EntityFlexFilter extends BaseFlexFilter
{
    public EntityFlexFilter(String settingName, String alias, String propertyPath)
    {
        super(settingName, alias, propertyPath);
    }

    @Override
    protected IDQLExpression internalCreateCondition(Object o)
    {
        if (o instanceof Collection)
            return in(DQLExpressions.property(getAlias(), getPath()), (Collection) o);
        return eq(DQLExpressions.property(getAlias(), getPath()), value(((IEntity)o).getId()));
    }
}