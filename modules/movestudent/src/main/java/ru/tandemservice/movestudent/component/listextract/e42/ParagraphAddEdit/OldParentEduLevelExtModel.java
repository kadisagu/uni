/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e42.ParagraphAddEdit;

import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.IExtEducationLevelModel;

/**
 * @author Andrey Avetisov
 * @since 05.05.2014
 */
public class OldParentEduLevelExtModel implements IExtEducationLevelModel
{
    private Model _model;

    public OldParentEduLevelExtModel(Model model)
    {
        _model = model;
    }

    @Override
    public EducationLevels getParentEduLevel()
    {
        return EducationOrgUnitUtil.getParentLevel(_model.getGroupOld().getEducationOrgUnit().getEducationLevelHighSchool());
    }
}
