/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.list.StudentListOrderPrint;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentOrderTextRelation;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * @author vip_delete
 * @since 08.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setOrder(getNotNull(StudentListOrder.class, model.getOrder().getId()));

        String stateCode = model.getOrder().getState().getCode();

        // если приказ на согласовании, согласован или проведен, то печатная форма должна быть сохранена
        if (UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTABLE.equals(stateCode)
                || UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED.equals(stateCode)
                || UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(stateCode))
        {
            // приказ проведен => печатная форма сохранена
            StudentOrderTextRelation rel = MoveStudentDaoFacade.getMoveStudentDao().getOrderTextRelation(model.getOrder().getId());
            // В процессе деперсонализации БД связь могла быть удалена
            if (rel != null)
                model.setData(rel.getText());
            else
                prepareDefaultTemplate(model);
        }
        else
            prepareDefaultTemplate(model);
    }

    private void prepareDefaultTemplate(Model model)
    {
        model.setData(MoveStudentDaoFacade.getMoveStudentDao().getTemplate(model.getOrder().getType(), 1));
    }

    @Override
    public IDocumentRenderer getOrderPrintForm(Model model)
    {
        String stateCode = model.getOrder().getState().getCode();

        String fileName = model.getFileName();
        setBarcodeAndAddNumOfPages(model);

        if (UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTABLE.equals(stateCode)
                || UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED.equals(stateCode)
                || UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(stateCode))
        {
            // выписка уже проведена и печатная форма сохранена
            if (null != model.getPrintPdf() && model.getPrintPdf())
            {
                if (StringUtils.isEmpty(fileName))
                    fileName = "StudentListOrder orderId" + String.valueOf(model.getOrder().getId()) + ".pdf";
                else
                    fileName += ".pdf";
                if (StringUtils.isEmpty(model.getBarcode()))
                    return UniReportUtils.createConvertingToPdfRenderer(fileName, model.getData());
                else
                    return UniReportUtils.createConvertingToPdfRenderer(fileName, model.getData(), model.getBarcode(), model.isAddNumOfPages());
            }
            else
            {
                if (StringUtils.isEmpty(fileName))
                    fileName = "StudentListOrder.rtf";
                else
                    fileName += ".rtf";
                return new CommonBaseRenderer().rtf().document(model.getData()).fileName(fileName);
            }
        }
        else
        {
            IPrintFormCreator<StudentListOrder> formCreator = CommonExtractPrint.getPrintFormCreator("studentListOrder_" + model.getOrder().getType().getCode() + "_orderPrint");
            if (formCreator == null)
                formCreator = CommonExtractPrint.getPrintFormCreator("studentListOrder" + model.getOrder().getType().getIndex() + "_orderPrint");

            final RtfDocument document = formCreator.createPrintForm(model.getData(), model.getOrder());
            if (null != model.getPrintPdf() && model.getPrintPdf())
            {
                if (StringUtils.isEmpty(fileName))
                    fileName = "StudentListOrder orderId" + String.valueOf(model.getOrder().getId()) + ".pdf";
                else
                    fileName += ".pdf";
                if (StringUtils.isEmpty(model.getBarcode()))
                    return UniReportUtils.createConvertingToPdfRenderer(fileName, document);
                else
                    return UniReportUtils.createConvertingToPdfRenderer(fileName, document, model.getBarcode(), model.isAddNumOfPages());
            }
            else
            {
                if (StringUtils.isEmpty(fileName))
                    fileName = "StudentListOrder.rtf";
                else
                    fileName += ".rtf";
                return new CommonBaseRenderer().rtf().document(document).fileName(fileName);
            }
        }
    }

    protected void setBarcodeAndAddNumOfPages(Model model)
    {
    }
}