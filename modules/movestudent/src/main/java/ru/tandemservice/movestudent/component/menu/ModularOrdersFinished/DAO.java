/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.menu.ModularOrdersFinished;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import ru.tandemservice.movestudent.component.commons.ExtractTypeSelectModel;
import ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder.*;
import ru.tandemservice.movestudent.component.commons.OrderCategory;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.movestudent.ui.UnimoveStudentMultiSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.dao.MoveDaoFacade;

import java.util.Arrays;
import java.util.List;

import static ru.tandemservice.movestudent.component.commons.OrderFlexBuilder.*;

/**
 * @author vip_delete
 * @since 18.10.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEducationYearList(new EducationYearModel());
        model.setStudentList(new UnimoveStudentMultiSelectModel());
        model.setEmployeePostModel(new OrderExecutorSelectModel());
        model.setExtractTypesListModel(new ExtractTypeSelectModel(OrderCategory.MODULAR));
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(Model model)
    {
        IFlexConfig extractConfig = new FlexConfig(AbstractStudentExtract.class, EXTRACT_ALIAS)
                .addColumns(new SimpleFlexColumn(PARAGRAPH_ALIAS, IAbstractParagraph.L_ORDER))
                .addJoins(new SimpleFlexJoin(PARAGRAPH_ALIAS, EXTRACT_ALIAS, IAbstractExtract.L_PARAGRAPH, true), STUDENT_JOIN)
                .addFilters(STUDENT_FILTER, EXTRACT_TYPE_FILTER);

        IFlexConfig mainConfig = new FlexConfig(StudentModularOrder.class, ORDER_ALIAS)
                .addColumns(
                        ORDER_CREATE_DATE_COLUMN,
                        ORDER_COMMIT_DATE_COLUMN,
                        ORDER_NUMBER_COLUMN,
                        ORDER_COMMIT_DATE_SYSTEM_COLUMN,
                        EXTRACT_COUNT_COLUMN,
                        ORDER_EXECUTOR_COLUMN
                )
                .addJoins(
                        new OrdersDataJoin(Arrays.asList(OrderCategory.MODULAR_ORDER_CATEGORY), DQLJoinType.inner, false, false, false, false)
                )
                .addOrders(
                        ORDER_CREATE_DATE_SORT,
                        ORDER_COMMIT_DATE_SORT,
                        ORDER_NUMBER_SORT,
                        ORDER_COMMIT_SYSTEM_DATE_SORT,
                        ORDER_EXTRACT_COUNT_SORT,
                        ORDER_EXECUTOR_SORT
                )
                .addFilters(
                        ORDER_FINISHED_FILTER,
                        ORDER_EDU_YEAR_FILTER,
                        ORDER_COMMIT_DATE_PERIOD_FILTER,
                        ORDER_CREATE_DATE_PERIOD_FILTER,
                        ORDER_COMMIT_DATE_SYSTEM_PERIOD_FILTER,
                        ORDER_NUMBER_FILTER,
                        EXECUTOR_FILTER,
                        new SubQueryFlexFilter(ORDER_ALIAS, "id", extractConfig, IAbstractParagraph.L_ORDER)
                );

        FlexDQLBuilder flexBuilder = new FlexDQLBuilder(mainConfig, false);

        flexBuilder.fillDataSource(getSession(), model.getSettings(), model.getDataSource(), new IPatchResolver()
        {
            @Override
            public void patch(List<Object[]> srcList, List<IEntity> destList, List<IFlexColumn> columns)
            {
                int numberColIdx = columns.indexOf(ORDER_NUMBER_COLUMN);
                for (int i = 0; i < srcList.size(); i++)
                {
                    Object[] cols = srcList.get(i);
                    DataWrapper wrapper = (DataWrapper) destList.get(i);
                    wrapper.setTitle("Приказ №" + cols[numberColIdx]);
                }
            }
        });
    }

    @Override
    public void deleteRow(IBusinessComponent context)
    {
        IAbstractOrder order = getNotNull(StudentModularOrder.class, (Long) context.getListenerParameter());
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(order);
    }
}
