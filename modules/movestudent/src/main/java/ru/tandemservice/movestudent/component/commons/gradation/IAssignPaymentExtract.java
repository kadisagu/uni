/* $Id$ */
package ru.tandemservice.movestudent.component.commons.gradation;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Выписка о назначении выплаты.
 *
 * @author Nikolay Fedorovskih
 * @since 15.12.2013
 */
public interface IAssignPaymentExtract
{
    /**
     * Дата начала выплаты.
     */
    Date getPaymentBeginDate();

    /**
     * Дата ококнчания выплаты.
     */
    Date getPaymentEndDate();

    /**
     * Сумма выплаты.
     */
    BigDecimal getPaymentAmount();
}