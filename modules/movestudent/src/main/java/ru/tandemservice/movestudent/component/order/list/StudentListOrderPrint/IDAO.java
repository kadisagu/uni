/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.list.StudentListOrderPrint;

import org.tandemframework.core.document.IDocumentRenderer;
import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;

/**
 * @author vip_delete
 * @since 08.04.2009
 */
public interface IDAO extends IUniDao<Model>
{
    IDocumentRenderer getOrderPrintForm(Model model);
}
