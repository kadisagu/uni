/* $Id$ */
package ru.tandemservice.movestudent.component.settings.OtherOrderTypeFieldsSettings;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.meta.entity.data.ItemPolicy;
import org.tandemframework.core.runtime.EntityDataRuntime;
import org.tandemframework.core.tool.synchronization.SynchronizeMeta;
import ru.tandemservice.movestudent.entity.OtherStudentFieldsSettings;
import ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;

import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 05.04.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        List<StudentExtractType> result = new ArrayList<>();
        for (StudentExtractType type : getList(StudentExtractType.class))
        {
            StudentExtractType parentType = type;
            while (null != parentType.getParent())
            {
                parentType = parentType.getParent();
            }
            if (StudentExtractTypeCodes.OTHER_ORDER.equals(parentType.getCode()))
                result.add(type);
        }

        Map<Long, OtherStudentFieldsSettings> fieldsSettingsMap = new HashMap<>();
        for (OtherStudentFieldsSettings settings : getList(OtherStudentFieldsSettings.class))
        {
            fieldsSettingsMap.put(settings.getStudentExtractType().getId(), settings);
        }

        Collections.sort(result, new Comparator<StudentExtractType>()
        {
            @Override
            public int compare(StudentExtractType o1, StudentExtractType o2)
            {
                Stack<StudentExtractType> parents1 = new Stack<>();
                Stack<StudentExtractType> parents2 = new Stack<>();

                while (o1 != null)
                {
                    parents1.push(o1);
                    o1 = o1.getParent();
                }

                while (o2 != null)
                {
                    parents2.push(o2);
                    o2 = o2.getParent();
                }

                int result = parents1.pop().getCode().compareTo(parents2.pop().getCode());

                if (result != 0)
                {
                    return result;
                }

                while (!parents1.empty() && !parents2.empty())
                {
                    result = parents1.pop().getTitle().compareTo(parents2.pop().getTitle());
                    if (result != 0)
                    {
                        return result;
                    }
                }

                if (!parents1.empty() || !parents2.empty())
                {
                    result = parents1.empty() ? -1 : 1;
                }

                return result;
            }
        });

        model.getDataSource().setCountRow(result.size());
        UniBaseUtils.createPage(model.getDataSource(), result);

        Map<StudentExtractType, String> groupMap = new HashMap<>();
        for (StudentExtractTypeToGroup rel : getList(StudentExtractTypeToGroup.class))
        {
            String group = groupMap.get(rel.getType());
            if (null == group)
                group = rel.getGroup().getTitle();
            else
                group = group + ", " + rel.getGroup().getTitle();
            groupMap.put(rel.getType(), group);
        }
        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            final String groupTitle = groupMap.get(wrapper.getEntity());
            final OtherStudentFieldsSettings setting = fieldsSettingsMap.get(wrapper.getId());
            wrapper.setViewProperty("group", groupTitle == null ? "" : groupTitle);
            wrapper.setViewProperty("fields", setting == null ? null : setting.getAllowedFields());
            wrapper.setViewProperty(Model.DISABLE_REVERT_PROPERTY, isRevertDisabled(setting));
        }
    }

    @Override
    public void deleteSetting(Long studentExtractTypeId)
    {
        OtherStudentFieldsSettings settings = get(OtherStudentFieldsSettings.class, OtherStudentFieldsSettings.studentExtractType().id(), studentExtractTypeId);
        if (null != settings) delete(settings);
    }

    private boolean isRevertDisabled(OtherStudentFieldsSettings settings)
    {
        if (null == settings) return true;
        ItemPolicy itemPolicy = EntityDataRuntime.getEntityPolicy(OtherStudentFieldsSettings.ENTITY_NAME).getItemPolicyById(settings.getStudentExtractType().getCode());
        return itemPolicy != null && !SynchronizeMeta.temp.equals(itemPolicy.getSynchronize());
    }
}