/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.list.StudentListParagraphAdd;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;

/**
 * @author vip_delete
 * @since 07.05.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);
    }

    public void onChangeParagraphType(IBusinessComponent component)
    {
        Model model = getModel(component);

        //deactivate(component);

        component.createDefaultChildRegion(new ComponentActivator(MoveStudentUtils.getListParagraphAddEditComponent(model.getType()), new ParametersMap()
                .add(AbstractListParagraphAddEditModel.PARAMETER_ORDER_ID, model.getOrderId())
                .add(AbstractListParagraphAddEditModel.PARAMETER_PARAGRAPH_TYPE_ID, model.getType().getId())
                .add(AbstractListParagraphAddEditModel.PARAMETER_PARAGRAPH_ID, null)
        ));
    }
}
