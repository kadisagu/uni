package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.entity.gen.AdmitToDiplomaWithoutExamStuExtractGen;

/**
 * Выписка из сборного приказа по студенту. О допуске к защите выпускной квалификационной работы
 */
public class AdmitToDiplomaWithoutExamStuExtract extends AdmitToDiplomaWithoutExamStuExtractGen
{
}