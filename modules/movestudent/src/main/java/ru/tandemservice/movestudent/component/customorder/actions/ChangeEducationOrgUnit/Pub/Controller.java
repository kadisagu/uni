/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeEducationOrgUnit.Pub;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.BaseCustomActionPubController;
import ru.tandemservice.movestudent.entity.custom.ChangeEducationOrgUnitAction;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class Controller extends BaseCustomActionPubController<ChangeEducationOrgUnitAction, IDAO, Model>
{
}