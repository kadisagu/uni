/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Session;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.EducationLevelDAO;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.CustomStateUtil;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 09.04.2009
 */
public abstract class AbstractListParagraphAddEditDAO<T extends ListStudentExtract, Model extends AbstractListParagraphAddEditModel<T>> extends UniDao<Model> implements IAbstractListParagraphAddEditDAO<T, Model>
{
    protected static final String STUDENT_ALIAS = "p";
    protected static OrderDescriptionRegistry orderRegistry = new OrderDescriptionRegistry("p");

    static
    {
        orderRegistry.setOrders(AbstractListParagraphAddEditModel.STATUS_KEY,
                                new OrderDescription("p", new String[]{Student.L_STATUS, StudentStatus.P_PRIORITY}),
                                new OrderDescription("p", Student.STUDENT_LAST_NAME), new OrderDescription("p", Student.STUDENT_FIRST_NAME), new OrderDescription("p", Student.STUDENT_MIDDLE_NAME));
        orderRegistry.setOrders(Student.FIO_KEY, new OrderDescription("p", Student.STUDENT_LAST_NAME),
                                new OrderDescription("p", Student.STUDENT_FIRST_NAME), new OrderDescription("p", Student.STUDENT_MIDDLE_NAME));
        orderRegistry.setOrders(Student.educationOrgUnit().educationLevelHighSchool().configurableTitle().s(),
                                new OrderDescription("p", new String[]{Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY}),
                                new OrderDescription("p", Student.STUDENT_LAST_NAME), new OrderDescription("p", Student.STUDENT_FIRST_NAME), new OrderDescription("p", Student.STUDENT_MIDDLE_NAME));
    }

    @Override
    public void prepare(Model model)
    {
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setStudentCustomStateCIModel(new CommonMultiSelectModel(StudentCustomStateCI.title().s())
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomStateCI.class, "st").column(property("st"))
                        .order(property(StudentCustomStateCI.title().fromAlias("st")));

                if (!StringUtils.isEmpty(filter))
                    builder.where(likeUpper(property("st", StudentCustomStateCI.P_TITLE), value(CoreStringUtils.escapeLike(filter, true))));

                if (set != null)
                    builder.where(in("st.id", set));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        if (model.getParagraphId() != null)
        {
            // форма редактирования
            model.setEditForm(true);
            model.setParagraph(this.getNotNull(model.getParagraphId()));
            model.setFirstExtract(MoveStudentDaoFacade.getMoveStudentDao().getParagraphFirstExtract(model.getParagraphId(), false));
            model.setParagraphType(model.getFirstExtract().getType());
            List<IEntity> listStudent = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                    .column(property("e", IAbstractExtract.L_ENTITY))
                    .where(eq(property("e", IAbstractExtract.L_PARAGRAPH), value(model.getParagraphId())))
		            .order(property("e", IAbstractExtract.P_NUMBER))
                    .createStatement(getSession()).list();
            model.setSelectedItemList(listStudent);
            model.setParagraphOnlyOneInTheOrder(model.getParagraph().getOrder().getParagraphCount() == 1);
        }
        else
        {
            StudentListOrder order = getNotNull(model.getOrderId());
            // форма создания
            model.setEditForm(false);
            model.setParagraph(new StudentListParagraph());
            model.getParagraph().setOrder(order);
            model.setParagraphType(getNotNull(StudentExtractType.class, model.getParagraphType().getId()));

            // Если приказ может содержать выписки разных типов, то грузим первую выписку из приказа без конкретизирования типа выписки
            if (MoveStudentDaoFacade.getMoveStudentDao().isVariousExtractTypeInOrder(order.getType()))
                model.setFirstExtract(MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(model.getOrderId()));
            else // иначе можно сделать более оптимальный разпрос, указывая нужный тип выписки
                model.setFirstExtract(MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(model.getOrderId(), false));
            model.setParagraphOnlyOneInTheOrder(model.getFirstExtract() == null);
        }

        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getParagraph().getOrder().getState().getCode()))
            throw new ApplicationException("Нельзя изменять приказ. Изменение приказа возможно только в состоянии «Формируется».");
    }

    protected List<Long> getDisabledStudentIDs(Model model, Collection<Long> studentIDs)
    {
        // TODO optimize in DEV-3640 - приджойнить признаком к основному запросу

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                .column(property("e", AbstractStudentExtract.entity().id()))
                .where(in(property("e", IAbstractExtract.L_ENTITY), studentIDs))
                .where(or(
                        isNull(property("e", IAbstractExtract.L_PARAGRAPH)),
                        and(
                                exists(
                                        new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e2").column("e2.id")
                                                .where(eq("e.id", "e2.id"))
                                                .where(notIn(property("e2", AbstractStudentExtract.paragraph().order().state().code()), UnimoveDefines.CATALOG_ORDER_STATE_FINISHED, UnimoveDefines.CATALOG_ORDER_STATE_REJECTED))
                                                .buildQuery()
                                ),
                                model.isEditForm() ? ne(property("e", IAbstractExtract.L_PARAGRAPH), value(model.getParagraph().getId())) : null
                        )
                ));

        return dql.createStatement(getSession()).list();
    }

	protected void setAdditionalViewProperties(Model model, List<Student> studentList, boolean needCustomStates)
	{
		setAdditionalViewProperties(model, model.getDataSource(), studentList, needCustomStates);
	}

    protected void setAdditionalViewProperties(Model model, DynamicListDataSource<Student> dataSource, List<Student> studentList, boolean needCustomStates)
    {
        if (studentList.size() > 0)
        {
            Collection<Long> ids = UniBaseDao.ids(studentList);

            Set<Long> disabledStudentIds = new HashSet<>(getDisabledStudentIDs(model, ids));

            //Текущие активные доп. статусы
            Map<Long, Set<StudentCustomState>> studentActiveCustomStateMap = needCustomStates ? CustomStateUtil.getActiveCustomStatesMap(ids) : null;

            // дизаблим чекбоксы у студентов, которые уже присутствуют в других приказах
            for (ViewWrapper<IEntity> wrapper : ViewWrapper.getPatchedList(dataSource))
            {
                wrapper.setViewProperty(AbstractListParagraphAddEditModel.P_STUDENT_DISABLED, disabledStudentIds.contains(wrapper.getId()));
                if (needCustomStates)
                    wrapper.setViewProperty(AbstractListParagraphAddEditModel.P_STUDENT_ACTIVE_CUSTOME_STATES, studentActiveCustomStateMap.get(wrapper.getId()));
            }
        }
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, STUDENT_ALIAS);

        CustomStateUtil.addCustomStatesFilter(builder, STUDENT_ALIAS, model.getStudentCustomStateCIs());
        patchListDataSource(builder, model);
        appendStudentsWithGroup(builder);
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.P_ARCHIVAL, Boolean.FALSE));

        orderRegistry.applyOrder(builder, model.getDataSource().getEntityOrder());
        List<Student> list = builder.getResultList(getSession());
        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);

        setAdditionalViewProperties(model, list, true);
    }

    protected abstract void patchListDataSource(MQBuilder builder, Model model);

	@Override
	public void update(Model model)
	{
		final List<Student> list = ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).getSelectedObjects().stream()
                .map(o -> (Student) ((ViewWrapper) o).getEntity())
                .collect(Collectors.toList());

		updateParagraph(model, list);
	}

    @SuppressWarnings("unchecked")
    protected void updateParagraph(Model model, List<Student> studentList)
    {
        // V A L I D A T E

        final Session session = getSession();

        session.refresh(model.getParagraph().getOrder());
        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getParagraph().getOrder().getState().getCode()))
            throw new ApplicationException("Нельзя изменять приказ. Изменение приказа возможно только в состоянии «Формируется».");

        if (studentList.isEmpty())
            throw new ApplicationException("Параграф приказа не может быть пустым. При необходимости удалите параграф приказа и создайте новый.");

        studentList.sort(Student.FULL_FIO_AND_ID_COMPARATOR);

        // пока мы были на форме state мог очень сильно поменяться, поэтому надо все данные проверить на актуаность
        for (Student student : studentList)
        {
            // студент не должен быть в другом приказе
            if (!MoveStudentDaoFacade.getMoveStudentDao().isMoveAccessible(student, model.getParagraphId()))
                throw new ApplicationException("Нельзя добавить параграф, так как у студента " + student.getPerson().getFullFio() + " уже есть непроведенные выписки.");
        }

        ErrorCollector errs = UserContext.getInstance().getErrorCollector();

        if (model.isEditForm())
        {
            // E D I T   P A R A G R A P H

            List<T> extractsList = new ArrayList<>();
            List<T> extractsToDeleteList = new ArrayList<>();

            // удаляем выписки у студентов, которые не выбранны на форме. тут специально не перенумеровываем выписки,
            // так как потом будет общая перенумерация по ФИО
            for (IAbstractExtract e : model.getParagraph().getExtractList()) {
                T extract = (T) e;
                if (!studentList.remove(extract.getEntity()))
                    extractsToDeleteList.add(extract);
                else
                {
                    model.setExtract(extract);
                    fillExtract(extract, extract.getEntity(), model);
                }
            }

            for (Student student : studentList)
            {
                // создаем выписку и заполняем в ней стандартные поля
                T extract = createAndFillExtract(student, model);
                extractsList.add(extract);
            }

            if (errs.hasErrors()) throw new ApplicationException("При заполнении формы допущена ошибка.");

            extractsToDeleteList.forEach(session::delete);
            extractsList.forEach(session::save);

        }
        else
        {
            // C R E A T E   P A R A G R A P H

            StudentListParagraph paragraph = new StudentListParagraph();
            paragraph.setOrder(model.getParagraph().getOrder());
            paragraph.setNumber(model.getParagraph().getOrder().getParagraphCount() + 1);  // номер параграфа с единицы

            model.setParagraph(paragraph);
            List<T> extractsList = new ArrayList<>();

            for (Student student : studentList)
            {
                // создаем выписку и заполняем в ней стандартные поля
                T extract = createAndFillExtract(student, model);
                extractsList.add(extract);
            }

            if (errs.hasErrors()) throw new ApplicationException("При заполнении формы допущена ошибка.");

            session.save(paragraph);
            extractsList.forEach(session::save);
        }

        // Перенумеруем все выписки, так чтобы сортировка была по ФИО
        final MutableInt counter = new MutableInt(1);
        this.getList(ListStudentExtract.class, IAbstractExtract.L_PARAGRAPH, model.getParagraph()).stream()
                .sorted(AbstractStudentExtract.FULL_FIO_AND_ID_COMPARATOR)
                .forEachOrdered(extract -> {
                    extract.setNumber(counter.intValue());
                    session.update(extract);
                    counter.increment();
                });
    }

    // конструктор выписки
    protected abstract T createNewInstance(Model model);

    // заполняем специфические параметры выписки
    protected abstract void fillExtract(T extract, Student student, Model model);

    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.NOMINATIVE;
    }

    protected T createAndFillExtract(Student student, Model model)
    {
        T extract = createNewInstance(model);

        // сохраняем данные в модели
        model.setExtract(extract);

        // в student уже есть все данные для проведения выписки
        extract.setEntity(student);
        extract.setParagraph(model.getParagraph());
        extract.setType(model.getParagraphType());
        extract.setCreateDate(model.getParagraph().getOrder().getCreateDate());
        extract.setNumber(null); // нет номера. перенумеруем в конце метода update

        // Хочу иметь возможность взять данные entity в этом методе
        fillExtract(extract, student, model);

        //обновляем fio в нужном падеже
        syncStudentTitle(model);

        //обновляем текстовые значения старых атрибутов
        syncPreCommitData(model);

        return extract;
    }

    private void syncPreCommitData(Model model)
    {
        T extract = model.getExtract();
        Student student = extract.getEntity();

        extract.setStudentTitleStr(student.getPerson().getFullFio());
        extract.setStudentStatusStr(student.getStatus().getTitle());
        extract.setPersonalNumberStr(student.getPersonalNumber());
        extract.setCourseStr(student.getCourse().getTitle());
        extract.setGroupStr(student.getGroup() == null ? "-" : student.getGroup().getTitle());
        extract.setCompensationTypeStr(student.getCompensationType().getShortTitle());
        extract.setFormativeOrgUnitStr(student.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle());
        extract.setTerritorialOrgUnitStr(student.getEducationOrgUnit().getTerritorialOrgUnit().getTerritorialShortTitle());
        extract.setEducationLevelHighSchoolStr((String) student.getEducationOrgUnit().getEducationLevelHighSchool().getProperty(EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY));
        extract.setDevelopFormStr(student.getEducationOrgUnit().getDevelopForm().getTitle());
        extract.setDevelopConditionStr(student.getEducationOrgUnit().getDevelopCondition().getTitle());
        extract.setDevelopTechStr(student.getEducationOrgUnit().getDevelopTech().getTitle());
        extract.setDevelopPeriodStr(student.getEducationOrgUnit().getDevelopPeriod().getTitle());
    }

    private void syncStudentTitle(Model model)
    {
        GrammaCase rusCase = getStudentTitleCase();
        if (rusCase != null)
        {
            IdentityCard identityCard = model.getExtract().getEntity().getPerson().getIdentityCard();
            //boolean isMaleSex = identityCard.getSex().isMale();

            InflectorVariant variant = DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(rusCase);

            CoreCollectionUtils.Pair<String, InflectorVariant> lastNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.lastName().s(), variant);
            CoreCollectionUtils.Pair<String, InflectorVariant> firstNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.firstName().s(), variant);
            CoreCollectionUtils.Pair<String, InflectorVariant> middleNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.middleName().s(), variant);

            String lastName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(lastNamePairKey);
            String firstName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(firstNamePairKey);
            String middleName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(middleNamePairKey);

            StringBuilder str = new StringBuilder(lastName);
            str.append(" ").append(firstName);
            if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
                str.append(" ").append(middleName);
            model.getExtract().setStudentTitle(str.toString());
        }
        else
            model.getExtract().setStudentTitle(model.getExtract().getEntity().getPerson().getFullFio());
    }

    protected void appendStudentsWithGroup(MQBuilder builder)
    {
        builder.add(MQExpression.isNotNull(STUDENT_ALIAS, Student.L_GROUP));
    }
}