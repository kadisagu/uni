/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeCompensationType.AddEdit;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditModel;
import ru.tandemservice.movestudent.entity.custom.ChangeCompensationTypeAction;
import ru.tandemservice.uni.entity.catalog.CompensationType;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public class Model extends BaseCustomActionAddEditModel<ChangeCompensationTypeAction>
{
    private List<CompensationType> _newCompensationTypeList;

    public List<CompensationType> getNewCompensationTypeList()
    {
        return _newCompensationTypeList;
    }

    public void setNewCompensationTypeList(List<CompensationType> newCompensationTypeList)
    {
        _newCompensationTypeList = newCompensationTypeList;
    }
}