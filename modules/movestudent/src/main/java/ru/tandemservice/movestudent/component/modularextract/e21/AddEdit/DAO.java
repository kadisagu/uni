/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e21.AddEdit;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.ProlongWeekndChildStuExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 08.05.2009
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<ProlongWeekndChildStuExtract, Model>
{
    @Override
    protected ProlongWeekndChildStuExtract createNewInstance()
    {
        return new ProlongWeekndChildStuExtract();
    }
    
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        List<String> statusCodes = new ArrayList<String>();

        statusCodes.add(UniDefines.CATALOG_STUDENT_STATUS_CHILD);
        statusCodes.add(UniDefines.CATALOG_STUDENT_STATUS_CHILD_WITH_ATTENDANCE);
        statusCodes.add(UniDefines.CATALOG_STUDENT_STATUS_CHILD_WITHOUT_ATTENDANCE);

        List<StudentStatus> statusList = new DQLSelectBuilder()
                .fromEntity(StudentStatus.class, "ss")
                .where(DQLExpressions.eq(DQLExpressions.property(StudentStatus.usedInSystem().fromAlias("ss")), DQLExpressions.value(Boolean.TRUE)))
                .where(DQLExpressions.in(DQLExpressions.property(StudentStatus.code().fromAlias("ss")), statusCodes))
                .order(DQLExpressions.property(StudentStatus.code().fromAlias("ss")))
                .createStatement(getSession())
                .list();

        model.setStudentStatusList(statusList);

        if (model.isAddForm())
        {
            ProlongWeekndChildStuExtract extract = model.getExtract();

            if (statusList.size() == 0)
            {
                extract.setStudentStatusNew(null);

            } else
            {
                Map<String, StudentStatus> studentStatusMap = new HashMap<String, StudentStatus>();
                for (StudentStatus studentStatus : statusList)
                    studentStatusMap.put(studentStatus.getCode(), studentStatus);

                if (studentStatusMap.containsKey(UniDefines.CATALOG_STUDENT_STATUS_CHILD))
                {
                    extract.setStudentStatusNew(studentStatusMap.get(UniDefines.CATALOG_STUDENT_STATUS_CHILD));

                } else if (studentStatusMap.containsKey(UniDefines.CATALOG_STUDENT_STATUS_CHILD_WITHOUT_ATTENDANCE))
                {
                    extract.setStudentStatusNew(studentStatusMap.get(UniDefines.CATALOG_STUDENT_STATUS_CHILD_WITHOUT_ATTENDANCE));

                } else
                {
                    extract.setStudentStatusNew(studentStatusMap.get(UniDefines.CATALOG_STUDENT_STATUS_CHILD_WITH_ATTENDANCE));
                }
            }
        }
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setStudentStatusOld(model.getExtract().getEntity().getStatus());
        super.update(model);
    }
}