package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.GiveDiplIncompleteStuListExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О выдаче диплома о неполном высшем профессиональном образовании
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GiveDiplIncompleteStuListExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.GiveDiplIncompleteStuListExtract";
    public static final String ENTITY_NAME = "giveDiplIncompleteStuListExtract";
    public static final int VERSION_HASH = 859573222;
    private static IEntityMeta ENTITY_META;

    public static final String P_DISMISS_DATE = "dismissDate";
    public static final String P_DISCHARGE_ORDER_NUMBER = "dischargeOrderNumber";
    public static final String P_BEGIN_YEAR = "beginYear";
    public static final String P_END_YEAR = "endYear";

    private Date _dismissDate;     // Дата отчисления
    private String _dischargeOrderNumber;     // № приказа
    private Date _beginYear;     // Начало обучения
    private Date _endYear;     // Окончание обучения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата отчисления.
     */
    public Date getDismissDate()
    {
        return _dismissDate;
    }

    /**
     * @param dismissDate Дата отчисления.
     */
    public void setDismissDate(Date dismissDate)
    {
        dirty(_dismissDate, dismissDate);
        _dismissDate = dismissDate;
    }

    /**
     * @return № приказа.
     */
    @Length(max=255)
    public String getDischargeOrderNumber()
    {
        return _dischargeOrderNumber;
    }

    /**
     * @param dischargeOrderNumber № приказа.
     */
    public void setDischargeOrderNumber(String dischargeOrderNumber)
    {
        dirty(_dischargeOrderNumber, dischargeOrderNumber);
        _dischargeOrderNumber = dischargeOrderNumber;
    }

    /**
     * @return Начало обучения.
     */
    public Date getBeginYear()
    {
        return _beginYear;
    }

    /**
     * @param beginYear Начало обучения.
     */
    public void setBeginYear(Date beginYear)
    {
        dirty(_beginYear, beginYear);
        _beginYear = beginYear;
    }

    /**
     * @return Окончание обучения.
     */
    public Date getEndYear()
    {
        return _endYear;
    }

    /**
     * @param endYear Окончание обучения.
     */
    public void setEndYear(Date endYear)
    {
        dirty(_endYear, endYear);
        _endYear = endYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof GiveDiplIncompleteStuListExtractGen)
        {
            setDismissDate(((GiveDiplIncompleteStuListExtract)another).getDismissDate());
            setDischargeOrderNumber(((GiveDiplIncompleteStuListExtract)another).getDischargeOrderNumber());
            setBeginYear(((GiveDiplIncompleteStuListExtract)another).getBeginYear());
            setEndYear(((GiveDiplIncompleteStuListExtract)another).getEndYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GiveDiplIncompleteStuListExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GiveDiplIncompleteStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new GiveDiplIncompleteStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "dismissDate":
                    return obj.getDismissDate();
                case "dischargeOrderNumber":
                    return obj.getDischargeOrderNumber();
                case "beginYear":
                    return obj.getBeginYear();
                case "endYear":
                    return obj.getEndYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "dismissDate":
                    obj.setDismissDate((Date) value);
                    return;
                case "dischargeOrderNumber":
                    obj.setDischargeOrderNumber((String) value);
                    return;
                case "beginYear":
                    obj.setBeginYear((Date) value);
                    return;
                case "endYear":
                    obj.setEndYear((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dismissDate":
                        return true;
                case "dischargeOrderNumber":
                        return true;
                case "beginYear":
                        return true;
                case "endYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dismissDate":
                    return true;
                case "dischargeOrderNumber":
                    return true;
                case "beginYear":
                    return true;
                case "endYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "dismissDate":
                    return Date.class;
                case "dischargeOrderNumber":
                    return String.class;
                case "beginYear":
                    return Date.class;
                case "endYear":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GiveDiplIncompleteStuListExtract> _dslPath = new Path<GiveDiplIncompleteStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GiveDiplIncompleteStuListExtract");
    }
            

    /**
     * @return Дата отчисления.
     * @see ru.tandemservice.movestudent.entity.GiveDiplIncompleteStuListExtract#getDismissDate()
     */
    public static PropertyPath<Date> dismissDate()
    {
        return _dslPath.dismissDate();
    }

    /**
     * @return № приказа.
     * @see ru.tandemservice.movestudent.entity.GiveDiplIncompleteStuListExtract#getDischargeOrderNumber()
     */
    public static PropertyPath<String> dischargeOrderNumber()
    {
        return _dslPath.dischargeOrderNumber();
    }

    /**
     * @return Начало обучения.
     * @see ru.tandemservice.movestudent.entity.GiveDiplIncompleteStuListExtract#getBeginYear()
     */
    public static PropertyPath<Date> beginYear()
    {
        return _dslPath.beginYear();
    }

    /**
     * @return Окончание обучения.
     * @see ru.tandemservice.movestudent.entity.GiveDiplIncompleteStuListExtract#getEndYear()
     */
    public static PropertyPath<Date> endYear()
    {
        return _dslPath.endYear();
    }

    public static class Path<E extends GiveDiplIncompleteStuListExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _dismissDate;
        private PropertyPath<String> _dischargeOrderNumber;
        private PropertyPath<Date> _beginYear;
        private PropertyPath<Date> _endYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата отчисления.
     * @see ru.tandemservice.movestudent.entity.GiveDiplIncompleteStuListExtract#getDismissDate()
     */
        public PropertyPath<Date> dismissDate()
        {
            if(_dismissDate == null )
                _dismissDate = new PropertyPath<Date>(GiveDiplIncompleteStuListExtractGen.P_DISMISS_DATE, this);
            return _dismissDate;
        }

    /**
     * @return № приказа.
     * @see ru.tandemservice.movestudent.entity.GiveDiplIncompleteStuListExtract#getDischargeOrderNumber()
     */
        public PropertyPath<String> dischargeOrderNumber()
        {
            if(_dischargeOrderNumber == null )
                _dischargeOrderNumber = new PropertyPath<String>(GiveDiplIncompleteStuListExtractGen.P_DISCHARGE_ORDER_NUMBER, this);
            return _dischargeOrderNumber;
        }

    /**
     * @return Начало обучения.
     * @see ru.tandemservice.movestudent.entity.GiveDiplIncompleteStuListExtract#getBeginYear()
     */
        public PropertyPath<Date> beginYear()
        {
            if(_beginYear == null )
                _beginYear = new PropertyPath<Date>(GiveDiplIncompleteStuListExtractGen.P_BEGIN_YEAR, this);
            return _beginYear;
        }

    /**
     * @return Окончание обучения.
     * @see ru.tandemservice.movestudent.entity.GiveDiplIncompleteStuListExtract#getEndYear()
     */
        public PropertyPath<Date> endYear()
        {
            if(_endYear == null )
                _endYear = new PropertyPath<Date>(GiveDiplIncompleteStuListExtractGen.P_END_YEAR, this);
            return _endYear;
        }

        public Class getEntityClass()
        {
            return GiveDiplIncompleteStuListExtract.class;
        }

        public String getEntityName()
        {
            return "giveDiplIncompleteStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
