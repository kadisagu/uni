package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.RestorationCourseStuExtract;
import ru.tandemservice.uni.entity.catalog.Term;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О восстановлении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RestorationCourseStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.RestorationCourseStuExtract";
    public static final String ENTITY_NAME = "restorationCourseStuExtract";
    public static final int VERSION_HASH = -2064764013;
    private static IEntityMeta ENTITY_META;

    public static final String P_RESTORATION_DATE = "restorationDate";
    public static final String P_DISMISS_DATE = "dismissDate";
    public static final String P_DISMISS_ORDER = "dismissOrder";
    public static final String P_DISMISS_ORDER_DATE = "dismissOrderDate";
    public static final String P_APPLY_DATE = "applyDate";
    public static final String P_UNREASONABLE_DISMISS_EXCUSE = "unreasonableDismissExcuse";
    public static final String P_HAS_DEBTS = "hasDebts";
    public static final String P_DEADLINE_DATE = "deadlineDate";
    public static final String P_DISMISSED_FROM = "dismissedFrom";
    public static final String P_DISMISS_REASON = "dismissReason";
    public static final String P_PRINT_RESTORATION_DATE_AND_TERM = "printRestorationDateAndTerm";
    public static final String L_RESTORATION_TERM = "restorationTerm";

    private Date _restorationDate;     // Дата восстановления
    private Date _dismissDate;     // Дата отчисления
    private String _dismissOrder;     // Приказ об отчислении
    private Date _dismissOrderDate;     // Дата приказа об отчислении
    private Date _applyDate;     // Дата подачи заявления
    private boolean _unreasonableDismissExcuse;     // Отчисление по неуважительной причине
    private boolean _hasDebts;     // Разница в учебных планах
    private Date _deadlineDate;     // Срок ликвидации задолженности
    private String _dismissedFrom;     // Отчислен из (название ОУ)
    private String _dismissReason;     // Причина отчисления
    private boolean _printRestorationDateAndTerm;     // Выводить в текст приказа дату и семестр восстановления
    private Term _restorationTerm;     // Семестр восстановления

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата восстановления. Свойство не может быть null.
     */
    @NotNull
    public Date getRestorationDate()
    {
        return _restorationDate;
    }

    /**
     * @param restorationDate Дата восстановления. Свойство не может быть null.
     */
    public void setRestorationDate(Date restorationDate)
    {
        dirty(_restorationDate, restorationDate);
        _restorationDate = restorationDate;
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     */
    @NotNull
    public Date getDismissDate()
    {
        return _dismissDate;
    }

    /**
     * @param dismissDate Дата отчисления. Свойство не может быть null.
     */
    public void setDismissDate(Date dismissDate)
    {
        dirty(_dismissDate, dismissDate);
        _dismissDate = dismissDate;
    }

    /**
     * @return Приказ об отчислении. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDismissOrder()
    {
        return _dismissOrder;
    }

    /**
     * @param dismissOrder Приказ об отчислении. Свойство не может быть null.
     */
    public void setDismissOrder(String dismissOrder)
    {
        dirty(_dismissOrder, dismissOrder);
        _dismissOrder = dismissOrder;
    }

    /**
     * @return Дата приказа об отчислении. Свойство не может быть null.
     */
    @NotNull
    public Date getDismissOrderDate()
    {
        return _dismissOrderDate;
    }

    /**
     * @param dismissOrderDate Дата приказа об отчислении. Свойство не может быть null.
     */
    public void setDismissOrderDate(Date dismissOrderDate)
    {
        dirty(_dismissOrderDate, dismissOrderDate);
        _dismissOrderDate = dismissOrderDate;
    }

    /**
     * @return Дата подачи заявления. Свойство не может быть null.
     */
    @NotNull
    public Date getApplyDate()
    {
        return _applyDate;
    }

    /**
     * @param applyDate Дата подачи заявления. Свойство не может быть null.
     */
    public void setApplyDate(Date applyDate)
    {
        dirty(_applyDate, applyDate);
        _applyDate = applyDate;
    }

    /**
     * @return Отчисление по неуважительной причине. Свойство не может быть null.
     */
    @NotNull
    public boolean isUnreasonableDismissExcuse()
    {
        return _unreasonableDismissExcuse;
    }

    /**
     * @param unreasonableDismissExcuse Отчисление по неуважительной причине. Свойство не может быть null.
     */
    public void setUnreasonableDismissExcuse(boolean unreasonableDismissExcuse)
    {
        dirty(_unreasonableDismissExcuse, unreasonableDismissExcuse);
        _unreasonableDismissExcuse = unreasonableDismissExcuse;
    }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     */
    @NotNull
    public boolean isHasDebts()
    {
        return _hasDebts;
    }

    /**
     * @param hasDebts Разница в учебных планах. Свойство не может быть null.
     */
    public void setHasDebts(boolean hasDebts)
    {
        dirty(_hasDebts, hasDebts);
        _hasDebts = hasDebts;
    }

    /**
     * @return Срок ликвидации задолженности.
     */
    public Date getDeadlineDate()
    {
        return _deadlineDate;
    }

    /**
     * @param deadlineDate Срок ликвидации задолженности.
     */
    public void setDeadlineDate(Date deadlineDate)
    {
        dirty(_deadlineDate, deadlineDate);
        _deadlineDate = deadlineDate;
    }

    /**
     * @return Отчислен из (название ОУ).
     */
    @Length(max=255)
    public String getDismissedFrom()
    {
        return _dismissedFrom;
    }

    /**
     * @param dismissedFrom Отчислен из (название ОУ).
     */
    public void setDismissedFrom(String dismissedFrom)
    {
        dirty(_dismissedFrom, dismissedFrom);
        _dismissedFrom = dismissedFrom;
    }

    /**
     * @return Причина отчисления.
     */
    @Length(max=255)
    public String getDismissReason()
    {
        return _dismissReason;
    }

    /**
     * @param dismissReason Причина отчисления.
     */
    public void setDismissReason(String dismissReason)
    {
        dirty(_dismissReason, dismissReason);
        _dismissReason = dismissReason;
    }

    /**
     * @return Выводить в текст приказа дату и семестр восстановления. Свойство не может быть null.
     */
    @NotNull
    public boolean isPrintRestorationDateAndTerm()
    {
        return _printRestorationDateAndTerm;
    }

    /**
     * @param printRestorationDateAndTerm Выводить в текст приказа дату и семестр восстановления. Свойство не может быть null.
     */
    public void setPrintRestorationDateAndTerm(boolean printRestorationDateAndTerm)
    {
        dirty(_printRestorationDateAndTerm, printRestorationDateAndTerm);
        _printRestorationDateAndTerm = printRestorationDateAndTerm;
    }

    /**
     * @return Семестр восстановления.
     */
    public Term getRestorationTerm()
    {
        return _restorationTerm;
    }

    /**
     * @param restorationTerm Семестр восстановления.
     */
    public void setRestorationTerm(Term restorationTerm)
    {
        dirty(_restorationTerm, restorationTerm);
        _restorationTerm = restorationTerm;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RestorationCourseStuExtractGen)
        {
            setRestorationDate(((RestorationCourseStuExtract)another).getRestorationDate());
            setDismissDate(((RestorationCourseStuExtract)another).getDismissDate());
            setDismissOrder(((RestorationCourseStuExtract)another).getDismissOrder());
            setDismissOrderDate(((RestorationCourseStuExtract)another).getDismissOrderDate());
            setApplyDate(((RestorationCourseStuExtract)another).getApplyDate());
            setUnreasonableDismissExcuse(((RestorationCourseStuExtract)another).isUnreasonableDismissExcuse());
            setHasDebts(((RestorationCourseStuExtract)another).isHasDebts());
            setDeadlineDate(((RestorationCourseStuExtract)another).getDeadlineDate());
            setDismissedFrom(((RestorationCourseStuExtract)another).getDismissedFrom());
            setDismissReason(((RestorationCourseStuExtract)another).getDismissReason());
            setPrintRestorationDateAndTerm(((RestorationCourseStuExtract)another).isPrintRestorationDateAndTerm());
            setRestorationTerm(((RestorationCourseStuExtract)another).getRestorationTerm());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RestorationCourseStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RestorationCourseStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new RestorationCourseStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "restorationDate":
                    return obj.getRestorationDate();
                case "dismissDate":
                    return obj.getDismissDate();
                case "dismissOrder":
                    return obj.getDismissOrder();
                case "dismissOrderDate":
                    return obj.getDismissOrderDate();
                case "applyDate":
                    return obj.getApplyDate();
                case "unreasonableDismissExcuse":
                    return obj.isUnreasonableDismissExcuse();
                case "hasDebts":
                    return obj.isHasDebts();
                case "deadlineDate":
                    return obj.getDeadlineDate();
                case "dismissedFrom":
                    return obj.getDismissedFrom();
                case "dismissReason":
                    return obj.getDismissReason();
                case "printRestorationDateAndTerm":
                    return obj.isPrintRestorationDateAndTerm();
                case "restorationTerm":
                    return obj.getRestorationTerm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "restorationDate":
                    obj.setRestorationDate((Date) value);
                    return;
                case "dismissDate":
                    obj.setDismissDate((Date) value);
                    return;
                case "dismissOrder":
                    obj.setDismissOrder((String) value);
                    return;
                case "dismissOrderDate":
                    obj.setDismissOrderDate((Date) value);
                    return;
                case "applyDate":
                    obj.setApplyDate((Date) value);
                    return;
                case "unreasonableDismissExcuse":
                    obj.setUnreasonableDismissExcuse((Boolean) value);
                    return;
                case "hasDebts":
                    obj.setHasDebts((Boolean) value);
                    return;
                case "deadlineDate":
                    obj.setDeadlineDate((Date) value);
                    return;
                case "dismissedFrom":
                    obj.setDismissedFrom((String) value);
                    return;
                case "dismissReason":
                    obj.setDismissReason((String) value);
                    return;
                case "printRestorationDateAndTerm":
                    obj.setPrintRestorationDateAndTerm((Boolean) value);
                    return;
                case "restorationTerm":
                    obj.setRestorationTerm((Term) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "restorationDate":
                        return true;
                case "dismissDate":
                        return true;
                case "dismissOrder":
                        return true;
                case "dismissOrderDate":
                        return true;
                case "applyDate":
                        return true;
                case "unreasonableDismissExcuse":
                        return true;
                case "hasDebts":
                        return true;
                case "deadlineDate":
                        return true;
                case "dismissedFrom":
                        return true;
                case "dismissReason":
                        return true;
                case "printRestorationDateAndTerm":
                        return true;
                case "restorationTerm":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "restorationDate":
                    return true;
                case "dismissDate":
                    return true;
                case "dismissOrder":
                    return true;
                case "dismissOrderDate":
                    return true;
                case "applyDate":
                    return true;
                case "unreasonableDismissExcuse":
                    return true;
                case "hasDebts":
                    return true;
                case "deadlineDate":
                    return true;
                case "dismissedFrom":
                    return true;
                case "dismissReason":
                    return true;
                case "printRestorationDateAndTerm":
                    return true;
                case "restorationTerm":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "restorationDate":
                    return Date.class;
                case "dismissDate":
                    return Date.class;
                case "dismissOrder":
                    return String.class;
                case "dismissOrderDate":
                    return Date.class;
                case "applyDate":
                    return Date.class;
                case "unreasonableDismissExcuse":
                    return Boolean.class;
                case "hasDebts":
                    return Boolean.class;
                case "deadlineDate":
                    return Date.class;
                case "dismissedFrom":
                    return String.class;
                case "dismissReason":
                    return String.class;
                case "printRestorationDateAndTerm":
                    return Boolean.class;
                case "restorationTerm":
                    return Term.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RestorationCourseStuExtract> _dslPath = new Path<RestorationCourseStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RestorationCourseStuExtract");
    }
            

    /**
     * @return Дата восстановления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#getRestorationDate()
     */
    public static PropertyPath<Date> restorationDate()
    {
        return _dslPath.restorationDate();
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#getDismissDate()
     */
    public static PropertyPath<Date> dismissDate()
    {
        return _dslPath.dismissDate();
    }

    /**
     * @return Приказ об отчислении. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#getDismissOrder()
     */
    public static PropertyPath<String> dismissOrder()
    {
        return _dslPath.dismissOrder();
    }

    /**
     * @return Дата приказа об отчислении. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#getDismissOrderDate()
     */
    public static PropertyPath<Date> dismissOrderDate()
    {
        return _dslPath.dismissOrderDate();
    }

    /**
     * @return Дата подачи заявления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#getApplyDate()
     */
    public static PropertyPath<Date> applyDate()
    {
        return _dslPath.applyDate();
    }

    /**
     * @return Отчисление по неуважительной причине. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#isUnreasonableDismissExcuse()
     */
    public static PropertyPath<Boolean> unreasonableDismissExcuse()
    {
        return _dslPath.unreasonableDismissExcuse();
    }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#isHasDebts()
     */
    public static PropertyPath<Boolean> hasDebts()
    {
        return _dslPath.hasDebts();
    }

    /**
     * @return Срок ликвидации задолженности.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#getDeadlineDate()
     */
    public static PropertyPath<Date> deadlineDate()
    {
        return _dslPath.deadlineDate();
    }

    /**
     * @return Отчислен из (название ОУ).
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#getDismissedFrom()
     */
    public static PropertyPath<String> dismissedFrom()
    {
        return _dslPath.dismissedFrom();
    }

    /**
     * @return Причина отчисления.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#getDismissReason()
     */
    public static PropertyPath<String> dismissReason()
    {
        return _dslPath.dismissReason();
    }

    /**
     * @return Выводить в текст приказа дату и семестр восстановления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#isPrintRestorationDateAndTerm()
     */
    public static PropertyPath<Boolean> printRestorationDateAndTerm()
    {
        return _dslPath.printRestorationDateAndTerm();
    }

    /**
     * @return Семестр восстановления.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#getRestorationTerm()
     */
    public static Term.Path<Term> restorationTerm()
    {
        return _dslPath.restorationTerm();
    }

    public static class Path<E extends RestorationCourseStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<Date> _restorationDate;
        private PropertyPath<Date> _dismissDate;
        private PropertyPath<String> _dismissOrder;
        private PropertyPath<Date> _dismissOrderDate;
        private PropertyPath<Date> _applyDate;
        private PropertyPath<Boolean> _unreasonableDismissExcuse;
        private PropertyPath<Boolean> _hasDebts;
        private PropertyPath<Date> _deadlineDate;
        private PropertyPath<String> _dismissedFrom;
        private PropertyPath<String> _dismissReason;
        private PropertyPath<Boolean> _printRestorationDateAndTerm;
        private Term.Path<Term> _restorationTerm;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата восстановления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#getRestorationDate()
     */
        public PropertyPath<Date> restorationDate()
        {
            if(_restorationDate == null )
                _restorationDate = new PropertyPath<Date>(RestorationCourseStuExtractGen.P_RESTORATION_DATE, this);
            return _restorationDate;
        }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#getDismissDate()
     */
        public PropertyPath<Date> dismissDate()
        {
            if(_dismissDate == null )
                _dismissDate = new PropertyPath<Date>(RestorationCourseStuExtractGen.P_DISMISS_DATE, this);
            return _dismissDate;
        }

    /**
     * @return Приказ об отчислении. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#getDismissOrder()
     */
        public PropertyPath<String> dismissOrder()
        {
            if(_dismissOrder == null )
                _dismissOrder = new PropertyPath<String>(RestorationCourseStuExtractGen.P_DISMISS_ORDER, this);
            return _dismissOrder;
        }

    /**
     * @return Дата приказа об отчислении. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#getDismissOrderDate()
     */
        public PropertyPath<Date> dismissOrderDate()
        {
            if(_dismissOrderDate == null )
                _dismissOrderDate = new PropertyPath<Date>(RestorationCourseStuExtractGen.P_DISMISS_ORDER_DATE, this);
            return _dismissOrderDate;
        }

    /**
     * @return Дата подачи заявления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#getApplyDate()
     */
        public PropertyPath<Date> applyDate()
        {
            if(_applyDate == null )
                _applyDate = new PropertyPath<Date>(RestorationCourseStuExtractGen.P_APPLY_DATE, this);
            return _applyDate;
        }

    /**
     * @return Отчисление по неуважительной причине. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#isUnreasonableDismissExcuse()
     */
        public PropertyPath<Boolean> unreasonableDismissExcuse()
        {
            if(_unreasonableDismissExcuse == null )
                _unreasonableDismissExcuse = new PropertyPath<Boolean>(RestorationCourseStuExtractGen.P_UNREASONABLE_DISMISS_EXCUSE, this);
            return _unreasonableDismissExcuse;
        }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#isHasDebts()
     */
        public PropertyPath<Boolean> hasDebts()
        {
            if(_hasDebts == null )
                _hasDebts = new PropertyPath<Boolean>(RestorationCourseStuExtractGen.P_HAS_DEBTS, this);
            return _hasDebts;
        }

    /**
     * @return Срок ликвидации задолженности.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#getDeadlineDate()
     */
        public PropertyPath<Date> deadlineDate()
        {
            if(_deadlineDate == null )
                _deadlineDate = new PropertyPath<Date>(RestorationCourseStuExtractGen.P_DEADLINE_DATE, this);
            return _deadlineDate;
        }

    /**
     * @return Отчислен из (название ОУ).
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#getDismissedFrom()
     */
        public PropertyPath<String> dismissedFrom()
        {
            if(_dismissedFrom == null )
                _dismissedFrom = new PropertyPath<String>(RestorationCourseStuExtractGen.P_DISMISSED_FROM, this);
            return _dismissedFrom;
        }

    /**
     * @return Причина отчисления.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#getDismissReason()
     */
        public PropertyPath<String> dismissReason()
        {
            if(_dismissReason == null )
                _dismissReason = new PropertyPath<String>(RestorationCourseStuExtractGen.P_DISMISS_REASON, this);
            return _dismissReason;
        }

    /**
     * @return Выводить в текст приказа дату и семестр восстановления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#isPrintRestorationDateAndTerm()
     */
        public PropertyPath<Boolean> printRestorationDateAndTerm()
        {
            if(_printRestorationDateAndTerm == null )
                _printRestorationDateAndTerm = new PropertyPath<Boolean>(RestorationCourseStuExtractGen.P_PRINT_RESTORATION_DATE_AND_TERM, this);
            return _printRestorationDateAndTerm;
        }

    /**
     * @return Семестр восстановления.
     * @see ru.tandemservice.movestudent.entity.RestorationCourseStuExtract#getRestorationTerm()
     */
        public Term.Path<Term> restorationTerm()
        {
            if(_restorationTerm == null )
                _restorationTerm = new Term.Path<Term>(L_RESTORATION_TERM, this);
            return _restorationTerm;
        }

        public Class getEntityClass()
        {
            return RestorationCourseStuExtract.class;
        }

        public String getEntityName()
        {
            return "restorationCourseStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
