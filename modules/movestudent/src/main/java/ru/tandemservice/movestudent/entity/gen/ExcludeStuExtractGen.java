package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ExcludeStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об отчислении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExcludeStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ExcludeStuExtract";
    public static final String ENTITY_NAME = "excludeStuExtract";
    public static final int VERSION_HASH = -1414048268;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATE = "date";
    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";

    private Date _date;     // Дата отчисления
    private StudentStatus _studentStatusOld;     // Предыдущее состояние студента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата отчисления. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Предыдущее состояние студента. Свойство не может быть null.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ExcludeStuExtractGen)
        {
            setDate(((ExcludeStuExtract)another).getDate());
            setStudentStatusOld(((ExcludeStuExtract)another).getStudentStatusOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExcludeStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExcludeStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new ExcludeStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "date":
                    return obj.getDate();
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "date":
                        return true;
                case "studentStatusOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "date":
                    return true;
                case "studentStatusOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "date":
                    return Date.class;
                case "studentStatusOld":
                    return StudentStatus.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExcludeStuExtract> _dslPath = new Path<ExcludeStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExcludeStuExtract");
    }
            

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeStuExtract#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeStuExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    public static class Path<E extends ExcludeStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _date;
        private StudentStatus.Path<StudentStatus> _studentStatusOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeStuExtract#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(ExcludeStuExtractGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeStuExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

        public Class getEntityClass()
        {
            return ExcludeStuExtract.class;
        }

        public String getEntityName()
        {
            return "excludeStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
