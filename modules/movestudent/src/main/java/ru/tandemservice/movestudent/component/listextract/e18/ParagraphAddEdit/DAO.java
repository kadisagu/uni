/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e18.ParagraphAddEdit;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.SplitStudentsStuListExtract;
import ru.tandemservice.movestudent.utils.ExtEducationLevelsHighSchoolSelectModel;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author ListExtractComponentGenerator
 * @since 30.07.2009
 */
public class DAO extends AbstractListParagraphAddEditDAO<SplitStudentsStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));

        SplitStudentsStuListExtract firstExtract = model.getFirstExtract();
       if (null != firstExtract)
        {
            model.setGroup(firstExtract.getEntity().getGroup());
            model.setCourse(firstExtract.getEntity().getGroup().getCourse());
            model.setEducationLevelsHighSchool(firstExtract.getEducationOrgUnitNew().getEducationLevelHighSchool());
        }

        model.setSpecializationsListModel(new ExtEducationLevelsHighSchoolSelectModel(model, model).dependFromFUTS(true));
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
    }

    @Override
    protected SplitStudentsStuListExtract createNewInstance(Model model)
    {
        return new SplitStudentsStuListExtract();
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        if (!model.getEducationLevelsHighSchool().getEducationLevel().getLevelType().isAllowStudents())
            throw new ApplicationException("Для группы указан неверный уровень образования.");
    }

    @Override
    protected void fillExtract(SplitStudentsStuListExtract extract, Student student, Model model)
    {
        extract.setEducationOrgUnitOld(student.getEducationOrgUnit());
        extract.setEducationOrgUnitNew(UniDaoFacade.getEducationLevelDao().getEducationOrgUnit(model));
    }
}