/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.settings.UsedStudentExtractTypes;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;

/**
 * @author dseleznev
 * Created on: 12.12.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareListDataSource(component);
    }

    protected void prepareListDataSource(IBusinessComponent component)
    {
        boolean admin = UserContext.getInstance().getPrincipalContext().isAdmin();
        DynamicListDataSource<StudentExtractType> dataSource = new DynamicListDataSource<StudentExtractType>(component, this);
        dataSource.addColumn(new SimpleColumn("Название", admin ? StudentExtractType.titleWithCode().s() : StudentExtractType.title().s()).setClickable(false).setOrderable(false).setTreeColumn(true));
        dataSource.addColumn(new SimpleColumn("Описание", StudentExtractType.description().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Группа приказов", "group").setClickable(false).setOrderable(false));
        dataSource.addColumn(new ToggleColumn("Использовать", StudentExtractType.active().s()).setListener("onClickActive").setDisabledProperty(Model.ACTIVATION_DISABLED));
        getModel(component).setDataSource(dataSource);
    }

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickActive(IBusinessComponent component)
    {
        getDao().updateActive((Long)component.getListenerParameter());
    }
}