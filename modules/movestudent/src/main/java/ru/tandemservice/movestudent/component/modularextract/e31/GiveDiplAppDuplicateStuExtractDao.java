/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e31;

import java.util.Map;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.GiveDiplAppDuplicateStuExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 18.05.2009
 */
public class GiveDiplAppDuplicateStuExtractDao extends UniBaseDao implements IExtractComponentDao<GiveDiplAppDuplicateStuExtract>
{
    @Override
    public void doCommit(GiveDiplAppDuplicateStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);
        //do nothing
    }

    @Override
    public void doRollback(GiveDiplAppDuplicateStuExtract extract, Map parameters)
    {
        //do nothing
    }
}