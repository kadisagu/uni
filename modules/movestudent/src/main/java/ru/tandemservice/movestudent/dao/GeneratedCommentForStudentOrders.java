/* $Id$ */
package ru.tandemservice.movestudent.dao;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.customorder.CustomOrderUtils;
import ru.tandemservice.movestudent.component.modularextract.e85.GiveDiplomaStuExtractPrint;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.movestudent.entity.custom.ModifyListStuExtract;
import ru.tandemservice.movestudent.entity.custom.ModifyModularStuExtract;
import ru.tandemservice.movestudent.utils.SessionPartModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.DevelopConditionCodes;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.catalog.codes.DevelopTechCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.formatters.RuMoneyFormatter;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.UniStringUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 03.10.2013
 */
public class GeneratedCommentForStudentOrders implements IGeneratedCommentForStudentOrders
{
    ///// Выписки из кастомных приказов по студентам /////

    @Override
    public String getComment(ModifyModularStuExtract extract, boolean isPrintOrder)
    {
        String preambleOrder = CustomOrderUtils.getDescriptionPreamble(extract.getType().getCode());
        AbstractStudentExtract modifiableExtract = extract.getModifiableExtract();
        AbstractStudentOrder order = modifiableExtract.getParagraph()!= null? modifiableExtract.getParagraph().getOrder() : null;
        String modifiableOrderStr = null != order? " от " + date(order.getCommitDate()) + " № " + order.getNumber() : "";
        return preambleOrder + reason(extract.getReason()) + " «" + modifiableExtract.getType().getTitle() + "» " + modifiableOrderStr + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ModifyListStuExtract extract, boolean isPrintOrder)
    {
        String preambleOrder = CustomOrderUtils.getDescriptionPreamble(extract.getType().getCode());
        AbstractStudentExtract modifiableExtract = extract.getModifiableExtract();
        AbstractStudentOrder order = modifiableExtract.getParagraph()!= null? modifiableExtract.getParagraph().getOrder() : null;
        String modifiableOrderStr = null != order? " от " + date(order.getCommitDate()) + " № " + order.getNumber():"";
        return preambleOrder + " «" + modifiableExtract.getType().getTitle() + "» " + modifiableOrderStr + order(extract, isPrintOrder);
    }

    ///// Выписки из прочих приказов по студентам ////

    @Override
    public String getComment(OtherStudentExtract extract, boolean isPrintOrder)
    {
        if (null != extract.getComment())
            return extract.getComment();

//        if (isPrintOrder)
//            return extract.getType().getTitle() + order(extract, isPrintOrder);
//        else
            return "";
    }

    ///// Выписки из сборных приказов по студентам /////

    @Override
    public String getComment(RevertOrderStuExtract extract, boolean isPrintOrder)
    {
        AbstractStudentExtract canceledExtract = extract.getCanceledExtract();
        AbstractStudentOrder order = canceledExtract.getParagraph()!= null? canceledExtract.getParagraph().getOrder() : null;
        String canceledOrderStr = order != null? " от " + date(order.getCommitDate()) + " № " + order.getNumber():"";
        return "Отменен приказ «" + canceledExtract.getType().getTitle() + "» " + canceledOrderStr + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(WeekendStuExtract extract, boolean isPrintOrder)
    {
        return "Предоставлен академический отпуск с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TransferDevFormStuExtract extract, boolean isPrintOrder)
    {
        String developFormOld = extract.getEducationOrgUnitOld().getDevelopForm().getGenCaseTitle();
        String developFormNew = extract.getEducationOrgUnitNew().getDevelopForm().getAccCaseTitle();
        return (isMale(extract) ? "Переведен" : "Переведена") + " с " + developFormOld + " формы освоения на " + developFormNew + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TransferCompTypeStuExtract extract, boolean isPrintOrder)
    {
        String developConditionOld = extract.getEducationOrgUnitOld().getDevelopCondition().getTitle();
        String developConditionNew = extract.getEducationOrgUnitNew().getDevelopCondition().getTitle();
        return (isMale(extract) ? "Переведен" : "Переведена") + " на другую основу оплаты обучения: с " + developConditionOld + "  на " + developConditionNew + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TransferEduTypeStuExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Переведен" : "Переведена") + " " + getTransferEduStr(extract.getEducationOrgUnitOld(), false, true) + " " + getTransferEduStr(extract.getEducationOrgUnitNew(), true, true) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TransferStuExtract extract, boolean isPrintOrder)
    {
        DevelopForm devForm = extract.getEducationOrgUnitNew().getDevelopForm();
        Boolean isInternalForm = devForm.getCode().equals(DevelopFormCodes.FULL_TIME_FORM);
        StringBuilder result = new StringBuilder("Перевод  на ").append(extract.getCourseNew().getTitle()).append(" курс").append(isInternalForm ? " в гр. " + extract.getGroupNew().getTitle() : "");
        result.append(" по ").append(devForm.getGenCaseTitle()).append(" форме обучения");
        if (!extract.getEducationOrgUnitNew().getDevelopTech().getCode().equals(DevelopTechCodes.GENERAL))
            result.append(" с применением дистанционных образовательных технологий");

        result.append(" на ").append(extract.getCompensationTypeNew().isBudget()? "бюджетной основе" : "договорной основе");

        String territorialOrgUnitOldStr = extract.getEducationOrgUnitOld().getTerritorialOrgUnit().getPrintTitle();
        String territorialOrgUnitNewStr = extract.getEducationOrgUnitNew().getTerritorialOrgUnit().getPrintTitle();

        result.append(" в связи со сменой территориального подразделения с «").append(territorialOrgUnitOldStr).append("» на «").append(territorialOrgUnitNewStr).append("»") ;
        return result.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(ExcludeStuExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Отчислен" : "Отчислена") + " с " + extract.getCourseStr() + " курса" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ChangeFioStuExtract extract, boolean isPrintOrder)
    {
        String reasonStr = " в связи с " + reason(extract.getReason());
        return "Изменение Ф.И.О. Фамилия: " + extract.getLastNameNew() + reasonStr + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(WeekendOutStuExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment = new StringBuilder("Выход из академического отпуска на ").append(extract.getCourseNew().getTitle()).append(" курс");
        DevelopForm devForm = extract.getEducationOrgUnitNew().getDevelopForm();
        if (devForm.getCode().equals(DevelopFormCodes.FULL_TIME_FORM))
            comment.append(" в гр. ").append(extract.getGroupNew().getTitle());
        comment.append(" с ").append(date(extract.getDate()));
        return comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(EduEnrolmentStuExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Зачислен" : "Зачислена") + " на " + extract.getCourseNew().getTitle() + " курс в группу " + extract.getGroupNew().getTitle() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(RestorationStuExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Восстановлен" : "Восстановлена") + " на " + extract.getCourseNew().getTitle() + " курс в группу " + extract.getGroupNew().getTitle() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(WeekendPregnancyStuExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment =  new StringBuilder("Предоставлен отпуск по беременности и родам с ").append(date(extract.getBeginDate())).append(" по ").append(date(extract.getEndDate()));
        if (extract.isPayBenefit() && extract.isPayOnetimeBenefit())
            comment.append(" с выплатой пособий по беременности и родам и в связи с ранней постановкой на учёт по беременности");
        else if (extract.isPayBenefit())
            comment.append(" с выплатой пособия по беременности и родам");
        else if (extract.isPayOnetimeBenefit())
            comment.append(" с выплатой пособия в связи с ранней постановкой на учёт по беременности");
        return comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(WeekendChildStuExtract extract, boolean isPrintOrder)
    {
        return "Предоставлен отпуск по уходу за ребенком с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(EduEnrolmentStuExtractExt extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Зачислен" : "Зачислена") + " на " + extract.getCourseNew().getTitle() + " курс в группу " + extract.getGroupNew().getTitle() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(EduEnrAsTransferStuExtract extract, boolean isPrintOrder)
    {
        String university = null != extract.getUniversity() ? " из " + extract.getUniversity() : "";
        return (isMale(extract) ? "Зачислен" : "Зачислена") + university + " в порядке перевода на " + extract.getCourseNew().getTitle() + " курс в группу " + extract.getGroupNew().getTitle() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(WeekendStuExtractExt extract, boolean isPrintOrder)
    {
        StringBuilder comment = new StringBuilder("Предоставлен академический отпуск").append(reason(extract.getReason()));
        if (extract.getReason().isCommentRequired())
            comment.append(" ").append(extract.getReasonComment());
        comment.append(" с ").append( date(extract.getBeginDate())).append(" по ").append(date(extract.getEndDate()));

        return comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(RestorationStuExtractExt extract, boolean isPrintOrder)
    {
        String restorationDateStr = null != extract.getRestorationDate() ? " от " + date(extract.getRestorationDate()) : "";
        return (isMale(extract) ? "Восстановлен" : "Восстановлена") + " на " + extract.getCourseNew().getTitle() + " курс в группу " + extract.getGroupNew().getTitle() + restorationDateStr + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TransferStuExtractExt extract, boolean isPrintOrder)
    {
        String transferDateStr = null != extract.getTransferDate() ? " от " + date(extract.getTransferDate()) : "";
        return (isMale(extract) ? "Переведен" : "Переведена") + transferDateStr + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(DischargingStuExtract extract, boolean isPrintOrder)
    {
        String dischargingDateStr = null != extract.getDischargingDate() ? " с " + date(extract.getDischargingDate()) : "";
        return (isMale(extract) ? "Отстранен" : "Отстранена") + " от занятий" + dischargingDateStr + " до ликвидации " + extract.getBeforeLiquidation() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(SessionProlongStuExtract extract, boolean isPrintOrder)
    {
        return "Продлена экзаменационная сессия в связи с " + extract.getProlongReason() + " с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ProlongWeekndChildStuExtract extract, boolean isPrintOrder)
    {
        return "Продлен отпуск по причине ухода за ребенком с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AcadGrantAssignStuExtract extract, boolean isPrintOrder)
    {
        return "Назначена выплата академической стипендии с " + date(extract.getBeginDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(SocGrantAssignStuExtract extract, boolean isPrintOrder)
    {
        String grantSizeStr = null != extract.getGrantSize() ? " в размере " + money(extract.getGrantSize()) : "";
        return "Назначена выплата социальной стипендии" + grantSizeStr + " с " + date(extract.getBeginDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(SocGrantStopStuExtract extract, boolean isPrintOrder)
    {
        return "Приостановлена выплата социальной стипендии с " + date(extract.getBeginDate()) + " до " + extract.getBeforeLiquidation() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(SocGrantResumptionStuExtract extract, boolean isPrintOrder)
    {
        return "Возобновлена выплата социальной стипендии с " + date(extract.getBeginDate()) + " до " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ExcludeStuExtractExt extract, boolean isPrintOrder)
    {
        String reasonStr = null != extract.getReasonStr() ? " по причине " + extract.getReasonStr() : "";
        return (isMale(extract) ? "Отчислен" : "Отчислена") + " с " + extract.getCourseStr() + " курса " + reasonStr + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ExcludeTransfStuExtract extract, boolean isPrintOrder)
    {
        String transferTo = null != extract.getTransferTo() ? " в " + extract.getTransferTo() : "";
        return (isMale(extract) ? "Отчислен" : "Отчислена") + " в связи с переводом " + transferTo + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(GiveBookDuplicateStuExtract extract, boolean isPrintOrder)
    {
        return "Выдан дубликат зачетной книжки № " + extract.getBookNumber() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(GiveCardDuplicateStuExtract extract, boolean isPrintOrder)
    {
        return "Выдан дубликат студенческого билета № " + extract.getCardNumber() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(GiveDiplomaDuplicateStuExtract extract, boolean isPrintOrder)
    {
        return "Выдан дубликат диплома № " + extract.getDiplomaNumber() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(GiveDiplAppDuplicateStuExtract extract, boolean isPrintOrder)
    {
        return "Выдан дубликат приложения к диплому № " + extract.getDiplomaNumber() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AdmissionStuExtract extract, boolean isPrintOrder)
    {
        String admissionDateStr = null != extract.getAdmissionDate() ? " с " + date(extract.getAdmissionDate()) : "";
        return "Имеет допуск к занятиям" + admissionDateStr + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AssignBenefitStuExtract extract, boolean isPrintOrder)
    {
        String benefitTypeStr = null != extract.getBenefitType() ? ": " + extract.getBenefitType() : "";
        String reasonStr = null != extract.getReasonStr() ? " в связи с " + extract.getReasonStr() : "";
        String beginDateStr = null != extract.getBeginDate() ? " с " + date(extract.getBeginDate()) : "";
        String endDateStr = null != extract.getEndDate() ? " по " + date(extract.getEndDate()) : "";
        return "Назначена выплата пособия" + benefitTypeStr + reasonStr + beginDateStr + endDateStr + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ChangeFirstNameStuExtract extract, boolean isPrintOrder)
    {
        String reasonStr = null != extract.getReasonStr() ? " в связи с " + extract.getReasonStr() : "";
        return "Изменение Ф.И.О. Имя: " + extract.getFirstNameNew() + reasonStr + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ChangeMiddleNameStuExtract extract, boolean isPrintOrder)
    {
        String reasonStr = null != extract.getReasonStr() ? " в связи с " + extract.getReasonStr() : "";
        return "Изменение Ф.И.О. Отчество: " + extract.getMiddleNameNew() + reasonStr + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ReEducationStuExtract extract, boolean isPrintOrder)
    {
        return "На повторном обучении с " + date(extract.getBeginDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ProlongWeekendStuExtract extract, boolean isPrintOrder)
    {
        return "Продлен академический отпуск с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(WeekendChildOutStuExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Вышел" : "Вышла") + " из отпуска по уходу за ребенком" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(PaymentDiscountStuExtract extract, boolean isPrintOrder)
    {
        return "Предоставлена скидка по оплате за обучение в размере " + money(extract.getDiscountSize()) + " с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(GiveDiplIncompleteStuListExtract extract, boolean isPrintOrder)
    {
        return "Выдан диплом о неполном высшем профессиональном образовании" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(PrescheduleSessionPassStuExtract extract, boolean isPrintOrder)
    {
        return extract.getSessionType().getTitle() + " сессия сдана досрочно в " + extract.getSessionYear().getTitle() + " году" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ChangePassDiplomaWorkPeriodStuExtract extract, boolean isPrintOrder)
    {
        return "Защита дипломной работы перенесена с " + date(extract.getCurrentDate()) + " на " + date(extract.getNewDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ChangeDateStateExaminationStuExtract extract, boolean isPrintOrder)
    {
        return "Сдача государственного экзамена перенесена с " + date(extract.getCurrentDate()) + " на " + date(extract.getNewDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(RepeatProtectDiplomaWorkStuExtract extract, boolean isPrintOrder)
    {
        return "Повторно " + (isMale(extract) ? "защитил" : "защитила") + " дипломную работу" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(RepeatPassStateExamStuExtract extract, boolean isPrintOrder)
    {
        return "Повторно " + (isMale(extract) ? "сдал" : "сдала") + " государственный экзамен: " + extract.getExamTitle() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FreeStudiesAttendanceStuExtract extract, boolean isPrintOrder)
    {
        return "Свободное посещении занятий с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AddGroupManagerStuExtract extract, boolean isPrintOrder)
    {
        return "Назначен старостой учебной группы " + extract.getGroupStr() + " на весь период обучения" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(RemoveGroupManagerStuExtract extract, boolean isPrintOrder)
    {
        return "Освобожден от обязанностей старосты учебной группы " + extract.getGroupStr() + " с " + date(extract.getBeginDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(DisciplinaryPenaltyStuExtract extract, boolean isPrintOrder)
    {
         return "Объявлен выговор " + reason(extract.getReason()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(RecordBookLossPenaltyStuExtract extract, boolean isPrintOrder)
    {
        return "Объявлен выговор за утерю зачётной книжки" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(StudentCardLossPenaltyStuExtract extract, boolean isPrintOrder)
    {
        return "Объявлен выговор за утерю студенческого билета" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AdmitToStateExamsStuExtract extract, boolean isPrintOrder)
    {
        return "Допуск к сдаче" + reason(extract.getReason())  + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(HolidayStuExtract extract, boolean isPrintOrder)
    {
        return "Предоставлены каникулы после успешного прохождения государственной итоговой аттестации" + " с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ChangePassStateExamsStuExtract extract, boolean isPrintOrder)
    {
        String season = (null != extract.getSeason() ? " на " + extract.getSeason() : "") + " (до " + date(extract.getBeginDate()) + ")";
        return "Перенос государственной итоговой аттестации" + season + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(SessionIndividualStuExtract extract, boolean isPrintOrder)
    {
        return "Индивидуальный срок сдачи " + SessionPartModel.getSessionShortTitle(extract.getSessionType(), GrammaCase.GENITIVE) + " экзаменационной сессии до " + date(extract.getBeginDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TransferIndPlanStuExtract extract, boolean isPrintOrder)
    {
        return "Перевод на индивидуальный план обучения на " + extract.getEducationYear().getTitle() + " учебный год" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AdmitToDiplomaStuExtract extract, boolean isPrintOrder)
    {
        StructureEducationLevels levels = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
        return "Допуск к защите выпускной квалификационной работы" + (levels.isBachelor() ? " (бакалаврской работы)" : (levels.isMaster() ? " (магистерской диссертации)" : "")) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TransferIndGraphStuExtract extract, boolean isPrintOrder)
    {
        List<StuExtractToTermRelation> list = UniDaoFacade.getCoreDao().getList(StuExtractToTermRelation.class, StuExtractToTermRelation.extract(), extract);
        String termString = UniStringUtils.join(list, StuExtractToTermRelation.term().title(), ", ");
        return "Перевод на индивидуальный график обучения на "+ termString + (list.size()>1 ? " семестры " : " семестр ") + extract.getEducationYear().getTitle() + " учебного года" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(WeekendChildOneAndHalfStuExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment = new StringBuilder("Предоставлен отпуск по уходу за ребенком до 1,5 лет с ").append(date(extract.getBeginDate())).append(" по ").append(date(extract.getEndDate()));
        if (extract.isPayBenefit() && extract.isPayOnetimeBenefit())
            comment.append(" с выплатой пособий по беременности и родам и в связи с ранней постановкой на учёт по беременности");
        else if (extract.isPayBenefit())
            comment.append(" с выплатой пособия по беременности и родам");
        else if (extract.isPayOnetimeBenefit())
            comment.append(" с выплатой пособия в связи с ранней постановкой на учёт по беременности");

        if (extract.isFreeAttendance())
            comment.append(" с правом свободного посещения занятий");
        return comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(ReEducationFGOSStuExtract extract, boolean isPrintOrder)
    {
        return "Предоставлен повторный год обучения с переходом на ФГОС" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(RestorationCourseStuExtract extract, boolean isPrintOrder)
    {
        DevelopForm devForm = extract.getEducationOrgUnitNew().getDevelopForm();
        Boolean isInternalForm = devForm.getCode().equals(DevelopFormCodes.FULL_TIME_FORM);

        String formativeOrgUnitNew = extract.getEducationOrgUnitNew().getFormativeOrgUnit().getGenitiveCaseTitle();
        StringBuilder result = new StringBuilder("Восстановлен на ").append(extract.getCourseNew().getTitle()).append(" курс ");
        if (extract.isPrintRestorationDateAndTerm() && extract.getRestorationTerm()!= null)
            result.append("(").append(extract.getRestorationTerm().getCode()).append(" семестр").append(")");

        result.append(isInternalForm ? " в гр. " + extract.getGroupNew().getTitle() : "");

        result.append(" ").append(formativeOrgUnitNew).append(" по ").append(devForm.getGenCaseTitle()).append(" форме обучения");
        if (!extract.getEducationOrgUnitNew().getDevelopTech().getCode().equals(DevelopTechCodes.GENERAL))
            result.append(" с применением дистанционных образовательных технологий");

        result.append(" на ").append(extract.getCompensationTypeNew().isBudget()? "бюджетной основе" : "договорной основе");
        if (extract.isPrintRestorationDateAndTerm())
            result.append(" c ").append(date(extract.getRestorationDate()));

        if (extract.isHasDebts())
            result.append(". Разницу в учебных планах ликвидировать в срок до " + date(extract.getDeadlineDate()));
         return result.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(ExcludeStateExamStuExtract extract, boolean isPrintOrder)
    {
        return "Отчислить " + reason(extract.getReason()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ExcludeUnacceptedToGPDefenceStuExtract extract, boolean isPrintOrder)
    {
        StructureEducationLevels levels = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();

        return "Отчислен как не допущенный к защите выпускной квалификационной работы" + (levels.isBachelor() ? " (бакалаврской работы)"
                : (levels.isMaster() ? " (магистерской диссертации)" : "")) + order(extract, isPrintOrder);

    }

    @Override
    public String getComment(ExcludeGPDefenceFailStuExtract extract, boolean isPrintOrder)
    {
//        StructureEducationLevels levels = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
//        String comment = "Отчислить как " + reason(extract.getReason());

//        if (levels.isBachelor())
//            comment += " (бакалаврской работы)";
//        if (levels.isMaster())
//            comment += " (магистерской диссертации)";
        return "Отчислить " + reason(extract.getReason()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TransferGroupStuExtract extract, boolean isPrintOrder)
    {
        return "Перевод из гр. " + (null != extract.getGroupOld() ? extract.getGroupOld().getTitle() : "") + " в гр. " + extract.getGroupNew().getTitle() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TransferCourseStuExtract extract, boolean isPrintOrder)
    {
        String comment = "Перевод на " + extract.getCourseNew().getTitle() + " курс";
        DevelopForm devForm = extract.getEducationOrgUnitNew().getDevelopForm();
        if (devForm.getCode().equals(DevelopFormCodes.FULL_TIME_FORM))
            comment += " гр. " + extract.getGroupNew().getTitle();
        return comment + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TransferEduLevelStuExtract extract, boolean isPrintOrder)
    {
        DevelopForm devForm = extract.getEducationOrgUnitNew().getDevelopForm();
        Boolean isInternalForm = devForm.getCode().equals(DevelopFormCodes.FULL_TIME_FORM);
        StringBuilder comment = new StringBuilder("Перевод с ").append(extract.getCourseOld().getTitle()).append(" курса ")
                .append(isInternalForm && extract.getGroupOld() != null ? " гр. " + extract.getGroupOld().getTitle() : "");
        comment.append(" на ").append(extract.getCourseNew().getTitle()).append(" курс ").append(isInternalForm ? "в гр. " + extract.getGroupNew().getTitle() : "")
                .append(" в связи со сменой направления подготовки");
        comment.append(" по ").append(devForm.getGenCaseTitle()).append(" форме обучения");
        if (!extract.getEducationOrgUnitNew().getDevelopTech().getCode().equals(DevelopTechCodes.GENERAL))
            comment.append(" с применением дистанционных образовательных технологий");

        comment.append(" на ").append(extract.getCompensationTypeNew().isBudget()? "бюджетной основе" : "договорной основе");

        if (extract.isLiquidateEduPlanDifference())
            comment.append(". Разницу в учебных планах ликвидировать в срок до " + date(extract.getLiquidationDeadlineDate()));
        return comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(TransferFormativeStuExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment = new StringBuilder("Перевод").append(getTransferOrgUnitString(extract, true, false));
        if (extract.isLiquidateEduPlanDifference())
            comment.append(". Разницу в учебных планах ликвидировать в срок до ").append(date(extract.getLiquidationDeadlineDate()));
        return comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(TransferTerritorialStuExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment = new StringBuilder("Перевод").append(getTransferOrgUnitString(extract, true, true));
        if (extract.isLiquidateEduPlanDifference())
            comment.append(". Разницу в учебных планах ликвидировать в срок до ").append(date(extract.getLiquidationDeadlineDate()));
        return comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(TransferTerritorialExtStuExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment = new StringBuilder("Перевод").append(getTransferOrgUnitString(extract, true, true));
        if (extract.isLiquidateEduPlanDifference())
            comment.append(". Разницу в учебных планах ликвидировать в срок до ").append(date(extract.getLiquidationDeadlineDate()));
        return comment + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TransferDevCondExtStuExtract extract, boolean isPrintOrder)
    {
        String developPeriodNew = extract.getEducationOrgUnitNew().getDevelopPeriod().getTitle();

        StringBuilder builder = new StringBuilder("Перевод на ").append(extract.getCourseNew().getTitle()).append(" курс по ");

        switch(extract.getEducationOrgUnitNew().getDevelopCondition().getCode())
        {
            case DevelopConditionCodes.FULL_PERIOD:

                break;
            case DevelopConditionCodes.REDUCED_PERIOD:
                builder.append("сокращенной").append(" (").append(developPeriodNew).append(")");
                break;
            case DevelopConditionCodes.REDUCED_ACCELERATED_PERIOD:
                builder.append("сокращенной ускоренной").append(" (").append(developPeriodNew).append(")");
                break;
            case DevelopConditionCodes.ACCELERATED_PERIOD:
                builder.append("ускоренной").append(" (").append(developPeriodNew).append(")");
                break;
        }

        builder.append(" основной образовательной программе ");
        builder.append(" по ").append(extract.getEducationOrgUnitNew().getDevelopForm().getGenCaseTitle()).append(" форме обучения");
        if (!extract.getEducationOrgUnitNew().getDevelopTech().getCode().equals(DevelopTechCodes.GENERAL))
            builder.append(" с применением дистанционных образовательных технологий");

        builder.append(" на ").append(extract.getCompensationTypeNew().isBudget()? "бюджетной основе" : "договорной основе");


        return builder.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(TransferDevCondStuExtract extract, boolean isPrintOrder)
    {
        String developConditionOld = extract.getEducationOrgUnitOld().getDevelopCondition().getTitle();
        String developConditionNew = extract.getEducationOrgUnitNew().getDevelopCondition().getTitle();
        return (isMale(extract) ? "Переведен" : "Переведена") + " на другое условие освоения: с «" + developConditionOld + "» на «" + developConditionNew + "»" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(RestorationAdmitStateExamsExtStuExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment = new StringBuilder("Восстановлен и повторно допущен к сдаче");
        if (extract.isStateExamPlural())
            comment.append(" государственных экзаменов");
        else comment.append(" государственного экзамена");

        if (extract.getSeason() != null)
        {
            int indexOfSeasonTerm = Arrays.asList((ApplicationRuntime.getProperty("seasonTerm_N")).split(";")).indexOf(extract.getSeason());
            if (indexOfSeasonTerm >= 0)
            {
                String season = Arrays.asList((ApplicationRuntime.getProperty("seasonTerm_P")).split(";")).get(indexOfSeasonTerm);
                comment.append(" в ").append(season);
            }
        }

        comment.append(" (до ").append(date(extract.getStateExamPassDeadlineDate())).append(")");
        return comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(TransferProfileStuExtract extract, boolean isPrintOrder)
    {
        return "Закрепление" + getAssignEduStr(extract.getEducationOrgUnitNew()) + " и перераспределение в гр. " + extract.getGroupNew().getTitle() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(RestorationAdmitStateExamsStuExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment = new StringBuilder("Восстановлен и повторно допущен к сдаче");
        if (extract.isStateExamPlural())
            comment.append(" государственных экзаменов");
        else comment.append(" государственного экзамена");
        if (extract.getSeason() != null)
        {
            int indexOfSeasonTerm = Arrays.asList((ApplicationRuntime.getProperty("seasonTerm_N")).split(";")).indexOf(extract.getSeason());
            if (indexOfSeasonTerm >= 0)
            {
                String season = Arrays.asList((ApplicationRuntime.getProperty("seasonTerm_P")).split(";")).get(indexOfSeasonTerm);
                comment.append(" в ").append(season);
            }
        }
        comment.append(" (до ").append(date(extract.getStateExamPassDeadlineDate())).append(")").append(order(extract, isPrintOrder));
        return comment.toString();
    }

    @Override
    public String getComment(TransferProfileGroupExtStuExtract extract, boolean isPrintOrder)
    {
        String comment = "Перевод "+ getTransferEduStr(extract.getEducationOrgUnitNew(), true, false)
        + ", перераспределение из гр. " + (null != extract.getGroupOld() ? extract.getGroupOld().getTitle(): "") + " в гр. " + extract.getGroupNew().getTitle() + order(extract, isPrintOrder);
        return comment;
    }

    @Override
    public String getComment(RestorationAdmitToDiplomaStuExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment =  new StringBuilder("Восстановлен и повторно допущен к подготовке выпускной квалификационной работы");
        StructureEducationLevels levels = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
        if (levels.isBachelor())
            comment.append(" (бакалаврской работы)");
        if (levels.isMaster())
            comment.append(" (магистерской диссертации)");
        return  comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(TransferCompTypeExtStuExtract extract, boolean isPrintOrder)
    {
        return "Перевод на вакантное бюджетное место с " + date(extract.getTransferDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TransferProfileExtStuExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment = new StringBuilder("Перевод ").append(getTransferEduStr(extract.getEducationOrgUnitOld(), false, false)).append(" ").append(getTransferEduStr(extract.getEducationOrgUnitNew(), true, false));
        if (extract.isLiquidateEduPlanDifference())
            comment.append(". Разницу в учебных планах ликвидировать в срок до ").append(date(extract.getLiquidationDeadlineDate()));
        return  comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(ChangeCategoryStuExtract extract, boolean isPrintOrder)
    {
        String categoryOld = (null != extract.getStudentCategoryOld() ? getCategoryString(extract.getStudentCategoryOld(), false) : "");
        String categoryNew = (null != extract.getStudentCategoryNew() ? getCategoryString(extract.getStudentCategoryNew(), true): "");
        return new StringBuilder(categoryOld).append(" считать ").append(categoryNew).append(" c ")
                .append(date(extract.getCategoryChangeDate())).append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(RestorationAdmitToDiplomaDefStuExtract extract, boolean isPrintOrder)
    {
        StructureEducationLevels levels = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
        StringBuilder comment = new StringBuilder("Восстановлен и повторно допущен к защите выпускной квалификационной работы");
        if (levels.isBachelor())
            comment.append(" (бакалаврской работы)");
        if (levels.isMaster())
            comment.append(" (магистерской диссертации)");

        if (extract.getSeason() != null)
        {
            int indexOfSeasonTerm = Arrays.asList((ApplicationRuntime.getProperty("seasonTerm_N")).split(";")).indexOf(extract.getSeason());
            if (indexOfSeasonTerm >= 0)
            {
                String season = Arrays.asList((ApplicationRuntime.getProperty("seasonTerm_P")).split(";")).get(indexOfSeasonTerm);
                comment.append(" в ").append(season);
            }
        }
        return comment.append(" (до ").append(date(extract.getDpDefenceDeadlineDate())).append(")").append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(RePassDiscStuExtract extract, boolean isPrintOrder)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(StuExtractToRePassDiscipline.class, "d").column("d")
                .where(eq(property(StuExtractToRePassDiscipline.extract().id().fromAlias("d")), value(extract)))
                .order(property(StuExtractToRePassDiscipline.priority().fromAlias("d")));
        List<StuExtractToRePassDiscipline> list = UniDaoFacade.getCoreDao().getList(builder);
        StringBuilder comment = new StringBuilder("Пересдача ");
        boolean isFirst = true;
        for (StuExtractToRePassDiscipline discipline : list)
        {
            if (!isFirst) comment.append(", ");
            int indexOfAction = Arrays.asList((ApplicationRuntime.getProperty("commonextract.controlActionsList")).split(";")).indexOf(discipline.getControlAction());
            if (indexOfAction >= 0)
            {
                String action = Arrays.asList((ApplicationRuntime.getProperty("commonextract.controlActionsList_G")).split(";")).get(indexOfAction);
                comment.append(action).append(" по дисциплине «").append(discipline.getDiscipline()).append("» (").append(discipline.getSrcTerm().getCode()).append(" семестр)");
            }
            isFirst = false;
        }
        return comment.append(" в ").append(extract.getDestTerm().getTitle()).append(" семестре ").append(extract.getDestEduYear().getTitle())
                .append(" учебного года").append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(RestorationAdmitToDiplomaDefExtStuExtract extract, boolean isPrintOrder)
    {
        StructureEducationLevels levels = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
        StringBuilder comment = new StringBuilder("Восстановлен и повторно допущен к защите выпускной квалификационной работы");
        if (levels.isBachelor())
            comment.append(" (бакалаврской работы)");
        if (levels.isMaster())
            comment.append(" (магистерской диссертации)");

        if (extract.getSeason() != null)
        {
            int indexOfSeasonTerm = Arrays.asList((ApplicationRuntime.getProperty("seasonTerm_N")).split(";")).indexOf(extract.getSeason());
            if (indexOfSeasonTerm >= 0)
            {
                String season = Arrays.asList((ApplicationRuntime.getProperty("seasonTerm_P")).split(";")).get(indexOfSeasonTerm);
                comment.append(" в ").append(season);
            }
        }

        return comment.append(" (до ").append(date(extract.getDpDefenceDeadlineDate())).append(")").append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(RePassDiscComissStuExtract extract, boolean isPrintOrder)
    {
        int indexOfAction = Arrays.asList((ApplicationRuntime.getProperty("commonextract.controlActionsList")).split(";")).indexOf(extract.getActionType());
        String action="";
        if (indexOfAction >= 0)
        {
            action = Arrays.asList((ApplicationRuntime.getProperty("commonextract.controlActionsList_G")).split(";")).get(indexOfAction);
        }
        return "Пересдача комиссии " + action + " по дисциплине «" + extract.getDiscipline() + "» " +
               DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getActionDateTime()) + " в " + new DateFormatter("HH-mm").format(extract.getActionDateTime())
                + " в аудитории " + extract.getActionPlace() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(GiveDiplomaStuExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment = new StringBuilder("Присвоена квалификация ");

        EducationLevelsHighSchool eduHs = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool();
        EducationLevels eduLevel = eduHs.getEducationLevel();

        comment.append(eduHs.getQualificationTitleNullSafe().toUpperCase());
        comment.append(", выдан диплом ").append(GiveDiplomaStuExtractPrint.getDiplomaName_G(eduLevel.getSafeQCode()));

        if (extract.isWithExcellent())
            comment.append(" с отличием");
        comment.append(", отчислен в связи с окончанием обучения");

        return comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(MatAidAssignStuExtract extract, boolean isPrintOrder)
    {
        return "Оказана материальная помощь на " + RussianDateFormatUtils.getMonthName(extract.getMonth(), true) + " " + String.valueOf(extract.getYear()) + " года в размере " + money(extract.getGrantSize()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(GrantRiseStuExtract extract, boolean isPrintOrder)
    {
        return "Однократно повышена государственная академическая стипендия на " + RussianDateFormatUtils.getMonthName(extract.getMonth(), true) + " " + String.valueOf(extract.getYear()) + " года в размере " + money(extract.getGrantRiseAmount()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ProfileAssignStuExtract extract, boolean isPrintOrder)
    {
        return "Закрепление" + getAssignEduStr(extract.getEducationOrgUnitNew()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(WeekendPregnancyOutStuExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment = new StringBuilder("Выход из отпуска по беременности и родам на ").append(extract.getGroupNew().getCourse().getTitle()).append(" курс");
        DevelopForm devForm = extract.getEntity().getEducationOrgUnit().getDevelopForm();
        if (devForm.getCode().equals(DevelopFormCodes.FULL_TIME_FORM))
            comment.append(" в гр. ").append(extract.getGroupNew().getTitle());
        return  comment.append(" c ").append(date(extract.getBeginDate())).append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(WeekendChildCareOutStuExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment = new StringBuilder("Выход из отпуска по уходу за ребенком на ").append(extract.getGroupNew().getCourse().getTitle()).append(" курс");
        DevelopForm devForm = extract.getEntity().getEducationOrgUnit().getDevelopForm();
        if (devForm.getCode().equals(DevelopFormCodes.FULL_TIME_FORM))
            comment.append(" в гр. ").append(extract.getGroupNew().getTitle());
        return  comment.append(" c ").append(date(extract.getBeginDate())).append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(AcadGrantBonusAssignStuExtract extract, boolean isPrintOrder)
    {
        return "Назначена надбавка к государственной академической стипендии в размере " + money(extract.getGrantBonusSize()) + " с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TransferCourseWithChangeTerrOuStuExtract extract, boolean isPrintOrder)
    {
        return "Перевод " + getTransferOrgUnitString(extract, true, true) +order(extract, isPrintOrder);
    }

    @Override
    public String getComment(GroupAssignStuExtract extract, boolean isPrintOrder)
    {
        return "Распределение в гр.  " + extract.getGroupNew().getTitle() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(WeekendChildThreeStuExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment = new StringBuilder("Предоставлен отпуск по уходу за ребенком до 3 лет с ").append(date(extract.getBeginDate())).append(" по ").append(date(extract.getEndDate()));
        if (extract.isPayBenefit() && extract.isPayOnetimeBenefit())
            comment.append(" с выплатой пособий по беременности и родам и в связи с ранней постановкой на учёт по беременности");
        else if (extract.isPayBenefit())
            comment.append(" с выплатой пособия по беременности и родам");
        else if (extract.isPayOnetimeBenefit())
            comment.append(" с выплатой пособия в связи с ранней постановкой на учёт по беременности");

        if (extract.isFreeAttendance())
            comment.append(" с правом свободного посещения занятий");
        return comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(SessionIndividualAheadStuExtract extract, boolean isPrintOrder)
    {
        return "Индивидуальный срок сдачи " + SessionPartModel.getSessionShortTitle(extract.getSessionType(), GrammaCase.GENITIVE) + " экзаменационной сессии с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AdmitToDiplomaWithoutExamStuExtract extract, boolean isPrintOrder)
    {
        StructureEducationLevels levels = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
        return "Допуск к защите выпускной квалификационной работы" + (levels.isBachelor() ? " (бакалаврской работы)" : (levels.isMaster() ? " (магистерской диссертации)" : "")) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AdoptRussianCitizenshipStuExtract extract, boolean isPrintOrder)
    {
        return "Считать гражданином Российской Федерации c " + date(extract.getBeginDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AttestationPracticStuExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Получил" : "Получила") + " аттестацию практики ("+ extract.getPracticeType() + ") в объеме " + extract.getPracticeDurationStr() + " недель, на основании академической справки" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AcadGrantPaymentStopStuExtract extract, boolean isPrintOrder)
    {
        return "Прекращена выплата академической стипендии" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(SocGrantPaymentStopStuExtract extract, boolean isPrintOrder)
    {
        return "Прекращена выплата социальной стипендии" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AcadGrantBonusPaymentStopStuExtract extract, boolean isPrintOrder)
    {
        return "Прекращена выплата надбавки к государственной академической стипендии" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AttestationPracticWithServiceRecStuExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Получил" : "Получила") + " аттестацию практики ("+ extract.getPracticeType() + ") в объеме " + extract.getPracticeDurationStr() + " недель, имея стаж работы по профилю обучения" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AttestationPracticWithProbationStuExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Получил" : "Получила") + " аттестацию практики ("+ extract.getPracticeType() + ") в объеме " + extract.getPracticeDurationStr() + " недель, пройдя стажировку с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TransferBetweenTerritorialStuExtract extract, boolean isPrintOrder)
    {
        String territorialOrgUnitOldStr = extract.getEducationOrgUnitOld().getTerritorialOrgUnit().getPrintTitle();
        String territorialOrgUnitNewStr = extract.getEducationOrgUnitNew().getTerritorialOrgUnit().getPrintTitle();
        return (isMale(extract) ? "Переведен" : "Переведена") + " из «" + territorialOrgUnitOldStr + "» в «" + territorialOrgUnitNewStr + "»" + order(extract, isPrintOrder);
    }

    ///// Выписки из списочных приказов по студентам /////

    @Override
    public String getComment(CourseTransferStuListExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Переведен" : "Переведена") + " с " + extract.getCourseOld().getTitle() + " курса на " + extract.getCourseNew().getTitle() + " курс в группу " + extract.getGroup().getTitle() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(CourseNoChangeStuListExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Остался" : "Осталась") + " на прежнем " + extract.getCourseOld().getTitle() + " курсе в группе " + extract.getGroup().getTitle() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(CourseTransferDebtorStuListExtract extract, boolean isPrintOrder)
    {
        String transferDateStr = null != extract.getTransferDate() ? " от " + date(extract.getTransferDate()) : "";
        return (isMale(extract) ? "Переведен" : "Переведена") + " с " + extract.getCourseOld().getTitle() + " курса на следующий курс в группу " + extract.getGroup().getTitle() + ", имея задолженности" + transferDateStr + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(GraduateSuccessStuListExtract extract, boolean isPrintOrder)
    {
        String graduateDateStr = null != extract.getGraduateDate() ? " от " + date(extract.getGraduateDate()) : "";
        return (isMale(extract) ? "Выпустился" : "Выпустилась") + ", получив диплом с отличием" + graduateDateStr + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(GraduateStuListExtract extract, boolean isPrintOrder)
    {
        String graduateDateStr = null != extract.getGraduateDate() ? " от " + date(extract.getGraduateDate()) : "";
        return (isMale(extract) ? "Выпустился" : "Выпустилась") + ", получив диплом" + graduateDateStr + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(GradExcludeStuListExtract extract, boolean isPrintOrder)
    {
        String excludeDateStr = null != extract.getExcludeDate() ? " от " + date(extract.getExcludeDate()) : "";
        return (isMale(extract) ? "Исключен" : "Исключена") + " из состава студентов" + excludeDateStr + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ExcludeStuListExtract extract, boolean isPrintOrder)
    {
        String excludeDateStr = null != extract.getBeginDate() ? " от " + date(extract.getBeginDate()) : "";
        return (isMale(extract) ? "Отчислен" : "Отчислена") + " с " + extract.getCourseStr() + " курса" + excludeDateStr + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AdmitToStateExamsStuListExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Допущен" : "Допущена") + " к Государственной итоговой аттестации" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(GiveDiplStuListExtract extract, boolean isPrintOrder)
    {
        return "Выдан диплом и присвоена квалификация" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(GiveDiplSuccessStuListExtract extract, boolean isPrintOrder)
    {
        return "Выдан диплом с отличием и присвоена квалификация" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(HolidayStuListExtract extract, boolean isPrintOrder)
    {
        return "Предоставлены каникулы после успешного прохождения государственной итоговой аттестации с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ExcludeSingGrpSuccStuListExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Отчислен" : "Отчислена") + " с " + extract.getCourseStr() + " курса с выдачей диплома с отличием от " + date(extract.getExcludeDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ExcludeSingGrpStuListExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Отчислен" : "Отчислена") + " с " + extract.getCourseStr() + " курса с выдачей диплома без отличия от " + date(extract.getExcludeDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ExcludeSingGrpCathStuListExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Отчислен" : "Отчислена") + " с " + extract.getCourseStr() + " курса от " + date(extract.getExcludeDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(SplitStudentsStuListExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Закреплен" : "Закреплена") + getAssignEduStr(extract.getEducationOrgUnitNew()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TransferDevConditionStuListExtract extract, boolean isPrintOrder)
    {
        String developConditionOld = extract.getEducationOrgUnitOld().getDevelopCondition().getTitle();
        String developConditionNew = extract.getEducationOrgUnitNew().getDevelopCondition().getTitle();
        return "Изменено условие освоения с «" + developConditionOld + "» на «" + developConditionNew + "»" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TerritorialTransferStuListExtract extract, boolean isPrintOrder)
    {
        String territorialOrgUnitOldStr = extract.getEducationOrgUnitOld().getTerritorialOrgUnit().getPrintTitle();
        String territorialOrgUnitNewStr = extract.getEducationOrgUnitNew().getTerritorialOrgUnit().getPrintTitle();
        return (isMale(extract) ? "Переведен" : "Переведена") + " с «" + territorialOrgUnitOldStr + "» на «" + territorialOrgUnitNewStr + "»" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AdmitToPassDiplomaWorkStuListExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Допущен" : "Допущена") + " к защите дипломной работы" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(SetReviewerStuListExtract extract, boolean isPrintOrder)
    {
        return "Назначен рецензент: " + extract.getReviewerName() + ", ученая степень: " + extract.getReviewerDegree() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ChangeScientificAdviserExtract extract, boolean isPrintOrder)
    {
        return "Назначен новый научный руководитель: " + extract.getScientificAdviserName().getPerson().getIdentityCard().getFio() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ChangeReviewerStuListExtract extract, boolean isPrintOrder)
    {
        return "Назначен новый рецензент: " + extract.getReviewerName() + ", ученая степень: " + extract.getReviewerDegree() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(ChangeDiplomaWorkTopicStuListExtract extract, boolean isPrintOrder)
    {
        String diplomaWorkTopicOld = null != extract.getDiplomaWorkTopicOld() ? (" с «" + extract.getDiplomaWorkTopicOld() + "»") : "";
        return "Изменена тема дипломной работы" + diplomaWorkTopicOld + " на «" + extract.getDiplomaWorkTopicNew() + "»" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(SetDiplomaWorkTopicAndScientificAdviserStuListExtract extract, boolean isPrintOrder)
    {
        return "Утверждена тема дипломной работы: «" + extract.getDiplomaWorkTopicNew() + "» и назначен научный руководитель: " + extract.getScientificAdviserName().getPerson().getIdentityCard().getFio() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(SplitStudentsGroupStuListExtract extract, boolean isPrintOrder)
    {
        return "Закрепление " + getAssignEduStr(extract.getEducationOrgUnitNew()) + " и перераспределение в гр. " + extract.getGroupNew().getTitle() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AdmitToStateExamsExtStuListExtract extract, boolean isPrintOrder)
    {
        String reason = "";
        if (extract.getParagraph() != null)
            reason = reason(((StudentListOrder)extract.getParagraph().getOrder()).getReason());
        return "Допуск к сдаче " + (reason.isEmpty()? "государственного экзамена" : reason)  + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AdmitToPassDiplomaWorkExtStuListExtract extract, boolean isPrintOrder)
    {

        StructureEducationLevels levels = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
        return "Допуск к защите выпускной квалификационной работы" + (levels.isBachelor() ? " (бакалаврской работы)" : (levels.isMaster() ? " (магистерской диссертации)" : "")) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AddGroupManagerStuListExtract extract, boolean isPrintOrder)
    {
        return "Назначен старостой учебной группы " + extract.getGroup().getTitle() + " на весь период обучения" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(GiveDiplExtStuListExtract extract, boolean isPrintOrder)
    {
        return "Выдан диплом и присвоена квалификация" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(GiveDiplSuccessExtStuListExtract extract, boolean isPrintOrder)
    {
        return "Выдан диплом с отличием и присвоена квалификация" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(CourseTransferExtStuListExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment =  new StringBuilder("Перевод на ").append(extract.getCourseNew().getTitle()).append(" курс");
        DevelopForm devForm = extract.getEntity().getEducationOrgUnit().getDevelopForm();
        if (devForm.getCode().equals(DevelopFormCodes.FULL_TIME_FORM))
            comment.append(" гр. ").append(extract.getGroupNew().getTitle());
        return  comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(AcadGrantAssignStuListExtract extract, boolean isPrintOrder)
    {
        String grantSize = null != extract.getGroupManagerBonusSizeRuble() ? " в размере " + money(extract.getGroupManagerBonusSizeRuble()) :
            null != extract.getGrantSizeRuble() ? " в размере " + money(extract.getGrantSizeRuble()) : "";
            return "Назначена академическая стипендия" + grantSize + " с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(SocGrantAssignStuListExtract extract, boolean isPrintOrder)
    {
        return "Назначена социальная стипендия в размере " + money(extract.getGrantSize())+ " с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FinAidAssignStuListExtract extract, boolean isPrintOrder)
    {
        return "Оказана материальная помощь на " + RussianDateFormatUtils.getMonthName(extract.getMonth(), true) + " " + String.valueOf(extract.getYear()) + " года в размере " + money(extract.getFinAidSize()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(SplitFirstCourseStudentsGroupStuListExtract extract, boolean isPrintOrder)
    {
        return "Распределение в гр. " + extract.getNewGroup().getTitle() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(GrantRiseStuListExtract extract, boolean isPrintOrder)
    {
        return "Однократно повышена государственная академическая стипендия на " + RussianDateFormatUtils.getMonthName(extract.getMonth(), true) + " " + String.valueOf(extract.getYear()) + " года в размере " + money(extract.getGrantRiseAmount()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TerritorialCourseTransferStuListExtract extract, boolean isPrintOrder)
    {
        DevelopForm devForm = extract.getEducationOrgUnitNew().getDevelopForm();
        Boolean isInternalForm = devForm.getCode().equals(DevelopFormCodes.FULL_TIME_FORM);

        String formativeOrgUnitOld = extract.getEducationOrgUnitOld().getFormativeOrgUnit().getGenitiveCaseTitle();
        String formativeOrgUnitNew = extract.getEducationOrgUnitNew().getFormativeOrgUnit().getGenitiveCaseTitle();

        String territorialOrgUnitOld = extract.getEducationOrgUnitOld().getTerritorialOrgUnit().getPrintTitle();
        String territorialOrgUnitNew = extract.getEducationOrgUnitNew().getTerritorialOrgUnit().getPrintTitle();

        StringBuilder comment = new StringBuilder("Перевод с ").append(extract.getCourseOld().getTitle()).append(" курса").append(isInternalForm ? " гр. " + extract.getGroupOld().getTitle() : "");
        comment.append(" ").append(formativeOrgUnitOld).append(!extract.getEducationOrgUnitOld().getTerritorialOrgUnit().isTop() ? " (" + territorialOrgUnitOld + ")" : "") ;
        comment.append(" на ").append(extract.getCourseNew().getTitle()).append(" курс").append(isInternalForm ? " гр. " + extract.getGroupNew().getTitle() : "");
        comment.append(" ").append(formativeOrgUnitNew).append(!extract.getEducationOrgUnitNew().getTerritorialOrgUnit().isTop() ? " (" + territorialOrgUnitNew + ")" : "");
        comment.append(" по ").append(devForm.getGenCaseTitle()).append(" форме обучения");
        if (!extract.getEducationOrgUnitNew().getDevelopTech().getCode().equals(DevelopTechCodes.GENERAL))
            comment.append(" с применением дистанционных образовательных технологий");

        if (null != extract.getCompensationType())
            comment.append(" на ").append(extract.getCompensationType().isBudget()? "бюджетной основе" : "договорной основе");
        comment.append(order(extract, isPrintOrder));

        return comment.toString();
    }

    @Override
    public String getComment(SplitStudentsExtStuListExtract extract, boolean isPrintOrder)
    {
        return "Закрепление" + getAssignEduStr(extract.getEducationOrgUnitNew()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(AcadGrantBonusAssignStuListExtract extract, boolean isPrintOrder)
    {
        return "Назначена дополнительная стипендия в размере " + money(extract.getGrantBonusSize())+ " с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(GroupTransferExtStuListExtract extract, boolean isPrintOrder)
    {
        return "Перевод из гр. " + extract.getGroupOld().getTitle() + " в гр. " + extract.getGroupNew().getTitle() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(SendPracticeInnerStuListExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Направлен" : "Направлена") + " на практику (" + extract.getPracticeKind()+ ") в объеме " + extract.getPracticeDurationStr() + " недель с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + " в пределах ОУ" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TerritorialTransferExtStuListExtract extract, boolean isPrintOrder)
    {
       DevelopForm devForm = extract.getEducationOrgUnitNew().getDevelopForm();
        Boolean isInternalForm = devForm.getCode().equals(DevelopFormCodes.FULL_TIME_FORM);

        String formativeOrgUnitOld = extract.getEducationOrgUnitOld().getFormativeOrgUnit().getGenitiveCaseTitle();
        String formativeOrgUnitNew = extract.getEducationOrgUnitNew().getFormativeOrgUnit().getGenitiveCaseTitle();

        String territorialOrgUnitOld = extract.getEducationOrgUnitOld().getTerritorialOrgUnit().getPrintTitle();
        String territorialOrgUnitNew = extract.getEducationOrgUnitNew().getTerritorialOrgUnit().getPrintTitle();

        StringBuilder comment = new StringBuilder("Перевод с ").append(extract.getGroupOld().getCourse().getTitle()).append(" курса").append(isInternalForm ? " гр. " + extract.getGroupOld().getTitle() : "");
        comment.append(" ").append(formativeOrgUnitOld).append(!extract.getEducationOrgUnitOld().getTerritorialOrgUnit().isTop() ? " (" + territorialOrgUnitOld + ")" : "");
        comment.append(" на ").append(extract.getCourse().getTitle()).append(" курс").append(isInternalForm ? " гр. " + extract.getGroupNew().getTitle() : "");
        comment.append(" ").append(formativeOrgUnitNew).append(!extract.getEducationOrgUnitNew().getTerritorialOrgUnit().isTop() ? " (" + territorialOrgUnitNew + ")" : "");
        comment.append(" по ").append(devForm.getGenCaseTitle()).append(" форме обучения");
        if (!extract.getEducationOrgUnitNew().getDevelopTech().getCode().equals(DevelopTechCodes.GENERAL))
            comment.append(" с применением дистанционных образовательных технологий");

        comment.append(" на ").append(extract.getCompensationTypeNew().isBudget()? "бюджетной основе" : "договорной основе");

        if (extract.isChangeEduPlan())
            comment.append(". Разницу в учебных планах ликвидировать в срок до " + date(extract.getChangeEduPlanDeadlineDate()));
        return comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(ChangeProfileStuListExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment = new StringBuilder("Перевод ").append(getTransferEduStr(extract.getEducationOrgUnitOld(), false, false)).append(" ").append(getTransferEduStr(extract.getEducationOrgUnitNew(), true, false));
        if (extract.isChangeEduPlan())
            comment.append(". Разницу в учебных планах ликвидировать в срок до ").append(date(extract.getChangeEduPlanDeadlineDate()));
        return  comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(ChangeProfileAndGroupStuListExtract extract, boolean isPrintOrder)
    {
        return "Перевод "+ getTransferEduStr(extract.getEducationOrgUnitNew(), true, false)
                + ", перераспределение из гр. " + extract.getGroupOld().getTitle()+ " в гр. " + extract.getGroupNew().getTitle() + order(extract, isPrintOrder);
    }


    // Базовый метод для вывода даты и номера приказа
    public String getDefaultOrder(AbstractStudentExtract extract, boolean isPrintOrder, boolean isComma)
    {
        if (isPrintOrder)
        {
            String comma = "";
            if (isComma) comma = ", ";

            if (null != extract.getParagraph() || null != extract.getParagraph().getOrder())
            {
                AbstractStudentOrder order = extract.getParagraph().getOrder();
                if (null != order.getNumber())
                    return comma + "приказ № " + order.getNumber() + (order.getCommitDate() != null ? " от " + date(order.getCommitDate()) + " года." : "");
                else if (order.getCommitDate() != null)
                    return comma + "приказ от " + date(order.getCommitDate()) + " года.";
            }
        }
        return "";
    }

    public String getAssignEduStr(EducationOrgUnit eou)
    {
        EducationLevels eduLevel = eou.getEducationLevelHighSchool().getEducationLevel();
        StructureEducationLevels level = eduLevel.getLevelType();
        return " за " + (level.isSpecialization() ? "специализацией" : level.isMaster() ? "магистерской программой" : "профилем") + " «" + eduLevel.getTitle() + "»";
    }


    public String getTransferEduStr(EducationOrgUnit eou, boolean isNew, boolean isPrintParent)
    {
        String direction = isNew ? "на направление" : "с направления";
        String specialty = isNew ? "на специальность" : "со специальности";
        String specialization = isNew ? "специализацию" : "специализации";
        String masterProgram = isNew ? "магистерскую программу" : "магистерской программы";
        String profile = isNew ? "профиль" : "профиля";

        EducationLevelsHighSchool highSchool = eou.getEducationLevelHighSchool();
        EducationLevels parentEduLevel = EducationOrgUnitUtil.getParentLevel(highSchool);
        EducationLevels eduLevel = highSchool.getEducationLevel();

        StructureEducationLevels parentLevel = parentEduLevel.getLevelType();
        StructureEducationLevels childLevel = eduLevel.getLevelType();

        String result="";

        if (eduLevel.isProfileOrSpecialization())
        {
            if (!isPrintParent)
              result =  isNew? "на " : (childLevel.isSpecialization()? "co " : "c ");
            result += (childLevel.isSpecialization() ? specialization : childLevel.isMaster() ? masterProgram : profile) + " «" + eduLevel.getTitle() + "»";
        }
        if (isPrintParent)
            result = (parentLevel.isSpecialty() ? specialty : direction) + ": " + parentEduLevel.getTitleCodePrefix() + " «" + parentEduLevel.getTitle() + "»" + result;

        return result;
    }

    // Вывод даты и номера приказа с запятой
    public String order(AbstractStudentExtract extract, boolean isPrintOrder)
    {
        return getDefaultOrder(extract, isPrintOrder, true);
    }

    // Вывод причины приказа
    public String reason(StudentOrderReasons reason)
    {
        return null != reason ? null != reason.getPrintTitle() ? " " + reason.getPrintTitle() : "" : "";
    }

    public String date(Date date)
    {
        return null != date ? DateFormatter.DEFAULT_DATE_FORMATTER.format(date) : "";
    }

    public String money(Double source)
    {
        return RuMoneyFormatter.INSTANCE.format(source);
    }

    public boolean isMale(AbstractStudentExtract extract)
    {
        return extract.getEntity().getPerson().getIdentityCard().getSex().isMale();
    }

    /**
     * Формирование строки к приказам о переводе между подразделениями
     * @param extract - выписка
     * @param showFormativeOrgUnit - нужно ли показывать данные формирующего подразделения
     * @param showTerritorialOrgUnit - нужно ли показывать данные территориального подразделения
     */
    protected String getTransferOrgUnitString(CommonStuExtract extract, boolean showFormativeOrgUnit, boolean showTerritorialOrgUnit)
    {
        DevelopForm devForm = extract.getEducationOrgUnitNew().getDevelopForm();
        Boolean isInternalForm = devForm.getCode().equals(DevelopFormCodes.FULL_TIME_FORM);

        String formativeOrgUnitOld = extract.getEducationOrgUnitOld().getFormativeOrgUnit().getGenitiveCaseTitle();
        String formativeOrgUnitNew = extract.getEducationOrgUnitNew().getFormativeOrgUnit().getGenitiveCaseTitle();

        String territorialOrgUnitOld = extract.getEducationOrgUnitOld().getTerritorialOrgUnit().getPrintTitle();
        String territorialOrgUnitNew = extract.getEducationOrgUnitNew().getTerritorialOrgUnit().getPrintTitle();

        StringBuilder comment = new StringBuilder(" с ").append(extract.getCourseOld().getTitle()).append(" курса")
                .append(isInternalForm ? " гр. " + (extract.getGroupOld() != null? extract.getGroupOld().getTitle():"") : "");
        comment.append(showFormativeOrgUnit ? " " + formativeOrgUnitOld : "")
                .append(showTerritorialOrgUnit && !extract.getEducationOrgUnitOld().getTerritorialOrgUnit().isTop() ? " (" + territorialOrgUnitOld + ")" : "") ;
        comment.append(" на ").append(extract.getCourseNew().getTitle()).append(" курс").append(isInternalForm ? " гр. " + extract.getGroupNew().getTitle() : "");
        comment.append(showFormativeOrgUnit ? " " + formativeOrgUnitNew : "")
                .append(showTerritorialOrgUnit && !extract.getEducationOrgUnitNew().getTerritorialOrgUnit().isTop() ? " (" + territorialOrgUnitNew + ")" : "");
        comment.append(" по ").append(devForm.getGenCaseTitle()).append(" форме обучения");
        if (!extract.getEducationOrgUnitNew().getDevelopTech().getCode().equals(DevelopTechCodes.GENERAL))
            comment.append(" с применением дистанционных образовательных технологий");

        comment.append(" на ").append(extract.getCompensationTypeNew().isBudget()? "бюджетной основе" : "договорной основе");
        return comment.toString();
    }



    private String getCategoryString(StudentCategory category, boolean isNew)
    {
        if (category.isDppListener())
            return (!isNew ? "Слушателя ДПО" : "слушателем ДПО");
        if (category.isListener())
            return (!isNew ? "Слушателя" : "слушателем");
        else
            return (!isNew ? "Студента" : "студентом");
    }
}
