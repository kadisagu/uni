/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeIdentityCard.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.bo.util.CountryAutocompleteModel;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.utils.AddressBaseUtils;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Nationality;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.movestudent.component.customorder.IStudentCustomOrderExtract;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class DAO extends BaseCustomActionAddEditDAO<ChangeIdentityCardAction, Model>
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        if (model.isActionActive())
        {
            model.setCitizenshipModel(new CountryAutocompleteModel());
            model.setIdentityCardTypeList(getCatalogItemListOrderByCode(IdentityCardType.class));
            model.setNationalityModel(new LazySimpleSelectModel<>(Nationality.class).setRecordLimit(50));
            model.setSexList(getCatalogItemList(Sex.class));

            ChangeIdentityCardAction action = model.getCustomAction();
            Student student = model.getStudent();
            if (!model.isEdit() && student != null)
            {
                IdentityCard card = student.getPerson().getIdentityCard();
                action.setLastNameNew(card.getLastName());
                action.setFirstNameNew(card.getFirstName());
                action.setMiddleNameNew(card.getMiddleName());
                action.setSex(card.getSex());
                action.setBirthDate(card.getBirthDate());
                action.setBirthPlace(card.getBirthPlace());
                action.setNationality(card.getNationality());
                action.setCitizenship(card.getCitizenship());
            }
        }
    }

    @Override
    protected Class<ChangeIdentityCardAction> getActionClass()
    {
        return ChangeIdentityCardAction.class;
    }

    @Override
    public void updatePrepare(Model model, ChangeIdentityCardAction action, IStudentCustomOrderExtract extract)
    {
        Student student = (Student) action.getCustomExtract().getEntity();
        action.setLastActiveIdentityCard(student.getPerson().getIdentityCard());

        action.setFirstNameNew(StringUtils.capitalize(model.getCustomAction().getFirstNameNew()));
        action.setLastNameNew(StringUtils.capitalize(model.getCustomAction().getLastNameNew()));
        action.setMiddleNameNew(StringUtils.capitalize(model.getCustomAction().getMiddleNameNew()));
    }

    @Override
    public void doCommit(ChangeIdentityCardAction action)
    {
        Student student = (Student) action.getCustomExtract().getEntity();
        IdentityCard oldCard = student.getPerson().getIdentityCard();

        IdentityCard card = new IdentityCard();
        card.setFirstName(action.getFirstNameNew());
        card.setLastName(action.getLastNameNew());
        card.setMiddleName(action.getMiddleNameNew());
        card.setSeria(action.getCardSeria());
        card.setNumber(action.getCardNumber());
        card.setBirthDate(action.getBirthDate());
        card.setBirthPlace(action.getBirthPlace());
        card.setIssuanceDate(action.getIssuanceDate());
        card.setIssuancePlace(action.getIssuancePlace());
        card.setNationality(action.getNationality());
        card.setSex(action.getSex());
        card.setCitizenship(action.getCitizenship());
        card.setCardType(action.getCardType());
        card.setPerson(student.getPerson());

        // фото копируется из предыдущего
        if (oldCard.getPhoto() != null)
        {
            DatabaseFile photo = new DatabaseFile();
            photo.update(oldCard.getPhoto());
            save(photo);
            card.setPhoto(photo);
        }

        // адрес копируется из предыдущего
        if (oldCard.getAddress() != null)
        {
            AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(card, AddressBaseUtils.getSameAddress(oldCard.getAddress()), IdentityCard.L_ADDRESS);
        }

        save(card);

        student.getPerson().setIdentityCard(card);
        update(student.getPerson());

        action.setLastActiveIdentityCard(oldCard); //save rollback data
        action.setIdentityCardNew(card);
        update(action);
    }

    @Override
    public void doRollback(ChangeIdentityCardAction action)
    {
        Student student = (Student) action.getCustomExtract().getEntity();

        IdentityCard identityCardActive = student.getPerson().getIdentityCard();
        IdentityCard identityCardNew = action.getIdentityCardNew();

        if (!identityCardActive.equals(identityCardNew))
            throw new ApplicationException("Текущее удостоверение личности для «" + student.getPerson().getFullFio() + "» уже не то, что было установлено выпиской. Откат выписки невозможен.");

        action.setIdentityCardNew(null);
        student.getPerson().setIdentityCard(action.getLastActiveIdentityCard());
        update(student.getPerson());
        update(action);

        delete(identityCardNew);
    }
}