/* $Id$ */
package ru.tandemservice.movestudent.component.settings.IndividualOrderSettings;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.movestudent.entity.ExtractCreationRule;

/**
 * @author Dmitry Seleznev
 * @since 25.09.2012
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().updateForNewExtracts();
        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        boolean admin = UserContext.getInstance().getPrincipalContext().isAdmin();
        DynamicListDataSource<ExtractCreationRule> dataSource = new DynamicListDataSource<ExtractCreationRule>(component, this);
        dataSource.addColumn(new SimpleColumn("Название", admin ? (ExtractCreationRule.studentExtractType().titleWithCode().s()) : ExtractCreationRule.studentExtractType().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ToggleColumn("Приказ формируется индивидуально", ExtractCreationRule.individualOrder().s()).setListener("onClickChangeIndividualExtract"));
        getModel(component).setDataSource(dataSource);
    }

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickChangeIndividualExtract(IBusinessComponent component)
    {
        getDao().updateExtractIndividual((Long) component.getListenerParameter());
    }
}