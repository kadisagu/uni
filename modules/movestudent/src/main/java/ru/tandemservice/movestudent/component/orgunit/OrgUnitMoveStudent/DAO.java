/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.component.orgunit.OrgUnitMoveStudent;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.component.commons.*;
import ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder.*;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.movestudent.ui.UnimoveStudentMultiSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.movestudent.component.commons.OrderFlexBuilder.*;

/**
 * @author vip_delete
 * @since 18.10.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry(IMoveStudentMQBuilder.EXTRACT_ALIAS);

    static
    {
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_FULL_FIO, new OrderDescription(IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS, IdentityCard.P_LAST_NAME), new OrderDescription(IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS, IdentityCard.P_FIRST_NAME), new OrderDescription(IMoveStudentMQBuilder.IDENTITY_CARD_ALIAS, IdentityCard.P_MIDDLE_NAME));
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_EXTRACT_FORMATIVE_ORG_UNIT_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_TITLE}));
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_EXTRACT_TERRITORIAL_ORG_UNIT_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_TITLE}));
        _orderSettings.setOrders(MoveStudentDefines.STUDENT_EXTRACT_PRODUCING_ORG_UNIT_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.L_ORG_UNIT, OrgUnit.P_TITLE}));
        _orderSettings.setOrders(MoveStudentDefines.EXTRACT_ORDER_TYPE_TITLE, new OrderDescription(IMoveStudentMQBuilder.EXTRACT_ALIAS, new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, IAbstractOrder.L_TYPE, StudentExtractType.P_TITLE}));
    }

    @Override
    public void prepare(final Model model)
    {
        final boolean admin = UserContext.getInstance().getPrincipalContext().isAdmin();

        model.setOrgUnit(getNotNull(model.getOrgUnitId()));
        String orgUnitType = model.getOrgUnit().getOrgUnitType().getCode();
        model.setEducationYearList(new EducationYearModel());
        model.setOrdersStateList(getCatalogItemList(OrderStates.class));
        model.setStudentList(new UnimoveStudentMultiSelectModel(model.getOrgUnit()));
        model.setEmployeePostModel(new OrderExecutorSelectModel());

        //individual orders tab
        ExtractListUtil.initExtractListModel(model.getIndividualOrdersModel(), orgUnitType + "_individualOrders", OrderCategory.INDIVIDUAL);

        //projects tab
        ExtractListUtil.initExtractListModel(model.getProjectsModel(), orgUnitType + "_projects", OrderCategory.MODULAR);
        model.getProjectsModel().getExtractStateList().remove(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
        model.getProjectsModel().getExtractStateList().remove(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED));

        //modular order tab
        model.setModularOrdersSecModel(new CommonPostfixPermissionModel(orgUnitType + "_modularorders"));

        //modular extracts tab
        ExtractListUtil.initExtractListModel(model.getExtractsModel(), orgUnitType + "_extracts", OrderCategory.MODULAR);
        List<ExtractStates> extractStateList = model.getExtractsModel().getExtractStateList();
        extractStateList.remove(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE));
        extractStateList.remove(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTABLE));
        extractStateList.remove(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED));
        extractStateList.remove(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_REJECTED));

        //list orders tab
        model.setListOrdersSecModel(new CommonPostfixPermissionModel(orgUnitType + "_listorders"));

        model.setListOrderTypesListModel(new CommonMultiSelectModel(admin ? StudentExtractType.P_TITLE_WITH_CODE : StudentExtractType.P_TITLE)
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                        .fromEntity(AbstractStudentExtract.class, "ase").column(property(AbstractStudentExtract.type().id().fromAlias("ase")))
                        .where(or(
                                eq(property(AbstractStudentExtract.type().code().fromAlias("ase")), value(StudentExtractTypeCodes.LIST_ORDER)),
                                eq(property(AbstractStudentExtract.type().parent().code().fromAlias("ase")), value(StudentExtractTypeCodes.LIST_ORDER))
                        ));

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(StudentExtractType.class, "sext").column("sext")
                        .where(eq(property(StudentExtractType.parent().code().fromAlias("sext")), value(StudentExtractTypeCodes.LIST_ORDER)))
                        .where(or(
                                eq(property(StudentExtractType.active().fromAlias("sext")), value(Boolean.TRUE)),
                                in(property(StudentExtractType.id().fromAlias("sext")), subBuilder.buildQuery())
                        ))
                        .order(property(StudentExtractType.title().fromAlias("sext")));

                if(admin)
                {
                    builder.where(or(
                            likeUpper(property(StudentExtractType.title().fromAlias("sext")), value(CoreStringUtils.escapeLike(filter, true))),
                            likeUpper(property(StudentExtractType.code().fromAlias("sext")), value(CoreStringUtils.escapeLike(filter, true)))
                    ));
                }
                else builder.where(likeUpper(property(StudentExtractType.title().fromAlias("sext")), value(CoreStringUtils.escapeLike(filter, true))));

                if (set != null) builder.where(in(property(StudentExtractType.id().fromAlias("sext")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        model.setListOrderReasonModel(new CommonMultiSelectModel(StudentOrderReasons.title().s())
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                List<StudentExtractType> typesList = model.getListOrdersSettings().get(IMoveStudentMQBuilder.TYPE);

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(StudentReasonToTypeRel.class, "rel").column(property(StudentReasonToTypeRel.first().fromAlias("rel")))
                        .where(likeUpper(property(StudentReasonToTypeRel.first().title().fromAlias("rel")), value(CoreStringUtils.escapeLike(filter, true))));
                if (null != typesList && !typesList.isEmpty())
                    builder.where(in(property(StudentReasonToTypeRel.second().fromAlias("rel")), typesList));
                if (set != null) builder.where(in(property(StudentReasonToTypeRel.first().id().fromAlias("rel")), set));
                builder.order(property(StudentReasonToTypeRel.first().title().fromAlias("rel")));
                return new DQLListResultBuilder(builder, 50);
            }
        });

        //list extracts tab
        ExtractListUtil.initExtractListModel(model.getListextractsModel(), orgUnitType + "_listextracts", OrderCategory.LIST);
        List<ExtractStates> listExtractStateList = new ArrayList<>();
        listExtractStateList.add(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
        listExtractStateList.add(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED));
        model.getListextractsModel().setExtractStateList(listExtractStateList);

        //other orders tab
        ExtractListUtil.initExtractListModel(model.getOtherOrdersModel(), orgUnitType + "_otherOrders", OrderCategory.OTHER);
        List<OrderStates> otherOrderStateList = new ArrayList<>();
        otherOrderStateList.add(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
        otherOrderStateList.add(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED));
        model.setOtherOrdersStateList(otherOrderStateList);

        model.getOtherOrdersModel().setExtractTypesListModel(new CommonMultiSelectModel(admin ? StudentExtractType.P_TITLE_WITH_CODE : StudentExtractType.P_TITLE)
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                Set<Long> ids = new HashSet<>();
                List<StudentExtractType> list = getList(StudentExtractType.class);
                for (StudentExtractType type : list)
                {
                    StudentExtractType parent = type.getParent();
                    while (null != parent)
                    {
                        if (null == parent.getParent() && type.isActive() && StudentExtractTypeCodes.OTHER_ORDER.equals(parent.getCode()))
                        {
                            ids.add(type.getId());
                        }
                        parent = parent.getParent();
                    }
                }

                DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                        .fromEntity(AbstractStudentExtract.class, "ase").column(property(AbstractStudentExtract.type().id().fromAlias("ase")))
                        .where(in(property(AbstractStudentExtract.type().id().fromAlias("ase")), ids))
                        .predicate(DQLPredicateType.distinct);
                ids.addAll(subBuilder.createStatement(getSession()).<Long>list());

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(StudentExtractType.class, "sext").column("sext")
                        .where(in(property(StudentExtractType.id().fromAlias("sext")), ids))
                        .order(property(StudentExtractType.title().fromAlias("sext")));

                if(admin)
                {
                    builder.where(or(likeUpper(property(StudentExtractType.title().fromAlias("sext")), value(CoreStringUtils.escapeLike(filter, true))),
                            likeUpper(property(StudentExtractType.code().fromAlias("sext")), value(CoreStringUtils.escapeLike(filter, true)))
                    ));
                }
                else builder.where(likeUpper(property(StudentExtractType.title().fromAlias("sext")), value(CoreStringUtils.escapeLike(filter, true))));

                if (set != null) builder.where(in(property(StudentExtractType.id().fromAlias("sext")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        // Здесь покоится "Фамилия[студента]"
        // поле с вводом фамилии студента было решено убрать, а вместо него построить мульти селект
        // теперь выбранные пользователем значения необходимо грохнуть
        model.getIndividualOrdersModel().getSettings().set(IMoveStudentMQBuilder.LAST_NAME, null);
        model.getListOrdersSettings().set(IMoveStudentMQBuilder.LAST_NAME, null);
        model.getModularOrdersSettings().set(IMoveStudentMQBuilder.LAST_NAME, null);
        model.getListextractsModel().getSettings().set(IMoveStudentMQBuilder.LAST_NAME, null);
        model.getExtractsModel().getSettings().set(IMoveStudentMQBuilder.LAST_NAME, null);
        model.getProjectsModel().getSettings().set(IMoveStudentMQBuilder.LAST_NAME, null);
        model.setExtractTypesListModel(new ExtractTypeSelectModel(OrderCategory.MODULAR));
    }

    @Override
    public void prepareIndividualOrdersDataSource(Model model)
    {
        ExtractListModel individualOrdersModel = model.getIndividualOrdersModel();

        // init datasource
        IMoveStudentMQBuilder builder = MoveStudentMQBuilder.createExtractMQBuilder(ModularStudentExtract.class, individualOrdersModel.getSettings());

        // apply user defined restrictions
        MQBuilder subBuilder = new MQBuilder(ExtractCreationRule.ENTITY_CLASS, "cr", new String[]{ExtractCreationRule.studentExtractType().id().s()});
        subBuilder.add(MQExpression.eq("cr", ExtractCreationRule.individualOrder().s(), Boolean.TRUE));
        builder.getMQBuilder().add(MQExpression.in(IMoveStudentMQBuilder.EXTRACT_ALIAS, ModularStudentExtract.type().id().s(), subBuilder));
        builder.getMQBuilder().add(MQExpression.isNotNull(IMoveStudentMQBuilder.EXTRACT_ALIAS, ModularStudentExtract.paragraph().s()));

        // apply orgunit restrictions
        AbstractExpression eqFormative = MQExpression.eq(IMoveStudentMQBuilder.EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getOrgUnit());
        AbstractExpression eqTerritorial = MQExpression.eq(IMoveStudentMQBuilder.EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getOrgUnit());
        AbstractExpression eqOrgUnit = MQExpression.eq(IMoveStudentMQBuilder.EXTRACT_ALIAS, AbstractStudentExtract.L_ORG_UNIT, model.getOrgUnit());
        AbstractExpression nullOrgUnit = MQExpression.isNull(IMoveStudentMQBuilder.EXTRACT_ALIAS, AbstractStudentExtract.L_ORG_UNIT);
        builder.getMQBuilder().add(MQExpression.or(eqOrgUnit, MQExpression.and(MQExpression.or(eqFormative, eqTerritorial), nullOrgUnit)));

        // apply filters
        builder.applyExtractEducationYear();
        builder.applyExtractCreateDate();
        builder.applyOrderCommitDate();
        builder.applyOrderNumber();
        builder.applyExtractOrderDate();
        builder.applyExtractLastName();
        builder.applyExtractFormativeOrgUnit();
        builder.applyExtractTerritorialOrgUnit();
        builder.applyExtractProducingOrgUnit();
        builder.applyExtractEducationLevelHighSchool();
        builder.applyExtractCourse();
        builder.applyExtractDevelopForm();
        builder.applyExtractCompensationType();
        builder.applyExtractType();
        builder.applyOrderState();
        builder.applyExtractReason();
        builder.applyOrderStudent(getSession());
        builder.applyExecutor();

        // create page
        builder.createPage(getSession(), _orderSettings, individualOrdersModel.getDataSource());
    }

    @Override
    public void prepareProjectsDataSource(Model model)
    {
        ExtractListModel projectsModel = model.getProjectsModel();

        // init datasource
        IMoveStudentMQBuilder builder = MoveStudentMQBuilder.createExtractMQBuilder(ModularStudentExtract.class, projectsModel.getSettings());

        // apply user defined restrictions
        builder.getMQBuilder().add(MQExpression.isNull(IMoveStudentMQBuilder.EXTRACT_ALIAS, IAbstractExtract.L_PARAGRAPH));

        // apply orgunit restrictions
        AbstractExpression eqFormative = MQExpression.eq(IMoveStudentMQBuilder.EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getOrgUnit());
        AbstractExpression eqTerritorial = MQExpression.eq(IMoveStudentMQBuilder.EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getOrgUnit());
        AbstractExpression eqOrgUnit = MQExpression.eq(IMoveStudentMQBuilder.EXTRACT_ALIAS, AbstractStudentExtract.L_ORG_UNIT, model.getOrgUnit());
        AbstractExpression nullOrgUnit = MQExpression.isNull(IMoveStudentMQBuilder.EXTRACT_ALIAS, AbstractStudentExtract.L_ORG_UNIT);
        builder.getMQBuilder().add(MQExpression.or(eqOrgUnit, MQExpression.and(MQExpression.or(eqFormative, eqTerritorial), nullOrgUnit)));

        // apply filters
        builder.applyExtractEducationYear();
        builder.applyExtractCreateDate();
        builder.applyExtractLastName();
        builder.applyExtractFormativeOrgUnit();
        builder.applyExtractTerritorialOrgUnit();
        builder.applyExtractProducingOrgUnit();
        builder.applyExtractEducationLevelHighSchool();
        builder.applyExtractCourse();
        builder.applyExtractDevelopForm();
        builder.applyExtractCompensationType();
        builder.applyExtractType();
        builder.applyExtractState();
        builder.applyExtractReason();
        builder.applyExtractStudent(getSession());

        // create page
        builder.createPage(getSession(), _orderSettings, projectsModel.getDataSource());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareModularOrdersDataSource(final Model model)
    {
        IFlexFilter context = new IFlexFilter()
        {
            @Override
            public String getAlias()
            {
                return ORDER_DATA_JOIN_ALIAS;
            }

            @Override
            public boolean addToBuilder(IDataSettings dataSettings, DQLSelectBuilder builder, IJoinCreator joinCreator)
            {
                builder.where(and(
                        // огрюнит
                        eq(property(ORDER_ALIAS, AbstractStudentOrder.L_ORG_UNIT), value(model.getOrgUnitId())),
                        // нет еще выписок, либо это сбоный приказ
                        eq(DQLFunctions.coalesce(property(ORDER_DATA_JOIN_ALIAS, IS_INDIVIDUAL_ORDER), value(Boolean.FALSE)), value(Boolean.FALSE))
                ));
                return true;
            }
        };

        IFlexConfig extractConfig = new FlexConfig(AbstractStudentExtract.class, EXTRACT_ALIAS)
                .addColumns(new SimpleFlexColumn(PARAGRAPH_ALIAS, IAbstractParagraph.L_ORDER))
                .addJoins(new SimpleFlexJoin(PARAGRAPH_ALIAS, EXTRACT_ALIAS, IAbstractExtract.L_PARAGRAPH, true), STUDENT_JOIN)
                .addFilters(STUDENT_FILTER, EXTRACT_TYPE_FILTER);

        final IFlexColumn stateCol = new SimpleFlexColumn(ORDER_ALIAS, IAbstractOrder.L_STATE + ".code", "stateCol", false);

        IFlexConfig mainConfig = new FlexConfig(StudentModularOrder.class, ORDER_ALIAS)
                .addColumns(
                        ORDER_CREATE_DATE_COLUMN,
                        ORDER_COMMIT_DATE_COLUMN,
                        ORDER_NUMBER_COLUMN,
                        ORDER_STATE_COLUMN,
                        ORDER_COMMIT_DATE_SYSTEM_COLUMN,
                        EXTRACT_COUNT_COLUMN,
                        ORDER_EXECUTOR_COLUMN,
                        stateCol
                )
                .addJoins(
                        new OrdersDataJoin(Arrays.asList(OrderCategory.MODULAR_ORDER_CATEGORY, OrderCategory.INDIVIDUAL_ORDER_CATEGORY), DQLJoinType.left, false, false, false, true)
                )
                .addOrders(
                        ORDER_CREATE_DATE_SORT,
                        ORDER_COMMIT_DATE_SORT,
                        ORDER_NUMBER_SORT,
                        ORDER_EXTRACT_COUNT_SORT,
                        ORDER_EXECUTOR_SORT,
                        ORDER_COMMIT_SYSTEM_DATE_SORT
                )
                .addFilters(
                        context,
                        ORDER_EDU_YEAR_FILTER,
                        ORDER_COMMIT_DATE_PERIOD_FILTER,
                        ORDER_CREATE_DATE_PERIOD_FILTER,
                        ORDER_COMMIT_DATE_SYSTEM_PERIOD_FILTER,
                        ORDER_NUMBER_FILTER,
                        ORDER_STATE_FILTER,
                        EXECUTOR_FILTER,
                        new SubQueryFlexFilter(ORDER_ALIAS, "id", extractConfig, IAbstractParagraph.L_ORDER)
                );

        FlexDQLBuilder flexBuilder = new FlexDQLBuilder(mainConfig, false);

        flexBuilder.fillDataSource(getSession(), model.getModularOrdersSettings(), model.getModularOrdersdataSource(), new IPatchResolver()
        {
            @Override
            public void patch(List<Object[]> srcList, List<IEntity> destList, List<IFlexColumn> columns)
            {
                int extractCountIdx = columns.indexOf(EXTRACT_COUNT_COLUMN);
                int stateColIdx = columns.indexOf(stateCol);
                int numberColIdx = columns.indexOf(ORDER_NUMBER_COLUMN);
                for (int i = 0; i < srcList.size(); i++)
                {
                    Object[] cols = srcList.get(i);
                    DataWrapper wrapper = (DataWrapper) destList.get(i);

                    Number count = (Number) cols[extractCountIdx];
                    wrapper.setProperty(DISABLE_PRINT, count == null || count.longValue() == 0L);
                    wrapper.setProperty(AbstractStudentOrder.P_READONLY, !UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(cols[stateColIdx]));
                    String orderNumber = (String) cols[numberColIdx];
                    wrapper.setTitle("Приказ №" + (orderNumber != null ? orderNumber : ""));
                }
            }
        });
    }

    @Override
    public void prepareExtractsDataSource(Model model)
    {
        ExtractListModel extractsModel = model.getExtractsModel();

        // init datasource
        IMoveStudentMQBuilder builder = MoveStudentMQBuilder.createExtractMQBuilder(ModularStudentExtract.class, extractsModel.getSettings());

        // apply user defined restrictions
        builder.getMQBuilder().add(MQExpression.isNotNull(IMoveStudentMQBuilder.EXTRACT_ALIAS, IAbstractExtract.L_PARAGRAPH));

        // apply orgunit restrictions
        AbstractExpression eqFormative = MQExpression.eq(IMoveStudentMQBuilder.EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getOrgUnit());
        AbstractExpression eqTerritorial = MQExpression.eq(IMoveStudentMQBuilder.EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getOrgUnit());
        AbstractExpression eqOrgUnit = MQExpression.eq(IMoveStudentMQBuilder.EXTRACT_ALIAS, AbstractStudentExtract.L_ORG_UNIT, model.getOrgUnit());
        AbstractExpression nullOrgUnit = MQExpression.isNull(IMoveStudentMQBuilder.EXTRACT_ALIAS, AbstractStudentExtract.L_ORG_UNIT);
        builder.getMQBuilder().add(MQExpression.or(eqOrgUnit, MQExpression.and(MQExpression.or(eqFormative, eqTerritorial), nullOrgUnit)));

        // apply filters
        builder.applyExtractEducationYear();
        builder.applyExtractCreateDate();
        builder.applyExtractOrderDate();
        builder.applyOrderNumber();
        builder.applyOrderCommitDateSystem();
        builder.applyExtractLastName();
        builder.applyExtractFormativeOrgUnit();
        builder.applyExtractTerritorialOrgUnit();
        builder.applyExtractProducingOrgUnit();
        builder.applyExtractEducationLevelHighSchool();
        builder.applyExtractCourse();
        builder.applyExtractDevelopForm();
        builder.applyExtractCompensationType();
        builder.applyExtractType();
        builder.applyExtractState();
        builder.applyExtractReason();
        builder.applyExtractStudent(getSession());

        // create page
        builder.createPage(getSession(), _orderSettings, extractsModel.getDataSource());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListOrdersDataSource(Model model)
    {
        // init datasource
        IMoveStudentMQBuilder builder = MoveStudentMQBuilder.createOrderMQBuilder(StudentListOrder.class, model.getListOrdersSettings());

        // apply user defined restrictions
        builder.getMQBuilder().add(MQExpression.eq(IMoveStudentMQBuilder.ORDER_ALIAS, AbstractStudentOrder.L_ORG_UNIT, model.getOrgUnit()));

        // apply filters
        builder.applyOrderEducationYear();
        builder.applyOrderCreateDate();
        builder.applyOrderCommitDate();
        builder.applyOrderNumber();
        builder.applyOrderState();
        builder.applyOrderCommitDateSystem();
        builder.applyListOrderTypes();
        builder.applyListOrderReason();
        builder.applyOrderStudent(getSession());
        builder.applyExecutor();

        // apply order
        new OrderDescriptionRegistry(IMoveStudentMQBuilder.ORDER_ALIAS).applyOrder(builder.getMQBuilder(), model.getListOrdersdataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getListOrdersdataSource(), builder.getMQBuilder(), getSession());

        // get statistics for view properties

        String hql = "select " + IAbstractParagraph.L_ORDER + ".id, count(*) from " + StudentListParagraph.ENTITY_CLASS
                + " where " + IAbstractParagraph.L_ORDER + " is not null group by " + IAbstractParagraph.L_ORDER;
        Map<Long, Integer> id2count = new HashMap<>();
        for (Object[] row : (List<Object[]>) getSession().createQuery(hql).list())
        {
            Long id = (Long) row[0];
            Number count = (Number) row[1];
            id2count.put(id, count == null ? 0 : count.intValue());
        }

        // inject view properties
        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getListOrdersdataSource()))
        {
            Integer value = id2count.get(wrapper.getId());
            wrapper.setViewProperty(AbstractStudentOrder.P_COUNT_PARAGRAPH, value == null ? 0 : value);
            wrapper.setViewProperty("disabledPrint", value == null);
        }
    }

    @Override
    public void prepareListExtractsDataSource(Model model)
    {
        ExtractListModel listExtractsModel = model.getListextractsModel();

        // init dataSource
        IMoveStudentMQBuilder builder = MoveStudentMQBuilder.createExtractMQBuilder(ListStudentExtract.class, listExtractsModel.getSettings());

        // apply user defined restrictions
        builder.getMQBuilder().add(MQExpression.isNotNull(IMoveStudentMQBuilder.EXTRACT_ALIAS, IAbstractExtract.L_PARAGRAPH));

        // apply orgunit restrictions
        AbstractExpression eqFormative = MQExpression.eq(IMoveStudentMQBuilder.EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getOrgUnit());
        AbstractExpression eqTerritorial = MQExpression.eq(IMoveStudentMQBuilder.EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getOrgUnit());
        builder.getMQBuilder().add(MQExpression.or(eqFormative, eqTerritorial));

        // apply filters
        builder.applyExtractEducationYear();
        //builder.applyExtractCreateDate();
        builder.applyExtractOrderDate();
        builder.applyOrderNumber();
        builder.applyOrderCommitDateSystem();
        builder.applyExtractLastName();
        builder.applyExtractFormativeOrgUnit();
        builder.applyExtractTerritorialOrgUnit();
        builder.applyExtractProducingOrgUnit();
        builder.applyExtractEducationLevelHighSchool();
        builder.applyExtractCourse();
        builder.applyExtractDevelopForm();
        builder.applyExtractCompensationType();
        builder.applyListExtractType();
        builder.applyListOrderReason();
        builder.applyExtractState();
        builder.applyExtractStudent(getSession());

        // create page
        builder.createPage(getSession(), _orderSettings, listExtractsModel.getDataSource());

        // get statistics for view properties
        DynamicListDataSource<AbstractStudentExtract> ds = listExtractsModel.getDataSource();
        Set<Long> disabledExtractTypes = MoveStudentDaoFacade.getMoveStudentDao().getDisabledFormPrintingExtracts(ds.getEntityList(), MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        // inject view properties
        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(listExtractsModel.getDataSource()))
        {
            wrapper.setViewProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY, disabledExtractTypes.contains(wrapper.getId()));
        }
    }

    @Override
    public void prepareOtherOrdersDataSource(Model model)
    {
        ExtractListModel otherOrdersModel = model.getOtherOrdersModel();

        // init datasource
        IMoveStudentMQBuilder builder = MoveStudentMQBuilder.createExtractMQBuilder(OtherStudentExtract.class, otherOrdersModel.getSettings());

        // apply orgunit restrictions
        AbstractExpression eqFormative = MQExpression.eq(IMoveStudentMQBuilder.EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getOrgUnit());
        AbstractExpression eqTerritorial = MQExpression.eq(IMoveStudentMQBuilder.EDUCATION_ORGUNIT_ALIAS, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getOrgUnit());
        AbstractExpression eqOrgUnit = MQExpression.eq(IMoveStudentMQBuilder.EXTRACT_ALIAS, AbstractStudentExtract.L_ORG_UNIT, model.getOrgUnit());
        AbstractExpression nullOrgUnit = MQExpression.isNull(IMoveStudentMQBuilder.EXTRACT_ALIAS, AbstractStudentExtract.L_ORG_UNIT);
        builder.getMQBuilder().add(MQExpression.or(eqOrgUnit, MQExpression.and(MQExpression.or(eqFormative, eqTerritorial), nullOrgUnit)));

        // apply filters
        builder.applyExtractOrderEducationYear();
        builder.applyExtractCreateDate();
        builder.applyOrderCommitDate();
        builder.applyOrderNumber();
        builder.applyExtractOrderDate();
        builder.applyExtractLastName();
        builder.applyExtractFormativeOrgUnit();
        builder.applyExtractTerritorialOrgUnit();
        builder.applyExtractProducingOrgUnit();
        builder.applyExtractEducationLevelHighSchool();
        builder.applyExtractCourse();
        builder.applyExtractDevelopForm();
        builder.applyExtractCompensationType();
        builder.applyExtractType();
        builder.applyOrderState();
        builder.applyExtractReason();
        builder.applyOrderStudent(getSession());
        builder.applyExecutor();

        // create page
        builder.createPage(getSession(), _orderSettings, otherOrdersModel.getDataSource());

        // get statistics for view properties
        DynamicListDataSource<OtherStudentExtract> ds = otherOrdersModel.getDataSource();
        Set<Long> disabledExtractTypes = MoveStudentDaoFacade.getMoveStudentDao().getPrintFormAttachedOtherOrders(ds.getEntityList());

        // inject view properties
        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(otherOrdersModel.getDataSource()))
        {
            wrapper.setViewProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY, disabledExtractTypes.contains(wrapper.getId()));
        }
    }

    //

    @Override
    public void deleteIndividualOrder(Long orderId)
    {
        StudentModularOrder order = getNotNull(StudentModularOrder.class, orderId);
        if (UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(order.getState().getCode()))
            MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(order);
        else
            MoveDaoFacade.getMoveDao().deleteOrder(order, getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED));
    }

    @Override
    public void deleteModularOrder(Long orderId)
    {
        StudentModularOrder order = getNotNull(StudentModularOrder.class, orderId);
        if (UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(order.getState().getCode()))
            MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(order);
        else
            MoveDaoFacade.getMoveDao().deleteOrder(order, getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED));
    }

    @Override
    public void deleteListOrder(Long orderId)
    {
        StudentListOrder order = getNotNull(StudentListOrder.class, orderId);
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(order);
    }

    @Override
    public void deleteModularExtract(IBusinessComponent component)
    {
        ModularStudentExtract extract = get(ModularStudentExtract.class, (Long) component.getListenerParameter());
        MoveDaoFacade.getMoveDao().deleteExtractWithParagraph(extract);
    }
}