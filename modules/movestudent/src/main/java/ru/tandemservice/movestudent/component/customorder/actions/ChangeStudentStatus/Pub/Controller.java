/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeStudentStatus.Pub;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.BaseCustomActionPubController;
import ru.tandemservice.movestudent.entity.custom.ChangeStudentStatusAction;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class Controller extends BaseCustomActionPubController<ChangeStudentStatusAction, IDAO, Model>
{
}