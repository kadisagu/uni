package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITakeOffPayment;
import ru.tandemservice.movestudent.entity.gen.AcadGrantPaymentStopStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О прекращении выплаты академической стипендии
 */
public class AcadGrantPaymentStopStuExtract extends AcadGrantPaymentStopStuExtractGen implements ITakeOffPayment
{
    @Override
    public Date getBeginDate()
    {
        return getAcadGrantPaymentStopDate();
    }

    @Override
    public Date getPaymentTakeOffDate()
    {
        return getAcadGrantPaymentStopDate();
    }
}