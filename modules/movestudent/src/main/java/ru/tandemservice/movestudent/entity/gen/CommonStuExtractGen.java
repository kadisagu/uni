package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Базовая выписка из сборного (индивидуального) приказа по студентам о переводе
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CommonStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.CommonStuExtract";
    public static final String ENTITY_NAME = "commonStuExtract";
    public static final int VERSION_HASH = 2095451402;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATE = "date";
    public static final String L_EDUCATION_ORG_UNIT_NEW = "educationOrgUnitNew";
    public static final String L_COURSE_NEW = "courseNew";
    public static final String L_GROUP_NEW = "groupNew";
    public static final String L_COMPENSATION_TYPE_NEW = "compensationTypeNew";
    public static final String P_BOOK_NUMBER_NEW = "bookNumberNew";
    public static final String P_TARGET_ADMISSION_NEW = "targetAdmissionNew";
    public static final String L_EDUCATION_ORG_UNIT_OLD = "educationOrgUnitOld";
    public static final String L_COURSE_OLD = "courseOld";
    public static final String L_GROUP_OLD = "groupOld";
    public static final String L_COMPENSATION_TYPE_OLD = "compensationTypeOld";
    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";
    public static final String P_BOOK_NUMBER_OLD = "bookNumberOld";
    public static final String P_TARGET_ADMISSION_OLD = "targetAdmissionOld";

    private Date _date;     // Дата
    private EducationOrgUnit _educationOrgUnitNew;     // Новый уровень образования подразделения
    private Course _courseNew;     // Новый курс
    private Group _groupNew;     // Новая группа
    private CompensationType _compensationTypeNew;     // Новый вид возмещения затрат
    private String _bookNumberNew;     // Новый номер зачетной книжки
    private Boolean _targetAdmissionNew;     // Новый статус целевого приема
    private EducationOrgUnit _educationOrgUnitOld;     // Предыдущий уровень образования подразделения
    private Course _courseOld;     // Предыдущий курс студента
    private Group _groupOld;     // Предыдущая группа студента
    private CompensationType _compensationTypeOld;     // Предыдущий вид возмещения затрат
    private StudentStatus _studentStatusOld;     // Предыдущее состояние студента
    private String _bookNumberOld;     // Предыдущий номер зачетной книжки
    private Boolean _targetAdmissionOld;     // Предыдущий статус целевого приема

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Новый уровень образования подразделения. Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitNew()
    {
        return _educationOrgUnitNew;
    }

    /**
     * @param educationOrgUnitNew Новый уровень образования подразделения. Свойство не может быть null.
     */
    public void setEducationOrgUnitNew(EducationOrgUnit educationOrgUnitNew)
    {
        dirty(_educationOrgUnitNew, educationOrgUnitNew);
        _educationOrgUnitNew = educationOrgUnitNew;
    }

    /**
     * @return Новый курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourseNew()
    {
        return _courseNew;
    }

    /**
     * @param courseNew Новый курс. Свойство не может быть null.
     */
    public void setCourseNew(Course courseNew)
    {
        dirty(_courseNew, courseNew);
        _courseNew = courseNew;
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroupNew()
    {
        return _groupNew;
    }

    /**
     * @param groupNew Новая группа. Свойство не может быть null.
     */
    public void setGroupNew(Group groupNew)
    {
        dirty(_groupNew, groupNew);
        _groupNew = groupNew;
    }

    /**
     * @return Новый вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationTypeNew()
    {
        return _compensationTypeNew;
    }

    /**
     * @param compensationTypeNew Новый вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationTypeNew(CompensationType compensationTypeNew)
    {
        dirty(_compensationTypeNew, compensationTypeNew);
        _compensationTypeNew = compensationTypeNew;
    }

    /**
     * Если поле не заполнено, номер зачетки студента не меняется при проведении приказа и не откатывается при откате.
     *
     * @return Новый номер зачетной книжки.
     */
    @Length(max=255)
    public String getBookNumberNew()
    {
        return _bookNumberNew;
    }

    /**
     * @param bookNumberNew Новый номер зачетной книжки.
     */
    public void setBookNumberNew(String bookNumberNew)
    {
        dirty(_bookNumberNew, bookNumberNew);
        _bookNumberNew = bookNumberNew;
    }

    /**
     * Если поле не заполнено, статус ЦП студента не меняется при проведении приказа и не откатывается при откате.
     *
     * @return Новый статус целевого приема.
     */
    public Boolean getTargetAdmissionNew()
    {
        return _targetAdmissionNew;
    }

    /**
     * @param targetAdmissionNew Новый статус целевого приема.
     */
    public void setTargetAdmissionNew(Boolean targetAdmissionNew)
    {
        dirty(_targetAdmissionNew, targetAdmissionNew);
        _targetAdmissionNew = targetAdmissionNew;
    }

    /**
     * @return Предыдущий уровень образования подразделения. Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitOld()
    {
        return _educationOrgUnitOld;
    }

    /**
     * @param educationOrgUnitOld Предыдущий уровень образования подразделения. Свойство не может быть null.
     */
    public void setEducationOrgUnitOld(EducationOrgUnit educationOrgUnitOld)
    {
        dirty(_educationOrgUnitOld, educationOrgUnitOld);
        _educationOrgUnitOld = educationOrgUnitOld;
    }

    /**
     * @return Предыдущий курс студента. Свойство не может быть null.
     */
    @NotNull
    public Course getCourseOld()
    {
        return _courseOld;
    }

    /**
     * @param courseOld Предыдущий курс студента. Свойство не может быть null.
     */
    public void setCourseOld(Course courseOld)
    {
        dirty(_courseOld, courseOld);
        _courseOld = courseOld;
    }

    /**
     * @return Предыдущая группа студента.
     */
    public Group getGroupOld()
    {
        return _groupOld;
    }

    /**
     * @param groupOld Предыдущая группа студента.
     */
    public void setGroupOld(Group groupOld)
    {
        dirty(_groupOld, groupOld);
        _groupOld = groupOld;
    }

    /**
     * @return Предыдущий вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationTypeOld()
    {
        return _compensationTypeOld;
    }

    /**
     * @param compensationTypeOld Предыдущий вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationTypeOld(CompensationType compensationTypeOld)
    {
        dirty(_compensationTypeOld, compensationTypeOld);
        _compensationTypeOld = compensationTypeOld;
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Предыдущее состояние студента. Свойство не может быть null.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    /**
     * @return Предыдущий номер зачетной книжки.
     */
    @Length(max=255)
    public String getBookNumberOld()
    {
        return _bookNumberOld;
    }

    /**
     * @param bookNumberOld Предыдущий номер зачетной книжки.
     */
    public void setBookNumberOld(String bookNumberOld)
    {
        dirty(_bookNumberOld, bookNumberOld);
        _bookNumberOld = bookNumberOld;
    }

    /**
     * @return Предыдущий статус целевого приема.
     */
    public Boolean getTargetAdmissionOld()
    {
        return _targetAdmissionOld;
    }

    /**
     * @param targetAdmissionOld Предыдущий статус целевого приема.
     */
    public void setTargetAdmissionOld(Boolean targetAdmissionOld)
    {
        dirty(_targetAdmissionOld, targetAdmissionOld);
        _targetAdmissionOld = targetAdmissionOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof CommonStuExtractGen)
        {
            setDate(((CommonStuExtract)another).getDate());
            setEducationOrgUnitNew(((CommonStuExtract)another).getEducationOrgUnitNew());
            setCourseNew(((CommonStuExtract)another).getCourseNew());
            setGroupNew(((CommonStuExtract)another).getGroupNew());
            setCompensationTypeNew(((CommonStuExtract)another).getCompensationTypeNew());
            setBookNumberNew(((CommonStuExtract)another).getBookNumberNew());
            setTargetAdmissionNew(((CommonStuExtract)another).getTargetAdmissionNew());
            setEducationOrgUnitOld(((CommonStuExtract)another).getEducationOrgUnitOld());
            setCourseOld(((CommonStuExtract)another).getCourseOld());
            setGroupOld(((CommonStuExtract)another).getGroupOld());
            setCompensationTypeOld(((CommonStuExtract)another).getCompensationTypeOld());
            setStudentStatusOld(((CommonStuExtract)another).getStudentStatusOld());
            setBookNumberOld(((CommonStuExtract)another).getBookNumberOld());
            setTargetAdmissionOld(((CommonStuExtract)another).getTargetAdmissionOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CommonStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CommonStuExtract.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("CommonStuExtract is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "date":
                    return obj.getDate();
                case "educationOrgUnitNew":
                    return obj.getEducationOrgUnitNew();
                case "courseNew":
                    return obj.getCourseNew();
                case "groupNew":
                    return obj.getGroupNew();
                case "compensationTypeNew":
                    return obj.getCompensationTypeNew();
                case "bookNumberNew":
                    return obj.getBookNumberNew();
                case "targetAdmissionNew":
                    return obj.getTargetAdmissionNew();
                case "educationOrgUnitOld":
                    return obj.getEducationOrgUnitOld();
                case "courseOld":
                    return obj.getCourseOld();
                case "groupOld":
                    return obj.getGroupOld();
                case "compensationTypeOld":
                    return obj.getCompensationTypeOld();
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
                case "bookNumberOld":
                    return obj.getBookNumberOld();
                case "targetAdmissionOld":
                    return obj.getTargetAdmissionOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "educationOrgUnitNew":
                    obj.setEducationOrgUnitNew((EducationOrgUnit) value);
                    return;
                case "courseNew":
                    obj.setCourseNew((Course) value);
                    return;
                case "groupNew":
                    obj.setGroupNew((Group) value);
                    return;
                case "compensationTypeNew":
                    obj.setCompensationTypeNew((CompensationType) value);
                    return;
                case "bookNumberNew":
                    obj.setBookNumberNew((String) value);
                    return;
                case "targetAdmissionNew":
                    obj.setTargetAdmissionNew((Boolean) value);
                    return;
                case "educationOrgUnitOld":
                    obj.setEducationOrgUnitOld((EducationOrgUnit) value);
                    return;
                case "courseOld":
                    obj.setCourseOld((Course) value);
                    return;
                case "groupOld":
                    obj.setGroupOld((Group) value);
                    return;
                case "compensationTypeOld":
                    obj.setCompensationTypeOld((CompensationType) value);
                    return;
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
                case "bookNumberOld":
                    obj.setBookNumberOld((String) value);
                    return;
                case "targetAdmissionOld":
                    obj.setTargetAdmissionOld((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "date":
                        return true;
                case "educationOrgUnitNew":
                        return true;
                case "courseNew":
                        return true;
                case "groupNew":
                        return true;
                case "compensationTypeNew":
                        return true;
                case "bookNumberNew":
                        return true;
                case "targetAdmissionNew":
                        return true;
                case "educationOrgUnitOld":
                        return true;
                case "courseOld":
                        return true;
                case "groupOld":
                        return true;
                case "compensationTypeOld":
                        return true;
                case "studentStatusOld":
                        return true;
                case "bookNumberOld":
                        return true;
                case "targetAdmissionOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "date":
                    return true;
                case "educationOrgUnitNew":
                    return true;
                case "courseNew":
                    return true;
                case "groupNew":
                    return true;
                case "compensationTypeNew":
                    return true;
                case "bookNumberNew":
                    return true;
                case "targetAdmissionNew":
                    return true;
                case "educationOrgUnitOld":
                    return true;
                case "courseOld":
                    return true;
                case "groupOld":
                    return true;
                case "compensationTypeOld":
                    return true;
                case "studentStatusOld":
                    return true;
                case "bookNumberOld":
                    return true;
                case "targetAdmissionOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "date":
                    return Date.class;
                case "educationOrgUnitNew":
                    return EducationOrgUnit.class;
                case "courseNew":
                    return Course.class;
                case "groupNew":
                    return Group.class;
                case "compensationTypeNew":
                    return CompensationType.class;
                case "bookNumberNew":
                    return String.class;
                case "targetAdmissionNew":
                    return Boolean.class;
                case "educationOrgUnitOld":
                    return EducationOrgUnit.class;
                case "courseOld":
                    return Course.class;
                case "groupOld":
                    return Group.class;
                case "compensationTypeOld":
                    return CompensationType.class;
                case "studentStatusOld":
                    return StudentStatus.class;
                case "bookNumberOld":
                    return String.class;
                case "targetAdmissionOld":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CommonStuExtract> _dslPath = new Path<CommonStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CommonStuExtract");
    }
            

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Новый уровень образования подразделения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getEducationOrgUnitNew()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
    {
        return _dslPath.educationOrgUnitNew();
    }

    /**
     * @return Новый курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getCourseNew()
     */
    public static Course.Path<Course> courseNew()
    {
        return _dslPath.courseNew();
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getGroupNew()
     */
    public static Group.Path<Group> groupNew()
    {
        return _dslPath.groupNew();
    }

    /**
     * @return Новый вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getCompensationTypeNew()
     */
    public static CompensationType.Path<CompensationType> compensationTypeNew()
    {
        return _dslPath.compensationTypeNew();
    }

    /**
     * Если поле не заполнено, номер зачетки студента не меняется при проведении приказа и не откатывается при откате.
     *
     * @return Новый номер зачетной книжки.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getBookNumberNew()
     */
    public static PropertyPath<String> bookNumberNew()
    {
        return _dslPath.bookNumberNew();
    }

    /**
     * Если поле не заполнено, статус ЦП студента не меняется при проведении приказа и не откатывается при откате.
     *
     * @return Новый статус целевого приема.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getTargetAdmissionNew()
     */
    public static PropertyPath<Boolean> targetAdmissionNew()
    {
        return _dslPath.targetAdmissionNew();
    }

    /**
     * @return Предыдущий уровень образования подразделения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getEducationOrgUnitOld()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
    {
        return _dslPath.educationOrgUnitOld();
    }

    /**
     * @return Предыдущий курс студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getCourseOld()
     */
    public static Course.Path<Course> courseOld()
    {
        return _dslPath.courseOld();
    }

    /**
     * @return Предыдущая группа студента.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getGroupOld()
     */
    public static Group.Path<Group> groupOld()
    {
        return _dslPath.groupOld();
    }

    /**
     * @return Предыдущий вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getCompensationTypeOld()
     */
    public static CompensationType.Path<CompensationType> compensationTypeOld()
    {
        return _dslPath.compensationTypeOld();
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    /**
     * @return Предыдущий номер зачетной книжки.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getBookNumberOld()
     */
    public static PropertyPath<String> bookNumberOld()
    {
        return _dslPath.bookNumberOld();
    }

    /**
     * @return Предыдущий статус целевого приема.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getTargetAdmissionOld()
     */
    public static PropertyPath<Boolean> targetAdmissionOld()
    {
        return _dslPath.targetAdmissionOld();
    }

    public static class Path<E extends CommonStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _date;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitNew;
        private Course.Path<Course> _courseNew;
        private Group.Path<Group> _groupNew;
        private CompensationType.Path<CompensationType> _compensationTypeNew;
        private PropertyPath<String> _bookNumberNew;
        private PropertyPath<Boolean> _targetAdmissionNew;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitOld;
        private Course.Path<Course> _courseOld;
        private Group.Path<Group> _groupOld;
        private CompensationType.Path<CompensationType> _compensationTypeOld;
        private StudentStatus.Path<StudentStatus> _studentStatusOld;
        private PropertyPath<String> _bookNumberOld;
        private PropertyPath<Boolean> _targetAdmissionOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(CommonStuExtractGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Новый уровень образования подразделения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getEducationOrgUnitNew()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
        {
            if(_educationOrgUnitNew == null )
                _educationOrgUnitNew = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_NEW, this);
            return _educationOrgUnitNew;
        }

    /**
     * @return Новый курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getCourseNew()
     */
        public Course.Path<Course> courseNew()
        {
            if(_courseNew == null )
                _courseNew = new Course.Path<Course>(L_COURSE_NEW, this);
            return _courseNew;
        }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getGroupNew()
     */
        public Group.Path<Group> groupNew()
        {
            if(_groupNew == null )
                _groupNew = new Group.Path<Group>(L_GROUP_NEW, this);
            return _groupNew;
        }

    /**
     * @return Новый вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getCompensationTypeNew()
     */
        public CompensationType.Path<CompensationType> compensationTypeNew()
        {
            if(_compensationTypeNew == null )
                _compensationTypeNew = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE_NEW, this);
            return _compensationTypeNew;
        }

    /**
     * Если поле не заполнено, номер зачетки студента не меняется при проведении приказа и не откатывается при откате.
     *
     * @return Новый номер зачетной книжки.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getBookNumberNew()
     */
        public PropertyPath<String> bookNumberNew()
        {
            if(_bookNumberNew == null )
                _bookNumberNew = new PropertyPath<String>(CommonStuExtractGen.P_BOOK_NUMBER_NEW, this);
            return _bookNumberNew;
        }

    /**
     * Если поле не заполнено, статус ЦП студента не меняется при проведении приказа и не откатывается при откате.
     *
     * @return Новый статус целевого приема.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getTargetAdmissionNew()
     */
        public PropertyPath<Boolean> targetAdmissionNew()
        {
            if(_targetAdmissionNew == null )
                _targetAdmissionNew = new PropertyPath<Boolean>(CommonStuExtractGen.P_TARGET_ADMISSION_NEW, this);
            return _targetAdmissionNew;
        }

    /**
     * @return Предыдущий уровень образования подразделения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getEducationOrgUnitOld()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
        {
            if(_educationOrgUnitOld == null )
                _educationOrgUnitOld = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_OLD, this);
            return _educationOrgUnitOld;
        }

    /**
     * @return Предыдущий курс студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getCourseOld()
     */
        public Course.Path<Course> courseOld()
        {
            if(_courseOld == null )
                _courseOld = new Course.Path<Course>(L_COURSE_OLD, this);
            return _courseOld;
        }

    /**
     * @return Предыдущая группа студента.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getGroupOld()
     */
        public Group.Path<Group> groupOld()
        {
            if(_groupOld == null )
                _groupOld = new Group.Path<Group>(L_GROUP_OLD, this);
            return _groupOld;
        }

    /**
     * @return Предыдущий вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getCompensationTypeOld()
     */
        public CompensationType.Path<CompensationType> compensationTypeOld()
        {
            if(_compensationTypeOld == null )
                _compensationTypeOld = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE_OLD, this);
            return _compensationTypeOld;
        }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

    /**
     * @return Предыдущий номер зачетной книжки.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getBookNumberOld()
     */
        public PropertyPath<String> bookNumberOld()
        {
            if(_bookNumberOld == null )
                _bookNumberOld = new PropertyPath<String>(CommonStuExtractGen.P_BOOK_NUMBER_OLD, this);
            return _bookNumberOld;
        }

    /**
     * @return Предыдущий статус целевого приема.
     * @see ru.tandemservice.movestudent.entity.CommonStuExtract#getTargetAdmissionOld()
     */
        public PropertyPath<Boolean> targetAdmissionOld()
        {
            if(_targetAdmissionOld == null )
                _targetAdmissionOld = new PropertyPath<Boolean>(CommonStuExtractGen.P_TARGET_ADMISSION_OLD, this);
            return _targetAdmissionOld;
        }

        public Class getEntityClass()
        {
            return CommonStuExtract.class;
        }

        public String getEntityName()
        {
            return "commonStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
