/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e54.AddEdit;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.ChangePassStateExamsStuExtract;

import java.util.Arrays;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 15.08.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<ChangePassStateExamsStuExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setYear(model.getExtract().getYear() == 0 ? null : model.getExtract().getYear());
        model.setSeasons(Arrays.asList((ApplicationRuntime.getProperty("seasons_A")).split(";")));
        model.setSeason(model.getSeasons().contains(model.getExtract().getSeason()) ? model.getExtract().getSeason() : null);
    }

    @Override
    protected ChangePassStateExamsStuExtract createNewInstance()
    {
        return new ChangePassStateExamsStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setYear(model.getYear());
        model.getExtract().setSeason(model.getSeason());
        super.update(model);
    }
}