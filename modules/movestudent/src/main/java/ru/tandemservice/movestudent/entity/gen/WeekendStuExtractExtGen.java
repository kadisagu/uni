package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.WeekendStuExtractExt;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О предоставлении академического отпуска (расш. вариант)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class WeekendStuExtractExtGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.WeekendStuExtractExt";
    public static final String ENTITY_NAME = "weekendStuExtractExt";
    public static final int VERSION_HASH = -912452109;
    private static IEntityMeta ENTITY_META;

    public static final String P_REASON_STR = "reasonStr";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_STOP_GRANTS_PAYING = "stopGrantsPaying";
    public static final String P_STOP_PAYING_FROM = "stopPayingFrom";
    public static final String P_STOP_PAYING_TO = "stopPayingTo";
    public static final String P_ASSIGN_PAYMENT = "assignPayment";
    public static final String P_PAYMENT_VALUE = "paymentValue";
    public static final String P_PAYMENT_ASSIGN_DATE = "paymentAssignDate";
    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";
    public static final String P_PREV_BEGIN_DATE = "prevBeginDate";
    public static final String P_PREV_END_DATE = "prevEndDate";

    private String _reasonStr;     // В связи с
    private Date _beginDate;     // Дата начала академ. отпуска
    private Date _endDate;     // Дата окончания академ. отпуска
    private boolean _stopGrantsPaying;     // Прекратить выплату стипендии
    private Date _stopPayingFrom;     // Прекратить с
    private Date _stopPayingTo;     // Прекратить по
    private boolean _assignPayment;     // Назначить выплату компенсации
    private String _paymentValue;     // В размере
    private Date _paymentAssignDate;     // Назначить с
    private StudentStatus _studentStatusOld;     // Предыдущее состояние студента
    private Date _prevBeginDate;     // Дата начала отпуска по предыдущему приказу
    private Date _prevEndDate;     // Дата окончания отпуска по предыдущему приказу

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return В связи с. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getReasonStr()
    {
        return _reasonStr;
    }

    /**
     * @param reasonStr В связи с. Свойство не может быть null.
     */
    public void setReasonStr(String reasonStr)
    {
        dirty(_reasonStr, reasonStr);
        _reasonStr = reasonStr;
    }

    /**
     * @return Дата начала академ. отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала академ. отпуска. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания академ. отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания академ. отпуска. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Прекратить выплату стипендии. Свойство не может быть null.
     */
    @NotNull
    public boolean isStopGrantsPaying()
    {
        return _stopGrantsPaying;
    }

    /**
     * @param stopGrantsPaying Прекратить выплату стипендии. Свойство не может быть null.
     */
    public void setStopGrantsPaying(boolean stopGrantsPaying)
    {
        dirty(_stopGrantsPaying, stopGrantsPaying);
        _stopGrantsPaying = stopGrantsPaying;
    }

    /**
     * @return Прекратить с.
     */
    public Date getStopPayingFrom()
    {
        return _stopPayingFrom;
    }

    /**
     * @param stopPayingFrom Прекратить с.
     */
    public void setStopPayingFrom(Date stopPayingFrom)
    {
        dirty(_stopPayingFrom, stopPayingFrom);
        _stopPayingFrom = stopPayingFrom;
    }

    /**
     * @return Прекратить по.
     */
    public Date getStopPayingTo()
    {
        return _stopPayingTo;
    }

    /**
     * @param stopPayingTo Прекратить по.
     */
    public void setStopPayingTo(Date stopPayingTo)
    {
        dirty(_stopPayingTo, stopPayingTo);
        _stopPayingTo = stopPayingTo;
    }

    /**
     * @return Назначить выплату компенсации. Свойство не может быть null.
     */
    @NotNull
    public boolean isAssignPayment()
    {
        return _assignPayment;
    }

    /**
     * @param assignPayment Назначить выплату компенсации. Свойство не может быть null.
     */
    public void setAssignPayment(boolean assignPayment)
    {
        dirty(_assignPayment, assignPayment);
        _assignPayment = assignPayment;
    }

    /**
     * @return В размере.
     */
    @Length(max=255)
    public String getPaymentValue()
    {
        return _paymentValue;
    }

    /**
     * @param paymentValue В размере.
     */
    public void setPaymentValue(String paymentValue)
    {
        dirty(_paymentValue, paymentValue);
        _paymentValue = paymentValue;
    }

    /**
     * @return Назначить с.
     */
    public Date getPaymentAssignDate()
    {
        return _paymentAssignDate;
    }

    /**
     * @param paymentAssignDate Назначить с.
     */
    public void setPaymentAssignDate(Date paymentAssignDate)
    {
        dirty(_paymentAssignDate, paymentAssignDate);
        _paymentAssignDate = paymentAssignDate;
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Предыдущее состояние студента. Свойство не может быть null.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    /**
     * @return Дата начала отпуска по предыдущему приказу.
     */
    public Date getPrevBeginDate()
    {
        return _prevBeginDate;
    }

    /**
     * @param prevBeginDate Дата начала отпуска по предыдущему приказу.
     */
    public void setPrevBeginDate(Date prevBeginDate)
    {
        dirty(_prevBeginDate, prevBeginDate);
        _prevBeginDate = prevBeginDate;
    }

    /**
     * @return Дата окончания отпуска по предыдущему приказу.
     */
    public Date getPrevEndDate()
    {
        return _prevEndDate;
    }

    /**
     * @param prevEndDate Дата окончания отпуска по предыдущему приказу.
     */
    public void setPrevEndDate(Date prevEndDate)
    {
        dirty(_prevEndDate, prevEndDate);
        _prevEndDate = prevEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof WeekendStuExtractExtGen)
        {
            setReasonStr(((WeekendStuExtractExt)another).getReasonStr());
            setBeginDate(((WeekendStuExtractExt)another).getBeginDate());
            setEndDate(((WeekendStuExtractExt)another).getEndDate());
            setStopGrantsPaying(((WeekendStuExtractExt)another).isStopGrantsPaying());
            setStopPayingFrom(((WeekendStuExtractExt)another).getStopPayingFrom());
            setStopPayingTo(((WeekendStuExtractExt)another).getStopPayingTo());
            setAssignPayment(((WeekendStuExtractExt)another).isAssignPayment());
            setPaymentValue(((WeekendStuExtractExt)another).getPaymentValue());
            setPaymentAssignDate(((WeekendStuExtractExt)another).getPaymentAssignDate());
            setStudentStatusOld(((WeekendStuExtractExt)another).getStudentStatusOld());
            setPrevBeginDate(((WeekendStuExtractExt)another).getPrevBeginDate());
            setPrevEndDate(((WeekendStuExtractExt)another).getPrevEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends WeekendStuExtractExtGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) WeekendStuExtractExt.class;
        }

        public T newInstance()
        {
            return (T) new WeekendStuExtractExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "reasonStr":
                    return obj.getReasonStr();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "stopGrantsPaying":
                    return obj.isStopGrantsPaying();
                case "stopPayingFrom":
                    return obj.getStopPayingFrom();
                case "stopPayingTo":
                    return obj.getStopPayingTo();
                case "assignPayment":
                    return obj.isAssignPayment();
                case "paymentValue":
                    return obj.getPaymentValue();
                case "paymentAssignDate":
                    return obj.getPaymentAssignDate();
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
                case "prevBeginDate":
                    return obj.getPrevBeginDate();
                case "prevEndDate":
                    return obj.getPrevEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "reasonStr":
                    obj.setReasonStr((String) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "stopGrantsPaying":
                    obj.setStopGrantsPaying((Boolean) value);
                    return;
                case "stopPayingFrom":
                    obj.setStopPayingFrom((Date) value);
                    return;
                case "stopPayingTo":
                    obj.setStopPayingTo((Date) value);
                    return;
                case "assignPayment":
                    obj.setAssignPayment((Boolean) value);
                    return;
                case "paymentValue":
                    obj.setPaymentValue((String) value);
                    return;
                case "paymentAssignDate":
                    obj.setPaymentAssignDate((Date) value);
                    return;
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
                case "prevBeginDate":
                    obj.setPrevBeginDate((Date) value);
                    return;
                case "prevEndDate":
                    obj.setPrevEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reasonStr":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "stopGrantsPaying":
                        return true;
                case "stopPayingFrom":
                        return true;
                case "stopPayingTo":
                        return true;
                case "assignPayment":
                        return true;
                case "paymentValue":
                        return true;
                case "paymentAssignDate":
                        return true;
                case "studentStatusOld":
                        return true;
                case "prevBeginDate":
                        return true;
                case "prevEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reasonStr":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "stopGrantsPaying":
                    return true;
                case "stopPayingFrom":
                    return true;
                case "stopPayingTo":
                    return true;
                case "assignPayment":
                    return true;
                case "paymentValue":
                    return true;
                case "paymentAssignDate":
                    return true;
                case "studentStatusOld":
                    return true;
                case "prevBeginDate":
                    return true;
                case "prevEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "reasonStr":
                    return String.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "stopGrantsPaying":
                    return Boolean.class;
                case "stopPayingFrom":
                    return Date.class;
                case "stopPayingTo":
                    return Date.class;
                case "assignPayment":
                    return Boolean.class;
                case "paymentValue":
                    return String.class;
                case "paymentAssignDate":
                    return Date.class;
                case "studentStatusOld":
                    return StudentStatus.class;
                case "prevBeginDate":
                    return Date.class;
                case "prevEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<WeekendStuExtractExt> _dslPath = new Path<WeekendStuExtractExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "WeekendStuExtractExt");
    }
            

    /**
     * @return В связи с. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getReasonStr()
     */
    public static PropertyPath<String> reasonStr()
    {
        return _dslPath.reasonStr();
    }

    /**
     * @return Дата начала академ. отпуска. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания академ. отпуска. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Прекратить выплату стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#isStopGrantsPaying()
     */
    public static PropertyPath<Boolean> stopGrantsPaying()
    {
        return _dslPath.stopGrantsPaying();
    }

    /**
     * @return Прекратить с.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getStopPayingFrom()
     */
    public static PropertyPath<Date> stopPayingFrom()
    {
        return _dslPath.stopPayingFrom();
    }

    /**
     * @return Прекратить по.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getStopPayingTo()
     */
    public static PropertyPath<Date> stopPayingTo()
    {
        return _dslPath.stopPayingTo();
    }

    /**
     * @return Назначить выплату компенсации. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#isAssignPayment()
     */
    public static PropertyPath<Boolean> assignPayment()
    {
        return _dslPath.assignPayment();
    }

    /**
     * @return В размере.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getPaymentValue()
     */
    public static PropertyPath<String> paymentValue()
    {
        return _dslPath.paymentValue();
    }

    /**
     * @return Назначить с.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getPaymentAssignDate()
     */
    public static PropertyPath<Date> paymentAssignDate()
    {
        return _dslPath.paymentAssignDate();
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    /**
     * @return Дата начала отпуска по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getPrevBeginDate()
     */
    public static PropertyPath<Date> prevBeginDate()
    {
        return _dslPath.prevBeginDate();
    }

    /**
     * @return Дата окончания отпуска по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getPrevEndDate()
     */
    public static PropertyPath<Date> prevEndDate()
    {
        return _dslPath.prevEndDate();
    }

    public static class Path<E extends WeekendStuExtractExt> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<String> _reasonStr;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Boolean> _stopGrantsPaying;
        private PropertyPath<Date> _stopPayingFrom;
        private PropertyPath<Date> _stopPayingTo;
        private PropertyPath<Boolean> _assignPayment;
        private PropertyPath<String> _paymentValue;
        private PropertyPath<Date> _paymentAssignDate;
        private StudentStatus.Path<StudentStatus> _studentStatusOld;
        private PropertyPath<Date> _prevBeginDate;
        private PropertyPath<Date> _prevEndDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return В связи с. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getReasonStr()
     */
        public PropertyPath<String> reasonStr()
        {
            if(_reasonStr == null )
                _reasonStr = new PropertyPath<String>(WeekendStuExtractExtGen.P_REASON_STR, this);
            return _reasonStr;
        }

    /**
     * @return Дата начала академ. отпуска. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(WeekendStuExtractExtGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания академ. отпуска. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(WeekendStuExtractExtGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Прекратить выплату стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#isStopGrantsPaying()
     */
        public PropertyPath<Boolean> stopGrantsPaying()
        {
            if(_stopGrantsPaying == null )
                _stopGrantsPaying = new PropertyPath<Boolean>(WeekendStuExtractExtGen.P_STOP_GRANTS_PAYING, this);
            return _stopGrantsPaying;
        }

    /**
     * @return Прекратить с.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getStopPayingFrom()
     */
        public PropertyPath<Date> stopPayingFrom()
        {
            if(_stopPayingFrom == null )
                _stopPayingFrom = new PropertyPath<Date>(WeekendStuExtractExtGen.P_STOP_PAYING_FROM, this);
            return _stopPayingFrom;
        }

    /**
     * @return Прекратить по.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getStopPayingTo()
     */
        public PropertyPath<Date> stopPayingTo()
        {
            if(_stopPayingTo == null )
                _stopPayingTo = new PropertyPath<Date>(WeekendStuExtractExtGen.P_STOP_PAYING_TO, this);
            return _stopPayingTo;
        }

    /**
     * @return Назначить выплату компенсации. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#isAssignPayment()
     */
        public PropertyPath<Boolean> assignPayment()
        {
            if(_assignPayment == null )
                _assignPayment = new PropertyPath<Boolean>(WeekendStuExtractExtGen.P_ASSIGN_PAYMENT, this);
            return _assignPayment;
        }

    /**
     * @return В размере.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getPaymentValue()
     */
        public PropertyPath<String> paymentValue()
        {
            if(_paymentValue == null )
                _paymentValue = new PropertyPath<String>(WeekendStuExtractExtGen.P_PAYMENT_VALUE, this);
            return _paymentValue;
        }

    /**
     * @return Назначить с.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getPaymentAssignDate()
     */
        public PropertyPath<Date> paymentAssignDate()
        {
            if(_paymentAssignDate == null )
                _paymentAssignDate = new PropertyPath<Date>(WeekendStuExtractExtGen.P_PAYMENT_ASSIGN_DATE, this);
            return _paymentAssignDate;
        }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

    /**
     * @return Дата начала отпуска по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getPrevBeginDate()
     */
        public PropertyPath<Date> prevBeginDate()
        {
            if(_prevBeginDate == null )
                _prevBeginDate = new PropertyPath<Date>(WeekendStuExtractExtGen.P_PREV_BEGIN_DATE, this);
            return _prevBeginDate;
        }

    /**
     * @return Дата окончания отпуска по предыдущему приказу.
     * @see ru.tandemservice.movestudent.entity.WeekendStuExtractExt#getPrevEndDate()
     */
        public PropertyPath<Date> prevEndDate()
        {
            if(_prevEndDate == null )
                _prevEndDate = new PropertyPath<Date>(WeekendStuExtractExtGen.P_PREV_END_DATE, this);
            return _prevEndDate;
        }

        public Class getEntityClass()
        {
            return WeekendStuExtractExt.class;
        }

        public String getEntityName()
        {
            return "weekendStuExtractExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
