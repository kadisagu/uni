/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeGroup.AddEdit;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.IBaseCustomActionAddEditDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeGroupAction;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public interface IDAO extends IBaseCustomActionAddEditDAO<ChangeGroupAction, Model>
{
}