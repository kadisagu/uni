/* $Id: DAO.java.vm 24710 2012-10-30 11:06:12Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e92.AddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.TransferCourseWithChangeTerrOuStuExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 14.01.2013
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<TransferCourseWithChangeTerrOuStuExtract, Model> implements IDAO
{
    @Override
    protected TransferCourseWithChangeTerrOuStuExtract createNewInstance()
    {
        return new TransferCourseWithChangeTerrOuStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setEduModel(MoveStudentDaoFacade.getCommonExtractUtil().createEduModel(model.isAddForm(), model.getExtract()));

        //FIXME WTF? зачем тут OrgUnitKindAutocompleteModel?
        model.getEduModel().setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL)
        {
            @Override
            public ListResult<OrgUnit> findValues(String filter)
            {
                EducationOrgUnit eduOrgUnit = model.getExtract().getEntity().getEducationOrgUnit();
                if (model.getEduModel().getCourse() == null || eduOrgUnit.getFormativeOrgUnit() == null)
                    return ListResult.getEmpty();

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "eou");
                builder.column(property(EducationOrgUnit.territorialOrgUnit().fromAlias("eou")));
                builder.where(ne(property(EducationOrgUnit.territorialOrgUnit().fromAlias("eou")), value(eduOrgUnit.getTerritorialOrgUnit())));
                builder.where(eq(property(EducationOrgUnit.formativeOrgUnit().fromAlias("eou")), value(eduOrgUnit.getFormativeOrgUnit())));
                builder.where(eq(property(EducationOrgUnit.educationLevelHighSchool().fromAlias("eou")), value(eduOrgUnit.getEducationLevelHighSchool())));
                builder.where(eq(property(EducationOrgUnit.developForm().fromAlias("eou")), value(eduOrgUnit.getDevelopForm())));
                builder.where(eq(property(EducationOrgUnit.developTech().fromAlias("eou")), value(eduOrgUnit.getDevelopTech())));
                builder.where(eq(property(EducationOrgUnit.developCondition().fromAlias("eou")), value(eduOrgUnit.getDevelopCondition())));
                builder.where(eq(property(EducationOrgUnit.developPeriod().fromAlias("eou")), value(eduOrgUnit.getDevelopPeriod())));

                UniDaoFacade.getEducationLevelDao().applyUsedFilterForEducationOrgUnit(builder, "eou");
                
                int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                if (count > 50) builder.top(50);

                return new ListResult<>(builder.createStatement(getSession()).<OrgUnit>list(), count);
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                OrgUnit ou = get((Long) primaryKey);
                if (null != ou && findValues(null).getObjects().contains(ou))
                    return ou;
                return null;
            }
        });

        model.getEduModel().setGroupModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                EducationOrgUnit eduOrgUnit = model.getExtract().getEntity().getEducationOrgUnit();
                if (model.getEduModel().getCourse() == null || eduOrgUnit.getFormativeOrgUnit() == null || model.getEduModel().getTerritorialOrgUnit() == null)
                    return ListResult.getEmpty();

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, "g")
                        .where(eq(property(Group.archival().fromAlias("g")), value(Boolean.FALSE)))
                        .where(eq(property(Group.educationOrgUnit().formativeOrgUnit().fromAlias("g")), value(eduOrgUnit.getFormativeOrgUnit())))
                        .where(eq(property(Group.educationOrgUnit().territorialOrgUnit().fromAlias("g")), value(model.getEduModel().getTerritorialOrgUnit())))
                        .where(eq(property(Group.educationOrgUnit().educationLevelHighSchool().fromAlias("g")), value(eduOrgUnit.getEducationLevelHighSchool())))
                        .where(eq(property(Group.educationOrgUnit().developForm().fromAlias("g")), value(eduOrgUnit.getDevelopForm())))
                        .where(eq(property(Group.educationOrgUnit().developCondition().fromAlias("g")), value(eduOrgUnit.getDevelopCondition())))
                        .where(eq(property(Group.educationOrgUnit().developTech().fromAlias("g")), value(eduOrgUnit.getDevelopTech())))
                        .where(eq(property(Group.educationOrgUnit().developPeriod().fromAlias("g")), value(eduOrgUnit.getDevelopPeriod())))
                        .where(eq(property(Group.course().fromAlias("g")), value(model.getEduModel().getCourse())))
                        .where(like(DQLFunctions.upper(property(Group.title().fromAlias("g"))), value(CoreStringUtils.escapeLike(filter))))
                        .order(property(Group.title().fromAlias("g")));

                int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                if (count > 50) builder.top(50);

                return new ListResult<>(builder.createStatement(getSession()).list(), count);
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                Group grp = get((Long) primaryKey);
                if (null != grp && findValues(null).getObjects().contains(grp))
                    return grp;
                return null;
            }
        });

        if (model.isAddForm())
        {
            model.getEduModel().setFormativeOrgUnit(model.getExtract().getEntity().getEducationOrgUnit().getFormativeOrgUnit());
            model.getEduModel().setTerritorialOrgUnit(null);
            model.getEduModel().setCompensationType(model.getExtract().getEntity().getCompensationType());
            model.getEduModel().setEducationLevelsHighSchool(model.getExtract().getEntity().getEducationOrgUnit().getEducationLevelHighSchool());
            model.getEduModel().setDevelopForm(model.getExtract().getEntity().getEducationOrgUnit().getDevelopForm());
            model.getEduModel().setDevelopCondition(model.getExtract().getEntity().getEducationOrgUnit().getDevelopCondition());
            model.getEduModel().setDevelopTech(model.getExtract().getEntity().getEducationOrgUnit().getDevelopTech());
            model.getEduModel().setDevelopPeriod(model.getExtract().getEntity().getEducationOrgUnit().getDevelopPeriod());
            model.getExtract().setCourseOld(model.getExtract().getEntity().getCourse());
            if (null != model.getExtract().getEntity().getGroup())
            {
                model.getExtract().setGroupOld(model.getExtract().getEntity().getGroup());
                model.getEduModel().setCourse(model.getExtract().getEntity().getGroup().getCourse());
            }
        }
    }

    @Override
    public void update(Model model)
    {
        MoveStudentDaoFacade.getCommonExtractUtil().update(model.getEduModel(), model.getExtract());
        super.update(model);
        //model.getExtract().setGroupStr(model.getExtract().getOldGroupNumber());
        //getSession().update(model.getExtract());
    }

}