package ru.tandemservice.movestudent.entity;

import com.google.common.collect.Ordering;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.gen.AbstractStudentExtractGen;
import ru.tandemservice.movestudent.utils.ICommentableExtract;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.Date;

public abstract class AbstractStudentExtract extends AbstractStudentExtractGen implements IAbstractExtract<Student, AbstractStudentParagraph>, ICommentableExtract
{
    public static final Comparator<AbstractStudentExtract> BY_EXTRACT_NUMBER_COMPARATOR = (o1, o2) -> {
        if (o1 == o2) { return 0; }
        return Ordering.natural().nullsLast().compare(o1.getNumber(), o2.getNumber());
    };

    public static final Comparator<AbstractStudentExtract> BY_PAR_AND_EXTRACT_NUMBER_COMPARATOR = (o1, o2) -> {
        if (o1 == o2) { return 0; }
        int i;
        if (0 != (i = o1.getParagraph().getNumber() - o2.getParagraph().getNumber())) { return i; }
        if (0 != (i = o1.getNumber() - o2.getNumber())) { return i; }
        return 0;
    };

    public static final Comparator<AbstractStudentExtract> FULL_FIO_AND_ID_COMPARATOR = CommonCollator.<AbstractStudentExtract>comparing(e -> e.getEntity().getPerson().getFullFio(), true);

    public static final String P_NO_EDIT = "noEdit";       // нельзя редактировать

    public static final String P_NO_DELETE = "noDelete";   // нельзя удалять

    public static final String P_NO_EXCLUDE = "noExclude"; // нельзя исплючать из приказа

    public static final String P_GROUP_TITLE_OLD_AND_ACTUAL = "groupTitleOldAndActual"; // наименование группы на момент проведения приказа и текущее

    public static final String P_PRINTING_DISABLED_VIEW_PROPERTY = "printingDisabled"; // Ключ для viewProperty, указывающей на невозможность печати выписки

    @Override
    public String getTitle()
    {
        if (getEntity() == null) {
            return this.getClass().getSimpleName();
        }

        IdentityCard identityCard = getEntity().getPerson().getIdentityCard();
        String student = " | " + (SexCodes.FEMALE.equals(identityCard.getSex().getCode()) ? "студентка " : "студент ") + identityCard.getFullFio();

        if (getParagraph() == null)
            return "Проект приказа «" + getType().getTitle() + "»" + student;
        else
        {
            String orderNumber = getParagraph().getOrder().getNumber();
            orderNumber = (orderNumber != null) ? " №" + orderNumber : "";

            if ((this instanceof ModularStudentExtract) && MoveStudentDaoFacade.getMoveStudentDao().isModularOrderIndividual(getType()))
                return "Индивидуальный приказ «" + getType().getTitle() + "»" + orderNumber + student;
            else
                return "Выписка «" + getType().getTitle() + "» №" + getParagraph().getNumber() + " из приказа" + orderNumber + student;
        }
    }

    public boolean isNoEdit()
    {
        //нельзя редактировать выписки у архивных студентов и если они не в состоянии формирования
        return getEntity().isArchival() || !UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE.equals(getState().getCode());
    }

    public boolean isNoDelete()
    {
        //нельзя удалять выписки у архивных студентов
        //нельзя удалять выписки, которые нельзя исключать из приказа
        return getEntity().isArchival() || isNoExclude();
    }

    public boolean isNoExclude()
    {
        //нельзя исключать выписки из приказа не в состоянии формирования
        //другими словами: можно исключать выписки не из приказа и можно исключать выписки из формирующегося приказа
        return !(getParagraph() == null || UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(getParagraph().getOrder().getState().getCode()));
    }

    @Override
    public void setState(ICatalogItem state)
    {
        setState((ExtractStates) state);
    }

    @Override
    public void setType(ICatalogItem type)
    {
        setType((StudentExtractType) type);
    }

    public String getGroupTitleOldAndActual()
    {
        if (null != getEntity() && null != getEntity().getGroup() && !getGroupStr().equals(getEntity().getGroup().getTitle()))
            return getGroupStr() + " (" + getEntity().getGroup().getTitle() + ")";
        return getGroupStr();
    }

    // Генерируемое примечание приказа
    public String getGeneratedComment(boolean isPrintOrderData)
    {
        final String methodName = "getComment";
        try
        {
            Method method = MoveStudentDaoFacade.getGenerateComment().getClass().getMethod(methodName, new Class[]{this.getClass(), boolean.class});
            return (String) method.invoke(MoveStudentDaoFacade.getGenerateComment(), this, isPrintOrderData);
        }
        catch (NoSuchMethodException e1)
        {
            String orderStr = getType().getTitle();
            if (isPrintOrderData && null != getParagraph() && null != getParagraph().getOrder())
            {
                AbstractStudentOrder order = getParagraph().getOrder();
                if (null != order.getNumber())
                    orderStr += ", приказ № " + order.getNumber() + (order.getCommitDate() != null ? " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()) + " года." : "");
                else if (order.getCommitDate() != null)
                    orderStr += ", приказ от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate());
            }
            return orderStr;
        }
        catch (InvocationTargetException | IllegalAccessException e2)
        {
            throw new RuntimeException(e2);
        }
    }

    // даты начала и окончания "действия" выписки

    public Date getBeginDate()
    {
        return getParagraph() != null ? getParagraph().getOrder().getCommitDate() : null;
    }

    public Date getEndDate()
    {
        return null;
    }
}