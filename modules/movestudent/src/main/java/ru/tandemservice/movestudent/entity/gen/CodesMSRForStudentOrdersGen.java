package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.CodesMSRForStudentOrders;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Соответствие приказов, используемых при движении студентов, кодам ГУП МСР
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CodesMSRForStudentOrdersGen extends EntityBase
 implements INaturalIdentifiable<CodesMSRForStudentOrdersGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.CodesMSRForStudentOrders";
    public static final String ENTITY_NAME = "codesMSRForStudentOrders";
    public static final int VERSION_HASH = 1570101085;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT_TYPE = "extractType";
    public static final String P_CODE = "code";

    private StudentExtractType _extractType;     // Тип выписки
    private int _code;     // Код МСР

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип выписки. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public StudentExtractType getExtractType()
    {
        return _extractType;
    }

    /**
     * @param extractType Тип выписки. Свойство не может быть null и должно быть уникальным.
     */
    public void setExtractType(StudentExtractType extractType)
    {
        dirty(_extractType, extractType);
        _extractType = extractType;
    }

    /**
     * @return Код МСР. Свойство не может быть null.
     */
    @NotNull
    public int getCode()
    {
        return _code;
    }

    /**
     * @param code Код МСР. Свойство не может быть null.
     */
    public void setCode(int code)
    {
        dirty(_code, code);
        _code = code;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof CodesMSRForStudentOrdersGen)
        {
            if (withNaturalIdProperties)
            {
                setExtractType(((CodesMSRForStudentOrders)another).getExtractType());
            }
            setCode(((CodesMSRForStudentOrders)another).getCode());
        }
    }

    public INaturalId<CodesMSRForStudentOrdersGen> getNaturalId()
    {
        return new NaturalId(getExtractType());
    }

    public static class NaturalId extends NaturalIdBase<CodesMSRForStudentOrdersGen>
    {
        private static final String PROXY_NAME = "CodesMSRForStudentOrdersNaturalProxy";

        private Long _extractType;

        public NaturalId()
        {}

        public NaturalId(StudentExtractType extractType)
        {
            _extractType = ((IEntity) extractType).getId();
        }

        public Long getExtractType()
        {
            return _extractType;
        }

        public void setExtractType(Long extractType)
        {
            _extractType = extractType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof CodesMSRForStudentOrdersGen.NaturalId) ) return false;

            CodesMSRForStudentOrdersGen.NaturalId that = (NaturalId) o;

            if( !equals(getExtractType(), that.getExtractType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getExtractType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getExtractType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CodesMSRForStudentOrdersGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CodesMSRForStudentOrders.class;
        }

        public T newInstance()
        {
            return (T) new CodesMSRForStudentOrders();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extractType":
                    return obj.getExtractType();
                case "code":
                    return obj.getCode();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extractType":
                    obj.setExtractType((StudentExtractType) value);
                    return;
                case "code":
                    obj.setCode((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extractType":
                        return true;
                case "code":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extractType":
                    return true;
                case "code":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extractType":
                    return StudentExtractType.class;
                case "code":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CodesMSRForStudentOrders> _dslPath = new Path<CodesMSRForStudentOrders>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CodesMSRForStudentOrders");
    }
            

    /**
     * @return Тип выписки. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.CodesMSRForStudentOrders#getExtractType()
     */
    public static StudentExtractType.Path<StudentExtractType> extractType()
    {
        return _dslPath.extractType();
    }

    /**
     * @return Код МСР. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CodesMSRForStudentOrders#getCode()
     */
    public static PropertyPath<Integer> code()
    {
        return _dslPath.code();
    }

    public static class Path<E extends CodesMSRForStudentOrders> extends EntityPath<E>
    {
        private StudentExtractType.Path<StudentExtractType> _extractType;
        private PropertyPath<Integer> _code;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип выписки. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.CodesMSRForStudentOrders#getExtractType()
     */
        public StudentExtractType.Path<StudentExtractType> extractType()
        {
            if(_extractType == null )
                _extractType = new StudentExtractType.Path<StudentExtractType>(L_EXTRACT_TYPE, this);
            return _extractType;
        }

    /**
     * @return Код МСР. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CodesMSRForStudentOrders#getCode()
     */
        public PropertyPath<Integer> code()
        {
            if(_code == null )
                _code = new PropertyPath<Integer>(CodesMSRForStudentOrdersGen.P_CODE, this);
            return _code;
        }

        public Class getEntityClass()
        {
            return CodesMSRForStudentOrders.class;
        }

        public String getEntityName()
        {
            return "codesMSRForStudentOrders";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
