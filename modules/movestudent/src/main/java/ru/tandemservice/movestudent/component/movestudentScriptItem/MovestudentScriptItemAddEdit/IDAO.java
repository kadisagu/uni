/* $Id$ */
package ru.tandemservice.movestudent.component.movestudentScriptItem.MovestudentScriptItemAddEdit;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogAddEdit.IDefaultScriptCatalogAddEditDAO;

/**
 * @author Vasily Zhukov
 * @since 22.12.2011
 */
public interface IDAO<T extends ICatalogItem & IScriptItem, Model extends ru.tandemservice.movestudent.component.movestudentScriptItem.MovestudentScriptItemAddEdit.Model<T>> extends IDefaultScriptCatalogAddEditDAO
{
}