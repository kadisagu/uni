/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e15.AddEdit;

import java.util.ArrayList;
import java.util.List;

import ru.tandemservice.movestudent.component.modularextract.CommonExtractModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 28.04.2009
 */
public class Model extends CommonModularStudentExtractAddEditModel<EduEnrAsTransferStuExtract>
{
    private CommonExtractModel _eduModel;

    private String _studentStatusNewStr;
    
    private StuExtractToDebtRelation _currentDebt; 
    
    private List<StuExtractToDebtRelation> _debtsList = new ArrayList<StuExtractToDebtRelation>();
    
    private List<StuExtractToDebtRelation> _debtsToDel = new ArrayList<StuExtractToDebtRelation>();

    public CommonExtractModel getEduModel()
    {
        return _eduModel;
    }

    public void setEduModel(CommonExtractModel eduModel)
    {
        _eduModel = eduModel;
    }

    public String getStudentStatusNewStr()
    {
        return _studentStatusNewStr;
    }

    public void setStudentStatusNewStr(String studentStatusNewStr)
    {
        _studentStatusNewStr = studentStatusNewStr;
    }

    public StuExtractToDebtRelation getCurrentDebt()
    {
        return _currentDebt;
    }

    public void setCurrentDebt(StuExtractToDebtRelation currentDebt)
    {
        this._currentDebt = currentDebt;
    }

    public List<StuExtractToDebtRelation> getDebtsList()
    {
        return _debtsList;
    }

    public void setDebtsList(List<StuExtractToDebtRelation> debtsList)
    {
        this._debtsList = debtsList;
    }

    public List<StuExtractToDebtRelation> getDebtsToDel()
    {
        return _debtsToDel;
    }

    public void setDebtsToDel(List<StuExtractToDebtRelation> debtsToDel)
    {
        this._debtsToDel = debtsToDel;
    }
    
    public Integer getCurrentDebtNumber()
    {
        return getDebtsList().indexOf(getCurrentDebt());
    }
}