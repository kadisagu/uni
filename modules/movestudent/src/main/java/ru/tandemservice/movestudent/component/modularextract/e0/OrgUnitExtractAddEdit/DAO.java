/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e0.OrgUnitExtractAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.OtherOrgUnitExtractType;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author dseleznev
 * Created on: 18.03.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        final boolean admin = UserContext.getInstance().getPrincipalContext().isAdmin();

        MQBuilder builder = new MQBuilder(OtherOrgUnitExtractType.ENTITY_CLASS, "s", new String[] {OtherOrgUnitExtractType.L_TYPE});
        builder.add(MQExpression.eq("s", OtherOrgUnitExtractType.type().active().s(), Boolean.TRUE));
        builder.add(MQExpression.eq("s", OtherOrgUnitExtractType.active().s(), Boolean.TRUE));
        model.setExtractTypeList(builder.<StudentExtractType>getResultList(getSession()));

        final int extractTypesCount = new DQLSelectBuilder()
                .fromEntity(OtherOrgUnitExtractType.class, "et").column("et")
                .where(eq(property(OtherOrgUnitExtractType.type().active().fromAlias("et")), value(Boolean.TRUE)))
                .where(eq(property(OtherOrgUnitExtractType.active().fromAlias("et")), value(Boolean.TRUE)))
                .createStatement(getSession()).list().size();

        model.setExtractTypeListModel(new FullCheckSelectModel(admin ? StudentExtractType.P_TITLE_WITH_CODE : StudentExtractType.P_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(OtherOrgUnitExtractType.class, "et")
                        .column(property(OtherOrgUnitExtractType.type().fromAlias("et")))
                        .where(eq(property(OtherOrgUnitExtractType.type().parent().code().fromAlias("et")), value(StudentExtractTypeCodes.MODULAR_ORDER)))
                        .where(eq(property(OtherOrgUnitExtractType.type().active().fromAlias("et")), value(Boolean.TRUE)))
                        .where(eq(property(OtherOrgUnitExtractType.active().fromAlias("et")), value(Boolean.TRUE)))
                        .order(property(OtherOrgUnitExtractType.type().title().fromAlias("et")));
                if(admin)
                {
                    builder.where(or(
                            like(DQLFunctions.upper(DQLExpressions.property(OtherOrgUnitExtractType.type().title().fromAlias("et"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))),
                            like(DQLFunctions.upper(DQLExpressions.property(OtherOrgUnitExtractType.type().code().fromAlias("et"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter)))
                    ));
                }
                else
                    builder.where(like(DQLFunctions.upper(DQLExpressions.property(OtherOrgUnitExtractType.type().title().fromAlias("et"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                return new ListResult<>(builder.createStatement(getSession()).<StudentExtractType>list(), extractTypesCount);
            }
        });
        
        final OrgUnit orgUnit = get(OrgUnit.class, model.getOrgUnitId());
        
        model.setStudentModel(new FullCheckSelectModel(Student.P_FULL_FIO_WITH_BIRTH_YEAR_GROUP_AND_STATUS)
        {
            @Override
            public ListResult findValues(String filter)
            {
                String[] fio = StringUtils.split(filter);
                MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "s");
                builder.add(MQExpression.eq("s", Student.P_ARCHIVAL, Boolean.FALSE));
                //builder.add(MQExpression.eq("s", Student.L_STATUS + "." + StudentStatus.P_ACTIVE, Boolean.TRUE));
                builder.add(MQExpression.notIn("s", Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, orgUnit));

                if (fio.length > 0) builder.add(MQExpression.like("s", Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME, CoreStringUtils.escapeLike("!" + fio[0])));
                if (fio.length > 1) builder.add(MQExpression.like("s", Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME, CoreStringUtils.escapeLike("!" + fio[1])));
                if (fio.length > 2) builder.add(MQExpression.like("s", Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_MIDDLE_NAME, CoreStringUtils.escapeLike("!" + fio[2])));
                builder.addOrder("s", Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME);
                builder.addOrder("s", Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME);
                
                return new ListResult<>(builder.getResultList(getSession(), 0, 50), builder.getResultCount(getSession()));
            }
            
            @Override
            public Object getValue(Object primaryKey)
            {
                return UniDaoFacade.getCoreDao().get(Student.class, (Long)primaryKey);
            }
        });
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if(!MoveStudentDaoFacade.getMoveStudentDao().isMoveAccessible(model.getStudent()))
            errors.add("Нельзя добавить выписку, так как у студента уже есть непроведенные выписки.", "student");
    }
}