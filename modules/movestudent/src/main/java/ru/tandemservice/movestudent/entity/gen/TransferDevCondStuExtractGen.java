package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.TransferDevCondStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О переводе на другие условия освоения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TransferDevCondStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.TransferDevCondStuExtract";
    public static final String ENTITY_NAME = "transferDevCondStuExtract";
    public static final int VERSION_HASH = 1678671580;
    private static IEntityMeta ENTITY_META;

    public static final String P_PLANNED_GRADUATE_YEAR = "plannedGraduateYear";

    private int _plannedGraduateYear;     // Планируемый год окончания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Планируемый год окончания. Свойство не может быть null.
     */
    @NotNull
    public int getPlannedGraduateYear()
    {
        return _plannedGraduateYear;
    }

    /**
     * @param plannedGraduateYear Планируемый год окончания. Свойство не может быть null.
     */
    public void setPlannedGraduateYear(int plannedGraduateYear)
    {
        dirty(_plannedGraduateYear, plannedGraduateYear);
        _plannedGraduateYear = plannedGraduateYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TransferDevCondStuExtractGen)
        {
            setPlannedGraduateYear(((TransferDevCondStuExtract)another).getPlannedGraduateYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TransferDevCondStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TransferDevCondStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new TransferDevCondStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "plannedGraduateYear":
                    return obj.getPlannedGraduateYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "plannedGraduateYear":
                    obj.setPlannedGraduateYear((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "plannedGraduateYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "plannedGraduateYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "plannedGraduateYear":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TransferDevCondStuExtract> _dslPath = new Path<TransferDevCondStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TransferDevCondStuExtract");
    }
            

    /**
     * @return Планируемый год окончания. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferDevCondStuExtract#getPlannedGraduateYear()
     */
    public static PropertyPath<Integer> plannedGraduateYear()
    {
        return _dslPath.plannedGraduateYear();
    }

    public static class Path<E extends TransferDevCondStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<Integer> _plannedGraduateYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Планируемый год окончания. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.TransferDevCondStuExtract#getPlannedGraduateYear()
     */
        public PropertyPath<Integer> plannedGraduateYear()
        {
            if(_plannedGraduateYear == null )
                _plannedGraduateYear = new PropertyPath<Integer>(TransferDevCondStuExtractGen.P_PLANNED_GRADUATE_YEAR, this);
            return _plannedGraduateYear;
        }

        public Class getEntityClass()
        {
            return TransferDevCondStuExtract.class;
        }

        public String getEntityName()
        {
            return "transferDevCondStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
