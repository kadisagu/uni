/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e101.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AcadGrantBonusPaymentStopStuExtract;

import java.util.Date;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 22.02.2013
 */
public class Model extends CommonModularStudentExtractAddEditModel<AcadGrantBonusPaymentStopStuExtract>
{
    private ISelectModel _grantBonusOrderModel;
    private AbstractStudentExtract _acadGrantBonusExtract;
    private Date _acadGrantBonusOrderDate;
    private String _acadGrantBonusOrderNumber;
    private String _acadGrantBonusReason;

    public ISelectModel getGrantBonusOrderModel()
    {
        return _grantBonusOrderModel;
    }

    public void setGrantBonusOrderModel(ISelectModel grantBonusOrderModel)
    {
        _grantBonusOrderModel = grantBonusOrderModel;
    }

    public AbstractStudentExtract getAcadGrantBonusExtract()
    {
        return _acadGrantBonusExtract;
    }

    public void setAcadGrantBonusExtract(AbstractStudentExtract acadGrantBonusExtract)
    {
        _acadGrantBonusExtract = acadGrantBonusExtract;
    }

    public Date getAcadGrantBonusOrderDate()
    {
        return _acadGrantBonusOrderDate;
    }

    public void setAcadGrantBonusOrderDate(Date acadGrantBonusOrderDate)
    {
        _acadGrantBonusOrderDate = acadGrantBonusOrderDate;
    }

    public String getAcadGrantBonusOrderNumber()
    {
        return _acadGrantBonusOrderNumber;
    }

    public void setAcadGrantBonusOrderNumber(String acadGrantBonusOrderNumber)
    {
        _acadGrantBonusOrderNumber = acadGrantBonusOrderNumber;
    }

    public String getAcadGrantBonusReason()
    {
        return _acadGrantBonusReason;
    }

    public void setAcadGrantBonusReason(String acadGrantBonusReason)
    {
        _acadGrantBonusReason = acadGrantBonusReason;
    }
}
