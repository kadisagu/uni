package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ChangePassStateExamsStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О переносе итоговой государственной аттестации
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChangePassStateExamsStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ChangePassStateExamsStuExtract";
    public static final String ENTITY_NAME = "changePassStateExamsStuExtract";
    public static final int VERSION_HASH = 1758506597;
    private static IEntityMeta ENTITY_META;

    public static final String P_NEW_DATE = "newDate";
    public static final String P_YEAR = "year";
    public static final String P_SEASON = "season";

    private Date _newDate;     // Новая дата
    private int _year;     // Год
    private String _season;     // Время года

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Новая дата. Свойство не может быть null.
     */
    @NotNull
    public Date getNewDate()
    {
        return _newDate;
    }

    /**
     * @param newDate Новая дата. Свойство не может быть null.
     */
    public void setNewDate(Date newDate)
    {
        dirty(_newDate, newDate);
        _newDate = newDate;
    }

    /**
     * @return Год. Свойство не может быть null.
     */
    @NotNull
    public int getYear()
    {
        return _year;
    }

    /**
     * @param year Год. Свойство не может быть null.
     */
    public void setYear(int year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Время года.
     */
    @Length(max=255)
    public String getSeason()
    {
        return _season;
    }

    /**
     * @param season Время года.
     */
    public void setSeason(String season)
    {
        dirty(_season, season);
        _season = season;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ChangePassStateExamsStuExtractGen)
        {
            setNewDate(((ChangePassStateExamsStuExtract)another).getNewDate());
            setYear(((ChangePassStateExamsStuExtract)another).getYear());
            setSeason(((ChangePassStateExamsStuExtract)another).getSeason());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChangePassStateExamsStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChangePassStateExamsStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new ChangePassStateExamsStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "newDate":
                    return obj.getNewDate();
                case "year":
                    return obj.getYear();
                case "season":
                    return obj.getSeason();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "newDate":
                    obj.setNewDate((Date) value);
                    return;
                case "year":
                    obj.setYear((Integer) value);
                    return;
                case "season":
                    obj.setSeason((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "newDate":
                        return true;
                case "year":
                        return true;
                case "season":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "newDate":
                    return true;
                case "year":
                    return true;
                case "season":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "newDate":
                    return Date.class;
                case "year":
                    return Integer.class;
                case "season":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChangePassStateExamsStuExtract> _dslPath = new Path<ChangePassStateExamsStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChangePassStateExamsStuExtract");
    }
            

    /**
     * @return Новая дата. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangePassStateExamsStuExtract#getNewDate()
     */
    public static PropertyPath<Date> newDate()
    {
        return _dslPath.newDate();
    }

    /**
     * @return Год. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangePassStateExamsStuExtract#getYear()
     */
    public static PropertyPath<Integer> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Время года.
     * @see ru.tandemservice.movestudent.entity.ChangePassStateExamsStuExtract#getSeason()
     */
    public static PropertyPath<String> season()
    {
        return _dslPath.season();
    }

    public static class Path<E extends ChangePassStateExamsStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _newDate;
        private PropertyPath<Integer> _year;
        private PropertyPath<String> _season;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Новая дата. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangePassStateExamsStuExtract#getNewDate()
     */
        public PropertyPath<Date> newDate()
        {
            if(_newDate == null )
                _newDate = new PropertyPath<Date>(ChangePassStateExamsStuExtractGen.P_NEW_DATE, this);
            return _newDate;
        }

    /**
     * @return Год. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangePassStateExamsStuExtract#getYear()
     */
        public PropertyPath<Integer> year()
        {
            if(_year == null )
                _year = new PropertyPath<Integer>(ChangePassStateExamsStuExtractGen.P_YEAR, this);
            return _year;
        }

    /**
     * @return Время года.
     * @see ru.tandemservice.movestudent.entity.ChangePassStateExamsStuExtract#getSeason()
     */
        public PropertyPath<String> season()
        {
            if(_season == null )
                _season = new PropertyPath<String>(ChangePassStateExamsStuExtractGen.P_SEASON, this);
            return _season;
        }

        public Class getEntityClass()
        {
            return ChangePassStateExamsStuExtract.class;
        }

        public String getEntityName()
        {
            return "changePassStateExamsStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
