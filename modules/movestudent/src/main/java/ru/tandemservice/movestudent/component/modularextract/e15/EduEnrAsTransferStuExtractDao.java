/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.movestudent.component.modularextract.e15;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 28.04.2009
 */
public class EduEnrAsTransferStuExtractDao extends UniBaseDao implements IExtractComponentDao<EduEnrAsTransferStuExtract>
{
    @Override
    public void doCommit(EduEnrAsTransferStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);
        MoveStudentDaoFacade.getCommonExtractUtil().doCommit(extract, this);

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        else
        {
            extract.setPrevOrderDate(orderData.getEduEnrollmentOrderDate());
            extract.setPrevOrderNumber(orderData.getEduEnrollmentOrderNumber());
            extract.setPrevOrderEntryDate(orderData.getEduEnrollmentOrderEnrDate());
        }
        orderData.setEduEnrollmentOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setEduEnrollmentOrderNumber(extract.getParagraph().getOrder().getNumber());
        orderData.setEduEnrollmentOrderEnrDate(extract.getEntryDate());
        getSession().saveOrUpdate(orderData);
    }

    @Override
    public void doRollback(EduEnrAsTransferStuExtract extract, Map parameters)
    {
        MoveStudentDaoFacade.getCommonExtractUtil().doRollback(extract);

        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setEduEnrollmentOrderDate(extract.getPrevOrderDate());
        orderData.setEduEnrollmentOrderNumber(extract.getPrevOrderNumber());
        orderData.setEduEnrollmentOrderEnrDate(extract.getPrevOrderEntryDate());
        getSession().saveOrUpdate(orderData);
    }
}