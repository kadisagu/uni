package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.SocGrantPaymentStopStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О прекращении выплаты социальной стипендии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SocGrantPaymentStopStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.SocGrantPaymentStopStuExtract";
    public static final String ENTITY_NAME = "socGrantPaymentStopStuExtract";
    public static final int VERSION_HASH = -1123227255;
    private static IEntityMeta ENTITY_META;

    public static final String L_SOC_GRANT_EXTRACT = "socGrantExtract";
    public static final String P_SOC_GRANT_ORDER_NUMBER = "socGrantOrderNumber";
    public static final String P_SOC_GRANT_ORDER_DATE = "socGrantOrderDate";
    public static final String P_SOC_GRANT_PAYMENT_STOP_DATE = "socGrantPaymentStopDate";

    private AbstractStudentExtract _socGrantExtract;     // Выписка о назначении социальной стипендии
    private String _socGrantOrderNumber;     // Номер приказа о назначении социальной стипендии
    private Date _socGrantOrderDate;     // Дата приказа о назначении социальной стипендии
    private Date _socGrantPaymentStopDate;     // Дата прекращения выплаты стипендии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка о назначении социальной стипендии.
     */
    public AbstractStudentExtract getSocGrantExtract()
    {
        return _socGrantExtract;
    }

    /**
     * @param socGrantExtract Выписка о назначении социальной стипендии.
     */
    public void setSocGrantExtract(AbstractStudentExtract socGrantExtract)
    {
        dirty(_socGrantExtract, socGrantExtract);
        _socGrantExtract = socGrantExtract;
    }

    /**
     * @return Номер приказа о назначении социальной стипендии. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSocGrantOrderNumber()
    {
        return _socGrantOrderNumber;
    }

    /**
     * @param socGrantOrderNumber Номер приказа о назначении социальной стипендии. Свойство не может быть null.
     */
    public void setSocGrantOrderNumber(String socGrantOrderNumber)
    {
        dirty(_socGrantOrderNumber, socGrantOrderNumber);
        _socGrantOrderNumber = socGrantOrderNumber;
    }

    /**
     * @return Дата приказа о назначении социальной стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getSocGrantOrderDate()
    {
        return _socGrantOrderDate;
    }

    /**
     * @param socGrantOrderDate Дата приказа о назначении социальной стипендии. Свойство не может быть null.
     */
    public void setSocGrantOrderDate(Date socGrantOrderDate)
    {
        dirty(_socGrantOrderDate, socGrantOrderDate);
        _socGrantOrderDate = socGrantOrderDate;
    }

    /**
     * @return Дата прекращения выплаты стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getSocGrantPaymentStopDate()
    {
        return _socGrantPaymentStopDate;
    }

    /**
     * @param socGrantPaymentStopDate Дата прекращения выплаты стипендии. Свойство не может быть null.
     */
    public void setSocGrantPaymentStopDate(Date socGrantPaymentStopDate)
    {
        dirty(_socGrantPaymentStopDate, socGrantPaymentStopDate);
        _socGrantPaymentStopDate = socGrantPaymentStopDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SocGrantPaymentStopStuExtractGen)
        {
            setSocGrantExtract(((SocGrantPaymentStopStuExtract)another).getSocGrantExtract());
            setSocGrantOrderNumber(((SocGrantPaymentStopStuExtract)another).getSocGrantOrderNumber());
            setSocGrantOrderDate(((SocGrantPaymentStopStuExtract)another).getSocGrantOrderDate());
            setSocGrantPaymentStopDate(((SocGrantPaymentStopStuExtract)another).getSocGrantPaymentStopDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SocGrantPaymentStopStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SocGrantPaymentStopStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new SocGrantPaymentStopStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "socGrantExtract":
                    return obj.getSocGrantExtract();
                case "socGrantOrderNumber":
                    return obj.getSocGrantOrderNumber();
                case "socGrantOrderDate":
                    return obj.getSocGrantOrderDate();
                case "socGrantPaymentStopDate":
                    return obj.getSocGrantPaymentStopDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "socGrantExtract":
                    obj.setSocGrantExtract((AbstractStudentExtract) value);
                    return;
                case "socGrantOrderNumber":
                    obj.setSocGrantOrderNumber((String) value);
                    return;
                case "socGrantOrderDate":
                    obj.setSocGrantOrderDate((Date) value);
                    return;
                case "socGrantPaymentStopDate":
                    obj.setSocGrantPaymentStopDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "socGrantExtract":
                        return true;
                case "socGrantOrderNumber":
                        return true;
                case "socGrantOrderDate":
                        return true;
                case "socGrantPaymentStopDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "socGrantExtract":
                    return true;
                case "socGrantOrderNumber":
                    return true;
                case "socGrantOrderDate":
                    return true;
                case "socGrantPaymentStopDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "socGrantExtract":
                    return AbstractStudentExtract.class;
                case "socGrantOrderNumber":
                    return String.class;
                case "socGrantOrderDate":
                    return Date.class;
                case "socGrantPaymentStopDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SocGrantPaymentStopStuExtract> _dslPath = new Path<SocGrantPaymentStopStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SocGrantPaymentStopStuExtract");
    }
            

    /**
     * @return Выписка о назначении социальной стипендии.
     * @see ru.tandemservice.movestudent.entity.SocGrantPaymentStopStuExtract#getSocGrantExtract()
     */
    public static AbstractStudentExtract.Path<AbstractStudentExtract> socGrantExtract()
    {
        return _dslPath.socGrantExtract();
    }

    /**
     * @return Номер приказа о назначении социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantPaymentStopStuExtract#getSocGrantOrderNumber()
     */
    public static PropertyPath<String> socGrantOrderNumber()
    {
        return _dslPath.socGrantOrderNumber();
    }

    /**
     * @return Дата приказа о назначении социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantPaymentStopStuExtract#getSocGrantOrderDate()
     */
    public static PropertyPath<Date> socGrantOrderDate()
    {
        return _dslPath.socGrantOrderDate();
    }

    /**
     * @return Дата прекращения выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantPaymentStopStuExtract#getSocGrantPaymentStopDate()
     */
    public static PropertyPath<Date> socGrantPaymentStopDate()
    {
        return _dslPath.socGrantPaymentStopDate();
    }

    public static class Path<E extends SocGrantPaymentStopStuExtract> extends ModularStudentExtract.Path<E>
    {
        private AbstractStudentExtract.Path<AbstractStudentExtract> _socGrantExtract;
        private PropertyPath<String> _socGrantOrderNumber;
        private PropertyPath<Date> _socGrantOrderDate;
        private PropertyPath<Date> _socGrantPaymentStopDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка о назначении социальной стипендии.
     * @see ru.tandemservice.movestudent.entity.SocGrantPaymentStopStuExtract#getSocGrantExtract()
     */
        public AbstractStudentExtract.Path<AbstractStudentExtract> socGrantExtract()
        {
            if(_socGrantExtract == null )
                _socGrantExtract = new AbstractStudentExtract.Path<AbstractStudentExtract>(L_SOC_GRANT_EXTRACT, this);
            return _socGrantExtract;
        }

    /**
     * @return Номер приказа о назначении социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantPaymentStopStuExtract#getSocGrantOrderNumber()
     */
        public PropertyPath<String> socGrantOrderNumber()
        {
            if(_socGrantOrderNumber == null )
                _socGrantOrderNumber = new PropertyPath<String>(SocGrantPaymentStopStuExtractGen.P_SOC_GRANT_ORDER_NUMBER, this);
            return _socGrantOrderNumber;
        }

    /**
     * @return Дата приказа о назначении социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantPaymentStopStuExtract#getSocGrantOrderDate()
     */
        public PropertyPath<Date> socGrantOrderDate()
        {
            if(_socGrantOrderDate == null )
                _socGrantOrderDate = new PropertyPath<Date>(SocGrantPaymentStopStuExtractGen.P_SOC_GRANT_ORDER_DATE, this);
            return _socGrantOrderDate;
        }

    /**
     * @return Дата прекращения выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.SocGrantPaymentStopStuExtract#getSocGrantPaymentStopDate()
     */
        public PropertyPath<Date> socGrantPaymentStopDate()
        {
            if(_socGrantPaymentStopDate == null )
                _socGrantPaymentStopDate = new PropertyPath<Date>(SocGrantPaymentStopStuExtractGen.P_SOC_GRANT_PAYMENT_STOP_DATE, this);
            return _socGrantPaymentStopDate;
        }

        public Class getEntityClass()
        {
            return SocGrantPaymentStopStuExtract.class;
        }

        public String getEntityName()
        {
            return "socGrantPaymentStopStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
