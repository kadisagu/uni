/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e48.ParagraphAddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.CourseTransferCustomStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Date;
import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 03.02.2017
 */
public class Model extends AbstractListParagraphAddEditModel<CourseTransferCustomStuListExtract> implements IGroupModel, IEducationLevelModel
{

    private DevelopForm _developForm;
    private Course _course;
    private Course _courseNew;
    private List<Group> _groupList;
    private CompensationType _compensationType;
    private Date _deadlineDate;

    private ISelectModel _developFormModel;
    private ISelectModel _groupListModel;
    private List<CompensationType> _compensationTypeList;
    private ISelectModel _courseNewListModel;

    public String getCurrentStudId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "currentStudId" + dataSource.getCurrentEntity().getId();
    }

    public boolean isStudentDisabled()
    {
        Boolean disabled = (Boolean) getDataSource().getCurrentValueEntity().getProperty(AbstractListParagraphAddEditModel.P_STUDENT_DISABLED);
        return null != disabled ? disabled : false;
    }


    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return getParagraph().getOrder().getOrgUnit();
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return null;
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return null;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return null;
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return null;
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return null;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public List<Group> getGroupList()
    {
        return _groupList;
    }

    public void setGroupList(List<Group> groupList)
    {
        _groupList = groupList;
    }

    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public Date getDeadlineDate()
    {
        return _deadlineDate;
    }

    public void setDeadlineDate(Date deadlineDate)
    {
        _deadlineDate = deadlineDate;
    }

    public Course getCourseNew()
    {
        return _courseNew;
    }

    public void setCourseNew(Course courseNew)
    {
        _courseNew = courseNew;
    }

    public ISelectModel getCourseNewListModel()
    {
        return _courseNewListModel;
    }

    public void setCourseNewListModel(ISelectModel courseNewListModel)
    {
        _courseNewListModel = courseNewListModel;
    }
}
