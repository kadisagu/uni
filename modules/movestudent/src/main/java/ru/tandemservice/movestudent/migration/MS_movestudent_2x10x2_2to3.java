package ru.tandemservice.movestudent.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_movestudent_2x10x2_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность admitToDiplomaWithoutExamStuExtract

		// у сущности появилась таблица
		// ранее сущность не имела своей таблицы, теперь ее нужно создать и заполнить данными
		{
			// создать таблицу (все колонки как nullable)
			DBTable dbt = new DBTable("dmttdplmwthtexmstextrct_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_d542a8a2"), 
				new DBColumn("notneedadmissiontogia_p", DBType.BOOLEAN)
			);
			tool.createTable(dbt);

			// заполнить данными новую таблицу
			short discriminator0 = tool.entityCodes().get("admitToDiplomaWithoutExamStuExtract");
			java.lang.Boolean defaultNotNeedAdmissionToGIA = false;
			tool.executeUpdate("insert into dmttdplmwthtexmstextrct_t(id, notneedadmissiontogia_p) select parent.id, ? from modularstudentextract_t parent inner join abstractstudentextract_t root on root.id=parent.id where root.discriminator in(" + discriminator0 + ")", defaultNotNeedAdmissionToGIA);

			// установить NOT NULL обязательным колонкам
			tool.setColumnNullable("dmttdplmwthtexmstextrct_t", "notneedadmissiontogia_p", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность admitToStateExamsStuExtract

		// у сущности появилась таблица
		// ранее сущность не имела своей таблицы, теперь ее нужно создать и заполнить данными
		{
			// создать таблицу (все колонки как nullable)
			DBTable dbt = new DBTable("admittostateexamsstuextract_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_admittostateexamsstuextract"), 
				new DBColumn("notneedadmissiontogia_p", DBType.BOOLEAN)
			);
			tool.createTable(dbt);

			// заполнить данными новую таблицу
			short discriminator0 = tool.entityCodes().get("admitToStateExamsStuExtract");
			java.lang.Boolean defaultNotNeedAdmissionToGIA = false;
			tool.executeUpdate("insert into admittostateexamsstuextract_t(id, notneedadmissiontogia_p) select parent.id, ? from modularstudentextract_t parent inner join abstractstudentextract_t root on root.id=parent.id where root.discriminator in(" + discriminator0 + ")", defaultNotNeedAdmissionToGIA);

			// установить NOT NULL обязательным колонкам
			tool.setColumnNullable("admittostateexamsstuextract_t", "notneedadmissiontogia_p", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность admitToDiplomaStuExtract

		// создано обязательное свойство notNeedAdmissionToGIA
		{
			// создать колонку
			tool.createColumn("admittodiplomastuextract_t", new DBColumn("notneedadmissiontogia_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultNotNeedAdmissionToGIA = false;
			tool.executeUpdate("update admittodiplomastuextract_t set notneedadmissiontogia_p=? where notneedadmissiontogia_p is null", defaultNotNeedAdmissionToGIA);

			// сделать колонку NOT NULL
			tool.setColumnNullable("admittodiplomastuextract_t", "notneedadmissiontogia_p", false);

		}


    }
}