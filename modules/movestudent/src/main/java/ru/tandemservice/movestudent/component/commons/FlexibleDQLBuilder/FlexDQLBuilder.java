/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

import org.hibernate.Session;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe;
import org.tandemframework.shared.commonbase.base.util.Zlo;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 27.11.2013
 */
@Zlo(value = "Эксперимент с FlexDQLBuilder признан несостоявшимся. Нужно всё переосмыслить.")
@DoNotUseMe
public class FlexDQLBuilder implements IJoinCreator
{
    private DQLSelectBuilder _builder;
    private IFlexConfig _config;
    private Set<String> _availableAliases = new HashSet<>();
    private boolean _loadEntity;

    public static final String MAIN_ENTITY_COLUMN_NAME = "superMario";

    public FlexDQLBuilder(IFlexConfig config, boolean loadEntity)
    {
        _config = config;
        _loadEntity = loadEntity;

        // Инициализируем
        _builder = new DQLSelectBuilder().fromEntity(_config.getClazz(), _config.getAlias());
        _availableAliases.add(config.getAlias());

        // Сразу добавляем все обязательные джойны
        for (IFlexJoin join : _config.getJoins())
        {
            if (join.isRequired())
                checkAlias(join.getAlias());
        }
    }

    public IFlexConfig getConfig()
    {
        return _config;
    }

    public DQLSelectBuilder getBuilder()
    {
        return _builder;
    }

    public void checkAlias(String alias)
    {
        if (null != alias && !_availableAliases.contains(alias))
        {
            IFlexJoin join = _config.getJoin(alias);
            if (null == join)
                throw new RuntimeException("Join for alias \"" + alias + "\" not found in builder configuration.");

            _availableAliases.add(alias);
            checkAlias(join.getLeftAlias());
            join.addToBuilder(_builder);
        }
    }

    public int applyFilters(IDataSettings settings)
    {
        int appliedFiltersCount = 0;
        for (IFlexFilter filter : _config.getFilters())
        {
            if (filter.addToBuilder(settings, _builder, this))
                appliedFiltersCount++;
        }
        return appliedFiltersCount;
    }

    public void addOrdersToBuilder(Map<IFlexOrder, OrderDirection> ordersMap)
    {
        for (Map.Entry<IFlexOrder, OrderDirection> entry : ordersMap.entrySet())
        {
            IFlexOrder order = entry.getKey();
            _builder.order(property(order.getAlias(), order.getName()), entry.getValue());
        }
    }

    public List<IFlexColumn> getColumns(Boolean root)
    {
        List<IFlexColumn> result = new ArrayList<>();
        for (IFlexColumn column : _config.getColumns())
        {
            if (null == root || root == column.isRoot())
            {
                if (column.isRoot() && column.isEntityColumn())
                {
                    // Рутовыми имеет смысл делать только колонки с подзапросами. Сущности там быть не надо.
                    throw new RuntimeException("Entity column can't be root.");
                }

                result.add(column);
            }
        }
        return result;
    }

    public void addColumnsToBuilder(Collection<IFlexColumn> columns)
    {
        for (IFlexColumn column : columns)
        {
            checkAlias(column.getAlias());
            column.addToBuilder(_builder, column.getAlias());
        }
    }

    public void addColumns(DynamicListDataSource ds, Collection<IFlexColumn> srcList, Collection<IFlexColumn> destList)
    {
        for (IFlexColumn column : srcList)
        {
            boolean needColumn = !column.canBeHidden();
            if (!needColumn)
            {
                for (String viewColumnName : column.getViewNames())
                {
                    if (isColumnVisible(ds, viewColumnName))
                    {
                        needColumn = true;
                        break;
                    }
                }
            }

            if (needColumn)
                destList.add(column);
        }
    }

    public static boolean isColumnVisible(DynamicListDataSource ds, String columnViewName)
    {
        AbstractColumn column = ds.getColumn(columnViewName);
        if (null == column)
        {
            column = ds.getColumn(columnViewName + "." + columnViewName);
            if (null == column)
                column = ds.getColumn("null." + columnViewName); // см. TF-454
        }
        return null == column || column.isVisible(ds.getColumnSettings());
    }

    private IDQLExpression buildRowNumberFunction(Map<IFlexOrder, OrderDirection> orderMap)
    {
        IDQLExpression[] expressions = new IDQLExpression[orderMap.size()];
        OrderDirection[] orderDirections = new OrderDirection[orderMap.size()];
        int idx = 0;
        for (Map.Entry<IFlexOrder, OrderDirection> entry : orderMap.entrySet())
        {
            String alias = entry.getKey().getAlias();
            expressions[idx] = property(alias, entry.getKey().getName());
            orderDirections[idx] = entry.getValue();
            idx++;
        }
        return DQLFunctions.rowNumber(expressions, orderDirections);
    }

    protected Map<IFlexOrder, OrderDirection> prepareOrderMap(EntityOrder entityOrder)
    {
        IFlexOrder flexOrder = _config.getOrder(entityOrder.getColumnName());
        if (null == flexOrder)
            throw new IllegalStateException("Sort ordering for column \"" + entityOrder.getColumnName() + "\" is missing in builder.");
        Map<IFlexOrder, OrderDirection> orderMap = new LinkedHashMap<>(flexOrder.getSubOrders().size() + 1);

        orderMap.put(flexOrder, entityOrder.getDirection());
        for (IFlexOrder order : flexOrder.getSubOrders())
        {
            orderMap.put(order, order.getDefaultDirection());
        }
        for (IFlexOrder order : orderMap.keySet())
        {
            if (order.getAlias() == null)
            {
                // Очевидно, что алиас может быть не задан только у вычисляемых колонок. В остальных случая алиас задать можно.
                // Если потребуется, необходимо реализовать сортировку по вычисляемым колонкам.
                // Для этого необходимо врапать билдер, при этом учитывая рутовые колонки - получается двойное оборачивание.
                throw new RuntimeException("Order alias must be set. Ordering calculating columns is not supported for the time being.");
            }

            checkAlias(order.getAlias());
        }
        return orderMap;
    }

    public void fillDataSource(Session session, IDataSettings settings, DynamicListDataSource<IEntity> ds, IPatchResolver patchResolver)
    {
        // TODO Важные доработки:
        // 1. Попробовать одним запросом получать и кол-во элементов и сами элемениы - через CTE.
        // 2. Перейти на DataMatrix - для быстрого доступа к свойствам через FastBean..
        // Какждый dataWrapper знает свой id, свой номер в списке и ссылку на dataMatrix.
        // 3. Добавить в IFlexOrder атрибут invert - как в классе OrderDescription
        // 4. Обеспечить совместимость с DQLOrderDescriptionRegistry, возможно, какую-то трансляцию из DQLOrderDescriptionRegistry в IFLexOrder


        // Применяем фильтры
        applyFilters(settings);

        DQLExecutionContext dqlContext = new DQLExecutionContext(session);

        long maxResults = !ds.isPrintEnabled() ? ds.getCountRow() : ds.getPrintCountRow();
        long startRow = ds.getActualStartRow();
        long allCount = -1;
        if (startRow != 0)
        {
            // Получаем полный размер списка, если это не первая страница (на первой странице снала надо сделать запрос с top X,
            // а потом получать размер, если пришло ровно X строк)
            Number count = _builder.createCountStatement(dqlContext).uniqueResult();
            allCount = count != null ? count.longValue() : 0L;
            //System.out.println(statement.getActualSQL());
            ds.setTotalSize(allCount);
            if (allCount == 0L)
            {
                ds.createPage(Collections.<IEntity>emptyList());
                return;
            }

            startRow = allCount < maxResults ? 0 : ((int) (!ds.isPrintEnabled() ? ds.getStartRow() : ds.getPrintStartRow()));
        }

        // Сортировки
        final Map<IFlexOrder, OrderDirection> orderMap = prepareOrderMap(ds.getEntityOrder());

        // колонки
        final List<IFlexColumn> columns = new ArrayList<>();
        // Нулевой колонкой всегда будет либо идентификатор, либо основная сущность, если она грузится
        if (_loadEntity)
            columns.add(new SimpleFlexColumn(_config.getAlias(), null, MAIN_ENTITY_COLUMN_NAME, false));
        else
            columns.add(new SimpleFlexColumn(_config.getAlias(), "id", "id", false));


        // Далее определяем, надо ли оборачивать запрос для постраничного вывода.
        // Дело в том, что MS SQL до v.2011 не умеет делать OFFSET, поэтому приходится оборачивать запрос в запрос
        // с ипользованием ROW_NUMBER()
        // А, поскольку у нас могут быть вычислимые колонки с подрапросами (к примеру, получить первую выписку из приказа),
        // простым заданием setFirstResult и setMaxResults для statement не обойтись, иначе подзапрос будет выполняться
        // для всего найденного множества, а не только для одной страницы, на которой может быть от 5 до 500 элементов.
        // Такие особые колонки методом IFlexColumn.inRootSelect() возвращают true.
        // Если оборачивать запро не надо, то либо это первая страница и можно обойтись обычным TOP (LIMIT), либо вообще все записи влезают на страницу.
        final List<IFlexColumn> rootColumns = new ArrayList<>();
        if (startRow != 0)
        {
            List<IFlexColumn> allRootColumns = getColumns(true);

            // Сортировать по рутовым колонкам нельзя!!!
            for (IFlexColumn rootColumn : allRootColumns)
            {
                for (IFlexOrder order : orderMap.keySet())
                {
                    if (order.getName().equals(rootColumn.getName()))
                        throw new RuntimeException("Root column can't be sortable!");
                }
            }

            // Добавлем нерутовые колонки из конфига
            addColumns(ds, getColumns(false), columns);
            // Добавляем рутовые колонки в отдельный список
            addColumns(ds, allRootColumns, rootColumns);
        }
        else
        {
            // Добавлем все колонки сразу. Враппер не нужен - можно обойтись одним TOP (или LIMIT)
            addColumns(ds, getColumns(null), columns);
        }

        // Добавляем колонки в билдер
        addColumnsToBuilder(columns);

        // враппер нужен, если рутовые колонки есть
        if (!rootColumns.isEmpty())
        {
            // TODO проверить, как оно вообще работае. А пока вроде бы не используется.
            final String rnAlias = "row_number_col";
            // Добавляем колонку с ROW_NUMBER() применяя все нужные сортировки
            if (startRow > 0)
                _builder.column(buildRowNumberFunction(orderMap), rnAlias);
            else if (allCount < 0 || allCount >= maxResults)
                _builder.top(maxResults);
            // Добавляем колонку с идентификатором, если грузится сущность - обучно именно id нужен для рутовых колонок в виде подзапросов
            if (_loadEntity)
                _builder.column(property(_config.getAlias(), "id"), "id");
            // Создаем враппер
            _builder = new DQLSelectBuilder().fromDataSource(_builder.buildQuery(), _config.getAlias());
            // Основные колонки повторяем во враппере поименно
            for (IFlexColumn column : columns)
            {
                _builder.column(_config.getAlias() + "." + column.getName());
            }
            // Применяем рутовые колонки
            addColumnsToBuilder(rootColumns);
            // Добавляем рутовые колонки в список всех колонок
            columns.addAll(rootColumns);
            // Обрезание по конкретной странице
            if (startRow > 0)
                _builder.where(between(_config.getAlias() + "." + rnAlias, value(startRow + 1), value(startRow + maxResults)));
            // Сортировать можно только по row_number, т.к. там всё уже упорядочено как надо
            // TODO а надо ли? нужно исследование
            //_builder.order(_config.getAlias() + "." + rnAlias);
        }
        else
        {
            // Применяем сортировки
            addOrdersToBuilder(orderMap);
        }

        if (columns.size() == 1)
        {
            // Чтобы нам всегда возвращался List<Object[]> добавляем фэйковую колонку в конец
            // Реально такая ситуация вряд ли будет происходить - не для того билдер написан.
            _builder.column(value(Boolean.FALSE));
        }

        IDQLStatement statement = _builder.createStatement(dqlContext);

        if (rootColumns.isEmpty() && !(allCount > 0 && allCount < maxResults))
        {
            // В случае, если мы не использовали местный враппер,
            // используем стандартный платформенный механизм постраничного вывода, который
            // умеет использовать offset для постгреса и оракла, что конечно же более выгодно, чем костыль в виде row_number
            statement.setFirstResult((int) startRow);
            statement.setMaxResults((int) maxResults);
        }

        final List<Object[]> resultList = statement.list();
        //System.out.println(statement.getActualSQL());

        if (resultList.size() >= maxResults && allCount < 0)
        {
            Number count = _builder.createCountStatement(dqlContext).uniqueResult();
            allCount = count != null ? count.longValue() : 0L;
            //System.out.println(statement.getActualSQL());
            ds.setTotalSize(allCount);
        }
        else
        {
            ds.setTotalSize(allCount < 0 ? resultList.size() : allCount);
        }

        // Это ахтунг, но getStartRow обновляет значение startRow в датасорсе на основе totalSize
//        if (ds.isPrintEnabled())
//            ds.getPrintStartRow();
//        else
//            ds.getStartRow();

        final List<IEntity> wrappedList = new ArrayList<>(resultList.size());
        for (Object[] item : resultList)
        {
            final IEntity entity = _loadEntity ? (IEntity) item[0] : null;
            final DataWrapper wrapper = new DataWrapper(null != entity ? entity.getId() : (Long) item[0], "", entity);
            for (int i = 1; i < columns.size(); i++)
            {
                columns.get(i).deploy(wrapper, item[i]);
            }
            wrappedList.add(wrapper);
        }

        // Кастомный патч
        if (patchResolver != null)
            patchResolver.patch(resultList, wrappedList, columns);

        ds.createPage(wrappedList);
    }
}