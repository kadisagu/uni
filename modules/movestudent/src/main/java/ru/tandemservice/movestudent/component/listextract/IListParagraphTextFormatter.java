/* $Id$ */
package ru.tandemservice.movestudent.component.listextract;

import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author Dmitry Seleznev
 * @since 02.08.2011
 */
public interface IListParagraphTextFormatter
{
    String createParagraphText(IAbstractParagraph paragraph);
}