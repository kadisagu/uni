package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.OtherOrgUnitExtractType;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тип приказа, формируемый на студентов чужого подразделения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OtherOrgUnitExtractTypeGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.OtherOrgUnitExtractType";
    public static final String ENTITY_NAME = "otherOrgUnitExtractType";
    public static final int VERSION_HASH = 325200737;
    private static IEntityMeta ENTITY_META;

    public static final String L_TYPE = "type";
    public static final String P_ACTIVE = "active";

    private StudentExtractType _type;     // Тип выписки по студенту
    private boolean _active;     // Активный

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип выписки по студенту. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public StudentExtractType getType()
    {
        return _type;
    }

    /**
     * @param type Тип выписки по студенту. Свойство не может быть null и должно быть уникальным.
     */
    public void setType(StudentExtractType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Активный. Свойство не может быть null.
     */
    @NotNull
    public boolean isActive()
    {
        return _active;
    }

    /**
     * @param active Активный. Свойство не может быть null.
     */
    public void setActive(boolean active)
    {
        dirty(_active, active);
        _active = active;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OtherOrgUnitExtractTypeGen)
        {
            setType(((OtherOrgUnitExtractType)another).getType());
            setActive(((OtherOrgUnitExtractType)another).isActive());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OtherOrgUnitExtractTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OtherOrgUnitExtractType.class;
        }

        public T newInstance()
        {
            return (T) new OtherOrgUnitExtractType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "type":
                    return obj.getType();
                case "active":
                    return obj.isActive();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "type":
                    obj.setType((StudentExtractType) value);
                    return;
                case "active":
                    obj.setActive((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "type":
                        return true;
                case "active":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "type":
                    return true;
                case "active":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "type":
                    return StudentExtractType.class;
                case "active":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OtherOrgUnitExtractType> _dslPath = new Path<OtherOrgUnitExtractType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OtherOrgUnitExtractType");
    }
            

    /**
     * @return Тип выписки по студенту. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.OtherOrgUnitExtractType#getType()
     */
    public static StudentExtractType.Path<StudentExtractType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Активный. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherOrgUnitExtractType#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    public static class Path<E extends OtherOrgUnitExtractType> extends EntityPath<E>
    {
        private StudentExtractType.Path<StudentExtractType> _type;
        private PropertyPath<Boolean> _active;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип выписки по студенту. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.OtherOrgUnitExtractType#getType()
     */
        public StudentExtractType.Path<StudentExtractType> type()
        {
            if(_type == null )
                _type = new StudentExtractType.Path<StudentExtractType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Активный. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.OtherOrgUnitExtractType#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(OtherOrgUnitExtractTypeGen.P_ACTIVE, this);
            return _active;
        }

        public Class getEntityClass()
        {
            return OtherOrgUnitExtractType.class;
        }

        public String getEntityName()
        {
            return "otherOrgUnitExtractType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
