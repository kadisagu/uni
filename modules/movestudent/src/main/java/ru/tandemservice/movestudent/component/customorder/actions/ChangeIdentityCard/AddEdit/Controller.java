/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeIdentityCard.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditController;
import ru.tandemservice.movestudent.entity.custom.ChangeIdentityCardAction;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class Controller extends BaseCustomActionAddEditController<ChangeIdentityCardAction, IDAO, Model>
{
    public void onRefresh(IBusinessComponent component)
    {
        ChangeIdentityCardAction action = getModel(component).getCustomAction();
        IdentityCardType cardType = action.getCardType();
        if (cardType != null && cardType.isSeriaRequired() && isNeedChange(action.getCardSeria(), cardType.getMaxSeriaLength()))
            action.setCardSeria(getDefaultValue(cardType.getMaxSeriaLength()));
        if (cardType != null && cardType.isNumberRequired() && isNeedChange(action.getCardNumber(), cardType.getMaxNumberLength()))
            action.setCardNumber(getDefaultValue(cardType.getMaxNumberLength()));
        if (cardType == null && StringUtils.containsOnly(action.getCardNumber(), "0"))
            action.setCardNumber(null);
        if (null != cardType && !cardType.isShowSeria())
            action.setCardSeria(null);
    }

    private static boolean isNeedChange(String value, int maxLength)
    {
        return StringUtils.isEmpty(value) || StringUtils.containsOnly(value, "0") || value.length() > maxLength;
    }

    private static String getDefaultValue(int maxLength)
    {
        return String.format("%" + maxLength + "s", "").replaceAll(" ", "0");
    }
}