/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeStudentStatus.AddEdit;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditModel;
import ru.tandemservice.movestudent.entity.custom.ChangeStudentStatusAction;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public class Model extends BaseCustomActionAddEditModel<ChangeStudentStatusAction>
{
    private List<StudentStatus> _newStatusList;

    public List<StudentStatus> getNewStatusList()
    {
        return _newStatusList;
    }

    public void setNewStatusList(List<StudentStatus> newStatusList)
    {
        _newStatusList = newStatusList;
    }
}