/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e27.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 13.09.2012
 */
public class AdmitToPassDiplomaWorkExtParagraphWrapper implements Comparable<AdmitToPassDiplomaWorkExtParagraphWrapper>
{
    private Course _course;
    private CompensationType _compensationType;
    private EducationOrgUnit _educationOrgUnit;
    private String _eduBaseText;
    private final ListStudentExtract _firstExtract;

    public AdmitToPassDiplomaWorkExtParagraphWrapper(Course course, CompensationType compensationType, EducationOrgUnit educationOrgUnit, String eduBaseText, ListStudentExtract firstExtract)
    {
        _course = course;
        _compensationType = compensationType;
        _educationOrgUnit = educationOrgUnit;
        _eduBaseText = eduBaseText;
        _firstExtract = firstExtract;
    }

    private List<AdmitToPassDiplomaWorkExtParagraphPartWrapper> _paragraphPartWrapperList = new ArrayList<AdmitToPassDiplomaWorkExtParagraphPartWrapper>();


    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        _educationOrgUnit = educationOrgUnit;
    }

    public List<AdmitToPassDiplomaWorkExtParagraphPartWrapper> getParagraphPartWrapperList()
    {
        return _paragraphPartWrapperList;
    }

    public void setParagraphPartWrapperList(List<AdmitToPassDiplomaWorkExtParagraphPartWrapper> paragraphPartWrapperList)
    {
        _paragraphPartWrapperList = paragraphPartWrapperList;
    }

    public String getEduBaseText()
    {
        return _eduBaseText;
    }

    public void setEduBaseText(String eduBaseText)
    {
        _eduBaseText = eduBaseText;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof AdmitToPassDiplomaWorkExtParagraphWrapper))
            return false;

        AdmitToPassDiplomaWorkExtParagraphWrapper that = (AdmitToPassDiplomaWorkExtParagraphWrapper) o;
        return (_compensationType.equals(that.getCompensationType()) &&
        _educationOrgUnit.equals(that.getEducationOrgUnit())) &&
        _eduBaseText.equals(that.getEduBaseText());
    }

    @Override
    public int hashCode()
    {
        return _course.hashCode() & _compensationType.hashCode() & _educationOrgUnit.hashCode() & _eduBaseText.hashCode();
    }

    @Override
    public int compareTo(AdmitToPassDiplomaWorkExtParagraphWrapper o)
    {
        String thisCode = _educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix();
        String thatCode = o.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix();

        int result = ((Integer) _course.getIntValue()).compareTo(o.getCourse().getIntValue());
        if (result ==0)
            result = _compensationType.getCode().compareTo(o.getCompensationType().getCode());
        if (result == 0)
            result = thisCode.compareTo(thatCode);

        if (result == 0)
            result = _educationOrgUnit.getTitle().compareTo(o.getEducationOrgUnit().getTitle());

        if (result == 0)
            result = _eduBaseText.compareTo(o.getEduBaseText());

        return result;
    }
}