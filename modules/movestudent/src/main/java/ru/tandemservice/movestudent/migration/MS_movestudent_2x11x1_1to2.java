package ru.tandemservice.movestudent.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_movestudent_2x11x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность courseTransferCustomStuListExtract

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("crstrnsfrcstmstlstextrct_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_5dd40b46"), 
				new DBColumn("courseold_id", DBType.LONG).setNullable(false), 
				new DBColumn("coursenew_id", DBType.LONG).setNullable(false), 
				new DBColumn("deadlinedate_p", DBType.TIMESTAMP), 
				new DBColumn("conditionaltransfer_p", DBType.BOOLEAN).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("courseTransferCustomStuListExtract");

		}


    }
}