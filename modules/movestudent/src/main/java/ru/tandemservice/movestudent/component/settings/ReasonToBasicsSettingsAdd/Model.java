/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.settings.ReasonToBasicsSettingsAdd;

import org.tandemframework.core.component.Input;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;

import java.util.List;

/**
 * @author vip_delete
 * @since 16.10.2008
 */
@Input(keys = "firstId", bindings = "firstId")
public class Model
{
    private long _firstId;
    private StudentOrderReasons _first;
    private StudentOrderBasics _second;
    private List<StudentOrderBasics> _secondList;

    public long getFirstId()
    {
        return _firstId;
    }

    public void setFirstId(long firstId)
    {
        _firstId = firstId;
    }

    public StudentOrderReasons getFirst()
    {
        return _first;
    }

    public void setFirst(StudentOrderReasons first)
    {
        _first = first;
    }

    public StudentOrderBasics getSecond()
    {
        return _second;
    }

    public void setSecond(StudentOrderBasics second)
    {
        _second = second;
    }

    public List<StudentOrderBasics> getSecondList()
    {
        return _secondList;
    }

    public void setSecondList(List<StudentOrderBasics> secondList)
    {
        _secondList = secondList;
    }
}