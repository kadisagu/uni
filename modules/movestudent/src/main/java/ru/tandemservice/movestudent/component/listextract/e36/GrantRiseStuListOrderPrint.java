/* $Id$ */
package ru.tandemservice.movestudent.component.listextract.e36;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.e36.utils.GrantRiseExtractWrapper;
import ru.tandemservice.movestudent.component.listextract.e36.utils.GrantRiseParagraphPartWrapper;
import ru.tandemservice.movestudent.component.listextract.e36.utils.GrantRiseParagraphWrapper;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.GrantRiseStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 22.11.2012
 */
public class GrantRiseStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<GrantRiseStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, prepareParagraphsStructure(extracts));

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }

    protected List<GrantRiseParagraphWrapper> prepareParagraphsStructure(List<GrantRiseStuListExtract> extracts)
    {
        List<GrantRiseParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        int ind;
        for (GrantRiseStuListExtract extract : extracts)
        {
            Student student = extract.getEntity();
            Person person = student.getPerson();

            Group group = student.getGroup();
            EducationOrgUnit educationOrgUnit = extract.getGroup().getEducationOrgUnit();

            GrantRiseParagraphWrapper paragraphWrapper = new GrantRiseParagraphWrapper(extract.getCompensationType(), educationOrgUnit.getEducationLevelHighSchool().getEducationLevel(), educationOrgUnit.getDevelopForm(), educationOrgUnit.getDevelopCondition(), educationOrgUnit.getDevelopTech(), educationOrgUnit.getDevelopPeriod(), extract.getMonth(), extract.getYear(), educationOrgUnit.getFormativeOrgUnit(), educationOrgUnit.getTerritorialOrgUnit(), CommonListOrderPrint.getEducationBaseText(group), extract);

            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else
                paragraphWrapper = paragraphWrapperList.get(ind);

            GrantRiseParagraphPartWrapper paragraphPartWrapper = new GrantRiseParagraphPartWrapper(extract.getCourse(), extract.getGroup(), extract);

            ind = paragraphWrapper.getParagraphPartWrapperList().indexOf(paragraphPartWrapper);
            if (ind == -1)
                paragraphWrapper.getParagraphPartWrapperList().add(paragraphPartWrapper);
            else
                paragraphPartWrapper = paragraphWrapper.getParagraphPartWrapperList().get(ind);

            paragraphPartWrapper.getExtractWrapperList().add(new GrantRiseExtractWrapper(person, extract.getGrantRiseAmount()));
        }

        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<GrantRiseParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            int parNumber = 0;
            for (GrantRiseParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.ACAD_GRANT_RISE_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphWrapper.getFirstExtract();
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(firstExtract.getParagraph(), firstExtract);

                CommonExtractPrint.initDevelopForm(paragraphInjectModifier, paragraphWrapper.getDevelopForm(), "");

                CommonListExtractPrint.injectCompensationType(paragraphInjectModifier, paragraphWrapper.getCompensationType(), "", false);

                OrgUnit formativeOrgUnit = paragraphWrapper.getFormativeOrgUnit();
                OrgUnit territorialOrgUnit = paragraphWrapper.getTerritorialOrgUnit();
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, formativeOrgUnit, "", "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, paragraphWrapper.getEducationLevels(), formativeOrgUnit, territorialOrgUnit, "formativeOrgUnitStr", "");

                paragraphInjectModifier.put("month_P", CommonBaseDateUtil.getMonthNameDeclined(paragraphWrapper.getMonth(), GrammaCase.PREPOSITIONAL));
                paragraphInjectModifier.put("year", String.valueOf(paragraphWrapper.getYear()));

                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, paragraphWrapper.getEducationLevels());
                paragraphInjectModifier.put("parNumber", String.valueOf(++parNumber));
                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");
                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, paragraphWrapper.getDevelopCondition(), paragraphWrapper.getDevelopTech(), paragraphWrapper.getDevelopPeriod(), paragraphWrapper.getEduBaseText(), "fefuShortFastExtendedOptionalText");
                customModifier.listParagraphModifier(paragraphInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphInjectModifier.modify(paragraph);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getParagraphPartWrapperList(), paragraphWrapper.getDevelopForm());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraph, List<GrantRiseParagraphPartWrapper> paragraphPartWrappers, DevelopForm developForm)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartWrappers);
            List<IRtfElement> parList = new ArrayList<>();

            for (GrantRiseParagraphPartWrapper paragraphPartWrapper : paragraphPartWrappers)
            {
                // Получаем шаблон подпараграфа
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.ACAD_GRANT_RISE_LIST_EXTRACT), 3);
                RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = paragraphPartWrapper.getFirstExtract();
                RtfInjectModifier paragraphPartInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, firstExtract);

                paragraphPartInjectModifier.put("course", paragraphPartWrapper.getCourse().getTitle());
                paragraphPartInjectModifier.put("group", paragraphPartWrapper.getGroup().getTitle());
                CommonListOrderPrint.injectFefuGroupPar(paragraphPartInjectModifier, paragraphPartWrapper.getGroup(), developForm);
                customModifier.listParagraphModifier(paragraphPartInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphPartInjectModifier.modify(paragraphPart);

                RtfTableModifier paragraphPartTableModifier = new RtfTableModifier();
                List<GrantRiseExtractWrapper> extractWrapperList = paragraphPartWrapper.getExtractWrapperList();

                Collections.sort(extractWrapperList);

                int j = 0;
                String[][] tableData = new String[extractWrapperList.size()][];
                for (GrantRiseExtractWrapper extractWrapper : extractWrapperList)
                {
                    tableData[j++] = new String[]{String.valueOf(j) + ".", extractWrapper.getPerson().getFullFio(),
                            DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extractWrapper.getGrantRiseAmount()) + " руб."};
                }

                paragraphPartTableModifier.put("STUDENTS_TABLE", tableData);
                customModifier.listParagraphTableModifier(paragraphPartTableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                paragraphPartTableModifier.modify(paragraphPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}