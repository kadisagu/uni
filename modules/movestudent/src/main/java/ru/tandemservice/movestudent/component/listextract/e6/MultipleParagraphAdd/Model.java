/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e6.MultipleParagraphAdd;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListMultipleParagraphAdd.AbstractListMultipleParagraphAddModel;
import ru.tandemservice.movestudent.entity.GiveDiplStuListExtract;
import ru.tandemservice.uni.entity.catalog.Course;

import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 11.06.2009
 */
public class Model extends AbstractListMultipleParagraphAddModel<GiveDiplStuListExtract>
{
    private List<Course> _courseList;
    private Course _course;

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        this._courseList = courseList;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        this._course = course;
    }
}