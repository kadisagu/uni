package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ExtractCreationRule;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Правила формирования сборных приказов по студентам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExtractCreationRuleGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ExtractCreationRule";
    public static final String ENTITY_NAME = "extractCreationRule";
    public static final int VERSION_HASH = -572413376;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_EXTRACT_TYPE = "studentExtractType";
    public static final String P_INDIVIDUAL_ORDER = "individualOrder";

    private StudentExtractType _studentExtractType;     // Тип выписки по студенту
    private boolean _individualOrder;     // Приказ формируется индивидуально

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип выписки по студенту. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public StudentExtractType getStudentExtractType()
    {
        return _studentExtractType;
    }

    /**
     * @param studentExtractType Тип выписки по студенту. Свойство не может быть null и должно быть уникальным.
     */
    public void setStudentExtractType(StudentExtractType studentExtractType)
    {
        dirty(_studentExtractType, studentExtractType);
        _studentExtractType = studentExtractType;
    }

    /**
     * @return Приказ формируется индивидуально. Свойство не может быть null.
     */
    @NotNull
    public boolean isIndividualOrder()
    {
        return _individualOrder;
    }

    /**
     * @param individualOrder Приказ формируется индивидуально. Свойство не может быть null.
     */
    public void setIndividualOrder(boolean individualOrder)
    {
        dirty(_individualOrder, individualOrder);
        _individualOrder = individualOrder;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExtractCreationRuleGen)
        {
            setStudentExtractType(((ExtractCreationRule)another).getStudentExtractType());
            setIndividualOrder(((ExtractCreationRule)another).isIndividualOrder());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExtractCreationRuleGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExtractCreationRule.class;
        }

        public T newInstance()
        {
            return (T) new ExtractCreationRule();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentExtractType":
                    return obj.getStudentExtractType();
                case "individualOrder":
                    return obj.isIndividualOrder();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentExtractType":
                    obj.setStudentExtractType((StudentExtractType) value);
                    return;
                case "individualOrder":
                    obj.setIndividualOrder((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentExtractType":
                        return true;
                case "individualOrder":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentExtractType":
                    return true;
                case "individualOrder":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentExtractType":
                    return StudentExtractType.class;
                case "individualOrder":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExtractCreationRule> _dslPath = new Path<ExtractCreationRule>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExtractCreationRule");
    }
            

    /**
     * @return Тип выписки по студенту. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.ExtractCreationRule#getStudentExtractType()
     */
    public static StudentExtractType.Path<StudentExtractType> studentExtractType()
    {
        return _dslPath.studentExtractType();
    }

    /**
     * @return Приказ формируется индивидуально. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExtractCreationRule#isIndividualOrder()
     */
    public static PropertyPath<Boolean> individualOrder()
    {
        return _dslPath.individualOrder();
    }

    public static class Path<E extends ExtractCreationRule> extends EntityPath<E>
    {
        private StudentExtractType.Path<StudentExtractType> _studentExtractType;
        private PropertyPath<Boolean> _individualOrder;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип выписки по студенту. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.ExtractCreationRule#getStudentExtractType()
     */
        public StudentExtractType.Path<StudentExtractType> studentExtractType()
        {
            if(_studentExtractType == null )
                _studentExtractType = new StudentExtractType.Path<StudentExtractType>(L_STUDENT_EXTRACT_TYPE, this);
            return _studentExtractType;
        }

    /**
     * @return Приказ формируется индивидуально. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExtractCreationRule#isIndividualOrder()
     */
        public PropertyPath<Boolean> individualOrder()
        {
            if(_individualOrder == null )
                _individualOrder = new PropertyPath<Boolean>(ExtractCreationRuleGen.P_INDIVIDUAL_ORDER, this);
            return _individualOrder;
        }

        public Class getEntityClass()
        {
            return ExtractCreationRule.class;
        }

        public String getEntityName()
        {
            return "extractCreationRule";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
