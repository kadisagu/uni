/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.movestudent.base.bo.OrderComment.ui.Edit.OrderCommentEdit;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.ui.formatters.RuMoneyFormatter;

/**
 * @author vip_delete
 * @since 12.05.2009
 */
public class AbstractListExtractPubController<T extends ListStudentExtract, Model extends AbstractListExtractPubModel<T>, IDAO extends IAbstractListExtractPubDAO<T, Model>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);
    }

    public void onClickDeleteExtract(IBusinessComponent component)
    {
        MoveStudentDaoFacade.getMoveStudentDao().deleteListExtract(getModel(component).getExtractId());
        deactivate(component);
    }

    /**
     * Перевод сумм из double в формат XX руб. 00 коп.
     */
    public static String getMoneyFromDouble(double value)
    {
        return RuMoneyFormatter.INSTANCE.format(value);
    }

    public void onClickEditComment(IBusinessComponent component)
    {
        Model model = getModel(component);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(OrderCommentEdit.class.getSimpleName(),
            new ParametersMap().add("extractId", model.getExtractId())));
    }
}
