package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.TransferTerritorialStuExtractGen;

import java.util.Date;

/**
 * О переводе со специальности на специальность (с направления на направление) со сменой территориального подразделения
 */
public class TransferTerritorialStuExtract extends TransferTerritorialStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}