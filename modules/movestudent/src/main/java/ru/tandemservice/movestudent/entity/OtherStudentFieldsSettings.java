package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.gen.OtherStudentFieldsSettingsGen;

/**
 * Используемые поля для типов «прочих» приказов
 */
public class OtherStudentFieldsSettings extends OtherStudentFieldsSettingsGen
{
    public OtherStudentFieldsSettings()
    {
    }

    public OtherStudentFieldsSettings(StudentExtractType extractType)
    {
        setStudentExtractType(extractType);
    }

    public String getAllowedFields()
    {
        StringBuilder result = new StringBuilder("Номер приказа\nДата приказа");
        if (isUseReasonField()) result.append("\nПричина");
        if (isUseCourseField()) result.append("\nКурс");
        if (isUseTermField()) result.append("\nСеместр");
        if (isUseInstituteField()) result.append("\nИнститут");
        if (isUseFacultyField()) result.append("\nФакультет / Школа");
        if (isUseSpecialityField()) result.append("\nНаправление (специальность)");
        if (isUseProfileField()) result.append("\nПрофиль (специализация)");
        if (isUseDopEduLevelField()) result.append("\nДополнительная образовательная программа / Дополнительная квалификация");
        if (isUseEncourageltyField()) result.append("\nПоощрение / взыскание");
        if (isUseEncourageltyEndDateField()) result.append("\nДата снятия поощрения / взыскания");
        if (isUseGrantSizeField()) result.append("\nРазмер стипендии / выплаты");
        if (isUseGrantCategoryField()) result.append("\nКатегория стипендии / выплаты");
        if (isUsePracticeTypeField()) result.append("\nВид практики");
        if (isUsePracticePlaceField()) result.append("\nМесто прохождения практики");
        if (isUsePracticeOwnershipTypeField()) result.append("\nФорма собственности предприятия прохождения практики");
        if (isUsePracticeSettlementField()) result.append("\nГород прохождения практики");
        if (isUsePracticePeriodField()) result.append("\nСроки практики");
        if (isUsePracticeSupervisorField()) result.append("\nРуководитель от вуза");
        result.append("\nПримечание");
        return result.toString();
    }
}