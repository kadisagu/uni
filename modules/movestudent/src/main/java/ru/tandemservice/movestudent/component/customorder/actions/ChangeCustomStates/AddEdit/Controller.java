/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeCustomStates.AddEdit;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditController;
import ru.tandemservice.movestudent.entity.custom.ChangeCustomStatesAction;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class Controller extends BaseCustomActionAddEditController<ChangeCustomStatesAction, IDAO, Model>
{
}