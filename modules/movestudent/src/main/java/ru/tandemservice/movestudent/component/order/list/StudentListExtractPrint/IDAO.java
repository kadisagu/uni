/* $Id$ */
package ru.tandemservice.movestudent.component.order.list.StudentListExtractPrint;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * @since 27.01.2012
 */
public interface IDAO extends IUniDao<Model>
{
}