/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

import org.tandemframework.core.entity.IEntity;

import java.util.Collection;

/**
 * @author Nikolay Fedorovskih
 * @since 27.11.2013
 */
public interface IFlexConfig
{
    IFlexConfig addFilters(IFlexFilter... filters);

    IFlexConfig addColumns(IFlexColumn... columns);

    IFlexConfig addJoins(IFlexJoin... joins);

    IFlexConfig addOrders(IFlexOrder... orders);

    String getAlias();

    Class<? extends IEntity> getClazz();

    Collection<IFlexColumn> getColumns();

    Collection<IFlexFilter> getFilters();

    Collection<IFlexJoin> getJoins();

    IFlexColumn getColumn(String name);

    IFlexOrder getOrder(String name);

    IFlexJoin getJoin(String alias);
}