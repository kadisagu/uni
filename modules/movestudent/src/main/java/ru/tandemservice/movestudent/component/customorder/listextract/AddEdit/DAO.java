/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.customorder.listextract.AddEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.IComponentRegion;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.customorder.CustomOrderUtils;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditModel;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.IBaseCustomActionAddEditDAO;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDao;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.movestudent.entity.custom.ModifyListStuExtract;
import ru.tandemservice.movestudent.entity.custom.OrderActionType;
import ru.tandemservice.movestudent.entity.gen.AbstractStudentExtractGen;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.ICustomOrderAction;
import ru.tandemservice.unimove.entity.gen.ICustomOrderActionGen;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 07.10.2013
 */
public class DAO extends AbstractListParagraphAddEditDAO<ModifyListStuExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        model.setPossibleActionComponents(MoveStudentDaoFacade.getMoveStudentDao().getPossibleCustomActionComponents(true));

        List<OrderActionType> actionTypes = new ArrayList<>();
        actionTypes.addAll(Arrays.asList(OrderActionType.values()));
        if (!StudentExtractTypeCodes.CUSTOM_REVERT_LIST_ORDER.equals(model.getParagraph().getOrder().getType().getCode()))
            actionTypes.remove(OrderActionType.DO_REVERT);
        model.setActionTypeList(actionTypes);

        model.setModifiableOrderModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentListOrder.class, "o")
                        /* [0] */.column(property("o", StudentListOrder.id()))
                        /* [1] */.column(property("o", StudentListOrder.number()))
                        /* [2] */.column(property("o", StudentListOrder.commitDate()))
                        /* [3] */.column(property("o", StudentListOrder.type().title()))
                        .where(eq(property("o", StudentListOrder.state().code()), value(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)))
                        .order(property("o", StudentListOrder.commitDate()), OrderDirection.desc)
                        .order(property("o", StudentListOrder.number()), OrderDirection.desc);

                if (filter != null && !filter.isEmpty())
                {
                    builder.where(likeUpper(DQLFunctions.concat(property("o", StudentListOrder.number()),
                                                                property("o", StudentListOrder.type().title())),
                                       value(CoreStringUtils.escapeLike(filter, true))));
                }

                builder.top(MoveStudentDao.MAX_ROWS);

                DQLExecutionContext context = new DQLExecutionContext(getSession());

                List<Object[]> items = builder.createStatement(context).list();
                List<DataWrapper> resultList = new ArrayList<>(items.size());
                for (Object[] item : items)
                {
                    resultList.add(new DataWrapper(
                            (Long) item[0],
                            CustomOrderUtils.getOrderTitle((String) item[1], (Date) item[2], (String) item[3]))
                    );
                }

                return new ListResult<>(resultList, builder.createCountStatement(context).uniqueResult());
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                if (value instanceof DataWrapper)
                    return ((DataWrapper) value).getTitle();
                else if (value instanceof StudentListOrder)
                    return CustomOrderUtils.getOrderTitle((StudentListOrder) value);
                return super.getLabelFor(value, columnIndex);
            }
        });

        if (model.getParagraphId() != null)
        {
            ModifyListStuExtract extract = (ModifyListStuExtract) model.getParagraph().getFirstExtract();
            model.setModifiableOrder((StudentListOrder) extract.getModifiableExtract().getParagraph().getOrder());
            model.setOrderActionType(extract.getOrderActionType());
        }
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        MQBuilder subQuery = new MQBuilder(ListStudentExtract.ENTITY_CLASS, "le", new String[]{AbstractStudentExtractGen.L_ENTITY + ".id"});
        subQuery.add(MQExpression.eq("le", ListStudentExtract.paragraph().order().s(), model.getModifiableOrder()));
        builder.add(MQExpression.in(STUDENT_ALIAS, Student.P_ID, subQuery));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        if (model.getModifiableOrder() != null)
            super.prepareListDataSource(model);
        else
            model.getDataSource().createPage(Collections.<Student>emptyList());
    }

    @Override
    protected ModifyListStuExtract createNewInstance(Model model)
    {
        return new ModifyListStuExtract();
    }

    @Override
    protected void fillExtract(ModifyListStuExtract extract, Student student, Model model)
    {
        Long modifiableExtractId = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                .column("e.id")
                .where(eq(property("e", AbstractStudentExtract.paragraph().order().id()), value(model.getModifiableOrder().getId())))
                .where(eq(property("e", AbstractStudentExtract.entity().id()), value(student.getId())))
                .createStatement(getSession()).uniqueResult();

        extract.setModifiableExtract(proxy(modifiableExtractId));
        extract.setOrderActionType(model.getOrderActionType());
    }

    @Override
    public void update(Model model)
    {
        super.update(model);
        for (IAbstractExtract extract : model.getParagraph().getExtractList())
        {
            afterFillExtract((ModifyListStuExtract) extract, model);
        }
    }

    @SuppressWarnings("unchecked")
    protected void afterFillExtract(ModifyListStuExtract extract, Model model)
    {
        if (OrderActionType.DO_CUSTOM_ACTION == model.getOrderActionType())
        {
            boolean hasSelectedActions = false;
            for (String regionName : model.getPossibleActionComponents().keySet())
            {
                IComponentRegion region = model.getContext().getChildRegion(regionName);
                IBusinessComponent component = region.getActiveComponent();
                IBaseCustomActionAddEditDAO dao = (IBaseCustomActionAddEditDAO) component.getController().getDao();
                BaseCustomActionAddEditModel actionModel = component.getModel();
                dao.updateForExtract(actionModel, extract, model.isEditForm());
                if (actionModel.isActionActive())
                    hasSelectedActions = true;
            }
            if (!hasSelectedActions)
            {
                throw new ApplicationException("Не выбрано ни одного действия для актуализации данных студента.");
            }
        }
        else
        {
            if (model.isEditForm())
            {
                for (ICustomOrderAction action : getList(ICustomOrderAction.class, ICustomOrderActionGen.customExtract().id(), extract.getId()))
                {
                    delete(action);
                }
            }
            CustomOrderUtils.checkRevertAvailable(extract);
        }
    }
}