package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.entity.gen.AdoptRussianCitizenshipStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О приеме гражданства Российской Федерации
 */
public class AdoptRussianCitizenshipStuExtract extends AdoptRussianCitizenshipStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getCitizenshipDate();
    }
}