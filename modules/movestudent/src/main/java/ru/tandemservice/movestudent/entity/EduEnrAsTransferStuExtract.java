package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IEnrollmentExtract;
import ru.tandemservice.movestudent.entity.gen.EduEnrAsTransferStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О зачислении в порядке перевода
 */
public class EduEnrAsTransferStuExtract extends EduEnrAsTransferStuExtractGen implements IEnrollmentExtract
{
    @Override
    public Date getEntranceDate()
    {
        return getEntryDate();
    }
}