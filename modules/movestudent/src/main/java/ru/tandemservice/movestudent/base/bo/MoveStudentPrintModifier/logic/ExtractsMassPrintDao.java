/* $Id$ */
package ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic;

import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 24.11.2016
 */
public class ExtractsMassPrintDao extends UniBaseDao implements IExtractsMassPrintDao {

    @Override
    public IDocumentRenderer printMassOrder(AbstractStudentOrder order) {
        String stateCode = order.getState().getCode();
        boolean  isListOrder =  order instanceof StudentListOrder;
        List<AbstractStudentExtract> extractList = new LinkedList<>();
        order.getParagraphList().stream().forEach(par -> extractList.addAll(par.getExtractList()));

        RtfDocument result = null;
        byte[] template=null;

        if(isListOrder) {
            template = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(extractList.get(0).getType(), MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);
            if (null == template)
                return null;
        }

        for (AbstractStudentExtract extract : extractList) {
            RtfDocument document = null;

            if(!isListOrder)
            {
                template = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(extract.getType(), MoveStudentDefines.EXTRACT_TEXT_CODE);
            }

            if (UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTABLE.equals(stateCode)
                    || UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED.equals(stateCode)
                    || UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(stateCode)) {

                // приказ проведен => печатная форма сохранена
                StudentExtractTextRelation rel = MoveStudentDaoFacade.getMoveStudentDao().getExtractTextRelation(extract.getId(), isListOrder? MoveStudentDefines.LIST_EXTRACT_TEXT_CODE: MoveStudentDefines.EXTRACT_TEXT_CODE);
                // В процессе деперсонализации БД связь могла быть удалена
                if (rel != null)
                    document = new RtfReader().read(rel.getText());
            } else {
                String printName = EntityRuntime.getMeta(extract).getName() + "_extractPrint";
                IPrintFormCreator<AbstractStudentExtract> componentPrint = CommonExtractPrint.getPrintFormCreator(printName);
                assert componentPrint != null;
                document = componentPrint.createPrintForm(template, extract);
            }

            if (null == result) {
                result = document;
            } else {
                result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                result.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                result.getElementList().addAll(document.getElementList());
            }
        }

        return new CommonBaseRenderer().rtf().document(result).fileName("StudentOrderExtracts.rtf");
    }
}

