package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AdmitToPassDiplomaWorkExtStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О допуске к защите выпускной квалификационной работы»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AdmitToPassDiplomaWorkExtStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.AdmitToPassDiplomaWorkExtStuListExtract";
    public static final String ENTITY_NAME = "admitToPassDiplomaWorkExtStuListExtract";
    public static final int VERSION_HASH = 1659130180;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_DEVELOP_CONDITION = "developCondition";
    public static final String L_COMPENSATION_TYPE = "compensationType";

    private Course _course;     // Курс
    private Group _group;     // Группа
    private DevelopForm _developForm;     // Форма освоения
    private DevelopCondition _developCondition;     // Условие освоения
    private CompensationType _compensationType;     // Вид возмещения затрат

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения. Свойство не может быть null.
     */
    public void setDevelopCondition(DevelopCondition developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof AdmitToPassDiplomaWorkExtStuListExtractGen)
        {
            setCourse(((AdmitToPassDiplomaWorkExtStuListExtract)another).getCourse());
            setGroup(((AdmitToPassDiplomaWorkExtStuListExtract)another).getGroup());
            setDevelopForm(((AdmitToPassDiplomaWorkExtStuListExtract)another).getDevelopForm());
            setDevelopCondition(((AdmitToPassDiplomaWorkExtStuListExtract)another).getDevelopCondition());
            setCompensationType(((AdmitToPassDiplomaWorkExtStuListExtract)another).getCompensationType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AdmitToPassDiplomaWorkExtStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AdmitToPassDiplomaWorkExtStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new AdmitToPassDiplomaWorkExtStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "compensationType":
                    return obj.getCompensationType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((DevelopCondition) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "group":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "compensationType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "group":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "compensationType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "developForm":
                    return DevelopForm.class;
                case "developCondition":
                    return DevelopCondition.class;
                case "compensationType":
                    return CompensationType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AdmitToPassDiplomaWorkExtStuListExtract> _dslPath = new Path<AdmitToPassDiplomaWorkExtStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AdmitToPassDiplomaWorkExtStuListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassDiplomaWorkExtStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassDiplomaWorkExtStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassDiplomaWorkExtStuListExtract#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassDiplomaWorkExtStuListExtract#getDevelopCondition()
     */
    public static DevelopCondition.Path<DevelopCondition> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassDiplomaWorkExtStuListExtract#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    public static class Path<E extends AdmitToPassDiplomaWorkExtStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private DevelopForm.Path<DevelopForm> _developForm;
        private DevelopCondition.Path<DevelopCondition> _developCondition;
        private CompensationType.Path<CompensationType> _compensationType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassDiplomaWorkExtStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassDiplomaWorkExtStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassDiplomaWorkExtStuListExtract#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassDiplomaWorkExtStuListExtract#getDevelopCondition()
     */
        public DevelopCondition.Path<DevelopCondition> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new DevelopCondition.Path<DevelopCondition>(L_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.movestudent.entity.AdmitToPassDiplomaWorkExtStuListExtract#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

        public Class getEntityClass()
        {
            return AdmitToPassDiplomaWorkExtStuListExtract.class;
        }

        public String getEntityName()
        {
            return "admitToPassDiplomaWorkExtStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
