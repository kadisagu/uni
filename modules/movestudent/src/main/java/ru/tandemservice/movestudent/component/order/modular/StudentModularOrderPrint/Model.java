/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.modular.StudentModularOrderPrint;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.movestudent.entity.StudentModularOrder;

/**
 * @author vip_delete
 * @since 20.10.2008
 */
@State({
        @Bind(key = "orderId", binding = "orderId"),
        @Bind(key = "printPdf", binding = "printPdf")
})
public class Model
{
    private Long _orderId;
    private StudentModularOrder _order;
    private byte[] _data;
    private String[][] _visaData;
    private Boolean _printPdf;
    private String _studentFio;
    private boolean _isCustomIndividual;

    public String getFileName()
    {
        return (null != _studentFio ? (_studentFio + " ") : "Order ") + "orderId" + String.valueOf(_orderId);
    }
    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public StudentModularOrder getOrder()
    {
        return _order;
    }

    public void setOrder(StudentModularOrder order)
    {
        _order = order;
    }

    public byte[] getData()
    {
        return _data;
    }

    public void setData(byte[] data)
    {
        _data = data;
    }

    public String[][] getVisaData()
    {
        return _visaData;
    }

    public void setVisaData(String[][] visaData)
    {
        _visaData = visaData;
    }

    public Boolean getPrintPdf()
    {
        return _printPdf;
    }

    public void setPrintPdf(Boolean printPdf)
    {
        _printPdf = printPdf;
    }

    public String getStudentFio()
    {
        return _studentFio;
    }

    public void setStudentFio(String studentFio)
    {
        _studentFio = studentFio;
    }

    public boolean isCustomIndividual()
    {
        return _isCustomIndividual;
    }

    public void setCustomIndividual(boolean customIndividual)
    {
        _isCustomIndividual = customIndividual;
    }
}