/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeGroup.Pub;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.BaseCustomActionPubDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeGroupAction;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class DAO extends BaseCustomActionPubDAO<ChangeGroupAction, Model>
{
    @Override
    protected Class<ChangeGroupAction> getActionClass()
    {
        return ChangeGroupAction.class;
    }
}