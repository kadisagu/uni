/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.modular.ModularStudentExtractPrint;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentExtractTextRelation;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * @author vip_delete
 * @since 24.10.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setExtract(getNotNull(model.getExtractId()));

        if (UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED.equals(model.getExtract().getState().getCode()))
        {
            // выписка проведена => печатная форма сохранена
            StudentExtractTextRelation rel = MoveStudentDaoFacade.getMoveStudentDao().getExtractTextRelation(model.getExtract().getId(), MoveStudentDefines.EXTRACT_TEXT_CODE);
            // В процессе деперсонализации БД связь могла быть удалена
            if (rel != null)
                model.setData(rel.getText());
            else
                prepareDefaultTemplate(model);
        }
        else
            prepareDefaultTemplate(model);
    }

    private void prepareDefaultTemplate(Model model)
    {
        model.setData(MoveStudentDaoFacade.getMoveStudentDao().getTemplate(model.getExtract().getType(), MoveStudentDefines.EXTRACT_TEXT_CODE));
    }
}
