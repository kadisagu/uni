package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CounterMSRForExportFile;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Счетчик выгруженных за день файлов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CounterMSRForExportFileGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.CounterMSRForExportFile";
    public static final String ENTITY_NAME = "counterMSRForExportFile";
    public static final int VERSION_HASH = 915556901;
    private static IEntityMeta ENTITY_META;

    public static final String P_CURRENT_DATE = "currentDate";
    public static final String P_COUNTER = "counter";

    private Date _currentDate;     // Дата актуальности счетчика
    private int _counter = 1;     // Счетчик

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата актуальности счетчика. Свойство не может быть null.
     */
    @NotNull
    public Date getCurrentDate()
    {
        return _currentDate;
    }

    /**
     * @param currentDate Дата актуальности счетчика. Свойство не может быть null.
     */
    public void setCurrentDate(Date currentDate)
    {
        dirty(_currentDate, currentDate);
        _currentDate = currentDate;
    }

    /**
     * @return Счетчик. Свойство не может быть null.
     */
    @NotNull
    public int getCounter()
    {
        return _counter;
    }

    /**
     * @param counter Счетчик. Свойство не может быть null.
     */
    public void setCounter(int counter)
    {
        dirty(_counter, counter);
        _counter = counter;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof CounterMSRForExportFileGen)
        {
            setCurrentDate(((CounterMSRForExportFile)another).getCurrentDate());
            setCounter(((CounterMSRForExportFile)another).getCounter());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CounterMSRForExportFileGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CounterMSRForExportFile.class;
        }

        public T newInstance()
        {
            return (T) new CounterMSRForExportFile();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "currentDate":
                    return obj.getCurrentDate();
                case "counter":
                    return obj.getCounter();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "currentDate":
                    obj.setCurrentDate((Date) value);
                    return;
                case "counter":
                    obj.setCounter((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "currentDate":
                        return true;
                case "counter":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "currentDate":
                    return true;
                case "counter":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "currentDate":
                    return Date.class;
                case "counter":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CounterMSRForExportFile> _dslPath = new Path<CounterMSRForExportFile>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CounterMSRForExportFile");
    }
            

    /**
     * @return Дата актуальности счетчика. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CounterMSRForExportFile#getCurrentDate()
     */
    public static PropertyPath<Date> currentDate()
    {
        return _dslPath.currentDate();
    }

    /**
     * @return Счетчик. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CounterMSRForExportFile#getCounter()
     */
    public static PropertyPath<Integer> counter()
    {
        return _dslPath.counter();
    }

    public static class Path<E extends CounterMSRForExportFile> extends EntityPath<E>
    {
        private PropertyPath<Date> _currentDate;
        private PropertyPath<Integer> _counter;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата актуальности счетчика. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CounterMSRForExportFile#getCurrentDate()
     */
        public PropertyPath<Date> currentDate()
        {
            if(_currentDate == null )
                _currentDate = new PropertyPath<Date>(CounterMSRForExportFileGen.P_CURRENT_DATE, this);
            return _currentDate;
        }

    /**
     * @return Счетчик. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.CounterMSRForExportFile#getCounter()
     */
        public PropertyPath<Integer> counter()
        {
            if(_counter == null )
                _counter = new PropertyPath<Integer>(CounterMSRForExportFileGen.P_COUNTER, this);
            return _counter;
        }

        public Class getEntityClass()
        {
            return CounterMSRForExportFile.class;
        }

        public String getEntityName()
        {
            return "counterMSRForExportFile";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
