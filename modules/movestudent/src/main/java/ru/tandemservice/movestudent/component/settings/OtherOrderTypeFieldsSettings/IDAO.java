/* $Id$ */
package ru.tandemservice.movestudent.component.settings.OtherOrderTypeFieldsSettings;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Dmitry Seleznev
 * @since 05.04.2013
 */
public interface IDAO extends IUniDao<Model>
{
    void deleteSetting(Long studentExtractTypeId);
}