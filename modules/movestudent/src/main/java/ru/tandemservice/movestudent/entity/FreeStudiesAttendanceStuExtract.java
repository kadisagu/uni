package ru.tandemservice.movestudent.entity;

import java.util.Date;

import ru.tandemservice.movestudent.entity.gen.FreeStudiesAttendanceStuExtractGen;

/**
 * Выписка из сборного приказа по студенту. О свободном посещении занятий
 */
public class FreeStudiesAttendanceStuExtract extends FreeStudiesAttendanceStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getFromDate();
    }

    @Override
    public Date getEndDate()
    {
        return getToDate();
    }
}