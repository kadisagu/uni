package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.movestudent.entity.gen.SplitStudentsGroupStuListExtractGen;

import java.util.Date;

/**
 * Проект приказа «О разделении студентов по специализациям»
 */
public class SplitStudentsGroupStuListExtract extends SplitStudentsGroupStuListExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getBeginDate();
    }
}