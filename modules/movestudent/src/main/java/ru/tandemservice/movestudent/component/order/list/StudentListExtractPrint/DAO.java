/* $Id$ */
package ru.tandemservice.movestudent.component.order.list.StudentListExtractPrint;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentExtractTextRelation;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * @author dseleznev
 * @since 27.01.2012
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setExtract(getNotNull(model.getExtractId()));

        if (UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED.equals(model.getExtract().getState().getCode()))
        {
            // выписка проведена => печатная форма сохранена
            StudentExtractTextRelation rel = MoveStudentDaoFacade.getMoveStudentDao().getExtractTextRelation(model.getExtract().getId(), MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);
            // В процессе деперсонализации БД связь могла быть удалена
            if (rel != null)
                model.setData(rel.getText());
            else
                prepareDefaultTemplate(model);
        }
        else
            prepareDefaultTemplate(model);
    }

    private void prepareDefaultTemplate(Model model)
    {
        model.setData(MoveStudentDaoFacade.getMoveStudentDao().getTemplate(model.getExtract().getType(), MoveStudentDefines.LIST_EXTRACT_TEXT_CODE));
    }
}