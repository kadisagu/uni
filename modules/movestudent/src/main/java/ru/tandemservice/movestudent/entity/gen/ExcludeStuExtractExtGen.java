package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ExcludeStuExtractExt;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об отчислении (расш. вариант)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExcludeStuExtractExtGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ExcludeStuExtractExt";
    public static final String ENTITY_NAME = "excludeStuExtractExt";
    public static final int VERSION_HASH = -42883584;
    private static IEntityMeta ENTITY_META;

    public static final String P_PRINT_PREAMBULA = "printPreambula";
    public static final String P_DATE = "date";
    public static final String P_REASON_STR = "reasonStr";
    public static final String P_STOP_GRANTS_PAYING = "stopGrantsPaying";
    public static final String P_STOP_PAYING_DATE = "stopPayingDate";
    public static final String P_GIVE_DIPLOMA = "giveDiploma";
    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";

    private boolean _printPreambula;     // Выводить при печати констатирующую часть
    private Date _date;     // Дата отчисления
    private String _reasonStr;     // Обстоятельства прекращения обучения
    private boolean _stopGrantsPaying;     // Отменить выплату стипендии
    private Date _stopPayingDate;     // Дата прекращения выплаты стипендии
    private boolean _giveDiploma;     // Выдается диплом о незаконченном высшем образовании
    private StudentStatus _studentStatusOld;     // Предыдущее состояние студента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выводить при печати констатирующую часть. Свойство не может быть null.
     */
    @NotNull
    public boolean isPrintPreambula()
    {
        return _printPreambula;
    }

    /**
     * @param printPreambula Выводить при печати констатирующую часть. Свойство не может быть null.
     */
    public void setPrintPreambula(boolean printPreambula)
    {
        dirty(_printPreambula, printPreambula);
        _printPreambula = printPreambula;
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата отчисления. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Обстоятельства прекращения обучения.
     */
    @Length(max=255)
    public String getReasonStr()
    {
        return _reasonStr;
    }

    /**
     * @param reasonStr Обстоятельства прекращения обучения.
     */
    public void setReasonStr(String reasonStr)
    {
        dirty(_reasonStr, reasonStr);
        _reasonStr = reasonStr;
    }

    /**
     * @return Отменить выплату стипендии. Свойство не может быть null.
     */
    @NotNull
    public boolean isStopGrantsPaying()
    {
        return _stopGrantsPaying;
    }

    /**
     * @param stopGrantsPaying Отменить выплату стипендии. Свойство не может быть null.
     */
    public void setStopGrantsPaying(boolean stopGrantsPaying)
    {
        dirty(_stopGrantsPaying, stopGrantsPaying);
        _stopGrantsPaying = stopGrantsPaying;
    }

    /**
     * @return Дата прекращения выплаты стипендии.
     */
    public Date getStopPayingDate()
    {
        return _stopPayingDate;
    }

    /**
     * @param stopPayingDate Дата прекращения выплаты стипендии.
     */
    public void setStopPayingDate(Date stopPayingDate)
    {
        dirty(_stopPayingDate, stopPayingDate);
        _stopPayingDate = stopPayingDate;
    }

    /**
     * @return Выдается диплом о незаконченном высшем образовании. Свойство не может быть null.
     */
    @NotNull
    public boolean isGiveDiploma()
    {
        return _giveDiploma;
    }

    /**
     * @param giveDiploma Выдается диплом о незаконченном высшем образовании. Свойство не может быть null.
     */
    public void setGiveDiploma(boolean giveDiploma)
    {
        dirty(_giveDiploma, giveDiploma);
        _giveDiploma = giveDiploma;
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Предыдущее состояние студента. Свойство не может быть null.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ExcludeStuExtractExtGen)
        {
            setPrintPreambula(((ExcludeStuExtractExt)another).isPrintPreambula());
            setDate(((ExcludeStuExtractExt)another).getDate());
            setReasonStr(((ExcludeStuExtractExt)another).getReasonStr());
            setStopGrantsPaying(((ExcludeStuExtractExt)another).isStopGrantsPaying());
            setStopPayingDate(((ExcludeStuExtractExt)another).getStopPayingDate());
            setGiveDiploma(((ExcludeStuExtractExt)another).isGiveDiploma());
            setStudentStatusOld(((ExcludeStuExtractExt)another).getStudentStatusOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExcludeStuExtractExtGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExcludeStuExtractExt.class;
        }

        public T newInstance()
        {
            return (T) new ExcludeStuExtractExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "printPreambula":
                    return obj.isPrintPreambula();
                case "date":
                    return obj.getDate();
                case "reasonStr":
                    return obj.getReasonStr();
                case "stopGrantsPaying":
                    return obj.isStopGrantsPaying();
                case "stopPayingDate":
                    return obj.getStopPayingDate();
                case "giveDiploma":
                    return obj.isGiveDiploma();
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "printPreambula":
                    obj.setPrintPreambula((Boolean) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "reasonStr":
                    obj.setReasonStr((String) value);
                    return;
                case "stopGrantsPaying":
                    obj.setStopGrantsPaying((Boolean) value);
                    return;
                case "stopPayingDate":
                    obj.setStopPayingDate((Date) value);
                    return;
                case "giveDiploma":
                    obj.setGiveDiploma((Boolean) value);
                    return;
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "printPreambula":
                        return true;
                case "date":
                        return true;
                case "reasonStr":
                        return true;
                case "stopGrantsPaying":
                        return true;
                case "stopPayingDate":
                        return true;
                case "giveDiploma":
                        return true;
                case "studentStatusOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "printPreambula":
                    return true;
                case "date":
                    return true;
                case "reasonStr":
                    return true;
                case "stopGrantsPaying":
                    return true;
                case "stopPayingDate":
                    return true;
                case "giveDiploma":
                    return true;
                case "studentStatusOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "printPreambula":
                    return Boolean.class;
                case "date":
                    return Date.class;
                case "reasonStr":
                    return String.class;
                case "stopGrantsPaying":
                    return Boolean.class;
                case "stopPayingDate":
                    return Date.class;
                case "giveDiploma":
                    return Boolean.class;
                case "studentStatusOld":
                    return StudentStatus.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExcludeStuExtractExt> _dslPath = new Path<ExcludeStuExtractExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExcludeStuExtractExt");
    }
            

    /**
     * @return Выводить при печати констатирующую часть. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeStuExtractExt#isPrintPreambula()
     */
    public static PropertyPath<Boolean> printPreambula()
    {
        return _dslPath.printPreambula();
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeStuExtractExt#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Обстоятельства прекращения обучения.
     * @see ru.tandemservice.movestudent.entity.ExcludeStuExtractExt#getReasonStr()
     */
    public static PropertyPath<String> reasonStr()
    {
        return _dslPath.reasonStr();
    }

    /**
     * @return Отменить выплату стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeStuExtractExt#isStopGrantsPaying()
     */
    public static PropertyPath<Boolean> stopGrantsPaying()
    {
        return _dslPath.stopGrantsPaying();
    }

    /**
     * @return Дата прекращения выплаты стипендии.
     * @see ru.tandemservice.movestudent.entity.ExcludeStuExtractExt#getStopPayingDate()
     */
    public static PropertyPath<Date> stopPayingDate()
    {
        return _dslPath.stopPayingDate();
    }

    /**
     * @return Выдается диплом о незаконченном высшем образовании. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeStuExtractExt#isGiveDiploma()
     */
    public static PropertyPath<Boolean> giveDiploma()
    {
        return _dslPath.giveDiploma();
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeStuExtractExt#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    public static class Path<E extends ExcludeStuExtractExt> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Boolean> _printPreambula;
        private PropertyPath<Date> _date;
        private PropertyPath<String> _reasonStr;
        private PropertyPath<Boolean> _stopGrantsPaying;
        private PropertyPath<Date> _stopPayingDate;
        private PropertyPath<Boolean> _giveDiploma;
        private StudentStatus.Path<StudentStatus> _studentStatusOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выводить при печати констатирующую часть. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeStuExtractExt#isPrintPreambula()
     */
        public PropertyPath<Boolean> printPreambula()
        {
            if(_printPreambula == null )
                _printPreambula = new PropertyPath<Boolean>(ExcludeStuExtractExtGen.P_PRINT_PREAMBULA, this);
            return _printPreambula;
        }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeStuExtractExt#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(ExcludeStuExtractExtGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Обстоятельства прекращения обучения.
     * @see ru.tandemservice.movestudent.entity.ExcludeStuExtractExt#getReasonStr()
     */
        public PropertyPath<String> reasonStr()
        {
            if(_reasonStr == null )
                _reasonStr = new PropertyPath<String>(ExcludeStuExtractExtGen.P_REASON_STR, this);
            return _reasonStr;
        }

    /**
     * @return Отменить выплату стипендии. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeStuExtractExt#isStopGrantsPaying()
     */
        public PropertyPath<Boolean> stopGrantsPaying()
        {
            if(_stopGrantsPaying == null )
                _stopGrantsPaying = new PropertyPath<Boolean>(ExcludeStuExtractExtGen.P_STOP_GRANTS_PAYING, this);
            return _stopGrantsPaying;
        }

    /**
     * @return Дата прекращения выплаты стипендии.
     * @see ru.tandemservice.movestudent.entity.ExcludeStuExtractExt#getStopPayingDate()
     */
        public PropertyPath<Date> stopPayingDate()
        {
            if(_stopPayingDate == null )
                _stopPayingDate = new PropertyPath<Date>(ExcludeStuExtractExtGen.P_STOP_PAYING_DATE, this);
            return _stopPayingDate;
        }

    /**
     * @return Выдается диплом о незаконченном высшем образовании. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeStuExtractExt#isGiveDiploma()
     */
        public PropertyPath<Boolean> giveDiploma()
        {
            if(_giveDiploma == null )
                _giveDiploma = new PropertyPath<Boolean>(ExcludeStuExtractExtGen.P_GIVE_DIPLOMA, this);
            return _giveDiploma;
        }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeStuExtractExt#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

        public Class getEntityClass()
        {
            return ExcludeStuExtractExt.class;
        }

        public String getEntityName()
        {
            return "excludeStuExtractExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
