/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e60.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.movestudent.entity.ReEducationFGOSStuExtract;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 24.08.2012
 */
public class Controller extends CommonModularStudentExtractAddEditController<ReEducationFGOSStuExtract, IDAO, Model>
{
    public void onChangeEduLevel(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getEduModel().setDevelopForm(model.getExtract().getEntity().getEducationOrgUnit().getDevelopForm());
        model.getEduModel().setDevelopCondition(model.getExtract().getEntity().getEducationOrgUnit().getDevelopCondition());
        model.getEduModel().setDevelopTech(model.getExtract().getEntity().getEducationOrgUnit().getDevelopTech());
    }

    public void onChangeAnnulAttestation(IBusinessComponent component)
    {
        Model model = getModel(component);
        if(model.getExtract().isAnnulAttestationResults())
        {
            model.getExtract().setPrevCourse(model.getExtract().getEntity().getCourse());
            model.getExtract().setEduYear(EducationYear.getCurrentRequired());
        }
    }
}