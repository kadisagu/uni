package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ExcludeGPDefenceFailStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об отчислении (ВКР)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExcludeGPDefenceFailStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ExcludeGPDefenceFailStuExtract";
    public static final String ENTITY_NAME = "excludeGPDefenceFailStuExtract";
    public static final int VERSION_HASH = 1266298549;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";
    public static final String P_PASS_STATE_EXAM = "passStateExam";
    public static final String P_PLURAL_FOR_STATE_EXAM = "pluralForStateExam";

    private StudentStatus _studentStatusOld;     // Состояние студента на момент проведения приказа
    private Boolean _passStateExam;     // Сдача ГЭ предусмотрена
    private Boolean _pluralForStateExam;     // Множественное число для ГЭ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Состояние студента на момент проведения приказа. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Состояние студента на момент проведения приказа. Свойство не может быть null.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    /**
     * @return Сдача ГЭ предусмотрена.
     */
    public Boolean getPassStateExam()
    {
        return _passStateExam;
    }

    /**
     * @param passStateExam Сдача ГЭ предусмотрена.
     */
    public void setPassStateExam(Boolean passStateExam)
    {
        dirty(_passStateExam, passStateExam);
        _passStateExam = passStateExam;
    }

    /**
     * @return Множественное число для ГЭ.
     */
    public Boolean getPluralForStateExam()
    {
        return _pluralForStateExam;
    }

    /**
     * @param pluralForStateExam Множественное число для ГЭ.
     */
    public void setPluralForStateExam(Boolean pluralForStateExam)
    {
        dirty(_pluralForStateExam, pluralForStateExam);
        _pluralForStateExam = pluralForStateExam;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ExcludeGPDefenceFailStuExtractGen)
        {
            setStudentStatusOld(((ExcludeGPDefenceFailStuExtract)another).getStudentStatusOld());
            setPassStateExam(((ExcludeGPDefenceFailStuExtract)another).getPassStateExam());
            setPluralForStateExam(((ExcludeGPDefenceFailStuExtract)another).getPluralForStateExam());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExcludeGPDefenceFailStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExcludeGPDefenceFailStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new ExcludeGPDefenceFailStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
                case "passStateExam":
                    return obj.getPassStateExam();
                case "pluralForStateExam":
                    return obj.getPluralForStateExam();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
                case "passStateExam":
                    obj.setPassStateExam((Boolean) value);
                    return;
                case "pluralForStateExam":
                    obj.setPluralForStateExam((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                        return true;
                case "passStateExam":
                        return true;
                case "pluralForStateExam":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                    return true;
                case "passStateExam":
                    return true;
                case "pluralForStateExam":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                    return StudentStatus.class;
                case "passStateExam":
                    return Boolean.class;
                case "pluralForStateExam":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExcludeGPDefenceFailStuExtract> _dslPath = new Path<ExcludeGPDefenceFailStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExcludeGPDefenceFailStuExtract");
    }
            

    /**
     * @return Состояние студента на момент проведения приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeGPDefenceFailStuExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    /**
     * @return Сдача ГЭ предусмотрена.
     * @see ru.tandemservice.movestudent.entity.ExcludeGPDefenceFailStuExtract#getPassStateExam()
     */
    public static PropertyPath<Boolean> passStateExam()
    {
        return _dslPath.passStateExam();
    }

    /**
     * @return Множественное число для ГЭ.
     * @see ru.tandemservice.movestudent.entity.ExcludeGPDefenceFailStuExtract#getPluralForStateExam()
     */
    public static PropertyPath<Boolean> pluralForStateExam()
    {
        return _dslPath.pluralForStateExam();
    }

    public static class Path<E extends ExcludeGPDefenceFailStuExtract> extends ModularStudentExtract.Path<E>
    {
        private StudentStatus.Path<StudentStatus> _studentStatusOld;
        private PropertyPath<Boolean> _passStateExam;
        private PropertyPath<Boolean> _pluralForStateExam;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Состояние студента на момент проведения приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ExcludeGPDefenceFailStuExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

    /**
     * @return Сдача ГЭ предусмотрена.
     * @see ru.tandemservice.movestudent.entity.ExcludeGPDefenceFailStuExtract#getPassStateExam()
     */
        public PropertyPath<Boolean> passStateExam()
        {
            if(_passStateExam == null )
                _passStateExam = new PropertyPath<Boolean>(ExcludeGPDefenceFailStuExtractGen.P_PASS_STATE_EXAM, this);
            return _passStateExam;
        }

    /**
     * @return Множественное число для ГЭ.
     * @see ru.tandemservice.movestudent.entity.ExcludeGPDefenceFailStuExtract#getPluralForStateExam()
     */
        public PropertyPath<Boolean> pluralForStateExam()
        {
            if(_pluralForStateExam == null )
                _pluralForStateExam = new PropertyPath<Boolean>(ExcludeGPDefenceFailStuExtractGen.P_PLURAL_FOR_STATE_EXAM, this);
            return _pluralForStateExam;
        }

        public Class getEntityClass()
        {
            return ExcludeGPDefenceFailStuExtract.class;
        }

        public String getEntityName()
        {
            return "excludeGPDefenceFailStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
