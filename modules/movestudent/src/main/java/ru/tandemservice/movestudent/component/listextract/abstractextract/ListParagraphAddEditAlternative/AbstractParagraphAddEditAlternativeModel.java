/*$Id$*/
package ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEditAlternative;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author DMITRY KNYAZEV
 * @since 30.05.2014
 */
public abstract class AbstractParagraphAddEditAlternativeModel<T extends ListStudentExtract> extends AbstractListParagraphAddEditModel<T>
{
	public static final String P_CHECKBOX_SELECTED_COLUMN = "checkboxSelected";

	private static final String _commonPageAddEdit = AbstractParagraphAddEditAlternativeModel.class.getPackage().getName() + ".CommonPageAddEdit";

	private String _filterPage;
	private String _attributesPage;
	private String _searchStuListSettingsKey;

	private DynamicListDataSource<Student> _searchDataSource;
	private List<Student> _addedStudentsList = new ArrayList<>();
	private Set<Student> _markedStudentsList = new HashSet<>();
	private boolean _initialized = false;

	public boolean isInitialized()
	{
		return _initialized;
	}

	public void setInitialized(boolean initialized)
	{
		_initialized = initialized;
	}

	public OrgUnit getOrgUnit()
	{
		return getParagraph().getOrder().getOrgUnit();
	}

	public String getCommonPageAddEdit()
	{
		return _commonPageAddEdit;
	}

	public String getFilterPage()
	{
		return _filterPage;
	}

	public void setFilterPage(String filterPage)
	{
		_filterPage = filterPage;
	}

	public String getAttributesPage()
	{
		return _attributesPage;
	}

	public void setAttributesPage(String attributesPage)
	{
		_attributesPage = attributesPage;
	}

	public String getSearchStuListSettingsKey()
	{
		return _searchStuListSettingsKey;
	}

	public void setSearchStuListSettingsKey(String searchStuListSettingsKey)
	{
		_searchStuListSettingsKey = searchStuListSettingsKey;
	}

	public DynamicListDataSource<Student> getSearchDataSource()
	{
		return _searchDataSource;
	}

	public void setSearchDataSource(DynamicListDataSource<Student> searchDataSource)
	{
		_searchDataSource = searchDataSource;
	}

	public List<Student> getAddedStudentsList()
	{
		return _addedStudentsList;
	}

	public void setAddedStudentsList(List<Student> addedStudentsList)
	{
		_addedStudentsList = addedStudentsList;
	}

	public Set<Student> getMarkedStudentsList()
	{
		return _markedStudentsList;
	}

	public void setMarkedStudentsList(Set<Student> markedStudentsList)
	{
		_markedStudentsList = markedStudentsList;
	}
}
