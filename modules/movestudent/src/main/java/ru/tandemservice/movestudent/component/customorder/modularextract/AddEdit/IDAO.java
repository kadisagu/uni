/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.modularextract.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.custom.ModifyModularStuExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 28.08.2013
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<ModifyModularStuExtract, Model>
{
}