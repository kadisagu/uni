package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.movestudent.entity.TransferCourseStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О переводе с курса на следующий курс
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TransferCourseStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.TransferCourseStuExtract";
    public static final String ENTITY_NAME = "transferCourseStuExtract";
    public static final int VERSION_HASH = 1115963265;
    private static IEntityMeta ENTITY_META;

    public static final String P_OLD_GROUP_NUMBER = "oldGroupNumber";

    private String _oldGroupNumber;     // Номер группы на момент формирования приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер группы на момент формирования приказа.
     */
    @Length(max=255)
    public String getOldGroupNumber()
    {
        return _oldGroupNumber;
    }

    /**
     * @param oldGroupNumber Номер группы на момент формирования приказа.
     */
    public void setOldGroupNumber(String oldGroupNumber)
    {
        dirty(_oldGroupNumber, oldGroupNumber);
        _oldGroupNumber = oldGroupNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TransferCourseStuExtractGen)
        {
            setOldGroupNumber(((TransferCourseStuExtract)another).getOldGroupNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TransferCourseStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TransferCourseStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new TransferCourseStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "oldGroupNumber":
                    return obj.getOldGroupNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "oldGroupNumber":
                    obj.setOldGroupNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "oldGroupNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "oldGroupNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "oldGroupNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TransferCourseStuExtract> _dslPath = new Path<TransferCourseStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TransferCourseStuExtract");
    }
            

    /**
     * @return Номер группы на момент формирования приказа.
     * @see ru.tandemservice.movestudent.entity.TransferCourseStuExtract#getOldGroupNumber()
     */
    public static PropertyPath<String> oldGroupNumber()
    {
        return _dslPath.oldGroupNumber();
    }

    public static class Path<E extends TransferCourseStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<String> _oldGroupNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер группы на момент формирования приказа.
     * @see ru.tandemservice.movestudent.entity.TransferCourseStuExtract#getOldGroupNumber()
     */
        public PropertyPath<String> oldGroupNumber()
        {
            if(_oldGroupNumber == null )
                _oldGroupNumber = new PropertyPath<String>(TransferCourseStuExtractGen.P_OLD_GROUP_NUMBER, this);
            return _oldGroupNumber;
        }

        public Class getEntityClass()
        {
            return TransferCourseStuExtract.class;
        }

        public String getEntityName()
        {
            return "transferCourseStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
