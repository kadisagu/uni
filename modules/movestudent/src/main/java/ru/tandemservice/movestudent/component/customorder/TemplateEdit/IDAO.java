/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.TemplateEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Nikolay Fedorovskih
 * @since 24.09.2013
 */
public interface IDAO extends IUniDao<Model>
{
}