package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.entity.gen.RepeatPassStateExamStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О повторной сдаче государственного экзамена
 */
public class RepeatPassStateExamStuExtract extends RepeatPassStateExamStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return null;
    }
}