/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e2;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.RestorationStuExtract;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 24.12.2008
 */
public class RestorationStuExtractPrint implements IPrintFormCreator<RestorationStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, RestorationStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        CommonExtractPrint.injectFefuDevelopConditionAndTech(modifier, extract.getEducationOrgUnitNew(), extract.getGroupNew(), "fefuShortFastExtendedOptionalTextNew");
        CommonExtractPrint.addEntryDateToModifier(modifier, extract.getRestorationDate()).modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}
