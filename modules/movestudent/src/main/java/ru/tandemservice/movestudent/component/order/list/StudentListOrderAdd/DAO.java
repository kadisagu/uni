/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.list.StudentListOrderAdd;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDao;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 07.05.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        final boolean admin = UserContext.getInstance().getPrincipalContext().isAdmin();

        MQBuilder builder = new MQBuilder(StudentExtractType.ENTITY_CLASS, "t");
        builder.add(MQExpression.eq("t", StudentExtractType.L_PARENT + "." + StudentExtractType.P_CODE, StudentExtractTypeCodes.LIST_ORDER));

        if (null != model.getExtractTypeGroups())
        {
            List<Long> orderTypesId = new DQLSelectBuilder().fromEntity(StudentExtractTypeToGroup.class, "eg")
                    .column(property(StudentExtractTypeToGroup.type().parent().id().fromAlias("eg")))
                    .where(isNotNull(property(StudentExtractTypeToGroup.type().parent().fromAlias("eg"))))
                    .where(in(property(StudentExtractTypeToGroup.group().code().fromAlias("eg")), model.getExtractTypeGroups()))
                    .createStatement(getSession()).list();

            builder.add(MQExpression.in("t", StudentExtractType.id().s(), orderTypesId));
        }

        builder.add(MQExpression.eq("t", StudentExtractType.P_ACTIVE, Boolean.TRUE));

        model.setTypeList(builder.<StudentExtractType>getResultList(getSession()));

        model.setOrderTypeListModel(new FullCheckSelectModel(admin ? StudentExtractType.P_TITLE_WITH_CODE : StudentExtractType.P_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(StudentExtractType.class, "et").column("et")
                        .where(eq(property(StudentExtractType.parent().code().fromAlias("et")), value(StudentExtractTypeCodes.LIST_ORDER)))
                        .where(eq(property(StudentExtractType.active().fromAlias("et")), value(Boolean.TRUE)));

                if (admin)
                {
                    builder.where(or(
                            likeUpper(property(StudentExtractType.title().fromAlias("et")), value(CoreStringUtils.escapeLike(filter))),
                            likeUpper(property(StudentExtractType.code().fromAlias("et")), value(CoreStringUtils.escapeLike(filter)))
                    ));
                } else
                    builder.where(like(DQLFunctions.upper(property(StudentExtractType.title().fromAlias("et"))), value(CoreStringUtils.escapeLike(filter))));

                /*
                DQLSelectBuilder countBuilder = new DQLSelectBuilder()
                        .fromEntity(StudentExtractType.class, "et").column("et")
                        .where(eq(property(StudentExtractType.parent().code().fromAlias("et")), value(StudentExtractTypeCodes.LIST_ORDER)))
                        .where(eq(property(StudentExtractType.active().fromAlias("et")), value(Boolean.TRUE)));
                int extractTypesCount = countBuilder.createStatement(getSession()).list().size();
                */

                if (null != model.getExtractTypeGroups())
                {
                    DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(StudentExtractTypeToGroup.class, "eg")
                            .column(property(StudentExtractTypeToGroup.type().parent().id().fromAlias("eg")))
                            .where(isNotNull(property(StudentExtractTypeToGroup.type().parent().fromAlias("eg"))))
                            .where(eq(property(StudentExtractTypeToGroup.group().code().fromAlias("eg")), value("17")));

                    builder.where(in(property(StudentExtractType.id().fromAlias("et")), subBuilder.buildQuery()));
                }

                builder.order(property(StudentExtractType.title().fromAlias("et")));
                List<StudentExtractType> list = builder.createStatement(getSession()).list();
                return new ListResult<>(list);
            }
        });
    }
}