/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e18;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.IStudentListParagraphPrintFormatter;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.SplitStudentsStuListExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author ListExtractComponentGenerator
 * @since 30.07.2009
 */
public class SplitStudentsStuListExtractPrint implements IPrintFormCreator<SplitStudentsStuListExtract>, IListParagraphPrintFormCreator<SplitStudentsStuListExtract>, IStudentListParagraphPrintFormatter
{
    @Override
    public RtfDocument createPrintForm(byte[] template, SplitStudentsStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, SplitStudentsStuListExtract firstExtract)
    {
        EducationLevels eduLevel = EducationOrgUnitUtil.getParentLevel(firstExtract.getEntity().getGroup().getEducationOrgUnit().getEducationLevelHighSchool());
        String specialityTitle = null != eduLevel ? eduLevel.getDisplayableTitle() : "";

        modifier.put("course", firstExtract.getEntity().getGroup().getCourse().getTitle());
        modifier.put("educationOrgUnit", firstExtract.getEntity().getGroup().getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitleWithoutOrgUnit());
        modifier.put("group", firstExtract.getEntity().getGroup().getTitle());
        modifier.put("speciality", specialityTitle);
        modifier.put("usue_educationOrgUnit", firstExtract.getEntity().getGroup().getEducationOrgUnit().getEducationLevelHighSchool().getTitle());

        CommonExtractPrint.initDevelopForm(modifier, firstExtract.getEntity().getGroup().getEducationOrgUnit(), "");
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, SplitStudentsStuListExtract firstExtract)
    {
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract)
                .put("specialization", firstExtract.getEducationOrgUnitNew().getEducationLevelHighSchool().getPrintTitle())
                .put("usue_specialization", firstExtract.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle());
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, SplitStudentsStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public String formatSingleStudent(Student student, int extractNumber)
    {
        StringBuilder buffer = new StringBuilder();
        buffer.append("\\par ").append(extractNumber).append(".  ");
        buffer.append(student.getPerson().getFullFio());
        /*if(UniDefines.COMPENSATION_TYPE_CONTRACT.equals(student.getCompensationType().getCode()))
            buffer.append(" (дог.)");*/
        return buffer.toString();
    }
}