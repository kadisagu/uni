/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e29.ParagraphAddEdit;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.entity.GiveDiplExtStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.IExtEducationLevelModel;

import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 17.09.2012
 */
public class Model extends AbstractListParagraphAddEditModel<GiveDiplExtStuListExtract> implements IEducationLevelModel, IGroupModel, IExtEducationLevelModel
{
    private Course _course;
    private Group _group;
    private EducationLevelsHighSchool _educationLevelsHighSchool;
    private CompensationType _compensationType;
    private DevelopForm _developForm;
    private DevelopCondition _developCondition;
    private DevelopTech _developTech;
    private DevelopPeriod _developPeriod;
    private boolean _printDiplomaQualification;
    private ISelectModel _groupListModel;
    private ISelectModel _educationLevelsHighSchoolListModel;
    private List<CompensationType> _compensationTypeList;
    private ISelectModel _developFormModel;
    private ISelectModel _developConditionModel;
    private ISelectModel _developTechModel;
    private ISelectModel _developPeriodModel;

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public void setDevelopCondition(DevelopCondition developCondition)
    {
        _developCondition = developCondition;
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public void setDevelopTech(DevelopTech developTech)
    {
        _developTech = developTech;
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        _developPeriod = developPeriod;
    }

    public boolean isPrintDiplomaQualification()
    {
        return _printDiplomaQualification;
    }

    public void setPrintDiplomaQualification(boolean printDiplomaQualification)
    {
        _printDiplomaQualification = printDiplomaQualification;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public ISelectModel getEducationLevelsHighSchoolListModel()
    {
        return _educationLevelsHighSchoolListModel;
    }

    public void setEducationLevelsHighSchoolListModel(ISelectModel educationLevelsHighSchoolListModel)
    {
        _educationLevelsHighSchoolListModel = educationLevelsHighSchoolListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public ISelectModel getDevelopConditionModel()
    {
        return _developConditionModel;
    }

    public void setDevelopConditionModel(ISelectModel developConditionModel)
    {
        _developConditionModel = developConditionModel;
    }

    public ISelectModel getDevelopTechModel()
    {
        return _developTechModel;
    }

    public void setDevelopTechModel(ISelectModel developTechModel)
    {
        _developTechModel = developTechModel;
    }

    public ISelectModel getDevelopPeriodModel()
    {
        return _developPeriodModel;
    }

    public void setDevelopPeriodModel(ISelectModel developPeriodModel)
    {
        _developPeriodModel = developPeriodModel;
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return null;
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return null;
    }

    @Override
    public EducationLevels getParentEduLevel()
    {
        return getEducationLevelsHighSchool()!=null ? EducationOrgUnitUtil.getParentLevel(getEducationLevelsHighSchool()) : null;
    }
}
