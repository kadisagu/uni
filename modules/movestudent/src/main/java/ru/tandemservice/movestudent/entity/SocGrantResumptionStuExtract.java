package ru.tandemservice.movestudent.entity;

import java.util.Date;

import ru.tandemservice.movestudent.entity.gen.SocGrantResumptionStuExtractGen;

/**
 * Выписка из сборного приказа по студенту. О возобновлении выплаты социальной стипендии
 */
public class SocGrantResumptionStuExtract extends SocGrantResumptionStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getStartDate();
    }
}