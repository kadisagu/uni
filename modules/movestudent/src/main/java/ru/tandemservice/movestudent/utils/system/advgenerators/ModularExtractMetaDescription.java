/* $Id$ */
package ru.tandemservice.movestudent.utils.system.advgenerators;

/**
 * Описание Выписки из сборного приказа для генерации инфраструктуры
 *
 * @author Dmitry Seleznev
 * @since 10.08.2012
 */
public class ModularExtractMetaDescription
{
    private String _typeIndex; // Индекс выписки для генерации компонентов
    private String _typeCode; // Код элемента справочника типа выписки
	private String _typeConstantName; // Константа кода типа в StudentExtractTypeCodes
    private String _typeTitle; // Имя элемента справочника типа выписки и сущности
    private String _templateCode; // Код шаблона выписки
    private String _typeDescription; // Описание элемента справочника типа выписки
    private EntityMetaDescriptor _metaDescriptor; // Описание сущности выписки
    private String _fileName; // имя файла для шаблона выписки

    /**
     * Конструктор
     *
     * @param typeIndex      - Индекс выписки для генерации компонентов
     * @param typeCode       - Код элемента справочника типа выписки
     * @param typeTitle      - Имя элемента справочника типа выписки и сущности
     * @param templateCode   - Код шаблона выписки
     * @param fileName       - Описание элемента справочника типа выписки
     * @param metaDescriptor - Описание сущности выписки
     */
    public ModularExtractMetaDescription(String typeIndex, String typeCode, String typeTitle, String templateCode, String fileName, EntityMetaDescriptor metaDescriptor)
    {
        _typeIndex = typeIndex;
        _typeCode = typeCode;
        _typeTitle = typeTitle;
        _templateCode = templateCode;
        _fileName = fileName;
        _metaDescriptor = metaDescriptor;
    }

    /**
     * Конструктор
     *
     * @param typeIndex       - Индекс выписки для генерации компонентов
     * @param typeCode        - Код элемента справочника типа выписки
     * @param typeTitle       - Имя элемента справочника типа выписки и сущности
     * @param templateCode    - Код шаблона выписки
     * @param fileName        - Описание элемента справочника типа выписки
     * @param typeDescription - Описание элемента справочника типа выписки
     * @param metaDescriptor  - Описание сущности выписки
     */
    public ModularExtractMetaDescription(String typeIndex, String typeCode, String typeTitle, String templateCode, String fileName, String typeDescription, EntityMetaDescriptor metaDescriptor)
    {
        _typeIndex = typeIndex;
        _typeCode = typeCode;
        _typeTitle = typeTitle;
        _fileName = fileName;
        _typeDescription = typeDescription;
        _metaDescriptor = metaDescriptor;
    }

	/**
	 * Конструктор
	 *
	 * @param typeIndex			- Индекс выписки для генерации компонентов
	 * @param typeCode			- Код элемента справочника типа выписки
	 * @param typeConstantName	- Константа кода типа в StudentExtractTypeCodes
	 * @param typeTitle			- Имя элемента справочника типа выписки и сущности
	 * @param templateCode		- Код шаблона выписки
	 * @param fileName			- Описание элемента справочника типа выписки
	 * @param typeDescription	- Описание элемента справочника типа выписки
	 * @param metaDescriptor	- Описание сущности выписки
	 */
	public ModularExtractMetaDescription(String typeIndex, String typeCode, String typeConstantName, String typeTitle, String templateCode, String fileName, String typeDescription, EntityMetaDescriptor metaDescriptor)
	{
		_typeIndex = typeIndex;
		_typeCode = typeCode;
		_typeConstantName = typeConstantName;
		_typeTitle = typeTitle;
		_templateCode = templateCode;
		_fileName = fileName;
		_typeDescription = typeDescription;
		_metaDescriptor = metaDescriptor;
	}

    /**
     * Индекс выписки для генерации компонентов
     *
     * @return - Индекс выписки для генерации компонентов
     */
    public String getTypeIndex()
    {
        return _typeIndex;
    }

    /**
     * Код элемента справочника типа выписки
     *
     * @return - Код элемента справочника типа выписки
     */
    public String getTypeCode()
    {
        return _typeCode;
    }

	/**
	 * Константа кода типа в StudentExtractTypeCodes
	 *
	 * @return - Константа кода типа в StudentExtractTypeCodes
	 */
	public String getTypeConstantName()
	{
		return _typeConstantName;
	}

    /**
     * Имя элемента справочника типа выписки и сущности
     *
     * @return - Имя элемента справочника типа выписки и сущности
     */
    public String getTypeTitle()
    {
        return _typeTitle;
    }

    /**
     * Код шаблона выписки
     *
     * @return - Код шаблона выписки
     */
    public String getTemplateCode()
    {
        return _templateCode;
    }

    /**
     * Описание элемента справочника типа выписки
     *
     * @return - Описание элемента справочника типа выписки
     */
    public String getTypeDescription()
    {
        return _typeDescription;
    }

    /**
     * Описание сущности выписки
     *
     * @return - Описание сущности выписки
     */
    public EntityMetaDescriptor getMetaDescriptor()
    {
        return _metaDescriptor;
    }

    /**
     * имя файла для генерации шаблона выписки
     *
     * @return - имя файла для генерации шаблона выписки
     */
    public String getFileName()
    {
        return _fileName;
    }
}