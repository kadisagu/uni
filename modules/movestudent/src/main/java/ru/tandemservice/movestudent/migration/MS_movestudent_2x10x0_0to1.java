package ru.tandemservice.movestudent.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_movestudent_2x10x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность changeDiplomaWorkTopicStuListExtract

		// изменен тип свойства diplomaWorkTopicOld
		{
			// изменить тип колонки
			tool.changeColumnType("chngdplmwrktpcstlstextrct_t", "diplomaworktopicold_p", DBType.createVarchar(1024));

		}

		// изменен тип свойства diplomaWorkTopicNew
		{
			// изменить тип колонки
			tool.changeColumnType("chngdplmwrktpcstlstextrct_t", "diplomaworktopicnew_p", DBType.createVarchar(1024));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность setDiplomaWorkTopicAndScientificAdviserStuListExtract

		// изменен тип свойства diplomaWorkTopicOld
		{
			// изменить тип колонки
			tool.changeColumnType("stdplmwrktpcandscntfcadvsrst_t", "diplomaworktopicold_p", DBType.createVarchar(1024));

		}

		// изменен тип свойства diplomaWorkTopicNew
		{
			// изменить тип колонки
			tool.changeColumnType("stdplmwrktpcandscntfcadvsrst_t", "diplomaworktopicnew_p", DBType.createVarchar(1024));

		}


    }
}