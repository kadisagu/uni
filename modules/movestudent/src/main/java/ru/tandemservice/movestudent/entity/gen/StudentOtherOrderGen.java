package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.StudentOtherOrder;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Прочие приказы по студентам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentOtherOrderGen extends AbstractStudentOrder
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.StudentOtherOrder";
    public static final String ENTITY_NAME = "studentOtherOrder";
    public static final int VERSION_HASH = -670190571;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof StudentOtherOrderGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentOtherOrderGen> extends AbstractStudentOrder.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentOtherOrder.class;
        }

        public T newInstance()
        {
            return (T) new StudentOtherOrder();
        }
    }
    private static final Path<StudentOtherOrder> _dslPath = new Path<StudentOtherOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentOtherOrder");
    }
            

    public static class Path<E extends StudentOtherOrder> extends AbstractStudentOrder.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return StudentOtherOrder.class;
        }

        public String getEntityName()
        {
            return "studentOtherOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
