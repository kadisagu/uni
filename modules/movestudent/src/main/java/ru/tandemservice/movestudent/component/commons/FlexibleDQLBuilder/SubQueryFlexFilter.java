/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import java.util.Arrays;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 07.12.2013
 */
public class SubQueryFlexFilter implements IFlexFilter
{
    private IFlexConfig _config;
    private String _path;
    private String _alias;
    private String _rightColumnName;

    public SubQueryFlexFilter(String alias, String path, IFlexConfig config, String rightColumnName)
    {
        _path = path;
        _alias = alias;
        _config = config;
        _rightColumnName = rightColumnName;
    }

    @Override
    public String getAlias()
    {
        return _alias;
    }

    @Override
    public boolean addToBuilder(IDataSettings dataSettings, DQLSelectBuilder builder, IJoinCreator joinCreator)
    {
        FlexDQLBuilder flexBuilder = new FlexDQLBuilder(_config, false);
        if (flexBuilder.applyFilters(dataSettings) == 0)
            return false;

        joinCreator.checkAlias(_alias);
        IFlexColumn rightColumn = _config.getColumn(_rightColumnName);
        flexBuilder.addColumnsToBuilder(Arrays.asList(rightColumn));
        builder.where(in(property(_alias, _path), flexBuilder.getBuilder().buildQuery()));
        return true;
    }
}