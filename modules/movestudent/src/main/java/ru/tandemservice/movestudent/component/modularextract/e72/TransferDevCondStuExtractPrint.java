/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e72;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.TransferDevCondStuExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author ModularStudentExtractComponentGenerator
 * @since 29.08.2012
 */
public class TransferDevCondStuExtractPrint implements IPrintFormCreator<TransferDevCondStuExtract>
{
    private static final Map<String, String[]> DEVELOP_CONDITION_MALE_SEX_CASES_ARRAY = new HashMap<>();
    private static final Map<String, String[]> DEVELOP_CONDITION_FEMALE_SEX_CASES_ARRAY = new HashMap<>();
    private static final List<String> SPEC_QUALIFICATION_CODES = new ArrayList<>();

    static
    {
        SPEC_QUALIFICATION_CODES.add(QualificationsCodes.SPETSIALIST);
        SPEC_QUALIFICATION_CODES.add(QualificationsCodes.BAZOVYY_UROVEN_S_P_O);
        SPEC_QUALIFICATION_CODES.add(QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O);
        DEVELOP_CONDITION_MALE_SEX_CASES_ARRAY.put(UniDefines.DEVELOP_CONDITION_FULL_TIME, new String[]{"полный", "полного", "полному", "полный", "полным", "полном"});
        DEVELOP_CONDITION_MALE_SEX_CASES_ARRAY.put(UniDefines.DEVELOP_CONDITION_SHORT, new String[]{"сокращенный", "сокращенного", "сокращенному", "сокращенный", "сокращенным", "сокращенном"});
        DEVELOP_CONDITION_MALE_SEX_CASES_ARRAY.put(UniDefines.DEVELOP_CONDITION_FAST, new String[]{"ускоренный", "ускоренного", "ускоренному", "ускоренный", "ускоренным", "ускоренном"});
        DEVELOP_CONDITION_MALE_SEX_CASES_ARRAY.put(UniDefines.DEVELOP_CONDITION_SHORT_FAST, new String[]{"сокращенный ускоренный", "сокращенного ускоренного", "сокращенному ускоренному", "сокращенный ускоренный", "сокращенным ускоренным", "сокращенном ускоренном"});
        DEVELOP_CONDITION_FEMALE_SEX_CASES_ARRAY.put(UniDefines.DEVELOP_CONDITION_FULL_TIME, new String[]{"полная", "полной", "полной", "полную", "полной", "полной"});
        DEVELOP_CONDITION_FEMALE_SEX_CASES_ARRAY.put(UniDefines.DEVELOP_CONDITION_SHORT, new String[]{"сокращенная", "сокращенной", "сокращенной", "сокращенную", "сокращенной", "сокращенной"});
        DEVELOP_CONDITION_FEMALE_SEX_CASES_ARRAY.put(UniDefines.DEVELOP_CONDITION_FAST, new String[]{"ускоренная", "ускоренной", "ускоренной", "ускоренную", "ускоренной", "ускоренной"});
        DEVELOP_CONDITION_FEMALE_SEX_CASES_ARRAY.put(UniDefines.DEVELOP_CONDITION_SHORT_FAST, new String[]{"сокращенная ускоренная", "сокращенной ускоренной", "сокращенной ускоренной", "сокращенную ускоренную", "сокращенной ускоренной", "сокращенной ускоренной"});
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, TransferDevCondStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());

        if (null != qualification && SPEC_QUALIFICATION_CODES.contains(qualification.getCode()))
        {
            modifier.put("eduLevelTextAlt", "вышеуказанной специальности");
        } else
        {
            modifier.put("eduLevelTextAlt", "вышеуказанному направлению");
        }

        CommonExtractPrint.injectFefuEducationLevelNewText(modifier, "fefuEduLevelText", extract.getEducationOrgUnitOld(), extract.getEducationOrgUnitNew());

        for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
        {
            String casePostfix = UniRtfUtil.CASE_POSTFIX.get(i);
            String[] oldMaleCases = DEVELOP_CONDITION_MALE_SEX_CASES_ARRAY.get(extract.getEducationOrgUnitOld().getDevelopCondition().getCode());
            String[] oldFemaleCases = DEVELOP_CONDITION_FEMALE_SEX_CASES_ARRAY.get(extract.getEducationOrgUnitOld().getDevelopCondition().getCode());
            String[] newMaleCases = DEVELOP_CONDITION_MALE_SEX_CASES_ARRAY.get(extract.getEducationOrgUnitNew().getDevelopCondition().getCode());
            String[] newFemaleCases = DEVELOP_CONDITION_FEMALE_SEX_CASES_ARRAY.get(extract.getEducationOrgUnitNew().getDevelopCondition().getCode());
            modifier.put("developConditionOld" + casePostfix, (null != oldMaleCases ? oldMaleCases[i] : ""));
            modifier.put("developConditionOld" + casePostfix + "F", (null != oldFemaleCases ? oldFemaleCases[i] : ""));
            modifier.put("developConditionNew" + casePostfix, (null != newMaleCases ? newMaleCases[i] : ""));
            modifier.put("developConditionNew" + casePostfix + "F", (null != newFemaleCases ? newFemaleCases[i] : ""));
        }

        modifier.put("plannedGraduateYear", String.valueOf(extract.getPlannedGraduateYear()));
        modifier.put("developPeriodNew", extract.getEducationOrgUnitNew().getDevelopPeriod().getTitle());

        if (extract.getEducationOrgUnitNew().getEducationLevelHighSchool().getEducationLevel().getLevelType().isHigh())
        {
            modifier.put("eduLevelType", "высшее");
            modifier.put("eduLevelType_G", "высшего");
            modifier.put("eduLevelType_D", "высшему");
            modifier.put("eduLevelType_A", "высшее");
            modifier.put("eduLevelType_I", "высшим");
            modifier.put("eduLevelType_P", "высшем");
        } else
        {
            modifier.put("eduLevelType", "среднее");
            modifier.put("eduLevelType_G", "среднего");
            modifier.put("eduLevelType_D", "среднему");
            modifier.put("eduLevelType_A", "среднее");
            modifier.put("eduLevelType_I", "средним");
            modifier.put("eduLevelType_P", "среднем");
        }

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}