/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.TemplateEdit;

import org.apache.commons.io.IOUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.movestudent.component.customorder.ICustomPrintFormContainer;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import ru.tandemservice.movestudent.entity.custom.CustomOrderPrintForm;
import ru.tandemservice.movestudent.entity.custom.CustomParagraphPrintForm;
import ru.tandemservice.uni.dao.UniDao;

import java.io.IOException;

/**
 * @author Nikolay Fedorovskih
 * @since 24.09.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        IEntity object = getNotNull(model.getObjectId());
        ICustomPrintFormContainer container;
        if (object instanceof ICustomPrintFormContainer)
        {
            container = (ICustomPrintFormContainer) object;
        }
        else if (object instanceof AbstractStudentParagraph)
        {
            CustomParagraphPrintForm rel = get(CustomParagraphPrintForm.class, CustomParagraphPrintForm.L_PARAGRAPH, object);
            if (rel == null)
            {
                rel = new CustomParagraphPrintForm();
                rel.setParagraph((AbstractStudentParagraph) object);
            }
            container = rel;
        }
        else if (object instanceof AbstractStudentOrder)
        {
            CustomOrderPrintForm rel = get(CustomOrderPrintForm.class, CustomOrderPrintForm.L_ORDER, object);
            if (rel == null)
            {
                rel = new CustomOrderPrintForm();
                rel.setOrder((AbstractStudentOrder) object);
            }
            container = rel;
        }
        else
            throw new RuntimeException("Parameter " + Model.OBJECT_ID_BIND + " must contains id of special customizable object.");
        model.setUserTemplateContainer(container);
        model.setModifiableOrderId(container.getModifiableOrderId());
    }

    @Override
    public void update(Model model)
    {
        byte[] userFile;
        try
        {
            userFile = IOUtils.toByteArray(model.getUploadFile().getStream());
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        try
        {
            // проверим на валидность rtf-файл
            new RtfReader().read(userFile);
        }
        catch (Throwable e)
        {
            throw new ApplicationException("Файл поврежден или не является RTF-документом.");
        }


        DatabaseFile file = model.getUserTemplateContainer().getFile();
        if (file == null)
            file = new DatabaseFile();

        file.setContent(userFile);
        saveOrUpdate(file);

        model.getUserTemplateContainer().setFile(file);
        saveOrUpdate(model.getUserTemplateContainer());
    }
}