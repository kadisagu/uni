/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.modularextract.e73.AddEdit;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsExtStuExtract;
import ru.tandemservice.movestudent.utils.system.StudentExcludeOrderData;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Arrays;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 29.08.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<RestorationAdmitStateExamsExtStuExtract, Model> implements IDAO
{
    @Override
    protected RestorationAdmitStateExamsExtStuExtract createNewInstance()
    {
        return new RestorationAdmitStateExamsExtStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setEduModel(MoveStudentDaoFacade.getCommonExtractUtil().createEduModel(model.isAddForm(), model.getExtract()));
        model.setStudentStatusNewStr(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE).getTitle());

        if (model.isAddForm())
        {
            model.getExtract().setDismissedFrom(TopOrgUnit.getInstance().getShortTitle());

            StudentExcludeOrderData data = MoveStudentDaoFacade.getMoveStudentDao().getStudentExcludeOrderData(model.getExtract().getEntity());
            if (data != null)
            {
                model.getExtract().setDismissOrder(data.getDismissOrderNum());
                model.getExtract().setDismissOrderDate(data.getDismissOrderDate());
                model.getExtract().setDismissDate(data.getDismissDate());
            }
        }

        Student student = model.getExtract().getEntity();
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
        CommonExtractModel eduModel = model.getEduModel();

        eduModel.setFormativeOrgUnit(educationOrgUnit.getFormativeOrgUnit());
        eduModel.setTerritorialOrgUnit(educationOrgUnit.getTerritorialOrgUnit());
        eduModel.setEducationLevelsHighSchool(educationOrgUnit.getEducationLevelHighSchool());
        eduModel.setDevelopForm(educationOrgUnit.getDevelopForm());
        eduModel.setDevelopCondition(educationOrgUnit.getDevelopCondition());
        eduModel.setDevelopTech(educationOrgUnit.getDevelopTech());
        eduModel.setDevelopPeriod(educationOrgUnit.getDevelopPeriod());

        eduModel.setCourse(student.getCourse());
        eduModel.setGroup(student.getGroup());

        model.setSeasons(Arrays.asList((ApplicationRuntime.getProperty("seasonTerm_A")).split(";")).subList(0, 2));
        model.setSeason(model.getSeasons().contains(model.getExtract().getSeason()) ? model.getExtract().getSeason() : null);
    }

    @Override
    public void update(Model model)
    {
        //save rollback data
        MoveStudentDaoFacade.getCommonExtractUtil().update(model.getEduModel(), model.getExtract());
        model.getExtract().setSeason(model.getSeason());
        super.update(model);
    }
}