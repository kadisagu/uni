/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.order.modular.ModularStudentExtractPrint;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;

import ru.tandemservice.movestudent.entity.ModularStudentExtract;

/**
 * @author vip_delete
 * @since 24.10.2008
 */
@State({
        @Bind(key = "extractId", binding = "extractId"),
        @Bind(key = "printPdf", binding = "printPdf")
})
public class Model
{
    private Long _extractId;
    private ModularStudentExtract _extract;
    private byte[] _data;
    private Boolean _printPdf;

    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        _extractId = extractId;
    }

    public ModularStudentExtract getExtract()
    {
        return _extract;
    }

    public void setExtract(ModularStudentExtract extract)
    {
        _extract = extract;
    }

    public byte[] getData()
    {
        return _data;
    }

    public void setData(byte[] data)
    {
        _data = data;
    }

    public Boolean getPrintPdf()
    {
        return _printPdf;
    }

    public void setPrintPdf(Boolean printPdf)
    {
        _printPdf = printPdf;
    }
}
