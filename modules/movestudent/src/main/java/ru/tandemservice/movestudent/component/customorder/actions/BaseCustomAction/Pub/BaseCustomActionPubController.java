/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.unimove.entity.ICustomOrderAction;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public abstract class BaseCustomActionPubController<T extends ICustomOrderAction, IDAO extends IBaseCustomActionPubDAO<T, Model>, Model extends BaseCustomActionPubModel<T>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
    }
}