package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.RepeatPassStateExamStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О повторной сдаче государственного экзамена
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepeatPassStateExamStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.RepeatPassStateExamStuExtract";
    public static final String ENTITY_NAME = "repeatPassStateExamStuExtract";
    public static final int VERSION_HASH = 2098938577;
    private static IEntityMeta ENTITY_META;

    public static final String P_EXAM_TITLE = "examTitle";

    private String _examTitle;     // Название государственного экзамена

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название государственного экзамена. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getExamTitle()
    {
        return _examTitle;
    }

    /**
     * @param examTitle Название государственного экзамена. Свойство не может быть null.
     */
    public void setExamTitle(String examTitle)
    {
        dirty(_examTitle, examTitle);
        _examTitle = examTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RepeatPassStateExamStuExtractGen)
        {
            setExamTitle(((RepeatPassStateExamStuExtract)another).getExamTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepeatPassStateExamStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepeatPassStateExamStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new RepeatPassStateExamStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "examTitle":
                    return obj.getExamTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "examTitle":
                    obj.setExamTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "examTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "examTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "examTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepeatPassStateExamStuExtract> _dslPath = new Path<RepeatPassStateExamStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepeatPassStateExamStuExtract");
    }
            

    /**
     * @return Название государственного экзамена. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RepeatPassStateExamStuExtract#getExamTitle()
     */
    public static PropertyPath<String> examTitle()
    {
        return _dslPath.examTitle();
    }

    public static class Path<E extends RepeatPassStateExamStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<String> _examTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название государственного экзамена. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.RepeatPassStateExamStuExtract#getExamTitle()
     */
        public PropertyPath<String> examTitle()
        {
            if(_examTitle == null )
                _examTitle = new PropertyPath<String>(RepeatPassStateExamStuExtractGen.P_EXAM_TITLE, this);
            return _examTitle;
        }

        public Class getEntityClass()
        {
            return RepeatPassStateExamStuExtract.class;
        }

        public String getEntityName()
        {
            return "repeatPassStateExamStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
