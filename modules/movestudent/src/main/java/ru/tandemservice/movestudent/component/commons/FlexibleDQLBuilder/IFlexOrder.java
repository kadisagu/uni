/* $Id$ */
package ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder;

import org.tandemframework.core.entity.OrderDirection;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 02.12.2013
 */
public interface IFlexOrder
{
    String getName();

    String getAlias();

    String getPath();

    OrderDirection getDefaultDirection();

    List<IFlexOrder> getSubOrders();

    IFlexOrder addSubOrders(IFlexOrder... orders);
}