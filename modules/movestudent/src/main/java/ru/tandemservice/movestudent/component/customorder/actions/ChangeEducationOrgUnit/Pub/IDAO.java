/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeEducationOrgUnit.Pub;

import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.Pub.IBaseCustomActionPubDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeEducationOrgUnitAction;

/**
 * @author Nikolay Fedorovskih
 * @since 05.09.2013
 */
public interface IDAO extends IBaseCustomActionPubDAO<ChangeEducationOrgUnitAction, Model>
{
}