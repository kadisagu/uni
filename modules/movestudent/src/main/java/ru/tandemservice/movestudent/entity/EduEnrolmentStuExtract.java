package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IEnrollmentExtract;
import ru.tandemservice.movestudent.entity.gen.EduEnrolmentStuExtractGen;

import java.util.Date;

public class EduEnrolmentStuExtract extends EduEnrolmentStuExtractGen implements IEnrollmentExtract
{
    @Override
    public Date getEntranceDate()
    {
        return getEntryDate();
    }
}