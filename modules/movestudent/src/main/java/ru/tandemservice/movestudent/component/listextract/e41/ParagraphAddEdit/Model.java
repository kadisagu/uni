/* $Id: Model.java.vm 7834 2009-05-06 09:39:03Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e41.ParagraphAddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.component.listextract.e41.utils.EmployeePostVO;
import ru.tandemservice.movestudent.entity.SendPracticeInnerStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Date;
import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 13.02.2013
 */
public class Model extends AbstractListParagraphAddEditModel<SendPracticeInnerStuListExtract> implements IGroupModel
{
    private Course _course;
    private Group _group;
    private CompensationType _compensationType;
    private String _practiceType;
    private String _practiceKind;
    private String _practiceDuration;
    private Date _practiceBeginDate;
    private Date _practiceEndDate;
    private Date _attestationDate;
    private boolean _outClassTime = false;
    private boolean _doneEduPlan = false;
    private boolean _donePractice = false;
    private boolean _provideFundsAccordingToEstimates = false;
    private String _estimateNum;
    private Course _practiceCourse;
    private EmployeePostVO _preventAccidentsIC;
    private String _preventAccidentsICStr;
    private EmployeePostVO _responsForRecieveCash;
    private String _responsForRecieveCashStr;

    private ISelectModel _groupListModel;
    private List<CompensationType> _compensationTypeList;
    private List<String> _practiceTypeList;
    private ISelectModel _practiceKindModel;
    private ISelectModel _employeePostModel;
    private ISelectModel _orgUnitModel;

    public List<String> getPracticeTypeList()
    {
        return _practiceTypeList;
    }

    public void setPracticeTypeList(List<String> practiceTypeList)
    {
        _practiceTypeList = practiceTypeList;
    }

    public String getPracticeType()
    {
        return _practiceType;
    }

    public void setPracticeType(String _practiceType)
    {
        this._practiceType = _practiceType;
    }

    //Calculate method
    public String getPracticeOrgUnitId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "practiceOrgUnitId_" + dataSource.getCurrentEntity().getId();
    }

    public String getPracticeOrgUnitStrId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "practiceOrgUnitStrId_" + dataSource.getCurrentEntity().getId();
    }

    //Calculate method
    public String getPracticeHeaderId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "practiceHeaderId_" + dataSource.getCurrentEntity().getId();
    }

    //Calculate method
    public String getPracticeHeaderStrId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "practiceHeaderStrId_" + dataSource.getCurrentEntity().getId();
    }

    public boolean getStudentDisabled()
    {
        Boolean disabled = (Boolean) getDataSource().getCurrentValueEntity().getProperty(AbstractListParagraphAddEditModel.P_STUDENT_DISABLED);
        return null != disabled ? disabled : false ;
    }

    public Course getCourse()
    {
        return _course;

    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public String getPracticeKind()
    {
        return _practiceKind;
    }

    public void setPracticeKind(String practiceKind)
    {
        _practiceKind = practiceKind;
    }

    public String getPracticeDuration()
    {
        return _practiceDuration;
    }

    public void setPracticeDuration(String practiceDuration)
    {
        _practiceDuration = practiceDuration;
    }

    public Date getPracticeBeginDate()
    {
        return _practiceBeginDate;
    }

    public void setPracticeBeginDate(Date practiceBeginDate)
    {
        _practiceBeginDate = practiceBeginDate;
    }

    public Date getPracticeEndDate()
    {
        return _practiceEndDate;
    }

    public void setPracticeEndDate(Date practiceEndDate)
    {
        _practiceEndDate = practiceEndDate;
    }

    public Date getAttestationDate()
    {
        return _attestationDate;
    }

    public void setAttestationDate(Date attestationDate)
    {
        _attestationDate = attestationDate;
    }

    public boolean isOutClassTime()
    {
        return _outClassTime;
    }

    public void setOutClassTime(boolean outClassTime)
    {
        _outClassTime = outClassTime;
    }

    public boolean isDoneEduPlan()
    {
        return _doneEduPlan;
    }

    public void setDoneEduPlan(boolean doneEduPlan)
    {
        _doneEduPlan = doneEduPlan;
    }

    public Course getPracticeCourse()
    {
        return _practiceCourse;
    }

    public void setPracticeCourse(Course practiceCourse)
    {
        _practiceCourse = practiceCourse;
    }

    public EmployeePostVO getPreventAccidentsIC()
    {
        return _preventAccidentsIC;
    }

    public void setPreventAccidentsIC(EmployeePostVO preventAccidentsIC)
    {
        _preventAccidentsIC = preventAccidentsIC;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public ISelectModel getPracticeKindModel()
    {
        return _practiceKindModel;
    }

    public void setPracticeKindModel(ISelectModel practiceKindModel)
    {
        _practiceKindModel = practiceKindModel;
    }

    public ISelectModel getEmployeePostModel()
    {
        return _employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        _employeePostModel = employeePostModel;
    }

    public ISelectModel getOrgUnitModel()
    {
        return _orgUnitModel;
    }

    public void setOrgUnitModel(ISelectModel orgUnitModel)
    {
        _orgUnitModel = orgUnitModel;
    }

    public boolean isDonePractice()
    {
        return _donePractice;
    }

    public void setDonePractice(boolean donePractice)
    {
        _donePractice = donePractice;
    }

    public String getPreventAccidentsICStr()
    {
        return _preventAccidentsICStr;
    }

    public void setPreventAccidentsICStr(String preventAccidentsICStr)
    {
        _preventAccidentsICStr = preventAccidentsICStr;
    }

    public boolean getProvideFundsAccordingToEstimates()
    {
        return _provideFundsAccordingToEstimates;
    }

    public void setProvideFundsAccordingToEstimates(boolean provideFundsAccordingToEstimates)
    {
        _provideFundsAccordingToEstimates = provideFundsAccordingToEstimates;
    }

    public EmployeePostVO getResponsForRecieveCash()
    {
        return _responsForRecieveCash;
    }

    public void setResponsForRecieveCash(EmployeePostVO responsForRecieveCash)
    {
        _responsForRecieveCash = responsForRecieveCash;
    }

    public String getResponsForRecieveCashStr()
    {
        return _responsForRecieveCashStr;
    }

    public void setResponsForRecieveCashStr(String responsForRecieveCashStr)
    {
        _responsForRecieveCashStr = responsForRecieveCashStr;
    }

    public String getEstimateNum()
    {
        return _estimateNum;
    }

    public void setEstimateNum(String estimateNum)
    {
        _estimateNum = estimateNum;
    }
}
