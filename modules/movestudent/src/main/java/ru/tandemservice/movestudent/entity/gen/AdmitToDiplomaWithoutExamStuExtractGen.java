package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AdmitToDiplomaWithoutExamStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О допуске к защите выпускной квалификационной работы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AdmitToDiplomaWithoutExamStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.AdmitToDiplomaWithoutExamStuExtract";
    public static final String ENTITY_NAME = "admitToDiplomaWithoutExamStuExtract";
    public static final int VERSION_HASH = -1139811395;
    private static IEntityMeta ENTITY_META;

    public static final String P_NOT_NEED_ADMISSION_TO_G_I_A = "notNeedAdmissionToGIA";

    private boolean _notNeedAdmissionToGIA = false;     // Без допуска к ГИА

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Без допуска к ГИА. Свойство не может быть null.
     */
    @NotNull
    public boolean isNotNeedAdmissionToGIA()
    {
        return _notNeedAdmissionToGIA;
    }

    /**
     * @param notNeedAdmissionToGIA Без допуска к ГИА. Свойство не может быть null.
     */
    public void setNotNeedAdmissionToGIA(boolean notNeedAdmissionToGIA)
    {
        dirty(_notNeedAdmissionToGIA, notNeedAdmissionToGIA);
        _notNeedAdmissionToGIA = notNeedAdmissionToGIA;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof AdmitToDiplomaWithoutExamStuExtractGen)
        {
            setNotNeedAdmissionToGIA(((AdmitToDiplomaWithoutExamStuExtract)another).isNotNeedAdmissionToGIA());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AdmitToDiplomaWithoutExamStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AdmitToDiplomaWithoutExamStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new AdmitToDiplomaWithoutExamStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "notNeedAdmissionToGIA":
                    return obj.isNotNeedAdmissionToGIA();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "notNeedAdmissionToGIA":
                    obj.setNotNeedAdmissionToGIA((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "notNeedAdmissionToGIA":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "notNeedAdmissionToGIA":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "notNeedAdmissionToGIA":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AdmitToDiplomaWithoutExamStuExtract> _dslPath = new Path<AdmitToDiplomaWithoutExamStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AdmitToDiplomaWithoutExamStuExtract");
    }
            

    /**
     * @return Без допуска к ГИА. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToDiplomaWithoutExamStuExtract#isNotNeedAdmissionToGIA()
     */
    public static PropertyPath<Boolean> notNeedAdmissionToGIA()
    {
        return _dslPath.notNeedAdmissionToGIA();
    }

    public static class Path<E extends AdmitToDiplomaWithoutExamStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Boolean> _notNeedAdmissionToGIA;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Без допуска к ГИА. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AdmitToDiplomaWithoutExamStuExtract#isNotNeedAdmissionToGIA()
     */
        public PropertyPath<Boolean> notNeedAdmissionToGIA()
        {
            if(_notNeedAdmissionToGIA == null )
                _notNeedAdmissionToGIA = new PropertyPath<Boolean>(AdmitToDiplomaWithoutExamStuExtractGen.P_NOT_NEED_ADMISSION_TO_G_I_A, this);
            return _notNeedAdmissionToGIA;
        }

        public Class getEntityClass()
        {
            return AdmitToDiplomaWithoutExamStuExtract.class;
        }

        public String getEntityName()
        {
            return "admitToDiplomaWithoutExamStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
