/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e32.ParagraphAddEdit;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.movestudent.utils.SessionPartModel;
import ru.tandemservice.uni.dao.student.IAcademicGrantSizeDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.GroupCaptainStudent;

import java.util.*;

/**
 * @author ListExtractComponentGenerator
 * @since 01.10.2012
 */
public class DAO extends AbstractListParagraphAddEditDAO<AcadGrantAssignStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));
        model.setPartModel(new SessionPartModel());

        AcadGrantAssignStuListExtract extract = model.getFirstExtract();

        if (model.isParagraphOnlyOneInTheOrder() && !model.isEditForm())
        {
            model.setCompensationType(get(CompensationType.class, CompensationType.P_CODE, CompensationTypeCodes.COMPENSATION_TYPE_BUDGET));
        }

        if (!model.isParagraphOnlyOneInTheOrder())
        {
            model.setCompensationType(extract.getCompensationType());
            model.setCompensationTypeDisabled(true);
            model.setSessionType(extract.getSessionType());
            model.setSessionTypeDisabled(true);
        }

        if (model.isEditForm())
        {
            model.setCourse(extract.getCourse());
            model.setGroup(extract.getGroup());
            model.setCompensationType(extract.getCompensationType());
            model.setBeginDate(extract.getBeginDate());
            model.setEndDate(extract.getEndDate());
            model.setSessionType(extract.getSessionType());
        }
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        if (model.getCompensationType() != null)
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));
    }

    @Override
    protected AcadGrantAssignStuListExtract createNewInstance(Model model)
    {
        return new AcadGrantAssignStuListExtract();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void fillExtract(AcadGrantAssignStuListExtract extract, Student student, Model model)
    {
        IValueMapHolder<Number> grantSizeRubleHolder = (IValueMapHolder<Number>) model.getDataSource().getColumn("grantSizeRuble");
        Map<Long, Number> grantSizeRubleMap = (null == grantSizeRubleHolder ? Collections.<Long, Number>emptyMap() : grantSizeRubleHolder.getValueMap());
        Number grantSizeRuble = grantSizeRubleMap.get(student.getId());

        IValueMapHolder<Number> groupManagerBonusSizeRubleHolder = (IValueMapHolder<Number>) model.getDataSource().getColumn("groupManagerBonusSizeRuble");
        Map<Long, Number> groupManagerBonusSizeRubleMap = (null == groupManagerBonusSizeRubleHolder ? Collections.<Long, Number>emptyMap() : groupManagerBonusSizeRubleHolder.getValueMap());
        Number groupManagerBonusSizeRuble = groupManagerBonusSizeRubleMap.get(student.getId());

        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setCompensationType(model.getCompensationType());
        extract.setBeginDate(model.getBeginDate());
        extract.setEndDate(model.getEndDate());
        extract.setSessionType(model.getSessionType());
        extract.setGrantSizeRuble(grantSizeRuble != null ? grantSizeRuble.doubleValue() : null);
        extract.setGroupManagerBonusSizeRuble(groupManagerBonusSizeRuble != null ? groupManagerBonusSizeRuble.doubleValue() : null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(Model model)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        IValueMapHolder<String> grantSizeRubleHolder = (IValueMapHolder<String>) model.getDataSource().getColumn("grantSizeRuble");
        Map<Long, String> grantSizeRubleMap = (null == grantSizeRubleHolder ? Collections.<Long, String>emptyMap() : grantSizeRubleHolder.getValueMap());

        List<ViewWrapper<Student>> viewList = new ArrayList<ViewWrapper<Student>>((Collection) ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).getSelectedObjects());

        for (ViewWrapper<Student> wrapper : viewList)
        {
            if (grantSizeRubleMap.get(wrapper.getId()) == null)
                errorCollector.add("Поле «Размер стипендии, руб» обязательно для заполнения.", "grantSizeRubleId_" + wrapper.getId());
        }

        if (!model.getBeginDate().before(model.getEndDate()))
            errorCollector.add("Дата окончания должна быть больше даты начала", "beginDate", "endDate");

        if (errorCollector.hasErrors())
            return;

        super.update(model);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareGroupManagerList(Model model)
    {
        DynamicListDataSource dataSource = model.getDataSource();
        List<ViewWrapper<Student>> studentList = dataSource.getEntityList();
        List<Long> studentIds = new ArrayList<>();
        for (ViewWrapper wrapper : studentList)
        {
            studentIds.add(wrapper.getId());
        }

        List<Long> ids = new DQLSelectBuilder()
                .fromEntity(GroupCaptainStudent.class, "g")
                .column(DQLExpressions.property(GroupCaptainStudent.student().id().fromAlias("g")))
                .where(DQLExpressions.in(DQLExpressions.property(GroupCaptainStudent.student().id().fromAlias("g")), studentIds))
                .createStatement(getSession())
                .list();

        model.setGroupManagerIds(ids);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void fillGrantSizeDefaultValues(Model model)
    {
        DynamicListDataSource dataSource = model.getDataSource();
        List<ViewWrapper<Student>> studentList = dataSource.getEntityList();

        IValueMapHolder grantSizeRubleHolder = (IValueMapHolder) model.getDataSource().getColumn("grantSizeRuble");
        Map<Long, Double> grantSizeRubleMap = (null == grantSizeRubleHolder ? Collections.emptyMap() : grantSizeRubleHolder.getValueMap());

        IValueMapHolder groupManagerBonusSizeRubleHolder = (IValueMapHolder) model.getDataSource().getColumn("groupManagerBonusSizeRuble");
        Map<Long, Double> groupManagerBonusSizeRubleMap = (null == groupManagerBonusSizeRubleHolder ? Collections.emptyMap() : groupManagerBonusSizeRubleHolder.getValueMap());

        List<Long> groupManagerIds = model.getGroupManagerIds();

        Integer defaultGrantSize = IAcademicGrantSizeDAO.instance.get().getDefaultGrantSize();
        Integer defaultGroupManagerBonusSize = IAcademicGrantSizeDAO.instance.get().getDefaultGroupManagerBonusSize();

        for (ViewWrapper<Student> wrapper : studentList)
        {
            Long id = wrapper.getId();
            if (grantSizeRubleMap.get(id) == null)
            grantSizeRubleMap.put(id, defaultGrantSize != null ? defaultGrantSize.doubleValue() : null);
            if (groupManagerIds.contains(id) && groupManagerBonusSizeRubleMap.get(id) == null)
            {
                groupManagerBonusSizeRubleMap.put(id, defaultGroupManagerBonusSize != null ? defaultGroupManagerBonusSize.doubleValue() : null);
            }
        }
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        super.prepareListDataSource(model);
        prepareGroupManagerList(model);
        if (model.isFillDefaults())
        {
            fillGrantSizeDefaultValues(model);
        }
        model.setFillDefaults(true);
    }
}