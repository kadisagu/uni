package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сборный приказ по студентам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentModularOrderGen extends AbstractStudentOrder
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.StudentModularOrder";
    public static final String ENTITY_NAME = "studentModularOrder";
    public static final int VERSION_HASH = 2077661659;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof StudentModularOrderGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentModularOrderGen> extends AbstractStudentOrder.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentModularOrder.class;
        }

        public T newInstance()
        {
            return (T) new StudentModularOrder();
        }
    }
    private static final Path<StudentModularOrder> _dslPath = new Path<StudentModularOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentModularOrder");
    }
            

    public static class Path<E extends StudentModularOrder> extends AbstractStudentOrder.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return StudentModularOrder.class;
        }

        public String getEntityName()
        {
            return "studentModularOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
