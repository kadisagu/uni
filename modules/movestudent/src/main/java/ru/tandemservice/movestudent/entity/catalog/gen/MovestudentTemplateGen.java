package ru.tandemservice.movestudent.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Шаблон выписки из сборного приказа (приказы по студентам)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MovestudentTemplateGen extends EntityBase
 implements INaturalIdentifiable<MovestudentTemplateGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate";
    public static final String ENTITY_NAME = "movestudentTemplate";
    public static final int VERSION_HASH = -511431109;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_TYPE = "type";
    public static final String P_INDEX = "index";
    public static final String P_PATH = "path";
    public static final String P_DOCUMENT = "document";
    public static final String P_EDIT_DATE = "editDate";
    public static final String P_COMMENT = "comment";
    public static final String P_TITLE = "title";
    public static final String P_TITLE_WITH_CODE = "titleWithCode";

    private String _code;     // Системный код
    private StudentExtractType _type;     // Тип выписки по студенту
    private int _index;     // Индекс шаблона внутри типа
    private String _path;     // Путь в classpath до шаблона по умолчанию
    private byte[] _document;     // Шаблон в zip текущий
    private Date _editDate;     // Дата редактирования
    private String _comment;     // комментарий
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Тип выписки по студенту.
     */
    public StudentExtractType getType()
    {
        return _type;
    }

    /**
     * @param type Тип выписки по студенту.
     */
    public void setType(StudentExtractType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Индекс шаблона внутри типа. Свойство не может быть null.
     */
    @NotNull
    public int getIndex()
    {
        return _index;
    }

    /**
     * @param index Индекс шаблона внутри типа. Свойство не может быть null.
     */
    public void setIndex(int index)
    {
        dirty(_index, index);
        _index = index;
    }

    /**
     * @return Путь в classpath до шаблона по умолчанию. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getPath()
    {
        return _path;
    }

    /**
     * @param path Путь в classpath до шаблона по умолчанию. Свойство не может быть null и должно быть уникальным.
     */
    public void setPath(String path)
    {
        dirty(_path, path);
        _path = path;
    }

    /**
     * @return Шаблон в zip текущий.
     */
    public byte[] getDocument()
    {
        return _document;
    }

    /**
     * @param document Шаблон в zip текущий.
     */
    public void setDocument(byte[] document)
    {
        dirty(_document, document);
        _document = document;
    }

    /**
     * @return Дата редактирования.
     */
    public Date getEditDate()
    {
        return _editDate;
    }

    /**
     * @param editDate Дата редактирования.
     */
    public void setEditDate(Date editDate)
    {
        dirty(_editDate, editDate);
        _editDate = editDate;
    }

    /**
     * @return комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MovestudentTemplateGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((MovestudentTemplate)another).getCode());
            }
            setType(((MovestudentTemplate)another).getType());
            setIndex(((MovestudentTemplate)another).getIndex());
            setPath(((MovestudentTemplate)another).getPath());
            setDocument(((MovestudentTemplate)another).getDocument());
            setEditDate(((MovestudentTemplate)another).getEditDate());
            setComment(((MovestudentTemplate)another).getComment());
            setTitle(((MovestudentTemplate)another).getTitle());
        }
    }

    public INaturalId<MovestudentTemplateGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<MovestudentTemplateGen>
    {
        private static final String PROXY_NAME = "MovestudentTemplateNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof MovestudentTemplateGen.NaturalId) ) return false;

            MovestudentTemplateGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MovestudentTemplateGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MovestudentTemplate.class;
        }

        public T newInstance()
        {
            return (T) new MovestudentTemplate();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "type":
                    return obj.getType();
                case "index":
                    return obj.getIndex();
                case "path":
                    return obj.getPath();
                case "document":
                    return obj.getDocument();
                case "editDate":
                    return obj.getEditDate();
                case "comment":
                    return obj.getComment();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "type":
                    obj.setType((StudentExtractType) value);
                    return;
                case "index":
                    obj.setIndex((Integer) value);
                    return;
                case "path":
                    obj.setPath((String) value);
                    return;
                case "document":
                    obj.setDocument((byte[]) value);
                    return;
                case "editDate":
                    obj.setEditDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "type":
                        return true;
                case "index":
                        return true;
                case "path":
                        return true;
                case "document":
                        return true;
                case "editDate":
                        return true;
                case "comment":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "type":
                    return true;
                case "index":
                    return true;
                case "path":
                    return true;
                case "document":
                    return true;
                case "editDate":
                    return true;
                case "comment":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "type":
                    return StudentExtractType.class;
                case "index":
                    return Integer.class;
                case "path":
                    return String.class;
                case "document":
                    return byte[].class;
                case "editDate":
                    return Date.class;
                case "comment":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MovestudentTemplate> _dslPath = new Path<MovestudentTemplate>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MovestudentTemplate");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Тип выписки по студенту.
     * @see ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate#getType()
     */
    public static StudentExtractType.Path<StudentExtractType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Индекс шаблона внутри типа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate#getIndex()
     */
    public static PropertyPath<Integer> index()
    {
        return _dslPath.index();
    }

    /**
     * @return Путь в classpath до шаблона по умолчанию. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate#getPath()
     */
    public static PropertyPath<String> path()
    {
        return _dslPath.path();
    }

    /**
     * @return Шаблон в zip текущий.
     * @see ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate#getDocument()
     */
    public static PropertyPath<byte[]> document()
    {
        return _dslPath.document();
    }

    /**
     * @return Дата редактирования.
     * @see ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate#getEditDate()
     */
    public static PropertyPath<Date> editDate()
    {
        return _dslPath.editDate();
    }

    /**
     * @return комментарий.
     * @see ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate#getTitleWithCode()
     */
    public static SupportedPropertyPath<String> titleWithCode()
    {
        return _dslPath.titleWithCode();
    }

    public static class Path<E extends MovestudentTemplate> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private StudentExtractType.Path<StudentExtractType> _type;
        private PropertyPath<Integer> _index;
        private PropertyPath<String> _path;
        private PropertyPath<byte[]> _document;
        private PropertyPath<Date> _editDate;
        private PropertyPath<String> _comment;
        private PropertyPath<String> _title;
        private SupportedPropertyPath<String> _titleWithCode;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(MovestudentTemplateGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Тип выписки по студенту.
     * @see ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate#getType()
     */
        public StudentExtractType.Path<StudentExtractType> type()
        {
            if(_type == null )
                _type = new StudentExtractType.Path<StudentExtractType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Индекс шаблона внутри типа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate#getIndex()
     */
        public PropertyPath<Integer> index()
        {
            if(_index == null )
                _index = new PropertyPath<Integer>(MovestudentTemplateGen.P_INDEX, this);
            return _index;
        }

    /**
     * @return Путь в classpath до шаблона по умолчанию. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate#getPath()
     */
        public PropertyPath<String> path()
        {
            if(_path == null )
                _path = new PropertyPath<String>(MovestudentTemplateGen.P_PATH, this);
            return _path;
        }

    /**
     * @return Шаблон в zip текущий.
     * @see ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate#getDocument()
     */
        public PropertyPath<byte[]> document()
        {
            if(_document == null )
                _document = new PropertyPath<byte[]>(MovestudentTemplateGen.P_DOCUMENT, this);
            return _document;
        }

    /**
     * @return Дата редактирования.
     * @see ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate#getEditDate()
     */
        public PropertyPath<Date> editDate()
        {
            if(_editDate == null )
                _editDate = new PropertyPath<Date>(MovestudentTemplateGen.P_EDIT_DATE, this);
            return _editDate;
        }

    /**
     * @return комментарий.
     * @see ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(MovestudentTemplateGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(MovestudentTemplateGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate#getTitleWithCode()
     */
        public SupportedPropertyPath<String> titleWithCode()
        {
            if(_titleWithCode == null )
                _titleWithCode = new SupportedPropertyPath<String>(MovestudentTemplateGen.P_TITLE_WITH_CODE, this);
            return _titleWithCode;
        }

        public Class getEntityClass()
        {
            return MovestudentTemplate.class;
        }

        public String getEntityName()
        {
            return "movestudentTemplate";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitleWithCode();
}
