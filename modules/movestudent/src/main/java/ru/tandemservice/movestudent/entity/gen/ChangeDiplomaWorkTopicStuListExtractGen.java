package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «Об изменении тем дипломных работ»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChangeDiplomaWorkTopicStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract";
    public static final String ENTITY_NAME = "changeDiplomaWorkTopicStuListExtract";
    public static final int VERSION_HASH = -1312893603;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_DEVELOP_CONDITION = "developCondition";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_DIPLOMA_WORK_TOPIC_OLD = "diplomaWorkTopicOld";
    public static final String P_DIPLOMA_WORK_TOPIC_NEW = "diplomaWorkTopicNew";
    public static final String P_SCIENTIFIC_ADVISER_NAME_STR_OLD = "scientificAdviserNameStrOld";
    public static final String L_SCIENTIFIC_ADVISER_NAME = "scientificAdviserName";
    public static final String L_SCIENTIFIC_ADVISER_NAME_OLD = "scientificAdviserNameOld";
    public static final String P_PREVIOUS_ORDER_NUMBER = "previousOrderNumber";
    public static final String P_PREVIOUS_ORDER_DATE = "previousOrderDate";

    private Course _course;     // Курс
    private Group _group;     // Группа
    private DevelopForm _developForm;     // Форма освоения
    private DevelopCondition _developCondition;     // Условие освоения
    private CompensationType _compensationType;     // Вид возмещения затрат
    private String _diplomaWorkTopicOld;     // Старая тема дипломной работы
    private String _diplomaWorkTopicNew;     // Новая тема дипломной работы
    private String _scientificAdviserNameStrOld;     // Прежний научный руководитель (текстом)
    private PpsEntry _scientificAdviserName;     // Новый научный руководитель
    private PpsEntry _scientificAdviserNameOld;     // Прежний научный руководитель (ППС)
    private String _previousOrderNumber;     // Номер предыдущего приказа
    private Date _previousOrderDate;     // Дата предыдущего приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения. Свойство не может быть null.
     */
    public void setDevelopCondition(DevelopCondition developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Старая тема дипломной работы.
     */
    @Length(max=1024)
    public String getDiplomaWorkTopicOld()
    {
        return _diplomaWorkTopicOld;
    }

    /**
     * @param diplomaWorkTopicOld Старая тема дипломной работы.
     */
    public void setDiplomaWorkTopicOld(String diplomaWorkTopicOld)
    {
        dirty(_diplomaWorkTopicOld, diplomaWorkTopicOld);
        _diplomaWorkTopicOld = diplomaWorkTopicOld;
    }

    /**
     * @return Новая тема дипломной работы. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1024)
    public String getDiplomaWorkTopicNew()
    {
        return _diplomaWorkTopicNew;
    }

    /**
     * @param diplomaWorkTopicNew Новая тема дипломной работы. Свойство не может быть null.
     */
    public void setDiplomaWorkTopicNew(String diplomaWorkTopicNew)
    {
        dirty(_diplomaWorkTopicNew, diplomaWorkTopicNew);
        _diplomaWorkTopicNew = diplomaWorkTopicNew;
    }

    /**
     * @return Прежний научный руководитель (текстом).
     */
    @Length(max=1024)
    public String getScientificAdviserNameStrOld()
    {
        return _scientificAdviserNameStrOld;
    }

    /**
     * @param scientificAdviserNameStrOld Прежний научный руководитель (текстом).
     */
    public void setScientificAdviserNameStrOld(String scientificAdviserNameStrOld)
    {
        dirty(_scientificAdviserNameStrOld, scientificAdviserNameStrOld);
        _scientificAdviserNameStrOld = scientificAdviserNameStrOld;
    }

    /**
     * @return Новый научный руководитель. Свойство не может быть null.
     */
    @NotNull
    public PpsEntry getScientificAdviserName()
    {
        return _scientificAdviserName;
    }

    /**
     * @param scientificAdviserName Новый научный руководитель. Свойство не может быть null.
     */
    public void setScientificAdviserName(PpsEntry scientificAdviserName)
    {
        dirty(_scientificAdviserName, scientificAdviserName);
        _scientificAdviserName = scientificAdviserName;
    }

    /**
     * @return Прежний научный руководитель (ППС).
     */
    public PpsEntry getScientificAdviserNameOld()
    {
        return _scientificAdviserNameOld;
    }

    /**
     * @param scientificAdviserNameOld Прежний научный руководитель (ППС).
     */
    public void setScientificAdviserNameOld(PpsEntry scientificAdviserNameOld)
    {
        dirty(_scientificAdviserNameOld, scientificAdviserNameOld);
        _scientificAdviserNameOld = scientificAdviserNameOld;
    }

    /**
     * @return Номер предыдущего приказа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPreviousOrderNumber()
    {
        return _previousOrderNumber;
    }

    /**
     * @param previousOrderNumber Номер предыдущего приказа. Свойство не может быть null.
     */
    public void setPreviousOrderNumber(String previousOrderNumber)
    {
        dirty(_previousOrderNumber, previousOrderNumber);
        _previousOrderNumber = previousOrderNumber;
    }

    /**
     * @return Дата предыдущего приказа. Свойство не может быть null.
     */
    @NotNull
    public Date getPreviousOrderDate()
    {
        return _previousOrderDate;
    }

    /**
     * @param previousOrderDate Дата предыдущего приказа. Свойство не может быть null.
     */
    public void setPreviousOrderDate(Date previousOrderDate)
    {
        dirty(_previousOrderDate, previousOrderDate);
        _previousOrderDate = previousOrderDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ChangeDiplomaWorkTopicStuListExtractGen)
        {
            setCourse(((ChangeDiplomaWorkTopicStuListExtract)another).getCourse());
            setGroup(((ChangeDiplomaWorkTopicStuListExtract)another).getGroup());
            setDevelopForm(((ChangeDiplomaWorkTopicStuListExtract)another).getDevelopForm());
            setDevelopCondition(((ChangeDiplomaWorkTopicStuListExtract)another).getDevelopCondition());
            setCompensationType(((ChangeDiplomaWorkTopicStuListExtract)another).getCompensationType());
            setDiplomaWorkTopicOld(((ChangeDiplomaWorkTopicStuListExtract)another).getDiplomaWorkTopicOld());
            setDiplomaWorkTopicNew(((ChangeDiplomaWorkTopicStuListExtract)another).getDiplomaWorkTopicNew());
            setScientificAdviserNameStrOld(((ChangeDiplomaWorkTopicStuListExtract)another).getScientificAdviserNameStrOld());
            setScientificAdviserName(((ChangeDiplomaWorkTopicStuListExtract)another).getScientificAdviserName());
            setScientificAdviserNameOld(((ChangeDiplomaWorkTopicStuListExtract)another).getScientificAdviserNameOld());
            setPreviousOrderNumber(((ChangeDiplomaWorkTopicStuListExtract)another).getPreviousOrderNumber());
            setPreviousOrderDate(((ChangeDiplomaWorkTopicStuListExtract)another).getPreviousOrderDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChangeDiplomaWorkTopicStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChangeDiplomaWorkTopicStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new ChangeDiplomaWorkTopicStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "compensationType":
                    return obj.getCompensationType();
                case "diplomaWorkTopicOld":
                    return obj.getDiplomaWorkTopicOld();
                case "diplomaWorkTopicNew":
                    return obj.getDiplomaWorkTopicNew();
                case "scientificAdviserNameStrOld":
                    return obj.getScientificAdviserNameStrOld();
                case "scientificAdviserName":
                    return obj.getScientificAdviserName();
                case "scientificAdviserNameOld":
                    return obj.getScientificAdviserNameOld();
                case "previousOrderNumber":
                    return obj.getPreviousOrderNumber();
                case "previousOrderDate":
                    return obj.getPreviousOrderDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((DevelopCondition) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "diplomaWorkTopicOld":
                    obj.setDiplomaWorkTopicOld((String) value);
                    return;
                case "diplomaWorkTopicNew":
                    obj.setDiplomaWorkTopicNew((String) value);
                    return;
                case "scientificAdviserNameStrOld":
                    obj.setScientificAdviserNameStrOld((String) value);
                    return;
                case "scientificAdviserName":
                    obj.setScientificAdviserName((PpsEntry) value);
                    return;
                case "scientificAdviserNameOld":
                    obj.setScientificAdviserNameOld((PpsEntry) value);
                    return;
                case "previousOrderNumber":
                    obj.setPreviousOrderNumber((String) value);
                    return;
                case "previousOrderDate":
                    obj.setPreviousOrderDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "group":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "compensationType":
                        return true;
                case "diplomaWorkTopicOld":
                        return true;
                case "diplomaWorkTopicNew":
                        return true;
                case "scientificAdviserNameStrOld":
                        return true;
                case "scientificAdviserName":
                        return true;
                case "scientificAdviserNameOld":
                        return true;
                case "previousOrderNumber":
                        return true;
                case "previousOrderDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "group":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "compensationType":
                    return true;
                case "diplomaWorkTopicOld":
                    return true;
                case "diplomaWorkTopicNew":
                    return true;
                case "scientificAdviserNameStrOld":
                    return true;
                case "scientificAdviserName":
                    return true;
                case "scientificAdviserNameOld":
                    return true;
                case "previousOrderNumber":
                    return true;
                case "previousOrderDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "developForm":
                    return DevelopForm.class;
                case "developCondition":
                    return DevelopCondition.class;
                case "compensationType":
                    return CompensationType.class;
                case "diplomaWorkTopicOld":
                    return String.class;
                case "diplomaWorkTopicNew":
                    return String.class;
                case "scientificAdviserNameStrOld":
                    return String.class;
                case "scientificAdviserName":
                    return PpsEntry.class;
                case "scientificAdviserNameOld":
                    return PpsEntry.class;
                case "previousOrderNumber":
                    return String.class;
                case "previousOrderDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChangeDiplomaWorkTopicStuListExtract> _dslPath = new Path<ChangeDiplomaWorkTopicStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChangeDiplomaWorkTopicStuListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getDevelopCondition()
     */
    public static DevelopCondition.Path<DevelopCondition> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Старая тема дипломной работы.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getDiplomaWorkTopicOld()
     */
    public static PropertyPath<String> diplomaWorkTopicOld()
    {
        return _dslPath.diplomaWorkTopicOld();
    }

    /**
     * @return Новая тема дипломной работы. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getDiplomaWorkTopicNew()
     */
    public static PropertyPath<String> diplomaWorkTopicNew()
    {
        return _dslPath.diplomaWorkTopicNew();
    }

    /**
     * @return Прежний научный руководитель (текстом).
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getScientificAdviserNameStrOld()
     */
    public static PropertyPath<String> scientificAdviserNameStrOld()
    {
        return _dslPath.scientificAdviserNameStrOld();
    }

    /**
     * @return Новый научный руководитель. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getScientificAdviserName()
     */
    public static PpsEntry.Path<PpsEntry> scientificAdviserName()
    {
        return _dslPath.scientificAdviserName();
    }

    /**
     * @return Прежний научный руководитель (ППС).
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getScientificAdviserNameOld()
     */
    public static PpsEntry.Path<PpsEntry> scientificAdviserNameOld()
    {
        return _dslPath.scientificAdviserNameOld();
    }

    /**
     * @return Номер предыдущего приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getPreviousOrderNumber()
     */
    public static PropertyPath<String> previousOrderNumber()
    {
        return _dslPath.previousOrderNumber();
    }

    /**
     * @return Дата предыдущего приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getPreviousOrderDate()
     */
    public static PropertyPath<Date> previousOrderDate()
    {
        return _dslPath.previousOrderDate();
    }

    public static class Path<E extends ChangeDiplomaWorkTopicStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private DevelopForm.Path<DevelopForm> _developForm;
        private DevelopCondition.Path<DevelopCondition> _developCondition;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<String> _diplomaWorkTopicOld;
        private PropertyPath<String> _diplomaWorkTopicNew;
        private PropertyPath<String> _scientificAdviserNameStrOld;
        private PpsEntry.Path<PpsEntry> _scientificAdviserName;
        private PpsEntry.Path<PpsEntry> _scientificAdviserNameOld;
        private PropertyPath<String> _previousOrderNumber;
        private PropertyPath<Date> _previousOrderDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getDevelopCondition()
     */
        public DevelopCondition.Path<DevelopCondition> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new DevelopCondition.Path<DevelopCondition>(L_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Старая тема дипломной работы.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getDiplomaWorkTopicOld()
     */
        public PropertyPath<String> diplomaWorkTopicOld()
        {
            if(_diplomaWorkTopicOld == null )
                _diplomaWorkTopicOld = new PropertyPath<String>(ChangeDiplomaWorkTopicStuListExtractGen.P_DIPLOMA_WORK_TOPIC_OLD, this);
            return _diplomaWorkTopicOld;
        }

    /**
     * @return Новая тема дипломной работы. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getDiplomaWorkTopicNew()
     */
        public PropertyPath<String> diplomaWorkTopicNew()
        {
            if(_diplomaWorkTopicNew == null )
                _diplomaWorkTopicNew = new PropertyPath<String>(ChangeDiplomaWorkTopicStuListExtractGen.P_DIPLOMA_WORK_TOPIC_NEW, this);
            return _diplomaWorkTopicNew;
        }

    /**
     * @return Прежний научный руководитель (текстом).
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getScientificAdviserNameStrOld()
     */
        public PropertyPath<String> scientificAdviserNameStrOld()
        {
            if(_scientificAdviserNameStrOld == null )
                _scientificAdviserNameStrOld = new PropertyPath<String>(ChangeDiplomaWorkTopicStuListExtractGen.P_SCIENTIFIC_ADVISER_NAME_STR_OLD, this);
            return _scientificAdviserNameStrOld;
        }

    /**
     * @return Новый научный руководитель. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getScientificAdviserName()
     */
        public PpsEntry.Path<PpsEntry> scientificAdviserName()
        {
            if(_scientificAdviserName == null )
                _scientificAdviserName = new PpsEntry.Path<PpsEntry>(L_SCIENTIFIC_ADVISER_NAME, this);
            return _scientificAdviserName;
        }

    /**
     * @return Прежний научный руководитель (ППС).
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getScientificAdviserNameOld()
     */
        public PpsEntry.Path<PpsEntry> scientificAdviserNameOld()
        {
            if(_scientificAdviserNameOld == null )
                _scientificAdviserNameOld = new PpsEntry.Path<PpsEntry>(L_SCIENTIFIC_ADVISER_NAME_OLD, this);
            return _scientificAdviserNameOld;
        }

    /**
     * @return Номер предыдущего приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getPreviousOrderNumber()
     */
        public PropertyPath<String> previousOrderNumber()
        {
            if(_previousOrderNumber == null )
                _previousOrderNumber = new PropertyPath<String>(ChangeDiplomaWorkTopicStuListExtractGen.P_PREVIOUS_ORDER_NUMBER, this);
            return _previousOrderNumber;
        }

    /**
     * @return Дата предыдущего приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.ChangeDiplomaWorkTopicStuListExtract#getPreviousOrderDate()
     */
        public PropertyPath<Date> previousOrderDate()
        {
            if(_previousOrderDate == null )
                _previousOrderDate = new PropertyPath<Date>(ChangeDiplomaWorkTopicStuListExtractGen.P_PREVIOUS_ORDER_DATE, this);
            return _previousOrderDate;
        }

        public Class getEntityClass()
        {
            return ChangeDiplomaWorkTopicStuListExtract.class;
        }

        public String getEntityName()
        {
            return "changeDiplomaWorkTopicStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
