package ru.tandemservice.movestudent.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITakeOffPayment;
import ru.tandemservice.movestudent.entity.gen.AcadGrantBonusPaymentStopStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О прекращении выплаты надбавки к государственной академической стипендии 
 */
public class AcadGrantBonusPaymentStopStuExtract extends AcadGrantBonusPaymentStopStuExtractGen implements ITakeOffPayment
{
    @Override
    public Date getBeginDate()
    {
        return getAcadGrantBonusPaymentStopDate();
    }

    @Override
    public Date getPaymentTakeOffDate()
    {
        return getAcadGrantBonusPaymentStopDate();
    }
}