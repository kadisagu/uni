/*$Id$*/
package ru.tandemservice.movestudent.component.listextract.e44.utils;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.ChangeProfileAndGroupStuListExtract;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2014
 */
public class ChangeProfileAndGroupParagraphWrapper implements Comparable<ChangeProfileAndGroupParagraphWrapper>
{
    /**
     * course, group, compensationType, EducationLevels, formativeOrgUnit, territorialOrgUnit, developForm, специализации (профили и т.д.)
     */
    private final Course _course;
    private final Group _group;
    private final CompensationType _compensationType;
    private final EducationLevelsHighSchool _educationLevels;
    private final OrgUnit _formativeOrgUnit;
    private final OrgUnit _territorialOrgUnit;
    private final DevelopForm _developForm;
    private final DevelopCondition _developCondition;
    private final DevelopTech _developTech;
    private final DevelopPeriod _developPeriod;
    private final Qualifications _qualification;
    private final EducationLevels _educationLevelNew;
    private final Group _groupNew;
    private final StudentCategory _studentCategory;

    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    public EducationLevels getEducationLevelNew()
    {
        return _educationLevelNew;
    }

    private final ChangeProfileAndGroupStuListExtract _firstExtract;

    private final List<Person> _personList = new ArrayList<>();

    public ChangeProfileAndGroupParagraphWrapper(
            Course course,
            Group group,
            CompensationType compensationType,
            EducationLevelsHighSchool educationLevels,
            OrgUnit formativeOrgUnit,
            OrgUnit territorialOrgUnit,
            DevelopForm developForm,
            DevelopCondition developCondition,
            DevelopTech developTech,
            DevelopPeriod developPeriod,
            Qualifications qualification,
            ChangeProfileAndGroupStuListExtract firstExtract,
            EducationLevels educationLevelNew,
            Group groupNew,
            StudentCategory studentCategory)
    {
        _course = course;
        _group = group;
        _compensationType = compensationType;
        _educationLevels = educationLevels;
        _formativeOrgUnit = formativeOrgUnit;
        _territorialOrgUnit = territorialOrgUnit;
        _developForm = developForm;
        _developCondition = developCondition;
        _developTech = developTech;
        _developPeriod = developPeriod;
        _qualification = qualification;
        _firstExtract = firstExtract;
        _educationLevelNew = educationLevelNew;
        _groupNew = groupNew;
        _studentCategory = studentCategory;
    }

    public Course getCourse()
    {
        return _course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public EducationLevelsHighSchool getEducationLevels()
    {
        return _educationLevels;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public Qualifications getQualification()
    {
        return _qualification;
    }

    public ChangeProfileAndGroupStuListExtract getFirstExtract()
    {
        return _firstExtract;
    }

    public List<Person> getPersonList()
    {
        return _personList;
    }

    public Group getGroupNew()
    {
        return _groupNew;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof ChangeProfileAndGroupParagraphWrapper))
            return false;

        ChangeProfileAndGroupParagraphWrapper that = (ChangeProfileAndGroupParagraphWrapper) o;

        return _course.equals(that.getCourse())
                && _group.equals(that.getGroup())
                && _compensationType.equals(that.getCompensationType())
                && _educationLevels.equals(that.getEducationLevels())
                && _formativeOrgUnit.equals(that.getFormativeOrgUnit())
                && _territorialOrgUnit.equals(that.getTerritorialOrgUnit())
                && _developForm.equals(that.getDevelopForm())
                && _developCondition.equals(that.getDevelopCondition())
                && _developTech.equals(that.getDevelopTech())
                && _developPeriod.equals(that.getDevelopPeriod())
                && _groupNew.equals(that.getGroupNew())
                && _studentCategory.equals(that.getStudentCategory());
    }

    @Override
    public int hashCode()
    {
        return _course.hashCode() & _group.hashCode() & _compensationType.hashCode() & _educationLevels.hashCode() & _formativeOrgUnit.hashCode() & _territorialOrgUnit.hashCode() & _developForm.hashCode() & _developCondition.hashCode() & _developTech.hashCode() & _developPeriod.hashCode() & _groupNew.hashCode() & _studentCategory.hashCode();
    }

    @Override
    public int compareTo(ChangeProfileAndGroupParagraphWrapper o)
    {
        if (StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(_studentCategory.getCode()) && StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(o.getStudentCategory().getCode()))
            return -1;
        else if (StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(_studentCategory.getCode()) && StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(o.getStudentCategory().getCode()))
            return 1;
        else if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(_compensationType.getCode()) && CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(o.getCompensationType().getCode()))
            return -1;
        else if (CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(_compensationType.getCode()) && CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(o.getCompensationType().getCode()))
            return 1;
        else return 0;
    }
}
