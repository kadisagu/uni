package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Абстрактная выписка из списочного приказа по студенту
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ListStudentExtractGen extends AbstractStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.ListStudentExtract";
    public static final String ENTITY_NAME = "listStudentExtract";
    public static final int VERSION_HASH = -779979641;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ListStudentExtractGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ListStudentExtractGen> extends AbstractStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ListStudentExtract.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("ListStudentExtract is abstract");
        }
    }
    private static final Path<ListStudentExtract> _dslPath = new Path<ListStudentExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ListStudentExtract");
    }
            

    public static class Path<E extends ListStudentExtract> extends AbstractStudentExtract.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return ListStudentExtract.class;
        }

        public String getEntityName()
        {
            return "listStudentExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
