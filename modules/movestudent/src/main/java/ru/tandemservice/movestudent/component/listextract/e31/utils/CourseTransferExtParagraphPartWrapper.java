/**
 *$Id$
 */
package ru.tandemservice.movestudent.component.listextract.e31.utils;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 27.09.2012
 */
public class CourseTransferExtParagraphPartWrapper implements Comparable<CourseTransferExtParagraphPartWrapper>
{
    private final Course _courseNew;
    private final Group _group;
    private final ListStudentExtract _firstExtract;

    public CourseTransferExtParagraphPartWrapper(Course courseNew, Group group, ListStudentExtract firstExtract)
    {
        _courseNew = courseNew;
        _group = group;
        _firstExtract = firstExtract;
    }

    private List<Person> _personList = new ArrayList<Person>();

    public Course getCourseNew()
    {
        return _courseNew;
    }

    public Group getGroup()
    {
        return _group;
    }

    public List<Person> getPersonList()
    {
        return _personList;
    }

    public void setPersonList(List<Person> personList)
    {
        _personList = personList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof CourseTransferExtParagraphPartWrapper))
            return false;

        CourseTransferExtParagraphPartWrapper that = (CourseTransferExtParagraphPartWrapper) o;

        return _courseNew.equals(that.getCourseNew()) && _group.equals(that.getGroup());
    }

    @Override
    public int hashCode()
    {
        return _courseNew.hashCode() & _group.hashCode();
    }

    @Override
    public int compareTo(CourseTransferExtParagraphPartWrapper o)
    {
        int result = ((Integer) _courseNew.getIntValue()).compareTo((Integer) o.getCourseNew().getIntValue());
        if (result == 0)
            result = _group.getTitle().compareTo(o.getGroup().getTitle());

        return result;
    }
}