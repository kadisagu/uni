/* $Id$ */
package ru.tandemservice.movestudent.component.customorder.actions.ChangeStudentStatus.AddEdit;

import ru.tandemservice.movestudent.component.customorder.IStudentCustomOrderExtract;
import ru.tandemservice.movestudent.component.customorder.actions.BaseCustomAction.AddEdit.BaseCustomActionAddEditDAO;
import ru.tandemservice.movestudent.entity.custom.ChangeStudentStatusAction;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author Nikolay Fedorovskih
 * @since 03.09.2013
 */
public class DAO extends BaseCustomActionAddEditDAO<ChangeStudentStatusAction, Model>
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        if (model.isActionActive())
        {
            model.setNewStatusList(getList(StudentStatus.class, StudentStatus.P_PRIORITY));
            if (model.getStudent() != null)
                model.getNewStatusList().remove(model.getStudent().getStatus());
        }
    }

    @Override
    protected Class<ChangeStudentStatusAction> getActionClass()
    {
        return ChangeStudentStatusAction.class;
    }

    @Override
    public void updatePrepare(Model model, ChangeStudentStatusAction action, IStudentCustomOrderExtract extract)
    {
        Student student = (Student) extract.getEntity();
        action.setOldStatus(student.getStatus());
    }

    @Override
    public void doCommit(ChangeStudentStatusAction action)
    {
        Student student = (Student) action.getCustomExtract().getEntity();

        action.setOldStatus(student.getStatus());
        update(action);

        student.setStatus(action.getNewStatus());
        update(student);
    }

    @Override
    public void doRollback(ChangeStudentStatusAction action)
    {
        Student student = (Student) action.getCustomExtract().getEntity();
        student.setStatus(action.getOldStatus());
        update(student);
    }
}