/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderAddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;

/**
 * @author vip_delete
 * @since 05.05.2009
 */
public abstract class AbstractListOrderAddEditController<T extends StudentListOrder, IDAO extends IAbstractListOrderAddEditDAO<T, Model>, Model extends AbstractListOrderAddEditModel<T>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setFieldsPage("ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderAddEdit.ListOrderAddEditFields");
        model.setCommentsPage("ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderAddEdit.BasicCommentAddEdit");

        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().update(model);

        if (model.getOrderId() == null)
        {
            // форма добавления
            deactivate(component, 2);//назад на 2-е страницы
            activateInRoot(component, new ComponentActivator(MoveStudentUtils.getListOrderPubComponent(model.getOrder().getType()), new ParametersMap()
                    .add(PublisherActivator.PUBLISHER_ID_KEY, model.getOrder().getId()
            )));
        } else
            deactivate(component);
    }

    public void onClickDeactivate(IBusinessComponent component)
    {
        if (getModel(component).getOrderId() == null)
            deactivate(component, 2);// форма добавления
        else
            deactivate(component);
    }
}