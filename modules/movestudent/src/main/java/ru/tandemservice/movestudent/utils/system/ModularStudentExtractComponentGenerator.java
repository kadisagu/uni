/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.utils.system;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uni.util.system.UniSystemUtils;

/**
 * @author vip_delete
 * @since 05.11.2008
 */
public class ModularStudentExtractComponentGenerator
{
    private static String _tandemUniPath = UniSystemUtils.getModulesDir().getAbsolutePath();
    private static String _templatePath = _tandemUniPath + "/movestudent/src/main/java/ru/tandemservice/movestudent/utils/system/templates";

    public static void main(String[] args) throws Exception
    {
        for (String index : args)
        {
            int extractIndex = Integer.parseInt(index);
            String[] extractData = ExtractData.EXTRACT_LIST[extractIndex - 1];
            String entityName = extractData[0];

            createExtractComponent(entityName, extractIndex);
        }
    }

    private static void createExtractComponent(String entityName, int extractIndex) throws Exception
    {
        String extractSimpleClass = StringUtils.capitalize(entityName);

        String componentPath = _tandemUniPath + "/movestudent/src/main/java/ru/tandemservice/movestudent/component/modularextract/e" + extractIndex;

        UniSystemUtils.initVelocity();

        VelocityContext context = new VelocityContext();
        context.put("entityName", entityName);
        context.put("creationDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        context.put("entityClassName", extractSimpleClass);
        context.put("extractIndex", extractIndex);

        UniSystemUtils.createComponentByTemplate(componentPath + "/AddEdit", _templatePath + "/AddEdit", context, false);

        UniSystemUtils.createComponentByTemplate(componentPath + "/Pub", _templatePath + "/Pub", context, false);

        UniSystemUtils.createFileByTemplate(componentPath + "/" + extractSimpleClass + "Dao.java", _templatePath + "/extractDao.vm", context, false);

        UniSystemUtils.createFileByTemplate(componentPath + "/" + extractSimpleClass + "Print.java", _templatePath + "/extractPrint.vm", context, false);
    }
}
