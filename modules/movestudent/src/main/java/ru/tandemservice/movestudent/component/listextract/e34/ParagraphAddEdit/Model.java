/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use _file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e34.ParagraphAddEdit;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEditAlternative.AbstractParagraphAddEditAlternativeModel;
import ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.IExtEducationLevelModel;

import java.util.Date;
import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 03.10.2012
 */
public class Model extends AbstractParagraphAddEditAlternativeModel<FinAidAssignStuListExtract> implements IGroupModel, IExtEducationLevelModel
{
    private Course _course;
    private Group _group;
    private CompensationType _compensationType;
    private IdentifiableWrapper _month;
    private Integer _year;
	private Date _matchingDate;
	private String _protocolNumber;
	private EmployeePost _responsibleForPayments;
	private EmployeePost _responsibleForAgreement;
	private EducationLevelsHighSchool _educationLevelsHighSchool;

	private ISelectModel _groupListModel;
	private ISelectModel _eduLevelHighSchoolListModel;
    private List<CompensationType> _compensationTypeList;
    private List<IdentifiableWrapper> _monthList;
	private ISelectModel _employeePostModel;


    //Calculate method
    public String getFinAidSizeId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "finAidSizeId_" + dataSource.getCurrentEntity().getId();
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public IdentifiableWrapper getMonth()
    {
        return _month;
    }

    public void setMonth(IdentifiableWrapper month)
    {
        _month = month;
    }

    public Integer getYear()
    {
        return _year;
    }

    public void setYear(Integer year)
    {
        _year = year;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public List<IdentifiableWrapper> getMonthList()
    {
        return _monthList;
    }

    public void setMonthList(List<IdentifiableWrapper> monthList)
    {
        _monthList = monthList;
    }

	public ISelectModel getEmployeePostModel()
	{
		return _employeePostModel;
	}

	public void setEmployeePostModel(ISelectModel employeePostModel)
	{
		_employeePostModel = employeePostModel;
	}

	public Date getMatchingDate()
	{
		return _matchingDate;
	}

	public void setMatchingDate(Date matchingDate)
	{
		_matchingDate = matchingDate;
	}

	public String getProtocolNumber()
	{
		return _protocolNumber;
	}

	public void setProtocolNumber(String protocolNumber)
	{
		_protocolNumber = protocolNumber;
	}

	public EmployeePost getResponsibleForPayments()
	{
		return _responsibleForPayments;
	}

	public void setResponsibleForPayments(EmployeePost responsibleForPayments)
	{
		_responsibleForPayments = responsibleForPayments;
	}

	public EmployeePost getResponsibleForAgreement()
	{
		return _responsibleForAgreement;
	}

	public void setResponsibleForAgreement(EmployeePost responsibleForAgreement)
	{
		_responsibleForAgreement = responsibleForAgreement;
	}

	public EducationLevelsHighSchool getEducationLevelsHighSchool()
	{
		return _educationLevelsHighSchool;
	}

	public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
	{
		_educationLevelsHighSchool = educationLevelsHighSchool;
	}

	public ISelectModel getEduLevelHighSchoolListModel()
	{
		return _eduLevelHighSchoolListModel;
	}

	public void setEduLevelHighSchoolListModel(ISelectModel eduLevelHighSchoolListModel)
	{
		_eduLevelHighSchoolListModel = eduLevelHighSchoolListModel;
	}

	@Override
	public EducationLevels getParentEduLevel()
	{
		return getEducationLevelsHighSchool() != null ? getEducationLevelsHighSchool().getEducationLevel() : null;
	}
}
