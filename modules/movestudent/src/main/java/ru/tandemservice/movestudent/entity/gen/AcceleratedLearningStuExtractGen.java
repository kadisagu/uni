package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AcceleratedLearningStuExtract;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О переводе на ускоренное обучение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AcceleratedLearningStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.AcceleratedLearningStuExtract";
    public static final String ENTITY_NAME = "acceleratedLearningStuExtract";
    public static final int VERSION_HASH = -825358858;
    private static IEntityMeta ENTITY_META;

    public static final String P_INDIVIDUAL_EDU_PLAN = "individualEduPlan";
    public static final String P_PROTOCOL_NUMBER = "protocolNumber";
    public static final String P_PROTOCOL_DATE = "protocolDate";

    private String _individualEduPlan;     // Индивидуальный учебный план
    private String _protocolNumber;     // Номер протокола
    private Date _protocolDate;     // Дата протокола

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Индивидуальный учебный план. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getIndividualEduPlan()
    {
        return _individualEduPlan;
    }

    /**
     * @param individualEduPlan Индивидуальный учебный план. Свойство не может быть null.
     */
    public void setIndividualEduPlan(String individualEduPlan)
    {
        dirty(_individualEduPlan, individualEduPlan);
        _individualEduPlan = individualEduPlan;
    }

    /**
     * @return Номер протокола. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getProtocolNumber()
    {
        return _protocolNumber;
    }

    /**
     * @param protocolNumber Номер протокола. Свойство не может быть null.
     */
    public void setProtocolNumber(String protocolNumber)
    {
        dirty(_protocolNumber, protocolNumber);
        _protocolNumber = protocolNumber;
    }

    /**
     * @return Дата протокола. Свойство не может быть null.
     */
    @NotNull
    public Date getProtocolDate()
    {
        return _protocolDate;
    }

    /**
     * @param protocolDate Дата протокола. Свойство не может быть null.
     */
    public void setProtocolDate(Date protocolDate)
    {
        dirty(_protocolDate, protocolDate);
        _protocolDate = protocolDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof AcceleratedLearningStuExtractGen)
        {
            setIndividualEduPlan(((AcceleratedLearningStuExtract)another).getIndividualEduPlan());
            setProtocolNumber(((AcceleratedLearningStuExtract)another).getProtocolNumber());
            setProtocolDate(((AcceleratedLearningStuExtract)another).getProtocolDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AcceleratedLearningStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AcceleratedLearningStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new AcceleratedLearningStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "individualEduPlan":
                    return obj.getIndividualEduPlan();
                case "protocolNumber":
                    return obj.getProtocolNumber();
                case "protocolDate":
                    return obj.getProtocolDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "individualEduPlan":
                    obj.setIndividualEduPlan((String) value);
                    return;
                case "protocolNumber":
                    obj.setProtocolNumber((String) value);
                    return;
                case "protocolDate":
                    obj.setProtocolDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "individualEduPlan":
                        return true;
                case "protocolNumber":
                        return true;
                case "protocolDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "individualEduPlan":
                    return true;
                case "protocolNumber":
                    return true;
                case "protocolDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "individualEduPlan":
                    return String.class;
                case "protocolNumber":
                    return String.class;
                case "protocolDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AcceleratedLearningStuExtract> _dslPath = new Path<AcceleratedLearningStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AcceleratedLearningStuExtract");
    }
            

    /**
     * @return Индивидуальный учебный план. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcceleratedLearningStuExtract#getIndividualEduPlan()
     */
    public static PropertyPath<String> individualEduPlan()
    {
        return _dslPath.individualEduPlan();
    }

    /**
     * @return Номер протокола. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcceleratedLearningStuExtract#getProtocolNumber()
     */
    public static PropertyPath<String> protocolNumber()
    {
        return _dslPath.protocolNumber();
    }

    /**
     * @return Дата протокола. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcceleratedLearningStuExtract#getProtocolDate()
     */
    public static PropertyPath<Date> protocolDate()
    {
        return _dslPath.protocolDate();
    }

    public static class Path<E extends AcceleratedLearningStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<String> _individualEduPlan;
        private PropertyPath<String> _protocolNumber;
        private PropertyPath<Date> _protocolDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Индивидуальный учебный план. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcceleratedLearningStuExtract#getIndividualEduPlan()
     */
        public PropertyPath<String> individualEduPlan()
        {
            if(_individualEduPlan == null )
                _individualEduPlan = new PropertyPath<String>(AcceleratedLearningStuExtractGen.P_INDIVIDUAL_EDU_PLAN, this);
            return _individualEduPlan;
        }

    /**
     * @return Номер протокола. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcceleratedLearningStuExtract#getProtocolNumber()
     */
        public PropertyPath<String> protocolNumber()
        {
            if(_protocolNumber == null )
                _protocolNumber = new PropertyPath<String>(AcceleratedLearningStuExtractGen.P_PROTOCOL_NUMBER, this);
            return _protocolNumber;
        }

    /**
     * @return Дата протокола. Свойство не может быть null.
     * @see ru.tandemservice.movestudent.entity.AcceleratedLearningStuExtract#getProtocolDate()
     */
        public PropertyPath<Date> protocolDate()
        {
            if(_protocolDate == null )
                _protocolDate = new PropertyPath<Date>(AcceleratedLearningStuExtractGen.P_PROTOCOL_DATE, this);
            return _protocolDate;
        }

        public Class getEntityClass()
        {
            return AcceleratedLearningStuExtract.class;
        }

        public String getEntityName()
        {
            return "acceleratedLearningStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
