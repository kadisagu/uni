package ru.tandemservice.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.StuListOrderToBasicRelation;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderBasics;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь списочного приказа с основанием. В списочном приказе есть список оснований (приказы по студентам)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StuListOrderToBasicRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudent.entity.StuListOrderToBasicRelation";
    public static final String ENTITY_NAME = "stuListOrderToBasicRelation";
    public static final int VERSION_HASH = -1718193;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String L_BASIC = "basic";
    public static final String P_COMMENT = "comment";

    private StudentListOrder _order;     // Списочный приказ
    private StudentOrderBasics _basic;     // Основание списочного приказа
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Списочный приказ.
     */
    public StudentListOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Списочный приказ.
     */
    public void setOrder(StudentListOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Основание списочного приказа.
     */
    public StudentOrderBasics getBasic()
    {
        return _basic;
    }

    /**
     * @param basic Основание списочного приказа.
     */
    public void setBasic(StudentOrderBasics basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=1024)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StuListOrderToBasicRelationGen)
        {
            setOrder(((StuListOrderToBasicRelation)another).getOrder());
            setBasic(((StuListOrderToBasicRelation)another).getBasic());
            setComment(((StuListOrderToBasicRelation)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StuListOrderToBasicRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StuListOrderToBasicRelation.class;
        }

        public T newInstance()
        {
            return (T) new StuListOrderToBasicRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "basic":
                    return obj.getBasic();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((StudentListOrder) value);
                    return;
                case "basic":
                    obj.setBasic((StudentOrderBasics) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "basic":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "basic":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return StudentListOrder.class;
                case "basic":
                    return StudentOrderBasics.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StuListOrderToBasicRelation> _dslPath = new Path<StuListOrderToBasicRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StuListOrderToBasicRelation");
    }
            

    /**
     * @return Списочный приказ.
     * @see ru.tandemservice.movestudent.entity.StuListOrderToBasicRelation#getOrder()
     */
    public static StudentListOrder.Path<StudentListOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Основание списочного приказа.
     * @see ru.tandemservice.movestudent.entity.StuListOrderToBasicRelation#getBasic()
     */
    public static StudentOrderBasics.Path<StudentOrderBasics> basic()
    {
        return _dslPath.basic();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.movestudent.entity.StuListOrderToBasicRelation#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends StuListOrderToBasicRelation> extends EntityPath<E>
    {
        private StudentListOrder.Path<StudentListOrder> _order;
        private StudentOrderBasics.Path<StudentOrderBasics> _basic;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Списочный приказ.
     * @see ru.tandemservice.movestudent.entity.StuListOrderToBasicRelation#getOrder()
     */
        public StudentListOrder.Path<StudentListOrder> order()
        {
            if(_order == null )
                _order = new StudentListOrder.Path<StudentListOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Основание списочного приказа.
     * @see ru.tandemservice.movestudent.entity.StuListOrderToBasicRelation#getBasic()
     */
        public StudentOrderBasics.Path<StudentOrderBasics> basic()
        {
            if(_basic == null )
                _basic = new StudentOrderBasics.Path<StudentOrderBasics>(L_BASIC, this);
            return _basic;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.movestudent.entity.StuListOrderToBasicRelation#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(StuListOrderToBasicRelationGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return StuListOrderToBasicRelation.class;
        }

        public String getEntityName()
        {
            return "stuListOrderToBasicRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
