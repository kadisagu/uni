/* $Id$ */
package ru.tandemservice.movestudent.component.settings.OtherOrderTypeFieldsSettings;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.movestudent.IMoveStudentComponents;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;

/**
 * @author Dmitry Seleznev
 * @since 05.04.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareListDataSource(component);
    }

    public void prepareListDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<StudentExtractType> dataSource = new DynamicListDataSource<StudentExtractType>(component, this);
        dataSource.addColumn(new SimpleColumn("Название", StudentExtractType.P_TITLE).setClickable(false).setOrderable(false).setTreeColumn(true));
        dataSource.addColumn(new SimpleColumn("Доступные к заполнению поля", "fields", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Описание", StudentExtractType.description().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Группа приказов", "group").setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditFields"));
        dataSource.addColumn(new ActionColumn("Наследовать от родительской группы типов", "cancel", "onClickOverrideFromParent", "Наследовать настройку от родительского типа приказов для «{0}»?", StudentExtractType.title().s()).setDisabledProperty(Model.DISABLE_REVERT_PROPERTY));
        getModel(component).setDataSource(dataSource);
    }

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickEditFields(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(IMoveStudentComponents.OTHER_ORDER_TYPE_FIELD_SETTINGS_EDIT, new ParametersMap()
                .add("studentExtractTypeId", component.getListenerParameter()))
        );
    }

    public void onClickOverrideFromParent(IBusinessComponent component)
    {
        getDao().deleteSetting(component.<Long>getListenerParameter());
    }
}