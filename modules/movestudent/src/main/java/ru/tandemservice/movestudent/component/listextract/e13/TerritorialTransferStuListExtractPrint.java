/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.listextract.e13;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.IStudentListParagraphPrintFormatter;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.TerritorialTransferStuListExtract;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author ListExtractComponentGenerator
 * @since 06.09.2010
 */
public class TerritorialTransferStuListExtractPrint implements IPrintFormCreator<TerritorialTransferStuListExtract>, IListParagraphPrintFormCreator<TerritorialTransferStuListExtract>, IStudentListParagraphPrintFormatter
{
    @Override
    public RtfDocument createPrintForm(byte[] template, TerritorialTransferStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        AddressDetailed terrOrgUnitAddress = extract.getEducationOrgUnitNew().getTerritorialOrgUnit().getAddress();
        if (null != terrOrgUnitAddress && null != terrOrgUnitAddress.getSettlement())
        {
            modifier.put("territorialOrgUnitNew", terrOrgUnitAddress.getSettlement().getTitleWithType());
            modifier.put("territorialOrgUnitNew_A", terrOrgUnitAddress.getSettlement().getTitleWithType());
            modifier.put("territorialOrgUnitNew_P", terrOrgUnitAddress.getSettlement().getTitleWithType());
        } else
        {
            modifier.put("territorialOrgUnitNew", "______________________");
            modifier.put("territorialOrgUnitNew_A", "______________________");
            modifier.put("territorialOrgUnitNew_P", "______________________");
        }
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public String formatSingleStudent(Student student, int extractNumber)
    {
        StringBuilder buffer = new StringBuilder();
        buffer.append("\\par ").append(extractNumber).append(".  ");
        buffer.append(student.getPerson().getFullFio());
        return buffer.toString();
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, TerritorialTransferStuListExtract firstExtract)
    {
        OrgUnit territorialOrgUnit = firstExtract.getEntity().getEducationOrgUnit().getTerritorialOrgUnit();
        CommonExtractPrint.initOrgUnit(modifier, territorialOrgUnit, "territorialOrgUnit", "");

        boolean isBudget = firstExtract.getEntity().getCompensationType().isBudget();
        modifier.put("compensationType_Alt", isBudget ? "на бюджетной основе" : "по договору с оплатой за обучение");

        AddressDetailed terrOrgUnitAddress = firstExtract.getEducationOrgUnitNew().getTerritorialOrgUnit().getAddress();
        if (null != terrOrgUnitAddress && null != terrOrgUnitAddress.getSettlement())
        {
            modifier.put("territorialOrgUnitNew", terrOrgUnitAddress.getSettlement().getTitleWithType());
            modifier.put("territorialOrgUnitNew_A", terrOrgUnitAddress.getSettlement().getTitleWithType());
            modifier.put("territorialOrgUnitNew_P", terrOrgUnitAddress.getSettlement().getTitleWithType());
        } else
        {
            modifier.put("territorialOrgUnitNew", "______________________");
            modifier.put("territorialOrgUnitNew_A", "______________________");
            modifier.put("territorialOrgUnitNew_P", "______________________");
        }

        modifier.put("educationOrgUnit", firstExtract.getEducationOrgUnitOld().getEducationLevelHighSchool().getPrintTitleWithoutOrgUnit());
        CommonExtractPrint.addEntryDateToModifier(modifier, firstExtract.getTransferDate());
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, TerritorialTransferStuListExtract firstExtract)
    {
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
        return CommonExtractPrint.addEntryDateToModifier(injectModifier, firstExtract.getTransferDate());
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, TerritorialTransferStuListExtract firstExtract)
    {
        return null;
    }
}