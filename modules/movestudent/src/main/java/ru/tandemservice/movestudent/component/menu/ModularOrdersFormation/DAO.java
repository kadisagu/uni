/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.movestudent.component.menu.ModularOrdersFormation;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.component.commons.ExtractTypeSelectModel;
import ru.tandemservice.movestudent.component.commons.FlexibleDQLBuilder.*;
import ru.tandemservice.movestudent.component.commons.OrderCategory;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.movestudent.ui.UnimoveStudentMultiSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.Arrays;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.movestudent.component.commons.OrderFlexBuilder.*;

/**
 * @author vip_delete
 * @since 18.10.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEducationYearList(new EducationYearModel());
        model.setOrderStateList(getCatalogItemList(OrderStates.class));
        model.getOrderStateList().remove(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED));
        model.setStudentList(new UnimoveStudentMultiSelectModel());
        model.setEmployeePostModel(new OrderExecutorSelectModel());
        model.setExtractTypesListModel(new ExtractTypeSelectModel(OrderCategory.MODULAR));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        IFlexFilter context = new IFlexFilter()
        {
            @Override
            public String getAlias()
            {
                return ORDER_DATA_JOIN_ALIAS;
            }

            @Override
            public boolean addToBuilder(IDataSettings dataSettings, DQLSelectBuilder builder, IJoinCreator joinCreator)
            {
                builder.where(and(
                        // Только непроведенные приказы
                        ne(property(ORDER_ALIAS, IAbstractOrder.L_STATE + "." + OrderStates.P_CODE), value(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)),
                        // И нет еще выписок, либо это сбоный приказ
                        eq(DQLFunctions.coalesce(property(ORDER_DATA_JOIN_ALIAS, IS_INDIVIDUAL_ORDER), value(Boolean.FALSE)), value(Boolean.FALSE))
                ));
                return true;
            }
        };

        IFlexConfig extractConfig = new FlexConfig(AbstractStudentExtract.class, EXTRACT_ALIAS)
                .addColumns(new SimpleFlexColumn(PARAGRAPH_ALIAS, IAbstractParagraph.L_ORDER))
                .addJoins(new SimpleFlexJoin(PARAGRAPH_ALIAS, EXTRACT_ALIAS, IAbstractExtract.L_PARAGRAPH, true), STUDENT_JOIN)
                .addFilters(STUDENT_FILTER, EXTRACT_TYPE_FILTER);


        final IFlexColumn stateCol = new SimpleFlexColumn(ORDER_ALIAS, IAbstractOrder.L_STATE + ".code", "stateCol", false);

        IFlexConfig mainConfig = new FlexConfig(StudentModularOrder.class, ORDER_ALIAS)
                .addColumns(
                        ORDER_CREATE_DATE_COLUMN,
                        ORDER_COMMIT_DATE_COLUMN,
                        ORDER_NUMBER_COLUMN,
                        ORDER_STATE_COLUMN,
                        EXTRACT_COUNT_COLUMN,
                        ORDER_EXECUTOR_COLUMN,
                        stateCol
                )
                .addJoins(
                        new OrdersDataJoin(Arrays.asList(OrderCategory.MODULAR_ORDER_CATEGORY, OrderCategory.INDIVIDUAL_ORDER_CATEGORY), DQLJoinType.left, false, false, false, true)
                )
                .addOrders(
                        ORDER_CREATE_DATE_SORT,
                        ORDER_COMMIT_DATE_SORT,
                        ORDER_NUMBER_SORT,
                        ORDER_EXTRACT_COUNT_SORT,
                        ORDER_EXECUTOR_SORT
                )
                .addFilters(
                        context,
                        ORDER_EDU_YEAR_FILTER,
                        ORDER_COMMIT_DATE_PERIOD_FILTER,
                        ORDER_CREATE_DATE_PERIOD_FILTER,
                        ORDER_NUMBER_FILTER,
                        ORDER_STATE_FILTER,
                        EXECUTOR_FILTER,
                        new SubQueryFlexFilter(ORDER_ALIAS, "id", extractConfig, IAbstractParagraph.L_ORDER)
                );

        FlexDQLBuilder flexBuilder = new FlexDQLBuilder(mainConfig, false);

        flexBuilder.fillDataSource(getSession(), model.getSettings(), model.getDataSource(), new IPatchResolver()
        {
            @Override
            public void patch(List<Object[]> srcList, List<IEntity> destList, List<IFlexColumn> columns)
            {
                int extractCountIdx = columns.indexOf(EXTRACT_COUNT_COLUMN);
                int stateColIdx = columns.indexOf(stateCol);
                int numberColIdx = columns.indexOf(ORDER_NUMBER_COLUMN);
                for (int i = 0; i < srcList.size(); i++)
                {
                    Object[] cols = srcList.get(i);
                    DataWrapper wrapper = (DataWrapper) destList.get(i);
                    Number count = (Number) cols[extractCountIdx];
                    wrapper.setProperty(DISABLE_PRINT, count == null || count.longValue() == 0L);
                    wrapper.setProperty(AbstractStudentOrder.P_READONLY, !UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(cols[stateColIdx]));
                    String orderNumber = (String) cols[numberColIdx];
                    wrapper.setTitle("Приказ №" + (orderNumber != null ? orderNumber : ""));
                }
            }
        });
    }

    @Override
    public void deleteRow(IBusinessComponent context)
    {
        IAbstractOrder order = getNotNull(StudentModularOrder.class, (Long) context.getListenerParameter());
        ExtractStates excepted = getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED);
        MoveDaoFacade.getMoveDao().deleteOrder(order, excepted);
    }
}
