/* $Id$ */
package movestudent.scripts

import org.tandemframework.rtf.modifiers.*
import org.tandemframework.shared.organization.base.entity.*
import org.tandemframework.shared.person.base.entity.*
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.*
import ru.tandemservice.movestudent.dao.*
import ru.tandemservice.movestudent.entity.*
import ru.tandemservice.uni.entity.employee.*

return new CommonPrintModifierClass()

/**
 * Скрипт для вставки дополнительных или переопределения существующих печатных (RTF) меток в приказах модуля «Движения Студентов».
 * Применяется при печати сборных, списочных и индивидуальных приказов всех типов.
 *
 * Полезная информация:
 * - Инструкция по выгрузке схемы данных: http://tandemservice.ru/products/tandem-university/rukovodstva-polzovatelya
 *   (руководство пользователя "Базовые модули и функции TF", раздел "Системные действия")
 *
 * Для включения в импорты могут потребоваться пути до классов, которые можно найти по Javadoc:
 * - по классам модулей Tandem Framework: http://www.tandemservice.ru/javadoc/framework
 * - по классам модулей Shared: http://tandemservice.ru/javadoc/additional
 * - по классам модулей University: http://tandemservice.ru/javadoc/university
 */
class CommonPrintModifierClass extends EmptyBaseMoveStudentInjectModifier implements IMoveStudentInjectModifier
{
    /**
     * Заполнение меток при печати выписки из сборного приказа или индивидуального приказа.
     *
     * @param modifier контейнер для меток
     * @param extract выписка. extract.entity - студент.
     */
    @Override
    void modularExtractModifier(RtfInjectModifier modifier, ModularStudentExtract extract)
    {
        /*
        // Например:
        modifier.put('personalNumber', extract.entity.personalNumber) // Личный номер студента

        // Например:
        if (extract.type.code == '1.2') {
            // Только для выписок из приказа "О предоставлении академического отпуска"
            final beginDate = ((WeekendStuExtract) extract).beginAcadWeekendDate
            modifier.put('beginWeekendDate', beginDate.format('dd.MM.yyyy')) // Дата начала академического отпуска
        }

        // Например:
        injectTerritorialOrgUnitSettlement(modifier, extract.entity.educationOrgUnit.territorialOrgUnit)
        */
    }

    /**
     * Заполнение табличных меток при печати выписки из сборного приказа.
     *
     * @param modifier контейнер для меток
     * @param extract выписка. extract.entity - студент.
     */
    @Override
    void modularExtractTableModifier(RtfTableModifier modifier, ModularStudentExtract extract)
    {
        /*
        // Например
        if (extract instanceof CommonStuExtract) {
            // Таблица нужна только для различных выписок о переводе
            modifier.put('RECTOR_VISA', [['Ректор', TopOrgUnit.instance.head?.employee?.person?.identityCard?.fio]] as String[][]) // ФИО Ректора
        } else {
            // Если это любая другая выписка, таблицу удаляем
            modifier.put('RECTOR_VISA')
        }
        */
    }

    /**
     * Заполнение меток при печати сборного приказа.
     *
     * @param modifier контейнер для меток
     * @param order приказ
     */
    @Override
    void modularOrderModifier(RtfInjectModifier modifier, StudentModularOrder order)
    {
        /*
        // Например
        modifier.put('orderDate', order.commitDate ? order.commitDate.format('d.M.yy') : '') // Дата приказа в нужном формате
        */
    }

    /**
     * Заполнение табличных меток при печати сборного приказа.
     *
     * @param modifier контейнер для меток
     * @param order приказ
     */
    @Override
    void modularOrderTableModifier(RtfTableModifier modifier, StudentModularOrder order)
    {

    }

    /**
     * Заполнение меток при печати индивидуального приказа.
     *
     * @param modifier контейнер для меток
     * @param order приказ
     * @param extract единственная выписка в приказе
     */
    @Override
    void individualOrderModifier(RtfInjectModifier modifier, StudentModularOrder order, ModularStudentExtract extract)
    {
        /* Например
        // Город территориального подразделения студента
        injectTerritorialOrgUnitSettlement(modifier, extract.entity.educationOrgUnit.territorialOrgUnit)
        */
    }

    /**
     * Заполнение табличных меток при печати индивидуального приказа.
     *
     * @param modifier контейнер для меток
     * @param order приказ
     * @param extract единственная выписка в приказе
     */
    @Override
    void individualOrderTableModifier(RtfTableModifier modifier, StudentModularOrder order, ModularStudentExtract extract)
    {

    }

    /**
     * Заполнение меток при печати выписки из списочного приказа.
     *
     * @param modifier контейнер для меток
     * @param extract выписка. extract.entity - студент.
     */
    @Override
    void listExtractModifier(RtfInjectModifier modifier, ListStudentExtract extract)
    {
        /*
        // Например
        modifier.put('territorialOrgUnit', extract.entity.educationOrgUnit.territorialOrgUnit.title) // Название филиала
        */
    }

    /**
     * Заполнение табличных меток при печати выписки из списочного приказа.
     *
     * @param modifier контейнер для меток
     * @param extract выписка. extract.entity - студент.
     */
    @Override
    void listExtractTableModifier(RtfTableModifier modifier, ListStudentExtract extract)
    {

    }

    /**
     * Заполнение меток при печати списочного приказа.
     *
     * @param modifier контейнер для меток
     * @param order списочный приказ
     * @param firstExtract первая выписка из первого параграфа
     */
    @Override
    void listOrderModifier(RtfInjectModifier modifier, StudentListOrder order, ListStudentExtract firstExtract)
    {
        /* Например
        // Берем оргюнит из приказа, либо территориальное подразделение студента из первой выписки, если приказ создан глобально (не на подразделении)
        injectTerritorialOrgUnitSettlement(modifier, order.orgUnit ?: firstExtract.entity.educationOrgUnit.territorialOrgUnit)
        */
    }

    /**
     * Заполнение табличных меток при печати списочного приказа.
     *
     * @param modifier контейнер для меток
     * @param order списочный приказ
     */
    @Override
    void listOrderTableModifier(RtfTableModifier modifier, StudentListOrder order)
    {

    }

    /**
     * Заполнение меток при печати параграфов, подпараграфов и подподпараграфов списочного приказа.
     *
     * @param modifier контейнер для меток
     * @param paragraph параграф списочного приказ
     * @param firstExtract первая выписка параграфа (подпараграфа, подподпараграфа)
     */
    @Override
    void listParagraphModifier(RtfInjectModifier modifier, StudentListParagraph paragraph, ListStudentExtract firstExtract)
    {
        /*
        Как правило, во всех выписках параграфа содержатся одинаковые данные, заполняемые для параграфа.
        Поэтому, заполняя метки параграфа, можно ориентироваться на данные в любой выписке.
        Исключения составляют параграфы, где для каждого студента вводятся значения. Например, размер стипендии.
        Получить список выписок параграфа можно через paragraph.extractList
        Получить список студентов в параграфе можно так:
        def studentList = MoveStudentDaoFacade.getMoveStudentDao().getStudentsByParagraph(paragraph.id)
        Примеры кода
        // В метку "newGroupTitle" для выписки о переводе с курса на следующий курс вставляем название новой группы
        {
            if (firstExtract instanceof CourseTransferExtStuListExtract) {
                modifier.put('newGroupTitle', firstExtract.groupNew.title)
            }
        }
        // Для всех видов параграфов добавляем метку "subjectCode", в которую записывается код текущего направления подготовки
        {
           def student = firstExtract.entity // студент
           def npp = student.educationOrgUnit // НПП студента (Параметры обучения студентов по направлению подготовки)
           def npv = npp.educationLevelHighSchool // НПв (Параметры выпуска студентов по направлению подготовки)
           def npm = npv.educationLevel // НПм (Направление подготовки министерского классификатора)
           def programSubject = npm.eduProgramSubject // Направление подготовки профессионального образования

           modifier.put('subjectCode', programSubject.subjectCode)
        }

      */
    }

    /**
     * Заполнение табличных меток при печати параграфов, подпараграфов и подподпараграфов списочного приказа.
     *
     * @param modifier контейнер для меток
     * @param paragraph параграф списочного приказ
     * @param firstExtract первая выписка параграфа (подпараграфа)
     */
    @Override
    void listParagraphTableModifier(RtfTableModifier modifier, StudentListParagraph paragraph, ListStudentExtract firstExtract)
    {

    }


    /* ---------------- Добавляйте свои методы, для повторного использования кода ------------------ */

    /**
     * Пример дополнительного пользовательского метода, который можно использовать внутри скрипта.
     * В метку "territorialOrgUnitSettlement" выводится населенный пункт оргюнита.
     * Вы можете добавлять любые другие методы, если, например, для разных видов приказов используется одна и та же логина.
     */
    static void injectTerritorialOrgUnitSettlement(RtfInjectModifier modifier, OrgUnit orgUnit)
    {
        // Пытаемся взять город из фактического, юридического или почтового адреса последовательно.
        // Если нигде нет адреса или города, метка заменится на пустое значение.
        modifier.put('territorialOrgUnitSettlement',
                orgUnit.address?.settlement?.titleWithType ?:
                        orgUnit.legalAddress?.settlement?.titleWithType ?:
                                orgUnit.postalAddress?.settlement?.titleWithType
        )
    }
}
