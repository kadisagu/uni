package movestudent.scripts

import com.google.common.collect.Iterables
import com.google.common.collect.LinkedListMultimap
import com.google.common.collect.Maps
import com.google.common.collect.Multimap
import org.apache.commons.codec.binary.Base64
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.CoreExceptionUtils
import org.tandemframework.core.component.BusinessComponentUtils
import org.tandemframework.core.entity.OrderDirection
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.meta.entity.PropertyType
import org.tandemframework.core.util.BatchUtils
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLFunctions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.hibsupport.dql.IDQLStatement
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer
import org.tandemframework.shared.fias.base.entity.AddressBase
import org.tandemframework.shared.fias.base.entity.AddressInter
import org.tandemframework.shared.fias.base.entity.AddressItem
import org.tandemframework.shared.fias.base.entity.AddressRu
import org.tandemframework.shared.fias.base.entity.AddressString
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.Person
import org.tandemframework.shared.person.base.entity.PersonContactData
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes
import ru.tandemservice.movestudent.base.bo.MovestudentSystemAction.logic.ScriptResultData
import ru.tandemservice.movestudent.entity.AbstractStudentExtract
import ru.tandemservice.movestudent.entity.AbstractStudentOrder
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType
import ru.tandemservice.uni.entity.catalog.Course
import ru.tandemservice.uni.entity.catalog.EducationLevels
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI
import ru.tandemservice.uni.entity.catalog.StudentStatus
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes
import ru.tandemservice.uni.entity.catalog.codes.DevelopTechCodes
import ru.tandemservice.uni.entity.education.EducationOrgUnit
import ru.tandemservice.uni.entity.education.StudentCustomState
import ru.tandemservice.uni.entity.employee.OrderData
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.MovestudentMSRSettingsManager
import ru.tandemservice.movestudent.base.bo.MovestudentSystemAction.MovestudentSystemActionManager
import ru.tandemservice.movestudent.base.bo.MovestudentSystemAction.logic.script.IMovestudentUnloadScript
import ru.tandemservice.movestudent.entity.ExportMSRSettings
import ru.tandemservice.movestudent.ws.gupmsr.entity.Address
import ru.tandemservice.movestudent.ws.gupmsr.entity.ClientInfo
import ru.tandemservice.movestudent.ws.gupmsr.entity.Contacts
import ru.tandemservice.movestudent.ws.gupmsr.entity.Document
import ru.tandemservice.movestudent.ws.gupmsr.entity.FileInfo
import ru.tandemservice.movestudent.ws.gupmsr.entity.Name
import ru.tandemservice.movestudent.ws.gupmsr.entity.ObjectFactory
import ru.tandemservice.movestudent.ws.gupmsr.entity.OrderInfo
import ru.tandemservice.movestudent.ws.gupmsr.entity.Record
import ru.tandemservice.movestudent.ws.gupmsr.entity.RecordList
import ru.tandemservice.movestudent.ws.gupmsr.entity.UniversityInfo
import ru.tandemservice.uni.util.FilterUtils
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex
import ru.tandemservice.unimove.entity.catalog.OrderStates
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes

import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller
import javax.xml.datatype.DatatypeFactory
import javax.xml.datatype.XMLGregorianCalendar
import java.util.regex.Matcher
import java.util.regex.Pattern

import static org.tandemframework.hibsupport.dql.DQLExpressions.*


return new UnloadStudentForGupMsrScript();

class UnloadStudentForGupMsrScript implements IMovestudentUnloadScript
{
    private static String DISTANT_FORM = "DISTANT_ONLINE";

    private static final Integer SEX_MALE = 1;
    private static final Integer SEX_FEMALE = 2;

    private static final Long LEARNING = 1L;
    private static final Long ACADEMIC_HOLIDAY = 2L;
    private static final Long EXCLUDE = 3L;
    private static final Long RELEASED = 4L;

    private static final Integer ENROLLMENT_ORDER = 1;
    private static final Integer RESTORED_ORDER = 2;
    private static final Integer EXCLUDE_ORDER = 3;
    private static final Integer ACADEMIC_HOLIDAY_ORDER = 4;
    private static final Integer TRANSFER_GROUP_ORDER = 5;
    private static final Integer TRANSFER_COURSE_ORDER = 6;
    private static final Integer RELEASE_ORDER = 7;

    private static final Integer PROGRAMMA_BAKALAVRIATA = 1;
    private static final Integer PROGRAMMA_PODGOTOVKI_SPETSIALISTOV = 2;
    private static final Integer PROGRAMMA_MAGISTRATURY = 3;
    private static final Integer PROGRAMMA_ASPIRANTURY_ADYUNKTURY = 4;
    private static final Integer PROGRAMMA_ORDINATURY = 5;
    private static final Integer PROGRAMMA_ASSISTENTURY_STAGHIROVKY = 6; // Добавить в коде проверку, если появится в системе такая программа
    private static final Integer PROGRAMMA_SPO = 7;
    private static final Integer PROGRAMMA_OBSHEGO_OBRAZOVANIA = 8;

    private Map<String, Integer> _sexMap = Maps.newHashMap();
    private Map<String, String> _developFormMap = Maps.newHashMap();
    private Map<String, Integer> _documentMap = Maps.newHashMap();
    private Map<StudentStatus, Long> _statusMap = Maps.newHashMap();
    private Map<StudentExtractType, Integer> _orderMap = Maps.newHashMap();
    private Map<OrgUnit, String> _orgUnitMap = Maps.newHashMap();
    private Map<Long, Integer> _terrOuAddressMap = Maps.newHashMap();
    private ExportMSRSettings _settings;
    private Map<StudentStatus, Set<StudentExtractType>> statusToOrderRelation = Maps.newHashMap();
    private OrgUnit _formativeOrgUnit;

    private void initDirectoryMaps()
    {
        _sexMap.put(SexCodes.MALE, SEX_MALE);
        _sexMap.put(SexCodes.FEMALE, SEX_FEMALE);

        _developFormMap.put(DevelopFormCodes.FULL_TIME_FORM, "DAYTIME");
        _developFormMap.put(DevelopFormCodes.CORESP_FORM, "DISTANT");
        _developFormMap.put(DevelopFormCodes.PART_TIME_FORM, "EVENING_TIME");
        _developFormMap.put(DevelopFormCodes.EXTERNAL_FORM, "EXTERNAL");
        _developFormMap.put(DevelopFormCodes.APPLICANT_FORM, "DISTANT_ONLINE");

        _documentMap.put(IdentityCardTypeCodes.SVIDETELSTVO_O_ROJDENII, 3);
        _documentMap.put(IdentityCardTypeCodes.PASPORT_GRAJDANINA_INOSTRANNOGO_GOSUDARSTVA, 10);
        _documentMap.put(IdentityCardTypeCodes.VID_NA_JITELSTVO, 12);
        _documentMap.put(IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII, 21);
        _documentMap.put(IdentityCardTypeCodes.BEZ_UDOSTOVERENIYA, 91);
        _documentMap.put(IdentityCardTypeCodes.DRUGOY_DOKUMENT, 91);
        _documentMap.put(IdentityCardTypeCodes.ROSSIYSKIY_ZAGRANICHNYY_PASPORT, 22);
        _documentMap.put(IdentityCardTypeCodes.VREMENNOE_UDOSTOVERENIE, 14);
        _documentMap.put(IdentityCardTypeCodes.PASPORT_GRAJDANINA_S_S_S_R, 91);

        _statusMap = MovestudentMSRSettingsManager.instance().dao().getMapStudentStatusCodes()
        _orderMap = MovestudentMSRSettingsManager.instance().dao().getMapOrdersCodeRelation();
        _orgUnitMap = MovestudentMSRSettingsManager.instance().dao().getMapCodesForFormativeOrgUnit();
        _terrOuAddressMap = MovestudentMSRSettingsManager.instance().dao().getTerritorialOrgUnitAddresses(_formativeOrgUnit);
        _settings = MovestudentMSRSettingsManager.instance().dao().getSettings();
        if (_statusMap.size() == 0 || _orderMap.size() == 0 || _orgUnitMap.size() == 0 || _settings == null)
            throw new ApplicationException("Необходимо заполнить настройки для передачи данных в ГУП МСР");

        initRelationMap();
    }

    /**
     * Собираем приказы, изменяющие статус студента
     */
    private void initRelationMap()
    {
        for (Map.Entry<StudentExtractType, Integer> entry :_orderMap.entrySet())
        {
            switch (entry.getValue())
            {
                case ENROLLMENT_ORDER:
                case RESTORED_ORDER:
                    addItemToRelationMap(statusToOrderRelation, LEARNING, entry.getKey());
                    break;
                case EXCLUDE_ORDER:
                    addItemToRelationMap(statusToOrderRelation, EXCLUDE, entry.getKey());
                    break;
                case ACADEMIC_HOLIDAY_ORDER:
                    addItemToRelationMap(statusToOrderRelation, ACADEMIC_HOLIDAY, entry.getKey());
                    break;
                case RELEASE_ORDER:
                    addItemToRelationMap(statusToOrderRelation, RELEASED, entry.getKey());
                    break;
            }
        }
    }

    private void addItemToRelationMap(Map<StudentStatus, Set<StudentExtractType>> map, Long key, StudentExtractType value)
    {
        for (Map.Entry<StudentStatus, Long> entry : _statusMap.entrySet())
        {
            if (entry.getValue().equals(key))
            {
                Set<StudentExtractType> item = map.get(entry.getKey());
                if (item == null) map.put(entry.getKey(), item = new HashSet<>());
                item.add(value);
            }
        }
    }


    private String getDevelopForm(String form, String tech)
    {
        if (!tech.equals(DevelopTechCodes.GENERAL))
            return DISTANT_FORM;

        return _developFormMap.get(form);
    }

    private ru.tandemservice.movestudent.ws.gupmsr.entity.File getStudents(boolean isAllStudent, boolean onlyFullData, List<StudentCustomStateCI> studentCustomStates, Session session)
    {
        Map<Long, StudentData> studentsMap  = getStudentsData(isAllStudent, onlyFullData, studentCustomStates, session);

        final Map<Long, EnrOrderData> enrollmentOrderMap = new HashMap<>(studentsMap.size());

        Multimap<Long, LastOrdersData> orderDataMap = getOrdersData(studentsMap.keySet(), enrollmentOrderMap, session);

        ObjectFactory factory = new ObjectFactory();

        RecordList resultList = createRecordList(studentsMap, orderDataMap, enrollmentOrderMap, factory, onlyFullData);

        ru.tandemservice.movestudent.ws.gupmsr.entity.File file = factory.createFile();
        file.setRecordList(resultList);
        FileInfo fInfo = factory.createFileInfo();
        fInfo.setFileSender(_settings.getCodeSender());
        fInfo.setRecordCount(resultList.getRecord().size());
        fInfo.setVersion(_settings.getFileVersion());
        file.setFileInfo(fInfo);

        return file;

    }


    private Map<Long, StudentData> getStudentsData(boolean isAllStudent, boolean onlyFullData, List<StudentCustomStateCI> studentCustomStates, Session session)
    {
        DQLSelectColumnNumerator studentDQL = new DQLSelectColumnNumerator(
                new DQLSelectBuilder()
                        .fromEntity(Student.class, "s")
                        .joinPath(DQLJoinType.inner, Student.person().fromAlias("s"), "p")
                        .joinPath(DQLJoinType.inner, Person.identityCard().fromAlias("p"), "ic")
                        .joinPath(DQLJoinType.left, IdentityCard.address().fromAlias("ic"), "ad")
                        .joinPath(DQLJoinType.left, Person.contactData().fromAlias("p"), "cd")
                        .joinPath(DQLJoinType.left, Person.address().fromAlias("p"), "adf")
                        .joinPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias("s"), "eou", true)
                        .joinEntity("s", DQLJoinType.left, OrderData.class, "lo", eq(property(OrderData.student().id().fromAlias("lo")), property(Student.id().fromAlias("s"))))
                        .joinPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().educationLevel().fromAlias("eou"), "edl")
                        .joinPath(DQLJoinType.left, EducationLevels.eduProgramSubject().fromAlias("edl"),"eps")
                        .joinPath(DQLJoinType.left, EduProgramSubject.subjectIndex().fromAlias("eps"), "epsi", true)
                        .joinPath(DQLJoinType.left, EduProgramSubjectIndex.programKind().fromAlias("epsi"), "epk", true)

                        .where(eq(property("s", Student.educationOrgUnit().formativeOrgUnit()), value(_formativeOrgUnit)))
                        .where(eq(property("s", Student.archival()), value(Boolean.FALSE)))
                        .where(DQLExpressions.in(property(Student.status().id().fromAlias("s")), _statusMap.keySet())));
        if (onlyFullData)
        {
            studentDQL.getDql().where(isNotNull(property("ic", IdentityCard.birthDate())))
                    .where(ne(property("ic", IdentityCard.cardType().code()), value(IdentityCardTypeCodes.BEZ_UDOSTOVERENIYA)))
                    .where(isNotNull(property("ic", IdentityCard.number())))
                    .where(isNotNull(property("ic", IdentityCard.issuancePlace())));

            studentDQL.getDql().where(or(
                    ne(property("ic", IdentityCard.cardType().code()), value(IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII)),
                    and(eq(property("ic", IdentityCard.cardType().code()), value(IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII)),
                        isNotNull(property("ic", IdentityCard.issuanceDate())),
                        isNotNull(property("ic", IdentityCard.seria())),
                        isNotNull(property("ic", IdentityCard.issuanceCode())))
                    ));


        }

        if (!studentCustomStates.isEmpty())
        {
            DQLSelectBuilder customStateSubBuilder = new DQLSelectBuilder().fromEntity(StudentCustomState.class, "state")
            .where(DQLExpressions.in(property("state", StudentCustomState.customState()), studentCustomStates))
            Date currentDate = new Date();
            FilterUtils.applyIntersectPeriodFilterNullSafe(customStateSubBuilder, "state", StudentCustomState.P_BEGIN_DATE, StudentCustomState.P_END_DATE, currentDate, currentDate)

            studentDQL.getDql().where(exists(customStateSubBuilder.where(eq(property("state", StudentCustomState.student().id()), property("s.id"))).buildQuery()));
        }

        studentDQL.getDql().order(property(IdentityCard.lastName().fromAlias("ic")))
                .order(property(IdentityCard.firstName().fromAlias("ic")))
                .order(property(IdentityCard.middleName().fromAlias("ic")));

        final int studIdCol = studentDQL.column(property("s.id"));
        final int studStatusCol = studentDQL.column(property(Student.status().fromAlias("s")));
        final int studCourse = studentDQL.column(property(Student.course().fromAlias("s")));
        final int idCardCol = studentDQL.column(property("ic"));
        final int addressCol = studentDQL.column(property("ad"));
        final int addFactCol = studentDQL.column(property("adf"));
        final int contactDataCol = studentDQL.column(property("cd"));
        final int snilsCol = studentDQL.column(property(Person.snilsNumber().fromAlias("p")));
        final int devFormCol = studentDQL.column(property(EducationOrgUnit.developForm().code().fromAlias("eou")));
        final int devTechCol = studentDQL.column(property(EducationOrgUnit.developTech().code().fromAlias("eou")));
        final int enrollDateCol = studentDQL.column(property(OrderData.eduEnrollmentOrderDate().fromAlias("lo")));
        final int studPersonalNumberCol = studentDQL.column(property("s", Student.personalNumber()));
        final int entranceYearCol = studentDQL.column(property("s", Student.entranceYear()));
        final int eduProgramKindCol = studentDQL.column(property("epk"))
        final int terOUCol = studentDQL.column(property("eou", EducationOrgUnit.territorialOrgUnit().id()))


        if (!isAllStudent)
            studentDQL.getDql().where(eq(property("s", Student.status().active()), value(Boolean.TRUE)));

        List<Object[]> studentList = studentDQL.getDql().createStatement(session).list()

        Map<Long, StudentData> mapStudents = new LinkedHashMap<>(studentList.size());

        for (Object[] item : studentList)
        {
            final Long studentId = (Long) item[studIdCol];
            StudentData data = mapStudents.get(studentId);
            if (data == null)
                mapStudents.put(studentId, data = new StudentData());

            data.setCard((IdentityCard)item[idCardCol]);
            data.setAddressFact((AddressBase) item[addFactCol]);
            data.setAddressReg((AddressBase) item[addressCol]);
            data.setContacts((PersonContactData)item[contactDataCol]);
            data.setStStatus((StudentStatus) item[studStatusCol]);
            data.setDevForm((String) item[devFormCol]);
            data.setDevTech((String) item[devTechCol]);
            data.setSnils((String)item[snilsCol]);
            data.setCourse((Course) item[studCourse]);
            data.setEnrollmentDate((Date) item[enrollDateCol]);
            data.setStudentPersonalNumber((String) item[studPersonalNumberCol]);
            data.setEntranceYear((int)item[entranceYearCol]);
            data.setEduProgramKind((EduProgramKind) item[eduProgramKindCol])
            data.setTerrOuId((Long)item[terOUCol])
        }

        return mapStudents;

    }

    private Multimap<Long, LastOrdersData> getOrdersData(Set<Long> studIdList, final Map<Long, EnrOrderData> enrollmentMap, Session session)
    {
        //Информация о последнем приказе
        final Multimap<Long, LastOrdersData> orderMap = LinkedListMultimap.create(); //<Long, LastOrdersData>(studIdList.size());

        DQLSelectColumnNumerator orderDQL = new DQLSelectColumnNumerator(
                new DQLSelectBuilder()
                        .fromDataSource(
                        new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "ext")
                                .joinPath(DQLJoinType.inner, AbstractStudentExtract.paragraph().order().fromAlias("ext"), "order")
                                .joinPath(DQLJoinType.inner, AbstractStudentOrder.state().fromAlias("order"), "state")
                                .column(property("order"), "or")
                                .column(property(AbstractStudentExtract.entity().id().fromAlias("ext")), "st")
                                .column(property(AbstractStudentExtract.type().fromAlias("ext")), "extType")
                                .column(DQLFunctions.rowNumberBuilder().partition(property("ext", AbstractStudentExtract.entity()))
                                .partition(property(AbstractStudentExtract.type().fromAlias("ext")))
                                .order(property("order", AbstractStudentOrder.commitDate()), OrderDirection.desc).build(), "num")
                                .where(eq(property("state", OrderStates.code()), value(OrderStatesCodes.FINISHED)))
                                .where(or(DQLExpressions.in(property(AbstractStudentExtract.type().fromAlias("ext")), _orderMap.keySet()),
                                DQLExpressions.in(property(AbstractStudentExtract.type().parent().fromAlias("ext")), _orderMap.keySet())))
                                .order(property(AbstractStudentOrder.commitDate().fromAlias("order")), OrderDirection.desc)
                                .buildQuery(),
                        "data"
                )
                        .where(DQLExpressions.in(property("data.st"), parameter("studentIds", PropertyType.LONG)))
                        .where(eq(property("data.num"), value(1))));
        final int orderCol = orderDQL.column("data.or");
        final int studIdCol = orderDQL.column("data.st");
        final int extTypeCol = orderDQL.column("data.extType");

        final IDQLStatement orderStatement = orderDQL.getDql().createStatement(session);

        //информация о зачислении

//        DQLSelectColumnNumerator enrollmentDQL = new DQLSelectColumnNumerator(
//                new DQLSelectBuilder()
//                        .fromDataSource(
//                        new DQLSelectBuilder()
//                                .fromEntity(EnrEnrollmentExtract.class, "ext")
//                                .joinPath(DQLJoinType.inner, EnrEnrollmentExtract.paragraph().order().fromAlias("ext"), "order")
//                                .joinPath(DQLJoinType.inner, EnrOrder.state().fromAlias("order"), "state")
//                                .column(property("order"), "ord")
//                                .column(property(EnrEnrollmentExtract.student().id().fromAlias("ext")), "stId")
//                                .column(DQLFunctions.rowNumberBuilder().partition(property(EnrEnrollmentExtract.student().fromAlias("ext")))
//                                .order(property(EnrOrder.commitDate().fromAlias("order")), OrderDirection.desc)
//                                .order(property(EnrOrder.id().fromAlias("order"))).build(), "number")
//
//                                .where(eq(property("state", OrderStates.code()), value(OrderStatesCodes.FINISHED)))
//
//                                .order(property(EnrEnrollmentExtract.student().id().fromAlias("ext")), OrderDirection.desc).buildQuery(), "enroll")
//                        .where(DQLExpressions.in(property("enroll.stId"), parameter("studentsId", PropertyType.LONG)))
//                        .where(eq(property("enroll.number"), value(1))));

        DQLSelectBuilder enrollmentBuilder = new DQLSelectBuilder().fromEntity(OrderData.class, "ord")
                                                .where(DQLExpressions.in(property("ord", OrderData.student().id()), parameter("studentsId", PropertyType.LONG)))

//        final int enStId = enrollmentDQL.column(property("enroll.stId"));
//        final int enrOrderCol = enrollmentDQL.column(property("enroll.ord"));

        final IDQLStatement enrollmentStatement = enrollmentBuilder.createStatement(session);

        BatchUtils.execute(studIdList, 500, new BatchUtils.Action<Long>(){
            @Override
            public void execute(final Collection<Long> elements)
            {
                orderStatement.setParameter("studentIds", elements);
                for (Object[] item : orderStatement.<Object[]>list())
                {
                    Long studId = (Long)item[studIdCol];
                    AbstractStudentOrder order = (AbstractStudentOrder) item[orderCol];
                    StudentExtractType type = (StudentExtractType) item[extTypeCol];
                    LastOrdersData data = new LastOrdersData();

                    data.setLastOrder(order);
                    data.setLastOrderType(type);
                    orderMap.put(studId, data);
                }

                enrollmentStatement.setParameter("studentsId", elements);

                for(OrderData item : enrollmentStatement.<OrderData>list())
                {
                    EnrOrderData order = new EnrOrderData()
                    order.setEnrollDate(item.getEduEnrollmentOrderEnrDate())
                    order.setOrderNumber(item.getEduEnrollmentOrderNumber())
                    order.setOrderDate(item.getEduEnrollmentOrderDate())
                    enrollmentMap.put(item.getStudent().getId(), order);
                }
            };
        });

        return orderMap;
    }



    private RecordList createRecordList(Map<Long, StudentData> studMap, Multimap<Long, LastOrdersData> ordersDataMap,
                                        Map<Long, EnrOrderData> enrollmentMap, ObjectFactory factory, boolean onlyFullData)
    {
        RecordList resultList = new RecordList();

        int count = 1;

        for (Map.Entry<Long, StudentData> item : studMap.entrySet())
        {
            Record record = factory.createRecord();
            record.setRecordId(count);
            ClientInfo info = createClientInfo(factory, item.getValue());
            record.setClientInfo(info);

            //Инфа об обучении
            UniversityInfo uInfo = createUniversityInfo(factory, ordersDataMap, item, enrollmentMap, onlyFullData);
            if (uInfo == null && onlyFullData) continue;
            record.setUniversityInfo(uInfo);
            resultList.getRecord().add(record);
            count++;

        }
        return resultList;
    }

    private UniversityInfo createUniversityInfo(ObjectFactory factory, Multimap<Long, LastOrdersData> ordersDataMap,
                                                Map.Entry<Long, StudentData> item, Map<Long, EnrOrderData> enrollmentMap, boolean onlyFullData)
    {
        StudentData currentData = item.getValue();
        UniversityInfo uInfo = factory.createUniversityInfo();
        uInfo.setUniversityCode(_settings.getCodeHighSchool());
        String code = _orgUnitMap.get(_formativeOrgUnit);
        if (code == null && onlyFullData) return null;
        uInfo.setFacultyCode(factory.createUniversityInfoFacultyCode(code));
        uInfo.setFacultyName(factory.createUniversityInfoFacultyName(_formativeOrgUnit.getTitle()));
        uInfo.setCourse(currentData.getCourse().getIntValue());
        uInfo.setStudentId(currentData.getStudentPersonalNumber());

        Integer educationProgram = 0;

        if (currentData.getEduProgramKind() == null)
        {
            if (onlyFullData) return null;
            educationProgram = 0 //уточнить, т.к. в выгрузке поле обязательное, а в системе м.б. null
        }
        else if (currentData.getEduProgramKind().isProgramBachelorDegree())
            educationProgram = PROGRAMMA_BAKALAVRIATA;
        else if (currentData.getEduProgramKind().isProgramSpecialistDegree())
            educationProgram = PROGRAMMA_PODGOTOVKI_SPETSIALISTOV;
        else if (currentData.getEduProgramKind().isProgramMasterDegree())
            educationProgram = PROGRAMMA_MAGISTRATURY;
        else if (currentData.getEduProgramKind().isProgramPostgraduate())
            educationProgram = PROGRAMMA_ASPIRANTURY_ADYUNKTURY
        else if (currentData.getEduProgramKind().isProgramTraineeship())
            educationProgram = PROGRAMMA_ORDINATURY
        else if (currentData.getEduProgramKind().isProgramBasic())
            educationProgram = PROGRAMMA_OBSHEGO_OBRAZOVANIA
        else if (currentData.getEduProgramKind().isProgramSecondaryProf())
            educationProgram = PROGRAMMA_SPO
        uInfo.setEducationProgram(educationProgram)

        //Территория обучения
        uInfo.setLocationUniversity( _terrOuAddressMap.get(currentData.getTerrOuId()));

        //Статус обучения
        UniversityInfo.Status status = createStatusInfo(factory, currentData, ordersDataMap, item.getKey(), enrollmentMap);
        uInfo.setStatus(status);

        //Данные о зачислении
        EnrOrderData enrollmentOrd = enrollmentMap.get(item.getKey());
        if (enrollmentOrd != null && (enrollmentOrd.getEnrollDate() != null || enrollmentOrd.getOrderDate() != null))
            uInfo.setStartDate(getXmlGregorianCalendar(enrollmentOrd.getEnrollDate()? enrollmentOrd.getEnrollDate() : enrollmentOrd.getOrderDate()));
        else
        {
            //Устанавливаем датой 1 сентября года начала обучения
            Calendar calendar = Calendar.getInstance();
            calendar.set(currentData.getEntranceYear(), 8, 1);
            uInfo.setStartDate(getXmlGregorianCalendar(calendar.getTime()));
        }

        uInfo.setEducationType(getDevelopForm(currentData.getDevForm(), currentData.getDevTech()));

        //последний приказ по студенту
        OrderInfo order = createLastOrderInfo(ordersDataMap, item.getKey(), factory, enrollmentOrd, onlyFullData);
        if (order != null)
            uInfo.getOrder().add(order);
        return uInfo;
    }

    private UniversityInfo.Status createStatusInfo(ObjectFactory factory, StudentData currentData, Multimap<Long, LastOrdersData> ordersDataMap,Long studId, Map<Long, EnrOrderData> enrollmentMap)
    {
        UniversityInfo.Status status = factory.createUniversityInfoStatus();
        status.setCode(_statusMap.get(currentData.getStStatus()));

        Collection<LastOrdersData> ordersList = ordersDataMap.get(studId);
        LastOrdersData lastStatOrd = null;
        for (LastOrdersData ordItem : ordersList)
        {
            if (statusToOrderRelation.get(currentData.getStStatus()) != null && statusToOrderRelation.get(currentData.getStStatus()).contains(ordItem.getLastOrderType()))
            {
                lastStatOrd = ordItem;
                break;
            }
        }
        Date statusDate = (lastStatOrd != null ? lastStatOrd.getLastOrder().getCommitDate() : null);

        EnrOrderData enrollmentOrd = enrollmentMap.get(studId);

        if (statusDate == null && status.getCode() == LEARNING)
            statusDate = (enrollmentOrd != null ? enrollmentOrd.getOrderDate() : null);

        XMLGregorianCalendar statDt = null;
        if (statusDate != null)
            statDt = getXmlGregorianCalendar(statusDate);
        else
        {
            //Устанавливаем датой 1 сентября года начала обучения
            Calendar calendar = Calendar.getInstance();
            calendar.set(currentData.getEntranceYear(), 8,1);
            statDt = getXmlGregorianCalendar(calendar.getTime());
        }

        status.setDate(statDt);
        return status;
    }

    /**
     * Информация о последнем приказе
     */
    private OrderInfo createLastOrderInfo(Multimap<Long, LastOrdersData> ordersDataMap, Long studId, ObjectFactory factory, EnrOrderData enrollmentOrd, boolean onlyFullData)
    {
        OrderInfo order = null;
        if (ordersDataMap == null || factory == null || studId == null)
            return null;
        Collection<LastOrdersData> lastOrdersData = ordersDataMap.get(studId);
        if (lastOrdersData != null && !lastOrdersData.isEmpty())
        {
           for (LastOrdersData lastOrder : lastOrdersData)
            {
                order = factory.createOrderInfo();
                if (null != lastOrder.getLastOrder().getCommitDate())
                    order.setDate(getXmlGregorianCalendar(lastOrder.getLastOrder().getCommitDate()));
                else if (onlyFullData) continue;

                if (lastOrder.getLastOrderType() != null)
                {
                    Integer value = _orderMap.get(lastOrder.getLastOrderType());
                    if (value == null)
                        value = _orderMap.get(lastOrder.getLastOrderType().getParent());
                    order.setType(value);
                }
                else if (onlyFullData)
                    continue;

                if (null != lastOrder.getLastOrder().getNumber())
                    order.setNumber(lastOrder.getLastOrder().getNumber());
                else if (!onlyFullData) order.setNumber(null);
                else
                    continue;
                break;
            }
        }
        else if (enrollmentOrd != null)
        {
            order = factory.createOrderInfo();
            if (null != enrollmentOrd.getOrderDate())
                order.setDate(getXmlGregorianCalendar(enrollmentOrd.getOrderDate()));
            else if (onlyFullData) return null;

            order.setType(1);

            if (null != enrollmentOrd.getOrderNumber())
                order.setNumber(enrollmentOrd.getOrderNumber());
            else if (!onlyFullData)
                order.setNumber(null);
            else return null;
        }
        return order;
    }

    /**
     * Данные по студенту
     */
    private ClientInfo createClientInfo(ObjectFactory factory, StudentData currentData)
    {
        ClientInfo info = factory.createClientInfo();

        //Имя
        Name name = factory.createName();
        name.setLastName(currentData.getCard().getLastName());
        name.setFirstName(currentData.getCard().getFirstName());
        if (currentData.getCard().getMiddleName() != null)
            name.setMiddleName(factory.createNameMiddleName(currentData.getCard().getMiddleName()));

        info.setName(name);

        //дата рождения
        info.setDateOfBirth(getXmlGregorianCalendar(currentData.getCard().getBirthDate()));


        //пол
        info.setSex(_sexMap.get(currentData.getCard().getSex().getCode()));

        //Документ
        Document document = factory.createDocument();
        document.setCode(_documentMap.get(currentData.getCard().getCardType().getCode()));
        if (currentData.getCard().getSeria() != null)
            document.setSeries(factory.createDocumentSeries(currentData.getCard().getSeria()));
        document.setNumber(currentData.getCard().getNumber());

        document.setIssueDate(factory.createDocumentIssueDate(getXmlGregorianCalendar(currentData.getCard().getIssuanceDate())));

        if (currentData.getCard().getIssuancePlace() != null)
            document.setIssuedBy(currentData.getCard().getIssuancePlace());

        if (currentData.getCard().getIssuanceCode() != null)
            document.setUnitCode(factory.createDocumentUnitCode(currentData.getCard().getIssuanceCode()));

        info.setDocument(document);

        //Адрес регистрации
        Address addressReg = fillAddress(factory, currentData.getAddressReg());

        info.setRegistrationAddress(factory.createClientInfoRegistrationAddress(addressReg));

        //Адрес фактический
        Address addressFact = fillAddress(factory, currentData.getAddressFact());

        info.setResidenceAddress(factory.createClientInfoResidenceAddress(addressFact));

        if (null != currentData.getCard().getPhoto())
            info.setPhoto(factory.createClientInfoPhoto(Base64.encodeBase64(currentData.getCard().getPhoto().getContent())));
        else info.setPhoto(factory.createClientInfoPhoto(null));


        //Контакты
        Contacts contacts = factory.createContacts();
        if (currentData.getContacts() != null)
        {
            if (currentData.getContacts().getEmail() != null) contacts.setEmail(factory.createContactsEmail(prepareMail(currentData.getContacts().getEmail())));
            if (currentData.getContacts().getPhoneMobile() != null) contacts.setMobilePhone(factory.createContactsMobilePhone(preparePhoneNumber(currentData.getContacts().getPhoneMobile())));
            if (currentData.getContacts().getPhoneDefault() != null) contacts.setPhone(factory.createContactsPhone(preparePhoneNumber(currentData.getContacts().getPhoneDefault())));
        }
        info.setContacts(factory.createClientInfoContacts(contacts));

        //СНИЛС
        info.setInsuranceNumber(factory.createClientInfoInsuranceNumber(prepareSnils(currentData.getSnils())));

        return info;
    }

    /**
     * Форматирование номера СНИЛС
     * @param snils
     * @return
     */
    private String prepareSnils(String snils)
    {
        if (StringUtils.isEmpty(snils)) return null;
        Pattern pattern = Pattern.compile("\\d{3}-\\d{3}-\\d{3} \\d{2}");
        Matcher matcher = pattern.matcher(snils);
        if (matcher.find()) return snils;

        snils = snils.replaceAll("[\\s\\-]", "");

        if (snils.length() == 11)
            return snils.substring(0, 3) + "-" + snils.substring(3, 6) + "-" + snils.substring(6, 9) + " " + snils.substring(9);
        return null;
    }

    //проверка адреса eMail
    private String prepareMail(String mail)
    {
        Pattern pattern = Pattern.compile("[^@]+@[^\\.]+\\..+");
        Matcher matcher = pattern.matcher(mail);
        if (matcher.find()) return mail;
        else return null;
    }

    //форматирование номера телефона к нужному виду
    private String preparePhoneNumber(String phoneNumber)
    {
        if (StringUtils.isEmpty(phoneNumber)) return "";
        phoneNumber = phoneNumber.replaceAll("[() -]", "");
        if (phoneNumber.startsWith("+7") && phoneNumber.length() ==12) return phoneNumber;
        else if (phoneNumber.startsWith("7") && phoneNumber.length() == 11) return "+" + phoneNumber;
        else if (phoneNumber.startsWith("8") && phoneNumber.length() == 11) return phoneNumber;
        else if (phoneNumber.length() == 10) return "8"+phoneNumber;
        return null;
    }

    /**
     * Заполняем адрес
     */
    private Address fillAddress(ObjectFactory factory, AddressBase currentAddress)
    {

        Address address = factory.createAddress();
        if (currentAddress != null)
        {
            if (currentAddress instanceof AddressString)
            {
                address.setAddressText(((AddressString)currentAddress).getTitle());
            }
            else if (currentAddress instanceof AddressInter)
            {
                address.setAddressText(((AddressInter)currentAddress).getTitle());
            }
            else if (currentAddress instanceof AddressRu)
            {
                address = new Address();
                AddressRu addressRu = (AddressRu) currentAddress;
                address.setCountryName(addressRu.getCountry().getTitle());


                if (addressRu.getSettlement() != null)
                {
                    List<AddressItem> addressItemList = new ArrayList<>();
                    AddressItem parentAddrItem = addressRu.getSettlement();
                    while (null != parentAddrItem)
                    {
                        addressItemList.add(parentAddrItem);
                        parentAddrItem = parentAddrItem.getParent();
                    }

                    //для сельской местности
                    if (addressItemList.get(0).getAddressType().isCountryside())
                    {
                        address.setPlaceName(addressItemList.get(0).getTitle() + " " + addressItemList.get(0).getAddressType().getShortTitle());
                        if (addressItemList.size() == 2)
                        {
                            //область
                            address.setRegionName(addressItemList.get(1).getTitle() + " " + addressItemList.get(1).getAddressType().getShortTitle());
                        }
                        else if (addressItemList.size() == 3)
                        {
                            //область
                            address.setRegionName(addressItemList.get(2).getTitle() + " " + addressItemList.get(2).getAddressType().getShortTitle());
                            address.setDistrictName(addressItemList.get(1).getTitle() + " " + addressItemList.get(1).getAddressType().getShortTitle());
                        }
                        else if (addressItemList.size() == 4)
                        {
                            //область
                            address.setRegionName(addressItemList.get(3).getTitle() + " " + addressItemList.get(3).getAddressType().getShortTitle());
                            //район
                            address.setDistrictName(addressItemList.get(2).getTitle() + " " + addressItemList.get(2).getAddressType().getShortTitle());
                            address.setCityName(addressItemList.get(1).getTitle() + " " + addressItemList.get(1).getAddressType().getShortTitle());
                        }
                    }
                        //для города
                    else
                    {
                        address.setCityName(addressItemList.get(0).getTitle() + " " + addressItemList.get(0).getAddressType().getShortTitle());
                        if (addressItemList.size() == 2)
                        {
                            //область
                            address.setRegionName(addressItemList.get(1).getTitle() + " " + addressItemList.get(1).getAddressType().getShortTitle());
                        }
                        else if (addressItemList.size() >= 3)
                        {
                            //район
                            address.setDistrictName(addressItemList.get(1).getTitle() + " " + addressItemList.get(1).getAddressType().getShortTitle());
                            //область
                            address.setRegionName(addressItemList.get(2).getTitle() + " " + addressItemList.get(2).getAddressType().getShortTitle());
                        }
                    }

                }

                if (addressRu.getDistrict() != null)
                    address.setCityAreaName(addressRu.getDistrict().getTitle());

                if (addressRu.getStreet() != null)
                    address.setStreetName(addressRu.getStreet().getTitle());
                if (addressRu.getInheritedPostCode() != null) address.setIndex(factory.createAddressIndex(addressRu.getInheritedPostCode()));
                if (addressRu.getHouseNumber() != null) address.setHouse(addressRu.getHouseNumber());
                if (addressRu.getHouseUnitNumber() != null) address.setBuilding(addressRu.getHouseUnitNumber());
                if (addressRu.getFlatNumber() != null) address.setFlat(factory.createAddressFlat(addressRu.getFlatNumber()));
            }
            else
                address.setAddressText(currentAddress.getShortTitleWithSettlement());
        }
        return address;
    }

    private XMLGregorianCalendar getXmlGregorianCalendar(Date date)
    {
        if (date==null) return null;
        XMLGregorianCalendar commitDate = null;
        try
        {
            String dateStr = new DateFormatter("yyyy-MM-dd").format(date);
            commitDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(dateStr);
        }
        catch (Exception e)
        {
            throw CoreExceptionUtils.getRuntimeException(e);
        }
        return commitDate;
    }



    @Override
    ScriptResultData exportStudentsData(Session session, OrgUnit formativeOrgUnit, boolean isAllStudents, boolean onlyFullData, List<StudentCustomStateCI> studentCustomStates, int counter)
    {
        long startTime = System.currentTimeMillis();

        _formativeOrgUnit = formativeOrgUnit;
        initDirectoryMaps();
        ru.tandemservice.movestudent.ws.gupmsr.entity.File resultObject = getStudents(isAllStudents, onlyFullData, studentCustomStates, session);
        byte[] array = null;
        try
        {
            ByteArrayOutputStream result = new ByteArrayOutputStream();
            JAXBContext context = JAXBContext.newInstance(ru.tandemservice.movestudent.ws.gupmsr.entity.File.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(resultObject, result);
            array = result.toByteArray();
        } catch (Exception e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }

        String fileName = new StringBuilder(_settings.getCodeHighSchool()).append("-").append(new DateFormatter("yyMMdd").format(new Date()))
                .append("-").append(StringUtils.leftPad(String.valueOf(counter), 3, '0'))
                .append("-").append(StringUtils.leftPad(String.valueOf(resultObject.getFileInfo().getRecordCount()), 4, '0'))
                .append(".xml").toString();

        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println(totalTime/1000);

        return new ScriptResultData(fileName, array)
    }


    private class LastOrdersData
    {
        private AbstractStudentOrder _lastOrder;
        private StudentExtractType _lastOrderType;

        public AbstractStudentOrder getLastOrder()
        {
            return _lastOrder;
        }

        public void setLastOrder(AbstractStudentOrder lastOrder)
        {
            _lastOrder = lastOrder;
        }

        public StudentExtractType getLastOrderType()
        {
            return _lastOrderType;
        }

        public void setLastOrderType(StudentExtractType lastOrderType)
        {
            _lastOrderType = lastOrderType;
        }
    }

    private class StudentData
    {
        private IdentityCard _card;
        private StudentStatus _stStatus;
        private AddressBase _addressReg;
        private AddressBase _addressFact;
        private PersonContactData _contacts;
        private String _snils;
        private String _devForm;
        private String _devTech;
        private Course _course;
        private Date _enrollmentDate;
        private String _studentPersonalNumber;
        private int _entranceYear;
        private EduProgramKind _eduProgramKind;
        private Long _terrOuId;

        public int getEntranceYear()
        {
            return _entranceYear;
        }

        public void setEntranceYear(int entranceYear)
        {
            _entranceYear = entranceYear;
        }

        String getStudentPersonalNumber()
        {
            return _studentPersonalNumber;
        }

        void setStudentPersonalNumber(String studentPersonalNumber)
        {
            _studentPersonalNumber = studentPersonalNumber;
        }

        public Date getEnrollmentDate()
        {
            return _enrollmentDate;
        }

        public void setEnrollmentDate(Date enrollmentDate)
        {
            _enrollmentDate = enrollmentDate;
        }

        public Course getCourse()
        {
            return _course;
        }

        public void setCourse(Course course)
        {
            _course = course;
        }

        public IdentityCard getCard()
        {
            return _card;
        }

        public void setCard(IdentityCard card)
        {
            _card = card;
        }

        public StudentStatus getStStatus()
        {
            return _stStatus;
        }

        public void setStStatus(StudentStatus stStatus)
        {
            _stStatus = stStatus;
        }

        public AddressBase getAddressReg()
        {
            return _addressReg;
        }

        public void setAddressReg(AddressBase addressReg)
        {
            _addressReg = addressReg;
        }

        public AddressBase getAddressFact()
        {
            return _addressFact;
        }

        public void setAddressFact(AddressBase addressFact)
        {
            _addressFact = addressFact;
        }

        public PersonContactData getContacts()
        {
            return _contacts;
        }

        public void setContacts(PersonContactData contacts)
        {
            _contacts = contacts;
        }

        public String getSnils()
        {
            return _snils;
        }

        public void setSnils(String snils)
        {
            _snils = snils;
        }

        public String getDevForm()
        {
            return _devForm;
        }

        public void setDevForm(String devForm)
        {
            _devForm = devForm;
        }

        public String getDevTech()
        {
            return _devTech;
        }

        public void setDevTech(String devTech)
        {
            _devTech = devTech;
        }

        public EduProgramKind getEduProgramKind()
        {
            return _eduProgramKind
        }

        public void setEduProgramKind(EduProgramKind eduProgramKind)
        {
            _eduProgramKind = eduProgramKind
        }

        Long getTerrOuId()
        {
            return _terrOuId
        }

        void setTerrOuId(Long terrOuId)
        {
            _terrOuId = terrOuId
        }
    }

    private class EnrOrderData
    {
        private String _orderNumber;
        private Date _orderDate;
        private Date _enrollDate

        String getOrderNumber()
        {
            return _orderNumber
        }

        void setOrderNumber(String orderNumber)
        {
            _orderNumber = orderNumber
        }

        Date getOrderDate()
        {
            return _orderDate
        }

        void setOrderDate(Date orderDate)
        {
            _orderDate = orderDate
        }

        Date getEnrollDate()
        {
            return _enrollDate
        }

        void setEnrollDate(Date enrollDate)
        {
            _enrollDate = enrollDate
        }
    }
}
