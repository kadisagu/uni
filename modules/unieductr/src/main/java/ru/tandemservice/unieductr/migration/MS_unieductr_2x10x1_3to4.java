/* $Id$ */
package ru.tandemservice.unieductr.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;

/**
 * @author Ekaterina Zvereva
 * @since 20.05.2016
 */
public class MS_unieductr_2x10x1_3to4 extends IndependentMigrationScript
{

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        PreparedStatement update = tool.prepareStatement("update accessmatrix_t set permissionkey_p = ? where permissionkey_p = ?");
        update.setString(1, "eduCtrAgreementIncomeReport");
        update.setString(2, "eduCtrMonthlyPaymentsReport");
        update.executeUpdate();
    }


}