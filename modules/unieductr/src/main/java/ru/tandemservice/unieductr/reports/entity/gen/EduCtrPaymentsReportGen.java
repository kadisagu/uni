package ru.tandemservice.unieductr.reports.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Список платежей
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduCtrPaymentsReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport";
    public static final String ENTITY_NAME = "eduCtrPaymentsReport";
    public static final int VERSION_HASH = 256641500;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_PERIOD_DATE_FROM = "periodDateFrom";
    public static final String P_PERIOD_DATE_TO = "periodDateTo";
    public static final String P_CONTACTOR_TYPE = "contactorType";
    public static final String P_PROGRAM_SUBJECT = "programSubject";
    public static final String P_PROGRAM_FORM = "programForm";
    public static final String P_EDU_BEGIN_YEAR = "eduBeginYear";
    public static final String P_PROGRAM_TRAIT = "programTrait";
    public static final String P_EDU_PROGRAM = "eduProgram";
    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String P_EDUCATION_LEVEL_HIGH_SCHOOL = "educationLevelHighSchool";
    public static final String P_DEVELOP_FORM = "developForm";
    public static final String P_DEVELOP_CONDITION = "developCondition";
    public static final String P_DEVELOP_TECH = "developTech";
    public static final String P_DEVELOP_PERIOD = "developPeriod";
    public static final String P_COURSE = "course";
    public static final String P_GROUP = "group";
    public static final String P_ACCOUNT_INACTIVE_STUDENTS = "accountInactiveStudents";

    private OrgUnit _orgUnit;     // Подразделение
    private Date _periodDateFrom;     // Период с
    private Date _periodDateTo;     // Период по
    private String _contactorType;     // Тип контактного лица (заказчика)
    private String _programSubject;     // Направление, специальность
    private String _programForm;     // Форма обучения
    private String _eduBeginYear;     // Учебный год начала обучения
    private String _programTrait;     // Особенность реализации
    private String _eduProgram;     // Образовательная программа
    private String _formativeOrgUnit;     // Форм. подразделение
    private String _territorialOrgUnit;     // Терр. подразделение
    private String _educationLevelHighSchool;     // Направление подготовки (специальность)
    private String _developForm;     // Форма освоения
    private String _developCondition;     // Условие освоения
    private String _developTech;     // Технология освоения
    private String _developPeriod;     // Срок освоения
    private String _course;     // Курс
    private String _group;     // Группа
    private String _accountInactiveStudents;     // Учет неактивных, архивных студентов и архивных групп

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Период с.
     */
    public Date getPeriodDateFrom()
    {
        return _periodDateFrom;
    }

    /**
     * @param periodDateFrom Период с.
     */
    public void setPeriodDateFrom(Date periodDateFrom)
    {
        dirty(_periodDateFrom, periodDateFrom);
        _periodDateFrom = periodDateFrom;
    }

    /**
     * @return Период по.
     */
    public Date getPeriodDateTo()
    {
        return _periodDateTo;
    }

    /**
     * @param periodDateTo Период по.
     */
    public void setPeriodDateTo(Date periodDateTo)
    {
        dirty(_periodDateTo, periodDateTo);
        _periodDateTo = periodDateTo;
    }

    /**
     * @return Тип контактного лица (заказчика).
     */
    @Length(max=255)
    public String getContactorType()
    {
        return _contactorType;
    }

    /**
     * @param contactorType Тип контактного лица (заказчика).
     */
    public void setContactorType(String contactorType)
    {
        dirty(_contactorType, contactorType);
        _contactorType = contactorType;
    }

    /**
     * @return Направление, специальность.
     */
    @Length(max=1024)
    public String getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление, специальность.
     */
    public void setProgramSubject(String programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    /**
     * @return Форма обучения.
     */
    @Length(max=255)
    public String getProgramForm()
    {
        return _programForm;
    }

    /**
     * @param programForm Форма обучения.
     */
    public void setProgramForm(String programForm)
    {
        dirty(_programForm, programForm);
        _programForm = programForm;
    }

    /**
     * @return Учебный год начала обучения.
     */
    @Length(max=255)
    public String getEduBeginYear()
    {
        return _eduBeginYear;
    }

    /**
     * @param eduBeginYear Учебный год начала обучения.
     */
    public void setEduBeginYear(String eduBeginYear)
    {
        dirty(_eduBeginYear, eduBeginYear);
        _eduBeginYear = eduBeginYear;
    }

    /**
     * @return Особенность реализации.
     */
    @Length(max=255)
    public String getProgramTrait()
    {
        return _programTrait;
    }

    /**
     * @param programTrait Особенность реализации.
     */
    public void setProgramTrait(String programTrait)
    {
        dirty(_programTrait, programTrait);
        _programTrait = programTrait;
    }

    /**
     * @return Образовательная программа.
     */
    @Length(max=1024)
    public String getEduProgram()
    {
        return _eduProgram;
    }

    /**
     * @param eduProgram Образовательная программа.
     */
    public void setEduProgram(String eduProgram)
    {
        dirty(_eduProgram, eduProgram);
        _eduProgram = eduProgram;
    }

    /**
     * @return Форм. подразделение.
     */
    @Length(max=1024)
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Форм. подразделение.
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Терр. подразделение.
     */
    @Length(max=1024)
    public String getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Терр. подразделение.
     */
    public void setTerritorialOrgUnit(String territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    /**
     * @return Направление подготовки (специальность).
     */
    @Length(max=1024)
    public String getEducationLevelHighSchool()
    {
        return _educationLevelHighSchool;
    }

    /**
     * @param educationLevelHighSchool Направление подготовки (специальность).
     */
    public void setEducationLevelHighSchool(String educationLevelHighSchool)
    {
        dirty(_educationLevelHighSchool, educationLevelHighSchool);
        _educationLevelHighSchool = educationLevelHighSchool;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(String developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения.
     */
    public void setDevelopCondition(String developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения.
     */
    public void setDevelopTech(String developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Срок освоения.
     */
    @Length(max=255)
    public String getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Срок освоения.
     */
    public void setDevelopPeriod(String developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    /**
     * @return Курс.
     */
    @Length(max=255)
    public String getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс.
     */
    public void setCourse(String course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа.
     */
    @Length(max=255)
    public String getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа.
     */
    public void setGroup(String group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Учет неактивных, архивных студентов и архивных групп.
     */
    @Length(max=255)
    public String getAccountInactiveStudents()
    {
        return _accountInactiveStudents;
    }

    /**
     * @param accountInactiveStudents Учет неактивных, архивных студентов и архивных групп.
     */
    public void setAccountInactiveStudents(String accountInactiveStudents)
    {
        dirty(_accountInactiveStudents, accountInactiveStudents);
        _accountInactiveStudents = accountInactiveStudents;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduCtrPaymentsReportGen)
        {
            setOrgUnit(((EduCtrPaymentsReport)another).getOrgUnit());
            setPeriodDateFrom(((EduCtrPaymentsReport)another).getPeriodDateFrom());
            setPeriodDateTo(((EduCtrPaymentsReport)another).getPeriodDateTo());
            setContactorType(((EduCtrPaymentsReport)another).getContactorType());
            setProgramSubject(((EduCtrPaymentsReport)another).getProgramSubject());
            setProgramForm(((EduCtrPaymentsReport)another).getProgramForm());
            setEduBeginYear(((EduCtrPaymentsReport)another).getEduBeginYear());
            setProgramTrait(((EduCtrPaymentsReport)another).getProgramTrait());
            setEduProgram(((EduCtrPaymentsReport)another).getEduProgram());
            setFormativeOrgUnit(((EduCtrPaymentsReport)another).getFormativeOrgUnit());
            setTerritorialOrgUnit(((EduCtrPaymentsReport)another).getTerritorialOrgUnit());
            setEducationLevelHighSchool(((EduCtrPaymentsReport)another).getEducationLevelHighSchool());
            setDevelopForm(((EduCtrPaymentsReport)another).getDevelopForm());
            setDevelopCondition(((EduCtrPaymentsReport)another).getDevelopCondition());
            setDevelopTech(((EduCtrPaymentsReport)another).getDevelopTech());
            setDevelopPeriod(((EduCtrPaymentsReport)another).getDevelopPeriod());
            setCourse(((EduCtrPaymentsReport)another).getCourse());
            setGroup(((EduCtrPaymentsReport)another).getGroup());
            setAccountInactiveStudents(((EduCtrPaymentsReport)another).getAccountInactiveStudents());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduCtrPaymentsReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduCtrPaymentsReport.class;
        }

        public T newInstance()
        {
            return (T) new EduCtrPaymentsReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return obj.getOrgUnit();
                case "periodDateFrom":
                    return obj.getPeriodDateFrom();
                case "periodDateTo":
                    return obj.getPeriodDateTo();
                case "contactorType":
                    return obj.getContactorType();
                case "programSubject":
                    return obj.getProgramSubject();
                case "programForm":
                    return obj.getProgramForm();
                case "eduBeginYear":
                    return obj.getEduBeginYear();
                case "programTrait":
                    return obj.getProgramTrait();
                case "eduProgram":
                    return obj.getEduProgram();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
                case "educationLevelHighSchool":
                    return obj.getEducationLevelHighSchool();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developTech":
                    return obj.getDevelopTech();
                case "developPeriod":
                    return obj.getDevelopPeriod();
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "accountInactiveStudents":
                    return obj.getAccountInactiveStudents();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "periodDateFrom":
                    obj.setPeriodDateFrom((Date) value);
                    return;
                case "periodDateTo":
                    obj.setPeriodDateTo((Date) value);
                    return;
                case "contactorType":
                    obj.setContactorType((String) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((String) value);
                    return;
                case "programForm":
                    obj.setProgramForm((String) value);
                    return;
                case "eduBeginYear":
                    obj.setEduBeginYear((String) value);
                    return;
                case "programTrait":
                    obj.setProgramTrait((String) value);
                    return;
                case "eduProgram":
                    obj.setEduProgram((String) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((String) value);
                    return;
                case "educationLevelHighSchool":
                    obj.setEducationLevelHighSchool((String) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((String) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((String) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((String) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((String) value);
                    return;
                case "course":
                    obj.setCourse((String) value);
                    return;
                case "group":
                    obj.setGroup((String) value);
                    return;
                case "accountInactiveStudents":
                    obj.setAccountInactiveStudents((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                        return true;
                case "periodDateFrom":
                        return true;
                case "periodDateTo":
                        return true;
                case "contactorType":
                        return true;
                case "programSubject":
                        return true;
                case "programForm":
                        return true;
                case "eduBeginYear":
                        return true;
                case "programTrait":
                        return true;
                case "eduProgram":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "territorialOrgUnit":
                        return true;
                case "educationLevelHighSchool":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "developTech":
                        return true;
                case "developPeriod":
                        return true;
                case "course":
                        return true;
                case "group":
                        return true;
                case "accountInactiveStudents":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return true;
                case "periodDateFrom":
                    return true;
                case "periodDateTo":
                    return true;
                case "contactorType":
                    return true;
                case "programSubject":
                    return true;
                case "programForm":
                    return true;
                case "eduBeginYear":
                    return true;
                case "programTrait":
                    return true;
                case "eduProgram":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "territorialOrgUnit":
                    return true;
                case "educationLevelHighSchool":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "developTech":
                    return true;
                case "developPeriod":
                    return true;
                case "course":
                    return true;
                case "group":
                    return true;
                case "accountInactiveStudents":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return OrgUnit.class;
                case "periodDateFrom":
                    return Date.class;
                case "periodDateTo":
                    return Date.class;
                case "contactorType":
                    return String.class;
                case "programSubject":
                    return String.class;
                case "programForm":
                    return String.class;
                case "eduBeginYear":
                    return String.class;
                case "programTrait":
                    return String.class;
                case "eduProgram":
                    return String.class;
                case "formativeOrgUnit":
                    return String.class;
                case "territorialOrgUnit":
                    return String.class;
                case "educationLevelHighSchool":
                    return String.class;
                case "developForm":
                    return String.class;
                case "developCondition":
                    return String.class;
                case "developTech":
                    return String.class;
                case "developPeriod":
                    return String.class;
                case "course":
                    return String.class;
                case "group":
                    return String.class;
                case "accountInactiveStudents":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduCtrPaymentsReport> _dslPath = new Path<EduCtrPaymentsReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduCtrPaymentsReport");
    }
            

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Период с.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getPeriodDateFrom()
     */
    public static PropertyPath<Date> periodDateFrom()
    {
        return _dslPath.periodDateFrom();
    }

    /**
     * @return Период по.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getPeriodDateTo()
     */
    public static PropertyPath<Date> periodDateTo()
    {
        return _dslPath.periodDateTo();
    }

    /**
     * @return Тип контактного лица (заказчика).
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getContactorType()
     */
    public static PropertyPath<String> contactorType()
    {
        return _dslPath.contactorType();
    }

    /**
     * @return Направление, специальность.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getProgramSubject()
     */
    public static PropertyPath<String> programSubject()
    {
        return _dslPath.programSubject();
    }

    /**
     * @return Форма обучения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getProgramForm()
     */
    public static PropertyPath<String> programForm()
    {
        return _dslPath.programForm();
    }

    /**
     * @return Учебный год начала обучения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getEduBeginYear()
     */
    public static PropertyPath<String> eduBeginYear()
    {
        return _dslPath.eduBeginYear();
    }

    /**
     * @return Особенность реализации.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getProgramTrait()
     */
    public static PropertyPath<String> programTrait()
    {
        return _dslPath.programTrait();
    }

    /**
     * @return Образовательная программа.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getEduProgram()
     */
    public static PropertyPath<String> eduProgram()
    {
        return _dslPath.eduProgram();
    }

    /**
     * @return Форм. подразделение.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Терр. подразделение.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getTerritorialOrgUnit()
     */
    public static PropertyPath<String> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    /**
     * @return Направление подготовки (специальность).
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getEducationLevelHighSchool()
     */
    public static PropertyPath<String> educationLevelHighSchool()
    {
        return _dslPath.educationLevelHighSchool();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getDevelopForm()
     */
    public static PropertyPath<String> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getDevelopCondition()
     */
    public static PropertyPath<String> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getDevelopTech()
     */
    public static PropertyPath<String> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getDevelopPeriod()
     */
    public static PropertyPath<String> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getCourse()
     */
    public static PropertyPath<String> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getGroup()
     */
    public static PropertyPath<String> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Учет неактивных, архивных студентов и архивных групп.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getAccountInactiveStudents()
     */
    public static PropertyPath<String> accountInactiveStudents()
    {
        return _dslPath.accountInactiveStudents();
    }

    public static class Path<E extends EduCtrPaymentsReport> extends StorableReport.Path<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<Date> _periodDateFrom;
        private PropertyPath<Date> _periodDateTo;
        private PropertyPath<String> _contactorType;
        private PropertyPath<String> _programSubject;
        private PropertyPath<String> _programForm;
        private PropertyPath<String> _eduBeginYear;
        private PropertyPath<String> _programTrait;
        private PropertyPath<String> _eduProgram;
        private PropertyPath<String> _formativeOrgUnit;
        private PropertyPath<String> _territorialOrgUnit;
        private PropertyPath<String> _educationLevelHighSchool;
        private PropertyPath<String> _developForm;
        private PropertyPath<String> _developCondition;
        private PropertyPath<String> _developTech;
        private PropertyPath<String> _developPeriod;
        private PropertyPath<String> _course;
        private PropertyPath<String> _group;
        private PropertyPath<String> _accountInactiveStudents;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Период с.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getPeriodDateFrom()
     */
        public PropertyPath<Date> periodDateFrom()
        {
            if(_periodDateFrom == null )
                _periodDateFrom = new PropertyPath<Date>(EduCtrPaymentsReportGen.P_PERIOD_DATE_FROM, this);
            return _periodDateFrom;
        }

    /**
     * @return Период по.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getPeriodDateTo()
     */
        public PropertyPath<Date> periodDateTo()
        {
            if(_periodDateTo == null )
                _periodDateTo = new PropertyPath<Date>(EduCtrPaymentsReportGen.P_PERIOD_DATE_TO, this);
            return _periodDateTo;
        }

    /**
     * @return Тип контактного лица (заказчика).
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getContactorType()
     */
        public PropertyPath<String> contactorType()
        {
            if(_contactorType == null )
                _contactorType = new PropertyPath<String>(EduCtrPaymentsReportGen.P_CONTACTOR_TYPE, this);
            return _contactorType;
        }

    /**
     * @return Направление, специальность.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getProgramSubject()
     */
        public PropertyPath<String> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new PropertyPath<String>(EduCtrPaymentsReportGen.P_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

    /**
     * @return Форма обучения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getProgramForm()
     */
        public PropertyPath<String> programForm()
        {
            if(_programForm == null )
                _programForm = new PropertyPath<String>(EduCtrPaymentsReportGen.P_PROGRAM_FORM, this);
            return _programForm;
        }

    /**
     * @return Учебный год начала обучения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getEduBeginYear()
     */
        public PropertyPath<String> eduBeginYear()
        {
            if(_eduBeginYear == null )
                _eduBeginYear = new PropertyPath<String>(EduCtrPaymentsReportGen.P_EDU_BEGIN_YEAR, this);
            return _eduBeginYear;
        }

    /**
     * @return Особенность реализации.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getProgramTrait()
     */
        public PropertyPath<String> programTrait()
        {
            if(_programTrait == null )
                _programTrait = new PropertyPath<String>(EduCtrPaymentsReportGen.P_PROGRAM_TRAIT, this);
            return _programTrait;
        }

    /**
     * @return Образовательная программа.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getEduProgram()
     */
        public PropertyPath<String> eduProgram()
        {
            if(_eduProgram == null )
                _eduProgram = new PropertyPath<String>(EduCtrPaymentsReportGen.P_EDU_PROGRAM, this);
            return _eduProgram;
        }

    /**
     * @return Форм. подразделение.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(EduCtrPaymentsReportGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Терр. подразделение.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getTerritorialOrgUnit()
     */
        public PropertyPath<String> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new PropertyPath<String>(EduCtrPaymentsReportGen.P_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

    /**
     * @return Направление подготовки (специальность).
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getEducationLevelHighSchool()
     */
        public PropertyPath<String> educationLevelHighSchool()
        {
            if(_educationLevelHighSchool == null )
                _educationLevelHighSchool = new PropertyPath<String>(EduCtrPaymentsReportGen.P_EDUCATION_LEVEL_HIGH_SCHOOL, this);
            return _educationLevelHighSchool;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getDevelopForm()
     */
        public PropertyPath<String> developForm()
        {
            if(_developForm == null )
                _developForm = new PropertyPath<String>(EduCtrPaymentsReportGen.P_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getDevelopCondition()
     */
        public PropertyPath<String> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new PropertyPath<String>(EduCtrPaymentsReportGen.P_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getDevelopTech()
     */
        public PropertyPath<String> developTech()
        {
            if(_developTech == null )
                _developTech = new PropertyPath<String>(EduCtrPaymentsReportGen.P_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getDevelopPeriod()
     */
        public PropertyPath<String> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new PropertyPath<String>(EduCtrPaymentsReportGen.P_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getCourse()
     */
        public PropertyPath<String> course()
        {
            if(_course == null )
                _course = new PropertyPath<String>(EduCtrPaymentsReportGen.P_COURSE, this);
            return _course;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getGroup()
     */
        public PropertyPath<String> group()
        {
            if(_group == null )
                _group = new PropertyPath<String>(EduCtrPaymentsReportGen.P_GROUP, this);
            return _group;
        }

    /**
     * @return Учет неактивных, архивных студентов и архивных групп.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport#getAccountInactiveStudents()
     */
        public PropertyPath<String> accountInactiveStudents()
        {
            if(_accountInactiveStudents == null )
                _accountInactiveStudents = new PropertyPath<String>(EduCtrPaymentsReportGen.P_ACCOUNT_INACTIVE_STUDENTS, this);
            return _accountInactiveStudents;
        }

        public Class getEntityClass()
        {
            return EduCtrPaymentsReport.class;
        }

        public String getEntityName()
        {
            return "eduCtrPaymentsReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
