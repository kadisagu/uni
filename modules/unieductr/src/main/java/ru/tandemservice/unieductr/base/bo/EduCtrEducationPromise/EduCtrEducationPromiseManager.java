package ru.tandemservice.unieductr.base.bo.EduCtrEducationPromise;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrPromiceManager;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractPromice;

import ru.tandemservice.unieductr.base.bo.EduCtrEducationPromise.ui.InContractList.EduCtrEducationPromiseInContractList;
import ru.tandemservice.unieductr.base.bo.EduCtrEducationPromise.ui.InWizardStep.EduCtrEducationPromiseInWizardStep;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;

/**
 * @author vdanilov
 */
@Configuration
public class EduCtrEducationPromiseManager extends BusinessObjectManager implements ICtrPromiceManager {

    @Override public Class<? extends CtrContractPromice> promiceClass() {
        return EduCtrEducationPromise.class;
    }

    @Override public Class<? extends BusinessComponentManager> rootSectionComponent() {
        return EduCtrEducationPromiseInContractList.class;
    }

    @Override public Class<? extends BusinessComponentManager> inWizardPromiceStepComponent() {
        return EduCtrEducationPromiseInWizardStep.class;
    }

}
