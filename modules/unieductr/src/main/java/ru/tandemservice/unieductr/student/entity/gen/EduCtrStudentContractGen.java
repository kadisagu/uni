package ru.tandemservice.unieductr.student.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;
import ru.tandemservice.unieductr.student.entity.IEduStudentContract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь студента с договором на обучение по обр. программе
 *
 * Связывает договор на обучение и студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduCtrStudentContractGen extends EntityBase
 implements IEduContractRelation, IEduStudentContract, INaturalIdentifiable<EduCtrStudentContractGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unieductr.student.entity.EduCtrStudentContract";
    public static final String ENTITY_NAME = "eduCtrStudentContract";
    public static final int VERSION_HASH = -1551649313;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTRACT_OBJECT = "contractObject";
    public static final String L_STUDENT = "student";

    private CtrContractObject _contractObject;     // Договор на обучение
    private Student _student;     // Студент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Договор на обучение. Свойство не может быть null.
     */
    @NotNull
    public CtrContractObject getContractObject()
    {
        return _contractObject;
    }

    /**
     * @param contractObject Договор на обучение. Свойство не может быть null.
     */
    public void setContractObject(CtrContractObject contractObject)
    {
        dirty(_contractObject, contractObject);
        _contractObject = contractObject;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EduCtrStudentContractGen)
        {
            if (withNaturalIdProperties)
            {
                setContractObject(((EduCtrStudentContract)another).getContractObject());
                setStudent(((EduCtrStudentContract)another).getStudent());
            }
        }
    }

    public INaturalId<EduCtrStudentContractGen> getNaturalId()
    {
        return new NaturalId(getContractObject(), getStudent());
    }

    public static class NaturalId extends NaturalIdBase<EduCtrStudentContractGen>
    {
        private static final String PROXY_NAME = "EduCtrStudentContractNaturalProxy";

        private Long _contractObject;
        private Long _student;

        public NaturalId()
        {}

        public NaturalId(CtrContractObject contractObject, Student student)
        {
            _contractObject = ((IEntity) contractObject).getId();
            _student = ((IEntity) student).getId();
        }

        public Long getContractObject()
        {
            return _contractObject;
        }

        public void setContractObject(Long contractObject)
        {
            _contractObject = contractObject;
        }

        public Long getStudent()
        {
            return _student;
        }

        public void setStudent(Long student)
        {
            _student = student;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EduCtrStudentContractGen.NaturalId) ) return false;

            EduCtrStudentContractGen.NaturalId that = (NaturalId) o;

            if( !equals(getContractObject(), that.getContractObject()) ) return false;
            if( !equals(getStudent(), that.getStudent()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getContractObject());
            result = hashCode(result, getStudent());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getContractObject());
            sb.append("/");
            sb.append(getStudent());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduCtrStudentContractGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduCtrStudentContract.class;
        }

        public T newInstance()
        {
            return (T) new EduCtrStudentContract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "contractObject":
                    return obj.getContractObject();
                case "student":
                    return obj.getStudent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "contractObject":
                    obj.setContractObject((CtrContractObject) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "contractObject":
                        return true;
                case "student":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "contractObject":
                    return true;
                case "student":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "contractObject":
                    return CtrContractObject.class;
                case "student":
                    return Student.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduCtrStudentContract> _dslPath = new Path<EduCtrStudentContract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduCtrStudentContract");
    }
            

    /**
     * @return Договор на обучение. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.student.entity.EduCtrStudentContract#getContractObject()
     */
    public static CtrContractObject.Path<CtrContractObject> contractObject()
    {
        return _dslPath.contractObject();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.student.entity.EduCtrStudentContract#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    public static class Path<E extends EduCtrStudentContract> extends EntityPath<E>
    {
        private CtrContractObject.Path<CtrContractObject> _contractObject;
        private Student.Path<Student> _student;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Договор на обучение. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.student.entity.EduCtrStudentContract#getContractObject()
     */
        public CtrContractObject.Path<CtrContractObject> contractObject()
        {
            if(_contractObject == null )
                _contractObject = new CtrContractObject.Path<CtrContractObject>(L_CONTRACT_OBJECT, this);
            return _contractObject;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.student.entity.EduCtrStudentContract#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

        public Class getEntityClass()
        {
            return EduCtrStudentContract.class;
        }

        public String getEntityName()
        {
            return "eduCtrStudentContract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
