package ru.tandemservice.unieductr.student.entity;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContextObject;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unieductr.student.entity.gen.EduCtrStudentContractGen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Связь студента с договором на обучение по обр. программе
 *
 * Связывает договор на обучение и студента
 */
public class EduCtrStudentContract extends EduCtrStudentContractGen implements ISecLocalEntityOwner, ICtrContextObject
{
    @Override
    public String getTitle() {
        if (getContractObject() == null) {
            return this.getClass().getSimpleName();
        }
        return getContractObject().getTitle() + ", студент " + getStudent().getTitleWithFio();
    }

    @Override
    public PersonRole getPersonRole() {
        return getStudent();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities() {
        final List<IEntity> result = new ArrayList<IEntity>(this.getStudent().getSecLocalEntities());
        result.add(this.getContractObject().getOrgUnit());
        return result;
    }

}