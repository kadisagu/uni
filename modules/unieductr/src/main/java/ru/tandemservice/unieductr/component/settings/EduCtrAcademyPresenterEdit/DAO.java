/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unieductr.component.settings.EduCtrAcademyPresenterEdit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.ctr.base.bo.Contactor.ContactorManager;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unieductr.base.entity.EduCtrAcademyPresenter;

/**
 * @author iolshvang
 * @since 06.06.11 17:32
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.getOrgUnitHolder().refresh(OrgUnit.class);

        model.setEmployeePostModel(new DQLFullCheckSelectModel(EmployeePost.P_TITLE_WITH_ORG_UNIT)
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EmployeePost.class, alias)
                .order(property(EmployeePost.employee().person().identityCard().lastName().fromAlias(alias)))
                .order(property(EmployeePost.employee().person().identityCard().firstName().fromAlias(alias)))
                .order(property(EmployeePost.employee().person().identityCard().middleName().fromAlias(alias)));

                if (null != filter)
                {
                    IDQLExpression e = null;
                    final Set<String> lexems = new HashSet<>(Arrays.asList(StringUtils.split(filter, " -_")));
                    for (final String s : lexems)
                    {
                        e = and(e, or(
                            this.like(EmployeePost.employee().person().identityCard().lastName().fromAlias(alias), s),
                            this.like(EmployeePost.employee().person().identityCard().firstName().fromAlias(alias), s),
                            this.like(EmployeePost.employee().person().identityCard().seria().fromAlias(alias), s),
                            this.like(EmployeePost.employee().person().identityCard().number().fromAlias(alias), s)
                        ));
                    }
                    dql.where(e);
                }

                return dql;
            }
        });


        // заполняем на основе базы
        {
            final Map<EmployeePost, EduCtrAcademyPresenter> prevMap = model.getOrgUnitPresenterMap();
            final Map<EmployeePost, EduCtrAcademyPresenter> newMap = new LinkedHashMap<>();
            final List<EduCtrAcademyPresenter> presenters = this.getList(EduCtrAcademyPresenter.class, EduCtrAcademyPresenter.orgUnit(), model.getOrgUnit(), EduCtrAcademyPresenter.presenter().employeePost().employee().person().identityCard().fullFio().s());
            for (EduCtrAcademyPresenter p: presenters) {
                EduCtrAcademyPresenter prev = prevMap.get(p.getPresenter().getEmployeePost());
                if (null != prev) { p.update(prev, false); }
                newMap.put(p.getPresenter().getEmployeePost(), p);
            }
            model.setOrgUnitPresenterMap(newMap);
        }
    }

    @Override
    public void update(final Model model)
    {
        new MergeAction.SessionMergeAction<Long, EduCtrAcademyPresenter>() {
            @Override protected Long key(final EduCtrAcademyPresenter source) { return source.getPresenter().getEmployeePost().getId(); }
            @Override protected EduCtrAcademyPresenter buildRow(final EduCtrAcademyPresenter source) {
                if (null == source.getId()) {
                    EmployeePostContactor presenter = source.getPresenter();
                    if (null == presenter.getId()) {
                        EmployeePost employeePost = presenter.getEmployeePost();
                        presenter = ContactorManager.instance().dao().saveEmployeePostContactor(employeePost, employeePost.getPhone(), null, false);
                    }
                    source.setPresenter(presenter);
                }
                return source;
            }
            @Override protected void fill(final EduCtrAcademyPresenter target, final EduCtrAcademyPresenter source) {
                target.update(source, false);
            }
            @Override protected boolean isDeleteable(final EduCtrAcademyPresenter target) { return true; }
        }.merge(
            this.getList(EduCtrAcademyPresenter.class, EduCtrAcademyPresenter.orgUnit(), model.getOrgUnit()),
            model.getOrgUnitPresenterList()
        );
    }
}
