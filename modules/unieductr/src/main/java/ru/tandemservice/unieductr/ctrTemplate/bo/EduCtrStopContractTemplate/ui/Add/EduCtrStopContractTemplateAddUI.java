/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStopContractTemplate.ui.Add;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.commonbase.catalog.entity.codes.CurrencyCodes;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContractResultDao;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.AddByTemplate.CtrContractVersionAddByTemplateUI;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contract.*;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.ui.formatters.MoneyFormatter;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduContractPaymentPromiceResultPair;
import ru.tandemservice.unieductr.base.entity.EduCtrStopVersionTemplateData;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStopContractTemplate.EduCtrStopContractTemplateManager;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author azhebko
 * @since 01.09.2014
 */
@Input({
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTEXT_ID, binding = "sourceContractVersion.id", required = true),
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_KIND_ID, binding="versionCreateData.contractKindHolder.id", required=true),
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_PRINT_TEMPLATE_ID, binding="versionCreateData.printTemplateHolder.id")

})
public class EduCtrStopContractTemplateAddUI extends UIPresenter
{
    private CtrContractVersion _sourceContractVersion = new CtrContractVersion();
    public CtrContractVersion getSourceContractVersion() { return _sourceContractVersion; }

    private EduCtrStopVersionTemplateData _versionTemplateData = new EduCtrStopVersionTemplateData();
    public EduCtrStopVersionTemplateData getVersionTemplateData() { return _versionTemplateData; }

    private Map<String, ICtrContractResultDao.ICtrContractStatusItem> _statusItems;

    private Map<String, Long> _selectedPromicesCurrencyToAmount;
    private Map<String, Long> _balanceCurrencyToAmountMap;

    private List<ViewWrapper<CtrPaymentResult>> _paymentResults;
    private ViewWrapper<CtrPaymentResult> _paymentResult;

    private List<ViewWrapper<CtrPaymentPromice>> _paymentPromices;
    private ViewWrapper<CtrPaymentPromice> _paymentPromice;

    private Set<ViewWrapper<CtrPaymentPromice>> _selectedPromises;

    private final CtrContractVersionCreateData versionCreateData = new CtrContractVersionCreateData();

    public CtrContractVersionCreateData getVersionCreateData()
    {
        return versionCreateData;
    }

    private Date _refundDate;
    public Date getRefundDate() { return _refundDate; }
    public void setRefundDate(Date refundDate) { _refundDate = refundDate; }

    @Override
    public void onComponentRefresh()
    {
        _sourceContractVersion = IUniBaseDao.instance.get().getNotNull(CtrContractVersion.class, _sourceContractVersion.getId());
        getVersionCreateData().doRefresh();
        getVersionCreateData().setDocStartDate(new Date());
        Set<Currency> currencies = CommonBaseEntityUtil.getPropertiesSet(IUniBaseDao.instance.get().getList(CtrPaymentPromice.class, CtrPaymentPromice.src().owner(), _sourceContractVersion), CtrPaymentPromice.L_CURRENCY);
        getVersionTemplateData().setRefundCurrency(currencies.size() == 1 ? currencies.iterator().next() : IUniBaseDao.instance.get().getCatalogItem(Currency.class, CurrencyCodes.RUB));
        prepareContractStatusItems();
        prepareWrappers();
        selectPromicesByDefault();
        versionCreateData.setDurationBeginDate(new Date());
    }

    private void prepareWrappers()
    {
        _paymentResults = ViewWrapper.getPatchedList(DataAccessServices.dao().getList(
                CtrPaymentResult.class,
                CtrPaymentResult.contract(), _sourceContractVersion.getContract(),
                CtrPaymentResult.P_TIMESTAMP)
        );
        _paymentResults.forEach(
                result -> result.setViewProperty(
                        "formattedDate",
                        DateFormatter.DEFAULT_DATE_FORMATTER.format((Date)result.getProperty(CtrPaymentResult.timestamp()))
                )
        );

        _paymentPromices = ViewWrapper.getPatchedList(DataAccessServices.dao().getList(
                CtrPaymentPromice.class,
                CtrPaymentPromice.src().owner(), _sourceContractVersion,
                CtrPaymentPromice.P_DEADLINE_DATE)
        );

        for (ViewWrapper<CtrPaymentPromice> promice : _paymentPromices)
        {
            Long cost = (Long)promice.getProperty(CtrPaymentPromice.costAsLong());
            Long signedCost = promice.getProperty(CtrPaymentPromice.src().contactor()) instanceof EmployeePostContactor ? cost * -1 : cost;
            promice.setViewProperty("signedCost", signedCost);
            promice.setViewProperty("formattedDate", DateFormatter.DEFAULT_DATE_FORMATTER.format((Date)promice.getProperty(CtrPaymentPromice.P_DEADLINE_DATE)));
        }
    }

    public void onChangeSelectedPromices()
    {
       _selectedPromicesCurrencyToAmount = _selectedPromises.stream()
               .collect(Collectors.toMap(
                       wrapper -> (String) wrapper.getProperty(CtrPaymentPromice.currency().code()),
                       wrapper -> (Long) wrapper.getProperty("signedCost"),
                       (cost1, cost2) -> cost1 + cost2));
        refreshBalanceMap();
        refreshRefund();
    }

    public void onClickApply()
    {
        final Date durationBeginDate = versionCreateData.getDurationBeginDate();
        if(durationBeginDate != null && durationBeginDate.before(_sourceContractVersion.getDurationBeginDate()))
            _uiSupport.error("Дату начала действия нельзя установить раньше даты начала действия предыдущей версии", "durationBeginDate");

        if (getVersionTemplateData().getRefundAsLong() > 0 && getRefundDate() == null)
            _uiSupport.error("При указании суммы возврата необходимо заполнить дату возврата.", "refundDate");

        if(getUserContext().getErrorCollector().hasErrors())
            return;

        Collection<Long> selectedIds = _selectedPromises.stream()
                .map(wrapper -> (Long)wrapper.getProperty(CtrPaymentPromice.P_ID))
                .collect(Collectors.toList());

        EduCtrStopContractTemplateManager.instance().dao().doCreateCtrStopContractVersion(getVersionTemplateData(), _sourceContractVersion.getContract(), getVersionCreateData(), getRefundDate(), selectedIds);
        this.deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, getVersionTemplateData().getOwner().getId()).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.TRUE));
    }

    public void onClickClose()
    {
        this.deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, null).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.TRUE));
    }

    public void onClickBack()
    {
        this.deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, null).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.FALSE));
    }

    public String getEduOuDebt()
    {
        List<String> currencyList = new ArrayList<>();
        for (Map.Entry<String, Long> entry : _balanceCurrencyToAmountMap.entrySet())
        {
            Long debt = entry.getValue() < 0 ? Math.abs(entry.getValue()) : 0;
            currencyList.add(MoneyFormatter.formatter(entry.getKey(), 4).format(debt));
        }
        return getMoneyDisplayForm(currencyList);
    }

    public String getStudentDebt()
    {
        List<String> currencyList = new ArrayList<>();
        for (Map.Entry<String, Long> entry : _balanceCurrencyToAmountMap.entrySet())
        {
            Long debt = entry.getValue() > 0 ? entry.getValue() : 0L;
            currencyList.add(MoneyFormatter.formatter(entry.getKey(), 4).format(debt));
        }
        return getMoneyDisplayForm(currencyList);
    }

    public String getPromiceAmount()
    {
        List<String> currencyList = new ArrayList<>();
        for (Map.Entry<String, Long> item : _selectedPromicesCurrencyToAmount.entrySet())
        {
            currencyList.add(MoneyFormatter.formatter(item.getKey(), 4).format(item.getValue()));
        }
        return getMoneyDisplayForm(currencyList);
    }

    public String getResultAmount()
    {
        List<String> currencyList = new ArrayList<>();
        for (Map.Entry<String, ICtrContractResultDao.ICtrContractStatusItem> item : _statusItems.entrySet())
        {
            currencyList.add(MoneyFormatter.formatter(item.getKey(), 4).format(item.getValue().getResultAmount()));
        }
        return getMoneyDisplayForm(currencyList);
    }

    public String getBalance()
    {
        List<String> currencyList = new ArrayList<>();
        for (Map.Entry<String, Long> item : _balanceCurrencyToAmountMap.entrySet())
        {
            currencyList.add(MoneyFormatter.formatter(item.getKey(), 4).format(item.getValue()));
        }
        return getMoneyDisplayForm(currencyList);
    }

    // Util
    public String getRefundDisplayName() { return this.getConfig().getProperty("ui.refund", getVersionTemplateData().getRefundCurrency().getAbbreviationMain()); }

    private String getMoneyDisplayForm(List<String> currencyList)
    {
        return CollectionUtils.isEmpty(currencyList) ? MoneyFormatter.ruMoneyFormatter(4).format(0L) : StringUtils.join(currencyList, ", ");
    }

    private void refreshBalanceMap()
    {
        _balanceCurrencyToAmountMap = new HashMap<>();
        for (Map.Entry<String, ICtrContractResultDao.ICtrContractStatusItem> item : _statusItems.entrySet())
        {
            Long promiceAmount = _selectedPromicesCurrencyToAmount.get(item.getKey()) != null ? _selectedPromicesCurrencyToAmount.get(item.getKey()) : 0L;
            _balanceCurrencyToAmountMap.put(item.getKey(), promiceAmount - item.getValue().getResultAmount());
        }
    }

    private void refreshRefund()
    {
        String refundCurrencyCode = getVersionTemplateData().getRefundCurrency().getCode();
        Long balance = _balanceCurrencyToAmountMap.get(refundCurrencyCode);
        if (balance != null)
        {
            balance = balance < 0 ? Math.abs(balance) : 0;
            getVersionTemplateData().setRefundAsLong(balance);
        } else
            getVersionTemplateData().setRefundAsLong(0L);
    }

    private void selectPromicesByDefault()
    {
        Calendar todayMidnight = new GregorianCalendar();
        todayMidnight.set(Calendar.HOUR_OF_DAY, 0);
        todayMidnight.set(Calendar.MINUTE, 0);
        todayMidnight.set(Calendar.SECOND, 0);
        todayMidnight.set(Calendar.MILLISECOND, 0);

        _selectedPromises = _paymentPromices.stream()
                .filter(promiceWrapper -> ((Date)promiceWrapper.getProperty(CtrPaymentPromice.P_DEADLINE_DATE)).before(todayMidnight.getTime()))
                .collect(Collectors.toSet());
        onChangeSelectedPromices();
    }

    private void prepareContractStatusItems()
    {
        Long ctrVersionId = _sourceContractVersion.getId();
        List<Currency> currencyList = DataAccessServices.dao().getList(Currency.class);
        Map<Long, Map<String, ICtrContractResultDao.ICtrContractStatus>> contractVersionStatusMap = CtrContractVersionManager.instance().results().getContractVersionStatusMap(Collections.singletonList(ctrVersionId), new Date());

        _statusItems = new HashMap<>();
        for (Map.Entry<Long, Map<String, ICtrContractResultDao.ICtrContractStatus>> versionEntry : contractVersionStatusMap.entrySet())
        {
            for (Map.Entry<String, ICtrContractResultDao.ICtrContractStatus> keyVersionStatusEntry : versionEntry.getValue().entrySet())
            {
                for (Currency currency : currencyList)
                {
                    if (EduContractPaymentPromiceResultPair.payDirectedTo(keyVersionStatusEntry.getKey(), TopOrgUnit.getInstance().getId(), currency.getCode()))
                    {
                        Collection<ICtrContractResultDao.ICtrContractStatusItem> list = keyVersionStatusEntry.getValue().getList();
                        ICtrContractResultDao.ICtrContractStatusItem[] objects = list.toArray(new ICtrContractResultDao.ICtrContractStatusItem[list.size()]);
                        _statusItems.put(currency.getCode(), objects[objects.length - 1]);
                    }
                }
            }
        }
    }

    public boolean isPromiseSelected() { return _selectedPromises.contains(_paymentPromice); }

    public void setPromiseSelected(boolean promiseSelected)
    {
        if (promiseSelected)
            _selectedPromises.add(_paymentPromice);

        else _selectedPromises.remove(_paymentPromice);
    }

    public List<ViewWrapper<CtrPaymentResult>> getPaymentResults()
    {
        return _paymentResults;
    }

    public void setPaymentResults(List<ViewWrapper<CtrPaymentResult>> paymentResults)
    {
        this._paymentResults = paymentResults;
    }

    public ViewWrapper<CtrPaymentResult> getPaymentResult()
    {
        return _paymentResult;
    }

    public void setPaymentResult(ViewWrapper<CtrPaymentResult> paymentResult)
    {
        this._paymentResult = paymentResult;
    }

    public List<ViewWrapper<CtrPaymentPromice>> getPaymentPromices()
    {
        return _paymentPromices;
    }

    public void setPaymentPromices(List<ViewWrapper<CtrPaymentPromice>> paymentPromices)
    {
        this._paymentPromices = paymentPromices;
    }

    public ViewWrapper<CtrPaymentPromice> getPaymentPromice()
    {
        return _paymentPromice;
    }

    public void setPaymentPromice(ViewWrapper<CtrPaymentPromice> paymentPromice)
    {
        this._paymentPromice = paymentPromice;
    }
}