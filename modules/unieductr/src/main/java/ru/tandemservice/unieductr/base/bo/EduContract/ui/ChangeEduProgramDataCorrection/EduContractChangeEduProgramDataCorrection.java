/* $Id:$ */
package ru.tandemservice.unieductr.base.bo.EduContract.ui.ChangeEduProgramDataCorrection;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.bo.EduContract.ui.ChangeEduProgramDataCorrection.logic.ContractsComboDSHandler;
import ru.tandemservice.unieductr.base.bo.EduContract.ui.ChangeEduProgramDataCorrection.logic.EduProgramsNewComboDSHandler;
import ru.tandemservice.unieductr.base.bo.EduContract.ui.ChangeEduProgramDataCorrection.logic.EduProgramsOldComboDSHandler;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 14.08.15
 * Time: 12:57
 */
@Configuration
public class EduContractChangeEduProgramDataCorrection extends BusinessComponentManager
{
    public static final String CONTRACTS_DS = "contractsDS";
    public static final String EDU_PROGRAMS_OLD_DS = "eduProgramsOldDS";
    public static final String EDU_PROGRAMS_NEW_DS = "eduProgramsNewDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(CONTRACTS_DS, contractsDSHandler()).addColumn(CtrContractObject.number().s()))
                .addDataSource(selectDS(EDU_PROGRAMS_OLD_DS, eduProgramsOldDSHandler()))
                .addDataSource(selectDS(EDU_PROGRAMS_NEW_DS, eduProgramsNewDSHandler()).addColumn(EduProgramProf.titleWithCodeAndConditionsShortWithForm().s()))
                .create();
    }



    @Bean
    public IDefaultComboDataSourceHandler contractsDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), CtrContractObject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(DQLExpressions.exists(EduCtrEducationPromise.class, EduCtrEducationPromise.src().owner().contract().s(), DQLExpressions.property(alias)));
            }
        }
                .filter(CtrContractObject.number())
                .order(CtrContractObject.number());
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramsOldDSHandler()
    {
        return new EduProgramsOldComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramsNewDSHandler()
    {
        return new EduProgramsNewComboDSHandler(getName());
    }
}
