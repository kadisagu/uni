/* $Id$ */
package ru.tandemservice.unieductr.base.bo.EduProgramPrice.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;

import ru.tandemservice.unieductr.base.entity.EduProgramPrice;

/**
 * @author nvankov
 * @since 2/26/14
 */
public interface IEduProgramPriceDao extends INeedPersistenceSupport
{
    public EduProgramPrice doGetEduProgramPrice(Long eduProgramId);

    CtrContractType getEduProgramContractRootType();

    CtrContractType getEduPriceContractType(EduProgramPrice eduProgramPrice);
}
