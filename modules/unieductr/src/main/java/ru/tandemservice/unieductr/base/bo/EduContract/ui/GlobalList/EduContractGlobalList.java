package ru.tandemservice.unieductr.base.bo.EduContract.ui.GlobalList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.SimpleMergeIdResolver;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.PubCtr.CtrContractVersionPubCtr;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractorRole;
import org.tandemframework.shared.ctr.catalog.entity.CtrPriceCategory;
import org.tandemframework.shared.ctr.catalog.entity.CtrPricePaymentGrid;
import org.tandemframework.shared.person.base.entity.PersonRole;

import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduContractListDataSourceHandler;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;
import ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData;
import ru.tandemservice.unieductr.base.entity.IEducationPromise;

import java.util.Collection;

@Configuration
public class EduContractGlobalList extends BusinessComponentManager
{
    public static final String CONTRACT_LIST_DS = "contractListDS";
    public static final String DS_PAYMENT_GRID = "paymentGridDS";
    public static final String DS_PRICE_CATEGORY = "priceCategoryDS";
    public static final String DS_PRICE_SUB_CATEGORY = "priceSubCategoryDS";

    public static final String BIND_PAYMENT_GRID = "paymentGrid";
    public static final String BIND_PRICE_CATEGORY = "priceCategory";
    public static final String BIND_PRICE_SUB_CATEGORY = "priceSubCategory";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
//        .addAddon(this.uiAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME, UniEduProgramEducationOrgUnitAddon.class))
        .addDataSource(this.searchListDS(CONTRACT_LIST_DS, this.contractListDSColumns(), this.contractListDSHandler()))
        .addDataSource(selectDS(DS_PAYMENT_GRID, paymentGridDSHandler()))
        .addDataSource(selectDS(DS_PRICE_CATEGORY, priceCategoryDSHandler()))
        .addDataSource(selectDS(DS_PRICE_SUB_CATEGORY, priceSubCategoryDSHandler()).addColumn("title", null, new IFormatter<CtrPriceCategory>() {
            @Override public String format(CtrPriceCategory source) { return source.getTitle() + (source.getParent() == null ? "" : " (" + source.getParent().getTitle() + ")"); }}))
        .create();
    }

    @Bean
    public ColumnListExtPoint contractListDSColumns() {
        final IMergeRowIdResolver contractMerger = new SimpleMergeIdResolver(CtrContractVersion.contract().id());
        final IPublisherLinkResolver contractLinkResolver = new IPublisherLinkResolver() {
            @Override public Object getParameters(final IEntity entity) {
                final IEduContractRelation rel = (IEduContractRelation)entity.getProperty(EduContractListDataSourceHandler.VIEW_STUDENT_REL);
                if (null != rel) { return rel.getId(); }
                return ((CtrContractObject)entity.getProperty(CtrContractVersion.contract())).getId();
            }
            @Override public String getComponentName(final IEntity entity) {
                // final IEduContractRelation rel = (IEduContractRelation)entity.getProperty(EduContractListDataSourceHandler.VIEW_STUDENT_REL);
                // if (null == rel) { return null; /* вычисляем компонент по id */ }
                return CtrContractVersionPubCtr.class.getSimpleName();
            }
        };

        return this.columnListExtPointBuilder(CONTRACT_LIST_DS)

        // здесь должна быть ссылка на карточку договора (в контексте, если контекста нет - то в контексте самого договора)
        // .addColumn(textColumn("contractNumber", CtrContractVersion.contract().number()).merger(contractMerger).required(true))
        .addColumn(
            publisherColumn("contractNumber", CtrContractVersion.contract().number())
            .publisherLinkResolver(contractLinkResolver)
            .merger(contractMerger)
            .required(true)
        )

        // здесь должна быть ссылка на страницу с договорами студента (пока делаем ссылку студентов)
        .addColumn(
            publisherColumn("contractStudentList", PersonRole.title().fromAlias(IEduContractRelation.L_PERSON_ROLE))
            .merger(contractMerger)
            .entityListProperty(EduContractListDataSourceHandler.VIEW_STUDENT_REL_LIST)
            .primaryKeyPath(PersonRole.id().fromAlias(IEduContractRelation.L_PERSON_ROLE))
            .required(true)
        )
        .addColumn(textColumn("versionTemplateYear", EducationYear.title().fromAlias(EduContractListDataSourceHandler.VIEW_TEMPLATE+"."+IEducationContractVersionTemplateData.L_EDUCATION_YEAR)).required(true))
        .addColumn(
            publisherColumn("versionEduPromiseList", IEducationPromise.P_EDUCATION_PROMISE_DESCRIPTION)
            .entityListProperty(EduContractListDataSourceHandler.VIEW_EDU_PROMISE_LIST)
            .primaryKeyPath(IEducationPromise.L_EDUCATION_PROMISE_TARGET+".id")
            .required(true)
        )

        .addColumn(textColumn("versionBeginDate", CtrContractVersion.durationBeginDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).required(true))
        .addColumn(textColumn("versionEndDate", CtrContractVersion.durationEndDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).required(true))
        .addColumn(textColumn("versionCustomer", CtrContractVersionContractorRole.contactor().contactor().title().fromAlias(EduContractListDataSourceHandler.VIEW_ROLE_CUSTOMER)).required(true))
        // .addColumn(textColumn("versionProvider", CtrContractVersionContractorRole.contactor().contactor().title().fromAlias(EduContractListDataSourceHandler.VIEW_ROLE_PROVIDER)).required(false))
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> contractListDSHandler() {
        return new EduContractListDataSourceHandler(this.getName())
                .order(CtrContractVersion.contract().number())
                .order(CtrContractVersion.contract().id())
                .order(CtrContractVersion.creationDate())
                .pageable(true);
    }


    @Bean
    public IDefaultComboDataSourceHandler paymentGridDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), CtrPricePaymentGrid.class)
            .filter(CtrPricePaymentGrid.title())
            .order(CtrPricePaymentGrid.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler priceCategoryDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), CtrPriceCategory.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(DQLExpressions.isNull(DQLExpressions.property(alias, CtrPriceCategory.parent())));
            }
        }
            .filter(CtrPriceCategory.title())
            .order(CtrPriceCategory.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler priceSubCategoryDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), CtrPriceCategory.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(DQLExpressions.isNotNull(DQLExpressions.property(alias, CtrPriceCategory.parent())));

                Collection<CtrPriceCategory> priceCategories = context.get(BIND_PRICE_CATEGORY);
                if (priceCategories != null && !priceCategories.isEmpty())
                    dql.where(DQLExpressions.in(DQLExpressions.property(alias, CtrPriceCategory.parent()), priceCategories));
            }
        }
            .filter(CtrPriceCategory.title())
            .order(CtrPriceCategory.title());
    }
}
