/**
 *$Id:$
 */
package ru.tandemservice.unieductr.reports.bo.EduCtrDebitorsPayReport.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections15.keyvalue.MultiKey;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContractResultDao;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contract.*;
import org.tandemframework.shared.ctr.base.entity.contract.gen.CtrContractVersionTemplateDataGen;
import org.tandemframework.shared.ctr.catalog.entity.CtrTemplateScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduContractPaymentPromiceResultPair;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;
import ru.tandemservice.unieductr.base.entity.gen.IEduContractRelationGen;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrTemplateScriptItemCodes;
import ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;

import java.text.NumberFormat;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon.Filters.*;

/**
 * @author Alexander Shaburov
 * @since 06.11.12
 */
public class EduCtrDebitorsPayReportDao extends UniBaseDao implements IEduCtrDebitorsPayReportDao
{
    @Override
    public <M extends Model> RtfDocument createReportRtfDocument(M model)
    {
        List<MultiKey> dataList = getPrepareDataFromDataBase(model);

        CtrTemplateScriptItem template = getCatalogItem(CtrTemplateScriptItem.class, CtrTemplateScriptItemCodes.EDU_CTR_DEBITORS_PAY_REPORT);
        RtfDocument document = new RtfReader().read(template.getTemplate());
        injectTable(document, dataList, model).modify(document);
        injectModifier(document, dataList, model).modify(document);

        return document;
    }

    @Override
    public <M extends Model> EduCtrDebitorsPayReport saveReport(M model, RtfDocument document)
    {
        EduCtrDebitorsPayReport report = new EduCtrDebitorsPayReport();

        DatabaseFile content = new DatabaseFile();
        content.setContent(RtfUtil.toByteArray(document));
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        content.setFilename("debitorsPayReport_" + new DateFormatter("dd_mm_yyyy").format(new Date()) + ".rtf");
        save(content);

        UniEduProgramEducationOrgUnitAddon util = model.getUtil();

        if (model.getOrgUnitId() != null)
            report.setOrgUnit(get(OrgUnit.class, model.getOrgUnitId()));
        report.setContent(content);
        report.setFormingDate(model.getFormingDate());
        report.setReportDate(model.getReportDate());
        report.setCourse(model.isActiveCourse() ? model.getCourse().getTitle() : "");
        report.setGroup(model.isActiveGroup() ? model.getGroup().getTitle() : "");
        report.setFormativeOrgUnit(getValue(util, FORMATIVE_ORG_UNIT, OrgUnit.P_FULL_TITLE));
        report.setTerritorialOrgUnit(getValue(util, TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_FULL_TITLE));
        report.setEducationLevelHighSchool(getValue(util, EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.P_DISPLAYABLE_TITLE));
        report.setDevelopForm(getValue(util, DEVELOP_FORM, DevelopForm.P_TITLE));
        report.setDevelopCondition(getValue(util, DEVELOP_CONDITION, DevelopCondition.P_TITLE));
        report.setDevelopTech(getValue(util, DEVELOP_TECH, DevelopTech.P_TITLE));
        report.setDevelopPeriod(getValue(util, DEVELOP_PERIOD, DevelopPeriod.P_TITLE));

        save(report);
        return report;
    }

    @SuppressWarnings("unchecked")
    private String getValue(UniEduProgramEducationOrgUnitAddon util, UniEduProgramEducationOrgUnitAddon.Filters key, String propertyPath)
    {
        Map<UniEduProgramEducationOrgUnitAddon.Filters, Object> values = util.getValuesMap();
        return util.getFilterConfig(key).isCheckEnableCheckBox() ? UniStringUtils.join((Collection<IEntity>) values.get(key), propertyPath, ", ") : "";
    }

    /**
     * Подготавливает массив данных для отчета. Применяются фильтры с формы.
     *
     * @return {Student, Contract, debt, currencyCode}
     */
    @SuppressWarnings("unchecked")
    protected <M extends Model> List<MultiKey> getPrepareDataFromDataBase(M model)
    {
        // получаем список id студентов, удовлетворяющих фильтрам на форме и подразделению, если отчет строится с подразделения
        final List<Long> studentIds = new ArrayList<>();
        final DQLSelectBuilder studentBuilder = new DQLSelectBuilder().fromEntity(Student.class, "stud").column(property("stud"))
                .joinEntity("stud", DQLJoinType.left, Group.class, "gr", eq(property("gr"), property("stud", Student.group())))
                .where(eq(property(Student.archival().fromAlias("stud")), value(Boolean.FALSE)))
                .where(eq(property(Student.status().active().fromAlias("stud")), value(Boolean.TRUE)))
                .where(or(
                        eq(property("gr", Group.archival()), value(Boolean.FALSE)),
                        isNull(property("gr")))
                );

        if (model.getOrgUnitId() != null)
        {
            studentBuilder.where(or(
                    eq(property(Student.educationOrgUnit().formativeOrgUnit().id().fromAlias("stud")), value(model.getOrgUnitId())),
                    eq(property(Student.educationOrgUnit().territorialOrgUnit().id().fromAlias("stud")), value(model.getOrgUnitId()))
            ));
        }

        if (model.isActiveGroup() && model.getGroup() != null)
            studentBuilder.where(eq(property(Student.group().fromAlias("stud")), value(model.getGroup())));

        if (model.isActiveCourse() && model.getCourse() != null)
            studentBuilder.where(eq(property(Student.course().fromAlias("stud")), value(model.getCourse())));

        BatchUtils.execute(model.getUtil().getEducationOrgUnitFilteredList(), 128,
                           elements -> studentIds.addAll(new DQLSelectBuilder().fromDataSource(studentBuilder.buildQuery(), "b")
                                     .column(property(Student.id().fromAlias("b.stud")))
                                     .where(in(property(Student.educationOrgUnit().fromAlias("b.stud")), elements))
                                     .createStatement(getSession()).<Long>list())
        );

        // получаем список id договоров, для которых есть EduCtrStudentContract из списка студентов полученых выше
        Map<Long, Long> contractStudentIdMap = getStudentContractRel(studentIds);

        // получаем актуальные на дату отчета версии договоров, договоров id которых получены выше
        Map<Long, CtrContractVersion> contractVersionMap = CtrContractVersionManager.instance().dao().getCurrentVersions(contractStudentIdMap.keySet(), model.getReportDate());

        // получаем контрагентов в роли исполнителя для версий договоров полученых выше (если контрагент представитель ВУЗа, то контрагентом считается головное подразделение, иначе студент)
        List<Currency> currencyList = getList(Currency.class);
        model.setCurrencyMap(new HashMap<>());
        for (Currency currency : currencyList)
        {
            model.getCurrencyMap().put(currency.getCode(), currency);
        }

        Map<Long, Map<String, ICtrContractResultDao.ICtrContractStatus>> contractVersionStatusMap = CtrContractVersionManager.instance().results().getContractVersionStatusMap(ids(contractVersionMap.values()), model.getReportDate());

        Map<Long, List<Pair<Long, String>>> debtDataMap = new HashMap<>();
        for (Map.Entry<Long, Map<String, ICtrContractResultDao.ICtrContractStatus>> versionEntry : contractVersionStatusMap.entrySet())
        {

            for (Map.Entry<String, ICtrContractResultDao.ICtrContractStatus> keyVersionStatusEntry : versionEntry.getValue().entrySet())
            {
                for (Currency currency : currencyList)
                {
                    if (EduContractPaymentPromiceResultPair.payDirectedTo(keyVersionStatusEntry.getKey(), TopOrgUnit.getInstance().getId(), currency.getCode()))
                    {
                        Collection<ICtrContractResultDao.ICtrContractStatusItem> list = keyVersionStatusEntry.getValue().getList();
                        ICtrContractResultDao.ICtrContractStatusItem[] objects = list.toArray(new ICtrContractResultDao.ICtrContractStatusItem[list.size()]);

                        for (int i = objects.length - 1; i >= 0; i--)
                        {
                            if (objects[i].getTimestamp() <= model.getReportDate().getTime())
                            {
                                if (objects[i].getDebt() > 0) {
                                    if (!debtDataMap.containsKey(versionEntry.getKey())) {
                                        debtDataMap.put(versionEntry.getKey(), new ArrayList<>());
                                    }
                                    debtDataMap.get(versionEntry.getKey()).add(new Pair<>(objects[i].getDebt(), currency.getCode()));
                                }

                                break;
                            }
                        }
                    }
                }
            }
        }

        debtDataMap.entrySet().stream()
                .filter(debt -> debt.getValue().size() > 1)
                .forEach(debt -> debt.getValue().sort((o1, o2) -> {
                    Map<String, Currency> currencyMap = model.getCurrencyMap();
                    return currencyMap.get(o1.getY()).getTitle().compareTo(currencyMap.get(o2.getY()).getTitle());
                }
                ));

        final Map<Long, Date> contractIdFirstVersionMap = new HashMap<>();
        final Map<Long, CtrContractObject> versionContractId = new HashMap<>();
        BatchUtils.execute(debtDataMap.keySet(), 512, elements -> {
            DQLSelectBuilder contractObjectFromVersions = new DQLSelectBuilder().fromEntity(CtrContractVersion.class, "s")
                    .column(property("s", CtrContractVersion.contract().id()))
                    .where(in(property(CtrContractVersion.id().fromAlias("s")), elements));

            List<CtrContractVersionTemplateData> list = new DQLSelectBuilder().fromEntity(CtrContractVersion.class, "ver")
                    .where(in(property(CtrContractVersion.contract().id().fromAlias("ver")), contractObjectFromVersions.buildQuery()))
                    .where(eq(property(CtrContractVersion.kind().contract().fromAlias("ver")), value(Boolean.TRUE)))
                    .joinEntity("ver", DQLJoinType.inner, CtrContractVersionTemplateData.class, "tmp",
                            eq(property("tmp", CtrContractVersionTemplateData.owner().id()), property("ver", CtrContractVersion.id())))
                    .column(property("tmp"))
                    .createStatement(getSession()).list();

            versionContractId.putAll(new DQLSelectBuilder().fromEntity(CtrContractVersion.class, "s")
                    .where(in(property(CtrContractVersion.id().fromAlias("s")), elements))
                    .createStatement(getSession()).<CtrContractVersion>list().stream()
                    .collect(Collectors.toMap(CtrContractVersion::getId, CtrContractVersion::getContract)));

            contractIdFirstVersionMap.putAll(list.stream()
                    .collect(Collectors.toMap(
                            templateData -> templateData.getOwner().getContract().getId(),
                            templateData -> templateData.getOwner().getDocStartDate())));
        });

        model.setContractIdFirstVersionMap(contractIdFirstVersionMap);

        List<Long> resultStudentIdList = new ArrayList<>();
        for (Long versionId : debtDataMap.keySet())
        {
            resultStudentIdList.add(contractStudentIdMap.get(versionContractId.get(versionId).getId()));
        }

        final Map<Long, Student> studentMap = new HashMap<>();
        BatchUtils.execute(resultStudentIdList, 512, elements -> {
            List<Student> list = new DQLSelectBuilder().fromEntity(Student.class, "s").column(property("s"))
                    .where(in(property(Student.id().fromAlias("s")), elements))
                    .createStatement(getSession()).<Student>list();

            for (Student student : list)
                studentMap.put(student.getId(), student);
        });

        List<MultiKey> resultList = new ArrayList<>();
        for (Long versionId : debtDataMap.keySet())
        {
            Student student = studentMap.get(contractStudentIdMap.get(versionContractId.get(versionId).getId()));
            CtrContractObject contract = versionContractId.get(versionId);
            for (Pair<Long, String> debt : debtDataMap.get(versionId)) {
                Long debtSum = debt.getX();
                String currencyCode = debt.getY();

                resultList.add(new MultiKey(student, contract, debtSum, currencyCode));
            }
        }

        Collections.sort(resultList, (o1, o2) -> {
            Student student1 = (Student) o1.getKey(0);
            Student student2 = (Student) o2.getKey(0);

            int result = Integer.valueOf(student1.getCourse().getIntValue()).compareTo(student2.getCourse().getIntValue());
            if (result == 0)
            {
                if(student1.getGroup() == null && student2.getGroup() == null)
                    result = 0;
                else if(student1.getGroup() != null && student2.getGroup() != null)
                    result = student1.getGroup().getTitle().compareTo(student2.getGroup().getTitle());
                else if(student1.getGroup() != null)
                    result = -1;
                else
                    result = 1;
            }

            if (result == 0)
                result = Student.FULL_FIO_AND_ID_COMPARATOR.compare(student1, student2);

            if (result == 0)
            {
                CtrContractObject firstStudentContract = (CtrContractObject) o1.getKey(1);
                CtrContractObject secondStudentContract = (CtrContractObject) o2.getKey(1);
                result = firstStudentContract.getNumber().compareTo(secondStudentContract.getNumber());
            }

            return result;
        });

        return resultList;
    }

    @Override
    public Map<Long, Long> getStudentContractRel(List<Long> studentIds)
    {
        final Map<Long, Long> contractStudentIdMap = Maps.newHashMap();

        BatchUtils.execute(studentIds, 512, elements -> {
            DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                    new DQLSelectBuilder().fromEntity(EduCtrStudentContract.class, "sc")
                            .joinEntity("sc", DQLJoinType.inner, IEduContractRelation.class, "ie", eq(property("sc", EduCtrStudentContract.contractObject().id()), property("ie", IEduContractRelationGen.contractObject().id())))
                            .where(in(property("sc", EduCtrStudentContract.student().id()), elements))
            );
            int contract_id_col = dql.column(property("sc", EduCtrStudentContract.contractObject().id()));
            int student_id_col = dql.column(property("sc", EduCtrStudentContract.student().id()));

            for (Object[] row : dql.getDql().createStatement(getSession()).<Object[]>list())
            {
                contractStudentIdMap.put((Long) row[contract_id_col], (Long) row[student_id_col]);
            }
        });
        return contractStudentIdMap;
    }

    protected <M extends Model> RtfTableModifier injectTable(RtfDocument document, List<MultiKey> dataList, M model)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        if (dataList.isEmpty())
        {
            tableModifier.put("T", new String[][]{});
            return tableModifier;
        }

        boolean multiCurrency = false;

        String currentCode = (String) dataList.get(0).getKey(3);
        Map<OrgUnit, List<MultiKey>> orgUnitMap = new TreeMap<>(ITitled.TITLED_COMPARATOR);
        for (MultiKey multiKey : dataList)
        {
            Student student = (Student) multiKey.getKey(0);
            OrgUnit orgUnit = student.getEducationOrgUnit().getFormativeOrgUnit();

            if (!orgUnitMap.containsKey(orgUnit))
                orgUnitMap.put(orgUnit, Lists.<MultiKey>newLinkedList());
            orgUnitMap.get(orgUnit).add(multiKey);

            if (!currentCode.equals(multiKey.getKey(3)))
            {
                multiCurrency = true;
            }
        }

        int number = 0;
        final List<Integer> orgUnitRowIndexList = Lists.newArrayList();
        Set<OrgUnit> orgUnitList = orgUnitMap.keySet();
        List<String[]> lineList = new ArrayList<>();
        for (OrgUnit orgUnit : orgUnitList)
        {
            List<MultiKey> multiKeys = orgUnitMap.get(orgUnit);
            if (orgUnitList.size() > 1)
            {
                String[] orgUnitLine = new String[1];
                orgUnitLine[0] = orgUnit.getTitle();
                lineList.add(orgUnitLine);
                orgUnitRowIndexList.add(number++);
            }

            int index = 1;
            for (MultiKey multiKey : multiKeys)
            {
                Student student = (Student) multiKey.getKey(0);
                CtrContractObject contract = (CtrContractObject) multiKey.getKey(1);
                Long debt = (Long) multiKey.getKey(2);
                String currencyCode = (String) multiKey.getKey(3);
                Date docStartDate = model.getContractIdFirstVersionMap().get(contract.getContractObject().getId());

                NumberFormat numberFormat = NumberFormat.getInstance();
                numberFormat.setMinimumFractionDigits(2);

                String[] line = new String[multiCurrency ? 7 : 6];

                line[0] = String.valueOf(index++);
                line[1] = student.getPerson().getFullFio();
                line[2] = student.getCourse().getTitle();
                line[3] = student.getGroup() != null ? student.getGroup().getTitle() : "";
                line[4] = "№" + contract.getNumber() + (docStartDate != null ? (" от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(docStartDate)) : "");
                line[5] = multiCurrency ? numberFormat.format(Currency.wrap(debt)) : Currency.format(model.getCurrencyMap().get(currencyCode), debt);
                if (multiCurrency)
                    line[6] = model.getCurrencyMap().get(currencyCode).getTitle();

                lineList.add(line);
                number++;
            }
        }

        tableModifier.put("T", lineList.toArray(new String[lineList.size()][multiCurrency ? 7 : 6]));


        final boolean finalMultiCurrency = multiCurrency;
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                // в тех строках, где будет объединение ячеек нужно выделять болдом
                if (orgUnitRowIndexList.contains(rowIndex))
                {
                    SharedRtfUtil.setCellAlignment(cell, IRtfData.QC);
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                }
                return null;
            }

            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                if (finalMultiCurrency)
                {
                    for (RtfRow row : table.getRowList())
                    {
                        final int rowNumber = table.getRowList().indexOf(row);

                        RtfUtil.splitRow(row, 5, (newCell, index) -> {
                            if (rowNumber > 0)
                                return;

                            String content = index == 0 ? "Задолженность" : "Валюта";
                            IRtfElement text = RtfBean.getElementFactory().createRtfText(content);
                            newCell.getElementList().add(text);
                        }, new int[]{1, 1});
                    }
                }
                else
                {
                    RtfCell cell = table.getRowList().get(0).getCellList().get(5);
                    IRtfElement text = RtfBean.getElementFactory().createRtfText("Задолженность");
                    cell.getElementList().add(text);
                }
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                // объединяем все строки с нужными индексами
                for (Integer rowIndex : orgUnitRowIndexList)
                {
                    RtfRow row = newRowList.get(rowIndex + startIndex);
                    RtfUtil.unitAllCell(row, 0);
                }
            }
        });

        return tableModifier;
    }

    protected <M extends Model> RtfInjectModifier injectModifier(RtfDocument document, List<MultiKey> dataList, M model)
    {
        return new RtfInjectModifier()
                .put("reportDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getReportDate()));
    }
}
