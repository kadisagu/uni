package ru.tandemservice.unieductr.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData;
import ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные шаблона версии договора на обучение по обр. программе
 *
 * Данные базового шаблона для создания договора или доп.соглашения на обучение по образовательной программе.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduCtrContractVersionTemplateDataGen extends CtrContractVersionTemplateData
 implements IEducationContractVersionTemplateData{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData";
    public static final String ENTITY_NAME = "eduCtrContractVersionTemplateData";
    public static final int VERSION_HASH = -2123464288;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String P_CIPHER = "cipher";
    public static final String L_COST = "cost";

    private EducationYear _educationYear;     // Учебный год, на который создана версия
    private String _cipher;     // Шифр договора
    private CtrPriceElementCost _cost;     // Цена

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год, на который создана версия. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год, на который создана версия. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Шифр договора.
     */
    @Length(max=255)
    public String getCipher()
    {
        return _cipher;
    }

    /**
     * @param cipher Шифр договора.
     */
    public void setCipher(String cipher)
    {
        dirty(_cipher, cipher);
        _cipher = cipher;
    }

    /**
     * @return Цена.
     */
    public CtrPriceElementCost getCost()
    {
        return _cost;
    }

    /**
     * @param cost Цена.
     */
    public void setCost(CtrPriceElementCost cost)
    {
        dirty(_cost, cost);
        _cost = cost;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduCtrContractVersionTemplateDataGen)
        {
            setEducationYear(((EduCtrContractVersionTemplateData)another).getEducationYear());
            setCipher(((EduCtrContractVersionTemplateData)another).getCipher());
            setCost(((EduCtrContractVersionTemplateData)another).getCost());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduCtrContractVersionTemplateDataGen> extends CtrContractVersionTemplateData.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduCtrContractVersionTemplateData.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EduCtrContractVersionTemplateData is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return obj.getEducationYear();
                case "cipher":
                    return obj.getCipher();
                case "cost":
                    return obj.getCost();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "cipher":
                    obj.setCipher((String) value);
                    return;
                case "cost":
                    obj.setCost((CtrPriceElementCost) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                        return true;
                case "cipher":
                        return true;
                case "cost":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return true;
                case "cipher":
                    return true;
                case "cost":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return EducationYear.class;
                case "cipher":
                    return String.class;
                case "cost":
                    return CtrPriceElementCost.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduCtrContractVersionTemplateData> _dslPath = new Path<EduCtrContractVersionTemplateData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduCtrContractVersionTemplateData");
    }
            

    /**
     * @return Учебный год, на который создана версия. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Шифр договора.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData#getCipher()
     */
    public static PropertyPath<String> cipher()
    {
        return _dslPath.cipher();
    }

    /**
     * @return Цена.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData#getCost()
     */
    public static CtrPriceElementCost.Path<CtrPriceElementCost> cost()
    {
        return _dslPath.cost();
    }

    public static class Path<E extends EduCtrContractVersionTemplateData> extends CtrContractVersionTemplateData.Path<E>
    {
        private EducationYear.Path<EducationYear> _educationYear;
        private PropertyPath<String> _cipher;
        private CtrPriceElementCost.Path<CtrPriceElementCost> _cost;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год, на который создана версия. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Шифр договора.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData#getCipher()
     */
        public PropertyPath<String> cipher()
        {
            if(_cipher == null )
                _cipher = new PropertyPath<String>(EduCtrContractVersionTemplateDataGen.P_CIPHER, this);
            return _cipher;
        }

    /**
     * @return Цена.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData#getCost()
     */
        public CtrPriceElementCost.Path<CtrPriceElementCost> cost()
        {
            if(_cost == null )
                _cost = new CtrPriceElementCost.Path<CtrPriceElementCost>(L_COST, this);
            return _cost;
        }

        public Class getEntityClass()
        {
            return EduCtrContractVersionTemplateData.class;
        }

        public String getEntityName()
        {
            return "eduCtrContractVersionTemplateData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
