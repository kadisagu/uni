/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrObligationsContractReport.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unieductr.reports.entity.EduCtrObligationsContractReport;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 18.08.2016
 */
@Configuration
public class EduCtrObligationsContractReportList extends BusinessComponentManager
{
    private DQLOrderDescriptionRegistry _orderDescriptionRegistry = new DQLOrderDescriptionRegistry(EduCtrObligationsContractReport.class, "b");

    // dataSource
    public static final String REPORT_LIST_DS = "reportListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REPORT_LIST_DS, columnListHandler(), reportListDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> reportListDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName(), EduCtrObligationsContractReport.class)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Date formingDateFrom = context.get(EduCtrObligationsContractReportListUI.PROP_FORMING_DATE_FROM);
                Date formingDateTo = context.get(EduCtrObligationsContractReportListUI.PROP_FORMING_DATE_TO);
                Long orgUnitId = context.get(EduCtrObligationsContractReportListUI.PROP_ORG_UNIT_ID);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EduCtrObligationsContractReport.class, "b");


                if (orgUnitId != null)
                    builder.where(eq(property(EduCtrObligationsContractReport.orgUnit().id().fromAlias("b")), value(orgUnitId)));

                if (formingDateFrom != null)
                    builder.where(ge(property(EduCtrObligationsContractReport.formingDate().fromAlias("b")), value(formingDateFrom, PropertyType.DATE)));
                if (formingDateTo != null)
                    builder.where(le(property(EduCtrObligationsContractReport.formingDate().fromAlias("b")), value(formingDateTo, PropertyType.DATE)));

                _orderDescriptionRegistry.applyOrder(builder, input.getEntityOrder());

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
            }
        };
    }

    @Bean
    public ColumnListExtPoint columnListHandler()
    {
        return columnListExtPointBuilder(REPORT_LIST_DS)
                .addColumn(indicatorColumn("report").defaultIndicatorItem(new IndicatorColumn.Item("report")))
                .addColumn(publisherColumn("formDate", EduCtrObligationsContractReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order())
                .addColumn(textColumn("reportDate", EduCtrObligationsContractReport.reportDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .addColumn(textColumn("periodDate", EduCtrObligationsContractReport.periodString()))
                .addColumn(textColumn("formativeOrgUnit", EduCtrObligationsContractReport.formativeOrgUnit()).order())
                .addColumn(textColumn("territorialOrgUnit", EduCtrObligationsContractReport.territorialOrgUnit()).order())
                .addColumn(textColumn("educationLevelHighSchool", EduCtrObligationsContractReport.educationLevelHighSchool()).order())
                .addColumn(textColumn("developForm", EduCtrObligationsContractReport.developForm()).order())
                .addColumn(textColumn("developCondition", EduCtrObligationsContractReport.developCondition()).order())
                .addColumn(textColumn("developTech", EduCtrObligationsContractReport.developTech()).order())
                .addColumn(textColumn("developPeriod", EduCtrObligationsContractReport.developPeriod()).order())
                .addColumn(textColumn("course", EduCtrObligationsContractReport.course()).order())
                .addColumn(textColumn("group", EduCtrObligationsContractReport.group()).order())
                .addColumn(textColumn("status", EduCtrObligationsContractReport.studentStatus()))
                .addColumn(textColumn("comment", EduCtrObligationsContractReport.comment()))
                .addColumn(actionColumn("print", new Icon("printer"), "onClickPrintReport"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon("delete"), DELETE_LISTENER).permissionKey("ui:deleteStorableReportPermissionKey")
                                   .alert(FormattedMessage.with().template("reportListDS.delete.alert").parameter(EduCtrObligationsContractReport.formingDate().s(), DateFormatter.DATE_FORMATTER_WITH_TIME).create()))
                .create();
    }
}