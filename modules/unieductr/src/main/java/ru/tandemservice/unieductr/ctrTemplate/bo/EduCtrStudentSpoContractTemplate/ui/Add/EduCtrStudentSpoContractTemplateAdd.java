package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentSpoContractTemplate.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;
import org.tandemframework.shared.ctr.base.entity.contactor.PhysicalContactor;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentSpoContractTemplate.EduCtrStudentSpoContractTemplateManager;

@Configuration
public class EduCtrStudentSpoContractTemplateAdd extends BusinessComponentManager
{
    public static final String DS_FORMATIVE_ORG_UNIT = "formativeOrgUnitDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return EduCtrStudentSpoContractTemplateManager.programProfPresenterExtPointBuilder(this.presenterExtPointBuilder(), this.getName())
            .addDataSource(EduContractManager.instance().customerDSConfig())
            .addDataSource(formativeOrgUnitDSConfig())
            .create();
    }

    @Bean
    public SelectDSConfig formativeOrgUnitDSConfig() {
        return selectDS(DS_FORMATIVE_ORG_UNIT)
                .addColumn(OrgUnit.fullTitle().s())
                .handler(UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler())
                .create();
    }
}
