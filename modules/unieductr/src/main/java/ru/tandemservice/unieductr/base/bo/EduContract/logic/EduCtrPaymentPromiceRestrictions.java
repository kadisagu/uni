package ru.tandemservice.unieductr.base.bo.EduContract.logic;

import java.util.Collection;
import java.util.Collections;

import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrVersionTemplatePromiceRestrictions;
import org.tandemframework.shared.ctr.base.bo.CtrPaymentPromice.ui.SectionPromice.CtrPaymentPromiceSectionPromiceUI;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor;

import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;

/**
 * @author vdanilov
 */
public class EduCtrPaymentPromiceRestrictions implements ICtrVersionTemplatePromiceRestrictions {

    final CtrContractVersionContractor provider;
    final CtrContractVersionContractor customer;

    public EduCtrPaymentPromiceRestrictions(final CtrContractVersionContractor provider, final CtrContractVersionContractor customer) {
        this.provider = provider;
        this.customer = customer;
    }

    public static EduCtrPaymentPromiceRestrictions get(final CtrContractVersion version) {
        return new EduCtrPaymentPromiceRestrictions(
            CtrContractVersionManager.instance().dao().getContactor(version, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER),
            CtrContractVersionManager.instance().dao().getContactor(version, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER)
        );
    }

    @Override
    public boolean isAllowFeature(final String key) {
        return true; // можно все
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> Collection<T> filterOptions(final String key, final Collection<T> options) {
        if (null == options || options.isEmpty()) {
            return Collections.emptyList();
        }

        // фильтруем КЛ-получателя и КЛ-плательщика - разрешаем только платежи от заказчика к исполнителю
        if (CtrPaymentPromiceSectionPromiceUI.RESTRICTION_KEY_SRC_CONTRACTOR_LIST.equals(key) && customer != null && options.contains(customer)) {
            return Collections.singletonList((T) customer);
        }
        if (CtrPaymentPromiceSectionPromiceUI.RESTRICTION_KEY_DST_CONTRACTOR_LIST.equals(key) && provider != null && options.contains(provider)) {
            return Collections.singletonList((T) provider);
        }

        // не знаем, что это нам такое передали
        return Collections.emptyList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getDefaultValue(final String key, final Collection<T> options) {

        // дефолтные КЛ-получатель и КЛ-плательщик - платежи от заказчика к исполнителю
        if (CtrPaymentPromiceSectionPromiceUI.RESTRICTION_KEY_DEFAULT_SRC.equals(key)) {
            return (T) customer;
        }
        if (CtrPaymentPromiceSectionPromiceUI.RESTRICTION_KEY_DEFAULT_DST.equals(key)) {
            return (T) provider;
        }

        // не знаем, что это нам такое передали
        return null;
    }
}
