package ru.tandemservice.unieductr.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вид договора или доп. соглашения"
 * Имя сущности : ctrContractKind
 * Файл data.xml : unieductr.data.xml
 */
public interface CtrContractKindCodes
{
    /** Константа кода (code) элемента : Договор на обучение по ОП ВО (двухсторонний) (title) */
    String EDU_CONTRACT_VO_2_SIDES = "edu.vo.2s";
    /** Константа кода (code) элемента : Договор на обучение по ОП ВО (трехсторонний с физ. лицом) (title) */
    String EDU_CONTRACT_VO_3_SIDES_PERSON = "edu.vo.3sp";
    /** Константа кода (code) элемента : Договор на обучение по ОП ВО (трехсторонний с юр. лицом) (title) */
    String EDU_CONTRACT_VO_3_SIDES_ORG = "edu.vo.3so";
    /** Константа кода (code) элемента : Договор на обучение по ОП СПО (двухсторонний) (title) */
    String EDU_CONTRACT_SPO_2_SIDES = "edu.spo.2s";
    /** Константа кода (code) элемента : Договор на обучение по ОП СПО (трехсторонний с физ. лицом) (title) */
    String EDU_CONTRACT_SPO_3_SIDES_PERSON = "edu.spo.3sp";
    /** Константа кода (code) элемента : Договор на обучение по ОП СПО (трехсторонний с юр. лицом) (title) */
    String EDU_CONTRACT_SPO_3_SIDES_ORG = "edu.spo.3so";
    /** Константа кода (code) элемента : Договор на обучение по ОП ДПО (двухсторонний) (title) */
    String EDU_CONTRACT_DPO_2_SIDES = "edu.dpo.2s";
    /** Константа кода (code) элемента : Договор на обучение по ОП ДПО (трехсторонний с физ. лицом) (title) */
    String EDU_CONTRACT_DPO_3_SIDES_PERSON = "edu.dpo.3sp";
    /** Константа кода (code) элемента : Договор на обучение по ОП ДПО (трехсторонний с юр. лицом) (title) */
    String EDU_CONTRACT_DPO_3_SIDES_ORG = "edu.dpo.3so";
    /** Константа кода (code) элемента : Доп. соглашение об изменении стоимости обучения, порядка оплаты (двухсторонний) (title) */
    String TERM_EDU_COST_2_SIDES = "edu.termeducost.2s";
    /** Константа кода (code) элемента : Доп. соглашение об изменении стоимости обучения, порядка оплаты (трехсторонний с физ. лицом) (title) */
    String TERM_EDU_COST_3_SIDES_PERSON = "edu.termeducost.3sp";
    /** Константа кода (code) элемента : Доп. соглашение об изменении стоимости обучения, порядка оплаты (трехсторонний с юр. лицом) (title) */
    String TERM_EDU_COST_3_SIDES_ORG = "edu.termeducost.3so";
    /** Константа кода (code) элемента : Доп. соглашение об изменении ОП (двухсторонний) (title) */
    String PROGRAM_CHANGE_2_SIDES = "edu.programchange.2s";
    /** Константа кода (code) элемента : Доп. соглашение об изменении ОП (трехсторонний с физ. лицом) (title) */
    String PROGRAM_CHANGE_3_SIDES_PERSON = "edu.programchange.3sp";
    /** Константа кода (code) элемента : Доп. соглашение об изменении ОП (трехсторонний с юр. лицом) (title) */
    String PROGRAM_CHANGE_3_SIDES_ORG = "edu.programchange.3so";
    /** Константа кода (code) элемента : Доп. соглашение на расторжение договоров (title) */
    String EDU_CTR_STOP_CONTRACT = "edu.ctrstopcontract";

    Set<String> CODES = ImmutableSet.of(EDU_CONTRACT_VO_2_SIDES, EDU_CONTRACT_VO_3_SIDES_PERSON, EDU_CONTRACT_VO_3_SIDES_ORG, EDU_CONTRACT_SPO_2_SIDES, EDU_CONTRACT_SPO_3_SIDES_PERSON, EDU_CONTRACT_SPO_3_SIDES_ORG, EDU_CONTRACT_DPO_2_SIDES, EDU_CONTRACT_DPO_3_SIDES_PERSON, EDU_CONTRACT_DPO_3_SIDES_ORG, TERM_EDU_COST_2_SIDES, TERM_EDU_COST_3_SIDES_PERSON, TERM_EDU_COST_3_SIDES_ORG, PROGRAM_CHANGE_2_SIDES, PROGRAM_CHANGE_3_SIDES_PERSON, PROGRAM_CHANGE_3_SIDES_ORG, EDU_CTR_STOP_CONTRACT);
}
