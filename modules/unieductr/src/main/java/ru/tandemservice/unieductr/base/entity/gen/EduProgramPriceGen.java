package ru.tandemservice.unieductr.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.ctr.base.entity.ICtrPriceElement;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.unieductr.base.entity.EduProgramPrice;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Цена на образовательную программу
 *
 * Цена на образовательную программу
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramPriceGen extends EntityBase
 implements ICtrPriceElement, INaturalIdentifiable<EduProgramPriceGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unieductr.base.entity.EduProgramPrice";
    public static final String ENTITY_NAME = "eduProgramPrice";
    public static final int VERSION_HASH = -329254678;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROGRAM = "program";
    public static final String P_CIPHER = "cipher";

    private EduProgram _program;     // Образовательная программа
    private String _cipher;     // Шифр договора

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Образовательная программа. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EduProgram getProgram()
    {
        return _program;
    }

    /**
     * @param program Образовательная программа. Свойство не может быть null и должно быть уникальным.
     */
    public void setProgram(EduProgram program)
    {
        dirty(_program, program);
        _program = program;
    }

    /**
     * @return Шифр договора.
     */
    @Length(max=255)
    public String getCipher()
    {
        return _cipher;
    }

    /**
     * @param cipher Шифр договора.
     */
    public void setCipher(String cipher)
    {
        dirty(_cipher, cipher);
        _cipher = cipher;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EduProgramPriceGen)
        {
            if (withNaturalIdProperties)
            {
                setProgram(((EduProgramPrice)another).getProgram());
            }
            setCipher(((EduProgramPrice)another).getCipher());
        }
    }

    public INaturalId<EduProgramPriceGen> getNaturalId()
    {
        return new NaturalId(getProgram());
    }

    public static class NaturalId extends NaturalIdBase<EduProgramPriceGen>
    {
        private static final String PROXY_NAME = "EduProgramPriceNaturalProxy";

        private Long _program;

        public NaturalId()
        {}

        public NaturalId(EduProgram program)
        {
            _program = ((IEntity) program).getId();
        }

        public Long getProgram()
        {
            return _program;
        }

        public void setProgram(Long program)
        {
            _program = program;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EduProgramPriceGen.NaturalId) ) return false;

            EduProgramPriceGen.NaturalId that = (NaturalId) o;

            if( !equals(getProgram(), that.getProgram()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getProgram());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getProgram());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramPriceGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramPrice.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramPrice();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "program":
                    return obj.getProgram();
                case "cipher":
                    return obj.getCipher();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "program":
                    obj.setProgram((EduProgram) value);
                    return;
                case "cipher":
                    obj.setCipher((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "program":
                        return true;
                case "cipher":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "program":
                    return true;
                case "cipher":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "program":
                    return EduProgram.class;
                case "cipher":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgramPrice> _dslPath = new Path<EduProgramPrice>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramPrice");
    }
            

    /**
     * @return Образовательная программа. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unieductr.base.entity.EduProgramPrice#getProgram()
     */
    public static EduProgram.Path<EduProgram> program()
    {
        return _dslPath.program();
    }

    /**
     * @return Шифр договора.
     * @see ru.tandemservice.unieductr.base.entity.EduProgramPrice#getCipher()
     */
    public static PropertyPath<String> cipher()
    {
        return _dslPath.cipher();
    }

    public static class Path<E extends EduProgramPrice> extends EntityPath<E>
    {
        private EduProgram.Path<EduProgram> _program;
        private PropertyPath<String> _cipher;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Образовательная программа. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unieductr.base.entity.EduProgramPrice#getProgram()
     */
        public EduProgram.Path<EduProgram> program()
        {
            if(_program == null )
                _program = new EduProgram.Path<EduProgram>(L_PROGRAM, this);
            return _program;
        }

    /**
     * @return Шифр договора.
     * @see ru.tandemservice.unieductr.base.entity.EduProgramPrice#getCipher()
     */
        public PropertyPath<String> cipher()
        {
            if(_cipher == null )
                _cipher = new PropertyPath<String>(EduProgramPriceGen.P_CIPHER, this);
            return _cipher;
        }

        public Class getEntityClass()
        {
            return EduProgramPrice.class;
        }

        public String getEntityName()
        {
            return "eduProgramPrice";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
