package ru.tandemservice.unieductr.student.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData;
import ru.tandemservice.unieductr.student.entity.EduCtrTermEducationCostAgreementTemplateData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Изменение стоимости обучения, порядка оплаты
 *
 * Изменение стоимости обучения, порядка оплаты
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduCtrTermEducationCostAgreementTemplateDataGen extends EduCtrContractVersionTemplateData
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unieductr.student.entity.EduCtrTermEducationCostAgreementTemplateData";
    public static final String ENTITY_NAME = "eduCtrTermEducationCostAgreementTemplateData";
    public static final int VERSION_HASH = 908338245;
    private static IEntityMeta ENTITY_META;

    public static final String L_YEAR_PART = "yearPart";
    public static final String P_MOTHERHOOD_BENEFIT = "motherhoodBenefit";

    private YearDistributionPart _yearPart;     // Часть года
    private boolean _motherhoodBenefit;     // Оплата обучения за счет средств материнского капитала

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Часть года. Свойство не может быть null.
     */
    @NotNull
    public YearDistributionPart getYearPart()
    {
        return _yearPart;
    }

    /**
     * @param yearPart Часть года. Свойство не может быть null.
     */
    public void setYearPart(YearDistributionPart yearPart)
    {
        dirty(_yearPart, yearPart);
        _yearPart = yearPart;
    }

    /**
     * @return Оплата обучения за счет средств материнского капитала. Свойство не может быть null.
     */
    @NotNull
    public boolean isMotherhoodBenefit()
    {
        return _motherhoodBenefit;
    }

    /**
     * @param motherhoodBenefit Оплата обучения за счет средств материнского капитала. Свойство не может быть null.
     */
    public void setMotherhoodBenefit(boolean motherhoodBenefit)
    {
        dirty(_motherhoodBenefit, motherhoodBenefit);
        _motherhoodBenefit = motherhoodBenefit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduCtrTermEducationCostAgreementTemplateDataGen)
        {
            setYearPart(((EduCtrTermEducationCostAgreementTemplateData)another).getYearPart());
            setMotherhoodBenefit(((EduCtrTermEducationCostAgreementTemplateData)another).isMotherhoodBenefit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduCtrTermEducationCostAgreementTemplateDataGen> extends EduCtrContractVersionTemplateData.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduCtrTermEducationCostAgreementTemplateData.class;
        }

        public T newInstance()
        {
            return (T) new EduCtrTermEducationCostAgreementTemplateData();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "yearPart":
                    return obj.getYearPart();
                case "motherhoodBenefit":
                    return obj.isMotherhoodBenefit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "yearPart":
                    obj.setYearPart((YearDistributionPart) value);
                    return;
                case "motherhoodBenefit":
                    obj.setMotherhoodBenefit((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "yearPart":
                        return true;
                case "motherhoodBenefit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "yearPart":
                    return true;
                case "motherhoodBenefit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "yearPart":
                    return YearDistributionPart.class;
                case "motherhoodBenefit":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduCtrTermEducationCostAgreementTemplateData> _dslPath = new Path<EduCtrTermEducationCostAgreementTemplateData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduCtrTermEducationCostAgreementTemplateData");
    }
            

    /**
     * @return Часть года. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.student.entity.EduCtrTermEducationCostAgreementTemplateData#getYearPart()
     */
    public static YearDistributionPart.Path<YearDistributionPart> yearPart()
    {
        return _dslPath.yearPart();
    }

    /**
     * @return Оплата обучения за счет средств материнского капитала. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.student.entity.EduCtrTermEducationCostAgreementTemplateData#isMotherhoodBenefit()
     */
    public static PropertyPath<Boolean> motherhoodBenefit()
    {
        return _dslPath.motherhoodBenefit();
    }

    public static class Path<E extends EduCtrTermEducationCostAgreementTemplateData> extends EduCtrContractVersionTemplateData.Path<E>
    {
        private YearDistributionPart.Path<YearDistributionPart> _yearPart;
        private PropertyPath<Boolean> _motherhoodBenefit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Часть года. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.student.entity.EduCtrTermEducationCostAgreementTemplateData#getYearPart()
     */
        public YearDistributionPart.Path<YearDistributionPart> yearPart()
        {
            if(_yearPart == null )
                _yearPart = new YearDistributionPart.Path<YearDistributionPart>(L_YEAR_PART, this);
            return _yearPart;
        }

    /**
     * @return Оплата обучения за счет средств материнского капитала. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.student.entity.EduCtrTermEducationCostAgreementTemplateData#isMotherhoodBenefit()
     */
        public PropertyPath<Boolean> motherhoodBenefit()
        {
            if(_motherhoodBenefit == null )
                _motherhoodBenefit = new PropertyPath<Boolean>(EduCtrTermEducationCostAgreementTemplateDataGen.P_MOTHERHOOD_BENEFIT, this);
            return _motherhoodBenefit;
        }

        public Class getEntityClass()
        {
            return EduCtrTermEducationCostAgreementTemplateData.class;
        }

        public String getEntityName()
        {
            return "eduCtrTermEducationCostAgreementTemplateData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
