package ru.tandemservice.unieductr.base.bo.EduContract.ui.PaymentResultAddEdit;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.commonbase.catalog.entity.codes.CurrencyCodes;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentResult;
import org.tandemframework.shared.ctr.catalog.entity.CtrPaymentDocumentType;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Input({
    @Bind(key=EduContractPaymentResultAddEditUI.VERSION_ID, binding="versionHolder.id", required=true),
    @Bind(key=UIPresenter.PUBLISHER_ID, binding="resultHolder.id")
})
public class EduContractPaymentResultAddEditUI extends UIPresenter
{
    public static final String VERSION_ID = "versionId";

    private final EntityHolder<CtrContractVersion> versionHolder = new EntityHolder<>();
    public EntityHolder<CtrContractVersion> getVersionHolder() { return this.versionHolder; }
    public CtrContractVersion getVersion() { return this.getVersionHolder().getValue(); }

    private final EntityHolder<CtrPaymentResult> resultHolder = new EntityHolder<>(new CtrPaymentResult());
    public EntityHolder<CtrPaymentResult> getResultHolder() { return this.resultHolder; }
    public CtrPaymentResult getResult() { return this.getResultHolder().getValue(); }

    private List<ContactorPerson> contractPersonList;
    public List<ContactorPerson> getContractPersonList() { return this.contractPersonList; }
    public void setContractPersonList(final List<ContactorPerson> contractPersonList) { this.contractPersonList = contractPersonList; }

    private List<Currency> currencyList;
    public List<Currency> getCurrencyList() { return this.currencyList; }
    public void setCurrencyList(final List<Currency> currencyList) { this.currencyList = currencyList; }

    private List<CtrPaymentDocumentType> paymentDocumentTypeList;
    public List<CtrPaymentDocumentType> getPaymentDocumentTypeList() { return this.paymentDocumentTypeList; }
    public void setPaymentDocumentTypeList(final List<CtrPaymentDocumentType> paymentDocumentTypeList) { this.paymentDocumentTypeList = paymentDocumentTypeList; }

    public boolean isAddForm() {return null == this.getResult().getId();}

    @Override
    public void onComponentRefresh()
    {
        // обновляем версию
        final CtrContractVersion version = this.getVersionHolder().refresh(CtrContractVersion.class);

        // список валют и типы документов
        this.setCurrencyList(DataAccessServices.dao().getList(Currency.class, Currency.P_CODE));
        this.setPaymentDocumentTypeList(DataAccessServices.dao().getList(CtrPaymentDocumentType.class, CtrPaymentDocumentType.P_CODE));

        // список контактных лиц в версии
        this.setContractPersonList(new ArrayList<>(CollectionUtils.collect(
                CtrContractVersionManager.instance().dao().getContractors(version),
                CtrContractVersionContractor::getContactor
        )));

        // грузим факт оплаты
        final CtrPaymentResult result = this.getResultHolder().refresh();

        // параметры платежа
        if (null == result.getSrc()) {
            final CtrContractVersionContractor customer = CtrContractVersionManager.instance().dao().getContactor(version, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER);
            if (null != customer) { result.setSrc(customer.getContactor()); }
        }
        if (null == result.getDst()) {
            final CtrContractVersionContractor provider = CtrContractVersionManager.instance().dao().getContactor(version, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER);
            if (null != provider) { result.setDst(provider.getContactor()); }
        }
        if (null == result.getCurrency()) {
            result.setCurrency(DataAccessServices.dao().getByCode(Currency.class, CurrencyCodes.RUB));
        }
        if (null == result.getTimestamp()) {
            result.setTimestamp(new Date());
        }
    }

    public void onClickApply() {
        checkParties();
        this.getResult().setContract(this.getVersion().getContract());
        DataAccessServices.dao().save(this.getResult());
        this.deactivate();
    }

    private void checkParties()
    {
        if((getResult().getSrc() instanceof EmployeePostContactor && getResult().getDst() instanceof EmployeePostContactor)
                || (getResult().getSrc() instanceof JuridicalContactor && getResult().getDst() instanceof JuridicalContactor && Objects.equals(((JuridicalContactor) getResult().getSrc()).getExternalOrgUnit(), ((JuridicalContactor) getResult().getDst()).getExternalOrgUnit()))
                || (Objects.equals(getResult().getSrc(), getResult().getDst())))
        {
            throw new ApplicationException("Получатель и плательщик не могут быть одним лицом или являться представителями одной стороны");
        }
    }
}
