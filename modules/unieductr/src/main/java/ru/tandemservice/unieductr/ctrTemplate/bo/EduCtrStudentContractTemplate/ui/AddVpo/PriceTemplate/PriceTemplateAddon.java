/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.ui.AddVpo.PriceTemplate;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.ListenerNamePrefixFactoryFastBean;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.shared.ctr.base.entity.ICtrPriceElement;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.bo.EduProgramPrice.EduProgramPriceManager;
import ru.tandemservice.unieductr.base.bo.EduProgramPrice.utils.EduPriceSelectionModel;
import ru.tandemservice.unieductr.base.entity.EduProgramPrice;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.EduCtrStudentContractTemplateManager;

import java.util.Date;

/**
 * @author azhebko
 * @since 11.12.2014
 */
public class PriceTemplateAddon extends UIAddon
{
    public static final String PRICE_TEMPLATE_ADDON = PriceTemplateAddon.class.getSimpleName();

    public PriceTemplateAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    private final ListenerNamePrefixFactoryFastBean listeners = new ListenerNamePrefixFactoryFastBean(this);
    public ListenerNamePrefixFactoryFastBean getListeners() { return listeners; }

    private Date _priceDate; // требуется устанавливать
    private Student student; //     значение вручную
    private EducationYear eduProgramYear;
    private EduProgramKind eduProgramKind;
    private EduProgramSubject eduProgramSubject;
    private EduProgramForm eduProgramForm;
    private EduProgramProf eduProgram;
    private EduProgramPrice eduProgramPrice;

    private final ISelectModel educationYearModel = new EducationYearModel();

    private final EduPriceSelectionModel priceSelection = new EduPriceSelectionModel()
    {
        @Override public Date getPriceDate() { return PriceTemplateAddon.this.getPriceDate(); }
        @Override public ICtrPriceElement getPriceElement() { return PriceTemplateAddon.this.getEduProgramPrice(); }
    };


    @Override
    public void onComponentRefresh()
    {
        if (getStudent() != null)
        {
            setEduProgramKind(getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getEduProgramKind());
            setEduProgramForm(getStudent().getEducationOrgUnit().getDevelopForm().getProgramForm());
            setEduProgramSubject(getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject());

            EducationYear eduYearSuggestion = IUniBaseDao.instance.get()
                    .getList(EducationYear.class, EducationYear.P_INT_VALUE).stream()
                    .filter(year -> year.getIntValue() == getStudent().getEntranceYear())
                    .findFirst().orElse(null);
            setEduProgramYear(eduYearSuggestion);
        }
        getPriceSelection().refreshCostList();
    }

    public void onChangeEduProgram()
    {
        setEduProgramPrice(getEduProgramPrice(getEduProgram()));
        getPriceSelection().refreshCostList();
    }

    public void onChangePrice() { getPriceSelection().refreshCostStageList(getPriceSelection().getSelectedCost()); }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EduCtrStudentContractTemplateManager.PARAM_PROGRAM, this.getEduProgram());
        dataSource.put(EduCtrStudentContractTemplateManager.PARAM_PROGRAM_YEAR, this.getEduProgramYear());
        dataSource.put(EduCtrStudentContractTemplateManager.PARAM_PROGRAM_FORM, this.getEduProgramForm());
        dataSource.put(EduCtrStudentContractTemplateManager.PARAM_PROGRAM_KIND, this.getEduProgramKind());
        dataSource.put(EduCtrStudentContractTemplateManager.PARAM_PROGRAM_SUBJECT, this.getEduProgramSubject());
    }

    public Date getPriceDate() { return _priceDate; }
    public void setPriceDate(Date priceDate) { _priceDate = priceDate; }

    public EducationYear getEduProgramYear() { return this.eduProgramYear; }
    public void setEduProgramYear(EducationYear eduProgramYear) { this.eduProgramYear = eduProgramYear; }

    public EduProgramKind getEduProgramKind() { return this.eduProgramKind; }
    public void setEduProgramKind(EduProgramKind eduProgramKind) { this.eduProgramKind = eduProgramKind; }

    public EduProgramSubject getEduProgramSubject() { return this.eduProgramSubject; }
    public void setEduProgramSubject(EduProgramSubject eduProgramSubject) { this.eduProgramSubject = eduProgramSubject; }

    public EduProgramForm getEduProgramForm() { return this.eduProgramForm; }
    public void setEduProgramForm(EduProgramForm eduProgramForm) { this.eduProgramForm = eduProgramForm; }

    public EduProgramProf getEduProgram() { return this.eduProgram; }
    public void setEduProgram(final EduProgramProf eduProgram) { setEduProgramPrice(getEduProgramPrice(this.eduProgram = eduProgram)); }

    protected EduProgramPrice getEduProgramPrice() { return this.eduProgramPrice; }
    protected void setEduProgramPrice(EduProgramPrice eduProgramPrice) { this.eduProgramPrice = eduProgramPrice; }

    public ISelectModel getEducationYearModel() { return this.educationYearModel; }

    public EduPriceSelectionModel getPriceSelection() { return priceSelection; }

    public String getCipher() { return null == getEduProgramPrice() ? null : getEduProgramPrice().getCipher(); }

    protected EduProgramPrice getEduProgramPrice(EduProgramProf eduProgram)
    {
        if (null == eduProgram)
            return null;
        return EduProgramPriceManager.instance().dao().doGetEduProgramPrice(eduProgram.getId());
    }

    public Student getStudent()
    {
        return student;
    }

    public void setStudent(Student student)
    {
        this.student = student;
    }
}