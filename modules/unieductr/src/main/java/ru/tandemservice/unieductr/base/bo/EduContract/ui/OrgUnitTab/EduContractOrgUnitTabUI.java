/* $Id$ */
package ru.tandemservice.unieductr.base.bo.EduContract.ui.OrgUnitTab;

import org.apache.commons.collections15.Predicate;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.ctr.base.bo.Contactor.ui.FilterBlock.ContactorFilterBlock;
import org.tandemframework.shared.ctr.base.bo.Contactor.ui.FilterBlock.ContactorFilterBlockUI;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduContractListDataSourceHandler;
import ru.tandemservice.unieductr.base.bo.EduContract.ui.GlobalList.EduContractGlobalList;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;

/**
 * @author nvankov
 * @since 9/15/14
 */
@State({
        @Bind(key=UIPresenter.PUBLISHER_ID, binding="orgUnitId")
})
public class EduContractOrgUnitTabUI extends UIPresenter
{
    private static final String CONTACTOR_FILTER_BLOCK_REGION = "contactorFilterBlockRegion";

    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private CommonPostfixPermissionModel _secModel;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    private BaseSearchListDataSource contractListDataSource;
    public BaseSearchListDataSource getContractListDataSource() { return this.contractListDataSource; }

    private UniEduProgramEducationOrgUnitAddon educationOrgUnitUtil;
    public UniEduProgramEducationOrgUnitAddon getEducationOrgUnitUtil() { return this.educationOrgUnitUtil; }

    @Override
    public void onComponentRefresh() {
        setOrgUnit(DataAccessServices.dao().<OrgUnit>get(_orgUnitId));
        setSecModel(new OrgUnitSecModel(getOrgUnit()));
        this.contractListDataSource = (BaseSearchListDataSource)this.getConfig().getDataSource(EduContractGlobalList.CONTRACT_LIST_DS);
        this.educationOrgUnitUtil = (UniEduProgramEducationOrgUnitAddon) this.getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);

        if (this.educationOrgUnitUtil != null)
        {
            this.educationOrgUnitUtil.configSettings(this.getSettingsKey());
            this.educationOrgUnitUtil.configUseFilters(
                    UniEduProgramEducationOrgUnitAddon.Filters.FORMATIVE_ORG_UNIT,
                    UniEduProgramEducationOrgUnitAddon.Filters.TERRITORIAL_ORG_UNIT,
                    UniEduProgramEducationOrgUnitAddon.Filters.PRODUCING_ORG_UNIT,
                    UniEduProgramEducationOrgUnitAddon.Filters.EDUCATION_LEVEL_HIGH_SCHOOL,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_CONDITION,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_FORM,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_PERIOD,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_TECH
            );
        }

        this.configWhereFilters();

        IUIPresenter filterBlock = _uiSupport.getChildUI(CONTACTOR_FILTER_BLOCK_REGION);
        if (filterBlock == null)
            _uiActivation.asRegion(ContactorFilterBlock.class, CONTACTOR_FILTER_BLOCK_REGION).activate();
        else
            filterBlock.onComponentRefresh();
    }

    private void configWhereFilters()
    {
        final UniEduProgramEducationOrgUnitAddon util = this.getEducationOrgUnitUtil();
        if (util != null)
        {
            util.clearConfigWhere();
            util.configWhere(EducationOrgUnit.formativeOrgUnit().id(), getOrgUnit().getId(), true);
            util.configWhere(EducationOrgUnit.territorialOrgUnit().id(), getOrgUnit().getId(), true);
        }
    }

    @Override
    public void onBeforeDataSourceFetch(final IUIDataSource dataSource) {
        if (EduContractGlobalList.CONTRACT_LIST_DS.equals(dataSource.getName())) {
            dataSource.put(EduContractListDataSourceHandler.PARAM_STUDENT_EDUOU_UTIL, this.getEducationOrgUnitUtil());
            dataSource.put(EduContractListDataSourceHandler.PARAM_STUDENT_OU, this.getOrgUnit().getId());
            dataSource.putAll(this.getSettings().getAsMap(
                    EduContractListDataSourceHandler.PARAM_CONTRACT_NUMBER,
                    EduContractListDataSourceHandler.PARAM_STUDENT_FIRST_NAME,
                    EduContractListDataSourceHandler.PARAM_STUDENT_LAST_NAME,
                    EduContractListDataSourceHandler.PARAM_STUDENT_MIDDLE_NAME,
                    EduContractListDataSourceHandler.PARAM_STUDENT_ACTIVE,
                    EduContractListDataSourceHandler.PARAM_DATE_FROM,
                    EduContractListDataSourceHandler.PARAM_DATE_TO
            ));
            dataSource.put(EduContractListDataSourceHandler.VIEW_REL_PREDICATE, (Predicate<IEduContractRelation>) relation -> relation.getPersonRole() instanceof Student);

            // добавляем значения универсального блока фильтров
            ContactorFilterBlockUI blockUI = _uiSupport.getChildUI(CONTACTOR_FILTER_BLOCK_REGION);
            dataSource.put(EduContractListDataSourceHandler.CONTACTOR_DQL_QUERY_OR_IDS, blockUI.getQueryOrIds());
        }

        dataSource.put(EduContractGlobalList.BIND_PAYMENT_GRID, this.getSettings().get("paymentGrid"));
        dataSource.put(EduContractGlobalList.BIND_PRICE_CATEGORY, this.getSettings().get("priceCategory"));
        dataSource.put(EduContractGlobalList.BIND_PRICE_SUB_CATEGORY, this.getSettings().get("priceSubCategory"));
    }

    public void onClickSearch()
    {
        this.getSettings().save();
        final UniEduProgramEducationOrgUnitAddon util = this.getEducationOrgUnitUtil();
        if (util != null) { util.saveSettings(); }
    }

    public void onClickClear()
    {
        this.getSettings().clear();
        final UniEduProgramEducationOrgUnitAddon util = this.getEducationOrgUnitUtil();
        if (util != null) { util.clearFilters(); }
        _uiSupport.getChildUI(CONTACTOR_FILTER_BLOCK_REGION).onComponentRefresh();
        this.onClickSearch();
    }
}

