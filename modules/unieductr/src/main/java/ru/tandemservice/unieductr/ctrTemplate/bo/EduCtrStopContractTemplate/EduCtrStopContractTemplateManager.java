/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStopContractTemplate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;
import ru.tandemservice.unieductr.base.entity.EduCtrStopVersionTemplateData;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStopContractTemplate.logic.EduCtrStopContractDao;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStopContractTemplate.logic.IEduCtrStopContractDao;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStopContractTemplate.ui.Edit.EduCtrStopContractTemplateEdit;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStopContractTemplate.ui.Pub.EduCtrStopContractTemplatePub;

/**
 * @author azhebko
 * @since 29.08.2014
 */
@Configuration
public class EduCtrStopContractTemplateManager extends BusinessObjectManager implements ICtrContractTemplateManager
{
    public static EduCtrStopContractTemplateManager instance()
    {
        return instance(EduCtrStopContractTemplateManager.class);
    }

    @Override
    public Class<? extends CtrContractVersionTemplateData> getDataEntityClass()
    {
        return EduCtrStopVersionTemplateData.class;
    }

    @Override
    public Class<? extends BusinessComponentManager> getDataDisplayComponent()
    {
        return EduCtrStopContractTemplatePub.class;
    }

    @Override
    public Class<? extends BusinessComponentManager> getDataEditComponent()
    {
        return EduCtrStopContractTemplateEdit.class;
    }

    @Bean
    @Override
    public IEduCtrStopContractDao dao()
    {
        return new EduCtrStopContractDao();
    }

    @Override
    public boolean isAllowEditInWizard()
    {
        return false;
    }
}