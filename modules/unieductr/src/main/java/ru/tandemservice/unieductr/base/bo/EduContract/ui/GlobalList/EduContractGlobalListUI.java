package ru.tandemservice.unieductr.base.bo.EduContract.ui.GlobalList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduContractListDataSourceHandler;

public class EduContractGlobalListUI extends UIPresenter
{
    private BaseSearchListDataSource contractListDataSource;
    public BaseSearchListDataSource getContractListDataSource() { return this.contractListDataSource; }

    @Override
    public void onComponentRefresh() {
        this.contractListDataSource = (BaseSearchListDataSource)this.getConfig().getDataSource(EduContractGlobalList.CONTRACT_LIST_DS);
    }

    @Override
    public void onBeforeDataSourceFetch(final IUIDataSource dataSource) {
        if (EduContractGlobalList.CONTRACT_LIST_DS.equals(dataSource.getName())) {
            dataSource.putAll(this.getSettings().getAsMap(
                    EduContractListDataSourceHandler.PARAM_CONTRACT_NUMBER,
                    EduContractListDataSourceHandler.PARAM_DATE_FROM,
                    EduContractListDataSourceHandler.PARAM_DATE_TO,
                    EduContractListDataSourceHandler.PARAM_STUDENT_LAST_NAME,
                    EduContractListDataSourceHandler.PARAM_STUDENT_FIRST_NAME,
                    EduContractListDataSourceHandler.PARAM_STUDENT_MIDDLE_NAME
            ));
        }

        dataSource.put(EduContractGlobalList.BIND_PAYMENT_GRID, this.getSettings().get("paymentGrid"));
        dataSource.put(EduContractGlobalList.BIND_PRICE_CATEGORY, this.getSettings().get("priceCategory"));
        dataSource.put(EduContractGlobalList.BIND_PRICE_SUB_CATEGORY, this.getSettings().get("priceSubCategory"));
    }
}