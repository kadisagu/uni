/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrProgramChangeAgreementTemplate.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.student.entity.EduCtrProgramChangeAgreementTemplateData;

import java.util.Collection;
import java.util.Date;

/**
 * @author azhebko
 * @since 09.12.2014
 */
@State(@Bind(key = IUIPresenter.PUBLISHER_ID, binding = "template.id", required = true))
public class EduCtrProgramChangeAgreementTemplatePubUI extends UIPresenter
{
    private EduCtrProgramChangeAgreementTemplateData _template = new EduCtrProgramChangeAgreementTemplateData();
    private EduProgramProf _eduProgramOld;
    private EduProgramProf _eduProgramNew;

    @Override
    public void onComponentRefresh()
    {
        _template = IUniBaseDao.instance.get().getNotNull(EduCtrProgramChangeAgreementTemplateData.class, _template.getId());
        CtrContractVersion version = getTemplate().getOwner();

        {
            Date timestamp = version.getActivationDate() == null ? null : version.getActivationDate();
            CtrContractVersion prevVersion = CtrContractVersionManager.instance().dao().getPreviousVersion(version);
            Collection<EduCtrEducationPromise> eduPromises = IUniBaseDao.instance.get().getList(EduCtrEducationPromise.class, EduCtrEducationPromise.src().owner(), prevVersion);
            if (eduPromises.size() != 1)
                throw new IllegalStateException();

            _eduProgramOld = eduPromises.iterator().next().getEduProgram();
        }
        {
            Collection<EduCtrEducationPromise> eduPromises = IUniBaseDao.instance.get().getList(EduCtrEducationPromise.class, EduCtrEducationPromise.src().owner(), version);
            if (eduPromises.size() != 1)
                throw new IllegalStateException();

            _eduProgramNew = eduPromises.iterator().next().getEduProgram();
        }
    }

    public EduCtrProgramChangeAgreementTemplateData getTemplate() { return _template; }

    public EduProgramProf getEduProgramOld() { return _eduProgramOld; }
    public EduProgramProf getEduProgramNew() { return _eduProgramNew; }
}