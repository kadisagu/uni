package ru.tandemservice.unieductr.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unieductr_2x10x7_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.7")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eduCtrObligationsContractReport

		// создано обязательное свойство reportDate
		{
			// создать колонку
			tool.createColumn("edu_ctr_obl_ctr_report_t", new DBColumn("reportdate_p", DBType.DATE));

			// задать значение по умолчанию
            //все созданные отчеты построены исходя из актуальности договоров на дату окончания периода.
			tool.executeUpdate("update edu_ctr_obl_ctr_report_t set reportdate_p=periodDateTo_p where reportdate_p is null");

			// сделать колонку NOT NULL
			tool.setColumnNullable("edu_ctr_obl_ctr_report_t", "reportdate_p", false);

		}


    }
}