/* $Id$ */
package ru.tandemservice.unieductr.base.ext.CtrPrice.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.tandemframework.shared.ctr.base.bo.CtrPrice.util.ICtrPriceElementCostStageFactory;
import org.tandemframework.shared.ctr.base.entity.ICtrPriceElement;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;

import ru.tandemservice.unieductr.base.entity.EduProgramPrice;

/**
 * @author Vasily Zhukov
 * @since 26.08.2011
 */
public class EduProgramElementCostStageFactoryYear implements ICtrPriceElementCostStageFactory
{
    @Override
    public List<CtrPriceElementCostStage> getStageList(ICtrPriceElement priceElement)
    {
        final List<Integer> stageCodeSource;
        if (priceElement instanceof EduProgramPrice) {
            stageCodeSource = getStageCodeSource((EduProgramPrice) priceElement);
        } else {
            return new ArrayList<>(0);
        }

        List<CtrPriceElementCostStage> stagesList = new ArrayList<>();
        for (Integer stageCodeEntry : stageCodeSource)
        {
            CtrPriceElementCostStage stage = new CtrPriceElementCostStage();
            stage.setTitle("За " + stageCodeEntry + " курс");
            stage.setStageUniqueCode(String.valueOf(stageCodeEntry));
            stagesList.add(stage);
        }
        return stagesList;
    }

    @Override
    public List<CtrPriceElementCostStage> filterStageList(ICtrPriceElement priceElement, List<CtrPriceElementCostStage> stageList, Map<String, Object> filter)
    {
        return stageList;
    }

    // course.number
    public static List<Integer> getStageCodeSource(EduProgramPrice priceElement)
    {
        // делаем список курсов
        int courses = priceElement.getProgram().getDuration().getNumberOfYears();
        if(priceElement.getProgram().getDuration().getNumberOfMonths() > 0) courses++;
        List<Integer> list = new ArrayList<>(courses);
        for(int i = 0; i < courses; i++) {
            list.add(i+1);
        }

        return list;
    }

}