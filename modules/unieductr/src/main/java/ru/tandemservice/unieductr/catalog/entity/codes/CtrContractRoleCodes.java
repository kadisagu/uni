package ru.tandemservice.unieductr.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Роль контрагента в типе договора"
 * Имя сущности : ctrContractRole
 * Файл data.xml : unieductr.data.xml
 */
public interface CtrContractRoleCodes
{
    /** Константа кода (code) элемента : Заказчик (title) */
    String EDU_CONTRACT_CUSTOMER = "01.01.customer";
    /** Константа кода (code) элемента : Исполнитель (title) */
    String EDU_CONTRACT_PROVIDER = "01.01.provider";

    Set<String> CODES = ImmutableSet.of(EDU_CONTRACT_CUSTOMER, EDU_CONTRACT_PROVIDER);
}
