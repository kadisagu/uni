package ru.tandemservice.unieductr.reports.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Список должников по оплате
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduCtrDebitorsPayReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport";
    public static final String ENTITY_NAME = "eduCtrDebitorsPayReport";
    public static final int VERSION_HASH = -900809235;
    private static IEntityMeta ENTITY_META;

    public static final String P_REPORT_DATE = "reportDate";
    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String P_EDUCATION_LEVEL_HIGH_SCHOOL = "educationLevelHighSchool";
    public static final String P_DEVELOP_FORM = "developForm";
    public static final String P_DEVELOP_CONDITION = "developCondition";
    public static final String P_DEVELOP_TECH = "developTech";
    public static final String P_DEVELOP_PERIOD = "developPeriod";
    public static final String P_COURSE = "course";
    public static final String P_GROUP = "group";
    public static final String L_ORG_UNIT = "orgUnit";

    private Date _reportDate;     // Дата отчета
    private String _formativeOrgUnit;     // Форм. подразделение
    private String _territorialOrgUnit;     // Терр. подразделение
    private String _educationLevelHighSchool;     // Направление подготовки (специальность)
    private String _developForm;     // Форма освоения
    private String _developCondition;     // Условие освоения
    private String _developTech;     // Технология освоения
    private String _developPeriod;     // Срок освоения
    private String _course;     // Курс
    private String _group;     // Группа
    private OrgUnit _orgUnit;     // Подразделение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата отчета. Свойство не может быть null.
     */
    @NotNull
    public Date getReportDate()
    {
        return _reportDate;
    }

    /**
     * @param reportDate Дата отчета. Свойство не может быть null.
     */
    public void setReportDate(Date reportDate)
    {
        dirty(_reportDate, reportDate);
        _reportDate = reportDate;
    }

    /**
     * @return Форм. подразделение.
     */
    @Length(max=255)
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Форм. подразделение.
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Терр. подразделение.
     */
    @Length(max=255)
    public String getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Терр. подразделение.
     */
    public void setTerritorialOrgUnit(String territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    /**
     * @return Направление подготовки (специальность).
     */
    @Length(max=255)
    public String getEducationLevelHighSchool()
    {
        return _educationLevelHighSchool;
    }

    /**
     * @param educationLevelHighSchool Направление подготовки (специальность).
     */
    public void setEducationLevelHighSchool(String educationLevelHighSchool)
    {
        dirty(_educationLevelHighSchool, educationLevelHighSchool);
        _educationLevelHighSchool = educationLevelHighSchool;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(String developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения.
     */
    public void setDevelopCondition(String developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения.
     */
    public void setDevelopTech(String developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Срок освоения.
     */
    @Length(max=255)
    public String getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Срок освоения.
     */
    public void setDevelopPeriod(String developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    /**
     * @return Курс.
     */
    @Length(max=255)
    public String getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс.
     */
    public void setCourse(String course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа.
     */
    @Length(max=255)
    public String getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа.
     */
    public void setGroup(String group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduCtrDebitorsPayReportGen)
        {
            setReportDate(((EduCtrDebitorsPayReport)another).getReportDate());
            setFormativeOrgUnit(((EduCtrDebitorsPayReport)another).getFormativeOrgUnit());
            setTerritorialOrgUnit(((EduCtrDebitorsPayReport)another).getTerritorialOrgUnit());
            setEducationLevelHighSchool(((EduCtrDebitorsPayReport)another).getEducationLevelHighSchool());
            setDevelopForm(((EduCtrDebitorsPayReport)another).getDevelopForm());
            setDevelopCondition(((EduCtrDebitorsPayReport)another).getDevelopCondition());
            setDevelopTech(((EduCtrDebitorsPayReport)another).getDevelopTech());
            setDevelopPeriod(((EduCtrDebitorsPayReport)another).getDevelopPeriod());
            setCourse(((EduCtrDebitorsPayReport)another).getCourse());
            setGroup(((EduCtrDebitorsPayReport)another).getGroup());
            setOrgUnit(((EduCtrDebitorsPayReport)another).getOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduCtrDebitorsPayReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduCtrDebitorsPayReport.class;
        }

        public T newInstance()
        {
            return (T) new EduCtrDebitorsPayReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "reportDate":
                    return obj.getReportDate();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
                case "educationLevelHighSchool":
                    return obj.getEducationLevelHighSchool();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developTech":
                    return obj.getDevelopTech();
                case "developPeriod":
                    return obj.getDevelopPeriod();
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "orgUnit":
                    return obj.getOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "reportDate":
                    obj.setReportDate((Date) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((String) value);
                    return;
                case "educationLevelHighSchool":
                    obj.setEducationLevelHighSchool((String) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((String) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((String) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((String) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((String) value);
                    return;
                case "course":
                    obj.setCourse((String) value);
                    return;
                case "group":
                    obj.setGroup((String) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reportDate":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "territorialOrgUnit":
                        return true;
                case "educationLevelHighSchool":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "developTech":
                        return true;
                case "developPeriod":
                        return true;
                case "course":
                        return true;
                case "group":
                        return true;
                case "orgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reportDate":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "territorialOrgUnit":
                    return true;
                case "educationLevelHighSchool":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "developTech":
                    return true;
                case "developPeriod":
                    return true;
                case "course":
                    return true;
                case "group":
                    return true;
                case "orgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "reportDate":
                    return Date.class;
                case "formativeOrgUnit":
                    return String.class;
                case "territorialOrgUnit":
                    return String.class;
                case "educationLevelHighSchool":
                    return String.class;
                case "developForm":
                    return String.class;
                case "developCondition":
                    return String.class;
                case "developTech":
                    return String.class;
                case "developPeriod":
                    return String.class;
                case "course":
                    return String.class;
                case "group":
                    return String.class;
                case "orgUnit":
                    return OrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduCtrDebitorsPayReport> _dslPath = new Path<EduCtrDebitorsPayReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduCtrDebitorsPayReport");
    }
            

    /**
     * @return Дата отчета. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getReportDate()
     */
    public static PropertyPath<Date> reportDate()
    {
        return _dslPath.reportDate();
    }

    /**
     * @return Форм. подразделение.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Терр. подразделение.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getTerritorialOrgUnit()
     */
    public static PropertyPath<String> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    /**
     * @return Направление подготовки (специальность).
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getEducationLevelHighSchool()
     */
    public static PropertyPath<String> educationLevelHighSchool()
    {
        return _dslPath.educationLevelHighSchool();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getDevelopForm()
     */
    public static PropertyPath<String> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getDevelopCondition()
     */
    public static PropertyPath<String> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getDevelopTech()
     */
    public static PropertyPath<String> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getDevelopPeriod()
     */
    public static PropertyPath<String> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getCourse()
     */
    public static PropertyPath<String> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getGroup()
     */
    public static PropertyPath<String> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    public static class Path<E extends EduCtrDebitorsPayReport> extends StorableReport.Path<E>
    {
        private PropertyPath<Date> _reportDate;
        private PropertyPath<String> _formativeOrgUnit;
        private PropertyPath<String> _territorialOrgUnit;
        private PropertyPath<String> _educationLevelHighSchool;
        private PropertyPath<String> _developForm;
        private PropertyPath<String> _developCondition;
        private PropertyPath<String> _developTech;
        private PropertyPath<String> _developPeriod;
        private PropertyPath<String> _course;
        private PropertyPath<String> _group;
        private OrgUnit.Path<OrgUnit> _orgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата отчета. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getReportDate()
     */
        public PropertyPath<Date> reportDate()
        {
            if(_reportDate == null )
                _reportDate = new PropertyPath<Date>(EduCtrDebitorsPayReportGen.P_REPORT_DATE, this);
            return _reportDate;
        }

    /**
     * @return Форм. подразделение.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(EduCtrDebitorsPayReportGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Терр. подразделение.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getTerritorialOrgUnit()
     */
        public PropertyPath<String> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new PropertyPath<String>(EduCtrDebitorsPayReportGen.P_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

    /**
     * @return Направление подготовки (специальность).
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getEducationLevelHighSchool()
     */
        public PropertyPath<String> educationLevelHighSchool()
        {
            if(_educationLevelHighSchool == null )
                _educationLevelHighSchool = new PropertyPath<String>(EduCtrDebitorsPayReportGen.P_EDUCATION_LEVEL_HIGH_SCHOOL, this);
            return _educationLevelHighSchool;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getDevelopForm()
     */
        public PropertyPath<String> developForm()
        {
            if(_developForm == null )
                _developForm = new PropertyPath<String>(EduCtrDebitorsPayReportGen.P_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getDevelopCondition()
     */
        public PropertyPath<String> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new PropertyPath<String>(EduCtrDebitorsPayReportGen.P_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getDevelopTech()
     */
        public PropertyPath<String> developTech()
        {
            if(_developTech == null )
                _developTech = new PropertyPath<String>(EduCtrDebitorsPayReportGen.P_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getDevelopPeriod()
     */
        public PropertyPath<String> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new PropertyPath<String>(EduCtrDebitorsPayReportGen.P_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getCourse()
     */
        public PropertyPath<String> course()
        {
            if(_course == null )
                _course = new PropertyPath<String>(EduCtrDebitorsPayReportGen.P_COURSE, this);
            return _course;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getGroup()
     */
        public PropertyPath<String> group()
        {
            if(_group == null )
                _group = new PropertyPath<String>(EduCtrDebitorsPayReportGen.P_GROUP, this);
            return _group;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

        public Class getEntityClass()
        {
            return EduCtrDebitorsPayReport.class;
        }

        public String getEntityName()
        {
            return "eduCtrDebitorsPayReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
