/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrAgreementIncomeReport.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.unieductr.reports.entity.EduCtrAgreementIncomeReport;

/**
 * @author Alexey Lopatin
 * @since 21.04.2015
 */
public interface IEduCtrAgreementIncomeReportDao extends INeedPersistenceSupport
{
    /**
     * Создает документ отчета.
     * @return RtfDocument
     */
    <M extends Model> RtfDocument createReportRtfDocument(M model);

    /**
     * Сохраняет отчет и его печатную форму.
     */
    <M extends Model> EduCtrAgreementIncomeReport saveReport(M model, RtfDocument document);
}
