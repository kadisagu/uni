/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStopContractTemplate.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractKindSelectWrapper;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContractResultDao;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrVersionTemplatePromiceRestrictions;
import org.tandemframework.shared.ctr.base.bo.CtrPaymentPromice.logic.CtrPaymentPromiceResultPair;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contract.*;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractKind;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractRole;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.base.entity.EduCtrStopVersionTemplateData;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;
import ru.tandemservice.unieductr.base.entity.IEducationPromise;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractKindCodes;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractTypeCodes;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStopContractTemplate.ui.Add.EduCtrStopContractTemplateAdd;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;

import java.util.*;

/**
 * @author azhebko
 * @since 02.09.2014
 */
public class EduCtrStopContractDao extends UniBaseDao implements IEduCtrStopContractDao
{
    @Override
    public void doCreateCtrStopContractVersion(EduCtrStopVersionTemplateData newVersionTemplateData, CtrContractObject contract, CtrContractVersionCreateData versionCreateData, Date refundDate, Collection<Long> promiseIds)
    {
        CtrContractVersion prevVersion = CtrContractVersionManager.instance().dao().getLastActiveVersion(contract);

        final CtrContractVersion newVersion = new CtrContractVersion();
        {
            newVersion.setContract(contract);
            newVersion.setKind(versionCreateData.getContractKind());
            newVersion.setPrintTemplate(versionCreateData.getPrintTemplate());
            newVersion.setCreationDate(new Date());
            newVersion.setDocStartDate(versionCreateData.getDocStartDate());
            newVersion.setContractRegistrationDate(versionCreateData.getDocStartDate());
            newVersion.setCreator(UserContext.getInstance().getPrincipalContext());
            newVersion.setDurationBeginDate(versionCreateData.getDurationBeginDate());
            newVersion.setNumber(prevVersion.getNumber());

            this.save(newVersion);
        }

        newVersionTemplateData.setOwner(newVersion);
        this.save(newVersionTemplateData);

        // prev version contractors
        CtrContractVersionContractor customer = CtrContractVersionManager.instance().dao().getContactor(prevVersion, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER);
        CtrContractVersionContractor provider = CtrContractVersionManager.instance().dao().getContactor(prevVersion, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER);

        Map<String, ICtrContractResultDao.ICtrContractStatus> statusMap = CtrContractVersionManager.instance().results().getContractVersionStatusMap(Collections.singleton(prevVersion.getId()), null).get(prevVersion.getId());
        if (statusMap == null)
            return;

        String groupKey = CtrPaymentPromiceResultPair.key(customer == null ? null : customer.getContactor().getId(), provider == null ? null : provider.getContactor().getId(), newVersionTemplateData.getRefundCurrency().getCode());
        ICtrContractResultDao.ICtrContractStatus contractStatus = statusMap.get(groupKey);
        if (contractStatus == null)
            return;

        if (customer != null)
        {
            // заказчик
            customer = new CtrContractVersionContractor(newVersion, customer.getContactor());
            this.save(customer);
            this.save(new CtrContractVersionContractorRole(customer, this.getCatalogItem(CtrContractRole.class, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER)));
        }

        if (provider != null)
        {
            // исполнитель
            provider = new CtrContractVersionContractor(newVersion, provider.getContactor());
            this.save(provider);
            this.save(new CtrContractVersionContractorRole(provider, this.getCatalogItem(CtrContractRole.class, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER)));
        }

        // Копируем выбранные обязательства
        if (CollectionUtils.isNotEmpty(promiseIds))
        {
            for (CtrPaymentPromice sourcePromise: this.getList(CtrPaymentPromice.class, promiseIds))
            {
                CtrPaymentPromice newPromise = new CtrPaymentPromice();
                newPromise.update(sourcePromise);
                if (sourcePromise.getSrc().getContactor() instanceof EmployeePostContactor)
                {
                    newPromise.setSrc(provider);
                    newPromise.setDst(customer);
                } else {
                    newPromise.setSrc(customer);
                    newPromise.setDst(provider);
                }
                this.save(newPromise);
            }
        }

        // возврат
        if (newVersionTemplateData.getRefund() > 0)
        {
            CtrPaymentPromice refundPromise = new CtrPaymentPromice();

            refundPromise.setSrc(provider);
            refundPromise.setDst(customer);
            refundPromise.setCurrency(newVersionTemplateData.getRefundCurrency());
            refundPromise.setCostAsLong(newVersionTemplateData.getRefundAsLong());
            refundPromise.setDeadlineDate(refundDate);
            refundPromise.setStage("платеж от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(refundDate));

            this.save(refundPromise);
        }

        Map<ContactorPerson, CtrContractVersionContractor> contractorMap = SafeMap.get(key -> {
            CtrContractVersionContractor result = new CtrContractVersionContractor(newVersion, key);
            EduCtrStopContractDao.this.save(result);
            return result;
        });

        if (customer != null) contractorMap.put(customer.getContactor(), customer);
        if (provider != null) contractorMap.put(provider.getContactor(), provider);

        // копируем обязательства на обучение из действующей версии
        for (CtrContractPromice promise: this.<CtrContractPromice>getList(CtrContractPromice.class, CtrContractPromice.src().owner().s(), prevVersion))
        {
            if (promise instanceof IEducationPromise)
            {
                CtrContractPromice newPromise = promise.clone();
                newPromise.setSrc(contractorMap.get(promise.getSrc().getContactor()));
                newPromise.setDst(contractorMap.get(promise.getDst().getContactor()));
                this.save(newPromise);
            }
        }
    }

    @Override
    public Map<CtrContractType, List<CtrContractKindSelectWrapper>> getVersionAddComponent(Long contextEntityId)
    {
        CtrContractVersion currentVersion = null;
        IEntity context = this.getNotNull(contextEntityId);

        if (context instanceof EduCtrStudentContract)
            currentVersion = CtrContractVersionManager.instance().dao().getLastActiveVersion(((IEduContractRelation) context).getContractObject());

        else if (context instanceof CtrContractVersion)
            currentVersion = (CtrContractVersion) context;

        if (currentVersion == null || this.getCount(EduCtrEducationPromise.class, EduCtrEducationPromise.src().owner().s(), currentVersion) != 1 || this.getCount(EduCtrStudentContract.class, EduCtrStudentContract.contractObject().s(), currentVersion.getContract()) != 1)
            return Collections.emptyMap();

        if(CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE_V_O.equals(currentVersion.getContract().getType().getCode()))
        {
            Map<CtrContractType, List<CtrContractKindSelectWrapper>> map = Maps.newHashMap();
            map.put(this.getCatalogItem(CtrContractType.class, CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE_V_O), Lists.newArrayList(new CtrContractKindSelectWrapper(this.getCatalogItem(CtrContractKind.class, CtrContractKindCodes.EDU_CTR_STOP_CONTRACT), EduCtrStopContractTemplateAdd.class)));

            return map;
        }
        else
        {
            return Collections.emptyMap();
        }
    }

    @Override
    public ICtrVersionTemplatePromiceRestrictions getPromiceRestrictions(Class<? extends CtrContractPromice> promiceClass, CtrContractVersionTemplateData templateData)
    {
        return ICtrVersionTemplatePromiceRestrictions.NO_RESTRICTIONS;
    }

    @Override
    public IDocumentRenderer print(CtrContractVersionTemplateData templateData)
    {
        // чужие версии не печатаем
        if (!(templateData instanceof EduCtrStopVersionTemplateData))
            throw new IllegalStateException();

        // печатаем по шаблону, используя скрипт
        Map<String, Object> result = CommonManager.instance().scriptDao().getScriptResult(
            templateData.getOwner().getPrintTemplate(),
            IScriptExecutor.OBJECT_VARIABLE, templateData.getId());

        return (IDocumentRenderer)result.get(IScriptExecutor.RENDERER);
    }

    @Override
    public boolean isAllowCreateNextVersionByTemplate(CtrContractVersionTemplateData ctrContractVersionTemplateData) { return false; }

    @Override
    public boolean isAllowCreateNextVersionByCopy(CtrContractVersionTemplateData ctrContractVersionTemplateData) { return false; }

    @Override
    public boolean isAllowDeleteTemplate(CtrContractVersionTemplateData ctrContractVersionTemplateData) { return false; }
}
