package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentAddContractTemplate.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentAddContractTemplate.EduCtrStudentAddContractTemplateManager;

@Configuration
public class EduCtrStudentAddContractTemplateAdd extends BusinessComponentManager
{
    public static final String DS_FORMATIVE_ORG_UNIT = "formativeOrgUnitDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return EduCtrStudentAddContractTemplateManager.programAdditionalPresenterExtPointBuilder(this.presenterExtPointBuilder(), this.getName())
            .addDataSource(EduContractManager.instance().customerDSConfig())
            .addDataSource(formativeOrgUnitDSConfig())
            .create();
    }

    @Bean
    public SelectDSConfig formativeOrgUnitDSConfig() {
        return selectDS(DS_FORMATIVE_ORG_UNIT)
                .addColumn(OrgUnit.fullTitle().s())
                .handler(UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler())
                .create();
    }
}
