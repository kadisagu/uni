package ru.tandemservice.unieductr.base.entity;

import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.entity.gen.EduCtrEducationPromiseGen;

/**
 * Обязательство обучения по обр. программе ВО
 *
 * Обязательство обучить человека (dst - обучаемый)
 */
public class EduCtrEducationPromise extends EduCtrEducationPromiseGen implements IEducationPromise<EduProgramProf>
{
    @Override public EduProgramProf getEducationPromiseTarget() { return getEduProgram(); }
    @Override public String getEducationPromiseDescription() { return getEduProgram().getFullTitle(); }

}