/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrObligationsContractReport.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Ekaterina Zvereva
 * @since 18.08.2016
 */
@Configuration
public class EduCtrObligationsContractReportPub extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}