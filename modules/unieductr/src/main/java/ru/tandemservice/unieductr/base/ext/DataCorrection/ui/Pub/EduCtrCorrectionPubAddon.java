/**
 *$Id: OrgSystemActionPubAddon.java 4443 2014-05-21 08:10:28Z nvankov $
 */
package ru.tandemservice.unieductr.base.ext.DataCorrection.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unieductr.base.bo.EduContract.ui.ChangeEduProgramDataCorrection.EduContractChangeEduProgramDataCorrection;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
public class EduCtrCorrectionPubAddon extends UIAddon
{
    public EduCtrCorrectionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickEduCtrChangeEduProgramm()
    {
        getActivationBuilder().asCurrent(EduContractChangeEduProgramDataCorrection.class).activate();
    }

}
