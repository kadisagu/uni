/**
 *$Id:$
 */
package ru.tandemservice.unieductr.reports.bo.EduCtrDebitorsPayReport.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unieductr.reports.bo.EduCtrDebitorsPayReport.EduCtrDebitorsPayReportManager;
import ru.tandemservice.unieductr.reports.bo.EduCtrDebitorsPayReport.logic.Model;
import ru.tandemservice.unieductr.reports.bo.EduCtrDebitorsPayReport.ui.Pub.EduCtrDebitorsPayReportPub;
import ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport;

/**
 * @author Alexander Shaburov
 * @since 30.10.12
 */
@Input({
    @Bind(key = "orgUnitId", binding = "orgUnitId")
})
public class EduCtrDebitorsPayReportAddUI extends UIPresenter
{
    public static final String PROP_ORGUNIT_ID = "orgUnitId";
    public static final String PROP_EDU_ORGUNIT_LIST = "educationOrgUnitList";

    private Long _orgUnitId;

    private boolean _activeCourse;
    private boolean _activeGroup;

    private Model _model;

    public void onClickApply()
    {
        UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);

        _model.setOrgUnitId(_orgUnitId);
        _model.setUtil(util);
        _model.setActiveCourse(isActiveCourse());
        _model.setActiveGroup(isActiveGroup());

        RtfDocument document = EduCtrDebitorsPayReportManager.instance().dao().createReportRtfDocument(_model);

        EduCtrDebitorsPayReport report = EduCtrDebitorsPayReportManager.instance().dao().saveReport(_model, document);

        deactivate();
        getActivationBuilder()
        .asDesktopRoot(EduCtrDebitorsPayReportPub.class)
        .parameter(UIPresenter.PUBLISHER_ID, report.getId())
        .activate();
    }

    @Override
    public void onComponentRefresh()
    {
        if (null == _model) {
            _model = new Model();
        }

        UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);

        if (_orgUnitId != null) {
            util.configWhere(EducationOrgUnit.formativeOrgUnit().id(), _orgUnitId, false);
        }

        util
        .configNeedEnableCheckBox(true)
        .configUseFilters(
            UniEduProgramEducationOrgUnitAddon.Filters.FORMATIVE_ORG_UNIT,
            UniEduProgramEducationOrgUnitAddon.Filters.TERRITORIAL_ORG_UNIT,
            UniEduProgramEducationOrgUnitAddon.Filters.EDUCATION_LEVEL_HIGH_SCHOOL,
            UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_FORM,
            UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_CONDITION,
            UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_TECH,
            UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_PERIOD);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(EduCtrDebitorsPayReportAdd.GROUP_DS))
        {
            UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);

            dataSource.put(PROP_ORGUNIT_ID, _orgUnitId);
            dataSource.put(PROP_EDU_ORGUNIT_LIST, util.getEducationOrgUnitFilteredList());
        }
    }

    public String getPermissionKey()
    {
        return _orgUnitId == null ? "eduCtrDebitorsPayReport" : new OrgUnitSecModel(DataAccessServices.dao().get(OrgUnit.class, _orgUnitId)).getPermission("orgUnit_viewEduCtrDebitorsPayReport");
    }

    @Override
    public ISecured getSecuredObject()
    {
        return _orgUnitId != null ? DataAccessServices.dao().get(OrgUnit.class, _orgUnitId) : super.getSecuredObject();
    }

    // Getters & Setters

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public Model getModel()
    {
        return _model;
    }

    public void setModel(Model model)
    {
        _model = model;
    }

    public boolean isActiveCourse()
    {
        return _activeCourse;
    }

    public void setActiveCourse(boolean activeCourse)
    {
        _activeCourse = activeCourse;
    }

    public boolean isActiveGroup()
    {
        return _activeGroup;
    }

    public void setActiveGroup(boolean activeGroup)
    {
        _activeGroup = activeGroup;
    }
}
