/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrProgramChangeAgreementTemplate.ui.Add;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.StaticFullCheckSelectModel;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.AddByTemplate.CtrContractVersionAddByTemplateUI;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractKind;
import org.tandemframework.shared.ctr.catalog.entity.CtrPrintTemplate;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.base.bo.EduProgramPrice.utils.EduPriceSelectionModel;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrProgramChangeAgreementTemplate.EduCtrProgramChangeAgreementTemplateManager;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.EduCtrStudentContractTemplateManager;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.ui.AddVpo.PriceTemplate.PriceTemplateAddon;
import ru.tandemservice.unieductr.student.entity.EduCtrProgramChangeAgreementTemplateData;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;

import java.util.*;

/**
 * @author azhebko
 * @since 09.12.2014
 */
@Input(
        {
                @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTEXT_ID, binding = "prevVersion.id", required = true),
                @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_KIND_ID, binding="versionCreateData.contractKindHolder.id", required=true),
                @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_PRINT_TEMPLATE_ID, binding="versionCreateData.printTemplateHolder.id")}
)
public class EduCtrProgramChangeAgreementTemplateAddUI extends UIPresenter
{
    private CtrContractVersion _prevVersion = new CtrContractVersion();
    private EduCtrProgramChangeAgreementTemplateData _template = new EduCtrProgramChangeAgreementTemplateData();

    private final CtrContractVersionCreateData versionCreateData = new CtrContractVersionCreateData();

    public CtrContractVersionCreateData getVersionCreateData()
    {
        return versionCreateData;
    }

    private OrgUnit formativeOrgUnit;

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        this.formativeOrgUnit = formativeOrgUnit;
    }

    public OrgUnit getFormativeOrgUnit() { return formativeOrgUnit; }

    private final ISelectModel providerModel = new StaticFullCheckSelectModel(ContactorPerson.fullTitle().s()) {
        @Override protected List list() {
            final OrgUnit formativeOrgUnit = EduCtrProgramChangeAgreementTemplateAddUI.this.getFormativeOrgUnit();
            if (null == formativeOrgUnit) { return Collections.emptyList(); }

            final Collection<ContactorPerson> presenters = EduContractManager.instance().dao().getAcademyPresenters(formativeOrgUnit);
            return new ArrayList<>(presenters);
        }
    };
    public ISelectModel getProviderModel() { return this.providerModel; }

    private ContactorPerson provider;
    public ContactorPerson getProvider() { return this.provider; }
    public void setProvider(final ContactorPerson provider) { this.provider = provider; }

    @Override
    public void onComponentActivate() {
        _prevVersion = IUniBaseDao.instance.get().getNotNull(CtrContractVersion.class, _prevVersion.getId());
        EduCtrStudentContract contract = DataAccessServices.dao().getNotNull(EduCtrStudentContract.class, EduCtrStudentContract.contractObject(), getPrevVersion().getContract());
        setFormativeOrgUnit(contract.getStudent().getEducationOrgUnit().getFormativeOrgUnit());
        this.setProvider(CtrContractVersionManager.instance().dao().getContactor(getPrevVersion(), CtrContractRoleCodes.EDU_CONTRACT_PROVIDER).getContactor());
    }


    @Override
    public void onComponentRefresh()
    {
        getVersionCreateData().doRefresh();
        getTemplate().setEducationYear(EducationYearManager.instance().dao().getCurrent());
        getVersionCreateData().setDocStartDate(new Date());
        getVersionCreateData().setDurationBeginDate(new Date());
        getPriceTemplateAddon().setPriceDate(getVersionCreateData().getDocStartDate());

        Collection<EduCtrEducationPromise> eduPromises = IUniBaseDao.instance.get().getList(EduCtrEducationPromise.class, EduCtrEducationPromise.src().owner(), getPrevVersion());
        if (eduPromises.size() != 1)
            throw new IllegalStateException();

        EduProgramProf eduProgram = eduPromises.iterator().next().getEduProgram();

        getPriceTemplateAddon().setEduProgramKind(eduProgram.getKind());
        getPriceTemplateAddon().setEduProgramSubject(eduProgram.getProgramSubject());
        getPriceTemplateAddon().setEduProgramYear(eduProgram.getYear());
        getPriceTemplateAddon().setEduProgramForm(eduProgram.getForm());
        getPriceTemplateAddon().setEduProgram(eduProgram);
    }

    public void onChangePriceDate()
    {
        getPriceTemplateAddon().setPriceDate(getVersionCreateData().getDocStartDate());
        getPriceTemplateAddon().getPriceSelection().refreshCostList();
    }

    public void onClickApply()
    {
        final Date durationBeginDate = versionCreateData.getDurationBeginDate();
        if(durationBeginDate != null && durationBeginDate.before(_prevVersion.getDurationBeginDate()))
            _uiSupport.error("Дату начала действия нельзя установить раньше даты начала действия предыдущей версии", "durationBeginDate");
        if(getUserContext().getErrorCollector().hasErrors())
            return;
        getTemplate().setCipher(getPriceTemplateAddon().getCipher());
        EduPriceSelectionModel priceSelection = getPriceTemplateAddon().getPriceSelection();
        EduCtrProgramChangeAgreementTemplateManager.instance().dao().doCreateNewVersion(getTemplate(), getPrevVersion(), this.getPriceTemplateAddon().getEduProgram(), priceSelection.getSelectedCost(), priceSelection.getSelectedCostStages(), getVersionCreateData());
        EduCtrStudentContractTemplateManager.instance().dao().doUpdateContractVersionContactor(getTemplate().getOwner(), getProvider(), null);
        this.deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, getTemplate().getOwner().getId()).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.TRUE));
    }

    public void onClickClose()
    {
        this.deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, null).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.TRUE));
    }

    public void onClickBack()
    {
        this.deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, null).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.FALSE));
    }

    public CtrContractVersion getPrevVersion() { return _prevVersion; }
    public EduCtrProgramChangeAgreementTemplateData getTemplate() { return _template; }

    public PriceTemplateAddon getPriceTemplateAddon()
    {
        return ((PriceTemplateAddon) this.getConfig().getAddon(PriceTemplateAddon.PRICE_TEMPLATE_ADDON));
    }
}