/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unieductr.component.settings.EduCtrAcademyPresenter;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.IRawFormatter;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author iolshvang
 * @since 06.06.11 16:30
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @SuppressWarnings("deprecation")
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.setSettings(UniBaseUtils.getDataSettings(component, "orgstructureTree."));
        model.setFiltersBlockVisible(CoreServices.securityService().check(SecurityRuntime.getInstance().getCommonSecurityObject(), component.getUserContext().getPrincipalContext(), "orgstructTree_filtering"));

        this.getDao().prepare(model);
        this.prepareListDataSource(component);
    }

    @SuppressWarnings("deprecation")
    private void prepareListDataSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        if (model.getDataSource() != null) {
            return;
        }

        final DynamicListDataSource<OrgUnit> dataSource = new DynamicListDataSource<>(component, this);
        final PublisherLinkColumn column = new PublisherLinkColumn("Подразделение", OrgUnit.P_TYPE_TITLE);
        column.setTreeColumn(true);
        column.setFormatter((IRawFormatter) source -> {
            final String[] type2title = ((String)source).split(":");
            return "<b>" + type2title[0] + "</b>" + ":" + type2title[1];
        });
        column.setStyleResolver(rowEntity -> {
            if (Boolean.TRUE.equals(rowEntity.getProperty(OrgUnit.P_ARCHIVAL))) {
                return "background-color: " + UniDefines.COLOR_ERROR + ";";
            } else {
                return null;
            }
        });
        column.setOrderable(false);
        dataSource.addColumn(column);
        dataSource.addColumn(new SimpleColumn("Представители", "presenterColumn").setFormatter(NewLineFormatter.NOBR_IN_LINES).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditEduCtrAcademyPresenter"));
        model.setDataSource(dataSource);
    }

    public void onClickEditEduCtrAcademyPresenter(final IBusinessComponent component)
    {
        this.activate(component, new ComponentActivator(ru.tandemservice.unieductr.component.settings.EduCtrAcademyPresenterEdit.Model.COMPONENT_NAME, new ParametersMap().add("orgUnitId", component.getListenerParameter())));
    }

    @Override
    public void updateListDataSource(final IBusinessComponent component)
    {
        this.getDao().prepareListDataSource(this.getModel(component));
    }

    public void onClickSearch(final IBusinessComponent context)
    {
        final Model model = this.getModel(context);
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent context)
    {
        this.getModel(context).getSettings().clear();
        this.onClickSearch(context);
    }
}