package ru.tandemservice.unieductr.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unieductr_2x10x7_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.7")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность eduCtrStudentAddContractTemplateData

        // создана новая сущность
        {
            if(!tool.tableExists("eductr_stduent_dpo_ctmpldt_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("eductr_stduent_dpo_ctmpldt_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_e2fe7867")
                );
                tool.createTable(dbt);
            }

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("eduCtrStudentAddContractTemplateData");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eduCtrEducationAddPromise

        // создана новая сущность
        {
            if(!tool.tableExists("eductr_pr_edu_dpo_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("eductr_pr_edu_dpo_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eductreducationaddpromise"),
                        new DBColumn("eduprogram_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);
            }

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("eduCtrEducationAddPromise");

        }


    }
}