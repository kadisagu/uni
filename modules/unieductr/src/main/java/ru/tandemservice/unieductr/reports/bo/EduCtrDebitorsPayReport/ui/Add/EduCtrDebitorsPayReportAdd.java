/**
 *$Id:$
 */
package ru.tandemservice.unieductr.reports.bo.EduCtrDebitorsPayReport.ui.Add;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractReadAggregateHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 30.10.12
 */
@Configuration
public class EduCtrDebitorsPayReportAdd extends BusinessComponentManager
{
    public static final String GROUP_DS = "groupDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
        .addDataSource(UniStudentManger.instance().courseDSConfig())
        .addDataSource(selectDS(GROUP_DS, groupDSHandler()))
        .addAddon(uiAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME, UniEduProgramEducationOrgUnitAddon.class))
        .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> groupDSHandler()
    {
        return new AbstractReadAggregateHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                final Long orgUnitId = context.get(EduCtrDebitorsPayReportAddUI.PROP_ORGUNIT_ID);
                List<EducationOrgUnit> educationOrgUnitList = context.get(EduCtrDebitorsPayReportAddUI.PROP_EDU_ORGUNIT_LIST);

                // Группа - необязат. выпадающий список из контингентских неархивных групп.
                // Фильтрация по полям блока фильтров по НПП - группы, где НПП группы удовлетворяет выбранному в фильтрах,
                // или в группе есть студент, НПП которого удовлетворяет выбранному в фильтрах.
                // Если строится на деканате, то только группы студентов этого деканата.

                final Set keySet = input.getPrimaryKeys();
                if (null != keySet && (keySet.isEmpty())) {
                    return ListOutputBuilder.get(input, new ArrayList(0)).build();
                }

                final Set<Group> groupSet = new HashSet<>();
                final String filter = input.getComboFilterByValue();

                final DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(Group.class, "r")
                .column(property("r.id"))
                .where(eq(property(Group.archival().fromAlias("r")), value(false)));
                if (null != keySet) {
                    builder.where(in(property("r.id"), keySet));
                }

                if (null != orgUnitId) {
                    builder.where(
                        exists(
                            new DQLSelectBuilder()
                            .fromEntity(Student.class, "inclS")
                            .column(property("inclS.id"))
                            .where(eq(property(Student.group().fromAlias("inclS")), property("r")))
                            .where(or(
                                eq(property(Student.educationOrgUnit().groupOrgUnit().id().fromAlias("inclS")), value(orgUnitId)),
                                eq(property(Student.educationOrgUnit().formativeOrgUnit().id().fromAlias("inclS")), value(orgUnitId))
                            ))
                            .buildQuery()
                        )
                    );
                }

                if (!StringUtils.isEmpty(filter)) {
                    builder.where(DQLExpressions.likeUpper(property(Group.title().fromAlias("r")), value(CoreStringUtils.escapeLike(filter, true))));
                }

                BatchUtils.execute(educationOrgUnitList, 128, elements -> {

                    // параметры группы
                    groupSet.addAll(
                        new DQLSelectBuilder()
                        .fromEntity(Group.class, "g")
                        .where(in(property("g.id"), builder.buildQuery()))
                        .where(in(property(Group.educationOrgUnit().fromAlias("g")), elements))
                        .createStatement(context.getSession()).<Group>list()
                    );

                    // через студентов в группе
                    groupSet.addAll(
                        new DQLSelectBuilder()
                        .fromEntity(Group.class, "g")
                        .where(in(property("g.id"), builder.buildQuery()))
                        .where(exists(
                                new DQLSelectBuilder()
                                        .fromEntity(Student.class, "y")
                                        .column(property("y.id"))
                                        .where(eq(property(Student.group().fromAlias("y")), property("g")))
                                        .where(in(property(Student.educationOrgUnit().fromAlias("y")), elements))
                                        .buildQuery()
                        ))
                        .createStatement(context.getSession()).<Group>list()
                    );
                });

                return ListOutputBuilder.get(input, new ArrayList<>(groupSet)).pageable(false).build().ordering(new EntityComparator(new EntityOrder(Group.title().s())));
            }
        };
    }
}
