package ru.tandemservice.unieductr.base.bo.EduCtrEducationPromise.ui.SectionPromise;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.Section.CtrSectionPromiceListUIPresenter;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;

import java.util.Collection;
import java.util.List;

public class EduCtrEducationPromiseSectionPromiseUI extends CtrSectionPromiceListUIPresenter<EduCtrEducationPromise>
{
    @Override
    protected IdentifiableWrapper<EduCtrEducationPromise> buildElementEditWrapper(EduCtrEducationPromise selectedElement) {
        return new EduCtrEducationPromiseWrapper(selectedElement);
    }

    @SuppressWarnings("unchecked")
    public Collection<CtrContractVersionContractor> getEducationPromiseSrcList(final List<CtrContractVersionContractor> contractorList) {
        return CollectionUtils.select(contractorList, object -> (object.getContactor() instanceof EmployeePostContactor));
    }

    public Collection<CtrContractVersionContractor> getEducationPromiseSrcList() {
        return this.getEducationPromiseSrcList(this.getContractorList());
    }
}


