package ru.tandemservice.unieductr.base.entity;

import java.util.Date;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

/**
 * @author vdanilov
 */
public interface IEducationContractVersionTemplateData extends IEntity {

    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_OWNER = "owner";

    EducationYear getEducationYear();
    CtrContractVersion getOwner();

}
