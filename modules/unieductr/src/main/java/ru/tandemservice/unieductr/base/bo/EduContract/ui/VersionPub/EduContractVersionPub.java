package ru.tandemservice.unieductr.base.bo.EduContract.ui.VersionPub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * Публикатор версии договора на обучение
 * @author vdanilov
 */
@Configuration
public class EduContractVersionPub extends BusinessComponentManager
{
    public static final String ACTIONS_BL = "eduContractRelationActionButtonList";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder().create();
    }

    @Bean
    public ButtonListExtPoint actionsButtonList()
    {
        return this.buttonListExtPointBuilder(ACTIONS_BL)
        .addButton(submitButton("delete", "onDeleteEntity").alert("message:" + ACTIONS_BL + ".delete.alert").disabled("ui:readOnly").visible("ui:deleteAllowed").permissionKey("ui:context.secModel.delete").create())
        .create();
    }

}
