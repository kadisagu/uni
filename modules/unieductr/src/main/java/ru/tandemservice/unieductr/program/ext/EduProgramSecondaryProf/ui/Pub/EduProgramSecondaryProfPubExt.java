/* $Id$ */
package ru.tandemservice.unieductr.program.ext.EduProgramSecondaryProf.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;

import ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.ui.Pub.EduProgramSecondaryProfPub;
import ru.tandemservice.unieductr.base.bo.EduProgramPrice.ui.Tab.EduProgramPriceTab;

/**
 * @author nvankov
 * @since 2/26/14
 */

@Configuration
public class EduProgramSecondaryProfPubExt extends BusinessComponentExtensionManager
{
    @Autowired
    private EduProgramSecondaryProfPub _eduProgramSecondaryProfPub;

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_eduProgramSecondaryProfPub.tabPanelExtPoint())
        .addTab(
            componentTab("eduProgramPriceTab", EduProgramPriceTab.class.getSimpleName())
            .permissionKey("viewPriceTab_eduProgramSecondaryProfPrice")
            .parameters("mvel:['permissionKeyPostfix': 'eduProgramSecondaryProfPrice']")
        )
        .create();
    }
}
