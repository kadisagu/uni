package ru.tandemservice.unieductr.student.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContractTemplateData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные шаблона договора на обучение студента по ОП
 *
 * Данные базового шаблона для создания договора на обучение студента по образовательной программе.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduCtrStudentContractTemplateDataGen extends EduCtrContractVersionTemplateData
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unieductr.student.entity.EduCtrStudentContractTemplateData";
    public static final String ENTITY_NAME = "eduCtrStudentContractTemplateData";
    public static final int VERSION_HASH = 1866079143;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduCtrStudentContractTemplateDataGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduCtrStudentContractTemplateDataGen> extends EduCtrContractVersionTemplateData.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduCtrStudentContractTemplateData.class;
        }

        public T newInstance()
        {
            return (T) new EduCtrStudentContractTemplateData();
        }
    }
    private static final Path<EduCtrStudentContractTemplateData> _dslPath = new Path<EduCtrStudentContractTemplateData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduCtrStudentContractTemplateData");
    }
            

    public static class Path<E extends EduCtrStudentContractTemplateData> extends EduCtrContractVersionTemplateData.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EduCtrStudentContractTemplateData.class;
        }

        public String getEntityName()
        {
            return "eduCtrStudentContractTemplateData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
