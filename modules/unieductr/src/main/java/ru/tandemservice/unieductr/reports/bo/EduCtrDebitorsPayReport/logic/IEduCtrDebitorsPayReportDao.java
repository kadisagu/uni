/**
 *$Id:$
 */
package ru.tandemservice.unieductr.reports.bo.EduCtrDebitorsPayReport.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport;

import java.util.List;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 06.11.12
 */
public interface IEduCtrDebitorsPayReportDao extends INeedPersistenceSupport
{
    /**
     * Создает документ отчета.
     * @return RtfDocument
     */
    <M extends Model> RtfDocument createReportRtfDocument(M model);

    /**
     * Сохраняет отчет и его печатную форму.
     */
    <M extends Model> EduCtrDebitorsPayReport saveReport(M model, RtfDocument document);

    /**
     * @param studentIds ids студентов
     * @return Возвращает мап связи договора на обучение со студентом
     */
    Map<Long, Long> getStudentContractRel(List<Long> studentIds);
}
