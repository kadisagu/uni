/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStopContractTemplate.ui.Edit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unieductr.base.entity.EduCtrStopVersionTemplateData;

import java.util.Date;

/**
 * @author azhebko
 * @since 01.09.2014
 */
@Input(@Bind(key = UIPresenter.PUBLISHER_ID, binding = "versionTemplateData.id", required = true))
public class EduCtrStopContractTemplateEditUI extends UIPresenter
{
    private EduCtrStopVersionTemplateData _versionTemplateData = new EduCtrStopVersionTemplateData();
    public EduCtrStopVersionTemplateData getVersionTemplateData() { return _versionTemplateData; }
    private CtrContractVersion _version;
    private CtrContractVersion _prevVersion;

    @Override
    public void onComponentActivate() {
        _versionTemplateData = IUniBaseDao.instance.get().getNotNull(EduCtrStopVersionTemplateData.class, getVersionTemplateData().getId());
        setVersion(_versionTemplateData.getOwner());
        _prevVersion = CtrContractVersionManager.instance().dao().getPreviousVersion(_version);

    }

    @Override
    public void onComponentRefresh()
    {

    }

    public void onClickApply()
    {

        final Date durationBeginDate = _version.getDurationBeginDate();
        if(durationBeginDate != null && durationBeginDate.before(_prevVersion.getDurationBeginDate()))
            _uiSupport.error("Дату начала действия нельзя установить раньше даты начала действия предыдущей версии", "durationBeginDate");

        if(getUserContext().getErrorCollector().hasErrors())
            return;

        IUniBaseDao.instance.get().update(getVersionTemplateData());
        deactivate();
    }

    public void onClickClose() { deactivate(); }

    public CtrContractVersion getVersion()
    {
        return _version;
    }

    public void setVersion(CtrContractVersion version)
    {
        this._version = version;
    }

}