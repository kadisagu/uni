/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrPaymentsReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unieductr.reports.bo.EduCtrPaymentsReport.logic.EduCtrPaymentsReportDao;
import ru.tandemservice.unieductr.reports.bo.EduCtrPaymentsReport.logic.IEduCtrPaymentsReportDao;

/**
 * @author Alexey Lopatin
 * @since 17.04.2015
 */
@Configuration
public class EduCtrPaymentsReportManager extends BusinessObjectManager
{
    public static EduCtrPaymentsReportManager instance()
    {
        return instance(EduCtrPaymentsReportManager.class);
    }

    @Bean
    public IEduCtrPaymentsReportDao dao()
    {
        return new EduCtrPaymentsReportDao();
    }
}
