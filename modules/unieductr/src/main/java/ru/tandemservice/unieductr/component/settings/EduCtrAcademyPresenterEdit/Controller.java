/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unieductr.component.settings.EduCtrAcademyPresenterEdit;

import java.util.Iterator;
import java.util.Map;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;

import ru.tandemservice.unieductr.base.entity.EduCtrAcademyPresenter;

/**
 * @author iolshvang
 * @since 06.06.11 17:31
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickApply(final IBusinessComponent component)
    {
        this.getDao().update(this.getModel(component));
        this.deactivate(component);
    }

    public void onClickDeleteEmployeePost(final IBusinessComponent component)
    {
        final Long id = component.getListenerParameter();
        if (null == id) { return; }

        final Map<EmployeePost, EduCtrAcademyPresenter> map = this.getModel(component).getOrgUnitPresenterMap();
        for (final Iterator<EmployeePost> it = map.keySet().iterator(); it.hasNext(); ) {
            if (id.equals(it.next().getId())) { it.remove(); }
        }
    }
}
