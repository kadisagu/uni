package ru.tandemservice.unieductr.base.bo.EduContract.ui.StudentTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.SimpleMergeIdResolver;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.PubCtr.CtrContractVersionPubCtr;

import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractorRole;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduContractListDataSourceHandler;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduCtrStudentContractListHandler;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData;
import ru.tandemservice.unieductr.base.entity.IEducationPromise;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;

@Configuration
public class EduContractStudentTab extends BusinessComponentManager
{
    public static final String CONTRACT_LIST_DS = "contractListDS";
    private static final String EDU_CONTRACT_STUDENT_TAB_BL_EXT_POINT = "eduContractStudentTabBLExtPoint";
    private static final String BUTTON_ADD_CONTRACT = "addContract";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
        .addDataSource(this.searchListDS(CONTRACT_LIST_DS, this.contractListDSColumns(), this.contractListDSHandler()))
        .create();
    }

    @Bean
    public ColumnListExtPoint contractListDSColumns() {
        return columnListExtPointBuilder(CONTRACT_LIST_DS)
        .addColumn(publisherColumn("contractNumber", EduCtrStudentContract.contractObject().number()).businessComponent(CtrContractVersionPubCtr.class).required(true))
        .addColumn(textColumn("eduProgram", EduCtrStudentContractListHandler.VIEW_PROPERTY_EDU_PROMISE_FIRST+"."+EduCtrEducationPromise.eduProgram().title()).required(true))
        .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler contractListDSHandler() {
        return new EduCtrStudentContractListHandler(getName());
    }

    @Bean
    public ButtonListExtPoint eduContractStudentTabBLExtPoint()
    {
        return buttonListExtPointBuilder(EDU_CONTRACT_STUDENT_TAB_BL_EXT_POINT).
                addButton(submitButton(BUTTON_ADD_CONTRACT, "onClickShowPub")
                .listener("onClickAddStudentContract")
                .permissionKey("addContract_eduCtrContractList_student")
                .renderAsLink(true))
                .create();
    }

}
