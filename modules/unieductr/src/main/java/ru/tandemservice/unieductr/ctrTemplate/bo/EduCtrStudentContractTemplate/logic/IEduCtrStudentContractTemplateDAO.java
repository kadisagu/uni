package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateDAO;

import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContractTemplateData;

/**
 * @author vdanilov
 */
public interface IEduCtrStudentContractTemplateDAO extends ICtrContractTemplateDAO, INeedPersistenceSupport {

    void doUpdateContractVersionContactor(CtrContractVersion version, ContactorPerson provider, ContactorPerson customer);

    /** @param templateData */
    void doSaveTemplate(EduCtrStudentContractTemplateData templateData);

    /**
     * @param templateAddData
     * @return
     */
    EduCtrStudentContractTemplateData doCreateVersion(IEduCtrStudentContractTemplateAddData templateAddData);

}
