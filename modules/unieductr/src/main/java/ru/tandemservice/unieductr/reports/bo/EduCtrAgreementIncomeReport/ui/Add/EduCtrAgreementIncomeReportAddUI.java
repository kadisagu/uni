/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrAgreementIncomeReport.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unieductr.reports.bo.EduCtrAgreementIncomeReport.EduCtrAgreementIncomeReportManager;
import ru.tandemservice.unieductr.reports.bo.EduCtrAgreementIncomeReport.logic.Model;
import ru.tandemservice.unieductr.reports.bo.EduCtrAgreementIncomeReport.ui.List.EduCtrAgreementIncomeReportListUI;
import ru.tandemservice.unieductr.reports.bo.EduCtrAgreementIncomeReport.ui.Pub.EduCtrAgreementIncomeReportPub;
import ru.tandemservice.unieductr.reports.entity.EduCtrAgreementIncomeReport;

/**
 * @author Alexey Lopatin
 * @since 21.04.2015
 */
@Input({@Bind(key = EduCtrAgreementIncomeReportListUI.PROP_ORG_UNIT_ID, binding = "orgUnitId")})
public class EduCtrAgreementIncomeReportAddUI extends UIPresenter
{
    public static final String PROP_ORGUNIT_ID = "orgUnitId";

    public static final String PROP_PROGRAM_SUBJECT_LIST = "programSubjectList";
    public static final String PROP_PROGRAM_FORM_LIST = "programFormList";
    public static final String PROP_EDU_BEGIN_YEAR_LIST = "eduBeginYearList";
    public static final String PROP_PROGRAM_TRAIT_LIST = "programTraitList";

    public static final String PROP_EDU_ORGUNIT_LIST = "educationOrgUnitList";
    public static final String PROP_COURSE = "course";

    private Long _orgUnitId;

    private boolean _activeContactorType;

    private boolean _activeProgramSubject;
    private boolean _activeProgramForm;
    private boolean _activeEduBeginYear;
    private boolean _activeProgramTrait;
    private boolean _activeEduProgram;

    private boolean _activeCourse;
    private boolean _activeGroup;

    private Model _model;

    @Override
    public void onComponentRefresh()
    {
        if (null == _model) _model = new Model();
        UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);

        if (_orgUnitId != null) {
            util.configWhere(EducationOrgUnit.formativeOrgUnit().id(), _orgUnitId, false);
        }

        _model.setAccountInactiveStudents(TwinComboDataSourceHandler.getNoOption());

        util
                .configNeedEnableCheckBox(true)
                .configUseFilters(
                        UniEduProgramEducationOrgUnitAddon.Filters.FORMATIVE_ORG_UNIT,
                        UniEduProgramEducationOrgUnitAddon.Filters.TERRITORIAL_ORG_UNIT,
                        UniEduProgramEducationOrgUnitAddon.Filters.EDUCATION_LEVEL_HIGH_SCHOOL,
                        UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_FORM,
                        UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_CONDITION,
                        UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_TECH,
                        UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_PERIOD);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(EduCtrAgreementIncomeReportAdd.EDU_PROGRAM_DS))
        {
            dataSource.put(PROP_PROGRAM_SUBJECT_LIST, isActiveProgramSubject() ? _model.getProgramSubjectList() : null);
            dataSource.put(PROP_PROGRAM_FORM_LIST, isActiveProgramForm() ? _model.getProgramFormList() : null);
            dataSource.put(PROP_EDU_BEGIN_YEAR_LIST, isActiveEduBeginYear() ? _model.getEduBeginYearList() : null);
            dataSource.put(PROP_PROGRAM_TRAIT_LIST, isActiveProgramTrait() ? _model.getProgramTraitList() : null);
        }
        else if (dataSource.getName().equals(EduCtrAgreementIncomeReportAdd.GROUP_DS))
        {
            UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);

            dataSource.put(PROP_ORGUNIT_ID, _orgUnitId);
            dataSource.put(PROP_COURSE, isActiveCourse() ? _model.getCourse() : null);
            dataSource.put(PROP_EDU_ORGUNIT_LIST, util.getEducationOrgUnitFilteredList());
        }
    }

    public void onClickApply()
    {
        if (validate().hasErrors()) return;

        UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);

        _model.setOrgUnitId(_orgUnitId);
        _model.setUtil(util);
        _model.setActiveProgramSubject(isActiveProgramSubject());
        _model.setActiveProgramForm(isActiveProgramForm());
        _model.setActiveEduBeginYear(isActiveEduBeginYear());
        _model.setActiveProgramTrait(isActiveProgramTrait());
        _model.setActiveEduProgram(isActiveEduProgram());
        _model.setActiveFieldSetStudent(isActiveFieldSetStudent());
        _model.setActiveCourse(isActiveCourse());
        _model.setActiveGroup(isActiveGroup());

        RtfDocument document = EduCtrAgreementIncomeReportManager.instance().dao().createReportRtfDocument(_model);
        EduCtrAgreementIncomeReport report = EduCtrAgreementIncomeReportManager.instance().dao().saveReport(_model, document);

        deactivate();
        _uiActivation.asDesktopRoot(EduCtrAgreementIncomeReportPub.class).parameter(UIPresenter.PUBLISHER_ID, report.getId()).activate();
    }

    public ErrorCollector validate()
    {
        ErrorCollector errorCollector = ContextLocal.getErrorCollector();
        if (_model.getPeriodDateFrom().after(_model.getPeriodDateTo()))
        {
            errorCollector.add("Дата периода «с» не может быть позже даты периода «по».", "periodDateFrom", "periodDateTo");
        }
        return errorCollector;
    }

    public String getPermissionKey()
    {
        return _orgUnitId == null ? "eduCtrAgreementIncomeReport" : new OrgUnitSecModel(DataAccessServices.dao().get(OrgUnit.class, _orgUnitId)).getPermission("orgUnit_viewEduCtrAgreementIncomeReport");
    }

    @Override
    public ISecured getSecuredObject()
    {
        return _orgUnitId != null ? DataAccessServices.dao().get(OrgUnit.class, _orgUnitId) : super.getSecuredObject();
    }

    // Getters & Setters

    public boolean isActiveFieldSetEduProgram()
    {
        return isActiveProgramSubject() || isActiveProgramForm() || isActiveEduBeginYear() || isActiveProgramTrait() || isActiveEduProgram();
    }

    public boolean isActiveFieldSetStudent()
    {
        UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
        for (UniEduProgramEducationOrgUnitAddon.Filters filter : UniEduProgramEducationOrgUnitAddon.Filters.BASE_FILTER_SET)
        {
            if (util.getFilterConfig(filter).isCheckEnableCheckBox()) return true;
        }
        return isActiveCourse() || isActiveGroup();
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public Model getModel()
    {
        return _model;
    }

    public void setModel(Model model)
    {
        _model = model;
    }

    public boolean isActiveContactorType()
    {
        return _activeContactorType;
    }

    public void setActiveContactorType(boolean activeContactorType)
    {
        _activeContactorType = activeContactorType;
    }

    public boolean isActiveProgramSubject()
    {
        return _activeProgramSubject;
    }

    public void setActiveProgramSubject(boolean activeProgramSubject)
    {
        _activeProgramSubject = activeProgramSubject;
    }

    public boolean isActiveProgramForm()
    {
        return _activeProgramForm;
    }

    public void setActiveProgramForm(boolean activeProgramForm)
    {
        _activeProgramForm = activeProgramForm;
    }

    public boolean isActiveEduBeginYear()
    {
        return _activeEduBeginYear;
    }

    public void setActiveEduBeginYear(boolean activeEduBeginYear)
    {
        _activeEduBeginYear = activeEduBeginYear;
    }

    public boolean isActiveProgramTrait()
    {
        return _activeProgramTrait;
    }

    public void setActiveProgramTrait(boolean activeProgramTrait)
    {
        _activeProgramTrait = activeProgramTrait;
    }

    public boolean isActiveEduProgram()
    {
        return _activeEduProgram;
    }

    public void setActiveEduProgram(boolean activeEduProgram)
    {
        _activeEduProgram = activeEduProgram;
    }

    public boolean isActiveCourse()
    {
        return _activeCourse;
    }

    public void setActiveCourse(boolean activeCourse)
    {
        _activeCourse = activeCourse;
    }

    public boolean isActiveGroup()
    {
        return _activeGroup;
    }

    public void setActiveGroup(boolean activeGroup)
    {
        _activeGroup = activeGroup;
    }
}
