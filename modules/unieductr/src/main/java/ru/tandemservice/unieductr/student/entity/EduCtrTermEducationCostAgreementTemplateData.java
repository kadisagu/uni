package ru.tandemservice.unieductr.student.entity;

import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.EduCtrTermEducationCostAgreementTemplateManager;
import ru.tandemservice.unieductr.student.entity.gen.*;

/** @see ru.tandemservice.unieductr.student.entity.gen.EduCtrTermEducationCostAgreementTemplateDataGen */
public class EduCtrTermEducationCostAgreementTemplateData extends EduCtrTermEducationCostAgreementTemplateDataGen
{
    @Override
    public ICtrContractTemplateManager getManager()
    {
        return EduCtrTermEducationCostAgreementTemplateManager.instance();
    }
}