package ru.tandemservice.unieductr.migration;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unieductr_2x6x6_0to1 extends IndependentMigrationScript
{
    private final static Logger logger = Logger.getLogger(MS_unieductr_2x6x6_0to1.class);

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eduCtrContractVersionTemplateData

		// создано свойство priceElementCost
		{
			// создать колонку
			tool.createColumn("eductr_ctmpldt_t", new DBColumn("cost_id", DBType.LONG));

            // заполняем ценой по умолчанию
            Statement statement = tool.getConnection().createStatement();
            statement.execute("" +
                "select ctd.id, s.cost_id from eductr_ctmpldt_t ctd " +
                "inner join ctr_version_template_data_t cvtd on cvtd.id = ctd.id " +
                "inner join ctr_contractver_t cv on cv.id = cvtd.owner_id " +
                "inner join ctr_contractor_t c on c.owner_id = cv.id " +
                "inner join ctr_promice_t p on p.src_id = c.id " +
                "inner join ctr_pr_payment_t pp on pp.id = p.id " +
                "left join ctrpr_prelcost_stage s on s.id = pp.sourcestage_id ");

            ResultSet resultSet = statement.getResultSet();
            Map<Long, Collection<Long>> eduContractCostMap = SafeMap.get(HashSet.class);
            while (resultSet.next())
            {
                Collection<Long> eduContractCosts = eduContractCostMap.get(resultSet.getLong(1));
                long costId = resultSet.getLong(2);
                if (!resultSet.wasNull())
                    eduContractCosts.add((Long) costId);
            }

            logger.setLevel(Level.INFO);
            PreparedStatement preparedStatement = tool.getConnection().prepareStatement("update eductr_ctmpldt_t set cost_id = ? where id = ?");
            for (Map.Entry<Long, Collection<Long>> eduContractEntry: eduContractCostMap.entrySet())
            {
                Collection<Long> costs = eduContractEntry.getValue();
                Long eduContract = eduContractEntry.getKey();
                if (costs.size() != 1)
                {
                    logger.info("Не удалось помигрировать договор ( id = " + eduContract + ")");

                } else
                {
                    preparedStatement.setLong(1, costs.iterator().next());
                    preparedStatement.setLong(2, eduContract);
                    preparedStatement.execute();
                }
            }
		}
    }
}