package ru.tandemservice.unieductr.base.bo.EduSystemAction;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unieductr.base.bo.EduSystemAction.logic.EduSystemActionDao;
import ru.tandemservice.unieductr.base.bo.EduSystemAction.logic.IEduSystemActionDao;

/**
 * Created by Denny on 7/13/2016.
 */
@Configuration
public class EduActionActionManager extends BusinessObjectManager {

    public static EduActionActionManager instance() {
        return instance(EduActionActionManager.class);
    }

    @Bean
    public IEduSystemActionDao dao() {
        return new EduSystemActionDao();
    }
}
