/* $Id$ */
package ru.tandemservice.unieductr.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unieductr_2x10x7_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.7")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность eduCtrObligationsContractReport

        // создана новая сущность
       if (!tool.tableExists("edu_ctr_obl_ctr_report_t"))
       {
            // создать таблицу
            DBTable dbt = new DBTable("edu_ctr_obl_ctr_report_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_d9a492b0"),
                                      new DBColumn("orgunit_id", DBType.LONG),
                                      new DBColumn("comment_p", DBType.createVarchar(255)),
                                      new DBColumn("perioddatefrom_p", DBType.TIMESTAMP),
                                      new DBColumn("perioddateto_p", DBType.TIMESTAMP),
                                      new DBColumn("formativeorgunit_p", DBType.createVarchar(1024)),
                                      new DBColumn("territorialorgunit_p", DBType.createVarchar(1024)),
                                      new DBColumn("educationlevelhighschool_p", DBType.createVarchar(1024)),
                                      new DBColumn("developform_p", DBType.createVarchar(255)),
                                      new DBColumn("developcondition_p", DBType.createVarchar(255)),
                                      new DBColumn("developtech_p", DBType.createVarchar(255)),
                                      new DBColumn("developperiod_p", DBType.createVarchar(255)),
                                      new DBColumn("course_p", DBType.createVarchar(255)),
                                      new DBColumn("group_p", DBType.createVarchar(255)),
                                      new DBColumn("studentstatus_p", DBType.createVarchar(255))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("eduCtrObligationsContractReport");

        }


    }
}