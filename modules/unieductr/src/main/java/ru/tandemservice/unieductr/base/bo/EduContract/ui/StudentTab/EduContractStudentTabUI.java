package ru.tandemservice.unieductr.base.bo.EduContract.ui.StudentTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.AddByTemplate.CtrContractVersionAddByTemplate;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduCtrStudentContractListHandler;

@Input({
    @Bind(key=UIPresenter.PUBLISHER_ID, binding="studentHolder.id", required=true)
})
public class EduContractStudentTabUI extends UIPresenter
{
    private final EntityHolder<Student> studentHolder = new EntityHolder<>();
    public EntityHolder<Student> getStudentHolder() { return this.studentHolder; }
    public Student getStudent() { return getStudentHolder().getValue(); }

    private BaseSearchListDataSource contractListDataSource;
    public BaseSearchListDataSource getContractListDataSource() { return this.contractListDataSource; }

    @Override
    public void onComponentRefresh() {
        this.contractListDataSource = (BaseSearchListDataSource)this.getConfig().getDataSource(EduContractStudentTab.CONTRACT_LIST_DS);
        getStudentHolder().refresh(Student.class);
    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if (EduContractStudentTab.CONTRACT_LIST_DS.equals(dataSource.getName())) {
            dataSource.put(EduCtrStudentContractListHandler.PARAM_STUDENT_ID, getStudent().getId());
        }
    }

    public void onClickAddStudentContract() {
        this._uiActivation.asRegionDialog(CtrContractVersionAddByTemplate.class).parameter(IUIPresenter.PUBLISHER_ID, getStudent().getId()).activate();
    }

}
