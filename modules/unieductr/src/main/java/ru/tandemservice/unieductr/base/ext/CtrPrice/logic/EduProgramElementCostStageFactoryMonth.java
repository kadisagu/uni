/* $Id:$ */
package ru.tandemservice.unieductr.base.ext.CtrPrice.logic;

import org.tandemframework.shared.ctr.base.bo.CtrPrice.util.ICtrPriceElementCostStageFactory;
import org.tandemframework.shared.ctr.base.entity.ICtrPriceElement;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import ru.tandemservice.unieductr.base.entity.EduProgramPrice;

import java.util.*;

/**
 * @author oleyba
 * @since 7/1/15
 */
public class EduProgramElementCostStageFactoryMonth implements ICtrPriceElementCostStageFactory
{
    @Override
    public List<CtrPriceElementCostStage> getStageList(ICtrPriceElement priceElement)
    {
        final Map<Integer, Integer> term2courseMap;
        if (priceElement instanceof EduProgramPrice) {
            term2courseMap = EduProgramElementCostStageFactoryTerm.getStageCodeSource((EduProgramPrice) priceElement);
        } else {
            return new ArrayList<>(0);
        }

        List<CtrPriceElementCostStage> stagesList = new ArrayList<CtrPriceElementCostStage>();
        Set<Integer> courses = new HashSet<>(term2courseMap.values());

        for (Integer course : courses)  {
            for (int part = 1; part <= 10; part++) {
                CtrPriceElementCostStage stage = new CtrPriceElementCostStage();
                stage.setTitle(part + " часть (" + course + " курс)");
                stage.setStageUniqueCode(course + "-" + part);
                stagesList.add(stage);
            }
        }

        return stagesList;
    }

    @Override
    public List<CtrPriceElementCostStage> filterStageList(ICtrPriceElement priceElement, List<CtrPriceElementCostStage> stageList, Map<String, Object> filter)
    {
        return stageList;
    }
}
