/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unieductr.student.entity.EduCtrTermEducationCostAgreementTemplateData;

/**
 * @author azhebko
 * @since 04.12.2014
 */
@State(@Bind(key = IUIPresenter.PUBLISHER_ID, binding = "template.id", required = true))
public class EduCtrTermEducationCostAgreementTemplatePubUI extends UIPresenter
{
    private EduCtrTermEducationCostAgreementTemplateData _template = new EduCtrTermEducationCostAgreementTemplateData();

    @Override
    public void onComponentRefresh()
    {
        _template = IUniBaseDao.instance.get().getNotNull(EduCtrTermEducationCostAgreementTemplateData.class, getTemplate().getId());
    }

    public EduCtrTermEducationCostAgreementTemplateData getTemplate() { return _template; }
}