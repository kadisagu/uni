/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unieductr.component.settings.EduCtrAcademyPresenter;

import java.util.Comparator;
import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author iolshvang
 * @since 06.06.11 16:29
 */
public class Model
{
    public static final Long ALL_ORG_UNITS_ID = 0L;
    public static final Long NON_ARCHIVED_ORG_UNITS_ID = 1L;

    private DynamicListDataSource<OrgUnit> _dataSource;
    private Comparator<ViewWrapper<OrgUnit>> _comparator;
    private boolean _filtersBlockVisible;

    private IDataSettings _settings;
    private List<IdentifiableWrapper> _orgUnitKindsList;

    public DynamicListDataSource<OrgUnit> getDataSource()
    {
        return this._dataSource;
    }

    public void setDataSource(final DynamicListDataSource<OrgUnit> dataSource)
    {
        this._dataSource = dataSource;
    }

    public Comparator<ViewWrapper<OrgUnit>> getComparator()
    {
        return this._comparator;
    }

    public void setComparator(final Comparator<ViewWrapper<OrgUnit>> comparator)
    {
        this._comparator = comparator;
    }

    public boolean isFiltersBlockVisible()
    {
        return this._filtersBlockVisible;
    }

    public void setFiltersBlockVisible(final boolean filtersBlockVisible)
    {
        this._filtersBlockVisible = filtersBlockVisible;
    }

    public IDataSettings getSettings()
    {
        return this._settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        this._settings = settings;
    }

    public List<IdentifiableWrapper> getOrgUnitKindsList()
    {
        return this._orgUnitKindsList;
    }

    public void setOrgUnitKindsList(final List<IdentifiableWrapper> orgUnitKindsList)
    {
        this._orgUnitKindsList = orgUnitKindsList;
    }
}