package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentSpoContractTemplate.logic;

import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractKind;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.ctr.catalog.entity.CtrPrintTemplate;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;

import java.util.Date;
import java.util.List;

/**
 * @author vdanilov
 */
public interface IEduCtrStudentSpoContractTemplateAddData
{

    Student getStudent();
    EduProgramProf getEduProgram();
    EducationYear getContractEducationYear();

    OrgUnit getFormativeOrgUnit();
    CtrContractType getContractType();
    CtrContractVersionCreateData getVersionCreateData();
    ContactorPerson getCustomer();
    ContactorPerson getProvider();
    Date getStartDate();
    Date getPriceDate();
    Date getEndDate();
    Date getRegDate();
    Date getEnrollmentDate();
    String getCipher();

    CtrPriceElementCost getSelectedCost();
    List<CtrPriceElementCostStage> getSelectedCostStages();

}
