package ru.tandemservice.unieductr.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unieductr_2x8x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eduCtrPaymentsReport

		// изменен тип свойства programSubject
		{
			// изменить тип колонки
			tool.changeColumnType("edu_ctr_payments_report_t", "programsubject_p", DBType.createVarchar(255));
		}

		// изменен тип свойства programTrait
		{
			// изменить тип колонки
			tool.changeColumnType("edu_ctr_payments_report_t", "programtrait_p", DBType.createVarchar(255));
		}

		// изменен тип свойства eduProgram
		{
			// изменить тип колонки
			tool.changeColumnType("edu_ctr_payments_report_t", "eduprogram_p", DBType.createVarchar(255));
		}
    }
}