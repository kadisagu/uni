package ru.tandemservice.unieductr.student.entity;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;

/**
 * @author vdanilov
 */
public interface IEduStudentContract extends IEduContractRelation {
    public static final String L_STUDENT = "student";

    public Student getStudent();
}
