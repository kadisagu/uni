/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementPeriod;
import org.tandemframework.shared.ctr.catalog.entity.CtrPricePaymentGrid;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author azhebko
 * @since 04.12.2014
 */
@Configuration
public class EduCtrTermEducationCostAgreementTemplateAdd extends BusinessComponentManager
{
    public static final String DS_PRICE_ELEMENT_PERIOD = "priceElementPeriodDS";
    public static final String DS_PAYMENT_GRID = "paymentGridDS";
    public static final String DS_COST = "costDS";
    public static final String PARAM_PRICE_PERIOD_ID = "pricePeriodId";
    public static final String PARAM_PAYMENT_GRID_ID = "paymentGridId";
    public static final String PARAM_ELEMENT_PRICE_ID = "elementPriceId";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(EducationCatalogsManager.instance().eduYearDSConfig())
            .addDataSource(EducationCatalogsManager.instance().yearPartDSConfig())
            .addDataSource(selectDS(DS_PRICE_ELEMENT_PERIOD, priceElementPeriodDSHandler()))
            .addDataSource(selectDS(DS_PAYMENT_GRID, paymentGridDSHandler()))
            .addDataSource(selectDS(DS_COST, costDSHandler()).addColumn(null, null, source ->
            {
                CtrPriceElementCost cost = (CtrPriceElementCost) source;
                return cost.getCostAsCurrencyString() + ", " + cost.getPaymentGrid().getTitle() + ", " + cost.getCategory().getTitle();
            }))
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler priceElementPeriodDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), CtrPriceElementPeriod.class)
        {
            @Override
            protected IDQLExpression like(String alias, MetaDSLPath path, String filter)
            {
                if(EntityRuntime.getMeta(CtrPriceElementPeriod.class.getSimpleName()).getPropertyByThroughPath(path.s()).getValueType().equals(Date.class))
                {
                    return DQLExpressions.like(
                            DQLFunctions.upper(
                                    DQLFunctions.concat(
                                            caseExpr(lt(DQLFunctions.day(DQLExpressions.property(alias, path)), value(10)),
                                                    DQLFunctions.concat(
                                                            commonValue("0", PropertyType.STRING),
                                                            DQLFunctions.cast(DQLFunctions.day(DQLExpressions.property(alias, path)), PropertyType.STRING)
                                                    ),
                                                    DQLFunctions.cast(DQLFunctions.day(DQLExpressions.property(alias, path)), PropertyType.STRING))
                                            ,
                                            commonValue(".", PropertyType.STRING),
                                            caseExpr(lt(DQLFunctions.month(DQLExpressions.property(alias, path)), value(10)),
                                                    DQLFunctions.concat(
                                                            commonValue("0", PropertyType.STRING),
                                                            DQLFunctions.cast(DQLFunctions.month(DQLExpressions.property(alias, path)), PropertyType.STRING)
                                                    ),
                                                    DQLFunctions.cast(DQLFunctions.month(DQLExpressions.property(alias, path)), PropertyType.STRING)),
                                            commonValue(".", PropertyType.STRING),
                                            DQLFunctions.cast(DQLFunctions.year(DQLExpressions.property(alias, path)), PropertyType.STRING))),
                            DQLExpressions.value(CoreStringUtils.escapeLike(filter, true)));
                }
                else
                {
                    return super.like(alias, path, filter);
                }
            }
        }
                .customize((alias, dql, context, filter) ->
                {
                    Long elementId = context.getNotNull(PARAM_ELEMENT_PRICE_ID);

                    DQLSelectBuilder existBuilder = new DQLSelectBuilder().fromEntity(CtrPriceElementCost.class, "ec")
                            .where(eq(property("ec", CtrPriceElementCost.period()), property(alias)))
                            .where(eq(property("ec", CtrPriceElementCost.period().element().id()), value(elementId)));


                    return dql.where(exists(existBuilder.buildQuery()));
                })
                .order(CtrPriceElementPeriod.durationBegin())
                .order(CtrPriceElementPeriod.durationEnd())
                .filter(CtrPriceElementPeriod.durationEnd())
                .filter(CtrPriceElementPeriod.durationBegin());

    }

    @Bean
    public IDefaultComboDataSourceHandler paymentGridDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), CtrPricePaymentGrid.class).customize((alias, dql, context, filter) ->
        {
            Long elementId = context.getNotNull(PARAM_ELEMENT_PRICE_ID);
            Long periodId = context.get(PARAM_PRICE_PERIOD_ID);

            DQLSelectBuilder existBuilder = new DQLSelectBuilder().fromEntity(CtrPriceElementCost.class, "ec")
                    .where(eq(property("ec", CtrPriceElementCost.paymentGrid()), property(alias)))
                    .where(eq(property("ec", CtrPriceElementCost.period().element().id()), value(elementId)));

            if(periodId != null)
            {
                existBuilder.where(eq(property("ec", CtrPriceElementCost.period().id()), value(periodId)));
            }

            return dql.where(exists(existBuilder.buildQuery()));
        })
                .order(CtrPricePaymentGrid.title())
                .filter(CtrPricePaymentGrid.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler costDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), CtrPriceElementCost.class)
                .customize((alias, dql, context, filter) ->
        {
            Long elementId = context.getNotNull(PARAM_ELEMENT_PRICE_ID);
            Long periodId = context.get(PARAM_PRICE_PERIOD_ID);
            Long gridId = context.get(PARAM_PAYMENT_GRID_ID);

            if(periodId == null)
            {
                return dql.where(nothing());
            }

            dql.where(eq(property(alias, CtrPriceElementCost.period().element().id()), value(elementId)))
                    .where(eq(property(alias, CtrPriceElementCost.period().id()), value(periodId)));

            if(gridId != null)
            {
                dql.where(eq(property(alias, CtrPriceElementCost.paymentGrid().id()), value(gridId)));
            }

            return dql;
        })
                .order(CtrPriceElementCost.period().durationBegin())
                .order(CtrPriceElementCost.period().durationEnd())
                .order(CtrPriceElementCost.paymentGrid().title())
                .order(CtrPriceElementCost.totalCostAsLong())
                .filter(CtrPriceElementCost.paymentGrid().title())
                .filter(CtrPriceElementCost.category().title());
    }
}