package ru.tandemservice.unieductr.student.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentSpoContractTemplateData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные шаблона договора на обучение студента по ОП (СПО)
 *
 * Данные базового шаблона для создания договора на обучение студента по образовательной программе (СПО).
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduCtrStudentSpoContractTemplateDataGen extends EduCtrContractVersionTemplateData
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unieductr.student.entity.EduCtrStudentSpoContractTemplateData";
    public static final String ENTITY_NAME = "eduCtrStudentSpoContractTemplateData";
    public static final int VERSION_HASH = 513575362;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduCtrStudentSpoContractTemplateDataGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduCtrStudentSpoContractTemplateDataGen> extends EduCtrContractVersionTemplateData.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduCtrStudentSpoContractTemplateData.class;
        }

        public T newInstance()
        {
            return (T) new EduCtrStudentSpoContractTemplateData();
        }
    }
    private static final Path<EduCtrStudentSpoContractTemplateData> _dslPath = new Path<EduCtrStudentSpoContractTemplateData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduCtrStudentSpoContractTemplateData");
    }
            

    public static class Path<E extends EduCtrStudentSpoContractTemplateData> extends EduCtrContractVersionTemplateData.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EduCtrStudentSpoContractTemplateData.class;
        }

        public String getEntityName()
        {
            return "eduCtrStudentSpoContractTemplateData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
