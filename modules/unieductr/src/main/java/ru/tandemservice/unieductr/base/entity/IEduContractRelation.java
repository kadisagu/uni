package ru.tandemservice.unieductr.base.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContextObject;
import org.tandemframework.shared.person.base.entity.PersonRole;

/**
 * Связь договора на обучение обучаемым (с ролью персоны). Является персистентным интерфейсом.
 * 
 * <p>Договоры создаются на обучение контрагентов (обучаемый выступает в качестве стороны в обязательстве на обучение).
 * При этом договор может быть связан с конкретной ролью этой персоны (с целью фильтрации по контексту),
 * например, с конкретным абитуриентом определенной ПК определенного года (чтобы не показывать договоры этой же персоны прошлых приемок).</p>
 * 
 * <p>Все контекстные списки договоров на обучение (договоры на обучение студента, абитуриента, ...) строятся на основе этой связи.
 * Общий список договоров на обучение персоны стрится через обязательство на обучение {@link IEducationPromise}.</p>
 * 
 * @author vdanilov
 */
public interface IEduContractRelation extends IEntity, ITitled, ICtrContextObject
{
    public static final String L_CONTRACT_OBJECT = "contractObject";
    public static final String L_PERSON_ROLE = "personRole";

    /** тот, кого обучают */
    PersonRole getPersonRole();
}
