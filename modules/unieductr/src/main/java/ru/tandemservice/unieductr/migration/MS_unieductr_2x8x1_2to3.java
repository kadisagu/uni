package ru.tandemservice.unieductr.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unieductr_2x8x1_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eduCtrPaymentsReport

		// изменен тип свойства programSubject
		{
			// изменить тип колонки
			tool.changeColumnType("edu_ctr_payments_report_t", "programsubject_p", DBType.createVarchar(1024));
		}

		// изменен тип свойства eduProgram
		{
			// изменить тип колонки
			tool.changeColumnType("edu_ctr_payments_report_t", "eduprogram_p", DBType.createVarchar(1024));
		}

		// изменен тип свойства formativeOrgUnit
		{
			// изменить тип колонки
			tool.changeColumnType("edu_ctr_payments_report_t", "formativeorgunit_p", DBType.createVarchar(1024));
		}

		// изменен тип свойства territorialOrgUnit
		{
			// изменить тип колонки
			tool.changeColumnType("edu_ctr_payments_report_t", "territorialorgunit_p", DBType.createVarchar(1024));
		}

		// изменен тип свойства educationLevelHighSchool
		{
			// изменить тип колонки
			tool.changeColumnType("edu_ctr_payments_report_t", "educationlevelhighschool_p", DBType.createVarchar(1024));
		}
    }
}