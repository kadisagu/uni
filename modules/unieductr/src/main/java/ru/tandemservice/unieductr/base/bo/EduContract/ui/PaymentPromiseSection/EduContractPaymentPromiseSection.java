/* $Id:$ */
package ru.tandemservice.unieductr.base.bo.EduContract.ui.PaymentPromiseSection;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.meta.BaseComponentMeta;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.sec.bo.Sec.logic.IContextSecuredComponentManager;

/**
 * @author oleyba
 * @since 4/15/14
 */
@Configuration
public class EduContractPaymentPromiseSection extends BusinessComponentManager implements IContextSecuredComponentManager
{

    @Override
    public void fillPermissionGroup(final PermissionGroupMeta parent, final String securityPostfix) {

        // группа
        final BaseComponentMeta meta = getMeta();
        final String groupName = "sec_" + securityPostfix + "_" + meta.getName() + "PG";
        final String title = meta.getPropertySafe("bc.sec.title", meta.getTitle());
        final PermissionGroupMeta group = PermissionMetaUtil.createPermissionGroup(parent, groupName, title);

        // обязательства
        PermissionMetaUtil.createPermission(group, "print_paymentPromiseQuittance_" + securityPostfix, meta.getProperty("bc.sec.promise.print.title"));
        PermissionMetaUtil.createPermission(group, "add_paymentPromise_" + securityPostfix, meta.getProperty("bc.sec.promise.add.title"));
        PermissionMetaUtil.createPermission(group, "edit_paymentPromise_" + securityPostfix, meta.getProperty("bc.sec.promise.edit.title"));
        PermissionMetaUtil.createPermission(group, "delete_paymentPromise_" + securityPostfix, meta.getProperty("bc.sec.promise.delete.title"));

        // факты исполнения
        PermissionMetaUtil.createPermission(group, "add_paymentResult_" + securityPostfix, meta.getProperty("bc.sec.result.add.title"));
        PermissionMetaUtil.createPermission(group, "edit_paymentResult_" + securityPostfix, meta.getProperty("bc.sec.result.edit.title"));
        PermissionMetaUtil.createPermission(group, "delete_paymentResult_" + securityPostfix, meta.getProperty("bc.sec.result.delete.title"));

    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder().create();
    }

}
