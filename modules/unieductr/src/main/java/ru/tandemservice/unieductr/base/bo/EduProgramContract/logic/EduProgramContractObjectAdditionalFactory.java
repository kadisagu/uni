package ru.tandemservice.unieductr.base.bo.EduProgramContract.logic;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.commonbase.catalog.entity.codes.CurrencyCodes;
import org.tandemframework.shared.commonbase.catalog.entity.gen.CurrencyGen;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.CtrPriceManager;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contract.*;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractRole;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.program.entity.EduProgramAdditional;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.base.bo.EduProgramContract.EduProgramContractManager;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationAddPromise;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;

import java.util.*;

/**
 * Фабрика создания договора на обучение по образовательной программе
 * HINT: вызывается только внутри транзакционного dao-метода
 * HINT: шаблон использования - создание анонимного подкласса в месте использования (с заполнением всех абстрактных методов)
 * 
 * @author vdanilov
 * @param <S> класс обучаемого
 */
public abstract class EduProgramContractObjectAdditionalFactory<S> {

    // название блокировки (можно переопдерелять)
    protected String getLockName() {
        return CtrContractObject.ENTITY_CLASS;
    }


    /**@return валюта, в которой будет цена на договор */
    protected Currency getDefaultCurrency(final CtrContractVersion contractVersion) {
        return DataAccessServices.dao().getByNaturalId(new CurrencyGen.NaturalId(CurrencyCodes.RUB));
    }

    /** @return данные вида и подвида новой версии договора */
    protected abstract CtrContractVersionCreateData getVersionCreateData();

    /** @return подразделение, на котором будет создан договор (переопределяется в конкретном месте) */
    protected abstract OrgUnit getCtrContractObjectOrgUnit();

    /** @return номер договора (переопределяется в конкретном месте) */
    protected abstract String getCtrContractObjectNumber();

    /** @return тип договора (переопределяется в конкретном месте) */
    protected abstract CtrContractType getCtrContractObjectType();

    /** @return заказчик (переопределяется в конкретном месте) */
    protected abstract ContactorPerson getCustomer();

    /** @return перечень лиц, обучение которых нужно провести (переопределяется в конкретном месте) */
    protected abstract Collection<S> getStudentList();

    /** @return по обучаемому формирует его человекочитаемое название в интерфейсе */
    protected abstract String getStudentTitle(S student);

    /** @return по обучаемому возвращает (если нужно - создает) контрагента */
    protected abstract ContactorPerson getStudentContactPersion(S student);

    /** @return дата, когда обучаемый должен быть зачислен (выступает как deadlineDate в обязательстве об обучении) */
    protected abstract Date getStudentEnrollmentDate(S student);

    /** @return ОП, на которую должен быть зачислен студент в договоре */
    protected abstract EduProgramAdditional getStudentEduProgram(S student);

    /** @return перечень цен (как правило за одного студента - одна цена, но может быть и несколько) */
    protected abstract Collection<CtrPriceElementCost> getEducationPromiseCostList(S student, EduCtrEducationAddPromise eduPromise, CtrContractVersionContractor paymentSource, Currency defaultCurrency);

    /** @return список (конечных) этапов оплаты для указанной цены */
    protected List<CtrPriceElementCostStage> getEducationPromiseCostListStages(final S student, final EduCtrEducationAddPromise eduPromise, final CtrPriceElementCost cost) {
        return CtrPriceManager.instance().dao().getCostStages(cost);
    }

    /** @return цену за этап (stage.getStageCostAsLong()) */
    protected long getEducationPromiseStageCostAsLong(final S student, final EduCtrEducationAddPromise eduPromise, final CtrPriceElementCostStage stage) {
        return stage.getStageCostAsLong();
    }

    /** @return название обязательства оплаты (для этапов, у которых есть цена) */
    protected String getEducationPromiseStageTitle(final S student, final EduCtrEducationAddPromise eduPromise, final boolean multiStudent, final CtrPriceElementCostStage stage) {
        // if (multiStudent) {
        //     return StringUtils.trimToEmpty(this.getStudentTitle(student)) + " " + stage.getTitle();
        // }
        return stage.getTitle();
    }

    /** @return название обязательства оплаты (для стучая, когда цены нет) */
    protected String getEducationPromiseNoStageTitle(final S student, final EduCtrEducationAddPromise eduPromise, final boolean multiStudent) {
        // if (multiStudent) {
        //     StringUtils.trimToEmpty(this.getStudentTitle(student)) + " " + getPaymentNoStageTitle(eduPromise);
        // }
        return getPaymentNoStageTitle(eduPromise);
    }

    /** @return название обязательства об оплате (для указанного обязательсва по обучению - как правило, это название ОП) */
    public static String getPaymentNoStageTitle(final EduCtrEducationAddPromise eduPromise) {
        return "";
    }

    /** @return идентификатор этапа (по нему будет группировка) */
    protected String getEducationPromiseStageName(final S student, final EduCtrEducationAddPromise eduPromise, final boolean multiStudent, final CtrPriceElementCostStage stage) {
        return stage.getStageUniqueCode();
    }

    /** @return идентификатор этапа, когда этапов нет (по нему будет группировка) */
    protected String getEducationPromiseNoStageName(final S student, final EduCtrEducationAddPromise eduPromise, final boolean multiStudent) {
        return String.valueOf(eduPromise.getId());
    }


    public CtrContractVersion buildContractObject()
    {
        if (!TransactionSynchronizationManager.isActualTransactionActive()) { throw new IllegalStateException("no-transaction"); }
        if (TransactionSynchronizationManager.isCurrentTransactionReadOnly()) { throw new IllegalStateException("transaction-is-read-only"); }

        // мы находимся внутри сессии, так что все ок
        final Session session = DataAccessServices.dao().getCalculatedValue(session1 -> {
            NamedSyncInTransactionCheckLocker.register(session1, EduProgramContractObjectAdditionalFactory.this.getLockName());
            return session1;
        });

        // формируем договор
        final CtrContractObject contractObject = new CtrContractObject();
        contractObject.setOrgUnit(this.getCtrContractObjectOrgUnit());
        contractObject.setNumber(this.getCtrContractObjectNumber());
        contractObject.setType(this.getCtrContractObjectType());
        session.save(contractObject);

        // его первую печатную версию (номер будет скопирован из договора)
        final CtrContractVersion contractVersion = CtrContractVersionManager.instance().dao().doCreateNewVersion(contractObject, getVersionCreateData(), null);
        session.saveOrUpdate(contractVersion);

        // справочник контрагентов (автоматически добавляет в базу)
        final Map<ContactorPerson, CtrContractVersionContractor> contractorMap = SafeMap.get(key -> {
            final CtrContractVersionContractor c = new CtrContractVersionContractor(contractVersion, key);
            session.save(c);
            return c;
        });

        // представилети для заключения договоров (на указанном подразделении)
        final Collection<ContactorPerson> academyPresenters = this.getAcademyPresenters(contractObject);

        // добавляем всех (если что - удалят из договора)
        for (final ContactorPerson presenter: academyPresenters) {
            final CtrContractVersionContractor p = contractorMap.get(presenter);
            p.setRequireSignature(false); // по умолчанию НЕ подписывают
            session.saveOrUpdate(p);
        }

        // заявитель
        final CtrContractVersionContractor customer = contractorMap.get(this.getCustomer());
        customer.setRequireSignature(true); // этот подписывает, если что - поправят либо у себя в коде, либо на форме
        session.update(customer);
        session.save(new CtrContractVersionContractorRole(customer, IUniBaseDao.instance.get().getCatalogItem(CtrContractRole.class, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER)));

        // первый представитель ВУЗа
        final CtrContractVersionContractor provider = contractorMap.get(this.getProvider(academyPresenters));
        provider.setRequireSignature(true); // этот подписывает, если что - поправят либо у себя в коде, либо на форме
        session.update(provider);
        session.save(new CtrContractVersionContractorRole(provider, IUniBaseDao.instance.get().getCatalogItem(CtrContractRole.class, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER)));


        // список тех. кого необходимо обучить
        final Collection<S> students = this.getStudentList();
        if (students.isEmpty()) { throw new ApplicationException(this.error_noStudents()); }

        // список обязательств об оплате (ключ этапа -> список обязательств)
        final Map<String, List<CtrPaymentPromice>> paymentPromiseListMap = SafeMap.get(ArrayList.class);

        // валюта договора по умолчанию
        final Currency defaultCurrency = this.getDefaultCurrency(contractVersion);

        // поехали
        final boolean multiStudent = students.size() > 1;
        for (final S student: students)
        {
            final CtrContractVersionContractor listener = contractorMap.get(this.getStudentContactPersion(student));
            if (null == listener) { throw new ApplicationException(this.error_student_noListener(student)); }

            final Date enrollmentDate = this.getStudentEnrollmentDate(student);
            if (null == enrollmentDate) { throw new ApplicationException(this.error_student_noEnrollmentDate(student)); }

            final EduProgramAdditional eduProgram = this.getStudentEduProgram(student);
            if (null == eduProgram) { throw new ApplicationException(this.error_student_noEduProgram(student)); }

            // ВУЗ обязается зачислить СЛУШАТЕЛЯ на ОП
            EduCtrEducationAddPromise eduPromise = new EduCtrEducationAddPromise();
            eduPromise.setDeadlineDate(enrollmentDate);
            eduPromise.setSrc(provider);
            eduPromise.setDst(listener);
            eduPromise.setEduProgram(eduProgram);
            session.save(eduPromise = this.processEduPromise(student, eduPromise));

            // проверяем, нужно ли создавать обязательства на оплату
            if (this.isCreatePaymentPromises()) {

                // цена, оплата (за обучение)
                final CtrContractVersionContractor paymentSource = this.getPaymentSource(student, eduPromise, customer, contractorMap);
                final Collection<CtrPriceElementCost> costList = this.getEducationPromiseCostList(student, eduPromise, paymentSource, defaultCurrency);
                if (costList.size() > 0)
                {
                    // если есть цены, формируем по каждой цене элемент
                    for (final CtrPriceElementCost cost: costList)
                    {
                        // формируем список платежей (ЗАЯВИТЕЛЬ обязуется ВУЗу оплатить сумму за обучение)
                        final List<CtrPriceElementCostStage> stages = this.getEducationPromiseCostListStages(student, eduPromise, cost);
                        for (final CtrPriceElementCostStage stage: stages) {
                            final long costAsLong = this.getEducationPromiseStageCostAsLong(student, eduPromise, stage);
                            if (costAsLong < 0) { continue; }

                            Date deadlineDate = stage.getDeadlineDate();
                            if (null == deadlineDate) { deadlineDate = eduPromise.getDeadlineDate(); }

                            final CtrPriceElementCostStage dbCostStage = this.findSuitableDBCostStage(student, eduPromise, stage);
                            final CtrPaymentPromice payPromise = new CtrPaymentPromice();
                            payPromise.setSourceStage(dbCostStage);
                            payPromise.setCurrency(cost.getCurrency());
                            payPromise.setDeadlineDate(deadlineDate);
                            payPromise.setSrc(paymentSource);
                            payPromise.setDst(provider);
                            payPromise.setStage(this.getEducationPromiseStageTitle(student, eduPromise, multiStudent, stage));
                            payPromise.setCostAsLong(costAsLong);

                            final String stageKey = StringUtils.trimToEmpty(this.getEducationPromiseStageName(student, eduPromise, multiStudent, stage));
                            paymentPromiseListMap.get(stageKey).add(payPromise);
                        }
                    }
                }
                else
                {
                    // если цены нет - формируем пустую строку без цены
                    final CtrPaymentPromice payPromise = new CtrPaymentPromice();
                    payPromise.setSourceStage(null);
                    payPromise.setCurrency(defaultCurrency);
                    payPromise.setDeadlineDate(eduPromise.getDeadlineDate());
                    payPromise.setSrc(paymentSource);
                    payPromise.setDst(provider);
                    payPromise.setStage(this.getEducationPromiseNoStageTitle(student, eduPromise, multiStudent));
                    payPromise.setCostAsLong(0L);

                    final String stageKey = StringUtils.trimToEmpty(this.getEducationPromiseNoStageName(student, eduPromise, multiStudent));
                    paymentPromiseListMap.get(stageKey).add(payPromise);
                }
            }
        }


        // если есть что оплачивать - приводим к красивому виду и сохраняем в договор
        // группируем по ключу, по плательщику, получателю, валюте, датам и источнику цены, в описание добавляем уникальный перечень названий этапов
        if (paymentPromiseListMap.size() > 0)
        {
            final Map<MultiKey, List<CtrPaymentPromice>> resultMap = SafeMap.get(ArrayList.class);
            for (final Map.Entry<String, List<CtrPaymentPromice>> e: paymentPromiseListMap.entrySet()) {
                for (final CtrPaymentPromice payPromise: e.getValue()) {
                    final MultiKey key = new MultiKey(new Object[] {
                        e.getKey(),
                        payPromise.getSrc(),
                        payPromise.getDst(),
                        payPromise.getCurrency(),
                        payPromise.getDeadlineDate(),
                        (null == payPromise.getSourceStage() ? Long.valueOf(0L) : payPromise.getSourceStage().getId())
                    }, false);
                    resultMap.get(key).add(payPromise);
                }
            }

            for (final List<CtrPaymentPromice> pList: resultMap.values()) {

                final Iterator<CtrPaymentPromice> pIterator = pList.iterator();
                CtrPaymentPromice pFirst = pIterator.next();

                final Set<String> stageTitleSet = new TreeSet<>();
                while (pIterator.hasNext()) {
                    final CtrPaymentPromice p = pIterator.next();
                    pFirst.setCostAsLong(pFirst.getCostAsLong() + p.getCostAsLong());
                    stageTitleSet.add(p.getStage());
                }
                stageTitleSet.add(pFirst.getStage());
                pFirst.setStage(StringUtils.join(stageTitleSet, "; "));

                session.save(pFirst = this.processPayPromise(pFirst));

                // @deprecated CtrPaymentGarantorPromice
                // if (this.isNeedGarantorPromise(contractVersion, pFirst.getSrc()) && !customer.equals(pFirst.getSrc())) {
                //     final CtrPaymentGarantorPromice garantor = new CtrPaymentGarantorPromice();
                //     garantor.setDeadlineDate(pFirst.getDeadlineDate());
                //     garantor.setSrc(customer);
                //     garantor.setDst(pFirst.getSrc());
                //     session.save(garantor);
                // }
            }
        }

        return contractVersion;
    }

    /** @return подходящий stage из базы */
    protected CtrPriceElementCostStage findSuitableDBCostStage(S student, EduCtrEducationAddPromise eduPromise, CtrPriceElementCostStage stage) {
        if (null == stage) { return null; }
        return DataAccessServices.dao().getByNaturalId(stage.getNaturalId());
    }

    /** @return true, если тберуется создавать обязательства о гарантировании платежей */
    @Deprecated
    protected boolean isNeedGarantorPromise(final CtrContractVersion contractVersion, final CtrContractVersionContractor paymentSource) {
        return false;
    }

    /** @return представители ОУ для договора (подразделения договора) */
    protected Collection<ContactorPerson> getAcademyPresenters(final CtrContractObject contractObject) {
        final Collection<ContactorPerson> collection = EduContractManager.instance().dao().getAcademyPresenters(contractObject.getOrgUnit());
        if (collection.isEmpty()) { throw new ApplicationException(EduProgramContractManager.instance().getProperty("error.edu-academy-presenter-not-configured", contractObject.getOrgUnit().getFullTitle())); }
        return collection;
    }

    /** @return выбирвет из списка представлителей ОУ первого */
    protected ContactorPerson getProvider(final Collection<ContactorPerson> academyPresenters) {
        return academyPresenters.iterator().next();
    }

    /** @return контрагент в договоре, который будет оплачивать обучение указанного студента (по умолчанию это заказчик) */
    protected CtrContractVersionContractor getPaymentSource(final S student, final EduCtrEducationAddPromise eduPromise, final CtrContractVersionContractor customer, final Map<ContactorPerson, CtrContractVersionContractor> contractorMap) {
        return customer;
    }

    /** @return измененный CtrPaymentPromise если это требуется */
    protected CtrPaymentPromice processPayPromise(final CtrPaymentPromice payPromise) {
        return payPromise;
    }

    /** @return измененный EduCtrEducationPromise если это требуется */
    protected EduCtrEducationAddPromise processEduPromise(final S student, final EduCtrEducationAddPromise eduPromise) {
        return eduPromise;
    }

    /** @return true, если требуется добавлять обязательства об оплате */
    protected boolean isCreatePaymentPromises()
    {
        return true;
    }



    protected String error_noStudents() {
        return EduProgramContractManager.instance().getProperty("error.edu-no-students");
    }

    protected String error_student_noListener(final S student) {
        return EduProgramContractManager.instance().getProperty("error.edu-student-no-listener", this.getStudentTitle(student));
    }

    protected String error_student_noEnrollmentDate(final S student) {
        return EduProgramContractManager.instance().getProperty("error.edu-student-no-enrollment-date", this.getStudentTitle(student));
    }

    protected String error_student_noEduProgram(final S student) {
        return EduProgramContractManager.instance().getProperty("error.edu-student-no-edu-program", this.getStudentTitle(student));
    }


}
