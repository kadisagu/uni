/* $Id$ */
package ru.tandemservice.unieductr.base.ext.CtrPrice.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.shared.ctr.base.bo.CtrPrice.util.ICtrPriceElementCostStageFactory;
import org.tandemframework.shared.ctr.base.entity.ICtrPriceElement;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;

import ru.tandemservice.unieductr.base.entity.EduProgramPrice;

/**
 * @author Dmitry Seleznev
 * @since 18.10.2011
 */
public class EduProgramElementCostStageFactoryTerm implements ICtrPriceElementCostStageFactory
{
    @Override
    public List<CtrPriceElementCostStage> getStageList(ICtrPriceElement priceElement)
    {
        final Map<Integer, Integer> stageCodeSource;
        if (priceElement instanceof EduProgramPrice) {
            stageCodeSource = getStageCodeSource((EduProgramPrice) priceElement);
        } else {
            return new ArrayList<>(0);
        }

        List<CtrPriceElementCostStage> stagesList = new ArrayList<>();
        for (Map.Entry<Integer, Integer> stageCodeEntry : stageCodeSource.entrySet())
        {
            CtrPriceElementCostStage stage = new CtrPriceElementCostStage();
            stage.setTitle("За " + stageCodeEntry.getKey() + " семестр (" + stageCodeEntry.getValue() + " курс)");
            stage.setStageUniqueCode(getStageUniqueCode(stageCodeEntry));
            stagesList.add(stage);
        }

        return stagesList;

    }

    @Override
    public List<CtrPriceElementCostStage> filterStageList(ICtrPriceElement priceElement, List<CtrPriceElementCostStage> stageList, Map<String, Object> filter)
    {
        return stageList;
    }

    private String getStageUniqueCode(Map.Entry<Integer, Integer> stageCodeEntry)
    {
        return String.valueOf(stageCodeEntry.getKey());
    }


    // term.number -> course
    public static Map<Integer, Integer> getStageCodeSource(EduProgramPrice priceElement)
    {
        int courses = priceElement.getProgram().getDuration().getNumberOfYears();

        Map<Integer, Integer> map = new HashMap<>();
        int term = 1;
        for(int i = 0; i < courses; i++) {
            for(int t=0;t<2;t++) {
                map.put(term++, i+1);
            }
        }

        // если есть месяцы, то добавляем еще один курс и семестр (два семестра, если меясцев больше 6)
        if(priceElement.getProgram().getDuration().getNumberOfMonths() > 0)
        {
            map.put(term++, ++courses);
            if (priceElement.getProgram().getDuration().getNumberOfMonths() > 6)
                map.put(term, courses);
        }
        return map;
    }
}