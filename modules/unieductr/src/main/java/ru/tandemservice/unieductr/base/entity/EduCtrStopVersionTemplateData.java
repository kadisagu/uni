package ru.tandemservice.unieductr.base.entity;

import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import ru.tandemservice.unieductr.base.entity.gen.*;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStopContractTemplate.EduCtrStopContractTemplateManager;

/**
 * Данные шаблона версии о расторжении договора на обучение
 *
 * Расторжение договора на обучение.
 */
public class EduCtrStopVersionTemplateData extends EduCtrStopVersionTemplateDataGen
{
    @Override
    public ICtrContractTemplateManager getManager()
    {
        return EduCtrStopContractTemplateManager.instance();
    }

    public Double getRefund() { return Currency.wrap(this.getRefundAsLong()); }

    public void setRefund(Double refund) { this.setRefundAsLong(Currency.unwrap(refund)); }
}