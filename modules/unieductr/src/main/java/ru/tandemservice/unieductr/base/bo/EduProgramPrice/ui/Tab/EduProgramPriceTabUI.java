/* $Id$ */
package ru.tandemservice.unieductr.base.bo.EduProgramPrice.ui.Tab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.activator.TopRegionActivation;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.ui.ElementCostList.CtrPriceElementCostListUI;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.unieductr.base.bo.EduProgramPrice.EduProgramPriceManager;
import ru.tandemservice.unieductr.base.bo.EduProgramPrice.ui.CipherEdit.EduProgramPriceCipherEdit;
import ru.tandemservice.unieductr.base.entity.EduProgramPrice;

import java.util.Map;

/**
 * @author nvankov
 * @since 2/26/14
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id", required = true),
    @Bind(key = EduProgramPriceTabUI.PERMISSION_KEY_POSTFIX, binding = "permissionKeyPostfix", required = true)
})
@TopRegionActivation
public class EduProgramPriceTabUI extends UIPresenter
{
    public static final String PERMISSION_KEY_POSTFIX = CtrPriceElementCostListUI.PERMISSION_KEY_POSTFIX;

    private EntityHolder<EduProgram> _holder = new EntityHolder<>();
    public EntityHolder<EduProgram> getHolder() { return _holder; }
    public EduProgram getEduProgram() { return _holder.getValue(); }

    private String permissionKeyPostfix;
    public String getPermissionKeyPostfix() { return this.permissionKeyPostfix; }
    public void setPermissionKeyPostfix(String permissionKeyPostfix) { this.permissionKeyPostfix = permissionKeyPostfix; }

    private CommonPostfixPermissionModel secModel;
    public CommonPostfixPermissionModel getSecModel() { return this.secModel; }
    protected void setSecModel(CommonPostfixPermissionModel secModel) { this.secModel = secModel; }

    private EduProgramPrice eduProgramPrice;
    public EduProgramPrice getEduProgramPrice() { return this.eduProgramPrice; }
    protected void setEduProgramPrice(EduProgramPrice eduProgramPrice) { this.eduProgramPrice = eduProgramPrice; }

    public IEntity getSecurityObject() { return getEduProgramPrice(); }

    public Map<String, Object> getCtrPriceElementCostListParameters() {
        return new ParametersMap()
        .add(CtrPriceElementCostListUI.PRICE_ELEMENT_ID, getEduProgramPrice().getId())
        .add(CtrPriceElementCostListUI.SECURITY_OBJECT_ID, getSecurityObject().getId())
        .add(CtrPriceElementCostListUI.PERMISSION_KEY_POSTFIX, getPermissionKeyPostfix());
    }

    @Override
    public void onComponentRefresh() {
        EduProgram eduProgram = getHolder().refresh(EduProgram.class);
        setEduProgramPrice(EduProgramPriceManager.instance().dao().doGetEduProgramPrice(eduProgram.getId()));
        setSecModel(new CommonPostfixPermissionModel(this.getPermissionKeyPostfix()));
    }

    public void onClickEditCipher() {
        this.getActivationBuilder().asRegionDialog(EduProgramPriceCipherEdit.class).parameter(PUBLISHER_ID, getEduProgramPrice().getId()).activate();
        //FormContainerFacade.asForm(this, EduProgramPriceCipherEdit.class).parameter(PUBLISHER_ID, getEduProgramPrice().getId()).activate();
    }

}
