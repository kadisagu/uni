package ru.tandemservice.unieductr.base.entity;

import ru.tandemservice.uniedu.program.entity.EduProgramAdditional;
import ru.tandemservice.unieductr.base.entity.gen.*;

/** @see ru.tandemservice.unieductr.base.entity.gen.EduCtrEducationAddPromiseGen */
public class EduCtrEducationAddPromise extends EduCtrEducationAddPromiseGen implements IEducationPromise<EduProgramAdditional>
{
    @Override
    public EduProgramAdditional getEducationPromiseTarget()
    {
        return getEduProgram();
    }

    @Override
    public String getEducationPromiseDescription()
    {
        return getEduProgram().getFullTitle();
    }
}