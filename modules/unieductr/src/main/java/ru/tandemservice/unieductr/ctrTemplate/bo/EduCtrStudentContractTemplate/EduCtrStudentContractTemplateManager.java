package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.addon.DefaultAddonBuilder;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtPointBuilder;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.logic.EduCtrStudentContractTemplateDAO;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.logic.IEduCtrStudentContractTemplateDAO;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.ui.AddVpo.PriceTemplate.PriceTemplateAddon;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.ui.Edit.EduCtrStudentContractTemplateEdit;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.ui.Pub.EduCtrStudentContractTemplatePub;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContractTemplateData;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
@Configuration
public class EduCtrStudentContractTemplateManager extends BusinessObjectManager implements ICtrContractTemplateManager {

    public static EduCtrStudentContractTemplateManager instance() {
        return instance(EduCtrStudentContractTemplateManager.class);
    }

    @Override
    public Class<? extends CtrContractVersionTemplateData> getDataEntityClass() {
        return EduCtrStudentContractTemplateData.class;
    }

    @Override
    public Class<? extends BusinessComponentManager> getDataDisplayComponent() {
        return EduCtrStudentContractTemplatePub.class;
    }

    @Override
    public Class<? extends BusinessComponentManager> getDataEditComponent() {
        return EduCtrStudentContractTemplateEdit.class;
    }

    @Bean
    @Override
    public IEduCtrStudentContractTemplateDAO dao() {
        return new EduCtrStudentContractTemplateDAO();
    }

    @Override
    public boolean isAllowEditInWizard() {
        return false;
    }


    public static final String PARAM_PROGRAM = "program";
    public static final String PARAM_PROGRAM_YEAR = "programYear";
    public static final String PARAM_PROGRAM_KIND = "programKind";
    public static final String PARAM_PROGRAM_FORM = "programForm";
    public static final String PARAM_PROGRAM_SUBJECT = "programSubject";

    public static final String DS_PROGRAM_SUBJECT = "programSubjectDS";
    public static final String DS_PROGRAM = "programDS";
    public static final String DS_PROGRAM_KIND = "programKindDS";
    public static final String DS_START_EDU_YEAR = "startEduYearDS";
    public static final String DS_PROGRAM_FORM = "programFormDS";

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> programDSHandler() {
        return new EntityComboDataSourceHandler(getName(), EduProgramProf.class)
        .where(EduProgramProf.programSubject(), PARAM_PROGRAM_SUBJECT)
        .where(EduProgramProf.year(), PARAM_PROGRAM_YEAR)
        .where(EduProgramProf.form(), PARAM_PROGRAM_FORM)
        .order(EduProgramProf.title())
        .filter(EduProgramProf.title())
        .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler programKindDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramKind.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                dql.where(eq(property(alias, EduProgramKind.programHigherProf()), value(true)))
                        .where(exists(EduProgram.class, EduProgram.kind().s(), property(alias)));
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler startEduYearDSHandler()
    {
        return EducationYear.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) ->
                {
                    final EduProgramKind programKind = context.get(PARAM_PROGRAM_KIND);
                    final EduProgramSubject programSubject = context.get(PARAM_PROGRAM_SUBJECT);


                    if (programKind == null || programSubject == null) {
                        dql.where(nothing());
                    }
                    else {
                        DQLSelectBuilder eduProgramSubSelect = new DQLSelectBuilder().fromEntity(EduProgramHigherProf.class, "ep")
                                .where(eq(property("ep", EduProgramHigherProf.year()), property(alias)))
                                .where(eq(property("ep", EduProgramHigherProf.kind().id()), value(programKind.getId())))
                                .where(eq(property("ep", EduProgramHigherProf.programSubject().id()), value(programSubject.getId())));

                        dql.where(exists(eduProgramSubSelect.buildQuery()));
                    }

                    return dql;
                });
    }

    @Bean
    public IDefaultComboDataSourceHandler programFormDSHandler()
    {
        return EduProgramForm.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) -> {
                    final EducationYear startYear = context.get(PARAM_PROGRAM_YEAR);
                    final EduProgramKind programKind = context.get(PARAM_PROGRAM_KIND);
                    final EduProgramSubject programSubject = context.get(PARAM_PROGRAM_SUBJECT);

                    if (startYear == null || programKind == null || programSubject == null) {
                        dql.where(nothing());
                    }
                    else {
                        DQLSelectBuilder formConditionBuilder = new DQLSelectBuilder().fromEntity(EduProgramHigherProf.class, "ep")
                                .where(eq(property("ep", EduProgramHigherProf.year()), value(startYear)))
                                .where(eq(property("ep", EduProgramHigherProf.kind()), value(programKind)))
                                .where(eq(property("ep", EduProgramHigherProf.programSubject()), value(programSubject)))
                                .column(property("ep", EduProgramHigherProf.form())).distinct();

                        dql.where(in(property(alias), formConditionBuilder.buildQuery()));
                    }
                    return dql;
                });
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                final EduProgramKind programKind = context.get(PARAM_PROGRAM_KIND);
                if (programKind != null)
                    dql.where(eq(property(alias, EduProgramSubject.subjectIndex().programKind()), value(programKind)));
                else
                    dql.where(nothing());

                dql.where(exists(EduProgramProf.class, EduProgramProf.programSubject().s(), property(alias)));

            }
        }.order(EduProgramSubject.subjectCode())
                .order(EduProgramSubject.title())
                .filter(EduProgramSubject.subjectCode())
                .filter(EduProgramSubject.title())
                .pageable(true);
    }

    /* Селекты формы добавления договора, доп. соглашения с выбором ОП и этапов оплаты для автоматического формирования обязательств. */
    public static IPresenterExtPointBuilder programProfPresenterExtPointBuilder(IPresenterExtPointBuilder presenterExtPointBuilder, String name)
    {
        return presenterExtPointBuilder
                .addDataSource(EducationCatalogsManager.instance().eduYearDSConfig())
                .addDataSource(SelectDSConfig.with(DS_START_EDU_YEAR, name).dataSourceClass(SelectDataSource.class).handler(instance().startEduYearDSHandler()))
                .addDataSource(SelectDSConfig.with(DS_PROGRAM_KIND, name).dataSourceClass(SelectDataSource.class).handler(instance().programKindDSHandler()))
                .addDataSource(SelectDSConfig.with(DS_PROGRAM_FORM, name).dataSourceClass(SelectDataSource.class).handler(instance().programFormDSHandler()))
                .addDataSource(SelectDSConfig.with(DS_PROGRAM_SUBJECT, name).dataSourceClass(SelectDataSource.class).handler(instance().programSubjectDSHandler())
                .addColumn(EduProgramSubject.titleWithCode().s()).addColumn(EduProgramSubject.subjectIndex().title().s()))
                .addDataSource(SelectDSConfig.with(DS_PROGRAM, name).dataSourceClass(SelectDataSource.class).handler(instance().programDSHandler()).addColumn(EduProgramProf.titleAndConditionsShortWithForm().s()))
                .addAddon(new DefaultAddonBuilder(PriceTemplateAddon.PRICE_TEMPLATE_ADDON, name, PriceTemplateAddon.class));
    }
}
