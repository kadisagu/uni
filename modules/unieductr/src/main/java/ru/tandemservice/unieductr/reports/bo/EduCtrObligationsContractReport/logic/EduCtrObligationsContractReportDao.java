/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrObligationsContractReport.logic;

import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import jxl.JXLException;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.Number;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.catalog.entity.gen.CurrencyGen;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContractResultDao;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contract.*;
import org.tandemframework.shared.ctr.base.entity.contract.gen.CtrContractPromiceGen;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduContractPaymentPromiceResultPair;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;
import ru.tandemservice.unieductr.base.entity.gen.IEduContractRelationGen;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.unieductr.reports.entity.EduCtrObligationsContractReport;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Boolean;
import java.text.NumberFormat;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon.Filters.*;

/**
 * @author Ekaterina Zvereva
 * @since 19.08.2016
 */
public class EduCtrObligationsContractReportDao extends UniBaseDao implements IEduCtrObligationsContractReportDao
{

    @Override
    public <M extends Model> byte[] createReportXlsDocument(M model)
    {
        List<MultiKey> dataList = getPrepareDataFromDataBase(model);
        if (model.getStudentCount() == 0)
            throw new ApplicationException("Невозможно построить отчет, т.к. для выбранных параметров не найдены обязательства по оплате в договорах на обучение со студентами.");

        //поднимем обязательства, которые должны быть в отчете, согласно поднятой истории
        Map<Long, CtrPaymentPromice> paymentPromiceMap = new LinkedHashMap<>();
        Set<Long> promiceIds = new HashSet<>();
        dataList.stream().forEach(e->
                      promiceIds.addAll(((Collection<ICtrContractResultDao.ICtrContractStatusItem>) e.getKey(2)).stream().map(ICtrContractResultDao.ICtrContractStatusItem::getPromiceId).collect(Collectors.toList())));
        BatchUtils.execute(promiceIds, DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            paymentPromiceMap.putAll(CommonDAO.map(getList(CtrPaymentPromice.class, elements)));
        });

        try
        {
            // документ
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            WritableWorkbook workbook = Workbook.createWorkbook(out);
            WritableSheet sheet = workbook.createSheet("ReportSheet", 0);

            WritableFont headerFont = new WritableFont(WritableFont.createFont("Calibri"), 11, WritableFont.BOLD);
            WritableCellFormat headerFormat = new WritableCellFormat(headerFont);

            //печать данных фильтров
            int resultDataIndex = printFilterInfo(model, sheet);

            //печатает шапку таблицы
            int  currentIndex = printTableHeader(sheet, resultDataIndex + 4);
            //печатает содержимое таблицы
            currentIndex = printTableContent(sheet, dataList, paymentPromiceMap, model, currentIndex);

            printSummaryInfo(sheet, model, resultDataIndex);

            List<Integer> widths = new ArrayList<>();
            widths.add(5);
            widths.add(14);
            widths.add(12);
            widths.add(5);
            widths.add(10);
            widths.add(12);
            widths.add(17);
            widths.add(15);
            widths.add(22);
            widths.add(15);
            widths.add(14);
            widths.add(15);
            widths.add(15);
            widths.add(15);
            widths.add(15);


            setWidths(widths, sheet);

            //Закрываем потоки
            workbook.write();
            workbook.close();
            byte[] bytes = out.toByteArray();
            out.close();

            return bytes;
        }
        catch (JXLException | IOException e)
        {
            throw new RuntimeException(e);
        }
    }



    @Override
    public <M extends Model> EduCtrObligationsContractReport saveReport(M model, byte[] document)
    {
        EduCtrObligationsContractReport report = new EduCtrObligationsContractReport();

        //сохраняем сформированный файл
        DatabaseFile content = new DatabaseFile();
        content.setContent(document);
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        content.setFilename("obligationCtrReport_" + new DateFormatter("dd_MM_yyyy").format(new Date()) + ".xls");
        save(content);

        UniEduProgramEducationOrgUnitAddon util = model.getUtil();

        if (model.getOrgUnitId() != null)
            report.setOrgUnit(get(OrgUnit.class, model.getOrgUnitId()));
        report.setContent(content);
        report.setFormingDate(model.getFormingDate());
        report.setReportDate(model.getReportDate());
        report.setCourse(model.isActiveCourse() ? model.getCourse().getTitle() : "");
        report.setGroup(model.isActiveGroup() ? CommonBaseStringUtil.join(model.getGroupList(), Group.title(), ", ") : "");
        report.setStudentStatus(model.isActiveStatus()? CommonBaseStringUtil.join(model.getStateList(), StudentStatus.title(), ", ") : "");
        report.setFormativeOrgUnit(getValue(util, FORMATIVE_ORG_UNIT, OrgUnit.P_FULL_TITLE));
        report.setTerritorialOrgUnit(getValue(util, TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_FULL_TITLE));
        report.setEducationLevelHighSchool(getValue(util, EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.P_DISPLAYABLE_TITLE));
        report.setComment(model.getComment() != null? model.getComment() : "");
        report.setPeriodDateFrom(model.getDeadlineDateFrom());
        report.setPeriodDateTo(model.getDeadlineDateTo());
        report.setDevelopForm(getValue(util, DEVELOP_FORM, DevelopForm.P_TITLE));
        report.setDevelopCondition(getValue(util, DEVELOP_CONDITION, DevelopCondition.P_TITLE));
        report.setDevelopTech(getValue(util, DEVELOP_TECH, DevelopTech.P_TITLE));
        report.setDevelopPeriod(getValue(util, DEVELOP_PERIOD, DevelopPeriod.P_TITLE));

        save(report);
        return report;
    }

    @SuppressWarnings("unchecked")
    private String getValue(UniEduProgramEducationOrgUnitAddon util, UniEduProgramEducationOrgUnitAddon.Filters key, String propertyPath)
    {
        Map<UniEduProgramEducationOrgUnitAddon.Filters, Object> values = util.getValuesMap();
        return util.getFilterConfig(key).isCheckEnableCheckBox() ? UniStringUtils.join((Collection<IEntity>) values.get(key), propertyPath, ", ") : "";
    }

    /**
     * Подготавливает массив данных для отчета. Применяются фильтры с формы.
     *
     * @return {Student, Contract, debt, currencyCode}
     */
    @SuppressWarnings("unchecked")
    protected List<MultiKey> getPrepareDataFromDataBase(Model model)
    {
        // получаем список id студентов, удовлетворяющих фильтрам на форме и подразделению, если отчет строится с подразделения
        final List<Long> studentIds = new ArrayList<>();
        final DQLSelectBuilder studentBuilder = new DQLSelectBuilder().fromEntity(Student.class, "stud").column(property("stud"))
                .joinEntity("stud", DQLJoinType.left, Group.class, "gr", eq(property("gr"), property("stud", Student.group())))
                .where(eq(property(Student.archival().fromAlias("stud")), value(Boolean.FALSE)))
                .where(eq(property(Student.status().active().fromAlias("stud")), value(Boolean.TRUE)))
                .where(or(
                        eq(property("gr", Group.archival()), value(Boolean.FALSE)),
                        isNull(property("gr.id")))
                );

        //применяем подразделение
        if (model.getOrgUnitId() != null)
        {
            studentBuilder.where(or(
                    eq(property(Student.educationOrgUnit().formativeOrgUnit().id().fromAlias("stud")), value(model.getOrgUnitId())),
                    eq(property(Student.educationOrgUnit().territorialOrgUnit().id().fromAlias("stud")), value(model.getOrgUnitId()))
            ));
        }

        //статусы студента
        if (model.isActiveStatus() && model.getStateList() != null)
            studentBuilder.where(in(property(Student.status().fromAlias("stud")), model.getStateList()));

        //Группа
        if (model.isActiveGroup() && model.getGroupList() != null)
            studentBuilder.where(in(property(Student.group().fromAlias("stud")), model.getGroupList()));

        //Курс
        if (model.isActiveCourse() && model.getCourse() != null)
            studentBuilder.where(eq(property(Student.course().fromAlias("stud")), value(model.getCourse())));


        BatchUtils.execute(model.getUtil().getEducationOrgUnitFilteredList(), 128,
                           elements -> studentIds.addAll(new DQLSelectBuilder().fromDataSource(studentBuilder.buildQuery(), "b")
                                                                 .column(property(Student.id().fromAlias("b.stud")))
                                                                 .where(in(property(Student.educationOrgUnit().fromAlias("b.stud")), elements))
                                                                 .createStatement(getSession()).<Long>list())
        );

        // получаем список id договоров, для которых есть EduCtrStudentContract из списка студентов полученых выше
        Map<Long, Long> contractStudentIdMap = getStudentContractRel(studentIds);

        // получаем актуальные на дату отчета версии договоров, договоров id которых получены выше
        Map<Long, CtrContractVersion> contractVersionMap = CtrContractVersionManager.instance().dao().getCurrentVersions(contractStudentIdMap.keySet(), model.getReportDate());

        //мапа валют
        List<Currency> currencyList = getList(Currency.class);
        Map<String, Currency> currencyMap = currencyList.stream().collect(Collectors.toMap(CurrencyGen::getCode, e->e));
        model.setCurrencyMap(currencyMap);

        //история по договорам
        Map<Long, Map<String, ICtrContractResultDao.ICtrContractStatus>> contractVersionStatusMap = CtrContractVersionManager.instance().results().getContractVersionStatusMap(ids(contractVersionMap.values()), model.getDeadlineDateTo());

        //мапа элементов истории по версиям договоров, валюте
        Map<CoreCollectionUtils.Pair<Long, String>, List<ICtrContractResultDao.ICtrContractStatusItem>> historyMap = new HashMap<>();
        //сохраняем мапу последних элементов истории - из них будем брать окончательную сумму
        Map<CoreCollectionUtils.Pair<Long, String>, ICtrContractResultDao.ICtrContractStatusItem> lastHistoryElementMap = new HashMap<>();
        for (Map.Entry<Long, Map<String, ICtrContractResultDao.ICtrContractStatus>> versionEntry : contractVersionStatusMap.entrySet())
        {
            for (Map.Entry<String, ICtrContractResultDao.ICtrContractStatus> keyVersionStatusEntry : versionEntry.getValue().entrySet())
            {
                for (Currency currency : currencyList)
                {
                    if (EduContractPaymentPromiceResultPair.payDirectedTo(keyVersionStatusEntry.getKey(), TopOrgUnit.getInstance().getId(), currency.getCode()))
                    {
                        //последний элемент истории, попавший в выбранный период
                        ICtrContractResultDao.ICtrContractStatusItem last = Iterables.getLast(keyVersionStatusEntry.getValue().getList());
                        //список элементов истории (обязательства) за период
                        List<ICtrContractResultDao.ICtrContractStatusItem> list = keyVersionStatusEntry.getValue().getList().stream()
                                .filter(e->(e.getTimestamp() >= model.getDeadlineDateFrom().getTime() && e.isPromice())).collect(Collectors.toList());
                        historyMap.put(new CoreCollectionUtils.Pair<>(versionEntry.getKey(), currency.getCode()), list);

                        lastHistoryElementMap.put(new CoreCollectionUtils.Pair<>(versionEntry.getKey(), currency.getCode()), last);
                    }
                }
            }
        }

        //мапа первых версий договоров
        final Map<Long, CtrContractVersion> contractIdFirstVersionMap = new HashMap<>();

        final Map<Long, CtrContractObject> versionContractId = new HashMap<>();
        BatchUtils.execute(historyMap.keySet(), 512, elements -> {
            List<CtrContractVersion> list = getList(CtrContractVersion.class, elements.stream().map(CoreCollectionUtils.Pair::getX).collect(Collectors.toList()));

            for (CtrContractVersion version : list)
            {
                versionContractId.put(version.getId(), version.getContract());

                CtrContractVersion contractVersion = contractIdFirstVersionMap.get(version.getContract().getId());
                if (contractVersion != null)
                {
                    if (version.getActivationDate().getTime() < contractVersion.getActivationDate().getTime())
                        contractIdFirstVersionMap.put(version.getContract().getId(), version);
                }
                else
                {
                    contractIdFirstVersionMap.put(version.getContract().getId(), version);
                }
            }
        });

        model.setContractIdFirstVersionMap(contractIdFirstVersionMap);

        Set<Long> resultStudentIdList = new HashSet<>();
        for (CoreCollectionUtils.Pair<Long, String> versionId : historyMap.keySet())
        {
            resultStudentIdList.add(contractStudentIdMap.get(versionContractId.get(versionId.getX()).getId()));
        }

        //студенты
        final Map<Long, Student> studentMap = new HashMap<>();
        BatchUtils.execute(resultStudentIdList, 512, elements -> {
            List<Student> list = new DQLSelectBuilder().fromEntity(Student.class, "s").column(property("s"))
                    .where(in(property(Student.id().fromAlias("s")), elements))
                    .createStatement(getSession()).<Student>list();

            for (Student student : list)
                studentMap.put(student.getId(), student);
        });



        List<MultiKey> resultList = new ArrayList<>();
        for (CoreCollectionUtils.Pair<Long, String> versionId : historyMap.keySet())
        {
            Student student = studentMap.get(contractStudentIdMap.get(versionContractId.get(versionId.getX()).getId()));
            CtrContractObject contract = versionContractId.get(versionId.getX());
            Collection<ICtrContractResultDao.ICtrContractStatusItem> historyList = historyMap.get(versionId);
            String currencyCode = versionId.getY();
            ICtrContractResultDao.ICtrContractStatusItem lastItem = lastHistoryElementMap.get(versionId);

            resultList.add(new MultiKey(student, contract, historyList, currencyCode, lastItem));
        }

        Collections.sort(resultList, (o1, o2) -> {
            Student student1 = (Student) o1.getKey(0);
            Student student2 = (Student) o2.getKey(0);

            int result = CommonCollator.RUSSIAN_COLLATOR.compare(student1.getFullFio(), student2.getFullFio());

            /**
             * Сортировка строк по объекту "студент" (сами объекты сортировать по ФИО студента), затем по номеру договора,
             */
            CtrContractObject contract1 = (CtrContractObject) o1.getKey(1);
            CtrContractObject contract2 = (CtrContractObject) o2.getKey(1);

            if (result == 0)
                result = contract1.getNumber().compareTo(contract2.getNumber());

            if (result == 0)
            {
                String currency1 = (String) o1.getKey(3);
                String currency2 = (String) o2.getKey(3);
                result = currency1.compareTo(currency2);
            }


            return result;
        });
        model.setStudentCount(resultList.stream().filter(e->!((Collection)e.getKey(2)).isEmpty()).map(e->((Student)e.getKey(0)).getId()).collect(Collectors.toSet()).size());
        model.setContractCount(resultList.stream().filter(e->!((Collection)e.getKey(2)).isEmpty()).map(e->((CtrContractObject)e.getKey(1)).getId()).collect(Collectors.toSet()).size());

        return resultList;
    }

    @Override
    public Map<Long, Long> getStudentContractRel(List<Long> studentIds)
    {
        final Map<Long, Long> contractStudentIdMap = Maps.newHashMap();

        BatchUtils.execute(studentIds, 512, elements -> {
            DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                    new DQLSelectBuilder().fromEntity(EduCtrStudentContract.class, "sc")
                            .joinEntity("sc", DQLJoinType.inner, IEduContractRelation.class, "ie", eq(property("sc", EduCtrStudentContract.contractObject().id()), property("ie", IEduContractRelationGen.contractObject().id())))
                            .where(in(property("sc", EduCtrStudentContract.student().id()), elements))
            );
            int contract_id_col = dql.column(property("sc", EduCtrStudentContract.contractObject().id()));
            int student_id_col = dql.column(property("sc", EduCtrStudentContract.student().id()));

            for (Object[] row : dql.getDql().createStatement(getSession()).<Object[]>list())
            {
                contractStudentIdMap.put((Long) row[contract_id_col], (Long) row[student_id_col]);
            }
        });
        return contractStudentIdMap;
    }



    protected int printFilterInfo(Model model, WritableSheet sheet) throws JXLException
    {
        int lastUsedRow = 0;
        WritableFont labelFont = new WritableFont(WritableFont.createFont("Calibri"), 14, WritableFont.BOLD);
        WritableFont headerFont = new WritableFont(WritableFont.createFont("Calibri"), 11, WritableFont.BOLD);
        WritableFont contentFont = new WritableFont(WritableFont.createFont("Calibri"), 11);


        WritableCellFormat labelFormat = new WritableCellFormat(labelFont);
        WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);

        List<String[]> hTable = new ArrayList<>();
        hTable.add(new String[] {"Название отчета:", "Обязательства по договорам студентов"});
        hTable.add(new String[] {"Дата построения:", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getFormingDate())});
        hTable.add(new String[] {"Дата отчета:", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getReportDate())});
        hTable.add(new String[] {"Период исполнения обязательств с:", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getDeadlineDateFrom())});
        hTable.add(new String[] {"Период исполнения обязательств по:", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getDeadlineDateTo())});

        if (model.isActiveCourse())
            hTable.add(new String[] {"Курс:", model.getCourse().getTitle()});
        if (model.isActiveGroup())
            hTable.add(new String[] {"Группа:", CommonBaseStringUtil.join(model.getGroupList(), Group.title(), ", ")});
        if (model.isActiveStatus())
            hTable.add(new String[] {"Состояние студентов:", CommonBaseStringUtil.join(model.getStateList(), StudentStatus.title(), ", ")});


        for (UniEduProgramEducationOrgUnitAddon.Filters property : UniEduProgramEducationOrgUnitAddon.Filters.BASE_FILTER_SET)
        {

            String value = getValue(model.getUtil(), property, "title");
            if (!StringUtils.isEmpty(value))
                hTable.add(new String[] {property.getFieldName(), value});
        }

        if (!StringUtils.isEmpty(model.getComment()))
            hTable.add(new String[] {"Комментарий", model.getComment()});

        sheet.addCell(new Label(1, 0, "«Обязательства по договорам студентов»", labelFormat));

        for (int i = 0; i < hTable.size(); i++)
        {
            sheet.addCell(new Label(1, i + 1, hTable.get(i)[0], headerFormat));
            sheet.addCell(new Label(6, i + 1, hTable.get(i)[1], contentFormat));
            lastUsedRow = i + 1;
        }
        return lastUsedRow + 2;
    }

    protected int printTableHeader(WritableSheet sheet, int rowIndex) throws JXLException
    {
        WritableFont headerFont = new WritableFont(WritableFont.createFont("Calibri"), 10, WritableFont.BOLD);
        WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
        headerFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
        headerFormat.setAlignment(jxl.format.Alignment.CENTRE);
        headerFormat.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        headerFormat.setWrap(true);

        List<String> columns = new ArrayList<>();

        columns.add("№ п/п");
        columns.add("Ф.И.О");
        columns.add("Личный номер");
        columns.add("Курс");
        columns.add("Группа");
        columns.add("Номер договора");
        columns.add("Договор от");
        columns.add("Название этапа обязательства по оплате");
        columns.add("Оплатить до");
        columns.add("Размер обязательства");
        columns.add("Уже оплачено");
        columns.add("Валюта");
        columns.add("Телефон");
        columns.add("Состояние студента");
        columns.add("Год приема");
        columns.add("Формирующее подр.");
        columns.add("Территориальное подр.");
        columns.add("Направление подготовки (специальность)");
        columns.add("Форма освоения");
        columns.add("Условие освоения");
        columns.add("Технология освоения");
        columns.add("Нормативный срок освоения");


        for (int i = 0; i < columns.size(); i++)
        {
            sheet.addCell(new Label(i, rowIndex, columns.get(i), headerFormat));
        }

        return rowIndex + 1;
    }

   protected int printTableContent(WritableSheet sheet, List<MultiKey> dataList, Map<Long, CtrPaymentPromice> promiceMap, Model model, int currentIndex) throws JXLException
    {
        WritableFont contentFont = new WritableFont(WritableFont.createFont("Calibri"), 10);
        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);
        contentFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        contentFormat.setAlignment(Alignment.LEFT);
        contentFormat.setVerticalAlignment(VerticalAlignment.TOP);
        contentFormat.setWrap(true);
        int number = 0, index = 1;
        List<String[]> lineList = new ArrayList<>();

        Map<String, Long> sumPromiceMap = new HashMap<>();
        Map<String, Long> sumResultMap = new HashMap<>();

        for (MultiKey multiKey : dataList)
        {
            Student student = (Student) multiKey.getKey(0);
            CtrContractObject contract = (CtrContractObject) multiKey.getKey(1);
            List<ICtrContractResultDao.ICtrContractStatusItem> history = (List<ICtrContractResultDao.ICtrContractStatusItem>) multiKey.getKey(2);

            Map<Long, ICtrContractResultDao.ICtrContractStatusItem> historyToPromiceMap = history.stream().collect(Collectors.toMap(ICtrContractResultDao.ICtrContractStatusItem::getPromiceId, e->e));
            String currencyCode = (String) multiKey.getKey(3);
            ICtrContractResultDao.ICtrContractStatusItem lastItem = (ICtrContractResultDao.ICtrContractStatusItem) multiKey.getKey(4);
            long sumResult = 0, sumPromice=0;

            NumberFormat numberFormat = NumberFormat.getInstance();
            numberFormat.setMinimumFractionDigits(2);
            List<CtrPaymentPromice> promiceList = promiceMap.entrySet().stream().filter(e->historyToPromiceMap.keySet().contains(e.getKey())).map(Map.Entry::getValue).collect(Collectors.toList());
            Collections.sort(promiceList, Comparator.comparing(e->e.getDeadlineDate()));

            for (CtrPaymentPromice promice : promiceList)
            {
                String[] line = new String[22];

                line[0] = String.valueOf(index++);
                line[1] = student.getPerson().getFullFio();
                line[2] = student.getPersonalNumber();
                line[3] = student.getCourse().getTitle();
                line[4] = student.getGroup() != null ? student.getGroup().getTitle() : "";
                line[5] = "№" + contract.getNumber();
                line[6] = DateFormatter.DEFAULT_DATE_FORMATTER.format(contract.getDocStartDate());

                line[7] = promice.getStage();
                line[8] = DateFormatter.DEFAULT_DATE_FORMATTER.format(promice.getDeadlineDate());
                long promiceValue = promice.getSrc().getContactor() instanceof EmployeePostContactor ? -1 * promice.getCostAsLong() : promice.getCostAsLong();
                line[9] = numberFormat.format(Currency.wrap(promiceValue));

                long diff = lastItem.getResultAmount() - historyToPromiceMap.get(promice.getId()).getPromiceAmount();
                long sum = diff >= 0 ? promiceValue : (diff + promiceValue > 0 ? promiceValue + diff : 0);
                sumResult += sum;
                sumPromice += promiceValue;

                line[10] = numberFormat.format(Currency.wrap(sum));
                line[11] = model.getCurrencyMap().get(currencyCode).getTitle();
                line[12] = student.getPerson().getContactData().getContactPhones();
                line[13] = student.getStatus().getTitle();
                line[14] = String.valueOf(student.getEntranceYear());
                line[15] = student.getEducationOrgUnit().getFormativeOrgUnit().getTitle();
                line[16] = student.getEducationOrgUnit().getTerritorialOrgUnit().getTitle();
                line[17] = student.getEducationOrgUnit().getEducationLevelHighSchool().getFullTitle();
                line[18] = student.getEducationOrgUnit().getDevelopForm().getTitle();
                line[19] = student.getEducationOrgUnit().getDevelopCondition().getTitle();
                line[20] = student.getEducationOrgUnit().getDevelopTech().getTitle();
                line[21] = student.getEducationOrgUnit().getDevelopPeriod().getTitle();

                lineList.add(line);
            }
            sumPromiceMap.put(currencyCode, (sumPromiceMap.get(currencyCode) == null? 0:sumPromiceMap.get(currencyCode)) + sumPromice);
            sumResultMap.put(currencyCode, (sumResultMap.get(currencyCode)== null? 0:sumResultMap.get(currencyCode)) + sumResult);

        }
        model.setSumPromice(sumPromiceMap);
        model.setSumResult(sumResultMap);

        for (String[] line : lineList)
        {
            printrow(sheet, line, currentIndex + number, contentFormat);
            number++;
        }


        return currentIndex + number + 1;
    }

    protected int printSummaryInfo(WritableSheet sheet, Model model, int currentIndex) throws WriteException
    {
        WritableFont labelFont = new WritableFont(WritableFont.createFont("Calibri"), 14, WritableFont.BOLD);
        WritableFont headerFont = new WritableFont(WritableFont.createFont("Calibri"), 11, WritableFont.BOLD);
        WritableFont contentFont = new WritableFont(WritableFont.createFont("Calibri"), 11);


        WritableCellFormat labelFormat = new WritableCellFormat(labelFont);
        WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);

        //        Количество студентов: [выводить количество студентов попавших в отчет]
//        Количество договоров: [выводить количество договоров попавших в отчет]
//        Сумма всех обязательств, руб.: [выводить сумму всех обязательств попавших в отчет, по сути это сумма значений в колонке "Сумма обязательства по оплате"]
//        Сумма исполненных обязательств, руб.: [выводить сумму оплаченных обязательств, по сути это сумма значений в колонке "Уже оплачено"]

        List<String[]> hTable = new ArrayList<>();
        hTable.add(new String[] {"Количество студентов:", String.valueOf(model.getStudentCount())});
        hTable.add(new String[] {"Количество договоров:", String.valueOf(model.getContractCount())});

        for (int i = 0; i < hTable.size(); i++)
        {
            sheet.addCell(new Label(1, currentIndex, hTable.get(i)[0], headerFormat));
            sheet.addCell(new Label(6, currentIndex, hTable.get(i)[1], contentFormat));
            currentIndex++;
        }

        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMinimumFractionDigits(2);

        Map<String, Long> sumPromiceMap = model.getSumPromice();
        sheet.addCell(new Label(1, currentIndex, "Сумма всех обязательств:",headerFormat));
        int count = 6;
        for (Map.Entry<String, Long> entry : sumPromiceMap.entrySet())
        {
            sheet.addCell(new Label(count++, currentIndex, numberFormat.format(Currency.wrap(entry.getValue())), contentFormat));
            sheet.addCell(new Label(count, currentIndex, model.getCurrencyMap().get(entry.getKey()).getShortTitle(), contentFormat));
            count +=2;
        }
        currentIndex++;

        Map<String, Long> sumResultMap = model.getSumResult();
        sheet.addCell(new Label(1, currentIndex, "Сумма исполненных обязательств:",headerFormat));
        count = 6;
        for (Map.Entry<String, Long> entry : sumResultMap.entrySet())
        {
            sheet.addCell(new Label(count++, currentIndex, numberFormat.format(Currency.wrap(entry.getValue())), contentFormat));
            sheet.addCell(new Label(count, currentIndex, model.getCurrencyMap().get(entry.getKey()).getShortTitle(), contentFormat));
            count+=2;
        }

        return currentIndex++;

    }

    protected static void setWidths(List<Integer> widths, WritableSheet sheet)
    {
        for (int i = 0; i < widths.size(); i++)
        {
            sheet.setColumnView(i, widths.get(i));
        }
    }

    public static void printrow(WritableSheet sheet, String[] rowContent, int rowIndex, WritableCellFormat format) throws JXLException
    {
        for (int i = 0; i < rowContent.length; i++) {
            if (NumberUtils.isNumber(rowContent[i]))
                sheet.addCell(new Number(i, rowIndex, Double.parseDouble(rowContent[i]), format));
            else
                sheet.addCell(new Label(i, rowIndex, rowContent[i], format));
        }
    }
}