package ru.tandemservice.unieductr.base.bo.EduCtrEducationPromise.ui.InWizardStep;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.InWizardPromicesStep.InWizardPromicesUI;

import ru.tandemservice.unieductr.base.bo.EduCtrEducationPromise.ui.SectionPromise.EduCtrEducationPromiseSectionPromise;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;

public class EduCtrEducationPromiseInWizardStepUI extends InWizardPromicesUI<EduCtrEducationPromise>
{
    @Override
    protected Class<? extends BusinessComponentManager> promiceComponent() {
        return EduCtrEducationPromiseSectionPromise.class;
    }
}
