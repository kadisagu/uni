package ru.tandemservice.unieductr.base.entity;

import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import ru.tandemservice.unieductr.base.bo.EduProgramPrice.EduProgramPriceManager;
import ru.tandemservice.unieductr.base.entity.gen.*;

/**
 * Цена на образовательную программу
 * <p/>
 * Цена на образовательную программу
 */
public class EduProgramPrice extends EduProgramPriceGen
{
    @Override
    public CtrContractType getContractType()
    {
        return EduProgramPriceManager.instance().dao().getEduPriceContractType(this);
    }

    @Override
    public String getPriceElementTitle()
    {
        return getProgram().getTitle();
    }
}