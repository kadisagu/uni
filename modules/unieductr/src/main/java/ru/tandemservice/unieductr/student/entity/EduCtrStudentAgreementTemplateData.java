package ru.tandemservice.unieductr.student.entity;

import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;

import ru.tandemservice.unieductr.student.entity.gen.EduCtrStudentAgreementTemplateDataGen;

/**
 * Данные шаблона доп. соглашения к договору на обучение студента по ОП
 *
 * Данные базового шаблона для доп.соглашения студента на обучение по образовательной программе.
 */
public class EduCtrStudentAgreementTemplateData extends EduCtrStudentAgreementTemplateDataGen
{
    @Override
    public ICtrContractTemplateManager getManager() {
        return null;
    }
}