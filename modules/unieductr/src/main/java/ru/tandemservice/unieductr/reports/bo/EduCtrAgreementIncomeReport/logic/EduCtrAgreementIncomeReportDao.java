/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrAgreementIncomeReport.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections15.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.ctr.base.bo.Contactor.util.ContactorCategoryMeta;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contract.*;
import org.tandemframework.shared.ctr.catalog.entity.CtrTemplateScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.formatters.MoneyFormatter;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrTemplateScriptItemCodes;
import ru.tandemservice.unieductr.reports.entity.EduCtrAgreementIncomeReport;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon.Filters.*;

/**
 * @author Alexey Lopatin
 * @since 21.04.2015
 */
public class EduCtrAgreementIncomeReportDao extends UniBaseDao implements IEduCtrAgreementIncomeReportDao
{
    @Override
    public <M extends Model> RtfDocument createReportRtfDocument(M model)
    {
        CtrTemplateScriptItem template = getCatalogItem(CtrTemplateScriptItem.class, CtrTemplateScriptItemCodes.EDU_CTR_AGREEMENT_INCOME_REPORT);
        RtfDocument document = new RtfReader().read(template.getTemplate());
        RtfTableModifier tm = new RtfTableModifier();
        RtfInjectModifier im = new RtfInjectModifier();

        injectTable(tm, im, model, document).modify(document);
        injectModifier(im, model).modify(document);

        return document;
    }


    @Override
    public <M extends Model> EduCtrAgreementIncomeReport saveReport(M model, RtfDocument document)
    {
        EduCtrAgreementIncomeReport report = new EduCtrAgreementIncomeReport();

        DatabaseFile content = new DatabaseFile();
        content.setContent(RtfUtil.toByteArray(document));
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        content.setFilename("AgreementIncomeReport.rtf");
        save(content);

        UniEduProgramEducationOrgUnitAddon util = model.getUtil();

        if (model.getOrgUnitId() != null)
            report.setOrgUnit(get(OrgUnit.class, model.getOrgUnitId()));
        report.setContent(content);
        report.setFormingDate(model.getFormingDate());
        report.setPeriodDateFrom(model.getPeriodDateFrom());
        report.setPeriodDateTo(model.getPeriodDateTo());
        report.setContactorType(model.isActiveContactorType() ? model.getContactorType().getTitle() : "");

        report.setProgramSubject(model.isActiveProgramSubject() ? UniStringUtils.join(model.getProgramSubjectList(), EduProgramSubject.P_TITLE_WITH_CODE, ", ") : "");
        report.setProgramForm(model.isActiveProgramForm() ? UniStringUtils.join(model.getProgramFormList(), EduProgramForm.P_TITLE, ", ") : "");
        report.setEduBeginYear(model.isActiveEduBeginYear() ? UniStringUtils.join(model.getEduBeginYearList(), EducationYear.P_TITLE, ", ") : "");
        report.setProgramTrait(model.isActiveProgramTrait() ? UniStringUtils.join(model.getProgramTraitList(), EduProgramTrait.P_TITLE, ", ") : "");
        report.setEduProgram(model.isActiveEduProgram() ? UniStringUtils.join(model.getEduProgramList(), EduProgramProf.P_TITLE, ", ") : "");

        report.setFormativeOrgUnit(getValue(util, FORMATIVE_ORG_UNIT, OrgUnit.P_TITLE));
        report.setTerritorialOrgUnit(getValue(util, TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_TITLE));
        report.setEducationLevelHighSchool(getValue(util, EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.P_PRINT_TITLE));
        report.setDevelopForm(getValue(util, DEVELOP_FORM, DevelopForm.P_TITLE));
        report.setDevelopCondition(getValue(util, DEVELOP_CONDITION, DevelopCondition.P_TITLE));
        report.setDevelopTech(getValue(util, DEVELOP_TECH, DevelopTech.P_TITLE));
        report.setDevelopPeriod(getValue(util, DEVELOP_PERIOD, DevelopPeriod.P_TITLE));
        report.setCourse(model.isActiveCourse() ? model.getCourse().getTitle() : "");
        report.setGroup(model.isActiveGroup() ? model.getGroup().getTitle() : "");
        report.setAccountInactiveStudents(model.getAccountInactiveStudents().getTitle());

        save(report);

        return report;
    }

    @SuppressWarnings("unchecked")
    private String getValue(UniEduProgramEducationOrgUnitAddon util, UniEduProgramEducationOrgUnitAddon.Filters key, String propertyPath)
    {
        Map<UniEduProgramEducationOrgUnitAddon.Filters, Object> values = util.getValuesMap();
        return util.getFilterConfig(key).isCheckEnableCheckBox() ? UniStringUtils.join((Collection<IEntity>) values.get(key), propertyPath, ", ") : "";
    }

    /**
     * Подготавливает массив данных для отчета. Применяются фильтры с формы.
     *
     * @return {Student, Contract, debt, currencyCode}
     */
    @SuppressWarnings("unchecked")
    protected <M extends Model> Map<OrgUnit, Map<CtrContractObject, List<MultiKey>>> getContractMap(final M model)
    {
        // получаем список id студентов, удовлетворяющих фильтрам на форме и подразделению, если отчет строится с подразделения
        final List<Long> studentIds = Lists.newLinkedList();
        final DQLSelectBuilder studentBuilder = new DQLSelectBuilder().fromEntity(Student.class, "s")
                .column(property("s"));

        if(model.getAccountInactiveStudents() != null && !TwinComboDataSourceHandler.getSelectedValue(model.getAccountInactiveStudents()))
        {
            studentBuilder.where(eq(property("s", Student.archival()), value(Boolean.FALSE)))
                    .joinEntity("s", DQLJoinType.left, Group.class, "gr", eq(property("gr"), property("s", Student.group())))
                    .where(eq(property("s", Student.status().active()), value(Boolean.TRUE)))
                    .where(or(
                            eq(property("gr", Group.archival()), value(Boolean.FALSE)),
                            isNull(property("gr")))
                    );
        }

        if (model.getOrgUnitId() != null)
        {
            studentBuilder.where(or(
                    eq(property("s", Student.educationOrgUnit().formativeOrgUnit().id()), value(model.getOrgUnitId())),
                    eq(property("s", Student.educationOrgUnit().territorialOrgUnit().id()), value(model.getOrgUnitId()))
            ));
        }

        if (model.isActiveFieldSetStudent())
        {
            if (model.isActiveGroup())
                studentBuilder.where(eq(property("s", Student.group()), value(model.getGroup())));

            if (model.isActiveCourse())
                studentBuilder.where(eq(property("s", Student.course()), value(model.getCourse())));

            BatchUtils.execute(model.getUtil().getEducationOrgUnitFilteredList(), 128, elements -> studentIds.addAll(new DQLSelectBuilder().fromDataSource(studentBuilder.buildQuery(), "b")
                                      .column(property(Student.id().fromAlias("b.s")))
                                      .where(in(property(Student.educationOrgUnit().fromAlias("b.s")), elements))
                                      .createStatement(getSession()).<Long>list()));
        }
        else
            studentIds.addAll(ids(studentBuilder.createStatement(getSession()).<Student>list()));

        // получаем список id договоров, для которых есть EduCtrStudentContract из списка студентов полученых выше
        final Map<Long, Student> contractStudentIdMap = getStudentContractRel(studentIds);

        // получаем действующие на текущую дату версии договоров, id договоров которых получены выше
        Map<Long, CtrContractVersion> contractVersionMap = CtrContractVersionManager.instance().dao().getCurrentVersions(contractStudentIdMap.keySet(), new Date());

        // получаем версии, где контрагент в роли заказчика, для версий договоров полученых выше
        final List<Long> contractVersionContractorList = Lists.newArrayList();
        BatchUtils.execute(ids(contractVersionMap.values()), 128, elements -> {
            DQLSelectBuilder contractRole = new DQLSelectBuilder().fromEntity(CtrContractVersionContractorRole.class, "r")
                    .column(property("r", CtrContractVersionContractorRole.id()))
                    .where(eq(property("r", CtrContractVersionContractorRole.contactor().id()), property("vc", CtrContractVersionContractor.id())))
                    .where(eq(property("r", CtrContractVersionContractorRole.role().code()), value(CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER)));

            if (model.isActiveContactorType())
            {
                ContactorCategoryMeta meta = ContactorCategoryMeta.getMetaByUniqueCode(model.getContactorType().getId());
                contractRole.joinEntity("r", DQLJoinType.inner, meta.getContactorCategoryClass(), "c", eq(property("vc", CtrContractVersionContractor.contactor().id()), property("c", ContactorPerson.id())));
            }

            DQLSelectBuilder contractorBuilder = new DQLSelectBuilder().fromEntity(CtrContractVersionContractor.class, "vc")
                    .column(property("vc", CtrContractVersionContractor.owner().id())).distinct()
                    .where(exists(contractRole.buildQuery()))
                    .where(in(property("vc", CtrContractVersionContractor.owner().id()), elements));

            for (Long versionId : contractorBuilder.createStatement(getSession()).<Long>list())
                contractVersionContractorList.add(versionId);
        });

        final Map<Long, MultiKey> contractMultiKeys = Maps.newHashMap();
        BatchUtils.execute(contractVersionContractorList, 128, elements -> {
            DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                    new DQLSelectBuilder().fromEntity(EduCtrEducationPromise.class, "p")
                            .joinPath(DQLJoinType.inner, EduCtrEducationPromise.src().owner().fromAlias("p"), "v")
                            .joinPath(DQLJoinType.inner, EduCtrEducationPromise.eduProgram().fromAlias("p"), "edu")
                            .where(in(property("v.id"), elements))
            );
            int contract_id_col = dql.column(property("v", CtrContractVersion.contract().id()));
            int program_subject_id_col = dql.column(property("edu", EduProgramProf.programSubject()));
            DQLSelectBuilder eduPromisesBuilder = dql.getDql();

            if (model.isActiveProgramSubject())
                eduPromisesBuilder.where(in(property("edu", EduProgramProf.programSubject()), model.getProgramSubjectList()));

            if (model.isActiveProgramForm())
                eduPromisesBuilder.where(in(property("edu", EduProgramProf.form()), model.getProgramFormList()));

            if (model.isActiveEduBeginYear())
                eduPromisesBuilder.where(in(property("edu", EduProgramProf.year()), model.getEduBeginYearList()));

            if (model.isActiveProgramTrait())
                eduPromisesBuilder.where(in(property("edu", EduProgramProf.eduProgramTrait()), model.getProgramTraitList()));

            if (model.isActiveEduProgram())
                eduPromisesBuilder.where(in(property("edu"), model.getEduProgramList()));

            for (Object[] row : eduPromisesBuilder.createStatement(getSession()).<Object[]>list())
            {
                Long contractId = (Long) row[contract_id_col];

                if (!contractMultiKeys.containsKey(contractId))
                {
                    Student student = contractStudentIdMap.get(contractId);
                    EduProgramSubject subject = (EduProgramSubject) row[program_subject_id_col];
                    contractMultiKeys.put(contractId, new MultiKey(student, subject));
                }
            }
        });


        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CtrPaymentResult.class, "r")
                .where(in(property("r", CtrPaymentResult.contract().id()), contractMultiKeys.keySet()));

        if (null != model.getPeriodDateFrom())
        {
            builder.where(ge(property("r", CtrPaymentResult.timestamp()), valueDate(model.getPeriodDateFrom())));
        }
        if (null != model.getPeriodDateTo())
        {
            builder.where(le(property("r", CtrPaymentResult.timestamp()), valueDate(model.getPeriodDateTo())));
        }
        List<CtrPaymentResult> resultList = builder.createStatement(getSession()).list();
        if (resultList.isEmpty()) return Maps.newHashMap();

        Map<OrgUnit, Map<CtrContractObject, List<MultiKey>>> resultMap = Maps.newHashMap();
        for (CtrPaymentResult payment : resultList)
        {
            CtrContractObject contract = payment.getContract();

            Long contractId = contract.getId();
            OrgUnit orgUnit = contract.getOrgUnit();
            MultiKey keys = contractMultiKeys.get(contractId);

            Student student = (Student) keys.getKey(0);
            EduProgramSubject subject = (EduProgramSubject) keys.getKey(1);

            if (!resultMap.containsKey(orgUnit))
                resultMap.put(orgUnit, Maps.<CtrContractObject, List<MultiKey>>newLinkedHashMap());

            Map<CtrContractObject, List<MultiKey>> contractMap = resultMap.get(orgUnit);
            if (!contractMap.containsKey(contract))
                contractMap.put(contract, Lists.<MultiKey>newArrayList());

            contractMap.get(contract).add(new MultiKey(student, subject, payment));

            List list = new ArrayList(contractMap.entrySet());
            Collections.sort(list, new Comparator<Map.Entry<CtrContractObject, List<MultiKey>>>()
            {
                @Override
                public int compare(Map.Entry<CtrContractObject, List<MultiKey>> e1, Map.Entry<CtrContractObject, List<MultiKey>> e2)
                {
                    List<MultiKey> key1 = e1.getValue();
                    List<MultiKey> key2 = e2.getValue();

                    Student s1 = (Student) key1.get(0).getKey(0);
                    Student s2 = (Student) key2.get(0).getKey(0);
                    return CommonCollator.RUSSIAN_COLLATOR.compare(s1.getFullFio(), s2.getFullFio());
                }
            });
        }
        return resultMap;
    }

    /**
     * @return Возвращает используемые месяцы, согласно периоду (группировка по году)
     */
    private <M extends Model> Map<Integer, List<Integer>> getUsedMonth(M model)
    {
        Map<Integer, List<Integer>> resultMap = Maps.newLinkedHashMap();

        Date begin = model.getPeriodDateFrom();
        Date end = model.getPeriodDateTo();

        int monthBegin = CommonBaseDateUtil.getMonthStartingWithOne(begin);
        int monthEnd = CommonBaseDateUtil.getMonthStartingWithOne(end);

        int yearBegin = CoreDateUtils.createCalendar(begin).get(Calendar.YEAR);
        int yearEnd = CoreDateUtils.createCalendar(end).get(Calendar.YEAR);

        int yearCount = yearEnd - yearBegin;
        for (int i = monthBegin; i <= yearCount * 12 + monthEnd; i++)
        {
            int monthIndex = i;
            int yearIndex = yearBegin;
            while (monthIndex > 12)
            {
                monthIndex -= 12;
                yearIndex++;
            }
            if (!resultMap.containsKey(yearIndex))
                resultMap.put(yearIndex, Lists.<Integer>newArrayList());
            resultMap.get(yearIndex).add(monthIndex);
        }
        return resultMap;
    }

    protected Map<Long, Student> getStudentContractRel(List<Long> studentIds)
    {
        final Map<Long, Student> contractStudentMap = Maps.newHashMap();

        BatchUtils.execute(studentIds, 512, elements -> {
            DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                    new DQLSelectBuilder().fromEntity(EduCtrStudentContract.class, "sc")
                            .where(in(property("sc", EduCtrStudentContract.student().id()), elements))
            );
            int contract_id_col = dql.column(property("sc", EduCtrStudentContract.contractObject().id()));
            int student_col = dql.column(property("sc", EduCtrStudentContract.student()));

            for (Object[] row : dql.getDql().createStatement(getSession()).<Object[]>list())
            {
                contractStudentMap.put((Long) row[contract_id_col], (Student) row[student_col]);
            }
        });
        return contractStudentMap;
    }

    @SuppressWarnings("unchecked")
    protected <M extends Model> RtfTableModifier injectTable(RtfTableModifier tm, RtfInjectModifier im, M model, RtfDocument document)
    {
        final Map<Integer, List<Integer>> usedMonth = getUsedMonth(model);
        final Map<Integer, String> monthTitleMap = new TreeMap<>();
        String[] monthNames = RussianDateFormatUtils.MONTHS_NAMES_N;
        for (int i = 0; i < monthNames.length; i++)
        {
            String month = monthNames[i];
            String name = month.substring(0, 1).toUpperCase() + month.substring(1);
            monthTitleMap.put(i + 1, name);
        }

        Map<OrgUnit, Map<CtrContractObject, List<MultiKey>>> contractMap = getContractMap(model);
        if (contractMap.isEmpty())
        {
            im.put("total", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(0.0));
            tm.put("T", new String[][]{});
            return tm;
        }

        int number = 0;
        Map<String, Long> totalCurrencyCodeToSum = new HashMap<>();
        List<String[]> lineList = Lists.newLinkedList();
        final List<Integer> orgUnitRowIndexList = Lists.newArrayList();
        final List<Integer> totalRowIndexList = Lists.newArrayList();
        Set<OrgUnit> orgUnitList = contractMap.keySet();
        int monthSize = 0;
        List<List<Integer>> yearList = Lists.newArrayList(usedMonth.values());

        for (List<Integer> monthList : yearList)
            monthSize += monthList.size();

        for (OrgUnit orgUnit : orgUnitList)
        {
            if (orgUnitList.size() > 1)
            {
                String[] orgUnitLine = new String[1];
                orgUnitLine[0] = orgUnit.getTitle();
                lineList.add(orgUnitLine);
                orgUnitRowIndexList.add(number++);
            }

            Map<String, Long> orgUnitCurrencyCodeToSum = new HashMap<>();
            for (Map.Entry<CtrContractObject, List<MultiKey>> entry: contractMap.get(orgUnit).entrySet())
            {
                List<MultiKey> keys = entry.getValue();

                int col = 0;
                Student student = (Student) keys.get(0).getKey(0);
                EduProgramSubject subject = (EduProgramSubject) keys.get(0).getKey(1);

                Map<Integer, Map<Integer, List<Pair<String, Long>>>> paymentMap = Maps.newHashMap();
                for (MultiKey key : keys)
                {
                    CtrPaymentResult payment = (CtrPaymentResult) key.getKey(2);
                    Long value = payment.getSrc() instanceof EmployeePostContactor ? -1 * payment.getCostAsLong() : payment.getCostAsLong();
                    Date date = payment.getTimestamp();
                    org.tandemframework.shared.commonbase.catalog.entity.Currency currency = payment.getCurrency();
                    int indexYear = CoreDateUtils.createCalendar(date).get(Calendar.YEAR);
                    int indexMonth = CommonBaseDateUtil.getMonthStartingWithOne(date);

                    if (!paymentMap.containsKey(indexYear))
                        paymentMap.put(indexYear, Maps.newHashMap());

                    Map<Integer, List<Pair<String, Long>>> yearMap = paymentMap.get(indexYear);
                    if (!yearMap.containsKey(indexMonth))
                        yearMap.put(indexMonth, Lists.newArrayList());

                    Pair<String, Long> paymentInfo = new Pair<>();
                    paymentInfo.setX(currency.getCode());
                    paymentInfo.setY(value);
                    yearMap.get(indexMonth).add(paymentInfo);
                }

                Map<String, Long> totalRowCurrencyCodeToSum = new HashMap<>();
                String[] line = new String[monthSize + 3];
                line[col++] = student.getFullFio();
                line[col++] = subject.getTitleWithCode();

                for (Map.Entry<Integer, List<Integer>> usedEntry : usedMonth.entrySet())
                {
                    Integer year = usedEntry.getKey();
                    Map<Integer, List<Pair<String, Long>>> monthMap = paymentMap.get(year);
                    if (null == monthMap) { col += usedMonth.get(year).size(); continue; }

                    for (Integer month : usedEntry.getValue())
                    {
                        if (null == monthMap.get(month)) { col++; continue; }

                        List<String> values = Lists.newLinkedList();
                        for (Pair<String, Long> payment : monthMap.get(month))
                        {
                            if (!totalRowCurrencyCodeToSum.containsKey(payment.getX())) {
                                totalRowCurrencyCodeToSum.put(payment.getX(), payment.getY());
                            } else {
                                totalRowCurrencyCodeToSum.put(payment.getX(), totalRowCurrencyCodeToSum.get(payment.getX()) + payment.getY());
                            }
                            values.add(MoneyFormatter.formatter(payment.getX(), 4).format(payment.getY()));
                        }
                        line[col++] = StringUtils.join(values, "; ");
                    }
                }

                orgUnitCurrencyCodeToSum = Stream
                        .concat(orgUnitCurrencyCodeToSum.entrySet().stream(), totalRowCurrencyCodeToSum.entrySet().stream())
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (revenue1, revenue2) -> revenue1 + revenue2));
                line[line.length - 1] = getRevenueAsString(totalRowCurrencyCodeToSum);
                lineList.add(line);
                number++;
            }
            if (orgUnitList.size() > 1)
            {
                String[] totalLine = new String[monthSize + 3];
                totalLine[0] = "ИТОГО";
                totalLine[totalLine.length - 1] = getRevenueAsString(orgUnitCurrencyCodeToSum);
                lineList.add(totalLine);
                totalRowIndexList.add(number++);
            }
            totalCurrencyCodeToSum = Stream
                    .concat(totalCurrencyCodeToSum.entrySet().stream(), orgUnitCurrencyCodeToSum.entrySet().stream())
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                            (revenue1, revenue2) -> revenue1 + revenue2));
        }

        im.put("total", getRevenueAsString(totalCurrencyCodeToSum));

        if (usedMonth.size() == 1)
        {
            RtfSearchResult test = UniRtfUtil.findRtfTableMark(document, "T");
            RtfTable table = (RtfTable) test.getElementList().get(test.getIndex());
            table.getRowList().remove(1);
        }

        tm.put("T", lineList.toArray(new String[lineList.size()][4]));
        tm.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(final RtfTable table, int currentRowIndex)
            {
                RtfRow monthRow = table.getRowList().get(currentRowIndex - 1);
                RtfRow row = table.getRowList().get(currentRowIndex);

                final List<List<Integer>> monthList = Lists.newArrayList(usedMonth.values());
                int yearSize = usedMonth.size();
                final int[] years = new int[yearSize];
                Arrays.fill(years, 1);

                if (yearSize > 1)
                {
                    RtfRow yearRow = table.getRowList().get(currentRowIndex - 2);
                    // (заголовок) разбиваем 1-ую и 2-ую строку по количеству лет в периоде, в 1-ую записываем названия
                    RtfUtil.splitRow(monthRow, 2, null, years);
                    RtfUtil.splitRow(yearRow, 2, (newCell, index) -> {
                        List<Integer> yearTitles = Lists.newArrayList(usedMonth.keySet());
                        newCell.setElementList(new RtfString().append(String.valueOf(yearTitles.get(index))).toList());
                    }, years);

                    int monthColIdx = 0;
                    for (int i = 0; i < yearSize; i++)
                    {
                        final int yearIdx = i;
                        int monthSize = monthList.get(i).size();
                        final int[] month = new int[monthSize];
                        Arrays.fill(month, 1);

                        // (заголовок) разбиваем 2-ую строку по количеству месяцев в равных пропорциях, записывая их названия, в рамках года
                        RtfUtil.splitRow(monthRow, monthColIdx + 2, (newCell, index) -> {
                            Integer monthId = monthList.get(yearIdx).get(index);
                            newCell.setElementList(new RtfString().append(monthTitleMap.get(monthId)).toList());
                        }, month);
                        monthColIdx += monthSize;
                    }

                    RtfUtil.splitRow(row, 2, null, years);

                    monthColIdx = 0;
                    for (int i = 0; i < yearSize; i++)
                    {
                        int monthSize = monthList.get(i).size();
                        final int[] month = new int[monthSize];
                        Arrays.fill(month, 1);

                        // разбиваем 3-ую ячейку по количеству месяцев в равных пропорциях, в рамках года
                        RtfUtil.splitRow(row, monthColIdx + 2, null, month);
                        monthColIdx += monthSize;
                    }
                }
                else
                {
                    final List<Integer> month = usedMonth.entrySet().iterator().next().getValue();
                    final int[] parts = new int[month.size()];
                    Arrays.fill(parts, 1);
                    // разбиваем 3-ую ячейку по количеству месяцев в равных пропорциях, записывая их названия
                    RtfUtil.splitRow(monthRow, 2, (newCell, index) -> {
                        Integer monthId = month.get(index);
                        newCell.setElementList(new RtfString().append(monthTitleMap.get(monthId)).toList());
                    }, parts);

                    RtfUtil.splitRow(row, 2, null, parts);
                }
            }

            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                // в тех строках, где будет объединение ячеек нужно выделять болдом
                if (orgUnitRowIndexList.contains(rowIndex))
                {
                    SharedRtfUtil.setCellAlignment(cell, IRtfData.QC);
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                }
                if (totalRowIndexList.contains(rowIndex) && colIndex == 0)
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                return null;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                // объединяем все строки с нужными индексами
                for (Integer rowIndex : orgUnitRowIndexList)
                {
                    RtfRow row = newRowList.get(rowIndex + startIndex);
                    RtfUtil.unitAllCell(row, 0);
                }
            }
        });
        return tm;
    }

    private String getRevenueAsString(Map<String, Long> revenueHolder)
    {
        if (revenueHolder.isEmpty()) {
            return MoneyFormatter.ruMoneyFormatter(4).format(0L);
        }
        return revenueHolder.entrySet()
                .stream()
                .sorted(Comparator.comparing(Map.Entry::getKey))
                .map(e -> MoneyFormatter.formatter(e.getKey(), 4).format(e.getValue()))
                .collect(Collectors.joining(";"));
    }

    protected <M extends Model> RtfInjectModifier injectModifier(RtfInjectModifier modifier, M model)
    {
        OrgUnit orgUnit = model.getOrgUnitId() == null ? null : (OrgUnit) getNotNull(model.getOrgUnitId());

        return modifier
                .put("periodReport", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getPeriodDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getPeriodDateTo()))
                .put("scope", orgUnit == null ? "ИТОГО по всему ОУ" : "ИТОГО по подразделению (" + orgUnit.getTitle() + ")");
    }
}
