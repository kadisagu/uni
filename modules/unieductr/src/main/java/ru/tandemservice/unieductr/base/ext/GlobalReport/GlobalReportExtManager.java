/* $Id$ */
package ru.tandemservice.unieductr.base.ext.GlobalReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import ru.tandemservice.unieductr.reports.bo.EduCtrAgreementIncomeReport.ui.List.EduCtrAgreementIncomeReportList;
import ru.tandemservice.unieductr.reports.bo.EduCtrDebitorsPayReport.ui.List.EduCtrDebitorsPayReportList;
import ru.tandemservice.unieductr.reports.bo.EduCtrObligationsContractReport.ui.List.EduCtrObligationsContractReportList;
import ru.tandemservice.unieductr.reports.bo.EduCtrPaymentsReport.ui.List.EduCtrPaymentsReportList;

/**
 * @author azhebko
 * @since 28.03.2014
 */
@Configuration
public class GlobalReportExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private GlobalReportManager _globalReportManager;

    @Bean
    public ItemListExtension<GlobalReportDefinition> reportListExtension()
    {
        return itemListExtension(_globalReportManager.reportListExtPoint())
                .add("eduCtrDebitorsPayReport", new GlobalReportDefinition("eduCtrContract", "eduCtrDebitorsPay", EduCtrDebitorsPayReportList.class.getSimpleName()))
                .add("eduCtrPaymentsReport", new GlobalReportDefinition("eduCtrContract", "eduCtrPayments", EduCtrPaymentsReportList.class.getSimpleName()))
                .add("eduCtrAgreementIncomeReport", new GlobalReportDefinition("eduCtrContract", "eduCtrAgreementIncome", EduCtrAgreementIncomeReportList.class.getSimpleName()))
                .add("eduCtrObligationsContractReport", new GlobalReportDefinition("eduCtrContract", "eduCtrObligationContract", EduCtrObligationsContractReportList.class.getSimpleName()))
                .create();
    }
}