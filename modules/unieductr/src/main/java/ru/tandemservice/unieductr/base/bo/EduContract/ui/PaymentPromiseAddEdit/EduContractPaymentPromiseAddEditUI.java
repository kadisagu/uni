package ru.tandemservice.unieductr.base.bo.EduContract.ui.PaymentPromiseAddEdit;

import java.util.List;
import java.util.Objects;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.commonbase.catalog.entity.codes.CurrencyCodes;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice;

import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;

@Input({
    @Bind(key=EduContractPaymentPromiseAddEditUI.VERSION_ID, binding="versionHolder.id", required=true),
    @Bind(key=UIPresenter.PUBLISHER_ID, binding="promiseHolder.id")
})
public class EduContractPaymentPromiseAddEditUI extends UIPresenter
{
    public static final String VERSION_ID = "versionId";

    private final EntityHolder<CtrContractVersion> versionHolder = new EntityHolder<>();
    public EntityHolder<CtrContractVersion> getVersionHolder() { return this.versionHolder; }
    public CtrContractVersion getVersion() { return this.getVersionHolder().getValue(); }

    private final EntityHolder<CtrPaymentPromice> promiseHolder = new EntityHolder<>(new CtrPaymentPromice());
    public EntityHolder<CtrPaymentPromice> getPromiseHolder() { return this.promiseHolder; }
    public CtrPaymentPromice getPromise() { return this.getPromiseHolder().getValue(); }

    private List<CtrContractVersionContractor> contractorList;
    public List<CtrContractVersionContractor> getContractorList() { return this.contractorList; }
    public void setContractorList(final List<CtrContractVersionContractor> contractorList) {  this.contractorList = contractorList; }

    private List<Currency> currencyList;
    public List<Currency> getCurrencyList() { return this.currencyList; }
    public void setCurrencyList(final List<Currency> currencyList) { this.currencyList = currencyList; }

    public boolean isAddForm() {return null == this.getPromise().getId();}

    @Override
    public void onComponentRefresh()
    {
        // обновляем версию
        final CtrContractVersion version = this.getVersionHolder().refresh(CtrContractVersion.class);

        // список валют
        this.setCurrencyList(DataAccessServices.dao().getList(Currency.class, Currency.P_CODE));

        // список контактных лиц в версии
        this.setContractorList(CtrContractVersionManager.instance().dao().getContractors(version));

        // грузим обязательство
        final CtrPaymentPromice promise = this.getPromiseHolder().refresh();

        // параметры обязательства
        if (null == promise.getSrc()) {
            promise.setSrc(CtrContractVersionManager.instance().dao().getContactor(version, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER));
        }
        if (null == promise.getDst()) {
            promise.setDst(CtrContractVersionManager.instance().dao().getContactor(version, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER));
        }
        if (null == promise.getCurrency()) {
            promise.setCurrency(DataAccessServices.dao().getByCode(Currency.class, CurrencyCodes.RUB));
        }
    }

    public void onClickApply() {
        checkParties();
        DataAccessServices.dao().save(this.getPromise());
        this.deactivate();
    }

    private void checkParties()
    {
        if((getPromise().getSrc().getContactor() instanceof EmployeePostContactor && getPromise().getDst().getContactor() instanceof EmployeePostContactor)
                || (getPromise().getSrc().getContactor() instanceof JuridicalContactor && getPromise().getDst().getContactor() instanceof JuridicalContactor && Objects.equals(((JuridicalContactor) getPromise().getSrc().getContactor()).getExternalOrgUnit(), ((JuridicalContactor) getPromise().getDst().getContactor()).getExternalOrgUnit()))
                || (Objects.equals(getPromise().getSrc().getContactor(), getPromise().getDst().getContactor())))
        {
            throw new ApplicationException("Получатель и плательщик не могут быть одним лицом или являться представителями одной стороны");
        }
    }
}
