/**
 *$Id: SystemActionExtManager.java 26910 2013-04-10 05:45:04Z vdanilov $
 */
package ru.tandemservice.unieductr.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.unieductr.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        IItemListExtensionBuilder<SystemActionDefinition> itemListExtension = itemListExtension(_systemActionManager.buttonListExtPoint());
        itemListExtension.add("unieductr_exportPrices", new SystemActionDefinition("unieductr", "exportPrices", "onClickExportPrices", SystemActionPubExt.ACTION_PUB_ADDON_NAME));
        itemListExtension.add("unieductr_exportEduContract", new SystemActionDefinition("unieductr", "exportEduContract", "onClickExportEduContract", SystemActionPubExt.ACTION_PUB_ADDON_NAME));
        itemListExtension.add("unieductr_importEduContract", new SystemActionDefinition("unieductr", "importEduContract", "onClickImportEduContract", SystemActionPubExt.ACTION_PUB_ADDON_NAME));
        itemListExtension.add("unieductr_deleteInvalidEduContracts", new SystemActionDefinition("unieductr", "deleteInvalidEduContracts", "onClickDeleteInvalidEduContracts", SystemActionPubExt.ACTION_PUB_ADDON_NAME));
        return itemListExtension.create();
    }
}
