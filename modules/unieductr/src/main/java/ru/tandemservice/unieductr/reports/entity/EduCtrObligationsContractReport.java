package ru.tandemservice.unieductr.reports.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unieductr.reports.entity.gen.*;

import java.util.Arrays;
import java.util.List;

/** @see ru.tandemservice.unieductr.reports.entity.gen.EduCtrObligationsContractReportGen */
public class EduCtrObligationsContractReport extends EduCtrObligationsContractReportGen
{
    @EntityDSLSupport
    public String getPeriodString()
    {
        return new StringBuilder().append(DateFormatter.DEFAULT_DATE_FORMATTER.format(this.getPeriodDateFrom()))
                .append("-")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(this.getPeriodDateTo())).toString();
    }


    private static List<String> properties = Arrays.asList(
            L_ORG_UNIT,
            P_FORMING_DATE,
            P_REPORT_DATE,
            P_PERIOD_DATE_FROM,
            P_PERIOD_DATE_TO,
            P_FORMATIVE_ORG_UNIT,
            P_TERRITORIAL_ORG_UNIT,
            P_EDUCATION_LEVEL_HIGH_SCHOOL,
            P_DEVELOP_FORM,
            P_DEVELOP_CONDITION,
            P_DEVELOP_TECH,
            P_DEVELOP_PERIOD,
            P_COURSE,
            P_GROUP,
            P_STUDENT_STATUS,
            P_COMMENT
    );

    public List<String> getProperties()
    {
        return properties;
    }

}