/* $Id$ */
package ru.tandemservice.unieductr.program.ext.EduProgramHigherProf.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;

import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.ui.Pub.EduProgramHigherProfPub;
import ru.tandemservice.unieductr.base.bo.EduProgramPrice.ui.Tab.EduProgramPriceTab;

/**
 * @author nvankov
 * @since 2/26/14
 */
@Configuration
public class EduProgramHigherProfPubExt extends BusinessComponentExtensionManager
{
    @Autowired
    private EduProgramHigherProfPub _eduProgramHigherProfPub;


    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_eduProgramHigherProfPub.tabPanelExtPoint())
        .addTab(
            componentTab("eduProgramPriceTab", EduProgramPriceTab.class.getSimpleName())
            .permissionKey("viewPriceTab_eduProgramHigherProfPrice")
            .parameters("mvel:['permissionKeyPostfix': 'eduProgramHigherProfPrice']")
        )
        .create();
    }
}
