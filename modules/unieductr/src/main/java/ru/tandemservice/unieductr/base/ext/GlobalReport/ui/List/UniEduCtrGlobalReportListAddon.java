/* $Id$ */
package ru.tandemservice.unieductr.base.ext.GlobalReport.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;

/**
 * @author azhebko
 * @since 28.03.2014
 */
public class UniEduCtrGlobalReportListAddon extends UIAddon
{
    public static final String NAME = "uniEduCtrGlobalReportListAddon";

    public UniEduCtrGlobalReportListAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
}