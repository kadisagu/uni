package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentAddContractTemplate.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateDAO;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentAddContractTemplateData;

/**
 * @author vdanilov
 */
public interface IEduCtrStudentAddContractTemplateDAO extends ICtrContractTemplateDAO, INeedPersistenceSupport {

    void doUpdateContractVersionContactor(CtrContractVersion version, ContactorPerson provider, ContactorPerson customer);

    /** @param templateData */
    void doSaveTemplate(EduCtrStudentAddContractTemplateData templateData);

    /**
     * @param templateAddData
     * @return
     */
    EduCtrStudentAddContractTemplateData doCreateVersion(IEduCtrStudentAddContractTemplateAddData templateAddData);

}
