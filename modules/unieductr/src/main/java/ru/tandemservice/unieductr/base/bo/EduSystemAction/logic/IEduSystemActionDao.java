package ru.tandemservice.unieductr.base.bo.EduSystemAction.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

/**
 * Created by Denny on 7/13/2016.
 */
public interface IEduSystemActionDao extends INeedPersistenceSupport {

    /**
     * Удаляет договоры на обучение, которые не имеют связей со студентом или абитуриентом.
     * Также удаляются все версии договоров, факты исполнения, обязательства версий, контрагента в версии договора, печатные документы, приложенный к версии договора.
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRED)
    int deleteInvalidEduContracts();
}
