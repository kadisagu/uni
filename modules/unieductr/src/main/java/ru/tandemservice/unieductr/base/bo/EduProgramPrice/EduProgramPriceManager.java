/* $Id$ */
package ru.tandemservice.unieductr.base.bo.EduProgramPrice;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unieductr.base.bo.EduProgramPrice.logic.EduProgramPriceDao;
import ru.tandemservice.unieductr.base.bo.EduProgramPrice.logic.IEduProgramPriceDao;

/**
 * @author nvankov
 * @since 2/26/14
 */
@Configuration
public class EduProgramPriceManager extends BusinessObjectManager
{
    public static EduProgramPriceManager instance()
    {
        return instance(EduProgramPriceManager.class);
    }

    @Bean
    public IEduProgramPriceDao dao()
    {
        return new EduProgramPriceDao();
    }
}



    