/* $Id:$ */
package ru.tandemservice.unieductr.base.bo.EduContract.ui.ChangeEduProgramDataCorrection;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 14.08.15
 * Time: 12:57
 */

@Input({
        @Bind(key=EduContractChangeEduProgramDataCorrectionUI.CONTRACT_ID, binding=EduContractChangeEduProgramDataCorrectionUI.CONTRACT_ID),
        @Bind(key=EduContractChangeEduProgramDataCorrectionUI.EDU_PROGRAM_ID, binding=EduContractChangeEduProgramDataCorrectionUI.EDU_PROGRAM_ID)
})

public class EduContractChangeEduProgramDataCorrectionUI extends UIPresenter
{
    public static final String CONTRACT_ID = "contractId";
    public static final String EDU_PROGRAM_ID = "eduProgramId";

    private Long _contractId;
    private Long _eduProgramId;

    private CtrContractObject _contract;
    private DataWrapper _eduProgramOld;
    private EduProgramProf _eduProgramNew;

    @Override
    public void onComponentRefresh()
    {
        if(getContractId() != null)
            setContract(DataAccessServices.dao().get(getContractId()));
        if(getEduProgramId() != null)
            setEduProgramOld(DataAccessServices.dao().get(getEduProgramId()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("contractId", getContract() != null ? getContract().getId() : null);
        dataSource.put("eduProgramOld", getEduProgramOld() != null ? getEduProgramOld().getProperty("eduProgram") : null);
    }

    public void onClickApply()
    {
        EduContractManager.instance().dao().doChangeEduProgram(getContract().getId(), getEduProgramOld().getId(), getEduProgramNew().getId());
        setEduProgramOld(null);
        setEduProgramNew(null);
        if(getContractId() != null)
            deactivate();
        else
            _uiSupport.doRefresh();
    }

    public Long getContractId()
    {
        return _contractId;
    }

    public void setContractId(Long contractId)
    {
        _contractId = contractId;
    }

    public Long getEduProgramId()
    {
        return _eduProgramId;
    }

    public void setEduProgramId(Long eduProgramId)
    {
        _eduProgramId = eduProgramId;
    }

    public CtrContractObject getContract()
    {
        return _contract;
    }

    public void setContract(CtrContractObject contract)
    {
        _contract = contract;
    }

    public DataWrapper getEduProgramOld()
    {
        return _eduProgramOld;
    }

    public void setEduProgramOld(DataWrapper eduProgramOld)
    {
        _eduProgramOld = eduProgramOld;
    }

    public EduProgramProf getEduProgramNew()
    {
        return _eduProgramNew;
    }

    public void setEduProgramNew(EduProgramProf eduProgramNew)
    {
        _eduProgramNew = eduProgramNew;
    }
}
