package ru.tandemservice.unieductr.base.bo.EduContract.logic;

import com.google.common.collect.Lists;
import org.apache.commons.lang.mutable.MutableObject;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.events.CtrContractVersionDSetEventListenerBean;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContextObject;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;
import org.tandemframework.shared.ctr.base.entity.contactor.PhysicalContactor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractPromice;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractKind;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.entity.EduCtrAcademyPresenter;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.base.entity.IEducationPromise;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractKindCodes;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;

import java.text.NumberFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EduContractDAO extends UniBaseDao implements IEduContractDAO {

    /** @return true, если в иерархии от корня к текущему элементу есть элемент с указанным кодом */
    public static boolean isPathElementExist(CtrContractType type, String pathElementCode) {
        while (null != type) {
            if (pathElementCode.equals(type.getCode())) { return true; }
            type = type.getParent();
        }
        return false;
    }

    /* Поиск типа сторон договора на основании  данных о заказчике и обучаемом. */
    public static ContractSides getContractSides(ContactorPerson customer, ContactorPerson educationReceiver)
    {
        if (customer instanceof JuridicalContactor)
            return ContractSides.THREE_SIDES_ORG;

        if (customer instanceof PhysicalContactor)
        {
            if (customer.getPerson().equals(educationReceiver.getPerson()))
                return ContractSides.TWO_SIDES;

            return ContractSides.THREE_SIDES_PERSON;
        }

        return null;
    }

    /* Поиск типа сторон договора на основании вида договора. */
    @Override
    public ContractSides getContractSides(String ctrContractKindCode)
    {
        List<String> twoSideKinds = Lists.newArrayList(
                CtrContractKindCodes.EDU_CONTRACT_VO_2_SIDES,
                CtrContractKindCodes.EDU_CONTRACT_SPO_2_SIDES,
                CtrContractKindCodes.EDU_CONTRACT_DPO_2_SIDES,
                CtrContractKindCodes.PROGRAM_CHANGE_2_SIDES,
                CtrContractKindCodes.TERM_EDU_COST_2_SIDES
                );
        List<String> threeSidePersonKinds = Lists.newArrayList(
                CtrContractKindCodes.EDU_CONTRACT_VO_3_SIDES_PERSON,
                CtrContractKindCodes.EDU_CONTRACT_SPO_3_SIDES_PERSON,
                CtrContractKindCodes.EDU_CONTRACT_DPO_3_SIDES_PERSON,
                CtrContractKindCodes.PROGRAM_CHANGE_3_SIDES_PERSON,
                CtrContractKindCodes.TERM_EDU_COST_3_SIDES_PERSON
        );
        List<String> threeSideOrgKinds = Lists.newArrayList(
                CtrContractKindCodes.EDU_CONTRACT_VO_3_SIDES_ORG,
                CtrContractKindCodes.EDU_CONTRACT_SPO_3_SIDES_ORG,
                CtrContractKindCodes.EDU_CONTRACT_DPO_3_SIDES_ORG,
                CtrContractKindCodes.PROGRAM_CHANGE_3_SIDES_ORG,
                CtrContractKindCodes.TERM_EDU_COST_3_SIDES_ORG
        );

        if(twoSideKinds.contains(ctrContractKindCode))
        {
            return ContractSides.TWO_SIDES;
        }

        if(threeSidePersonKinds.contains(ctrContractKindCode))
        {
            return ContractSides.THREE_SIDES_PERSON;
        }

        if(threeSideOrgKinds.contains(ctrContractKindCode))
        {
            return ContractSides.THREE_SIDES_ORG;
        }

        return null;
    }

    @Override
    public List<CtrContractType> getContractTypes(Collection<String> codes) {
        final Set<String> codeSet = null == codes ? null : new HashSet<>(codes);
        final List<CtrContractType> types = new ArrayList<>();
        for (final CtrContractType type : this.getList(CtrContractType.class, CtrContractType.P_TITLE)) {

            boolean hasEduType = false;
            boolean hasCodes = (null == codeSet);

            {
                CtrContractType tmp = type;
                while (tmp != null) {
                    if (!hasEduType && IEduContractDAO.CTR_CONTRACT_TYPE_EDUCATION.equals(tmp.getCode())) {
                        hasEduType = true;
                    }
                    if (!hasCodes && (null == codeSet || codeSet.contains(tmp.getCode()))) {
                        hasCodes = true;
                    }
                    tmp = tmp.getParent();
                }
            }

            if (hasEduType && hasCodes) {
                types.add(type);
            }

        }
        return types;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Collection<ContactorPerson> getAcademyPresenters(final OrgUnit orgUnit)
    {
        if (null == orgUnit) {
            throw new IllegalArgumentException("orgUnit is null");
        }

        OrgUnit ou = orgUnit;
        while (null != ou) {
            final List<EmployeePostContactor> l = new DQLSelectBuilder()
            .fromEntity(EduCtrAcademyPresenter.class, "p").column(property(EduCtrAcademyPresenter.presenter().fromAlias("p")))
            .where(eq(property(EduCtrAcademyPresenter.orgUnit().fromAlias("p")), value(ou)))
            .order(property(EduCtrAcademyPresenter.presenter().employeePost().postRelation().postBoundedWithQGandQL().post().rank().fromAlias("p")))
            .createStatement(this.getSession()).list();
            if (l.size() > 0) { return (Collection)l; }

            ou = ou.getParent();
        }

        return Collections.emptyList();
    }

    @Override
    public String doGetNextNumber(final String prefix)
    {
        final String key = "eductr_contract_" + prefix;
        final NumberFormat format = buildNumberFormat();

        final MutableObject holder = new MutableObject();
        final List<CtrContractType> contractTypes = getContractTypes(null);
        UniDaoFacade.getNumberDao().execute(key, currentQueueValue -> {
            String number = prefix + format.format(currentQueueValue);

            final Set<String> usedNumbers = new HashSet<>(new DQLSelectBuilder()
                                                                  .fromEntity(CtrContractObject.class, "c").column(property(CtrContractObject.number().fromAlias("c")))
                                                                  .where(in(property(CtrContractObject.type().fromAlias("c")), contractTypes))
                                                                  .createStatement(getSession()).<String>list());
            while ((currentQueueValue <= 0) || usedNumbers.contains(String.valueOf(number))) {
                currentQueueValue++;
                number = prefix + format.format(currentQueueValue);
            }

            holder.setValue(number);
            return (1 + currentQueueValue);
        });

        return (String) holder.getValue();
    }

    // правила формирования номеров договоров после постфикса (по умолчанию - забитый слева нулями до четырех символов номер)
    protected NumberFormat buildNumberFormat()
    {
        final NumberFormat format = NumberFormat.getIntegerInstance();
        format.setMinimumIntegerDigits(4);
        format.setGroupingUsed(false);
        return format;
    }

    @Override
    public void doDeleteContract(ICtrContextObject relation) {
        if (!relation.equals(relation.getContractObject())) {
            // иногда здесь бывает сам договор (когда открывается карточка договора в контексте договора, потом что других контекстов больше нет)
            this.delete(relation);
        }
        CtrContractVersionManager.instance().dao().doDelete(relation.getContractObject());
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends IEntity> List<IEducationPromise<T>> getEducationPromiseList(CtrContractVersion version) {
        final List<IEducationPromise<T>> result = new ArrayList<>();
        for (CtrContractPromice p: getList(CtrContractPromice.class, CtrContractPromice.src().owner(), version, CtrContractPromice.P_DEADLINE_DATE)) {
            if (p instanceof IEducationPromise) {
                result.add((IEducationPromise)p);
            }
        }
        return result;
    }

    @Override
    public <T extends IEntity> IEducationPromise<T> getFirstEducationPromise(CtrContractVersion version) throws ApplicationException {
        final List<IEducationPromise<T>> list = getEducationPromiseList(version);
        if (list.isEmpty()) { throw new ApplicationException("В договоре нет ни одного обязательства по обучению."); }
        return list.get(0);
    }

    @Override
    public void doChangeEduProgram(Long contractId, Long eduProgramOldId, Long eduProgramNewId)
    {
        CtrContractObject contractObject = get(contractId);
        EduProgramProf eduProgramOld = get(eduProgramOldId);
        EduProgramProf eduProgramNew = get(eduProgramNewId);

        if(!eduProgramOld.getProgramSubject().equals(eduProgramNew.getProgramSubject())
                && !eduProgramOld.getProgramSubject().equals(eduProgramNew.getProgramSubject())
                && !eduProgramOld.getKind().equals(eduProgramNew.getKind())
                && !eduProgramOld.getYear().equals(eduProgramNew.getYear())
                && !eduProgramOld.getForm().equals(eduProgramNew.getForm())
                && !eduProgramOld.getDuration().equals(eduProgramNew.getDuration())
                && !eduProgramOld.getProgramQualification().equals(eduProgramNew.getProgramQualification())
                || ((eduProgramOld instanceof EduProgramHigherProf && eduProgramNew instanceof EduProgramHigherProf)
                    && !((EduProgramHigherProf) eduProgramOld).getProgramOrientation().equals(((EduProgramHigherProf) eduProgramNew).getProgramOrientation())
                    && !((EduProgramHigherProf) eduProgramOld).getProgramSpecialization().equals(((EduProgramHigherProf) eduProgramNew).getProgramSpecialization())))
        {
            throw new ApplicationException("Характеристики образовательных программ (направление подготовки, вид ОП, учебный год начала обучения, форма обучения, продолжительность обучения, квалификация, направленность для ВО и ориентация для ВО) должны совпадать.");
        }

        final DSetEventManager manager = DSetEventManager.getInstance();
        manager.unregisterListener(DSetEventType.afterUpdate, CtrContractPromice.class, CtrContractVersionDSetEventListenerBean.ActivatedVersionLocker.PROMICE);

        new DQLUpdateBuilder(EduCtrEducationPromise.class)
                .where(eq(property(EduCtrEducationPromise.src().owner().contract()), value(contractObject)))
                .where(eq(property(EduCtrEducationPromise.eduProgram()), value(eduProgramOld)))
                .set(EduCtrEducationPromise.eduProgram().s(), value(eduProgramNew))
                .createStatement(getSession()).execute();

        manager.registerListener(DSetEventType.afterUpdate, CtrContractPromice.class, CtrContractVersionDSetEventListenerBean.ActivatedVersionLocker.PROMICE);

    }

    @Override
    public Student getStudentByContract(CtrContractObject contractObject) {
        return new DQLSelectBuilder()
                .fromEntity(EduCtrStudentContract.class, "stc")
                .where(eq(property("stc",EduCtrStudentContract.contractObject()),value(contractObject)))
                .column(property("stc",EduCtrStudentContract.student()))
                .createStatement(getSession())
                .uniqueResult();
    }

}
