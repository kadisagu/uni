package ru.tandemservice.unieductr.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Сетка оплаты"
 * Имя сущности : ctrPricePaymentGrid
 * Файл data.xml : unieductr.data.xml
 */
public interface CtrPricePaymentGridCodes
{
    /** Константа кода (code) элемента : За весь период обучения (title) */
    String EPP_PERIOD = "01.01";
    /** Константа кода (code) элемента : За год (title) */
    String EPP_YEAR = "01.02";
    /** Константа кода (code) элемента : За семестр (title) */
    String EPP_SEMESTER = "01.03";
    /** Константа кода (code) элемента : За половину семестра (title) */
    String EPP_HALF_SEMESTER = "01.04";
    /** Константа кода (code) элемента : Помесячно (title) */
    String EPP_MONTH = "01.05";

    Set<String> CODES = ImmutableSet.of(EPP_PERIOD, EPP_YEAR, EPP_SEMESTER, EPP_HALF_SEMESTER, EPP_MONTH);
}
