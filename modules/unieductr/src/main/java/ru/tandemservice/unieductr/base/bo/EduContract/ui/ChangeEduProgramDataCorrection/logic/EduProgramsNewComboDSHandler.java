/* $Id:$ */
package ru.tandemservice.unieductr.base.bo.EduContract.ui.ChangeEduProgramDataCorrection.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.caf.logic.handler.SimpleTitledComboDataSourceWithPopupSizeHandler;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;

import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 17.08.15
 * Time: 4:23
 */
public class EduProgramsNewComboDSHandler extends SimpleTitledComboDataSourceWithPopupSizeHandler
{
    public EduProgramsNewComboDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EduProgramProf eduProgramProf = context.get("eduProgramOld");
        if(eduProgramProf != null)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EduProgramProf.class, "ep")
                    .column("ep")
                    .where(eq(property("ep", EduProgramProf.programSubject()), value(eduProgramProf.getProgramSubject())))
                    .where(eq(property("ep", EduProgramProf.kind()), value(eduProgramProf.getKind())))
                    .where(eq(property("ep", EduProgramProf.year()), value(eduProgramProf.getYear())))
                    .where(eq(property("ep", EduProgramProf.form()), value(eduProgramProf.getForm())))
                    .where(eq(property("ep", EduProgramProf.duration()), value(eduProgramProf.getDuration())))
                    .where(eq(property("ep", EduProgramProf.programQualification()), value(eduProgramProf.getProgramQualification())))
                    .where(ne(property("ep", EduProgramProf.id()), value(eduProgramProf.getId())));

            if(eduProgramProf instanceof EduProgramHigherProf) {
                builder.joinEntity("ep", DQLJoinType.inner, EduProgramHigherProf.class, "eph", eq(property("ep", EduProgramProf.id()), property("eph", EduProgramHigherProf.id())))
                        .where(eq(property("eph", EduProgramHigherProf.programOrientation()), value(((EduProgramHigherProf) eduProgramProf).getProgramOrientation())))
                        .where(eq(property("eph", EduProgramHigherProf.programSpecialization()), value(((EduProgramHigherProf) eduProgramProf).getProgramSpecialization())));

            } else if(eduProgramProf instanceof EduProgramSecondaryProf) {
                builder.joinEntity("ep", DQLJoinType.inner, EduProgramSecondaryProf.class, "eps", eq(property("ep", EduProgramProf.id()), property("eps", EduProgramSecondaryProf.id())));

            } else {
                return super.execute(input, context);
            }

            builder.order(property("ep", EduProgramProf.title()));

            String filter = input.getComboFilterByValue();
            if(!StringUtils.isEmpty(filter))
                builder.where(likeUpper(property("ep", EduProgramProf.title()), value(CoreStringUtils.escapeLike(filter, true))));

            List<EduProgram> eduPrograms = createStatement(builder).list();

            context.put(UIDefines.COMBO_OBJECT_LIST, eduPrograms);
            DSOutput output = super.execute(input, context);
            output.setTotalSize(eduPrograms.size());
            return output;
        }
        else
        {
            context.put(UIDefines.COMBO_OBJECT_LIST, Collections.emptyList());
            return super.execute(input, context);
        }
    }
}
