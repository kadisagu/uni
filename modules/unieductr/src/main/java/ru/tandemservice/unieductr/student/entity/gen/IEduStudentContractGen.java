package ru.tandemservice.unieductr.student.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;
import ru.tandemservice.unieductr.base.entity.gen.IEduContractRelationGen;
import ru.tandemservice.unieductr.student.entity.IEduStudentContract;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.unieductr.student.entity.IEduStudentContract;

/**
 * Связь студента с договором на обучение
 *
 * Интерфейс для связей студента с договором на обучение.
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IEduStudentContractGen extends InterfaceStubBase
 implements IEduContractRelation, IEduStudentContract{
    public static final int VERSION_HASH = 1854876272;

    public static final String L_CONTRACT_OBJECT = "contractObject";
    public static final String L_STUDENT = "student";

    private CtrContractObject _contractObject;
    private Student _student;

    @NotNull

    public CtrContractObject getContractObject()
    {
        return _contractObject;
    }

    public void setContractObject(CtrContractObject contractObject)
    {
        _contractObject = contractObject;
    }

    @NotNull

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    private static final Path<IEduStudentContract> _dslPath = new Path<IEduStudentContract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.unieductr.student.entity.IEduStudentContract");
    }
            

    /**
     * @return Договор на обучение. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.student.entity.IEduStudentContract#getContractObject()
     */
    public static CtrContractObject.Path<CtrContractObject> contractObject()
    {
        return _dslPath.contractObject();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.student.entity.IEduStudentContract#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    public static class Path<E extends IEduStudentContract> extends EntityPath<E>
    {
        private CtrContractObject.Path<CtrContractObject> _contractObject;
        private Student.Path<Student> _student;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Договор на обучение. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.student.entity.IEduStudentContract#getContractObject()
     */
        public CtrContractObject.Path<CtrContractObject> contractObject()
        {
            if(_contractObject == null )
                _contractObject = new CtrContractObject.Path<CtrContractObject>(L_CONTRACT_OBJECT, this);
            return _contractObject;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.student.entity.IEduStudentContract#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

        public Class getEntityClass()
        {
            return IEduStudentContract.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.unieductr.student.entity.IEduStudentContract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
