/* $Id$ */
package ru.tandemservice.unieductr.dao.mdbio;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.healthmarketscience.jackcess.*;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IFieldPropertyMeta;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.commonbase.catalog.entity.codes.CurrencyCodes;
import org.tandemframework.shared.ctr.base.bo.Contactor.ContactorManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contactor.PersonNextOfKin2physicalContactorRelation;
import org.tandemframework.shared.ctr.base.entity.contactor.PhysicalContactor;
import org.tandemframework.shared.ctr.base.entity.contract.*;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractRole;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.ctr.catalog.entity.CtrPrintTemplate;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.fias.utils.AddressBaseUtils;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.mdbio.BaseIODao;
import ru.tandemservice.uni.dao.mdbio.ICatalogIODao;
import ru.tandemservice.uni.dao.mdbio.IPersonIODao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.gen.StudentGen;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;
import ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractKindCodes;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractTypeCodes;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContractTemplateData;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentSpoContractTemplateData;

import java.io.IOException;
import java.util.*;

import static org.apache.commons.lang.StringUtils.trimToEmpty;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Andrey Avetisov
 * @since 08.12.2014
 */
public class ContractsIODao extends BaseIODao implements IContractsIODao
{
    private static final String STUDENT_TABLE = "student_t";
    private static final String CITIZENSHIP_TABLE = "citizenship_t";
    private static final String EMPLOYEE_TABLE = "employee_t";
    private static final String PERSON_NEXTOFKIN_TABLE = "person_nextofkin_t";
    private static final String EDU_PROGRAM_TABLE = "edu_program_t";
    private static final String CONTRACT_TABLE = "contract_t";
    private static final String CONTRACT_PRINT_TEMPLATE_TABLE = "ctr_print_template_t";
    private static final String PAYMENT_PROMISE_TABLE = "payment_promise_t";
    private static final String PAYMENT_RESULT_TABLE = "payment_result_t";

    private static final String CITIZENSHIP_ID_COLUMN = "citizenship_id";
    private static final String CITIZENSHIP_TITLE_COLUMN = "citizenship_title";

    private static final String STUDENT_ID_COLUMN = "student_id";
    private static final String PERSON_ID_COLUMN = "person_id";
    private static final String STUDENT_PERSONAL_NUMBER_COLUMN = "student_personal_number";
    private static final String STUDENT_COURSE_COLUMN = "student_course";
    private static final String STUDENT_GROUP_COLUMN = "student_group";
    private static final String STUDENT_ENTRANCE_COLUMN = "entrance_p";
    private static final String STUDENT_EDU_PROGRAM_KIND_COLUMN = "edu_program_kind";
    private static final String STUDENT_EDU_PROGRAM_FORM_COLUMN = "edu_program_form";
    private static final String STUDENT_EDU_PROGRAM_DURATION_COLUMN = "edu_program_duration";
    private static final String STUDENT_EDU_PROGRAM_DEVELOPTECH_COLUMN = "edu_program_developtech";
    private static final String STUDENT_EDU_PROGRAM_OWNER_ORGUNIT_COLUMN = "edu_program_ownerOrgUnit";
    private static final String STUDENT_EDU_PROGRAM_SUBJECT_COLUMN = "edu_program_programSubject";
    private static final String STUDENT_PROGRAM_QUALIFICATION_COLUMN = "edu_program_programQualification";
    private static final String STUDENT_PROGRAM_SPECIALIZATION_COLUMN = "edu_program_programSpecialization";


    private static final String EMPLOYEE_ID_COLUMN = "employee_id";
    private static final String EMPLOYEE_FIRST_NAME_COLUMN = "employee_firstName";
    private static final String EMPLOYEE_LAST_NAME_COLUMN = "employee_lastName";
    private static final String EMPLOYEE_MIDDLE_NAME_COLUMN = "employee_middleName";
    private static final String EMPLOYEE_POST_COLUMN = "employee_post";
    private static final String EMPLOYEE_ORG_UNIT_COLUMN = "employee_org_unit";

    private static final String EDU_PROGRAM_ID_COLUMN = "edu_program_id";
    private static final String EDU_PROGRAM_TITLE_COLUMN = "edu_program_title";
    private static final String EDU_PROGRAM_SHORT_TITLE_COLUMN = "edu_program_short_title";
    private static final String EDU_PROGRAM_YEAR_COLUMN = "edu_program_year";
    private static final String EDU_PROGRAM_KIND_COLUMN = "edu_program_kind";
    private static final String EDU_PROGRAM_FORM_COLUMN = "edu_program_form";
    private static final String EDU_PROGRAM_DURATION_COLUMN = "edu_program_duration";
    private static final String EDU_PROGRAM_TRAIT_COLUMN = "edu_program_trait";
    private static final String EDU_PROGRAM_OWNER_ORG_UNIT_COLUMN = "edu_program_ownerOrgUnit";
    private static final String EDU_PROGRAM_INSTITUTION_ORG_UNIT_COLUMN = "edu_program_institutionOrgUnit";
    private static final String EDU_PROGRAM_SUBJECT_COLUMN = "edu_program_programSubject";
    private static final String EDU_PROGRAM_QUALIFICATION_COLUMN = "edu_program_programQualification";
    private static final String EDU_PROGRAM_SPECIALIZATION_COLUMN = "edu_program_programSpecialization";
    private static final String EDU_PROGRAM_IN_DEPTH_STUDY_COLUMN = "edu_program_inDepthStudy";

    private static final String CONTRACT_PRINT_TEMPLATE_ID_COLUMN = "print_template_id";
    private static final String CONTRACT_PRINT_TEMPLATE_TITLE_COLUMN = "print_template_title";

    private static final String CONTRACT_ID_COLUMN = "contract_id";
    private static final String PAYER_ID_COLUMN = "payer_id";
    private static final String CONTRACT_NUMBER_COLUMN = "contract_number";
    private static final String CONTRACT_REG_DATE_COLUMN = "contract_reg_date";
    private static final String CONTRACT_BEGIN_DATE_COLUMN = "contract_begin_date";
    private static final String CONTRACT_END_DATE_COLUMN = "contract_end_date";
    private static final String COMMENT_COLUMN = "comment";
    private static final String CONTRACT_CIPHER_COLUMN = "cipher";
    private static final String CONTRACT_ACTIVATION_COLUMN = "activation";
    private static final String CONTRACT_EDU_YEAR_COLUMN = "education_year_id";
    private static final String CONTRACT_PRINT_TEMPLATE_COLUMN = "contract_print_template_id";

    private static final String COST_COLUMN = "cost";
    private static final String STAGE_COLUMN = "stage";
    private static final String DEADLINE_COLUMN = "deadline_date";

    private static final String PAYMENT_CHECK_NUMBER_COLUMN = "payment_check_number";
    private static final String PAYMENT_ORDER_NUMBER_COLUMN = "payment_order_number";
    private static final String TIMESTAMP_COLUMN = "timestamp";

    private static final String NEXTOFKIN_ID_COLUMN = "nextofkin_id";
    private static final String RELATION_DEGREE_COLUMN = "relation_degree_p";
    private static final String PASSPORT_TYPE_COLUMN = "passport_type_p";
    private static final String IDENTITY_SERIA_COLUMN = "identity_seria";
    private static final String IDENTITY_NUMBER_COLUMN = "identity_number";
    private static final String IDENTITY_FIRST_NAME_COLUMN = "identity_firstName";
    private static final String IDENTITY_LAST_NAME_COLUMN = "identity_lastName";
    private static final String IDENTITY_MIDDLE_NAME_COLUMN = "identity_middleName";
    private static final String SEX_COLUMN = "sex_p";
    private static final String IDENTITY_BIRTHDAY_COLUMN = "identity_birthDate";
    private static final String IDENTITY_DATE_COLUMN = "identity_date_p";
    private static final String IDENTITY_CODE_COLUMN = "identity_code_p";
    private static final String IDENTITY_PLACE_COLUMN = "identity_place_p";
    private static final String EMPLOYEE_PLACE_COLUMN = "employee_place_p";
    private static final String PHONES_COLUMN = "phones_p";
    private static final String ADDRESS_COLUMN = "address_id";


    @Override
    public void doExport_ContractsList(Database mdb) throws Exception
    {
        final List<Long> contractIds = new ArrayList<>();

        //  IStudentIODao.instance.get().export_StudentList(mdb, false);
        export_CitizenshipList(mdb);
        export_StudentList(mdb, false);
        export_EmployeePostList(mdb, false);
        IPersonIODao.instance.get().export_PersonList(mdb, false);
        export_PersonNextOfKin(mdb);
        export_EduProgramProf(mdb);
        export_printTemplate(mdb);
        export_contract(mdb, contractIds);
        export_paymentPromise(mdb, contractIds);
        export_paymentResult(mdb, contractIds);
        export_eduYear(mdb);
    }

    @Override
    public Map<String, Long> doImport_ContractList(Database mdb) throws Exception
    {
        validateTables(mdb);
        return doImport_ContractData(mdb);
    }

    private void export_StudentList(final Database mdb, boolean includeArchiveStudents) throws IOException
    {
        if (null != mdb.getTable(STUDENT_TABLE))
        {
            return;
        }
        final TableBuilder tableBuilder = new TableBuilder(STUDENT_TABLE)
                /* [0] */.addColumn(new ColumnBuilder(STUDENT_ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [1] */.addColumn(new ColumnBuilder(PERSON_ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [2] */.addColumn(new ColumnBuilder(STUDENT_PERSONAL_NUMBER_COLUMN, DataType.TEXT).setMaxLength().toColumn())
                /* [3] */.addColumn(new ColumnBuilder(STUDENT_COURSE_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [4] */.addColumn(new ColumnBuilder(STUDENT_GROUP_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())

                /* [5] */.addColumn(new ColumnBuilder(STUDENT_ENTRANCE_COLUMN, DataType.INT).toColumn())
                /* [6] */.addColumn(new ColumnBuilder(STUDENT_EDU_PROGRAM_KIND_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [7] */.addColumn(new ColumnBuilder(STUDENT_EDU_PROGRAM_FORM_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [8] */.addColumn(new ColumnBuilder(STUDENT_EDU_PROGRAM_DURATION_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [9] */.addColumn(new ColumnBuilder(STUDENT_EDU_PROGRAM_DEVELOPTECH_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [10] */.addColumn(new ColumnBuilder(STUDENT_EDU_PROGRAM_OWNER_ORGUNIT_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [11] */.addColumn(new ColumnBuilder(STUDENT_EDU_PROGRAM_SUBJECT_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [12] */.addColumn(new ColumnBuilder(STUDENT_PROGRAM_QUALIFICATION_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [13] */.addColumn(new ColumnBuilder(STUDENT_PROGRAM_SPECIALIZATION_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn());



        final Table student_t = tableBuilder.toTable(mdb);

        final Session session = this.getSession();
        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "e").column(DQLExpressions.property("e.id"))
                .where(DQLExpressions.eq(DQLExpressions.property("e", Student.compensationType().code()), DQLExpressions.value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)));
        if (!includeArchiveStudents)
            builder.where(DQLExpressions.eq(DQLExpressions.property(StudentGen.archival().fromAlias("e")), DQLExpressions.value(Boolean.FALSE)));
        final List<Long> ids = builder.createStatement(session).list();
        final List<Object[]> rows = new ArrayList<>(ids.size());

        BatchUtils.execute(ids, 128, ids1 -> {
            try
            {
                for (final Student student : ContractsIODao.this.getList(Student.class, "id", ids1))
                {
                    EducationLevelsHighSchool educationLevelsHighSchool = student.getEducationOrgUnit().getEducationLevelHighSchool();
                    EduProgramQualification programQualification = educationLevelsHighSchool.getAssignedQualification();
                    EduProgramSubject programSubject = educationLevelsHighSchool.getEducationLevel().getEduProgramSubject();
                    EduProgramSpecialization programSpecialization = educationLevelsHighSchool.getEducationLevel().getEduProgramSpecialization();
                    EduProgramForm programForm = student.getEducationOrgUnit().getDevelopForm().getProgramForm();
                    EduProgramDuration eduProgramDuration = student.getEducationOrgUnit().getDevelopPeriod().getEduProgramDuration();

                    List<Object> fields = new ArrayList<>(5);

                    /* [0] */
                    fields.add(Long.toHexString(student.getId()));
                    /* [1] */
                    fields.add(Long.toHexString(student.getPerson().getId()));
                    /* [2] */
                    fields.add(StringUtils.trimToNull(student.getPersonalNumber()));
                    /* [3] */
                    fields.add(StringUtils.trimToNull(student.getCourse().getTitle()));
                    /* [4] */
                    fields.add(student.getGroup() != null ? StringUtils.trimToNull(student.getGroup().getTitle()) : null);
                    /* [5] */
                    fields.add(student.getEntranceYear());
                    /* [6] */
                    fields.add(programSubject!=null?programSubject.getSubjectIndex().getProgramKind().getTitle():null);
                    /* [7] */
                    fields.add(programForm != null ? programForm.getTitle() : null);
                    /* [8] */
                    fields.add(eduProgramDuration != null ? eduProgramDuration.getTitle() : null);
                    /* [9] */
                    fields.add(student.getEducationOrgUnit().getDevelopTech().getTitle());
                    /* [10] */
                    fields.add(educationLevelsHighSchool.getOrgUnit().getTitle());
                    /* [11] */
                    fields.add(programSubject!=null?programSubject.getTitle():null);
                    /* [12] */
                    fields.add(programQualification!=null?programQualification.getTitle():null);
                    /* [13] */
                    fields.add(programSpecialization!=null?programSpecialization.getTitle():null);
                    rows.add(fields.toArray());
                }
            }
            catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();

        });
        student_t.addRows(rows);
    }

    private void export_CitizenshipList(final Database mdb) throws IOException
    {
        if (null != mdb.getTable(CITIZENSHIP_TABLE))
        {
            return;
        }
        final TableBuilder tableBuilder = new TableBuilder(CITIZENSHIP_TABLE)
                /* [0] */.addColumn(new ColumnBuilder(CITIZENSHIP_ID_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [1] */.addColumn(new ColumnBuilder(CITIZENSHIP_TITLE_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn());


        final Table citizenship_t = tableBuilder.toTable(mdb);

        final Session session = this.getSession();
        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ICitizenship.class, "e").column(DQLExpressions.property("e.id"));

        final List<Long> ids = builder.createStatement(session).list();
        final List<Object[]> rows = new ArrayList<>(ids.size());

        BatchUtils.execute(ids, 128, ids1 -> {
            try
            {
                for (final ICitizenship citizenship : ContractsIODao.this.getList(ICitizenship.class, "id", ids1))
                {
                    List<Object> fields = new ArrayList<>(5);

                    /* [0] */
                    fields.add(Long.toHexString(citizenship.getId()));
                    /* [1] */
                    fields.add(StringUtils.trimToNull(citizenship.getFullTitle()));
                    rows.add(fields.toArray());
                }
            }
            catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();

        });
        citizenship_t.addRows(rows);
    }

    private void export_EmployeePostList(Database mdb, boolean includeArchiveEmployee) throws IOException
    {
        if (null != mdb.getTable(EMPLOYEE_TABLE))
        {
            return;
        }

        final TableBuilder tableBuilder = new TableBuilder(EMPLOYEE_TABLE)
                /* [0] */.addColumn(new ColumnBuilder(EMPLOYEE_ID_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [1] */.addColumn(new ColumnBuilder(PERSON_ID_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [2] */.addColumn(new ColumnBuilder(EMPLOYEE_LAST_NAME_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [3] */.addColumn(new ColumnBuilder(EMPLOYEE_FIRST_NAME_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [4] */.addColumn(new ColumnBuilder(EMPLOYEE_MIDDLE_NAME_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [5] */.addColumn(new ColumnBuilder(EMPLOYEE_POST_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [6] */.addColumn(new ColumnBuilder(EMPLOYEE_ORG_UNIT_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn());
        final Table employee_t = tableBuilder.toTable(mdb);

        final Session session = this.getSession();
        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "e").column(DQLExpressions.property("e.id"))
                .where(DQLExpressions.eq(DQLExpressions.property("e", EmployeePost.postStatus().code()), DQLExpressions.value(EmployeePostStatusCodes.STATUS_ACTIVE)))
                .where(DQLExpressions.ne(DQLExpressions.property("e", EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().code()), DQLExpressions.value(EmployeeTypeCodes.EDU_STAFF)));
        if (!includeArchiveEmployee)
            builder.where(DQLExpressions.eq(DQLExpressions.property(EmployeePost.employee().archival().fromAlias("e")), DQLExpressions.value(Boolean.FALSE)));
        final List<Long> ids = builder.createStatement(session).list();
        final List<Object[]> rows = new ArrayList<>(ids.size());

        BatchUtils.execute(ids, 128, ids1 -> {
            try
            {
                for (final EmployeePost employeePost : ContractsIODao.this.getList(EmployeePost.class, "id", ids1))
                {
                    List<Object> fields = new ArrayList<>(7);

                    /* [0] */
                    fields.add(Long.toHexString(employeePost.getId()));
                    /* [1] */
                    fields.add(Long.toHexString(employeePost.getEmployee().getPerson().getId()));
                    /* [2] */
                    fields.add(StringUtils.trimToNull(employeePost.getEmployee().getPerson().getIdentityCard().getLastName()));
                    /* [3] */
                    fields.add(StringUtils.trimToNull(employeePost.getEmployee().getPerson().getIdentityCard().getFirstName()));
                    /* [4] */
                    fields.add(StringUtils.trimToNull(employeePost.getEmployee().getPerson().getIdentityCard().getMiddleName()));
                    /* [5] */
                    fields.add(StringUtils.trimToNull(employeePost.getPostRelation().getTitle()));
                    /* [6] */
                    fields.add(StringUtils.trimToNull(employeePost.getOrgUnit().getTitle()));


                    rows.add(fields.toArray());
                }
            }
            catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();

        });
        employee_t.addRows(rows);

    }


    private void export_PersonNextOfKin(final Database mdb) throws Exception
    {
        if (null != mdb.getTable(PERSON_NEXTOFKIN_TABLE))
        {
            return;
        }

        final Table address_t = mdb.getTable("address_t") != null ? mdb.getTable("address_t") : new TableBuilder("address_t")
                .addColumn(new ColumnBuilder("id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("address_p", DataType.MEMO).setCompressedUnicode(true).setMaxLength().toColumn())
                .toTable(mdb);

        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
        final Map<RelationDegree, String> relationDegreeMap = catalogIO.catalogRelationDegree().export(mdb);
        final Map<IdentityCardType, String> identityCardTypeMap = catalogIO.catalogIdentityCardType().export(mdb);
        final Map<Sex, String> sexMap = catalogIO.catalogSex().export(mdb);

        final Table person_nextofkin_t = new TableBuilder(PERSON_NEXTOFKIN_TABLE)
                /* [0] */.addColumn(new ColumnBuilder(NEXTOFKIN_ID_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [1] */.addColumn(new ColumnBuilder(PERSON_ID_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [2] */.addColumn(new ColumnBuilder(RELATION_DEGREE_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).setMaxLength().toColumn())
                /* [3] */.addColumn(new ColumnBuilder(PASSPORT_TYPE_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).setMaxLength().toColumn())
                /* [4] */.addColumn(new ColumnBuilder(IDENTITY_SERIA_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).setMaxLength().toColumn())
                /* [5] */.addColumn(new ColumnBuilder(IDENTITY_NUMBER_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).setMaxLength().toColumn())
                /* [6] */.addColumn(new ColumnBuilder(IDENTITY_FIRST_NAME_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).setMaxLength().toColumn())
                /* [7] */.addColumn(new ColumnBuilder(IDENTITY_LAST_NAME_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).setMaxLength().toColumn())
                /* [8] */.addColumn(new ColumnBuilder(IDENTITY_MIDDLE_NAME_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).setMaxLength().toColumn())
                /* [9] */.addColumn(new ColumnBuilder(SEX_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [10] */.addColumn(new ColumnBuilder(IDENTITY_BIRTHDAY_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [11] */.addColumn(new ColumnBuilder(IDENTITY_DATE_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [12] */.addColumn(new ColumnBuilder(IDENTITY_CODE_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).setMaxLength().toColumn())
                /* [13] */.addColumn(new ColumnBuilder(IDENTITY_PLACE_COLUMN, DataType.MEMO).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [14] */.addColumn(new ColumnBuilder(EMPLOYEE_PLACE_COLUMN, DataType.MEMO).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [15] */.addColumn(new ColumnBuilder(PHONES_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [16] */.addColumn(new ColumnBuilder(ADDRESS_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [17] */.addColumn(new ColumnBuilder(CITIZENSHIP_ID_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())

                .toTable(mdb);

        final Session session = this.getSession();
        final List<Long> ids = new DQLSelectBuilder().fromEntity(PersonNextOfKin.class, "e").column(property("e.id")).createStatement(session).list();

        BatchUtils.execute(ids, 128, ids1 -> {
            try
            {
                for (final PersonNextOfKin nextOfKin : getList(PersonNextOfKin.class, "id", ids1))
                {
                    AddressBase address = nextOfKin.getAddress();
                    person_nextofkin_t.addRow(
                            /* [0] */Long.toHexString(nextOfKin.getId()),
                            /* [1] */Long.toHexString(nextOfKin.getPerson().getId()),
                            /* [2] */relationDegreeMap.get(nextOfKin.getRelationDegree()),
                            /* [3] */identityCardTypeMap.get(nextOfKin.getPassportType()),
                            /* [4] */StringUtils.trimToNull(nextOfKin.getPassportSeria()),
                            /* [5] */StringUtils.trimToNull(nextOfKin.getPassportNumber()),
                            /* [6] */StringUtils.trimToNull(nextOfKin.getFirstName()),
                            /* [7] */StringUtils.trimToNull(nextOfKin.getLastName()),
                            /* [8] */StringUtils.trimToNull(nextOfKin.getMiddleName()),
                            /* [9] */sexMap.get(nextOfKin.getSex()),
                            /* [10] */DateFormatter.DEFAULT_DATE_FORMATTER.format(nextOfKin.getBirthDate()),
                            /* [11] */DateFormatter.DEFAULT_DATE_FORMATTER.format(nextOfKin.getPassportIssuanceDate()),
                            /* [12] */StringUtils.trimToNull(nextOfKin.getPassportIssuanceCode()),
                            /* [13] */StringUtils.trimToNull(nextOfKin.getPassportIssuancePlace()),
                            /* [14] */StringUtils.trimToNull(nextOfKin.getEmploymentPlace()),
                            /* [15] */StringUtils.trimToNull(nextOfKin.getPhones()),
                            /* [16] */address != null ? Long.toHexString(address.getId()) : null,
                            /* [17] */ nextOfKin.getCitizenship() != null ? Long.toHexString(nextOfKin.getCitizenship().getId()) : null

                    );
                    if (address != null)
                    {
                        address_t.addRow(
                                Long.toHexString(address.getId()),
                                address.getTitleWithFlat()
                        );
                    }
                }
            }
            catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();
        });
    }

    private void export_EduProgramProf(final Database mdb) throws Exception
    {
        if (null != mdb.getTable(EDU_PROGRAM_TABLE))
        {
            return;
        }

        final Table edu_program_t = new TableBuilder(EDU_PROGRAM_TABLE)
                /* [0] */.addColumn(new ColumnBuilder(EDU_PROGRAM_ID_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [1] */.addColumn(new ColumnBuilder(EDU_PROGRAM_TITLE_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [2] */.addColumn(new ColumnBuilder(EDU_PROGRAM_SHORT_TITLE_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [3] */.addColumn(new ColumnBuilder(EDU_PROGRAM_YEAR_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [4] */.addColumn(new ColumnBuilder(EDU_PROGRAM_KIND_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [5] */.addColumn(new ColumnBuilder(EDU_PROGRAM_FORM_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [6] */.addColumn(new ColumnBuilder(EDU_PROGRAM_DURATION_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [7] */.addColumn(new ColumnBuilder(EDU_PROGRAM_TRAIT_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [8] */.addColumn(new ColumnBuilder(EDU_PROGRAM_OWNER_ORG_UNIT_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [9] */.addColumn(new ColumnBuilder(EDU_PROGRAM_INSTITUTION_ORG_UNIT_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [10] */.addColumn(new ColumnBuilder(EDU_PROGRAM_SUBJECT_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [11] */.addColumn(new ColumnBuilder(EDU_PROGRAM_QUALIFICATION_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [12] */.addColumn(new ColumnBuilder(EDU_PROGRAM_SPECIALIZATION_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [13] */.addColumn(new ColumnBuilder(EDU_PROGRAM_IN_DEPTH_STUDY_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())

                .toTable(mdb);

        final Session session = this.getSession();
        final List<Long> ids = new DQLSelectBuilder().fromEntity(EduProgramProf.class, "e").column(property("e.id"))
                .createStatement(session).list();
        final List<Object[]> rows = new ArrayList<>(ids.size());
        BatchUtils.execute(ids, 128, ids1 -> {
            try
            {
                for (final EduProgramProf eduProgram : getList(EduProgramProf.class, "id", ids1))
                {
                    if (eduProgram instanceof EduProgramHigherProf || eduProgram instanceof EduProgramSecondaryProf)
                    {
                        List<Object> fields = new ArrayList<>(13);

                            /* [0] */
                        fields.add(Long.toHexString(eduProgram.getId()));
                            /* [1] */
                        fields.add(StringUtils.trimToNull(eduProgram.getTitle()));
                            /* [2] */
                        fields.add(StringUtils.trimToNull(eduProgram.getShortTitle()));
                            /* [3] */
                        fields.add(Long.toHexString(eduProgram.getYear().getId()));
                            /* [4] */
                        fields.add(StringUtils.trimToNull(eduProgram.getKind().getTitle()));
                            /* [5] */
                        fields.add(StringUtils.trimToNull(eduProgram.getForm().getTitle()));
                            /* [6] */
                        fields.add(StringUtils.trimToNull(eduProgram.getDuration().getTitle()));
                            /* [7] */
                        fields.add(StringUtils.trimToNull(eduProgram.getEduProgramTrait() != null ? eduProgram.getEduProgramTrait().getTitle() : ""));
                            /* [8] */
                        fields.add(StringUtils.trimToNull(eduProgram.getOwnerOrgUnit().getTitle()));
                            /* [9] */
                        fields.add(StringUtils.trimToNull(eduProgram.getInstitutionOrgUnit().getTitle()));
                            /* [10] */
                        fields.add(StringUtils.trimToNull(eduProgram.getProgramSubject().getTitle()));
                            /* [11] */
                        fields.add(StringUtils.trimToNull(eduProgram.getProgramQualification().getTitle()));

                        if (eduProgram instanceof EduProgramHigherProf)
                        {
                            /* [12] */
                            fields.add(StringUtils.trimToNull(((EduProgramHigherProf) eduProgram).getProgramSpecialization().getDisplayableTitle()));
                            /* [13] */
                            fields.add(StringUtils.trimToNull(""));
                        }
                        else
                        {
                            /* [12] */
                            fields.add(StringUtils.trimToNull(""));
                            /* [13] */
                            fields.add(StringUtils.trimToNull(YesNoFormatter.INSTANCE.format(((EduProgramSecondaryProf) eduProgram).isInDepthStudy())));
                        }
                        rows.add(fields.toArray());
                    }

                }

            }
            catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();
        });
        edu_program_t.addRows(rows);
    }


    private void export_printTemplate(final Database mdb) throws Exception
    {
        if (null != mdb.getTable(CONTRACT_PRINT_TEMPLATE_TABLE))
        {
            return;
        }

        final Table edu_program_t = new TableBuilder(CONTRACT_PRINT_TEMPLATE_TABLE)
                /* [0] */.addColumn(new ColumnBuilder(CONTRACT_PRINT_TEMPLATE_ID_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [1] */.addColumn(new ColumnBuilder(CONTRACT_PRINT_TEMPLATE_TITLE_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())

                .toTable(mdb);

        final Session session = this.getSession();
        final List<Long> ids = new DQLSelectBuilder().fromEntity(CtrPrintTemplate.class, "e").column(property("e.id"))
                .where(in(property("e", CtrPrintTemplate.kind().code()),
                        Lists.newArrayList(CtrContractKindCodes.EDU_CONTRACT_VO_2_SIDES, CtrContractKindCodes.EDU_CONTRACT_VO_3_SIDES_PERSON,
                                CtrContractKindCodes.EDU_CONTRACT_SPO_2_SIDES, CtrContractKindCodes.EDU_CONTRACT_SPO_3_SIDES_PERSON)))
                .createStatement(session).list();
        final List<Object[]> rows = new ArrayList<>(ids.size());
        BatchUtils.execute(ids, 128, ids1 -> {
            try
            {
                for (final CtrPrintTemplate printTemplate : getList(CtrPrintTemplate.class, "id", ids1))
                {
                    List<Object> fields = new ArrayList<>(2);

                            /* [0] */
                    fields.add(Long.toHexString(printTemplate.getId()));
                            /* [1] */
                    fields.add(StringUtils.trimToNull(printTemplate.getKind().getShortTitle() + " (" + printTemplate.getShortTitle() + ")"));

                    rows.add(fields.toArray());
                }

            }
            catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();
        });
        edu_program_t.addRows(rows);
    }

    private void export_contract(final Database mdb, final List<Long> contractIds) throws Exception
    {
        final Map<CtrContractObject, EduCtrStudentContract> contractStudentMap = new HashMap<>();
        final Map<CtrContractVersion, EduCtrStudentContractTemplateData> contractVersionTemplateMap = new HashMap<>();
        final Map<CtrContractVersion, EduCtrEducationPromise> eduCtrEducationPromiseMap = new HashMap<>();
        final Map<CtrContractVersion, CtrContractPromice> ctrPaymentPromiseMap = new HashMap<>();
        final Map<PhysicalContactor, PersonNextOfKin2physicalContactorRelation> physicalContactorMap = new HashMap<>();

        if (null != mdb.getTable(CONTRACT_TABLE))
        {
            return;
        }
        final Table contract_t = new TableBuilder(CONTRACT_TABLE)
                /* [0] */.addColumn(new ColumnBuilder(CONTRACT_ID_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [1] */.addColumn(new ColumnBuilder(STUDENT_ID_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [2] */.addColumn(new ColumnBuilder(CONTRACT_NUMBER_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [3] */.addColumn(new ColumnBuilder(CONTRACT_REG_DATE_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [4] */.addColumn(new ColumnBuilder(CONTRACT_BEGIN_DATE_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [5] */.addColumn(new ColumnBuilder(CONTRACT_END_DATE_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [6] */.addColumn(new ColumnBuilder(COMMENT_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [7] */.addColumn(new ColumnBuilder(CONTRACT_CIPHER_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [8] */.addColumn(new ColumnBuilder(CONTRACT_EDU_YEAR_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [9] */.addColumn(new ColumnBuilder(CONTRACT_PRINT_TEMPLATE_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [10] */.addColumn(new ColumnBuilder(EDU_PROGRAM_ID_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [11] */.addColumn(new ColumnBuilder(EMPLOYEE_ID_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [12] */.addColumn(new ColumnBuilder(PAYER_ID_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [13] */.addColumn(new ColumnBuilder(CONTRACT_ACTIVATION_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())

                .toTable(mdb);
        fillPhysicalContactorMap(physicalContactorMap);
        getStudentContractRelData(contractIds, contractStudentMap);
        getContractVersionTemplateData(contractVersionTemplateMap);
        getEduCtrEducationPromiseData(eduCtrEducationPromiseMap);
        getCtrPaymentPromiceData(ctrPaymentPromiseMap);
        final Session session = this.getSession();

        final List<Long> ctrVerIds = new ArrayList<>();
        for (List<Long> idsPart : Iterables.partition(contractIds, DQL.MAX_VALUES_ROW_NUMBER))
        {
            List<Long> ctrVerIdsPart = new DQLSelectBuilder().fromEntity(CtrContractVersion.class, "e").column(property("e.id"))
                .where(in(property("e", CtrContractVersion.contract().id()), idsPart))
                .createStatement(session).list();

            ctrVerIds.addAll(ctrVerIdsPart);
        }


        final List<Object[]> rows = new ArrayList<>(ctrVerIds.size());
        BatchUtils.execute(ctrVerIds, 128, ids -> {
            try
            {
                List<CtrContractVersion> contractVersionList = getList(CtrContractVersion.class, "id", ids);
                for (CtrContractVersion contractVersion : contractVersionList)
                {

                    if ((contractVersion.getActivationDate() != null && contractVersion.getRemovalDate() == null) ||
                            isLastContractVersion(contractVersion, contractVersionList))
                    {
                        List<Object> fields = new ArrayList<>(13);
                        EducationYear year = contractVersionTemplateMap.get(contractVersion) != null ? contractVersionTemplateMap.get(contractVersion).getEducationYear() : null;
                        /* [0] */
                        fields.add(Long.toHexString(contractVersion.getContract().getId()));
                        /* [1] */
                        fields.add(Long.toHexString(contractStudentMap.get(contractVersion.getContract()).getStudent().getId()));
                        /* [2] */
                        fields.add(StringUtils.trimToNull(contractVersion.getContract().getNumber()));
                        /* [3] */
                        fields.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(contractVersion.getContractRegistrationDate()));
                        /* [4] */
                        fields.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(contractVersion.getDurationBeginDate()));
                        /* [5] */
                        fields.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(contractVersion.getDurationEndDate()));
                        /* [6] */
                        fields.add(StringUtils.trimToNull(contractVersion.getContract().getComment()));
                        /* [7] */
                        fields.add(StringUtils.trimToNull(contractVersionTemplateMap.get(contractVersion) != null ?
                                                                  contractVersionTemplateMap.get(contractVersion).getCipher() : ""));

                        /* [8] */
                        fields.add(year != null ? Long.toHexString(year.getId()) : "");

                        /* [9] */
                        fields.add(contractVersion.getPrintTemplate() != null ? Long.toHexString(contractVersion.getPrintTemplate().getId()) : "");

                        /* [10] */
                        fields.add(Long.toHexString((eduCtrEducationPromiseMap.get(contractVersion) != null ?
                                eduCtrEducationPromiseMap.get(contractVersion).getEduProgram().getId() : 0)));

                        /* [11] */
                        fields.add(Long.toHexString(getEduPromiseEmployeeId(contractVersion, eduCtrEducationPromiseMap)));

                        //Long payerId = getPayerId(contractVersion, _ctrPaymentPromiseMap, _physicalContactorMap);
                        /* [12] */
                        fields.add("");
                        /* [13] */
                        fields.add("");

                        rows.add(fields.toArray());

                    }

                }
            }
            catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();
        });
        contract_t.addRows(rows);

    }


    private void export_paymentPromise(final Database mdb,
                                       final List<Long> contractIds) throws Exception
    {
        if (null != mdb.getTable(PAYMENT_PROMISE_TABLE))
        {
            return;
        }

        final Table payment_promise_t = new TableBuilder(PAYMENT_PROMISE_TABLE)
                /* [0] */.addColumn(new ColumnBuilder(CONTRACT_ID_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [1] */.addColumn(new ColumnBuilder(COST_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [2] */.addColumn(new ColumnBuilder(STAGE_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [3] */.addColumn(new ColumnBuilder(DEADLINE_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [4] */.addColumn(new ColumnBuilder(COMMENT_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())


                .toTable(mdb);
        final Session session = this.getSession();

        final List<Long> ctrPromiseIds = new ArrayList<>();
        for (List<Long> idsPart : Iterables.partition(contractIds, DQL.MAX_VALUES_ROW_NUMBER))
        {
            List<Long> ctrPromiseIdsPart = new DQLSelectBuilder().fromEntity(CtrPaymentPromice.class, "e").column(property("e.id"))
                    .where(in(property("e", CtrPaymentPromice.src().owner().contract().id()), idsPart))
                    .createStatement(session).list();
            ctrPromiseIds.addAll(ctrPromiseIdsPart);
        }


        final List<Object[]> rows = new ArrayList<>(ctrPromiseIds.size());
        BatchUtils.execute(ctrPromiseIds, 128, ids1 -> {
            try
            {
                for (CtrPaymentPromice paymentPromice : getList(CtrPaymentPromice.class, "id", ids1))
                {

                    List<Object> fields = new ArrayList<>(6);

                    /* [0] */
                    fields.add(Long.toHexString(paymentPromice.getSrc().getOwner().getContract().getId()));
                    /* [1] */
                    fields.add(StringUtils.trimToNull(String.valueOf(paymentPromice.getCostAsLong())));
                    /* [2] */
                    fields.add(StringUtils.trimToNull(paymentPromice.getStage()));
                    /* [3] */
                    fields.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(paymentPromice.getDeadlineDate()));
                    /* [4] */
                    fields.add(StringUtils.trimToNull(paymentPromice.getComment()));

                    rows.add(fields.toArray());


                }
            }
            catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();
        });
        payment_promise_t.addRows(rows);
    }


    private void export_paymentResult(final Database mdb,
                                      final List<Long> contractIds) throws Exception
    {
        if (null != mdb.getTable(PAYMENT_RESULT_TABLE))
        {
            return;
        }

        final Table payment_result_t = new TableBuilder(PAYMENT_RESULT_TABLE)
                /* [0] */.addColumn(new ColumnBuilder(CONTRACT_ID_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [1] */.addColumn(new ColumnBuilder(COST_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [2] */.addColumn(new ColumnBuilder(PAYMENT_CHECK_NUMBER_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [3] */.addColumn(new ColumnBuilder(PAYMENT_ORDER_NUMBER_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [4] */.addColumn(new ColumnBuilder(COMMENT_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())
                /* [5] */.addColumn(new ColumnBuilder(TIMESTAMP_COLUMN, DataType.TEXT).setMaxLength().setCompressedUnicode(true).toColumn())


                .toTable(mdb);
        final Session session = this.getSession();

        final List<Long> ctrPaymentIds = new ArrayList<>();
        for (List<Long> idsPart : Iterables.partition(contractIds, DQL.MAX_VALUES_ROW_NUMBER))
        {
            List<Long> ctrPaymentIdsPart = new DQLSelectBuilder().fromEntity(CtrPaymentResult.class, "e").column(property("e.id"))
                    .where(in(property("e", CtrPaymentResult.contract().id()), idsPart))
                    .createStatement(session).list();

            ctrPaymentIds.addAll(ctrPaymentIdsPart);
        }

        final List<Object[]> rows = new ArrayList<>(ctrPaymentIds.size());
        BatchUtils.execute(ctrPaymentIds, 128, ids1 -> {
            try
            {
                for (CtrPaymentResult paymentResult : getList(CtrPaymentResult.class, "id", ids1))
                {

                    List<Object> fields = new ArrayList<>(6);

                    /* [0] */
                    fields.add(Long.toHexString(paymentResult.getContract().getId()));
                    /* [1] */
                    fields.add(StringUtils.trimToNull(String.valueOf(paymentResult.getCostAsLong())));
                    /* [2] */
                    fields.add(StringUtils.trimToNull(paymentResult.getPaymentDocumentNumber()));
                    /* [3] */
                    fields.add(StringUtils.trimToNull(paymentResult.getSourceDocumentNumber()));
                    /* [4] */
                    fields.add(StringUtils.trimToNull(paymentResult.getComment()));
                    /* [5] */
                    fields.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(paymentResult.getTimestamp()));

                    rows.add(fields.toArray());
                }
            }
            catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();
        });
        payment_result_t.addRows(rows);
    }


    private void export_eduYear(final Database mdb) throws Exception
    {
        if (null != mdb.getTable("eduYear_t"))
        {
            return;
        }
        final Table eduYear_t = mdb.getTable("eduYear_t") != null ? mdb.getTable("eduYear_t") : new TableBuilder("eduYear_t")
                .addColumn(new ColumnBuilder("id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("title", DataType.MEMO).setCompressedUnicode(true).setMaxLength().toColumn())
                .toTable(mdb);

        List<EducationYear> educationYearList = getList(EducationYear.class, EducationYear.P_INT_VALUE);
        List<Object[]> rows = new ArrayList<>(educationYearList.size());

        for (EducationYear year : educationYearList)
        {
            List<Object> fields = new ArrayList<>(6);

                        /* [0] */
            fields.add(Long.toHexString(year.getId()));
                        /* [1] */
            fields.add(StringUtils.trimToNull(year.getTitle()));

            rows.add(fields.toArray());
        }

        getSession().clear();


        eduYear_t.addRows(rows);

    }

    private void getContractVersionTemplateData(final Map<CtrContractVersion, EduCtrStudentContractTemplateData> contractVersionTemplateMap) throws Exception
    {
        final Session session = this.getSession();
        List<Long> ids = new DQLSelectBuilder().fromEntity(EduCtrStudentContractTemplateData.class, "e").column(property("e.id"))
                .createStatement(session).list();
        BatchUtils.execute(ids, 128, ids1 -> {
            try
            {
                for (EduCtrStudentContractTemplateData contractTemplateData : getList(EduCtrStudentContractTemplateData.class, "id", ids1))
                {
                    contractVersionTemplateMap.put(contractTemplateData.getOwner(), contractTemplateData);
                }
            }
            catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();
        });
    }


    private boolean isLastContractVersion(CtrContractVersion contractVersion, List<CtrContractVersion> contractVersionList)
    {
        boolean isLast = true;
        boolean isActiveCtrVersionExist = false;

        for (CtrContractVersion item : contractVersionList)
        {
            //Ищем активные версии для договора
            if (item.getContract().getId().equals(contractVersion.getContract().getId()) &&
                    (contractVersion.getActivationDate() != null && contractVersion.getRemovalDate() == null))
            {
                isActiveCtrVersionExist = true;
                break;
            }
        }

        //Если в договоре есть активные версии или версии с более поздне датой создания, то не включаем contractVersion в экспорт
        for (CtrContractVersion item : contractVersionList)
        {

            if (item.getContract().getId().equals(contractVersion.getContract().getId())
                    && (item.getCreationDate().after(contractVersion.getCreationDate()) ||
                    isActiveCtrVersionExist))
            {
                isLast = false;
                break;
            }
        }

        return isLast;
    }


    private void getStudentContractRelData(final List<Long> contractIds,
                                           final Map<CtrContractObject, EduCtrStudentContract> contractStudentMap) throws Exception
    {
        final Session session = this.getSession();
        List<Long> ids = new DQLSelectBuilder().fromEntity(EduCtrStudentContract.class, "e").column(property("e.id"))
                .createStatement(session).list();
        BatchUtils.execute(ids, 128, ids1 -> {
            try
            {
                for (final EduCtrStudentContract studentContract : getList(EduCtrStudentContract.class, "id", ids1))
                {
                    contractStudentMap.put(studentContract.getContractObject(), studentContract);
                    contractIds.add(studentContract.getContractObject().getId());
                }
            }
            catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();
        });
    }

    private void getEduCtrEducationPromiseData(final Map<CtrContractVersion, EduCtrEducationPromise> eduCtrEducationPromiseMap) throws Exception
    {
        final Session session = this.getSession();
        List<Long> ids = new DQLSelectBuilder().fromEntity(EduCtrEducationPromise.class, "e").column(property("e.id"))
                .createStatement(session).list();
        BatchUtils.execute(ids, 128, ids1 -> {
            try
            {
                for (final EduCtrEducationPromise ctrEducationPromise : getList(EduCtrEducationPromise.class, "id", ids1))
                {

                    eduCtrEducationPromiseMap.put(ctrEducationPromise.getDst().getOwner(), ctrEducationPromise);
                }

            }
            catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();
        });
    }

    private void getCtrPaymentPromiceData(final Map<CtrContractVersion, CtrContractPromice> ctrPaymentPromiseMap) throws Exception
    {
        final Session session = this.getSession();
        List<Long> ids = new DQLSelectBuilder().fromEntity(CtrPaymentPromice.class, "e").column(property("e.id"))
                .createStatement(session).list();
        BatchUtils.execute(ids, 128, ids1 -> {
            try
            {
                for (final CtrPaymentPromice ctrPaymentPromice : getList(CtrPaymentPromice.class, "id", ids1))
                {

                    ctrPaymentPromiseMap.put(ctrPaymentPromice.getDst().getOwner(), ctrPaymentPromice);
                }

            }
            catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();
        });
    }

    private Long getEduPromiseEmployeeId(CtrContractVersion contractVersion, Map<CtrContractVersion, EduCtrEducationPromise> eduCtrEducationPromiseMap)
    {
        Long employeeId = 0L;
        if (eduCtrEducationPromiseMap.get(contractVersion) != null)
        {
            if (eduCtrEducationPromiseMap.get(contractVersion).getSrc().getContactor() instanceof EmployeePostContactor)
            {
                employeeId = ((EmployeePostContactor) eduCtrEducationPromiseMap.get(contractVersion).getSrc().getContactor()).getEmployeePost().getId();
            }
        }
        return employeeId;
    }

//    private Long getPayerId(CtrContractVersion contractVersion, Map<CtrContractVersion, CtrContractPromice> ctrPaymentPromiceMap,
//                            Map<PhysicalContactor, PersonNextOfKin2physicalContactorRelation> physicalContactorMap)
//    {
//        Long payerId = _contractStudentMap.get(contractVersion.getContract()) != null ? _contractStudentMap.get(contractVersion.getContract()).getStudent().getId() : 0L;
//        if (ctrPaymentPromiceMap.get(contractVersion) != null)
//        {
//            if (ctrPaymentPromiceMap.get(contractVersion).getSrc().getContactor() instanceof PhysicalContactor)
//            {
//                PhysicalContactor physicalContactor = (PhysicalContactor) ctrPaymentPromiceMap.get(contractVersion).getSrc().getContactor();
//
//                PersonNextOfKin2physicalContactorRelation nextOfKin2physicalContactorRelation = physicalContactorMap.get(physicalContactor);
//
//
//                payerId = (nextOfKin2physicalContactorRelation != null && nextOfKin2physicalContactorRelation.getPhysicalContactor() != null) ?
//                        nextOfKin2physicalContactorRelation.getPersonNextOfKin().getId() : 0L;
//            }
//        }
//        return payerId;
//    }

    private void fillPhysicalContactorMap(final Map<PhysicalContactor, PersonNextOfKin2physicalContactorRelation> physicalContactorMap)
    {
        List<PersonNextOfKin2physicalContactorRelation> nextOfKin2physicalContactorRelationList = getList(PersonNextOfKin2physicalContactorRelation.class);

        for (PersonNextOfKin2physicalContactorRelation nextOfKin2physicalContactorRel : nextOfKin2physicalContactorRelationList)
        {
            physicalContactorMap.put(nextOfKin2physicalContactorRel.getPhysicalContactor(), nextOfKin2physicalContactorRel);
        }
    }


    private PersonNextOfKin doImport_PersonNextOfKinList(Table person_nextofkin_t, final String ctrNextOfKinId, final Map<String, Long> personIdMap,
                                                         final Map<String, Map<String, Object>> addressMap, final Map<String, RelationDegree> relationDegreeMap,
                                                         Map<String, Sex> sexMap, final Map<String, IdentityCardType> identityCardTypeMap, final IFieldPropertyMeta passportIssuancePlaceProperty,
                                                         final IFieldPropertyMeta employmentPlaceProperty)
    {

        final Session session = getSession();
        final PersonNextOfKin[] personNextOfKin = new PersonNextOfKin[1];
        execute(person_nextofkin_t, 64, "Ближайшие родственики персон", new BatchUtils.Action<Map<String, Object>>()
        {
            private final Map<String, Person> personMap = SafeMap.get(id -> {
                final Long personId = personIdMap.get(id);
                if (null == personId) {
                    throw new IllegalStateException("Person with id '" + id + "'is not found in the database.");
                }
                return (Person) session.load(Person.class, personId);
            });


            @Override
            public void execute(final Collection<Map<String, Object>> rows)
            {


                for (final Map<String, Object> row : rows)
                {

                    String nextOfKinId = (String) row.get(NEXTOFKIN_ID_COLUMN);
                    Long longNextOfKinId = tryParseHexId(nextOfKinId);
                    if (longNextOfKinId != null && longNextOfKinId > 0 && longNextOfKinId <= 100000 && nextOfKinId.equals(ctrNextOfKinId))
                    {
                        String personId = (String) row.get(PERSON_ID_COLUMN);
                        String context = PERSON_NEXTOFKIN_TABLE + "[" + PERSON_ID_COLUMN + "=" + personId + "]";

                        final Person person = personMap.get(personId);
                        personNextOfKin[0] = new PersonNextOfKin(person);

                        String firstName = StringUtils.trimToNull((String) row.get(IDENTITY_FIRST_NAME_COLUMN));

                        String lastName = StringUtils.trimToNull((String) row.get(IDENTITY_LAST_NAME_COLUMN));
                        if (firstName == null || lastName == null)
                        {
                            throw new IllegalArgumentException("Values in fields 'identity_firstName' and 'identity_lastName' must not be null.");
                        }
                        String relationDegree = (String) row.get(RELATION_DEGREE_COLUMN);
                        personNextOfKin[0].setRelationDegree(relationDegreeMap.get(relationDegree));

                        String identityCardType = (String) row.get(PASSPORT_TYPE_COLUMN);
                        personNextOfKin[0].setPassportType(identityCardTypeMap.get(identityCardType));

                        personNextOfKin[0].setPassportSeria(trimToEmpty((String) row.get(IDENTITY_SERIA_COLUMN)));
                        personNextOfKin[0].setPassportNumber(trimToEmpty((String) row.get(IDENTITY_NUMBER_COLUMN)));
                        personNextOfKin[0].setFirstName(firstName);
                        personNextOfKin[0].setLastName(lastName);
                        personNextOfKin[0].setMiddleName(trimToEmpty((String) row.get(IDENTITY_MIDDLE_NAME_COLUMN)));

                        String sexCode = (String)row.get(SEX_COLUMN);

                        if(personNextOfKin[0].getRelationDegree().getSex() == null && !StringUtils.isEmpty(sexCode))
                        {
                            personNextOfKin[0].setSex(sexMap.get(sexCode));
                        }

                        personNextOfKin[0].setPassportIssuanceCode(trimToEmpty((String) row.get(IDENTITY_CODE_COLUMN)));
                        personNextOfKin[0].setPassportIssuancePlace(trimmedString(row, IDENTITY_PLACE_COLUMN, context, passportIssuancePlaceProperty));
                        personNextOfKin[0].setBirthDate(date(row, IDENTITY_BIRTHDAY_COLUMN, context, false));
                        personNextOfKin[0].setPassportIssuanceDate(date(row, IDENTITY_DATE_COLUMN, context, false));
                        personNextOfKin[0].setEmploymentPlace(trimmedString(row, EMPLOYEE_PLACE_COLUMN, context, employmentPlaceProperty));
                        personNextOfKin[0].setPhones(trimToEmpty((String) row.get(PHONES_COLUMN)));
                        Long citizenshipId = tryParseHexId((String) row.get(CITIZENSHIP_ID_COLUMN));
                        personNextOfKin[0].setCitizenship(get(AddressCountry.class, AddressCountry.P_ID, citizenshipId));

                        {
                            final String address_id = (String) row.get("address_id");
                            final Long addressId = tryParseHexId(address_id);

                            if (addressId != null)
                            {
                                // Если id совпадает с уже существующим адресом, то оставляем существующий адрес
                                if (personNextOfKin[0].getAddress() != null && personNextOfKin[0].getAddress().getId().equals(addressId))
                                    return;

                                final Map<String, Object> addressRow = addressMap.get(address_id);
                                if (addressRow == null)
                                {
                                    log4j_logger.error("Table address_t does not contain row with id=" + address_id + ", specified in column 'address_id' in table person_nextofkin_t in row with person_id=" + personId);
                                    return;
                                }

                                final String addressString = (String) addressRow.get("address_p");
                                if (!StringUtils.isEmpty(addressString))
                                {
                                    AddressString newAddress = new AddressString();
                                    newAddress.setAddress(addressString);
                                    AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(personNextOfKin[0], newAddress, PersonNextOfKin.L_ADDRESS);
                                }
                            }
                        }

                        save(personNextOfKin[0]);
                        session.flush();
                    }
                    else if (nextOfKinId.equals(ctrNextOfKinId))
                    {
                        personNextOfKin[0] = get(PersonNextOfKin.class, PersonNextOfKin.P_ID, longNextOfKinId);
                    }
                }
                session.flush();
                session.clear();

            }
        });
        return personNextOfKin[0];
    }

    protected Map<String, Long> doImport_ContractData(final Database mdb) throws Exception
    {
        final Session session = getSession();
        final Map<String, Long> contractMap = new HashMap<>();
        Table contract_t = mdb.getTable(CONTRACT_TABLE);
        if (null == contract_t)
        {
            throw new ApplicationException("Table «" + CONTRACT_TABLE + "» does not exist.");
        }

        final Table payment_promise_t = mdb.getTable(PAYMENT_PROMISE_TABLE);
        if (null == payment_promise_t)
        {
            throw new ApplicationException("Table «" + PAYMENT_PROMISE_TABLE + "» does not exist.");
        }

        final Table payment_result_t = mdb.getTable(PAYMENT_RESULT_TABLE);
        if (null == payment_result_t)
        {
            throw new ApplicationException("Table «" + PAYMENT_RESULT_TABLE + "» does not exist.");
        }

        //Таблицы для импорта родственников студентов
        final Table person_nextofkin_t = mdb.getTable(PERSON_NEXTOFKIN_TABLE);
        if (null == person_nextofkin_t)
        {
            throw new ApplicationException("Table «" + PERSON_NEXTOFKIN_TABLE + "» does not exist.");
        }
        Table person_t = mdb.getTable("person_t");
        if (null == person_t)
        {
            throw new ApplicationException("Table «person_t» does not exist.");
        }
        Table address_t = mdb.getTable("address_t");
        if (null == address_t)
        {
            throw new ApplicationException("Table «address_t» does not exist.");
        }

        final Map<String, Long> personIdMap = new HashMap<>();
        execute(person_t, 128, "Персоны", rows -> {
            for (final Map<String, Object> row : rows)
            {
                String id = (String) row.get("id");
                Long personId = tryParseHexId((String) row.get("id"));
                personIdMap.put(id, personId);
            }
        });

        final Map<String, Map<String, Object>> addressMap = new HashMap<>();
        execute(address_t, 128, "Адреса", rows -> {
            for (final Map<String, Object> row : rows)
            {
                final String id = (String) row.get("id");
                addressMap.put(id, row);
            }
        });

        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
        final Map<String, RelationDegree> relationDegreeMap = catalogIO.catalogRelationDegree().lookup(true);
        final Map<String, Sex> sexMap = catalogIO.catalogSex().lookup(true);
        final Map<String, IdentityCardType> identityCardTypeMap = catalogIO.catalogIdentityCardType().lookup(true);
        final IFieldPropertyMeta passportIssuancePlaceProperty = getFieldProperty(PersonNextOfKin.class, PersonNextOfKin.P_PASSPORT_ISSUANCE_PLACE);
        final IFieldPropertyMeta employmentPlaceProperty = getFieldProperty(PersonNextOfKin.class, PersonNextOfKin.P_EMPLOYMENT_PLACE);
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        execute(contract_t, 64, "Договоры об обучении", rows -> {
            for (final Map<String, Object> row : rows)
            {
                final String contractId = (String) row.get(CONTRACT_ID_COLUMN);
                if (StringUtils.isNumeric(contractId) && Long.parseLong(contractId) > 0 && Long.parseLong(contractId) <= 100000)
                {
                    String context = CONTRACT_TABLE + "[" + CONTRACT_ID_COLUMN + "=" + contractId + "]";

                    EduProgramProf eduProgramProf = get(EduProgramProf.class, tryParseHexId((String) row.get(EDU_PROGRAM_ID_COLUMN)));

                    CtrContractObject contractObject = new CtrContractObject();
                    if(eduProgramProf.getProgramSubject().getSubjectIndex().getProgramKind().isProgramHigherProf())
                    {
                        contractObject.setType(getByCode(CtrContractType.class, CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE_V_O));
                    }
                    else if(eduProgramProf.getProgramSubject().getSubjectIndex().getProgramKind().isProgramSecondaryProf())
                    {
                        contractObject.setType(getByCode(CtrContractType.class, CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE_S_P_O));
                    }
                    else
                    {
                        throw new ApplicationException("Образовательная программа для договора может быть только ВО или СПО.");
                    }

                    Student student = get(Student.class, tryParseHexId((String) row.get(STUDENT_ID_COLUMN)));
                    //создаем договор
                    contractObject.setOrgUnit(student.getEducationOrgUnit().getFormativeOrgUnit());
                    contractObject.setNumber((String) row.get(CONTRACT_NUMBER_COLUMN));

                    //связь договора со студентом
                    EduCtrStudentContract studentContract = new EduCtrStudentContract();
                    studentContract.setStudent(student);
                    studentContract.setContractObject(contractObject);

                    //версия договора
                    CtrContractVersion contractVersion = new CtrContractVersion();
                    contractVersion.setContract(contractObject);
                    contractVersion.setNumber((String) row.get(CONTRACT_NUMBER_COLUMN));
                    contractVersion.setContractRegistrationDate(date(row, CONTRACT_REG_DATE_COLUMN, context, true));
                    contractVersion.setDurationBeginDate(date(row, CONTRACT_BEGIN_DATE_COLUMN, context, true));
                    contractVersion.setDurationEndDate(date(row, CONTRACT_END_DATE_COLUMN, context, true));
                    contractVersion.setCreator(UserContext.getInstance().getPrincipalContext());
                    contractVersion.setPrint(null);
                    contractVersion.setComment((String) row.get(COMMENT_COLUMN));
                    contractVersion.setCreationDate(new Date());
                    CtrPrintTemplate printTemplate = get(CtrPrintTemplate.class, tryParseHexId((String) row.get(CONTRACT_PRINT_TEMPLATE_COLUMN)));
                    contractVersion.setPrintTemplate(printTemplate);
                    contractVersion.setKind(printTemplate.getKind());


                    EduCtrContractVersionTemplateData contractTemplateData = null;

                    if(eduProgramProf.getProgramSubject().getSubjectIndex().getProgramKind().isProgramHigherProf())
                    {
                        //Данные шаблона договора на обучение студента по ОП
                        contractTemplateData = new EduCtrStudentContractTemplateData();
                        contractTemplateData.setOwner(contractVersion);
                        EducationYear year = get(EducationYear.class, tryParseHexId((String) row.get(CONTRACT_EDU_YEAR_COLUMN)));
                        contractTemplateData.setEducationYear(year);
                        contractTemplateData.setCipher((String) row.get(CONTRACT_CIPHER_COLUMN));
                        contractVersion.setDocStartDate(date(row, CONTRACT_REG_DATE_COLUMN, context, true));
                    }
                    else if(eduProgramProf.getProgramSubject().getSubjectIndex().getProgramKind().isProgramSecondaryProf())
                    {
                        //Данные шаблона договора на обучение студента по ОП
                        contractTemplateData = new EduCtrStudentSpoContractTemplateData();
                        contractTemplateData.setOwner(contractVersion);
                        EducationYear year = get(EducationYear.class, tryParseHexId((String) row.get(CONTRACT_EDU_YEAR_COLUMN)));
                        contractTemplateData.setEducationYear(year);
                        contractTemplateData.setCipher((String) row.get(CONTRACT_CIPHER_COLUMN));
                        contractVersion.setDocStartDate(date(row, CONTRACT_REG_DATE_COLUMN, context, true));
                    }

                    //Контрагент в версии договора (сторона ВУЗа) находим уже существующего или создаем контрагента на основании данных о сотруднике:
                    Long emloyeePostId = tryParseHexId((String) row.get(EMPLOYEE_ID_COLUMN));
                    EmployeePost employeePost = get(EmployeePost.class, EmployeePost.P_ID, emloyeePostId);
                    EmployeePostContactor employeePostContactor = ContactorManager.instance().dao().saveEmployeePostContactor(employeePost, employeePost.getPhone(), employeePost.getEmployee().getPerson().getContactData().getEmail(), false);
                    CtrContractVersionContractor contractVersionContractorEmployee = new CtrContractVersionContractor();
                    contractVersionContractorEmployee.setContactor(employeePostContactor);
                    contractVersionContractorEmployee.setOwner(contractVersion);
                    contractVersionContractorEmployee.setRequireSignature(true);

                    //Контрагент в версии договора по обучению (сторона студента) находим уже существующего или создаем контрагента на основании данных о студенте:
                    PhysicalContactor physicalContactorStudent = ContactorManager.instance().dao().savePhysicalContactor(student.getPerson(), student.getPerson().getContactData().getPhoneDefault(), student.getPerson().getContactData().getEmail(), false);
                    CtrContractVersionContractor contractVersionContractorStudent = new CtrContractVersionContractor();
                    contractVersionContractorStudent.setContactor(physicalContactorStudent);
                    contractVersionContractorStudent.setOwner(contractVersion);
                    contractVersionContractorStudent.setRequireSignature(true);

                    //Контрагент в версии договора по обучению (сторона студента) находим уже существующего или создаем контрагента на основании данных о родственнике студенте:
                    PersonNextOfKin personNextOfKin = doImport_PersonNextOfKinList(person_nextofkin_t, (String) row.get(PAYER_ID_COLUMN), personIdMap, addressMap, relationDegreeMap, sexMap,
                                                                                   identityCardTypeMap, passportIssuancePlaceProperty, employmentPlaceProperty);
                    CtrContractVersionContractor contractVersionContractorPayer = getContractorPayer(row, contractVersionContractorStudent, contractId, contractVersion, personNextOfKin);

                    //Данные по ОП для "обязательства по обучения по ОП"
                    EduCtrEducationPromise ctrEducationPromise = new EduCtrEducationPromise();
                    ctrEducationPromise.setEduProgram(eduProgramProf);
                    ctrEducationPromise.setDst(contractVersionContractorStudent);
                    ctrEducationPromise.setSrc(contractVersionContractorEmployee);
                    ctrEducationPromise.setDeadlineDate(contractVersion.getDurationBeginDate());


                    save(contractObject);
                    save(contractVersion);
                    save(contractTemplateData);
                    save(studentContract);
                    save(contractVersionContractorEmployee);
                    save(new CtrContractVersionContractorRole(contractVersionContractorEmployee, IUniBaseDao.instance.get().getCatalogItem(CtrContractRole.class, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER)));
                    save(contractVersionContractorStudent);
                    saveOrUpdate(contractVersionContractorPayer);
                    save(new CtrContractVersionContractorRole(contractVersionContractorPayer, IUniBaseDao.instance.get().getCatalogItem(CtrContractRole.class, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER)));
                    save(ctrEducationPromise);
                    doImport_PaymentPromiceData(payment_promise_t, contractId, contractVersionContractorEmployee, contractVersionContractorPayer);

                    doImport_PaymentResultData(payment_result_t, contractId, contractVersionContractorEmployee, contractVersionContractorPayer, contractObject);
                    session.flush();
                    if (null == row.get(CONTRACT_ACTIVATION_COLUMN))
                    {
                        session.clear();
                        // to avoid org.springframework.orm.hibernate3.HibernateSystemException: a different object with the same identifier value was already associated with the session
                        session.saveOrUpdate(contractVersion);
                        CtrContractVersionManager.instance().dao().doActivate(contractVersion, false);
                        contractMap.put(contractId, contractObject.getId());
                    }
                }
            }

        });
        return contractMap;
    }

    private void doImport_PaymentPromiceData(Table payment_promise_t, final String contractObjectId,
                                             final CtrContractVersionContractor contractVersionContractorEmployee,
                                             final CtrContractVersionContractor contractVersionContractorPayer)
    {
        execute(payment_promise_t, 64, "Договоры об оплате", rows -> {
            for (final Map<String, Object> row : rows)
            {
                String contractId = (String) row.get(CONTRACT_ID_COLUMN);
                if (contractId.equals(contractObjectId))
                {
                    String context = PAYMENT_PROMISE_TABLE + "[" + CONTRACT_ID_COLUMN + "=" + row.get(CONTRACT_ID_COLUMN) + "]";
                    CtrPaymentPromice paymentPromice = new CtrPaymentPromice();
                    paymentPromice.setDst(contractVersionContractorEmployee);
                    paymentPromice.setSrc(contractVersionContractorPayer);
                    paymentPromice.setCostAsLong(Long.parseLong((String) row.get(COST_COLUMN)));
                    paymentPromice.setStage((String) row.get(STAGE_COLUMN));
                    paymentPromice.setDeadlineDate(date(row, DEADLINE_COLUMN, context, true));
                    paymentPromice.setComment((String) row.get(COMMENT_COLUMN));
                    paymentPromice.setCurrency(getByCode(Currency.class, CurrencyCodes.RUB));
                    save(paymentPromice);
                }
            }
        }
        );
    }

    private void doImport_PaymentResultData(Table payment_result_t, final String contractObjectId,
                                            final CtrContractVersionContractor contractVersionContractorEmployee,
                                            final CtrContractVersionContractor contractVersionContractorPayer,
                                            final CtrContractObject contractObject)
    {
        execute(payment_result_t, 64, "Данные по платежам", rows -> {
            for (final Map<String, Object> row : rows)
            {
                String contractId = (String) row.get(CONTRACT_ID_COLUMN);
                if (contractId.equals(contractObjectId))
                {
                    String context = PAYMENT_RESULT_TABLE + "[" + CONTRACT_ID_COLUMN + "=" + row.get(CONTRACT_ID_COLUMN) + "]";
                    CtrPaymentResult paymentResult = new CtrPaymentResult();
                    paymentResult.setDst(contractVersionContractorEmployee.getContactor());
                    paymentResult.setDstComment(contractVersionContractorEmployee.getContactor().getFio());
                    paymentResult.setSrc(contractVersionContractorPayer.getContactor());
                    paymentResult.setSrcComment(contractVersionContractorPayer.getContactor().getFio());
                    paymentResult.setTimestamp(date(row, TIMESTAMP_COLUMN, context, true));
                    paymentResult.setCostAsLong(Long.parseLong((String) row.get(COST_COLUMN)));
                    paymentResult.setContract(contractObject);
                    paymentResult.setCurrency(getByCode(Currency.class, CurrencyCodes.RUB));
                    paymentResult.setComment((String) row.get(COMMENT_COLUMN));
                    paymentResult.setPaymentDocumentNumber((String) row.get(PAYMENT_CHECK_NUMBER_COLUMN));
                    paymentResult.setSourceDocumentNumber((String) row.get(PAYMENT_ORDER_NUMBER_COLUMN));
                    save(paymentResult);
                }
            }
        }
        );
    }

    /**
     * Метод создает контрагента для версии контракта на основе студента или его родственника (в зависимости, от того чему равно поле payer_id в таблице contract_t)
     *
     * @param row                              строка из таблицы contract_t
     * @param contractVersionContractorStudent контрагент, созданный на основе студента
     * @param contractId                       id контракта
     * @param contractVersion                  верся контракта для которой создается контрагент
     * @param personNextOfKin                  родственник студента, на основе которого создается контрагент
     * @return котнрагент версии контракта
     */
    private CtrContractVersionContractor getContractorPayer(Map<String, Object> row, CtrContractVersionContractor contractVersionContractorStudent,
                                                            String contractId, CtrContractVersion contractVersion, PersonNextOfKin personNextOfKin)
    {
        //Контрагент в версии договора по оплате (сторона плательщика) находим уже существующего
        // или создаем контрагента на основании данных о студенте или родственника студента:
        String studentId = (String) row.get(STUDENT_ID_COLUMN);
        String payerId = (String) row.get(PAYER_ID_COLUMN);
        PhysicalContactor physicalContactorPayer;

        //если введен id студента, то плательщиком является студент (контрагент для студента уже был создан)
        if (studentId.equals(payerId))
        {
            return contractVersionContractorStudent;
        }

        //если введен id родственника, то создаем/находим контрагента, созданного на основе родственника (Необходимо убедиться что родственик относится к данному студенту).
        else if (personNextOfKin != null)
        {
            physicalContactorPayer = getPhysicalContactorNextOfKind(personNextOfKin);

        }

        //если id не введен, или введеный id не соответсвует студенту или родственнику студента
        else
        {
            throw new ApplicationException("Incorrect field " + PAYER_ID_COLUMN + " for " + CONTRACT_ID_COLUMN + "=" + contractId + " in the table " + CONTRACT_TABLE + ". " +
                                                   "This field must contain student's id or student's relation id");
        }

        CtrContractVersionContractor contractVersionContractorPayer = new CtrContractVersionContractor();
        contractVersionContractorPayer.setContactor(physicalContactorPayer);
        contractVersionContractorPayer.setOwner(contractVersion);
        contractVersionContractorPayer.setRequireSignature(true);

        return contractVersionContractorPayer;
    }


    private PhysicalContactor getPhysicalContactorNextOfKind(PersonNextOfKin personNextOfKin)
    {

        Long physicalContactorPayerId = ContactorManager.instance().dao().getPersonNextOfKinPhysicalContactorId(personNextOfKin.getId());

        PhysicalContactor physicalContactorPayer;

        if (physicalContactorPayerId != null)
        {
            physicalContactorPayer = get(PhysicalContactor.class, PhysicalContactor.P_ID, physicalContactorPayerId);
        }
        else
        {
            AddressBase factAddress = AddressBaseUtils.getSameAddress(personNextOfKin.getAddress());
            IdentityCard identityCard = new DQLSelectBuilder().fromEntity(IdentityCard.class, "i").column(property("i"))
                    .where(DQLExpressions.eq(DQLExpressions.property(IdentityCard.lastName().fromAlias("i")), DQLExpressions.value(personNextOfKin.getLastName())))
                    .where(DQLExpressions.eq(DQLExpressions.property(IdentityCard.firstName().fromAlias("i")), DQLExpressions.value(personNextOfKin.getFirstName())))
                    .where(DQLExpressions.eq(DQLExpressions.property(IdentityCard.middleName().fromAlias("i")), DQLExpressions.value(personNextOfKin.getMiddleName())))
                    .where(DQLExpressions.eq(DQLExpressions.property(IdentityCard.seria().fromAlias("i")), DQLExpressions.value(personNextOfKin.getPassportSeria())))
                    .where(DQLExpressions.eq(DQLExpressions.property(IdentityCard.number().fromAlias("i")), DQLExpressions.value(personNextOfKin.getPassportNumber())))
                    .createStatement(new DQLExecutionContext(getSession())).uniqueResult();

            if (null == identityCard)
            {
                identityCard = new IdentityCard();
                identityCard.setLastName(personNextOfKin.getLastName());
                identityCard.setFirstName(personNextOfKin.getFirstName());
                identityCard.setMiddleName(personNextOfKin.getMiddleName());

                Sex sex = personNextOfKin.getSex();

                if(sex == null)
                {
                    if(personNextOfKin.getRelationDegree().getSex() != null)
                    {
                        sex = personNextOfKin.getRelationDegree().getSex();
                    }
                    else
                    {
                        sex = getCatalogItem(Sex.class, SexCodes.MALE);
                    }
                }

                identityCard.setSex(sex);
                identityCard.setSeria(personNextOfKin.getPassportSeria());
                identityCard.setNumber(personNextOfKin.getPassportNumber());

                ICitizenship citizenship = personNextOfKin.getCitizenship();
                if(citizenship == null)
                    citizenship = getList(NoCitizenship.class).get(0);

                identityCard.setCitizenship(citizenship);

                IdentityCardType cardType = personNextOfKin.getPassportType();
                if(cardType == null)
                    cardType = getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.BEZ_UDOSTOVERENIYA);

                identityCard.setCardType(cardType);


                physicalContactorPayer = ContactorManager.instance().dao().savePhysicalContactor(identityCard, factAddress, factAddress, personNextOfKin.getPhones(), null);
            }
            else
            {
                physicalContactorPayer = new DQLSelectBuilder().fromEntity(PhysicalContactor.class, "i").column(property("i"))
                        .where(DQLExpressions.or(
                                DQLExpressions.eq(DQLExpressions.property(PhysicalContactor.person().identityCard().fromAlias("i")),
                                        DQLExpressions.value(identityCard)),
                                DQLExpressions.eq(DQLExpressions.property(PhysicalContactor.person().fromAlias("i")),
                                        DQLExpressions.value(identityCard.getPerson()))
                        )).createStatement(getSession()).uniqueResult();

                if(physicalContactorPayer == null)
                {
                    Person person =  new DQLSelectBuilder().fromEntity(Person.class, "p").column(property("p"))
                            .where(DQLExpressions.or(
                                    DQLExpressions.eq(DQLExpressions.property(Person.identityCard().fromAlias("p")),
                                            DQLExpressions.value(identityCard)),
                                    DQLExpressions.eq(DQLExpressions.property("p"),
                                            DQLExpressions.value(identityCard.getPerson()))
                            )).createStatement(getSession()).uniqueResult();
                    if(person != null)
                    {
                        physicalContactorPayer = ContactorManager.instance().dao().savePhysicalContactor(person,  personNextOfKin.getPhones(), null, false);
                    }
                    else
                    {
                        physicalContactorPayer = ContactorManager.instance().dao().savePhysicalContactor(identityCard, personNextOfKin.getPhones(), null);
                    }
                }
                ContactorManager.instance().dao().updatePhysicalContactor(physicalContactorPayer, personNextOfKin.getPhones(), null);
            }

            ContactorManager.instance().dao().savePersonNextOfKinPhysicalContactor(personNextOfKin.getId(), physicalContactorPayer.getId());

        }

        return physicalContactorPayer;
    }

    private void validateTables(final Database mdb) throws Exception
    {
        Table contract_t = mdb.getTable(CONTRACT_TABLE);
        if (null == contract_t)
        {
            throw new ApplicationException("Table «" + CONTRACT_TABLE + "» does not exist.");
        }

        final Table payment_promise_t = mdb.getTable(PAYMENT_PROMISE_TABLE);
        if (null == payment_promise_t)
        {
            throw new ApplicationException("Table «" + PAYMENT_PROMISE_TABLE + "»  does not exist.");
        }

        final Table payment_result_t = mdb.getTable(PAYMENT_RESULT_TABLE);
        if (null == payment_result_t)
        {
            throw new ApplicationException("Table «" + PAYMENT_RESULT_TABLE + "»  does not exist.");
        }

        //Таблицы для импорта родственников студентов
        final Table person_nextofkin_t = mdb.getTable(PERSON_NEXTOFKIN_TABLE);
        if (null == person_nextofkin_t)
        {
            throw new ApplicationException("Table «" + PERSON_NEXTOFKIN_TABLE + "» does not exist.");
        }
        final List<Long> contractIds = new ArrayList<>();
        final List<Long> nextOfKinIds = new ArrayList<>();
        final Map <String, String> nextOfKinPersonIds = new HashMap<>();
        final Map <String, NextOfKinPersonWrapper> nextOfKinPersonWrapMap = new HashMap<>();
        final String[] message = {""};

        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
        final Map<String, RelationDegree> relationDegreeMap = catalogIO.catalogRelationDegree().lookup(false);
        final Map<String, Sex> sexMap = catalogIO.catalogSex().lookup(false);

        execute(person_nextofkin_t, 64, "Валидация таблицы " + PERSON_NEXTOFKIN_TABLE, rows -> {
            for (final Map<String, Object> row : rows)
            {
                if (isEmptyColumn(row.get(NEXTOFKIN_ID_COLUMN)))
                {
                    message[0] += "Table " + PERSON_NEXTOFKIN_TABLE + " must contain field " + NEXTOFKIN_ID_COLUMN + "; \n";
                }
                final String nextOfKinId = (String) row.get(NEXTOFKIN_ID_COLUMN);
                if ((StringUtils.isNumeric(nextOfKinId) && Long.parseLong(nextOfKinId) > 0 && Long.parseLong(nextOfKinId) <= 100000) || isEmptyColumn(row.get(NEXTOFKIN_ID_COLUMN)))
                {
                    Long validatedNextOfKin = tryParseHexId((String) row.get(NEXTOFKIN_ID_COLUMN));
                    if (nextOfKinIds.contains(validatedNextOfKin))
                    {
                        message[0] += "Table " + PERSON_NEXTOFKIN_TABLE + " (for " + NEXTOFKIN_ID_COLUMN + " " + validatedNextOfKin + " ) must contain unique field " + NEXTOFKIN_ID_COLUMN + "; \n";
                    }
                    else
                    {
                        nextOfKinIds.add(validatedNextOfKin);
                    }
                    if (isEmptyColumn(row.get(PERSON_ID_COLUMN)))
                        message[0] += "Table " + PERSON_NEXTOFKIN_TABLE + " (for " + NEXTOFKIN_ID_COLUMN + " " + validatedNextOfKin + " ) must contain field " + PERSON_ID_COLUMN + "; \n";

                    if (isEmptyColumn(row.get(RELATION_DEGREE_COLUMN)))
                    {
                        message[0] += "Table " + PERSON_NEXTOFKIN_TABLE + " (for " + NEXTOFKIN_ID_COLUMN + " " + validatedNextOfKin + " ) must contain field " + RELATION_DEGREE_COLUMN + "; \n";
                    }

                    RelationDegree relationDegree = relationDegreeMap.get((String)row.get(RELATION_DEGREE_COLUMN));

                    if(relationDegree == null)
                    {
                        message[0] += "Table " + PERSON_NEXTOFKIN_TABLE + " (for " + NEXTOFKIN_ID_COLUMN + " " + validatedNextOfKin + " ) contain incorrect data in field " + RELATION_DEGREE_COLUMN + ". Relation degree " + row.get(RELATION_DEGREE_COLUMN)  + " not exist; \n";
                    }

                    if(relationDegree != null && relationDegree.getSex() == null && isEmptyColumn(row.get(SEX_COLUMN)))
                    {
                        message[0] += "Table " + PERSON_NEXTOFKIN_TABLE + " (for " + NEXTOFKIN_ID_COLUMN + " " + validatedNextOfKin + " ) must contain field " + SEX_COLUMN + ". Sex is null for relation degree " + row.get(RELATION_DEGREE_COLUMN) + " ; \n";
                    }

                    if (isEmptyColumn(row.get(PASSPORT_TYPE_COLUMN)))
                        message[0] += "Table " + PERSON_NEXTOFKIN_TABLE + " (for " + NEXTOFKIN_ID_COLUMN + " " + validatedNextOfKin + " ) must contain field " + PASSPORT_TYPE_COLUMN + "; \n";
                    if (isEmptyColumn(row.get(IDENTITY_SERIA_COLUMN)))
                        message[0] += "Table " + PERSON_NEXTOFKIN_TABLE + " (for " + NEXTOFKIN_ID_COLUMN + " " + validatedNextOfKin + " ) must contain field " + IDENTITY_SERIA_COLUMN + "; \n";
                    if (isEmptyColumn(row.get(IDENTITY_NUMBER_COLUMN)))
                        message[0] += "Table " + PERSON_NEXTOFKIN_TABLE + " (for " + NEXTOFKIN_ID_COLUMN + " " + validatedNextOfKin + " ) must contain field " + IDENTITY_NUMBER_COLUMN + "; \n";
                    if (isEmptyColumn(row.get(IDENTITY_FIRST_NAME_COLUMN)))
                        message[0] += "Table " + PERSON_NEXTOFKIN_TABLE + " (for " + NEXTOFKIN_ID_COLUMN + " " + validatedNextOfKin + " ) must contain field " + IDENTITY_FIRST_NAME_COLUMN + "; \n";
                    if (isEmptyColumn(row.get(IDENTITY_LAST_NAME_COLUMN)))
                        message[0] += "Table " + PERSON_NEXTOFKIN_TABLE + " (for " + NEXTOFKIN_ID_COLUMN + " " + validatedNextOfKin + " ) must contain field " + IDENTITY_LAST_NAME_COLUMN + "; \n";
                    if (isEmptyColumn(row.get(CITIZENSHIP_ID_COLUMN)))
                        message[0] += "Table " + PERSON_NEXTOFKIN_TABLE + " (for " + NEXTOFKIN_ID_COLUMN + " " + validatedNextOfKin + " ) must contain field " + CITIZENSHIP_ID_COLUMN + "; \n";

                    if(!isEmptyColumn(row.get(NEXTOFKIN_ID_COLUMN))
                            && !isEmptyColumn(row.get(IDENTITY_FIRST_NAME_COLUMN))
                            && !isEmptyColumn(row.get(IDENTITY_LAST_NAME_COLUMN))
                            && !isEmptyColumn(row.get(IDENTITY_SERIA_COLUMN))
                            && !isEmptyColumn(row.get(IDENTITY_NUMBER_COLUMN)))
                    {
                        nextOfKinPersonWrapMap.put((String)row.get(NEXTOFKIN_ID_COLUMN),
                                new NextOfKinPersonWrapper(true, null, (String) row.get(IDENTITY_FIRST_NAME_COLUMN),
                                        (String) row.get(IDENTITY_LAST_NAME_COLUMN), (String) row.get(IDENTITY_MIDDLE_NAME_COLUMN),
                                        (String) row.get(IDENTITY_SERIA_COLUMN), (String) row.get(IDENTITY_NUMBER_COLUMN), (String) row.get(RELATION_DEGREE_COLUMN), (String) row.get(SEX_COLUMN)));
                    }
                }
                else
                {
                    Long nextOfKin = tryParseHexId((String) row.get(NEXTOFKIN_ID_COLUMN));
                    if(nextOfKin != null)
                    {
                        PersonNextOfKin personNextOfKin = get(nextOfKin);
                        if(personNextOfKin != null)
                        {
                            nextOfKinPersonWrapMap.put((String) row.get(NEXTOFKIN_ID_COLUMN),
                                    new NextOfKinPersonWrapper(false, nextOfKin, personNextOfKin.getFirstName(),
                                            personNextOfKin.getLastName(), personNextOfKin.getMiddleName(),
                                            personNextOfKin.getPassportSeria(), personNextOfKin.getPassportNumber(), personNextOfKin.getRelationDegree().getCode(), personNextOfKin.getSex() != null ? personNextOfKin.getSex().getCode() : null));
                        }
                    }
                }
                nextOfKinPersonIds.put((String)row.get(NEXTOFKIN_ID_COLUMN), (String)row.get(PERSON_ID_COLUMN));
            }
        });

        execute(contract_t, 64, "Валидация таблицы " + CONTRACT_TABLE, rows -> {
            for (final Map<String, Object> row : rows)
            {
                if (isEmptyColumn(row.get(CONTRACT_ID_COLUMN)))
                {
                    message[0] += "Table " + CONTRACT_TABLE + " must contain field " + CONTRACT_ID_COLUMN + "; \n";
                }
                final String contractId = (String) row.get(CONTRACT_ID_COLUMN);
                if ((StringUtils.isNumeric(contractId) && Long.parseLong(contractId) > 0 && Long.parseLong(contractId) <= 100000) || isEmptyColumn(row.get(CONTRACT_ID_COLUMN)))
                {
                    Long validatedContract = tryParseHexId((String) row.get(CONTRACT_ID_COLUMN));
                    if (contractIds.contains(validatedContract))
                    {
                        message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain unique field " + CONTRACT_ID_COLUMN + "; \n";
                    }
                    else
                    {
                        contractIds.add(validatedContract);
                    }
                    if (isEmptyColumn(row.get(STUDENT_ID_COLUMN)))
                    {
                        message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain field " + STUDENT_ID_COLUMN + "; \n";
                    }
                    else if (!existsEntity(Student.class, Student.P_ID, tryParseHexId((String) row.get(STUDENT_ID_COLUMN))))
                    {

                        message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + STUDENT_ID_COLUMN + "; \n";
                    }


                    if (isEmptyColumn(row.get(CONTRACT_NUMBER_COLUMN)))
                        message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain field " + CONTRACT_NUMBER_COLUMN + "; \n";

                    if (isEmptyColumn(row.get(CONTRACT_PRINT_TEMPLATE_COLUMN)))
                    {
                        message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain field " + CONTRACT_PRINT_TEMPLATE_COLUMN + "; \n";
                    }
                    else if (!existsEntity(CtrPrintTemplate.class, CtrPrintTemplate.P_ID, tryParseHexId((String) row.get(CONTRACT_PRINT_TEMPLATE_COLUMN))))
                    {

                        message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + CONTRACT_PRINT_TEMPLATE_COLUMN + "; \n";
                    }

                    if (isEmptyColumn(row.get(CONTRACT_REG_DATE_COLUMN)))
                    {
                        message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain field " + CONTRACT_REG_DATE_COLUMN + "; \n";
                    }
                    else
                    {
                        try
                        {
                            Date date = date(row, CONTRACT_REG_DATE_COLUMN, CONTRACT_TABLE + "[" + CONTRACT_ID_COLUMN + "=" + contractId + "]", false);
                            if (CoreDateUtils.getYear(date) <= 1900)
                            {
                                message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + CONTRACT_REG_DATE_COLUMN + " (year must be greater than 1900); \n";
                            }
                        }
                        catch (NullPointerException e)
                        {
                            message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + CONTRACT_REG_DATE_COLUMN + " (year must be greater than 1900); \n";
                        }
                    }
                    if (isEmptyColumn(row.get(CONTRACT_BEGIN_DATE_COLUMN)))
                    {
                        message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain field " + CONTRACT_BEGIN_DATE_COLUMN + "; \n";
                    }
                    else
                    {
                        try
                        {
                            Date date = date(row, CONTRACT_BEGIN_DATE_COLUMN, CONTRACT_TABLE + "[" + CONTRACT_ID_COLUMN + "=" + contractId + "]", false);
                            if (CoreDateUtils.getYear(date) <= 1900)
                            {
                                message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + CONTRACT_BEGIN_DATE_COLUMN + " (year must be greater than 1900); \n";
                            }
                        }
                        catch (NullPointerException e)
                        {
                            message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + CONTRACT_BEGIN_DATE_COLUMN + " (year must be greater than 1900); \n";
                        }
                    }
                    if (!isEmptyColumn(row.get(CONTRACT_END_DATE_COLUMN)))
                    {
                        try
                        {
                            Date date = date(row, CONTRACT_END_DATE_COLUMN, CONTRACT_TABLE + "[" + CONTRACT_ID_COLUMN + "=" + contractId + "]", false);
                            if (CoreDateUtils.getYear(date) <= 1900)
                            {
                                message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + CONTRACT_END_DATE_COLUMN + " (year must be greater than 1900); \n";
                            }
                        }
                        catch (NullPointerException e)
                        {
                            message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + CONTRACT_END_DATE_COLUMN + " (year must be greater than 1900); \n";
                        }
                    }
                    if (isEmptyColumn(row.get(CONTRACT_EDU_YEAR_COLUMN)))
                        message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain field " + CONTRACT_EDU_YEAR_COLUMN + "; \n";
                    if (isEmptyColumn(row.get(EDU_PROGRAM_ID_COLUMN)))
                    {
                        message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain field " + EDU_PROGRAM_ID_COLUMN + "; \n";
                    }
                    else if (!existsEntity(EduProgram.class, EduProgram.P_ID, tryParseHexId((String) row.get(EDU_PROGRAM_ID_COLUMN))))
                    {

                        message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + EDU_PROGRAM_ID_COLUMN + "; \n";
                    }
                    else if (existsEntity(EduProgram.class, EduProgram.P_ID, tryParseHexId((String) row.get(EDU_PROGRAM_ID_COLUMN))))
                    {
                        EduProgramProf eduProgramProf = get(EduProgramProf.class, tryParseHexId((String) row.get(EDU_PROGRAM_ID_COLUMN)));

                        if(!eduProgramProf.getProgramSubject().getSubjectIndex().getProgramKind().isProgramHigherProf() && !eduProgramProf.getProgramSubject().getSubjectIndex().getProgramKind().isProgramSecondaryProf())
                        {
                            message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + EDU_PROGRAM_ID_COLUMN + ". Education program must be high or secondary prof; \n";
                        }

                        if (existsEntity(Student.class, Student.P_ID, tryParseHexId((String) row.get(STUDENT_ID_COLUMN))))
                        {
                            Student student = get(Student.class, tryParseHexId((String) row.get(STUDENT_ID_COLUMN)));

                            if(student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectIndex().getProgramKind().isProgramHigherProf()
                                    && !eduProgramProf.getProgramSubject().getSubjectIndex().getProgramKind().isProgramHigherProf())
                            {
                                message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + EDU_PROGRAM_ID_COLUMN + ". Education program must be high prof; \n";
                            }

                            if(student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectIndex().getProgramKind().isProgramSecondaryProf()
                                    && !eduProgramProf.getProgramSubject().getSubjectIndex().getProgramKind().isProgramSecondaryProf())
                            {
                                message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + EDU_PROGRAM_ID_COLUMN + ". Education program must be secondary prof; \n";
                            }
                        }

                        if(existsEntity(CtrPrintTemplate.class, CtrPrintTemplate.P_ID, tryParseHexId((String) row.get(CONTRACT_PRINT_TEMPLATE_COLUMN))))
                        {
                            CtrPrintTemplate printTemplate = get(CtrPrintTemplate.class, tryParseHexId((String) row.get(CONTRACT_PRINT_TEMPLATE_COLUMN)));

                            if(eduProgramProf.getProgramSubject().getSubjectIndex().getProgramKind().isProgramHigherProf()
                                    && !CtrContractKindCodes.EDU_CONTRACT_VO_3_SIDES_PERSON.equals(printTemplate.getKind().getCode())
                                    && !CtrContractKindCodes.EDU_CONTRACT_VO_2_SIDES.equals(printTemplate.getKind().getCode()))

                            {
                                message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + CONTRACT_PRINT_TEMPLATE_COLUMN + ". Print template must be for higher prof; \n";
                            }
                            else if(eduProgramProf.getProgramSubject().getSubjectIndex().getProgramKind().isProgramSecondaryProf()
                                    && !CtrContractKindCodes.EDU_CONTRACT_SPO_3_SIDES_PERSON.equals(printTemplate.getKind().getCode())
                                    && !CtrContractKindCodes.EDU_CONTRACT_SPO_2_SIDES.equals(printTemplate.getKind().getCode()))
                            {
                                message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + CONTRACT_PRINT_TEMPLATE_COLUMN + ". Print template must be for secondary prof; \n";
                            }
                        }
                    }

                    if (isEmptyColumn(row.get(EMPLOYEE_ID_COLUMN)))
                    {
                        message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain field " + EMPLOYEE_ID_COLUMN + "; \n";
                    }
                    else if (!existsEntity(EmployeePost.class, EmployeePost.P_ID, tryParseHexId((String) row.get(EMPLOYEE_ID_COLUMN))))
                    {
                        message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + EMPLOYEE_ID_COLUMN + "; \n";
                    }
                    if (isEmptyColumn(row.get(PAYER_ID_COLUMN)))
                    {
                        message[0] += "Table " + CONTRACT_TABLE + " (for contract " + validatedContract + " ) must contain field " + PAYER_ID_COLUMN + "; \n";
                    }
                    else
                    {
                        if (!row.get(PAYER_ID_COLUMN).equals(row.get(STUDENT_ID_COLUMN)))
                        {
                            Long personId = tryParseHexId(nextOfKinPersonIds.get((String)row.get(PAYER_ID_COLUMN)));
                            Student student = get(Student.class, tryParseHexId((String) row.get(STUDENT_ID_COLUMN)));
                            if (null == personId || student == null
                                    || !student.getPerson().getId().equals(personId))
                            {
                                message[0] += "Incorrect field " + PAYER_ID_COLUMN + " for " + CONTRACT_ID_COLUMN + "=" + validatedContract + " in the table " + CONTRACT_TABLE + ". " +
                                        "This field must contain student's id or student's relation id";
                            }

                            // если родственник, то проверяем, что в системе нет  УЛ больше чем два для данного родственника, если для родственника еще не создан контрагент
                            if(personId != null && student != null && student.getPerson().getId().equals(personId))
                            {
                                NextOfKinPersonWrapper nextOfKinWrapper = nextOfKinPersonWrapMap.get((String) row.get(PAYER_ID_COLUMN));
                                if(nextOfKinWrapper != null)
                                {
                                    boolean notExistContractor = true;

                                    // проверяем что нет контрагента
                                    if(!nextOfKinWrapper.isCreateNew())
                                    {
                                        PersonNextOfKin nextOfKin = get(nextOfKinWrapper.getExistedNextOfKinId());
                                        if(nextOfKin == null)
                                        {
                                            message[0] += "Incorrect field " + PAYER_ID_COLUMN + " for " + CONTRACT_ID_COLUMN + "=" + validatedContract + " in the table " + CONTRACT_TABLE + ". " +
                                                    "Entity personNextOfKind with id=" + nextOfKinWrapper.getExistedNextOfKinId() + " (id in table " + PERSON_NEXTOFKIN_TABLE + " = " + (String) row.get(PAYER_ID_COLUMN) + ") not exist.";
                                        }
                                        else
                                        {
                                            if(nextOfKin.getRelationDegree().getSex() == null && nextOfKin.getSex() == null)
                                            {
                                                message[0] += "Incorrect field " + PAYER_ID_COLUMN + " for " + CONTRACT_ID_COLUMN + "=" + validatedContract + " in the table " + CONTRACT_TABLE + ". " +
                                                        "Sex is null for entity personNextOfKind with id=" + nextOfKinWrapper.getExistedNextOfKinId() + " (id in table " + PERSON_NEXTOFKIN_TABLE + " = " + (String) row.get(PAYER_ID_COLUMN) + ").";
                                            }

                                            PersonNextOfKin2physicalContactorRelation relation = get(PersonNextOfKin2physicalContactorRelation.class, PersonNextOfKin2physicalContactorRelation.personNextOfKin().s(), nextOfKin);
                                            if(relation != null)
                                            {
                                                notExistContractor = false;
                                            }
                                        }
                                    }

                                    if(notExistContractor)
                                    {
                                        final Number number = new DQLSelectBuilder().fromEntity(IdentityCard.class, "i")
                                                .where(DQLExpressions.eq(DQLExpressions.property(IdentityCard.lastName().fromAlias("i")), DQLExpressions.value(nextOfKinWrapper.getLastName())))
                                                .where(DQLExpressions.eq(DQLExpressions.property(IdentityCard.firstName().fromAlias("i")), DQLExpressions.value(nextOfKinWrapper.getFirstName())))
                                                .where(DQLExpressions.eq(DQLExpressions.property(IdentityCard.middleName().fromAlias("i")), DQLExpressions.value(nextOfKinWrapper.getMiddleName())))
                                                .where(DQLExpressions.eq(DQLExpressions.property(IdentityCard.seria().fromAlias("i")), DQLExpressions.value(nextOfKinWrapper.getPassportSeria())))
                                                .where(DQLExpressions.eq(DQLExpressions.property(IdentityCard.number().fromAlias("i")), DQLExpressions.value(nextOfKinWrapper.getPassportNumber())))
                                                .createCountStatement(new DQLExecutionContext(this.getSession())).uniqueResult();

                                        final int count = number == null ? 0 : number.intValue();

                                        if (count > 1) {

                                            message[0] += "Incorrect field " + PAYER_ID_COLUMN + " for " + CONTRACT_ID_COLUMN + "=" + validatedContract + " in the table " + CONTRACT_TABLE + ". " +
                                                    "For personNextOfKind with id in table " + PERSON_NEXTOFKIN_TABLE + " = " + (String) row.get(PAYER_ID_COLUMN) + " more than one person with same data - " +
                                                    " firstName = " + nextOfKinWrapper.getFirstName() + ", " +
                                                    " lastName = " + nextOfKinWrapper.getLastName() + ", " +
                                                    " middleName = " + nextOfKinWrapper.getMiddleName() + ", " +
                                                    " passport seria = " + nextOfKinWrapper.getPassportSeria() + ", " +
                                                    " passport number = " + nextOfKinWrapper.getPassportNumber() + ".";
                                        }
                                    }

                                    // Проверяем что персона родственника не совпадет с персоной студента или представителя ВУЗа.
                                    if (!isEmptyColumn(row.get(STUDENT_ID_COLUMN)) && existsEntity(Student.class, Student.P_ID, tryParseHexId((String) row.get(STUDENT_ID_COLUMN)))
                                            && !isEmptyColumn(row.get(EMPLOYEE_ID_COLUMN)) && existsEntity(EmployeePost.class, EmployeePost.P_ID, tryParseHexId((String) row.get(EMPLOYEE_ID_COLUMN))))
                                    {
                                        if(Objects.equals(student.getPerson().getIdentityCard().getFirstName(), nextOfKinWrapper.getFirstName())
                                                && Objects.equals(student.getPerson().getIdentityCard().getLastName(), nextOfKinWrapper.getLastName())
                                                && Objects.equals(student.getPerson().getIdentityCard().getMiddleName(), nextOfKinWrapper.getMiddleName())
                                                && Objects.equals(student.getPerson().getIdentityCard().getSeria(), nextOfKinWrapper.getPassportSeria())
                                                && Objects.equals(student.getPerson().getIdentityCard().getNumber(), nextOfKinWrapper.getPassportNumber()))
                                        {
                                            message[0] += "Incorrect data for " + CONTRACT_ID_COLUMN + "=" + validatedContract + " in the table " + CONTRACT_TABLE + ". " +
                                                    "PersonNextOfKind with id in table " + PERSON_NEXTOFKIN_TABLE + " = " + (String) row.get(PAYER_ID_COLUMN) + " has same personal data as student (" + STUDENT_ID_COLUMN + ") " +
                                                    " firstName = " + nextOfKinWrapper.getFirstName() + ", " +
                                                    " lastName = " + nextOfKinWrapper.getLastName() + ", " +
                                                    " middleName = " + nextOfKinWrapper.getMiddleName() + ", " +
                                                    " passport seria = " + nextOfKinWrapper.getPassportSeria() + ", " +
                                                    " passport number = " + nextOfKinWrapper.getPassportNumber() + ".";
                                        }

                                        EmployeePost employee = get(EmployeePost.class, EmployeePost.P_ID, tryParseHexId((String) row.get(EMPLOYEE_ID_COLUMN)));

                                        if(Objects.equals(employee.getPerson().getIdentityCard().getFirstName(), nextOfKinWrapper.getFirstName())
                                                && Objects.equals(employee.getPerson().getIdentityCard().getLastName(), nextOfKinWrapper.getLastName())
                                                && Objects.equals(employee.getPerson().getIdentityCard().getMiddleName(), nextOfKinWrapper.getMiddleName())
                                                && Objects.equals(employee.getPerson().getIdentityCard().getSeria(), nextOfKinWrapper.getPassportSeria())
                                                && Objects.equals(employee.getPerson().getIdentityCard().getNumber(), nextOfKinWrapper.getPassportNumber()))
                                        {
                                            message[0] += "Incorrect data for " + CONTRACT_ID_COLUMN + "=" + validatedContract + " in the table " + CONTRACT_TABLE + ". " +
                                                    "PersonNextOfKind with id in table " + PERSON_NEXTOFKIN_TABLE + " = " + (String) row.get(PAYER_ID_COLUMN) + " has same personal data as employee (" + EMPLOYEE_ID_COLUMN + ") " +
                                                    " firstName = " + nextOfKinWrapper.getFirstName() + ", " +
                                                    " lastName = " + nextOfKinWrapper.getLastName() + ", " +
                                                    " middleName = " + nextOfKinWrapper.getMiddleName() + ", " +
                                                    " passport seria = " + nextOfKinWrapper.getPassportSeria() + ", " +
                                                    " passport number = " + nextOfKinWrapper.getPassportNumber() + ".";
                                        }
                                    }

                                    if(existsEntity(CtrPrintTemplate.class, CtrPrintTemplate.P_ID, tryParseHexId((String) row.get(CONTRACT_PRINT_TEMPLATE_COLUMN))))
                                    {
                                        CtrPrintTemplate printTemplate = get(CtrPrintTemplate.class, tryParseHexId((String) row.get(CONTRACT_PRINT_TEMPLATE_COLUMN)));
                                        if(!CtrContractKindCodes.EDU_CONTRACT_VO_3_SIDES_PERSON.equals(printTemplate.getKind().getCode()) && !CtrContractKindCodes.EDU_CONTRACT_SPO_3_SIDES_PERSON.equals(printTemplate.getKind().getCode()))
                                        {
                                            message[0] += "Incorrect data for " + CONTRACT_ID_COLUMN + "=" + validatedContract + " in the table " + CONTRACT_TABLE + ". Must contain 3-sides print template";
                                        }
                                    }

                                }
                            }
                            else
                            {
                                if(existsEntity(CtrPrintTemplate.class, CtrPrintTemplate.P_ID, tryParseHexId((String) row.get(CONTRACT_PRINT_TEMPLATE_COLUMN))))
                                {
                                    CtrPrintTemplate printTemplate = get(CtrPrintTemplate.class, tryParseHexId((String) row.get(CONTRACT_PRINT_TEMPLATE_COLUMN)));
                                    if(!CtrContractKindCodes.EDU_CONTRACT_VO_2_SIDES.equals(printTemplate.getKind().getCode()) && !CtrContractKindCodes.EDU_CONTRACT_SPO_2_SIDES.equals(printTemplate.getKind().getCode()))
                                    {
                                        message[0] += "Incorrect data for " + CONTRACT_ID_COLUMN + "=" + validatedContract + " in the table " + CONTRACT_TABLE + ". Must contain 2-sides print template";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });


        execute(payment_promise_t, 64, "Валидация таблицы " + PAYMENT_PROMISE_TABLE, rows -> {
            for (final Map<String, Object> row : rows)
            {
                if (isEmptyColumn(row.get(CONTRACT_ID_COLUMN)))
                {
                    message[0] += "Table " + PAYMENT_PROMISE_TABLE + " must contain field " + CONTRACT_ID_COLUMN + "; \n";
                }
                final String contractId = (String) row.get(CONTRACT_ID_COLUMN);
                if ((StringUtils.isNumeric(contractId) && Long.parseLong(contractId) > 0 && Long.parseLong(contractId) <= 100000) || isEmptyColumn(row.get(CONTRACT_ID_COLUMN)))
                {
                    Long validatedContract = tryParseHexId((String) row.get(CONTRACT_ID_COLUMN));
                    if (isEmptyColumn(row.get(COST_COLUMN)))
                    {
                        message[0] += "Table " + PAYMENT_PROMISE_TABLE + " (for contract " + validatedContract + " ) must contain field " + COST_COLUMN + "; \n";
                    }
                    else if (!StringUtils.isNumeric((String) row.get(COST_COLUMN))
                            || Long.parseLong((String) row.get(COST_COLUMN)) <= 0)
                    {
                        message[0] += "Table " + PAYMENT_PROMISE_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + COST_COLUMN + " (cost must be greater than 0); \n";
                    }
                    if (isEmptyColumn(row.get(DEADLINE_COLUMN)))
                    {
                        message[0] += "Table " + PAYMENT_PROMISE_TABLE + " (for contract " + validatedContract + " ) must contain field " + DEADLINE_COLUMN + "; \n";
                    }
                    else
                    {
                        try
                        {
                            Date deadlineDate = date(row, DEADLINE_COLUMN, PAYMENT_PROMISE_TABLE + "[" + CONTRACT_ID_COLUMN + "=" + contractId + "]", false);
                            if (CoreDateUtils.getYear(deadlineDate) <= 1900)
                            {
                                message[0] += "Table " + PAYMENT_PROMISE_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + DEADLINE_COLUMN + " (year must be greater than 1900); \n";
                            }
                        }
                        catch (NullPointerException e)
                        {
                            message[0] += "Table " + PAYMENT_PROMISE_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + DEADLINE_COLUMN + " (year must be greater than 1900); \n";
                        }
                    }

                }
            }
        });

        execute(payment_result_t, 64, "Валидация таблицы " + PAYMENT_RESULT_TABLE, rows -> {
            for (final Map<String, Object> row : rows)
            {
                if (isEmptyColumn(row.get(CONTRACT_ID_COLUMN)))
                {
                    message[0] += "Table " + PAYMENT_RESULT_TABLE + " must contain field " + CONTRACT_ID_COLUMN + "; \n";
                }
                final String contractId = (String) row.get(CONTRACT_ID_COLUMN);
                if ((StringUtils.isNumeric(contractId) && Long.parseLong(contractId) > 0 && Long.parseLong(contractId) <= 100000) || isEmptyColumn(row.get(CONTRACT_ID_COLUMN)))
                {
                    Long validatedContract = tryParseHexId((String) row.get(CONTRACT_ID_COLUMN));
                    if (isEmptyColumn(row.get(COST_COLUMN)))
                    {
                        message[0] += "Table " + PAYMENT_RESULT_TABLE + " (for contract " + validatedContract + ") must contain field " + COST_COLUMN + "; \n";
                    }
                    else if (!StringUtils.isNumeric((String) row.get(COST_COLUMN))
                            || Long.parseLong((String) row.get(COST_COLUMN)) <= 0)
                    {
                        message[0] += "Table " + PAYMENT_RESULT_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + COST_COLUMN + " (cost must be greater than 0); \n";
                    }
                    if (isEmptyColumn(row.get(PAYMENT_CHECK_NUMBER_COLUMN)))
                        message[0] += "Table " + PAYMENT_RESULT_TABLE + " (for contract " + validatedContract + ") must contain field " + PAYMENT_CHECK_NUMBER_COLUMN + "; \n";

                    if (isEmptyColumn(row.get(TIMESTAMP_COLUMN)))
                    {
                        message[0] += "Table " + PAYMENT_RESULT_TABLE + " (for contract " + validatedContract + " ) must contain field " + TIMESTAMP_COLUMN + "; \n";
                    }
                    else
                    {
                        try
                        {
                            Date deadlineDate = date(row, TIMESTAMP_COLUMN, PAYMENT_RESULT_TABLE + "[" + CONTRACT_ID_COLUMN + "=" + contractId + "]", false);
                            if (CoreDateUtils.getYear(deadlineDate) <= 1900)
                            {
                                message[0] += "Table " + PAYMENT_RESULT_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + TIMESTAMP_COLUMN + " (year must be greater than 1900); \n";
                            }
                        }
                        catch (NullPointerException e)
                        {
                            message[0] += "Table " + PAYMENT_RESULT_TABLE + " (for contract " + validatedContract + " ) must contain correct field " + TIMESTAMP_COLUMN + " (year must be greater than 1900); \n";
                        }
                    }

                }
            }
        });

        if (message[0].length() > 0)
        {
            log4j_logger.fatal(message[0]);
            throw new ApplicationException("some required fields empty or incorrect");
        }
    }

    private class NextOfKinPersonWrapper
    {
        boolean createNew;
        Long existedNextOfKinId;
        String firstName;
        String lastName;
        String middleName;
        String passportSeria;
        String passportNumber;
        String relationDegree;
        String sex;

        public NextOfKinPersonWrapper(boolean createNew, Long existedNextOfKinId, String firstName, String lastName, String middleName, String passportSeria, String passportNumber, String relationDegree, String sex)
        {
            this.createNew = createNew;
            this.existedNextOfKinId = existedNextOfKinId;
            this.firstName = firstName;
            this.lastName = lastName;
            this.middleName = middleName;
            this.passportSeria = passportSeria;
            this.passportNumber = passportNumber;
            this.relationDegree = relationDegree;
            this.sex = sex;
        }

        public boolean isCreateNew()
        {
            return createNew;
        }

        public Long getExistedNextOfKinId()
        {
            return existedNextOfKinId;
        }

        public String getFirstName()
        {
            return firstName;
        }

        public String getLastName()
        {
            return lastName;
        }

        public String getMiddleName()
        {
            return middleName;
        }

        public String getPassportSeria()
        {
            return passportSeria;
        }

        public String getPassportNumber()
        {
            return passportNumber;
        }

        public String getRelationDegree()
        {
            return relationDegree;
        }

        public String getSex()
        {
            return sex;
        }
    }

}
