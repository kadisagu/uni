/* $Id:$ */
package ru.tandemservice.unieductr.ws;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.CtrPriceManager;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.catalog.entity.CtrPriceCategory;
import org.tandemframework.shared.ctr.catalog.entity.CtrPricePaymentGrid;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.entity.EduProgramPrice;

import java.util.*;

/**
 * @author oleyba
 * @since 4/16/14
 */
public class EduProgramPriceServiceDAO extends UniBaseDao implements IEduProgramPriceServiceDAO
{
    @Override
    public EduProgramPriceEnvironmentNode getPriceData(Date date)
    {
        EduProgramPriceEnvironmentNode envNode = new EduProgramPriceEnvironmentNode();

        for (CtrPricePaymentGrid row : getList(CtrPricePaymentGrid.class, CtrPricePaymentGrid.P_CODE))
            envNode.paymentGrid.row.add(new EduProgramPriceEnvironmentNode.Row(row.getTitle(), row.getCode()));

        for (CtrPriceCategory row : getList(CtrPriceCategory.class, CtrPriceCategory.P_CODE))
            envNode.priceCategory.row.add(new EduProgramPriceEnvironmentNode.Row(row.getTitle(), row.getCode()));

        for (Currency row : getList(Currency.class, Currency.P_CODE))
            envNode.currency.row.add(new EduProgramPriceEnvironmentNode.Row(row.getTitle(), row.getCode()));


        for (EduLevel row : getList(EduLevel.class, EduLevel.P_CODE))
            envNode.eduLevel.row.add(new EduProgramPriceEnvironmentNode.Row(row.getTitle(), row.getCode()));

        for (EduProgramForm row : getList(EduProgramForm.class, EduProgramForm.P_CODE))
            envNode.eduProgramForm.row.add(new EduProgramPriceEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (EduProgramKind row : getList(EduProgramKind.class, EduProgramKind.P_PRIORITY))
            envNode.eduProgramKind.row.add(new EduProgramPriceEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (EduProgramTrait row : getList(EduProgramTrait.class, EduProgramTrait.P_CODE))
            envNode.eduProgramTrait.row.add(new EduProgramPriceEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (EduProgramDuration row : getList(EduProgramDuration.class, EduProgramDuration.P_CODE))
            envNode.eduProgramDuration.row.add(new EduProgramPriceEnvironmentNode.EduProgramDurationNode.EduProgramDurationRow(row.getTitle(), row.getCode(), row.getNumberOfYears() * 12 + row.getNumberOfMonths()));

        // сохраняем все направления приема

        Set<Long> usedOrgUnitIds = new HashSet<>();
        Set<OrgUnitType> usedOrgUnitTypes = new HashSet<>();
        Set<EduProgram> usedPrograms = new HashSet<>();
        Set<EduProgramSpecialization> usedSpecializations = new HashSet<>();
        Set<EduProgramSubject> usedSubjects = new HashSet<>();

        List<EduProgramPrice> programPriceList = getList(EduProgramPrice.class, "id");
        for (EduProgramPrice programPrice : programPriceList)
        {
            final List<CtrPriceElementCost> list = CtrPriceManager.instance().dao().getCurrencyGridCostList(date, programPrice);
            if (list.isEmpty()) continue;

            for (OrgUnit orgUnit : new OrgUnit[] {programPrice.getProgram().getInstitutionOrgUnit().getOrgUnit(), programPrice.getProgram().getOwnerOrgUnit().getOrgUnit()}) {
                if (usedOrgUnitIds.add(orgUnit.getId())) {
                    usedOrgUnitTypes.add(orgUnit.getOrgUnitType());
                    String settlementTitle = null;
                    if (orgUnit.getAddress() != null && orgUnit.getAddress().getSettlement() != null)
                        settlementTitle = orgUnit.getAddress().getSettlement().getTitle();
                    envNode.orgUnit.row.add(new EduProgramPriceEnvironmentNode.OrgUnitNode.OrgUnitRow(orgUnit.getShortTitle(), orgUnit.getTitle(), orgUnit.getId().toString(), orgUnit.getOrgUnitType().getCode(), orgUnit.getNominativeCaseTitle(), settlementTitle, orgUnit.getTerritorialTitle(), orgUnit.getTerritorialShortTitle()));
                }
            }
            usedPrograms.add(programPrice.getProgram());

            Collections.sort(list, new EntityComparator<CtrPriceElementCost>(new EntityOrder("id")));

            for (CtrPriceElementCost priceElementCost : list) {
                EduProgramPriceEnvironmentNode.PriceRow row = new EduProgramPriceEnvironmentNode.PriceRow();

                row.totalPriceMultipliedBy10000 = priceElementCost.getTotalCostAsLong();
                row.program = programPrice.getProgram().getId().toString();
                row.category = priceElementCost.getCategory().getCode();
                row.currency = priceElementCost.getCurrency().getCode();
                row.paymentGrid = priceElementCost.getPaymentGrid().getCode();
                row.validFrom = priceElementCost.getPeriod().getDurationBegin();
                row.validTo = null == priceElementCost.getPeriod().getDurationEnd() ? null : CoreDateUtils.getDayLastTimeMoment(priceElementCost.getPeriod().getDurationEnd());

                envNode.price.row.add(row);
            }
        }

        List<OrgUnitType> orgUnitTypeList = new ArrayList<>(usedOrgUnitTypes);
        Collections.sort(orgUnitTypeList, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);
        for (OrgUnitType type : orgUnitTypeList) {
            envNode.orgUnitType.row.add(new EduProgramPriceEnvironmentNode.Row(type.getTitle(), type.getCode()));
        }
        Collections.sort(envNode.orgUnit.row);

        List<EduProgram> programList = new ArrayList<>(usedPrograms);
        Collections.sort(programList, new EntityComparator<EduProgram>(new EntityOrder("id")));

        for (EduProgram program : programList) {
            EduProgramPriceEnvironmentNode.ProgramRow row = new EduProgramPriceEnvironmentNode.ProgramRow();

            row.id = program.getId().toString();
            row.title = program.getTitle();

            row.eduProgramForm = program.getForm().getCode();
            row.eduProgramKind = program.getKind().getCode();
            row.eduLevel = program.getKind().getEduLevel().getCode();
            row.eduYear = program.getYear().getEducationYear().getTitle();
            row.institutionOrgUnit = program.getInstitutionOrgUnit().getOrgUnit().getId().toString();
            row.ownerOrgUnit = program.getOwnerOrgUnit().getOrgUnit().getId().toString();
            row.duration = program.getDuration().getCode();
            row.programTrait = program.getEduProgramTrait() != null ? program.getEduProgramTrait().getCode() : null;

            if (program instanceof EduProgramProf) {
                EduProgramProf programProf = (EduProgramProf) program;
                usedSubjects.add(programProf.getProgramSubject());
                row.eduProgramSubject = programProf.getProgramSubject().getCode();
                row.baseLevel = programProf.getBaseLevel().getCode();
            }


            if (program instanceof EduProgramHigherProf) {
                EduProgramHigherProf programHigherProf = (EduProgramHigherProf) program;
                usedSpecializations.add(programHigherProf.getProgramSpecialization());
                row.eduProgramSpecialization = programHigherProf.getProgramSpecialization().getId().toString();
            }

            envNode.program.row.add(row);
        }

        for (EduProgramSpecialization specialization : usedSpecializations) {
            envNode.eduProgramSpecialization.row.add(new EduProgramPriceEnvironmentNode.Row(specialization.getTitle(), specialization.getId().toString()));
        }
        Collections.sort(envNode.eduProgramSpecialization.row);

        List<EduProgramSubject> sortedSubjects = new ArrayList<>();
        sortedSubjects.addAll(usedSubjects);
        Collections.sort(sortedSubjects, ITitled.TITLED_COMPARATOR);
        for (EduProgramSubject row : sortedSubjects)
            envNode.eduProgramSubject.row.add(new EduProgramPriceEnvironmentNode.Row(row.getSubjectCode(), row.getTitle(), row.getCode()));


        return envNode;

    }
}
