/* $Id:$ */
package ru.tandemservice.unieductr.ws;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 4/16/14
 */
@XmlRootElement
public class EduProgramPriceEnvironmentNode
{
    /** Справочник "Сетки оплаты" */
    public PaymentGridNode paymentGrid = new PaymentGridNode();

    /** Справочник "Категории цены" */
    public PriceCategoryNode priceCategory = new PriceCategoryNode();

    /** Справочник "Валюты" */
    public CurrencyNode currency = new CurrencyNode();

    /** Справочник "Виды и уровни образования" */
    public EduLevelNode eduLevel = new EduLevelNode();

    /** Справочник "Формы обучения" */
    public EduProgramFormNode eduProgramForm = new EduProgramFormNode();

    /** Справочник "Виды образовательных программ" */
    public EduProgramKindNode eduProgramKind = new EduProgramKindNode();

    /** Справочник "Особенности реализации образовательных программ" */
    public EduProgramTraitNode eduProgramTrait = new EduProgramTraitNode();

    /** Справочник "Продолжительность обучения" */
    public EduProgramDurationNode eduProgramDuration = new EduProgramDurationNode();

    /** Справочник "Направления подготовки, профессии и специальности профессионального образования " */
    public EduProgramSubjectNode eduProgramSubject = new EduProgramSubjectNode();

    /** Направленности (профили, специализации) образовательных программ */
    public EduProgramSpecializationNode eduProgramSpecialization = new EduProgramSpecializationNode();

    /** Все типы всех подразделений, указанных в обр. программах */
    public OrgUnitTypeNode orgUnitType = new OrgUnitTypeNode();

    /** Все подразделения, указанные в обр. программах */
    public OrgUnitNode orgUnit = new OrgUnitNode();

    /** Все образовательные программы, на которые есть настройка цен */
    public ProgramNode program = new ProgramNode();

    /** Все настройки цен */
    public PriceNode price = new PriceNode();

    /** Простая строка таблицы */
    public static class Row implements Comparable<Row>
    {
        public Row() { }

        public Row(String title, String id)
        {
            this.title = title;
            this.id = id;
        }

        public Row(String shortTitle, String title, String id)
        {
            this.shortTitle = shortTitle;
            this.title = title;
            this.id = id;
        }

        @Override
        public int compareTo(Row o)
        {
            return o.id.compareTo(o.toString());
        }

        /**
         * Сокращенное название
         */
        @XmlAttribute
        public String shortTitle;

        /**
         * Название
         */
        @XmlAttribute(required = true)
        public String title;

        /**
         * Идентификатор строки
         */
        @XmlAttribute(required = true)
        public String id;
    }

    /** Справочник "Сетки оплаты" */
    public static class PaymentGridNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Категории цены" */
    public static class PriceCategoryNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Валюты" */
    public static class CurrencyNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Виды и уровни образования" */
    public static class EduLevelNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Формы обучения" */
    public static class EduProgramFormNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Виды образовательных программ" */
    public static class EduProgramKindNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Особенности реализации образовательных программ" */
    public static class EduProgramTraitNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Направления подготовки, профессии и специальности профессионального образования " */
    public static class EduProgramSubjectNode { public List<Row> row = new ArrayList<Row>(); }

    /** Направленности (профили, специализации) образовательных программ */
    public static class EduProgramSpecializationNode { public List<Row> row = new ArrayList<Row>(); }

    /**
     * Все типы всех подразделений, ведущих прием, в указанной приемной кампании
     */
    public static class OrgUnitTypeNode { public List<Row> row = new ArrayList<Row>(); }

    /**
     * Справочник «Продолжительность обучения»
     */
    public static class EduProgramDurationNode
    {
        /**
         * Элемент справочника «Продолжительность обучения»
         */
        public static class EduProgramDurationRow extends Row
        {
            public EduProgramDurationRow()
            {
            }

            public EduProgramDurationRow(String title, String id, int durationInMonth)
            {
                super(title, id);
                this.durationInMonth = durationInMonth;
            }

            /**
             * Продолжительность обучения в месяцах
             *
             * @see EduProgramDurationNode
             */
            @XmlAttribute(required = true)
            public int durationInMonth;
        }

        public List<EduProgramDurationRow> row = new ArrayList<EduProgramDurationRow>();
    }


    /**
     * Все подразделения, ведущие прием, в указанной приемной кампании
     */
    public static class OrgUnitNode
    {
        /**
         * Подразделение
         */
        public static class OrgUnitRow extends Row
        {
            public OrgUnitRow()
            {
            }

            public OrgUnitRow(String shortTitle, String title, String id, String orgUnitType, String nominativeCaseTitle, String settlementTitle, String territorialTitle, String territorialShortTitle)
            {
                super(shortTitle, title, id);
                this.orgUnitType = orgUnitType;
                this.nominativeCaseTitle = nominativeCaseTitle;
                this.settlementTitle = settlementTitle;
                this.territorialTitle = territorialTitle;
                this.territorialShortTitle = territorialShortTitle;
            }

            /**
             * Тип подразделения
             *
             * @see OrgUnitTypeNode
             */
            @XmlAttribute(required = true)
            public String orgUnitType;

            /**
             * Печатное название подразделения в именительном падеже
             */
            @XmlAttribute
            public String nominativeCaseTitle;

            /**
             * Название населенного пункта из фактического адреса подразделения
             */
            @XmlAttribute
            public String settlementTitle;

            /**
             * Название подразделения в контексте территориального подр.
             */
            @XmlAttribute(required = true)
            public String territorialTitle;

            /**
             * Сокращенное название подразделения в контексте территориального подр.
             */
            @XmlAttribute(required = true)
            public String territorialShortTitle;
        }

        public List<OrgUnitRow> row = new ArrayList<OrgUnitRow>();
    }

    /**
     * Конкурсы (комбинации условий поступления) для приема
     */
    public static class PriceNode { public List<PriceRow> row = new ArrayList<PriceRow>(); }

    /**
     * Конкурс (комбинация условий поступления) для приема
     */
    public static class PriceRow
    {

        /**
         * Образовательная программа
         *
         * @see ProgramNode
         */
        @XmlAttribute(required = true)
        public String program;

        /**
         * Полная стоимость. Передается в целом числе со сдвигом на четыре порядка для дробной части - т.е. умноженная на 10000
         */
        public long totalPriceMultipliedBy10000;

        /**
         * Сетка оплат
         *
         * @see PaymentGridNode
         */
        @XmlAttribute(required = true)
        public String paymentGrid;

        /**
         * Категория цены
         *
         * @see PriceCategoryNode
         */
        @XmlAttribute(required = true)
        public String category;

        /**
         * Валюта цены
         *
         * @see CurrencyNode
         */
        @XmlAttribute(required = true)
        public String currency;

        /**
         * Дата начала действия цены
         */
        @XmlAttribute(required = true)
        public Date validFrom;


        /**
         * Дата окончания действия цены
         */
        @XmlAttribute()
        public Date validTo;
    }

    /**
     * Образовательные программы
     */
    public static class ProgramNode { public List<ProgramRow> row = new ArrayList<ProgramRow>(); }

    /**
     * Образовательная программа
     */
    public static class ProgramRow
    {
        /**
         * Идентификатор
         */
        @XmlAttribute(required = true)
        public String id;

        /**
         * Название
         */
        @XmlAttribute(required = true)
        public String title;

        /**
         * Направление (профессия, специальность)
         *
         * @see EduProgramSubjectNode
         */
        @XmlAttribute()
        public String eduProgramSubject;

        /**
         * Направленность (профиль, специализация)
         *
         * @see EduProgramSpecializationNode
         */
        @XmlAttribute()
        public String eduProgramSpecialization;

        /**
         * Форма обучения
         *
         * @see EduProgramFormNode
         */
        @XmlAttribute(required = true)
        public String eduProgramForm;

        /**
         * Вид обр. программы
         *
         * @see EduProgramKindNode
         */
        @XmlAttribute(required = true)
        public String eduProgramKind;

        /**
         * Продолжительность обучения по ОП
         *
         * @see EduProgramDurationNode
         */
        @XmlAttribute(required = true)
        public String duration;

        /**
         * Особенность реализации ОП
         *
         * @see EduProgramTraitNode
         */
        @XmlAttribute
        public String programTrait;

        /**
         * Необходимый базовый уровень для обучения по ОП
         *
         * @see EduLevelNode
         */
        @XmlAttribute
        public String baseLevel;

        /**
         * Уровень образования
         *
         * @see EduLevelNode
         */
        @XmlAttribute(required = true)
        public String eduLevel;

        /**
         * Год начала обучения
         */
        @XmlAttribute(required = true)
        public String eduYear;

        /**
         * Выпускающее подразделение
         */
        @XmlAttribute(required = true)
        public String ownerOrgUnit;

        /**
         * Лицензированное подразделение
         */
        @XmlAttribute(required = true)
        public String institutionOrgUnit;

    }

}
