/* $Id: PriceTemplateAddon.java 47942 2016-04-29 13:06:39Z nvankov $ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentAddContractTemplate.ui.Add.PriceTemplate;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.ListenerNamePrefixFactoryFastBean;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.shared.ctr.base.entity.ICtrPriceElement;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.program.entity.EduProgramAdditional;
import ru.tandemservice.unieductr.base.bo.EduProgramPrice.utils.EduPriceSelectionModel;
import ru.tandemservice.unieductr.base.entity.EduProgramPrice;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentAddContractTemplate.EduCtrStudentAddContractTemplateManager;

import java.util.Date;

/**
 * @author azhebko
 * @since 11.12.2014
 */
public class PriceTemplateAddon extends UIAddon
{
    public static final String PRICE_TEMPLATE_ADDON = PriceTemplateAddon.class.getSimpleName();

    public PriceTemplateAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    private final ListenerNamePrefixFactoryFastBean listeners = new ListenerNamePrefixFactoryFastBean(this);
    public ListenerNamePrefixFactoryFastBean getListeners() { return listeners; }

    private Date _priceDate; // требуется устанавливать
    private Student student; //   значение вручную
    private EducationYear eduProgramYear;
//    private EduProgramKind eduProgramKind;
    private EduProgramForm eduProgramForm;
    private EduProgramAdditional eduProgram;
    private EduProgramPrice eduProgramPrice;

    private final ISelectModel educationYearModel = new EducationYearModel();

    private final EduPriceSelectionModel priceSelection = new EduPriceSelectionModel()
    {
        @Override public Date getPriceDate() { return PriceTemplateAddon.this.getPriceDate(); }
        @Override public ICtrPriceElement getPriceElement() { return PriceTemplateAddon.this.getEduProgramPrice(); }
    };


    @Override
    public void onComponentRefresh()
    {
        if (getStudent() != null)
        {
            EducationYear eduYearSuggestion = IUniBaseDao.instance.get()
                    .getList(EducationYear.class, EducationYear.P_INT_VALUE).stream()
                    .filter(year -> year.getIntValue() == getStudent().getEntranceYear())
                    .findFirst().orElse(null);
            setEduProgramYear(eduYearSuggestion);
            setEduProgramForm(getStudent().getEducationOrgUnit().getDevelopForm().getProgramForm());
            setEduProgramPrice(getEduProgramPrice(getEduProgram()));
        }
        getPriceSelection().refreshCostList();
    }

    public void onChangeEduProgram()
    {
        getPriceSelection().refreshCostList();
    }

    public void onChangePrice() { getPriceSelection().refreshCostStageList(getPriceSelection().getSelectedCost()); }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EduCtrStudentAddContractTemplateManager.PARAM_PROGRAM_YEAR, this.getEduProgramYear());
        dataSource.put(EduCtrStudentAddContractTemplateManager.PARAM_PROGRAM_FORM, this.getEduProgramForm());
//        dataSource.put(EduCtrStudentAddContractTemplateManager.PARAM_PROGRAM_KIND, this.getEduProgramKind());
    }

    public Date getPriceDate() { return _priceDate; }
    public void setPriceDate(Date priceDate) { _priceDate = priceDate; }

    public EducationYear getEduProgramYear() { return this.eduProgramYear; }
    public void setEduProgramYear(EducationYear eduProgramYear) { this.eduProgramYear = eduProgramYear; }

//    public EduProgramKind getEduProgramKind() { return this.eduProgramKind; }
//    public void setEduProgramKind(EduProgramKind eduProgramKind) { this.eduProgramKind = eduProgramKind; }

    public EduProgramForm getEduProgramForm() { return this.eduProgramForm; }
    public void setEduProgramForm(EduProgramForm eduProgramForm) { this.eduProgramForm = eduProgramForm; }

    public EduProgramAdditional getEduProgram() { return this.eduProgram; }
    public void setEduProgram(final EduProgramAdditional eduProgram) { setEduProgramPrice(getEduProgramPrice(this.eduProgram = eduProgram)); }

    protected EduProgramPrice getEduProgramPrice() { return this.eduProgramPrice; }
    protected void setEduProgramPrice(EduProgramPrice eduProgramPrice) { this.eduProgramPrice = eduProgramPrice; }

    public ISelectModel getEducationYearModel() { return this.educationYearModel; }

    public EduPriceSelectionModel getPriceSelection() { return priceSelection; }

    public String getCipher() { return null == getEduProgramPrice() ? null : getEduProgramPrice().getCipher(); }

    protected EduProgramPrice getEduProgramPrice(EduProgramAdditional eduProgram)
    {
        return null;
//        if (null == eduProgram)
//            return null;
//        return DataAccessServices.dao().getByNaturalId(new EduProgramPriceGen.NaturalId(eduProgram));
    }

    public Student getStudent()
    {
        return student;
    }

    public void setStudent(Student student)
    {
        this.student = student;
    }
}