/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.StaticFullCheckSelectModel;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.AddByTemplate.CtrContractVersionAddByTemplateUI;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.CtrPriceManager;
import org.tandemframework.shared.ctr.base.entity.ICtrPriceElement;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementPeriod;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractKind;
import org.tandemframework.shared.ctr.catalog.entity.CtrPricePaymentGrid;
import org.tandemframework.shared.ctr.catalog.entity.CtrPrintTemplate;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.base.bo.EduProgramPrice.EduProgramPriceManager;
import ru.tandemservice.unieductr.base.bo.EduProgramPrice.utils.EduPriceSelectionModel;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.base.entity.EduProgramPrice;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrPricePaymentGridCodes;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.EduCtrStudentContractTemplateManager;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.EduCtrTermEducationCostAgreementTemplateManager;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.logic.CostStageWrapper;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;
import ru.tandemservice.unieductr.student.entity.EduCtrTermEducationCostAgreementTemplateData;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author azhebko
 * @since 04.12.2014
 */
@Input({
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTEXT_ID, binding = "prevVersion.id", required = true),
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_KIND_ID, binding="versionCreateData.contractKindHolder.id", required=true),
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_PRINT_TEMPLATE_ID, binding="versionCreateData.printTemplateHolder.id")
})
public class EduCtrTermEducationCostAgreementTemplateAddUI extends UIPresenter
{
    private CtrContractVersion _prevVersion = new CtrContractVersion();


    private EduCtrTermEducationCostAgreementTemplateData _template = new EduCtrTermEducationCostAgreementTemplateData();

    private final CtrContractVersionCreateData versionCreateData = new CtrContractVersionCreateData();

    public CtrContractVersionCreateData getVersionCreateData()
    {
        return versionCreateData;
    }

    private EduProgramPrice _eduProgramPrice;
    private CtrPriceElementPeriod _pricePeriod;
    private CtrPricePaymentGrid _paymentGrid;
    private CtrPriceElementCost _cost;

    private CtrPriceElementCostStage _costStage; // вычисляется на основе семестра и цены за ОП

    private Date _costStageDeadlineDate; // дата оплаты, ввод на форме

    private OrgUnit formativeOrgUnit;


    private List<CostStageWrapper> nextVersionPromises = new ArrayList<>();
    private Set<CostStageWrapper> selectedNextVersionPromises = new HashSet<>();


    private CostStageWrapper nextVersionPromise;


    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        this.formativeOrgUnit = formativeOrgUnit;
    }

    public OrgUnit getFormativeOrgUnit() { return formativeOrgUnit; }

    private final ISelectModel providerModel = new StaticFullCheckSelectModel(ContactorPerson.fullTitle().s()) {
        @Override protected List list() {
            final OrgUnit formativeOrgUnit = EduCtrTermEducationCostAgreementTemplateAddUI.this.getFormativeOrgUnit();
            if (null == formativeOrgUnit) { return Collections.emptyList(); }

            final Collection<ContactorPerson> presenters = EduContractManager.instance().dao().getAcademyPresenters(formativeOrgUnit);
            return new ArrayList<>(presenters);
        }
    };
    public ISelectModel getProviderModel() { return this.providerModel; }

    private ContactorPerson provider;
    public ContactorPerson getProvider() { return this.provider; }
    public void setProvider(final ContactorPerson provider) { this.provider = provider; }

    private Collection<CtrPaymentPromice> _prevVerPromises; // перечень обязательств по оплате пред. версии договора
    private Set<CtrPaymentPromice> _selectedPromises; // выбранные обязательства пред. версии

    @Override
    public void onComponentActivate() {
        _prevVersion = IUniBaseDao.instance.get().getNotNull(CtrContractVersion.class, getPrevVersion().getId());
        EduCtrStudentContract contract = IUniBaseDao.instance.get().getNotNull(EduCtrStudentContract.class, EduCtrStudentContract.contractObject(), getPrevVersion().getContract());
        setFormativeOrgUnit(contract.getStudent().getEducationOrgUnit().getFormativeOrgUnit());
        this.setProvider(CtrContractVersionManager.instance().dao().getContactor(getPrevVersion(), CtrContractRoleCodes.EDU_CONTRACT_PROVIDER).getContactor());
    }

    @Override
    public void onComponentRefresh()
    {
        IUniBaseDao dao = IUniBaseDao.instance.get();
        Collection<EduCtrEducationPromise> eduPromises = dao.getList(EduCtrEducationPromise.class, EduCtrEducationPromise.src().owner(), _prevVersion);
        if (eduPromises.size() != 1)
            throw new IllegalStateException(); // шаблон нельзя выбрать, если нет обязательств на обучение по ОП, либо их больше одного

        _eduProgramPrice = EduProgramPriceManager.instance().dao().doGetEduProgramPrice(eduPromises.iterator().next().getEduProgram().getId());

        _prevVerPromises = dao.getList(CtrPaymentPromice.class, CtrPaymentPromice.src().owner(), _prevVersion, CtrPaymentPromice.deadlineDate().s());
        Date currentDate = new Date();
        _selectedPromises = getPrevVerPromises().stream().filter(ctrPaymentPromice -> ctrPaymentPromice.getDeadlineDate().before(currentDate)).collect(Collectors.toSet());

        Collection<EduCtrStudentContract> studentContracts = dao.getList(EduCtrStudentContract.class, EduCtrStudentContract.contractObject(), getPrevVersion().getContract());

        if (studentContracts.size() != 1)
            throw new IllegalStateException();

        getTemplate().setEducationYear(EducationYearManager.instance().dao().getCurrent());
        getVersionCreateData().setDocStartDate(new Date());
        getVersionCreateData().doRefresh();

        List<CtrPriceElementPeriod> periods = CtrPriceManager.instance().dao().getUsedPeriodList(getEduProgramPrice(), new Date());

        if(!periods.isEmpty() && dao.existsEntity(CtrPriceElementCost.class, CtrPriceElementCost.period().s(), periods.get(0)))
        {
            setPricePeriod(periods.get(0));
        }

        setupStageCost();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EduCtrTermEducationCostAgreementTemplateAdd.PARAM_ELEMENT_PRICE_ID, getEduProgramPrice().getId());
        dataSource.put(EduCtrTermEducationCostAgreementTemplateAdd.PARAM_PRICE_PERIOD_ID, getPricePeriod() != null ? getPricePeriod().getId() : null);
        dataSource.put(EduCtrTermEducationCostAgreementTemplateAdd.PARAM_PAYMENT_GRID_ID, getPaymentGrid() != null ? getPaymentGrid().getId() : null);
    }

    public void onChangePriceDate()
    {
        setupStageCost();
    }

    public void onChangePrice()
    {
        setupStageCost();
    }

    private void setupStageCost()
    {
        // чистим списки
        nextVersionPromises.clear();
        selectedNextVersionPromises.clear();

        // если выбраны семестр и стоимость то заполняем новые обязательства.
        if (getCost() != null) // && _termNumber != null)
        {
            // создаем обязательства и выбираем их
            for (CtrPriceElementCostStage costStage : CtrPriceManager.instance().dao().getCostStages(getCost())) {
                final CostStageWrapper priceElementCostStage = new CostStageWrapper(costStage);
                nextVersionPromises.add(priceElementCostStage);
                selectedNextVersionPromises.add(priceElementCostStage);
            }

            // деселект всех старых обязательств
            _selectedPromises.clear();

            final long currentTime = new Date().getTime();
            // селект всех старых обязательства, у которых дедлайн дэйт раньше текущей
            _selectedPromises.addAll(_prevVerPromises.stream()
                    .filter(ctrPaymentPromice -> ctrPaymentPromice.getDeadlineDate().getTime() < currentTime)
                    .collect(Collectors.toList()));

        }
    }

    public void onClickApply()
    {
        final Date durationBeginDate = versionCreateData.getDurationBeginDate();
        if(durationBeginDate != null && durationBeginDate.before(_prevVersion.getDurationBeginDate()))
            _uiSupport.error("Дату начала действия нельзя установить раньше даты начала действия предыдущей версии");
        if(getUserContext().getErrorCollector().hasErrors())
            return;

        if (getCostStage() != null && getCostStage().getDeadlineDate() == null)
            getCostStage().setDeadlineDate(getCostStageDeadlineDate());

        EduCtrTermEducationCostAgreementTemplateManager.instance().dao().doCreateNewVersion(getTemplate(), getPrevVersion(), _selectedPromises, selectedNextVersionPromises, getVersionCreateData());
        EduCtrStudentContractTemplateManager.instance().dao().doUpdateContractVersionContactor(getTemplate().getOwner(), getProvider(), null);
        this.deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, getTemplate().getOwner().getId()).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.TRUE));
    }

    public void onClickClose()
    {
        this.deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, null).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.TRUE));
    }

    public void onClickBack()
    {
        this.deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, null).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.FALSE));
    }

    public boolean isShowPromisesInfo()
    {
        return getCost() == null;
    }

    public String getPromisesInfo()
    {
        if(isShowPromisesInfo())
        {
            if (IUniBaseDao.instance.get().existsEntity(CtrPriceElementCost.class, CtrPriceElementCost.period().element().id().s(), getEduProgramPrice().getId()))
            {
                return _uiConfig.getPropertySafe("ui.info.cost.empty");
            } else
            {
                return _uiConfig.getPropertySafe("ui.info.cost.notExist");
            }
        }

        return "";
    }

    public boolean isExistNextVersionPromises()
    {
        return !getNextVersionPromises().isEmpty();
    }

    // Getters, Setters
    public CtrContractVersion getPrevVersion() { return _prevVersion; }

    public EduCtrTermEducationCostAgreementTemplateData getTemplate() { return _template; }

    public CtrPriceElementCostStage getCostStage() { return _costStage; }

    public Date getCostStageDeadlineDate() { return _costStageDeadlineDate; }
    public void setCostStageDeadlineDate(Date costStageDeadlineDate) { _costStageDeadlineDate = costStageDeadlineDate; }

    public Collection<CtrPaymentPromice> getPrevVerPromises() { return _prevVerPromises; }

    private CtrPaymentPromice _prevVerPromise;
    public CtrPaymentPromice getPrevVerPromise() { return _prevVerPromise; }
    public void setPrevVerPromise(CtrPaymentPromice prevVerPromise) { _prevVerPromise = prevVerPromise; }


    public List<CostStageWrapper> getNextVersionPromises() {
        return nextVersionPromises;
    }

    public void setNextVersionPromises(List<CostStageWrapper> nextVersionPromises) {
        this.nextVersionPromises = nextVersionPromises;
    }

    public boolean isNextVersionPromiseSelected() { return selectedNextVersionPromises.contains(getNextVersionPromise()); }

    public void setNextVersionPromiseSelected(boolean promiseSelected)
    {
        if (promiseSelected)
            selectedNextVersionPromises.add(getNextVersionPromise());

        else selectedNextVersionPromises.remove(getNextVersionPromise());
    }

    public CostStageWrapper getNextVersionPromise() {
        return nextVersionPromise;
    }

    public void setNextVersionPromise(CostStageWrapper nextVersionPromise) {
        this.nextVersionPromise = nextVersionPromise;
    }

    // Util
    public boolean isPromiseSelected() { return _selectedPromises.contains(getPrevVerPromise()); }
    public void setPromiseSelected(boolean promiseSelected)
    {
        if (promiseSelected)
            _selectedPromises.add(getPrevVerPromise());

        else _selectedPromises.remove(getPrevVerPromise());;
    }


    public CtrPriceElementPeriod getPricePeriod()
    {
        return _pricePeriod;
    }

    public void setPricePeriod(CtrPriceElementPeriod pricePeriod)
    {
        _pricePeriod = pricePeriod;
    }

    public CtrPricePaymentGrid getPaymentGrid()
    {
        return _paymentGrid;
    }

    public void setPaymentGrid(CtrPricePaymentGrid paymentGrid)
    {
        _paymentGrid = paymentGrid;
    }

    public CtrPriceElementCost getCost()
    {
        return _cost;
    }

    public void setCost(CtrPriceElementCost cost)
    {
        _cost = cost;
    }

    public EduProgramPrice getEduProgramPrice()
    {
        return _eduProgramPrice;
    }
}