/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStopContractTemplate.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateDAO;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.unieductr.base.entity.EduCtrStopVersionTemplateData;

import java.util.Collection;
import java.util.Date;

/**
 * @author azhebko
 * @since 02.09.2014
 */
public interface IEduCtrStopContractDao extends ICtrContractTemplateDAO, INeedPersistenceSupport
{
    /** Создает версию договора (доп. соглашение о расторжении) на основе предоставленного шаблона. */
    public void doCreateCtrStopContractVersion(EduCtrStopVersionTemplateData versionTemplateData, CtrContractObject contract, CtrContractVersionCreateData versionCreateData, Date refundDate, Collection<Long> promiseIds);
}