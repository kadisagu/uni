package ru.tandemservice.unieductr.base.bo.EduContract.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.transformer.IOutputTransformer;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationAddPromise;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.base.entity.IEducationPromise;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EduCtrStudentContractListHandler extends DefaultSearchDataSourceHandler
{
    public static final String PARAM_STUDENT_ID = "entrantId";

    public static final String VIEW_PROPERTY_EDU_PROMISE_LIST = "educationPromiseList";
    public static final String VIEW_PROPERTY_EDU_PROMISE_FIRST = "educationPromiseFirst";


    public EduCtrStudentContractListHandler(final String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(final DSInput input, final ExecutionContext context) {
        DQLSelectBuilder studentContractsBuilder = getContractsByStudentIdBuilder(context);
        studentContractsBuilder.order(property("esc",EduCtrStudentContract.contractObject().number()));

        /* { eduCtrStudentContract.id -> { eduCtrEducationPromise } } */
        final Map<Long, List<IEducationPromise>> promiseMap = getPromiseMap(context);

        DSOutput output = DQLSelectOutputBuilder.get(input, studentContractsBuilder, context.getSession()).build().transform((EduCtrStudentContract contract) -> {
            final DataWrapper wrapper = new DataWrapper(contract);
            addPromiseToWrapper(wrapper, promiseMap, contract.getId());

            return wrapper;
        });

        for(IOutputTransformer rt : getOutputTransformers())
        {
            rt.transformOutput(input, output, context);
        }
        return output;

    }


    private void addPromiseToWrapper(DataWrapper wrapper, Map<Long, List<IEducationPromise>> promiseMap, Long studentContractId) {
        List<IEducationPromise> list = promiseMap.get(studentContractId);
        if (null == list || list.isEmpty()) {
            wrapper.putInMap(VIEW_PROPERTY_EDU_PROMISE_LIST, Collections.emptyList());
            wrapper.putInMap(VIEW_PROPERTY_EDU_PROMISE_FIRST, null);
        } else {
            list = new ArrayList<>(list); // создаем list ровно на выбранные значения
            wrapper.putInMap(VIEW_PROPERTY_EDU_PROMISE_LIST, list);
            wrapper.putInMap(VIEW_PROPERTY_EDU_PROMISE_FIRST, list.get(0));
        }
    }

    private Map<Long, List<IEducationPromise>> getPromiseMap(final ExecutionContext context) {
        final Map<Long, List<IEducationPromise>> promiseMap = new HashMap<>();
        final DQLSelectBuilder promiseDql = new DQLSelectBuilder()
                .fromEntity(EduCtrEducationPromise.class, "p")
                .fromEntity(EduCtrStudentContract.class, "x")
                .where(eq(
                        property(EduCtrStudentContract.contractObject().fromAlias("x")),
                        property(EduCtrEducationPromise.src().owner().contract().fromAlias("p"))
                ))
                .where(isNull(property(EduCtrEducationPromise.src().owner().removalDate().fromAlias("p"))))
                .where(in(property("x.id"), getContractsByStudentIdBuilder(context).column(property("esc", EduCtrStudentContract.id())).buildQuery()))
                .column(property("x.id"))
                .column(property("p"));

        final List<Object[]> rows = createStatement(promiseDql).list();
        for (final Object[] row : rows) {
            // как правило там одно значение
            SafeMap.safeGet(promiseMap, (Long) row[0], ArrayList.class).add((EduCtrEducationPromise) row[1]);
        }

        final DQLSelectBuilder promiseAddDql = new DQLSelectBuilder()
                .fromEntity(EduCtrEducationAddPromise.class, "p")
                .fromEntity(EduCtrStudentContract.class, "x")
                .where(eq(
                        property(EduCtrStudentContract.contractObject().fromAlias("x")),
                        property(EduCtrEducationAddPromise.src().owner().contract().fromAlias("p"))
                ))
                .where(isNull(property(EduCtrEducationAddPromise.src().owner().removalDate().fromAlias("p"))))
                .where(in(property("x.id"), getContractsByStudentIdBuilder(context).column(property("esc", EduCtrStudentContract.id())).buildQuery()))
                .column(property("x.id"))
                .column(property("p"));

        final List<Object[]> addRows = createStatement(promiseAddDql).list();
        for (final Object[] row : addRows) {
            // как правило там одно значение
            SafeMap.safeGet(promiseMap, (Long) row[0], ArrayList.class).add((EduCtrEducationAddPromise) row[1]);
        }

        return promiseMap;
    }

    private DQLSelectBuilder getContractsByStudentIdBuilder(ExecutionContext context) {
        Long studentId = context.get(EduCtrStudentContractListHandler.PARAM_STUDENT_ID);
        DQLSelectBuilder builder = new DQLSelectBuilder().
                fromEntity(EduCtrStudentContract.class, "esc");

        if (studentId != null)
            builder.where(eq(property("esc", EduCtrStudentContract.student().id()), value(studentId)));

        return builder;
    }

}
