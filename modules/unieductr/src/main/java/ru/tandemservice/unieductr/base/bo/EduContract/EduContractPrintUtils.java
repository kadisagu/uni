/* $Id:$ */
package ru.tandemservice.unieductr.base.bo.EduContract;

import java.util.List;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.ctr.base.bo.Contactor.ContactorManager;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPersonActivityBase;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.IdentityCardDeclinability;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonRole;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.ContractSides;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduContractDAO;
import ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData;

/**
 * Утильный класс для печати договоров и доп. соглашений на обучение
 *
 * @author oleyba
 * @since 7/27/12
 */
public class EduContractPrintUtils
{
    /* Поиск типа сторон договора на основании  данных о заказчике и обучаемом. */
    public static ContractSides getContractSides(ContactorPerson customer, ContactorPerson educationReceiver)
    {
        ContractSides contractSides = EduContractDAO.getContractSides(customer, educationReceiver);
        if(contractSides != null)
        {
            return contractSides;
        }

        throw new ApplicationException("Печать по шаблону невозможна - неизвестный тип заказчика.");
    }

    public static String called(PersonRole personRole)
    {
        return personRole.getPerson().isMale() ? "именуемый" : "именуемая";
    }

    public static void printProviderData(EmployeePostContactor provider, RtfInjectModifier modifier)
    {
        ContactorPersonActivityBase activityBase = ContactorManager.instance().dao().getCurrentCotactorPersonActivity(provider);

        modifier.put("post_G", StringUtils.trimToEmpty(provider.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getPost().getGenitiveCaseTitle()));
        modifier.put("orgUnit_G", StringUtils.trimToEmpty(provider.getEmployeePost().getOrgUnit().getGenitiveCaseTitle()));
        modifier.put("employeeFio_G", PersonManager.instance().declinationDao().getCalculatedFIODeclination(provider.getPerson().getIdentityCard(), InflectorVariantCodes.RU_GENITIVE));
        modifier.put("acted", provider.getPerson().isMale() ? "действующего" : "действующей");
        modifier.put("activityGround", activityBase == null ? "" : activityBase.getDisplayableTitle());
    }

    public static void printVersionData(IEducationContractVersionTemplateData templateData, CtrContractVersion contractVersion, RtfInjectModifier modifier)
    {
        modifier.put("contractNumber", contractVersion.getNumber());
        modifier.put("educationYear", templateData.getEducationYear().getTitle());

        modifier.put("formingDay", String.valueOf(CoreDateUtils.getDayOfMonth(contractVersion.getDocStartDate())));
        modifier.put("formingMonthStr", RussianDateFormatUtils.getMonthName(contractVersion.getDocStartDate(), false));
        modifier.put("formingYr", RussianDateFormatUtils.getYearString(contractVersion.getDocStartDate(), true));

        modifier.put("addFormingDay", String.valueOf(CoreDateUtils.getDayOfMonth(contractVersion.getDocStartDate())));
        modifier.put("addFormingMonthStr", RussianDateFormatUtils.getMonthName(contractVersion.getDocStartDate(), false));
        modifier.put("addFormingYr", RussianDateFormatUtils.getYearString(contractVersion.getDocStartDate(), true));

        modifier.put("contractBeginDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(contractVersion.getDurationBeginDate()) + " г.");
        modifier.put("contractBeginDay", String.valueOf(CoreDateUtils.getDayOfMonth(contractVersion.getDurationBeginDate())));
        modifier.put("contractBeginMonthStr", RussianDateFormatUtils.getMonthName(contractVersion.getDurationBeginDate(), false));
        modifier.put("contractBeginYr", RussianDateFormatUtils.getYearString(contractVersion.getDurationBeginDate(), true));

        modifier.put("contractEndDate", contractVersion.getDurationEndDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(contractVersion.getDurationEndDate()) + " г." : "");
        modifier.put("contractEndDay", contractVersion.getDurationEndDate() != null ? String.valueOf(CoreDateUtils.getDayOfMonth(contractVersion.getDurationEndDate())) : "");
        modifier.put("contractEndMonthStr", contractVersion.getDurationEndDate() != null ? RussianDateFormatUtils.getMonthName(contractVersion.getDurationEndDate(), false) : "");
        modifier.put("contractEndYr", contractVersion.getDurationEndDate() != null ? RussianDateFormatUtils.getYearString(contractVersion.getDurationEndDate(), true) : "");

        modifier.put("formativeOrgUnit", contractVersion.getContract().getOrgUnit().getPrintTitle());
        modifier.put("formativeOrgUnit_G", contractVersion.getContract().getOrgUnit().getGenitiveCaseTitle());
    }

    public static void printCustomerData(ContactorPerson customer, RtfInjectModifier modifier)
    {
        if (customer instanceof JuridicalContactor)
        {
            JuridicalContactor jCustomer = (JuridicalContactor) customer;
            ContactorPersonActivityBase cActivityBase = ContactorManager.instance().dao().getCurrentCotactorPersonActivity(jCustomer);

            ExternalOrgUnit extOrgUnit = jCustomer.getExternalOrgUnit();
            modifier.put("customerShortInfo", extOrgUnit.getTitle());
            modifier.put("customerPlenipotenriaryPost", jCustomer.getPost());
            modifier.put("customerPlenipotenriaryFio", customer.getPerson().getFullFio());

            modifier.put("customerActivityGround", cActivityBase == null ? "" : cActivityBase.getDisplayableTitle());


            modifier.put("customerTitle", extOrgUnit.getLegalFormWithTitle());
            modifier.put("customerInfoCaption", "Банковские реквизиты");
            modifier.put("customerAddress", extOrgUnit.getLegalAddress() == null ? "" : extOrgUnit.getLegalAddress().getTitleWithFlat());
            modifier.put("customerOrgFactAddress", extOrgUnit.getFactAddress() == null ? "" : extOrgUnit.getFactAddress().getTitleWithFlat());

            modifier.put("customerINNKPP", StringUtils.trimToEmpty(extOrgUnit.getInn()));
            modifier.put("customerBIK", StringUtils.trimToEmpty(extOrgUnit.getBic()));
            modifier.put("customerCurAcc", StringUtils.trimToEmpty(extOrgUnit.getCurAccount()));
            modifier.put("customerCorAcc", StringUtils.trimToEmpty(extOrgUnit.getCorAccount()));
            modifier.put("customerBankTitle", StringUtils.trimToEmpty(extOrgUnit.getBank()));
            modifier.put("customerOrgPhoneNumber", StringUtils.trimToEmpty(extOrgUnit.getPhone()));
            modifier.put("customerOtherRequisites", StringUtils.trimToEmpty(extOrgUnit.getMiscellaneousDetails()));
            modifier.put("customerOrgOKATO", StringUtils.trimToEmpty(extOrgUnit.getOkato()));
            modifier.put("customerOrgOGRN", StringUtils.trimToEmpty(extOrgUnit.getOgrn()));
            modifier.put("customerOrgOKPO", StringUtils.trimToEmpty(extOrgUnit.getOkpo()));
        }
        else
        {
            IdentityCard idc = customer.getPerson().getIdentityCard();
            modifier.put("customerInfoCaption", idc.getCardType().getShortTitle());
            modifier.put("customerActivityGround", "личной инициативы");
        }

        modifier.put("calledCustomer", called(customer));
        modifier.put("actedCustomer", customer.getPerson().isMale() ? "действующий" : "действующая");

        {
            IdentityCard idc = customer.getPerson().getIdentityCard();
            modifier.put("customerFio", customer.getPerson().getFullFio());

            String customerPersonFioGen;
            Person person = customer.getPerson();
            IdentityCardDeclinability identityCardDeclinability = IUniBaseDao.instance.get().get(IdentityCardDeclinability.class, IdentityCardDeclinability.identityCard(), person.getIdentityCard());
            if (identityCardDeclinability != null)
            {
                String lastName = person.getIdentityCard().getLastName();
                if (identityCardDeclinability.isLastNameDeclinable())
                    lastName = PersonManager.instance().declinationDao().getDeclinationLastName(lastName, GrammaCase.GENITIVE, person.isMale());

                String firstName = person.getIdentityCard().getFirstName();
                if (identityCardDeclinability.isFirstNameDeclinable())
                    firstName = PersonManager.instance().declinationDao().getDeclinationFirstName(firstName, GrammaCase.GENITIVE, person.isMale());

                String middleName = person.getIdentityCard().getMiddleName();
                if (identityCardDeclinability.isMiddleNameDeclinable())
                    middleName = PersonManager.instance().declinationDao().getDeclinationMiddleName(middleName, GrammaCase.GENITIVE, person.isMale());

                List<String> result = Lists.newArrayList();
                if (!StringUtils.isEmpty(lastName)) result.add(lastName);
                if (!StringUtils.isEmpty(firstName)) result.add(firstName);
                if (!StringUtils.isEmpty(middleName)) result.add(middleName);

                customerPersonFioGen = StringUtils.join(result, ' ');

            } else
            {
                customerPersonFioGen = PersonManager.instance().declinationDao().getDeclinationFIO(person.getIdentityCard(), GrammaCase.GENITIVE);
            }

            modifier.put("customerPersonFio_G", customerPersonFioGen);
            modifier.put("customerPassportSeria", idc.getSeria());
            modifier.put("customerPassportNumber", idc.getNumber());
            modifier.put("customerPassportDate", idc.getIssuanceDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(idc.getIssuanceDate()));
            modifier.put("customerPassportInfo", StringUtils.trimToEmpty(idc.getIssuancePlace()));
            modifier.put("customerRegAddress", idc.getAddress() == null ? "" : idc.getAddress().getTitleWithFlat());
            modifier.put("customerPersonFactAddress", customer.getPerson().getAddress() !=  null ? customer.getPerson().getAddress().getShortTitleWithSettlement() : "");
            modifier.put("customerPersonFactPhone", customer.getPerson().getContactData().getPhoneFact() !=  null ? customer.getPerson().getContactData().getPhoneFact() : "");
            modifier.put("customerPersonPhoneMobile", customer.getPerson().getContactData().getPhoneMobile() !=  null ? customer.getPerson().getContactData().getPhoneMobile() : "");
            modifier.put("customerPhoneNumber", idc.getPerson().getContactData().getMainPhones());
        }
    }

    public static void printPaymentData(RtfInjectModifier modifier, List<CtrPaymentPromice> paymentPromices)
    {
        if (paymentPromices.isEmpty())
            throw new ApplicationException("Печать по шаблону невозможна - в версии договора отсутствуют обязательства по оплате.");
        CtrPaymentPromice paymentPromice = paymentPromices.get(0);

        long sum = 0L;
        for (CtrPaymentPromice promice : paymentPromices)
            sum = sum + promice.getCostAsLong();
        Double sumD = Currency.wrap(sum);
        Double eduPriceFirstStage = Currency.wrap((Long) paymentPromice.getCostAsLong());

        modifier.put("eduFullPrice", String.valueOf(sumD.intValue()));
        modifier.put("eduFullPriceStr", NumberSpellingUtil.spellNumberMasculineGender(sumD.intValue()));
        modifier.put("eduFullPriceKop", String.valueOf(Math.round(100.0d * sumD) - 100 * sumD.intValue()));
        modifier.put("eduFullPriceKopStr", NumberSpellingUtil.spellNumberFeminineGender(Math.round(100.0d * sumD) - 100 * sumD.intValue()));

        modifier.put("chargeFirstYearRate", String.valueOf(sumD.intValue()));
        modifier.put("chargeFirstYearRateInWords", NumberSpellingUtil.spellNumberMasculineGender(sumD.intValue()));
        modifier.put("chargeFirstYearRateKop", String.valueOf(Math.round(100.0d * sumD) - 100 * sumD.intValue()));

        modifier.put("chargeNewYearRate", String.valueOf(sumD.intValue()));
        modifier.put("chargeNewYearRateInWords", NumberSpellingUtil.spellNumberMasculineGender(sumD.intValue()));
        modifier.put("chargeNewYearRateKop", String.valueOf(Math.round(100.0d * sumD) - 100 * sumD.intValue()));

        modifier.put("eduPriceFirstStage", String.valueOf(eduPriceFirstStage.intValue()));
        modifier.put("eduPriceFirstStageStr", NumberSpellingUtil.spellNumberMasculineGender(eduPriceFirstStage.intValue()));
        modifier.put("eduPriceFirstStageKop", String.valueOf(Math.round(100.0d * eduPriceFirstStage) - 100 * eduPriceFirstStage.intValue()));
        modifier.put("eduPriceFirstStageKopStr", NumberSpellingUtil.spellNumberFeminineGender(Math.round(100.0d * eduPriceFirstStage) - 100 * eduPriceFirstStage.intValue()));

        modifier.put("payDay", String.valueOf(CoreDateUtils.getDayOfMonth(paymentPromice.getDeadlineDate())));
        modifier.put("payMonthStr", RussianDateFormatUtils.getMonthName(paymentPromice.getDeadlineDate(), false));
        modifier.put("payYr", RussianDateFormatUtils.getYearString(paymentPromice.getDeadlineDate(), true));
    }

    public static void printAcademyData(RtfInjectModifier modifier)
    {
        TopOrgUnit academy = TopOrgUnit.getInstance(true);
        AcademyData academyData = AcademyData.getInstance();
        EmployeePost headFio = EmployeeManager.instance().dao().getHead(academy);

        modifier.put("academy", academy.getPrintTitle());
        modifier.put("licence", academyData.getLicenceTitle());
        modifier.put("certificate", academyData.getCertificateTitle());

        modifier.put("INN", StringUtils.trimToEmpty(academy.getInn()));
        modifier.put("BIK", StringUtils.trimToEmpty(academy.getBic()));
        modifier.put("CurAcc", StringUtils.trimToEmpty(academy.getCurAccount()));
        modifier.put("CorAcc", StringUtils.trimToEmpty(academy.getCorAccount()));
        modifier.put("BankTitle", StringUtils.trimToEmpty(academy.getBank()));
        modifier.put("OKATO", StringUtils.trimToEmpty(academy.getOkato()));
        modifier.put("address", academy.getAddress() == null ? "" : academy.getAddress().getTitleWithFlat());
        modifier.put("rectorFio", StringUtils.trimToEmpty(headFio == null ? "" : headFio.getPerson().getFio()));
    }
}
