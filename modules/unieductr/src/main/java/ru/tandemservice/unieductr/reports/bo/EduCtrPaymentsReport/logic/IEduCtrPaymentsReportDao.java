/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrPaymentsReport.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport;

/**
 * @author Alexey Lopatin
 * @since 17.04.2015
 */
public interface IEduCtrPaymentsReportDao extends INeedPersistenceSupport
{
    /**
     * Создает документ отчета.
     * @return RtfDocument
     */
    <M extends Model> RtfDocument createReportRtfDocument(M model);

    /**
     * Сохраняет отчет и его печатную форму.
     */
    <M extends Model> EduCtrPaymentsReport saveReport(M model, RtfDocument document);
}
