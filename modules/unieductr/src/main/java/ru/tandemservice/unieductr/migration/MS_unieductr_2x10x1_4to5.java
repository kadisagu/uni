package ru.tandemservice.unieductr.migration;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.*;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.ctr.base.util.CtrMigrationUtil;

import java.sql.Blob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unieductr_2x10x1_4to5 extends IndependentMigrationScript
{
    // CtrPrintTemplateCodes
    /** Константа кода (code) элемента : Основной (title) */
    private String EDU_CONTRACT_TEMPLATE_VO_2_SIDES = "edu.ctr.template.vo.2s";
    /** Константа кода (code) элемента : Основной (title) */
    private String EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON = "edu.ctr.template.vo.3sp";
    /** Константа кода (code) элемента : Основной (title) */
    private String EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG = "edu.ctr.template.vo.3so";
    /** Константа кода (code) элемента : Основной (title) */
    private String EDU_CONTRACT_TEMPLATE_SPO_2_SIDES = "edu.ctr.template.spo.2s";
    /** Константа кода (code) элемента : Основной (title) */
    private String EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_PERSON = "edu.ctr.template.spo.3sp";
    /** Константа кода (code) элемента : Основной (title) */
    private String EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_ORG = "edu.ctr.template.spo.3so";
    /** Константа кода (code) элемента : Основной (title) */
    private String TERM_EDU_COST_TEMPLATE_2_SIDES = "edu.ctr.template.termeducost.2s";
    /** Константа кода (code) элемента : Основной (title) */
    private String TERM_EDU_COST_TEMPLATE_3_SIDES_PERSON = "edu.ctr.template.termeducost.3sp";
    /** Константа кода (code) элемента : Основной (title) */
    private String TERM_EDU_COST_TEMPLATE_3_SIDES_ORG = "edu.ctr.template.termeducost.3so";
    /** Константа кода (code) элемента : Основной (title) */
    private String PROGRAM_CHANGE_TEMPLATE_2_SIDES = "edu.ctr.template.programchange.2s";
    /** Константа кода (code) элемента : Основной (title) */
    private String PROGRAM_CHANGE_TEMPLATE_3_SIDES_PERSON = "edu.ctr.template.programchange.3sp";
    /** Константа кода (code) элемента : Основной (title) */
    private String PROGRAM_CHANGE_TEMPLATE_3_SIDES_ORG = "edu.ctr.template.programchange.3so";
    /** Константа кода (code) элемента : Основной (title) */
    private String EDU_CTR_STOP_TEMPLATE_CONTRACT = "edu.ctr.template.stopcontract";

    // CtrContractKindCodes
    /** Константа кода (code) элемента : Договор на обучение по ОП ВО (двухсторонний) (title) */
    private String EDU_CONTRACT_VO_2_SIDES = "edu.vo.2s";
    /** Константа кода (code) элемента : Договор на обучение по ОП ВО (трехсторонний с физ. лицом) (title) */
    private String EDU_CONTRACT_VO_3_SIDES_PERSON = "edu.vo.3sp";
    /** Константа кода (code) элемента : Договор на обучение по ОП ВО (трехсторонний с юр. лицом) (title) */
    private String EDU_CONTRACT_VO_3_SIDES_ORG = "edu.vo.3so";
    /** Константа кода (code) элемента : Договор на обучение по ОП СПО (двухсторонний) (title) */
    private String EDU_CONTRACT_SPO_2_SIDES = "edu.spo.2s";
    /** Константа кода (code) элемента : Договор на обучение по ОП СПО (трехсторонний с физ. лицом) (title) */
    private String EDU_CONTRACT_SPO_3_SIDES_PERSON = "edu.spo.3sp";
    /** Константа кода (code) элемента : Договор на обучение по ОП СПО (трехсторонний с юр. лицом) (title) */
    private String EDU_CONTRACT_SPO_3_SIDES_ORG = "edu.spo.3so";
    /** Константа кода (code) элемента : Доп. соглашение об установлении стоимости обучения за семестр (двухсторонний) (title) */
    private String TERM_EDU_COST_2_SIDES = "edu.termeducost.2s";
    /** Константа кода (code) элемента : Доп. соглашение об установлении стоимости обучения за семестр (трехсторонний с физ. лицом) (title) */
    private String TERM_EDU_COST_3_SIDES_PERSON = "edu.termeducost.3sp";
    /** Константа кода (code) элемента : Доп. соглашение об установлении стоимости обучения за семестр (трехсторонний с юр. лицом) (title) */
    private String TERM_EDU_COST_3_SIDES_ORG = "edu.termeducost.3so";
    /** Константа кода (code) элемента : Доп. соглашение об изменении ОП (двухсторонний) (title) */
    private String PROGRAM_CHANGE_2_SIDES = "edu.programchange.2s";
    /** Константа кода (code) элемента : Доп. соглашение об изменении ОП (трехсторонний с физ. лицом) (title) */
    private String PROGRAM_CHANGE_3_SIDES_PERSON = "edu.programchange.3sp";
    /** Константа кода (code) элемента : Доп. соглашение об изменении ОП (трехсторонний с юр. лицом) (title) */
    private String PROGRAM_CHANGE_3_SIDES_ORG = "edu.programchange.3so";
    /** Константа кода (code) элемента : Доп. соглашение на расторжение договоров (title) */
    private String EDU_CTR_STOP_CONTRACT = "edu.ctrstopcontract";

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1")
                };
    }


    @Override
    public void run(DBTool tool) throws Exception
    {
        // Получаем уже существующие элементы ctrcontractkind_t
        Map<String, Long> contractKindMap = MigrationUtils.getCatalogCode2IdMap(tool, "ctrcontractkind_t");

        // Получаем уже существующие элементы ctrPrintTemplate
        final Map<String, Long> ctrPrintTemplateMap = Maps.newHashMap();

        SQLSelectQuery selectCtrPrintTemplateQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_c_prnt_tmpl_t", "pt").innerJoin(SQLFrom.table("scriptitem_t", "si"), "si.id=pt.id"))
                .column("si.id")
                .column("si.code_p");

        List<Object[]> ctrPrintTemplates = tool.executeQuery(
                MigrationUtils.processor(Long.class, String.class),
                tool.getDialect().getSQLTranslator().toSql(selectCtrPrintTemplateQuery));

        for(Object[] obj : ctrPrintTemplates)
        {
            ctrPrintTemplateMap.put((String) obj[1], (Long) obj[0]);
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность ctrContractVersion

        // заполняем printTemplate и kind

        SQLSelectQuery selectContractVersionQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv")).column("id");

        List<Object[]> contractVersionList = tool.executeQuery(
                MigrationUtils.processor(Long.class),
                tool.getDialect().getSQLTranslator().toSql(selectContractVersionQuery));

        for (Object[] contractVersion : contractVersionList)
        {
            fixCtrPrintTemplateAndKind((Long) contractVersion[0], ctrPrintTemplateMap, contractKindMap, tool);
        }
    }

    private void fixCtrPrintTemplateAndKind(Long contractVersionId, Map<String, Long> ctrPrintTemplateMap, Map<String, Long> contractKindMap, DBTool tool) throws SQLException
    {
        SQLUpdateQuery updateQuery = new SQLUpdateQuery("ctr_contractver_t")
                .set("kind_id", "?")
                .set("printtemplate_id", "?")
                .where("id=?");


        List<Long> voPrintTemplatesIds = Lists.newArrayList(
                ctrPrintTemplateMap.get(EDU_CONTRACT_TEMPLATE_VO_2_SIDES),
                ctrPrintTemplateMap.get(EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON),
                ctrPrintTemplateMap.get(EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG)
        );

        if(CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "eductr_stduent_c_ctmpldt_t", tool) && (CtrMigrationUtil.checkContractType(contractVersionId, "01.01.vpo", tool) || CtrMigrationUtil.checkContractType(contractVersionId, "01.01", tool)) && checkPrintTemplate(contractVersionId, voPrintTemplatesIds, tool))
        {
            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery),
                    getKind(contractVersionId,
                            EDU_CONTRACT_VO_2_SIDES,
                            EDU_CONTRACT_VO_3_SIDES_PERSON,
                            EDU_CONTRACT_VO_3_SIDES_ORG,
                            contractKindMap, tool),
                    getPrintTemplate(contractVersionId,
                            EDU_CONTRACT_TEMPLATE_VO_2_SIDES,
                            EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON,
                            EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG,
                            ctrPrintTemplateMap, tool),
                    contractVersionId);
            return;
        }


        List<Long> spoPrintTemplatesIds = Lists.newArrayList(
                ctrPrintTemplateMap.get(EDU_CONTRACT_TEMPLATE_SPO_2_SIDES),
                ctrPrintTemplateMap.get(EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_PERSON),
                ctrPrintTemplateMap.get(EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_ORG)
        );

        if(CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "eductr_stduent_spo_ctmpldt_t", tool) && CtrMigrationUtil.checkContractType(contractVersionId, "01.01.spo", tool) && checkPrintTemplate(contractVersionId, spoPrintTemplatesIds, tool))
        {
            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery),
                    getKind(contractVersionId,
                            EDU_CONTRACT_SPO_2_SIDES,
                            EDU_CONTRACT_SPO_3_SIDES_PERSON,
                            EDU_CONTRACT_SPO_3_SIDES_ORG,
                            contractKindMap, tool),
                    getPrintTemplate(contractVersionId,
                            EDU_CONTRACT_TEMPLATE_SPO_2_SIDES,
                            EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_PERSON,
                            EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_ORG,
                            ctrPrintTemplateMap, tool),
                    contractVersionId);
            return;
        }

        List<Long> programChangePrintTemplatesIds = Lists.newArrayList(
                ctrPrintTemplateMap.get(PROGRAM_CHANGE_TEMPLATE_2_SIDES),
                ctrPrintTemplateMap.get(PROGRAM_CHANGE_TEMPLATE_3_SIDES_PERSON),
                ctrPrintTemplateMap.get(PROGRAM_CHANGE_TEMPLATE_3_SIDES_ORG)
        );

        if(CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "eductr_progchange_a_ctmplt_t",tool) && checkPrintTemplate(contractVersionId, programChangePrintTemplatesIds, tool))
        {
            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery),
                    getKind(contractVersionId,
                            PROGRAM_CHANGE_2_SIDES,
                            PROGRAM_CHANGE_3_SIDES_PERSON,
                            PROGRAM_CHANGE_3_SIDES_ORG,
                            contractKindMap, tool),
                    getPrintTemplate(contractVersionId,
                            PROGRAM_CHANGE_TEMPLATE_2_SIDES,
                            PROGRAM_CHANGE_TEMPLATE_3_SIDES_PERSON,
                            PROGRAM_CHANGE_TEMPLATE_3_SIDES_ORG,
                            ctrPrintTemplateMap, tool),
                    contractVersionId);
            return;
        }

        List<Long> termEduPrintTemplatesIds = Lists.newArrayList(
                ctrPrintTemplateMap.get(TERM_EDU_COST_TEMPLATE_2_SIDES),
                ctrPrintTemplateMap.get(TERM_EDU_COST_TEMPLATE_3_SIDES_PERSON),
                ctrPrintTemplateMap.get(TERM_EDU_COST_TEMPLATE_3_SIDES_ORG)
        );

        if(CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "eductr_termeducost_a_ctmplt_t", tool) && checkPrintTemplate(contractVersionId, termEduPrintTemplatesIds, tool))
        {
            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery),
                    getKind(contractVersionId,
                            TERM_EDU_COST_2_SIDES,
                            TERM_EDU_COST_3_SIDES_PERSON,
                            TERM_EDU_COST_3_SIDES_ORG,
                            contractKindMap, tool),
                    getPrintTemplate(contractVersionId,
                            TERM_EDU_COST_TEMPLATE_2_SIDES,
                            TERM_EDU_COST_TEMPLATE_3_SIDES_PERSON,
                            TERM_EDU_COST_TEMPLATE_3_SIDES_ORG,
                            ctrPrintTemplateMap, tool),
                    contractVersionId);
            return;
        }

        if(CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "eductr_stop_ctmpldt_t", tool))
        {
            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery),
                    contractKindMap.get(EDU_CTR_STOP_CONTRACT),
                    ctrPrintTemplateMap.get(EDU_CTR_STOP_TEMPLATE_CONTRACT),
                    contractVersionId);
        }
    }

    private boolean checkPrintTemplate(Long contractVersionId, List<Long> printTemplatesIds, DBTool tool) throws SQLException
    {
        SQLSelectQuery selectQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t")).column("printtemplate_id")
                .where("id=?");

        Long printTemplateId = (Long) tool.getUniqueResult(tool.getDialect().getSQLTranslator().toSql(selectQuery), contractVersionId);

        return printTemplateId != null && printTemplatesIds.contains(printTemplateId);

    }

    private Long getKind(Long contractVersionId, String twoSide, String threeSidePerson, String threeSideOrg, Map<String, Long> contractKindMap, DBTool tool) throws SQLException
    {
        Long ctrKind;

        if(checkTwoSides(contractVersionId, tool))
        {
            ctrKind = contractKindMap.get(twoSide);
        }
        else
        {
            if(checkPersonSide(contractVersionId, tool))
            {
                ctrKind = contractKindMap.get(threeSidePerson);
            }
            else
            {
                ctrKind = contractKindMap.get(threeSideOrg);
            }
        }

        return ctrKind;
    }


    private Long getPrintTemplate(Long contractVersionId, String twoSide, String threeSidePerson, String threeSideOrg, Map<String, Long> ctrPrintTemplateMap, DBTool tool) throws SQLException
    {
        Long ctrPrintTemplate;

        if(checkTwoSides(contractVersionId, tool))
        {
            ctrPrintTemplate = ctrPrintTemplateMap.get(twoSide);
        }
        else
        {
            if(checkPersonSide(contractVersionId, tool))
            {
                ctrPrintTemplate = ctrPrintTemplateMap.get(threeSidePerson);
            }
            else
            {
                ctrPrintTemplate = ctrPrintTemplateMap.get(threeSideOrg);
            }
        }

        return ctrPrintTemplate;
    }

    private boolean checkTwoSides(Long contractVersionId, DBTool tool) throws SQLException
    {
        ISQLTranslator translator = tool.getDialect().getSQLTranslator();
        SQLSelectQuery contractorQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractor_role_t", "cr")
                .innerJoin(SQLFrom.table("ctr_c_contract_role_t", "r"), "r.id=cr.role_id")
                .innerJoin(SQLFrom.table("ctr_contractor_t", "c"), "c.id=cr.contactor_id")
                .innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=c.contactor_id")
        )
                .column("pr.person_id")
                .where("c.owner_id=?")
                .where("r.code_p=?");

        // CtrContractRole
        /** Константа кода (code) элемента : Заказчик (title) */
        String EDU_CONTRACT_CUSTOMER = "01.01.customer";
        /** Константа кода (code) элемента : Исполнитель (title) */
        String EDU_CONTRACT_PROVIDER = "01.01.provider";

        Long providerPerson = (Long) tool.getUniqueResult(translator.toSql(contractorQuery), contractVersionId, EDU_CONTRACT_PROVIDER);
        Long customerPerson = (Long) tool.getUniqueResult(translator.toSql(contractorQuery), contractVersionId, EDU_CONTRACT_CUSTOMER);

        SQLSelectQuery studentPersonQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv").innerJoin(SQLFrom.table("eductr_student_contract_t", "sc"), "sc.contractobject_id=cv.contract_id") .innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=sc.student_id")).column("pr.person_id")
                .where("cv.id=?");

        Long studentPerson = (Long) tool.getUniqueResult(translator.toSql(studentPersonQuery), contractVersionId);

        if(studentPerson == null)
        {
            return true;
        }

        return Objects.equals(studentPerson, providerPerson) || Objects.equals(studentPerson, customerPerson);
    }

    private boolean checkPersonSide(Long contractVersionId, DBTool tool) throws SQLException
    {
        // Проверяем по наличию юр. лица

        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        SQLSelectQuery contractorQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractor_role_t", "cr")
                .innerJoin(SQLFrom.table("ctr_c_contract_role_t", "r"), "r.id=cr.role_id")
                .innerJoin(SQLFrom.table("ctr_contractor_t", "c"), "c.id=cr.contactor_id")
                .innerJoin(SQLFrom.table("juridicalcontactor_t", "jc"), "jc.id=c.contactor_id")
        )
                .where("c.owner_id=?")
                .column("jc.id");

        List<Object[]> juridicalContator = tool.executeQuery(MigrationUtils.processor(Long.class), translator.toSql(contractorQuery), contractVersionId);

        return juridicalContator.isEmpty();
    }
}
