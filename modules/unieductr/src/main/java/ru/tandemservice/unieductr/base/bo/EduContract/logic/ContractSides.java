/* $Id$ */
package ru.tandemservice.unieductr.base.bo.EduContract.logic;

/**
 *
 * Стороны договора: студент - ОУ, студент - организация - ОУ, студент - физ. лицо - ОУ.
 *
 * @author nvankov
 * @since 17.05.2016
 */
public enum ContractSides
{
    TWO_SIDES, THREE_SIDES_ORG, THREE_SIDES_PERSON
}
