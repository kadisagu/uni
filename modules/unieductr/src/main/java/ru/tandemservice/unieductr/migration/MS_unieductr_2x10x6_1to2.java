package ru.tandemservice.unieductr.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unieductr_2x10x6_1to2 extends IndependentMigrationScript
{
    /** Константа кода (code) элемента : Доп. соглашение об установлении стоимости обучения за семестр (двухсторонний) (title) */
    public String TERM_EDU_COST_2_SIDES = "edu.termeducost.2s";
    /** Константа кода (code) элемента : Доп. соглашение об установлении стоимости обучения за семестр (трехсторонний с физ. лицом) (title) */
    public String TERM_EDU_COST_3_SIDES_PERSON = "edu.termeducost.3sp";
    /** Константа кода (code) элемента : Доп. соглашение об установлении стоимости обучения за семестр (трехсторонний с юр. лицом) (title) */
    public String TERM_EDU_COST_3_SIDES_ORG = "edu.termeducost.3so";

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.6")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // переименование элеметов справочника ctrСontractkind (ctrcontractkind_t)

        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        SQLUpdateQuery updateTitleQuery = new SQLUpdateQuery("ctrcontractkind_t")
                .set("title_p", "?")
                .where("title_p=?")
                .where("code_p=?");

        // Доп. соглашение об установлении стоимости обучения за семестр (двухсторонний) → Доп. соглашение об изменении стоимости обучения, порядка оплаты (двухсторонний)
        tool.executeUpdate(translator.toSql(updateTitleQuery),
                "Доп. соглашение об изменении стоимости обучения, порядка оплаты (двухсторонний)",
                "Доп. соглашение об изменение стоимости обучения, порядка оплаты (двухсторонний)",
                TERM_EDU_COST_2_SIDES
        );

        // Доп. соглашение об установлении стоимости обучения за семестр (трехсторонний с физ. лицом) → Доп. соглашение об изменении стоимости обучения, порядка оплаты (трехсторонний с физ. лицом)
        tool.executeUpdate(translator.toSql(updateTitleQuery),
                "Доп. соглашение об изменении стоимости обучения, порядка оплаты (трехсторонний с физ. лицом)",
                "Доп. соглашение об изменение стоимости обучения, порядка оплаты (трехсторонний с физ. лицом)",
                TERM_EDU_COST_3_SIDES_PERSON
        );

        // Доп. соглашение об установлении стоимости обучения за семестр (трехсторонний с юр. лицом) → Доп. соглашение об изменении стоимости обучения, порядка оплаты (трехсторонний с юр. лицом)
        tool.executeUpdate(translator.toSql(updateTitleQuery),
                "Доп. соглашение об изменении стоимости обучения, порядка оплаты (трехсторонний с юр. лицом)",
                "Доп. соглашение об изменение стоимости обучения, порядка оплаты (трехсторонний с юр. лицом)",
                TERM_EDU_COST_3_SIDES_ORG
        );
    }
}
