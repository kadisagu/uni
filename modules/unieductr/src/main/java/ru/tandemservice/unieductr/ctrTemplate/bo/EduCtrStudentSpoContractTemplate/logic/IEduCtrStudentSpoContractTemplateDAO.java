package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentSpoContractTemplate.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateDAO;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentSpoContractTemplateData;

/**
 * @author vdanilov
 */
public interface IEduCtrStudentSpoContractTemplateDAO extends ICtrContractTemplateDAO, INeedPersistenceSupport {

    void doUpdateContractVersionContactor(CtrContractVersion version, ContactorPerson provider, ContactorPerson customer);

    /** @param templateData */
    void doSaveTemplate(EduCtrStudentSpoContractTemplateData templateData);

    /**
     * @param templateAddData
     * @return
     */
    EduCtrStudentSpoContractTemplateData doCreateVersion(IEduCtrStudentSpoContractTemplateAddData templateAddData);

}
