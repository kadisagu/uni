package ru.tandemservice.unieductr.base.bo.EduContract.ui.VersionPub;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrPromiceManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContextObject;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.CtrContractVersionContextOwnerUIPublisher;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.CtrContractVersionInlinePubUIPresenter;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.InContractSectionList.CtrInContractListUIPresenter;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractPromice;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractPromiceType;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.base.entity.IEducationPromise;
import ru.tandemservice.unieductr.student.entity.IEduStudentContract;

import javax.annotation.Nullable;
import java.util.*;
import java.util.Map.Entry;

/**
 * Публикатор для версии договора на обучение
 * @author vdanilov
 */
public class EduContractVersionPubUI extends CtrContractVersionInlinePubUIPresenter
{
    // студент, в рамках которого открыт договор (если это студент)
    @Nullable
    private Student student;
    public Student getStudent() { return this.student; }
    public void setStudent(final Student student) { this.student = student; }

    // обязательство на обучение
    @Nullable
    private IEducationPromise educationPromise;
    public IEducationPromise getEducationPromise() { return this.educationPromise; }
    public void setEducationPromise(final IEducationPromise educationPromise) { this.educationPromise = educationPromise; }

    // по контексту договора определяет студента (если он есть)
    private Student resolveStudent(final ICtrContextObject contractContextObject) {
        if (contractContextObject instanceof IEduStudentContract) {
            return ((IEduStudentContract)contractContextObject).getStudent();
        }
        else{
            return EduContractManager.instance().dao().getStudentByContract(contractContextObject.getContractObject());
        }
    }

    public ParametersMap getStudentComponentParams()
    {
        if(getStudent() != null)
        {
            return ParametersMap.createWith(PUBLISHER_ID, getStudent().getId()).add("selectedStudentTab", "studentTab");
        }
        else
            return null;
    }

    // среди обязателств находит обязательство на обучение студента, если таковых нет - возвращает первое
    private IEducationPromise resulveEducationPromise(final Collection<CtrContractPromice> promiceList, final Student student) {

        @SuppressWarnings("unchecked")
        final
        Collection<IEducationPromise> eduPromiseList = (Collection)CollectionUtils.select(promiceList, object -> object instanceof IEducationPromise);

        if (eduPromiseList.isEmpty()) {
            return null;
        }

        if (null != student) {
            final Person studentPerson = student.getPerson();
            final IEducationPromise candidate = CollectionUtils.find(eduPromiseList, object -> object.getDst().getContactor().getPerson().equals(studentPerson));
            if (null != candidate) {
                return candidate;
            }
        }

        return eduPromiseList.iterator().next();
    }

    // список секций (дочерних компонентов) обязателств, зарегистрированных в презентере
    @SuppressWarnings("unchecked")
    public Collection<Map.Entry<String, CtrInContractListUIPresenter>> getSectionRootEntries() {
        return (Collection) CollectionUtils.select(this._uiSupport.getChildrenUI().entrySet(), entry -> (entry.getValue() instanceof CtrInContractListUIPresenter));
    }

    // список названий из getSectionRootEntries
    public Collection<String> getSectionRegionNames() {
        return CollectionUtils.collect(this.getSectionRootEntries(), Entry::getKey);
    }

    /** @return название региона для шага */
    protected String getRegionName(final String name) {
        return (null == name ? UIDefines.DEFAULT_REGION_NAME : ("scope_"+name).replace('.', '_'));
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        // сначала находим студента, если он есть
        this.setStudent(this.resolveStudent(this.getParent().getHolder().getValue()));

        // затем, находим соответствующее ему обязательство на обучение
        this.setEducationPromise(this.resulveEducationPromise(this.getContext().getPromiceList(), this.getStudent()));

        // стартуем все необходимые компоненты секций обязательств на обучение (если договор в свободной форме)
        {
            final Set<String> promiseRegionNames = new HashSet<>();
            if (null == getContext().getTemplateData()) {
                final List<CtrContractPromiceType> tpList = DataAccessServices.dao().getList(CtrContractPromiceType.class);
                for (final CtrContractPromiceType tp: tpList) {
                    final ICtrPromiceManager promiceManager = CtrContractVersionManager.getPromiceManager(tp, true);
                    if (null == promiceManager) { continue; }
                    if (!IEducationPromise.class.isAssignableFrom(promiceManager.promiceClass())) { continue; }

                    final Class<? extends BusinessComponentManager> rootSectionClass = promiceManager.rootSectionComponent();
                    if (null == rootSectionClass) { continue; }

                    final String regionName = this.getRegionName(rootSectionClass.getName());
                    if (!promiseRegionNames.add(regionName)) { throw new IllegalStateException(); }

                    if (null == this._uiSupport.getChildUI(regionName)) {
                        // создаем компонент
                        this._uiActivation
                        .asRegion(rootSectionClass, regionName)
                        .parameter(CtrContractVersionContextOwnerUIPublisher.PARAM_CONTEXT, this.getContext())
                        .activate();
                    }
                }
            }

            for (final String regionName: this.getSectionRegionNames()) {
                if (!promiseRegionNames.contains(regionName)) {
                    final IUIPresenter childUI = this._uiSupport.getChildUI(regionName);
                    if (null != childUI) {
                        // здесь не нужно вызывать refresh
                        childUI.getConfig().getBusinessComponent().getParentRegion().deactivateComponent(false);
                    }
                }
            }
        }

    }


    /** удаление договора на обучение с карточки связи */
    public void onDeleteEntity()
    {
        try {
            // удаляем договор со связью
            EduContractManager.instance().dao().doDeleteContract(
                this.getParent().getHolder().getValue()
            );
        } finally {
            // обновляем компонент (если ошибка - тем более)
            this.getSupport().setRefreshScheduled(true);
        }

        // деактивируем презентер
        // TODO: this.getRootPresenter().deactivate();
    }

    // удалять договор можно, если версия первая (она же последняя)
    public boolean isDeleteAllowed() {
        return this.getContext().isEditable() && (this.getContractVersion().equals(CtrContractVersionManager.instance().dao().getFirstVersion(this.getContract())));
    }

    public boolean isShowPrintTemplate()
    {
        return getContractVersion().getPrintTemplate() != null;
    }
}
