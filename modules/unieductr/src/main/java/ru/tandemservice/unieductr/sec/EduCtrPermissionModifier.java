// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unieductr.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.ClassGroupMeta;
import org.tandemframework.sec.meta.ModuleLocalGroupMeta;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.util.CtrContractVersionUtil;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;

import java.util.Map;

public class EduCtrPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(final Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        final SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("unieductr");
        config.setName("unieductr-sec-config");
        config.setTitle("");

        // права на объект «Договор со студентом»

        PermissionGroupMeta pg = CtrContractVersionUtil.registerPermissionGroup(config, EduCtrStudentContract.class, EduContractManager.instance());
        PermissionMetaUtil.createGroupRelation(config, pg.getName(), "eduCtrStudentContractCG");

        ModuleLocalGroupMeta moduleLocalGroupMeta = PermissionMetaUtil.createModuleLocalGroup(config, "unieductr", "Модуль «Договоры на обучение»");
        ClassGroupMeta lcg = PermissionMetaUtil.createClassGroup(moduleLocalGroupMeta, "eduCtrStudentContractLC", "Объект «Договор на обучение со студентом»", "ru.tandemservice.unieductr.student.entity.EduCtrStudentContract");
        PermissionMetaUtil.createGroupRelation(config, pg.getName(), lcg.getName());

        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            String code = description.getCode();

            PermissionGroupMeta permissionGroupStudentsTab = PermissionMetaUtil.createPermissionGroup(config, code + "StudentsPermissionGroup", "Вкладка «Студенты»");

            PermissionGroupMeta pgEcfPkgTab = PermissionMetaUtil.createPermissionGroup(permissionGroupStudentsTab, code + "EduCtrStudentContractTabPermissionGroup", "Вкладка «Договоры»");
            PermissionMetaUtil.createPermission(pgEcfPkgTab, "orgUnit_viewEduContractStudentsList_" + code, "Просмотр");

            // Вкладка «Отчеты»
            final PermissionGroupMeta permissionGroupReports = PermissionMetaUtil.createPermissionGroup(config, code + "ReportsTabPermissionGroup", "Вкладка «Отчеты»");
            final PermissionGroupMeta eduCtrReports = PermissionMetaUtil.createPermissionGroup(permissionGroupReports, code + "EduCtrReportsPG", "Отчеты модуля «Договоры на обучение»");
            PermissionMetaUtil.createPermission(eduCtrReports, "orgUnit_viewEduCtrDebitorsPayReport_" + code, "Просмотр и печать отчета «Список должников по оплате»");
            PermissionMetaUtil.createPermission(eduCtrReports, "orgUnit_addEduCtrDebitorsPayReport_" + code, "Добавление отчета «Список должников по оплате»");
            PermissionMetaUtil.createPermission(eduCtrReports, "orgUnit_deleteEduCtrDebitorsPayReport_" + code, "Удаление отчета «Список должников по оплате»");
            PermissionMetaUtil.createPermission(eduCtrReports, "orgUnit_viewEduCtrPaymentsReport_" + code, "Просмотр и печать отчета «Список платежей»");
            PermissionMetaUtil.createPermission(eduCtrReports, "orgUnit_addEduCtrPaymentsReport_" + code, "Добавление отчета «Список платежей»");
            PermissionMetaUtil.createPermission(eduCtrReports, "orgUnit_deleteEduCtrPaymentsReport_" + code, "Удаление отчета «Список платежей»");
            PermissionMetaUtil.createPermission(eduCtrReports, "orgUnit_viewEduCtrAgreementIncomeReport_" + code, "Просмотр и печать отчета «Помесячное поступление средств»");
            PermissionMetaUtil.createPermission(eduCtrReports, "orgUnit_addEduCtrAgreementIncomeReport_" + code, "Добавление отчета «Помесячное поступление средств»");
            PermissionMetaUtil.createPermission(eduCtrReports, "orgUnit_deleteEduCtrAgreementIncomeReport_" + code, "Удаление отчета «Помесячное поступление средств»");
            PermissionMetaUtil.createPermission(eduCtrReports, "orgUnit_viewEduCtrObligationsContractReport_" + code, "Просмотр и печать отчета «Обязательства по договорам студентов»");
            PermissionMetaUtil.createPermission(eduCtrReports, "orgUnit_addEduCtrObligationsContractReport_" + code, "Добавление отчета «Обязательства по договорам студентов»");
            PermissionMetaUtil.createPermission(eduCtrReports, "orgUnit_deleteEduCtrObligationsContractReport_" + code, "Удаление отчета «Обязательства по договорам студентов»");
        }
        securityConfigMetaMap.put(config.getName(), config);
    }

}
