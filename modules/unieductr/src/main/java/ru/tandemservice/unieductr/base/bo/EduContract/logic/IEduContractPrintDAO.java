package ru.tandemservice.unieductr.base.bo.EduContract.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContractVersionPrinter;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice;

/**
 * @author vdanilov
 */
public interface IEduContractPrintDAO extends INeedPersistenceSupport, ICtrContractVersionPrinter {

    /** @param promise печатает квитанцию по обязательтсву */
    IDocumentRenderer printPromiseQuittance(CtrPaymentPromice promise);

}
