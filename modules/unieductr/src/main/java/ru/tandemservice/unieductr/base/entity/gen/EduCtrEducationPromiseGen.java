package ru.tandemservice.unieductr.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractPromice;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Обязательство обучения по обр. программе
 *
 * Обязательство обучить человека (src - сторона ОУ, dst - обучаемый)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduCtrEducationPromiseGen extends CtrContractPromice
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise";
    public static final String ENTITY_NAME = "eduCtrEducationPromise";
    public static final int VERSION_HASH = 1360838269;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PROGRAM = "eduProgram";

    private EduProgramProf _eduProgram;     // Образовательная программа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Образовательная программа. Свойство не может быть null.
     */
    @NotNull
    public EduProgramProf getEduProgram()
    {
        return _eduProgram;
    }

    /**
     * @param eduProgram Образовательная программа. Свойство не может быть null.
     */
    public void setEduProgram(EduProgramProf eduProgram)
    {
        dirty(_eduProgram, eduProgram);
        _eduProgram = eduProgram;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduCtrEducationPromiseGen)
        {
            setEduProgram(((EduCtrEducationPromise)another).getEduProgram());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduCtrEducationPromiseGen> extends CtrContractPromice.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduCtrEducationPromise.class;
        }

        public T newInstance()
        {
            return (T) new EduCtrEducationPromise();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "eduProgram":
                    return obj.getEduProgram();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "eduProgram":
                    obj.setEduProgram((EduProgramProf) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eduProgram":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eduProgram":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "eduProgram":
                    return EduProgramProf.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduCtrEducationPromise> _dslPath = new Path<EduCtrEducationPromise>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduCtrEducationPromise");
    }
            

    /**
     * @return Образовательная программа. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise#getEduProgram()
     */
    public static EduProgramProf.Path<EduProgramProf> eduProgram()
    {
        return _dslPath.eduProgram();
    }

    public static class Path<E extends EduCtrEducationPromise> extends CtrContractPromice.Path<E>
    {
        private EduProgramProf.Path<EduProgramProf> _eduProgram;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Образовательная программа. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise#getEduProgram()
     */
        public EduProgramProf.Path<EduProgramProf> eduProgram()
        {
            if(_eduProgram == null )
                _eduProgram = new EduProgramProf.Path<EduProgramProf>(L_EDU_PROGRAM, this);
            return _eduProgram;
        }

        public Class getEntityClass()
        {
            return EduCtrEducationPromise.class;
        }

        public String getEntityName()
        {
            return "eduCtrEducationPromise";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
