package ru.tandemservice.unieductr.base.entity;

import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.unieductr.base.entity.gen.EduCtrAcademyPresenterGen;

/**
 * Представитель ОУ при заключении договора на обучение
 */
public class EduCtrAcademyPresenter extends EduCtrAcademyPresenterGen
{
    /**
     * @param orgUnit
     * @param employeePost
     * @return EduCtrAcademyPresenter (не в базе), EmployeePostContactor (не в базе)
     */
    public static EduCtrAcademyPresenter buildTransientPresenter(OrgUnit orgUnit, EmployeePost employeePost)
    {
        EmployeePostContactor contactor = new EmployeePostContactor();
        contactor.setEmployeePost(employeePost);

        EduCtrAcademyPresenter presenter = new EduCtrAcademyPresenter();
        presenter.setOrgUnit(orgUnit);
        presenter.setPresenter(contactor);
        presenter.setEmployeeSignatureFio(employeePost.getFio());

        return presenter;
    }
}