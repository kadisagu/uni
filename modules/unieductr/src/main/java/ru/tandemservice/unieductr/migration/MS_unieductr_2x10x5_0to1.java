package ru.tandemservice.unieductr.migration;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.ctr.base.util.CtrMigrationUtil;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unieductr_2x10x5_0to1 extends IndependentMigrationScript
{
    /** Константа кода (code) элемента : Договор на обучение (title) */
    private String DOGOVOR_NA_OBUCHENIE = "01.01";

    // edulevel
    /** Константа кода (code) элемента : Договор на обучение ВО (title) */
    private String DOGOVOR_NA_OBUCHENIE_V_O = "01.01.vpo";

    /** Константа кода(code) элемента : Высшее образование - бакалавриат(title) */
    private String VYSSHEE_OBRAZOVANIE_BAKALAVRIAT = "2013.2.2";
    /** Константа кода(code) элемента : Высшее образование - специалитет, магистратура(title) */
    private String VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA = "2013.2.3";
    /** Константа кода(code) элемента : Высшее образование - подготовка кадров высшей квалификации(title) */
    private String VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII = "2013.2.4";

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.5")
                };
    }


    @Override
    public void run(DBTool tool) throws Exception
    {

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //      Сущность CtrContractObject смена типа с 01.01 на 01.01.vpo

        //        ctr_contractobj_t - type_id -> ctrcontracttype_t
        //        eductr_stduent_c_ctmpldt_t ctr_version_template_data_t owner_id -> ctr_contractver_t contract_id
        //        eductr_pr_edu_t  ctr_promice_t src_id -> ctr_contractor_t owner_id
        //        eductr_pr_edu_t eduprogram_id -> edu_program_prof_t programqualification_id ->edu_c_pr_qual_t  subjectindex_id
        //                          -> edu_c_pr_subject_index_t programkind_id -> edu_c_program_kind_t edulevel_id -> c_edu_level_t
        //        ('2013.2.2', '2013.2.3', '2013.2.4')

        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        Map<String, Long> typesMap = MigrationUtils.getCatalogCode2IdMap(tool, "ctrcontracttype_t");

        SQLSelectQuery selectContractQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractobj_t", "co")
                .innerJoin(SQLFrom.table("ctrcontracttype_t", "ct"), "ct.id=co.type_id")
                .innerJoin(SQLFrom.table("ctr_contractver_t", "v"), "v.contract_id=co.id")
                .innerJoin(SQLFrom.table("ctr_version_template_data_t", "vt"), "vt.owner_id=v.id")
                .innerJoin(SQLFrom.table("eductr_stduent_c_ctmpldt_t", "st"), "st.id=vt.id")

                .innerJoin(SQLFrom.table("ctr_contractor_t", "ctr"), "ctr.owner_id=v.id")
                .innerJoin(SQLFrom.table("ctr_promice_t", "prom"), "prom.src_id=ctr.id")
                .innerJoin(SQLFrom.table("eductr_pr_edu_t", "edprom"), "edprom.id=prom.id")
                .innerJoin(SQLFrom.table("edu_program_prof_t", "pprof"), "pprof.id=edprom.eduprogram_id")
                .innerJoin(SQLFrom.table("edu_c_pr_qual_t", "qual"), "qual.id=pprof.programqualification_id")
                .innerJoin(SQLFrom.table("edu_c_pr_subject_index_t", "indext"), "indext.id=qual.subjectindex_id")
                .innerJoin(SQLFrom.table("edu_c_program_kind_t", "pkind"), "pkind.id=indext.programkind_id")
                .innerJoin(SQLFrom.table("c_edu_level_t", "elev"), "elev.id=pkind.edulevel_id")
        )
                .column("co.id")
                .where("elev.code_p in ('2013.2.2', '2013.2.3', '2013.2.4')")
                .where("ct.code_p=?");

        SQLUpdateQuery updateQuery = new SQLUpdateQuery("ctr_contractobj_t", "cobj")
                .where("cobj.id in ("+ translator.toSql(selectContractQuery) + ")")
                .set("type_id", "?");

        tool.executeUpdate(translator.toSql(updateQuery), typesMap.get(DOGOVOR_NA_OBUCHENIE_V_O), DOGOVOR_NA_OBUCHENIE);
    }
}
