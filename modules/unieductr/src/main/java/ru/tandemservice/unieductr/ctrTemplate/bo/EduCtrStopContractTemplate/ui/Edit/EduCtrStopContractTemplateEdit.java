/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStopContractTemplate.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author azhebko
 * @since 01.09.2014
 */
@Configuration
public class EduCtrStopContractTemplateEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder().create();
    }
}