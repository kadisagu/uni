/* $Id$ */
package ru.tandemservice.unieductr.base.bo.EduProgramContract;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;

import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractTypeCodes;

/**
 * @author nvankov
 * @since 3/4/14
 */
@Configuration
public class EduProgramContractManager extends BusinessObjectManager
{
    public static EduProgramContractManager instance() {
        return instance(EduProgramContractManager.class);
    }

    @Bean
    public ItemListExtPoint<String> eduProgramClassContractTypeExtPoint()
    {
        return this.itemList(String.class)
        .add(ru.tandemservice.uniedu.program.entity.EduProgram.class.getName(), CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE)
        .add(ru.tandemservice.uniedu.program.entity.EduProgramHigherProf.class.getName(), CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE_V_O)
        .add(ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf.class.getName(), CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE_S_P_O)
        .create();
    }

}



