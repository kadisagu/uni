/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.logic;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.functors.InstanceofPredicate;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractKindSelectWrapper;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrVersionTemplatePromiceRestrictions;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contract.*;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractKind;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.ctr.catalog.entity.CtrTemplateScriptItem;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractPrintUtils;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.ContractSides;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduCtrPaymentPromiceRestrictions;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.base.entity.EduProgramPrice;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractKindCodes;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractTypeCodes;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrPricePaymentGridCodes;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStopContractTemplate.ui.Add.EduCtrStopContractTemplateAdd;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.EduCtrTermEducationCostAgreementTemplateManager;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.ui.Add.EduCtrTermEducationCostAgreementTemplateAdd;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.ui.Add.EduCtrTermEducationCostAgreementTemplateAddUI;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;
import ru.tandemservice.unieductr.student.entity.EduCtrTermEducationCostAgreementTemplateData;

import java.util.*;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author azhebko
 * @since 04.12.2014
 */
public class EduCtrTermEducationCostAgreementTemplateDao extends UniBaseDao implements IEduCtrTermEducationCostAgreementTemplateDao
{
    @Override
    public DevelopGrid getStudentDevelopGrid(Student student)
    {
        checkNotNull(student);
        return null;
    }

    @Override
    public CtrPriceElementCostStage getPriceElementCostStage(CtrPriceElementCost priceElementCost, Integer termNumber)
    {
        checkNotNull(priceElementCost);
        checkNotNull(termNumber);
        checkArgument(priceElementCost.getPaymentGrid().getCode().equals(CtrPricePaymentGridCodes.EPP_SEMESTER));

        final String termAsStr = termNumber.toString();
        Collection<CtrPriceElementCostStage> costStages = this.getList(CtrPriceElementCostStage.class, CtrPriceElementCostStage.cost(), priceElementCost);
        costStages = CollectionUtils.select(costStages, costStage -> costStage.getStageUniqueCode().equals(termAsStr));

        return costStages.size() == 1 ? costStages.iterator().next() : null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void doCreateNewVersion(EduCtrTermEducationCostAgreementTemplateData template, CtrContractVersion prevVersion, Collection<CtrPaymentPromice> prevVersionPromises, Collection<CostStageWrapper> costStages, CtrContractVersionCreateData versionCreateData)
    {
        checkNotNull(template);
        checkNotNull(prevVersion);
        checkNotNull(prevVersionPromises);

        CtrContractVersion newVersion = CtrContractVersionManager.instance().dao().doCreateNewVersion(prevVersion, versionCreateData);

        Map<MultiKey, CtrPaymentPromice> promiseMap = new HashMap<>();
        for (CtrPaymentPromice paymentPromise: prevVersionPromises)
            promiseMap.put(paymentPromise.getPromiceLocalKey(), paymentPromise);

        for (CostStageWrapper costStageWrapper : costStages) {
            final CtrPriceElementCostStage costStage = costStageWrapper.getCostStage();
            checkArgument(costStage.getCost().getPeriod().getElement() instanceof EduProgramPrice);

            template.setCost(costStage.getCost());
            template.setCipher(((EduProgramPrice) costStage.getCost().getPeriod().getElement()).getCipher());

            CtrPaymentPromice newPaymentPromise = new CtrPaymentPromice();
            newPaymentPromise.setSourceStage(costStage);
            newPaymentPromise.setStage(costStage.getTitle());
            newPaymentPromise.setCurrency(costStage.getCost().getCurrency());
            newPaymentPromise.setCostAsLong(costStage.getStageCostAsLong());

            newPaymentPromise.setDeadlineDate(costStageWrapper.getDeadlineDate());
            newPaymentPromise.setComment(costStage.getComment());

            newPaymentPromise.setSrc(CtrContractVersionManager.instance().dao().getContactor(newVersion, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER));
            newPaymentPromise.setDst(CtrContractVersionManager.instance().dao().getContactor(newVersion, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER));

            if (promiseMap.put(newPaymentPromise.getPromiceLocalKey(), newPaymentPromise) != null)
                throw new ApplicationException(EduCtrTermEducationCostAgreementTemplateManager.instance().getProperty("actual-promises-only"));
        }

        CtrContractVersionManager.instance().dao().doUpdateContractPromice(newVersion, promiseMap.values(), new InstanceofPredicate(CtrPaymentPromice.class));

        template.setOwner(newVersion);
        this.save(template);
    }

    @Override
    public Map<CtrContractType, List<CtrContractKindSelectWrapper>> getVersionAddComponent(final Long contextEntityId)
    {
        CtrContractVersion currentVersion = null;
        IEntity context = this.getNotNull(contextEntityId);

        if (context instanceof EduCtrStudentContract)
            currentVersion = CtrContractVersionManager.instance().dao().getLastActiveVersion(((EduCtrStudentContract) context).getContractObject());

        else if (context instanceof CtrContractVersion)
            currentVersion = (CtrContractVersion) context;

        if (currentVersion == null || this.getCount(EduCtrEducationPromise.class, EduCtrEducationPromise.src().owner().s(), currentVersion) != 1 || this.getCount(EduCtrStudentContract.class, EduCtrStudentContract.contractObject().s(), currentVersion.getContract()) != 1)
            return Collections.emptyMap();

        Map<CtrContractType, List<CtrContractKindSelectWrapper>> map = Maps.newHashMap();

        if(CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE_V_O.equals(currentVersion.getContract().getType().getCode()))
        {
            CtrContractVersionContractor customerRel = CtrContractVersionManager.instance().dao().getContactor(currentVersion, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER);

            List<EduCtrEducationPromise> promiseList = this.getList(EduCtrEducationPromise.class, EduCtrEducationPromise.dst().owner(), currentVersion);

            EduCtrEducationPromise eduPromise = promiseList.get(0);
            ContactorPerson educationReceiver = eduPromise.getDst().getContactor();

            ContractSides contractSides = EduContractPrintUtils.getContractSides(customerRel.getContactor(), educationReceiver);
            CtrContractType contractType = this.getCatalogItem(CtrContractType.class, CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE_V_O);

            if(ContractSides.TWO_SIDES.equals(contractSides))
            {
                SafeMap.safeGet(map, contractType, ArrayList.class).add(new CtrContractKindSelectWrapper( this.getCatalogItem(CtrContractKind.class, CtrContractKindCodes.TERM_EDU_COST_2_SIDES), EduCtrTermEducationCostAgreementTemplateAdd.class));
            } else if(ContractSides.THREE_SIDES_PERSON.equals(contractSides))
            {
                SafeMap.safeGet(map, contractType, ArrayList.class).add(new CtrContractKindSelectWrapper( this.getCatalogItem(CtrContractKind.class, CtrContractKindCodes.TERM_EDU_COST_3_SIDES_PERSON), EduCtrTermEducationCostAgreementTemplateAdd.class));
            } else if(ContractSides.THREE_SIDES_ORG.equals(contractSides))
            {
                SafeMap.safeGet(map, contractType, ArrayList.class).add(new CtrContractKindSelectWrapper( this.getCatalogItem(CtrContractKind.class, CtrContractKindCodes.TERM_EDU_COST_3_SIDES_ORG), EduCtrTermEducationCostAgreementTemplateAdd.class));
            }
        }

        return map;
    }

    @Override
    public ICtrVersionTemplatePromiceRestrictions getPromiceRestrictions(Class<? extends CtrContractPromice> promiceClass, CtrContractVersionTemplateData templateData)
    {
        // проверяем, что это тот самый шаблон
        if (!(templateData instanceof EduCtrTermEducationCostAgreementTemplateData))
            throw new IllegalStateException();

        // для обязательств по оплате отделные разрешения
        if (CtrPaymentPromice.class.equals(promiceClass))
            return EduCtrPaymentPromiceRestrictions.get(templateData.getOwner());

        // запрещаем редактировать все, кроме обязательств по оплате
        return ICtrVersionTemplatePromiceRestrictions.DENY_ALL;
    }

    @Override
    public IDocumentRenderer print(CtrContractVersionTemplateData templateData)
    {
        if (!(templateData instanceof EduCtrTermEducationCostAgreementTemplateData))
            throw new IllegalStateException();

        CtrContractVersion contractVersion = templateData.getOwner();

        // проверяем, что можем печатать по шаблону

        CtrContractVersionContractor providerRel = CtrContractVersionManager.instance().dao().getContactor(contractVersion, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER);
        if (null == providerRel)
            throw new ApplicationException("Печать по шаблону невозможна - для версии договора не указан исполнитель.");

        CtrContractVersionContractor customerRel = CtrContractVersionManager.instance().dao().getContactor(contractVersion, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER);
        if (null == customerRel)
            throw new ApplicationException("Печать по шаблону невозможна - для версии договора не указан заказчик.");

        if (!(providerRel.getContactor() instanceof EmployeePostContactor))
            throw new ApplicationException("Печать по шаблону невозможна - неизвестный тип исполнителя.");

        List<EduCtrEducationPromise> promiseList = this.getList(EduCtrEducationPromise.class, EduCtrEducationPromise.dst().owner(), contractVersion);
        if (promiseList.size() > 1)
            throw new ApplicationException("Печать по шаблону невозможна - в версии договора более одного обязательства по обучению по ОП.");

        if (promiseList.size() < 1)
            throw new ApplicationException("Печать по шаблону невозможна - в версии договора отсутствуют обязательства по обучению по ОП.");

        EduCtrEducationPromise eduPromise = promiseList.get(0);

        // печатаем по шаблону, используя скрипт
        Map<String, Object> result = CommonManager.instance().scriptDao().getScriptResult(
            contractVersion.getPrintTemplate(),
            "versionTemplateDataId", templateData.getId(),
            "customerId", customerRel.getId(),
            "providerId", providerRel.getId(),
            "eduPromiseId", eduPromise.getId());

        return (IDocumentRenderer)result.get(IScriptExecutor.RENDERER);
    }

    @Override public boolean isAllowCreateNextVersionByTemplate(CtrContractVersionTemplateData ctrContractVersionTemplateData) { return true; }

    @Override public boolean isAllowCreateNextVersionByCopy(CtrContractVersionTemplateData ctrContractVersionTemplateData) { return false; }

    @Override public boolean isAllowDeleteTemplate(CtrContractVersionTemplateData ctrContractVersionTemplateData) { return false; }
}