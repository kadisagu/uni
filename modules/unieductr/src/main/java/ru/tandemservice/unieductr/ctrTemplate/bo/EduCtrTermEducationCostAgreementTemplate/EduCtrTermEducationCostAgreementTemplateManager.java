/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.logic.EduCtrTermEducationCostAgreementTemplateDao;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.logic.IEduCtrTermEducationCostAgreementTemplateDao;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.ui.Edit.EduCtrTermEducationCostAgreementTemplateEdit;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.ui.Pub.EduCtrTermEducationCostAgreementTemplatePub;
import ru.tandemservice.unieductr.student.entity.EduCtrTermEducationCostAgreementTemplateData;

/**
 * @author azhebko
 * @since 04.12.2014
 */
@Configuration
public class EduCtrTermEducationCostAgreementTemplateManager extends BusinessObjectManager implements ICtrContractTemplateManager
{
    public static EduCtrTermEducationCostAgreementTemplateManager instance()
    {
        return instance(EduCtrTermEducationCostAgreementTemplateManager.class);
    }

    @Override
    public Class<? extends CtrContractVersionTemplateData> getDataEntityClass()
    {
        return EduCtrTermEducationCostAgreementTemplateData.class;
    }

    @Override
    public Class<? extends BusinessComponentManager> getDataDisplayComponent()
    {
        return EduCtrTermEducationCostAgreementTemplatePub.class;
    }

    @Override
    public Class<? extends BusinessComponentManager> getDataEditComponent()
    {
        return EduCtrTermEducationCostAgreementTemplateEdit.class;
    }

    @Override @Bean public IEduCtrTermEducationCostAgreementTemplateDao dao() { return new EduCtrTermEducationCostAgreementTemplateDao(); }

    @Override
    public boolean isAllowEditInWizard()
    {
        return false;
    }
}