package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.logic;

import java.util.Date;
import java.util.List;

import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractKind;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.ctr.catalog.entity.CtrPrintTemplate;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;

/**
 * @author vdanilov
 */
public interface IEduCtrStudentContractTemplateAddData {

    Student getStudent();
    EduProgramProf getEduProgram();
    EducationYear getContractEducationYear();

    OrgUnit getFormativeOrgUnit();
    CtrContractType getContractType();
    CtrContractVersionCreateData getVersionCreateData();
    ContactorPerson getCustomer();
    ContactorPerson getProvider();
    Date getStartDate();
    Date getPriceDate();
    Date getEndDate();
    Date getEnrollmentDate();
    String getCipher();

    CtrPriceElementCost getSelectedCost();
    List<CtrPriceElementCostStage> getSelectedCostStages();

}
