package ru.tandemservice.unieductr.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unieductr_2x10x1_0to1 extends IndependentMigrationScript
{

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData
        tool.dropView("edctncntrctvrsntmpltdt_v");

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eduCtrOrgUnitContractTemplateData

        // сущность была удалена
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("eductr_ou_ctmpldt_c_t", "eductr_ou_ctmpldt_t", "ctr_version_template_data_t");

            // удалить таблицу
            tool.dropTable("eductr_ou_ctmpldt_c_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("eduCtrOrgUnitContractTemplateData");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eduCtrOrgUnitAgreementTemplateData

        // сущность была удалена
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("eductr_ou_ctmpldt_v_t", "eductr_ou_ctmpldt_t", "ctr_version_template_data_t");

            // удалить таблицу
            tool.dropTable("eductr_ou_ctmpldt_v_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("eduCtrOrgUnitAgreementTemplateData");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eduCtrOrgUnitVersionTemplateData

        // сущность была удалена
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("eductr_ou_ctmpldt_t", "ctr_version_template_data_t");

            // удалить таблицу
            tool.dropTable("eductr_ou_ctmpldt_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("eduCtrOrgUnitVersionTemplateData");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eduCtrOrgUnitPromise

        // сущность была удалена
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("eductr_ou_promise_t", "ctr_promice_t");

            // удалить таблицу
            tool.dropTable("eductr_ou_promise_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("eduCtrOrgUnitPromise");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eduCtrOrgUnit

        // сущность была удалена
        {
            tool.dropView("ictrpriceelement_v");

            // удалить таблицу
            tool.dropTable("eductr_ou_config", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("eduCtrOrgUnit");

        }


    }
}