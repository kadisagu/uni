package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentAddContractTemplate.logic;

import com.beust.jcommander.internal.Maps;
import com.google.common.collect.Lists;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractKindSelectWrapper;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrVersionTemplatePromiceRestrictions;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.logic.CtrPriceDao;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contract.*;
import org.tandemframework.shared.ctr.base.entity.contract.gen.CtrContractVersionContractorRoleGen;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractKind;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractRole;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgramAdditional;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduCtrPaymentPromiceRestrictions;
import ru.tandemservice.unieductr.base.bo.EduProgramContract.logic.EduProgramSingleStudentContractObjectAdditionalFactory;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationAddPromise;
import ru.tandemservice.unieductr.base.entity.EduProgramPrice;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractKindCodes;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractTypeCodes;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentAddContractTemplate.ui.Add.EduCtrStudentAddContractTemplateAdd;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentAddContractTemplateData;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EduCtrStudentAddContractTemplateDAO extends UniBaseDao implements IEduCtrStudentAddContractTemplateDAO
{

    // базовые (статические) методы для формирования и заполнения договора (если хочется, можно переопределить фабрику польностью в buildContractFactory)
    protected static abstract class ContractFactory<T extends IEduCtrStudentAddContractTemplateAddData> extends EduProgramSingleStudentContractObjectAdditionalFactory<Student>
    {
        protected abstract T getUi();

        @Override protected Student getStudent() { return getUi().getStudent(); }
        @Override protected OrgUnit getCtrContractObjectOrgUnit() { return getUi().getFormativeOrgUnit(); }
        @Override protected CtrContractType getCtrContractObjectType() { return getUi().getContractType(); }
        @Override protected ContactorPerson getCustomer() { return getUi().getCustomer(); }

        @Override protected ContactorPerson getStudentContactPersion(Student student) {
            return getContactPersion(student.getPerson());
        }
        @Override protected Collection<ContactorPerson> getAcademyPresenters(final CtrContractObject contractObject) {
            return Collections.singleton(getUi().getProvider());
        }
        @Override protected String getStudentTitle(Student student) {
            return student.getFullTitle();
        }

        @Override protected EduProgramAdditional getStudentEduProgram(final Student student) { return getUi().getEduProgram(); }
        @Override protected Date getStudentEnrollmentDate(final Student student) { return getUi().getEnrollmentDate(); }

        @Override protected Collection<CtrPriceElementCost> getEducationPromiseCostList(final Student entrant, final EduCtrEducationAddPromise eduPromise, final CtrContractVersionContractor paymentSource, final Currency defaultCurrency) {
            final CtrPriceElementCost selectedCost = getUi().getSelectedCost();
            return null == selectedCost ? Collections.<CtrPriceElementCost>emptyList() : Collections.singleton(selectedCost);
        }

        @Override protected List<CtrPriceElementCostStage> getEducationPromiseCostListStages(final Student student, final EduCtrEducationAddPromise eduPromise, final CtrPriceElementCost cost) {
            final List<CtrPriceElementCostStage> stageList = getUi().getSelectedCostStages();
            return null == stageList ? Collections.<CtrPriceElementCostStage>emptyList() : stageList;
        }
    }

    // метод для переопределения в проектном слое генерации номеров договоров
    protected String getContractObjectNumber(final IEduCtrStudentAddContractTemplateAddData ui) {
        final EducationYear educationYear = ui.getEduProgram().getYear();
        return EduContractManager.instance().dao().doGetNextNumber(String.valueOf(educationYear.getIntValue()));
    }

    // создает фабрику договоров (можно переопределять, если хочется)
    protected EduProgramSingleStudentContractObjectAdditionalFactory<Student> buildContractFactory(final IEduCtrStudentAddContractTemplateAddData ui) {
        return new ContractFactory<IEduCtrStudentAddContractTemplateAddData>() {
            @Override protected IEduCtrStudentAddContractTemplateAddData getUi() { return ui; }

            @Override
            protected CtrContractVersionCreateData getVersionCreateData()
            {
                return getUi().getVersionCreateData();
            }

            @Override protected String getCtrContractObjectNumber() { return getContractObjectNumber(getUi()); }
        };
    }


    @Override
    public EduCtrStudentAddContractTemplateData doCreateVersion(IEduCtrStudentAddContractTemplateAddData templateAddData)
    {
        final Student student = templateAddData.getStudent();

        // создаем версию
        final CtrContractVersion version = buildContractFactory(templateAddData).buildContractObject();

        // связываем версию со студентом
        final EduCtrStudentContract studentContract = new EduCtrStudentContract();
        studentContract.setContractObject(version.getContract());
        studentContract.setStudent(student);
        this.save(studentContract);

        // создаем шаблон (договора)
        final EduCtrStudentAddContractTemplateData templateData = new EduCtrStudentAddContractTemplateData();
        templateData.setCipher(templateAddData.getCipher());
        templateData.setOwner(version);
        templateData.setEducationYear(templateAddData.getContractEducationYear());
        templateData.setCost(templateAddData.getSelectedCost());
        this.save(templateData);

        // врзвращаем результат
        return templateData;
    }


    @Override
    public void doUpdateContractVersionContactor(CtrContractVersion version, ContactorPerson newProvider, ContactorPerson newCustomer)
    {
        CtrContractRole providerCatalogRole = IUniBaseDao.instance.get().getCatalogItem(CtrContractRole.class, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER);
        CtrContractRole customerCatalogRole = IUniBaseDao.instance.get().getCatalogItem(CtrContractRole.class, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER);

        List<CtrContractVersionContractor> contractors = CtrContractVersionManager.instance().dao().getContractors(version);
        final Map<ContactorPerson, CtrContractVersionContractor> contractorMap = Maps.newHashMap();
        contractors.forEach(contractor -> {
            contractorMap.put(contractor.getContactor(), contractor);
        });

        CtrContractVersionContractor currentProvider = CtrContractVersionManager.instance().dao().getContactor(version, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER);
        CtrContractVersionContractor currentCustomer = CtrContractVersionManager.instance().dao().getContactor(version, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER);
        CtrContractVersionContractorRole currentProviderRole = getByNaturalId(new CtrContractVersionContractorRoleGen.NaturalId(currentProvider, providerCatalogRole));
        CtrContractVersionContractorRole currentCustomerRole = getByNaturalId(new CtrContractVersionContractorRoleGen.NaturalId(currentCustomer, customerCatalogRole));

        // Исполнитель
        if(newProvider != null)
        {
            if (currentProvider != null && !Objects.equals(currentProvider.getContactor(), newProvider))
            {
                CtrContractVersionContractor newProv = contractorMap.get(newProvider);
                if (newProv == null)
                {
                    newProv = new CtrContractVersionContractor(version, newProvider);
                    save(newProv);
                }

                updatePromices(CtrContractPromice.class, currentProvider, newProv);

                currentProvider = newProv;
                currentProviderRole = new CtrContractVersionContractorRole(currentProvider, IUniBaseDao.instance.get().getCatalogItem(CtrContractRole.class, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER));
            }
            else if (currentProvider == null)
            {
                currentProvider = new CtrContractVersionContractor(version, newProvider);
                currentProviderRole = new CtrContractVersionContractorRole(currentProvider, IUniBaseDao.instance.get().getCatalogItem(CtrContractRole.class, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER));
            }
        }

        // Заказчик
        if(newCustomer != null)
        {

            if (currentCustomer != null && !Objects.equals(currentCustomer.getContactor(), newCustomer))
            {
                CtrContractVersionContractor newCust = contractorMap.get(newCustomer);
                if (newCust == null)
                {
                    newCust = new CtrContractVersionContractor(version, newCustomer);
                    save(newCust);
                }

                updatePromices(CtrPaymentPromice.class, currentCustomer, newCust);

                currentCustomer = newCust;
                currentCustomerRole = new CtrContractVersionContractorRole(currentCustomer, IUniBaseDao.instance.get().getCatalogItem(CtrContractRole.class, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER));
            }
            else if (currentCustomer == null)
            {
                currentCustomer = new CtrContractVersionContractor(version, newCustomer);
                currentCustomerRole = new CtrContractVersionContractorRole(currentCustomer, IUniBaseDao.instance.get().getCatalogItem(CtrContractRole.class, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER));
            }

        }

        CtrContractVersionManager.instance().dao().doSaveOrUpdateContractVersion(version, Lists.newArrayList(currentProvider, currentCustomer), Lists.newArrayList(currentProviderRole, currentCustomerRole));
    }

    private void updatePromices(Class<? extends CtrContractPromice> ctrContractPromiceClass, final CtrContractVersionContractor currentContractor, CtrContractVersionContractor newContractor)
    {
        List<CtrContractPromice> providerPromices = new DQLSelectBuilder().fromEntity(ctrContractPromiceClass, "cp")
                .where(exists(new DQLSelectBuilder().fromEntity(CtrContractVersionContractor.class, "c")
                        .where(or(
                                eq(property("cp", CtrContractPromice.src()), property("c")),
                                eq(property("cp", CtrContractPromice.dst()), property("c"))
                        ))
                        .where(eq(property("c"), value(currentContractor)))
                        .buildQuery()))
                .createStatement(getSession()).list();

        providerPromices.forEach(promice -> {
            if(Objects.equals(promice.getSrc(), currentContractor))
            {
                promice.setSrc(newContractor);
            }
            else if(Objects.equals(promice.getDst(), currentContractor))
            {
                promice.setDst(newContractor);
            }
            update(promice);
        });
    }

    @Override
    public Map<CtrContractType, List<CtrContractKindSelectWrapper>> getVersionAddComponent(final Long contextEntityId) {
        final Map<CtrContractType, List<CtrContractKindSelectWrapper>> map = new HashMap<>();

        final Class entityClass = EntityRuntime.getMeta(contextEntityId).getEntityClass();
        if (Student.class.equals(entityClass)) {
            final Student student = this.get(Student.class, contextEntityId);

            final EducationLevels educationLevel = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
            boolean hasAdd = (educationLevel.getLevelType().isAdditional());

            final EduProgramSubject eduProgramSubject = educationLevel.getEduProgramSubject();
            if (null != eduProgramSubject) {
                hasAdd |= eduProgramSubject.getSubjectIndex().getProgramKind().isProgramAdditionalProf();
            }

            if (hasAdd)
            {
                CtrContractType contractType = getCatalogItem(CtrContractType.class, CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE_D_P_O);
                SafeMap.safeGet(map, contractType, ArrayList.class).add(new CtrContractKindSelectWrapper(getCatalogItem(CtrContractKind.class, CtrContractKindCodes.EDU_CONTRACT_DPO_2_SIDES), EduCtrStudentAddContractTemplateAdd.class));
                SafeMap.safeGet(map, contractType, ArrayList.class).add(new CtrContractKindSelectWrapper(getCatalogItem(CtrContractKind.class, CtrContractKindCodes.EDU_CONTRACT_DPO_3_SIDES_PERSON), EduCtrStudentAddContractTemplateAdd.class));
                SafeMap.safeGet(map, contractType, ArrayList.class).add(new CtrContractKindSelectWrapper(getCatalogItem(CtrContractKind.class, CtrContractKindCodes.EDU_CONTRACT_DPO_3_SIDES_ORG), EduCtrStudentAddContractTemplateAdd.class));
            }
        }

        return map;
    }

    @Override
    public ICtrVersionTemplatePromiceRestrictions getPromiceRestrictions(final Class<? extends CtrContractPromice> promiceClass, final CtrContractVersionTemplateData templateData)
    {
        // проверяем, что это тот самый шаблон
        if (!(templateData instanceof EduCtrStudentAddContractTemplateData)) {
            throw new IllegalStateException();
        }

        // для обязательств по оплате отделные разрешения
        if (CtrPaymentPromice.class.equals(promiceClass)) {
            return EduCtrPaymentPromiceRestrictions.get(templateData.getOwner());
        }

        // запрещаем редактировать все, кроме обязательств по оплате
        return ICtrVersionTemplatePromiceRestrictions.DENY_ALL;
    }

    @Override
    public void doSaveTemplate(final EduCtrStudentAddContractTemplateData templateData) {
        this.saveOrUpdate(templateData.getOwner());
        this.saveOrUpdate(templateData);
    }

    @Override
    public IDocumentRenderer print(final CtrContractVersionTemplateData templateData) {
        // чужие версии не печатаем
        if (!(templateData instanceof EduCtrStudentAddContractTemplateData)) {
            throw new IllegalStateException();
        }

        final EduCtrStudentAddContractTemplateData versionTemplateData = (EduCtrStudentAddContractTemplateData) templateData;
        final CtrContractVersion contractVersion = templateData.getOwner();

        // проверяем, что можем печатать по шаблону

        final CtrContractVersionContractor providerRel = CtrContractVersionManager.instance().dao().getContactor(contractVersion, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER);
        if (null == providerRel) {
            throw new ApplicationException("Печать по шаблону невозможна - для версии договора не указан исполнитель.");
        }

        final CtrContractVersionContractor customerRel = CtrContractVersionManager.instance().dao().getContactor(contractVersion, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER);
        if (null == customerRel) {
            throw new ApplicationException("Печать по шаблону невозможна - для версии договора не указан заказчик.");
        }

        if (!(providerRel.getContactor() instanceof EmployeePostContactor)) {
            throw new ApplicationException("Печать по шаблону невозможна - неизвестный тип исполнителя.");
        }

        final List<EduCtrEducationAddPromise> promiseList = this.getList(EduCtrEducationAddPromise.class, EduCtrEducationAddPromise.dst().owner(), contractVersion);
        if (promiseList.size() > 1) {
            throw new ApplicationException("Печать по шаблону невозможна - в версии договора более одного обязательства по обучению по ОП.");
        }
        if (promiseList.size() < 1) {
            throw new ApplicationException("Печать по шаблону невозможна - в версии договора отсутствуют обязательства по обучению по ОП.");
        }

        final EduCtrEducationAddPromise eduPromise = promiseList.get(0);

        // печатаем по шаблону, используя скрипт
        final Map<String, Object> result = CommonManager.instance().scriptDao().getScriptResult(
            contractVersion.getPrintTemplate(),
            "versionTemplateDataId", versionTemplateData.getId(),
            "customerId", customerRel.getId(),
            "providerId", providerRel.getId(),
            "eduPromiseId", eduPromise.getId()
        );
        return (IDocumentRenderer)result.get(IScriptExecutor.RENDERER);
    }

    @Override
    public boolean isAllowCreateNextVersionByTemplate(final CtrContractVersionTemplateData ctrContractVersionTemplateData) {
        return true;
    }

    @Override
    public boolean isAllowCreateNextVersionByCopy(final CtrContractVersionTemplateData ctrContractVersionTemplateData) {
        return false;
    }

    @Override
    public boolean isAllowDeleteTemplate(final CtrContractVersionTemplateData ctrContractVersionTemplateData) {
        return false;
    }

    /**
     * @param docStartDate дата действия цен
     * @param contractVersion договор (версия), в рамках которого заключены обязательства
     * @return билдер списка цен, подходящих под заданную дату, на которые есть обязательства обучения по ОП в рамках указанного договора
     */
    public static DQLSelectBuilder createEduProgramPromisePriceElementCostBuilder(Date docStartDate, CtrContractVersion contractVersion)
    {
        return CtrPriceDao.createPriceElementCostBuilder("price", docStartDate, null)
            .where(exists(
                new DQLSelectBuilder()
                    .fromEntity(EduCtrEducationAddPromise.class, "p")
                    .where(exists(
                        new DQLSelectBuilder()
                            .fromEntity(EduProgramPrice.class, "pp")
                            .where(eq(property("pp", EduProgramPrice.program()), property("p", EduCtrEducationAddPromise.eduProgram())))
                            .where(eq(property("pp"), property("price", CtrPriceElementCost.period().element())))
                            .buildQuery()))
                    .where(eq(property("p", EduCtrEducationAddPromise.src().owner()), value(contractVersion)))
                    .buildQuery()));
    }
}
