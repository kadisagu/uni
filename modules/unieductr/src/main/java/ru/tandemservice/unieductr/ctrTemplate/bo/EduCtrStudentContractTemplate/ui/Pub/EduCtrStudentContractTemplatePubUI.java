package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;

import ru.tandemservice.unieductr.student.entity.EduCtrStudentContractTemplateData;

@Input({
    @Bind(key= UIPresenter.PUBLISHER_ID, binding="dataHolder.id", required=true)
})
public class EduCtrStudentContractTemplatePubUI extends UIPresenter
{
    private final EntityHolder<EduCtrStudentContractTemplateData> dataHolder = new EntityHolder<>();
    public EntityHolder<EduCtrStudentContractTemplateData> getDataHolder() { return this.dataHolder; }
    public EduCtrStudentContractTemplateData getTemplateData() { return getDataHolder().getValue(); }

    public CtrContractVersion getContractVersion() { return getTemplateData().getOwner(); }
    public CtrContractObject getContract() { return getContractVersion().getContract(); }

    @Override
    public void onComponentRefresh() {
        getDataHolder().refresh(EduCtrStudentContractTemplateData.class);
    }
}
