package ru.tandemservice.unieductr.base.bo.EduCtrEducationPromise.ui.SectionPromise;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;

/** враппер формы выбора ОП, работает только с программами EduProgramProf */
@SuppressWarnings("serial")
public class EduCtrEducationPromiseWrapper extends IdentifiableWrapper<EduCtrEducationPromise> {


    private final SelectModel<EduProgramSubjectIndex> subjectIndexModel = new SelectModel<>(DataAccessServices.dao().getList(EduProgramSubjectIndex.class, EduProgramSubjectIndex.P_CODE));
    public SelectModel<EduProgramSubjectIndex> getSubjectIndexModel() { return this.subjectIndexModel; }

    private final SelectModel<EduProgramSubject> subjectModel = new SelectModel<>(new DQLFullCheckSelectModel() {
        @Override protected DQLSelectBuilder query(final String alias, final String filter) {
            final EduProgramSubjectIndex subjectIndex = EduCtrEducationPromiseWrapper.this.getSubjectIndexModel().getValue();
            if (null == subjectIndex) { return null; }

            return new DQLSelectBuilder()
            .fromEntity(EduProgramSubject.class, alias)
            .where(eq(property(EduProgramSubject.subjectIndex().fromAlias(alias)), value(subjectIndex)))
            .where(this.like(EduProgramSubject.title().fromAlias(alias), filter))
            .order(property(EduProgramSubject.code().fromAlias(alias)));
        }
    });
    public SelectModel<EduProgramSubject> getSubjectModel() { return this.subjectModel; }

    private final SelectModel<EducationYear> eduProgramYearModel = new SelectModel<>(new DQLFullCheckSelectModel() {
        @Override protected DQLSelectBuilder query(final String alias, final String filter) {
            final EduProgramSubject subject = EduCtrEducationPromiseWrapper.this.getSubjectModel().getValue();
            if (null == subject) { return null; }

            return new DQLSelectBuilder()
            .fromEntity(EducationYear.class, alias)
            .where(in(
                property(alias, "id"),
                new DQLSelectBuilder()
                .fromEntity(EduProgramProf.class, "p")
                .column(property(EduProgramProf.year().id().fromAlias("p")))
                .where(eq(property(EduProgramProf.programSubject().fromAlias("p")), value(subject)))
                .buildQuery()
            ))
            .order(property(EducationYear.intValue().fromAlias(alias)));
        }
    });
    public SelectModel<EducationYear> getEduProgramYearModel() { return this.eduProgramYearModel; }

    private final ISelectModel eduProgramSelectModel = new DQLFullCheckSelectModel() {
        @Override protected DQLSelectBuilder query(final String alias, final String filter) {

            final EduProgramSubject subject = EduCtrEducationPromiseWrapper.this.getSubjectModel().getValue();
            if (null == subject) { return null; }

            final EducationYear year = EduCtrEducationPromiseWrapper.this.getEduProgramYearModel().getValue();
            if (null == year) { return null; }

            return new DQLSelectBuilder()
            .fromEntity(EduProgramProf.class, alias)
            .where(eq(property(EduProgramProf.programSubject().fromAlias(alias)), value(subject)))
            .where(eq(property(EduProgramProf.year().fromAlias(alias)), value(year)))
            .where(this.like(EduProgramProf.title().fromAlias(alias), filter))
            .order(property(EduProgramProf.title().fromAlias(alias)));
        }
    };
    public ISelectModel getEduProgramSelectModel() { return this.eduProgramSelectModel; }


    public EduCtrEducationPromiseWrapper(final EduCtrEducationPromise e) {
        super(e);

        if (null != e.getEduProgram()) {
            final EduProgramProf eduProgramProf = e.getEduProgram();
            this.getSubjectIndexModel().setValue(eduProgramProf.getProgramSubject().getSubjectIndex());
            this.getSubjectModel().setValue(eduProgramProf.getProgramSubject());
            this.getEduProgramYearModel().setValue(eduProgramProf.getYear());
        }
    }
}