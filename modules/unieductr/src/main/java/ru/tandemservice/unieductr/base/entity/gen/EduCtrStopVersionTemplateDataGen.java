package ru.tandemservice.unieductr.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;
import ru.tandemservice.unieductr.base.entity.EduCtrStopVersionTemplateData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные шаблона версии о расторжении договора на обучение
 *
 * Расторжение договора на обучение.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduCtrStopVersionTemplateDataGen extends CtrContractVersionTemplateData
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unieductr.base.entity.EduCtrStopVersionTemplateData";
    public static final String ENTITY_NAME = "eduCtrStopVersionTemplateData";
    public static final int VERSION_HASH = -295352006;
    private static IEntityMeta ENTITY_META;

    public static final String P_STOP_REASON = "stopReason";
    public static final String P_COMMENT = "comment";
    public static final String L_REFUND_CURRENCY = "refundCurrency";
    public static final String P_REFUND_AS_LONG = "refundAsLong";

    private String _stopReason;     // Причина расторжения договора
    private String _comment;     // Комментарий
    private Currency _refundCurrency;     // Валюта
    private long _refundAsLong;     // Сумма возврата

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Причина расторжения договора.
     */
    public String getStopReason()
    {
        return _stopReason;
    }

    /**
     * @param stopReason Причина расторжения договора.
     */
    public void setStopReason(String stopReason)
    {
        dirty(_stopReason, stopReason);
        _stopReason = stopReason;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Валюта. Свойство не может быть null.
     */
    @NotNull
    public Currency getRefundCurrency()
    {
        return _refundCurrency;
    }

    /**
     * @param refundCurrency Валюта. Свойство не может быть null.
     */
    public void setRefundCurrency(Currency refundCurrency)
    {
        dirty(_refundCurrency, refundCurrency);
        _refundCurrency = refundCurrency;
    }

    /**
     * Хранится в формате валюты (4 знака после запятой).
     *
     * @return Сумма возврата. Свойство не может быть null.
     */
    @NotNull
    public long getRefundAsLong()
    {
        return _refundAsLong;
    }

    /**
     * @param refundAsLong Сумма возврата. Свойство не может быть null.
     */
    public void setRefundAsLong(long refundAsLong)
    {
        dirty(_refundAsLong, refundAsLong);
        _refundAsLong = refundAsLong;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduCtrStopVersionTemplateDataGen)
        {
            setStopReason(((EduCtrStopVersionTemplateData)another).getStopReason());
            setComment(((EduCtrStopVersionTemplateData)another).getComment());
            setRefundCurrency(((EduCtrStopVersionTemplateData)another).getRefundCurrency());
            setRefundAsLong(((EduCtrStopVersionTemplateData)another).getRefundAsLong());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduCtrStopVersionTemplateDataGen> extends CtrContractVersionTemplateData.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduCtrStopVersionTemplateData.class;
        }

        public T newInstance()
        {
            return (T) new EduCtrStopVersionTemplateData();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "stopReason":
                    return obj.getStopReason();
                case "comment":
                    return obj.getComment();
                case "refundCurrency":
                    return obj.getRefundCurrency();
                case "refundAsLong":
                    return obj.getRefundAsLong();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "stopReason":
                    obj.setStopReason((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "refundCurrency":
                    obj.setRefundCurrency((Currency) value);
                    return;
                case "refundAsLong":
                    obj.setRefundAsLong((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "stopReason":
                        return true;
                case "comment":
                        return true;
                case "refundCurrency":
                        return true;
                case "refundAsLong":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "stopReason":
                    return true;
                case "comment":
                    return true;
                case "refundCurrency":
                    return true;
                case "refundAsLong":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "stopReason":
                    return String.class;
                case "comment":
                    return String.class;
                case "refundCurrency":
                    return Currency.class;
                case "refundAsLong":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduCtrStopVersionTemplateData> _dslPath = new Path<EduCtrStopVersionTemplateData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduCtrStopVersionTemplateData");
    }
            

    /**
     * @return Причина расторжения договора.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrStopVersionTemplateData#getStopReason()
     */
    public static PropertyPath<String> stopReason()
    {
        return _dslPath.stopReason();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrStopVersionTemplateData#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Валюта. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrStopVersionTemplateData#getRefundCurrency()
     */
    public static Currency.Path<Currency> refundCurrency()
    {
        return _dslPath.refundCurrency();
    }

    /**
     * Хранится в формате валюты (4 знака после запятой).
     *
     * @return Сумма возврата. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrStopVersionTemplateData#getRefundAsLong()
     */
    public static PropertyPath<Long> refundAsLong()
    {
        return _dslPath.refundAsLong();
    }

    public static class Path<E extends EduCtrStopVersionTemplateData> extends CtrContractVersionTemplateData.Path<E>
    {
        private PropertyPath<String> _stopReason;
        private PropertyPath<String> _comment;
        private Currency.Path<Currency> _refundCurrency;
        private PropertyPath<Long> _refundAsLong;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Причина расторжения договора.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrStopVersionTemplateData#getStopReason()
     */
        public PropertyPath<String> stopReason()
        {
            if(_stopReason == null )
                _stopReason = new PropertyPath<String>(EduCtrStopVersionTemplateDataGen.P_STOP_REASON, this);
            return _stopReason;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrStopVersionTemplateData#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EduCtrStopVersionTemplateDataGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Валюта. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrStopVersionTemplateData#getRefundCurrency()
     */
        public Currency.Path<Currency> refundCurrency()
        {
            if(_refundCurrency == null )
                _refundCurrency = new Currency.Path<Currency>(L_REFUND_CURRENCY, this);
            return _refundCurrency;
        }

    /**
     * Хранится в формате валюты (4 знака после запятой).
     *
     * @return Сумма возврата. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrStopVersionTemplateData#getRefundAsLong()
     */
        public PropertyPath<Long> refundAsLong()
        {
            if(_refundAsLong == null )
                _refundAsLong = new PropertyPath<Long>(EduCtrStopVersionTemplateDataGen.P_REFUND_AS_LONG, this);
            return _refundAsLong;
        }

        public Class getEntityClass()
        {
            return EduCtrStopVersionTemplateData.class;
        }

        public String getEntityName()
        {
            return "eduCtrStopVersionTemplateData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
