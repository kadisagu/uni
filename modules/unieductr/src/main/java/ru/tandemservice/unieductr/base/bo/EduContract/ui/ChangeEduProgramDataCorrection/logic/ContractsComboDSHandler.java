/* $Id:$ */
package ru.tandemservice.unieductr.base.bo.EduContract.ui.ChangeEduProgramDataCorrection.logic;

import com.beust.jcommander.internal.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.caf.logic.handler.SimpleTitledComboDataSourceWithPopupSizeHandler;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 17.08.15
 * Time: 4:21
 */
public class ContractsComboDSHandler extends SimpleTitledComboDataSourceWithPopupSizeHandler
{
    public ContractsComboDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {

            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(CtrContractObject.class, "c")
                    .where(exists(EduCtrEducationPromise.class, EduCtrEducationPromise.src().owner().contract().s(), property("c")))
                    .order(property("c", CtrContractObject.number()));

            String filter = input.getComboFilterByValue();
            if(!StringUtils.isEmpty(filter))
                builder.where(likeUpper(property("c", CtrContractObject.number()), value(CoreStringUtils.escapeLike(filter, true))));

            List<CtrContractObject> contractObjects = createStatement(builder).list();

            context.put(UIDefines.COMBO_OBJECT_LIST, contractObjects);
            DSOutput output = super.execute(input, context);
            output.setTotalSize(contractObjects.size());
            return output;

    }

}
