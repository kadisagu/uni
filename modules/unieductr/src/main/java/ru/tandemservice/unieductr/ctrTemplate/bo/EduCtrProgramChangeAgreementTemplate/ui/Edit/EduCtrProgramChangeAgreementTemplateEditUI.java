/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrProgramChangeAgreementTemplate.ui.Edit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.StaticFullCheckSelectModel;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.EduCtrStudentContractTemplateManager;
import ru.tandemservice.unieductr.student.entity.EduCtrProgramChangeAgreementTemplateData;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;

import java.util.*;

/**
 * @author azhebko
 * @since 04.12.2014
 */
@Input(@Bind(key = IUIPresenter.PUBLISHER_ID, binding = "templateId", required = true))
public class EduCtrProgramChangeAgreementTemplateEditUI extends UIPresenter
{
    private Long _templateId;
    private CtrContractVersion _version;
    private CtrContractVersion _prevVersion;
    private EduCtrProgramChangeAgreementTemplateData _template;

    private OrgUnit formativeOrgUnit;

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        this.formativeOrgUnit = formativeOrgUnit;
    }

    public OrgUnit getFormativeOrgUnit() { return formativeOrgUnit; }

    private final ISelectModel providerModel = new StaticFullCheckSelectModel(ContactorPerson.fullTitle().s()) {
        @Override protected List list() {
            final OrgUnit formativeOrgUnit = EduCtrProgramChangeAgreementTemplateEditUI.this.getFormativeOrgUnit();
            if (null == formativeOrgUnit) { return Collections.emptyList(); }

            final Collection<ContactorPerson> presenters = EduContractManager.instance().dao().getAcademyPresenters(formativeOrgUnit);
            return new ArrayList<>(presenters);
        }
    };
    public ISelectModel getProviderModel() { return this.providerModel; }

    private ContactorPerson provider;
    public ContactorPerson getProvider() { return this.provider; }
    public void setProvider(final ContactorPerson provider) { this.provider = provider; }

    @Override
    public void onComponentActivate() {
        _template = IUniBaseDao.instance.get().getNotNull(getTemplateId());
        setVersion(_template.getOwner());
        _prevVersion = CtrContractVersionManager.instance().dao().getPreviousVersion(_version);
        EduCtrStudentContract contract = DataAccessServices.dao().getNotNull(EduCtrStudentContract.class, EduCtrStudentContract.contractObject(), getVersion().getContract());
        setFormativeOrgUnit(contract.getStudent().getEducationOrgUnit().getFormativeOrgUnit());
        this.setProvider(CtrContractVersionManager.instance().dao().getContactor(getVersion(), CtrContractRoleCodes.EDU_CONTRACT_PROVIDER).getContactor());
    }

    @Override
    public void onComponentRefresh()
    {

    }

    public void onClickApply()
    {
        final Date durationBeginDate = _version.getDurationBeginDate();
        if(durationBeginDate != null && durationBeginDate.before(_prevVersion.getDurationBeginDate()))
            _uiSupport.error("Дату начала действия нельзя установить раньше даты начала действия предыдущей версии", "durationBeginDate");

        if(getUserContext().getErrorCollector().hasErrors())
            return;

        IUniBaseDao.instance.get().update(getTemplate());
        EduCtrStudentContractTemplateManager.instance().dao().doUpdateContractVersionContactor(getVersion(), getProvider(), null);
        deactivate();
    }

    public EduCtrProgramChangeAgreementTemplateData getTemplate()
    {
        return _template;
    }

    public void setTemplate(EduCtrProgramChangeAgreementTemplateData template)
    {
        this._template = template;
    }

    public Long getTemplateId()
    {
        return _templateId;
    }

    public void setTemplateId(Long templateId)
    {
        _templateId = templateId;
    }

    public CtrContractVersion getVersion()
    {
        return _version;
    }

    public void setVersion(CtrContractVersion version)
    {
        _version = version;
    }
}