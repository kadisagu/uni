/**
 *$Id: SystemActionPubExt.java 4443 2014-05-21 08:10:28Z nvankov $
 */
package ru.tandemservice.unieductr.base.ext.DataCorrection.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.base.bo.DataCorrection.ui.Pub.DataCorrectionPub;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
@Configuration
public class DataCorrectionPubExt extends BusinessComponentExtensionManager
{
    public static final String EDU_CTR_CORRECTION_PUB_ADDON_NAME = "EduCtrCorrectionPubAddon";

    @Autowired
    private DataCorrectionPub _dataCorrectionPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_dataCorrectionPub.presenterExtPoint())
                .addAddon(uiAddon(EDU_CTR_CORRECTION_PUB_ADDON_NAME, EduCtrCorrectionPubAddon.class))
                .create();
    }
}
