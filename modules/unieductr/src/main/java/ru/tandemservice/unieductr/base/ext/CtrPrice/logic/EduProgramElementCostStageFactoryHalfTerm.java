/* $Id$ */
package ru.tandemservice.unieductr.base.ext.CtrPrice.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.tandemframework.shared.ctr.base.bo.CtrPrice.util.ICtrPriceElementCostStageFactory;
import org.tandemframework.shared.ctr.base.entity.ICtrPriceElement;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;

import ru.tandemservice.unieductr.base.entity.EduProgramPrice;

/**
 * @author Dmitry Seleznev
 * @since 21.10.2011
 */
public class EduProgramElementCostStageFactoryHalfTerm implements ICtrPriceElementCostStageFactory
{
    protected static final int[] half_terms = new int[] {1, 2};


    @Override
    public List<CtrPriceElementCostStage> getStageList(ICtrPriceElement priceElement)
    {
        final Map<Integer, Integer> term2courseMap;
        if (priceElement instanceof EduProgramPrice) {
            term2courseMap = EduProgramElementCostStageFactoryTerm.getStageCodeSource((EduProgramPrice) priceElement);
        } else {
            return new ArrayList<>(0);
        }

        List<CtrPriceElementCostStage> stagesList = new ArrayList<CtrPriceElementCostStage>();
        for (Map.Entry<Integer, Integer> term2courseEntry : term2courseMap.entrySet())
        {
            for (int halfTerm: half_terms)
            {
                CtrPriceElementCostStage stage = new CtrPriceElementCostStage();
                stage.setTitle("За " + term2courseEntry.getKey() + " семестр, " + halfTerm + " часть (" + term2courseEntry.getValue() + " курс)");
                stage.setStageUniqueCode(getStageUniqueCode(term2courseEntry, halfTerm));
                stagesList.add(stage);
            }
        }

        return stagesList;
    }

    @Override
    public List<CtrPriceElementCostStage> filterStageList(ICtrPriceElement priceElement, List<CtrPriceElementCostStage> stageList, Map<String, Object> filter)
    {
        return stageList;
    }

    private String getStageUniqueCode(Map.Entry<Integer, Integer> stageCodeEntry, int halfTermNum)
    {
        return String.valueOf(stageCodeEntry.getKey()) + "-" + String.valueOf(halfTermNum);
    }


}