package ru.tandemservice.unieductr.student.entity;

import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;

import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.EduCtrStudentContractTemplateManager;
import ru.tandemservice.unieductr.student.entity.gen.EduCtrStudentContractTemplateDataGen;

/**
 * Данные шаблона договора на обучение студента по ОП
 *
 * Данные базового шаблона для создания договора на обучение студента по образовательной программе.
 */
public class EduCtrStudentContractTemplateData extends EduCtrStudentContractTemplateDataGen
{
    @Override
    public ICtrContractTemplateManager getManager() {
        return EduCtrStudentContractTemplateManager.instance();
    }
}