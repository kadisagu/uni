/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrPaymentsReport.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport;

/**
 * @author Alexey Lopatin
 * @since 17.04.2015
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "reportId", required = true)})
public class EduCtrPaymentsReportPubUI extends UIPresenter
{
    private Long _reportId;
    private EduCtrPaymentsReport _report;

    @Override
    public void onComponentRefresh()
    {
        _report = DataAccessServices.dao().getNotNull(_reportId);
    }

    public void onClickPrint()
    {
        byte[] content = _report.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл отчета пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("PaymentReport.rtf").document(content), true);
    }

    public String getPermissionKey()
    {
        return _report.getOrgUnit() == null ? "eduCtrPaymentsReport" : new OrgUnitSecModel(_report.getOrgUnit()).getPermission("orgUnit_viewEduCtrPaymentsReport");
    }

    @Override
    public ISecured getSecuredObject()
    {
        return _report.getOrgUnit() != null ? _report.getOrgUnit() : super.getSecuredObject();
    }

    // Getters & Setters

    public Long getReportId()
    {
        return _reportId;
    }

    public void setReportId(Long reportId)
    {
        _reportId = reportId;
    }

    public EduCtrPaymentsReport getReport()
    {
        return _report;
    }

    public void setReport(EduCtrPaymentsReport report)
    {
        _report = report;
    }
}
