package ru.tandemservice.unieductr.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData;

/**
 * Данные шаблона договора на обучение
 *
 * Интерфейс данных шаблонов на обучение.
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IEducationContractVersionTemplateDataGen extends InterfaceStubBase
 implements IEducationContractVersionTemplateData{
    public static final int VERSION_HASH = -1623303820;

    public static final String L_OWNER = "owner";
    public static final String L_EDUCATION_YEAR = "educationYear";

    private CtrContractVersion _owner;
    private EducationYear _educationYear;

    @NotNull

    public CtrContractVersion getOwner()
    {
        return _owner;
    }

    public void setOwner(CtrContractVersion owner)
    {
        _owner = owner;
    }

    @NotNull

    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    public void setEducationYear(EducationYear educationYear)
    {
        _educationYear = educationYear;
    }

    private static final Path<IEducationContractVersionTemplateData> _dslPath = new Path<IEducationContractVersionTemplateData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData");
    }
            

    /**
     * Версия договора, созданная по данному шаблону.
     *
     * @return Версия договора. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData#getOwner()
     */
    public static CtrContractVersion.Path<CtrContractVersion> owner()
    {
        return _dslPath.owner();
    }

    /**
     * Учебный год на который создан договор или доп.соглашение.
     *
     * @return Учебный год, на который создана версия. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    public static class Path<E extends IEducationContractVersionTemplateData> extends EntityPath<E>
    {
        private CtrContractVersion.Path<CtrContractVersion> _owner;
        private EducationYear.Path<EducationYear> _educationYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * Версия договора, созданная по данному шаблону.
     *
     * @return Версия договора. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData#getOwner()
     */
        public CtrContractVersion.Path<CtrContractVersion> owner()
        {
            if(_owner == null )
                _owner = new CtrContractVersion.Path<CtrContractVersion>(L_OWNER, this);
            return _owner;
        }

    /**
     * Учебный год на который создан договор или доп.соглашение.
     *
     * @return Учебный год, на который создана версия. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

        public Class getEntityClass()
        {
            return IEducationContractVersionTemplateData.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
