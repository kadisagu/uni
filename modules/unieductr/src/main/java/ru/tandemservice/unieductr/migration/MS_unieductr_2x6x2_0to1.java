package ru.tandemservice.unieductr.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unieductr_2x6x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eduCtrStudentAgreementTemplateData

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("eductr_stduent_a_ctmpldt_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey()
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eduCtrStudentAgreementTemplateData");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eduCtrStudentContractTemplateData

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("eductr_stduent_c_ctmpldt_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey()
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eduCtrStudentContractTemplateData");

		}


    }
}