/**
 *$Id:$
 */
package ru.tandemservice.unieductr.reports.bo.EduCtrDebitorsPayReport.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 30.10.12
 */
@Configuration
public class EduCtrDebitorsPayReportList extends BusinessComponentManager
{
    private DQLOrderDescriptionRegistry _orderDescriptionRegistry = new DQLOrderDescriptionRegistry(EduCtrDebitorsPayReport.class, "b");
    
    // dataSource
    public static final String REPORT_LIST_DS = "reportListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REPORT_LIST_DS, columnListHandler(), reportListDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> reportListDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName(), EduCtrDebitorsPayReport.class)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Date formingDateFrom = context.get(EduCtrDebitorsPayReportListUI.PROP_FORMING_DATE_FROM);
                Date formingDateTo = context.get(EduCtrDebitorsPayReportListUI.PROP_FORMING_DATE_TO);
                Date reportDateFrom = context.get(EduCtrDebitorsPayReportListUI.PROP_REPORT_DATE_FROM);
                Date reportDateTo = context.get(EduCtrDebitorsPayReportListUI.PROP_REPORT_DATE_TO);
                Long orgUnitId = context.get(EduCtrDebitorsPayReportListUI.PROP_ORG_UNIT_ID);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EduCtrDebitorsPayReport.class, "b");

                if (orgUnitId != null)
                    builder.where(eq(property(EduCtrDebitorsPayReport.orgUnit().id().fromAlias("b")), value(orgUnitId)));

                if (formingDateFrom != null)
                    builder.where(ge(property(EduCtrDebitorsPayReport.formingDate().fromAlias("b")), value(formingDateFrom, PropertyType.DATE)));
                if (formingDateTo != null)
                    builder.where(le(property(EduCtrDebitorsPayReport.formingDate().fromAlias("b")), value(formingDateTo, PropertyType.DATE)));
                if (reportDateFrom != null)
                    builder.where(ge(property(EduCtrDebitorsPayReport.reportDate().fromAlias("b")), value(reportDateFrom, PropertyType.DATE)));
                if (reportDateTo != null)
                    builder.where(le(property(EduCtrDebitorsPayReport.reportDate().fromAlias("b")), value(reportDateTo, PropertyType.DATE)));
                
                _orderDescriptionRegistry.applyOrder(builder, input.getEntityOrder());

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
            }
        };
    }

    @Bean
    public ColumnListExtPoint columnListHandler()
    {
        return columnListExtPointBuilder(REPORT_LIST_DS)
                .addColumn(indicatorColumn("report").defaultIndicatorItem(new IndicatorColumn.Item("report")))
                .addColumn(publisherColumn("formDate", EduCtrDebitorsPayReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order())
                .addColumn(dateColumn("reportDate", EduCtrDebitorsPayReport.reportDate()).order())
                .addColumn(textColumn("formativeOrgUnit", EduCtrDebitorsPayReport.formativeOrgUnit()).order())
                .addColumn(textColumn("territorialOrgUnit", EduCtrDebitorsPayReport.territorialOrgUnit()).order())
                .addColumn(textColumn("educationLevelHighSchool", EduCtrDebitorsPayReport.educationLevelHighSchool()).order())
                .addColumn(textColumn("developForm", EduCtrDebitorsPayReport.developForm()).order())
                .addColumn(textColumn("developCondition", EduCtrDebitorsPayReport.developCondition()).order())
                .addColumn(textColumn("developTech", EduCtrDebitorsPayReport.developTech()).order())
                .addColumn(textColumn("developPeriod", EduCtrDebitorsPayReport.developPeriod()).order())
                .addColumn(textColumn("course", EduCtrDebitorsPayReport.course()).order())
                .addColumn(textColumn("group", EduCtrDebitorsPayReport.group()).order())
                .addColumn(actionColumn("print", new Icon("printer"), "onClickPrintReport"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon("delete"), DELETE_LISTENER).permissionKey("ui:deleteStorableReportPermissionKey")
                        .alert(FormattedMessage.with().template("reportListDS.delete.alert").parameter(EduCtrDebitorsPayReport.formingDate().s(), DateFormatter.DATE_FORMATTER_WITH_TIME).create()))
                .create();
    }
}
