/* $Id:$ */
package ru.tandemservice.unieductr.base.bo.EduContract.ui.PaymentPromiseSection;

import com.google.common.collect.FluentIterable;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableLong;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.datasource.resolver.DefaultLinkResolver;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionContext;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContractResultDao;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.CtrContractVersionContextOwnerUIPublisher;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.PubVer.CtrContractVersionPubVer;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.PubVer.CtrContractVersionPubVerUI;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contract.*;
import org.tandemframework.shared.ctr.base.entity.contract.gen.CtrPaymentPromiceGen;
import org.tandemframework.shared.ctr.base.entity.contract.gen.CtrPaymentResultGen;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.ui.formatters.MoneyFormatter;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduContractPaymentPromiceResultPair;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.IEduContractPrintDAO;
import ru.tandemservice.unieductr.base.bo.EduContract.ui.PaymentPromiseAddEdit.EduContractPaymentPromiseAddEdit;
import ru.tandemservice.unieductr.base.bo.EduContract.ui.PaymentPromiseAddEdit.EduContractPaymentPromiseAddEditUI;
import ru.tandemservice.unieductr.base.bo.EduContract.ui.PaymentResultAddEdit.EduContractPaymentResultAddEdit;
import ru.tandemservice.unieductr.base.bo.EduContract.ui.PaymentResultAddEdit.EduContractPaymentResultAddEditUI;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 4/15/14
 */
public class EduContractPaymentPromiseSectionUI extends CtrContractVersionContextOwnerUIPublisher
{
    private static final String PROPERTY_PROMISE__FIRST_VERSION__TITLE = "promiseFirstVersionTitle";
    private static final String PROPERTY_PROMISE__FIRST_VERSION = "promiseFirstVersion";
    private static final String PROPERTY_PROMISE__HISTORY = "promiseHistory";

    private StaticListDataSource<DataWrapper> promiseDataSource;
    public StaticListDataSource<DataWrapper> getPromiseDataSource() { return this.promiseDataSource; }
    protected void setPromiseDataSource(final StaticListDataSource<DataWrapper> promiseDataSource) { this.promiseDataSource = promiseDataSource; }

    private StaticListDataSource<DataWrapper> resultDataSource;
    public StaticListDataSource<DataWrapper> getResultDataSource() { return this.resultDataSource; }
    protected void setResultDataSource(final StaticListDataSource<DataWrapper> resultDataSource) { this.resultDataSource = resultDataSource; }

    private Map<String, ICtrContractResultDao.ICtrContractStatusItem> _statusItems;
    private Map<String, Long> _eduOuPromiceCurrencyToAmount;
    private Map<String, Long> _eduOuResultCurrencyToAmount;
    private Map<String, Long> _balanceCurrencyToAmount;
    private Map<String, Long> _eduOuDebtCurrencyToAmount;


    public Long getHolderId() {
        return this.getRootPresenter().getHolder().getId();
    }

    @Override
    public void onComponentRefresh()
    {
        final CtrContractVersionContext context = this.getContext();
        final CtrContractVersion version = context.getDocument();
        {
            final Collection<DataWrapper> promiseList = FluentIterable
            .from(context.getPromiceList())
            .filter(CtrPaymentPromice.class)
            .transform(DataWrapper::new)
            .toList();

            Map<MultiKey, MutableLong> totalPromiseMap = new HashMap<>();
            final Map<MultiKey, List<CtrContractPromice>> historyMap = context.getPromiceHistoryMap();
            for (final DataWrapper w: promiseList)
            {
                final CtrPaymentPromice p = w.getWrapped();
                final List<CtrContractPromice> history = historyMap.get(p.getPromiceLocalKey());
                final CtrContractPromice historyItem = history.get(0);
                final CtrContractVersion historyVersion = historyItem.getSrc().getOwner();
                final CtrContractVersionTemplateData templateData = context.getTemplateData();
                final String title = "№ " + historyVersion.getNumber() + " " + (version.equals(historyVersion) ? "(текущая версия)" : ("от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(version.getDocStartDate())));
                w.putInMap(PROPERTY_PROMISE__HISTORY, history);
                w.putInMap(PROPERTY_PROMISE__FIRST_VERSION, historyItem);
                w.putInMap(PROPERTY_PROMISE__FIRST_VERSION__TITLE, title);

                final ContactorPerson paymentDst = p.getDst().getContactor();
                final String paymentDstTitle = paymentDst.getTitle(); // ((paymentDst instanceof EmployeePostContactor) ? "ВУЗ" : paymentDst.getTitle());
                w.putInMap("paymentDstTitle", paymentDstTitle);

                final Currency currency = p.getCurrency();
                SafeMap.safeGet(totalPromiseMap, new MultiKey(currency, p.getSrc()), MutableLong.class).add(p.getCostAsLong());
            }

            final StaticListDataSource<DataWrapper> promiseDataSource = new StaticListDataSource<>(promiseList);
            promiseDataSource.addColumn(
                new PublisherLinkColumn("Документ", PROPERTY_PROMISE__FIRST_VERSION__TITLE)
                .setResolver(
                    DefaultLinkResolver.with()
                    .component(CtrContractVersionPubVer.class)
                    .primaryKeyPath("promiseFirstVersion."+CtrPaymentPromice.src().owner().id().s())
                    .primaryKeyAsParameter(CtrContractVersionPubVerUI.VERSION_ID)
                    .parameter(UIPresenter.PUBLISHER_ID, this.getHolderId())
                    .create()
                )
                .setOrderable(false)
            );
            promiseDataSource.addColumn(new SimpleColumn("Этап", CtrPaymentPromice.stage().s()).setOrderable(false).setClickable(false));
            promiseDataSource.addColumn(new SimpleColumn("Оплатить до", CtrPaymentPromice.deadlineString().s()).setOrderable(false).setClickable(false));
            promiseDataSource.addColumn(new SimpleColumn("Сумма оплаты", CtrPaymentPromice.costAsString().s()).setOrderable(false).setClickable(false));
            promiseDataSource.addColumn(new SimpleColumn("Плательщик", CtrPaymentPromice.src().contactor().delegateTitle().s()).setOrderable(false).setClickable(false));
            promiseDataSource.addColumn(new SimpleColumn("Получатель", CtrPaymentPromice.dst().contactor().delegateTitle().s()).setOrderable(false).setClickable(false));
            promiseDataSource.addColumn(new SimpleColumn("Комментарий", CtrPaymentPromice.comment().s()).setOrderable(false).setClickable(false));
            promiseDataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrintPromiseQuittance", "Печать").setPermissionKey(context.getSecModel().getPermission("print_paymentPromiseQuittance")));

            if (this.isEditable()) {
                final IEntityHandler entityDisabler = entity -> {
                    final CtrContractPromice historyItem = ((DataWrapper)entity).get(PROPERTY_PROMISE__FIRST_VERSION);
                    return null == historyItem || !version.equals(historyItem.getSrc().getOwner());
                };
                promiseDataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditPromise").setDisableHandler(entityDisabler).setPermissionKey(context.getSecModel().getPermission("edit_paymentPromise")));
                promiseDataSource.addColumn(
                    new ActionColumn(
                        "Удалить",
                        ActionColumn.DELETE,
                        "onClickDeletePromise",
                        "Удалить обязательство по оплате, плательщик {0}, {1}, {2}?",
                        CtrPaymentPromice.src().contactor().title().s(),
                        CtrPaymentPromice.costAsString().s(),
                        CtrPaymentPromice.deadlineString().s()
                    )
                    .setDisableHandler(entityDisabler)
                    .setPermissionKey(context.getSecModel().getPermission("delete_paymentPromise"))
                );
            }

            this.setPromiseDataSource(promiseDataSource);
        }

        {
            final Collection<DataWrapper> resultList = FluentIterable
            .from(context.getResultList())
            .filter(CtrPaymentResult.class)
            .transform(DataWrapper::new)
            .toList();

            Map<MultiKey, MutableLong> totalResultMap = new HashMap<>();
            for (DataWrapper w: resultList) {
                CtrPaymentResult r = w.getWrapped();
                Currency currency = r.getCurrency();
                SafeMap.safeGet(totalResultMap, new MultiKey(currency, r.getSrc()), MutableLong.class).add(r.getCostAsLong());
            }

            final StaticListDataSource<DataWrapper> resultDataSource = new StaticListDataSource<>(resultList);
            resultDataSource.addColumn(new SimpleColumn("Подтверждающий документ", CtrPaymentResult.paymentDocumentTitle().s()).setOrderable(false));
            resultDataSource.addColumn(new SimpleColumn("Платежное поручение", CtrPaymentResult.sourceDocumentNumber().s()).setOrderable(false));
            resultDataSource.addColumn(new SimpleColumn("Дата платежа", CtrPaymentResult.timestamp().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false));
            resultDataSource.addColumn(new SimpleColumn("Сумма оплаты", CtrPaymentResult.costAsString().s()).setOrderable(false));
            resultDataSource.addColumn(new SimpleColumn("Плательщик", CtrPaymentResult.src().delegateTitle().s()).setOrderable(false));
            resultDataSource.addColumn(new SimpleColumn("Получатель", CtrPaymentResult.dst().delegateTitle().s()).setOrderable(false));
            resultDataSource.addColumn(new SimpleColumn("Комментарий", CtrPaymentResult.comment().s()).setOrderable(false));

            if (this.isResultEditable()) {
                resultDataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditResult").setPermissionKey(context.getSecModel().getPermission("edit_paymentResult")));
                resultDataSource.addColumn(
                    new ActionColumn(
                        "Удалить",
                        ActionColumn.DELETE,
                        "onClickDeleteResult",
                        "Удалить платеж, плательщик  {0}, {1}, {2}?",
                        CtrPaymentResult.srcAsText().s(),
                        CtrPaymentResult.costAsString().s(),
                        CtrPaymentResult.timestampString().s()
                    )
                    .setPermissionKey(context.getSecModel().getPermission("delete_paymentResult"))
                );
            }

            this.setResultDataSource(resultDataSource);

        }
        prepareContractStatusItems();
        prepareBalanceMap();
        prepareEduOuPromiceMap();
        prepareEduOuDebtMap();
        prepareEduOuResultList();
    }

    public void onClickPrintPromiseQuittance() {
        final Long id = this.getListenerParameterAsLong();
        final CtrContractPromice promise = this.getContext().getPromiceMap().get(id);
        if (promise instanceof CtrPaymentPromice) {
            IDocumentRenderer rtf = ((IEduContractPrintDAO) EduContractManager.instance().printer()).printPromiseQuittance((CtrPaymentPromice)promise);
            BusinessComponentUtils.downloadDocument(rtf, true);
        }

    }


    public void onClickAddPromise() {
        if (!this.isEditable()) { return; }
        getActivationBuilder()
        .asRegionDialog(EduContractPaymentPromiseAddEdit.class)
        .parameter(EduContractPaymentPromiseAddEditUI.VERSION_ID, getContext().getDocument().getId())
//        .parameter(PUBLISHER_ID, null)
        .activate();
    }

    public void onClickEditPromise() {
        if (!this.isEditable()) { return; }
        getActivationBuilder()
        .asRegionDialog(EduContractPaymentPromiseAddEdit.class)
        .parameter(EduContractPaymentPromiseAddEditUI.VERSION_ID, getContext().getDocument().getId())
        .parameter(PUBLISHER_ID, this.getListenerParameterAsLong())
        .activate();
    }

    public void onClickDeletePromise() {
        try {
            if (!this.isEditable()) { return; }
            final Long id = this.getListenerParameterAsLong();
            final CtrContractPromice promise = this.getContext().getPromiceMap().get(id);
            if (promise instanceof CtrPaymentPromice) {
                DataAccessServices.dao().delete(promise);
            }
        } finally {
            this.getSupport().setRefreshScheduled(true);
        }
    }


    public void onClickAddResult() {
        if (!this.isResultEditable()) { return; }
        getActivationBuilder()
        .asRegionDialog(EduContractPaymentResultAddEdit.class)
        .parameter(EduContractPaymentResultAddEditUI.VERSION_ID, getContext().getDocument().getId())
//        .parameter(PUBLISHER_ID, null)
        .activate();
    }

    public void onClickEditResult() {
        if (!this.isResultEditable()) { return; }
        getActivationBuilder()
        .asRegionDialog(EduContractPaymentResultAddEdit.class)
        .parameter(EduContractPaymentResultAddEditUI.VERSION_ID, getContext().getDocument().getId())
        .parameter(PUBLISHER_ID, this.getListenerParameterAsLong())
        .activate();
    }

    public void onClickDeleteResult() {
        try {
            if (!this.isResultEditable()) { return; }
            final Long id = this.getListenerParameterAsLong();
            final CtrContractResult result = this.getContext().getResultMap().get(id);
            if (result instanceof CtrPaymentResult) {
                DataAccessServices.dao().delete(result);
            }
        } finally {
            this.getSupport().setRefreshScheduled(true);
        }

    }

    public String getStudentPromice()
    {
        final CtrContractVersionContext context = this.getContext();
        Map<String, Long> currencyToSum = context.getPromiceMap().values()
                .stream()
                .filter(payment -> payment instanceof CtrPaymentPromice)
                .map(CtrPaymentPromice.class::cast)
                .filter(payment -> !(payment.getSrc().getContactor() instanceof EmployeePostContactor))
                .collect(Collectors.toMap(
                        payment -> payment.getCurrency().getCode(),
                        CtrPaymentPromiceGen::getCostAsLong,
                        (cost1, cost2) -> cost1 + cost2));

        List<String> currencyList = new ArrayList<>();
        for (Map.Entry<String, Long> item : currencyToSum.entrySet())
        {
            currencyList.add(MoneyFormatter.formatter(item.getKey(), 4).format(item.getValue()));
        }
        return getMoneyDisplayForm(currencyList);
    }

    public String getStudentPayment()
    {
        final CtrContractVersionContext context = this.getContext();
        Map<String, Long> paymentResults = context.getResultList().stream()
                .filter(result -> result instanceof CtrPaymentResult)
                .map(CtrPaymentResult.class::cast)
                .filter(result -> !(result.getSrc() instanceof EmployeePostContactor))
                .collect(Collectors.toMap(
                        result -> result.getCurrency().getCode(),
                        CtrPaymentResultGen::getCostAsLong,
                        (cost1, cost2) -> cost1 + cost2));

        List<String> currencyList = new ArrayList<>();
        for (Map.Entry<String, Long> result: paymentResults.entrySet())
        {
            currencyList.add(MoneyFormatter.formatter(result.getKey(), 4).format(result.getValue()));
        }
        return getMoneyDisplayForm(currencyList);
    }

    public String getEduOuResult()
    {
        List<String> currencyList = new ArrayList<>();
        for (Map.Entry<String, Long> item : _eduOuResultCurrencyToAmount.entrySet())
        {
            currencyList.add(MoneyFormatter.formatter(item.getKey(), 4).format(item.getValue()));
        }
        return getMoneyDisplayForm(currencyList);
    }

    public String getEduOuPromice()
    {
        List<String> currencyList = new ArrayList<>();
        for (Map.Entry<String, Long> item : _eduOuPromiceCurrencyToAmount.entrySet())
        {
            currencyList.add(MoneyFormatter.formatter(item.getKey(), 4).format(item.getValue()));
        }
        return getMoneyDisplayForm(currencyList);
    }

    public boolean isEduOuHasPromice()
    {
        return _eduOuPromiceCurrencyToAmount.size() > 0;
    }

    public boolean isEduOuHasResult()
    {
        return _eduOuResultCurrencyToAmount.size() > 0;
    }

    public boolean isEduOuHasDebt()
    {
        return _eduOuDebtCurrencyToAmount.size() > 0;
    }

    public String getPromiceAmount()
    {
        List<String> currencyList = new ArrayList<>();
        for (Map.Entry<String, ICtrContractResultDao.ICtrContractStatusItem> item : _statusItems.entrySet())
        {
            currencyList.add(MoneyFormatter.formatter(item.getKey(), 4).format(item.getValue().getPromiceAmount()));
        }
        return getMoneyDisplayForm(currencyList);
    }

    public String getResultAmount()
    {
        List<String> currencyList = new ArrayList<>();
        for (Map.Entry<String, ICtrContractResultDao.ICtrContractStatusItem> item : _statusItems.entrySet())
        {
            currencyList.add(MoneyFormatter.formatter(item.getKey(), 4).format(item.getValue().getResultAmount()));
        }
        return getMoneyDisplayForm(currencyList);
    }

    public String getBalance()
    {
        List<String> currencyList = new ArrayList<>();
        for (Map.Entry<String, Long> item : _balanceCurrencyToAmount.entrySet())
        {
            if (item.getValue() != 0) currencyList.add(MoneyFormatter.formatter(item.getKey(), 4).format(item.getValue()));
        }
        return getMoneyDisplayForm(currencyList);
    }

    public String getStudentDebt()
    {
        List<String> currencyList = new ArrayList<>();
        for (Map.Entry<String, Long> item : _balanceCurrencyToAmount.entrySet())
        {
            Long debt = item.getValue() > 0 ? item.getValue() : 0L;
            if (debt != 0) currencyList.add(MoneyFormatter.formatter(item.getKey(), 4).format(debt));
        }
        return getMoneyDisplayForm(currencyList);
    }

    public String getEduOuDebt()
    {
        List<String> currencyList = new ArrayList<>();
        for (Map.Entry<String, Long> item : _eduOuDebtCurrencyToAmount.entrySet())
        {
            if (item.getValue() != 0) currencyList.add(MoneyFormatter.formatter(item.getKey(), 4).format(item.getValue()));
        }
        return getMoneyDisplayForm(currencyList);
    }

    private void prepareBalanceMap()
    {
        _balanceCurrencyToAmount = new HashMap<>();
        for (Map.Entry<String, ICtrContractResultDao.ICtrContractStatusItem> item : _statusItems.entrySet())
        {
            _balanceCurrencyToAmount.put(item.getKey(), item.getValue().getPromiceAmount() - item.getValue().getResultAmount());
        }
    }

    private void prepareEduOuDebtMap()
    {
        _eduOuDebtCurrencyToAmount = new HashMap<>();
        for (Map.Entry<String, Long> item : _balanceCurrencyToAmount.entrySet())
        {
            Long debt = item.getValue() < 0 ? Math.abs(item.getValue()) : 0;
            if (debt != 0)
                _eduOuDebtCurrencyToAmount.put(item.getKey(), debt);
        }
    }

    private void prepareEduOuPromiceMap()
    {
        final CtrContractVersionContext context = this.getContext();
        _eduOuPromiceCurrencyToAmount = context.getPromiceMap().values()
                .stream()
                .filter(p -> p instanceof CtrPaymentPromice)
                .map(CtrPaymentPromice.class::cast)
                .filter(p -> (p.getSrc().getContactor() instanceof EmployeePostContactor))
                .filter(p -> p.getCostAsLong() > 0L)
                .collect(Collectors.toMap(
                        p -> p.getCurrency().getCode(),
                        CtrPaymentPromiceGen::getCostAsLong,
                        (value1, value2) -> value1 + value2));
    }

    private void prepareEduOuResultList()
    {
        final CtrContractVersionContext context = this.getContext();
        _eduOuResultCurrencyToAmount = context.getResultMap().values()
                .stream()
                .filter(p -> p instanceof CtrPaymentResult)
                .map(CtrPaymentResult.class::cast)
                .filter(p -> (p.getSrc() instanceof EmployeePostContactor))
                .filter(p -> p.getCostAsLong() > 0L)
                .collect(Collectors.toMap(
                        p -> p.getCurrency().getCode(),
                        CtrPaymentResult::getCostAsLong,
                        (value1, value2) -> value1 + value2));
    }

    private String getMoneyDisplayForm(List<String> currencyList)
    {
        return currencyList.isEmpty() ? MoneyFormatter.ruMoneyFormatter(4).format(0L) : StringUtils.join(currencyList, ", ");
    }

    private void prepareContractStatusItems()
    {
        final CtrContractVersionContext context = this.getContext();
        final CtrContractVersion version = context.getDocument();
        Long ctrVersionId = version.getId();
        List<Currency> currencyList = DataAccessServices.dao().getList(Currency.class);
        Map<Long, Map<String, ICtrContractResultDao.ICtrContractStatus>> contractVersionStatusMap = CtrContractVersionManager.instance().results().getContractVersionStatusMap(Collections.singletonList(ctrVersionId), new Date());

        _statusItems = new HashMap<>();
        for (Map.Entry<Long, Map<String, ICtrContractResultDao.ICtrContractStatus>> versionEntry : contractVersionStatusMap.entrySet())
        {
            for (Map.Entry<String, ICtrContractResultDao.ICtrContractStatus> keyVersionStatusEntry : versionEntry.getValue().entrySet())
            {
                for (Currency currency : currencyList)
                {
                    if (EduContractPaymentPromiceResultPair.payDirectedTo(keyVersionStatusEntry.getKey(), TopOrgUnit.getInstance().getId(), currency.getCode()))
                    {
                        Collection<ICtrContractResultDao.ICtrContractStatusItem> list = keyVersionStatusEntry.getValue().getList();
                        ICtrContractResultDao.ICtrContractStatusItem[] objects = list.toArray(new ICtrContractResultDao.ICtrContractStatusItem[list.size()]);
                        _statusItems.put(currency.getCode(), objects[objects.length - 1]);
                    }
                }
            }
        }
    }


}