package ru.tandemservice.unieductr.base.entity;

import ru.tandemservice.unieductr.base.entity.gen.EduCtrContractVersionTemplateDataGen;

/**
 * Данные шаблона версии договора на обучение по обр. программе
 *
 * Данные базового шаблона для создания договора или доп.соглашения на обучение по образовательной программе.
 */
public abstract class EduCtrContractVersionTemplateData extends EduCtrContractVersionTemplateDataGen
{
}