package ru.tandemservice.unieductr.base.bo.EduContract.logic;

import java.util.Collection;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContextObject;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.entity.IEducationPromise;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractTypeCodes;

/**
 * @author vdanilov
 */
public interface IEduContractDAO extends INeedPersistenceSupport {

    /** { x.code | x ∊ CtrContractType, x.title = "Договор на обучение" }  */
    String CTR_CONTRACT_TYPE_EDUCATION = CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE;

    /**
    *  @return тип стороны договора на основании кода справочника "Виды договоров и доп. соглашений"
    **/
    ContractSides getContractSides(String ctrContractKindCode);

    /** @return тип договора "Договор на обучение" и все его дочерние, коды родительских элементов которых попадают в codes (если указан не null) */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    List<CtrContractType> getContractTypes(Collection<String> codes);

    /** @return перечень контактных лиц - представителей ОУ для указанного подразделения вверх по иерархии до первого найденного (если таковых нет - пустая коллекция) */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Collection<ContactorPerson> getAcademyPresenters(OrgUnit orgUnit);

    /** @return номер договора */
    @Transactional(propagation=Propagation.REQUIRED)
    String doGetNextNumber(String prefix);

    /** @param relation */
    @Transactional(propagation=Propagation.REQUIRED)
    void doDeleteContract(ICtrContextObject relation);

    /** @return список обязательств на обучение в указанной версии, отсортированнй по дедлайну */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    <T extends IEntity> List<IEducationPromise<T>> getEducationPromiseList(CtrContractVersion version);

    /** @return getEducationPromiseList(version).get(0) or ApplicationException */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    <T extends IEntity> IEducationPromise<T> getFirstEducationPromise(CtrContractVersion version) throws ApplicationException;

    /** Изменяет образовательную программу во всех версиях договоров
     * @param contractId - id договора
     * @param eduProgramOldId - id cтарой образовательной программы
     * @param eduProgramNewId - id новой образовательной программы
    */
    void doChangeEduProgram(Long contractId, Long eduProgramOldId, Long eduProgramNewId);

    /**
     * Возвращает студента по контракту
     * @param contractObject контракт
     * @return
     */
    @Transactional(propagation=Propagation.REQUIRED)
    Student getStudentByContract( CtrContractObject contractObject);

}
