package ru.tandemservice.unieductr.student.entity;

import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrProgramChangeAgreementTemplate.EduCtrProgramChangeAgreementTemplateManager;
import ru.tandemservice.unieductr.student.entity.gen.*;

/** @see ru.tandemservice.unieductr.student.entity.gen.EduCtrProgramChangeAgreementTemplateDataGen */
public class EduCtrProgramChangeAgreementTemplateData extends EduCtrProgramChangeAgreementTemplateDataGen
{
    @Override
    public ICtrContractTemplateManager getManager()
    {
        return EduCtrProgramChangeAgreementTemplateManager.instance();
    }
}