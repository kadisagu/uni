/**
 *$Id: EppSystemActionPubAddon.java 26910 2013-04-10 05:45:04Z vdanilov $
 */
package ru.tandemservice.unieductr.base.ext.SystemAction.ui.Pub;

import com.healthmarketscience.jackcess.Database;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uni.dao.mdbio.BaseIODao;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.unieductr.base.bo.EduSystemAction.EduActionActionManager;
import ru.tandemservice.unieductr.dao.mdbio.IContractsIODao;
import ru.tandemservice.unieductr.ws.EduProgramPriceEnvironmentNode;
import ru.tandemservice.unieductr.ws.IEduProgramPriceServiceDAO;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Date;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class EduCtrSystemActionPubAddon extends UIAddon
{
    public EduCtrSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickExportPrices() throws Exception
    {
        EduProgramPriceEnvironmentNode node = IEduProgramPriceServiceDAO.INSTANCE.get().getPriceData(new Date());

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(output);

        ByteArrayOutputStream result = new ByteArrayOutputStream();
        Marshaller marshaller = JAXBContext.newInstance(EduProgramPriceEnvironmentNode.class).createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(node, result);

        zip.putNextEntry(new ZipEntry("data.xml"));
        IOUtils.write(result.toByteArray(), zip);
        zip.close();

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName("Ratings.zip").document(output.toByteArray()).zip(), false);
    }

    public void onClickExportEduContract()
    {
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
                .document(getTemplate4Contracts())
                .fileName("contracts-" + DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()) + ".mdb")
                .bin(),
            true);
    }

    public void onClickImportEduContract()
    {
        // пытаемся открыть файл
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {

                // предварительно
                String path = ApplicationRuntime.getAppInstallPath();
                final File file = new File(path, "data/importContracts.mdb");
                if (!file.exists())
                {
                    throw new ApplicationException("Импорт данных провести не удалось: не найден файл «importContracts.mdb» с исходными данными в каталоге «${app.install.path}/data/». Разместите файл для импорта с указанным названием на сервере приложения по указанному пути.");
                }
                if (!file.canRead())
                {
                    throw new ApplicationException("Импорт данных провести не удалось: файл «${app.install.path}/data/importContracts.mdb» не доступен для чтения. Проверьте доступ к файлу на сервере приложения.");
                }

                // отключаем логирование в базу и прочие системные штуки
                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

                try
                {
                    BaseIODao.setupProcessState(state);
                    BaseIODao.doLoggedAction(() -> {
                        BaseIODao.getLog4jLogger().info("Import started.");
                        try
                        {
                            // открываем файл
                            try (Database mdb = Database.open(file))
                            {
                                // запускаем импорт
                                final Map<String, Long> contractMap = IContractsIODao.instance.get().doImport_ContractList(mdb);
                                BaseIODao.getLog4jLogger().info("Import finished. Done.");
                                return contractMap;
                            }
                        }
                        catch (Throwable t)
                        {
                            BaseIODao.getLog4jLogger().info("Import finished. Error.");
                            throw CoreExceptionUtils.getRuntimeException(t);
                        }
                    }, "importContracts");

                }
                catch (final Exception t)
                {

                    if (t instanceof ApplicationException)
                    {
                        // пробрасываем ApplicationException, как есть и добавляем ошибку в ErrorCollector для отображения сообщения о файле лога
                        UserContext.getInstance().getErrorCollector().add("Обратитесь к логу импорта (файл importContracts.log) на сервере приложения для получения детальной информации об ошибке.");
                        throw (ApplicationException) t;
                    }

                    Debug.exception(t.getMessage(), t);
                    throw new ApplicationException("Импорт данных провести не удалось: произошла ошибка при обращении к файлу импорта. Обратитесь к логу импорта (файл importContracts.log) на сервере приложения для получения детальной информации об ошибке.", t);

                }
                finally
                {
                    // включаем штуки и логирование обратно
                    Debug.resumeLogging();
                    eventLock.release();
                }

                // если все ок, просто закрываем диалог
                return new ProcessResult("Импорт договоров завершен."); // закрываем диалог
            }
        };
        new BackgroundProcessHolder().start("Импорт данных договоров об обучении", process, ProcessDisplayMode.unknown);
    }


    public static byte[] getTemplate4Contracts()
    {
        try
        {
            final File file = File.createTempFile("mdb-io-", "mdb");
            try
            {
                final Database mdb = Database.create(Database.FileFormat.V2003, file, false);
                IContractsIODao.instance.get().doExport_ContractsList(mdb);
                mdb.close();
                return FileUtils.readFileToByteArray(file);
            }
            finally
            {
                file.delete();
            }
        }
        catch (final Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public void onClickDeleteInvalidEduContracts() {
        UserContext.getInstance().getInfoCollector().add("Удалено договоров: " + EduActionActionManager.instance().dao().deleteInvalidEduContracts() + ".");
    }
}
