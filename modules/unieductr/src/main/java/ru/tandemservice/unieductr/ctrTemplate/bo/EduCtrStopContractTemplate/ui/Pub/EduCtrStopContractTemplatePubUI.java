/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStopContractTemplate.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unieductr.base.entity.EduCtrStopVersionTemplateData;

/**
 * @author azhebko
 * @since 01.09.2014
 */
@Input(@Bind(key = UIPresenter.PUBLISHER_ID, binding = "versionTemplateData.id", required = true))
public class EduCtrStopContractTemplatePubUI extends UIPresenter
{
    private EduCtrStopVersionTemplateData _versionTemplateData = new EduCtrStopVersionTemplateData();
    public EduCtrStopVersionTemplateData getVersionTemplateData() { return _versionTemplateData; }

    @Override
    public void onComponentRefresh()
    {
        _versionTemplateData = IUniBaseDao.instance.get().getNotNull(EduCtrStopVersionTemplateData.class, getVersionTemplateData().getId());
    }
}