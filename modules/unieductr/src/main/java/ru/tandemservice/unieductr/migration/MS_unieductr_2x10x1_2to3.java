package ru.tandemservice.unieductr.migration;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.*;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.ctr.base.util.CtrMigrationUtil;

import java.sql.Blob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unieductr_2x10x1_2to3 extends IndependentMigrationScript
{
    // CtrPrintTemplateCodes
    /** Константа кода (code) элемента : Основной (title) */
    private String EDU_CONTRACT_TEMPLATE_VO_2_SIDES = "edu.ctr.template.vo.2s";
    /** Константа кода (code) элемента : Основной (title) */
    private String EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON = "edu.ctr.template.vo.3sp";
    /** Константа кода (code) элемента : Основной (title) */
    private String EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG = "edu.ctr.template.vo.3so";
    /** Константа кода (code) элемента : Основной (title) */
    private String EDU_CONTRACT_TEMPLATE_SPO_2_SIDES = "edu.ctr.template.spo.2s";
    /** Константа кода (code) элемента : Основной (title) */
    private String EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_PERSON = "edu.ctr.template.spo.3sp";
    /** Константа кода (code) элемента : Основной (title) */
    private String EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_ORG = "edu.ctr.template.spo.3so";
    /** Константа кода (code) элемента : Основной (title) */
    private String TERM_EDU_COST_TEMPLATE_2_SIDES = "edu.ctr.template.termeducost.2s";
    /** Константа кода (code) элемента : Основной (title) */
    private String TERM_EDU_COST_TEMPLATE_3_SIDES_PERSON = "edu.ctr.template.termeducost.3sp";
    /** Константа кода (code) элемента : Основной (title) */
    private String TERM_EDU_COST_TEMPLATE_3_SIDES_ORG = "edu.ctr.template.termeducost.3so";
    /** Константа кода (code) элемента : Основной (title) */
    private String PROGRAM_CHANGE_TEMPLATE_2_SIDES = "edu.ctr.template.programchange.2s";
    /** Константа кода (code) элемента : Основной (title) */
    private String PROGRAM_CHANGE_TEMPLATE_3_SIDES_PERSON = "edu.ctr.template.programchange.3sp";
    /** Константа кода (code) элемента : Основной (title) */
    private String PROGRAM_CHANGE_TEMPLATE_3_SIDES_ORG = "edu.ctr.template.programchange.3so";
    /** Константа кода (code) элемента : Основной (title) */
    private String EDU_CTR_STOP_TEMPLATE_CONTRACT = "edu.ctr.template.stopcontract";

    // CtrContractKindCodes
    /** Константа кода (code) элемента : Договор на обучение по ОП ВО (двухсторонний) (title) */
    private String EDU_CONTRACT_VO_2_SIDES = "edu.vo.2s";
    /** Константа кода (code) элемента : Договор на обучение по ОП ВО (трехсторонний с физ. лицом) (title) */
    private String EDU_CONTRACT_VO_3_SIDES_PERSON = "edu.vo.3sp";
    /** Константа кода (code) элемента : Договор на обучение по ОП ВО (трехсторонний с юр. лицом) (title) */
    private String EDU_CONTRACT_VO_3_SIDES_ORG = "edu.vo.3so";
    /** Константа кода (code) элемента : Договор на обучение по ОП СПО (двухсторонний) (title) */
    private String EDU_CONTRACT_SPO_2_SIDES = "edu.spo.2s";
    /** Константа кода (code) элемента : Договор на обучение по ОП СПО (трехсторонний с физ. лицом) (title) */
    private String EDU_CONTRACT_SPO_3_SIDES_PERSON = "edu.spo.3sp";
    /** Константа кода (code) элемента : Договор на обучение по ОП СПО (трехсторонний с юр. лицом) (title) */
    private String EDU_CONTRACT_SPO_3_SIDES_ORG = "edu.spo.3so";
    /** Константа кода (code) элемента : Доп. соглашение об установлении стоимости обучения за семестр (двухсторонний) (title) */
    private String TERM_EDU_COST_2_SIDES = "edu.termeducost.2s";
    /** Константа кода (code) элемента : Доп. соглашение об установлении стоимости обучения за семестр (трехсторонний с физ. лицом) (title) */
    private String TERM_EDU_COST_3_SIDES_PERSON = "edu.termeducost.3sp";
    /** Константа кода (code) элемента : Доп. соглашение об установлении стоимости обучения за семестр (трехсторонний с юр. лицом) (title) */
    private String TERM_EDU_COST_3_SIDES_ORG = "edu.termeducost.3so";
    /** Константа кода (code) элемента : Доп. соглашение об изменении ОП (двухсторонний) (title) */
    private String PROGRAM_CHANGE_2_SIDES = "edu.programchange.2s";
    /** Константа кода (code) элемента : Доп. соглашение об изменении ОП (трехсторонний с физ. лицом) (title) */
    private String PROGRAM_CHANGE_3_SIDES_PERSON = "edu.programchange.3sp";
    /** Константа кода (code) элемента : Доп. соглашение об изменении ОП (трехсторонний с юр. лицом) (title) */
    private String PROGRAM_CHANGE_3_SIDES_ORG = "edu.programchange.3so";
    /** Константа кода (code) элемента : Доп. соглашение на расторжение договоров (title) */
    private String EDU_CTR_STOP_CONTRACT = "edu.ctrstopcontract";

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1")
                };
    }


    @Override
    public void run(DBTool tool) throws Exception
    {
        // Получаем уже существующие элементы ctrcontractkind_t
        Map<String, Long> contractKindMap = MigrationUtils.getCatalogCode2IdMap(tool, "ctrcontractkind_t");

        // Получаем уже существующие элементы ctrPrintTemplate
        final Map<String, Long> ctrPrintTemplateMap = Maps.newHashMap();

        SQLSelectQuery selectCtrPrintTemplateQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_c_prnt_tmpl_t", "pt").innerJoin(SQLFrom.table("scriptitem_t", "si"), "si.id=pt.id"))
                .column("si.id")
                .column("si.code_p");

        List<Object[]> ctrPrintTemplates = tool.executeQuery(
                MigrationUtils.processor(Long.class, String.class),
                tool.getDialect().getSQLTranslator().toSql(selectCtrPrintTemplateQuery));

        for(Object[] obj : ctrPrintTemplates)
        {
            ctrPrintTemplateMap.put((String) obj[1], (Long) obj[0]);
        }

        fillCatalogs(ctrPrintTemplateMap, contractKindMap, tool);
        clearCatalogs(tool);

        ////////////////////////////////////////////////////////////////////////////////
        // сущность ctrContractVersion

        // заполняем printTemplate и kind

        SQLSelectQuery selectContractVersionQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv")).column("id")
                .where("kind_id is null or printtemplate_id is null");

        List<Object[]> contractVersionList = tool.executeQuery(
                MigrationUtils.processor(Long.class),
                tool.getDialect().getSQLTranslator().toSql(selectContractVersionQuery));

        for (Object[] contractVersion : contractVersionList)
        {
            setCtrPrintTemplate((Long) contractVersion[0], ctrPrintTemplateMap, contractKindMap, tool);
        }

        // Проверяем, что нет версий без вида, если нет, то делаем колонку обязательной
        SQLSelectQuery selectCtrVerQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv")).column("id")
                .where("kind_id is null");

        List<Object[]> ctrVerList = tool.executeQuery(
                MigrationUtils.processor(Long.class),
                tool.getDialect().getSQLTranslator().toSql(selectContractVersionQuery));

        if (ctrVerList.isEmpty())
        {
            tool.setColumnNullable("ctr_contractver_t", "kind_id", false);
        }
    }

    private void setCtrPrintTemplate(Long contractVersionId, Map<String, Long> ctrPrintTemplateMap, Map<String, Long> contractKindMap, DBTool tool) throws SQLException
    {
        SQLUpdateQuery updateQuery = new SQLUpdateQuery("ctr_contractver_t")
                .set("kind_id", "?")
                .set("printtemplate_id", "?")
                .where("id=?");

        // пропускаем если не "Договор на обучение ВО" ("01.01.vpo") или не импортированные договора ВО ("01.01")
        if(CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "eductr_stduent_c_ctmpldt_t", tool) && (CtrMigrationUtil.checkContractType(contractVersionId, "01.01.vpo", tool) || CtrMigrationUtil.checkContractType(contractVersionId, "01.01", tool)))
        {
            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery),
                    getKind(contractVersionId,
                            EDU_CONTRACT_VO_2_SIDES,
                            EDU_CONTRACT_VO_3_SIDES_PERSON,
                            EDU_CONTRACT_VO_3_SIDES_ORG,
                            contractKindMap, tool),
                    getPrintTemplate(contractVersionId,
                            EDU_CONTRACT_TEMPLATE_VO_2_SIDES,
                            EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON,
                            EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG,
                            ctrPrintTemplateMap, tool),
                    contractVersionId);
            return;
        }

        // пропускаем если не "Договор на обучение СПО" ("01.01.spo")
        if(CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "eductr_stduent_spo_ctmpldt_t", tool) && CtrMigrationUtil.checkContractType(contractVersionId, "01.01.spo", tool))
        {
            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery),
                    getKind(contractVersionId,
                            EDU_CONTRACT_SPO_2_SIDES,
                            EDU_CONTRACT_SPO_3_SIDES_PERSON,
                            EDU_CONTRACT_SPO_3_SIDES_ORG,
                            contractKindMap, tool),
                    getPrintTemplate(contractVersionId,
                            EDU_CONTRACT_TEMPLATE_SPO_2_SIDES,
                            EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_PERSON,
                            EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_ORG,
                            ctrPrintTemplateMap, tool),
                    contractVersionId);
            return;
        }

        if(CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "eductr_progchange_a_ctmplt_t",tool))
        {
            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery),
                    getKind(contractVersionId,
                            PROGRAM_CHANGE_2_SIDES,
                            PROGRAM_CHANGE_3_SIDES_PERSON,
                            PROGRAM_CHANGE_3_SIDES_ORG,
                            contractKindMap, tool),
                    getPrintTemplate(contractVersionId,
                            PROGRAM_CHANGE_TEMPLATE_2_SIDES,
                            PROGRAM_CHANGE_TEMPLATE_3_SIDES_PERSON,
                            PROGRAM_CHANGE_TEMPLATE_3_SIDES_ORG,
                            ctrPrintTemplateMap, tool),
                    contractVersionId);
            return;
        }

        if(CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "eductr_termeducost_a_ctmplt_t", tool))
        {
            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery),
                    getKind(contractVersionId,
                            TERM_EDU_COST_2_SIDES,
                            TERM_EDU_COST_3_SIDES_PERSON,
                            TERM_EDU_COST_3_SIDES_ORG,
                            contractKindMap, tool),
                    getPrintTemplate(contractVersionId,
                            TERM_EDU_COST_TEMPLATE_2_SIDES,
                            TERM_EDU_COST_TEMPLATE_3_SIDES_PERSON,
                            TERM_EDU_COST_TEMPLATE_3_SIDES_ORG,
                            ctrPrintTemplateMap, tool),
                    contractVersionId);
            return;
        }

        if(CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "eductr_stop_ctmpldt_t", tool))
        {
            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery),
                    contractKindMap.get(EDU_CTR_STOP_CONTRACT),
                    ctrPrintTemplateMap.get(EDU_CTR_STOP_TEMPLATE_CONTRACT),
                    contractVersionId);
        }
    }

    private Long getKind(Long contractVersionId, String twoSide, String threeSidePerson, String threeSideOrg, Map<String, Long> contractKindMap, DBTool tool) throws SQLException
    {
        Long ctrKind;

        if(checkTwoSides(contractVersionId, tool))
        {
            ctrKind = contractKindMap.get(twoSide);
        }
        else
        {
            if(checkPersonSide(contractVersionId, tool))
            {
                ctrKind = contractKindMap.get(threeSidePerson);
            }
            else
            {
                ctrKind = contractKindMap.get(threeSideOrg);
            }
        }

        return ctrKind;
    }


    private Long getPrintTemplate(Long contractVersionId, String twoSide, String threeSidePerson, String threeSideOrg, Map<String, Long> ctrPrintTemplateMap, DBTool tool) throws SQLException
    {
        Long ctrPrintTemplate;

        if(checkTwoSides(contractVersionId, tool))
        {
            ctrPrintTemplate = ctrPrintTemplateMap.get(twoSide);
        }
        else
        {
            if(checkPersonSide(contractVersionId, tool))
            {
                ctrPrintTemplate = ctrPrintTemplateMap.get(threeSidePerson);
            }
            else
            {
                ctrPrintTemplate = ctrPrintTemplateMap.get(threeSideOrg);
            }
        }

        return ctrPrintTemplate;
    }

    private boolean checkTwoSides(Long contractVersionId, DBTool tool) throws SQLException
    {
        ISQLTranslator translator = tool.getDialect().getSQLTranslator();
        SQLSelectQuery contractorQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractor_role_t", "cr")
                .innerJoin(SQLFrom.table("ctr_c_contract_role_t", "r"), "r.id=cr.role_id")
                .innerJoin(SQLFrom.table("ctr_contractor_t", "c"), "c.id=cr.contactor_id")
                .innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=c.contactor_id")
        )
                .column("pr.person_id")
                .where("c.owner_id=?")
                .where("r.code_p=?");

        // CtrContractRole
        /** Константа кода (code) элемента : Заказчик (title) */
        String EDU_CONTRACT_CUSTOMER = "01.01.customer";
        /** Константа кода (code) элемента : Исполнитель (title) */
        String EDU_CONTRACT_PROVIDER = "01.01.provider";

        Long providerPerson = (Long) tool.getUniqueResult(translator.toSql(contractorQuery), contractVersionId, EDU_CONTRACT_PROVIDER);
        Long customerPerson = (Long) tool.getUniqueResult(translator.toSql(contractorQuery), contractVersionId, EDU_CONTRACT_CUSTOMER);

        SQLSelectQuery studentPersonQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv").innerJoin(SQLFrom.table("eductr_student_contract_t", "sc"), "sc.contractobject_id=cv.contract_id") .innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=sc.student_id")).column("pr.person_id")
                .where("cv.id=?");

        Long studentPerson = (Long) tool.getUniqueResult(translator.toSql(studentPersonQuery), contractVersionId);

        if(studentPerson == null)
        {
            return true;
        }

        return Objects.equals(studentPerson, providerPerson) || Objects.equals(studentPerson, customerPerson);
    }

    private boolean checkPersonSide(Long contractVersionId, DBTool tool) throws SQLException
    {
        // Проверяем по наличию юр. лица

        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        SQLSelectQuery contractorQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractor_role_t", "cr")
                .innerJoin(SQLFrom.table("ctr_c_contract_role_t", "r"), "r.id=cr.role_id")
                .innerJoin(SQLFrom.table("ctr_contractor_t", "c"), "c.id=cr.contactor_id")
                .innerJoin(SQLFrom.table("juridicalcontactor_t", "jc"), "jc.id=c.contactor_id")
        )
                .where("c.owner_id=?")
                .column("jc.id");

        List<Object[]> juridicalContator = tool.executeQuery(MigrationUtils.processor(Long.class), translator.toSql(contractorQuery), contractVersionId);

        return juridicalContator.isEmpty();
    }

    private void addCtrPrintTemplate(Long id, String code, String templatePath, String scriptPath, Object[] templateScriptItem, short ctrPrintTemplateCode, Long kindId, String title, String shortTitle, DBTool tool, ISQLTranslator translator) throws SQLException
    {
        SQLInsertQuery insertScriptItemQuery = new SQLInsertQuery("scriptitem_t")
                .set("id", "?")
                .set("discriminator", "?")
                .set("code_p", "?")
                .set("catalogcode_p", "?")
                .set("templatepath_p", "?")
                .set("scriptpath_p", "?")
                .set("usertemplate_p", "?")
                .set("usertemplateeditdate_p", "?")
                .set("usertemplatecomment_p", "?")
                .set("userscript_p", "?")
                .set("defaultscript_p", "?")
                .set("userscripteditdate_p", "?")
                .set("usescript_p", "?")
                .set("title_p", "?");

        SQLInsertQuery insertScriptItemNoBlobQuery = new SQLInsertQuery("scriptitem_t")
                .set("id", "?")
                .set("discriminator", "?")
                .set("code_p", "?")
                .set("catalogcode_p", "?")
                .set("templatepath_p", "?")
                .set("scriptpath_p", "?")
//                .set("usertemplate_p", "?")
                .set("usertemplateeditdate_p", "?")
                .set("usertemplatecomment_p", "?")
                .set("userscript_p", "?")
                .set("defaultscript_p", "?")
                .set("userscripteditdate_p", "?")
                .set("usescript_p", "?")
                .set("title_p", "?");

        SQLInsertQuery insertCtrPrintTemplateQuery = new SQLInsertQuery("ctr_c_prnt_tmpl_t")
                .set("id", "?")
                .set("shorttitle_p", "?")
                .set("kind_id", "?");

        String catalogCode = "ctrPrintTemplate";

        Blob userTemplate;
        Timestamp userTemplateEditDate;
        String userTemplateComment;
        String userScript;
        String defaultScript;
        Timestamp userScriptEditDate;
        Boolean useScript;

        if(templateScriptItem != null)
        {
            userTemplate = (Blob) templateScriptItem[2];
            userTemplateEditDate = (Timestamp) templateScriptItem[3];
            userTemplateComment = (String) templateScriptItem[4];
            userScript = (String) templateScriptItem[5];
            defaultScript = (String) templateScriptItem[6];
            userScriptEditDate = (Timestamp) templateScriptItem[7];
            useScript = (Boolean) templateScriptItem[8];
            if(useScript == null) useScript = false;
        }
        else
        {
            userTemplate = null;
            userTemplateEditDate = null;
            userTemplateComment = null;
            userScript = null;
            defaultScript = null;
            userScriptEditDate = null;
            useScript = false;
        }

        if(userTemplate != null)
        {
            tool.executeUpdate(translator.toSql(insertScriptItemQuery), id, ctrPrintTemplateCode, code, catalogCode, templatePath, scriptPath, userTemplate, userTemplateEditDate, userTemplateComment, userScript, defaultScript, userScriptEditDate, useScript, title);
        }
        else
        {
            tool.executeUpdate(translator.toSql(insertScriptItemNoBlobQuery), id, ctrPrintTemplateCode, code, catalogCode, templatePath, scriptPath, userTemplateEditDate, userTemplateComment, userScript, defaultScript, userScriptEditDate, useScript, title);
        }
        tool.executeUpdate(translator.toSql(insertCtrPrintTemplateQuery), id, shortTitle, kindId);
    }

    private void fillCatalogs(Map<String, Long> ctrPrintTemplateMap, Map<String, Long> contractKindMap, DBTool tool) throws SQLException
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность ctrContractKind

        // гарантировать наличие кода сущности
        short ctrContractKindCode = tool.entityCodes().ensure("ctrContractKind");

        ////////////////////////////////////////////////////////////////////////////////
        // сущность ctrPrintTemplate

        // гарантировать наличие кода сущности
        short ctrPrintTemplateCode = tool.entityCodes().ensure("ctrPrintTemplate");


        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        ////////////////////////////////////////////////////////////////////////////////
        // заполняем справочники ctrContractKind ctrcontractkind_t

        SQLInsertQuery insertCtrContractKindQuery = new SQLInsertQuery("ctrcontractkind_t")
                .set("id", "?")
                .set("discriminator", "?")
                .set("code_p", "?")
                .set("title_p", "?")
                .set("shorttitle_p", "?")
                .set("contract_p", "?");



        // code - "edu.vo.2s", title - "Договор на обучение по ОП ВО (двухсторонний)", shortTitle - "Договор ОП ВО (2х)", contract - "true"
        Long eduVo2sId;
        if(contractKindMap.containsKey(EDU_CONTRACT_VO_2_SIDES))
            eduVo2sId = contractKindMap.get(EDU_CONTRACT_VO_2_SIDES);
        else
        {
            eduVo2sId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(EDU_CONTRACT_VO_2_SIDES, eduVo2sId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduVo2sId, ctrContractKindCode, "edu.vo.2s", "Договор на обучение по ОП ВО (двухсторонний)", "Договор ОП ВО (2х)", true);
        }

        // code - "edu.vo.3sp", title - "Договор на обучение по ОП ВО (трехсторонний с физ. лицом)", shortTitle - "Договор ОП ВО (3х с физ.л.)", contract - "true"
        Long eduVo3spId;
        if(contractKindMap.containsKey(EDU_CONTRACT_VO_3_SIDES_PERSON))
            eduVo3spId = contractKindMap.get(EDU_CONTRACT_VO_3_SIDES_PERSON);
        else
        {
            eduVo3spId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(EDU_CONTRACT_VO_3_SIDES_PERSON, eduVo3spId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduVo3spId, ctrContractKindCode, "edu.vo.3sp", "Договор на обучение по ОП ВО (трехсторонний с физ. лицом)", "Договор ОП ВО (3х с физ.л.)", true);
        }

        // code - "edu.vo.3so", title - "Договор на обучение по ОП ВО (трехсторонний с юр. лицом)", shortTitle - "Договор ОП ВО (3х с юр.л.)", contract - "true"
        Long eduVo3soId;
        if(contractKindMap.containsKey(EDU_CONTRACT_VO_3_SIDES_ORG))
            eduVo3soId = contractKindMap.get(EDU_CONTRACT_VO_3_SIDES_ORG);
        else
        {
            eduVo3soId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(EDU_CONTRACT_VO_3_SIDES_ORG, eduVo3soId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduVo3soId, ctrContractKindCode, "edu.vo.3so", "Договор на обучение по ОП ВО (трехсторонний с юр. лицом)", "Договор ОП ВО (3х с юр.л.)", true);
        }

        // code - "edu.spo.2s", title - "Договор на обучение по ОП СПО (двухсторонний)", shortTitle - "Договор ОП СПО (2х)", contract - "true"
        Long eduSpo2sId;
        if(contractKindMap.containsKey(EDU_CONTRACT_SPO_2_SIDES))
            eduSpo2sId = contractKindMap.get(EDU_CONTRACT_SPO_2_SIDES);
        else
        {
            eduSpo2sId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(EDU_CONTRACT_SPO_2_SIDES, eduSpo2sId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduSpo2sId, ctrContractKindCode, "edu.spo.2s", "Договор на обучение по ОП СПО (двухсторонний)", "Договор ОП СПО (2х)", true);
        }

        // code - "edu.spo.3sp", title - "Договор на обучение по ОП СПО (трехсторонний с физ. лицом)", shortTitle - "Договор ОП СПО (3х с физ.л.)", contract - "true"
        Long eduSpo3spId;
        if(contractKindMap.containsKey(EDU_CONTRACT_SPO_3_SIDES_PERSON))
            eduSpo3spId = contractKindMap.get(EDU_CONTRACT_SPO_3_SIDES_PERSON);
        else
        {
            eduSpo3spId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(EDU_CONTRACT_SPO_3_SIDES_PERSON, eduSpo3spId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduSpo3spId, ctrContractKindCode, "edu.spo.3sp", "Договор на обучение по ОП СПО (трехсторонний с физ. лицом)", "Договор ОП СПО (3х с физ.л.)", true);
        }

        // code - "edu.spo.3so", title - "Договор на обучение по ОП СПО (трехсторонний с юр. лицом)", shortTitle - "Договор ОП СПО (3х с юр.л.)", contract - "true"
        Long eduSpo3soId;
        if(contractKindMap.containsKey(EDU_CONTRACT_SPO_3_SIDES_ORG))
            eduSpo3soId = contractKindMap.get(EDU_CONTRACT_SPO_3_SIDES_ORG);
        else
        {
            eduSpo3soId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(EDU_CONTRACT_SPO_3_SIDES_ORG, eduSpo3soId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduSpo3soId, ctrContractKindCode, "edu.spo.3so", "Договор на обучение по ОП СПО (трехсторонний с юр. лицом)", "Договор ОП СПО (3х с юр.л.)", true);
        }

        // доп. соглашения
        // code - "edu.termeducost.2s", title - "Доп. соглашение об установлении стоимости обучения за семестр (двухсторонний)", shortTitle - "Доп.согл. об уст. ст-сти (2х)", contract - "false"
        Long eduTermeducost2sId;
        if(contractKindMap.containsKey(TERM_EDU_COST_2_SIDES))
            eduTermeducost2sId = contractKindMap.get(TERM_EDU_COST_2_SIDES);
        else
        {
            eduTermeducost2sId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(TERM_EDU_COST_2_SIDES, eduTermeducost2sId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduTermeducost2sId, ctrContractKindCode, "edu.termeducost.2s", "Доп. соглашение об установлении стоимости обучения за семестр (двухсторонний)", "Доп.согл. об уст. ст-сти (2х)", false);
        }

        // code - "edu.termeducost.3sp", title - "Доп. соглашение об установлении стоимости обучения за семестр (трехсторонний с физ. лицом)", shortTitle - "Доп.согл. об уст. ст-сти (3х с физ.л.)", contract - "false"
        Long eduTermeducost3spId;
        if(contractKindMap.containsKey(TERM_EDU_COST_3_SIDES_PERSON))
            eduTermeducost3spId = contractKindMap.get(TERM_EDU_COST_3_SIDES_PERSON);
        else
        {
            eduTermeducost3spId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(TERM_EDU_COST_3_SIDES_PERSON, eduTermeducost3spId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduTermeducost3spId, ctrContractKindCode, "edu.termeducost.3sp", "Доп. соглашение об установлении стоимости обучения за семестр (трехсторонний с физ. лицом)", "Доп.согл. об уст. ст-сти (3х с физ.л.)", false);
        }

        // code - "edu.termeducost.3so", title - "Доп. соглашение об установлении стоимости обучения за семестр (трехсторонний с юр. лицом)", shortTitle - "Доп.согл. об уст. ст-сти (3х с юр.л.)", contract - "false"
        Long eduTermeducost3soId;
        if(contractKindMap.containsKey(TERM_EDU_COST_3_SIDES_ORG))
            eduTermeducost3soId = contractKindMap.get(TERM_EDU_COST_3_SIDES_ORG);
        else
        {
            eduTermeducost3soId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(TERM_EDU_COST_3_SIDES_ORG, eduTermeducost3soId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduTermeducost3soId, ctrContractKindCode, "edu.termeducost.3so", "Доп. соглашение об установлении стоимости обучения за семестр (трехсторонний с юр. лицом)", "Доп.согл. об уст. ст-сти (3х с юр.л.)", false);
        }

        // code - "edu.programchange.2s", title - "Доп. соглашение об изменении ОП (двухсторонний)", shortTitle - "Доп.согл. об изм. ОП (2х)", contract - "false"
        Long eduProgramchange2sId;
        if(contractKindMap.containsKey(PROGRAM_CHANGE_2_SIDES))
            eduProgramchange2sId = contractKindMap.get(PROGRAM_CHANGE_2_SIDES);
        else
        {
            eduProgramchange2sId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(PROGRAM_CHANGE_2_SIDES, eduProgramchange2sId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduProgramchange2sId, ctrContractKindCode, "edu.programchange.2s", "Доп. соглашение об изменении ОП (двухсторонний)", "Доп.согл. об изм. ОП (2х)", false);
        }

        // code - "edu.programchange.3sp", title - "Доп. соглашение об изменении ОП (трехсторонний с физ. лицом)", shortTitle - "Доп.согл. об изм. ОП (3х с физ.л.)", contract - "false"
        Long eduProgramchange3spId;
        if(contractKindMap.containsKey(PROGRAM_CHANGE_3_SIDES_PERSON))
            eduProgramchange3spId = contractKindMap.get(PROGRAM_CHANGE_3_SIDES_PERSON);
        else
        {
            eduProgramchange3spId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(PROGRAM_CHANGE_3_SIDES_PERSON, eduProgramchange3spId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduProgramchange3spId, ctrContractKindCode, "edu.programchange.3sp", "Доп. соглашение об изменении ОП (трехсторонний с физ. лицом)", "Доп.согл. об изм. ОП (3х с физ.л.)", false);
        }

        // code - "edu.programchange.3so", title - "Доп. соглашение об изменении ОП (трехсторонний с юр. лицом)", shortTitle - "Доп.согл. об изм. ОП (3х с юр.л.)", contract - "false"
        Long eduProgramchange3soId;
        if(contractKindMap.containsKey(PROGRAM_CHANGE_3_SIDES_ORG))
            eduProgramchange3soId = contractKindMap.get(PROGRAM_CHANGE_3_SIDES_ORG);
        else
        {
            eduProgramchange3soId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(PROGRAM_CHANGE_3_SIDES_ORG, eduProgramchange3soId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduProgramchange3soId, ctrContractKindCode, "edu.programchange.3so", "Доп. соглашение об изменении ОП (трехсторонний с юр. лицом)", "Доп.согл. об изм. ОП (3х с юр.л.)", false);
        }

        // code - "edu.ctrstopcontract", title - "Доп. соглашение на расторжение договоров", shortTitle - "Доп. согл. о расторжении", contract - "false"
        Long eduCtrstopcontractId;
        if(contractKindMap.containsKey(EDU_CTR_STOP_CONTRACT))
            eduCtrstopcontractId = contractKindMap.get(EDU_CTR_STOP_CONTRACT);
        else
        {
            eduCtrstopcontractId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(EDU_CTR_STOP_CONTRACT, eduCtrstopcontractId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduCtrstopcontractId, ctrContractKindCode, "edu.ctrstopcontract", "Доп. соглашение на расторжение договоров", "Доп. согл. о расторжении", false);
        }

        ////////////////////////////////////////////////////////////////////////////////
        // заполняем справочники ctrPrintTemplate ctr_c_prnt_tmpl_t

        // Получаем скрипты ctrTemplateScriptItem которые мигрируем
        SQLSelectQuery selectСtrTemplateScriptItemQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_c_tmpl_script_t", "ts").innerJoin(SQLFrom.table("scriptitem_t", "si"), "si.id=ts.id"))
                .column("si.id")
                .column("si.code_p")
                .column("si.usertemplate_p")
                .column("si.usertemplateeditdate_p")
                .column("si.usertemplatecomment_p")
                .column("si.userscript_p")
                .column("si.defaultscript_p")
                .column("si.userscripteditdate_p")
                .column("si.usescript_p");

        List<Object[]> ctrTemplateScriptItems = tool.executeQuery(
                MigrationUtils.processor(Long.class, String.class, Blob.class, Timestamp.class, String.class, String.class, String.class, Timestamp.class, Boolean.class),
                translator.toSql(selectСtrTemplateScriptItemQuery));

        final Map<String, Object[]> ctrTemplateScriptItemMap = Maps.newHashMap();
        for(Object[] obj : ctrTemplateScriptItems)
        {
            ctrTemplateScriptItemMap.put((String) obj[1], obj);
        }

        tool.dropConstraint("scriptitem_t", "chk_class_scriptitem");

        String ctrPrintTemplateTitle = "Основной";
        String ctrPrintTemplateShortTitle = "осн.";

        // code - "edu.ctr.template.vo.2s",
        // templatePath - "unieductr/templates/ctr/EduCtrContractTemplateVO2Sides.rtf"
        // scriptPath - "unieductr/script/ctr/EduCtrContractTemplateVOPrintScript.groovy"
        // kind - "edu.vo.2s"

        if(!ctrPrintTemplateMap.containsKey(EDU_CONTRACT_TEMPLATE_VO_2_SIDES))
        {
            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = EDU_CONTRACT_TEMPLATE_VO_2_SIDES;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "unieductr/templates/ctr/EduCtrContractTemplateVO2Sides.rtf";
            String scriptPath = "unieductr/script/ctr/EduCtrContractTemplateVOPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("edu.ctrtemplate.2s");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, eduVo2sId, ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "edu.ctr.template.vo.3sp",
        // templatePath - "unieductr/templates/ctr/EduCtrContractTemplateVO3SidesPerson.rtf"
        // scriptPath - "unieductr/script/ctr/EduCtrContractTemplateVOPrintScript.groovy"
        // kind - "edu.vo.3sp"

        if(!ctrPrintTemplateMap.containsKey(EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON))
        {
            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "unieductr/templates/ctr/EduCtrContractTemplateVO3SidesPerson.rtf";
            String scriptPath = "unieductr/script/ctr/EduCtrContractTemplateVOPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("edu.ctrtemplate.3sp");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, eduVo3spId, ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "edu.ctr.template.vo.3so",
        // templatePath - "unieductr/templates/ctr/EduCtrContractTemplateVO3SidesOrg.rtf"
        // scriptPath - "unieductr/script/ctr/EduCtrContractTemplateVOPrintScript.groovy"
        // kind - "edu.vo.3so"

        if(!ctrPrintTemplateMap.containsKey(EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG))
        {
            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "unieductr/templates/ctr/EduCtrContractTemplateVO3SidesPerson.rtf";
            String scriptPath = "unieductr/script/ctr/EduCtrContractTemplateVOPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("edu.ctrtemplate.3so");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, eduVo3soId, ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "edu.ctr.template.spo.2s",
        // templatePath - "unieductr/templates/ctr/EduCtrContractTemplateSpo2Sides.rtf"
        // scriptPath - "unieductr/script/ctr/EduCtrContractTemplateSpoPrintScript.groovy"
        // kind - "edu.spo.2s"

        if(!ctrPrintTemplateMap.containsKey(EDU_CONTRACT_TEMPLATE_SPO_2_SIDES))
        {
            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = EDU_CONTRACT_TEMPLATE_SPO_2_SIDES;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "unieductr/templates/ctr/EduCtrContractTemplateSpo2Sides.rtf";
            String scriptPath = "unieductr/script/ctr/EduCtrContractTemplateSpoPrintScript.groovy";

            addCtrPrintTemplate(id, code, templatePath, scriptPath, null, ctrPrintTemplateCode, eduSpo2sId, ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "edu.ctr.template.spo.3sp",
        // templatePath - "unieductr/templates/ctr/EduCtrContractTemplateSpo3SidesPerson.rtf"
        // scriptPath - "unieductr/script/ctr/EduCtrContractTemplateSpoPrintScript.groovy"
        // kind - "edu.spo.3sp"

        if(!ctrPrintTemplateMap.containsKey(EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_PERSON))
        {
            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_PERSON;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "unieductr/templates/ctr/EduCtrContractTemplateSpo3SidesPerson.rtf";
            String scriptPath = "unieductr/script/ctr/EduCtrContractTemplateSpoPrintScript.groovy";

            addCtrPrintTemplate(id, code, templatePath, scriptPath, null, ctrPrintTemplateCode, eduSpo3spId, ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "edu.ctr.template.spo.3so",
        // templatePath - "unieductr/templates/ctr/EduCtrContractTemplateSpo3SidesOrg.rtf"
        // scriptPath - "unieductr/script/ctr/EduCtrContractTemplateSpoPrintScript.groovy"
        // kind - "edu.spo.3so"

        if(!ctrPrintTemplateMap.containsKey(EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_ORG))
        {
            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_ORG;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "unieductr/templates/ctr/EduCtrContractTemplateSpo3SidesOrg.rtf";
            String scriptPath = "unieductr/script/ctr/EduCtrContractTemplateSpoPrintScript.groovy";

            addCtrPrintTemplate(id, code, templatePath, scriptPath, null, ctrPrintTemplateCode, eduSpo3soId, ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "edu.ctr.template.termeducost.2s",
        // templatePath - "unieductr/templates/ctr/EduCtrTermEduCostTemplate2Sides.rtf"
        // scriptPath - "unieductr/script/ctr/EduCtrTermEduCostAgreementTemplatePrintScript.groovy"
        // kind - "edu.termeducost.2s"

        if(!ctrPrintTemplateMap.containsKey(TERM_EDU_COST_TEMPLATE_2_SIDES))
        {
            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = TERM_EDU_COST_TEMPLATE_2_SIDES;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "unieductr/templates/ctr/EduCtrTermEduCostTemplate2Sides.rtf";
            String scriptPath = "unieductr/script/ctr/EduCtrTermEduCostAgreementTemplatePrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("edu.termeducosttemplate.2s");

            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, eduTermeducost2sId, ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "edu.ctr.template.termeducost.3sp",
        // templatePath - "unieductr/templates/ctr/EduCtrTermEduCostTemplate3SidesPerson.rtf"
        // scriptPath - "unieductr/script/ctr/EduCtrTermEduCostAgreementTemplatePrintScript.groovy"
        // kind - "edu.termeducost.3sp"

        if(!ctrPrintTemplateMap.containsKey(TERM_EDU_COST_TEMPLATE_3_SIDES_PERSON))
        {
            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = TERM_EDU_COST_TEMPLATE_3_SIDES_PERSON;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "unieductr/templates/ctr/EduCtrTermEduCostTemplate3SidesPerson.rtf";
            String scriptPath = "unieductr/script/ctr/EduCtrTermEduCostAgreementTemplatePrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("edu.termeducosttemplate.3sp");

            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, eduTermeducost3spId, ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "edu.ctr.template.termeducost.3so",
        // templatePath - "unieductr/templates/ctr/EduCtrTermEduCostTemplate3SidesOrg.rtf"
        // scriptPath - "unieductr/script/ctr/EduCtrTermEduCostAgreementTemplatePrintScript.groovy"
        // kind - "edu.termeducost.3so"

        if(!ctrPrintTemplateMap.containsKey(TERM_EDU_COST_TEMPLATE_3_SIDES_ORG))
        {
            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = TERM_EDU_COST_TEMPLATE_3_SIDES_ORG;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "unieductr/templates/ctr/EduCtrTermEduCostTemplate3SidesOrg.rtf";
            String scriptPath = "unieductr/script/ctr/EduCtrTermEduCostAgreementTemplatePrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("edu.termeducosttemplate.3so");

            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, eduTermeducost3soId, ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "edu.ctr.template.programchange.2s",
        // templatePath - "unieductr/templates/ctr/EduCtrProgramChangeTemplate2Sides.rtf"
        // scriptPath - "unieductr/script/ctr/EduCtrProgramChangeAgreementTemplatePrintScript.groovy"
        // kind - "edu.programchange.2s"

        if(!ctrPrintTemplateMap.containsKey(PROGRAM_CHANGE_TEMPLATE_2_SIDES))
        {
            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = PROGRAM_CHANGE_TEMPLATE_2_SIDES;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "unieductr/templates/ctr/EduCtrProgramChangeTemplate2Sides.rtf";
            String scriptPath = "unieductr/script/ctr/EduCtrProgramChangeAgreementTemplatePrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("edu.programchangetemplate.2s");

            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, eduProgramchange2sId, ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "edu.ctr.template.programchange.3sp",
        // templatePath - "unieductr/templates/ctr/EduCtrProgramChangeTemplate3SidesPerson.rtf"
        // scriptPath - "unieductr/script/ctr/EduCtrProgramChangeAgreementTemplatePrintScript.groovy"
        // kind - "edu.programchange.3sp"

        if(!ctrPrintTemplateMap.containsKey(PROGRAM_CHANGE_TEMPLATE_3_SIDES_PERSON))
        {
            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = PROGRAM_CHANGE_TEMPLATE_3_SIDES_PERSON;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "unieductr/templates/ctr/EduCtrProgramChangeTemplate3SidesPerson.rtf";
            String scriptPath = "unieductr/script/ctr/EduCtrProgramChangeAgreementTemplatePrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("edu.programchangetemplate.3sp");

            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, eduProgramchange3spId, ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "edu.ctr.template.programchange.3so",
        // templatePath - "unieductr/templates/ctr/EduCtrProgramChangeTemplate3SidesOrg.rtf"
        // scriptPath - "unieductr/script/ctr/EduCtrProgramChangeAgreementTemplatePrintScript.groovy"
        // kind - "edu.programchange.3so"

        if(!ctrPrintTemplateMap.containsKey(PROGRAM_CHANGE_TEMPLATE_3_SIDES_ORG))
        {
            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = PROGRAM_CHANGE_TEMPLATE_3_SIDES_ORG;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "unieductr/templates/ctr/EduCtrProgramChangeTemplate3SidesOrg.rtf";
            String scriptPath = "unieductr/script/ctr/EduCtrProgramChangeAgreementTemplatePrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("edu.programchangetemplate.3so");

            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, eduProgramchange3soId, ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "edu.ctr.template.stopcontract",
        // templatePath - "unieductr/templates/ctr/EduCtrStopContract.rtf"
        // scriptPath - "unieductr/script/ctr/EduCtrStopContractPrintScript.groovy"
        // kind - "edu.ctrstopcontract"

        if(!ctrPrintTemplateMap.containsKey(EDU_CTR_STOP_TEMPLATE_CONTRACT))
        {
            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = EDU_CTR_STOP_TEMPLATE_CONTRACT;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "unieductr/templates/ctr/EduCtrStopContract.rtf";
            String scriptPath = "unieductr/script/ctr/EduCtrStopContractPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("edu.ctrstopcontract");

            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, eduCtrstopcontractId, ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }
    }

    private void clearCatalogs(DBTool tool) throws SQLException
    {
        // удаляем скрипты
        // "edu.ctrtemplate.2s" Договор на обучение по ОП (двухсторонний)
        // "edu.ctrtemplate.3sp"  Договор на обучение по ОП (трехсторонний с физ. лицом)
        // "edu.ctrtemplate.3so" Договор на обучение по ОП (трехсторонний с юр. лицом)
        // "edu.ctrtemplate.spo.2s" Договор на обучение по ОП (СПО) (двухсторонний)
        // "edu.ctrtemplate.spo.3sp" Договор на обучение по ОП (СПО) (трехсторонний с физ. лицом)
        // "edu.ctrtemplate.spo.3so" Договор на обучение по ОП (СПО) (трехсторонний с юр. лицом)
        // "edu.termeducosttemplate.2s" Доп. соглашение об установлении стоимости обучения за семестр (двухсторонний)
        // "edu.termeducosttemplate.3sp" Доп. соглашение об установлении стоимости обучения за семестр (трехсторонний с физ. лицом)
        // "edu.termeducosttemplate.3so" Доп. соглашение об установлении стоимости обучения за семестр (трехсторонний с юр. лицом)
        // "edu.programchangetemplate.2s" Доп. соглашение об изменении ОП (двухсторонний)
        // "edu.programchangetemplate.3sp" Доп. соглашение об изменении ОП (трехсторонний с физ. лицом)
        // "edu.programchangetemplate.3so" Доп. соглашение об изменении ОП (трехсторонний с юр. лицом)
        // "edu.ctrstopcontract" Доп. соглашение на расторжение договоров

        SQLSelectQuery selectScriptItemQuery = new SQLSelectQuery().from(SQLFrom.table("scriptitem_t"))
                .where("code_p in ('edu.ctrtemplate.2s', 'edu.ctrtemplate.3sp', 'edu.ctrtemplate.3so', 'edu.ctrtemplate.spo.2s', 'edu.ctrtemplate.spo.3sp', 'edu.ctrtemplate.spo.3so', 'edu.termeducosttemplate.2s', 'edu.termeducosttemplate.3sp', 'edu.termeducosttemplate.3so', 'edu.programchangetemplate.2s', 'edu.programchangetemplate.3sp', 'edu.programchangetemplate.3so', 'edu.ctrstopcontract')")
                .column("id");

        List<Object[]> scriptItemIdObjList = tool.executeQuery(
                MigrationUtils.processor(Long.class),
                tool.getDialect().getSQLTranslator().toSql(selectScriptItemQuery)); //, "(" + StringUtils.join(scriptItemCodes, ",") + ")");

        if(!scriptItemIdObjList.isEmpty())
        {
            List<Long> scriptItemIdList = scriptItemIdObjList.stream().map(a -> (Long) a[0]).collect(Collectors.toList());

            SQLDeleteQuery deleteCtrTemplateScriptQuery = new SQLDeleteQuery("ctr_c_tmpl_script_t").where("id in " + "(" + StringUtils.join(scriptItemIdList, ", ") + ")");

            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(deleteCtrTemplateScriptQuery)); // , "(" + StringUtils.join(scriptItemIdList, ", ") + ")");

            SQLDeleteQuery deleteScriptItemQuery = new SQLDeleteQuery("scriptitem_t").where("id in " + "(" + StringUtils.join(scriptItemIdList, ", ") + ")");

            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(deleteScriptItemQuery));
        }
    }
}
