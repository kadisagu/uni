/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrProgramChangeAgreementTemplate.logic;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractKindSelectWrapper;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrVersionTemplatePromiceRestrictions;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractPromice;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractKind;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.ctr.catalog.entity.CtrTemplateScriptItem;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractPrintUtils;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.ContractSides;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduCtrPaymentPromiceRestrictions;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractKindCodes;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractTypeCodes;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrProgramChangeAgreementTemplate.EduCtrProgramChangeAgreementTemplateManager;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrProgramChangeAgreementTemplate.ui.Add.EduCtrProgramChangeAgreementTemplateAdd;
import ru.tandemservice.unieductr.student.entity.EduCtrProgramChangeAgreementTemplateData;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 09.12.2014
 */
public class EduCtrProgramChangeAgreementTemplateDao extends UniBaseDao implements IEduCtrProgramChangeAgreementTemplateDao
{
    @Override
    public void doCreateNewVersion(EduCtrProgramChangeAgreementTemplateData template, CtrContractVersion prevVersion, EduProgramProf eduProgram, CtrPriceElementCost cost, Collection<CtrPriceElementCostStage> costStages, CtrContractVersionCreateData versionCreateData)
    {
        checkNotNull(template);
        checkNotNull(prevVersion);
        checkNotNull(eduProgram);
        checkNotNull(costStages);

        CtrContractVersion newVersion = CtrContractVersionManager.instance().dao().doCreateNewVersion(prevVersion, versionCreateData);

        template.setCost(cost);
        template.setOwner(newVersion);
        this.save(template);

        new DQLDeleteBuilder(CtrPaymentPromice.class)
            .where(eq(property(CtrPaymentPromice.src().owner()), value(newVersion)))
            .where(ge(property(CtrPaymentPromice.deadlineDate()), valueTimestamp(newVersion.getDocStartDate())))
            .createStatement(this.getSession()).execute();

        CtrContractVersionContractor customer = CtrContractVersionManager.instance().dao().getContactor(newVersion, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER);
        CtrContractVersionContractor provider = CtrContractVersionManager.instance().dao().getContactor(newVersion, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER);

        if (cost != null)
        {
            Collection<MultiKey> promiseKeys = new HashSet<>();
            for (CtrPaymentPromice paymentPromise: this.getList(CtrPaymentPromice.class, CtrPaymentPromice.src().owner(), newVersion))
                promiseKeys.add(paymentPromise.getPromiceLocalKey());

            // формируем список платежей (ЗАЯВИТЕЛЬ обязуется ВУЗу оплатить сумму за обучение)
            for (CtrPriceElementCostStage costStage: costStages)
            {
                CtrPaymentPromice payPromise = new CtrPaymentPromice();
                payPromise.setSourceStage(this.<CtrPriceElementCostStage>getByNaturalId(costStage.getNaturalId()));
                payPromise.setCurrency(cost.getCurrency());
                payPromise.setDeadlineDate(costStage.getDeadlineDate());
                payPromise.setSrc(customer);
                payPromise.setDst(provider);
                payPromise.setStage(costStage.getTitle());
                payPromise.setCostAsLong(costStage.getStageCostAsLong());

                if (promiseKeys.contains(payPromise.getPromiceLocalKey()))
                    throw new ApplicationException(EduCtrProgramChangeAgreementTemplateManager.instance().getProperty("actual-cost-stages-only"));

                this.save(payPromise);
            }
        }

        Collection<EduCtrEducationPromise> eduPromises = this.getList(EduCtrEducationPromise.class, EduCtrEducationPromise.src().owner(), template.getOwner());
        checkState(eduPromises.size() == 1);

        EduCtrEducationPromise eduPromise = eduPromises.iterator().next();
        eduPromise.setEduProgram(eduProgram);

        this.update(eduPromise);
    }

    @Override
    public Map<CtrContractType, List<CtrContractKindSelectWrapper>> getVersionAddComponent(Long contextEntityId)
    {
        CtrContractVersion currentVersion = null;
        IEntity context = this.getNotNull(contextEntityId);

        if (context instanceof EduCtrStudentContract)
            currentVersion = CtrContractVersionManager.instance().dao().getLastActiveVersion(((EduCtrStudentContract) context).getContractObject());

        else if (context instanceof CtrContractVersion)
            currentVersion = (CtrContractVersion) context;

        if (currentVersion == null || this.getCount(EduCtrEducationPromise.class, EduCtrEducationPromise.src().owner().s(), currentVersion) != 1 || this.getCount(EduCtrStudentContract.class, EduCtrStudentContract.contractObject().s(), currentVersion.getContract()) != 1)
            return Collections.emptyMap();

        if(CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE_V_O.equals(currentVersion.getContract().getType().getCode()))
        {
            CtrContractVersionContractor customerRel = CtrContractVersionManager.instance().dao().getContactor(currentVersion, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER);

            List<EduCtrEducationPromise> promiseList = this.getList(EduCtrEducationPromise.class, EduCtrEducationPromise.dst().owner(), currentVersion);

            EduCtrEducationPromise eduPromise = promiseList.get(0);
            ContactorPerson educationReceiver = eduPromise.getDst().getContactor();

            ContractSides contractSides = EduContractPrintUtils.getContractSides(customerRel.getContactor(), educationReceiver);

            Map<CtrContractType, List<CtrContractKindSelectWrapper>> map = Maps.newHashMap();
            CtrContractType type = this.getCatalogItem(CtrContractType.class, CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE_V_O);

            if (ContractSides.TWO_SIDES.equals(contractSides))
            {
                map.put(type, Lists.newArrayList(new CtrContractKindSelectWrapper(this.getCatalogItem(CtrContractKind.class, CtrContractKindCodes.PROGRAM_CHANGE_2_SIDES), EduCtrProgramChangeAgreementTemplateAdd.class)));
            } else if (ContractSides.THREE_SIDES_PERSON.equals(contractSides))
            {
                map.put(type, Lists.newArrayList(new CtrContractKindSelectWrapper(this.getCatalogItem(CtrContractKind.class, CtrContractKindCodes.PROGRAM_CHANGE_3_SIDES_PERSON), EduCtrProgramChangeAgreementTemplateAdd.class)));
            } else if (ContractSides.THREE_SIDES_ORG.equals(contractSides))
            {
                map.put(type, Lists.newArrayList(new CtrContractKindSelectWrapper(this.getCatalogItem(CtrContractKind.class, CtrContractKindCodes.PROGRAM_CHANGE_3_SIDES_ORG), EduCtrProgramChangeAgreementTemplateAdd.class)));
            }

            return map;
        }
        else
        {
            return Collections.emptyMap();
        }
    }

    @Override
    public ICtrVersionTemplatePromiceRestrictions getPromiceRestrictions(Class<? extends CtrContractPromice> promiceClass, CtrContractVersionTemplateData templateData)
    {
        // проверяем, что это тот самый шаблон
        if (!(templateData instanceof EduCtrProgramChangeAgreementTemplateData))
            throw new IllegalStateException();

        // для обязательств по оплате отделные разрешения
        if (CtrPaymentPromice.class.equals(promiceClass))
            return EduCtrPaymentPromiceRestrictions.get(templateData.getOwner());

        // запрещаем редактировать все, кроме обязательств по оплате
        return ICtrVersionTemplatePromiceRestrictions.DENY_ALL;
    }

    @Override
    public IDocumentRenderer print(CtrContractVersionTemplateData templateData)
    {
        if (!(templateData instanceof EduCtrProgramChangeAgreementTemplateData))
            throw new IllegalStateException();

        CtrContractVersion contractVersion = templateData.getOwner();

        // проверяем, что можем печатать по шаблону

        CtrContractVersionContractor providerRel = CtrContractVersionManager.instance().dao().getContactor(contractVersion, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER);
        if (null == providerRel)
            throw new ApplicationException("Печать по шаблону невозможна - для версии договора не указан исполнитель.");

        CtrContractVersionContractor customerRel = CtrContractVersionManager.instance().dao().getContactor(contractVersion, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER);
        if (null == customerRel)
            throw new ApplicationException("Печать по шаблону невозможна - для версии договора не указан заказчик.");

        if (!(providerRel.getContactor() instanceof EmployeePostContactor))
            throw new ApplicationException("Печать по шаблону невозможна - неизвестный тип исполнителя.");

        List<EduCtrEducationPromise> promiseList = this.getList(EduCtrEducationPromise.class, EduCtrEducationPromise.dst().owner(), contractVersion);
        if (promiseList.size() > 1)
            throw new ApplicationException("Печать по шаблону невозможна - в версии договора более одного обязательства по обучению по ОП.");

        if (promiseList.size() < 1)
            throw new ApplicationException("Печать по шаблону невозможна - в версии договора отсутствуют обязательства по обучению по ОП.");

        EduCtrEducationPromise eduPromise = promiseList.get(0);

        // печатаем по шаблону, используя скрипт
        Map<String, Object> result = CommonManager.instance().scriptDao().getScriptResult(
            contractVersion.getPrintTemplate(),
            "versionTemplateDataId", templateData.getId(),
            "customerId", customerRel.getId(),
            "providerId", providerRel.getId(),
            "eduPromiseId", eduPromise.getId());

        return (IDocumentRenderer)result.get(IScriptExecutor.RENDERER);
    }

    @Override public boolean isAllowCreateNextVersionByTemplate(CtrContractVersionTemplateData ctrContractVersionTemplateData) { return true; }

    @Override public boolean isAllowCreateNextVersionByCopy(CtrContractVersionTemplateData ctrContractVersionTemplateData) { return false; }

    @Override public boolean isAllowDeleteTemplate(CtrContractVersionTemplateData ctrContractVersionTemplateData) { return false; }
}