/**
 *$Id:$
 */
package ru.tandemservice.unieductr.reports.bo.EduCtrDebitorsPayReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unieductr.reports.bo.EduCtrDebitorsPayReport.logic.EduCtrDebitorsPayReportDao;
import ru.tandemservice.unieductr.reports.bo.EduCtrDebitorsPayReport.logic.IEduCtrDebitorsPayReportDao;

/**
 * @author Alexander Shaburov
 * @since 30.10.12
 */
@Configuration
public class EduCtrDebitorsPayReportManager extends BusinessObjectManager
{
    public static EduCtrDebitorsPayReportManager instance()
    {
        return instance(EduCtrDebitorsPayReportManager.class);
    }

    @Bean
    public IEduCtrDebitorsPayReportDao dao()
    {
        return new EduCtrDebitorsPayReportDao();
    }
}
