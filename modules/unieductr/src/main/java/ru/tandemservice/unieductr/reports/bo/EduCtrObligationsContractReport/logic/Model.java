/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrObligationsContractReport.logic;

import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 19.08.2016
 */
public class Model
{
    private Long _orgUnitId;
    private Date _formingDate = new Date();
    private Course _course;
    private List<Group> _groupList;
    private UniEduProgramEducationOrgUnitAddon _util;
    private boolean _activeCourse;
    private boolean _activeGroup;
    private boolean _activeStatus;

    private Map<Long, CtrContractVersion> _contractIdFirstVersionMap;
    private Map<String, Currency> _currencyMap;

    private List<StudentStatus> _stateList;
    private String _comment;
    private Date _deadlineDateFrom;
    private Date _deadlineDateTo;
    private Date _reportDate;

    //вычисленные результаты отчета
    private int _studentCount;
    private int _contractCount;
    private Map<String, Long> _sumResult;
    private Map<String, Long> _sumPromice;

    // Getters & Setters

    public Map<String, Currency> getCurrencyMap()
    {
        return _currencyMap;
    }

    public void setCurrencyMap(Map<String, Currency> currencyMap)
    {
        _currencyMap = currencyMap;
    }

    public Map<Long, CtrContractVersion> getContractIdFirstVersionMap()
    {
        return _contractIdFirstVersionMap;
    }

    public void setContractIdFirstVersionMap(Map<Long, CtrContractVersion> contractIdFirstVersionMap)
    {
        _contractIdFirstVersionMap = contractIdFirstVersionMap;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public List<Group> getGroupList()
    {
        return _groupList;
    }

    public void setGroupList(List<Group> groupList)
    {
        _groupList = groupList;
    }

    public Date getFormingDate()
    {
        return _formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        _formingDate = formingDate;
    }

    public boolean isActiveCourse()
    {
        return _activeCourse;
    }

    public void setActiveCourse(boolean activeCourse)
    {
        _activeCourse = activeCourse;
    }

    public boolean isActiveGroup()
    {
        return _activeGroup;
    }

    public void setActiveGroup(boolean activeGroup)
    {
        _activeGroup = activeGroup;
    }

    public UniEduProgramEducationOrgUnitAddon getUtil()
    {
        return _util;
    }

    public void setUtil(UniEduProgramEducationOrgUnitAddon util)
    {
        _util = util;
    }

    public List<StudentStatus> getStateList()
    {
        return _stateList;
    }

    public void setStateList(List<StudentStatus> stateList)
    {
        _stateList = stateList;
    }

    public String getComment()
    {
        return _comment;
    }

    public void setComment(String comment)
    {
        _comment = comment;
    }

    public Date getDeadlineDateFrom()
    {
        return _deadlineDateFrom;
    }

    public void setDeadlineDateFrom(Date deadlineDateFrom)
    {
        _deadlineDateFrom = deadlineDateFrom;
    }

    public Date getDeadlineDateTo()
    {
        return _deadlineDateTo;
    }

    public void setDeadlineDateTo(Date deadlineDateTo)
    {
        _deadlineDateTo = deadlineDateTo;
    }

    public boolean isActiveStatus()
    {
        return _activeStatus;
    }

    public void setActiveStatus(boolean activeStatus)
    {
        _activeStatus = activeStatus;
    }

    public int getStudentCount()
    {
        return _studentCount;
    }

    public void setStudentCount(int studentCount)
    {
        _studentCount = studentCount;
    }

    public int getContractCount()
    {
        return _contractCount;
    }

    public void setContractCount(int contractCount)
    {
        _contractCount = contractCount;
    }

    public Map<String, Long> getSumResult()
    {
        return _sumResult;
    }

    public void setSumResult(Map<String, Long> sumResult)
    {
        _sumResult = sumResult;
    }

    public Map<String, Long> getSumPromice()
    {
        return _sumPromice;
    }

    public void setSumPromice(Map<String, Long> sumPromice)
    {
        _sumPromice = sumPromice;
    }

    public Date getReportDate()
    {
        return _reportDate;
    }

    public void setReportDate(Date reportDate)
    {
        _reportDate = reportDate;
    }
}