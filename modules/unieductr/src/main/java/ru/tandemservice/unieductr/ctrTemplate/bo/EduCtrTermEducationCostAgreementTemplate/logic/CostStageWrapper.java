/* $Id:$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.logic;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;

import java.util.Date;

/**
 * @author rsizonenko
 * @since 13.05.2016
 */
public class CostStageWrapper
{
    private Date deadlineDate;
    private CtrPriceElementCostStage costStage;

    public CostStageWrapper(CtrPriceElementCostStage costStage) throws ClassCastException {
        setCostStage(costStage);
        if (null != costStage)
            setDeadlineDate(costStage.getDeadlineDate());
    }

    public CtrPriceElementCostStage getCostStage() {
        return costStage;
    }

    public void setCostStage(CtrPriceElementCostStage costStage) {
        this.costStage = costStage;
    }

    public Date getDeadlineDate() {
        return deadlineDate;
    }

    public void setDeadlineDate(Date deadlineDate) {
        this.deadlineDate = deadlineDate;
    }


}
