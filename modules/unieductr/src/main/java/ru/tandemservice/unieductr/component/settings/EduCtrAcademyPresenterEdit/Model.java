/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unieductr.component.settings.EduCtrAcademyPresenterEdit;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.unieductr.base.entity.EduCtrAcademyPresenter;

/**
 * @author iolshvang
 * @since 06.06.11 17:31
 */
@Input({ @Bind(key = "orgUnitId", binding = "orgUnitHolder.id") })
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();

    private final EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<OrgUnit>();
    public EntityHolder<OrgUnit> getOrgUnitHolder() { return this.orgUnitHolder; }
    public OrgUnit getOrgUnit() { return this.getOrgUnitHolder().getValue(); }
    public Long getOrgUnitId() { return this.getOrgUnitHolder().getId(); }

    private ISelectModel employeePostModel;
    public ISelectModel getEmployeePostModel() { return this.employeePostModel; }
    public void setEmployeePostModel(ISelectModel employeePostModel) { this.employeePostModel = employeePostModel; }

    public Collection<EmployeePost> getEmployeePostList() {
        return getOrgUnitPresenterMap().keySet();
    }

    public void setEmployeePostList(Collection<EmployeePost> employeePostList) {
        Map<EmployeePost, EduCtrAcademyPresenter> prevMap = getOrgUnitPresenterMap();
        Map<EmployeePost, EduCtrAcademyPresenter> newMap = new LinkedHashMap<>();
        for (EmployeePost e: employeePostList) {
            EduCtrAcademyPresenter prev = prevMap.get(e);
            if (null == prev) {
                prev = EduCtrAcademyPresenter.buildTransientPresenter(getOrgUnit(), e);
            }
            newMap.put(e, prev);
        }
        this.setOrgUnitPresenterMap(newMap);
    }

    private Map<EmployeePost, EduCtrAcademyPresenter> orgUnitPresenterMap = Collections.emptyMap();
    public Map<EmployeePost, EduCtrAcademyPresenter> getOrgUnitPresenterMap() { return this.orgUnitPresenterMap; }
    public void setOrgUnitPresenterMap(Map<EmployeePost, EduCtrAcademyPresenter> orgUnitPresenterMap) { this.orgUnitPresenterMap = orgUnitPresenterMap; }

    public Collection<EduCtrAcademyPresenter> getOrgUnitPresenterList() { return getOrgUnitPresenterMap().values(); }

    private EduCtrAcademyPresenter orgUnitPresenter;
    public EduCtrAcademyPresenter getOrgUnitPresenter() { return this.orgUnitPresenter; }
    public void setOrgUnitPresenter(EduCtrAcademyPresenter orgUnitPresenter) { this.orgUnitPresenter = orgUnitPresenter; }




}
