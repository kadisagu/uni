package ru.tandemservice.unieductr.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип договора"
 * Имя сущности : ctrContractType
 * Файл data.xml : unieductr.data.xml
 */
public interface CtrContractTypeCodes
{
    /** Константа кода (code) элемента : Договоры возмездного оказания услуг (title) */
    String DOGOVORY_VOZMEZDNOGO_OKAZANIYA_USLUG = "01";
    /** Константа кода (code) элемента : Договор на обучение (title) */
    String DOGOVOR_NA_OBUCHENIE = "01.01";
    /** Константа кода (code) элемента : Договор поставки (title) */
    String DOGOVOR_POSTAVKI = "02";
    /** Константа кода (code) элемента : Договор аренды (title) */
    String DOGOVOR_ARENDY = "03";
    /** Константа кода (code) элемента : Договор аренды оборудования (title) */
    String DOGOVOR_ARENDY_OBORUDOVANIYA = "03.01";
    /** Константа кода (code) элемента : Договор найма (title) */
    String DOGOVOR_NAYMA = "04";
    /** Константа кода (code) элемента : Договор найма койкоместа (title) */
    String DOGOVOR_NAYMA_KOYKOMESTA = "04.01";
    /** Константа кода (code) элемента : Договор найма помещения (title) */
    String DOGOVOR_NAYMA_POMETSHENIYA = "04.02";
    /** Константа кода (code) элемента : Договор подряда (title) */
    String DOGOVOR_PODRYADA = "05";
    /** Константа кода (code) элемента : Договор купли-продажи (title) */
    String DOGOVOR_KUPLI_PRODAJI = "06";
    /** Константа кода (code) элемента : Договор займа (title) */
    String DOGOVOR_ZAYMA = "07";
    /** Константа кода (code) элемента : Кредитный договор (title) */
    String KREDITNYY_DOGOVOR = "08";
    /** Константа кода (code) элемента : Договор банковского счета (title) */
    String DOGOVOR_BANKOVSKOGO_SCHETA = "09";
    /** Константа кода (code) элемента : Договор банковского вклада (title) */
    String DOGOVOR_BANKOVSKOGO_VKLADA = "10";
    /** Константа кода (code) элемента : Договор мены (title) */
    String DOGOVOR_MENY = "11";
    /** Константа кода (code) элемента : Договор дарения (title) */
    String DOGOVOR_DARENIYA = "12";
    /** Константа кода (code) элемента : Договор хранения (title) */
    String DOGOVOR_HRANENIYA = "13";
    /** Константа кода (code) элемента : Договор поручения (title) */
    String DOGOVOR_PORUCHENIYA = "14";
    /** Константа кода (code) элемента : Договор комиссии (title) */
    String DOGOVOR_KOMISSII = "15";
    /** Константа кода (code) элемента : Агентский договор (title) */
    String AGENTSKIY_DOGOVOR = "16";
    /** Константа кода (code) элемента : Договор доверительного управления имуществом (title) */
    String DOGOVOR_DOVERITELNOGO_UPRAVLENIYA_IMUTSHESTVOM = "17";
    /** Константа кода (code) элемента : Договор коммерческой концессии (title) */
    String DOGOVOR_KOMMERCHESKOY_KONTSESSII = "18";
    /** Константа кода (code) элемента : Договор простого товарищества (title) */
    String DOGOVOR_PROSTOGO_TOVARITSHESTVA = "19";
    /** Константа кода (code) элемента : Договоры на выполнение НИОКР (title) */
    String DOGOVORY_NA_VYPOLNENIE_N_I_O_K_R = "20";
    /** Константа кода (code) элемента : Контракт на выполнение гранта (title) */
    String KONTRAKT_NA_VYPOLNENIE_GRANTA = "20.01";
    /** Константа кода (code) элемента : Договор перевозки (title) */
    String DOGOVOR_PEREVOZKI = "21";
    /** Константа кода (code) элемента : Договор транспортной экспедиции (title) */
    String DOGOVOR_TRANSPORTNOY_EKSPEDITSII = "22";
    /** Константа кода (code) элемента : Договор страхования (title) */
    String DOGOVOR_STRAHOVANIYA = "23";
    /** Константа кода (code) элемента : Учебные поручения преподавателей (title) */
    String UCHEBNYE_PORUCHENIYA_PREPODAVATELEY = "24";
    /** Константа кода (code) элемента : Управление проектами (title) */
    String UPRAVLENIE_PROEKTAMI = "25";
    /** Константа кода (code) элемента : Чеклисты (title) */
    String CHEKLISTY = "26";
    /** Константа кода (code) элемента : Договор на обучение ВО (title) */
    String DOGOVOR_NA_OBUCHENIE_V_O = "01.01.vpo";
    /** Константа кода (code) элемента : Договор на обучение СПО (title) */
    String DOGOVOR_NA_OBUCHENIE_S_P_O = "01.01.spo";
    /** Константа кода (code) элемента : Договор на обучение ДПО (title) */
    String DOGOVOR_NA_OBUCHENIE_D_P_O = "01.01.dpo";

    Set<String> CODES = ImmutableSet.of(DOGOVORY_VOZMEZDNOGO_OKAZANIYA_USLUG, DOGOVOR_NA_OBUCHENIE, DOGOVOR_POSTAVKI, DOGOVOR_ARENDY, DOGOVOR_ARENDY_OBORUDOVANIYA, DOGOVOR_NAYMA, DOGOVOR_NAYMA_KOYKOMESTA, DOGOVOR_NAYMA_POMETSHENIYA, DOGOVOR_PODRYADA, DOGOVOR_KUPLI_PRODAJI, DOGOVOR_ZAYMA, KREDITNYY_DOGOVOR, DOGOVOR_BANKOVSKOGO_SCHETA, DOGOVOR_BANKOVSKOGO_VKLADA, DOGOVOR_MENY, DOGOVOR_DARENIYA, DOGOVOR_HRANENIYA, DOGOVOR_PORUCHENIYA, DOGOVOR_KOMISSII, AGENTSKIY_DOGOVOR, DOGOVOR_DOVERITELNOGO_UPRAVLENIYA_IMUTSHESTVOM, DOGOVOR_KOMMERCHESKOY_KONTSESSII, DOGOVOR_PROSTOGO_TOVARITSHESTVA, DOGOVORY_NA_VYPOLNENIE_N_I_O_K_R, KONTRAKT_NA_VYPOLNENIE_GRANTA, DOGOVOR_PEREVOZKI, DOGOVOR_TRANSPORTNOY_EKSPEDITSII, DOGOVOR_STRAHOVANIYA, UCHEBNYE_PORUCHENIYA_PREPODAVATELEY, UPRAVLENIE_PROEKTAMI, CHEKLISTY, DOGOVOR_NA_OBUCHENIE_V_O, DOGOVOR_NA_OBUCHENIE_S_P_O, DOGOVOR_NA_OBUCHENIE_D_P_O);
}
