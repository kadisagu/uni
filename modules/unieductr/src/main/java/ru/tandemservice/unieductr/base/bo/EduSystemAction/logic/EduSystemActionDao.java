package ru.tandemservice.unieductr.base.bo.EduSystemAction.logic;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.entity.contract.*;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;
import ru.tandemservice.unieductr.base.entity.gen.IEduContractRelationGen;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractTypeCodes;

import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * Created by Denny on 7/13/2016.
 */
public class EduSystemActionDao extends UniBaseDao implements IEduSystemActionDao {
    @Override
    public int deleteInvalidEduContracts() {
        Object[] eduCodeArray = {CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE_V_O, CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE_S_P_O};

        //ищем договора
        DQLSelectBuilder dqlSelectBuilder = new DQLSelectBuilder()
                .fromEntity(CtrContractObject.class, "contract")
                .where(notExists(IEduContractRelation.class, IEduContractRelationGen.contractObject().id().s(), property("contract", CtrContractObject.id())))
                .where(in(property("contract", CtrContractObject.type().code()), eduCodeArray));
        List<CtrContractObject> ctrContractObjectList = dqlSelectBuilder.createStatement(getSession()).list();

        int deleted = ctrContractObjectList.size();

        for(CtrContractObject contract : ctrContractObjectList)
        {
           CtrContractVersionManager.instance().dao().doDeleteForce(contract);
        }

        return deleted;
    }
}
