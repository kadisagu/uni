/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrObligationsContractReport.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.unieductr.reports.entity.EduCtrObligationsContractReport;

import java.util.List;
import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 19.08.2016
 */
public interface IEduCtrObligationsContractReportDao extends INeedPersistenceSupport
{
    /**
     * Создает документ отчета.
     * @return RtfDocument
     */
    <M extends Model> byte[] createReportXlsDocument(M model);

    /**
     * Сохраняет отчет и его печатную форму.
     */
    <M extends Model> EduCtrObligationsContractReport saveReport(M model, byte[] document);

    /**
     * @param studentIds ids студентов
     * @return Возвращает мап связи договора на обучение со студентом
     */
    Map<Long, Long> getStudentContractRel(List<Long> studentIds);
}
