/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrProgramChangeAgreementTemplate.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateDAO;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.student.entity.EduCtrProgramChangeAgreementTemplateData;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * @author azhebko
 * @since 11.12.2014
 */
public interface IEduCtrProgramChangeAgreementTemplateDao extends ICtrContractTemplateDAO, INeedPersistenceSupport
{
    /** Создает версию договора об изменении ОП по шаблону на основе предыдущей. */
    public void doCreateNewVersion(@NotNull EduCtrProgramChangeAgreementTemplateData template, @NotNull CtrContractVersion prevVersion, @NotNull EduProgramProf eduProgram, @Nullable CtrPriceElementCost cost, @NotNull Collection<CtrPriceElementCostStage> costStages, @NotNull CtrContractVersionCreateData versionCreateData);
}