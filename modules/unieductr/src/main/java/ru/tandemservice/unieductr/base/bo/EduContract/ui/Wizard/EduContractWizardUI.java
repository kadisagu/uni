package ru.tandemservice.unieductr.base.bo.EduContract.ui.Wizard;

import java.util.Collection;
import java.util.Collections;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.InWizardDataStep.CtrContractVersionInWizardDataStepUI;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.Wizard.CtrContractWizardUIPrsenter;

import ru.tandemservice.unieductr.base.bo.EduContract.logic.IEduContractDAO;



/**
 * @author vdanilov
 */
@Output({
    @Bind(key=CtrContractVersionInWizardDataStepUI.CONTRACT_TYPE_CODES, binding="contractTypeCodes")
})
public class EduContractWizardUI extends CtrContractWizardUIPrsenter
{

    /* @Output */
    public Collection<String> getContractTypeCodes() {
        return Collections.singleton(IEduContractDAO.CTR_CONTRACT_TYPE_EDUCATION);
    }

}
