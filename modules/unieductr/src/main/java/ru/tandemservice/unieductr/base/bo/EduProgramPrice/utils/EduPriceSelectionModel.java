package ru.tandemservice.unieductr.base.bo.EduProgramPrice.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.CtrPriceManager;
import org.tandemframework.shared.ctr.base.entity.ICtrPriceElement;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import org.tandemframework.shared.ctr.catalog.entity.CtrPriceCategory;
import org.tandemframework.shared.ctr.catalog.entity.CtrPricePaymentGrid;

/**
 * Аналог PriceSelectionUI для цен на образовательные программы.
 * Используется совместо с template: EduPriceSelectionTemplate.html.
 * Допускает полного копирования его содержимого в компонент с последующей кастомизацией.
 * @author vdanilov
 */
public abstract class EduPriceSelectionModel {

    /** @return дата, на которую нужно получить цены */
    public abstract Date getPriceDate();

    /** @return элемент, для которого бурется цена */
    public abstract ICtrPriceElement getPriceElement();

    private List<CtrPriceElementCost> costList;
    public List<CtrPriceElementCost> getCostList() { return this.costList; }
    protected void setCostList(final List<CtrPriceElementCost> costList) { this.costList = costList; }

    protected DataWrapper wrap(final CtrPriceElementCost cost) {
        if (null == cost) {
            return new DataWrapper(0L, "Без указания цены", null);
        }
        return new DataWrapper(cost.getId(), cost.getCostAsCurrencyString() + ", " + cost.getPaymentGrid().getTitle() + ", " + cost.getCategory().getTitle(), cost);
    }

    public List<DataWrapper> getCostWrapperList() {
        final List<CtrPriceElementCost> costList = this.getCostList();
        final List<DataWrapper> result = new ArrayList<>(1+costList.size());
        result.add(this.wrap(null));
        for (final CtrPriceElementCost cost: costList) {
            result.add(this.wrap(cost));
        }
        return result;
    }

    // текущая цена (для отрисовки в for)
    private CtrPriceElementCost cost;
    public CtrPriceElementCost getCost() { return this.cost; }
    public void setCost(final CtrPriceElementCost cost) { this.cost = cost; }

    public String getCostAsString() {
        final CtrPriceElementCost cost = this.getCost();
        return cost.getCostAsCurrencyString();
    }

    public String getCostParameters() {
        final CtrPriceElementCost cost = this.getCost();
        return cost.getPaymentGrid().getTitle() + ", " + cost.getCategory().getTitle();
    }

    // выбранная цена
    private CtrPriceElementCost selectedCost;
    public CtrPriceElementCost getSelectedCost() { return this.selectedCost; }
    public void setSelectedCost(final CtrPriceElementCost selectedCost) { this.selectedCost = selectedCost; }

    public DataWrapper getSelectedCostWrapper() { return this.wrap(this.getSelectedCost()); }
    public void setSelectedCostWrapper(final DataWrapper wrapper) { this.setSelectedCost(wrapper.<CtrPriceElementCost>getWrapped()); }

    protected List<CtrPriceElementCost> preparePriceElementCostList()
    {
        return CtrPriceManager.instance().dao().getCurrencyGridCostList(this.getPriceDate(), this.getPriceElement());
    }

    /** обновляет перечнеь цен на основе getPriceElement и getPriceDate */
    public void refreshCostList()
    {
        final List<CtrPriceElementCost> list = preparePriceElementCostList();
        Collections.sort(list, new Comparator<CtrPriceElementCost>() {
            @Override public int compare(final CtrPriceElementCost o1, final CtrPriceElementCost o2) {

                // валюта (как правило, она одинакова)
                {
                    final Currency c1 = o1.getCurrency();
                    final Currency c2 = o2.getCurrency();
                    final int c = c1.getCode().compareTo(c2.getCode());
                    if (0 != c) { return c; }
                }

                // по сетке оплаты
                {
                    final CtrPricePaymentGrid g1 = o1.getPaymentGrid();
                    final CtrPricePaymentGrid g2 = o2.getPaymentGrid();
                    final int c = g1.getCode().compareTo(g2.getCode());
                    if (0 != c) { return c; }
                }

                // по категории
                {
                    final CtrPriceCategory c1 = o1.getCategory();
                    final CtrPriceCategory c2 = o2.getCategory();
                    final int c = c1.getCode().compareTo(c2.getCode());
                    if (0 != c) { return c; }
                }

                // цена
                {
                    final long l1 = o1.getTotalCostAsLong();
                    final long l2 = o1.getTotalCostAsLong();
                    final int c = Long.compare(l1, l2);
                    if (0 != c) { return -c; }
                }

                return o1.getId().compareTo(o2.getId());
            }
        });
        this.setCostList(list);

        // если что-то поменялось - сбрасываем цену
        if (null != this.getSelectedCost() && !list.contains(this.getSelectedCost())) {
            this.setSelectedCost(null);
        }

        // если цены нет - выбираем первую
        if (null == this.getSelectedCost() && list.size() > 0) {
            this.setSelectedCost(list.iterator().next());
        }

        // обновляем список этапок в цене
        this.refreshCostStageList(this.getSelectedCost());
    }


    public static class CostStageData {

        private boolean selected;
        public boolean isSelected() { return this.selected; }
        public void setSelected(final boolean selected) { this.selected = selected; }

        private Date deadlineDate;
        public Date getDeadlineDate() { return this.deadlineDate; }
        public void setDeadlineDate(final Date deadlineDate) { this.deadlineDate = deadlineDate; }

        public CostStageData(CtrPriceElementCostStage stage) {
            this.setSelected(true);
            this.setDeadlineDate(stage.getDeadlineDate());
        }

    }

    private Map<CtrPriceElementCostStage, CostStageData> stageMap;
    public Map<CtrPriceElementCostStage, CostStageData> getStageMap() { return this.stageMap; }
    protected void setStageMap(final Map<CtrPriceElementCostStage, CostStageData> stageMap) { this.stageMap = stageMap; }
    public List<CtrPriceElementCostStage> getStageList() { return new ArrayList<>(this.getStageMap().keySet()); }

    private CtrPriceElementCostStage stage;
    public CtrPriceElementCostStage getStage() { return this.stage; }
    public void setStage(final CtrPriceElementCostStage stage) { this.stage = stage; }

    public CostStageData getStageData() { return this.getStageMap().get(this.getStage()); }

    public void refreshCostStageList(final CtrPriceElementCost cost) {
        if (!EntityBase.equals(this.getSelectedCost(), cost)) { throw new IllegalStateException(); }

        final List<CtrPriceElementCostStage> list = null == cost ? Collections.<CtrPriceElementCostStage>emptyList() : CtrPriceManager.instance().dao().getCostStages(cost);
        final Map<CtrPriceElementCostStage, CostStageData> prevMap = this.getStageMap();
        final Map<CtrPriceElementCostStage, CostStageData> resultMap = new LinkedHashMap<>();
        for (final CtrPriceElementCostStage stage: list) {
            final CostStageData prev = null != prevMap ? prevMap.get(stage) : null;
            resultMap.put(stage, null != prev ? prev : new CostStageData(stage));
        }
        this.setStageMap(resultMap);
    }

    public List<CtrPriceElementCostStage> getSelectedCostStages()
    {
        List<CtrPriceElementCostStage> result = new ArrayList<>();
        for (Map.Entry<CtrPriceElementCostStage, EduPriceSelectionModel.CostStageData> e: this.getStageMap().entrySet()) {
            if (!e.getValue().isSelected()) { continue; }
            CtrPriceElementCostStage fake = new CtrPriceElementCostStage();
            fake.update(e.getKey());
            fake.setId(e.getKey().getId());
            fake.setDeadlineDate(e.getValue().getDeadlineDate());
            result.add(fake);
        }
        return result;
    }


}
