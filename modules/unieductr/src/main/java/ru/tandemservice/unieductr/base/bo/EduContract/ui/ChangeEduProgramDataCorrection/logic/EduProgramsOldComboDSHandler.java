/* $Id:$ */
package ru.tandemservice.unieductr.base.bo.EduContract.ui.ChangeEduProgramDataCorrection.logic;

import com.beust.jcommander.internal.Lists;
import com.beust.jcommander.internal.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.caf.logic.handler.SimpleTitledComboDataSourceWithPopupSizeHandler;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 17.08.15
 * Time: 4:23
 */
public class EduProgramsOldComboDSHandler extends SimpleTitledComboDataSourceWithPopupSizeHandler
{
    public EduProgramsOldComboDSHandler(String ownerId)
    {
        super(ownerId);
    }


    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long contractId = context.get("contractId");
        if(contractId != null)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EduCtrEducationPromise.class, "c")
                    .where(eq(property("c", EduCtrEducationPromise.src().owner().contract().id()), value(contractId)))
                    .order(property("c", EduCtrEducationPromise.eduProgram().title()))
                    ;

            String filter = input.getComboFilterByValue();
            if(!StringUtils.isEmpty(filter))
                builder.where(likeUpper(property("c", EduCtrEducationPromise.eduProgram().title()), value(CoreStringUtils.escapeLike(filter, true))));

            Map<EduProgramProf, List<CtrContractVersion>> eduProgramProfMap = Maps.newHashMap();
            for(EduCtrEducationPromise educationPromise : createStatement(builder).<EduCtrEducationPromise>list())
            {
                SafeMap.safeGet(eduProgramProfMap, educationPromise.getEduProgram(), ArrayList.class).add(educationPromise.getSrc().getOwner());
            }

            List<DataWrapper> wrappers = Lists.newArrayList();
            for(Map.Entry<EduProgramProf, List<CtrContractVersion>> entry : eduProgramProfMap.entrySet())
            {
                List<CtrContractVersion> contractVersions = entry.getValue();
                if(contractVersions.size() > 1)
                    Collections.sort(contractVersions, new Comparator<CtrContractVersion>()
                    {
                        @Override
                        public int compare(CtrContractVersion o1, CtrContractVersion o2)
                        {
                            if(o1.getActivationDate() == null) return 1;
                            if(o2.getActivationDate() == null) return -1;
                            return o1.getActivationDate().compareTo(o2.getActivationDate());
                        }
                    });
                List<String> versions = Lists.newArrayList();

                for(CtrContractVersion version : contractVersions)
                {
                    versions.add("от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(version.getDocStartDate()));
                }
                DataWrapper wrapper = new DataWrapper(entry.getKey().getId(), entry.getKey().getTitleWithCodeAndConditionsShortWithForm()  + " (Версия " + StringUtils.join(versions, ", ") + ")");
                wrapper.setProperty("eduProgram", entry.getKey());
                wrappers.add(wrapper);
            }

            context.put(UIDefines.COMBO_OBJECT_LIST, wrappers);
            DSOutput output = super.execute(input, context);
            output.setTotalSize(wrappers.size());
            return output;
        }
        else
            return super.execute(input, context);
    }

}
