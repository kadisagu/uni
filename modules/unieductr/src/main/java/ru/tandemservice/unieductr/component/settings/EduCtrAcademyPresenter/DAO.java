/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unieductr.component.settings.EduCtrAcademyPresenter;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unieductr.base.entity.EduCtrAcademyPresenter;

import java.util.*;

/**
 * @author iolshvang
 * @since 06.06.11 16:30
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @SuppressWarnings("unchecked")
    @Override
    public void prepare(final Model model)
    {
        final List<IdentifiableWrapper> orgUnitKindsList = new ArrayList<>();
        orgUnitKindsList.add(new IdentifiableWrapper(Model.ALL_ORG_UNITS_ID, "все"));
        orgUnitKindsList.add(new IdentifiableWrapper(Model.NON_ARCHIVED_ORG_UNITS_ID, "не архивные"));
        model.setOrgUnitKindsList(orgUnitKindsList);

        model.setComparator(new Comparator<ViewWrapper<OrgUnit>>()
            {
            @Override
            public int compare(final ViewWrapper<OrgUnit> o1, final ViewWrapper<OrgUnit> o2)
            {
                final int result = o1.getEntity().getOrgUnitType().getPriority() - o2.getEntity().getOrgUnitType().getPriority();
                return (result != 0) ? result : o1.getEntity().getTitle().compareTo(o2.getEntity().getTitle());
            }
            });
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(final Model model)
    {
        final DynamicListDataSource<OrgUnit> dataSource = model.getDataSource();
        final Criteria crit = this.getSession().createCriteria(OrgUnit.class);

        final IdentifiableWrapper orgUnitKind = model.getSettings().get("orgUnitKind");
        if ((model.isFiltersBlockVisible() && (null != orgUnitKind) && Model.NON_ARCHIVED_ORG_UNITS_ID.equals(orgUnitKind.getId())))
        {
            crit.add(Restrictions.eq(OrgUnit.P_ARCHIVAL, Boolean.FALSE));
        }
        else if (!model.isFiltersBlockVisible())
        {
            crit.add(Restrictions.eq(OrgUnit.P_ARCHIVAL, Boolean.FALSE));
        }

        final List<OrgUnit> orgUnits = crit.list();

        dataSource.setCountRow(orgUnits.size());
        UniBaseUtils.createPage(dataSource, orgUnits);

        final Map<Long, Set<String>> contractorMap = new HashMap<>();
        final List<EduCtrAcademyPresenter> eduCtrAcademyPresenterList = this.getList(EduCtrAcademyPresenter.class);
        for (final EduCtrAcademyPresenter eduCtrAcademyPresenter : eduCtrAcademyPresenterList)
        {
            SafeMap.safeGet(contractorMap, eduCtrAcademyPresenter.getOrgUnit().getId(), LinkedHashSet.class).add(eduCtrAcademyPresenter.getPresenter().getFullTitle());
        }

        for (final ViewWrapper<OrgUnit> wrapper : ViewWrapper.<OrgUnit>getPatchedList(model.getDataSource()))
        {
            final Long id = wrapper.getId();
            wrapper.setViewProperty("presenterColumn", StringUtils.join(contractorMap.get(id), '\n'));
        }
    }
}