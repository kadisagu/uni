package ru.tandemservice.unieductr.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unieductr.base.entity.EduCtrAcademyPresenter;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Представитель ОУ при заключении договора на обучение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduCtrAcademyPresenterGen extends EntityBase
 implements INaturalIdentifiable<EduCtrAcademyPresenterGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unieductr.base.entity.EduCtrAcademyPresenter";
    public static final String ENTITY_NAME = "eduCtrAcademyPresenter";
    public static final int VERSION_HASH = 561347232;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_PRESENTER = "presenter";
    public static final String P_ORG_UNIT_BANK_REQUISITES = "orgUnitBankRequisites";
    public static final String P_EMPLOYEE_SIGNATURE_FIO = "employeeSignatureFio";

    private OrgUnit _orgUnit;     // Подразделение
    private EmployeePostContactor _presenter;     // Сотрудник на должности (КА)
    private String _orgUnitBankRequisites;     // Банковские реквизиты подразделения
    private String _employeeSignatureFio;     // Расшифровка подписи сотрудника

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Сотрудник на должности (КА). Свойство не может быть null.
     */
    @NotNull
    public EmployeePostContactor getPresenter()
    {
        return _presenter;
    }

    /**
     * @param presenter Сотрудник на должности (КА). Свойство не может быть null.
     */
    public void setPresenter(EmployeePostContactor presenter)
    {
        dirty(_presenter, presenter);
        _presenter = presenter;
    }

    /**
     * @return Банковские реквизиты подразделения.
     */
    public String getOrgUnitBankRequisites()
    {
        return _orgUnitBankRequisites;
    }

    /**
     * @param orgUnitBankRequisites Банковские реквизиты подразделения.
     */
    public void setOrgUnitBankRequisites(String orgUnitBankRequisites)
    {
        dirty(_orgUnitBankRequisites, orgUnitBankRequisites);
        _orgUnitBankRequisites = orgUnitBankRequisites;
    }

    /**
     * @return Расшифровка подписи сотрудника.
     */
    @Length(max=255)
    public String getEmployeeSignatureFio()
    {
        return _employeeSignatureFio;
    }

    /**
     * @param employeeSignatureFio Расшифровка подписи сотрудника.
     */
    public void setEmployeeSignatureFio(String employeeSignatureFio)
    {
        dirty(_employeeSignatureFio, employeeSignatureFio);
        _employeeSignatureFio = employeeSignatureFio;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EduCtrAcademyPresenterGen)
        {
            if (withNaturalIdProperties)
            {
                setOrgUnit(((EduCtrAcademyPresenter)another).getOrgUnit());
                setPresenter(((EduCtrAcademyPresenter)another).getPresenter());
            }
            setOrgUnitBankRequisites(((EduCtrAcademyPresenter)another).getOrgUnitBankRequisites());
            setEmployeeSignatureFio(((EduCtrAcademyPresenter)another).getEmployeeSignatureFio());
        }
    }

    public INaturalId<EduCtrAcademyPresenterGen> getNaturalId()
    {
        return new NaturalId(getOrgUnit(), getPresenter());
    }

    public static class NaturalId extends NaturalIdBase<EduCtrAcademyPresenterGen>
    {
        private static final String PROXY_NAME = "EduCtrAcademyPresenterNaturalProxy";

        private Long _orgUnit;
        private Long _presenter;

        public NaturalId()
        {}

        public NaturalId(OrgUnit orgUnit, EmployeePostContactor presenter)
        {
            _orgUnit = ((IEntity) orgUnit).getId();
            _presenter = ((IEntity) presenter).getId();
        }

        public Long getOrgUnit()
        {
            return _orgUnit;
        }

        public void setOrgUnit(Long orgUnit)
        {
            _orgUnit = orgUnit;
        }

        public Long getPresenter()
        {
            return _presenter;
        }

        public void setPresenter(Long presenter)
        {
            _presenter = presenter;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EduCtrAcademyPresenterGen.NaturalId) ) return false;

            EduCtrAcademyPresenterGen.NaturalId that = (NaturalId) o;

            if( !equals(getOrgUnit(), that.getOrgUnit()) ) return false;
            if( !equals(getPresenter(), that.getPresenter()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOrgUnit());
            result = hashCode(result, getPresenter());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOrgUnit());
            sb.append("/");
            sb.append(getPresenter());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduCtrAcademyPresenterGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduCtrAcademyPresenter.class;
        }

        public T newInstance()
        {
            return (T) new EduCtrAcademyPresenter();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "presenter":
                    return obj.getPresenter();
                case "orgUnitBankRequisites":
                    return obj.getOrgUnitBankRequisites();
                case "employeeSignatureFio":
                    return obj.getEmployeeSignatureFio();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "presenter":
                    obj.setPresenter((EmployeePostContactor) value);
                    return;
                case "orgUnitBankRequisites":
                    obj.setOrgUnitBankRequisites((String) value);
                    return;
                case "employeeSignatureFio":
                    obj.setEmployeeSignatureFio((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "presenter":
                        return true;
                case "orgUnitBankRequisites":
                        return true;
                case "employeeSignatureFio":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "presenter":
                    return true;
                case "orgUnitBankRequisites":
                    return true;
                case "employeeSignatureFio":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "presenter":
                    return EmployeePostContactor.class;
                case "orgUnitBankRequisites":
                    return String.class;
                case "employeeSignatureFio":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduCtrAcademyPresenter> _dslPath = new Path<EduCtrAcademyPresenter>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduCtrAcademyPresenter");
    }
            

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrAcademyPresenter#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Сотрудник на должности (КА). Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrAcademyPresenter#getPresenter()
     */
    public static EmployeePostContactor.Path<EmployeePostContactor> presenter()
    {
        return _dslPath.presenter();
    }

    /**
     * @return Банковские реквизиты подразделения.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrAcademyPresenter#getOrgUnitBankRequisites()
     */
    public static PropertyPath<String> orgUnitBankRequisites()
    {
        return _dslPath.orgUnitBankRequisites();
    }

    /**
     * @return Расшифровка подписи сотрудника.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrAcademyPresenter#getEmployeeSignatureFio()
     */
    public static PropertyPath<String> employeeSignatureFio()
    {
        return _dslPath.employeeSignatureFio();
    }

    public static class Path<E extends EduCtrAcademyPresenter> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private EmployeePostContactor.Path<EmployeePostContactor> _presenter;
        private PropertyPath<String> _orgUnitBankRequisites;
        private PropertyPath<String> _employeeSignatureFio;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrAcademyPresenter#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Сотрудник на должности (КА). Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrAcademyPresenter#getPresenter()
     */
        public EmployeePostContactor.Path<EmployeePostContactor> presenter()
        {
            if(_presenter == null )
                _presenter = new EmployeePostContactor.Path<EmployeePostContactor>(L_PRESENTER, this);
            return _presenter;
        }

    /**
     * @return Банковские реквизиты подразделения.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrAcademyPresenter#getOrgUnitBankRequisites()
     */
        public PropertyPath<String> orgUnitBankRequisites()
        {
            if(_orgUnitBankRequisites == null )
                _orgUnitBankRequisites = new PropertyPath<String>(EduCtrAcademyPresenterGen.P_ORG_UNIT_BANK_REQUISITES, this);
            return _orgUnitBankRequisites;
        }

    /**
     * @return Расшифровка подписи сотрудника.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrAcademyPresenter#getEmployeeSignatureFio()
     */
        public PropertyPath<String> employeeSignatureFio()
        {
            if(_employeeSignatureFio == null )
                _employeeSignatureFio = new PropertyPath<String>(EduCtrAcademyPresenterGen.P_EMPLOYEE_SIGNATURE_FIO, this);
            return _employeeSignatureFio;
        }

        public Class getEntityClass()
        {
            return EduCtrAcademyPresenter.class;
        }

        public String getEntityName()
        {
            return "eduCtrAcademyPresenter";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
