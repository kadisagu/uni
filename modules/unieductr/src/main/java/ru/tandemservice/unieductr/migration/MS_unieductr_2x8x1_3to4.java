package ru.tandemservice.unieductr.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unieductr_2x8x1_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eduCtrAgreementIncomeReport

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("edu_ctr_agr_income_report_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eductragreementincomereport"), 
				new DBColumn("orgunit_id", DBType.LONG), 
				new DBColumn("perioddatefrom_p", DBType.TIMESTAMP), 
				new DBColumn("perioddateto_p", DBType.TIMESTAMP), 
				new DBColumn("contactortype_p", DBType.createVarchar(255)), 
				new DBColumn("programsubject_p", DBType.createVarchar(1024)), 
				new DBColumn("programform_p", DBType.createVarchar(255)), 
				new DBColumn("edubeginyear_p", DBType.createVarchar(255)), 
				new DBColumn("programtrait_p", DBType.createVarchar(255)), 
				new DBColumn("eduprogram_p", DBType.createVarchar(1024)), 
				new DBColumn("formativeorgunit_p", DBType.createVarchar(1024)), 
				new DBColumn("territorialorgunit_p", DBType.createVarchar(1024)), 
				new DBColumn("educationlevelhighschool_p", DBType.createVarchar(1024)), 
				new DBColumn("developform_p", DBType.createVarchar(255)), 
				new DBColumn("developcondition_p", DBType.createVarchar(255)), 
				new DBColumn("developtech_p", DBType.createVarchar(255)), 
				new DBColumn("developperiod_p", DBType.createVarchar(255)), 
				new DBColumn("course_p", DBType.createVarchar(255)), 
				new DBColumn("group_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eduCtrAgreementIncomeReport");
		}
    }
}