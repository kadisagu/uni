/* $Id$ */
package ru.tandemservice.unieductr.base.ext.CtrPrice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.CtrPriceManager;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.util.ICtrPriceElementCostStageFactory;
import ru.tandemservice.unieductr.base.ext.CtrPrice.logic.*;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrPricePaymentGridCodes;

/**
 * @author Vasily Zhukov
 * @since 25.08.2011
 */
@Configuration
public class CtrPriceExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CtrPriceManager _ctrPriceManager;

    @Bean
    public ItemListExtension<ICtrPriceElementCostStageFactory> stageFactoryItemListExt()
    {
        return itemListExtension(_ctrPriceManager.stageFactoryItemList())
            .add(CtrPricePaymentGridCodes.EPP_PERIOD, new ElementCostStageFactoryWholePeriod())
            .add(CtrPricePaymentGridCodes.EPP_YEAR, new EduProgramElementCostStageFactoryYear())
            .add(CtrPricePaymentGridCodes.EPP_SEMESTER, new EduProgramElementCostStageFactoryTerm())
            .add(CtrPricePaymentGridCodes.EPP_HALF_SEMESTER, new EduProgramElementCostStageFactoryHalfTerm())
            .add(CtrPricePaymentGridCodes.EPP_MONTH, new EduProgramElementCostStageFactoryMonth())
            .create();
    }
}