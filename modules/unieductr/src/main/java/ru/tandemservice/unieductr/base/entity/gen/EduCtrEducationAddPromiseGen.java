package ru.tandemservice.unieductr.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractPromice;
import ru.tandemservice.uniedu.program.entity.EduProgramAdditional;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationAddPromise;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Обязательство обучения по обр. программе ДПО
 *
 * Обязательство обучить человека (src - сторона ОУ, dst - обучаемый)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduCtrEducationAddPromiseGen extends CtrContractPromice
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unieductr.base.entity.EduCtrEducationAddPromise";
    public static final String ENTITY_NAME = "eduCtrEducationAddPromise";
    public static final int VERSION_HASH = -1571190632;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PROGRAM = "eduProgram";

    private EduProgramAdditional _eduProgram;     // Образовательная программа ДПО

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Образовательная программа ДПО. Свойство не может быть null.
     */
    @NotNull
    public EduProgramAdditional getEduProgram()
    {
        return _eduProgram;
    }

    /**
     * @param eduProgram Образовательная программа ДПО. Свойство не может быть null.
     */
    public void setEduProgram(EduProgramAdditional eduProgram)
    {
        dirty(_eduProgram, eduProgram);
        _eduProgram = eduProgram;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduCtrEducationAddPromiseGen)
        {
            setEduProgram(((EduCtrEducationAddPromise)another).getEduProgram());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduCtrEducationAddPromiseGen> extends CtrContractPromice.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduCtrEducationAddPromise.class;
        }

        public T newInstance()
        {
            return (T) new EduCtrEducationAddPromise();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "eduProgram":
                    return obj.getEduProgram();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "eduProgram":
                    obj.setEduProgram((EduProgramAdditional) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eduProgram":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eduProgram":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "eduProgram":
                    return EduProgramAdditional.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduCtrEducationAddPromise> _dslPath = new Path<EduCtrEducationAddPromise>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduCtrEducationAddPromise");
    }
            

    /**
     * @return Образовательная программа ДПО. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrEducationAddPromise#getEduProgram()
     */
    public static EduProgramAdditional.Path<EduProgramAdditional> eduProgram()
    {
        return _dslPath.eduProgram();
    }

    public static class Path<E extends EduCtrEducationAddPromise> extends CtrContractPromice.Path<E>
    {
        private EduProgramAdditional.Path<EduProgramAdditional> _eduProgram;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Образовательная программа ДПО. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.EduCtrEducationAddPromise#getEduProgram()
     */
        public EduProgramAdditional.Path<EduProgramAdditional> eduProgram()
        {
            if(_eduProgram == null )
                _eduProgram = new EduProgramAdditional.Path<EduProgramAdditional>(L_EDU_PROGRAM, this);
            return _eduProgram;
        }

        public Class getEntityClass()
        {
            return EduCtrEducationAddPromise.class;
        }

        public String getEntityName()
        {
            return "eduCtrEducationAddPromise";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
