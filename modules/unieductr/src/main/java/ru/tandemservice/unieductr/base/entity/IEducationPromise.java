package ru.tandemservice.unieductr.base.entity;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor;

/**
 * Обязательство на обучение (не является персистентным интерфейсом).
 * 
 * @author vdanilov
 */
public interface IEducationPromise<T extends IEntity> {

    String L_EDUCATION_PROMISE_TARGET = "educationPromiseTarget";
    String P_EDUCATION_PROMISE_DESCRIPTION = "educationPromiseDescription";

    /** @return исполнитель */
    public CtrContractVersionContractor getSrc();

    /** @return обучаемый */
    public CtrContractVersionContractor getDst();

    /** @return объект, который характеризует обучение (на всех карточках ссылка будет идти на него) */
    T getEducationPromiseTarget();

    /** @return текстовое описание этого объекта (возможно, с уточнениями, хоранящимися в данном обязателстве) */
    String getEducationPromiseDescription();

}
