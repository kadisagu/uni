/* $Id:$ */
package ru.tandemservice.unieductr.ws;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;

/**
 * @author oleyba
 * @since 4/16/14
 */
@WebService
public class EduProgramPriceService
{
    public EduProgramPriceEnvironmentNode getPriceData()
    {
        return IEduProgramPriceServiceDAO.INSTANCE.get().getPriceData(new Date());
    }

    public EduProgramPriceEnvironmentNode getPriceDataOnDate(@WebParam(name = "date") Date date)
    {
        return IEduProgramPriceServiceDAO.INSTANCE.get().getPriceData(date);
    }


}
