/**
 *$Id:$
 */
package ru.tandemservice.unieductr.reports.bo.EduCtrDebitorsPayReport.logic;

import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Date;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 06.11.12
 */
public class Model
{
    private Long _orgUnitId;
    private Date _formingDate = new Date();
    private Date _reportDate;
    private Course _course;
    private Group _group;
    private UniEduProgramEducationOrgUnitAddon _util;
    private boolean _activeCourse;
    private boolean _activeGroup;

    private Map<Long, Date> _contractIdFirstVersionMap;
    private Map<String, Currency> _currencyMap;

    // Getters & Setters

    public Map<String, Currency> getCurrencyMap()
    {
        return _currencyMap;
    }

    public void setCurrencyMap(Map<String, Currency> currencyMap)
    {
        _currencyMap = currencyMap;
    }

    public Map<Long, Date> getContractIdFirstVersionMap()
    {
        return _contractIdFirstVersionMap;
    }

    public void setContractIdFirstVersionMap(Map<Long, Date> contractIdFirstVersionMap)
    {
        _contractIdFirstVersionMap = contractIdFirstVersionMap;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public Date getReportDate()
    {
        return _reportDate;
    }

    public void setReportDate(Date reportDate)
    {
        _reportDate = reportDate;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public Date getFormingDate()
    {
        return _formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        _formingDate = formingDate;
    }

    public boolean isActiveCourse()
    {
        return _activeCourse;
    }

    public void setActiveCourse(boolean activeCourse)
    {
        _activeCourse = activeCourse;
    }

    public boolean isActiveGroup()
    {
        return _activeGroup;
    }

    public void setActiveGroup(boolean activeGroup)
    {
        _activeGroup = activeGroup;
    }

    public UniEduProgramEducationOrgUnitAddon getUtil()
    {
        return _util;
    }

    public void setUtil(UniEduProgramEducationOrgUnitAddon util)
    {
        _util = util;
    }
}
