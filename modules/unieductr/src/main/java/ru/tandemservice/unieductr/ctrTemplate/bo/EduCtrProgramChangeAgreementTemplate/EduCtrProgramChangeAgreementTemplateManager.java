/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrProgramChangeAgreementTemplate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrProgramChangeAgreementTemplate.logic.EduCtrProgramChangeAgreementTemplateDao;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrProgramChangeAgreementTemplate.logic.IEduCtrProgramChangeAgreementTemplateDao;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrProgramChangeAgreementTemplate.ui.Edit.EduCtrProgramChangeAgreementTemplateEdit;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrProgramChangeAgreementTemplate.ui.Pub.EduCtrProgramChangeAgreementTemplatePub;
import ru.tandemservice.unieductr.student.entity.EduCtrProgramChangeAgreementTemplateData;

/**
 * @author azhebko
 * @since 09.12.2014
 */
@Configuration
public class EduCtrProgramChangeAgreementTemplateManager extends BusinessObjectManager implements ICtrContractTemplateManager
{
    public static EduCtrProgramChangeAgreementTemplateManager instance()
    {
        return instance(EduCtrProgramChangeAgreementTemplateManager.class);
    }

    @Override
    public Class<? extends CtrContractVersionTemplateData> getDataEntityClass() { return EduCtrProgramChangeAgreementTemplateData.class; }

    @Override
    public Class<? extends BusinessComponentManager> getDataDisplayComponent() { return EduCtrProgramChangeAgreementTemplatePub.class; }

    @Override
    public Class<? extends BusinessComponentManager> getDataEditComponent() { return EduCtrProgramChangeAgreementTemplateEdit.class; }

    @Override @Bean public IEduCtrProgramChangeAgreementTemplateDao dao() { return new EduCtrProgramChangeAgreementTemplateDao(); }

    @Override
    public boolean isAllowEditInWizard()
    {
        return false;
    }
}