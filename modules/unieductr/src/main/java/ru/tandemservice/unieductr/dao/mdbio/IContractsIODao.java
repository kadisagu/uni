/* $Id$ */
package ru.tandemservice.unieductr.dao.mdbio;

import com.healthmarketscience.jackcess.Database;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 08.12.2014
 */
public interface IContractsIODao
{
    SpringBeanCache<IContractsIODao> instance = new SpringBeanCache<>(IContractsIODao.class.getName());

    /**
     * Экспорт списка договоров на обучение
     * @param mdb файл для экспорта
     * @throws Exception -
     */
    @Transactional(propagation= Propagation.SUPPORTS, readOnly=true)
    void doExport_ContractsList(Database mdb) throws Exception;

    /**
     * Импорт договоров об обучениии
     * @param mdb фаил для импорта
     * @return { mdb.contract_id -> contract.id }
     * @throws Exception
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    Map<String, Long> doImport_ContractList(Database mdb) throws Exception;
}
