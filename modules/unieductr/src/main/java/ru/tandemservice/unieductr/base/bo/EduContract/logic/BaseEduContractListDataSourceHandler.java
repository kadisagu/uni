package ru.tandemservice.unieductr.base.bo.EduContract.logic;

import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractPromice;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractorRole;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;
import ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData;
import ru.tandemservice.unieductr.base.entity.IEducationPromise;
import ru.tandemservice.unieductr.base.entity.gen.IEduContractRelationGen;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * CtrContractVersion
 * @author vdanilov
 */
public abstract class BaseEduContractListDataSourceHandler extends EntityComboDataSourceHandler {

    // связи со студентами (наследники IEduContractRelation)
    public static final String VIEW_REL_LIST = "vp$relList";
    public static final String VIEW_REL = "vp$rel";
    public static final String VIEW_REL_PREDICATE = "vrelPredicate";

    // обязательства по обчению (только те, которые наследуются от IEducationPromise)
    public static final String VIEW_EDU_PROMISE_LIST = "vp$eduPromiseList";
    public static final String VIEW_EDU_PROMISE = "vp$eduPromise";

    // роли
    public static final String VIEW_ROLE_LIST = "vp$roleList";
    public static final String VIEW_ROLE_CUSTOMER = "vp$roleCustomer";
    public static final String VIEW_ROLE_PROVIDER = "vp$roleProvider";

    // шаблон (только те, которые наследуются от IEducationContractVersionTemplateData)
    public static final String VIEW_TEMPLATE = "vp$template";

    // действует
    public static final String VIEW_ACTIVE = "vp$active";

    public BaseEduContractListDataSourceHandler(final String ownerId) {
        super(ownerId, CtrContractVersion.class);
    }

    public static void setupActive(final DataWrapper wrapper)
    {
        final CtrContractVersion input = wrapper.getWrapped();
        wrapper.putInMap(VIEW_ACTIVE, input.getActivationDate() != null && input.getRemovalDate() == null);
    }

    /**
     */
    public static void setupTemplate(final DataWrapper wrapper, final Map<Long, IEducationContractVersionTemplateData> templateMap)
    {
        final CtrContractVersion input = wrapper.getWrapped();
        wrapper.putInMap(VIEW_TEMPLATE, templateMap.get(input.getId()));
    }

    /**
     * @param roleListMap getContractVersionRoleListMap
     */
    public static void setupRoleList(final DataWrapper wrapper, final Map<Long, List<CtrContractVersionContractorRole>> roleListMap)
    {
        final CtrContractVersion input = wrapper.getWrapped();
        List<CtrContractVersionContractorRole> list = roleListMap.get(input.getId());
        if (null == list || list.isEmpty()) {
            wrapper.putInMap(VIEW_ROLE_LIST, Collections.emptyList());
            wrapper.putInMap(VIEW_ROLE_CUSTOMER, null);
            wrapper.putInMap(VIEW_ROLE_PROVIDER, null);
        } else {
            list = new ArrayList<>(list);
            wrapper.putInMap(VIEW_ROLE_LIST, list);
            wrapper.putInMap(VIEW_ROLE_CUSTOMER, role(list, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER));
            wrapper.putInMap(VIEW_ROLE_PROVIDER, role(list, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER));
        }
    }

    private static CtrContractVersionContractorRole role(final List<CtrContractVersionContractorRole> list, final String roleCode) {
        for (final CtrContractVersionContractorRole role: list) {
            if (roleCode.equals(role.getRole().getCode())) { return role; }
        }
        return null;
    }

    /**
     * @param promiseListMap getContractVersionPromiseListMap
     */
    public static void setupEducationPromiseList(final DataWrapper wrapper, final Map<Long, List<CtrContractPromice>> promiseListMap)
    {
        final CtrContractVersion input = wrapper.getWrapped();
        final List<CtrContractPromice> list = promiseListMap.get(input.getId());
        if (null == list || list.isEmpty()) {
            wrapper.putInMap(VIEW_EDU_PROMISE_LIST, Collections.emptyList());
            wrapper.putInMap(VIEW_EDU_PROMISE, null);
        } else {
            final List<CtrContractPromice> eduList = new ArrayList<>(Collections2.filter(list, Predicates.instanceOf(IEducationPromise.class)));
            if (eduList.isEmpty()) {
                wrapper.putInMap(VIEW_EDU_PROMISE_LIST, Collections.emptyList());
                wrapper.putInMap(VIEW_EDU_PROMISE, null);
            } else {
                wrapper.putInMap(VIEW_EDU_PROMISE_LIST, eduList);
                wrapper.putInMap(VIEW_EDU_PROMISE, eduList.get(0));
            }
        }
    }

    /**
     * @param contractRelationListMap getContractRelationListMap
     */
    public static <R extends IEduContractRelation> void setupEduContractRelationList(final DataWrapper wrapper, final Map<Long, List<R>> contractRelationListMap)
    {
        final CtrContractVersion input = wrapper.getWrapped();
        List<R> list = contractRelationListMap.get(input.getContract().getId());
        if (null == list || list.isEmpty()) {
            wrapper.putInMap(VIEW_REL_LIST, Collections.emptyList());
            wrapper.putInMap(VIEW_REL, null);
        } else {
            final Map<PersonRole, R> reducedMap = new LinkedHashMap<>();
            for (final R rel: list) {
                if (null != rel.getPersonRole()) { reducedMap.put(rel.getPersonRole(), rel); }
            }
            list = new ArrayList<>(reducedMap.values());
            wrapper.putInMap(VIEW_REL_LIST, list);
            wrapper.putInMap(VIEW_REL, list.get(0));
        }
    }


    /**
     * @param contractVersionIds { ctrContractVersion.id }
     * @return { version.id -> template }
     */
    public static Map<Long, IEducationContractVersionTemplateData> getContractVersionEduTemplateMap(final ExecutionContext context, final List<Long> contractVersionIds)
    {
        final Map<Long, IEducationContractVersionTemplateData> templateMap = new HashMap<>(contractVersionIds.size());
        BatchUtils.execute(contractVersionIds, DQL.MAX_VALUES_ROW_NUMBER, ids -> {
            final List<IEducationContractVersionTemplateData> templateList = new DQLSelectBuilder()
            .fromEntity(IEducationContractVersionTemplateData.class, "t")
            .column(property("t"))
            .where(in(property(EduCtrContractVersionTemplateData.owner().id().fromAlias("t")), ids))
            .createStatement(context.getSession()).list();
            for (final IEducationContractVersionTemplateData t: templateList) {
                if (null != templateMap.put(t.getOwner().getId(), t)) {
                    throw new IllegalStateException();
                }
            }
        });
        return templateMap;
    }

    /**
     * @param contractVersionIds { ctrContractVersion.id }
     * @return { version.id -> { role } }
     */
    public static Map<Long, List<CtrContractVersionContractorRole>> getContractVersionRoleListMap(final ExecutionContext context, final List<Long> contractVersionIds)
    {
        final Map<Long, List<CtrContractVersionContractorRole>> roleListMap = new HashMap<>(contractVersionIds.size());
        BatchUtils.execute(contractVersionIds, DQL.MAX_VALUES_ROW_NUMBER, ids -> {
            final List<CtrContractVersionContractorRole> roleList = new DQLSelectBuilder()
            .fromEntity(CtrContractVersionContractorRole.class, "r")
            .column(property("r"))
            .where(in(property(CtrContractVersionContractorRole.contactor().owner().id().fromAlias("r")), ids))
            .createStatement(context.getSession()).list();
            for (final CtrContractVersionContractorRole r: roleList) {
                SafeMap.safeGet(roleListMap, r.getContactor().getOwner().getId(), ArrayList.class).add(r);
            }
        });
        return roleListMap;
    }

    /**
     * @param contractVersionIds { ctrContractVersion.id }
     * @return { version.id -> { promise } }
     */
    public static Map<Long, List<CtrContractPromice>> getContractVersionPromiseListMap(final ExecutionContext context, final List<Long> contractVersionIds)
    {
        final Map<Long, List<CtrContractPromice>> promiseListMap = new HashMap<>(contractVersionIds.size());
        BatchUtils.execute(contractVersionIds, DQL.MAX_VALUES_ROW_NUMBER, ids -> {
            final List<CtrContractPromice> promiseList = new DQLSelectBuilder()
            .fromEntity(CtrContractPromice.class, "p")
            .column(property("p"))
            .where(in(property(CtrContractPromice.src().owner().id().fromAlias("p")), ids))
            .order(property(CtrContractPromice.deadlineDate().fromAlias("p")))
            .createStatement(context.getSession()).list();
            for (final CtrContractPromice p: promiseList) {
                SafeMap.safeGet(promiseListMap, p.getSrc().getOwner().getId(), ArrayList.class).add(p);
            }
        });
        return promiseListMap;
    }


    private static final Comparator<IEduContractRelation> eduStudentContractComparator = CommonCollator.comparing(r -> r.getPersonRole().getFullFio(), true);

    /**
     * @param contractVersionIds { ctrContractVersion.id }
     * @param relationClass класс связи (проверяется по instanceof, не обязательно персистентный)
     * @return { contractObject.id -> { relation } }
     */
    @SuppressWarnings("unchecked")
    public static <R extends IEduContractRelation> Map<Long, List<R>> getContractRelationListMap(final ExecutionContext context, final List<Long> contractVersionIds, final Class<R> relationClass, final Predicate<IEduContractRelation> eduContractRelationPredicate)
    {
        final Map<Long, List<R>> relationListMap = new HashMap<>(contractVersionIds.size());
        BatchUtils.execute(contractVersionIds, DQL.MAX_VALUES_ROW_NUMBER, ids -> {
            List<IEduContractRelation> relationList = new DQLSelectBuilder()
            .fromEntity(IEduContractRelation.class, "r")
            .column(property("r"))
            .where(in(
                    property(IEduContractRelationGen.contractObject().id().fromAlias("r")),
                    new DQLSelectBuilder()
                            .fromEntity(CtrContractVersion.class, "v")
                            .column(property(CtrContractVersion.contract().id().fromAlias("v")))
                            .where(in(property("v.id"), ids))
                            .buildQuery()
            ))
            .createStatement(context.getSession()).list();

            if(eduContractRelationPredicate != null)
                relationList = (List) CollectionUtils.select(relationList, eduContractRelationPredicate);

            for (final IEduContractRelation rel: relationList) {
                if (relationClass.isInstance(rel)) {
                    SafeMap.safeGet(relationListMap, rel.getContractObject().getId(), ArrayList.class /* как правило, здесь 1 человек */).add((R)rel);
                }
            }
        });
        for (final Map.Entry<Long, List<R>> e: relationListMap.entrySet()) {
            final List<R> list = new ArrayList<>(e.getValue()); // создаем list ровно на выбранные значения
            if (list.size() > 0) { Collections.sort(list, eduStudentContractComparator); }
            e.setValue(list);
        }
        return relationListMap;
    }

}
