/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrPaymentsReport.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.unieductr.reports.bo.EduCtrPaymentsReport.ui.Add.EduCtrPaymentsReportAdd;
import ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport;

/**
 * @author Alexey Lopatin
 * @since 17.04.2015
 */
@State({@Bind(key = EduCtrPaymentsReportListUI.PROP_ORG_UNIT_ID, binding = "orgUnitId")})
public class EduCtrPaymentsReportListUI extends UIPresenter
{
    public static final String PROP_ORG_UNIT_ID = "orgUnitId";

    private Long _orgUnitId;

    public void onClickAdd()
    {
        _uiActivation.asRegion(EduCtrPaymentsReportAdd.class).parameter(PROP_ORG_UNIT_ID, _orgUnitId).activate();
    }

    public void onClickPrint()
    {
        EduCtrPaymentsReport report = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());
        byte[] content = report.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл отчета пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("PaymentReport.rtf").document(content), true);
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(EduCtrPaymentsReportList.REPORT_LIST_DS))
        {
            dataSource.put(PROP_ORG_UNIT_ID, _orgUnitId);
        }
    }

    public String getViewPermissionKey()
    {
        return _orgUnitId == null ? "eduCtrPaymentsReport" : new OrgUnitSecModel(DataAccessServices.dao().get(OrgUnit.class, _orgUnitId)).getPermission("orgUnit_viewEduCtrPaymentsReport");
    }

    @Override
    public ISecured getSecuredObject()
    {
        return _orgUnitId != null ? DataAccessServices.dao().get(OrgUnit.class, _orgUnitId) : super.getSecuredObject();
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return new CommonPostfixPermissionModel(_orgUnitId != null ? OrgUnitSecModel.getPostfix(DataAccessServices.dao().get(OrgUnit.class, _orgUnitId)) : null);
    }

    public String getAddStorableReportPermissionKey(){ return getSec().getPermission(_orgUnitId != null ? "orgUnit_addEduCtrPaymentsReport" : "addEduCtrStorableReport"); }

    public String getDeleteStorableReportPermissionKey(){ return getSec().getPermission(_orgUnitId != null ? "orgUnit_deleteEduCtrPaymentsReport" : "deleteEduCtrStorableReport"); }

    // Getters & Setters

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }
}
