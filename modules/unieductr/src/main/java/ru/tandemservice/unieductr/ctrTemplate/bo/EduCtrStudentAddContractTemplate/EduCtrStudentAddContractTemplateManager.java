package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentAddContractTemplate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.addon.DefaultAddonBuilder;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtPointBuilder;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.program.bo.EduProgramAdditional.EduProgramAdditionalManager;
import ru.tandemservice.uniedu.program.entity.EduProgramAdditional;
import ru.tandemservice.uniedu.program.entity.EduProgramAdditionalProf;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentAddContractTemplate.logic.EduCtrStudentAddContractTemplateDAO;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentAddContractTemplate.logic.IEduCtrStudentAddContractTemplateDAO;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentAddContractTemplate.ui.Add.PriceTemplate.PriceTemplateAddon;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentAddContractTemplate.ui.Edit.EduCtrStudentAddContractTemplateEdit;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentAddContractTemplate.ui.Pub.EduCtrStudentAddContractTemplatePub;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentAddContractTemplateData;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
@Configuration
public class EduCtrStudentAddContractTemplateManager extends BusinessObjectManager implements ICtrContractTemplateManager {

    public static EduCtrStudentAddContractTemplateManager instance() {
        return instance(EduCtrStudentAddContractTemplateManager.class);
    }

    @Override
    public Class<? extends CtrContractVersionTemplateData> getDataEntityClass() {
        return EduCtrStudentAddContractTemplateData.class;
    }

    @Override
    public Class<? extends BusinessComponentManager> getDataDisplayComponent() {
        return EduCtrStudentAddContractTemplatePub.class;
    }

    @Override
    public Class<? extends BusinessComponentManager> getDataEditComponent() {
        return EduCtrStudentAddContractTemplateEdit.class;
    }

    @Bean
    @Override
    public IEduCtrStudentAddContractTemplateDAO dao() {
        return new EduCtrStudentAddContractTemplateDAO();
    }

    @Override
    public boolean isAllowEditInWizard() {
        return false;
    }


    public static final String PARAM_PROGRAM_YEAR = "programYear";
    public static final String PARAM_PROGRAM_KIND = "programKind";
    public static final String PARAM_PROGRAM_FORM = "programForm";


    public static final String DS_PROGRAM = "programDS";
    public static final String DS_START_EDU_YEAR = "startEduYearDS";
    public static final String DS_PROGRAM_FORM = "programFormDS";

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> programDSHandler() {
        return new EntityComboDataSourceHandler(getName(), EduProgramAdditional.class)
        .where(EduProgramAdditional.kind(), PARAM_PROGRAM_KIND)
        .where(EduProgramAdditional.year(), PARAM_PROGRAM_YEAR)
        .where(EduProgramAdditional.form(), PARAM_PROGRAM_FORM)
        .order(EduProgramAdditional.title())
        .filter(EduProgramAdditional.title())
        .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler startEduYearDSHandler()
    {
        return EducationYear.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) ->
                {
                    DQLSelectBuilder eduProgramSubSelect = new DQLSelectBuilder()
                            .fromEntity(EduProgramAdditionalProf.class, "ep")
                            .where(eq(property("ep", EduProgramAdditionalProf.year()), property(alias)));

                    dql.where(exists(eduProgramSubSelect.buildQuery()));
                    return dql;
                });
    }

    @Bean
    public IDefaultComboDataSourceHandler programFormDSHandler()
    {
        return EduProgramForm.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) -> {
                    final EducationYear startYear = context.get(PARAM_PROGRAM_YEAR);

                    if (startYear == null) {
                        dql.where(nothing());
                    } else {
                        DQLSelectBuilder formConditionBuilder = new DQLSelectBuilder().fromEntity(EduProgramAdditionalProf.class, "ep")
                                .where(eq(property("ep", EduProgramAdditionalProf.year()), value(startYear)))
                                .column(property("ep", EduProgramAdditionalProf.form())).distinct();

                        dql.where(in(property(alias), formConditionBuilder.buildQuery()));
                    }
                    return dql;
                });
    }

    /* Селекты формы добавления договора, доп. соглашения с выбором ОП и этапов оплаты для автоматического формирования обязательств. */
    public static IPresenterExtPointBuilder programAdditionalPresenterExtPointBuilder(IPresenterExtPointBuilder presenterExtPointBuilder, String name)
    {
        return presenterExtPointBuilder
            .addDataSource(EducationCatalogsManager.instance().eduYearDSConfig())
            .addDataSource(SelectDSConfig.with(DS_START_EDU_YEAR, name).dataSourceClass(SelectDataSource.class).handler(instance().startEduYearDSHandler()))
            .addDataSource(EduProgramAdditionalManager.instance().programKindAdditionalDSConfig())
            .addDataSource(SelectDSConfig.with(DS_PROGRAM_FORM, name).dataSourceClass(SelectDataSource.class).handler(instance().programFormDSHandler()))
            .addDataSource(SelectDSConfig.with(DS_PROGRAM, name).dataSourceClass(SelectDataSource.class).handler(instance().programDSHandler()).addColumn(EduProgramAdditional.fullTitle().s()))
            .addAddon(new DefaultAddonBuilder(PriceTemplateAddon.PRICE_TEMPLATE_ADDON, name, PriceTemplateAddon.class));
    }
}
