/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrAgreementIncomeReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unieductr.reports.bo.EduCtrAgreementIncomeReport.logic.EduCtrAgreementIncomeReportDao;
import ru.tandemservice.unieductr.reports.bo.EduCtrAgreementIncomeReport.logic.IEduCtrAgreementIncomeReportDao;

/**
 * @author Alexey Lopatin
 * @since 21.04.2015
 */
@Configuration
public class EduCtrAgreementIncomeReportManager extends BusinessObjectManager
{
    public static EduCtrAgreementIncomeReportManager instance()
    {
        return instance(EduCtrAgreementIncomeReportManager.class);
    }

    @Bean
    public IEduCtrAgreementIncomeReportDao dao()
    {
        return new EduCtrAgreementIncomeReportDao();
    }
}
