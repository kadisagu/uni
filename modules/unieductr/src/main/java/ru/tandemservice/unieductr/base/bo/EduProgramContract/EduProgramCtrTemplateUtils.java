/* $Id:$ */
package ru.tandemservice.unieductr.base.bo.EduProgramContract;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractPrintUtils;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationAddPromise;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;

/**
 * Утильный класс для печати договоров и доп. соглашений на обучение
 *
 * @author oleyba
 * @since 7/27/12
 */
public class EduProgramCtrTemplateUtils extends EduContractPrintUtils
{
    public static String getBaseEduLevel(EduCtrEducationPromise eduPromice)
    {
        EduLevel baseLevel = eduPromice.getEduProgram().getBaseLevel();
        if (baseLevel == null) { return ""; }
        return ", на базе уровня образования «" + baseLevel.getTitle() + "»";
    }

    public static String getEducationType(EduCtrEducationPromise eduPromice)
    {
        return "основная";
    }

    public static String getEduLevelG(EduCtrEducationPromise eduPromice)
    {
        EduProgramKind programKind = eduPromice.getEduProgram().getProgramSubject().getSubjectIndex().getProgramKind();
        if (programKind.isProgramBasic())
            return "начального профессионального образования";
        if (programKind.isProgramSecondaryProf())
            return "среднего профессионального образования";
        if (programKind.isProgramHigherProf())
            return "высшего профессионального образования";
        if (programKind.isProgramAdditionalProf())
            return "дополнительного профессионального образования";
        return programKind.getTitle();
    }

    public static void printEduPromiceData(EduCtrEducationPromise eduPromice, RtfInjectModifier modifier)
    {

        modifier.put("apprenticeshipUP", eduPromice.getEduProgram().getDuration().getTitle());
        modifier.put("course", "1");

        modifier.put("qualification", eduPromice.getEduProgram().getProgramQualification().getTitle());
        modifier.put("developForm", eduPromice.getEduProgram().getForm().getTitle());
        modifier.put("speciality", eduPromice.getEduProgram().getProgramSubject().getTitleWithCode());
        modifier.put("educationLevelStr_G", getEduLevelG(eduPromice));
        modifier.put("educationType", getEducationType(eduPromice));
        modifier.put("basedEducationLevel", getBaseEduLevel(eduPromice));
        modifier.put("apprenticeshipGOS", eduPromice.getEduProgram().getDuration().getTitle());

        ContactorPerson educationReceiver = eduPromice.getDst().getContactor();
        IdentityCard idc = educationReceiver.getPerson().getIdentityCard();

        modifier.put("nationality", idc.getCitizenship().getShortTitle());
        modifier.put("calledStudent", called(educationReceiver));
        modifier.put("learned", idc.getSex().isMale() ? "обучающийся" : "обучающаяся");
        modifier.put("studentFio", educationReceiver.getFio());
        modifier.put("studentFio_G", PersonManager.instance().declinationDao().getCalculatedFIODeclination(educationReceiver.getPerson().getIdentityCard(), InflectorVariantCodes.RU_GENITIVE));
        modifier.put("studentFullFio", educationReceiver.getFullFio());
        modifier.put("studentBirthDate", educationReceiver.getPerson().getBirthDateStr());
        modifier.put("studentPhoneMobile", StringUtils.trimToEmpty(educationReceiver.getPerson().getContactData().getPhoneMobile()));
        modifier.put("studentPassportSeria", idc.getSeria());
        modifier.put("studentPassportNumber", idc.getNumber());
        modifier.put("studentPassportDate", idc.getIssuanceDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(idc.getIssuanceDate()));
        modifier.put("studentPassportInfo", StringUtils.trimToEmpty(idc.getIssuancePlace()));
        modifier.put("studentRegAddress", idc.getAddress() == null ? "" : idc.getAddress().getTitleWithFlat());
    }

    public static void printEduPromiceData(EduCtrEducationAddPromise eduPromice, RtfInjectModifier modifier)
    {

        modifier.put("apprenticeshipUP", eduPromice.getEduProgram().getDuration().getTitle());
        modifier.put("course", "1");

        modifier.put("eduProgram", eduPromice.getEduProgram().getTitle());
        modifier.put("developForm", eduPromice.getEduProgram().getForm().getTitle());
        modifier.put("apprenticeshipGOS", eduPromice.getEduProgram().getDuration().getTitle());

        ContactorPerson educationReceiver = eduPromice.getDst().getContactor();
        IdentityCard idc = educationReceiver.getPerson().getIdentityCard();

        modifier.put("nationality", idc.getCitizenship().getShortTitle());
        modifier.put("calledStudent", called(educationReceiver));
        modifier.put("learned", idc.getSex().isMale() ? "обучающийся" : "обучающаяся");
        modifier.put("studentFio", educationReceiver.getFio());
        modifier.put("studentFio_G", PersonManager.instance().declinationDao().getCalculatedFIODeclination(educationReceiver.getPerson().getIdentityCard(), InflectorVariantCodes.RU_GENITIVE));
        modifier.put("studentFullFio", educationReceiver.getFullFio());
        modifier.put("studentBirthDate", educationReceiver.getPerson().getBirthDateStr());
        modifier.put("studentPhoneMobile", StringUtils.trimToEmpty(educationReceiver.getPerson().getContactData().getPhoneMobile()));
        modifier.put("studentPassportSeria", idc.getSeria());
        modifier.put("studentPassportNumber", idc.getNumber());
        modifier.put("studentPassportDate", idc.getIssuanceDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(idc.getIssuanceDate()));
        modifier.put("studentPassportInfo", StringUtils.trimToEmpty(idc.getIssuancePlace()));
        modifier.put("studentRegAddress", idc.getAddress() == null ? "" : idc.getAddress().getTitleWithFlat());
    }


}
