/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateDAO;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.ui.Add.EduCtrTermEducationCostAgreementTemplateAdd;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.ui.Add.EduCtrTermEducationCostAgreementTemplateAddUI;
import ru.tandemservice.unieductr.student.entity.EduCtrTermEducationCostAgreementTemplateData;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

/**
 * @author azhebko
 * @since 04.12.2014
 */
public interface IEduCtrTermEducationCostAgreementTemplateDao extends ICtrContractTemplateDAO, INeedPersistenceSupport
{
    /** @return Учебная сетка студента. */
    public DevelopGrid getStudentDevelopGrid(@NotNull Student student);

    /**
     * Для выбранной цены определяет этап оплаты требуемого семестра.
     * @param priceElementCost цена с сеткой оплаты "За семестр"
     * @param termNumber номер семестра
     * @return найденный этап оплаты
     * @throws java.lang.IllegalArgumentException если сетка оплаты цены не "За семестр"
     * */
    public CtrPriceElementCostStage getPriceElementCostStage(@NotNull CtrPriceElementCost priceElementCost, @NotNull Integer termNumber);

    /** Создает версию договора об установлении стоимости обучения за семестр по шаблону на основе предыдущей. */
    public void doCreateNewVersion(@NotNull EduCtrTermEducationCostAgreementTemplateData template, @NotNull CtrContractVersion prevVersion, @NotNull Collection<CtrPaymentPromice> prevVersionPromises, @Nullable Collection<CostStageWrapper> costStages, @NotNull CtrContractVersionCreateData versionCreateData);
}