package ru.tandemservice.unieductr.base.bo.EduCtrEducationPromise.ui.SectionPromise;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.sec.bo.Sec.logic.IContextSecuredComponentManager;

@Configuration
public class EduCtrEducationPromiseSectionPromise extends BusinessComponentManager implements IContextSecuredComponentManager {

    @Override
    public void fillPermissionGroup(final PermissionGroupMeta parent, final String securityPostfix) {
        PermissionMetaUtil.createPermission(parent, "add_educationPromise_" + securityPostfix, getMeta().getProperty("bc.sec.add.title"));
        PermissionMetaUtil.createPermission(parent, "edit_educationPromise_" + securityPostfix, getMeta().getProperty("bc.sec.edit.title"));
        PermissionMetaUtil.createPermission(parent, "delete_educationPromise_" + securityPostfix, getMeta().getProperty("bc.sec.delete.title"));
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder().create();
    }

}
