/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrPaymentsReport.ui.Add;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractReadAggregateHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.bo.Contactor.ContactorManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.upper;

/**
 * @author Alexey Lopatin
 * @since 17.04.2015
 */
@Configuration
public class EduCtrPaymentsReportAdd extends BusinessComponentManager
{
    public static final String CONTACTOR_TYPE_DS = "contactorTypeDS";
    public static final String PROGRAM_SUBJECT_DS = "programSubjectDS";
    public static final String EDU_PROGRAM_DS = "eduProgramDS";

    public static final String GROUP_DS = "groupDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(CONTACTOR_TYPE_DS, ContactorManager.instance().contactorTypeComboDSHandler()))
                .addDataSource(selectDS(PROGRAM_SUBJECT_DS, programSubjectDS()).addColumn(EduProgramSubject.P_TITLE_WITH_CODE))
                .addDataSource(EducationCatalogsManager.instance().programFormDSConfig())
                .addDataSource(EducationCatalogsManager.instance().programTraitDSConfig())
                .addDataSource(selectDS(EDU_PROGRAM_DS, eduProgramDS()))
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(selectDS(GROUP_DS, groupDSHandler()))
                .addDataSource(selectDS(EducationCatalogsManager.DS_EDU_YEAR, EducationCatalogsManager.instance().eduYearDSHandler()))
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .addAddon(uiAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME, UniEduProgramEducationOrgUnitAddon.class))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> programSubjectDS()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
                .filter(EduProgramSubject.subjectCode())
                .filter(EduProgramSubject.title())
                .order(EduProgramSubject.subjectCode())
                .order(EduProgramSubject.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eduProgramDS()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramProf.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                List<EduProgramSubject> programSubjectList = context.get(EduCtrPaymentsReportAddUI.PROP_PROGRAM_SUBJECT_LIST);
                List<EduProgramForm> programFormList = context.get(EduCtrPaymentsReportAddUI.PROP_PROGRAM_FORM_LIST);
                List<EducationYear> eduBeginYearList = context.get(EduCtrPaymentsReportAddUI.PROP_EDU_BEGIN_YEAR_LIST);
                List<EduProgramTrait> programTraitList = context.get(EduCtrPaymentsReportAddUI.PROP_PROGRAM_TRAIT_LIST);

                if (null != programSubjectList && !programSubjectList.isEmpty())
                {
                    dql.where(in(property(alias, EduProgramProf.programSubject()), programSubjectList));
                }
                if (null != programFormList && !programFormList.isEmpty())
                {
                    dql.where(in(property(alias, EduProgramProf.form()), programFormList));
                }
                if (null != eduBeginYearList && !eduBeginYearList.isEmpty())
                {
                    dql.where(in(property(alias, EduProgramProf.year()), eduBeginYearList));
                }
                if (null != programTraitList && !programTraitList.isEmpty())
                {
                    dql.where(in(property(alias, EduProgramProf.eduProgramTrait()), programTraitList));
                }
            }
        }
                .filter(EduProgramProf.title())
                .order(EduProgramProf.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> groupDSHandler()
    {
        return new AbstractReadAggregateHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                final Long orgUnitId = context.get(EduCtrPaymentsReportAddUI.PROP_ORGUNIT_ID);
                final Course course = context.get(EduCtrPaymentsReportAddUI.PROP_COURSE);
                List<EducationOrgUnit> educationOrgUnitList = context.get(EduCtrPaymentsReportAddUI.PROP_EDU_ORGUNIT_LIST);

                // Группа - необязат. выпадающий список из контингентских неархивных групп.
                // Фильтрация по полям блока фильтров по НПП - группы, где НПП группы удовлетворяет выбранному в фильтрах,
                // или в группе есть студент, НПП которого удовлетворяет выбранному в фильтрах.
                // Если строится на деканате, то только группы студентов этого деканата.

                final Set keySet = input.getPrimaryKeys();
                if (null != keySet && (keySet.isEmpty())) return ListOutputBuilder.get(input, new ArrayList(0)).build();

                final Set<Group> groupSet = new HashSet<>();
                final String filter = input.getComboFilterByValue();

                final DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(Group.class, "r")
                        .column(property("r.id"))
                        .where(eq(property(Group.archival().fromAlias("r")), value(Boolean.FALSE)));
                if (null != keySet)
                {
                    builder.where(in(property("r.id"), keySet));
                }

                if (null != orgUnitId)
                {
                    builder.where(
                            exists(
                                    new DQLSelectBuilder()
                                            .fromEntity(Student.class, "inclS")
                                            .column(property("inclS.id"))
                                            .where(eq(property(Student.group().fromAlias("inclS")), property("r")))
                                            .where(or(
                                                    eq(property(Student.educationOrgUnit().groupOrgUnit().id().fromAlias("inclS")), value(orgUnitId)),
                                                    eq(property(Student.educationOrgUnit().formativeOrgUnit().id().fromAlias("inclS")), value(orgUnitId))
                                            ))
                                            .buildQuery()
                            )
                    );
                }

                if (!StringUtils.isEmpty(filter))
                {
                    builder.where(DQLExpressions.like(upper(property(Group.title().fromAlias("r"))), value(CoreStringUtils.escapeLike(filter, true))));
                }

                BatchUtils.execute(educationOrgUnitList, 128, elements -> {
                    // параметры группы
                    groupSet.addAll(
                            new DQLSelectBuilder()
                                    .fromEntity(Group.class, "g")
                                    .where(in(property("g.id"), builder.buildQuery()))
                                    .where(in(property("g", Group.educationOrgUnit()), elements))
                                    .where(eq(property("g", Group.course()), value(course)))
                                    .createStatement(context.getSession()).<Group>list()
                    );

                    // через студентов в группе
                    groupSet.addAll(
                            new DQLSelectBuilder()
                                    .fromEntity(Group.class, "g")
                                    .where(in(property("g.id"), builder.buildQuery()))
                                    .where(eq(property("g", Group.course()), value(course)))
                                    .where(exists(
                                            new DQLSelectBuilder()
                                                    .fromEntity(Student.class, "y")
                                                    .column(property("y.id"))
                                                    .where(eq(property(Student.group().fromAlias("y")), property("g")))
                                                    .where(in(property(Student.educationOrgUnit().fromAlias("y")), elements))
                                                    .buildQuery()
                                    ))
                                    .createStatement(context.getSession()).<Group>list()
                    );
                });
                return ListOutputBuilder.get(input, new ArrayList<>(groupSet)).pageable(false).build().ordering(new EntityComparator(new EntityOrder(Group.title().s())));
            }
        };
    }
}
