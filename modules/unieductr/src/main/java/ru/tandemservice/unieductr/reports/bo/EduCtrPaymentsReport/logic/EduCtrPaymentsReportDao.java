/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrPaymentsReport.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections15.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.ctr.base.bo.Contactor.util.ContactorCategoryMeta;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contract.*;
import org.tandemframework.shared.ctr.catalog.entity.CtrPaymentDocumentType;
import org.tandemframework.shared.ctr.catalog.entity.CtrTemplateScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrTemplateScriptItemCodes;
import ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon.Filters.*;

/**
 * @author Alexey Lopatin
 * @since 17.04.2015
 */
public class EduCtrPaymentsReportDao extends UniBaseDao implements IEduCtrPaymentsReportDao
{
    @Override
    public <M extends Model> RtfDocument createReportRtfDocument(M model)
    {
        CtrTemplateScriptItem template = getCatalogItem(CtrTemplateScriptItem.class, CtrTemplateScriptItemCodes.EDU_CTR_PAYMENTS_REPORT);
        RtfDocument document = new RtfReader().read(template.getTemplate());
        RtfTableModifier tm = new RtfTableModifier();
        RtfInjectModifier im = new RtfInjectModifier();

        injectTable(tm, im, model).modify(document);
        injectModifier(im, model).modify(document);

        return document;
    }


    @Override
    public <M extends Model> EduCtrPaymentsReport saveReport(M model, RtfDocument document)
    {
        EduCtrPaymentsReport report = new EduCtrPaymentsReport();

        DatabaseFile content = new DatabaseFile();
        content.setContent(RtfUtil.toByteArray(document));
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        content.setFilename("PaymentReport.rtf");
        save(content);

        UniEduProgramEducationOrgUnitAddon util = model.getUtil();

        if (model.getOrgUnitId() != null)
            report.setOrgUnit(get(OrgUnit.class, model.getOrgUnitId()));
        report.setContent(content);
        report.setFormingDate(model.getFormingDate());
        report.setPeriodDateFrom(model.getPeriodDateFrom());
        report.setPeriodDateTo(model.getPeriodDateTo());
        report.setContactorType(model.isActiveContactorType() ? model.getContactorType().getTitle() : "");

        report.setProgramSubject(model.isActiveProgramSubject() ? UniStringUtils.join(model.getProgramSubjectList(), EduProgramSubject.P_TITLE_WITH_CODE, ", ") : "");
        report.setProgramForm(model.isActiveProgramForm() ? UniStringUtils.join(model.getProgramFormList(), EduProgramForm.P_TITLE, ", ") : "");
        report.setEduBeginYear(model.isActiveEduBeginYear() ? UniStringUtils.join(model.getEduBeginYearList(), EducationYear.P_TITLE, ", ") : "");
        report.setProgramTrait(model.isActiveProgramTrait() ? UniStringUtils.join(model.getProgramTraitList(), EduProgramTrait.P_TITLE, ", ") : "");
        report.setEduProgram(model.isActiveEduProgram() ? UniStringUtils.join(model.getEduProgramList(), EduProgramProf.P_TITLE, ", ") : "");

        report.setFormativeOrgUnit(getValue(util, FORMATIVE_ORG_UNIT, OrgUnit.P_TITLE));
        report.setTerritorialOrgUnit(getValue(util, TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_TITLE));
        report.setEducationLevelHighSchool(getValue(util, EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.P_PRINT_TITLE));
        report.setDevelopForm(getValue(util, DEVELOP_FORM, DevelopForm.P_TITLE));
        report.setDevelopCondition(getValue(util, DEVELOP_CONDITION, DevelopCondition.P_TITLE));
        report.setDevelopTech(getValue(util, DEVELOP_TECH, DevelopTech.P_TITLE));
        report.setDevelopPeriod(getValue(util, DEVELOP_PERIOD, DevelopPeriod.P_TITLE));
        report.setCourse(model.isActiveCourse() ? model.getCourse().getTitle() : "");
        report.setGroup(model.isActiveGroup() ? model.getGroup().getTitle() : "");
        report.setAccountInactiveStudents(model.getAccountInactiveStudents().getTitle());

        save(report);

        return report;
    }

    @SuppressWarnings("unchecked")
    private String getValue(UniEduProgramEducationOrgUnitAddon util, UniEduProgramEducationOrgUnitAddon.Filters key, String propertyPath)
    {
        Map<UniEduProgramEducationOrgUnitAddon.Filters, Object> values = util.getValuesMap();
        return util.getFilterConfig(key).isCheckEnableCheckBox() ? UniStringUtils.join((Collection<IEntity>) values.get(key), propertyPath, ", ") : "";
    }

    /**
     * Подготавливает массив данных для отчета. Применяются фильтры с формы.
     *
     * @return {Student, EduProgramSubject, CtrPaymentResult}
     */
    @SuppressWarnings("unchecked")
    protected <M extends Model> Map<Long, MultiKey> getContractMap(final M model)
    {
        // получаем список id студентов, удовлетворяющих фильтрам на форме и подразделению, если отчет строится с подразделения
        final List<Long> studentIds = Lists.newLinkedList();
        final DQLSelectBuilder studentBuilder = new DQLSelectBuilder().fromEntity(Student.class, "s")
                .column(property("s"));

        if(model.getAccountInactiveStudents() != null && !TwinComboDataSourceHandler.getSelectedValue(model.getAccountInactiveStudents()))
        {
            studentBuilder.where(eq(property("s", Student.archival()), value(Boolean.FALSE)))
                    .joinEntity("s", DQLJoinType.left, Group.class, "gr", eq(property("gr"), property("s", Student.group())))
                    .where(eq(property("s", Student.status().active()), value(Boolean.TRUE)))
                    .where(or(
                            eq(property("gr", Group.archival()), value(Boolean.FALSE)),
                            isNull(property("gr")))
                    );
        }

        if (model.getOrgUnitId() != null)
        {
            studentBuilder.where(or(
                    eq(property("s", Student.educationOrgUnit().formativeOrgUnit().id()), value(model.getOrgUnitId())),
                    eq(property("s", Student.educationOrgUnit().territorialOrgUnit().id()), value(model.getOrgUnitId()))
            ));
        }

        if (model.isActiveFieldSetStudent())
        {
            if (model.isActiveGroup())
                studentBuilder.where(eq(property("s", Student.group()), value(model.getGroup())));

            if (model.isActiveCourse())
                studentBuilder.where(eq(property("s", Student.course()), value(model.getCourse())));

            BatchUtils.execute(model.getUtil().getEducationOrgUnitFilteredList(), DQL.MAX_VALUES_ROW_NUMBER, elements ->
                    studentIds.addAll(new DQLSelectBuilder().fromDataSource(studentBuilder.buildQuery(), "b")
                                              .column(property(Student.id().fromAlias("b.s")))
                                              .where(in(property(Student.educationOrgUnit().fromAlias("b.s")), elements))
                                              .createStatement(getSession()).<Long>list())
            );
        }
        else
            studentIds.addAll(ids(studentBuilder.createStatement(getSession()).<Student>list()));

        // получаем список id договоров, для которых есть EduCtrStudentContract из списка студентов полученых выше
        final Map<Long, Student> contractStudentIdMap = getStudentContractRel(studentIds);

        // получаем действующие на текущую дату версии договоров, id договоров которых получены выше
        Map<Long, CtrContractVersion> contractVersionMap = CtrContractVersionManager.instance().dao().getCurrentVersions(contractStudentIdMap.keySet(), new Date());

        // получаем версии, где контрагент в роли заказчика, для версий договоров полученых выше
        final List<Long> contractVersionContractorList = Lists.newArrayList();
        BatchUtils.execute(ids(contractVersionMap.values()), DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            DQLSelectBuilder contractRole = new DQLSelectBuilder().fromEntity(CtrContractVersionContractorRole.class, "r")
                    .column(property("r", CtrContractVersionContractorRole.id()))
                    .where(eq(property("r", CtrContractVersionContractorRole.contactor().id()), property("vc", CtrContractVersionContractor.id())))
                    .where(eq(property("r", CtrContractVersionContractorRole.role().code()), value(CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER)));

            if (model.isActiveContactorType())
            {
                ContactorCategoryMeta meta = ContactorCategoryMeta.getMetaByUniqueCode(model.getContactorType().getId());
                contractRole.joinEntity("r", DQLJoinType.inner, meta.getContactorCategoryClass(), "c", eq(property("vc", CtrContractVersionContractor.contactor().id()), property("c", ContactorPerson.id())));
            }

            DQLSelectBuilder contractorBuilder = new DQLSelectBuilder().fromEntity(CtrContractVersionContractor.class, "vc")
                    .column(property("vc", CtrContractVersionContractor.owner().id())).distinct()
                    .where(exists(contractRole.buildQuery()))
                    .where(in(property("vc", CtrContractVersionContractor.owner().id()), elements));

            for (Long versionId : contractorBuilder.createStatement(getSession()).<Long>list())
                contractVersionContractorList.add(versionId);
        });

        final Map<Long, MultiKey> resultMap = Maps.newHashMap();
        BatchUtils.execute(contractVersionContractorList, DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                    new DQLSelectBuilder().fromEntity(EduCtrEducationPromise.class, "p")
                            .joinPath(DQLJoinType.inner, EduCtrEducationPromise.src().owner().fromAlias("p"), "v")
                            .joinPath(DQLJoinType.inner, EduCtrEducationPromise.eduProgram().fromAlias("p"), "edu")
                            .where(in(property("v.id"), elements))
            );
            int contract_id_col = dql.column(property("v", CtrContractVersion.contract().id()));
            int program_subject_id_col = dql.column(property("edu", EduProgramProf.programSubject()));
            DQLSelectBuilder eduPromisesBuilder = dql.getDql();

            if (model.isActiveProgramSubject())
                eduPromisesBuilder.where(in(property("edu", EduProgramProf.programSubject()), model.getProgramSubjectList()));

            if (model.isActiveProgramForm())
                eduPromisesBuilder.where(in(property("edu", EduProgramProf.form()), model.getProgramFormList()));

            if (model.isActiveEduBeginYear())
                eduPromisesBuilder.where(in(property("edu", EduProgramProf.year()), model.getEduBeginYearList()));

            if (model.isActiveProgramTrait())
                eduPromisesBuilder.where(in(property("edu", EduProgramProf.eduProgramTrait()), model.getProgramTraitList()));

            if (model.isActiveEduProgram())
                eduPromisesBuilder.where(in(property("edu"), model.getEduProgramList()));

            for (Object[] row : eduPromisesBuilder.createStatement(getSession()).<Object[]>list())
            {
                Long contractId = (Long) row[contract_id_col];

                if (!resultMap.containsKey(contractId))
                {
                    Student student = contractStudentIdMap.get(contractId);
                    EduProgramSubject subject = (EduProgramSubject) row[program_subject_id_col];
                    resultMap.put(contractId, new MultiKey(student, subject));
                }
            }
        });

        return resultMap;

    }

    protected Map<Long, Student> getStudentContractRel(List<Long> studentIds)
    {
        final Map<Long, Student> contractStudentMap = Maps.newHashMap();

        BatchUtils.execute(studentIds, DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                    new DQLSelectBuilder().fromEntity(EduCtrStudentContract.class, "sc")
                            .where(in(property("sc", EduCtrStudentContract.student().id()), elements))
            );
            int contract_id_col = dql.column(property("sc", EduCtrStudentContract.contractObject().id()));
            int student_col = dql.column(property("sc", EduCtrStudentContract.student()));

            for (Object[] row : dql.getDql().createStatement(getSession()).<Object[]>list())
            {
                contractStudentMap.put((Long) row[contract_id_col], (Student) row[student_col]);
            }
        });
        return contractStudentMap;
    }

    @SuppressWarnings("unchecked")
    protected <M extends Model> RtfTableModifier injectTable(RtfTableModifier tm, RtfInjectModifier im, M model)
    {
        Map<Long, MultiKey> dataMap = getContractMap(model);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CtrPaymentResult.class, "r")
                .where(in(property("r", CtrPaymentResult.contract().id()), dataMap.keySet()));

        if (null != model.getPeriodDateFrom())
        {
            builder.where(ge(property("r", CtrPaymentResult.timestamp()), valueDate(model.getPeriodDateFrom())));
        }
        if (null != model.getPeriodDateTo())
        {
            builder.where(le(property("r", CtrPaymentResult.timestamp()), valueDate(model.getPeriodDateTo())));
        }

        List<CtrPaymentResult> resultList = builder.createStatement(getSession()).list();
        if (resultList.isEmpty())
        {
            im.put("total", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(0.0));
            tm.put("T", new String[][]{});
            return tm;
        }

        Comparator<MultiKey> paymentSort = (k1, k2) -> {
            Student s1 = (Student) k1.getKey(0);
            Student s2 = (Student) k2.getKey(0);

            CtrPaymentResult p1 = (CtrPaymentResult) k1.getKey(2);
            CtrPaymentResult p2 = (CtrPaymentResult) k2.getKey(2);

            int result = p1.getTimestamp().compareTo(p2.getTimestamp());
            if (result != 0) return result;
            return Student.FULL_FIO_AND_ID_COMPARATOR.compare(s1, s2);
        };

        Map<OrgUnit, List<MultiKey>> paymentOrgUnitMap = Maps.newHashMap();
        for (CtrPaymentResult payment : resultList)
        {
            CtrContractObject contract = payment.getContract();
            Long contractId = contract.getId();
            OrgUnit orgUnit = contract.getOrgUnit();
            MultiKey keys = dataMap.get(contractId);

            if (!paymentOrgUnitMap.containsKey(orgUnit))
                paymentOrgUnitMap.put(orgUnit, Lists.<MultiKey>newLinkedList());

            paymentOrgUnitMap.get(orgUnit).add(new MultiKey(keys.getKey(0), keys.getKey(1), payment));
            Collections.sort(paymentOrgUnitMap.get(orgUnit), paymentSort);
        }

        int number = 0;
        double total = 0.0;
        List<String[]> lineList = Lists.newLinkedList();
        final List<Integer> orgUnitRowIndexList = Lists.newArrayList();
        final List<Integer> totalRowIndexList = Lists.newArrayList();
        Set<OrgUnit> orgUnitList = paymentOrgUnitMap.keySet();

        for (OrgUnit orgUnit : orgUnitList)
        {
            List<MultiKey> paymentList = paymentOrgUnitMap.get(orgUnit);

            if (orgUnitList.size() > 1)
            {
                String[] orgUnitLine = new String[1];
                orgUnitLine[0] = orgUnit.getTitle();
                lineList.add(orgUnitLine);
                orgUnitRowIndexList.add(number++);
            }

            double orgUnitTotal = 0.0;
            for (MultiKey key : paymentList)
            {
                Student student = (Student) key.getKey(0);
                EduProgramSubject subject = (EduProgramSubject) key.getKey(1);
                CtrPaymentResult payment = (CtrPaymentResult) key.getKey(2);
                if(payment.getSrc() instanceof EmployeePostContactor)
                    orgUnitTotal += -1 * payment.getCostAsDouble();
                else
                    orgUnitTotal += payment.getCostAsDouble();

                CtrPaymentDocumentType documentType = payment.getPaymentDocumentType();
                String docNumber = payment.getPaymentDocumentNumber();
                String docTitle = documentType == null ? "№" + StringUtils.trimToEmpty(docNumber) : documentType.getTitle() + " №" + StringUtils.trimToEmpty(docNumber);

                String[] line = new String[4];
                line[0] = docTitle + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(payment.getTimestamp());
                line[1] = student.getFullFio();
                line[2] = subject.getTitleWithCode();
                line[3] = (payment.getSrc() instanceof EmployeePostContactor ? "-" : "") + payment.getCostAsString();

                lineList.add(line);
                number++;
            }

            if (orgUnitList.size() > 1)
            {
                String[] totalLine = new String[4];
                totalLine[0] = "ИТОГО";
                totalLine[3] = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(orgUnitTotal);
                lineList.add(totalLine);
                totalRowIndexList.add(number++);
            }
            total += orgUnitTotal;
        }

        im.put("total", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(total));

        tm.put("T", lineList.toArray(new String[lineList.size()][4]));
        tm.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                // в тех строках, где будет объединение ячеек нужно выделять болдом
                if (orgUnitRowIndexList.contains(rowIndex))
                {
                    SharedRtfUtil.setCellAlignment(cell, IRtfData.QC);
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                }
                if (totalRowIndexList.contains(rowIndex) && colIndex == 0)
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                return null;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                // объединяем все строки с нужными индексами
                for (Integer rowIndex : orgUnitRowIndexList)
                {
                    RtfRow row = newRowList.get(rowIndex + startIndex);
                    RtfUtil.unitAllCell(row, 0);
                }
            }
        });
        return tm;
    }

    protected <M extends Model> RtfInjectModifier injectModifier(RtfInjectModifier modifier, M model)
    {
        OrgUnit orgUnit = model.getOrgUnitId() == null ? null : (OrgUnit) getNotNull(model.getOrgUnitId());

        return modifier
                .put("periodReport", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getPeriodDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getPeriodDateTo()))
                .put("scope", orgUnit == null ? "ИТОГО по всей обр. организации" : "ИТОГО по подразделению (" + orgUnit.getTitle() + ")");
    }
}
