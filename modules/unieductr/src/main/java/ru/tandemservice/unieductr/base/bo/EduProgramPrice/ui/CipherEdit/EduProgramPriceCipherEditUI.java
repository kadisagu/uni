package ru.tandemservice.unieductr.base.bo.EduProgramPrice.ui.CipherEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;

import ru.tandemservice.unieductr.base.entity.EduProgramPrice;

@Input({
    @Bind(key=UIPresenter.PUBLISHER_ID, binding="holder.id", required=true)
})
public class EduProgramPriceCipherEditUI extends UIPresenter
{

    private final EntityHolder<EduProgramPrice> holder = new EntityHolder<>();
    public EntityHolder<EduProgramPrice> getHolder() { return this.holder; }
    public EduProgramPrice getEduProgramPrice() { return this.getHolder().getValue(); }

    @Override
    public void onComponentRefresh() {
        this.getHolder().refresh(EduProgramPrice.class);
    }

    public void onClickApply() {
        DataAccessServices.dao().save(this.getEduProgramPrice());
        this.deactivate();
    }
}
