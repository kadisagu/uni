package ru.tandemservice.unieductr.base.bo.EduContract.logic;

import java.util.Map;

import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice;
import org.tandemframework.shared.ctr.catalog.entity.CtrTemplateScriptItem;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrTemplateScriptItemCodes;

/**
 * @author vdanilov
 */
public class EduContractPrintDAO extends UniBaseDao implements IEduContractPrintDAO {

    @Override
    public IDocumentRenderer print(CtrContractVersion contract)
    {
        CtrContractVersionTemplateData templateData = get(CtrContractVersionTemplateData.class, CtrContractVersionTemplateData.owner(), contract);
        if (null == templateData || templateData.getOwner().getPrintTemplate() == null) {
            throw new ApplicationException("Версия разрешена к редактированию в свободной форме, и не может быть напечатана из системы по шаблону. Создайте печатную версию вне системы и воспользуйтесь опцией «Загрузить файл», или удалите версию и создайте вновь с использованием шаблона.");
        }
        return templateData.getManager().dao().print(templateData);
    }


    @Override
    public IDocumentRenderer printPromiseQuittance(CtrPaymentPromice payPromise)
    {
        final CtrTemplateScriptItem template = this.getCatalogItem(CtrTemplateScriptItem.class, CtrTemplateScriptItemCodes.EDU_CONTRACT_PAYMENT_QUITTANCE);
        final Map<String, Object> result = CommonManager.instance().scriptDao().getScriptResult(template, payPromise.getId());
        return (IDocumentRenderer)result.get(IScriptExecutor.RENDERER);
    }

}
