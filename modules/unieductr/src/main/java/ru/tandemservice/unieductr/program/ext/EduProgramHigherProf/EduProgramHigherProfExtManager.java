/* $Id$ */
package ru.tandemservice.unieductr.program.ext.EduProgramHigherProf;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author nvankov
 * @since 2/26/14
 */
@Configuration
public class EduProgramHigherProfExtManager extends BusinessObjectExtensionManager
{
}
