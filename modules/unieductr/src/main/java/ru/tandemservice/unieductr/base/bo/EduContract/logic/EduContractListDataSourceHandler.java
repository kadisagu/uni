package ru.tandemservice.unieductr.base.bo.EduContract.logic;

import org.apache.commons.collections15.Predicate;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectQuery;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contract.*;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.catalog.entity.CtrPriceCategory;
import org.tandemframework.shared.ctr.catalog.entity.CtrPricePaymentGrid;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.base.bo.EduContract.ui.GlobalList.EduContractGlobalList;
import ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;
import ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData;
import ru.tandemservice.unieductr.base.entity.gen.IEducationContractVersionTemplateDataGen;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.unieductr.student.entity.IEduStudentContract;
import ru.tandemservice.unieductr.student.entity.gen.IEduStudentContractGen;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * CtrContractVersion
 * @author vdanilov
 */
public class EduContractListDataSourceHandler extends BaseEduContractListDataSourceHandler {

    // связи со студентами (наследники IEduContractRelation)
    public static final String VIEW_STUDENT_REL_LIST = BaseEduContractListDataSourceHandler.VIEW_REL_LIST;
    public static final String VIEW_STUDENT_REL = BaseEduContractListDataSourceHandler.VIEW_REL;

    // обязательства по обчению (только те, которые наследуются от IEducationPromise)
    public static final String VIEW_EDU_PROMISE_LIST = BaseEduContractListDataSourceHandler.VIEW_EDU_PROMISE_LIST;
    public static final String VIEW_EDU_PROMISE = BaseEduContractListDataSourceHandler.VIEW_EDU_PROMISE;

    // роли
    public static final String VIEW_ROLE_LIST = BaseEduContractListDataSourceHandler.VIEW_ROLE_LIST;
    public static final String VIEW_ROLE_CUSTOMER = BaseEduContractListDataSourceHandler.VIEW_ROLE_CUSTOMER;
    public static final String VIEW_ROLE_PROVIDER = BaseEduContractListDataSourceHandler.VIEW_ROLE_PROVIDER;

    // шаблон (только те, которые наследуются от IEducationContractVersionTemplateData)
    public static final String VIEW_TEMPLATE = BaseEduContractListDataSourceHandler.VIEW_TEMPLATE;

    // показывать только последнюю активную версию договора
    // если параметр не задан, его значение FALSE
    public static final String PARAM_SINGLE_ACTIVE_VERSION_MODE = "singleActiveVersionMode";

    // показывать только те договоры, по которым есть активные версии (работает, только если singleActiveVersionMode=false)
    // если параметр не задан, то его значение FALSE
    public static final String PARAM_ACTIVE_CONTRACT_ONLY = "activeContractOnly";

    // скрывать не вступившие в действие версии договоров (работает, только если singleActiveVersionMode=false)
    // если параметр не задан, то его значение FALSE
    public static final String PARAM_HIDE_NEXT_VERSION = "hideNextVersion";

    // данные договора
    public static final String PARAM_CONTRACT_NUMBER = "contractNumber";
    public static final String PARAM_CONTRACT_NO_STUDENT_RELATION_ONLY = "contractNoStudentRelationOnly";
    public static final String PARAM_CONTRACT_NO_TEMPLATE_DATA = "contractNoTemplateData";

    // утиль выбора НПП студента
    public static final String PARAM_STUDENT_EDUOU_UTIL = "studentEducationOrgUnitUtil";

    // фильтры студента
    public static final String PARAM_STUDENT_FIRST_NAME = "studentFirstName";
    public static final String PARAM_STUDENT_LAST_NAME = "studentLastName";
    public static final String PARAM_STUDENT_MIDDLE_NAME = "studentMiddleName";
    public static final String PARAM_STUDENT_COURSE_LIST = "studentCourseList";
    public static final String PARAM_STUDENT_CATEGORY = "studentCategory";
    public static final String PARAM_STUDENT_STATUS_LIST = "studentStatusList";
    public static final String PARAM_STUDENT_ACTIVE = "studentActive";

    public static final String PARAM_STUDENT_OU = "studentOrgUnit";

    public static final String PARAM_DATE_FROM = "dateFrom";
    public static final String PARAM_DATE_TO = "dateTo";

    public static final String CONTACTOR_DQL_QUERY_OR_IDS = "contactorDqlQueryOrIds";

    public EduContractListDataSourceHandler(final String ownerId) {
        super(ownerId);
    }

    @Override
    protected void applyWhereConditions(final String alias, final DQLSelectBuilder dql, final ExecutionContext context) {
        super.applyWhereConditions(alias, dql, context);

        final String contractNumber = StringUtils.trimToNull(context.<String>get(PARAM_CONTRACT_NUMBER));
        if (null != contractNumber) {
            FilterUtils.applySimpleLikeFilter(dql, alias, CtrContractVersion.contract().number(), contractNumber);
        }

        final Boolean singleActiveVersionMode = context.get(PARAM_SINGLE_ACTIVE_VERSION_MODE);
        if (Boolean.TRUE.equals(singleActiveVersionMode))
        {
            dql.where(isNotNull(property(CtrContractVersion.activationDate().fromAlias(alias))));
            dql.where(isNull(property(CtrContractVersion.removalDate().fromAlias(alias))));
        }
        else
        {
            final Boolean activeContractOnly = context.get(PARAM_ACTIVE_CONTRACT_ONLY);
            if (Boolean.TRUE.equals(activeContractOnly)) {
                dql.where(in(
                    property(CtrContractVersion.contract().id().fromAlias(alias)),
                    new DQLSelectBuilder()
                    .fromEntity(CtrContractVersion.class, "tmp")
                    .column(property(CtrContractVersion.contract().id().fromAlias("tmp")))
                    .where(isNotNull(CtrContractVersion.activationDate().fromAlias("tmp")))
                    .where(isNull(CtrContractVersion.removalDate().fromAlias("tmp")))
                    .buildQuery()
                ));
            }

            final Boolean hideNextVersion = context.get(PARAM_HIDE_NEXT_VERSION);
            if (Boolean.TRUE.equals(hideNextVersion)) {
                dql.where(isNotNull(property(CtrContractVersion.activationDate().fromAlias(alias))));
            }
        }

        final Boolean noTemplateData = context.get(PARAM_CONTRACT_NO_TEMPLATE_DATA);
        if (Boolean.TRUE.equals(noTemplateData)) {
            dql.where(notExists(
                new DQLSelectBuilder()
                .fromEntity(IEducationContractVersionTemplateData.class, "tmpl")
                .column(property("tmpl.id"))
                .where(eq(property(IEducationContractVersionTemplateDataGen.owner().fromAlias("tmpl")), property(alias)))
                .buildQuery()
            ));
        }

        final Boolean noRelationOnly = context.get(PARAM_CONTRACT_NO_STUDENT_RELATION_ONLY);
        if (Boolean.TRUE.equals(noRelationOnly))
        {
            // без связей со студентами
            dql.where(notExists(
                new DQLSelectBuilder()
                .fromEntity(IEduStudentContract.class, "sRel")
                .column(property("sRel.id"))
                .where(eq(property(IEduStudentContractGen.contractObject().fromAlias("sRel")), property(CtrContractVersion.contract().fromAlias(alias))))
                .buildQuery()
            ));


        }
        else
        {
            // параметры студента (если утиль есть, то выбираем только студентов)
            final UniEduProgramEducationOrgUnitAddon util = context.get(PARAM_STUDENT_EDUOU_UTIL);
            if (null != util)
            {
                final DQLSelectBuilder studentRelationDql = new DQLSelectBuilder()
                        .fromEntity(IEduStudentContract.class, "sRel")
                        .column(property(IEduStudentContractGen.contractObject().id().fromAlias("sRel")))
                        .joinPath(DQLJoinType.inner, IEduStudentContractGen.student().fromAlias("sRel"), "st")
                        .joinPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias("st"), "sEduOu");

                util.applyFilters(studentRelationDql, "sEduOu");

                FilterUtils.applySimpleLikeFilter(studentRelationDql, "st", Student.person().identityCard().firstName(), context.<String>get(PARAM_STUDENT_FIRST_NAME));
                FilterUtils.applySimpleLikeFilter(studentRelationDql, "st", Student.person().identityCard().lastName(), context.<String>get(PARAM_STUDENT_LAST_NAME));
                FilterUtils.applySimpleLikeFilter(studentRelationDql, "st", Student.person().identityCard().middleName(), context.get(PARAM_STUDENT_MIDDLE_NAME));
                FilterUtils.applySelectFilter(studentRelationDql, "st", Student.status(), context.get(PARAM_STUDENT_STATUS_LIST));
                FilterUtils.applySelectFilter(studentRelationDql, "st", Student.course(), context.get(PARAM_STUDENT_COURSE_LIST));
                FilterUtils.applySelectFilter(studentRelationDql, "st", Student.studentCategory(), context.get(PARAM_STUDENT_CATEGORY));

                Long orgUnitId = context.get(PARAM_STUDENT_OU);
                if(orgUnitId != null)
                {
                    studentRelationDql.where(
                            or(
                                    eq(property("st", Student.educationOrgUnit().formativeOrgUnit().id()), value(orgUnitId)),
                                    eq(property("st", Student.educationOrgUnit().territorialOrgUnit().id()), value(orgUnitId))
                            ));
                }

                final DataWrapper studentActive = context.get(EduContractListDataSourceHandler.PARAM_STUDENT_ACTIVE);
                if (studentActive != null) {
                   studentRelationDql.where(eq(property("st", Student.status().active()), value(TwinComboDataSourceHandler.getSelectedValue(studentActive))));
                }

                dql.where(in(property(CtrContractVersion.contract().id().fromAlias(alias)), studentRelationDql.buildQuery()));
            }
            else
                {
                    // Выбираем id версий, у которых контрагент не является должностным лицом
                    DQLSelectBuilder nameBuilder = new DQLSelectBuilder().fromEntity(CtrContractVersionContractor.class, "contractor")
                            .joinPath(DQLJoinType.inner, CtrContractVersionContractor.owner().fromAlias("contractor"), "ctr_ver")
                            .joinEntity("contractor", DQLJoinType.left, EmployeePostContactor.class, "emp_contractor",
                                    eq(property("contractor", CtrContractVersionContractor.contactor().id()), property("emp_contractor", EmployeePostContactor.id())))
                            .where(isNull(property("emp_contractor", EmployeePostContactor.id())))
                            .column(property("ctr_ver", CtrContractVersion.contract().id()));

                    final String lastName = context.get(PARAM_STUDENT_LAST_NAME);
                    final String firstName = context.get(PARAM_STUDENT_FIRST_NAME);
                    final String middleName = context.get(PARAM_STUDENT_MIDDLE_NAME);

                    if (StringUtils.isNotEmpty(lastName))
                        nameBuilder.where(likeUpper(CtrContractVersionContractor.contactor().person().identityCard().lastName().fromAlias("contractor"), value(CoreStringUtils.escapeLike(lastName, true))));
                    if (StringUtils.isNotEmpty(firstName))
                        nameBuilder.where(likeUpper(CtrContractVersionContractor.contactor().person().identityCard().firstName().fromAlias("contractor"), value(CoreStringUtils.escapeLike(firstName, true))));
                    if (StringUtils.isNotEmpty(middleName))
                        nameBuilder.where(likeUpper(CtrContractVersionContractor.contactor().person().identityCard().middleName().fromAlias("contractor"), value(CoreStringUtils.escapeLike(middleName, true))));

                    dql.where(in(property(CtrContractVersion.contract().id().fromAlias(alias)), nameBuilder.buildQuery()));
                }
        }

        // только договоры на обучение
        dql.where(in(
            property(CtrContractVersion.contract().type().fromAlias(alias)),
            EduContractManager.instance().dao().getContractTypes(null)
        ));

        Date dateFrom = context.get(PARAM_DATE_FROM);
        Date dateTo = context.get(PARAM_DATE_TO);

        if (null != dateFrom || null != dateTo)
        {
            dql.where(isNull(property(alias, CtrContractVersion.removalDate())));
            dql.where(isNotNull(property(alias, CtrContractVersion.activationDate())));
            if (null != dateFrom)
                dql.where(ge(property(alias, CtrContractVersion.docStartDate()), value(dateFrom, PropertyType.DATE)));
            if (null != dateTo)
                dql.where(le(property(alias, CtrContractVersion.docStartDate()), value(dateTo, PropertyType.DATE)));
        }

        Collection<CtrPricePaymentGrid> paymentGrids = context.get(EduContractGlobalList.BIND_PAYMENT_GRID);
        Collection<CtrPriceCategory> priceCategories = context.get(EduContractGlobalList.BIND_PRICE_CATEGORY);
        Collection<CtrPriceCategory> priceSubCategories = context.get(EduContractGlobalList.BIND_PRICE_SUB_CATEGORY);

        if ((paymentGrids != null && !paymentGrids.isEmpty()) || (priceCategories != null && !priceCategories.isEmpty()) || (priceSubCategories != null && !priceSubCategories.isEmpty()))
        {
            DQLSelectBuilder versionTemplateBuilder = new DQLSelectBuilder()
                .fromEntity(EduCtrContractVersionTemplateData.class, "vt")
                .joinPath(DQLJoinType.inner, EduCtrContractVersionTemplateData.cost().fromAlias("vt"), "cost")
                .joinPath(DQLJoinType.inner, CtrPriceElementCost.category().fromAlias("cost"), "cat")
                .joinPath(DQLJoinType.left, CtrPriceCategory.parent().fromAlias("cat"), "parent_cat")
                .where(eq(property("vt", EduCtrContractVersionTemplateData.owner().id()), property(alias, CtrContractVersion.id())));

            if (paymentGrids != null && !paymentGrids.isEmpty())
            {
                versionTemplateBuilder
                    .where(in(property("cost", CtrPriceElementCost.paymentGrid()), paymentGrids));
            }

            if (priceSubCategories != null && !priceSubCategories.isEmpty())
            {
                versionTemplateBuilder
                    .where(in(property("cat"), priceSubCategories));

            } else if (priceCategories != null && !priceCategories.isEmpty())
            {
                versionTemplateBuilder
                    .where(or(
                        in(property("cat"), priceCategories),
                        in(property("parent_cat"), priceCategories)));
            }

            dql.where(exists(versionTemplateBuilder.buildQuery()));
        }

        Object contactorDqlQueryOrIds = context.get(CONTACTOR_DQL_QUERY_OR_IDS);
        if(contactorDqlQueryOrIds != null)
        {
            DQLSelectBuilder filterContactorBuilder = new DQLSelectBuilder()
                    .fromEntity(CtrContractVersionContractorRole.class, "role")
                    .where(eq(property(CtrContractVersionContractorRole.contactor().owner().id().fromAlias("role")), property(alias)))
                    .where(eq(property(CtrContractVersionContractorRole.role().code().fromAlias("role")), value(CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER)));

            if (contactorDqlQueryOrIds instanceof Long[])
            {
                List<Long> ids = new ArrayList<>(Arrays.<Long>asList((Long[]) contactorDqlQueryOrIds));
                if(ids.size() > 100)
                    throw new RuntimeException("Use another way!"); // Рассчитано на использование фильтров, где выбор в селекте больше 100 контрагентов маловероятен

                filterContactorBuilder.where(in(property(CtrContractVersionContractorRole.contactor().contactor().id().fromAlias("role")), ids));
            }
            else
            {
                filterContactorBuilder.where(in(property(CtrContractVersionContractorRole.contactor().contactor().id().fromAlias("role")), (IDQLSelectQuery) contactorDqlQueryOrIds));
            }
            dql.where(exists(filterContactorBuilder.buildQuery()));
        }
    }

    @Override
    protected DSOutput execute(final DSInput input, final ExecutionContext context) {
        final DSOutput output = super.execute(input, context);
        final List<Long> recordIds = output.getRecordIds();

        final Predicate<IEduContractRelation> predicate = context.get(VIEW_REL_PREDICATE);

        // список связей для договоров
        // contractObject.id -> { relation }
        final Map<Long, List<IEduContractRelation>> contractRelationListMap = getContractRelationListMap(context, recordIds, IEduContractRelation.class, predicate);

        // обязательства по версиям
        // version.id -> { promise }
        final Map<Long, List<CtrContractPromice>> promiseListMap = getContractVersionPromiseListMap(context, recordIds);

        // роли
        // version.id -> { role }
        final Map<Long, List<CtrContractVersionContractorRole>> roleListMap = getContractVersionRoleListMap(context, recordIds);

        // шаблоны по версиям (только шаблоны на обучение)
        // version.id -> template
        final Map<Long, IEducationContractVersionTemplateData> templateMap = getContractVersionEduTemplateMap(context, recordIds);

        // поехали

        return output.transform((CtrContractVersion version) -> {
            final DataWrapper wrapper = new DataWrapper(version);
            setupEduContractRelationList(wrapper, contractRelationListMap);
            setupEducationPromiseList(wrapper, promiseListMap);
            setupRoleList(wrapper, roleListMap);
            setupTemplate(wrapper, templateMap);
            return wrapper;
        });
    }

}
