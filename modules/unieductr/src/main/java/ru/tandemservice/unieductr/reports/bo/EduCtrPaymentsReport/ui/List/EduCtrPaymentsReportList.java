/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrPaymentsReport.ui.List;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unieductr.reports.bo.EduCtrPaymentsReport.ui.Pub.EduCtrPaymentsReportPub;
import ru.tandemservice.unieductr.reports.entity.EduCtrPaymentsReport;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 17.04.2015
 */
@Configuration
public class EduCtrPaymentsReportList extends BusinessComponentManager
{
    private DQLOrderDescriptionRegistry _orderDescriptionRegistry = new DQLOrderDescriptionRegistry(EduCtrPaymentsReport.class, "r");

    public static final String REPORT_LIST_DS = "reportListDS";
    public static final String COLUMN_PERIOD = "period";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REPORT_LIST_DS, columnListHandler(), reportListDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> reportListDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName(), EduCtrPaymentsReport.class)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Long orgUnitId = context.get(EduCtrPaymentsReportListUI.PROP_ORG_UNIT_ID);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EduCtrPaymentsReport.class, "r");

                if (null != orgUnitId)
                    builder.where(eq(property("r", EduCtrPaymentsReport.orgUnit().id()), value(orgUnitId)));

                _orderDescriptionRegistry.applyOrder(builder, input.getEntityOrder());

                List<DataWrapper> resultList = Lists.newArrayList();
                for (EduCtrPaymentsReport report : builder.createStatement(context.getSession()).<EduCtrPaymentsReport>list())
                {
                    DataWrapper dataWrapper = new DataWrapper(report);
                    dataWrapper.setProperty(COLUMN_PERIOD, DateFormatter.DEFAULT_DATE_FORMATTER.format(report.getPeriodDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(report.getPeriodDateTo()));
                    resultList.add(dataWrapper);
                }

                return ListOutputBuilder.get(input, resultList).pageable(true).build();
            }
        };
    }

    @Bean
    public ColumnListExtPoint columnListHandler()
    {
        return columnListExtPointBuilder(REPORT_LIST_DS)
                .addColumn(indicatorColumn("report").defaultIndicatorItem(new IndicatorColumn.Item("report")))
                .addColumn(publisherColumn(EduCtrPaymentsReport.P_FORMING_DATE, EduCtrPaymentsReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).businessComponent(EduCtrPaymentsReportPub.class).order())
                .addColumn(textColumn(COLUMN_PERIOD, COLUMN_PERIOD))
                .addColumn(actionColumn("print", new Icon("printer"), "onClickPrint"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon("delete"), DELETE_LISTENER).permissionKey("ui:deleteStorableReportPermissionKey")
                                   .alert(FormattedMessage.with().template("reportListDS.delete.alert").parameter(EduCtrPaymentsReport.formingDate().s(), DateFormatter.DATE_FORMATTER_WITH_TIME).create()))
                .create();
    }
}
