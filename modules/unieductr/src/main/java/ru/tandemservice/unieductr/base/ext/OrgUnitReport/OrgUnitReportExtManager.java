/* $Id:$ */
package ru.tandemservice.unieductr.base.ext.OrgUnitReport;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.OrgUnitReportManager;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportBlockDefinition;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.StudentReportPersonAddUI;
import ru.tandemservice.uni.base.ext.ReportPerson.ui.Add.ReportPersonAddExt;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList.DefaultOrgUnitReportDefinition;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList.IOrgUnitReportBlockDefinition;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList.IOrgUnitReportDefinition;
import ru.tandemservice.uni.component.reports.FormativeAndTerritorialOrgUnitReportVisibleResolver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author rsizonenko
 * @since 05.11.2015
 */
@Configuration
public class OrgUnitReportExtManager extends BusinessObjectExtensionManager {

    /** Код блока отчетов на подразделении "Отчеты модуля «Договоры на обучение»" **/
    public static final String EDUCTR_ORG_UNIT_STUDENT_REPORT_BLOCK = "uniEductrOrgUnitStudentReportBlock";

    @Autowired
    private OrgUnitReportManager _orgUnitReportManager;


    @Bean
    public ItemListExtension<OrgUnitReportBlockDefinition> blockListExtension()
    {

        final IItemListExtensionBuilder<OrgUnitReportBlockDefinition> builder = itemListExtension(_orgUnitReportManager.blockListExtPoint());

        return builder
                .add(EDUCTR_ORG_UNIT_STUDENT_REPORT_BLOCK, new OrgUnitReportBlockDefinition("Отчеты модуля «Договоры на обучение»", EDUCTR_ORG_UNIT_STUDENT_REPORT_BLOCK, new FormativeAndTerritorialOrgUnitReportVisibleResolver()))
                .create();
    }

    @Bean
    public ItemListExtension<OrgUnitReportDefinition> reportListExtension()
    {

        final IItemListExtensionBuilder<OrgUnitReportDefinition> itemList = itemListExtension(_orgUnitReportManager.reportListExtPoint());

        return itemList
                .add("eduCtrDebitorsPayReportList", new OrgUnitReportDefinition("Список должников по оплате", "eduCtrDebitorsPayReportList", EDUCTR_ORG_UNIT_STUDENT_REPORT_BLOCK, "EduCtrDebitorsPayReportList", "orgUnit_viewEduCtrDebitorsPayReport"))
                .add("eduCtrPaymentsReportList", new OrgUnitReportDefinition("Список платежей", "eduCtrPaymentsReportList", EDUCTR_ORG_UNIT_STUDENT_REPORT_BLOCK, "EduCtrPaymentsReportList", "orgUnit_viewEduCtrPaymentsReport"))
                .add("eduCtrAgreementIncomeReportList", new OrgUnitReportDefinition("Помесячное поступление средств", "eduCtrAgreementIncomeReportList", EDUCTR_ORG_UNIT_STUDENT_REPORT_BLOCK, "EduCtrAgreementIncomeReportList", "orgUnit_viewEduCtrAgreementIncomeReport"))
                .add("eduCtrObligationsContractReportList", new OrgUnitReportDefinition("Обязательства по договорам студентов", "eduCtrObligationsContractReportList", EDUCTR_ORG_UNIT_STUDENT_REPORT_BLOCK, "EduCtrObligationsContractReportList", "orgUnit_viewEduCtrObligationsContractReport"))
                .create();
    }

}
