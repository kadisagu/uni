/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrObligationsContractReport.ui.Pub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.unieductr.reports.entity.EduCtrObligationsContractReport;

import java.util.Date;

/**
 * @author Ekaterina Zvereva
 * @since 18.08.2016
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "reportId", required = true)
})
public class EduCtrObligationsContractReportPubUI extends UIPresenter
{
    private Long _reportId;
    private EduCtrObligationsContractReport _report;


    @Override
    public void onComponentRefresh()
    {
        _report = DataAccessServices.dao().getNotNull(_reportId);
        setEntityMeta(EntityRuntime.getMeta(getReport().getClass()));
    }

    public void onClickPrint()
    {
        if (getReport().getContent().getContent() == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(getReport().getContent()).rtf(), true);
    }

    public String getPermissionKey()
    {
        return _report.getOrgUnit() == null ? "eduCtrObligationsContractReport" : new OrgUnitSecModel(_report.getOrgUnit()).getPermission("orgUnit_viewEduCtrObligationsContractReport");
    }

    @Override
    public ISecured getSecuredObject()
    {
        return _report.getOrgUnit() != null ? _report.getOrgUnit() : super.getSecuredObject();
    }

    // Getters & Setters

    public Long getReportId()
    {
        return _reportId;
    }

    public void setReportId(Long reportId)
    {
        _reportId = reportId;
    }

    public EduCtrObligationsContractReport getReport()
    {
        return _report;
    }

    public void setReport(EduCtrObligationsContractReport report)
    {
        _report = report;
    }

    private IEntityMeta entityMeta;

    private String property;


    // presenter

    public String getPropertyValue() {
        Object propertyValue = getReport().getProperty(getProperty());
        if (null == propertyValue)
            return null;
        if (propertyValue instanceof String)
            return (String) propertyValue;
        if (propertyValue instanceof ITitled)
            return ((ITitled) propertyValue).getTitle();
        if (propertyValue instanceof Date)
            return DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) propertyValue);
        throw new IllegalArgumentException();
    }

    public boolean isShowProperty() { return !StringUtils.isEmpty(getPropertyValue()); }
    public String getPropertyName() { return getEntityMeta().getProperty(getProperty()).getTitle(); }

    public String getProperty()
    {
        return property;
    }

    public void setProperty(String property)
    {
        this.property = property;
    }

    public IEntityMeta getEntityMeta()
    {
        return entityMeta;
    }

    public void setEntityMeta(IEntityMeta entityMeta)
    {
        this.entityMeta = entityMeta;
    }
}