/* $Id$ */
package ru.tandemservice.unieductr.base.bo.EduProgramPrice.logic;

import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ClassUtils;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.ctr.catalog.entity.gen.CtrContractTypeGen;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.unieductr.IUniEduCtrDefines;
import ru.tandemservice.unieductr.base.bo.EduProgramContract.EduProgramContractManager;
import ru.tandemservice.unieductr.base.entity.EduProgramPrice;

import java.util.Map;

/**
 * @author nvankov
 * @since 2/26/14
 */
@Transactional
public class EduProgramPriceDao extends UniBaseDao implements IEduProgramPriceDao
{
    @Override
    public EduProgramPrice doGetEduProgramPrice(Long eduProgramId)
    {
        Session session = lock("eduProgramPrice@"+eduProgramId);
        EduProgram eduProgram = (EduProgram) session.load(EduProgram.class, eduProgramId);
        EduProgramPrice price = getByNaturalId(new EduProgramPrice.NaturalId(eduProgram));
        if(null == price)
        {
            price = new EduProgramPrice();
            price.setProgram(eduProgram);
            session.save(price);
        }
        return price;
    }

    @Override
    public CtrContractType getEduProgramContractRootType() {
        return this.getByNaturalId(new CtrContractTypeGen.NaturalId(IUniEduCtrDefines.CTR_CONTRACT_TYPE_EDU_PROGRAM));
    }

    @Override
    public CtrContractType getEduPriceContractType(EduProgramPrice eduProgramPrice)
    {
        final CtrContractType eduProgramContractRootType = getEduProgramContractRootType();

        Map<String, String> classMap = EduProgramContractManager.instance().eduProgramClassContractTypeExtPoint().getItems();

        // берем код для класса направления подготовки, если кода нет - возвращаем eppRoot
        final EduProgram eduProgram = eduProgramPrice.getProgram();
        Class<?> klass = ClassUtils.getUserClass(eduProgram);
        while (null != klass) {
            final String code = classMap.get(klass.getName());
            if (null != code)
            {
                // по коду берем элемент справочника, если нет - возвращаем eppRoot
                final CtrContractType type = this.getByNaturalId(new CtrContractTypeGen.NaturalId(code));
                if (null == type) { return eduProgramContractRootType; }

                // проверяем, что то, что мы взяли - содержится в ветке eppRoot
                CtrContractType tp = type;
                while (null != tp) {
                    if (tp.equals(eduProgramContractRootType)) { return type; }
                    tp = tp.getParent();
                }
                return eduProgramContractRootType;
            }

            // пробуем для другого класса
            klass = klass.getSuperclass();
        }

        // если так ничего и не нашли - то возвращаем eppRoot
        return eduProgramContractRootType;
    }
}
