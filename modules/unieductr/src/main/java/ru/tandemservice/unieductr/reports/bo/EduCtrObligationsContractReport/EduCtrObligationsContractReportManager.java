/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrObligationsContractReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unieductr.reports.bo.EduCtrObligationsContractReport.logic.EduCtrObligationsContractReportDao;
import ru.tandemservice.unieductr.reports.bo.EduCtrObligationsContractReport.logic.IEduCtrObligationsContractReportDao;

/**
 * @author Ekaterina Zvereva
 * @since 18.08.2016
 */
@Configuration
public class EduCtrObligationsContractReportManager extends BusinessObjectManager
{
    public static EduCtrObligationsContractReportManager instance()
    {
        return instance(EduCtrObligationsContractReportManager.class);
    }

    @Bean
    public IEduCtrObligationsContractReportDao dao()
    {
        return new EduCtrObligationsContractReportDao();
    }
}