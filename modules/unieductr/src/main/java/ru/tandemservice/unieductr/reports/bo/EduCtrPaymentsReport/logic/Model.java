/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrPaymentsReport.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;

import java.util.Date;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 17.04.2015
 */
public class Model
{
    private Long _orgUnitId;
    private Date _formingDate = new Date();

    private Date _periodDateFrom;
    private Date _periodDateTo;

    private boolean _activeContactorType;
    private DataWrapper _contactorType;

    private boolean _activeProgramSubject;
    private boolean _activeProgramForm;
    private boolean _activeEduBeginYear;
    private boolean _activeProgramTrait;
    private boolean _activeEduProgram;
    private List<EduProgramSubject> _programSubjectList;
    private List<EduProgramForm> _programFormList;
    private List<EducationYear> _eduBeginYearList;
    private List<EduProgramTrait> _programTraitList;
    private List<EduProgramProf> _eduProgramList;

    private boolean _activeFieldSetStudent;
    private boolean _activeCourse;
    private boolean _activeGroup;
    private Course _course;
    private Group _group;
    private UniEduProgramEducationOrgUnitAddon _util;
    private DataWrapper _accountInactiveStudents;

    // Getters & Setters

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public Date getFormingDate()
    {
        return _formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        _formingDate = formingDate;
    }

    public Date getPeriodDateFrom()
    {
        return _periodDateFrom;
    }

    public void setPeriodDateFrom(Date periodDateFrom)
    {
        _periodDateFrom = periodDateFrom;
    }

    public Date getPeriodDateTo()
    {
        return _periodDateTo;
    }

    public void setPeriodDateTo(Date periodDateTo)
    {
        _periodDateTo = periodDateTo;
    }

    public boolean isActiveContactorType()
    {
        return _activeContactorType;
    }

    public void setActiveContactorType(boolean activeContactorType)
    {
        _activeContactorType = activeContactorType;
    }

    public DataWrapper getContactorType()
    {
        return _contactorType;
    }

    public void setContactorType(DataWrapper contactorType)
    {
        _contactorType = contactorType;
    }

    public boolean isActiveProgramSubject()
    {
        return _activeProgramSubject;
    }

    public void setActiveProgramSubject(boolean activeProgramSubject)
    {
        _activeProgramSubject = activeProgramSubject;
    }

    public boolean isActiveProgramForm()
    {
        return _activeProgramForm;
    }

    public void setActiveProgramForm(boolean activeProgramForm)
    {
        _activeProgramForm = activeProgramForm;
    }

    public boolean isActiveEduBeginYear()
    {
        return _activeEduBeginYear;
    }

    public void setActiveEduBeginYear(boolean activeEduBeginYear)
    {
        _activeEduBeginYear = activeEduBeginYear;
    }

    public boolean isActiveProgramTrait()
    {
        return _activeProgramTrait;
    }

    public void setActiveProgramTrait(boolean activeProgramTrait)
    {
        _activeProgramTrait = activeProgramTrait;
    }

    public boolean isActiveEduProgram()
    {
        return _activeEduProgram;
    }

    public void setActiveEduProgram(boolean activeEduProgram)
    {
        _activeEduProgram = activeEduProgram;
    }

    public List<EduProgramSubject> getProgramSubjectList()
    {
        return _programSubjectList;
    }

    public void setProgramSubjectList(List<EduProgramSubject> programSubjectList)
    {
        _programSubjectList = programSubjectList;
    }

    public List<EduProgramForm> getProgramFormList()
    {
        return _programFormList;
    }

    public void setProgramFormList(List<EduProgramForm> programFormList)
    {
        _programFormList = programFormList;
    }

    public List<EducationYear> getEduBeginYearList()
    {
        return _eduBeginYearList;
    }

    public void setEduBeginYearList(List<EducationYear> eduBeginYearList)
    {
        _eduBeginYearList = eduBeginYearList;
    }

    public List<EduProgramTrait> getProgramTraitList()
    {
        return _programTraitList;
    }

    public void setProgramTraitList(List<EduProgramTrait> programTraitList)
    {
        _programTraitList = programTraitList;
    }

    public List<EduProgramProf> getEduProgramList()
    {
        return _eduProgramList;
    }

    public void setEduProgramList(List<EduProgramProf> eduProgramList)
    {
        _eduProgramList = eduProgramList;
    }

    public boolean isActiveFieldSetStudent()
    {
        return _activeFieldSetStudent;
    }

    public void setActiveFieldSetStudent(boolean activeFieldSetStudent)
    {
        _activeFieldSetStudent = activeFieldSetStudent;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public UniEduProgramEducationOrgUnitAddon getUtil()
    {
        return _util;
    }

    public void setUtil(UniEduProgramEducationOrgUnitAddon util)
    {
        _util = util;
    }

    public boolean isActiveCourse()
    {
        return _activeCourse;
    }

    public void setActiveCourse(boolean activeCourse)
    {
        _activeCourse = activeCourse;
    }

    public boolean isActiveGroup()
    {
        return _activeGroup;
    }

    public void setActiveGroup(boolean activeGroup)
    {
        _activeGroup = activeGroup;
    }

    public DataWrapper getAccountInactiveStudents()
    {
        return _accountInactiveStudents;
    }

    public void setAccountInactiveStudents(DataWrapper accountInactiveStudents)
    {
        _accountInactiveStudents = accountInactiveStudents;
    }
}
