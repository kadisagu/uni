package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.ui.AddVpo;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.StaticFullCheckSelectModel;
import org.tandemframework.shared.ctr.base.bo.Contactor.ui.PickForm.ContactorPickForm;
import org.tandemframework.shared.ctr.base.bo.Contactor.ui.PickForm.ContactorPickFormUI;
import org.tandemframework.shared.ctr.base.bo.Contactor.util.ContactorCategoryMeta;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.AddByTemplate.CtrContractVersionAddByTemplateUI;
import org.tandemframework.shared.ctr.base.bo.PhysicalContactor.ui.AddByNextOfKin.PhysicalContactorAddByNextOfKin;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;
import org.tandemframework.shared.ctr.base.entity.contactor.PhysicalContactor;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.ContractSides;
import ru.tandemservice.unieductr.base.bo.EduProgramContract.logic.EduProgramSingleStudentContractObjectFactory;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.EduCtrStudentContractTemplateManager;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.logic.IEduCtrStudentContractTemplateAddData;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.ui.AddVpo.PriceTemplate.PriceTemplateAddon;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContractTemplateData;

import java.util.*;

@Input({
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTEXT_ID, binding="studentHolder.id", required=true),
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_TYPE_ID, binding="contractTypeHolder.id", required=true),
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_KIND_ID, binding="versionCreateData.contractKindHolder.id", required=true),
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_PRINT_TEMPLATE_ID, binding="versionCreateData.printTemplateHolder.id")
})
public class EduCtrStudentContractTemplateAddVpoUI extends UIPresenter implements IEduCtrStudentContractTemplateAddData
{
    public static final String REGION_CUSTOMER_PICK = "customerRegion";

    private final EntityHolder<Student> studentHolder = new EntityHolder<>();
    public EntityHolder<Student> getStudentHolder() { return this.studentHolder; }
    @Override public Student getStudent() { return this.getStudentHolder().getValue(); }

    private final EntityHolder<CtrContractType> contractTypeHolder = new EntityHolder<>();
    public EntityHolder<CtrContractType> getContractTypeHolder() { return this.contractTypeHolder; }
    @Override public CtrContractType getContractType() { return this.getContractTypeHolder().getValue(); }

    private final CtrContractVersionCreateData versionCreateData = new CtrContractVersionCreateData();

    public CtrContractVersionCreateData getVersionCreateData()
    {
        return versionCreateData;
    }

    private EducationYear contractEducationYear;
    @Override public EducationYear getContractEducationYear() { return contractEducationYear; }
    public void setContractEducationYear(EducationYear contractEducationYear) { this.contractEducationYear = contractEducationYear; }

    private OrgUnit formativeOrgUnit;

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        this.formativeOrgUnit = formativeOrgUnit;
    }

    @Override public OrgUnit getFormativeOrgUnit() { return formativeOrgUnit; }

    private final ISelectModel providerModel = new StaticFullCheckSelectModel(ContactorPerson.fullTitle().s()) {
        @Override protected List list() {
            final OrgUnit formativeOrgUnit = EduCtrStudentContractTemplateAddVpoUI.this.getFormativeOrgUnit();
            if (null == formativeOrgUnit) { return Collections.emptyList(); }

            final Collection<ContactorPerson> presenters = EduContractManager.instance().dao().getAcademyPresenters(formativeOrgUnit);
            return new ArrayList<>(presenters);
        }
    };
    public ISelectModel getProviderModel() { return this.providerModel; }

    private ContactorPerson provider;
    @Override public ContactorPerson getProvider() { return this.provider; }
    public void setProvider(final ContactorPerson provider) { this.provider = provider; }

    private ContactorPerson customer;
    @Override public ContactorPerson getCustomer() { return this.customer; }
    public void setCustomer(final ContactorPerson customer) { this.customer = customer; }

    private Date priceDate = new Date();
    @Override public Date getPriceDate() { return this.priceDate; }
    public void setPriceDate(final Date priceDate) { this.priceDate = priceDate; }

    private Date startDate = new Date();
    @Override public Date getStartDate() { return this.startDate; }
    public void setStartDate(final Date startDate) { this.startDate = startDate; }

    private Date endDate = new Date();
    @Override public Date getEndDate() { return this.endDate; }
    public void setEndDate(final Date endDate) { this.endDate = endDate; }

    @Override public Date getEnrollmentDate() {
        // TODO: подумать, откуда ее брать
        return getStartDate();
    }

    private boolean twoSides = true;
    private boolean threeSidesPerson = false;

    public boolean isTwoSides()
    {
        return twoSides;
    }

    public void setTwoSides(boolean twoSides)
    {
        this.twoSides = twoSides;
    }

    public boolean isThreeSidesPerson()
    {
        return threeSidesPerson;
    }

    public void setThreeSidesPerson(boolean threeSidesPerson)
    {
        this.threeSidesPerson = threeSidesPerson;
    }

    @Override
    public void onComponentActivate() {
        final Student student =  this.getStudentHolder().refresh(Student.class);
        this.setCustomer(EduProgramSingleStudentContractObjectFactory.getContactPersion(student.getPerson()));
        this.setContractEducationYear(EducationYearManager.instance().dao().getCurrent());
        this.formativeOrgUnit = student.getEducationOrgUnit().getFormativeOrgUnit();

        final List list = this.getProviderModel().findValues(null).getObjects();
        if (list.size() == 1)
            this.setProvider((ContactorPerson) list.get(0));
    }

    @Override
    public void onComponentRefresh()
    {
        this.getStudentHolder().refresh(Student.class);
        this.getContractTypeHolder().refresh(CtrContractType.class);
        getVersionCreateData().doRefresh();
        getVersionCreateData().setDocStartDate(new Date());
        getVersionCreateData().setDurationBeginDate(new Date());
        ContractSides sides = EduContractManager.instance().dao().getContractSides(getVersionCreateData().getContractKind().getCode());
        if(ContractSides.TWO_SIDES.equals(sides))
        {
            setTwoSides(true);
            setThreeSidesPerson(false);
        }
        else if(ContractSides.THREE_SIDES_PERSON.equals(sides))
        {
            setTwoSides(false);
            setThreeSidesPerson(true);
        }
        else if(ContractSides.THREE_SIDES_ORG.equals(sides))
        {
            setTwoSides(false);
            setThreeSidesPerson(false);
        }

        getPriceTemplateAddon().setPriceDate(getStartDate());
        getPriceTemplateAddon().setStudent(getStudent());
    }

    @Override
    public void onComponentBindReturnParameters(final String childRegionName, final Map<String, Object> returnedData) {
        if (REGION_CUSTOMER_PICK.equals(childRegionName)) {
            final Object id = returnedData.get(ContactorPickFormUI.CONTACTOR_ID);
            if (id instanceof Long) {
                final ContactorPerson contactor = DataAccessServices.dao().get(ContactorPerson.class, (Long) id);
                if (null != contactor) { this.setCustomer(contactor); }
            }
        }
    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(EduContractManager.DS_CUSTOMER.equals(dataSource.getName()))
        {
            dataSource.put(EduContractManager.BIND_PHYSICAL_CONTRACTORS, isTwoSides() || isThreeSidesPerson());
        }
    }

    public void onChangePriceDate()
    {
        getPriceTemplateAddon().setPriceDate(getStartDate());
        getPriceTemplateAddon().getPriceSelection().refreshCostList();
    }

    public void onClickAddCustomer() {
        DataWrapper contactorType = ContactorCategoryMeta.getDataRecordMap().get(isThreeSidesPerson() ? PhysicalContactor.class.getName() : JuridicalContactor.class.getName());

        this.getActivationBuilder().asRegion(ContactorPickForm.class, REGION_CUSTOMER_PICK)
                .parameter(ContactorPickFormUI.PRE_SELECTED_CONTRACT_TYPE, contactorType)
                .activate();
    }

    public void onClickAddCustomerNextOfKin() {
        this.getActivationBuilder().asRegion(PhysicalContactorAddByNextOfKin.class, REGION_CUSTOMER_PICK).parameter(PhysicalContactorAddByNextOfKin.PERSON_ID, getStudent().getPerson().getId()).activate();
    }

    public boolean isCustomerPickerActive() {
        return (null != this.getSupport().getChildUI(REGION_CUSTOMER_PICK));
    }

    public boolean isApplyDisabled() {
        return this.isCustomerPickerActive();
    }

    public void onClickApply()
    {
        if (this.isApplyDisabled()) { return; }

        if(getUserContext().getErrorCollector().hasErrors())
            return;
        final EduCtrStudentContractTemplateData templateData = EduCtrStudentContractTemplateManager.instance().dao().doCreateVersion(this);
        this.deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, templateData.getOwner().getId()).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.TRUE));
    }

    public void onClickClose()
    {
        this.deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, null).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.TRUE));
    }

    public void onClickBack()
    {
        this.deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, null).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.FALSE));
    }

    public PriceTemplateAddon getPriceTemplateAddon()
    {
        return ((PriceTemplateAddon) this.getConfig().getAddon(PriceTemplateAddon.PRICE_TEMPLATE_ADDON));
    }

    @Override public EduProgramProf getEduProgram() { return getPriceTemplateAddon().getEduProgram(); }
    @Override public String getCipher() { return getPriceTemplateAddon().getCipher(); }
    @Override public CtrPriceElementCost getSelectedCost() { return this.getPriceTemplateAddon().getPriceSelection().getSelectedCost(); }
    @Override public List<CtrPriceElementCostStage> getSelectedCostStages() { return this.getPriceTemplateAddon().getPriceSelection().getSelectedCostStages(); }
}
