package ru.tandemservice.unieductr.student.entity;

import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentSpoContractTemplate.EduCtrStudentSpoContractTemplateManager;
import ru.tandemservice.unieductr.student.entity.gen.*;

/** @see ru.tandemservice.unieductr.student.entity.gen.EduCtrStudentSpoContractTemplateDataGen */
public class EduCtrStudentSpoContractTemplateData extends EduCtrStudentSpoContractTemplateDataGen
{
    @Override
    public ICtrContractTemplateManager getManager() {
        return EduCtrStudentSpoContractTemplateManager.instance();
    }
}