package ru.tandemservice.unieductr.component.settings.EduCtrAcademyPresenter;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * 
 * @author iolshvang
 * @since 06.06.2011
 */

public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{

}
