/* $Id:$ */
package ru.tandemservice.unieductr.ws;

import org.tandemframework.core.util.cache.SpringBeanCache;

import java.util.Date;

/**
 * @author oleyba
 * @since 4/16/14
 */
public interface IEduProgramPriceServiceDAO
{
    public static final SpringBeanCache<IEduProgramPriceServiceDAO> INSTANCE = new SpringBeanCache<IEduProgramPriceServiceDAO>(IEduProgramPriceServiceDAO.class.getName());

    public EduProgramPriceEnvironmentNode getPriceData(Date date);
}
