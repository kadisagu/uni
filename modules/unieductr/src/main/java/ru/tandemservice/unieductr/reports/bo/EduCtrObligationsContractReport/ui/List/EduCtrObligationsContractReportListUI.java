/* $Id$ */
package ru.tandemservice.unieductr.reports.bo.EduCtrObligationsContractReport.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.unieductr.reports.bo.EduCtrObligationsContractReport.ui.Add.EduCtrObligationsContractReportAdd;
import ru.tandemservice.unieductr.reports.bo.EduCtrObligationsContractReport.ui.Add.EduCtrObligationsContractReportAddUI;

/**
 * @author Ekaterina Zvereva
 * @since 18.08.2016
 */
@State({@Bind(key = EduCtrObligationsContractReportAddUI.PROP_ORGUNIT_ID, binding = "orgUnitId")})
public class EduCtrObligationsContractReportListUI extends UIPresenter
{
    public static final String PROP_ORG_UNIT_ID = "orgUnitId";
    public static final String PROP_FORMING_DATE_FROM = "formingDateFromFilter";
    public static final String PROP_FORMING_DATE_TO = "formingDateToFilter";


    private Long _orgUnitId;

    public void  onClickAddReport()
    {
        getActivationBuilder().asRegion(EduCtrObligationsContractReportAdd.class)
                .parameter("orgUnitId", _orgUnitId)
                .activate();
    }

    public void onClickPrintReport()
    {
        IStorableReport report = DataAccessServices.dao().get(getListenerParameterAsLong());
        if (report.getContent().getContent() == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(report.getContent()).rtf(), true);
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(EduCtrObligationsContractReportList.REPORT_LIST_DS))
        {
            dataSource.put(PROP_FORMING_DATE_FROM, getSettings().get(PROP_FORMING_DATE_FROM));
            dataSource.put(PROP_FORMING_DATE_TO, getSettings().get(PROP_FORMING_DATE_TO));
            dataSource.put(PROP_ORG_UNIT_ID, _orgUnitId);
        }
    }

    public String getPermissionKey()
    {
        return _orgUnitId == null ? "eduCtrObligationsContractReport" : new OrgUnitSecModel(DataAccessServices.dao().get(OrgUnit.class, _orgUnitId)).getPermission("orgUnit_viewEduCtrObligationsContractReport");
    }

    @Override
    public ISecured getSecuredObject()
    {
        return _orgUnitId != null ? DataAccessServices.dao().get(OrgUnit.class, _orgUnitId) : super.getSecuredObject();
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return new CommonPostfixPermissionModel(_orgUnitId != null ? OrgUnitSecModel.getPostfix(DataAccessServices.dao().get(OrgUnit.class, _orgUnitId)) : null);
    }

    public String getAddStorableReportPermissionKey(){ return getSec().getPermission(_orgUnitId != null ? "orgUnit_addEduCtrObligationsContractReport" : "addEduCtrStorableReport"); }


    public String getDeleteStorableReportPermissionKey(){ return getSec().getPermission(_orgUnitId != null ? "orgUnit_deleteEduCtrObligationsContractReport" : "deleteEduCtrStorableReport"); }

    // Getters & Setters

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }
}