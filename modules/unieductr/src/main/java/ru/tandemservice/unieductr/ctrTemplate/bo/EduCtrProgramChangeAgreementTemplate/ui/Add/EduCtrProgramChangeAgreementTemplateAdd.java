/* $Id$ */
package ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrProgramChangeAgreementTemplate.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.EduCtrStudentContractTemplateManager;

/**
 * @author azhebko
 * @since 09.12.2014
 */
@Configuration
public class EduCtrProgramChangeAgreementTemplateAdd extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return EduCtrStudentContractTemplateManager.programProfPresenterExtPointBuilder(this.presenterExtPointBuilder(), this.getName())
            .create();
    }
}