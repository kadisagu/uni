package ru.tandemservice.unieductr.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;

/**
 * Связь договора с обучаемым
 *
 * Интерфейс для связей обучаемого (студент, абитуриент, слушатель ДПО, путевка ДПО) с договором на обучение.
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IEduContractRelationGen extends InterfaceStubBase
 implements IEduContractRelation{
    public static final int VERSION_HASH = 460257429;

    public static final String L_CONTRACT_OBJECT = "contractObject";

    private CtrContractObject _contractObject;

    @NotNull

    public CtrContractObject getContractObject()
    {
        return _contractObject;
    }

    public void setContractObject(CtrContractObject contractObject)
    {
        _contractObject = contractObject;
    }

    private static final Path<IEduContractRelation> _dslPath = new Path<IEduContractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.unieductr.base.entity.IEduContractRelation");
    }
            

    /**
     * @return Договор на обучение. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.IEduContractRelation#getContractObject()
     */
    public static CtrContractObject.Path<CtrContractObject> contractObject()
    {
        return _dslPath.contractObject();
    }

    public static class Path<E extends IEduContractRelation> extends EntityPath<E>
    {
        private CtrContractObject.Path<CtrContractObject> _contractObject;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Договор на обучение. Свойство не может быть null.
     * @see ru.tandemservice.unieductr.base.entity.IEduContractRelation#getContractObject()
     */
        public CtrContractObject.Path<CtrContractObject> contractObject()
        {
            if(_contractObject == null )
                _contractObject = new CtrContractObject.Path<CtrContractObject>(L_CONTRACT_OBJECT, this);
            return _contractObject;
        }

        public Class getEntityClass()
        {
            return IEduContractRelation.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.unieductr.base.entity.IEduContractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
