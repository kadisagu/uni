/**
 *$Id:$
 */
package ru.tandemservice.unieductr.reports.bo.EduCtrDebitorsPayReport.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.unieductr.reports.entity.EduCtrDebitorsPayReport;

/**
 * @author Alexander Shaburov
 * @since 08.11.12
 */
@State({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "reportId", required = true)
})
public class EduCtrDebitorsPayReportPubUI extends UIPresenter
{
    private Long _reportId;
    private EduCtrDebitorsPayReport _report;

    @Override
    public void onComponentRefresh()
    {
        _report = DataAccessServices.dao().getNotNull(_reportId);
    }

    public void onClickPrint()
    {
        if (getReport().getContent().getContent() == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(getReport().getContent()).rtf(), true);
    }

    public String getPermissionKey()
    {
        return _report.getOrgUnit() == null ? "eduCtrDebitorsPayReport" : new OrgUnitSecModel(_report.getOrgUnit()).getPermission("orgUnit_viewEduCtrDebitorsPayReport");
    }

    @Override
    public ISecured getSecuredObject()
    {
        return _report.getOrgUnit() != null ? _report.getOrgUnit() : super.getSecuredObject();
    }

    // Getters & Setters

    public Long getReportId()
    {
        return _reportId;
    }

    public void setReportId(Long reportId)
    {
        _reportId = reportId;
    }

    public EduCtrDebitorsPayReport getReport()
    {
        return _report;
    }

    public void setReport(EduCtrDebitorsPayReport report)
    {
        _report = report;
    }
}
