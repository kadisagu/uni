/* $Id$ */
package ru.tandemservice.unieductr.base.ext.CtrPrice.logic;

import org.tandemframework.shared.ctr.base.bo.CtrPrice.util.ICtrPriceElementCostStageFactory;
import org.tandemframework.shared.ctr.base.entity.ICtrPriceElement;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 21.10.2011
 */
public class ElementCostStageFactoryWholePeriod implements ICtrPriceElementCostStageFactory
{
    @Override
    public List<CtrPriceElementCostStage> getStageList(ICtrPriceElement priceElement)
    {
        CtrPriceElementCostStage stage = new CtrPriceElementCostStage();
        stage.setTitle("За весь период");
        stage.setStageUniqueCode("1");

        return Collections.singletonList(stage);
    }

    @Override
    public List<CtrPriceElementCostStage> filterStageList(ICtrPriceElement priceElement, List<CtrPriceElementCostStage> stageList, Map<String, Object> filter)
    {
        return stageList;
    }

}