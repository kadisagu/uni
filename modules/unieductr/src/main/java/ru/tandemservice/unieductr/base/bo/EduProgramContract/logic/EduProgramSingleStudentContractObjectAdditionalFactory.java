package ru.tandemservice.unieductr.base.bo.EduProgramContract.logic;

import org.tandemframework.shared.ctr.base.bo.Contactor.ContactorManager;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;

import java.util.Collection;
import java.util.Collections;

/**
 * @author vdanilov
 */
public abstract class EduProgramSingleStudentContractObjectAdditionalFactory<S> extends EduProgramContractObjectAdditionalFactory<S> {

    /** @return обучаемый */
    protected abstract S getStudent();

    @Override protected Collection<S> getStudentList() {
        return Collections.singleton(getStudent());
    }

    public static ContactorPerson getContactPersion(final Person person) {
        final PersonContactData contactData = person.getContactData();
        return ContactorManager.instance().dao().savePhysicalContactor(
            person,
            contactData.getPhoneDefault(),
            contactData.getEmail(),
            false
        );
    }
}
