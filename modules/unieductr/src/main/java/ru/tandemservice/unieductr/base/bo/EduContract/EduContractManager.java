/* $Id: EduProgramContractManager.java 33788 2014-04-22 11:01:20Z azhebko $ */
package ru.tandemservice.unieductr.base.bo.EduContract;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.CAFServices;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.service.IConfigurationService;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrPromiceManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContractVersionPrinter;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;
import org.tandemframework.shared.ctr.base.entity.contactor.PhysicalContactor;
import org.tandemframework.shared.organization.sec.bo.Sec.logic.IContextSecuredComponentManager;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduContractDAO;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduContractPrintDAO;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.IEduContractDAO;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.IEduContractPrintDAO;
import ru.tandemservice.unieductr.base.bo.EduContract.ui.PaymentPromiseSection.EduContractPaymentPromiseSection;
import ru.tandemservice.unieductr.base.bo.EduContract.ui.VersionPub.EduContractVersionPub;
import ru.tandemservice.unieductr.base.bo.EduContract.ui.Wizard.EduContractWizard;
import ru.tandemservice.unieductr.base.entity.IEducationPromise;

@Configuration
public class EduContractManager extends BusinessObjectManager implements ICtrContractManager, IContextSecuredComponentManager
{
    public static final String DS_CUSTOMER = "customerDS";
    public static final String BIND_PHYSICAL_CONTRACTORS = "physicalContractors";

    public static EduContractManager instance() {
        return instance(EduContractManager.class);
    }

    @Bean
    public IEduContractDAO dao() {
        return new EduContractDAO();
    }

    @Bean
    @Override
    public ICtrContractVersionPrinter printer() {
        return new EduContractPrintDAO();
    }

    @Override
    public Class<? extends BusinessComponentManager> versionInlinePublisher() {
        return EduContractVersionPub.class;
    }

    @Override
    public Class<? extends BusinessComponentManager> versionWizard() {
        return EduContractWizard.class;
    }

    @Override
    public boolean isPromiceManagerAllowed(ICtrPromiceManager promiceManager) {

        // для обязательств на обучение секции выводятся в договорах в свободной форме (придется их показывать в правах)
        if (IEducationPromise.class.isAssignableFrom(promiceManager.promiceClass())) { return true; }

        // для всего остального - публикатор руками задает элементы на своей карточке (бузет вызван метод fillPermissionGroup)
        return false;
    }

    @Override
    public void fillPermissionGroup(PermissionGroupMeta permissionGroup, String securityPostfix) {
        final IConfigurationService configurationService = CAFServices.configurationService();

        // у нас свой собственный публикатор - в нем есть этот компонент, права будем брать из него
        final EduContractPaymentPromiseSection myPaymentPromiseSection = configurationService.getBusinessComponent(EduContractPaymentPromiseSection.class);
        myPaymentPromiseSection.fillPermissionGroup(permissionGroup, securityPostfix);
    }

    @Bean
    public SelectDSConfig customerDSConfig()
    {
        return selectDS(DS_CUSTOMER)
                .addColumn("ui.contactorTypeTitle", ContactorPerson.contactorTypeTitle().s())
                .addColumn("ui.contactorPersonFullFio", ContactorPerson.person().fullFio().s())
                .handler(this.customerDSHandler())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler customerDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), ContactorPerson.class)
                .customize(((alias, dql, context, filter) ->
                {
                    boolean isPhysicalContractors = context.getBoolean(BIND_PHYSICAL_CONTRACTORS, true);

                    if(isPhysicalContractors)
                    {
                        dql.where(DQLExpressions.exists(PhysicalContactor.class, PhysicalContactor.id().s(), DQLExpressions.property("e.id")));
                    }
                    else
                    {
                        dql.where(DQLExpressions.exists(JuridicalContactor.class, JuridicalContactor.id().s(), DQLExpressions.property("e.id")));
                    }

                    return dql;
                }))
                .order(ContactorPerson.person().identityCard().lastName())
                .order(ContactorPerson.person().identityCard().firstName())
                .order(ContactorPerson.person().identityCard().middleName())
                .filter(ContactorPerson.searchIndex());
    }
}



