package ru.tandemservice.unieductr.student.entity;

import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentAddContractTemplate.EduCtrStudentAddContractTemplateManager;
import ru.tandemservice.unieductr.student.entity.gen.*;

/** @see ru.tandemservice.unieductr.student.entity.gen.EduCtrStudentAddContractTemplateDataGen */
public class EduCtrStudentAddContractTemplateData extends EduCtrStudentAddContractTemplateDataGen
{
    @Override
    public ICtrContractTemplateManager getManager() {
        return EduCtrStudentAddContractTemplateManager.instance();
    }
}