/* $Id$ */
package ru.tandemservice.unieductr.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Andrey Andreev
 * @since 15.09.2015
 */
public class MS_unieductr_2x9x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.9.1")
                };
    }


    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.executeUpdate("UPDATE ctrpr_c_paymentgrid " +
                        "SET shorttitle_p = 'за полсеместра'" +
                        "WHERE shorttitle_p = 'за пол семестра'"
        );
    }
}