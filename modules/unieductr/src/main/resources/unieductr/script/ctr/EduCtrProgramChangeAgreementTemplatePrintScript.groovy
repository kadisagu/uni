package unieductr.script.ctr

import com.google.common.collect.Lists
import org.hibernate.Session
import org.tandemframework.core.CoreDateUtils
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationChild
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf
import ru.tandemservice.uniedu.program.entity.EduProgramProf
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractPrintUtils
import ru.tandemservice.unieductr.base.bo.EduProgramContract.EduProgramCtrTemplateUtils
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise
import ru.tandemservice.unieductr.student.entity.EduCtrProgramChangeAgreementTemplateData

return new EduCtrProgramChangeAgreementTemplatePrint(
        session: session,    // сессия
        template: template,  // шаблон
        customer: DataAccessServices.dao().get(CtrContractVersionContractor.class, customerId),
        provider: DataAccessServices.dao().get(CtrContractVersionContractor.class, providerId),
        versionTemplateData: DataAccessServices.dao().get(EduCtrProgramChangeAgreementTemplateData.class, versionTemplateDataId),
        eduPromise: DataAccessServices.dao().get(EduCtrEducationPromise.class, eduPromiseId)
).print()

class EduCtrProgramChangeAgreementTemplatePrint
{
    Session session
    byte[] template
    CtrContractVersionContractor customer
    CtrContractVersionContractor provider
    EduCtrProgramChangeAgreementTemplateData versionTemplateData
    EduCtrEducationPromise eduPromise

    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def deleteLabels = Lists.newArrayList()

    def print()
    {
        CtrContractVersion version = eduPromise.getSrc().getOwner();

        // данные версии

        EduContractPrintUtils.printVersionData(versionTemplateData, version, im);
        EduContractPrintUtils.printProviderData((EmployeePostContactor) provider.getContactor(), im);
        EduContractPrintUtils.printCustomerData(customer.getContactor(), im);
        EduContractPrintUtils.printPaymentData(im, DataAccessServices.dao().getList(CtrPaymentPromice.class, CtrPaymentPromice.dst(), provider, CtrPaymentPromice.deadlineDate().s()));
        EduContractPrintUtils.printAcademyData(im);
        EduProgramCtrTemplateUtils.printEduPromiceData(eduPromise, im);

        def firstVersion = CtrContractVersionManager.instance().dao().getFirstVersion(versionTemplateData.owner.contract)

        def contractDate = firstVersion.docStartDate
        im.put('contractDate', DateFormatter.DEFAULT_DATE_FORMATTER.format(contractDate))

        Date timestamp = version.activationDate
        CtrContractVersion prevVersion = CtrContractVersionManager.instance().dao().getPreviousVersion(version);
        Collection<EduCtrEducationPromise> eduPromises = IUniBaseDao.instance.get().getList(EduCtrEducationPromise.class, EduCtrEducationPromise.src().owner(), prevVersion);
        if (eduPromises.size() != 1)
            throw new IllegalStateException();

        im.put('programTitleOld', programTitle(eduPromises.iterator().next().eduProgram))

        eduPromises = IUniBaseDao.instance.get().getList(EduCtrEducationPromise.class, EduCtrEducationPromise.src().owner(), version)
        if (eduPromises.size() != 1)
            throw new IllegalStateException();

        im.put('programTitleNew', programTitle(eduPromises.iterator().next().eduProgram))

        // стандартные выходные параметры скрипта

        CommonBaseRenderer renderer = new CommonBaseRenderer()
                .rtf()
                .fileName("Доп. соглашение к договору ${version.getNumber()}.rtf")
                .document(UniRtfUtil.toByteArray(template, im, tm, deleteLabels));
        return [renderer: renderer]
    }

    private static String programTitle(EduProgramProf eduProgram)
    {
        String result = eduProgram.programSubject.subjectIndex.subjectTypeTitleDative + ' ' + eduProgram.programSubject.titleWithCode
        if (eduProgram instanceof EduProgramHigherProf && eduProgram.programSpecialization instanceof EduProgramSpecializationChild)
            result += ', ' + eduProgram.programSubject.subjectIndex.specializationTypeTitleDative + ' ' + eduProgram.programSpecialization.title

        return result
    }
}

