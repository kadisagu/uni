package unieductr.script.ctr

import com.google.common.collect.Lists
import org.hibernate.Session
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractPrintUtils;
import ru.tandemservice.unieductr.base.bo.EduProgramContract.EduProgramCtrTemplateUtils;
import ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData
import ru.tandemservice.unieductr.base.entity.EduCtrEducationAddPromise;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData

return new EduCtrContractTemplateAddBasePrint(
        session: session,    // сессия
        template: template,  // шаблон
        customer: DataAccessServices.dao().get(CtrContractVersionContractor.class, customerId),
        provider: DataAccessServices.dao().get(CtrContractVersionContractor.class, providerId),
        versionTemplateData: DataAccessServices.dao().get(EduCtrContractVersionTemplateData.class, versionTemplateDataId),
        eduPromise: DataAccessServices.dao().get(EduCtrEducationAddPromise.class, eduPromiseId)
).print()

class EduCtrContractTemplateAddBasePrint
{
    Session session
    byte[] template
    CtrContractVersionContractor customer
    CtrContractVersionContractor provider
    IEducationContractVersionTemplateData versionTemplateData
    EduCtrEducationAddPromise eduPromise
    
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def deleteLabels = Lists.newArrayList() 
    
    def print()
    {
        CtrContractVersion contractVersion = eduPromise.getSrc().getOwner();
        
        // данные версии
        
        EduContractPrintUtils.printVersionData(versionTemplateData, contractVersion, im);
        EduContractPrintUtils.printProviderData((EmployeePostContactor)provider.getContactor(), im);
        EduContractPrintUtils.printCustomerData(customer.getContactor(), im);
        EduContractPrintUtils.printPaymentData(im, DataAccessServices.dao().getList(CtrPaymentPromice.class, CtrPaymentPromice.dst(), provider, CtrPaymentPromice.deadlineDate().s()));
        EduContractPrintUtils.printAcademyData(im);
        EduProgramCtrTemplateUtils.printEduPromiceData(eduPromise, im);

        // стандартные выходные параметры скрипта
        
        CommonBaseRenderer renderer = new CommonBaseRenderer()
        .rtf()
        .fileName("Договор ${contractVersion.getNumber()}.rtf")
        .document(UniRtfUtil.toByteArray(template, im, tm, deleteLabels));
        return [renderer: renderer]
    }
}
