package unieductr.script.ctr

import com.google.common.collect.Lists
import org.apache.commons.lang.mutable.MutableLong
import org.hibernate.Session
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer
import org.tandemframework.shared.commonbase.catalog.entity.Currency
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractPrintUtils
import ru.tandemservice.unieductr.base.bo.EduProgramContract.EduProgramCtrTemplateUtils
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise
import ru.tandemservice.unieductr.student.entity.EduCtrTermEducationCostAgreementTemplateData

return new EduCtrTermEduCostAgreementTemplatePrint(
        session: session,    // сессия
        template: template,  // шаблон
        customer: DataAccessServices.dao().get(CtrContractVersionContractor.class, customerId),
        provider: DataAccessServices.dao().get(CtrContractVersionContractor.class, providerId),
        versionTemplateData: DataAccessServices.dao().get(EduCtrTermEducationCostAgreementTemplateData.class, versionTemplateDataId),
        eduPromise: DataAccessServices.dao().get(EduCtrEducationPromise.class, eduPromiseId)
).print()

class EduCtrTermEduCostAgreementTemplatePrint
{
    Session session
    byte[] template
    CtrContractVersionContractor customer
    CtrContractVersionContractor provider
    EduCtrTermEducationCostAgreementTemplateData versionTemplateData
    EduCtrEducationPromise eduPromise
    
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def deleteLabels = Lists.newArrayList() 
    
    def print()
    {
        CtrContractVersion contractVersion = eduPromise.getSrc().getOwner();
        
        // данные версии
        
        EduContractPrintUtils.printVersionData(versionTemplateData, contractVersion, im);
        EduContractPrintUtils.printProviderData((EmployeePostContactor)provider.getContactor(), im);
        EduContractPrintUtils.printCustomerData(customer.getContactor(), im);
        EduContractPrintUtils.printPaymentData(im, DataAccessServices.dao().getList(CtrPaymentPromice.class, CtrPaymentPromice.dst(), provider, CtrPaymentPromice.deadlineDate().s()));
        EduContractPrintUtils.printAcademyData(im);
        EduProgramCtrTemplateUtils.printEduPromiceData(eduPromise, im);

        def firstVersion = CtrContractVersionManager.instance().dao().getFirstVersion(versionTemplateData.owner.contract)
        def contractDate = firstVersion.docStartDate
        im.put('contractDate', DateFormatter.DEFAULT_DATE_FORMATTER.format(contractDate))


        def termNumber = versionTemplateData.yearPart.number.toString()
        im.put('termNumber', termNumber)

        def promiseHistoryMap = CtrContractVersionManager.instance().dao().getPromiceHistoryMap(versionTemplateData.owner)
        Collection<CtrPaymentPromice> paymentPromises = IUniBaseDao.instance.get().getList(CtrPaymentPromice.class, CtrPaymentPromice.src().owner(), versionTemplateData.owner).findAll() {
            p -> p instanceof CtrPaymentPromice && promiseHistoryMap.get(p.promiceLocalKey).iterator().next().src.owner.equals(p.src.owner)
        }

        if (paymentPromises.empty)
        {
            im.put('deadlineDate', '________________')
            im.put('costAmount', '________________________________.')

        } else
        {
            def costMap = SafeMap.<Currency, MutableLong>get(MutableLong.class)
            def deadlineDate = paymentPromises.iterator().next().deadlineDate

            for (CtrPaymentPromice paymentPromise: paymentPromises)
            {
                if (paymentPromise.deadlineDate.before(deadlineDate))
                    deadlineDate = paymentPromise.deadlineDate

                costMap.get(paymentPromise.currency).add(paymentPromise.costAsLong)
            }

            im.put('deadlineDate', DateFormatter.DEFAULT_DATE_FORMATTER.format(deadlineDate))
            im.put('costAmount', costMap.entrySet().collect { e -> Currency.format(e.key, e.value.longValue()) }.join(', '))
        }

        im.put('motherhoodBenefit', versionTemplateData.motherhoodBenefit ? '3. Оплата обучения в ' + termNumber + ' семестре ' + versionTemplateData.educationYear.title + ' производится за счет средств материнского капитала.' : '')

        // стандартные выходные параметры скрипта
        
        CommonBaseRenderer renderer = new CommonBaseRenderer()
        .rtf()
        .fileName("Доп. соглашение к договору ${contractVersion.getNumber()}.rtf")
        .document(UniRtfUtil.toByteArray(template, im, tm, deleteLabels));
        return [renderer: renderer]
    }
}
