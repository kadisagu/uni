package unieductr.script.ctr

import com.google.common.collect.Lists
import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.catalog.entity.Currency
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;

import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager
import ru.tandemservice.unieductr.base.entity.IEducationPromise;

return new PaymentQuittancePrint( 
        session: session,                                         // сессия
        template: template,                                       // шаблон
        payPromise: session.get(CtrPaymentPromice.class, object)  // объект печати
).print()

class PaymentQuittancePrint
{
    Session session
    byte[] template
    CtrPaymentPromice payPromise
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def deleteLabels = Lists.newArrayList() 
    
    def print()
    {
        CtrContractVersion version = payPromise.getSrc().getOwner();
        IEducationPromise firstEduPromise = EduContractManager.instance().dao().getFirstEducationPromise(version); 
        Person studentPerson = firstEduPromise.getDst().getContactor().getPerson();
        TopOrgUnit topOrgUnit = TopOrgUnit.getInstance(true);

        im.put("paymentDstTitle", topOrgUnit.getTitle());
        im.put("paymentStageTitle", payPromise.getStage());

        im.put("studentSubdivision", version.getContract().getOrgUnit().getTitle());

        im.put("contractNumber", version.getNumber());
        im.put("studentSurname", studentPerson.getIdentityCard().getLastName());
        im.put("studentName", studentPerson.getIdentityCard().getFirstName());
        im.put("studentPatronymic", studentPerson.getIdentityCard().getMiddleName());

        im.put("chargeNRate", Long.toString(Currency.getAmount(payPromise.getCostAsLong())));
        im.put("chargeNRateKopeeks", Long.toString(Currency.getReminder(payPromise.getCostAsLong())));

        im.put("paymentDstInn", topOrgUnit.getInn());
        im.put("paymentDstBank", topOrgUnit.getBank());
        im.put("paymentDstAcc", topOrgUnit.getCurAccount());
        im.put("paymentDstBik", topOrgUnit.getBic());

        // стандартные выходные параметры скрипта
        CommonBaseRenderer renderer = new CommonBaseRenderer()
        .rtf()
        .fileName("Квитанция ${version.getContract().getNumber()} от ${DateFormatter.DEFAULT_DATE_FORMATTER.format(payPromise.getDeadlineDate())}.rtf")
        .document(UniRtfUtil.toByteArray(template, im, tm, deleteLabels));
        return [renderer: renderer]
    }
}
