package unieductr.script.ctr

import com.google.common.collect.Lists
import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer
import org.tandemframework.shared.commonbase.catalog.entity.Currency
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.unieductr.base.entity.EduCtrStopVersionTemplateData

return new EduCtrStopContractPrint(
        session: session,                                         // сессия
        template: template,                                       // шаблон
        versionTemplateData: session.get(EduCtrStopVersionTemplateData.class, object)  // объект печати
).print()

class EduCtrStopContractPrint
{
    Session session
    byte[] template
    EduCtrStopVersionTemplateData versionTemplateData
    def tm = new RtfTableModifier()
    def deleteLabels = Lists.newArrayList()

    def im = new RtfInjectModifier()

    def print()
    {
        im.put('contractNumber', versionTemplateData.owner.contract.number)
        im.put('contractDate', DateFormatter.DEFAULT_DATE_FORMATTER.format(versionTemplateData.owner.docStartDate))
        im.put('stopReason', versionTemplateData.stopReason)
        im.put('refund', Currency.formatSimple(versionTemplateData.refundAsLong))
        im.put('refundCurrency', versionTemplateData.refundCurrency.shortTitle)
        im.put('comment', versionTemplateData.comment)

        // стандартные выходные параметры скрипта
        CommonBaseRenderer renderer = new CommonBaseRenderer()
        .rtf()
        .fileName("Доп. соглашение на расторжение договора ${versionTemplateData.owner.contract.number} от ${DateFormatter.DEFAULT_DATE_FORMATTER.format(versionTemplateData.owner.docStartDate)}.rtf")
        .document(UniRtfUtil.toByteArray(template, im, tm, deleteLabels));
        return [renderer: renderer]
    }
}
