package ru.tandemservice.uniepp_load.business_logic.tests;


import org.junit.Assert;
import org.junit.Test;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.codes.CourseCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.util.GroupTitleAlgorithm;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.EplStudentSummaryGroupTab;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.EplStudentSummaryGroupTabUI;
import ru.tandemservice.uniepp_load.business_logic.bdc.GroupBDC;
import ru.tandemservice.uniepp_load.business_logic.bdc.StudentBDC;
import ru.tandemservice.uniepp_load.business_logic.bdc.StudentInEppBDC;
import ru.tandemservice.uniepp_load.business_logic.bdc.SummaryBDC;
import ru.tandemservice.uniepp_load.business_logic.high_level_logic.ContingentOnNpmNpvNpp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class UpdateEplStudentsAlgorithm extends BusinessTestCase {
    //Начальное сосотояние (todo в код)
    /*
    1. Настройки Орг. Структуры
    - Орг. Структура > Настройка «Используемые типы подразделений, их вложенность и доп. Признаки»  / нажать на ОУ и добавить ему дочерние подразделения факультеты, сделать дочерние подразделения у факультетов и тд
    - Создать структуру подразделений
    2. Настройки для создания НПВ+НПП
    - Подразделения в учебном процессе > Настройка «Типы подразделений, разрешенные в качестве выпускающих, формирующих, территориальных (для совместимости)» / выделить как минимум одно для каждой категории, подразделения с таким типом должны быть созданы
    - Подразделения в учебном процессе >  Настройка «Выпускающие, формирующие и территориальные подразделения (для совместимости)» / выбрать подразделения на всех вкладках. Выбрать для Формирующих подразделений флаги «Осуществляет формирование групп» и «Осуществляет обучение студентов вне групп»
    - Системные действия >  Синхронизировать направления подготовки и квалификации / просто нажать, заполнит справочники направлений
    3. Настойки учебного процесса
    - Учебный процесс > Настройка «Подразделения, работающие с УП и РУП» / выбрать читающие подразделения (для создания дисциплин)
    - Учебный процесс > Настройка «Начало учебного года» / создать учебные года на которые будут созданы РУП и сводка. То есть МИНИМУМ на 5 лет до текущего года и на 1 год после
    3. Создать НПВ+НПП (Настройка «Параметры выпуска студентов (НПв)» (для совместимости) \ создать НПВ)
     */

    EducationOrgUnit npp5yearFull = dao.getNotNull(new Long("1558755817513041345"));
    EducationOrgUnit npp5yearDistance = dao.getNotNull(new Long("1558755882965155265"));

    private SummaryBDC summary;
    private List<DataWrapper> summaryGroups;

    /**
     * Добавить сводку на текущий учебный год
     */
    private void addSummaryCurrentYear() {
        summary = new SummaryBDC();
        summary.eppYearEducationProcess.choiceFirst();
        summary.fillSummary = true;
        summary.add();
        fillGroupsList();
    }

    /**
     * Добавить сводку на следующий год
     *
     * @param fillFirstCourse Выбрать опцию "Заполнить первый курс копией текущего первого курса"
     */
    private void addSummaryNextYear(boolean fillFirstCourse) {
        summary = new SummaryBDC();
        summary.eppYearEducationProcess.choiceByValue(EppYearEducationProcess.class, "title", "2017/2018");
        summary.fillSummary = true;
        summary.fillCopyFirstCourse = fillFirstCourse;
        summary.add();
        fillGroupsList();
    }

    /**
     * Обновить последнюю созданную сводку
     */
    private void updateSummary() {
        summary.update();
        fillGroupsList();
    }

    /**
     * Прочитать список план. групп в память из последней добавленной сводки
     */
    private void fillGroupsList() {
        activateCAF(EplStudentSummaryGroupTab.class,
                new ParametersMap().add(EplStudentSummaryGroupTabUI.PUBLISHER_ID, summary.summary.getId()));
        EplStudentSummaryGroupTabUI presenter = (EplStudentSummaryGroupTabUI) getPresenter();
        presenter.getPrioritizedGroupingItemList().stream().filter(item -> item.getTitle().equals("Курс")).forEach(item -> {
            item.setSelected(true);
            presenter.saveSettings();
        });
        summaryGroups = presenter.getGroupDS().getRecords();
        close();
    }

    /**
     * Проверить наличие группы в списке план. групп
     */
    private void assertInPlanGroups(EducationOrgUnit npp, int count, GroupBDC group, boolean isContracter, Integer course) {
        Map<String, Object> expected = new HashMap<>();
        //expected.put("eduLevel", npp.getEducationLevelHighSchool().getFullTitle()); тут слишком сложный формат вывода. Не беда, хватит и формы обучения
        if (group != null)
            expected.put("group", group.group.getTitle());
        else expected.put("group", "Вне групп");
        if (course != null)
            expected.put("course", course);
        expected.put("developForm", npp.getDevelopForm().getTitle());
        expected.put("compSource", (isContracter) ? "По договору" : "Бюджет");
        expected.put("count", count);
        assertInList(expected, summaryGroups);
    }

    /**
     * Контингент студентов  имеющих академ группы  группируется по параметрам: академ. группа, НПП, вид возмещения затрат.
     * <p>
     * Во время обновления сводки подходящие по данным параметрам группы должна обновляться
     * <p>
     * Примечание: курс берется из группы, поэтому группировка по группе автоматически приводит к группировке по курсу.
     */
    @Test
    public void shouldGroupingStudents_byAcademGroupAndCompensationTypeAndNPP_ifStudentsInGroups() {
    /*
        Создаваетмый контингент:
        1ст - npp5yearFull - Группа1 - Бюджет
        2ст - npp5yearFull - Группа1 -  - Договор
        3ст - npp5yearDistance - Группа1 -  Бюджет
        4ст - npp5yearFull - Группа2 -  - Бюджет
     */
        GroupBDC group1 = new GroupBDC(npp5yearFull).add();
        GroupBDC group2 = new GroupBDC(npp5yearFull).add();
        ContingentOnNpmNpvNpp npp5yearFullContingent = new ContingentOnNpmNpvNpp(npp5yearFull);
        ContingentOnNpmNpvNpp npp5yearDistanceContingent = new ContingentOnNpmNpvNpp(npp5yearDistance);

        Runnable addStudents = () -> {
            npp5yearFullContingent.addContingent(1, group1, false, null);
            npp5yearFullContingent.addContingent(2, group1, true, null);
            npp5yearDistanceContingent.addContingent(3, group1, false, null);
            npp5yearFullContingent.addContingent(4, group2, false, null);
        };

        Consumer<Integer> checkPlanGroups = (n) -> {
            Assert.assertEquals("Должно получиться 4 план. группы", summaryGroups.size(), 4);
            assertInPlanGroups(npp5yearFull, n, group1, false, null);
            assertInPlanGroups(npp5yearFull, 2 * n, group1, true, null);
            assertInPlanGroups(npp5yearDistance, 3 * n, group1, false, null);
            assertInPlanGroups(npp5yearFull, 4 * n, group2, false, null);
        };

        addStudents.run();
        addSummaryCurrentYear();
        checkPlanGroups.accept(1);

        addStudents.run();
        updateSummary();
        checkPlanGroups.accept(2); // студентов стало в 2 раза больше
    }

    /**
     * Контингент студентов не имеющих академ. группы  группируется по параметрам: курс, НПП, вид возмещения затрат.
     * Название группы "вне групп"
     * Во время обновления сводки подходящие по данным параметрам группы должна обновляться
     */
    @Test
    public void shouldGroupingStudents_byCourseAndCompensationTypeAndNPP_ifStudentsNotInGroups() {
    /*
        Создаваетмый контингент:
        1ст - npp5yearFull - курс 1 - Бюджет
        2ст - npp5yearFull - курс 1 -  - Договор
        3ст - npp5yearDistance - курс 1 -  Бюджет
        4ст - npp5yearFull - курс 2 -  - Бюджет
     */
        ContingentOnNpmNpvNpp npp5yearFullContingent = new ContingentOnNpmNpvNpp(npp5yearFull);
        ContingentOnNpmNpvNpp npp5yearDistanceContingent = new ContingentOnNpmNpvNpp(npp5yearDistance);


        Runnable addStudents = () -> {
            npp5yearFullContingent.addContingent(1, null, false, 1);
            npp5yearFullContingent.addContingent(2, null, true, 1);
            npp5yearDistanceContingent.addContingent(3, null, false, 1);
            npp5yearFullContingent.addContingent(4, null, false, 2);
        };

        Consumer<Integer> checkPlanGroups = (n) -> {
            Assert.assertEquals("Должно получиться 4 план. группы", summaryGroups.size(), 4);
            assertInPlanGroups(npp5yearFull, n, null, false, 1);
            assertInPlanGroups(npp5yearFull, 2 * n, null, true, 1);
            assertInPlanGroups(npp5yearDistance, 3 * n, null, false, 1);
            assertInPlanGroups(npp5yearFull, 4 * n, null, false, 2);
        };

        addStudents.run();
        addSummaryCurrentYear();
        checkPlanGroups.accept(1);

        addStudents.run();
        updateSummary();
        checkPlanGroups.accept(2); // студентов стало в 2 раза больше
    }

    /**
     * Неактивные или архивные студенты не считаются в сводке
     */
    @Test
    public void shouldNotCalculate_archiveAndInactiveStudents() {
        GroupBDC group = new GroupBDC(npp5yearFull).add();
        List<StudentBDC> students = new ContingentOnNpmNpvNpp(npp5yearFull)
                .addContingent(4, group, true, null);
        students.get(0).setStatus("отчислен");
        students.get(1).setStatus("отчислен").setArchived(); //нельзя отправить студента в архив пока он имеет активный статус.

        addSummaryCurrentYear();
        Assert.assertEquals("Должно получиться 1 план. группа", summaryGroups.size(), 1);
        assertInPlanGroups(npp5yearFull, 2, group, true, null);
    }

    /**
     * Студенты без учебного плана  не считаются в сводке
     */
    @Test
    public void shouldNotCalculate_studentsWithoutEppPlan() {
        // Добавить 3х студентов и отцепить одному из них УП
        List<StudentBDC> students = new ContingentOnNpmNpvNpp(npp5yearFull)
                .addContingent(3, null, true, 1);
        new StudentInEppBDC(students.get(0).student).setEduPlan(null);

        addSummaryCurrentYear();
        // Проверить что в группе два студента
        Assert.assertEquals("Должна получиться 1 план. группа", summaryGroups.size(), 1);
        assertInPlanGroups(npp5yearFull, 2, null, true, 1);
    }

    /**
     * При форм. и обновл. сводки на сл. год, студенты обучающиеся последний год не должны попадать в сводку
     */
    @Test
    public void shouldNotCalculate_studentsInLastCourse_ifEppYearLargerEduYear() {
        ContingentOnNpmNpvNpp contingent = new ContingentOnNpmNpvNpp(npp5yearFull);

        GroupBDC groupLastCourse = new GroupBDC(npp5yearFull);
        groupLastCourse.course.choiceByCode(Course.class, CourseCodes.COURSE_5);
        groupLastCourse.add();

        GroupBDC groupPreviousCourse = new GroupBDC(npp5yearFull);
        groupPreviousCourse.course.choiceByCode(Course.class, CourseCodes.COURSE_4);
        groupPreviousCourse.add();

        contingent.addContingent(1, groupLastCourse, true, null);
        contingent.addContingent(2, groupPreviousCourse, true, null);

        addSummaryNextYear(false);

        Assert.assertEquals("Должна получиться 1 план. группа", summaryGroups.size(), 1);
        assertInPlanGroups(npp5yearFull, 2, groupPreviousCourse, true, null);
    }

    /**
     * Курс планируемой группы соответствует курсу академ группы если сводка на текущий учебный год
     * и выше на 1 курс, если на следующий
     */
    @Test
    public void plangroupCourse_depended_summaryDelta() {
        // Создать группу определенного курса  и добавить в нее студентов
        GroupBDC group = new GroupBDC(npp5yearFull);
        group.course.choiceByCode(Course.class, CourseCodes.COURSE_2);
        group.add();
        new ContingentOnNpmNpvNpp(npp5yearFull).addContingent(1, group, false, null);
        addSummaryCurrentYear();
        assertInPlanGroups(npp5yearFull, 1, group, false, 2);

        addSummaryNextYear(false);
        assertInPlanGroups(npp5yearFull, 1, group, false, 3);
    }

    /**
     * При формировании сводки на следующий год и выборе опции "Заполнить первый курс копией текущего первого курса"
     * группы первого курса должны продублироваться. Если опция не выбрана, то групп первого курса быть не должно.
     * <p>
     * Проверяем также для студентов вне групп
     */
    @Test
    public void option_bindFirstCourse() {
        ContingentOnNpmNpvNpp contingent = new ContingentOnNpmNpvNpp(npp5yearFull);

        GroupBDC groupFirstCourse = new GroupBDC(npp5yearFull);
        groupFirstCourse.course.choiceByCode(Course.class, CourseCodes.COURSE_1);
        groupFirstCourse.add();
        contingent.addContingent(1, groupFirstCourse, true, null);
        contingent.addContingent(1, null, true, 1);

        addSummaryNextYear(false);
        Assert.assertEquals("Должно получиться 2 план. группы", summaryGroups.size(), 2);
        assertInPlanGroups(npp5yearFull, 1, groupFirstCourse, true, 2);
        assertInPlanGroups(npp5yearFull, 1, null, true, 2);

        addSummaryNextYear(true);
        Assert.assertEquals("Должно получиться 4 план. группы", summaryGroups.size(), 4);
        // по две на каждом курсе
        assertInPlanGroups(npp5yearFull, 1, groupFirstCourse, true, 1);
        assertInPlanGroups(npp5yearFull, 1, groupFirstCourse, true, 2);
        assertInPlanGroups(npp5yearFull, 1, null, true, 1);
        assertInPlanGroups(npp5yearFull, 1, null, true, 2);
    }

    /*
    Курс берется из академ. группы студента, даже если у студента другой курс
     */
    @Test
    public void shouldUseCourseOfGroup_evenCourseOfStudentIsDifferent() {
        ContingentOnNpmNpvNpp contingent = new ContingentOnNpmNpvNpp(npp5yearFull);

        GroupBDC group = new GroupBDC(npp5yearFull);
        group.course.choiceByCode(Course.class, CourseCodes.COURSE_2);
        group.add();

        contingent.addContingent(2, group, true, null);
        contingent.addContingent(1, group, true, 1);

        addSummaryCurrentYear();
        Assert.assertEquals("Должно получиться 1 план. группа", summaryGroups.size(), 1);
        // Группа содержит трех студентов, все считаются второкурсниками
        assertInPlanGroups(npp5yearFull, 3, group, true, 2);
    }

    /*
    При формировании сводки со сдвигом учитывается настройка «Механизм формирования названия групп»
    Названия план групп должны формироваться в соотвсетвии с выбранной опцией
    */
    @Test
    public void shouldUseSetting_groupTitleManagement() {
        activateMVC(ru.tandemservice.uni.component.settings.GroupTitleManagement.Controller.class, new ParametersMap());
        Long algorithmId = dao.getId(GroupTitleAlgorithm.class, "daoName", "groupTitleAlgorithmSample3");
        bc.setListenerParameter(algorithmId);
        ru.tandemservice.uni.component.settings.GroupTitleManagement.Controller controller = (ru.tandemservice.uni.component.settings.GroupTitleManagement.Controller) getController();
        controller.onClickCurrent(bc);
        close();

        ContingentOnNpmNpvNpp contingent = new ContingentOnNpmNpvNpp(npp5yearFull);
        GroupBDC group = new GroupBDC(npp5yearFull);
        group.course.choiceByCode(Course.class, CourseCodes.COURSE_2);
        group.add();
        String titleByNpp = npp5yearFull.getEducationLevelHighSchool().getShortTitle();


        Assert.assertEquals("Автосформированное название группы не соотвествует выбранной опции. Не могу продолжать тест",
                titleByNpp + "-201", group.group.getTitle());

        contingent.addContingent(1, group, false, null);

        Consumer<String> checkPlanGroupName = (expected) -> {
            Assert.assertEquals("Должна получиться 1 план. группа", summaryGroups.size(), 1);
            Assert.assertEquals("Имя группы должно соответствовать настройке с учетом курса",
                    expected,
                    summaryGroups.get(0).get("group"));
        };

        addSummaryCurrentYear();
        checkPlanGroupName.accept(titleByNpp + "-201");

        addSummaryNextYear(false);
        checkPlanGroupName.accept(titleByNpp + "-301");

        contingent.addContingent(1, group, false, null);
        updateSummary();
        checkPlanGroupName.accept(titleByNpp + "-301");
    }

    /**
     * При обновлении сводки план. группы для которых не нашлось подходящих студентов в контингенте должны быть удалены
     * <p>
     * Должно работать для студентов в группах и без групп
     */
    @Test
    public void shouldDelete_planGroups_withoutStudents() {
        ContingentOnNpmNpvNpp contingent = new ContingentOnNpmNpvNpp(npp5yearFull);
        GroupBDC group = new GroupBDC(npp5yearFull).add();

        contingent.addContingent(1, group, false, null).get(0); // хороший студент на бюджете
        StudentBDC badStudentInGroup = contingent.addContingent(1, group, true, null).get(0); // плохой студент по договору
        StudentBDC badStudentWithoutGroup = contingent.addContingent(1, null, false, 1).get(0);

        addSummaryCurrentYear();
        Assert.assertEquals("Должно получиться 3 план. группы", summaryGroups.size(), 3);

        badStudentInGroup.setStatus("отчислен");
        badStudentWithoutGroup.setStatus("отчислен");

        updateSummary();
        Assert.assertEquals("Должна остаться одна группа, две группы с плохими студентами должна быть удалены", summaryGroups.size(), 1);
        assertInPlanGroups(npp5yearFull, 1, group, false, null);
    }

    /*
    Если в план. группе присуствуют студенты с разными сетками, то то сохранять только студента у которого больше число
    студентов и в журнал сводки для пропущенного план.студента выводить сообщение.

    Должно работать для студентов в группах и без групп
    */
    @Test
    public void shouldCalculateStudents_partially_ifStudentsInOneGroupAndHasDifferentDevelopGrid() {
        Assert.fail("Не сделан тест");
        //todo надо делать большой блок добавления инд. УП только ради этого теста, поэтому оставляю на потом
    }

}
