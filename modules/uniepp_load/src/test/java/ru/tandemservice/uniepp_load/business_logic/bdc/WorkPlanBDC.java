package ru.tandemservice.uniepp_load.business_logic.bdc;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.ChoiceConstants;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.ChoiceInSelect;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.ChoiceInUtil;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.VirtualBcUser;


public class WorkPlanBDC extends VirtualBcUser {
    public EppWorkPlan workPlan;

    public final ChoiceInSelect<EppYearEducationProcess> eppYearEducationProcess = new ChoiceInSelect<>();
    public final ChoiceInUtil<EppEduPlanVersionBlock> eppEduPlanVersionBlock = new ChoiceInUtil<>();
    public final ChoiceInSelect<Term> term = new ChoiceInSelect<>();
    public final StateChange stateChange = new StateChange();

    public WorkPlanBDC add() {
        activateMVC(ru.tandemservice.uniepp.component.workplan.WorkPlanWizard.Controller.class, new ParametersMap());
        IBusinessComponent childBc1 = bc.getChildRegion("scope_ru_tandemservice_uniepp_component_workplan_WorkPlanAddEdit").getActiveComponent();
        ru.tandemservice.uniepp.component.workplan.WorkPlanAddEdit.Model model1 = childBc1.getModel();
        ru.tandemservice.uniepp.component.workplan.WorkPlanAddEdit.Controller controller1 = (ru.tandemservice.uniepp.component.workplan.WorkPlanAddEdit.Controller) childBc1.getController();

        workPlan = model1.getElement();
        workPlan.setYear(eppYearEducationProcess.choiceIn(model1.getYearEducationProcessListModel()));
        String some_number = String.valueOf(_random.nextInt());
        workPlan.setNumber(some_number);
        workPlan.setRegistrationNumber(some_number);

        eppEduPlanVersionBlock.choice(model1::setProgramKind, EppEduPlanVersionBlock.eduPlanVersion().eduPlan().programKind(), model1.getProgramKindModel());
        if (eppEduPlanVersionBlock.choiceType == ChoiceConstants.SPECIFIC &&
                !eppEduPlanVersionBlock.getEntity().getEduPlanVersion().getEduPlan().getProgramKind().isProgramAdditionalProf()) {
            eppEduPlanVersionBlock.choice(model1::setEduPlanVersion, EppEduPlanVersionBlock.eduPlanVersion(), model1.getEduPlanVersionModel());
        }
        eppEduPlanVersionBlock.choice(model1::setEduPlanVersionBlock, model1.getEduPlanVersionBlockModel());

        workPlan.setTerm(term.choiceIn(model1.getTermListModel()));
        controller1.onClickApply(childBc1);

        // todo спросить у димы почему такие названия у чайл регионов получились
        IBusinessComponent childBc2 = bc.getChildRegion("scope_ru_tandemservice_uniepp_component_workplan_WorkPlanPartSetEdit").getActiveComponent();
        ru.tandemservice.uniepp.component.workplan.WorkPlanPartSetEdit.Model model2 = childBc2.getModel();
        ru.tandemservice.uniepp.component.workplan.WorkPlanPartSetEdit.Controller controller2 = (ru.tandemservice.uniepp.component.workplan.WorkPlanPartSetEdit.Controller) childBc2.getController();

        //для простоты в наших семестрах пусть будет одна часть
        model2.setPartsInTerm(model2.getPartsInTermList().get(0));
        if (!model2.getPartsInTerm().getTitle().equals("1"))
            throw new RuntimeException("Some unsupported state in used bc. Need to fix");
        controller2.onChangePartsInTerm(childBc2);
        EppWorkPlanPart part = model2.getPartMap().get(1);

        // Почему цифры именно такие лучше спросить у аналитика. Они имеют какой-то смысл в реальной жизни
        part.setTotalWeeks(26);
        part.setAuditWeeks(17);
        part.setSelfworkWeeks(9);
        controller2.onClickApply(childBc2);

        IBusinessComponent childBc3 = bc.getChildRegion("scope_ru_tandemservice_uniepp_component_workplan_WorkPlanFillDiscipline").getActiveComponent();
        ru.tandemservice.uniepp.component.workplan.WorkPlanFillDiscipline.Controller controller3 = (ru.tandemservice.uniepp.component.workplan.WorkPlanFillDiscipline.Controller) childBc3.getController();
        controller3.onClickApply(childBc3);

        close();
        stateChange.setEntity(workPlan);
        return this;
    }


}
