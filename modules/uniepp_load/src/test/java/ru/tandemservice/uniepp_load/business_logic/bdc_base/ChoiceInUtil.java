package ru.tandemservice.uniepp_load.business_logic.bdc_base;


import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import java.util.List;
import java.util.function.Consumer;


public class ChoiceInUtil<T extends IEntity> extends ChoiceConstants {
    public static final int NOTHING = 3;

    //todo убрать копипаст
    // -----------------------начало копипаста
    private T entity;

    public T getEntity() {
        return entity;
    }

    public ChoiceInUtil() {
        this.choiceType = FIRST;
    }

    public ChoiceInUtil(T entity) {
        this.choiceType = SPECIFIC;
        this.entity = entity;
    }

    public ChoiceInUtil choiceTo(T entity) {
        choiceType = SPECIFIC;
        this.entity = entity;
        return this;
    }

    public ChoiceInUtil<T> choiceByValue(Class<T> entityClass, String propertyName, Object propertyValue) {
        choiceType = SPECIFIC;
        entity = DataAccessServices.dao().get(entityClass, propertyName, propertyValue);
        if (null == entity)
            throw new ChoiceException("Не найдена сущность класса " + entityClass + ", где " + propertyName + "=" + propertyValue);
        return this;
    }

    public ChoiceInUtil<T> choiceByCode(Class<T> entityClass, String code) {
        choiceByValue(entityClass, "code", code);
        return this;
    }

    public ChoiceInUtil<T> choiceEmpty() {
        choiceType = EMPTY;
        return this;
    }

    public ChoiceInUtil<T> choiceFirst() {
        choiceType = FIRST;
        return this;
    }

    // -------------- конец копипаста

    public ChoiceInUtil<T> choiceNothing() {
        choiceType = NOTHING;
        return this;
    }

    @SuppressWarnings("unchecked")
    private <V extends IEntity> ChoiceInSelect<V> getChoiceInSelect(MetaDSLPath<V> path) {
        ChoiceInSelect<V> choice;
        if (choiceType == SPECIFIC) {
            V value = (V) entity.getProperty(path);
            choice = new ChoiceInSelect<>(value);
        } else {
            choice = new ChoiceInSelect<>();
            choice.choiceType = this.choiceType;
        }
        return choice;
    }

    public <V extends IEntity> ChoiceInUtil<T> choice(Consumer<V> setter, MetaDSLPath<V> path, List<V> select) {
        if (choiceType != NOTHING) setter.accept(getChoiceInSelect(path).choiceIn(select));
        return this;
    }

    public <V extends IEntity> ChoiceInUtil<T> choice(Consumer<V> setter, MetaDSLPath<V> path, ISelectModel select) {
        if (choiceType != NOTHING) setter.accept(getChoiceInSelect(path).choiceIn(select));
        return this;
    }

    public ChoiceInUtil<T> choice(Consumer<T> setter, ISelectModel select) {
        if (choiceType != NOTHING) {
            ChoiceInSelect<T> choice = new ChoiceInSelect<>(entity);
            choice.choiceType = this.choiceType;
            setter.accept(choice.choiceIn(select));
        }
        return this;
    }
}
