package ru.tandemservice.uniepp_load.business_logic.bdc_base;


import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessController;
import org.tandemframework.core.component.impl.BusinessComponent;
import org.tandemframework.core.component.impl.ConversationFacade;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;

import java.util.Map;
import java.util.Random;

public class VirtualBcUser {
    protected BusinessComponent bc;
    protected ICommonDAO dao = DataAccessServices.dao();

    protected BusinessComponent activateMVC(Class controllerClass, Map<String, Object> inputParams) {
        ContextLocal.createDesktop("PersonShellDesktop", new ComponentActivator(controllerClass.getPackage().getName(), inputParams));
        bc = (BusinessComponent) ContextLocal.getRootComponent();
        ConversationFacade.begin(bc, true);
        return bc;
    }

    protected BusinessComponent activateCAF(Class bcClass, Map<String, Object> inputParams) {
        ContextLocal.createDesktop("PersonShellDesktop", new ComponentActivator(bcClass.getSimpleName(), inputParams));
        bc = (BusinessComponent) ContextLocal.getRootComponent();
        ConversationFacade.begin(bc, true);
        return bc;
    }

    protected void close() {
        ConversationFacade.end();
        if (bc.isCAFComponent()) {
            UIPresenter presenter = (UIPresenter) bc.getPresenter();
            presenter.deactivate();
        } else {
            bc.getController().deactivate(bc);
        }
    }

    public IBusinessController getController() {
        if (bc == null || bc.isCAFComponent()) throw new IllegalStateException();
        return bc.getController();
    }

    public <T> T getModel() {
        if (bc == null || bc.isCAFComponent()) throw new IllegalStateException();
        return bc.getModel();
    }

    public UIPresenter getPresenter() {
        if (bc == null || !bc.isCAFComponent()) throw new IllegalStateException();
        return (UIPresenter) bc.getPresenter();
    }

    protected SelectDataSource getDataSource(String dataSourceName) {
        if (!bc.isCAFComponent()) throw new IllegalStateException();
        SelectDataSource dataSource = getPresenter().getConfig().getDataSource(dataSourceName);
        getPresenter().onBeforeDataSourceFetch(dataSource);
        return dataSource;
    }

    protected void validateNotNull(IEntity entity) {
        if (entity == null || entity.getId() == null)
            throw new IllegalStateException("Нельзя выполнить действие пока объект класса пустой. Сначала используйте метод создания");
    }

    //todo рандоматорам в этом классе делать нечего. вынести в отдельный класс в рамкам впиливания красивых рандомов
    protected static Random _random = new Random();

    protected String _getRandomString(int length) {
        String result = "";
        for (int i = 0; i < length; i++) {
            result += (char) (_random.nextInt(26) + 'a');
        }
        return result;
    }

    protected Integer _getRandomNumber(int min, int max) {
        return _random.nextInt(max + 1 - min) + min;
    }
}

