package ru.tandemservice.uniepp_load.business_logic.bdc;

import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Add.EplStudentSummaryAdd;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Add.EplStudentSummaryAddUI;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Refresh.EplStudentSummaryRefresh;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Refresh.EplStudentSummaryRefreshUI;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.ChoiceInSelect;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.VirtualBcUser;


public class SummaryBDC extends VirtualBcUser {
    public EplStudentSummary summary;

    public final ChoiceInSelect<EppYearEducationProcess> eppYearEducationProcess = new ChoiceInSelect<>();
    public boolean fillSummary = true;
    public boolean fillCopyFirstCourse = false;
    public boolean fillEduGroups = false;

    public SummaryBDC add() {
        activateCAF(EplStudentSummaryAdd.class, new ParametersMap());
        EplStudentSummaryAddUI presenter = (EplStudentSummaryAddUI) getPresenter();
        presenter.setEppYear(eppYearEducationProcess.choiceIn(getDataSource(EplStudentSummaryAdd.DS_EPP_YEAR)));
        presenter.setFillSummary(fillSummary);
        presenter.setFillCopyFirstCourse(fillCopyFirstCourse);
        presenter.setFillEduGroups(fillEduGroups);

        //Элемента нет в презентере, потому генерирую уникальное имя для сводки
        String uniqueTitle = String.valueOf(_random.nextInt());
        presenter.setTitle(uniqueTitle);
        presenter.onClickApply();
        summary = dao.get(EplStudentSummary.class, "title", uniqueTitle);
        close();
        return this;
    }

    public SummaryBDC update() {
        validateNotNull(summary);
        activateCAF(EplStudentSummaryRefresh.class,
                new ParametersMap().add(EplStudentSummaryRefreshUI.PUBLISHER_ID, summary.getId()));
        EplStudentSummaryRefreshUI presenter = (EplStudentSummaryRefreshUI) getPresenter();
        presenter.setBackupCopy(false);
        presenter.onClickApply();
        close();
        return this;
    }
}
