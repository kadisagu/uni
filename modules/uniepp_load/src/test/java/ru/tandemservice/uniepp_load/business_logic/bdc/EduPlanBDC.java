package ru.tandemservice.uniepp_load.business_logic.bdc;


import javafx.util.Pair;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.EppEduPlanManager;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.HigherProfAddEdit.EppEduPlanHigherProfAddEdit;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.HigherProfAddEdit.EppEduPlanHigherProfAddEditUI;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockListEdit.EppEduPlanVersionBlockListEdit;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockListEdit.EppEduPlanVersionBlockListEditUI;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RegRowAddEdit.EppEduPlanVersionRegRowAddEditUI;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RowAdd.EppEduPlanVersionRowAdd;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RowAdd.EppEduPlanVersionRowAddUI;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.ChoiceInSelect;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.VirtualBcUser;

import java.util.List;

public class EduPlanBDC extends VirtualBcUser {
    // todo тип добавляемого УП. Сделать параметром конструктора, когда будем делать добавление УП любого типа
    public final EduProgramKind eduProgramKind = dao.get(EduProgramKind.class, "code", EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV);

    //Параметры УП
    public EppEduPlanHigherProf eduPlan;

    public final ChoiceInSelect<EduProgramSubject> subject = new ChoiceInSelect<>();
    public final ChoiceInSelect<EduProgramQualification> eduProgramQualification = new ChoiceInSelect<>();
    public final ChoiceInSelect<EduLevel> baseLevel = new ChoiceInSelect<>();
    public final ChoiceInSelect<EduProgramForm> programForm = new ChoiceInSelect<>();
    public final ChoiceInSelect<DevelopCondition> developCondition = new ChoiceInSelect<>();
    public final ChoiceInSelect<EduProgramTrait> programTrait = new ChoiceInSelect<EduProgramTrait>().choiceEmpty();
    public final ChoiceInSelect<DevelopGrid> developGrid = new ChoiceInSelect<>();
    public Integer eduStartYear = 2017;

    //Параметры версии
    public EppEduPlanVersion eduPlanVersion;

    public final ChoiceInSelect<DevelopGridTerm> versionDevelopGridTerm = new ChoiceInSelect<>();

    // Параметры блока
    public EppEduPlanVersionBlock eppEduPlanVersionBlock;

    public final Pair<ChoiceInSelect<EduProgramSpecialization>, ChoiceInSelect<EduOwnerOrgUnit>> versionChildBlock = new Pair<>(new ChoiceInSelect<>(), new ChoiceInSelect<>());

    //Изменение состояния УП
    public final StateChange stateChangePlan = new StateChange();
    public final StateChange stateChangeVersion = new StateChange();

    public EduPlanBDC add() {
        activateCAF(EppEduPlanHigherProfAddEdit.class,
                new ParametersMap().add(EppEduPlanManager.BIND_PROGRAM_KIND_ID, eduProgramKind.getId()));
        EppEduPlanHigherProfAddEditUI presenter = (EppEduPlanHigherProfAddEditUI) getPresenter();
        eduPlan = presenter.getElement();
        eduPlan.setRegistrationNumber(String.valueOf(_random.nextInt(9999)));
        eduPlan.setProgramSubject(subject.choiceIn(getDataSource("programSubjectDS")));
        eduPlan.setProgramQualification(eduProgramQualification.choiceIn(getDataSource("programQualificationDS")));
        eduPlan.setBaseLevel(baseLevel.choiceIn(getDataSource("eduLevelDS")));
        eduPlan.setProgramForm(programForm.choiceIn(getDataSource("programFormDS")));
        eduPlan.setDevelopCondition(developCondition.choiceIn(getDataSource("developConditionDS")));
        eduPlan.setProgramTrait(programTrait.choiceIn(getDataSource("programTraitDS")));
        eduPlan.setDevelopGrid(developGrid.choiceIn(getDataSource("developGridDS")));
        eduPlan.setEduStartYear(eduStartYear);
        presenter.onClickApply();
        close();

        stateChangePlan.setEntity(eduPlan);
        return this;
    }

    public EduPlanBDC addVersionAndRootBlock() {
        validateNotNull(eduPlan);
        activateMVC(ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddEdit.Controller.class,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, eduPlan.getId()));
        ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddEdit.Controller controller = (ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddEdit.Controller) getController();
        ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddEdit.Model model = getModel();
        eduPlanVersion = model.getElement();
        eduPlanVersion.setRegistrationNumber(String.valueOf(_random.nextInt(9999)));
        eduPlanVersion.setDevelopGridTerm(versionDevelopGridTerm.choiceIn(model.getDevelopGridTermList()));
        controller.onClickApply(bc);
        close();
        stateChangeVersion.setEntity(eduPlanVersion);
        return this;
    }

    public EduPlanBDC addSpecializationBlock() {
        validateNotNull(eduPlanVersion);
        activateCAF(EppEduPlanVersionBlockListEdit.class, new ParametersMap()
                .add(PublisherActivator.PUBLISHER_ID_KEY, eduPlanVersion.getId()));
        EppEduPlanVersionBlockListEditUI presenter = (EppEduPlanVersionBlockListEditUI) getPresenter();
        presenter.setCurrentSpec(versionChildBlock.getKey().choiceIn(presenter.getSpecList()));
        presenter.setInclude(true);
        presenter.setOrgUnit(versionChildBlock.getValue().choiceIn(presenter.getOrgUnitModel()));
        presenter.onClickApply();
        close();
        List<EppEduPlanVersionBlock> blocks = dao.getList(EppEduPlanVersionBlock.class,
                EppEduPlanVersionBlock.eduPlanVersion(), eduPlanVersion,
                EppEduPlanVersionBlock.id().s());
        eppEduPlanVersionBlock = blocks.get(blocks.size() - 1);
        return this;
    }

    /**
     * Добавить дисциплину в текущий блок учебного плана с указанием семестров, в которые должны попасть части этой дисциплины
     *
     * @param eppRegistryDiscipline Дисциплина
     * @param partsToTermNumber     Лист с номерами семестров учебного плана, в которые должны попасть части дисциплины
     */
    //todo лист с номерами семестров - это неудобно и непонятно. но иначе надо делать более сложную логику распределения частей по УП
    public EduPlanBDC addDiscipline(EppRegistryDiscipline eppRegistryDiscipline, List<Integer> partsToTermNumber) {
        validateNotNull(eppEduPlanVersionBlock);
        activateCAF(EppEduPlanVersionRowAdd.class, new ParametersMap()
                .add(PublisherActivator.PUBLISHER_ID_KEY, eppEduPlanVersionBlock.getId()));
        EppEduPlanVersionRowAddUI presenter = (EppEduPlanVersionRowAddUI) getPresenter();
        presenter.setElementType((IdentifiableWrapper) presenter.getElementTypeList().findValues("Мероприятие реестра (дисциплина, практика, ГИА)").getObjects().get(0));
        presenter.onClickChangeType();
        EppEduPlanVersionRegRowAddEditUI childPresenter = (EppEduPlanVersionRegRowAddEditUI) bc.getChildRegion(UIDefines.DEFAULT_REGION_NAME).getActiveComponent().getPresenter();

        childPresenter.getRow().setNumber(String.valueOf(_random.nextInt(9999)));
        childPresenter.getRow().setRegistryElementType(new ChoiceInSelect<>(eppRegistryDiscipline.getParent()).choiceInHSelect(childPresenter.getTypeList()));
        childPresenter.getRow().setRegistryElementOwner(new ChoiceInSelect<>(eppRegistryDiscipline.getOwner()).choiceIn(childPresenter.getOwnerSelectModel()));
        childPresenter.getRow().setRegistryElement(new ChoiceInSelect<>(eppRegistryDiscipline).choiceIn(childPresenter.getRegistrySelectModel()));
        childPresenter.onClickFillLoadFromRegistryElement();
        if (partsToTermNumber != null) {
            if (partsToTermNumber.size() != childPresenter.getColumns().size())
                throw new IllegalStateException("Кол-во семестров в учебном плане должно соотвествовать числу частей дисциплины");

            for (int i = 0; i < partsToTermNumber.size(); i++) {
                childPresenter.getColumns().get(i).setTermNumber(partsToTermNumber.get(i));
            }
        }
        childPresenter.onClickApply();
        close();
        return this;
    }

    /**
     * Добавить дисциплину в текущий блок учебного плана в семестры по умолчанию (первые семестры)
     *
     * @param eppRegistryDiscipline дисциплина
     */
    public EduPlanBDC addDiscipline(EppRegistryDiscipline eppRegistryDiscipline) {
        return addDiscipline(eppRegistryDiscipline, null);
    }

}
