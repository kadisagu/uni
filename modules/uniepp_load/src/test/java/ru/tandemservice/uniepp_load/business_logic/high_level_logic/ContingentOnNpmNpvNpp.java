package ru.tandemservice.uniepp_load.business_logic.high_level_logic;


import com.google.common.collect.ImmutableList;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.DevelopTechCodes;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp_load.business_logic.bdc.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ContingentOnNpmNpvNpp {

    ICommonDAO dao = DataAccessServices.dao();

    private Map<Term, Map<EppYearEducationProcess, EppWorkPlan>> existedWorkPlans = new HashMap<>();
    private Map<Integer, EppYearEducationProcess> tmpYears = new HashMap<>();
    private Map<Integer, List<Term>> tmpTerms = new HashMap<>();

    private EppEduPlanVersionBlock eppEduPlanVersionBlock;
    private EducationOrgUnit educationOrgUnit;
    private EppYearEducationProcess currentYearEducationProcess = DataAccessServices.dao().get(EppYearEducationProcess.class, "title", "2016/2017");
    private int currentEducationYear = currentYearEducationProcess.getEducationYear().getIntValue();


    public ContingentOnNpmNpvNpp(EducationOrgUnit educationOrgUnit) {
        this.educationOrgUnit = educationOrgUnit;

        EduPlanBDC planBDC = new EduPlanBDC();

        // Создаем план и блок подходящий под НПМ+НПВ+НПП
        planBDC.subject.choiceTo(educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject());
        EduProgramSubjectQualification q = dao.getNotNull(EduProgramSubjectQualification.class,
                EduProgramSubjectQualification.programSubject(),
                educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject());
        planBDC.eduProgramQualification.choiceTo(q.getProgramQualification());
        planBDC.programForm.choiceTo(educationOrgUnit.getDevelopForm().getProgramForm());
        planBDC.developCondition.choiceTo(educationOrgUnit.getDevelopCondition());
        if (!educationOrgUnit.getDevelopTech().getCode().equals(DevelopTechCodes.GENERAL))
            planBDC.programTrait.choiceTo(educationOrgUnit.getDevelopTech().getProgramTrait());
        else planBDC.programTrait.choiceEmpty();
        DevelopGrid dg = dao.getNotNull(DevelopGrid.class, DevelopGrid.developPeriod(), educationOrgUnit.getDevelopPeriod());
        planBDC.developGrid.choiceTo(dg);
        planBDC.add();

        planBDC.addVersionAndRootBlock();
        planBDC.versionChildBlock.getKey().choiceTo(educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getEduProgramSpecialization());
        planBDC.addSpecializationBlock();
        this.eppEduPlanVersionBlock = planBDC.eppEduPlanVersionBlock;

        // Заполним вспомогательные мапы хранящие информацию о семестрах обучения на этом плане
        fillTmp();

        // В каждый семестр добавим по дисциплине
        for (List<Term> t : tmpTerms.values()) {
            for (Term tt : t) {
                DisciplineBDC d = new DisciplineBDC();
                d.add().stateChange.setStateConformed();
                planBDC.addDiscipline(d.eppRegistryDiscipline, ImmutableList.of(tt.getIntValue()));
            }
        }

        // Согласуем УП
        planBDC.stateChangeVersion.setStateConformed();
        planBDC.stateChangePlan.setStateConformed();

    }

    //    public ContingentOnNpmNpvNpp(EducationOrgUnit educationOrgUnit, EppEduPlanVersionBlock eppEduPlanVersionBlock) {
    // todo здесь сначала надо добавить проверки что студенты на этом НПП могут учиться на этом плане и блоке
//        this.educationOrgUnit = educationOrgUnit;
//        this.eppEduPlanVersionBlock = eppEduPlanVersionBlock;
//        fillTmp();
//    }

    private void fillTmp() {
        List<DevelopGridTerm> dgtList = dao.getList(DevelopGridTerm.class,
                DevelopGridTerm.developGrid(),
                eppEduPlanVersionBlock.getEduPlanVersion().getEduPlan().getDevelopGrid());
        for (DevelopGridTerm dgt : dgtList) {
            Integer courseInt = dgt.getCourse().getIntValue();
            if (!tmpTerms.containsKey(courseInt))
                tmpTerms.put(courseInt, new ArrayList<>());

            tmpTerms.get(courseInt).add(dgt.getTerm());

            existedWorkPlans.put(dgt.getTerm(), new HashMap<>());
        }
    }

    public List<StudentBDC> addContingent(Integer studentCount,
                                          GroupBDC groupBDC,
                                          Boolean isContracter,
                                          Integer course) {

        if (groupBDC == null && course == null)
            throw new IllegalArgumentException("Необходимо указать курс для создаваемого студента без группы");

        if (groupBDC != null && groupBDC.group == null)
            throw new IllegalStateException("Необходимо сначала добавить группу вызвав метод add()");

        List<StudentBDC> result = new ArrayList<>();

        for (int i = 0; i < studentCount; i++) {
            StudentBDC studentBDC;
            if (groupBDC != null) {
                studentBDC = new StudentBDC(groupBDC.group);
                if (!educationOrgUnit.equals(groupBDC.group.getEducationOrgUnit()))
                    studentBDC.educationOrgUnit.choiceTo(educationOrgUnit);
            } else studentBDC = new StudentBDC(educationOrgUnit);

            if (course == null)
                course = groupBDC.group.getCourse().getIntValue();
            studentBDC.course.choiceByValue(Course.class, "intValue", course);
            studentBDC.entranceYear = currentEducationYear - course + 1;

            if (isContracter)
                studentBDC.compensationType.choiceByCode(CompensationType.class, CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT);
            else
                studentBDC.compensationType.choiceByCode(CompensationType.class, CompensationTypeCodes.COMPENSATION_TYPE_BUDGET);

            studentBDC.add();

            StudentInEppBDC studentInEppBDC = new StudentInEppBDC(studentBDC.student);
            studentInEppBDC.setEduPlan(eppEduPlanVersionBlock);

            List<EppWorkPlan> studentWorkPlans = new ArrayList<>();

            // Год и семестр РУПа должен соответствовать году, который был (будет) когда студент учился (будет учиться) в данном семестре
            // Поэтому для каждого семестра необходимо создать столько РУПов, сколько разных годов обучения возможно у студентов
            //----------------------------------
            // Получаем РУП для семестров обучения которые в текущем учебном году, либо в следующем, либо были ранее.
            // напишите лучше :) с=0 текущий уч. год, с+ прошлое, с- будущее
            // Если РУП на этот семестр и год еще не создан ранее, то он создается, далее возвращаются уже
            // созданные рупы подходящие по параметрам
            for (int c = -1; c < course; c++) {
                int targetCourse = course - c;
                int targetYear = currentEducationYear - c;

                // если студент на последнем курсе, то пропускаем создание РУП на следующий год. Если на первом, то на предыдущие года
                if (targetCourse > educationOrgUnit.getDevelopPeriod().getLastCourse() || targetCourse < 1)
                    continue;

                EppYearEducationProcess targetYearEducationProcess = tmpYears.get(targetYear);
                if (targetYearEducationProcess == null) {
                    targetYearEducationProcess = dao.getNotNull(EppYearEducationProcess.class,
                            EppYearEducationProcess.educationYear().intValue(), targetYear);
                    tmpYears.put(targetYear, targetYearEducationProcess);
                }

                for (Term t : tmpTerms.get(targetCourse))
                    studentWorkPlans.add(getWorkPlan(targetYearEducationProcess, t));
            }
            studentInEppBDC.setWorkPlans(studentWorkPlans);
            result.add(studentBDC);
        }
        return result;
    }


    private EppWorkPlan getWorkPlan(EppYearEducationProcess yearEducationProcess, Term term) {
        EppWorkPlan wp = existedWorkPlans.get(term).get(yearEducationProcess);
        if (wp == null) {
            WorkPlanBDC workPlanBDC = new WorkPlanBDC();
            workPlanBDC.eppEduPlanVersionBlock.choiceTo(eppEduPlanVersionBlock);
            workPlanBDC.eppYearEducationProcess.choiceTo(yearEducationProcess);
            workPlanBDC.term.choiceTo(term);
            workPlanBDC.add().stateChange.setStateConformed();
            wp = workPlanBDC.workPlan;
            existedWorkPlans.get(term).put(yearEducationProcess, wp);
        }
        return wp;
    }
}
