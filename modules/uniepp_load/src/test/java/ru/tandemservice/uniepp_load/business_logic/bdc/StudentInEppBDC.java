package ru.tandemservice.uniepp_load.business_logic.bdc;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.ChoiceInUtil;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.VirtualBcUser;

import java.util.List;

public class StudentInEppBDC extends VirtualBcUser {
    public Student student;

    public StudentInEppBDC(Student student) {
        this.student = student;
    }

    public StudentInEppBDC setEduPlan(EppEduPlanVersionBlock block) {
        validateNotNull(student);
        if (block == null) {
            activateMVC(ru.tandemservice.uniepp.component.student.StudentEduplanTab.Controller.class,
                    new ParametersMap().add("studentId", student.getId()));
            ru.tandemservice.uniepp.component.student.StudentEduplanTab.Controller controller = (ru.tandemservice.uniepp.component.student.StudentEduplanTab.Controller) getController();
            controller.onClickDisconnectEduplan(bc);
            close();
            return this;
        }

        bc = activateMVC(ru.tandemservice.uniepp.component.student.MassChangeEduPlanVersions.Controller.class,
                new ParametersMap().add("ids", ImmutableList.<Long>of(student.getId())));
        ru.tandemservice.uniepp.component.student.MassChangeEduPlanVersions.Model model = bc.getModel();
        ru.tandemservice.uniepp.component.student.MassChangeEduPlanVersions.Controller controller = (ru.tandemservice.uniepp.component.student.MassChangeEduPlanVersions.Controller) bc.getController();

        new ChoiceInUtil<>(block)
                .choice(model::setEduPlan, EppEduPlanVersionBlock.eduPlanVersion().eduPlan(), model.getEduPlanListModel())
                .choice(model::setEduPlanVersion, EppEduPlanVersionBlock.eduPlanVersion(), model.getEduPlanVersionListModel())
                .choice(model::setBlock, model.getBlockListModel());
        controller.onClickApply(bc);
        close();
        return this;
    }

    public StudentInEppBDC setWorkPlans(List<EppWorkPlan> workPlans) {
        validateNotNull(student);
        bc = activateMVC(ru.tandemservice.uniepp.component.student.MassChangeWorkPlan.Controller.class,
                new ParametersMap().add("ids", ImmutableList.<Long>of(student.getId())));
        ru.tandemservice.uniepp.component.student.MassChangeWorkPlan.Controller controller = (ru.tandemservice.uniepp.component.student.MassChangeWorkPlan.Controller) bc.getController();
        ru.tandemservice.uniepp.component.student.MassChangeWorkPlan.Model model = bc.getModel();

        for (EppWorkPlan workPlan : workPlans) {
            new ChoiceInUtil<>(workPlan)
                    .choice(model::setCurrentTerm, EppWorkPlan.cachedGridTerm(), model.getTermList())
                    .choice(model::setCurrentYear, EppWorkPlan.year(), model.getYearListModel())
                    .choice(model::setCurrentWorkPlan, model.getWorkplanListModel());
        }
        controller.onClickApply(bc);
        close();
        return this;
    }
}



