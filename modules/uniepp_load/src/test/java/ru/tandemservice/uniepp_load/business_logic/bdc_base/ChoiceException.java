package ru.tandemservice.uniepp_load.business_logic.bdc_base;


public class ChoiceException extends IllegalStateException {
    public ChoiceException(String message) {
        super(message);
    }
}
