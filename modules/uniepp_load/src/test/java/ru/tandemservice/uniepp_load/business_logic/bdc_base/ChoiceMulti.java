package ru.tandemservice.uniepp_load.business_logic.bdc_base;


import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import java.util.ArrayList;
import java.util.List;

public class ChoiceMulti<T extends IEntity> extends ChoiceConstants {
    private List<T> entities = new ArrayList<>();

    public ChoiceMulti() {
        choiceType = FIRST;
    }

    public ChoiceMulti(T entity) {
        choiceType = SPECIFIC;
        this.entities.add(entity);
    }

    public ChoiceMulti choiceTo(T entity) {
        choiceType = SPECIFIC;
        entities.add(entity);
        return this;
    }

    public ChoiceMulti<T> choiceByValue(Class<T> entityClass, String propertyName, Object propertyValue) {
        choiceType = SPECIFIC;
        T entity = DataAccessServices.dao().get(entityClass, propertyName, propertyValue);
        if (entity == null)
            throw new ChoiceException("Не найдена сущность класса " + entityClass + ", где " + propertyName + "=" + propertyValue);
        else
            entities.add(entity);
        return this;
    }

    public ChoiceMulti<T> choiceByCode(Class<T> entityClass, String code) {
        choiceByValue(entityClass, "code", code);
        return this;
    }

    public ChoiceMulti<T> choiceEmpty() {
        choiceType = EMPTY;
        return this;
    }

    public ChoiceMulti<T> choiceFirst() {
        choiceType = FIRST;
        return this;
    }

    public Boolean hasItems() {
        return choiceType == FIRST || !entities.isEmpty();
    }

    public void clear() {
        this.entities.clear();
    }

    public List<T> choiceIn(ISelectModel selectList) {
        if (choiceType == SPECIFIC) {
            for (T entity : entities) {
                // Выкинуть исключение если этой сущности нет в списке
                new ChoiceInSelect<>(entity).choiceIn(selectList);
                return entities;
            }
        }
        if (choiceType == FIRST) {
            List<T> l = new ArrayList<>();
            l.add(new ChoiceInSelect<T>().choiceIn(selectList));
            return l;
        }
        if (choiceType == EMPTY) {
            return new ArrayList<>();
        }
        throw new RuntimeException("Unsupported choice type");
    }
}
