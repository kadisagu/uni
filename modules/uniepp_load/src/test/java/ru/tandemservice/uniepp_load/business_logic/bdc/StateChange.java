package ru.tandemservice.uniepp_load.business_logic.bdc;


import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.ChoiceException;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.ChoiceInSelect;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.VirtualBcUser;

public class StateChange extends VirtualBcUser {
    private IEntity entity;

    public IEntity getEntity() {
        return entity;
    }

    public void setEntity(IEntity entity) {
        this.entity = entity;
    }

    public void setStateConformed() {
        validateNotNull(entity);
        activateMVC(ru.tandemservice.uniepp.component.base.ChangeState.Controller.class,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId()));
        ru.tandemservice.uniepp.component.base.ChangeState.Model model = getModel();
        ru.tandemservice.uniepp.component.base.ChangeState.Controller controller = (ru.tandemservice.uniepp.component.base.ChangeState.Controller) getController();

        try {
            bc.setListenerParameter(new ChoiceInSelect<EppState>()
                    .choiceByCode(EppState.class, EppState.STATE_ACCEPTABLE)
                    .choiceIn(model.getPossibleTransitions())
                    .getCode());
            controller.onClickChangeState(bc);
            bc.setListenerParameter(new ChoiceInSelect<EppState>()
                    .choiceByCode(EppState.class, EppState.STATE_ACCEPTED)
                    .choiceIn(model.getPossibleTransitions())
                    .getCode());
            controller.onClickChangeState(bc);

        } catch (ChoiceException e) {
            throw new IllegalStateException("Типовая транзакция 'Отправить на согласование > Согласовать' не разрешена, либо статуст не 'Формируется'");
        }
        close();
    }
}
