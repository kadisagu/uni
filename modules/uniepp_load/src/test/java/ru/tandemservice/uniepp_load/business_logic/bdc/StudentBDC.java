package ru.tandemservice.uniepp_load.business_logic.bdc;

import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.BusinessComponent;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import ru.tandemservice.uni.base.bo.UniStudent.ui.StatusEdit.UniStudentStatusEdit;
import ru.tandemservice.uni.base.bo.UniStudent.ui.StatusEdit.UniStudentStatusEditUI;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.ChoiceInSelect;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.ChoiceInUtil;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.VirtualBcUser;

import java.util.Calendar;
import java.util.Date;

public class StudentBDC extends VirtualBcUser {
    public Student student;

    private Group group;

    public final ChoiceInUtil<EducationOrgUnit> educationOrgUnit = new ChoiceInUtil<>();
    public final ChoiceInSelect<Course> course = new ChoiceInSelect<>();
    public final ChoiceInSelect<StudentCategory> studentCategory = new ChoiceInSelect<StudentCategory>().choiceByCode(StudentCategory.class, StudentCategoryCodes.STUDENT_CATEGORY_STUDENT);
    public final ChoiceInSelect<CompensationType> compensationType = new ChoiceInSelect<>();
    public String lastName = "Фамилия";
    public String firstName = "Имя";

    public Integer entranceYear = 2017;
    public boolean createWithActiveStatus = true;

    /**
     * Добавить студента в группу с параметрами обучения по умолчанию
     *
     * @param group академическая группа
     */
    public StudentBDC(Group group) {
        this.group = group;
        educationOrgUnit.choiceNothing();
    }

    /**
     * Добавить студента без группы с заданными параметрами обучения
     *
     * @param educationOrgUnit НПП+НПВ
     */

    public StudentBDC(EducationOrgUnit educationOrgUnit) {
        this.educationOrgUnit.choiceTo(educationOrgUnit);
    }


    public StudentBDC add() {
        BusinessComponent bc_student = activateMVC(ru.tandemservice.uni.component.student.StudentAdd.Controller.class,
                new ParametersMap().add("groupId", (group != null) ? group.getId() : null));
        ru.tandemservice.uni.component.student.StudentAdd.Controller controller_student = (ru.tandemservice.uni.component.student.StudentAdd.Controller) bc_student.getController();
        ru.tandemservice.uni.component.student.StudentAdd.Model model_student = bc_student.getModel();

        student = model_student.getStudent();
        student.setEntranceYear(entranceYear);
        student.setStudentCategory(studentCategory.choiceIn(model_student.getStudentCategoryList()));
        student.setCompensationType(compensationType.choiceIn(model_student.getCompensationTypesList()));
        student.setCourse(course.choiceIn(model_student.getCourseList()));

        educationOrgUnit
                .choice(model_student::setFormativeOrgUnit, EducationOrgUnit.formativeOrgUnit(), model_student.getFormativeOrgUnitModel())
                .choice(model_student::setEducationLevelsHighSchool, EducationOrgUnit.educationLevelHighSchool(), model_student.getEducationLevelsHighSchoolModel())
                .choice(model_student::setDevelopForm, EducationOrgUnit.developForm(), model_student.getDevelopFormModel())
                .choice(model_student::setDevelopCondition, EducationOrgUnit.developCondition(), model_student.getDevelopConditionModel())
                .choice(model_student::setDevelopTech, EducationOrgUnit.developTech(), model_student.getDevelopTechModel())
                .choice(model_student::setDevelopPeriod, EducationOrgUnit.developPeriod(), model_student.getDevelopPeriodModel());

        BusinessComponent bc_person = (BusinessComponent) bc_student.getChildRegion(ru.tandemservice.uni.component.student.StudentAdd.Controller.IDENTITY_CARD_REGION).getActiveComponent();
        org.tandemframework.shared.person.base.bo.Person.ui.IdentityCardEditInline.Model model_person = bc_person.getModel();

        IdentityCard identityCard = model_person.getIdentityCard();
        identityCard.setFirstName(firstName);
        identityCard.setLastName(lastName);
        identityCard.setCardType(new ChoiceInSelect<IdentityCardType>()
                .choiceByCode(IdentityCardType.class, IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII)
                .choiceIn(model_person.getIdentityCardTypeList()));
        identityCard.setSeria(_getRandomNumber(1000, 9999).toString());
        identityCard.setNumber(_getRandomNumber(100000, 999999).toString());
        identityCard.setIssuancePlace("Кем-то");
        identityCard.setIssuanceCode("999-999");
        identityCard.setIssuanceDate(new Date());
        identityCard.setSex(model_person.getSexList().get(_random.nextInt(2)));
        Calendar cal = Calendar.getInstance();
        cal.set(2000, Calendar.DECEMBER, 1);
        identityCard.setBirthDate(cal.getTime());
        identityCard.setBirthPlace("Где-то");

        controller_student.onClickApply(bc_student);
        close();

        if (createWithActiveStatus) setStatusActive();

        return this;
    }

    /**
     * Отправить студента в архив
     */
    public StudentBDC setArchived() {
        validateNotNull(student);
        activateMVC(ru.tandemservice.uni.component.student.StudentPub.Controller.class,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, student.getId()));
        ru.tandemservice.uni.component.student.StudentPub.Controller controller = (ru.tandemservice.uni.component.student.StudentPub.Controller) bc.getController();
        controller.onClickArchive(bc);
        close();
        return this;
    }

    /**
     * Поменять студенту статус
     */
    public StudentBDC setStatus(String newStatus) {
        validateNotNull(student);
        activateCAF(UniStudentStatusEdit.class, new ParametersMap().add("studentId", student.getId()));
        UniStudentStatusEditUI presenter = (UniStudentStatusEditUI) bc.getPresenter();
        presenter.getStudent().setStatus(new ChoiceInSelect<StudentStatus>()
                .choiceByValue(StudentStatus.class, "title", newStatus)
                .choiceIn(getDataSource(UniStudentStatusEdit.STUDENT_STATUS_DS)));
        presenter.onClickApply();
        close();
        return this;
    }

    /**
     * Поменять студенту статус на "активный"
     */
    public StudentBDC setStatusActive() {
        setStatus("активный");
        return this;
    }
}
