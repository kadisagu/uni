package ru.tandemservice.uniepp_load.business_logic.bdc_base;


public abstract class ChoiceConstants {

    /**
     * всегда возвращать null
     */
    public static final int EMPTY = 0;

    /**
     * Возвращать первое значение из списка
     * Если список пустой генерирует исключение. Если в списке сущности другого класса генериорует исключение
     */
    public static final int FIRST = 1;

    /**
     * Искать в списке конкретную сущность. Если ее нет генерировать исключение
     */
    public static final int SPECIFIC = 2;

    public int choiceType;

}
