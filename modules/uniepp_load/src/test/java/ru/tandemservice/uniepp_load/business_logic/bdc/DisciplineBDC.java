package ru.tandemservice.uniepp_load.business_logic.bdc;

import javafx.util.Pair;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.component.registry.base.Pub.RegistryElementPubContext;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.ChoiceInSelect;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.ChoiceMulti;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.VirtualBcUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DisciplineBDC extends VirtualBcUser {
    public EppRegistryDiscipline eppRegistryDiscipline;
    public EppRegistryElementPart eppRegistryElementPart;
    public EppRegistryModule eppRegistryModule;

    public String title = "Дисциплина" + _random.nextInt();
    public final ChoiceInSelect<OrgUnit> owner = new ChoiceInSelect<>();
    public final ChoiceInSelect<EppRegistryStructure> parent = new ChoiceInSelect<EppRegistryStructure>()
            .choiceByCode(EppRegistryStructure.class, EppRegistryStructureCodes.REGISTRY_DISCIPLINE);

    public final ChoiceMulti<EppFControlActionType> controlActions = new ChoiceMulti<>();
    public List<Pair<ChoiceInSelect<EppALoadType>, Integer>> moduleALoads =
            new ArrayList<Pair<ChoiceInSelect<EppALoadType>, Integer>>() {{
                add(new Pair<>(new ChoiceInSelect<>(), 15));
            }};

    public final StateChange stateChange = new StateChange();

    public DisciplineBDC add() {
        activateMVC(ru.tandemservice.uniepp.component.registry.DisciplineRegistry.AddEdit.Controller.class, new ParametersMap());
        ru.tandemservice.uniepp.component.registry.DisciplineRegistry.AddEdit.Model model = getModel();

        eppRegistryDiscipline = model.getElement();
        eppRegistryDiscipline.setTitle(title);
        eppRegistryDiscipline.setOwner(owner.choiceIn(model.getOwnerSelectModel()));
        eppRegistryDiscipline.setParent(parent.choiceInHSelect(model.getParentList()));
        eppRegistryDiscipline.setSizeAsDouble(0.0);
        eppRegistryDiscipline.setLaborAsDouble(0.0);
        model.getElement().setParts(1);

        ru.tandemservice.uniepp.component.registry.DisciplineRegistry.AddEdit.Controller controller = (ru.tandemservice.uniepp.component.registry.DisciplineRegistry.AddEdit.Controller) getController();
        controller.onClickApply(bc);
        close();

        editLastPartAndModule();
        stateChange.setEntity(eppRegistryDiscipline);
        return this;
    }

    public DisciplineBDC addPartAndModule() {
        validateNotNull(eppRegistryDiscipline);
        activateMVC(ru.tandemservice.uniepp.component.registry.DisciplineRegistry.Pub.Controller.class,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, eppRegistryDiscipline.getId()));
        RegistryElementPubContext registryElementPubContext = getModel();
        close();
        activateMVC(ru.tandemservice.uniepp.component.registry.RegElementPartList.Controller.class,
                new ParametersMap().add(RegistryElementPubContext.REGISTRY_ELEMENT_PUB_CONTEXT_PARAM, registryElementPubContext));
        ru.tandemservice.uniepp.component.registry.RegElementPartList.Controller controller = (ru.tandemservice.uniepp.component.registry.RegElementPartList.Controller) getController();
        //todo написать Диме, узнать почему листенер не заполняет модель
        bc.setListenerParameter(eppRegistryDiscipline.getId());
        controller.onClickAddPart(bc);
        close();
        editLastPartAndModule();
        return this;
    }

    private void editLastPartAndModule() {
        if (!moduleALoads.isEmpty()) {
            List<EppRegistryElementPartModule> modules = dao.getList(EppRegistryElementPartModule.class,
                    EppRegistryElementPartModule.part().registryElement(), eppRegistryDiscipline,
                    EppRegistryElementPartModule.part().number().s());
            eppRegistryModule = modules.get(modules.size() - 1).getModule();
            editModule();
        }
        if (controlActions.hasItems()) {
            List<EppRegistryElementPart> parts = dao.getList(EppRegistryElementPart.class,
                    EppRegistryElementPart.registryElement(), eppRegistryDiscipline,
                    EppRegistryElementPart.number().s());
            eppRegistryElementPart = parts.get(parts.size() - 1);
            editPart();
        }
        activateMVC(ru.tandemservice.uniepp.component.registry.DisciplineRegistry.AddEdit.Controller.class,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, eppRegistryDiscipline.getId()));
        ru.tandemservice.uniepp.component.registry.DisciplineRegistry.AddEdit.Controller controller = (ru.tandemservice.uniepp.component.registry.DisciplineRegistry.AddEdit.Controller) getController();
        controller.onClickFillLoadFromParts(bc);
        controller.onClickApply(bc);
        close();
    }

    private void editModule() {
        activateMVC(ru.tandemservice.uniepp.component.registry.ModuleRegistry.AddEdit.Controller.class,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, eppRegistryModule.getId()));
        ru.tandemservice.uniepp.component.registry.ModuleRegistry.AddEdit.Model model = getModel();

        // Неоптимально, но в случае отсутствия нужного типа Ауд. нагрузки будет стандартное исключение выбора
        Map<EppALoadType, EppRegistryModuleALoad> acceptedALoad = new HashMap<>();
        for (EppRegistryModuleALoad moduleALoad : model.getLoadList()) {
            acceptedALoad.put(moduleALoad.getLoadType(), moduleALoad);
        }

        for (Pair<ChoiceInSelect<EppALoadType>, Integer> loadPair : moduleALoads) {
            EppRegistryModuleALoad elem = acceptedALoad.get(loadPair.getKey().choiceIn(acceptedALoad.keySet()));
            model.setCurrent(elem);
            Integer v = loadPair.getValue() * 100;
            model.getCurrent().setProperty("load", v.longValue());
        }
        ru.tandemservice.uniepp.component.registry.ModuleRegistry.AddEdit.Controller controller = (ru.tandemservice.uniepp.component.registry.ModuleRegistry.AddEdit.Controller) getController();
        controller.onClickApply(bc);
        close();
    }

    private void editPart() {
        activateMVC(ru.tandemservice.uniepp.component.registry.RegElementPartEdit.Controller.class,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, eppRegistryElementPart.getId()));
        ru.tandemservice.uniepp.component.registry.RegElementPartEdit.Model model = getModel();

        model.setSelectedActions(controlActions.choiceIn(model.getControlActionModel()));
        ru.tandemservice.uniepp.component.registry.RegElementPartEdit.Controller controller = (ru.tandemservice.uniepp.component.registry.RegElementPartEdit.Controller) getController();
        controller.onClickApply(bc);
        close();
    }
}
