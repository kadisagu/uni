package ru.tandemservice.uniepp_load.business_logic.tests;


import org.junit.Assert;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.VirtualBcUser;

import java.util.List;
import java.util.Map;

public class BusinessTestCase extends VirtualBcUser {
    protected void assertInList(Map<String, Object> expected, List<DataWrapper> inList) {
        if (inList.size() < 1) {
            Assert.fail("Список пуст");
            return;
        } else {
            DataWrapper row = inList.get(0);
            for (String key : expected.keySet())
                if (!row.containsKey(key)) {
                    Assert.fail("Ключ " + key + " не найден в списке");
                    return;
                }
        }

        for (DataWrapper dw : inList) {
            boolean isCorrespond = true;
            for (String key : expected.keySet()) {
                if (!(expected.get(key).toString().trim().equals(dw.get(key).toString().trim()))) {
                    isCorrespond = false;
                    break;
                }
            }
            if (isCorrespond) return;
        }
        String failMessage = "Соответствие не найдено\n";
        failMessage += "Ожидалось: \n" + expected.values().toString() + "\n Объект для поиска содержит:\n";
        for (DataWrapper dw : inList) {
            failMessage += dw.values().toString() + "\n";
        }
        Assert.fail(failMessage);
    }
}
