package ru.tandemservice.uniepp_load.business_logic.bdc;

import com.google.common.collect.ImmutableMap;
import org.tandemframework.core.component.impl.BusinessComponent;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.ChoiceInSelect;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.ChoiceInUtil;
import ru.tandemservice.uniepp_load.business_logic.bdc_base.VirtualBcUser;


public class GroupBDC extends VirtualBcUser {
    public Group group;

    private OrgUnit formativeOrgUnit;

    public String title = "GroupBDC-" + _random.nextInt(9999);
    public final ChoiceInSelect<Course> course = new ChoiceInSelect<>();
    public final ChoiceInSelect<EducationYear> startEducationYear = new ChoiceInSelect<>();
    public final ChoiceInUtil<EducationOrgUnit> educationOrgUnitUtil = new ChoiceInUtil<>();


    public GroupBDC(EducationOrgUnit educationOrgUnit) {
        this.formativeOrgUnit = educationOrgUnit.getFormativeOrgUnit();
        this.educationOrgUnitUtil.choiceTo(educationOrgUnit);
    }

    public GroupBDC(OrgUnit formativeOrgUnit) {
        this.formativeOrgUnit = formativeOrgUnit;
        this.educationOrgUnitUtil.choiceFirst();
    }


    public GroupBDC add() {
        BusinessComponent bc = activateMVC(ru.tandemservice.uni.component.group.GroupAdd.Controller.class,
                ImmutableMap.<String, Object>of("orgUnitId", formativeOrgUnit.getId()));
        ru.tandemservice.uni.component.group.GroupAdd.Model model = bc.getModel();
        ru.tandemservice.uni.component.group.GroupAdd.Controller controller = (ru.tandemservice.uni.component.group.GroupAdd.Controller) bc.getController();

        group = model.getGroup();
        group.setTitle(title);
        group.setStartEducationYear(startEducationYear.choiceIn(model.getStartEducationYearList()));
        group.setCourse(course.choiceIn(model.getCourseList()));

        educationOrgUnitUtil
                .choice(model::setFormativeOrgUnit, EducationOrgUnit.formativeOrgUnit(), model.getFormativeOrgUnitModel())
                .choice(model::setEducationLevelsHighSchool, EducationOrgUnit.educationLevelHighSchool(), model.getEducationLevelsHighSchoolModel())
                .choice(model::setDevelopForm, EducationOrgUnit.developForm(), model.getDevelopFormModel())
                .choice(model::setDevelopCondition, EducationOrgUnit.developCondition(), model.getDevelopConditionModel())
                .choice(model::setDevelopTech, EducationOrgUnit.developTech(), model.getDevelopTechModel())
                .choice(model::setDevelopPeriod, EducationOrgUnit.developPeriod(), model.getDevelopPeriodModel());

        controller.onClickApply(bc);
        close();
        return this;
    }
}
