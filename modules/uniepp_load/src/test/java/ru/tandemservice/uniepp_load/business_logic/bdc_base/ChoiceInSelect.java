package ru.tandemservice.uniepp_load.business_logic.bdc_base;


import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class ChoiceInSelect<T extends IEntity> extends ChoiceConstants {
    private T entity;

    public ChoiceInSelect() {
        choiceType = FIRST;
    }

    public ChoiceInSelect(T entity) {
        choiceType = SPECIFIC;
        this.entity = entity;
    }

    public ChoiceInSelect choiceTo(T entity) {
        choiceType = SPECIFIC;
        this.entity = entity;
        return this;
    }

    public ChoiceInSelect<T> choiceByValue(Class<T> entityClass, String propertyName, Object propertyValue) {
        choiceType = SPECIFIC;
        entity = DataAccessServices.dao().get(entityClass, propertyName, propertyValue);
        if (null == entity)
            throw new ChoiceException("Не найдена сущность класса " + entityClass + ", где " + propertyName + "=" + propertyValue);
        return this;
    }

    public ChoiceInSelect<T> choiceByCode(Class<T> entityClass, String code) {
        choiceByValue(entityClass, "code", code);
        return this;
    }

    public ChoiceInSelect<T> choiceEmpty() {
        choiceType = EMPTY;
        return this;
    }

    public ChoiceInSelect<T> choiceFirst() {
        choiceType = FIRST;
        return this;
    }

    public T choiceIn(List<T> l) {
        if (null == entity && choiceType == SPECIFIC)
            throw new ChoiceException("Ожидался конкретный экземляр сущности но не был передан");

        if (choiceType == SPECIFIC) {
            if (!l.contains(entity))
                throw new ChoiceException("Объект " + entity.getId() + " нельзя выбрать с селекте");
            return entity;
        }

        if (choiceType == EMPTY)
            return null;

        if (choiceType == FIRST) {
            if (l.size() < 1)
                throw new ChoiceException("Не могу выбрать первое значение в пустом селекте");
            return l.get(0);
        }
        throw new ChoiceException("Заданная опция выбора не поддерживается");

    }

    @SuppressWarnings("unchecked")
    public T choiceIn(ISelectModel selectList) {
        // иначе вернет первые 50. Я думаю столько будет достаточно для всех тестов. Если нет, то проблема скорее в тестах
        //todo вопрос по разновидностям селект моделей. Если селект модель "большая" то нужно сказать ей чтобы загружала много элементов
        //todo число 2000 можно вынести в глобальные настройки тестов
        try {
            ((DQLFullCheckSelectModel) selectList).setPageSize(2000);
        } catch (ClassCastException ignored) {
        }

        try {
            return choiceIn((List<T>) selectList.findValues(null).getObjects());
        } catch (ClassCastException e) {
            throw new ChoiceException("Селект содержит сущности другого типа");
        }
    }

    public T choiceIn(SelectDataSource dataSource) {
        dataSource.setCountRecord(2000);
        return choiceIn(dataSource.getRecords());
    }

    public T choiceIn(Collection<T> tCollection) {
        List<T> l = new ArrayList<>();
        for (T entity : tCollection) l.add(entity);
        return choiceIn(l);
    }

    @SuppressWarnings("unchecked")
    public T choiceInHSelect(List<HSelectOption> hSelectOptionList) {
        List<T> l = new ArrayList<>();
        for (HSelectOption opt : hSelectOptionList) {
            try {
                if (opt.isCanBeSelected()) l.add((T) opt.getObject());
            } catch (ClassCastException e) {
                throw new ChoiceException("Селект содержит сущности другого типа");
            }
        }
        return choiceIn(l);
    }
}