/**
 *$Id:$
 */
package uniepp_load.scripts.print

import org.hibernate.Session
import org.springframework.util.StringUtils
import org.tandemframework.core.common.ITitled
import org.tandemframework.core.entity.EntityComparator
import org.tandemframework.core.entity.EntityOrder
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.OrgUnit
import ru.tandemservice.uni.UniDefines
import ru.tandemservice.uni.dao.UniDaoFacade
import ru.tandemservice.uni.entity.catalog.DevelopForm
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplSimpleTimeItem
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary
import ru.tandemservice.uniepp_load.catalog.entity.EplPrintScript
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleSimple
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplCompensationSourceCodes
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplPrintScriptCodes
import ru.tandemservice.uniepp_load.report.bo.EplReport.util.EplTimeByCompSourceSplitter
import ru.tandemservice.uniepp_load.report.bo.EplReport.util.EplTimeByDevFormSplitter
import ru.tandemservice.uniepp_load.report.bo.EplReport.util.EplTimeSplitter

return new EplReportHourlyFundPrint(                           // входные параметры скрипта
        session: session,                                    // сессия
        summaryId: object                                    // id подразделения
).print()

/**
 * Почасовой фонд по вузу
 */
class EplReportHourlyFundPrint
{
    // входные параметры
    Session session
    Long summaryId

    /**
     * Основной метод печати.
     */
    def print()
    {
        byte[] template = DataAccessServices.dao().getByCode(EplPrintScript.class, EplPrintScriptCodes.HOURLY_FUND).getCurrentTemplate();
        RtfDocument document = RtfDocument.fromByteArray(template);

        printData(document);

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document), fileName: "Распределение почасового фонда " + new Date().format("dd.MM.yyyy") + ".rtf"]
    }

    def printData(RtfDocument document)
    {
        Map<OrgUnit, List<EplOrgUnitSummary>> summaryMap = new HashMap<>();
        for (EplOrgUnitSummary summary : DataAccessServices.dao().getList(EplOrgUnitSummary.class, EplOrgUnitSummary.studentSummary().id(), summaryId)) {
            if (summary.getHourlyFund() == 0) continue;
            SafeMap.safeGet(summaryMap, getFormativeOrgUnit(summary.getOrgUnit()), ArrayList.class).add(summary);
        }

        List<OrgUnit> formativeOrgUnitList = new ArrayList<>(summaryMap.keySet());
        formativeOrgUnitList.sort { x, y -> x.top <=> y.top ?: x.printTitle <=> y.printTitle }

        int hours = 0;
        List<Integer> facultyRowNumbers = new ArrayList<>();
        int rowNumber = 0;
        List<String[]> t1 = new ArrayList<>();
        for (OrgUnit formative: formativeOrgUnitList) {
            facultyRowNumbers.add(rowNumber); rowNumber++;
            List<EplOrgUnitSummary> orgUnits = summaryMap.get(formative);
            orgUnits.sort({it.orgUnit.printTitle})
            int number = 1;
            int hoursT = 0; orgUnits.collect {hoursT += it.getHourlyFund()};
            hours += hoursT;
            t1.add(["", formative.isTop() ? "Общевузовские кафедры" : formative.getPrintTitle(), hoursT] as String[]);
            for (EplOrgUnitSummary ouSummary : orgUnits) {
                t1.add([number++, ouSummary.getOrgUnit().getPrintTitle(), ouSummary.getHourlyFund()] as String[]);
                rowNumber++;
            }
        }

        new RtfTableModifier()
            .put("T1", t1 as String[][])
            .put("T1", new RtfRowIntercepterBase() {
                @Override public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
                    if (facultyRowNumbers.contains(rowIndex))
                        return new RtfString().boldBegin().append(StringUtils.capitalize(value)).boldEnd().toList();
                    return super.beforeInject(table, row, cell, rowIndex, colIndex, value);
                }
            })
            .modify(document);

        EplStudentSummary summary = DataAccessServices.dao().get(EplStudentSummary.class, EplStudentSummary.id(), summaryId);
        new RtfInjectModifier()
            .put("eduYear", summary.eppYear.educationYear.title)
            .put("TT", String.valueOf(hours))
            .modify(document);
    }

    private static OrgUnit getFormativeOrgUnit(OrgUnit orgUnit) {
        if (orgUnit.getParent() == null || UniDaoFacade.getOrgstructDao().isOrgUnitHasKind(orgUnit, UniDefines.CATALOG_ORGUNIT_KIND_FORMING)) {
            return orgUnit;
        }
        return getFormativeOrgUnit(orgUnit.getParent());
    }
}