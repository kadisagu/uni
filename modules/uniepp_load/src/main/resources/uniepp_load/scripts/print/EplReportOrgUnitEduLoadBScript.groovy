/**
 *$Id:$
 */
package uniepp_load.scripts.print

import org.hibernate.Session
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import ru.tandemservice.uni.entity.catalog.DevelopForm
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplSimpleTimeItem
import ru.tandemservice.uniepp_load.catalog.entity.EplPrintScript
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleSimple
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplCompensationSourceCodes
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplPrintScriptCodes
import ru.tandemservice.uniepp_load.report.bo.EplReport.util.EplTimeByCompSourceSplitter
import ru.tandemservice.uniepp_load.report.bo.EplReport.util.EplTimeByDevFormSplitter
import ru.tandemservice.uniepp_load.report.bo.EplReport.util.EplTimeSplitter

return new EplReportOrgUnitEduLoadBPrint(                           // входные параметры скрипта
        session: session,                                    // сессия
        summaryId: object                                    // id подразделения
).print()

/**
 * Печать сводной нагрузки по читающему подразделению
 */
class EplReportOrgUnitEduLoadBPrint
{
    // входные параметры
    Session session
    Long summaryId

    DoubleFormatter formatter = DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS;
    EplTimeSplitter sc;
    EplTimeSplitter sf;

    /**
     * Основной метод печати.
     */
    def print()
    {
        byte[] template = DataAccessServices.dao().getByCode(EplPrintScript.class, EplPrintScriptCodes.ORG_UNIT_SUMMARY_B).getCurrentTemplate();
        RtfDocument document = RtfDocument.fromByteArray(template);

        printData(document);

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document), fileName: "Учебная нагрузка читающего подразделения (форма B) " + new Date().format("dd.MM.yyyy") + ".rtf"]
    }

    def printData(RtfDocument document)
    {
        Map<EplTimeRuleSimple, SimpleTimeItemHours> simpleHoursMap = new HashMap();
        double sHours = 0;
        double sHoursB = 0;
        for (EplSimpleTimeItem item: DataAccessServices.dao().getList(EplSimpleTimeItem.class, EplSimpleTimeItem.summary().id(), summaryId)) {
            SimpleTimeItemHours simpleHours = simpleHoursMap.get(item.getTimeRule());
            if (null == simpleHours) {
                simpleHoursMap.put((EplTimeRuleSimple) item.getTimeRule(), simpleHours = new SimpleTimeItemHours((EplTimeRuleSimple) item.getTimeRule()));
            }
            simpleHours.account(item);
            sHours += item.timeAmount;
            if (!item.isCountAsContract())
            {
                sHoursB += item.getTimeAmount();
            }
        }
        List<SimpleTimeItemHours> simpleHoursList = new ArrayList(simpleHoursMap.values());
        Collections.sort(simpleHoursList);
        List<String[]> t1 = new ArrayList();
        int number = 1;
        for (SimpleTimeItemHours simpleHours : simpleHoursList) {
            t1.add(simpleHours.row(number++));
        }
        new RtfTableModifier().put("T1", t1 as String[][]).modify(document);

        EplOrgUnitSummary summary = DataAccessServices.dao().get(EplOrgUnitSummary.class, EplOrgUnitSummary.id(), summaryId);

        new RtfInjectModifier()
            .put("orgUnitTitle", summary.orgUnit.orgUnitType.title + " «" + summary.orgUnit.title + "»")
            .put("eduYear", summary.studentSummary.eppYear.educationYear.title)
            .put("TT1", formatter.format(sHours))
            .put("TT2", formatter.format(sHoursB)).modify(document);

        sc = new EplTimeByCompSourceSplitter(summaryId);
        sf = new EplTimeByDevFormSplitter(summaryId);

        TotalHours r1 = new TotalHours(DevelopFormCodes.FULL_TIME_FORM);
        TotalHours r2 = new TotalHours(DevelopFormCodes.CORESP_FORM);
        TotalHours r3 = new TotalHours(DevelopFormCodes.PART_TIME_FORM);

        for (EplEduGroupTimeItem item: DataAccessServices.dao().getList(EplEduGroupTimeItem.class, EplEduGroupTimeItem.summary().id(), summaryId)) {
            r1.account(item);
            r2.account(item);
            r3.account(item);
        }

        List<String[]> t2 = new ArrayList<>();
        t2.add([formTitle(r1.formCode), s(r1.hours), s(r1.hoursB), s(sHours), s(sHoursB), s(r1.hours + sHours), s(r1.hoursB + sHoursB)] as String[]);
        t2.add([formTitle(r2.formCode), "", "", "", "", s(r2.hours), s(r2.hoursB)] as String[]);
        t2.add([formTitle(r3.formCode), "", "", "", "", s(r3.hours), s(r3.hoursB)] as String[]);
        t2.add(["Всего", "", "", "", "", s(r1.hours + r2.hours + r3.hours + sHours), s(r1.hoursB + r2.hoursB + r3.hoursB + sHoursB)] as String[]);

        new RtfTableModifier().put("T2", t2 as String[][]).modify(document);
    }

    private static String formTitle(String form) {
        DataAccessServices.dao().get(DevelopForm.class, DevelopForm.code(), form).title
    }

    private String s(double v) {
        return formatter.format(v);
    }

    private class SimpleTimeItemHours implements Comparable<SimpleTimeItemHours> {
        EplTimeRuleSimple rule;
        double parameter = 0;
        double parameterB = 0;
        double hours = 0;
        double hoursB = 0;

        SimpleTimeItemHours(EplTimeRuleSimple rule)
        {
            this.rule = rule
        }

        String[] row(int number) {
            return [number, rule.title, rule.parameterNeeded ? s(parameter) : "", rule.parameterNeeded ? s(parameterB) : "", s(hours), s(hoursB)] as String[];
        }

        void account(EplSimpleTimeItem item) {
            parameter += item.parameter;
            hours += item.timeAmount;
            if (!item.isCountAsContract()) {
                parameterB += item.getParameter();
                hoursB += item.getTimeAmount();
            }
        }

        @Override int compareTo(SimpleTimeItemHours o) {
            return rule.getTitle().compareTo(o.getRule().getTitle());
        }
    }

    private class TotalHours implements Comparable<TotalHours> {
        String formCode;
        double hours = 0;
        double hoursB = 0;

        TotalHours(String formCode)
        {
            this.formCode = formCode
        }

        void account(EplEduGroupTimeItem item) {
            double accHours = item.timeAmount * sf.c(item, formCode)
            hours += accHours;
            hoursB += accHours * sc.c(item, EplCompensationSourceCodes.COMPENSATION_TYPE_BUDGET);
        }

        @Override int compareTo(TotalHours o) {
            return formCode.compareTo(o.formCode);
        }
    }
}