/**
 *$Id:$
 */
package uniepp_load.scripts.print

import com.google.common.collect.ComparisonChain
import com.google.common.collect.Lists
import com.google.common.collect.Maps
import com.google.common.collect.Sets
import jxl.Workbook
import jxl.WorkbookSettings
import jxl.format.Alignment
import jxl.format.Border
import jxl.format.BorderLineStyle
import jxl.format.VerticalAlignment
import jxl.write.*
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.collections.keyvalue.MultiKey
import org.apache.commons.lang.ObjectUtils
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.common.ITitled
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.shared.commonbase.base.util.JExcelUtil
import org.tandemframework.shared.commonbase.base.util.key.PairKey
import org.tandemframework.shared.commonbase.utils.DQLSimple
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.EplOuSummaryManager
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.IEplOuSummaryDAO
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCategoryWrapper
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCommonRowWrapper
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplDiscOrRuleRowWrapper
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplSimpleRuleRowWrapper
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplBaseCategoryDataWrapper
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplBaseTimeDataWrapper
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplCommonRowWrapperComparator
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.distribution.EplDistributionCommonRowWrapper
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.distribution.EplDistributionDiscRowWrapper
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.distribution.EplDistributionSimpleRuleRowWrapper
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.distribution.EplDistributionSubRowWrapper
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup
import ru.tandemservice.uniepp_load.base.entity.ouSummary.*
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem
import ru.tandemservice.uniepp_load.util.UniEppLoadUtils

return new EplOrgUnitSummaryPrint(                           // входные параметры скрипта
        session: session,                                    // сессия
        summaryId: object                                    // id подразделения
).print()

/**
 * Печать сводной нагрузки по читающему подразделению
 */
class EplOrgUnitSummaryPrint
{
    // входные параметры
    Session session
    Long summaryId

    // данные для печати, заполняются в методе prepareData
    ByteArrayOutputStream out = new ByteArrayOutputStream();

    EplOrgUnitSummary summary
    EplBaseTimeDataWrapper dataWrapper
    EplBaseTimeDataWrapper dataDistributionWrapper
    EplBaseCategoryDataWrapper categoryDataWrapper
    Set<Long> discCategorySet = Sets.newHashSet()

    List<EplCommonRowWrapper> tableRows = Lists.newArrayList()
    List<EplDistributionDiscRowWrapper> tableDistributionEduGroupRows = Lists.newArrayList()
    List<EplDistributionCommonRowWrapper> tableDistributionRows = Lists.newArrayList()

    Map<Long, String> ppsScDegreeMap = Maps.newHashMap()
    Map<Long, String> ppsScStatusMap = Maps.newHashMap()

    List<EplTimeItem> timeItems = Lists.newArrayList()

    /**
     * Основной метод печати.
     */
    def print()
    {
        // создаем печатную форму
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        def workbook = Workbook.createWorkbook(out, ws);

        printData(workbook);

        workbook.write();
        workbook.close();

        // стандартные выходные параметры скрипта
        return [document: out.toByteArray(), fileName: "Учебная нагрузка читающего подразделения (форма A) " + new Date().format("dd.MM.yyyy") + ".xls"]
    }

    def printData(WritableWorkbook workbook)
    {
        prepareTimeData()

        CellFormats formats = new CellFormats()

        doCreateSheetSummary(workbook, formats)
        doCreateSheetOnPps(workbook, formats)
        doCreateSheetTransfer(workbook, formats)
        doCreateSheetEduGroups(workbook, formats)
        doCreateSheetData(workbook, formats)
    }

    private void doCreateSheetSummary(WritableWorkbook workbook, CellFormats formats)
    {
        def sheet = workbook.createSheet("Сводная", 0);

        sheet.addCell(new Label(0, 0, summary.title, formats.hl));
        sheet.mergeCells(0, 0, 5, 0);

        int rowNum = 1;
        sheet.addCell(new Label(0, rowNum, "Мероприятие реестра, норма времени", formats.hc));
        sheet.addCell(new Label(1, rowNum, "Передано с", formats.hc));
        sheet.setColumnView(0, 50);
        sheet.setColumnView(1, 12);
        if (categoryLevelCount > 1)
        {
            sheet.mergeCells(0, rowNum, 0, rowNum + categoryLevelCount - 1);
            sheet.mergeCells(1, rowNum, 1, rowNum + categoryLevelCount - 1);
        }

        int colNum = 2;
        for (EplCategoryWrapper c : categoryDataWrapper.topLevelCategories) {
            colNum = addCategoryCell(sheet, c, colNum, rowNum, formats);
        }

        sheet.addCell(new Label(colNum, rowNum, "Всего", formats.hc));
        if (categoryLevelCount > 1) { sheet.mergeCells(colNum, rowNum, colNum, rowNum + categoryLevelCount - 1); }

        rowNum = rowNum + categoryLevelCount;

        int rowDataStart = rowNum;
        for (EplCommonRowWrapper tableRow : tableRows)
        {
            def transferTo = null
            if (tableRow instanceof EplDiscOrRuleRowWrapper)
            {
                transferTo = tableRow.transferTo

                discCategorySet.addAll(tableRow.groups.keySet())
                discCategorySet.addAll(tableRow.transferGroups.keySet())
            }

            sheet.addCell(new Label(0, rowNum, tableRow.title, formats.dl));
            sheet.addCell(new Label(1, rowNum, transferTo?.orgUnit?.shortTitle, formats.dl));
            printTime(tableRow.timeMap, sheet, 2, rowNum++, formats, false)
        }

        colNum = 0
        sheet.addCell(new Label(colNum++, rowNum, "Всего", formats.hl));
        sheet.addCell(new Label(colNum++, rowNum, "", formats.hl));

        for (EplCategoryWrapper c : categoryDataWrapper.bottomLevelCategories)
            sheet.addCell(new Formula(colNum, rowNum, "SUM(" + JExcelUtil.getRange(colNum, rowDataStart, colNum++, rowNum-1) + ")", formats.hnum));

        sheet.addCell(new Formula(colNum, rowNum, "SUM(" + JExcelUtil.getRange(colNum, rowDataStart, colNum, rowNum-1) + ")", formats.hnum));
    }

    private void doCreateSheetOnPps(WritableWorkbook workbook, CellFormats formats)
    {
        def sheet = workbook.createSheet("По ППС", 1);

        int colNum = 0
        int rowNum = 1;
        sheet.addCell(new Label(colNum, rowNum, "Мероприятие реестра, норма времени", formats.hc));
        sheet.setColumnView(colNum, 50);
        sheet.addCell(new Label(++colNum, rowNum, "Передано с", formats.hc))
        sheet.setColumnView(colNum, 12)
        sheet.addCell(new Label(++colNum, rowNum, "Число студентов", formats.hc));
        sheet.setColumnView(colNum, 10);
        sheet.addCell(new Label(++colNum, rowNum, "Вид УГС", formats.hc));
        sheet.setColumnView(colNum, 5);
        sheet.addCell(new Label(++colNum, rowNum, "План. поток", formats.hc));
        sheet.setColumnView(colNum, 20);
        sheet.addCell(new Label(++colNum, rowNum, "Группа", formats.hc));
        sheet.setColumnView(colNum, 10);
        sheet.addCell(new Label(++colNum, rowNum, "Курс", formats.hc));
        sheet.setColumnView(colNum, 10);
        sheet.addCell(new Label(++colNum, rowNum, "Семестр", formats.hc));
        sheet.setColumnView(colNum, 10);
        sheet.addCell(new Label(++colNum, rowNum, "Часть года", formats.hc));
        sheet.setColumnView(colNum, 16);
        sheet.addCell(new Label(++colNum, rowNum, "НПП", formats.hc));
        sheet.setColumnView(colNum, 20);

        if (categoryLevelCount > 1) {
            for (int i = 0; i <= colNum; i++)
                sheet.mergeCells(i, rowNum, i, rowNum + categoryLevelCount - 1);
        }

        colNum++;
        for (EplCategoryWrapper c : categoryDataWrapper.topLevelCategories) {
            colNum = addCategoryCell(sheet, c, colNum, rowNum, formats);
        }

        sheet.addCell(new Label(colNum, rowNum, "Всего", formats.hc));
        sheet.addCell(new Label(colNum+1, rowNum, "Сверх ставки", formats.hc));
        sheet.addCell(new Label(colNum+2, rowNum, "ППС", formats.hc));
        sheet.setColumnView(colNum+2, 30);
        sheet.addCell(new Label(colNum+3, rowNum, "Ученая степень", formats.hc));
        sheet.setColumnView(colNum+3, 30);
        sheet.addCell(new Label(colNum+4, rowNum, "Ученое звание", formats.hc));
        sheet.setColumnView(colNum+4, 30);
        sheet.addCell(new Label(colNum+5, rowNum, "Должность", formats.hc));
        sheet.setColumnView(colNum+5, 30);
        sheet.addCell(new Label(colNum+6, rowNum, "Ставка", formats.hc));

        if (categoryLevelCount > 1) {
            for (int c = colNum; c < colNum + 7; c++) {
                sheet.mergeCells(c, rowNum, c, rowNum + categoryLevelCount - 1);
            }
        }

        rowNum = rowNum + categoryLevelCount
        colNum++

        List<EplDistributionCommonRowWrapper.TitleColumnType> numColumns = Lists.asList(
                EplDistributionCommonRowWrapper.TitleColumnType.STUDENT_COUNT,
                EplDistributionCommonRowWrapper.TitleColumnType.COURSE,
                EplDistributionCommonRowWrapper.TitleColumnType.TERM
        )

        for (EplDistributionCommonRowWrapper parent : tableDistributionRows)
        {
            if (parent.children.empty)
            {
                EplDistributionSubRowWrapper subRow = new EplDistributionSubRowWrapper(null, parent)
                subRow.initTimeFromParent()
                subRow.timeSumAsLong = parent.timeSumAsLong

                parent.children.add(subRow)
            }

            // добавим подстроки для нераспределенного времени
            EplDistributionSubRowWrapper rest = new EplDistributionSubRowWrapper(null, parent)

            Map<Long, Long> restDistributionTimeMap = Maps.newHashMap()
            restDistributionTimeMap.putAll(parent.distributionTimeMap)
            rest.distributionTimeMap = restDistributionTimeMap
            rest.timeSumAsLong = parent.timeSumAsLong

            for (EplDistributionSubRowWrapper sub : parent.children)
            {
                for (Map.Entry<Long, Long> e : sub.distributionTimeMap.entrySet())
                {
                    Long sum = rest.distributionTimeMap.get(e.key)
                    if (null == sum) sum = 0L;
                    rest.distributionTimeMap.put(e.key, sum - e.value)
                    rest.timeSumAsLong -= sub.timeSumAsLong;
                }
            }

            boolean restExists = false
            for (Long sum : rest.distributionTimeMap.values())
                restExists = restExists || sum != 0
            if (restExists)
                parent.children.add(rest)
            parent.children.sort()

            for (EplDistributionSubRowWrapper sub : parent.children)
            {
                WritableCellFormat format = sub.timeSumAsLong < 0 ? formats.erl : formats.dl

                int index = 0;

                for (EplDistributionCommonRowWrapper.TitleColumn titleColumn : EplDistributionCommonRowWrapper.TitleColumn.list)
                {
                    def columnType = titleColumn.type
                    String value = sub.parent.getViewProperty(columnType)

                    if (numColumns.contains(columnType) && StringUtils.isNotEmpty(value) && StringUtils.isNumeric(value))
                        sheet.addCell(new Number(index++, rowNum, Double.parseDouble(value), formats.dnum))
                    else
                        sheet.addCell(new Label(index++, rowNum, htmlNewLineToComma(value), format))
                }
                printTimeExact(sub.distributionTimeMap, sheet, 10, rowNum, format)

                if (sub.pps == null)
                {
                    for (int c = colNum; c < colNum + 6; c++)
                        sheet.addCell(new Label(c, rowNum, "", format))
                }
                else
                {
                    def ppsColNum = colNum - 1
                    sheet.addCell(new Label(++ppsColNum, rowNum, sub.overtime ? "да" : "нет", format))
                    sheet.addCell(new Label(++ppsColNum, rowNum, sub.pps.title, format))
                    sheet.addCell(new Label(++ppsColNum, rowNum, ppsScDegreeMap.getOrDefault(sub.pps.ppsEntry?.person?.id, ""), format))
                    sheet.addCell(new Label(++ppsColNum, rowNum, ppsScStatusMap.getOrDefault(sub.pps.ppsEntry?.person?.id, ""), format))
                    sheet.addCell(new Label(++ppsColNum, rowNum, sub.pps.post == null ? "Почасовик" : sub.pps.post.title, format))
                    sheet.addCell(new Label(++ppsColNum, rowNum, sub.pps.staffRateAsString, format))
                }
                rowNum++;
            }
        }
    }

    /**
     * Заменить все переносы строк в стиле html (на основе тэга {@code <div>}) на запятые; а затем удалить все html-тэги.
     * @param inputStr Строка, возможно, содержащая тэги {@code <div>}.
     * @return "abc" -> "abc", "<div>ABC</div>" -> "ABC", "<div>ABC</div><div>DEF</div>" -> "ABC, DEF"
     */
    private static String htmlNewLineToComma(final String inputStr)
    {
        final String[] replaceWhat =    ["</div><div>", "<div>",    "</div>"];
        final String[] replaceByWhat =  [", ",          "",         ""];
        return StringUtils.replaceEach(inputStr, replaceWhat, replaceByWhat);
    }

    private void doCreateSheetTransfer(WritableWorkbook workbook, CellFormats formats)
    {
        def sheet = workbook.createSheet("Передано", 2)

        int colNum = 0
        int rowNum = 0

        sheet.addCell(new Label(colNum, rowNum, summary.title, formats.hl));
        sheet.mergeCells(colNum, rowNum++, 5, colNum);

        sheet.addCell(new Label(colNum, rowNum, "Мероприятие реестра, норма времени", formats.hc))
        if (categoryLevelCount > 1) { sheet.mergeCells(colNum, rowNum, colNum, rowNum + categoryLevelCount - 1) }
        sheet.setColumnView(colNum++, 50)

        sheet.addCell(new Label(colNum, rowNum, "Передано на", formats.hc))
        if (categoryLevelCount > colNum) { sheet.mergeCells(colNum, rowNum, colNum, rowNum + categoryLevelCount - 1) }
        sheet.setColumnView(colNum++, 12)

        for (EplCategoryWrapper c : categoryDataWrapper.transferTopLevelCategories)
            colNum = addCategoryCell(sheet, c, colNum, rowNum, formats)

        sheet.addCell(new Label(colNum, rowNum, "Всего", formats.hc));
        if (categoryLevelCount > 1) { sheet.mergeCells(colNum, rowNum, colNum, rowNum + categoryLevelCount - 1); }

        rowNum = rowNum + categoryLevelCount;

        int rowDataStart = rowNum;
        for (EplCommonRowWrapper tableRow : tableRows)
        {
            if (!(tableRow instanceof EplDiscOrRuleRowWrapper)) continue

            Set<String> transferSummaryTitles = Sets.newHashSet()

            for (EplEduGroupTimeItem item : tableRow.eduGroupTimeItemList)
            {
                if (item instanceof EplTransferOrgUnitEduGroupTimeItem && !item.summary.equals(summary))
                    transferSummaryTitles.add(item.summary.orgUnit.shortTitle)
            }
            if (transferSummaryTitles.empty) continue

            sheet.addCell(new Label(0, rowNum, tableRow.title, formats.dl))
            sheet.addCell(new Label(1, rowNum, StringUtils.join(transferSummaryTitles, ", "), formats.dl))
            printTime(tableRow.timeMap, sheet, 2, rowNum++, formats, true)
        }

        if (!categoryDataWrapper.transferBottomLevelCategories.empty)
        {
            colNum = 0
            sheet.addCell(new Label(colNum++, rowNum, "Всего", formats.hl))
            sheet.addCell(new Label(colNum++, rowNum, "", formats.hl))

            for (EplCategoryWrapper c : categoryDataWrapper.transferBottomLevelCategories)
                sheet.addCell(new Formula(colNum, rowNum, "SUM(" + JExcelUtil.getRange(colNum, rowDataStart, colNum++, rowNum-1) + ")", formats.hnum))

            sheet.addCell(new Formula(colNum, rowNum, "SUM(" + JExcelUtil.getRange(colNum, rowDataStart, colNum, rowNum-1) + ")", formats.hnum))
        }
    }

    private void doCreateSheetEduGroups(WritableWorkbook workbook, CellFormats formats)
    {
        def sheet = workbook.createSheet("План. потоки", 3)

        def categoryDataWrapper = dataDistributionWrapper.categoryDataWrapper
        def categoryLevelCount = categoryDataWrapper.categoryLevelCount

        int colNum = 0
        int rowNum = 1
        sheet.addCell(new Label(colNum, rowNum, "Мероприятие реестра, норма времени", formats.hc));
        if (categoryLevelCount > 1) { sheet.mergeCells(colNum, rowNum, colNum, rowNum + categoryLevelCount - 1) }
        sheet.setColumnView(colNum++, 50)
        sheet.addCell(new Label(colNum, rowNum, "Передано с", formats.hc))
        if (categoryLevelCount > 1) { sheet.mergeCells(colNum, rowNum, colNum, rowNum + categoryLevelCount - 1) }
        sheet.setColumnView(colNum++, 12)

        sheet.addCell(new Label(colNum, rowNum, "План. поток", formats.hc));
        if (categoryLevelCount > 1) { sheet.mergeCells(colNum, rowNum, colNum, rowNum + categoryLevelCount - 1) }
        sheet.setColumnView(colNum++, 20)

        for (EplCategoryWrapper c : categoryDataWrapper.topLevelCategories)
        {
            if (!isUseCategoryWrapper(c)) continue
            colNum = addCategoryCell(sheet, c, colNum, rowNum, formats)
        }

        sheet.addCell(new Label(colNum, rowNum, "Всего", formats.hc));
        if (categoryLevelCount > 1) { sheet.mergeCells(colNum, rowNum, colNum, rowNum + categoryLevelCount - 1); }

        rowNum = rowNum + categoryLevelCount;

        int rowDataStart = rowNum;
        for (EplDistributionDiscRowWrapper tableRow : tableDistributionEduGroupRows)
        {
            sheet.addCell(new Label(colNum = 0, rowNum, tableRow.title, formats.dl));
            sheet.addCell(new Label(++colNum, rowNum, tableRow.transferTo?.orgUnit?.shortTitle, formats.dl));
            sheet.addCell(new Label(++colNum, rowNum, tableRow.eduGroup == null ? "Не указана" : tableRow.eduGroup.title + " (" + tableRow.eduGroup.groupType.shortTitle + ")", formats.dl));
            printTime(tableRow.distributionTimeMap, sheet, ++colNum, rowNum++, formats)
        }

        colNum = 0
        sheet.addCell(new Label(colNum++, rowNum, "Всего", formats.hl))
        sheet.addCell(new Label(colNum++, rowNum, "", formats.hl))
        sheet.addCell(new Label(colNum++, rowNum, "", formats.hl))

        for (EplCategoryWrapper c : categoryDataWrapper.bottomLevelCategories)
        {
            if (!isUseCategoryWrapper(c)) continue
            sheet.addCell(new Formula(colNum, rowNum, "SUM(" + JExcelUtil.getRange(colNum, rowDataStart, colNum++, rowNum-1) + ")", formats.hnum))
        }

        sheet.addCell(new Formula(colNum, rowNum, "SUM(" + JExcelUtil.getRange(colNum, rowDataStart, colNum, rowNum-1) + ")", formats.hnum))
    }

    private boolean isUseCategoryWrapper(EplCategoryWrapper categoryWrapper)
    {
        boolean useCategory = discCategorySet.contains(categoryWrapper.category.id)
        for (EplCategoryWrapper child : categoryWrapper.children)
        {
            useCategory = discCategorySet.contains(child.category.id)
            if (useCategory) break
        }
        return useCategory
    }

    private void doCreateSheetData(WritableWorkbook workbook, CellFormats formats)
    {
        def sheet = workbook.createSheet("Данные", 4)

        int colNum = 0
        int rowNum = 0
        sheet.addCell(new Label(colNum, rowNum, "Категория", formats.hl))
        sheet.setColumnView(colNum, 30)
        sheet.addCell(new Label(++colNum, rowNum, "Норма времени", formats.hl))
        sheet.setColumnView(colNum, 50)
        sheet.addCell(new Label(++colNum, rowNum, "Мероприятие реестра", formats.hl))
        sheet.setColumnView(colNum, 30);
        sheet.addCell(new Label(++colNum, rowNum, "Передано с", formats.hl))
        sheet.setColumnView(colNum, 12)
        sheet.addCell(new Label(++colNum, rowNum, "План. поток", formats.hl))
        sheet.setColumnView(colNum, 15)
        sheet.addCell(new Label(++colNum, rowNum, "Часы", formats.hl))
        sheet.addCell(new Label(++colNum, rowNum, "Описание", formats.hl))
        sheet.setColumnView(colNum, 200)

        rowNum++
        for (EplTimeItem item : timeItems)
        {
            colNum = 0
            sheet.addCell(new Label(colNum, rowNum, item.timeRule.category.title, formats.dl))
            sheet.addCell(new Label(++colNum, rowNum, item.timeRule.title, formats.dl))
            if (item instanceof EplEduGroupTimeItem)
            {
                EplEduGroupTimeItem groupTimeItem = (EplEduGroupTimeItem) item
                sheet.addCell(new Label(++colNum, rowNum, groupTimeItem.registryElementPart.titleWithNumber, formats.dl))
                if (item instanceof EplTransferOrgUnitEduGroupTimeItem)
                {
                    EplTransferOrgUnitEduGroupTimeItem transferTimeItem = (EplTransferOrgUnitEduGroupTimeItem) item
                    sheet.addCell(new Label(++colNum, rowNum, transferTimeItem.transferredFrom?.orgUnit?.shortTitle, formats.dl))
                } else
                    sheet.addCell(new Label(++colNum, rowNum, "", formats.dl))
                sheet.addCell(new Label(++colNum, rowNum, groupTimeItem.eduGroup == null ? "" : (item.eduGroup.title + " (" + item.eduGroup.groupType.shortTitle + ")"), formats.dl))
            } else
            {
                sheet.addCell(new Label(++colNum, rowNum, "", formats.dl))
                sheet.addCell(new Label(++colNum, rowNum, "", formats.dl))
                sheet.addCell(new Label(++colNum, rowNum, "", formats.dl))
            }
            sheet.addCell(new Number(++colNum, rowNum, item.timeAmount, formats.dnum))
            sheet.addCell(new Label(++colNum, rowNum++, item.description, formats.dl))
        }
    }

    private printTime(Map<PairKey<Long, Boolean>, EplCommonRowWrapper.ControlTime> timeMap, sheet, int colNum, int rowNum, CellFormats formats, boolean transferTo)
    {
        def categories = transferTo ? categoryDataWrapper.transferBottomLevelCategories : categoryDataWrapper.bottomLevelCategories
        for (EplCategoryWrapper c : categories)
        {
            def categoryId = c.category.id
            def categoryKey = PairKey.create(categoryId, transferTo)
            def timeAmount = timeMap.get(categoryKey)

            // если существуют переведенные часы, то их не печатаем
            if (!transferTo)
            {
                def transferCategoryKey = PairKey.create(categoryId, true)
                def transferTimeAmount = timeMap.get(transferCategoryKey)
                if (null != transferTimeAmount)
                {
                    timeAmount.time = timeAmount.time - transferTimeAmount.time
                    if (timeAmount == 0d) timeAmount = null
                }
            }

            if (null != timeAmount)
                sheet.addCell(new Number(colNum, rowNum, timeAmount.time, formats.dnum))
            else
                sheet.addCell(new Label(colNum, rowNum, "", formats.dnum))

            colNum++
        }
        sheet.addCell(new Formula(colNum, rowNum, "SUM(" + JExcelUtil.getRange(1, rowNum, colNum - 1, rowNum) + ")", formats.hnum))
    }

    private printTime(Map<Long, Long> timeMap, sheet, int colNum, int rowNum, CellFormats formats)
    {
        for (EplCategoryWrapper c : categoryDataWrapper.bottomLevelCategories)
        {
            def categoryId = c.category.id
            if (!discCategorySet.contains(categoryId)) continue

            def timeAmount = timeMap.get(categoryId)
            if (null != timeAmount)
                sheet.addCell(new Number(colNum, rowNum, UniEppLoadUtils.wrap(timeAmount), formats.dnum))
            else
                sheet.addCell(new Label(colNum, rowNum, "", formats.dnum))

            colNum++
        }
        sheet.addCell(new Formula(colNum, rowNum, "SUM(" + JExcelUtil.getRange(1, rowNum, colNum - 1, rowNum) + ")", formats.hnum));
    }

    private printTimeExact(Map<Long, Long> timeMap, sheet, int colNum, int rowNum, WritableCellFormat format)
    {
        int startTimeCol = colNum;
        for (EplCategoryWrapper c : categoryDataWrapper.bottomLevelCategories)
        {
            def timeAmount = timeMap.get(c.category.id);
            if (null != timeAmount)
                sheet.addCell(new Number(colNum, rowNum, UniEppLoadUtils.wrap(timeAmount), format))
            else
                sheet.addCell(new Label(colNum, rowNum, "", format))

            colNum++
        }
        sheet.addCell(new Formula(colNum, rowNum, "SUM(" + JExcelUtil.getRange(startTimeCol, rowNum, colNum - 1, rowNum) + ")", format))
    }

    private int addCategoryCell(sheet, EplCategoryWrapper c, int colNum, int rowNum, CellFormats formats)
    {
        sheet.addCell(new Label(colNum, rowNum, c.category.shortTitle, formats.hc));
        sheet.mergeCells(colNum, rowNum, colNum + c.columnCount - 1, rowNum + c.getRowCount(categoryLevelCount) - 1);
        int colNumSub = colNum;
        for (EplCategoryWrapper child : c.children) {
            colNumSub = addCategoryCell(sheet, child, colNumSub, rowNum+1, formats);
        }
        return colNum + c.columnCount;
    }

    private int getCategoryLevelCount()
    {
        return categoryDataWrapper.categoryLevelCount
    }

    private void prepareTimeData()
    {
        summary = DataAccessServices.dao().get(EplOrgUnitSummary.class, summaryId)
        dataWrapper = new EplBaseTimeDataWrapper(summary, true, true).fillDataSource(null, true)
        categoryDataWrapper = dataWrapper.categoryDataWrapper

        Set<String> propertyPathSet = Sets.newHashSet(EplEduGroupTimeItem.L_EDU_GROUP)
        dataDistributionWrapper = new EplBaseTimeDataWrapper(summary, false, true).fillDataSource(propertyPathSet, false)

        tableRows = dataWrapper.allRows
        Collections.sort(tableRows, EplCommonRowWrapperComparator.INSTANCE)

        IEplOuSummaryDAO.IDistributionEduGroupData eduGroupData = EplOuSummaryManager.instance().dao().prepareDistributionEduGroupData(Collections.singleton(summary), null, true)

        dataDistributionWrapper.discRowMap.entrySet().each { Map.Entry<String, Map<EplCommonRowWrapper.DisciplineOrRulePairKey, EplDiscOrRuleRowWrapper>> entry ->
            for (EplDiscOrRuleRowWrapper wrapper : entry.value.values())
            {
                if (CollectionUtils.isEmpty(wrapper.getEplGroups())) continue

                EplEduGroup eduGroup = wrapper.getEplGroups().iterator().next()
                if (null == eduGroup) continue

                EplDistributionDiscRowWrapper rowWrapper = new EplDistributionDiscRowWrapper(wrapper)
                Set<EplStudent> students = dataDistributionWrapper.groupMap.getOrDefault(eduGroup, Collections.emptyMap()).keySet()
                rowWrapper.init(eduGroup, eduGroupData, students)
                tableDistributionEduGroupRows.add(rowWrapper)
            }
        }

        // добавляем простые норм
        List<EplDistributionSimpleRuleRowWrapper> ruleRows = Lists.newArrayList()
        dataDistributionWrapper.simpleRuleRowMap.entrySet().each { Map.Entry<String, Map<EplCommonRowWrapper.DisciplineOrRulePairKey, EplSimpleRuleRowWrapper>> entry ->
            entry.value.values().each { EplSimpleRuleRowWrapper wrapper ->
                EplDistributionSimpleRuleRowWrapper simpleWrapper = new EplDistributionSimpleRuleRowWrapper(wrapper)
                wrapper.getSimpleTimeItemList().each { timeItem -> simpleWrapper.addTime(timeItem)}
                ruleRows.add(simpleWrapper)
            }
        }

        Comparator<EplDistributionDiscRowWrapper> distributionComparator = new Comparator<EplDistributionDiscRowWrapper>() {
            @Override
            int compare(EplDistributionDiscRowWrapper d1, EplDistributionDiscRowWrapper d2)
            {
                return ComparisonChain.start()
                        .compare(d1, d2, EplCommonRowWrapperComparator.INSTANCE)
                        .compare(d1.getMinCourse(), d2.getMinCourse())
                        .compare(d1.getEduGroup().getTitle(), d2.getEduGroup().getTitle())
                        .compare(d1.getEduGroup().getGroupType().getPriority(), d2.getEduGroup().getGroupType().getPriority())
                        .result()
            }
        }
        Collections.sort(tableDistributionEduGroupRows, distributionComparator)
        Collections.sort(ruleRows, ITitled.TITLED_COMPARATOR);

        tableDistributionRows.addAll(tableDistributionEduGroupRows)
        tableDistributionRows.addAll(ruleRows)

        Map<MultiKey, EplDistributionCommonRowWrapper> distributionRowByKeyMap = Maps.newHashMap()
        tableDistributionRows.each { row -> distributionRowByKeyMap.put(row.key, row) }

        for (EplCommonRowWrapper rowWrapper : tableRows)
        {
            if (rowWrapper instanceof EplDiscOrRuleRowWrapper)
            {
                timeItems.addAll(rowWrapper.eduGroupTimeItemList)
            }
            else if (rowWrapper instanceof EplSimpleRuleRowWrapper)
            {
                timeItems.addAll(rowWrapper.simpleTimeItemList)
            }
        }
        Collections.sort(timeItems, TIME_ITEM_COMPARATOR)

        // загружаем текущее распределение времени
        Set<Long> ppsPersons = Sets.newHashSet()
        List<EplPlannedPpsTimeItem> ppsTimeItems = new DQLSimple<>(EplPlannedPpsTimeItem.class).where(EplPlannedPpsTimeItem.pps().orgUnitSummary(), summary).list()
        for (EplPlannedPpsTimeItem ppsTimeItem : ppsTimeItems)
        {
            EplTimeItem timeItem = ppsTimeItem.timeItem
            EplTimeRuleEduLoad rule = timeItem.timeRule
            String groupingCode = rule.grouping.code
            EppRegistryElementPart discipline = null
            EplEduGroup eduGroup = null

            if (timeItem instanceof EplEduGroupTimeItem)
            {
                EplEduGroupTimeItem eduGroupTimeItem = (EplEduGroupTimeItem) timeItem
                if (null == eduGroupTimeItem.eduGroup) continue

                discipline = eduGroupTimeItem.registryElementPart
                eduGroup = eduGroupTimeItem.eduGroup
            }

            if (timeItem instanceof EplSimpleTimeItem)
                groupingCode = EplSimpleRuleRowWrapper.BY_SIMPLE_TIME_RULE;

            Object[] groupKeys = new Object[1];
            groupKeys[0] = eduGroup == null ? null : eduGroup.id
            MultiKey rowKey = EplDistributionCommonRowWrapper.getKey(discipline, rule, groupingCode, groupKeys)
            EplDistributionCommonRowWrapper rowWrapper = distributionRowByKeyMap.get(rowKey)

            // сохраняем в подстроки - вдруг распределили только часть времени, или по разным ППС
            EplDistributionSubRowWrapper subRow = null
            for (EplDistributionSubRowWrapper subRowWrapper : rowWrapper.children)
            {
                if (subRowWrapper.pps.equals(ppsTimeItem.pps) && ObjectUtils.equals(subRowWrapper.overtime, ppsTimeItem.overTime))
                {
                    subRow = subRowWrapper
                    break
                }
            }
            if (null == subRow)
            {
                subRow = new EplDistributionSubRowWrapper(null, rowWrapper)
                subRow.setPps(ppsTimeItem.pps)
                subRow.setOvertime(ppsTimeItem.overTime)
                rowWrapper.children.add(subRow)
            }
            subRow.addTime(timeItem.timeRule.category, ppsTimeItem.timeAmountAsLong)

            if (null != ppsTimeItem.pps.ppsEntry)
                ppsPersons.add(ppsTimeItem.pps.ppsEntry.person.id)
        }

        // загрузим ученые степени и звания
        List<PersonAcademicDegree> degrees = DataAccessServices.dao().getList(PersonAcademicDegree.class, PersonAcademicDegree.person().id(), ppsPersons)
        ppsPersons.each { person -> ppsScDegreeMap.put(person, degrees.findAll {d -> d.person.id.equals(person)}.collect {d -> d.academicDegree.title} .join(", ")) }

        List<PersonAcademicStatus> statuses = DataAccessServices.dao().getList(PersonAcademicStatus.class, PersonAcademicStatus.person().id(), ppsPersons)
        ppsPersons.each { person -> ppsScStatusMap.put(person, statuses.findAll {d -> d.person.id.equals(person)}.collect {d -> d.fullTitle} .join(", ")) }
    }

    private static class CellFormats
    {
        final WritableCellFormat dl;
        final WritableCellFormat dnum;
        final WritableCellFormat erl;
        final WritableCellFormat ernum;
        final WritableCellFormat hl;
        final WritableCellFormat hc;
        final WritableCellFormat hnum;

        private CellFormats() throws Throwable
        {
            WritableFont baseFont = new WritableFont(WritableFont.ARIAL, 9)
            WritableFont erFont = new WritableFont(WritableFont.ARIAL, 9)
            erFont.setColour(jxl.format.Colour.RED)
            WritableFont boldFont = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD)

            dl = new WritableCellFormat(baseFont);
            dl.setBorder(Border.ALL, BorderLineStyle.THIN);
            dl.setAlignment(Alignment.LEFT);
            dl.setVerticalAlignment(VerticalAlignment.CENTRE);

            dnum = new WritableCellFormat(baseFont);
            dnum.setBorder(Border.ALL, BorderLineStyle.THIN);

            erl = new WritableCellFormat(erFont);
            erl.setBorder(Border.ALL, BorderLineStyle.THIN);
            erl.setAlignment(Alignment.LEFT);
            erl.setVerticalAlignment(VerticalAlignment.CENTRE);

            ernum = new WritableCellFormat(erFont);
            ernum.setBorder(Border.ALL, BorderLineStyle.THIN);

            hl = new WritableCellFormat(boldFont);
            hl.setBorder(Border.ALL, BorderLineStyle.THIN);
            hl.setAlignment(Alignment.LEFT);
            hl.setVerticalAlignment(VerticalAlignment.CENTRE);
            hl.setWrap(true);

            hc = new WritableCellFormat(boldFont);
            hc.setBorder(Border.ALL, BorderLineStyle.THIN);
            hc.setAlignment(Alignment.CENTRE);
            hc.setVerticalAlignment(VerticalAlignment.CENTRE);
            hc.setWrap(true);

            hnum = new WritableCellFormat(boldFont);
            hnum.setBorder(Border.ALL, BorderLineStyle.THIN);
        }
    }

    private static Comparator TIME_ITEM_COMPARATOR = new Comparator<EplTimeItem>() {
        @Override
        int compare(EplTimeItem o1, EplTimeItem o2)
        {
            def result = o1.timeRule.category.title.compareTo(o2.timeRule.category.title)
            if (0 == result) result = o1.timeRule.title.compareTo(o2.timeRule.title)
            if (0 == result && o1 instanceof EplEduGroupTimeItem && o2 instanceof EplEduGroupTimeItem)
            {
                EplEduGroupTimeItem gi1 = (EplEduGroupTimeItem) o1
                EplEduGroupTimeItem gi2 = (EplEduGroupTimeItem) o2
                if (0 == result) result = gi1.registryElementPart.titleWithNumber.compareTo(gi2.registryElementPart.titleWithNumber)
                if (gi1.eduGroup() == null && gi2.eduGroup != null)
                {
                    result = 1;
                } else if (gi1.eduGroup != null && gi2.eduGroup == null)
                {
                    result = -1;
                } else if (gi1.eduGroup != null && gi2.eduGroup != null)
                {
                    result = gi1.eduGroup.groupType.priority - gi2.eduGroup.groupType.priority
                    if (0 == result) result = gi1.eduGroup.title.compareTo(gi2.eduGroup.title)
                }
            }
            if (0 == result) result = o1.id.compareTo(o2.id)
            return result;
        }
    }
}