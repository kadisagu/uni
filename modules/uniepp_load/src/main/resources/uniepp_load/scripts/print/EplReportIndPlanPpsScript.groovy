package uniepp_load.scripts.print

import com.google.common.collect.Lists
import com.google.common.collect.Maps
import jxl.Workbook
import jxl.WorkbookSettings
import jxl.format.Orientation
import jxl.format.PageOrientation
import jxl.format.Alignment
import jxl.format.BorderLineStyle
import jxl.format.Border
import jxl.format.VerticalAlignment
import jxl.write.*
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang.ArrayUtils
import org.hibernate.Session
import org.tandemframework.shared.commonbase.base.util.JExcelUtil
import org.tandemframework.shared.commonbase.base.util.key.PairKey
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.uni.entity.catalog.YearDistributionPart
import ru.tandemservice.uni.entity.employee.pps.PpsEntry
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.EplIndividualPlanManager
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.logic.wrapper.EplNonEduLoadCommonWrapper
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.logic.wrapper.EplNonEduLoadRowWrapper
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.EplOuSummaryManager
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCategoryWrapper
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.assignment.EplAssignmentCommonWrapper
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.assignment.EplAssignmentRowWrapper
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplBaseCategoryDataWrapper
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary

import static jxl.biff.CellReferenceHelper.getCellReference

return new EplReportIndPlanPpsPrint(                                // входные параметры скрипта
        session: session,                                           // сессия
        indPlan: session.get(EplIndividualPlan.class, indPlanId)    // id ИПП
).print()

/**
 * Печать ИПП
 */
class EplReportIndPlanPpsPrint
{
    // входные параметры
    Session session
    EplIndividualPlan indPlan
    EplAssignmentCommonWrapper eduLoadDataWrapper
    EplNonEduLoadCommonWrapper nonEduLoadDataWrapper
    Map<YearDistributionPart, EplAssignmentCommonWrapper.Data> dataMap

    List<EplOrgUnitSummary> ouSummaries
    PpsEntry pps
    String ppsDegreesAndStatuses

    ByteArrayOutputStream out = new ByteArrayOutputStream()

    int sheetIndex = 0
    WritableSheet sheetTitle
    Map<Integer, PairKey<Integer, Integer>> sheetToTotalValueMap = Maps.newHashMap()
    PairKey<Integer, Integer> startFillTotalValues

    /**
     * Основной метод печати.
     */
    def print()
    {
        // создаем печатную форму
        WorkbookSettings ws = new WorkbookSettings()
        ws.setMergedCellChecking(false)
        ws.setRationalization(false)
        ws.setGCDisabled(true)
        def workbook = Workbook.createWorkbook(out, ws)

        printData(workbook)

        workbook.write()
        workbook.close()

        return [document: out.toByteArray(), fileName: "ИПП " + pps.fio + " " + new Date().format("dd.MM.yyyy") + ".xls"]
    }

    def printData(WritableWorkbook workbook)
    {
        CellFormats formats = new CellFormats()
        prepareTimeData()

        doCreateTitle(workbook, formats)

        if (CollectionUtils.isNotEmpty(ouSummaries))
            doCreateEduLoad(workbook, formats)

        doCreateNonEduLoad(workbook, formats)

        fillTimeItemForTitle(workbook, formats)
    }

    private void prepareTimeData()
    {
        ouSummaries = EplIndividualPlanManager.instance().dao().getOuSummaries(indPlan)
        pps = indPlan.pps
        ppsDegreesAndStatuses = pps.degreesAndStatuses

        if (CollectionUtils.isNotEmpty(ouSummaries))
        {
            eduLoadDataWrapper = EplOuSummaryManager.instance().dao().getEplTimeDataWrapperForPps(indPlan)
            dataMap = eduLoadDataWrapper.yearPartToDataMap
        }
        nonEduLoadDataWrapper = new EplNonEduLoadCommonWrapper(indPlan)
    }

    private void doCreateTitle(WritableWorkbook workbook, CellFormats formats)
    {
        def topOrgUnit = TopOrgUnit.getInstance()

        def orgUnit = indPlan.orgUnit
        def head = EmployeeManager.instance().dao().getHead(orgUnit)
        def post = pps instanceof PpsEntryByEmployeePost ? "Должность" : "Внештатный преподаватель"

        int rowNum = 0
        int[] columnViews = [3, 3, 45, 10, 20, 15, 20, 10]

        sheetTitle = workbook.createSheet("Титул", sheetIndex)
        fillSheetSettings(sheetTitle)
        fillColumnViews(sheetTitle, columnViews)

        sheetTitle.addCell(new Label(1, rowNum, topOrgUnit.title, formats.hf1))
        sheetTitle.mergeCells(1, rowNum, 7, rowNum += 2)

        sheetTitle.addCell(new Label(5, ++rowNum, "«УТВЕРЖДАЮ»", formats.df_c))
        sheetTitle.addCell(new Label(5, ++rowNum, head == null ? "" : head.postRelation.postBoundedWithQGandQL.post.title, formats.df_c))
        sheetTitle.mergeCells(5, rowNum, 7, rowNum)

        sheetTitle.addCell(new Label(5, ++rowNum, "", formats.line))
        sheetTitle.addCell(new Label(6, rowNum, "" + (head == null ? "" : head.employee.person.identityCard.fio), formats.df_c))
        sheetTitle.addCell(new Label(5, ++rowNum, "подпись", formats.df_c6))
        sheetTitle.setRowView(rowNum, 10 * 20)

        sheetTitle.addCell(new Label(5, ++rowNum, "«___» ________________ 20__ года", formats.df_c))
        sheetTitle.mergeCells(5, rowNum, 6, rowNum++)

        sheetTitle.addCell(new Label(1, ++rowNum, "Индивидуальный план работы преподавателя на " + indPlan.eppYear.title+ " уч. год", formats.hf2))
        sheetTitle.mergeCells(1, rowNum, 7, rowNum)

        sheetTitle.addCell(new Label(1, rowNum += 2, orgUnit?.orgUnitType?.title, formats.dfb_l))
        sheetTitle.mergeCells(1, rowNum, 2, rowNum)
        sheetTitle.addCell(new Label(3, rowNum, orgUnit?.title, formats.df_l))

        sheetTitle.addCell(new Label(1, ++rowNum, "Преподаватель", formats.dfb_l))
        sheetTitle.mergeCells(1, rowNum, 2, rowNum)
        sheetTitle.addCell(new Label(3, rowNum, pps.fullFio, formats.df_l))

        sheetTitle.addCell(new Label(1, ++rowNum, post, formats.dfb_l))
        sheetTitle.mergeCells(1, rowNum, 2, rowNum)
        sheetTitle.addCell(new Label(3, rowNum, pps.titlePostOrTimeWorkerData, formats.df_l))

        sheetTitle.addCell(new Label(1, ++rowNum, "Ученая степень, ученое звание", formats.dfb_l))
        sheetTitle.mergeCells(1, rowNum, 2, rowNum)
        sheetTitle.addCell(new Label(3, rowNum++, ppsDegreesAndStatuses, formats.df_l))

        sheetTitle.addCell(new Label(1, ++rowNum, "№", formats.dfc_b))
        sheetTitle.addCell(new Label(2, rowNum, "Нагрузка", formats.dfc_b))
        sheetTitle.addCell(new Label(3, rowNum, "Часов", formats.dfc_b))

        List<String> sheetTitles = Lists.newLinkedList()
        if (CollectionUtils.isNotEmpty(ouSummaries))
        {
            eduLoadDataWrapper.yearParts.each { yearPart -> sheetTitles.add(getHeaderTitle(yearPart, "Практики и государственная итоговая аттестация")) }
            sheetTitles.add("Другие виды нагрузки")
        }
        sheetTitles.add("Внеучебная нагрузка")

        double index = 0
        int rowTotalStart = rowNum + 1
        int colTotalStart = 3
        for (def title : sheetTitles)
        {
            sheetTitle.addCell(new Number(1, ++rowNum, ++index, formats.dfc))
            sheetTitle.addCell(new Label(2, rowNum, title, formats.dfl))
        }

        sheetTitle.addCell(new Label(1, ++rowNum, "", formats.dfl_b2))
        sheetTitle.addCell(new Label(2, rowNum, "Всего часов", formats.dfl_b3))
        sheetTitle.addCell(new Formula(3, rowNum, "SUM(" + JExcelUtil.getRange(colTotalStart, rowTotalStart, colTotalStart, rowNum-1) + ")", formats.dfr_b))

        startFillTotalValues = PairKey.create(colTotalStart, rowTotalStart)
    }

    private void doCreateEduLoad(WritableWorkbook workbook, CellFormats formats)
    {
        for (def yearPart : eduLoadDataWrapper.yearParts)
            printSheet(workbook, formats, yearPart, false)
        printSheet(workbook, formats, null, true)
    }

    private void doCreateNonEduLoad(WritableWorkbook workbook, CellFormats formats)
    {
        def sheetTitle = "Внеучебная нагрузка"
        def sheet = workbook.createSheet(sheetTitle, ++sheetIndex)

        int[] columnViews = [3, 52, 20, 12, 12, 12, 12, 20]
        fillSheetSettings(sheet)
        fillColumnViews(sheet, columnViews)

        int colNum = 0
        int rowNum = createHeader(sheet, formats, 1, 0, sheetTitle, true)
        sheet.setRowView(++rowNum, 20 * 20)

        for (def column : nonEduLoadDataWrapper.columns)
            sheet.addCell(new Label(colNum++, rowNum, column.title, formats.dfc))

        colNum = 0
        rowNum++
        for (def row : nonEduLoadDataWrapper.rows)
        {
            createNonEduLoadRow(sheet, formats, colNum, rowNum++, row)
            for (def child : row.children)
            {
                createNonEduLoadRow(sheet, formats, colNum, rowNum++, child)
            }
        }
    }

    private void printSheet(WritableWorkbook workbook, CellFormats formats, YearDistributionPart yearPart, boolean otherLoad)
    {
        def currentData = dataMap.get(yearPart)
        def categoryDataWrapper = currentData.categoryDataWrapper
        def usedCategories = categoryDataWrapper.usedCategories
        def topCategories = categoryDataWrapper.topLevelCategories
        def levelCount = categoryDataWrapper.categoryLevelCount

        def otherTitle = "Другие виды нагрузки"
        def sheetTitle = otherLoad ? otherTitle : getHeaderTitle(yearPart, "Практики и ГИА")
        def headerTitle = otherLoad ? otherTitle : getHeaderTitle(yearPart, "Практики и государственная итоговая аттестация")
        def sheet = workbook.createSheet(sheetTitle, ++sheetIndex)

        if (otherLoad)
        {
            int[] columnViews = [55, 45, 5, 15, 10, 7]
            fillSheetSettings(sheet)
            fillColumnViews(sheet, columnViews)
        }
        else
        {
            int[] columnViews1 = [55, 45, 4]
            int[] columnViews2 = [4, 4, 4, 4, 15, 7]
            int[] columnYearPartView = yearPart != null ? null : [15]

            // параметры
            int[] columnViews = ArrayUtils.addAll(columnViews1, columnYearPartView)
            columnViews = ArrayUtils.addAll(columnViews, columnViews2)

            // категории
            int[] categoryViews = new int[usedCategories.size()]

            Arrays.fill(categoryViews, 7)
            columnViews = ArrayUtils.addAll(columnViews, categoryViews)

            fillSheetSettings(sheet)
            fillColumnViews(sheet, columnViews)
        }

        int colNum = 0
        int rowNum = createHeader(sheet, formats, 0, 0, headerTitle, false)
        sheet.setRowView(++rowNum, 60 * 20)

        WritableCellFormat format = otherLoad ? formats.dfc : formats.dfc_vert
        def columns = EplAssignmentCommonWrapper.TitleColumn.getList(yearPart == null, otherLoad)
        for (def column : columns)
        {
            def formatVertical = column.type.equals(EplAssignmentCommonWrapper.TitleColumnType.TITLE) || column.type.equals(EplAssignmentCommonWrapper.TitleColumnType.EDU_PARAMS) ? formats.dfc : format
            sheet.addCell(new Label(colNum++, rowNum, column.title, formatVertical))
        }
        sheet.addCell(new Label(colNum, rowNum, "Всего часов", format))

        if (!otherLoad)
        {
            if (levelCount > 1)
            {
                for (int i = 0; i <= colNum; i++)
                    sheet.mergeCells(i, rowNum, i, rowNum + 1)
            }
            for (EplCategoryWrapper categoryWrapper : topCategories)
            {
                sheet.addCell(new Label(++colNum, rowNum, categoryWrapper.title, categoryWrapper.children.empty ? formats.dfc_vert : formats.dfc))

                if (levelCount > 1)
                {
                    def children = categoryWrapper.children
                    if (children.empty)
                        sheet.mergeCells(colNum, rowNum, colNum, rowNum + 1)
                    else
                    {
                        int colSubCategoryNum = colNum
                        sheet.mergeCells(colNum, rowNum, colNum += (children.size() - 1), rowNum)
                        for (def subCategoryWrapper : children)
                        {
                            sheet.addCell(new Label(colSubCategoryNum++, rowNum + 1, subCategoryWrapper.title, formats.dfc_vert))
                        }
                    }
                }
            }
            if (levelCount > 1) rowNum++
        }

        rowNum++
        colNum = 0
        def rows = otherLoad ? eduLoadDataWrapper.otherRows : currentData.rows
        for (def row : rows)
        {
            if (row.children.empty)
            {
                createEduLoadRow(sheet, formats, colNum, rowNum++, row, yearPart, categoryDataWrapper, otherLoad)
            }
            else
            {
                createEduLoadRow(sheet, formats, colNum, rowNum++, row, yearPart, categoryDataWrapper, otherLoad)
                for (def child : row.children)
                {
                    createEduLoadRow(sheet,  formats,colNum, rowNum++, child, yearPart, categoryDataWrapper, otherLoad)
                }
            }
        }
    }

    private int createHeader(WritableSheet sheet, CellFormats formats, int colNum, int rowNum, String title, boolean nonEduLoad)
    {
        def postExtendedTitle = pps.titlePostOrTimeWorkerData + (ppsDegreesAndStatuses.empty ? "" : ", " + ppsDegreesAndStatuses)
        def merge = colNum != 0
        if (merge) sheet.mergeCells(0, rowNum, colNum, rowNum)
        sheet.addCell(new Label(0, rowNum, (nonEduLoad ? "Внеучебная" :  "Учебная") + " нагрузка на " + indPlan.eppYear.title + " уч. год", formats.df_i))

        sheet.addCell(new Label(colNum + 1, rowNum, indPlan.orgUnit.title, formats.df))

        if (merge) sheet.mergeCells(0, rowNum, colNum, rowNum)
        sheet.addCell(new Label(0, ++rowNum, pps.fullFio, formats.df_b))

        if (merge) sheet.mergeCells(0, rowNum, colNum, rowNum)
        sheet.addCell(new Label(0, ++rowNum, postExtendedTitle, formats.df_i))

        if (merge) sheet.mergeCells(0, rowNum, colNum, rowNum)
        sheet.addCell(new Label(0, rowNum += 2, title, formats.df_b))

        return rowNum
    }

    private void createEduLoadRow(WritableSheet sheet, CellFormats formats, int colNum, int rowNum, EplAssignmentRowWrapper row, YearDistributionPart yearPart, EplBaseCategoryDataWrapper categoryDataWrapper, boolean otherLoad)
    {
        int startColNum = colNum
        WritableCellFormat format = row.total ? formats.dfr_b : formats.dfr

        sheet.addCell(new Label(colNum++, rowNum, row.title, row.total ? formats.dfl_b : row.child ? formats.dfl_tb : formats.dfl))
        sheet.addCell(new Label(colNum++, rowNum, row.eouTitles, formats.dfl))
        sheet.addCell(new Label(colNum++, rowNum, row.courses, formats.dfl))

        if (yearPart == null)
            sheet.addCell(new Label(colNum++, rowNum, row.groupYearPartTitles, formats.dfl))

        if (otherLoad)
        {
            createNumberCell(sheet, colNum++, rowNum, row.studentCount, formats.dfr)

            if (row.total)
            {
                sheet.mergeCells(startColNum, rowNum, colNum - 1, rowNum)
                sheetToTotalValueMap.put(sheetIndex, PairKey.create(colNum, rowNum))
            }
            sheet.addCell(new Number(colNum, rowNum, row.timeByCategorySum, format))
        }
        else
        {
            createNumberCell(sheet, colNum++, rowNum, row.flowCount, formats.dfr)
            createNumberCell(sheet, colNum++, rowNum, row.groupCount, formats.dfr)
            createNumberCell(sheet, colNum++, rowNum, row.subGroupCount, formats.dfr)
            createNumberCell(sheet, colNum++, rowNum, row.studentCount, formats.dfr)
            sheet.addCell(new Label(colNum++, rowNum, row.eplGroups, formats.dfl))

            if (row.total)
            {
                sheet.mergeCells(startColNum, rowNum, colNum - 1, rowNum)
                sheetToTotalValueMap.put(sheetIndex, PairKey.create(colNum, rowNum))
            }
            sheet.addCell(new Number(colNum++, rowNum, row.timeByCategorySum, format))

            for (EplCategoryWrapper categoryWrapper : categoryDataWrapper.bottomLevelCategories)
            {
                Double value = row.timeByCategoryMap.get(categoryWrapper.category)
                sheet.addCell(new Number(colNum++, rowNum, value, format))
            }
        }
    }

    private void createNonEduLoadRow(WritableSheet sheet, CellFormats formats, int colNum, int rowNum, EplNonEduLoadRowWrapper row)
    {
        if (row.total)
        {
            sheet.mergeCells(0, rowNum, ++colNum, rowNum)
            sheet.addCell(new Label(0, rowNum, row.title, formats.dfl_b))
            colNum++
        }
        else
        {
            createNumberCell(sheet, colNum++, rowNum, row.number, formats.dfr)
            sheet.addCell(new Label(colNum++, rowNum, row.title, row.category ? formats.dfl_b : formats.dfl))
        }

        sheet.addCell(new Label(colNum++, rowNum, row.paramName, formats.dfl))
        createNumberCell(sheet, colNum++, rowNum, row.paramValue, formats.dfr)

        sheetToTotalValueMap.put(sheetIndex, PairKey.create(colNum, rowNum))
        sheet.addCell(new Number(colNum++, rowNum, row.timeAmount, row.category || row.total ? formats.dfr_b : formats.dfr))

        sheet.addCell(new Label(colNum++, rowNum, row.dateFromStr, formats.dfl))
        sheet.addCell(new Label(colNum++, rowNum, row.dateToStr, formats.dfl))
        sheet.addCell(new Label(colNum, rowNum, row.yearPart, formats.dfl))
    }

    private static void createNumberCell(WritableSheet sheet, int colNum, int rowNum, Double value, WritableCellFormat format)
    {
        if (null == value)
            sheet.addCell(new Label(colNum, rowNum, "", format))
        else
            sheet.addCell(new Number(colNum, rowNum, value, format))
    }

    private void fillTimeItemForTitle(WritableWorkbook workbook, CellFormats formats)
    {
        int colNum = startFillTotalValues.first
        int rowNum = startFillTotalValues.second
        for (int sheetNum = 0; sheetNum <= sheetIndex; sheetNum++)
        {
            if (sheetNum == 0) continue
            def sheetName = workbook.getSheetNames()[sheetNum]

            StringBuffer buf = new StringBuffer("\'").append(sheetName).append("\'").append("!")
            PairKey<Integer, Integer> coordinates = sheetToTotalValueMap.get(sheetNum)
            getCellReference(coordinates.first, coordinates.second, buf)

            sheetTitle.addCell(new Formula(colNum, rowNum++, buf.toString(), formats.dfr))
        }
    }

    private static void fillSheetSettings(WritableSheet sheet)
    {
        def s = sheet.settings
        s.setOrientation(PageOrientation.LANDSCAPE)

        double margin = 1 / 2.54; // поле в дюймах
        s.setLeftMargin(margin * 0.64)
        s.setRightMargin(margin * 0.64)
        s.setTopMargin(margin * 1.91)
        s.setBottomMargin(margin * 1.91)
        s.setHeaderMargin(margin * 0.76)
        s.setFooterMargin(margin * 0.76)
    }

    private static void fillColumnViews(WritableSheet sheet, int[] columnViews)
    {
        for (int i = 0; i < columnViews.size(); i++)
            sheet.setColumnView(i, columnViews[i])
    }

    private static String getFirstCharUpperCase(String value)
    {
        return value.substring(0, 1).toUpperCase() + value.substring(1)
    }

    private static String getHeaderTitle(YearDistributionPart yearPart, String defaultTitle)
    {
        return yearPart != null ? getFirstCharUpperCase(yearPart.title) : defaultTitle
    }

    private static class CellFormats
    {
        final WritableCellFormat hf1
        final WritableCellFormat hf2
        final WritableCellFormat df
        final WritableCellFormat df_i
        final WritableCellFormat df_c
        final WritableCellFormat df_c6
        final WritableCellFormat dfb_l
        final WritableCellFormat df_l
        final WritableCellFormat df_b
        final WritableCellFormat dfc
        final WritableCellFormat dfc_vert
        final WritableCellFormat dfc_b
        final WritableCellFormat dfr
        final WritableCellFormat dfl
        final WritableCellFormat dfl_tb
        final WritableCellFormat dfl_b
        final WritableCellFormat dfl_b2
        final WritableCellFormat dfl_b3
        final WritableCellFormat dfr_b
        final WritableCellFormat line

        private CellFormats() throws Throwable
        {
            WritableFont headerFont12 = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD)
            WritableFont headerFont11 = new WritableFont(WritableFont.ARIAL, 11)
            WritableFont fontBold10 = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD)
            WritableFont font10 = new WritableFont(WritableFont.ARIAL, 10)
            WritableFont fontBold9 = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD)
            WritableFont font9 = new WritableFont(WritableFont.ARIAL, 9)
            WritableFont font6 = new WritableFont(WritableFont.ARIAL, 6)

            hf1 = new WritableCellFormat(headerFont11)
            hf1.setVerticalAlignment(VerticalAlignment.CENTRE)
            hf1.setAlignment(Alignment.CENTRE)
            hf1.setWrap(true)

            hf2 = new WritableCellFormat(headerFont12)
            hf2.setAlignment(Alignment.CENTRE)

            df_c6 = new WritableCellFormat(font6)
            df_c6.setAlignment(Alignment.CENTRE)

            df_c = new WritableCellFormat(font10)
            df_c.setAlignment(Alignment.CENTRE)

            line = new WritableCellFormat(font10)
            line.setAlignment(Alignment.CENTRE)
            line.setBorder(Border.BOTTOM, BorderLineStyle.THIN)

            dfb_l = new WritableCellFormat(fontBold10)
            dfb_l.setAlignment(Alignment.LEFT)

            df_l = new WritableCellFormat(font10)
            df_l.setAlignment(Alignment.LEFT)

            dfc_b = new WritableCellFormat(fontBold9)
            dfc_b.setAlignment(Alignment.CENTRE)
            dfc_b.setBorder(Border.ALL, BorderLineStyle.THIN)

            //

            dfc = new WritableCellFormat(font9)
            dfc.setAlignment(Alignment.CENTRE)
            dfc.setVerticalAlignment(VerticalAlignment.CENTRE)
            dfc.setBorder(Border.ALL, BorderLineStyle.THIN)
            dfc.setWrap(true)

            dfl = new WritableCellFormat(font9)
            dfl.setAlignment(Alignment.LEFT)
            dfl.setVerticalAlignment(VerticalAlignment.TOP)
            dfl.setBorder(Border.ALL, BorderLineStyle.THIN)
            dfl.setWrap(true)

            dfl_b2 = new WritableCellFormat(fontBold9)
            dfl_b2.setAlignment(Alignment.LEFT)
            dfl_b2.setBorder(Border.TOP, BorderLineStyle.THIN)
            dfl_b2.setBorder(Border.BOTTOM, BorderLineStyle.THIN)
            dfl_b2.setBorder(Border.LEFT, BorderLineStyle.THIN)

            dfl_b3 = new WritableCellFormat(fontBold9)
            dfl_b3.setAlignment(Alignment.LEFT)
            dfl_b3.setBorder(Border.TOP, BorderLineStyle.THIN)
            dfl_b3.setBorder(Border.BOTTOM, BorderLineStyle.THIN)

            df_i = new WritableCellFormat(font9)
            df_i.setAlignment(Alignment.LEFT)
            df_i.setVerticalAlignment(VerticalAlignment.TOP)
            df_i.setWrap(true)
            df_i.setIndentation(1)

            df = new WritableCellFormat(font9)
            df.setAlignment(Alignment.LEFT)
            df.setVerticalAlignment(VerticalAlignment.TOP)

            df_b = new WritableCellFormat(fontBold9)
            df_b.setAlignment(Alignment.LEFT)
            df_b.setIndentation(1)

            dfc_vert = new WritableCellFormat(font9)
            dfc_vert.setAlignment(Alignment.CENTRE)
            dfc_vert.setVerticalAlignment(VerticalAlignment.CENTRE)
            dfc_vert.setBorder(Border.ALL, BorderLineStyle.THIN)
            dfc_vert.setOrientation(Orientation.PLUS_90)
            dfc_vert.setWrap(true)

            dfl_tb = new WritableCellFormat(font9)
            dfl_tb.setAlignment(Alignment.LEFT)
            dfl_tb.setVerticalAlignment(VerticalAlignment.TOP)
            dfl_tb.setBorder(Border.ALL, BorderLineStyle.THIN)
            dfl_tb.setWrap(true)
            dfl_tb.setIndentation(1)

            dfr = new WritableCellFormat(font9)
            dfr.setAlignment(Alignment.RIGHT)
            dfr.setVerticalAlignment(VerticalAlignment.TOP)
            dfr.setBorder(Border.ALL, BorderLineStyle.THIN)
            dfr.setWrap(true)

            dfr_b = new WritableCellFormat(fontBold9)
            dfr_b.setAlignment(Alignment.RIGHT)
            dfr_b.setBorder(Border.ALL, BorderLineStyle.THIN)

            dfl_b = new WritableCellFormat(fontBold9)
            dfl_b.setAlignment(Alignment.LEFT)
            dfl_b.setBorder(Border.ALL, BorderLineStyle.THIN)
        }
    }
}
