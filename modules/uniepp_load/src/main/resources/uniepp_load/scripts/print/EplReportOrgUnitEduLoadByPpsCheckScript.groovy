/* $Id$ */
package uniepp_load.scripts.print

import com.google.common.collect.Lists
import com.google.common.collect.Maps
import org.hibernate.Session
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.utils.CommonCollator
import ru.tandemservice.uni.entity.employee.pps.PpsEntry
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary
import ru.tandemservice.uniepp_load.catalog.entity.EplPrintScript
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplPlannedPpsTypeCodes
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplPrintScriptCodes
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps
import ru.tandemservice.uniepp_load.loadPlan.util.PpsTimeStatsUtil

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new EplReportOrgUnitEduLoadByPpsCheckPrint(           // входные параметры скрипта
        session: session,                                    // сессия
        summaryId: object                                    // id подразделения
).print()

/**
 * Проверка распределения учебной нагрузки по ППС читающего подразделения
 */
class EplReportOrgUnitEduLoadByPpsCheckPrint
{
    static final Integer COLUMN_P_STAFF_RATE = 3
    static final Integer COLUMN_P_STAFF_RATE_SUM = 4
    static final Integer COLUMN_P_TOTAL_HOURS = 5
    static final Integer COLUMN_P_EDU_LOAD = 6
    static final Integer COLUMN_P_ADDITION_LOAD = 7

    static final Integer COLUMN_V_STAFF_RATE = 3
    static final Integer COLUMN_V_TOTAL_HOURS = 4
    static final Integer COLUMN_V_EDU_LOAD = 5
    static final Integer COLUMN_V_ADDITION_LOAD = 6

    static final Integer COLUMN_T_EDU_LOAD = 2

    static final List<Integer> p_columns = Arrays.asList(COLUMN_P_STAFF_RATE, COLUMN_P_STAFF_RATE_SUM, COLUMN_P_TOTAL_HOURS, COLUMN_P_EDU_LOAD, COLUMN_P_ADDITION_LOAD)
    static final List<Integer> v_columns = Arrays.asList(COLUMN_V_STAFF_RATE, COLUMN_V_TOTAL_HOURS, COLUMN_V_EDU_LOAD, COLUMN_V_ADDITION_LOAD)
    static final List<Integer> t_columns = Arrays.asList(COLUMN_T_EDU_LOAD)

    // входные параметры
    Session session
    Long summaryId
    EplOrgUnitSummary summary

    Map<Integer, List<Integer>> redPRowsMap = Maps.newHashMap()
    Map<Integer, List<Integer>> redVRowsMap = Maps.newHashMap()
    Map<Integer, List<Integer>> redTRowsMap = Maps.newHashMap()
    Double rate = 0.0
    Double sumHours = 0.0

    /**
     * Основной метод печати.
     */
    def print()
    {
        byte[] template = DataAccessServices.dao().getByCode(EplPrintScript.class, EplPrintScriptCodes.ORG_UNIT_LOAD_BY_PPS_CHECK).getCurrentTemplate();
        RtfDocument document = RtfDocument.fromByteArray(template);

        printData(document);

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document), fileName: "Проверка распределения учебной нагрузки по ППС читающего подразделения " + new Date().format("dd.MM.yyyy") + ".rtf"]
    }

    def printData(RtfDocument document)
    {
        summary = DataAccessServices.dao().get(EplOrgUnitSummary.class, summaryId)
        List<EplPlannedPps> ppsList = DataAccessServices.dao().getList(EplPlannedPps.class, EplPlannedPps.orgUnitSummary().id(), summaryId)
        List<EplPlannedPps> ppsPersonList = Lists.newArrayList()
        List<EplPlannedPps> ppsVacancyList = Lists.newArrayList()
        List<EplPlannedPps> ppsTimeWorkerList = Lists.newArrayList()

        for (EplPlannedPps pps : ppsList)
        {
            if (pps.type.code == EplPlannedPpsTypeCodes.STAFF)
                ppsPersonList.add(pps)
            else if (pps.type.code == EplPlannedPpsTypeCodes.TIMEWORKER)
                ppsTimeWorkerList.add(pps)
            else
                ppsVacancyList.add(pps)
        }
        ppsPersonList.sort({ pps1, pps2 -> CommonCollator.RUSSIAN_COLLATOR.compare(pps1.title, pps2.title) })
        ppsVacancyList.sort({ pps1, pps2 -> CommonCollator.RUSSIAN_COLLATOR.compare(pps1.title, pps2.title) })
        ppsTimeWorkerList.sort({ pps1, pps2 -> CommonCollator.RUSSIAN_COLLATOR.compare(pps1.title, pps2.title) })

        List<String[]> p = getRecordList(ppsPersonList, false)
        List<String[]> v = getRecordList(ppsVacancyList, true)
        List<String[]> t = getRecordList(ppsTimeWorkerList, null)

        def redColor = document.header.colorTable.addColor(255, 0, 0)
        def blackColor = document.header.colorTable.addColor(0, 0, 0)

        RtfTableModifier tm = new RtfTableModifier();
        tm.put("P", p as String[][])
        tm.put("P", ppsIntercepter(redPRowsMap, redColor, blackColor, false))
        tm.put("V", v as String[][])
        tm.put("V", ppsIntercepter(redVRowsMap, redColor, blackColor, false))
        tm.put("T", t as String[][])
        tm.put("T", ppsIntercepter(redTRowsMap, redColor, blackColor, true))
        tm.modify(document)

        new RtfInjectModifier()
                .put("orgUnitTitle", summary.orgUnit.orgUnitType.title + " «" + summary.orgUnit.title + "»")
                .put("eduYear", summary.studentSummary.eppYear.educationYear.title)
                .put("rate", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(rate))
                .put("sumHours", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(sumHours))
                .modify(document)
    }

    private List<String[]> getRecordList(List<EplPlannedPps> ppsList, Boolean isVacancy)
    {
        int number = 0;
        List<String[]> recordList = Lists.newArrayList()
        Map<EplPlannedPps, PpsTimeStatsUtil.ResultRow> ppsTimeStatsRows = PpsTimeStatsUtil.getPpsTimeStatsRows(summary.id)
        if (null == isVacancy)
        {
            for (EplPlannedPps pps : ppsList)
            {
                PpsTimeStatsUtil.ResultRow resultRow = ppsTimeStatsRows.get(pps)

                def ppsSummary = ppsTimeStatsRows.containsKey(pps) ? resultRow.total : 0.0
                def ppsSummaryMessage = ppsTimeStatsRows.containsKey(pps) ? resultRow.totalMessage : "0.0"
                sumHours += ppsSummary

                if (resultRow?.errorInTotal)
                    SafeMap.safeGet(redTRowsMap, t_columns.get(0), ArrayList.class).add(number)

                recordList.add([++number, pps.title, ppsSummaryMessage] as String[])
            }
            return recordList
        }

        Map<Integer, List<Integer>> redRowsMap = isVacancy ? redVRowsMap : redPRowsMap
        List<Integer> columns = isVacancy ? v_columns : p_columns
        List<Long> ppsEntryIds = Lists.newArrayList()
        Map<PpsEntry, Double> staffRateSumMap = Maps.newHashMap()

        for (EplPlannedPps pps : ppsList)
        {
            if (pps.isTimeWorker() || null == pps.ppsEntry) continue
            ppsEntryIds.add(pps.ppsEntry.id)
        }

        List<EplPlannedPps> orgUnitPlannedPpsList = new DQLSelectBuilder().fromEntity(EplPlannedPps.class, "pps")
                .where(eq(property("pps", EplPlannedPps.orgUnitSummary().studentSummary().id()), value(summary.studentSummary.id)))
                .where(DQLExpressions.in(property("pps", EplPlannedPps.ppsEntry().id()), ppsEntryIds))
                .createStatement(session).list()

        for (EplPlannedPps pps : orgUnitPlannedPpsList)
        {
            if (pps.staffRateAsDouble == null) continue
            if (!staffRateSumMap.containsKey(pps.ppsEntry))
                staffRateSumMap.put(pps.ppsEntry, 0.0)
            staffRateSumMap[pps.ppsEntry] += pps.staffRateAsDouble;
        }

        for (EplPlannedPps pps : ppsList)
        {
            int column_idx = 0
            def ppsEntry = pps.ppsEntry

            def title = pps.title
            def post = pps.post?.title
            def staffRate = pps.staffRateAsDouble
            String staffRateAsString = ""
            String staffRateSumAsString = ""

            if (null != staffRate)
            {
                rate += staffRate
                staffRateAsString = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRate)
            }

            if (staffRate > 1.5)
                SafeMap.safeGet(redRowsMap, columns.get(column_idx), ArrayList.class).add(number)

            if (!isVacancy)
            {
                column_idx++
                Double staffRateSum = staffRateSumMap.get(ppsEntry)
                if (null != staffRateSum)
                {
                    staffRateSumAsString = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRateSum)
                    if (staffRateSum > 1.5)
                        SafeMap.safeGet(redRowsMap, columns.get(column_idx), ArrayList.class).add(number)
                }
            }

            PpsTimeStatsUtil.ResultRow resultRow = ppsTimeStatsRows.get(pps)

            if (null == resultRow)
            {
                if (isVacancy)
                    recordList.add([++number, title, post, staffRateAsString, "", "", ""] as String[])
                else
                    recordList.add([++number, title, post, staffRateAsString, staffRateSumAsString, "", "", ""] as String[])
            }
            else
            {
                column_idx++
                if (resultRow.errorInTotal)
                    SafeMap.safeGet(redRowsMap, columns.get(column_idx), ArrayList.class).add(number)
                column_idx++
                if (resultRow.errorInRate)
                    SafeMap.safeGet(redRowsMap, columns.get(column_idx), ArrayList.class).add(number)
                column_idx++
                if (resultRow.errorInOverrate)
                    SafeMap.safeGet(redRowsMap, columns.get(column_idx), ArrayList.class).add(number)

                if (isVacancy)
                    recordList.add([++number, title, post, staffRateAsString, resultRow.totalMessage, resultRow.rateMessage, resultRow.overrateMessage] as String[])
                else
                    recordList.add([++number, title, post, staffRateAsString, staffRateSumAsString, resultRow.totalMessage, resultRow.rateMessage, resultRow.overrateMessage] as String[])
            }
        }
        return recordList
    }

    private static RtfRowIntercepterBase ppsIntercepter(Map<Integer, List<Integer>> redRowsMap, int colorIndexNew, int colorIndexOld, boolean timeWorker)
    {
        return new RtfRowIntercepterBase() {
            @Override
            List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                def columns = timeWorker ? t_columns : p_columns
                if (columns.contains(colIndex) && redRowsMap.containsKey(colIndex) && redRowsMap.get(colIndex).contains(rowIndex))
                    return new RtfString().color(colorIndexNew).append(value).color(colorIndexOld).toList();
                return super.beforeInject(table, row, cell, rowIndex, colIndex, value)
            }
        }
    }
}