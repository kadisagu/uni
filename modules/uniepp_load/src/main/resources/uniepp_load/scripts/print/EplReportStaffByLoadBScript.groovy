/**
 *$Id:$
 */
package uniepp_load.scripts.print

import jxl.Workbook
import jxl.WorkbookSettings
import jxl.format.Alignment
import jxl.format.Border
import jxl.format.BorderLineStyle
import jxl.format.VerticalAlignment
import jxl.write.*
import org.hibernate.Session
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.shared.commonbase.base.util.JExcelUtil
import org.tandemframework.shared.organization.base.entity.OrgUnit
import ru.tandemservice.uni.UniDefines
import ru.tandemservice.uni.dao.UniDaoFacade
import ru.tandemservice.uni.entity.catalog.DevelopForm
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplSimpleTimeItem
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplCompensationSourceCodes
import ru.tandemservice.uniepp_load.report.bo.EplReport.util.EplTimeByCompSourceSplitter
import ru.tandemservice.uniepp_load.report.bo.EplReport.util.EplTimeByDevFormSplitter
import ru.tandemservice.uniepp_load.report.bo.EplReport.util.EplTimeSplitter

import static jxl.biff.CellReferenceHelper.getCellReference

return new EplReportStaffByLoadBPrint(                           // входные параметры скрипта
        session: session,                                    // сессия
        summaryId: object                                    // id подразделения
).print()

/**
 * Расчет штата на основании учебной нагрузки
 */
class EplReportStaffByLoadBPrint
{
    // входные параметры
    Session session
    Long summaryId

    // данные для печати, заполняются в методе prepareData
    ByteArrayOutputStream out = new ByteArrayOutputStream();

    EplTimeSplitter sc;
    EplTimeSplitter sf;

    CellFormats formats = new CellFormats();


    /**
     * Основной метод печати.
     */
    def print()
    {
        // создаем печатную форму
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        def workbook = Workbook.createWorkbook(out, ws);

        printData(workbook);

        workbook.write();
        workbook.close();

        // стандартные выходные параметры скрипта
        return [document: out.toByteArray(), fileName: "Расчет штата на основании учебной нагрузки " + new Date().format("dd.MM.yyyy") + ".xls"]
    }

    def printData(WritableWorkbook workbook)
    {
        Map<OrgUnit, List<EplOuSummaryWrapper>> summaryMap = prepareTimeData();

        def sheet = workbook.createSheet("Штат", 0);

        EplStudentSummary summary = (EplStudentSummary) DataAccessServices.dao().get(EplOrgUnitSummary.class, summaryId);

        sheet.addCell(new Label(0, 0, "Распределение штата между кафедрами на " + summary.eppYear.educationYear.title + " учебный год", formats.hl));
        sheet.mergeCells(0, 0, 28, 0);

        if (summaryMap.isEmpty()) {
            sheet.addCell(new Label(0, 2, "Нет данных для отображения."));
            sheet.mergeCells(0, 2, 10, 0)
        }

        printTableHeader(sheet);

        List<OrgUnit> formativeOrgUnitList = new ArrayList<>(summaryMap.keySet());
        formativeOrgUnitList.sort { x, y -> x.top <=> y.top ?: x.printTitle <=> y.printTitle }


        int rateHoursC = 14;
        int hourlyFundC = 28;

        int rowNum = 4;
        List<Integer> formativeSumRowNumbers = new ArrayList<>();
        for (OrgUnit formative: formativeOrgUnitList) {

            List<EplOuSummaryWrapper> orgUnits = summaryMap.get(formative);
            orgUnits.sort({it.summary.orgUnit.printTitle})
            int number = 1;

            for (EplOuSummaryWrapper w : orgUnits) {
                int column = 0;

                sheet.addCell(new Label(column++, rowNum, number as String, formats.dl)); number++;
                sheet.addCell(new Label(column++, rowNum, w.summary.orgUnit.printTitle, formats.dl));

                sheet.addCell(new Number(column++, rowNum, w.ob, formats.dnum));
                sheet.addCell(new Number(column++, rowNum, w.oc, formats.dnum));
                addTwoSum(sheet, column++, rowNum);
                sheet.addCell(new Number(column++, rowNum, w.ozb, formats.dnum));
                sheet.addCell(new Number(column++, rowNum, w.ozc, formats.dnum));
                addTwoSum(sheet, column++, rowNum);
                sheet.addCell(new Number(column++, rowNum, w.zb, formats.dnum));
                sheet.addCell(new Number(column++, rowNum, w.zc, formats.dnum));
                addTwoSum(sheet, column++, rowNum);

                addThreeSum(sheet, column++, rowNum);
                addThreeSum(sheet, column++, rowNum);
                addTwoSum(sheet, column++, rowNum);

                sheet.addCell(new Number(column++, rowNum, w.summary.rateHours, formats.dnum));

                sheet.addCell(new Formula(column, rowNum, "(${getCellReference(column - 13, rowNum)}-${getCellReference(hourlyFundC, rowNum)})/${getCellReference(rateHoursC, rowNum)}", formats.dnum)); column++;
                addStaffCalc(sheet, column++, rowNum, rateHoursC);
                addTwoSum(sheet, column++, rowNum);
                addStaffCalcFix(sheet, column++, rowNum, 800);
                addStaffCalcFix(sheet, column++, rowNum, 800);
                addTwoSum(sheet, column++, rowNum);
                addStaffCalcFix(sheet, column++, rowNum, 900);
                addStaffCalcFix(sheet, column++, rowNum, 900);
                addTwoSum(sheet, column++, rowNum);

                addThreeSum(sheet, column++, rowNum);
                addThreeSum(sheet, column++, rowNum);
                addTwoSum(sheet, column++, rowNum);

                sheet.addCell(new Formula(column, rowNum, "(${getCellReference(rateHoursC-1, rowNum)}-${getCellReference(hourlyFundC, rowNum)})/${getCellReference(column-1, rowNum)}", formats.dnum)); column++;

                sheet.addCell(new Number(column, rowNum, w.summary.hourlyFund, formats.dnum));

                rowNum++;
            }

            sheet.addCell(new Label(0, rowNum, "", formats.hl));
            sheet.addCell(new Label(1, rowNum, formative.top ? "Общевузовские кафедры" : formative.printTitle, formats.hl));

            for (int column = 2; column < rateHoursC; column++) {
                sheet.addCell(new Formula(column, rowNum, "SUM(" + JExcelUtil.getRange(column, rowNum-number+1, column, rowNum-1) + ")", formats.hnum));
            }

            sheet.addCell(new Label(rateHoursC, rowNum, "", formats.hl));

            for (int column = rateHoursC+1; column < hourlyFundC-1; column++) {
                sheet.addCell(new Formula(column, rowNum, "SUM(" + JExcelUtil.getRange(column, rowNum-number+1, column, rowNum-1) + ")", formats.hnum));
            }

            sheet.addCell(new Label(hourlyFundC - 1, rowNum, "", formats.hl));
            sheet.addCell(new Formula(hourlyFundC, rowNum, "SUM(" + JExcelUtil.getRange(hourlyFundC, rowNum-number+1, hourlyFundC, rowNum-1) + ")", formats.hnum));

            formativeSumRowNumbers.add(rowNum);
            rowNum++;
        }

        sheet.addCell(new Label(0, rowNum, "", formats.hl));
        sheet.addCell(new Label(1, rowNum, "Итого", formats.hl));

        for (int column = 2; column < rateHoursC; column++) {
            sheet.addCell(new Formula(column, rowNum, sumFormula(column, formativeSumRowNumbers), formats.hnum));
        }

        sheet.addCell(new Label(rateHoursC, rowNum, "", formats.hl));

        for (int column = rateHoursC+1; column < hourlyFundC-1; column++) {
            sheet.addCell(new Formula(column, rowNum, sumFormula(column, formativeSumRowNumbers), formats.hnum));
        }

        sheet.addCell(new Label(hourlyFundC - 1, rowNum, "", formats.hl));
        sheet.addCell(new Formula(hourlyFundC, rowNum, sumFormula(hourlyFundC, formativeSumRowNumbers), formats.hnum));
    }

    private static String sumFormula(int column, ArrayList<Integer> rows)
    {
        StringBuffer b = new StringBuffer();
        int max = rows.last();
        for (int row : rows) {
            getCellReference(column, row, b);
            if (row < max) b.append("+");
        }
        return b.toString();
    }

    private void addStaffCalc(WritableSheet sheet, int column, int rowNum, int rateHoursC)
    {
        sheet.addCell(new Formula(column, rowNum, "${getCellReference(column - 13, rowNum)}/${getCellReference(rateHoursC, rowNum)}", formats.dnum))
    }

    private void addStaffCalcFix(WritableSheet sheet, int column, int rowNum, int amount)
    {
        sheet.addCell(new Formula(column, rowNum, "${getCellReference(column - 13, rowNum)}/$amount", formats.dnum))
    }

    private void addThreeSum(WritableSheet sheet, int column, int rowNum)
    {
        sheet.addCell(new Formula(column, rowNum, "${getCellReference(column - 9, rowNum)}+${getCellReference(column - 6, rowNum)}+${getCellReference(column - 3, rowNum)}", formats.dnum))
    }

    private void addTwoSum(WritableSheet sheet, int column, int rowNum)
    {
        sheet.addCell(new Formula(column, rowNum, "${getCellReference(column - 2, rowNum)}+${getCellReference(column - 1, rowNum)}", formats.dnum))
    }

    private void printTableHeader(WritableSheet sheet)
    {
        DevelopForm o = DataAccessServices.dao().get(DevelopForm.class, DevelopForm.code(), DevelopFormCodes.FULL_TIME_FORM);
        DevelopForm oz = DataAccessServices.dao().get(DevelopForm.class, DevelopForm.code(), DevelopFormCodes.PART_TIME_FORM);
        DevelopForm z = DataAccessServices.dao().get(DevelopForm.class, DevelopForm.code(), DevelopFormCodes.CORESP_FORM);

        int rowNum = 1;

        sheet.addCell(new Label(0, rowNum, "№", formats.hc));
        sheet.setColumnView(0, 10);
        sheet.mergeCells(0, rowNum, 0, rowNum + 2);

        sheet.addCell(new Label(1, rowNum, "Подразделение", formats.hc));
        sheet.setColumnView(1, 50);
        sheet.mergeCells(1, rowNum, 1, rowNum + 2);

        sheet.addCell(new Label(2, rowNum, "Расчетная учебная нагрузка", formats.hc));
        sheet.setColumnView(2, 120);
        sheet.mergeCells(2, rowNum, 13, rowNum);

        sheet.addCell(new Label(14, rowNum, "Норма \nтруд. \nна \nпреп.", formats.hc));
        sheet.setColumnView(14, 10);
        sheet.mergeCells(14, rowNum, 14, rowNum+2);

        sheet.addCell(new Label(15, rowNum, "Треб. по расчету штат", formats.hc));
        sheet.setColumnView(15, 120);
        sheet.mergeCells(15, rowNum, 26, rowNum);

        sheet.addCell(new Label(27, rowNum, "Расч. учеб. \nнагр. на \nпреп.", formats.hc));
        sheet.setColumnView(27, 20);
        sheet.mergeCells(27, rowNum, 27, rowNum+2);

        sheet.addCell(new Label(28, rowNum, "Почасовой \nфонд", formats.hc));
        sheet.setColumnView(28, 20);
        sheet.mergeCells(28, rowNum, 28, rowNum+2);

        rowNum++;

        for (int c : [2])
        {
            sheet.addCell(new Label(c, rowNum, o.title, formats.hc));
            sheet.mergeCells(c, rowNum, c+2, rowNum);

            sheet.addCell(new Label(c+3, rowNum, oz.title, formats.hc));
            sheet.mergeCells(c+3, rowNum, c+5, rowNum);

            sheet.addCell(new Label(c+6, rowNum, z.title, formats.hc));
            sheet.mergeCells(c+6, rowNum, c+8, rowNum);

            sheet.addCell(new Label(c+9, rowNum, "Итого", formats.hc));
            sheet.mergeCells(c+9, rowNum, c+11, rowNum);
        }

        for (int c : [15])
        {
            sheet.addCell(new Label(c, rowNum, o.title, formats.hc));
            sheet.mergeCells(c, rowNum, c+2, rowNum);

            sheet.addCell(new Label(c+3, rowNum, "$oz.title (норм. 800)", formats.hc));
            sheet.mergeCells(c+3, rowNum, c+5, rowNum);

            sheet.addCell(new Label(c+6, rowNum, "$z.title (норм. 900)", formats.hc));
            sheet.mergeCells(c+6, rowNum, c+8, rowNum);

            sheet.addCell(new Label(c+9, rowNum, "Итого", formats.hc));
            sheet.mergeCells(c+9, rowNum, c+11, rowNum);
        }

        rowNum++;

        for (int c : [2, 5, 8, 11, 15, 18, 21, 24])
        {
            sheet.addCell(new Label(c, rowNum, "ГБ", formats.hc));
            sheet.addCell(new Label(c+1, rowNum, "ВБ", formats.hc));
            sheet.addCell(new Label(c+2, rowNum, "Всего", formats.hc));
            sheet.setColumnView(c, 10);
            sheet.setColumnView(c+1, 10);
            sheet.setColumnView(c+2, 10);
        }
    }

    private Map<OrgUnit, List<EplOuSummaryWrapper>> prepareTimeData()
    {
        Map<OrgUnit, List<EplOuSummaryWrapper>> summaryMap = new HashMap<>();
        Map<EplOrgUnitSummary, EplOuSummaryWrapper> wrapperMap = new HashMap<>();
        for (EplOrgUnitSummary summary : DataAccessServices.dao().getList(EplOrgUnitSummary.class, EplOrgUnitSummary.studentSummary().id(), summaryId)) {
            EplOuSummaryWrapper wrapper = new EplOuSummaryWrapper(summary)
            SafeMap.safeGet(summaryMap, getFormativeOrgUnit(summary.getOrgUnit()), ArrayList.class).add(wrapper);
            wrapperMap.put(summary, wrapper);
        }

        sc = new EplTimeByCompSourceSplitter(summaryId);
        sf = new EplTimeByDevFormSplitter(summaryId);

        for (EplEduGroupTimeItem item: DataAccessServices.dao().getList(EplEduGroupTimeItem.class, EplEduGroupTimeItem.summary().studentSummary().id(), summaryId)) {
            wrapperMap.get(item.getSummary()).account(item);
        }

        for (EplSimpleTimeItem item: DataAccessServices.dao().getList(EplSimpleTimeItem.class, EplSimpleTimeItem.summary().studentSummary().id(), summaryId)) {
            wrapperMap.get(item.getSummary()).account(item);
        }


        return summaryMap;
    }

    private static OrgUnit getFormativeOrgUnit(OrgUnit orgUnit) {
        if (orgUnit.getParent() == null || UniDaoFacade.getOrgstructDao().isOrgUnitHasKind(orgUnit, UniDefines.CATALOG_ORGUNIT_KIND_FORMING)) {
            return orgUnit;
        }
        return getFormativeOrgUnit(orgUnit.getParent());
    }

    private class EplOuSummaryWrapper
    {
        EplOrgUnitSummary summary;

        EplOuSummaryWrapper(EplOrgUnitSummary summary)
        {
            this.summary = summary
        }

        double ob;
        double oc;
        double ozb;
        double ozc;
        double zb;
        double zc;

        void account(EplEduGroupTimeItem item) {
            double o = sf.c(item, DevelopFormCodes.FULL_TIME_FORM)
            double oz = sf.c(item, DevelopFormCodes.PART_TIME_FORM)
            double z = sf.c(item, DevelopFormCodes.CORESP_FORM)
            double b = item.timeAmount * sc.c(item, EplCompensationSourceCodes.COMPENSATION_TYPE_BUDGET)
            double c = item.timeAmount * sc.c(item, EplCompensationSourceCodes.COMPENSATION_TYPE_CONTRACT)
            ob += o * b;
            oc += o * c;
            ozb += oz * b;
            ozc += oz * c;
            zb += z * b;
            zc += z * c;
        }

        void account(EplSimpleTimeItem item) {
            if (item.countAsContract) {
                oc += item.getTimeAmount();
            } else {
                ob += item.getTimeAmount();
            }
        }
    }

    private static class CellFormats
    {
        final WritableCellFormat dl;
        final WritableCellFormat dnum;
        final WritableCellFormat hl;
        final WritableCellFormat hc;
        final WritableCellFormat hnum;

        private CellFormats() throws Throwable
        {
            dl = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9));
            dl.setBorder(Border.ALL, BorderLineStyle.THIN);
            dl.setAlignment(Alignment.LEFT);
            dl.setVerticalAlignment(VerticalAlignment.CENTRE);

            dnum = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9), NumberFormats.FLOAT);
            dnum.setBorder(Border.ALL, BorderLineStyle.THIN);

            hl = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD));
            hl.setBorder(Border.ALL, BorderLineStyle.THIN);
            hl.setAlignment(Alignment.LEFT);
            hl.setVerticalAlignment(VerticalAlignment.CENTRE);

            hc = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD));
            hc.setBorder(Border.ALL, BorderLineStyle.THIN);
            hc.setAlignment(Alignment.CENTRE);
            hc.setVerticalAlignment(VerticalAlignment.CENTRE);

            hnum = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD), NumberFormats.FLOAT);
            hnum.setBorder(Border.ALL, BorderLineStyle.THIN);
        }
    }
}