package uniepp_load.scripts.print

import com.google.common.collect.Lists
import com.google.common.collect.Maps
import com.google.common.collect.Sets
import jxl.Workbook
import jxl.WorkbookSettings
import jxl.format.Orientation
import jxl.format.PageOrientation
import jxl.format.Alignment
import jxl.format.BorderLineStyle
import jxl.format.Border
import jxl.format.VerticalAlignment
import jxl.write.*
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang.ArrayUtils
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.common.ITitled
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.shared.commonbase.base.util.JExcelUtil
import org.tandemframework.shared.commonbase.base.util.key.PairKey
import org.tandemframework.shared.commonbase.utils.DQLSimple
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager
import org.tandemframework.shared.employeebase.base.entity.EmployeePost
import ru.tandemservice.uni.entity.catalog.YearDistribution
import ru.tandemservice.uni.entity.catalog.YearDistributionPart
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear
import ru.tandemservice.uniepp.entity.catalog.EppGroupType
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.EplOuSummaryManager
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCategoryWrapper
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplDiscOrRuleRowWrapper
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplSimpleRuleRowWrapper
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.assignment.EplAssignmentCommonWrapper
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplBaseCategoryDataWrapper
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplCommonRowWrapperComparator
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplTimeItemVariantGroupingCodes

import static jxl.biff.CellReferenceHelper.getCellReference
import static ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.assignment.EplAssignmentCommonWrapper.*

return new EplReportOrgUnitEduAssignmentPrint(                                                  // входные параметры скрипта
        session: session,                                                                       // сессия
        ouSummary: session.get(EplOrgUnitSummary.class, ouSummaryId),                           // id расчета
        dataWrapper: EplOuSummaryManager.instance().dao().getEplTimeDataWrapperForEduAssignmentPrint(ouSummaryId, yearPartIds, developFormIds, developConditionIds),
        filterYearPartIds: yearPartIds
).print()

/**
 * Печать учебного поручения читающему подразделению
 */
class EplReportOrgUnitEduAssignmentPrint
{
    // входные параметры
    Session session
    EplOrgUnitSummary ouSummary
    EplAssignmentCommonWrapper dataWrapper
    List<Long> filterYearPartIds
    ByteArrayOutputStream out = new ByteArrayOutputStream()

    int sheetIndex = 0
    WritableSheet sheetTitle
    Map<Integer, PairKey<Integer, Integer>> sheetToTotalValueMap = Maps.newHashMap()

    EducationYear eduYear
    Set<YearDistributionPart> yearParts = Sets.newTreeSet(YEAR_PART_COMPARATOR)

    /**
     * Основной метод печати.
     */
    def print()
    {
        if (null == dataWrapper) throw new ApplicationException("Нет данных по нагрузке для печати.")

        // создаем печатную форму
        WorkbookSettings ws = new WorkbookSettings()
        ws.setMergedCellChecking(false)
        ws.setRationalization(false)
        ws.setGCDisabled(true)
        def workbook = Workbook.createWorkbook(out, ws)

        printData(workbook)

        workbook.write()
        workbook.close()

        return [document: out.toByteArray(), fileName: "Учебное поручение читающему подразделению " + new Date().format("dd.MM.yyyy") + ".xls"]
    }

    def printData(WritableWorkbook workbook)
    {
        prepareTimeData()

        CellFormats formats = new CellFormats()

        doCreateTitle(workbook, formats)
        doCreateYearParts(workbook, formats)
        doCreatePractices(workbook, formats)
        doCreateOther(workbook, formats)

        fillTimeItemForTitle(workbook, formats)
    }

    private void doCreateTitle(WritableWorkbook workbook, CellFormats formats)
    {
        EmployeePost head = EmployeeManager.instance().dao().getHead(ouSummary.orgUnit)

        int rowNum = 0
        int[] columnViews = [3, 45, 10, 20, 3, 25, 14]

        sheetTitle = workbook.createSheet("Титул", sheetIndex)
        fillSheetSettings(sheetTitle)
        fillColumnViews(sheetTitle, columnViews)

        sheetTitle.addCell(new Label(1, rowNum, "Учебное поручение на " + eduYear.title + " уч. год", formats.hf1))
        sheetTitle.mergeCells(1, rowNum, 5, rowNum)
        sheetTitle.addCell(new Label(6, rowNum, "«УТВЕРЖДАЮ»", formats.df_c))

        sheetTitle.addCell(new Label(1, ++rowNum, ouSummary.orgUnit.title, formats.hf2))
        sheetTitle.mergeCells(1, rowNum, 5, rowNum)

        sheetTitle.addCell(new Label(6, ++rowNum, "", formats.line))
        sheetTitle.mergeCells(6, rowNum, 8, rowNum)

        sheetTitle.addCell(new Label(0, ++rowNum, "№", formats.dfc_b))
        sheetTitle.addCell(new Label(1, rowNum, "Нагрузка", formats.dfc_b))
        sheetTitle.addCell(new Label(2, rowNum, "Часов", formats.dfc_b))

        List<String> sheetTitles = Lists.newLinkedList()
        yearParts.each { value -> sheetTitles.add(getFirstCharUpperCase(value.title)) }
        sheetTitles.add("Практики и государственная итоговая аттестация")
        sheetTitles.add("Другие виды нагрузки")

        double index = 0
        int rowDataStart = rowNum + 1
        for (def title : sheetTitles)
        {
            sheetTitle.addCell(new Number(0, ++rowNum, ++index, formats.dfc))
            sheetTitle.addCell(new Label(1, rowNum, title, formats.dfl))
        }

        sheetTitle.addCell(new Label(0, ++rowNum, "", formats.dfl_b2))
        sheetTitle.addCell(new Label(1, rowNum, "Всего часов", formats.dfl_b3))
        sheetTitle.addCell(new Formula(2, rowNum, "SUM(" + JExcelUtil.getRange(2, rowDataStart, 2, rowNum-1) + ")", formats.dfr_b))

        rowNum += 2
        sheetTitle.addCell(new Label(1, rowNum, "Руководитель подразделения", formats.df))
        sheetTitle.addCell(new Label(3, rowNum, "", formats.line))
        sheetTitle.addCell(new Label(5, rowNum, head == null ? "" : head.employee.person.identityCard.fio, formats.line))

        sheetTitle.addCell(new Label(3, ++rowNum, "подпись", formats.df_c))
        sheetTitle.addCell(new Label(5, rowNum, "Фамилия И.О.", formats.df_c))
    }

    private void doCreateYearParts(WritableWorkbook workbook, CellFormats formats)
    {
        def regTypeCodes = Collections.singletonList(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)

        for (def yearPart : yearParts)
        {
            def yearPartTitle = getFirstCharUpperCase(yearPart.title)
            printRows(workbook, formats, yearPart, yearPartTitle, regTypeCodes)
        }
    }

    private void doCreatePractices(WritableWorkbook workbook, CellFormats formats)
    {
        def yearPartTitle = "Практики и государственная итоговая аттестация"
        def regTypeCodes = Arrays.asList(EppRegistryStructureCodes.REGISTRY_PRACTICE, EppRegistryStructureCodes.REGISTRY_ATTESTATION)

        printRows(workbook, formats, null, yearPartTitle, regTypeCodes)
    }

    private void doCreateOther(WritableWorkbook workbook, CellFormats formats)
    {
        List<EplSimpleRuleRowWrapper> ruleRows = Lists.newArrayList(dataWrapper.simpleRuleRowMap.getOrDefault(EplSimpleRuleRowWrapper.BY_SIMPLE_TIME_RULE, Maps.newHashMap()).values())
        Collections.sort(ruleRows, ITitled.TITLED_COMPARATOR)

        List<EplDiscOrRuleRowWrapper> discRows = Lists.newArrayList(dataWrapper.discRowMap.getOrDefault(EplTimeItemVariantGroupingCodes.BY_TIME_RULE, Maps.newHashMap()).values())
        Collections.sort(discRows, EplCommonRowWrapperComparator.INSTANCE)

        int[] columnViews = [50, 45, 5, 15, 10, 7]
        def sheetTitle = "Другие виды нагрузки"
        def sheet = workbook.createSheet(sheetTitle, ++sheetIndex)
        fillSheetSettings(sheet)
        fillColumnViews(sheet, columnViews)

        int rowNum = 0
        sheet.addCell(new Label(0, rowNum, "Учебное поручение на " + eduYear.title + " уч. год", formats.df_i))
        sheet.addCell(new Label(1, rowNum, ouSummary.orgUnit.title, formats.df))

        sheet.addCell(new Label(0, ++rowNum, sheetTitle, formats.df_b))

        int colNum = 0
        sheet.setRowView(++rowNum, 60 * 20)
        sheet.addCell(new Label(colNum, rowNum, "Вид нагрузки", formats.dfc))
        sheet.addCell(new Label(++colNum, rowNum, "Параметры обучения (направление подготовки, формирующее подр., характеристики)", formats.dfc))
        sheet.addCell(new Label(++colNum, rowNum, "Курс", formats.dfc))
        sheet.addCell(new Label(++colNum, rowNum, "Часть года", formats.dfc))
        sheet.addCell(new Label(++colNum, rowNum, "Студентов", formats.dfc))
        sheet.addCell(new Label(++colNum, rowNum, "Всего часов", formats.dfc))

        int lastColNum = colNum

        rowNum++
        int ruleRowNum = rowNum
        for (EplSimpleRuleRowWrapper row : ruleRows)
        {
            colNum = 0
            double timeSum = 0

            row.simpleTimeItemList.each { timeItem -> timeSum += timeItem.timeAmount }

            sheet.addCell(new Label(colNum, rowNum, row.rule.title, formats.dfl))

            for (colNum = 1; colNum <= lastColNum; colNum++)
                sheet.addCell(new Label(colNum, rowNum, "", formats.dfr))

            sheet.addCell(new Number(lastColNum, rowNum, timeSum, formats.dfr))
            rowNum++
        }

        List<Integer> discRowNumList = Lists.newLinkedList()
        for (EplDiscOrRuleRowWrapper row : discRows)
        {
            Set<EppGroupType> eppGroupTypes = Sets.newTreeSet(GROUP_TYPE_COMPARATOR)
            Map<EppGroupType, Set<EplEduGroup>> gt2EplEduGroupMap = Maps.newHashMap()
            Map<EppGroupType, Double> timeMap = Maps.newHashMap()

            for (EplEduGroupTimeItem timeItem : row.eduGroupTimeItemList)
            {
                def eduGroup = timeItem.eduGroup
                def type = eduGroup.groupType

                eppGroupTypes.add(type)
                SafeMap.safeGet(gt2EplEduGroupMap, type, HashSet.class).add(eduGroup)

                timeMap.put(type, timeMap.getOrDefault(type, 0d) + timeItem.timeAmount)
            }

            int discRow = rowNum
            Map<EppGroupType, ParameterKey> gt2MultiKeyMap = dataWrapper.getGroupType2ParameterKeyMap(gt2EplEduGroupMap, false, false)
            Set<ParameterKey> parameters = Sets.newHashSet(gt2MultiKeyMap.values())

            // добавляем строку дисциплиночасти
            if (!eppGroupTypes.isEmpty())
            {
                colNum = 0
                sheet.addCell(new Label(colNum, discRow, row.rule.title, formats.dfl))

                // если данные в УГС совпадают
                if (parameters.size() == 1)
                {
                    ParameterKey key = parameters.iterator().next()
                    def time = timeMap.values().iterator().next()

                    sheet.addCell(new Label(++colNum, rowNum, key.eouTitles, formats.dfl))
                    sheet.addCell(new Label(++colNum, rowNum, key.courses, formats.dfl))
                    sheet.addCell(new Label(++colNum, rowNum, key.groupYearPartTitles, formats.dfl))
                    sheet.addCell(new Number(++colNum, rowNum, key.studentCount, formats.dfr))
                    sheet.addCell(new Number(++colNum, rowNum, time, formats.dfr))
                }
                else
                {
                    for (def eppGroupType : eppGroupTypes)
                    {
                        ParameterKey key = gt2MultiKeyMap.get(eppGroupType)

                        colNum = 0
                        sheet.addCell(new Label(colNum, ++rowNum, eppGroupType.title, formats.dfl_tb))
                        sheet.addCell(new Label(++colNum, rowNum, key.eouTitles, formats.dfl))
                        sheet.addCell(new Label(++colNum, rowNum, key.courses, formats.dfl))
                        sheet.addCell(new Label(++colNum, rowNum, key.groupYearPartTitles, formats.dfl))
                        sheet.addCell(new Number(++colNum, rowNum, key.studentCount, formats.dfr))
                        sheet.addCell(new Number(++colNum, rowNum, timeMap.get(eppGroupType), formats.dfr))
                    }

                    for (colNum = 1; colNum < lastColNum; colNum++)
                        sheet.addCell(new Label(colNum, discRow, "", formats.dfr))

                    sheet.addCell(new Formula(lastColNum, discRow, "SUM(" + JExcelUtil.getRange(lastColNum, discRow + 1, lastColNum, rowNum) + ")", formats.dfr))
                }

                discRowNumList.add(discRow)
                rowNum++
            }
        }

        colNum = 0
        sheet.addCell(new Label(colNum, rowNum, "Всего", formats.dfl_b))
        sheet.mergeCells(colNum, rowNum, lastColNum - 1, rowNum)

        colNum = lastColNum
        List<String> values = Lists.newArrayList()
        discRowNumList.each { discRowNum -> values.add(getCellReference(colNum, discRowNum)) }

        sheetToTotalValueMap.put(sheetIndex, PairKey.create(colNum, rowNum))
        def ruleRowCount = values.empty ? "" : StringUtils.join(values, "+")
        def totalCount = (ruleRows.empty ? "" : "SUM(" + JExcelUtil.getRange(colNum, ruleRowNum, colNum, ruleRowNum + (ruleRows.size() - 1)) + ")") + (ruleRowCount.empty ? "" : "+" + ruleRowCount)

        if (totalCount.empty)
            sheet.addCell(new Label(colNum, rowNum, "", formats.dfr_b))
        else
            sheet.addCell(new Formula(colNum, rowNum, totalCount, formats.dfr_b))
    }

    private void printRows(WritableWorkbook workbook, CellFormats formats, YearDistributionPart yearPart, String headerTitle, List<String> regTypeCodes)
    {
        def discRowMap = dataWrapper.discRowMap

        List<EplDiscOrRuleRowWrapper> discRows = Lists.newArrayList()

        def regElementList = discRowMap.getOrDefault(EplTimeItemVariantGroupingCodes.BY_REG_ELEMENT, Maps.newHashMap()).values()
        def regElementAndTimeRuleList = discRowMap.getOrDefault(EplTimeItemVariantGroupingCodes.BY_REG_ELEMENT_AND_TIME_RULE, Maps.newHashMap()).values()

        for (EplDiscOrRuleRowWrapper row : regElementList)
        {
            for (EplDiscOrRuleRowWrapper rowWithTimeRule : regElementAndTimeRuleList)
            {
                if (row.discipline.equals(rowWithTimeRule.discipline))
                {
                    row.eduGroupTimeItemList.addAll(rowWithTimeRule.eduGroupTimeItemList)
                    row.eplGroups.addAll(rowWithTimeRule.eplGroups)
                }
            }
            discRows.add(row)
        }
        Collections.sort(discRows, EplCommonRowWrapperComparator.INSTANCE)

        Set<EplTimeCategoryEduLoad> usedCategories = Sets.newHashSet()
        Map<PairKey<EplDiscOrRuleRowWrapper, YearDistributionPart>, List<EplEduGroupTimeItem>> rowsMap = Maps.newLinkedHashMap()

        for (EplDiscOrRuleRowWrapper row : discRows)
        {
            def regStructure = row.discipline.registryElement.parent
            if (!isRegTypeEquals(regStructure, regTypeCodes)) continue

            def timeItems = row.eduGroupTimeItemList
            if (null == yearPart)
                Collections.sort(timeItems, EDU_GROUP_TIME_ITEM_COMPARATOR)

            for (EplEduGroupTimeItem timeItem : timeItems)
            {
                def eduYearPart = timeItem.eduGroup.yearPart
                if (null != yearPart && !yearPart.equals(eduYearPart)) continue

                def category = timeItem.timeRule.category
                usedCategories.add(category)

                // для практик группируем по части уч. года
                SafeMap.safeGet(rowsMap, PairKey.create(row, yearPart != null ? null : eduYearPart), ArrayList.class).add(timeItem)
            }
        }

        EplBaseCategoryDataWrapper categoryDataWrapper = EplOuSummaryManager.instance().dao().getCategoryDataWrapper(usedCategories, Sets.newHashSet())
        def levelCount = categoryDataWrapper.categoryLevelCount
        def topCategories = categoryDataWrapper.topLevelCategories

        int[] columnViews1 = [50, 45, 4]
        int[] columnViews2 = [4, 4, 4, 4, 15, 7]
        int[] columnYearPartView = yearPart != null ? null : [15]

        // параметры
        int[] columnViews = ArrayUtils.addAll(columnViews1, columnYearPartView)
        columnViews = ArrayUtils.addAll(columnViews, columnViews2)

        // категории
        int[] categoryViews = new int[usedCategories.size()]

        Arrays.fill(categoryViews, 7)
        columnViews = ArrayUtils.addAll(columnViews, categoryViews)

        def sheetTitle = yearPart == null ? "Практики и ГИА" : headerTitle
        def sheet = workbook.createSheet(sheetTitle, ++sheetIndex)
        fillSheetSettings(sheet)
        fillColumnViews(sheet, columnViews)

        int rowNum = 0
        sheet.addCell(new Label(0, rowNum, "Учебное поручение на " + eduYear.title + " уч. год", formats.df_i))
        sheet.addCell(new Label(1, rowNum, ouSummary.orgUnit.title, formats.df))

        sheet.addCell(new Label(0, ++rowNum, headerTitle, formats.df_b))

        int colNum = 0
        sheet.setRowView(++rowNum, 60 * 20)
        sheet.addCell(new Label(colNum, rowNum, "Название мероприятия", formats.dfc))
        sheet.addCell(new Label(++colNum, rowNum, "Параметры обучения (направление подготовки, формирующее подр., характеристики)", formats.dfc))
        sheet.addCell(new Label(++colNum, rowNum, "Курс", formats.dfc_vert))
        if (null == yearPart)
            sheet.addCell(new Label(++colNum, rowNum, "Часть года", formats.dfc_vert))
        sheet.addCell(new Label(++colNum, rowNum, "Потоков", formats.dfc_vert))
        sheet.addCell(new Label(++colNum, rowNum, "Групп", formats.dfc_vert))
        sheet.addCell(new Label(++colNum, rowNum, "Подгрупп", formats.dfc_vert))
        sheet.addCell(new Label(++colNum, rowNum, "Студентов", formats.dfc_vert))
        sheet.addCell(new Label(++colNum, rowNum, "Группа", formats.dfc_vert))
        sheet.addCell(new Label(++colNum, rowNum, "Всего часов", formats.dfc_vert))

        if (levelCount > 1)
        {
            for (int i = 0; i <= colNum; i++)
                sheet.mergeCells(i, rowNum, i, rowNum + 1)
        }

        for (EplCategoryWrapper categoryWrapper : topCategories)
        {
            sheet.addCell(new Label(++colNum, rowNum, categoryWrapper.title, categoryWrapper.children.empty ? formats.dfc_vert : formats.dfc))

            if (levelCount > 1)
            {
                def children = categoryWrapper.children
                if (children.empty)
                    sheet.mergeCells(colNum, rowNum, colNum, rowNum + 1)
                else
                {
                    int colSubCategoryNum = colNum
                    sheet.mergeCells(colNum, rowNum, colNum += (children.size() - 1), rowNum)
                    for (def subCategoryWrapper : children)
                    {
                        sheet.addCell(new Label(colSubCategoryNum++, rowNum + 1, subCategoryWrapper.title, formats.dfc_vert))
                    }
                }
            }
        }
        int lastColNum = colNum
        int paramColNum = yearPart != null ? 8 : 9

        if (levelCount > 1) rowNum++
        rowNum++

        List<Integer> discRowNumList = Lists.newLinkedList()
        for (Map.Entry<PairKey<EplDiscOrRuleRowWrapper, YearDistributionPart>, List<EplEduGroupTimeItem>> entry : rowsMap.entrySet())
        {
            def row = entry.key.first

            Set<EppGroupType> eppGroupTypes = Sets.newTreeSet(GROUP_TYPE_COMPARATOR)
            Map<EppGroupType, Set<EplEduGroup>> gt2EplEduGroupMap = Maps.newHashMap()
            Map<EppGroupType, Map<EplTimeCategoryEduLoad, Double>> gt2CategoryTimeMap = Maps.newHashMap()
            Map<EplTimeCategoryEduLoad, Double> category2TimeMap = Maps.newHashMap()

            for (def timeItem : entry.value)
            {
                def eduGroup = timeItem.eduGroup

                def type = eduGroup.groupType
                def category = timeItem.timeRule.category

                eppGroupTypes.add(type)
                SafeMap.safeGet(gt2EplEduGroupMap, type, HashSet.class).add(eduGroup)

                Map<EplTimeCategoryEduLoad, Double> timeMap = SafeMap.safeGet(gt2CategoryTimeMap, type, HashMap.class)
                timeMap.put(category, timeMap.getOrDefault(category, 0d) + timeItem.timeAmount)
                category2TimeMap.put(category, category2TimeMap.getOrDefault(category, 0d) + timeItem.timeAmount)
            }

            // подсчитываем параметры
            Map<EppGroupType, ParameterKey> gt2MultiKeyMap = dataWrapper.getGroupType2ParameterKeyMap(gt2EplEduGroupMap, true, true)
            Set<ParameterKey> parameters = Sets.newHashSet(gt2MultiKeyMap.values())

            int discRow = rowNum
            paramColNum = 0

            if (parameters.size() > 1)
            {
                for (def eppGroupType : eppGroupTypes)
                {
                    ParameterKey key = gt2MultiKeyMap.get(eppGroupType)

                    colNum = 0
                    sheet.addCell(new Label(colNum, ++rowNum, eppGroupType.title, formats.dfl_tb))
                    colNum = printParametersRow(sheet, formats, yearPart, colNum, rowNum, key)
                    sheet.addCell(new Formula(++colNum, rowNum, "SUM(" + JExcelUtil.getRange(colNum + 1, rowNum, lastColNum, rowNum) + ")", formats.dfr))
                    paramColNum = colNum

                    Map<EplTimeCategoryEduLoad, Double> timeMap = gt2CategoryTimeMap.get(eppGroupType)
                    printCategoriesRow(sheet, formats, colNum, rowNum, topCategories, timeMap)
                }
            }

            // добавляем строку дисциплиночасти
            if (!eppGroupTypes.empty)
            {
                colNum = 0
                sheet.addCell(new Label(colNum, discRow, row.discipline.titleWithLabor, formats.dfl))

                // если данные в УГС совпадают
                if (parameters.size() == 1)
                {
                    // по параметрам
                    ParameterKey key = parameters.iterator().next()
                    colNum = printParametersRow(sheet, formats, yearPart, colNum, discRow, key)
                    sheet.addCell(new Formula(++colNum, discRow, "SUM(" + JExcelUtil.getRange(colNum + 1, discRow, lastColNum, discRow) + ")", formats.dfr))
                    paramColNum = colNum

                    // по категориям
                    printCategoriesRow(sheet, formats, colNum, rowNum, topCategories, category2TimeMap)
                }
                else
                {
                    // по параметрам
                    for (colNum = 1; colNum <= paramColNum; colNum++)
                        sheet.addCell(new Label(colNum, discRow, "", formats.dfr))

                    // по категориям
                    for (colNum = paramColNum; colNum <= lastColNum; colNum++)
                        sheet.addCell(new Formula(colNum, discRow, "SUM(" + JExcelUtil.getRange(colNum, discRow + 1, colNum, rowNum) + ")", formats.dfr))
                }

                discRowNumList.add(discRow)
                rowNum++
            }
        }

        colNum = 0
        sheet.addCell(new Label(colNum, rowNum, "Всего", formats.dfl_b))
        sheet.mergeCells(colNum, rowNum, paramColNum - 1, rowNum)

        sheetToTotalValueMap.put(sheetIndex, PairKey.create(paramColNum, rowNum))

        // по категориям
        for (colNum = paramColNum; colNum <= lastColNum; colNum++)
        {
            List<String> values = Lists.newArrayList()
            discRowNumList.each { discRowNum -> values.add(getCellReference(colNum, discRowNum)) }
            def totalCount = StringUtils.join(values, "+")

            if (totalCount.empty)
            {
                if (colNum == lastColNum)
                    sheet.addCell(new Number(colNum, rowNum, 0, formats.dfr_b))
                else
                    sheet.addCell(new Label(colNum, rowNum, "", formats.dfr_b))
            }
            else
                sheet.addCell(new Formula(colNum, rowNum, totalCount, formats.dfr_b))
        }
    }

    private static int printParametersRow(WritableSheet sheet, CellFormats formats, YearDistributionPart yearPart, int colNum, int rowNum, ParameterKey key)
    {
        sheet.addCell(new Label(++colNum, rowNum, key.eouTitles, formats.dfl))
        sheet.addCell(new Label(++colNum, rowNum, key.courses, null == yearPart ? formats.dfl : formats.dfr))

        if (null == yearPart)
            sheet.addCell(new Label(++colNum, rowNum, key.groupYearPartTitles, formats.dfl))

        sheet.addCell(new Number(++colNum, rowNum, key.flowCount, formats.dfr))
        sheet.addCell(new Number(++colNum, rowNum, key.groupCount, formats.dfr))
        sheet.addCell(new Number(++colNum, rowNum, key.subGroupCount, formats.dfr))
        sheet.addCell(new Number(++colNum, rowNum, key.studentCount, formats.dfr))
        sheet.addCell(new Label(++colNum, rowNum, key.eplGroups, formats.dfl))

        return colNum
    }

    private static void printCategoriesRow(WritableSheet sheet, CellFormats formats, int colNum, int rowNum, List<EplCategoryWrapper> topCategories, Map<EplTimeCategoryEduLoad, Double> timeMap)
    {
        for (EplCategoryWrapper categoryWrapper : topCategories)
        {
            def children = categoryWrapper.children
            if (children.empty)
            {
                double timeSum = timeMap.getOrDefault(categoryWrapper.category, 0)
                sheet.addCell(new Number(++colNum, rowNum, timeSum, formats.dfr))
            } else
            {
                for (def subCategoryWrapper : children)
                {
                    double timeSum = timeMap.getOrDefault(subCategoryWrapper.category, 0)
                    sheet.addCell(new Number(++colNum, rowNum, timeSum, formats.dfr))
                }
            }
        }
    }

    private static String getFirstCharUpperCase(String value)
    {
        return value.substring(0, 1).toUpperCase() + value.substring(1)
    }

    private static void fillColumnViews(WritableSheet sheet, int[] columnViews)
    {
        for (int i = 0; i < columnViews.size(); i++)
            sheet.setColumnView(i, columnViews[i])
    }

    private void prepareTimeData()
    {
        eduYear = ouSummary.studentSummary.eppYear.educationYear

        Set<EplEduGroup> eduGroups = dataWrapper.eduGroups
        Set<YearDistribution> yearDistributions = Sets.newHashSet()
        eduGroups.each { eduGroup -> yearDistributions.add(eduGroup.yearPart.yearDistribution) }

        List<YearDistributionPart> allYearParts = new DQLSimple<>(YearDistributionPart.class)
                .order(YearDistributionPart.yearDistribution().amount())
                .order(YearDistributionPart.number())
                .list()

        for (def dist : yearDistributions) {
            for (def item : allYearParts) {
                if (CollectionUtils.isNotEmpty(filterYearPartIds) && !filterYearPartIds.contains(item.id)) continue
                if (dist.equals(item.yearDistribution)) yearParts.add(item)
            }
        }
    }

    private static void fillSheetSettings(WritableSheet sheet)
    {
        def s = sheet.settings
        s.setOrientation(PageOrientation.LANDSCAPE)

        double margin = 1 / 2.54; // поле в дюймах
        s.setLeftMargin(margin * 0.64)
        s.setRightMargin(margin * 0.64)
        s.setTopMargin(margin * 1.91)
        s.setBottomMargin(margin * 1.91)
        s.setHeaderMargin(margin * 0.76)
        s.setFooterMargin(margin * 0.76)
    }

    private void fillTimeItemForTitle(WritableWorkbook workbook, CellFormats formats)
    {
        int rowNum = 4
        for (int sheetNum = 0; sheetNum <= sheetIndex; sheetNum++)
        {
            if (sheetNum == 0) continue
            def sheetName = workbook.getSheetNames()[sheetNum]

            StringBuffer buf = new StringBuffer("\'").append(sheetName).append("\'").append("!")
            PairKey<Integer, Integer> coordinates = sheetToTotalValueMap.get(sheetNum)
            getCellReference(coordinates.first, coordinates.second, buf)

            sheetTitle.addCell(new Formula(2, rowNum++, buf.toString(), formats.dfr))
        }
    }

    private static class CellFormats
    {
        final WritableCellFormat hf1
        final WritableCellFormat hf2
        final WritableCellFormat df
        final WritableCellFormat df_i
        final WritableCellFormat df_c
        final WritableCellFormat df_b
        final WritableCellFormat dfc
        final WritableCellFormat dfc_vert
        final WritableCellFormat dfc_b
        final WritableCellFormat dfr
        final WritableCellFormat dfl
        final WritableCellFormat dfl_tb
        final WritableCellFormat dfl_b
        final WritableCellFormat dfl_b2
        final WritableCellFormat dfl_b3
        final WritableCellFormat dfr_b
        final WritableCellFormat line

        private CellFormats() throws Throwable
        {
            WritableFont headerFont1 = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD)
            WritableFont headerFont2 = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD)
            WritableFont baseFontBold = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD)
            WritableFont baseFont = new WritableFont(WritableFont.ARIAL, 9)

            hf1 = new WritableCellFormat(headerFont1)
            hf1.setAlignment(Alignment.CENTRE)

            hf2 = new WritableCellFormat(headerFont2)
            hf2.setAlignment(Alignment.CENTRE)

            df = new WritableCellFormat(baseFont)
            df.setAlignment(Alignment.LEFT)
            df.setVerticalAlignment(VerticalAlignment.TOP)
            df.setWrap(true)

            df_i = new WritableCellFormat(baseFont)
            df_i.setAlignment(Alignment.LEFT)
            df_i.setVerticalAlignment(VerticalAlignment.TOP)
            df_i.setWrap(true)
            df_i.setIndentation(1)

            df_c = new WritableCellFormat(baseFont)
            df_c.setAlignment(Alignment.CENTRE)

            df_b = new WritableCellFormat(baseFontBold)
            df_b.setAlignment(Alignment.LEFT)
            df_b.setIndentation(1)

            dfc = new WritableCellFormat(baseFont)
            dfc.setAlignment(Alignment.CENTRE)
            dfc.setVerticalAlignment(VerticalAlignment.CENTRE)
            dfc.setBorder(Border.ALL, BorderLineStyle.THIN)
            dfc.setWrap(true)

            dfc_vert = new WritableCellFormat(baseFont)
            dfc_vert.setAlignment(Alignment.CENTRE)
            dfc_vert.setVerticalAlignment(VerticalAlignment.CENTRE)
            dfc_vert.setBorder(Border.ALL, BorderLineStyle.THIN)
            dfc_vert.setOrientation(Orientation.PLUS_90)
            dfc_vert.setWrap(true)

            dfc_b = new WritableCellFormat(baseFontBold)
            dfc_b.setAlignment(Alignment.CENTRE)
            dfc_b.setBorder(Border.ALL, BorderLineStyle.THIN)

            dfr = new WritableCellFormat(baseFont)
            dfr.setAlignment(Alignment.RIGHT)
            dfr.setVerticalAlignment(VerticalAlignment.TOP)
            dfr.setBorder(Border.ALL, BorderLineStyle.THIN)
            dfr.setWrap(true)

            dfl = new WritableCellFormat(baseFont)
            dfl.setAlignment(Alignment.LEFT)
            dfl.setVerticalAlignment(VerticalAlignment.TOP)
            dfl.setBorder(Border.ALL, BorderLineStyle.THIN)
            dfl.setWrap(true)

            dfl_tb = new WritableCellFormat(baseFont)
            dfl_tb.setAlignment(Alignment.LEFT)
            dfl_tb.setVerticalAlignment(VerticalAlignment.TOP)
            dfl_tb.setBorder(Border.ALL, BorderLineStyle.THIN)
            dfl_tb.setWrap(true)
            dfl_tb.setIndentation(1)

            dfl_b = new WritableCellFormat(baseFontBold)
            dfl_b.setAlignment(Alignment.LEFT)
            dfl_b.setBorder(Border.ALL, BorderLineStyle.THIN)

            dfl_b2 = new WritableCellFormat(baseFontBold)
            dfl_b2.setAlignment(Alignment.LEFT)
            dfl_b2.setBorder(Border.TOP, BorderLineStyle.THIN)
            dfl_b2.setBorder(Border.BOTTOM, BorderLineStyle.THIN)
            dfl_b2.setBorder(Border.LEFT, BorderLineStyle.THIN)

            dfl_b3 = new WritableCellFormat(baseFontBold)
            dfl_b3.setAlignment(Alignment.LEFT)
            dfl_b3.setBorder(Border.TOP, BorderLineStyle.THIN)
            dfl_b3.setBorder(Border.BOTTOM, BorderLineStyle.THIN)

            dfr_b = new WritableCellFormat(baseFontBold)
            dfr_b.setAlignment(Alignment.RIGHT)
            dfr_b.setBorder(Border.ALL, BorderLineStyle.THIN)

            line = new WritableCellFormat(baseFont)
            line.setAlignment(Alignment.CENTRE)
            line.setBorder(Border.BOTTOM, BorderLineStyle.THIN)
        }
    }
}
