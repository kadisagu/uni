/* $Id: $ */
package uniepp_load.scripts.timeRule

import ru.tandemservice.uni.entity.catalog.codes.CourseCodes
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes
import ru.tandemservice.uni.entity.catalog.codes.DevelopTechCodes
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.EplTimeRuleDao
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleDao
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleScript

/**
 * Руководство магистерской работой очной формы обучения: 10 часов * количество магистрантов в первый учебный год
 */
return new IEplTimeRuleScript() {

    @Override
    public IEplTimeRuleDao.IRuleResult count(IEplTimeRuleDao.ILoadData loadData)
    {
        // непонятно, к какому План. потоку применять - надо найти такой, который будет только один в РУП, чтобы магистранты не посчитались дважды
        // понадеемся, что в учебном году только одна курсовая работа или проект
        if (!EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_PROJECT.equals(loadData.eduGroup().getGroupType().getCode()) && !EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_WORK.equals(loadData.eduGroup().getGroupType().getCode()))
            return null;

        // на втором году обучения можно было бы брать госэкзамен - например, вот так:
        // if (!EppGroupTypeFCACodes.CONTROL_ACTION_EXAM.equals(loadData.eduGroup().getGroupType().getCode()))
        //    return null;
        // if (!EppRegistryStructureCodes.REGISTRY_ATTESTATION_EXAM.equals(loadData.eduGroup().getRegistryElementPart().getRegistryElement().getParent().getCode()))
        //    return null;

        // посчитаем число студентов очной формы, первого года обучения
        Integer students = 0;
        for (Map.Entry<EplStudent, Integer> e : loadData.groupContent().entrySet()) {
            EplStudent student = e.getKey()
            // только магистры
            def programKind = e.getKey().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject()?.getEduProgramKind()?.getCode()
            if (!EduProgramKindCodes.PROGRAMMA_MAGISTRATURY.equals(programKind)) continue;
            // только очная форма
            if (!DevelopFormCodes.FULL_TIME_FORM.equals(student.getEducationOrgUnit().getDevelopForm().getCode())) continue;
            // только первый год обучения
            if (!CourseCodes.COURSE_1.equals(student.getGroup().getCourse().getCode())) continue;
            // дистанционное обучение не считаем
            if (!DevelopTechCodes.GENERAL.equals(student.getEducationOrgUnit().getDevelopTech().getCode())) continue;
            students = students + e.getValue();
        }

        // если таких студентов нет - заканчиваем
        if (students == 0) return null;

        // сформируем описание
        String description = "План. поток: "+ loadData.eduGroup().getTitle() + ", дисциплина: " + loadData.eduGroup().getRegistryElementPart().getTitleWithNumber();

        // возвращаем результат
        return EplTimeRuleDao.getResult(10 * students, description);
    }
}