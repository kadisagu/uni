package uniepp_load.scripts.timeRule

import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleDao
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleScript

/**
 * Скрипт по умолчанию.
 */
return new IEplTimeRuleScript() {

    @Override
    IEplTimeRuleDao.IRuleResult count(IEplTimeRuleDao.ILoadData loadData)
    {
        return null
    }
}
