/* $Id: $ */
package uniepp_load.scripts.timeRule

import ru.tandemservice.uniepp.entity.catalog.EppALoadType
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeALTCodes
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.EplTimeRuleDao
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleDao
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleScript

/**
 * Реферат 0,1 часа на одну работу
 * Эссе 0,2 часа на одну работу
 * Расчетно-графическая работа 0,2 часа на одну работу
 * Репродуктивная контрольная работа 0,1 часа на одну работу
 *
 * Входит в План. поток по практикам, при их отсутствии - в План. поток по лабораторным.
 */
return new IEplTimeRuleScript() {

    // эти коды смотрим в справочнике «Формы текущего контроля», в поле code элемента в режиме просмотра объекта
    private static final String REFERAT_CODE = "25";
    private static final String ESSAY_CODE = "7";
    private static final String RGR_CODE = "22";
    private static final String REPR_CODE = "8";

    @Override
    public IEplTimeRuleDao.IRuleResult count(IEplTimeRuleDao.ILoadData loadData)
    {
        // если это План. поток по лабораторным
        if (EppGroupTypeALTCodes.TYPE_LABS.equals(loadData.eduGroup().getGroupType().getCode())) {
            // то проверим, есть ли часы практик (и, как следствие, План. поток по ним)
            double hours = loadData.discData().getLoadAsDouble(EppALoadType.FULL_CODE_TOTAL_PRACTICE);
            // если часы есть, то и План. поток есть - тогда в лабораторные не будем считать контрольные работы
            if (hours > 0) return null;
        } else if (!EppGroupTypeALTCodes.TYPE_PRACTICE.equals(loadData.eduGroup().getGroupType().getCode())) {
            // все остальные План. потоки, кроме практик, игнорируем
            return null;
        }

        // считаем часы только для дисциплин, для практик и ИГА не считаем
        if (!EppRegistryStructureCodes.REGISTRY_DISCIPLINE.equals(loadData.eduGroup().getRegistryElementPart().getRegistryElement().getParent().getCode()))
            return null;

        // получаем число контрольных мероприятий
        int refCount = loadData.discData().getActionSize(EppIControlActionType.getFullCode(REFERAT_CODE));
        int essayCount = loadData.discData().getActionSize(EppIControlActionType.getFullCode(ESSAY_CODE));
        int rgrCount = loadData.discData().getActionSize(EppIControlActionType.getFullCode(RGR_CODE));
        int reprCount = loadData.discData().getActionSize(EppIControlActionType.getFullCode(REPR_CODE));

        // рассчитываем число часов по норме времени
        double hours = 0.1 * refCount + 0.2 * essayCount + 0.2 * rgrCount + 0.1 * reprCount;

        // если часов нет - нужно вернуть null
        if (hours <= 0) return null;

        // умножим на число студентов
        Integer students = 0;
        for (Integer amount : loadData.groupContent().values()) students += amount;
        hours = hours * students;

        // составляем описание
        String description = "План. поток: "+ loadData.eduGroup().getTitle() + ", дисциплина: " + loadData.eduGroup().getRegistryElementPart().getTitleWithNumber();

        // возвращаем результат - часы и описание
        return EplTimeRuleDao.getResult(hours, description);
    }
}