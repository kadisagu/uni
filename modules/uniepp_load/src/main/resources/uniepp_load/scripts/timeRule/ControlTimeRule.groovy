/* $Id: $ */
package uniepp_load.scripts.timeRule

import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeALTCodes
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.EplTimeRuleDao
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleDao
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleScript

/**
 * 0,4 часа на одно задание, но не более 1 часа на одного обучающегося на дисциплину в семестр при отсутствии курсового проекта (работы) по дисциплине в данном семестре.
 */
return new IEplTimeRuleScript() {

    @Override
    public IEplTimeRuleDao.IRuleResult count(IEplTimeRuleDao.ILoadData loadData)
    {
        if (!EppGroupTypeALTCodes.TYPE_LECTURES.equals(loadData.eduGroup().getGroupType().getCode()))
            return null;
        if (!EppRegistryStructureCodes.REGISTRY_DISCIPLINE.equals(loadData.eduGroup().getRegistryElementPart().getRegistryElement().getParent().getCode()))
            return null;
        if (loadData.discData().hasActions4Groups(EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_PROJECT)) return null;
        Integer students = 0;
        for (Integer amount : loadData.groupContent().values()) students += amount;
        String description = "План. поток: "+ loadData.eduGroup().getTitle() + ", дисциплина: " + loadData.eduGroup().getRegistryElementPart().getTitleWithNumber() + ", студенты: " + students;
        int control = 0;
        for (EppIControlActionType type : loadData.icaTypes()) {
            // Контрольная работа, реферат или расчетно-графическая работа
            if (type.getUserCode().equals("ca_controlwork") || type.getUserCode().equals("ca_abstract") || type.getUserCode().equals("ca_graphwork")) {
                control += loadData.discData().getActionSize(type.getFullCode());
            }
        }
        if (control <= 0) return null;
        return EplTimeRuleDao.getResult(Math.min((double) (0.4 * control), (double) students), description);
    }
}