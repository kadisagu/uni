/* $Id: $ */
package uniepp_load.scripts.timeRule

import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes
import ru.tandemservice.uniepp.entity.catalog.EppALoadType
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeALTCodes
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.EplTimeRuleDao
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleDao
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleScript

/**
 * Процент от общего числа лекционных часов по каждой дисциплине на поток: 5% - по очной форме обучения; 10% - по очно-заочной форме обучения; 15% - по заочной форме обучения.
 */
return new IEplTimeRuleScript() {

    @Override
    public IEplTimeRuleDao.IRuleResult count(IEplTimeRuleDao.ILoadData loadData)
    {
        // применяем только к УГС по лекциям
        if (!EppGroupTypeALTCodes.TYPE_LECTURES.equals(loadData.eduGroup().getGroupType().getCode()))
            return null;
        // применяем только к дисциплинам, практики и ИГА пропускаем
        if (!EppRegistryStructureCodes.REGISTRY_DISCIPLINE.equals(loadData.eduGroup().getRegistryElementPart().getRegistryElement().getParent().getCode()))
            return null;

        // рассчитаем число студентов разных форм обучения
        Integer studentsFullTime = 0;
        Integer studentsPartTime = 0;
        Integer studentsExt = 0;
        for (Map.Entry<EplStudent, Integer> e : loadData.groupContent().entrySet()) {
            def devForm = e.getKey().getEducationOrgUnit().getDevelopForm().getCode()
            if (DevelopFormCodes.FULL_TIME_FORM.equals(devForm)) studentsFullTime = studentsFullTime + e.getValue();
            else if (DevelopFormCodes.PART_TIME_FORM.equals(devForm)) studentsPartTime = studentsPartTime + e.getValue();
            else studentsExt = studentsExt + e.getValue();
        }

        // сформируем описание
        String description = "План. поток: "+ loadData.eduGroup().getTitle() + ", дисциплина: " + loadData.eduGroup().getRegistryElementPart().getTitleWithNumber();

        // получим число часов лекций
        double hours = loadData.discData().getLoadAsDouble(EppALoadType.FULL_CODE_TOTAL_LECTURES);

        // если у нас есть студенты очной формы - применим ее коэффициент и вернем рассчитанные часы
        if (studentsFullTime > 0) return EplTimeRuleDao.getResult(0.05 * hours, description);
        // если у нас нет студентов очной формы, но есть очно-заочной - применим ее коэффициент и вернем рассчитанные часы
        else if (studentsPartTime > 0) return EplTimeRuleDao.getResult(0.1 * hours, description);
        // здесь возможен только вариант группы заочной формы - применяем ее коэффициент
        return EplTimeRuleDao.getResult(0.15 * hours, description);
    }
}