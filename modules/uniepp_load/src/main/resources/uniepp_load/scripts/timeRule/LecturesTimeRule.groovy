/* $Id: $ */
package uniepp_load.scripts.timeRule

import ru.tandemservice.uniepp.entity.catalog.EppALoadType
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeALTCodes
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.EplTimeRuleDao
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleDao
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleScript

/**
 * 1 час за 1 акад. час в потоке до 100 чел. В потоке студентов: от 100 до 149 чел.: 1,2ч/ак.ч.; от 150 до 199 чел.: 1,3ч/ак.ч.; от 200 чел.: 1,5ч/ак.ч.
 */
return new IEplTimeRuleScript() {

    @Override
    public IEplTimeRuleDao.IRuleResult count(IEplTimeRuleDao.ILoadData loadData)
    {
        if (!EppGroupTypeALTCodes.TYPE_LECTURES.equals(loadData.eduGroup().getGroupType().getCode()))
            return null;
        if (!EppRegistryStructureCodes.REGISTRY_DISCIPLINE.equals(loadData.eduGroup().getRegistryElementPart().getRegistryElement().getParent().getCode()))
            return null;
        Integer students = 0;
        for (Integer amount : loadData.groupContent().values())
            students += amount;
        String description = "План. поток: "+ loadData.eduGroup().getTitle() + ", дисциплина: " + loadData.eduGroup().getRegistryElementPart().getTitleWithNumber() + ", студенты: " + students;
        double hours = loadData.discData().getLoadAsDouble(EppALoadType.FULL_CODE_TOTAL_LECTURES);
        if (hours <= 0) return null;
        if (students < 100)
            return EplTimeRuleDao.getResult(hours, description);
        else if (students < 150)
            return EplTimeRuleDao.getResult(1.2 * hours, description);
        else if (students < 200)
            return EplTimeRuleDao.getResult(1.3 * hours, description);
        else
            return EplTimeRuleDao.getResult(1.5 * hours, description);
    }
}