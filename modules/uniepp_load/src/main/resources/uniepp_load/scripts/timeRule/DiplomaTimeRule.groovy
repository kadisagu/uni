/* $Id: $ */
package uniepp_load.scripts.timeRule

import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes
import ru.tandemservice.uniepp.entity.catalog.EppALoadType
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.EplTimeRuleDao
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleDao
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleScript

/**
 * Процент от общего числа лекционных часов по каждой дисциплине на поток: 5% - по очной форме обучения; 10% - по очно-заочной форме обучения; 15% - по заочной форме обучения.
 */
return new IEplTimeRuleScript() {

    @Override
    public IEplTimeRuleDao.IRuleResult count(IEplTimeRuleDao.ILoadData loadData)
    {
        if (!EppRegistryStructureCodes.REGISTRY_ATTESTATION_DIPLOMA.equals(loadData.eduGroup().getRegistryElementPart().getRegistryElement().getParent().getCode()))
            return null;
        Integer studentsBach = 0;
        Integer studentsSpec = 0;
        Integer studentsMaster = 0;
        for (Map.Entry<EplStudent, Integer> e : loadData.groupContent().entrySet()) {
            def programKind = e.getKey().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject()?.getEduProgramKind()?.getCode()
            if (EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA.equals(programKind)) studentsBach = studentsBach + e.getValue();
            else if (EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV.equals(programKind)) studentsSpec = studentsSpec + e.getValue();
            else if (EduProgramKindCodes.PROGRAMMA_MAGISTRATURY.equals(programKind)) studentsMaster = studentsMaster + e.getValue();
        }
        if (0 == studentsSpec + studentsBach + studentsMaster) return null;

        String description = "План. поток: "+ loadData.eduGroup().getTitle() + ", дисциплина: " + loadData.eduGroup().getRegistryElementPart().getTitleWithNumber();
        return EplTimeRuleDao.getResult(30 * studentsBach + 35 * studentsSpec + 43 * studentsMaster, description);
    }
}