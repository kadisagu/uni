/* $Id: $ */
package uniepp_load.scripts.timeRule

import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.EplTimeRuleDao
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleDao
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleScript

/**
 * 0,33 часа на одного студента + 1 час на проведение зачета на одну группу
 */
return new IEplTimeRuleScript() {

    @Override
    public IEplTimeRuleDao.IRuleResult count(IEplTimeRuleDao.ILoadData loadData)
    {
        // проверяем, что это УГС на зачет
        if (!EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF.equals(loadData.eduGroup().getGroupType().getCode()))
            return null;
        // только для дисциплин, практики и ИГА пропускаем
        if (!EppRegistryStructureCodes.REGISTRY_DISCIPLINE.equals(loadData.eduGroup().getRegistryElementPart().getRegistryElement().getParent().getCode()))
            return null;

        // рассчитаем число студентов
        Integer students = 0;
        for (Integer amount : loadData.groupContent().values())
            students += amount;

        // рассчитаем число часов нагрузки
        double hours = 0.33*students + 1;

        // сформируем описание
        String description = "План. поток: "+ loadData.eduGroup().getTitle() + ", дисциплина: " + loadData.eduGroup().getRegistryElementPart().getTitleWithNumber() + ", студенты: " + students;

        // возвращаем результат расчета и описание
        return EplTimeRuleDao.getResult(hours, description);
    }
}