/* $Id: $ */
package uniepp_load.scripts.timeRule

import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes
import ru.tandemservice.uniepp.entity.catalog.EppLoadType
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.EplTimeRuleDao
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleDao
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleScript

/**
 * Руководство всеми видами практики студентов очной формы обучения: 1 час * количество недель * количество студентов-очников
 */
return new IEplTimeRuleScript() {

    def PRACTICE_CODES = [EppRegistryStructureCodes.REGISTRY_PRACTICE, EppRegistryStructureCodes.REGISTRY_PRACTICE_PRE_DIPLOMA, EppRegistryStructureCodes.REGISTRY_PRACTICE_PRODUCTION, EppRegistryStructureCodes.REGISTRY_PRACTICE_TUTORING, EppRegistryStructureCodes.REGISTRY_PRACTICE_PRE_OTHER];

    @Override
    public IEplTimeRuleDao.IRuleResult count(IEplTimeRuleDao.ILoadData loadData)
    {
        // будем добавлять в любой План. поток по итоговому контролю
        if (!EppGroupTypeFCACodes.CODES.contains(loadData.eduGroup().getGroupType().getCode())) {
            return null;
        }
        // только практики
        if (!PRACTICE_CODES.contains(loadData.eduGroup().getRegistryElementPart().getRegistryElement().getParent().getCode())) {
            return null;
        }

        // посчитаем число студентов очной формы
        Integer students = 0;
        for (Map.Entry<EplStudent, Integer> e : loadData.groupContent().entrySet()) {
            EplStudent student = e.getKey()
            // только очная форма
            if (!DevelopFormCodes.FULL_TIME_FORM.equals(student.getEducationOrgUnit().getDevelopForm().getCode())) continue;
            students = students + e.getValue();
        }

        // получим число недель практики
        double weeks = loadData.discData().getLoadAsDouble(EppLoadType.FULL_CODE_WEEKS);

        // посчитаем часы, если ноль - не сохраняем их
        double hours = weeks * students;
        if (hours <= 0) return null;

        // возвращаем результат
        String description = "План. поток: "+ loadData.eduGroup().getTitle() + ", дисциплина: " + loadData.eduGroup().getRegistryElementPart().getTitleWithNumber() + ", студенты: " + students;
        return EplTimeRuleDao.getResult(hours, description);
    }
}