/* $Id$ */
package ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author nvankov
 * @since 1/28/15
 */
public interface IEplPlannedPpsDao extends INeedPersistenceSupport
{
    /**
     * Массовое добавление план. ППС с типом "Штат"
     * @param ouSummary расчет
     */
    void doAddAllPlanedPps(EplOrgUnitSummary ouSummary);

    /**
     * Массовое добавление план. ППС с типом "Почасовик"
     * @param ouSummary расчет
     */
    void doAddAllTimeWorker(EplOrgUnitSummary ouSummary);

    boolean existPersonPpsForOU(Person person, OrgUnit orgUnit);

    double getSummaryHours(Long orgUnitSummaryId);

    double getDistributionHours(Long orgUnitSummaryId);

    void doRefreshDistribution(Long orgUnitSummaryId);

    double getSummaryDistribHours(Long orgUnitSummaryId);

    interface ITimeData {
        EplTimeItem timeItem();
        EplPlannedPps pps();
        boolean overtime();
        long timeAmountAsLong();
    }

    void doUpdateTimeDistribution(EplOrgUnitSummary summary, List<ITimeData> timeMap);

    /**
     * Проверка, подключен ли М Почасовики
     * @return true - подключен, иначе - false
     */
    boolean isTimeWorkerExists();

    /**
     * Сохраняет планируемого ППС
     * @param pps план. ППС
     */
    void savePlannedPps(@NotNull EplPlannedPps pps);

    /**
     * Проверяет, есть ли в расчете ставки сотрудников, не кратные настройке «Шаг ставки» (М Кадры)
     * @param summary расчет
     * @return
     */
    Double hasWrongStaffRates(EplOrgUnitSummary summary);

    int getEmployeePost4PpsEntryCount(Long ppsEntryId);

    int getEplPlannedPpsCount(Long summaryId);
}
