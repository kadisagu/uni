package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_load_2x7x1_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eplPlannedPps

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epl_pl_pps_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("orgunitsummary_id", DBType.LONG).setNullable(false), 
				new DBColumn("person_id", DBType.LONG), 
				new DBColumn("vacancy_p", DBType.createVarchar(255)), 
				new DBColumn("post_id", DBType.LONG), 
				new DBColumn("staffrateinteger_p", DBType.LONG)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eplPlannedPps");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eplPlannedPpsTimeItem

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epl_pl_pps_time_item_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("pps_id", DBType.LONG).setNullable(false), 
				new DBColumn("timeitem_id", DBType.LONG).setNullable(false), 
				new DBColumn("timeamount_p", DBType.DOUBLE).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eplPlannedPpsTimeItem");

		}


    }
}