package ru.tandemservice.uniepp_load.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Норма времени учебной нагрузки (скрипт)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplTimeRuleEduGroupScriptGen extends EplTimeRuleEduLoad
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript";
    public static final String ENTITY_NAME = "eplTimeRuleEduGroupScript";
    public static final int VERSION_HASH = 2064978367;
    private static IEntityMeta ENTITY_META;

    public static final String P_USER_SCRIPT = "userScript";
    public static final String P_DEFAULT_SCRIPT_PATH = "defaultScriptPath";
    public static final String L_GROUP_TYPE = "groupType";
    public static final String L_REG_STRUCTURE = "regStructure";
    public static final String P_COUNTER_ENABLED = "counterEnabled";
    public static final String P_TOTAL_TIME_WORK = "totalTimeWork";
    public static final String P_REQUEST_COUNT = "requestCount";
    public static final String P_AVG_TIME_WORK = "avgTimeWork";
    public static final String P_HAS_NO_DEFAULT_SCRIPT = "hasNoDefaultScript";
    public static final String P_HAS_USER_SCRIPT = "hasUserScript";

    private String _userScript;     // Скрипт расчета
    private String _defaultScriptPath;     // Путь в classpath до скрипта по умолчанию
    private EppGroupType _groupType;     // Ограничить видом УГС
    private EppRegistryStructure _regStructure;     // Ограничить видом элемента реестра
    private boolean _counterEnabled;     // Счетчик производительности включен
    private long _totalTimeWork;     // Суммарное время работы (мкс)
    private int _requestCount;     // Число вызовов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Скрипт расчета.
     */
    public String getUserScript()
    {
        return _userScript;
    }

    /**
     * @param userScript Скрипт расчета.
     */
    public void setUserScript(String userScript)
    {
        dirty(_userScript, userScript);
        _userScript = userScript;
    }

    /**
     * @return Путь в classpath до скрипта по умолчанию.
     */
    @Length(max=255)
    public String getDefaultScriptPath()
    {
        return _defaultScriptPath;
    }

    /**
     * @param defaultScriptPath Путь в classpath до скрипта по умолчанию.
     */
    public void setDefaultScriptPath(String defaultScriptPath)
    {
        dirty(_defaultScriptPath, defaultScriptPath);
        _defaultScriptPath = defaultScriptPath;
    }

    /**
     * @return Ограничить видом УГС.
     */
    public EppGroupType getGroupType()
    {
        return _groupType;
    }

    /**
     * @param groupType Ограничить видом УГС.
     */
    public void setGroupType(EppGroupType groupType)
    {
        dirty(_groupType, groupType);
        _groupType = groupType;
    }

    /**
     * @return Ограничить видом элемента реестра.
     */
    public EppRegistryStructure getRegStructure()
    {
        return _regStructure;
    }

    /**
     * @param regStructure Ограничить видом элемента реестра.
     */
    public void setRegStructure(EppRegistryStructure regStructure)
    {
        dirty(_regStructure, regStructure);
        _regStructure = regStructure;
    }

    /**
     * @return Счетчик производительности включен. Свойство не может быть null.
     */
    @NotNull
    public boolean isCounterEnabled()
    {
        return _counterEnabled;
    }

    /**
     * @param counterEnabled Счетчик производительности включен. Свойство не может быть null.
     */
    public void setCounterEnabled(boolean counterEnabled)
    {
        dirty(_counterEnabled, counterEnabled);
        _counterEnabled = counterEnabled;
    }

    /**
     * @return Суммарное время работы (мкс). Свойство не может быть null.
     */
    @NotNull
    public long getTotalTimeWork()
    {
        return _totalTimeWork;
    }

    /**
     * @param totalTimeWork Суммарное время работы (мкс). Свойство не может быть null.
     */
    public void setTotalTimeWork(long totalTimeWork)
    {
        dirty(_totalTimeWork, totalTimeWork);
        _totalTimeWork = totalTimeWork;
    }

    /**
     * @return Число вызовов. Свойство не может быть null.
     */
    @NotNull
    public int getRequestCount()
    {
        return _requestCount;
    }

    /**
     * @param requestCount Число вызовов. Свойство не может быть null.
     */
    public void setRequestCount(int requestCount)
    {
        dirty(_requestCount, requestCount);
        _requestCount = requestCount;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EplTimeRuleEduGroupScriptGen)
        {
            setUserScript(((EplTimeRuleEduGroupScript)another).getUserScript());
            setDefaultScriptPath(((EplTimeRuleEduGroupScript)another).getDefaultScriptPath());
            setGroupType(((EplTimeRuleEduGroupScript)another).getGroupType());
            setRegStructure(((EplTimeRuleEduGroupScript)another).getRegStructure());
            setCounterEnabled(((EplTimeRuleEduGroupScript)another).isCounterEnabled());
            setTotalTimeWork(((EplTimeRuleEduGroupScript)another).getTotalTimeWork());
            setRequestCount(((EplTimeRuleEduGroupScript)another).getRequestCount());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplTimeRuleEduGroupScriptGen> extends EplTimeRuleEduLoad.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplTimeRuleEduGroupScript.class;
        }

        public T newInstance()
        {
            return (T) new EplTimeRuleEduGroupScript();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "userScript":
                    return obj.getUserScript();
                case "defaultScriptPath":
                    return obj.getDefaultScriptPath();
                case "groupType":
                    return obj.getGroupType();
                case "regStructure":
                    return obj.getRegStructure();
                case "counterEnabled":
                    return obj.isCounterEnabled();
                case "totalTimeWork":
                    return obj.getTotalTimeWork();
                case "requestCount":
                    return obj.getRequestCount();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "userScript":
                    obj.setUserScript((String) value);
                    return;
                case "defaultScriptPath":
                    obj.setDefaultScriptPath((String) value);
                    return;
                case "groupType":
                    obj.setGroupType((EppGroupType) value);
                    return;
                case "regStructure":
                    obj.setRegStructure((EppRegistryStructure) value);
                    return;
                case "counterEnabled":
                    obj.setCounterEnabled((Boolean) value);
                    return;
                case "totalTimeWork":
                    obj.setTotalTimeWork((Long) value);
                    return;
                case "requestCount":
                    obj.setRequestCount((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "userScript":
                        return true;
                case "defaultScriptPath":
                        return true;
                case "groupType":
                        return true;
                case "regStructure":
                        return true;
                case "counterEnabled":
                        return true;
                case "totalTimeWork":
                        return true;
                case "requestCount":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "userScript":
                    return true;
                case "defaultScriptPath":
                    return true;
                case "groupType":
                    return true;
                case "regStructure":
                    return true;
                case "counterEnabled":
                    return true;
                case "totalTimeWork":
                    return true;
                case "requestCount":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "userScript":
                    return String.class;
                case "defaultScriptPath":
                    return String.class;
                case "groupType":
                    return EppGroupType.class;
                case "regStructure":
                    return EppRegistryStructure.class;
                case "counterEnabled":
                    return Boolean.class;
                case "totalTimeWork":
                    return Long.class;
                case "requestCount":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplTimeRuleEduGroupScript> _dslPath = new Path<EplTimeRuleEduGroupScript>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplTimeRuleEduGroupScript");
    }
            

    /**
     * @return Скрипт расчета.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#getUserScript()
     */
    public static PropertyPath<String> userScript()
    {
        return _dslPath.userScript();
    }

    /**
     * @return Путь в classpath до скрипта по умолчанию.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#getDefaultScriptPath()
     */
    public static PropertyPath<String> defaultScriptPath()
    {
        return _dslPath.defaultScriptPath();
    }

    /**
     * @return Ограничить видом УГС.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#getGroupType()
     */
    public static EppGroupType.Path<EppGroupType> groupType()
    {
        return _dslPath.groupType();
    }

    /**
     * @return Ограничить видом элемента реестра.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#getRegStructure()
     */
    public static EppRegistryStructure.Path<EppRegistryStructure> regStructure()
    {
        return _dslPath.regStructure();
    }

    /**
     * @return Счетчик производительности включен. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#isCounterEnabled()
     */
    public static PropertyPath<Boolean> counterEnabled()
    {
        return _dslPath.counterEnabled();
    }

    /**
     * @return Суммарное время работы (мкс). Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#getTotalTimeWork()
     */
    public static PropertyPath<Long> totalTimeWork()
    {
        return _dslPath.totalTimeWork();
    }

    /**
     * @return Число вызовов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#getRequestCount()
     */
    public static PropertyPath<Integer> requestCount()
    {
        return _dslPath.requestCount();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#getAvgTimeWork()
     */
    public static SupportedPropertyPath<Double> avgTimeWork()
    {
        return _dslPath.avgTimeWork();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#isHasNoDefaultScript()
     */
    public static SupportedPropertyPath<Boolean> hasNoDefaultScript()
    {
        return _dslPath.hasNoDefaultScript();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#isHasUserScript()
     */
    public static SupportedPropertyPath<Boolean> hasUserScript()
    {
        return _dslPath.hasUserScript();
    }

    public static class Path<E extends EplTimeRuleEduGroupScript> extends EplTimeRuleEduLoad.Path<E>
    {
        private PropertyPath<String> _userScript;
        private PropertyPath<String> _defaultScriptPath;
        private EppGroupType.Path<EppGroupType> _groupType;
        private EppRegistryStructure.Path<EppRegistryStructure> _regStructure;
        private PropertyPath<Boolean> _counterEnabled;
        private PropertyPath<Long> _totalTimeWork;
        private PropertyPath<Integer> _requestCount;
        private SupportedPropertyPath<Double> _avgTimeWork;
        private SupportedPropertyPath<Boolean> _hasNoDefaultScript;
        private SupportedPropertyPath<Boolean> _hasUserScript;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Скрипт расчета.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#getUserScript()
     */
        public PropertyPath<String> userScript()
        {
            if(_userScript == null )
                _userScript = new PropertyPath<String>(EplTimeRuleEduGroupScriptGen.P_USER_SCRIPT, this);
            return _userScript;
        }

    /**
     * @return Путь в classpath до скрипта по умолчанию.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#getDefaultScriptPath()
     */
        public PropertyPath<String> defaultScriptPath()
        {
            if(_defaultScriptPath == null )
                _defaultScriptPath = new PropertyPath<String>(EplTimeRuleEduGroupScriptGen.P_DEFAULT_SCRIPT_PATH, this);
            return _defaultScriptPath;
        }

    /**
     * @return Ограничить видом УГС.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#getGroupType()
     */
        public EppGroupType.Path<EppGroupType> groupType()
        {
            if(_groupType == null )
                _groupType = new EppGroupType.Path<EppGroupType>(L_GROUP_TYPE, this);
            return _groupType;
        }

    /**
     * @return Ограничить видом элемента реестра.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#getRegStructure()
     */
        public EppRegistryStructure.Path<EppRegistryStructure> regStructure()
        {
            if(_regStructure == null )
                _regStructure = new EppRegistryStructure.Path<EppRegistryStructure>(L_REG_STRUCTURE, this);
            return _regStructure;
        }

    /**
     * @return Счетчик производительности включен. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#isCounterEnabled()
     */
        public PropertyPath<Boolean> counterEnabled()
        {
            if(_counterEnabled == null )
                _counterEnabled = new PropertyPath<Boolean>(EplTimeRuleEduGroupScriptGen.P_COUNTER_ENABLED, this);
            return _counterEnabled;
        }

    /**
     * @return Суммарное время работы (мкс). Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#getTotalTimeWork()
     */
        public PropertyPath<Long> totalTimeWork()
        {
            if(_totalTimeWork == null )
                _totalTimeWork = new PropertyPath<Long>(EplTimeRuleEduGroupScriptGen.P_TOTAL_TIME_WORK, this);
            return _totalTimeWork;
        }

    /**
     * @return Число вызовов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#getRequestCount()
     */
        public PropertyPath<Integer> requestCount()
        {
            if(_requestCount == null )
                _requestCount = new PropertyPath<Integer>(EplTimeRuleEduGroupScriptGen.P_REQUEST_COUNT, this);
            return _requestCount;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#getAvgTimeWork()
     */
        public SupportedPropertyPath<Double> avgTimeWork()
        {
            if(_avgTimeWork == null )
                _avgTimeWork = new SupportedPropertyPath<Double>(EplTimeRuleEduGroupScriptGen.P_AVG_TIME_WORK, this);
            return _avgTimeWork;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#isHasNoDefaultScript()
     */
        public SupportedPropertyPath<Boolean> hasNoDefaultScript()
        {
            if(_hasNoDefaultScript == null )
                _hasNoDefaultScript = new SupportedPropertyPath<Boolean>(EplTimeRuleEduGroupScriptGen.P_HAS_NO_DEFAULT_SCRIPT, this);
            return _hasNoDefaultScript;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript#isHasUserScript()
     */
        public SupportedPropertyPath<Boolean> hasUserScript()
        {
            if(_hasUserScript == null )
                _hasUserScript = new SupportedPropertyPath<Boolean>(EplTimeRuleEduGroupScriptGen.P_HAS_USER_SCRIPT, this);
            return _hasUserScript;
        }

        public Class getEntityClass()
        {
            return EplTimeRuleEduGroupScript.class;
        }

        public String getEntityName()
        {
            return "eplTimeRuleEduGroupScript";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract double getAvgTimeWork();

    public abstract boolean isHasNoDefaultScript();

    public abstract boolean isHasUserScript();
}
