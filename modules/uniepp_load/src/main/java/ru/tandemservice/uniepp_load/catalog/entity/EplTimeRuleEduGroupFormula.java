package ru.tandemservice.uniepp_load.catalog.entity;

import com.google.common.collect.ImmutableSet;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.ui.EduGroupFormulaAddEdit.EplTimeRuleEduGroupFormulaAddEdit;
import ru.tandemservice.uniepp_load.catalog.entity.gen.EplTimeRuleEduGroupFormulaGen;

import java.util.Collection;
import java.util.Set;

/** @see ru.tandemservice.uniepp_load.catalog.entity.gen.EplTimeRuleEduGroupFormulaGen */
public class EplTimeRuleEduGroupFormula extends EplTimeRuleEduGroupFormulaGen
{
    private static Set<String> HIDDEN_PROPERTIES = ImmutableSet.of(L_GROUPING, L_EPP_REGISTRY_STRUCTURE);

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public String getAddEditComponentName() { return EplTimeRuleEduGroupFormulaAddEdit.class.getSimpleName(); }
            @Override public Collection<String> getHiddenFields() { return HIDDEN_PROPERTIES; }
            @Override public Collection<String> getHiddenFieldsItemPub() { return ImmutableSet.of(); }
        };
    }
}