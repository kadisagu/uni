package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_load_2x10x6_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eplSimpleTimeItem

		// создано свойство yearPart
		{
			// создать колонку
			tool.createColumn("epl_simple_time_item_t", new DBColumn("yearpart_id", DBType.LONG));
		}

        // удаляем строки План. потока, для которых не сходится вид УГС
        String subQuery = "select r.id from epl_edu_group_row_t r " +
                "inner join epl_edu_group_t g on r.group_id = g.id " +
                "inner join epl_student_wp2gt_slot_t slot on r.studentWP2GTypeSlot_id = slot.id " +
                "where g.grouptype_id <> slot.grouptype_id";

        tool.executeUpdate("delete from epl_edu_group_row_t where id in (" + subQuery + ")");
    }
}