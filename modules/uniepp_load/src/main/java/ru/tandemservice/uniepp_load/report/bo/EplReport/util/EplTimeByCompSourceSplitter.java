/* $Id:$ */
package ru.tandemservice.uniepp_load.report.bo.EplReport.util;

import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplCompensationSourceCodes;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLFunctions.sum;

/**
 * @author oleyba
 * @since 2/17/15
 */
public class EplTimeByCompSourceSplitter extends EplTimeSplitter
{
    public EplTimeByCompSourceSplitter(Long summaryId)
    {
        super(summaryId);
    }

    protected Collection getPropertyValueList()
    {
        return EplCompensationSourceCodes.CODES;
    }

    protected String getPropertyPath()
    {
        return EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().compensationSource().code().s();
    }
}
