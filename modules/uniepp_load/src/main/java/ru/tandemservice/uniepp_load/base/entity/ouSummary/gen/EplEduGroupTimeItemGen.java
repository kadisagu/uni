package ru.tandemservice.uniepp_load.base.entity.ouSummary.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Часы нагрузки для план. потока
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplEduGroupTimeItemGen extends EplTimeItem
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem";
    public static final String ENTITY_NAME = "eplEduGroupTimeItem";
    public static final int VERSION_HASH = 481260107;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_GROUP = "eduGroup";
    public static final String L_REGISTRY_ELEMENT_PART = "registryElementPart";

    private EplEduGroup _eduGroup;     // План. поток
    private EppRegistryElementPart _registryElementPart;     // Дисциплиночасть

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return План. поток.
     */
    public EplEduGroup getEduGroup()
    {
        return _eduGroup;
    }

    /**
     * @param eduGroup План. поток.
     */
    public void setEduGroup(EplEduGroup eduGroup)
    {
        dirty(_eduGroup, eduGroup);
        _eduGroup = eduGroup;
    }

    /**
     * @return Дисциплиночасть. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPart getRegistryElementPart()
    {
        return _registryElementPart;
    }

    /**
     * @param registryElementPart Дисциплиночасть. Свойство не может быть null.
     */
    public void setRegistryElementPart(EppRegistryElementPart registryElementPart)
    {
        dirty(_registryElementPart, registryElementPart);
        _registryElementPart = registryElementPart;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EplEduGroupTimeItemGen)
        {
            setEduGroup(((EplEduGroupTimeItem)another).getEduGroup());
            setRegistryElementPart(((EplEduGroupTimeItem)another).getRegistryElementPart());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplEduGroupTimeItemGen> extends EplTimeItem.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplEduGroupTimeItem.class;
        }

        public T newInstance()
        {
            return (T) new EplEduGroupTimeItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "eduGroup":
                    return obj.getEduGroup();
                case "registryElementPart":
                    return obj.getRegistryElementPart();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "eduGroup":
                    obj.setEduGroup((EplEduGroup) value);
                    return;
                case "registryElementPart":
                    obj.setRegistryElementPart((EppRegistryElementPart) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eduGroup":
                        return true;
                case "registryElementPart":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eduGroup":
                    return true;
                case "registryElementPart":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "eduGroup":
                    return EplEduGroup.class;
                case "registryElementPart":
                    return EppRegistryElementPart.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplEduGroupTimeItem> _dslPath = new Path<EplEduGroupTimeItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplEduGroupTimeItem");
    }
            

    /**
     * @return План. поток.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem#getEduGroup()
     */
    public static EplEduGroup.Path<EplEduGroup> eduGroup()
    {
        return _dslPath.eduGroup();
    }

    /**
     * @return Дисциплиночасть. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem#getRegistryElementPart()
     */
    public static EppRegistryElementPart.Path<EppRegistryElementPart> registryElementPart()
    {
        return _dslPath.registryElementPart();
    }

    public static class Path<E extends EplEduGroupTimeItem> extends EplTimeItem.Path<E>
    {
        private EplEduGroup.Path<EplEduGroup> _eduGroup;
        private EppRegistryElementPart.Path<EppRegistryElementPart> _registryElementPart;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return План. поток.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem#getEduGroup()
     */
        public EplEduGroup.Path<EplEduGroup> eduGroup()
        {
            if(_eduGroup == null )
                _eduGroup = new EplEduGroup.Path<EplEduGroup>(L_EDU_GROUP, this);
            return _eduGroup;
        }

    /**
     * @return Дисциплиночасть. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem#getRegistryElementPart()
     */
        public EppRegistryElementPart.Path<EppRegistryElementPart> registryElementPart()
        {
            if(_registryElementPart == null )
                _registryElementPart = new EppRegistryElementPart.Path<EppRegistryElementPart>(L_REGISTRY_ELEMENT_PART, this);
            return _registryElementPart;
        }

        public Class getEntityClass()
        {
            return EplEduGroupTimeItem.class;
        }

        public String getEntityName()
        {
            return "eplEduGroupTimeItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
