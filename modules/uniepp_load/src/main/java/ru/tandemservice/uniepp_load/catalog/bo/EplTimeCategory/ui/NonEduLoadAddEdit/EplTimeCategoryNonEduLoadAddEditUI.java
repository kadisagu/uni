/* $Id$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeCategory.ui.NonEduLoadAddEdit;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemAddEditUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryNonEduLoad;

import java.util.Set;

/**
 * @author Alexey Lopatin
 * @since 22.07.2016
 */
@Input({
        @Bind(key = "catalogItemId", binding = "holder.id"),
        @Bind(key = DefaultCatalogAddEditModel.CATALOG_CODE, binding = "catalogCode")
})
public class EplTimeCategoryNonEduLoadAddEditUI extends BaseCatalogItemAddEditUI<EplTimeCategoryNonEduLoad>
{
    @Override
    public void onClickApply()
    {
        if (isAddForm())
            getCatalogItem().setUserCode(null);
        super.onClickApply();
    }

    @Override
    public Set<String> getDisabledProperties()
    {
        if (!isAddForm())
        {
            Set<String> properties = super.getDisabledProperties();
            properties.add(ICatalogItem.CATALOG_ITEM_TITLE);
            return properties;
        }
        else
            return super.getDisabledProperties();
    }
}
