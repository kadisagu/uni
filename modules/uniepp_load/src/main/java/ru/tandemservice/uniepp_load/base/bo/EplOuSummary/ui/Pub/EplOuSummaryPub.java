/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.LogTab.EplOuSummaryLogTab;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.MaxLoadTab.EplOuSummaryMaxLoadTab;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.Tab.EplOuSummaryTab;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.DistributionTab.EplPlannedPpsDistributionTab;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.OuSummaryTab.EplPlannedPpsOuSummaryTab;

/**
 * @author oleyba
 * @since 5/5/12
 */
@Configuration
public class EplOuSummaryPub extends BusinessComponentManager
{
    @Bean
    public TabPanelExtPoint eplOuSummaryPubTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder("eplOuSummaryPubTabPanel")
            .addTab(componentTab("eplOuSummaryPubTab", EplOuSummaryTab.class).permissionKey("eplOrgUnitSummaryPubView"))
            .addTab(componentTab("ppsTab", EplPlannedPpsOuSummaryTab.class).permissionKey("eplOuSummaryPpsTabView"))
            .addTab(componentTab("ppsDistributionTab", EplPlannedPpsDistributionTab.class).permissionKey("eplPlannedPpsDistributionTabView"))
            .addTab(componentTab("maxLoadTab", EplOuSummaryMaxLoadTab.class).permissionKey("eplOuSummaryMaxLoadTabView"))
            .addTab(componentTab("logTab", EplOuSummaryLogTab.class).permissionKey("eplOuSummaryLogTabView"))
            .create();
    }

    // disciplineTimeDS
}
