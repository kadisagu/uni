/**
 *$Id: OrgSystemActionPubAddon.java 4443 2014-05-21 08:10:28Z nvankov $
 */
package ru.tandemservice.uniepp_load.base.ext.DataCorrection.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.uniedu.program.bo.EduProgram.ui.ChangeParams.EduProgramChangeParams;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Merge.EplStudentSummaryMerge;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
public class EplDataCorrectionPubAddon extends UIAddon
{
    public EplDataCorrectionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickEplSummaryMerge()
    {
        getActivationBuilder().asCurrent(EplStudentSummaryMerge.class).activate();
    }
}
