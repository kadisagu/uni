package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_load_2x9x3_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eplOrgUnitSummary

		// создано обязательное свойство state
		{
			// создать колонку
			tool.createColumn("epl_ou_summary_t", new DBColumn("state_id", DBType.LONG));

			// задать значение по умолчанию
            Number defaultState = (Number) tool.getUniqueResult("select id from epp_c_state_t where code_p = ?", "1");
			tool.executeUpdate("update epl_ou_summary_t set state_id=? where state_id is null", defaultState);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epl_ou_summary_t", "state_id", false);
		}

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eplStudentSummaryState

        // создано обязательное свойство permissionSuffix
        {
            // создать колонку
            tool.createColumn("epl_student_summary_state_t", new DBColumn("permissionsuffix_p", DBType.createVarchar(255)));

            // задать значение по умолчанию
            String[] permissionSuffixes = {"contingentPlanning", "attachWp", "planningFlow", "timeItem", "accept"};
            final MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update epl_student_summary_state_t set permissionsuffix_p = ? where code_p = ?", DBType.createVarchar(255), DBType.createVarchar(255));

            for (int i = 0; i < 5; i++)
            {
                String permissionSuffix = permissionSuffixes[i];
                updater.addBatch(permissionSuffix, String.valueOf(i + 1));
            }
            updater.executeUpdate(tool);

            // сделать колонку NOT NULL
            tool.setColumnNullable("epl_student_summary_state_t", "permissionsuffix_p", false);
        }
    }
}