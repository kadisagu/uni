package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_load_2x7x1_11to12 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (tool.columnExists("epl_ou_summary_t", "ratehours_p")) return;

		////////////////////////////////////////////////////////////////////////////////
		// сущность eplOrgUnitSummary

		// создано обязательное свойство rateHours
        if(!tool.columnExists("epl_ou_summary_t", "ratehours_p"))
		{
			// создать колонку
			tool.createColumn("epl_ou_summary_t", new DBColumn("ratehours_p", DBType.INTEGER));

			// задать значение по умолчанию
            tool.executeUpdate("update epl_ou_summary_t set ratehours_p=? where ratehours_p is null", (Integer) 800);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epl_ou_summary_t", "ratehours_p", false);

		}

		// создано обязательное свойство hourlyFund
        if(!tool.columnExists("epl_ou_summary_t", "hourlyfund_p"))
		{
			// создать колонку
			tool.createColumn("epl_ou_summary_t", new DBColumn("hourlyfund_p", DBType.INTEGER));

			// задать значение по умолчанию
            tool.executeUpdate("update epl_ou_summary_t set hourlyfund_p=? where hourlyfund_p is null", (Integer) 0);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epl_ou_summary_t", "hourlyfund_p", false);
		}


    }
}