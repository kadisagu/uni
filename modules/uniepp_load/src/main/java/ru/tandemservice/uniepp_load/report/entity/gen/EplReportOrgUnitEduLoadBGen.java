package ru.tandemservice.uniepp_load.report.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduLoadB;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Учебная нагрузка читающего подразделения (форма B)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplReportOrgUnitEduLoadBGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduLoadB";
    public static final String ENTITY_NAME = "eplReportOrgUnitEduLoadB";
    public static final int VERSION_HASH = 1800599553;
    private static IEntityMeta ENTITY_META;

    public static final String P_STUDENT_SUMMARY = "studentSummary";
    public static final String P_ORG_UNIT = "orgUnit";
    public static final String L_EDU_YEAR = "eduYear";

    private String _studentSummary;     // Сводка контингента
    private String _orgUnit;     // Подразделение
    private EducationYear _eduYear;     // Учебный год

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сводка контингента. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStudentSummary()
    {
        return _studentSummary;
    }

    /**
     * @param studentSummary Сводка контингента. Свойство не может быть null.
     */
    public void setStudentSummary(String studentSummary)
    {
        dirty(_studentSummary, studentSummary);
        _studentSummary = studentSummary;
    }

    /**
     * @return Подразделение.
     */
    @Length(max=255)
    public String getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(String orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Учебный год.
     */
    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    /**
     * @param eduYear Учебный год.
     */
    public void setEduYear(EducationYear eduYear)
    {
        dirty(_eduYear, eduYear);
        _eduYear = eduYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EplReportOrgUnitEduLoadBGen)
        {
            setStudentSummary(((EplReportOrgUnitEduLoadB)another).getStudentSummary());
            setOrgUnit(((EplReportOrgUnitEduLoadB)another).getOrgUnit());
            setEduYear(((EplReportOrgUnitEduLoadB)another).getEduYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplReportOrgUnitEduLoadBGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplReportOrgUnitEduLoadB.class;
        }

        public T newInstance()
        {
            return (T) new EplReportOrgUnitEduLoadB();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "studentSummary":
                    return obj.getStudentSummary();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "eduYear":
                    return obj.getEduYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "studentSummary":
                    obj.setStudentSummary((String) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((String) value);
                    return;
                case "eduYear":
                    obj.setEduYear((EducationYear) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentSummary":
                        return true;
                case "orgUnit":
                        return true;
                case "eduYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentSummary":
                    return true;
                case "orgUnit":
                    return true;
                case "eduYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "studentSummary":
                    return String.class;
                case "orgUnit":
                    return String.class;
                case "eduYear":
                    return EducationYear.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplReportOrgUnitEduLoadB> _dslPath = new Path<EplReportOrgUnitEduLoadB>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplReportOrgUnitEduLoadB");
    }
            

    /**
     * @return Сводка контингента. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduLoadB#getStudentSummary()
     */
    public static PropertyPath<String> studentSummary()
    {
        return _dslPath.studentSummary();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduLoadB#getOrgUnit()
     */
    public static PropertyPath<String> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduLoadB#getEduYear()
     */
    public static EducationYear.Path<EducationYear> eduYear()
    {
        return _dslPath.eduYear();
    }

    public static class Path<E extends EplReportOrgUnitEduLoadB> extends StorableReport.Path<E>
    {
        private PropertyPath<String> _studentSummary;
        private PropertyPath<String> _orgUnit;
        private EducationYear.Path<EducationYear> _eduYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сводка контингента. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduLoadB#getStudentSummary()
     */
        public PropertyPath<String> studentSummary()
        {
            if(_studentSummary == null )
                _studentSummary = new PropertyPath<String>(EplReportOrgUnitEduLoadBGen.P_STUDENT_SUMMARY, this);
            return _studentSummary;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduLoadB#getOrgUnit()
     */
        public PropertyPath<String> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new PropertyPath<String>(EplReportOrgUnitEduLoadBGen.P_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduLoadB#getEduYear()
     */
        public EducationYear.Path<EducationYear> eduYear()
        {
            if(_eduYear == null )
                _eduYear = new EducationYear.Path<EducationYear>(L_EDU_YEAR, this);
            return _eduYear;
        }

        public Class getEntityClass()
        {
            return EplReportOrgUnitEduLoadB.class;
        }

        public String getEntityName()
        {
            return "eplReportOrgUnitEduLoadB";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
