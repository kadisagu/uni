/* $Id:$ */
package ru.tandemservice.uniepp_load.base.ext.GlobalReport.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;


/**
 * @author oleyba
 * @since 12/11/13
 */
public class EnrGlobalReportListAddon extends UIAddon
{
    public static final String NAME = "uniepp_loadGlobalReportListAddon";

    public EnrGlobalReportListAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
}
