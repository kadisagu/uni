/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GlobalList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.daemon.EplStudentSummaryDaemonBean;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Add.EplStudentSummaryAdd;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Copy.EplStudentSummaryCopy;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EditTitle.EplStudentSummaryEditTitle;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplStudentSummaryState;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplStudentSummaryStateCodes;

/**
 * @author oleyba
 * @since 10/6/11
 */
@Input({
        @Bind(key = "createdSummaryTitle", binding = "createdSummaryTitle")
})
public class EplStudentSummaryGlobalListUI extends UIPresenter
{
    public String _createdSummaryTitle;
    private EplStudentSummaryState _state;

    @Override
    public void onComponentRefresh()
    {
        if(!StringUtils.isEmpty(_createdSummaryTitle))
        {
            _uiSupport.info("Добавлена новая сводка контингента «" + _createdSummaryTitle + "».");
            _createdSummaryTitle = null;
        }
        _state = DataAccessServices.dao().getByCode(EplStudentSummaryState.class, EplStudentSummaryStateCodes.CONTINGENT_PLANNING);
    }

    public void onClickAdd() {
        _uiActivation.asRegionDialog(EplStudentSummaryAdd.class).activate();
    }

    public void onClickRefresh()
    {
        EplStudentSummaryDaemonBean.DAEMON.wakeUpDaemon();
    }

    public void onClickChangeArchival()
    {
        EplStudentSummary summary = _uiConfig.getDataSource(EplStudentSummaryGlobalList.EPL_STUDENT_SUMMARY_GLOBAL_DS).getRecordById(getListenerParameterAsLong());
        summary.setArchival(!summary.isArchival());
        DataAccessServices.dao().update(summary);
    }

    public void onClickCopy()
    {
        _uiActivation.asRegionDialog(EplStudentSummaryCopy.class).parameter(PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onClickEditTitle() {
        _uiActivation.asRegionDialog(EplStudentSummaryEditTitle.class).parameter(PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onDeleteEntityFromList()
    {
        EplStudentSummaryManager.instance().dao().doDeleteSummary(getListenerParameterAsLong());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.putAll(getSettings().getAll("f."));
    }

    public boolean isNotArchival()
    {
        EplStudentSummary summary = _uiConfig.getDataSource(EplStudentSummaryGlobalList.EPL_STUDENT_SUMMARY_GLOBAL_DS).getCurrent();
        return !summary.isArchival();
    }

    public boolean isSummaryDeleteDisabled()
    {
        EplStudentSummary summary = _uiConfig.getDataSource(EplStudentSummaryGlobalList.EPL_STUDENT_SUMMARY_GLOBAL_DS).getCurrent();
        return summary.getState().getPriority() > _state.getPriority();
    }

    public String getCreatedSummaryTitle()
    {
        return _createdSummaryTitle;
    }

    public void setCreatedSummaryTitle(String createdSummaryTitle)
    {
        _createdSummaryTitle = createdSummaryTitle;
    }

    public EplStudentSummaryState getState()
    {
        return _state;
    }
}
