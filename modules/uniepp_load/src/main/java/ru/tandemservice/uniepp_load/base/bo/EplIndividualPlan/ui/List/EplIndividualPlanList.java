/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.*;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.logic.EplIndividualPlanDSHandler;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.Pub.EplIndividualPlanPub;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndPlan2OuSummaryRel;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTabUI;
import ru.tandemservice.uniepp_load.report.bo.EplReport.EplReportManager;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.EplIndividualPlanManager.*;

/**
 * @author Alexey Lopatin
 * @since 01.08.2016
 */
@Configuration
public class EplIndividualPlanList extends BusinessComponentManager
{
    public static final String IND_PLAN_DS = "indPlanDS";

    public static final String PARAM_STUDENT_SUMMARY = "studentSummaries";
    public static final String PARAM_EDU_LOAD = "eduLoad";
    public static final String PARAM_NON_EDU_LOAD = "nonEduLoad";
    public static final String PARAM_PUB_PARAMETERS = "pubParameters";

    public static final String PRINT_COLUMN_NAME = "print";
    public static final String PRINT_LISTENER = "onClickPrint";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(EplReportManager.DS_SUMMARY_EPP_YEAR, getName(),
                                             EppYearEducationProcess.defaultSelectDSHandler(getName())
                                                    .customize((alias, dql, context, filter) ->
                                                           dql.where(exists(EplStudentSummary.class, EplStudentSummary.eppYear().s(), property(alias)))
                                                              .where(exists(
                                                                            getIndPlanBuilder(context.get(EplOrgUnitTabUI.BIND_ORG_UNIT_ID), context.get(EplIndividualPlanListUI.PARAM_PPS_PERSON))
                                                                            .where(eq(property(alias, "id"), property("i", EplIndividualPlan.eppYear().id())))
                                                                            .buildQuery())
                                                        )))
                        .valueStyleSource(EducationCatalogsManager.getEduYearValueStyleSource()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_ORG_UNIT, getName(), EplIndividualPlan.defaultSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> dql.where(exists(
                                getIndPlanBuilder(null, context.get(EplIndividualPlanListUI.PARAM_PPS_PERSON))
                                        .where(eq(property(alias, "id"), property("i", EplIndividualPlan.orgUnit().id())))
                                        .buildQuery())
                        ))))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_TEACHER, getName(), PpsEntry.defaultSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> dql.where(exists(
                                getIndPlanBuilder(context.get(EplOrgUnitTabUI.BIND_ORG_UNIT_ID), null)
                                        .where(eq(property(alias, "id"), property("i", EplIndividualPlan.pps().id())))
                                        .buildQuery())
                        )))
                                       .addColumn(PpsEntry.fio().s())
                                       .addColumn(PpsEntry.orgUnit().shortTitle().s())
                                       .addColumn(PpsEntry.titlePostOrTimeWorkerData().s())
                )
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_STUDENT_SUMMARY, getName(), EplStudentSummary.defaultSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> dql.where(exists(
                                getIndPlanBuilder(context.get(EplOrgUnitTabUI.BIND_ORG_UNIT_ID), context.get(EplIndividualPlanListUI.PARAM_PPS_PERSON))
                                        .where(eq(property(alias, "id"), property("rel", EplIndPlan2OuSummaryRel.ouSummary().studentSummary().id())))
                                        .buildQuery())
                        ))))
                .addDataSource(searchListDS(IND_PLAN_DS, indPlanDSColumns(), indPlanDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint indPlanDSColumns()
    {
        return columnListExtPointBuilder(IND_PLAN_DS)
                .addColumn(publisherColumn(EplIndividualPlan.P_FORMING_DATE, EplIndividualPlan.formingDate())
                                   .publisherLinkResolver(new DefaultPublisherLinkResolver() {
                                       @Override public String getComponentName(IEntity entity) { return EplIndividualPlanPub.class.getSimpleName(); }
                                       @Override public Object getParameters(IEntity entity) { return entity.getProperty(PARAM_PUB_PARAMETERS); }
                                   })
                                   .formatter(DateFormatter.DATE_FORMATTER_WITH_TIME)
                                   .order())
                .addColumn(textColumn(EplIndividualPlan.L_PPS, EplIndividualPlan.pps().titleFioInfoOrgUnitDegreesStatuses()).order())
                .addColumn(textColumn(EplIndividualPlan.L_ORG_UNIT, EplIndividualPlan.orgUnit().shortTitle()).visible("ui:orgUnitVisible").order())
                .addColumn(textColumn(EplIndividualPlan.L_EPP_YEAR, EplIndividualPlan.eppYear().title()).order())
                .addColumn(textColumn(PARAM_STUDENT_SUMMARY, PARAM_STUDENT_SUMMARY).formatter(RowCollectionFormatter.INSTANCE))
                .addColumn(textColumn(PARAM_EDU_LOAD, PARAM_EDU_LOAD).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED))
                .addColumn(textColumn(PARAM_NON_EDU_LOAD, PARAM_NON_EDU_LOAD).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED))
                .addColumn(actionColumn(PRINT_COLUMN_NAME, CommonDefines.ICON_PRINT, PRINT_LISTENER).permissionKey("ui:printPermissionKey"))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("ui:editPermissionKey"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
                                   .alert(FormattedMessage.with().template(IND_PLAN_DS + ".delete.alert")
                                                  .parameter(EplIndividualPlan.pps().titleFioInfoOrgUnitDegreesStatuses().s())
                                                  .parameter(EplIndividualPlan.formingDate(), DateFormatter.DATE_FORMATTER_WITH_TIME)
                                                  .create())
                                   .permissionKey("ui:deletePermissionKey")
                                   .create())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> indPlanDSHandler()
    {
        return new EplIndividualPlanDSHandler(getName());
    }


    private DQLSelectBuilder getIndPlanBuilder(Long orgUnitId, Person ppsPerson)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EplIndividualPlan.class, "i").column(value(1)).top(1);
        builder.joinEntity("i", DQLJoinType.left, EplIndPlan2OuSummaryRel.class, "rel", eq(property("rel", EplIndPlan2OuSummaryRel.indPlan().id()), property("i.id")));

        FilterUtils.applySelectFilter(builder, "i", EplIndividualPlan.pps().person(), ppsPerson);
        FilterUtils.applySelectFilter(builder, "i", EplIndividualPlan.orgUnit().id(), orgUnitId);
        return builder;
    }
}
