package ru.tandemservice.uniepp_load.base.entity.eduGroup;

import org.tandemframework.core.common.IEntityDebugTitled;
import org.tandemframework.core.common.ITitledEntity;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.gen.EplEduGroupGen;

/**
 * План. поток
 */
public class EplEduGroup extends EplEduGroupGen implements ITitledEntity, IEntityDebugTitled
{
    @Override
    public String getEntityDebugTitle()
    {
        return getTitle() + ", " + getRegistryElementPart().getNumberWithAbbreviationWithPart() + ", " + getGroupType().getAbbreviation();
    }
}