package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.dialect.MSSqlDBDialect;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_load_2x11x3_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        final String creationDate_p = tool.getDialect() instanceof MSSqlDBDialect ? "cast (creationDate_p as varchar) " :"creationDate_p";
        tool.executeUpdate("update epl_student_summary_t set title_p = " +
                tool.createStringConcatenationSQL("title_p", "' ('", creationDate_p, "')'") +
                " where title_p in\n" +
                " (select title_p from epl_student_summary_t group by title_p having count(1) > 1)");
    }
}