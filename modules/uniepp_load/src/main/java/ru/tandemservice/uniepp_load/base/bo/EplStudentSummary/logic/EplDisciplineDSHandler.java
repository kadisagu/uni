/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic;

import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.ui.support.IUISettings;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.base.bo.EppRegistry.logic.BaseRegistryElementPartDSHandler;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 11.05.2016
 */
public class EplDisciplineDSHandler extends BaseRegistryElementPartDSHandler
{
    public EplDisciplineDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
    {
        super.applyWhereConditions(alias, dql, context);
        dql.where(ne(property(alias, EppRegistryElementPart.registryElement().state().code()), value(EppState.STATE_ARCHIVED)));

        IUISettings settings = context.get(EplStudentSummaryManager.PARAM_SETTINGS);
        if (null != settings) {
            FilterUtils.applySelectFilter(dql, alias, EppRegistryElementPart.registryElement().owner(), settings.get(EplStudentSummaryManager.PARAM_READING_ORGUNIT));
        }
    }
}
