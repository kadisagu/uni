package ru.tandemservice.uniepp_load.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategory;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryNonEduLoad;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Категория времени внеучебной нагрузки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplTimeCategoryNonEduLoadGen extends EplTimeCategory
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryNonEduLoad";
    public static final String ENTITY_NAME = "eplTimeCategoryNonEduLoad";
    public static final int VERSION_HASH = 865297989;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EplTimeCategoryNonEduLoadGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplTimeCategoryNonEduLoadGen> extends EplTimeCategory.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplTimeCategoryNonEduLoad.class;
        }

        public T newInstance()
        {
            return (T) new EplTimeCategoryNonEduLoad();
        }
    }
    private static final Path<EplTimeCategoryNonEduLoad> _dslPath = new Path<EplTimeCategoryNonEduLoad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplTimeCategoryNonEduLoad");
    }
            

    public static class Path<E extends EplTimeCategoryNonEduLoad> extends EplTimeCategory.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EplTimeCategoryNonEduLoad.class;
        }

        public String getEntityName()
        {
            return "eplTimeCategoryNonEduLoad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
