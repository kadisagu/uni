package ru.tandemservice.uniepp_load.migration;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.tandemframework.shared.commonbase.utils.MigrationUtils.processor;
import static ru.tandemservice.uniepp_load.migration.MS_uniepp_load_2x11x0_0to1.PlanPpsType.*;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_load_2x11x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // В системе должен быть, хотя бы один элемент справочника «Квалификационные уровни»
        boolean hasQualificationLevel = tool.getNumericResult("select count(*) from qualificationlevel_t") > 0;
        if (!hasQualificationLevel)
            throw new IllegalStateException("Table qualificationlevel_t is empty.");

        // В системе должен быть, хотя бы один элемент справочника «Профессионально-квалификационные группы»
        boolean hasProfQualificationGroup = tool.getNumericResult("select count(*) from profqualificationgroup_t") > 0;
        if (!hasProfQualificationGroup)
            throw new IllegalStateException("Table profqualificationgroup_t is empty.");

        // 1. создание записей для справочника должностей ПКГ и КУ
        {
            List<Object[]> rows = tool.executeQuery(
                    processor(Long.class, String.class),
                    "select p.id, p.title_p from post_t p left join postboundedwithqgandql_t q on p.id = q.post_id where p.id in " +
                            "(select post_id from epl_pl_pps_t where post_id is not null union select post_id from epl_settings_pps_load_t where post_id is not null) and q.id is null");

            if (!rows.isEmpty())
            {
                short entityCode = tool.entityCodes().ensure("postBoundedWithQGandQL");
                MigrationUtils.BatchInsertBuilder insert = new MigrationUtils.BatchInsertBuilder(entityCode, "code_p", "title_p", "post_id", "profqualificationgroup_id", "qualificationlevel_id");

                long minQualificationLevel = tool.getNumericResult("select id from qualificationlevel_t where code_p = (select min(code_p) from qualificationlevel_t)");
                long minProfQualificationGroup = tool.getNumericResult("select id from profqualificationgroup_t where code_p = (select min(code_p) from profqualificationgroup_t)");
                // поскольку код - строка, находим максимальный через id
                String maxPostQGAndQLCode = (String) tool.getUniqueResult("select code_p from postboundedwithqgandql_t where id = (select max(id) from postboundedwithqgandql_t)");

                int code = Integer.valueOf(maxPostQGAndQLCode);
                for (final Object[] row : rows)
                {
                    code++;
                    Long postId = (Long) row[0];
                    String postTitle = (String) row[1];
                    insert.addRow(String.valueOf(code), postTitle, postId, minProfQualificationGroup, minQualificationLevel);
                }
                insert.executeInsert(tool, "postboundedwithqgandql_t");
            }
        }

        // 2. Коректируем настройку «Предельные значения нагрузки преподавателей»
        {
            String settingTable = "epl_settings_pps_load_t";
            tool.dropConstraint(settingTable, "fk_post_eplsettingsppsload");

            Map<Long, List<Long>> postOld2NewIdsMap = Maps.newHashMap();
            List<Object[]> postRows = tool.executeQuery(
                    processor(Long.class, Long.class),
                    "select p.id, q.id from post_t p left join postboundedwithqgandql_t q on p.id = q.post_id where p.id in (select post_id from epl_settings_pps_load_t where post_id is not null)"
            );
            for (final Object[] row : postRows)
            {
                Long postId = (Long) row[0];
                Long postQGAndQLId = (Long) row[1];
                SafeMap.safeGet(postOld2NewIdsMap, postId, ArrayList.class).add(postQGAndQLId);
            }

            short entityCode = tool.entityCodes().ensure("eplSettingsPpsLoad");
            MigrationUtils.BatchInsertBuilder insert = new MigrationUtils.BatchInsertBuilder(entityCode, "eduyear_id", "post_id", "hoursmin_p", "hoursmax_p", "hoursadditional_p");

            List<Object[]> settingRows = tool.executeQuery(
                    processor(Long.class, Long.class, Long.class, Double.class, Double.class, Double.class),
                    "select id, eduyear_id, post_id, hoursmin_p, hoursmax_p, hoursadditional_p from " + settingTable + " where post_id is not null"
            );

            List<Long> removeIds = Lists.newArrayList();
            for (final Object[] row : settingRows)
            {
                Long id = (Long) row[0];
                Long eduYearId = (Long) row[1];
                Long postId = (Long) row[2];
                Double hoursMin = (Double) row[3];
                Double hoursMax = (Double) row[4];
                Double hoursAdditional = (Double) row[5];

                for (Long newPostId : postOld2NewIdsMap.get(postId))
                {
                    insert.addRow(eduYearId, newPostId, hoursMin, hoursMax, hoursAdditional);
                }
                removeIds.add(id);
            }
            insert.executeInsert(tool, settingTable);

            int count = MigrationUtils.massDeleteByIds(tool, settingTable, removeIds);
            tool.info(count + " rows removed by " + settingTable);
        }

        // 3. Корректируем планируемых ППС
        {
            String planPpsTable = "epl_pl_pps_t";

            // если доля ставки отрицательна, сохраняем вместо нее 0
            tool.executeUpdate("update " + planPpsTable + " set staffrateaslong_p = ? where staffrateaslong_p is not null and staffrateaslong_p < 0", 0L);

            Long typeStaffId, typeVacancyAsStaffId, typeTimeWorkerId, typeVacancyAsTimeWorkerId;

            ////////////////////////////////////////////////////////////////////////////////
            // сущность eplPlannedPpsType
            // создана новая сущность
            {
                // создать таблицу
                DBTable dbt = new DBTable("epl_pl_pps_type_t",
                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eplplannedppstype"),
                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                      new DBColumn("shorttitle_p", DBType.createVarchar(255)).setNullable(false),
                      new DBColumn("priority_p", DBType.INTEGER).setNullable(false),
                      new DBColumn("title_p", DBType.createVarchar(1200))
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("eplPlannedPpsType");

                // заполняем элементы справочника по умолчанию
                MigrationUtils.BatchInsertBuilder insert = new MigrationUtils.BatchInsertBuilder(entityCode, "code_p", "title_p", "shorttitle_p", "priority_p");
                typeStaffId = insert.addRow("staff", "Штат", "штат", 1);
                typeVacancyAsStaffId = insert.addRow("vacancyAsStaff", "Вакансия (штат)", "вакансия (штат)", 2);
                typeTimeWorkerId = insert.addRow("timeWorker", "Почасовик", "почасовик", 3);
                typeVacancyAsTimeWorkerId = insert.addRow("vacancyAsTimeWorker", "Вакансия (почасовик)", "вакансия (почас.)", 4);
                insert.executeInsert(tool, "epl_pl_pps_type_t");
            }

            ////////////////////////////////////////////////////////////////////////////////
            // сущность eplPlannedPps
            // создаем колонки
            {
                tool.createColumn(planPpsTable, new DBColumn("type_id", DBType.LONG));
                tool.createColumn(planPpsTable, new DBColumn("maxtimeamountaslong_p", DBType.LONG));
            }

            // задаем значение по умолчанию + меняем ссылки на должности

            tool.dropConstraint(planPpsTable, "fk_post_eplplannedpps");
            tool.dropConstraint(planPpsTable, "eplplannedpps_2af86910_46a0210");
            tool.dropConstraint(planPpsTable, "eplplannedpps_654363a0_46a0210");

            // поднимаем всех ППС
            List<Object[]> rows = tool.executeQuery(
                    processor(Long.class, Long.class, Long.class, String.class, Long.class, Long.class, Long.class, Long.class, Long.class, String.class, String.class, String.class),
                    "select pl_pps.id, pl_pps.person_id, pl_pps.post_id, pl_pps.vacancy_p, pl_pps.ppsentry_id, pps_ep.id, pps_ep.post_id, ou.orgunit_id, ey.educationyear_id, " +
                            "icard.lastName_p, icard.firstName_p, icard.middleName_p " +
                            "from epl_pl_pps_t pl_pps " +
                            "left join pps_entry_employeepost_t pps_ep on pl_pps.ppsentry_id = pps_ep.id " +
                            "left join epl_ou_summary_t ou on pl_pps.orgunitsummary_id = ou.id " +
                            "left join epl_student_summary_t ss on ou.studentsummary_id = ss.id " +
                            "left join epp_year_epp_t ey on ss.eppyear_id = ey.id " +
                            "left join person_t person on pl_pps.person_id = person.id " +
                            "left join identitycard_t icard on person.identityCard_id = icard.id"
            );
            Map<PairKey<Long, Long>, Long> ppsLoadSettingMap = getPpsLoadSettingMap(tool);

            MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater(
                    "update " + planPpsTable + " set type_id = ?, post_id = ?, maxtimeamountaslong_p = ?, vacancy_p = ?, ppsentry_id = ? where id = ?",
                    DBType.LONG, DBType.LONG, DBType.LONG, DBType.EMPTY_STRING, DBType.LONG, DBType.LONG
            );

            for (final Object[] row : rows)
            {
                Long id = (Long) row[0];
                Long personId = (Long) row[1];
                Long postId = (Long) row[2];		// старая должность
                String vacancy = (String) row[3];
                Long teacherId = (Long) row[4];

                Long epPpsEntryId = (Long) row[5];
                Long epPostId = (Long) row[6];		// новая, возможная должность

                Long orgUnitId = (Long) row[7];
                Long eduYearId = (Long) row[8];
                String personLastName = (String) row[9];
                String personFirstName = (String) row[10];
                String personMiddleName = (String) row[11];
                String personFullFio = getFullFio(personLastName, personFirstName, personMiddleName);

                boolean isEmployeePost = null != teacherId && null != epPpsEntryId;
                boolean isTimeWorker = null != teacherId && null == epPpsEntryId;

                // определяем тип ППС
                PlanPpsType type = checkPlPps(personId, postId, vacancy, teacherId, epPpsEntryId);
                String postErrorMessage = "PostBoundedWithQGAndQL id not found. Params: [personId, postId, orgUnitId] = [" + StringUtils.join(Arrays.asList(personId, postId, orgUnitId), ", ") + "]";

                if (type.equals(STAFF))
                {
                    if (null == epPostId)
                        throw new IllegalStateException(postErrorMessage);

                    long maxHour = ppsLoadSettingMap.getOrDefault(PairKey.create(eduYearId, epPostId), 0L);
                    updater.addBatch(typeStaffId, epPostId, maxHour, vacancy, teacherId, id);
                }
                else if (type.equals(VACANCY_AS_STAFF_NEW))
                {
                    long newPostId = getPostQGAndQLIdByEmployeePost(tool, personId, postId, orgUnitId);
                    if (0 == newPostId)
                        newPostId = getPostQGAndQLId(tool, postId);
                    if (0 == newPostId)
                        throw new IllegalStateException(postErrorMessage);

                    long maxHour = ppsLoadSettingMap.getOrDefault(PairKey.create(eduYearId, newPostId), 0L);
                    updater.addBatch(typeVacancyAsStaffId, newPostId, maxHour, personFullFio, null, id);
                }
                else if (type.equals(VACANCY_AS_STAFF_OLD))
                {
                    long newPostId = isEmployeePost ? epPostId : getPostQGAndQLId(tool, postId);
                    if (0 == newPostId)
                        throw new IllegalStateException(postErrorMessage);

                    long maxHour = ppsLoadSettingMap.getOrDefault(PairKey.create(eduYearId, newPostId), 0L);
                    updater.addBatch(typeVacancyAsStaffId, newPostId, maxHour, vacancy, isTimeWorker ? null : teacherId, id);
                }
                else if (type.equals(TIMEWORKER))
                {
                    long maxHour = getTimeWorkerHour(tool, teacherId);
                    updater.addBatch(typeTimeWorkerId, null, maxHour, vacancy, teacherId, id);
                }
                else if (type.equals(VACANCY_AS_TIMEWORKER_NEW))
                {
                    updater.addBatch(typeVacancyAsTimeWorkerId, null, 0L, personFullFio, isEmployeePost ? null : teacherId, id);
                }
                else if (type.equals(VACANCY_AS_TIMEWORKER_OLD))
                {
                    long maxHour = isTimeWorker ? getTimeWorkerHour(tool, teacherId) : 0L;
                    updater.addBatch(typeVacancyAsTimeWorkerId, null, maxHour, vacancy, isEmployeePost ? null : teacherId, id);
                }
                else
                {
                    String params = StringUtils.join(Arrays.asList(personId, postId, vacancy, teacherId, epPpsEntryId), ", ");
                    throw new IllegalStateException("EplPlannedPps (" + id + ") has unknown type. Params: [personId, postId, vacancy, ppsEntryId, ppsEntryEmployeePostId] = [" + params + "]");
                }
            }
            updater.executeUpdate(tool);

            // делаем колонки NOT NULL
            tool.setColumnNullable(planPpsTable, "type_id", false);
            tool.setColumnNullable(planPpsTable, "maxtimeamountaslong_p", false);

            // удалено свойство person
            {
                // удалить колонку
                tool.dropColumn(planPpsTable, "person_id");
            }
        }
    }

    private Map<PairKey<Long, Long>, Long> getPpsLoadSettingMap(DBTool tool) throws Exception
    {
        Map<PairKey<Long, Long>, Long> resultMap = Maps.newHashMap();
        List<Object[]> rows = tool.executeQuery(processor(Long.class, Long.class, Long.class), "select eduyear_id, post_id, hoursadditional_p from epl_settings_pps_load_t");

        for (final Object[] row : rows)
        {
            Long year = (Long) row[0];
            Long post = (Long) row[1];
            Long hour = (Long) row[2];

            if (hour < 0) hour = 0L;

            resultMap.put(PairKey.create(year, post), hour * 100);
        }
        return resultMap;
    }

    private long getPostQGAndQLIdByEmployeePost(DBTool tool, Long personId, Long postId, Long orgUnitId) throws Exception
    {
        return tool.getNumericResult(
                "select min(pbw.id) from employeepost_t ep " +
                        "left join employee_t e on ep.employee_id = e.id " +
                        "left join personrole_t pr on pr.id = e.id " +
                        "left join employeepoststatus_t epps on ep.postStatus_id = epps.id " +
                        "left join orgunittypepostrelation_t postrel on ep.postRelation_id = postrel.id " +
                        "left join postboundedwithqgandql_t pbw on postrel.postboundedwithqgandql_id = pbw.id " +
                        "where pr.person_id = ? and pbw.post_id = ? and ep.orgUnit_id = ? and e.archival_p = ? and epps.actual_p = ?",
                personId, postId, orgUnitId, false, true
        );
    }

    private long getPostQGAndQLId(DBTool tool, Long postId) throws Exception
    {
        return tool.getNumericResult("select min(id) from postboundedwithqgandql_t where post_id = ?", postId);
    }

    private String getFullFio(String lastName, String firstName, String middleName)
    {
        if (null == lastName || null == firstName) return null;
        return lastName + " " + firstName + (StringUtils.isEmpty(middleName) ? "" : (" " + middleName));
    }

    private long getTimeWorkerHour(DBTool tool, Long teacherId) throws Exception
    {
        if (tool.tableExists("pps_entry_timeworker_t"))
        {
            return tool.getNumericResult("select timeamountaslong_p from pps_entry_timeworker_t where id = ?", teacherId);
        }
        return 0L;
    }

    private PlanPpsType checkPlPps(Long person, Long post, String vacancy, Long teacher, Long ppsEntry_ep)
    {
        // указан преподаватель, который является записью реестра ППС на базе сотрудника
        boolean employeePost = null != teacher && null != ppsEntry_ep;
        // указан преподаватель, который не является записью реестра ППС на базе сотрудника => почасовик
        boolean timeWorker = null != teacher && null == ppsEntry_ep;

        if (null != person && null != post && employeePost)
            return STAFF;
        else if (null != person && null != post && (null == teacher || timeWorker))
            return VACANCY_AS_STAFF_NEW;
        else if (null != vacancy && null != post)
            return VACANCY_AS_STAFF_OLD;
        else if (null != person && null == post && timeWorker)
            return TIMEWORKER;
        else if (null != person && null == post && (null == teacher || employeePost))
            return VACANCY_AS_TIMEWORKER_NEW;
        else if (null != vacancy && null == post)
            return VACANCY_AS_TIMEWORKER_OLD;
        else
            return null;
    }

    enum PlanPpsType
    {
        STAFF,						// ранее штатный ППС с корректно заполненным преподавателем останется штатным ППС
        VACANCY_AS_STAFF_NEW,		// ранее штатный ППС, у которого не заполнен или некорректно заполнен преподаватель, станет штатной вакансией
        VACANCY_AS_STAFF_OLD,		// ранее штатная вакансия останется штатной вакансией
        TIMEWORKER,					// ранее почасовик с корректно заполненным преподавателем останется почасовиком
        VACANCY_AS_TIMEWORKER_NEW, 	// ранее почасовик, у которого не заполнен или некорректно заполнен преподаватель, станет вакансией почасовика
        VACANCY_AS_TIMEWORKER_OLD	// ранее вакансия почасовика останется вакансией почасовика
    }
}