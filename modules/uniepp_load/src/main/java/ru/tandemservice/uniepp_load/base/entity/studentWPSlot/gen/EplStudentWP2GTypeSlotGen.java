package ru.tandemservice.uniepp_load.base.entity.studentWPSlot.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWP2GTypeSlot;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * МСРП-ВН (план.)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplStudentWP2GTypeSlotGen extends EntityBase
 implements INaturalIdentifiable<EplStudentWP2GTypeSlotGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWP2GTypeSlot";
    public static final String ENTITY_NAME = "eplStudentWP2GTypeSlot";
    public static final int VERSION_HASH = 856527500;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_W_P_SLOT = "studentWPSlot";
    public static final String L_GROUP_TYPE = "groupType";
    public static final String L_SPLIT_ELEMENT = "splitElement";
    public static final String P_COUNT = "count";

    private EplStudentWPSlot _studentWPSlot;     // МСРП (план.)
    private EppGroupType _groupType;     // Обобщенный вид нагрузки
    private EppEduGroupSplitBaseElement _splitElement;     // Элемент деления потоков
    private int _count;     // Число студентов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return МСРП (план.). Свойство не может быть null.
     */
    @NotNull
    public EplStudentWPSlot getStudentWPSlot()
    {
        return _studentWPSlot;
    }

    /**
     * @param studentWPSlot МСРП (план.). Свойство не может быть null.
     */
    public void setStudentWPSlot(EplStudentWPSlot studentWPSlot)
    {
        dirty(_studentWPSlot, studentWPSlot);
        _studentWPSlot = studentWPSlot;
    }

    /**
     * @return Обобщенный вид нагрузки. Свойство не может быть null.
     */
    @NotNull
    public EppGroupType getGroupType()
    {
        return _groupType;
    }

    /**
     * @param groupType Обобщенный вид нагрузки. Свойство не может быть null.
     */
    public void setGroupType(EppGroupType groupType)
    {
        dirty(_groupType, groupType);
        _groupType = groupType;
    }

    /**
     * @return Элемент деления потоков.
     */
    public EppEduGroupSplitBaseElement getSplitElement()
    {
        return _splitElement;
    }

    /**
     * @param splitElement Элемент деления потоков.
     */
    public void setSplitElement(EppEduGroupSplitBaseElement splitElement)
    {
        dirty(_splitElement, splitElement);
        _splitElement = splitElement;
    }

    /**
     * @return Число студентов. Свойство не может быть null.
     */
    @NotNull
    public int getCount()
    {
        return _count;
    }

    /**
     * @param count Число студентов. Свойство не может быть null.
     */
    public void setCount(int count)
    {
        dirty(_count, count);
        _count = count;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EplStudentWP2GTypeSlotGen)
        {
            if (withNaturalIdProperties)
            {
                setStudentWPSlot(((EplStudentWP2GTypeSlot)another).getStudentWPSlot());
                setGroupType(((EplStudentWP2GTypeSlot)another).getGroupType());
                setSplitElement(((EplStudentWP2GTypeSlot)another).getSplitElement());
            }
            setCount(((EplStudentWP2GTypeSlot)another).getCount());
        }
    }

    public INaturalId<EplStudentWP2GTypeSlotGen> getNaturalId()
    {
        return new NaturalId(getStudentWPSlot(), getGroupType(), getSplitElement());
    }

    public static class NaturalId extends NaturalIdBase<EplStudentWP2GTypeSlotGen>
    {
        private static final String PROXY_NAME = "EplStudentWP2GTypeSlotNaturalProxy";

        private Long _studentWPSlot;
        private Long _groupType;
        private Long _splitElement;

        public NaturalId()
        {}

        public NaturalId(EplStudentWPSlot studentWPSlot, EppGroupType groupType, EppEduGroupSplitBaseElement splitElement)
        {
            _studentWPSlot = ((IEntity) studentWPSlot).getId();
            _groupType = ((IEntity) groupType).getId();
            _splitElement = splitElement==null ? null : ((IEntity) splitElement).getId();
        }

        public Long getStudentWPSlot()
        {
            return _studentWPSlot;
        }

        public void setStudentWPSlot(Long studentWPSlot)
        {
            _studentWPSlot = studentWPSlot;
        }

        public Long getGroupType()
        {
            return _groupType;
        }

        public void setGroupType(Long groupType)
        {
            _groupType = groupType;
        }

        public Long getSplitElement()
        {
            return _splitElement;
        }

        public void setSplitElement(Long splitElement)
        {
            _splitElement = splitElement;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EplStudentWP2GTypeSlotGen.NaturalId) ) return false;

            EplStudentWP2GTypeSlotGen.NaturalId that = (NaturalId) o;

            if( !equals(getStudentWPSlot(), that.getStudentWPSlot()) ) return false;
            if( !equals(getGroupType(), that.getGroupType()) ) return false;
            if( !equals(getSplitElement(), that.getSplitElement()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStudentWPSlot());
            result = hashCode(result, getGroupType());
            result = hashCode(result, getSplitElement());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStudentWPSlot());
            sb.append("/");
            sb.append(getGroupType());
            sb.append("/");
            sb.append(getSplitElement());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplStudentWP2GTypeSlotGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplStudentWP2GTypeSlot.class;
        }

        public T newInstance()
        {
            return (T) new EplStudentWP2GTypeSlot();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentWPSlot":
                    return obj.getStudentWPSlot();
                case "groupType":
                    return obj.getGroupType();
                case "splitElement":
                    return obj.getSplitElement();
                case "count":
                    return obj.getCount();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentWPSlot":
                    obj.setStudentWPSlot((EplStudentWPSlot) value);
                    return;
                case "groupType":
                    obj.setGroupType((EppGroupType) value);
                    return;
                case "splitElement":
                    obj.setSplitElement((EppEduGroupSplitBaseElement) value);
                    return;
                case "count":
                    obj.setCount((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentWPSlot":
                        return true;
                case "groupType":
                        return true;
                case "splitElement":
                        return true;
                case "count":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentWPSlot":
                    return true;
                case "groupType":
                    return true;
                case "splitElement":
                    return true;
                case "count":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentWPSlot":
                    return EplStudentWPSlot.class;
                case "groupType":
                    return EppGroupType.class;
                case "splitElement":
                    return EppEduGroupSplitBaseElement.class;
                case "count":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplStudentWP2GTypeSlot> _dslPath = new Path<EplStudentWP2GTypeSlot>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplStudentWP2GTypeSlot");
    }
            

    /**
     * @return МСРП (план.). Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWP2GTypeSlot#getStudentWPSlot()
     */
    public static EplStudentWPSlot.Path<EplStudentWPSlot> studentWPSlot()
    {
        return _dslPath.studentWPSlot();
    }

    /**
     * @return Обобщенный вид нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWP2GTypeSlot#getGroupType()
     */
    public static EppGroupType.Path<EppGroupType> groupType()
    {
        return _dslPath.groupType();
    }

    /**
     * @return Элемент деления потоков.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWP2GTypeSlot#getSplitElement()
     */
    public static EppEduGroupSplitBaseElement.Path<EppEduGroupSplitBaseElement> splitElement()
    {
        return _dslPath.splitElement();
    }

    /**
     * @return Число студентов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWP2GTypeSlot#getCount()
     */
    public static PropertyPath<Integer> count()
    {
        return _dslPath.count();
    }

    public static class Path<E extends EplStudentWP2GTypeSlot> extends EntityPath<E>
    {
        private EplStudentWPSlot.Path<EplStudentWPSlot> _studentWPSlot;
        private EppGroupType.Path<EppGroupType> _groupType;
        private EppEduGroupSplitBaseElement.Path<EppEduGroupSplitBaseElement> _splitElement;
        private PropertyPath<Integer> _count;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return МСРП (план.). Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWP2GTypeSlot#getStudentWPSlot()
     */
        public EplStudentWPSlot.Path<EplStudentWPSlot> studentWPSlot()
        {
            if(_studentWPSlot == null )
                _studentWPSlot = new EplStudentWPSlot.Path<EplStudentWPSlot>(L_STUDENT_W_P_SLOT, this);
            return _studentWPSlot;
        }

    /**
     * @return Обобщенный вид нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWP2GTypeSlot#getGroupType()
     */
        public EppGroupType.Path<EppGroupType> groupType()
        {
            if(_groupType == null )
                _groupType = new EppGroupType.Path<EppGroupType>(L_GROUP_TYPE, this);
            return _groupType;
        }

    /**
     * @return Элемент деления потоков.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWP2GTypeSlot#getSplitElement()
     */
        public EppEduGroupSplitBaseElement.Path<EppEduGroupSplitBaseElement> splitElement()
        {
            if(_splitElement == null )
                _splitElement = new EppEduGroupSplitBaseElement.Path<EppEduGroupSplitBaseElement>(L_SPLIT_ELEMENT, this);
            return _splitElement;
        }

    /**
     * @return Число студентов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWP2GTypeSlot#getCount()
     */
        public PropertyPath<Integer> count()
        {
            if(_count == null )
                _count = new PropertyPath<Integer>(EplStudentWP2GTypeSlotGen.P_COUNT, this);
            return _count;
        }

        public Class getEntityClass()
        {
            return EplStudentWP2GTypeSlot.class;
        }

        public String getEntityName()
        {
            return "eplStudentWP2GTypeSlot";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
