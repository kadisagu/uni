package ru.tandemservice.uniepp_load.util;

/**
 * @author vdanilov
 */
public class UniEppLoadUtils
{
    // сущность, по которой определяется подключен ли М Почасовики
    public static final String UNI_SNPPS_REL = "unisnppsTimeworker";

    public static boolean eq(final double load1, final double load2) {
        return (Math.abs(load1 - load2) < 0.0001d);
    }

    public static double wrap(final long value)
    {
        return value / 100.0;
    }

    public static Double wrap(final Long value)
    {
        return value == null ? null : value / 100.0;
    }

    public static long unwrap(final double value)
    {
        return Math.round(value * 100);
    }

    public static Long unwrap(final Double value)
    {
        return value == null ? null : Math.round(value * 100);
    }

    public static Double staffRateWrap(final Long value)
    {
        return value == null ? null : value / 10000.0;
    }

    public static Long staffRateUnwrap(final Double value)
    {
        return value == null ? null : Math.round(value * 10000);
    }
}
