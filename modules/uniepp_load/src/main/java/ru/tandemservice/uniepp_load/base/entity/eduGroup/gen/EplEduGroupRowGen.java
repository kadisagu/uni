package ru.tandemservice.uniepp_load.base.entity.eduGroup.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWP2GTypeSlot;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка план. потока
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplEduGroupRowGen extends EntityBase
 implements INaturalIdentifiable<EplEduGroupRowGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow";
    public static final String ENTITY_NAME = "eplEduGroupRow";
    public static final int VERSION_HASH = 910733555;
    private static IEntityMeta ENTITY_META;

    public static final String L_GROUP = "group";
    public static final String L_STUDENT_W_P2_G_TYPE_SLOT = "studentWP2GTypeSlot";
    public static final String P_COUNT = "count";

    private EplEduGroup _group;     // План. поток
    private EplStudentWP2GTypeSlot _studentWP2GTypeSlot;     // МСРП (план.)
    private int _count;     // Число студентов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return План. поток. Свойство не может быть null.
     */
    @NotNull
    public EplEduGroup getGroup()
    {
        return _group;
    }

    /**
     * @param group План. поток. Свойство не может быть null.
     */
    public void setGroup(EplEduGroup group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return МСРП (план.). Свойство не может быть null.
     */
    @NotNull
    public EplStudentWP2GTypeSlot getStudentWP2GTypeSlot()
    {
        return _studentWP2GTypeSlot;
    }

    /**
     * @param studentWP2GTypeSlot МСРП (план.). Свойство не может быть null.
     */
    public void setStudentWP2GTypeSlot(EplStudentWP2GTypeSlot studentWP2GTypeSlot)
    {
        dirty(_studentWP2GTypeSlot, studentWP2GTypeSlot);
        _studentWP2GTypeSlot = studentWP2GTypeSlot;
    }

    /**
     * @return Число студентов. Свойство не может быть null.
     */
    @NotNull
    public int getCount()
    {
        return _count;
    }

    /**
     * @param count Число студентов. Свойство не может быть null.
     */
    public void setCount(int count)
    {
        dirty(_count, count);
        _count = count;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EplEduGroupRowGen)
        {
            if (withNaturalIdProperties)
            {
                setGroup(((EplEduGroupRow)another).getGroup());
                setStudentWP2GTypeSlot(((EplEduGroupRow)another).getStudentWP2GTypeSlot());
            }
            setCount(((EplEduGroupRow)another).getCount());
        }
    }

    public INaturalId<EplEduGroupRowGen> getNaturalId()
    {
        return new NaturalId(getGroup(), getStudentWP2GTypeSlot());
    }

    public static class NaturalId extends NaturalIdBase<EplEduGroupRowGen>
    {
        private static final String PROXY_NAME = "EplEduGroupRowNaturalProxy";

        private Long _group;
        private Long _studentWP2GTypeSlot;

        public NaturalId()
        {}

        public NaturalId(EplEduGroup group, EplStudentWP2GTypeSlot studentWP2GTypeSlot)
        {
            _group = ((IEntity) group).getId();
            _studentWP2GTypeSlot = ((IEntity) studentWP2GTypeSlot).getId();
        }

        public Long getGroup()
        {
            return _group;
        }

        public void setGroup(Long group)
        {
            _group = group;
        }

        public Long getStudentWP2GTypeSlot()
        {
            return _studentWP2GTypeSlot;
        }

        public void setStudentWP2GTypeSlot(Long studentWP2GTypeSlot)
        {
            _studentWP2GTypeSlot = studentWP2GTypeSlot;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EplEduGroupRowGen.NaturalId) ) return false;

            EplEduGroupRowGen.NaturalId that = (NaturalId) o;

            if( !equals(getGroup(), that.getGroup()) ) return false;
            if( !equals(getStudentWP2GTypeSlot(), that.getStudentWP2GTypeSlot()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getGroup());
            result = hashCode(result, getStudentWP2GTypeSlot());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getGroup());
            sb.append("/");
            sb.append(getStudentWP2GTypeSlot());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplEduGroupRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplEduGroupRow.class;
        }

        public T newInstance()
        {
            return (T) new EplEduGroupRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "group":
                    return obj.getGroup();
                case "studentWP2GTypeSlot":
                    return obj.getStudentWP2GTypeSlot();
                case "count":
                    return obj.getCount();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "group":
                    obj.setGroup((EplEduGroup) value);
                    return;
                case "studentWP2GTypeSlot":
                    obj.setStudentWP2GTypeSlot((EplStudentWP2GTypeSlot) value);
                    return;
                case "count":
                    obj.setCount((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "group":
                        return true;
                case "studentWP2GTypeSlot":
                        return true;
                case "count":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "group":
                    return true;
                case "studentWP2GTypeSlot":
                    return true;
                case "count":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "group":
                    return EplEduGroup.class;
                case "studentWP2GTypeSlot":
                    return EplStudentWP2GTypeSlot.class;
                case "count":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplEduGroupRow> _dslPath = new Path<EplEduGroupRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplEduGroupRow");
    }
            

    /**
     * @return План. поток. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow#getGroup()
     */
    public static EplEduGroup.Path<EplEduGroup> group()
    {
        return _dslPath.group();
    }

    /**
     * @return МСРП (план.). Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow#getStudentWP2GTypeSlot()
     */
    public static EplStudentWP2GTypeSlot.Path<EplStudentWP2GTypeSlot> studentWP2GTypeSlot()
    {
        return _dslPath.studentWP2GTypeSlot();
    }

    /**
     * @return Число студентов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow#getCount()
     */
    public static PropertyPath<Integer> count()
    {
        return _dslPath.count();
    }

    public static class Path<E extends EplEduGroupRow> extends EntityPath<E>
    {
        private EplEduGroup.Path<EplEduGroup> _group;
        private EplStudentWP2GTypeSlot.Path<EplStudentWP2GTypeSlot> _studentWP2GTypeSlot;
        private PropertyPath<Integer> _count;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return План. поток. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow#getGroup()
     */
        public EplEduGroup.Path<EplEduGroup> group()
        {
            if(_group == null )
                _group = new EplEduGroup.Path<EplEduGroup>(L_GROUP, this);
            return _group;
        }

    /**
     * @return МСРП (план.). Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow#getStudentWP2GTypeSlot()
     */
        public EplStudentWP2GTypeSlot.Path<EplStudentWP2GTypeSlot> studentWP2GTypeSlot()
        {
            if(_studentWP2GTypeSlot == null )
                _studentWP2GTypeSlot = new EplStudentWP2GTypeSlot.Path<EplStudentWP2GTypeSlot>(L_STUDENT_W_P2_G_TYPE_SLOT, this);
            return _studentWP2GTypeSlot;
        }

    /**
     * @return Число студентов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow#getCount()
     */
        public PropertyPath<Integer> count()
        {
            if(_count == null )
                _count = new PropertyPath<Integer>(EplEduGroupRowGen.P_COUNT, this);
            return _count;
        }

        public Class getEntityClass()
        {
            return EplEduGroupRow.class;
        }

        public String getEntityName()
        {
            return "eplEduGroupRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
