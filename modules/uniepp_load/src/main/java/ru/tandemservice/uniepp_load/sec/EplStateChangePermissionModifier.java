/* $Id$ */
package ru.tandemservice.uniepp_load.sec;

import com.google.common.collect.ImmutableMap;
import org.tandemframework.core.CoreCollectionUtils;
import ru.tandemservice.uniepp.sec.UnieppStateChangePermissionModifier;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;

import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 08.02.2016
 */
public class EplStateChangePermissionModifier extends UnieppStateChangePermissionModifier
{
    private static final Map<Class, CoreCollectionUtils.Pair<String, String>> CLASS_PERMISSION_MAP =
            ImmutableMap.<Class, CoreCollectionUtils.Pair<String, String>>of(
                    EplOrgUnitSummary.class, new CoreCollectionUtils.Pair<>("eplOrgUnitSummary", "Объект «Расчет учебной нагрузки на читающем подразделении»"));

    @Override
    protected String getModuleName()
    {
        return "uniepp_load";
    }

    @Override
    protected String getConfigName()
    {
        return "uniepp_load-stateChange-config";
    }

    @Override
    protected Map<Class, CoreCollectionUtils.Pair<String, String>> getClassPermissionMap()
    {
        return CLASS_PERMISSION_MAP;
    }
}