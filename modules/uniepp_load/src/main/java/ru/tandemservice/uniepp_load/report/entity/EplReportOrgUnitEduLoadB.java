package ru.tandemservice.uniepp_load.report.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.uniepp_load.report.bo.EplReport.ui.OrgUnitEduLoadBAdd.EplReportOrgUnitEduLoadBAdd;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.logic.IEplReport;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.logic.IEplStorableReportDesc;
import ru.tandemservice.uniepp_load.report.entity.gen.*;

import java.util.Arrays;
import java.util.List;

/** @see ru.tandemservice.uniepp_load.report.entity.gen.EplReportOrgUnitEduLoadBGen */
public class EplReportOrgUnitEduLoadB extends EplReportOrgUnitEduLoadBGen implements IEplReport
{
    private static final List<String> VIEW_PROP_LIST = Arrays.asList(EplReportOrgUnitEduLoadBGen.L_EDU_YEAR, P_STUDENT_SUMMARY, P_ORG_UNIT);

    public static final IEplStorableReportDesc DESC = new IEplStorableReportDesc() {
        @Override public String getReportKey() { return EplReportOrgUnitEduLoadB.class.getSimpleName(); }
        @Override public Class<? extends IEplReport> getReportClass() { return EplReportOrgUnitEduLoadB.class; }
        @Override public List<String> getPubPropertyList() { return VIEW_PROP_LIST; }
        @Override public List<String> getListPropertyList() { return VIEW_PROP_LIST; }
        @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return EplReportOrgUnitEduLoadBAdd.class; }
    };

    @Override
    public IEplStorableReportDesc getDesc()
    {
        return DESC;
    }
}