package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper;

/**
 * Created by nsvetlov on 19.12.2016.
 */
public enum EplRecalculateSummaryKind
{
    CHECK, // Только проверка
    PARTIAL_CHECK, // Частичная проверка
    FULL, // Полный перерасчет
}
