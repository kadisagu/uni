/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplGroup.ui.EditTitles;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.utils.DQLSimple;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniepp_load.base.bo.EplGroup.EplGroupManager;
import ru.tandemservice.uniepp_load.base.bo.EplGroup.logic.EplGroupWrapper;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTabUI;

import java.util.List;

/**
 * @author nvankov
 * @since 3/3/15
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "summaryHolder.id", required = true),
        @Bind(key = EplOrgUnitTabUI.BIND_ORG_UNIT_ID, binding = "orgUnitHolder.id")
})
public class EplGroupEditTitlesUI extends UIPresenter
{
    public static final String PARAM_FORMATIVE_ORG_UNIT = "formativeOrgUnit";

    private EntityHolder<EplStudentSummary> summaryHolder = new EntityHolder<EplStudentSummary>();
    private EntityHolder<OrgUnit> _orgUnitHolder = new EntityHolder<>();

    private List<EplGroupWrapper> _wrappers = Lists.newArrayList();
    private EplGroupWrapper _currentWrapper;

    public void onComponentRefresh()
    {
        getSummaryHolder().refresh();
        getOrgUnitHolder().refresh();

        if (null != getOrgUnitHolder().getId())
        {
            getSettings().set(PARAM_FORMATIVE_ORG_UNIT, getOrgUnitHolder().getValue());
            saveSettings();
        }
        onChangeFilterParams();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("studentSummaryId", getSummary().getId());
        dataSource.putAll(_uiSettings.getAsMap(true, PARAM_FORMATIVE_ORG_UNIT, "developForm_title", "course_title"));
    }

    public void onChangeFilterParams()
    {
        saveSettings();
        OrgUnit formativeOrgUnit = getSettings().get(PARAM_FORMATIVE_ORG_UNIT);
        DevelopForm developForm = getSettings().get("developForm_title");

        _wrappers.clear();

        if (formativeOrgUnit == null || developForm == null) {
            return;
        }
        _wrappers.addAll(EplGroupManager.instance().dao().getEplGroupWrappers(getSummary().getId(), _uiSettings.getAsMap(true, PARAM_FORMATIVE_ORG_UNIT, "developForm_title", "course_title", "compensationSource_title")));
    }

    public void onClickSave()
    {
        if (validateAndApply()) deactivate();
    }

    public void onClickApply()
    {
        if (validateAndApply()) _uiSupport.doRefresh();
    }

    private boolean validateAndApply()
    {
        for (EplGroupWrapper wrapper : _wrappers)
        {
            String title = wrapper.getGroupTitle();
            for (EplStudent student : wrapper.getStudents())
            {
                EplGroup group = student.getGroup();

                if (!title.equals(group.getTitle()))
                {
                    EplGroup foundGroup = new DQLSimple<>(EplGroup.class)
                            .where(EplGroup.summary(), group.getSummary())
                            .where(EplGroup.course(), group.getCourse())
                            .where(EplGroup.title(), title)
                            .whereNot(EplGroup.id(), group.getId())
                            .get();

                    if (null != foundGroup)
                    {
                        _uiSupport.error("В сводку уже добавлена группа «" + foundGroup.getTitle() + "» " + foundGroup.getCourse() + " курса.", getFieldId(student));
                    }
                }
            }
        }

        if (getUserContext().getErrorCollector().hasErrors()) return false;
        else
        {
            EplGroupManager.instance().dao().doUpdateEplGroups(getSummary().getId(), _wrappers);
            return true;
        }
    }

    // getters and setters
    //
    public EplStudentSummary getSummary()
    {
        return getSummaryHolder().getValue();
    }

    public String getCurrentId()
    {
        return getFieldId(_currentWrapper.getStudents().get(0));
    }

    public String getFieldId(EplStudent student)
    {
        return student.getGroup().getId() + "_" + student.getEducationOrgUnit().getId() + "_" + student.getDevelopGrid().getId();
    }

    public boolean isHasOrgUnit()
    {
        return null != _orgUnitHolder.getId();
    }

    public boolean isShowSelectWarn()
    {
        OrgUnit formativeOrgUnit = getSettings().get(PARAM_FORMATIVE_ORG_UNIT);
        DevelopForm developForm = getSettings().get("developForm_title");

        return formativeOrgUnit == null || developForm == null;
    }

    public EntityHolder<EplStudentSummary> getSummaryHolder()
    {
        return summaryHolder;
    }

    public EntityHolder<OrgUnit> getOrgUnitHolder()
    {
        return _orgUnitHolder;
    }

    public List<EplGroupWrapper> getWrappers()
    {
        return _wrappers;
    }

    public EplGroupWrapper getCurrentWrapper()
    {
        return _currentWrapper;
    }

    public void setCurrentWrapper(EplGroupWrapper currentWrapper)
    {
        _currentWrapper = currentWrapper;
    }

    @Override
    public String getSettingsKey()
    {
        final String settingsKey = super.getSettingsKey();
        return getOrgUnitHolder().getId() != null ? settingsKey + getOrgUnitHolder().getId() : settingsKey;
    }
}
