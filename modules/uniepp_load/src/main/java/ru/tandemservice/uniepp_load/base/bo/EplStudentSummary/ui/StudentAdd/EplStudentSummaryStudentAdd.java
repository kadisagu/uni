/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.StudentAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.base.bo.UniOrgUnit.logic.OrgUnitByKindComboDataSourceHandler;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.catalog.entity.EplCompensationSource;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 10/27/14
 */
@Configuration
public class EplStudentSummaryStudentAdd extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS("formOuDS").handler(formOuDSHandler()).addColumn(OrgUnit.fullTitle().s()))
            .addDataSource(selectDS("terrOuDS").handler(terrOuDSHandler()).addColumn(OrgUnit.territorialShortTitle().s()))
            .addDataSource(selectDS("eduHsDS").handler(EducationCatalogsManager.instance().eduHsDSHandler()).addColumn(EducationLevelsHighSchool.fullTitleExtended().s()))
            .addDataSource(selectDS("eduOuDS").handler(eduOuDSHandler()).addColumn(EducationOrgUnit.developCombinationTitle().s()))
            .addDataSource(selectDS("developGridDS").handler(developGridDSHandler()).addColumn(DevelopGrid.title().s()))
            .addDataSource(UniStudentManger.instance().courseDSConfig())
            .addDataSource(selectDS("compSourceDS").handler(EplStudentSummaryManager.instance().compSourceDSHandler()).addColumn(EplCompensationSource.title().s()))
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> formOuDSHandler()
    {
        return new OrgUnitByKindComboDataSourceHandler(getName(), UniDefines.CATALOG_ORGUNIT_KIND_FORMING) {
            @Override protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
                super.prepareConditions(ep);
                ep.dqlBuilder.where(exists(new DQLSelectBuilder()
                    .fromEntity(EducationOrgUnit.class, "eduOu")
                    .where(eq(property("e", "id"), property("eduOu", EducationOrgUnit.formativeOrgUnit().id())))
                    .buildQuery()));
            }
        };
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> terrOuDSHandler()
    {
        return new OrgUnitByKindComboDataSourceHandler(getName(), UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL) {
            @Override protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
                super.prepareConditions(ep);
                ep.dqlBuilder.where(exists(new DQLSelectBuilder()
                    .fromEntity(EducationOrgUnit.class, "eduOu")
                    .where(eq(property("eduOu", EducationOrgUnit.formativeOrgUnit()), commonValue(ep.context.get("formativeOrgUnit"))))
                    .where(eq(property("e", "id"), property("eduOu", EducationOrgUnit.territorialOrgUnit().id())))
                    .buildQuery()));
            }
        };
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eduOuDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EducationOrgUnit.class)
            .where(EducationOrgUnit.formativeOrgUnit(), "formativeOrgUnit")
            .where(EducationOrgUnit.territorialOrgUnit(), "territorialOrgUnit")
            .where(EducationOrgUnit.educationLevelHighSchool(), "educationLevelHighSchool")
            .order(EducationOrgUnit.developForm().code())
            .order(EducationOrgUnit.developPeriod().priority())
            .order(EducationOrgUnit.developCondition().code())
            .order(EducationOrgUnit.developTech().code())
            .filter(EducationOrgUnit.developForm().title())
            .filter(EducationOrgUnit.developPeriod().title())
            .filter(EducationOrgUnit.developCondition().title())
            .filter(EducationOrgUnit.developTech().title())
            ;
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> developGridDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), DevelopGrid.class)
            .where(DevelopGrid.developPeriod(), "developPeriod")
            .order(DevelopGrid.developPeriod().priority())
            .order(DevelopGrid.title())
            .filter(DevelopGrid.title())
            ;
    }

}