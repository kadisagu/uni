/* $Id$ */
package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Alexey Lopatin
 * @since 22.10.2015
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_load_2x9x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // для всех категорий, которые ссылаются сами на себя обнуляем основную категорию
        tool.executeUpdate("update epl_time_category_t set parent_id = null where id in (select id from epl_time_category_t where id = parent_id)");
    }
}