package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic;

/**
 * @author Alexey Lopatin
 * @since 28.06.2016
 */
public enum EplEduGroupType
{
    FLOW("Поток"),
    GROUP("Группа"),
    SUB_GROUP("Подгруппа");

    private final String _title;

    public String getTitle()
    {
        return _title;
    }

    EplEduGroupType(String title)
    {
        _title = title;
    }
}
