/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupSplitEdit;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupSplitTab.EplStudentSummaryGroupSplitTab;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentCount4SplitElement;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Katkov
 * @since 06.04.2016
 */
@Input({
        @Bind(key = EplStudentSummaryGroupSplitTab.DISCIPLINE, binding = "eppRegistryElement.id", required = true),
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eplStudent2WorkPlan.id", required = true)
})
public class EplStudentSummaryGroupSplitEditUI extends UIPresenter
{
    private static final String COUNT_FIELD_PREFIX = "cnt_";

    private EntityHolder<EplStudent2WorkPlan> eplStudent2WorkPlan = new EntityHolder<>();
    private EntityHolder<EppRegistryElement> eppRegistryElement = new EntityHolder<>();
    private List<EplStudentCount4SplitElement> splitElements;
    private EplStudentCount4SplitElement currentSplitElement;
    private EppEduGroupSplitVariant splitVariant;

    @Override
    public void onComponentRefresh()
    {
        getEplStudent2WorkPlan().refresh(EplStudent2WorkPlan.class);
        getEppRegistryElement().refresh(EppRegistryElement.class);

        //элементы деления
        String es4se = "es4se";
        DQLSelectBuilder dqlEs4se = new DQLSelectBuilder()
                .fromEntity(EplStudentCount4SplitElement.class, es4se)
                .where(eq(property(EplStudentCount4SplitElement.student2WorkPlan().id().fromAlias(es4se)), value(getStudent2WorkPlan().getId())))
                .where(eq(property(EplStudentCount4SplitElement.regElement().id().fromAlias(es4se)), value(getRegElement().getId())));
        List<EplStudentCount4SplitElement> splitElements = DataAccessServices.dao().getList(dqlEs4se);
        setSplitElements(splitElements);
        setSplitVariant(getRegElement().getEduGroupSplitVariant());

        // предзаполнение
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppEduGroupSplitBaseElement.class, "s").where(eq(property("s", EppEduGroupSplitBaseElement.splitVariant()), value(getSplitVariant())));
        List<EppEduGroupSplitBaseElement> eppEduGroupSplitBaseElementList = DataAccessServices.dao().getList(dql);
        if (splitElements.size() == 0 && eppEduGroupSplitBaseElementList.size() <= 5) {
            for (EppEduGroupSplitBaseElement object : eppEduGroupSplitBaseElementList) {
                EplStudentCount4SplitElement row = new EplStudentCount4SplitElement();
                row.setRegElement(eppRegistryElement.getValue());
                row.setStudent2WorkPlan(eplStudent2WorkPlan.getValue());
                row.setSplitElement(object);
                row.setStudentCount(0);
                row.setId((long) System.identityHashCode(row));
                getSplitElements().add(row);
            }
        }
        //сортировка
        setSplitElements(getSplitElements().stream()
                .sorted((o1, o2) -> o1.getSplitElement().getSplitElementTitle().compareTo(o2.getSplitElement().getSplitElementTitle()))
                .collect(Collectors.toList()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EplStudentSummaryGroupSplitEdit.PARAM_STUDENT_2_WORK_PLAN, getStudent2WorkPlan());
        dataSource.put(EplStudentSummaryGroupSplitEdit.PARAM_SPLIT_VARIANT, getSplitVariant());

        if (dataSource.getName().equals(EplStudentSummaryGroupSplitEdit.GROUP_SPLIT_VARIANT_DS)) {
            dataSource.put(EplStudentSummaryGroupSplitEdit.PARAM_CURRENT_SPLIT_ELEMENT, getCurrentSplitElement());
            dataSource.put(EplStudentSummaryGroupSplitEdit.PARAM_SPLIT_ELEMENTS, getSplitElements());
        }
    }

    public void onClickDeleteRow()
    {
        EplStudentCount4SplitElement elementToRemove = getSplitElements().stream().filter(o -> o.getId().equals(getListenerParameterAsLong())).findAny().get();
        if (splitElements.remove(elementToRemove)) {
            return;
        }
        throw new IllegalStateException();
    }

    public void onClickApply()
    {
        if (validate()) return;

        EplStudentSummaryManager.instance().dao().saveNewSplitRowList(
                getSplitElements().stream()
                        .filter(eplStudentCount4SplitElement -> eplStudentCount4SplitElement.getStudentCount() > 0)
                        .collect(Collectors.toList()), getRegElement(), getStudent2WorkPlan()
        );
        deactivate();
    }

    public boolean validate()
    {
        ErrorCollector errors = UserContext.getInstance().getErrorCollector();

        Set<Long> check = Sets.newHashSet();

        boolean uniqueError = false;
        int totalCount = 0;
        List<String> ids = Lists.newArrayList();

        for (EplStudentCount4SplitElement row : getSplitElements())
        {
            int count = row.getStudentCount();

            if (count != 0)
            {
                totalCount += count;
                ids.add(COUNT_FIELD_PREFIX + row.getId());
            }
            if (uniqueError || !check.add(row.getSplitElement().getId()))
            {
                uniqueError = true;
            }
        }
        if (uniqueError)
            errors.add("Строки могут быть только уникальными.");
        if (!ids.isEmpty() && getStudent2WorkPlan().getCount() != totalCount)
            errors.add(getConfig().getProperty("ui.info"), ids.toArray(new String[ids.size()]));

        return errors.hasErrors();
    }

    public void onClickAddRow()
    {
        EplStudentCount4SplitElement row = new EplStudentCount4SplitElement();
        row.setRegElement(getRegElement());
        row.setStudent2WorkPlan(getStudent2WorkPlan());
        row.setSplitElement(null);
        row.setStudentCount(0);
        row.setId((long) System.identityHashCode(row));
        getSplitElements().add(row);
    }

    public EplStudent2WorkPlan getStudent2WorkPlan()
    {
        return getEplStudent2WorkPlan().getValue();
    }

    public EppRegistryElement getRegElement()
    {
        return getEppRegistryElement().getValue();
    }

    public String getCurrentRowCountFieldId()
    {
        return COUNT_FIELD_PREFIX + getCurrentSplitElement().getId();
    }

    public EplStudentCount4SplitElement getCurrentSplitElement()
    {
        return currentSplitElement;
    }

    public void setCurrentSplitElement(EplStudentCount4SplitElement currentSplitElement)
    {
        this.currentSplitElement = currentSplitElement;
    }

    public List<EplStudentCount4SplitElement> getSplitElements()
    {
        return splitElements;
    }

    public void setSplitElements(List<EplStudentCount4SplitElement> splitElements)
    {
        this.splitElements = splitElements;
    }

    public EppEduGroupSplitVariant getSplitVariant()
    {
        return splitVariant;
    }

    public void setSplitVariant(EppEduGroupSplitVariant splitVariant)
    {
        this.splitVariant = splitVariant;
    }

    public EntityHolder<EplStudent2WorkPlan> getEplStudent2WorkPlan()
    {
        return eplStudent2WorkPlan;
    }

    public EntityHolder<EppRegistryElement> getEppRegistryElement()
    {
        return eppRegistryElement;
    }
}