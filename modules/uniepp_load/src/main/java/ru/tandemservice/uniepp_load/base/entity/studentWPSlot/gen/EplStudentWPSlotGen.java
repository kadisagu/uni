package ru.tandemservice.uniepp_load.base.entity.studentWPSlot.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * МСРП (план.)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplStudentWPSlotGen extends EntityBase
 implements INaturalIdentifiable<EplStudentWPSlotGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot";
    public static final String ENTITY_NAME = "eplStudentWPSlot";
    public static final int VERSION_HASH = 2021386323;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String L_ACTIVITY_ELEMENT_PART = "activityElementPart";
    public static final String L_YEAR_PART = "yearPart";
    public static final String L_TERM = "term";
    public static final String L_CACHED_COURSE = "cachedCourse";

    private EplStudent _student;     // План. студент
    private EppRegistryElementPart _activityElementPart;     // Мероприятие (элемент реестра), часть
    private YearDistributionPart _yearPart;     // Часть года
    private Term _term;     // Семестр
    private Course _cachedCourse;     // Курс (кэш для запросов)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return План. студент. Свойство не может быть null.
     */
    @NotNull
    public EplStudent getStudent()
    {
        return _student;
    }

    /**
     * @param student План. студент. Свойство не может быть null.
     */
    public void setStudent(EplStudent student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Мероприятие (элемент реестра), часть. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPart getActivityElementPart()
    {
        return _activityElementPart;
    }

    /**
     * @param activityElementPart Мероприятие (элемент реестра), часть. Свойство не может быть null.
     */
    public void setActivityElementPart(EppRegistryElementPart activityElementPart)
    {
        dirty(_activityElementPart, activityElementPart);
        _activityElementPart = activityElementPart;
    }

    /**
     * @return Часть года. Свойство не может быть null.
     */
    @NotNull
    public YearDistributionPart getYearPart()
    {
        return _yearPart;
    }

    /**
     * @param yearPart Часть года. Свойство не может быть null.
     */
    public void setYearPart(YearDistributionPart yearPart)
    {
        dirty(_yearPart, yearPart);
        _yearPart = yearPart;
    }

    /**
     * @return Семестр. Свойство не может быть null.
     */
    @NotNull
    public Term getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр. Свойство не может быть null.
     */
    public void setTerm(Term term)
    {
        dirty(_term, term);
        _term = term;
    }

    /**
     * Курс (кэш для запросов)
     *
     * @return Курс (кэш для запросов). Свойство не может быть null.
     */
    @NotNull
    public Course getCachedCourse()
    {
        return _cachedCourse;
    }

    /**
     * @param cachedCourse Курс (кэш для запросов). Свойство не может быть null.
     */
    public void setCachedCourse(Course cachedCourse)
    {
        dirty(_cachedCourse, cachedCourse);
        _cachedCourse = cachedCourse;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EplStudentWPSlotGen)
        {
            if (withNaturalIdProperties)
            {
                setStudent(((EplStudentWPSlot)another).getStudent());
                setActivityElementPart(((EplStudentWPSlot)another).getActivityElementPart());
                setYearPart(((EplStudentWPSlot)another).getYearPart());
                setTerm(((EplStudentWPSlot)another).getTerm());
            }
            setCachedCourse(((EplStudentWPSlot)another).getCachedCourse());
        }
    }

    public INaturalId<EplStudentWPSlotGen> getNaturalId()
    {
        return new NaturalId(getStudent(), getActivityElementPart(), getYearPart(), getTerm());
    }

    public static class NaturalId extends NaturalIdBase<EplStudentWPSlotGen>
    {
        private static final String PROXY_NAME = "EplStudentWPSlotNaturalProxy";

        private Long _student;
        private Long _activityElementPart;
        private Long _yearPart;
        private Long _term;

        public NaturalId()
        {}

        public NaturalId(EplStudent student, EppRegistryElementPart activityElementPart, YearDistributionPart yearPart, Term term)
        {
            _student = ((IEntity) student).getId();
            _activityElementPart = ((IEntity) activityElementPart).getId();
            _yearPart = ((IEntity) yearPart).getId();
            _term = ((IEntity) term).getId();
        }

        public Long getStudent()
        {
            return _student;
        }

        public void setStudent(Long student)
        {
            _student = student;
        }

        public Long getActivityElementPart()
        {
            return _activityElementPart;
        }

        public void setActivityElementPart(Long activityElementPart)
        {
            _activityElementPart = activityElementPart;
        }

        public Long getYearPart()
        {
            return _yearPart;
        }

        public void setYearPart(Long yearPart)
        {
            _yearPart = yearPart;
        }

        public Long getTerm()
        {
            return _term;
        }

        public void setTerm(Long term)
        {
            _term = term;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EplStudentWPSlotGen.NaturalId) ) return false;

            EplStudentWPSlotGen.NaturalId that = (NaturalId) o;

            if( !equals(getStudent(), that.getStudent()) ) return false;
            if( !equals(getActivityElementPart(), that.getActivityElementPart()) ) return false;
            if( !equals(getYearPart(), that.getYearPart()) ) return false;
            if( !equals(getTerm(), that.getTerm()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStudent());
            result = hashCode(result, getActivityElementPart());
            result = hashCode(result, getYearPart());
            result = hashCode(result, getTerm());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStudent());
            sb.append("/");
            sb.append(getActivityElementPart());
            sb.append("/");
            sb.append(getYearPart());
            sb.append("/");
            sb.append(getTerm());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplStudentWPSlotGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplStudentWPSlot.class;
        }

        public T newInstance()
        {
            return (T) new EplStudentWPSlot();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "activityElementPart":
                    return obj.getActivityElementPart();
                case "yearPart":
                    return obj.getYearPart();
                case "term":
                    return obj.getTerm();
                case "cachedCourse":
                    return obj.getCachedCourse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((EplStudent) value);
                    return;
                case "activityElementPart":
                    obj.setActivityElementPart((EppRegistryElementPart) value);
                    return;
                case "yearPart":
                    obj.setYearPart((YearDistributionPart) value);
                    return;
                case "term":
                    obj.setTerm((Term) value);
                    return;
                case "cachedCourse":
                    obj.setCachedCourse((Course) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "activityElementPart":
                        return true;
                case "yearPart":
                        return true;
                case "term":
                        return true;
                case "cachedCourse":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "activityElementPart":
                    return true;
                case "yearPart":
                    return true;
                case "term":
                    return true;
                case "cachedCourse":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return EplStudent.class;
                case "activityElementPart":
                    return EppRegistryElementPart.class;
                case "yearPart":
                    return YearDistributionPart.class;
                case "term":
                    return Term.class;
                case "cachedCourse":
                    return Course.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplStudentWPSlot> _dslPath = new Path<EplStudentWPSlot>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplStudentWPSlot");
    }
            

    /**
     * @return План. студент. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot#getStudent()
     */
    public static EplStudent.Path<EplStudent> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Мероприятие (элемент реестра), часть. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot#getActivityElementPart()
     */
    public static EppRegistryElementPart.Path<EppRegistryElementPart> activityElementPart()
    {
        return _dslPath.activityElementPart();
    }

    /**
     * @return Часть года. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot#getYearPart()
     */
    public static YearDistributionPart.Path<YearDistributionPart> yearPart()
    {
        return _dslPath.yearPart();
    }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot#getTerm()
     */
    public static Term.Path<Term> term()
    {
        return _dslPath.term();
    }

    /**
     * Курс (кэш для запросов)
     *
     * @return Курс (кэш для запросов). Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot#getCachedCourse()
     */
    public static Course.Path<Course> cachedCourse()
    {
        return _dslPath.cachedCourse();
    }

    public static class Path<E extends EplStudentWPSlot> extends EntityPath<E>
    {
        private EplStudent.Path<EplStudent> _student;
        private EppRegistryElementPart.Path<EppRegistryElementPart> _activityElementPart;
        private YearDistributionPart.Path<YearDistributionPart> _yearPart;
        private Term.Path<Term> _term;
        private Course.Path<Course> _cachedCourse;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return План. студент. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot#getStudent()
     */
        public EplStudent.Path<EplStudent> student()
        {
            if(_student == null )
                _student = new EplStudent.Path<EplStudent>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Мероприятие (элемент реестра), часть. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot#getActivityElementPart()
     */
        public EppRegistryElementPart.Path<EppRegistryElementPart> activityElementPart()
        {
            if(_activityElementPart == null )
                _activityElementPart = new EppRegistryElementPart.Path<EppRegistryElementPart>(L_ACTIVITY_ELEMENT_PART, this);
            return _activityElementPart;
        }

    /**
     * @return Часть года. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot#getYearPart()
     */
        public YearDistributionPart.Path<YearDistributionPart> yearPart()
        {
            if(_yearPart == null )
                _yearPart = new YearDistributionPart.Path<YearDistributionPart>(L_YEAR_PART, this);
            return _yearPart;
        }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot#getTerm()
     */
        public Term.Path<Term> term()
        {
            if(_term == null )
                _term = new Term.Path<Term>(L_TERM, this);
            return _term;
        }

    /**
     * Курс (кэш для запросов)
     *
     * @return Курс (кэш для запросов). Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot#getCachedCourse()
     */
        public Course.Path<Course> cachedCourse()
        {
            if(_cachedCourse == null )
                _cachedCourse = new Course.Path<Course>(L_CACHED_COURSE, this);
            return _cachedCourse;
        }

        public Class getEntityClass()
        {
            return EplStudentWPSlot.class;
        }

        public String getEntityName()
        {
            return "eplStudentWPSlot";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
