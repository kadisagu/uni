package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_load_2x7x0_10to11 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (tool.columnExists("epl_edu_group_t", "activityelementpart_id"))
            tool.renameColumn("epl_edu_group_t", "activityelementpart_id", "registryelementpart_id");

        if (tool.tableExists("epl_edu_group_time_item_t"))
            tool.executeUpdate("delete from epl_edu_group_time_item_t");

        if (tool.tableExists("epl_time_item_t"))
            tool.executeUpdate("delete from epl_time_item_t");

        if (tool.tableExists("epl_edu_group_time_item_t"))
        {
            tool.createColumn("epl_edu_group_time_item_t", new DBColumn("registryelementpart_id", DBType.LONG));
            tool.setColumnNullable("epl_edu_group_time_item_t", "registryelementpart_id", false);
        }
    }
}