package ru.tandemservice.uniepp_load.base.entity.studentWPSlot;

import org.tandemframework.core.common.IEntityDebugTitled;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.gen.EplStudentWP2GTypeSlotGen;

/**
 * МСРП-ВН (план.)
 */
public class EplStudentWP2GTypeSlot extends EplStudentWP2GTypeSlotGen implements IEntityDebugTitled
{
    @Override
    public String getEntityDebugTitle()
    {
        final EplStudent student = getStudentWPSlot().getStudent();
        return UniStringUtils.joinWithSeparator(", ",
            student.getGroup().getTitle(),
            student.getGroup().getCourse().getTitle() + " курс",
            student.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableShortTitle(),
            student.getEducationOrgUnit().getDevelopOrgUnitShortTitle(),
            student.getEducationOrgUnit().getDevelopCombinationTitle(),
            student.getCompensationSource().getTitle(),
            getStudentWPSlot().getActivityElementPart().getNumberWithAbbreviationWithPart()
        );
    }
}