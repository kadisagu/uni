/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.Pub;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreMessageDefines;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.EplIndividualPlanManager;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.EduLoad.EplIndividualPlanEduLoadUI;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.NonEduLoad.EplIndividualPlanNonEduLoadUI;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.Tab.EplIndividualPlanTabUI;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;

import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 03.08.2016
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required = true),
        @Bind(key = EplIndividualPlanPubUI.PARAM_SELECTED_TAB, binding = "selectedTab"),
        @Bind(key = EplIndividualPlanManager.PARAM_PPS_ACTIVE, binding = "ppsActive")
})
@Output({
        @Bind(key = EplIndividualPlanManager.PARAM_PPS_ACTIVE, binding = "ppsActive")
})
public class EplIndividualPlanPubUI extends UIPresenter
{
    public static final String PERMISSION_KEY = "eplIndividualPlanPub";
    public static final String PARAM_SELECTED_TAB = "selectedTab";

    private EntityHolder<EplIndividualPlan> _holder = new EntityHolder<>();
    private List<EplOrgUnitSummary> _ouSummaries;
    private String _selectedTab;
    private boolean _ppsActive;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        _ouSummaries = EplIndividualPlanManager.instance().dao().getOuSummaries(getIndPlan());

        if (isPpsActive())
        {
            Person ppsPerson = PersonManager.instance().dao().getPerson(_uiConfig.getUserContext().getPrincipalContext());
            if (!getIndPlan().getPps().getPerson().equals(ppsPerson))
            {
                throw new ApplicationException(ApplicationRuntime.getProperty(CoreMessageDefines.DENIED_NO_ACCESS));
            }
        }
    }

    public boolean isHasOuSummary()
    {
        return CollectionUtils.isNotEmpty(_ouSummaries);
    }

    public EplIndividualPlan getIndPlan()
    {
        return getHolder().getValue();
    }

    public String getIndPlanPubPermissionKey()
    {
        return EplIndividualPlanManager.getPermissionKeyView(PERMISSION_KEY, _ppsActive);
    }

    public String getIndPlanTabPermissionKey()
    {
        return EplIndividualPlanManager.getPermissionKeyView(EplIndividualPlanTabUI.PERMISSION_KEY, _ppsActive);
    }

    public String getIndPlanEduLoadTabPermissionKey()
    {
        return EplIndividualPlanManager.getPermissionKeyView(EplIndividualPlanEduLoadUI.PERMISSION_KEY, _ppsActive);
    }

    public String getIndPlanNonEduLoadTabPermissionKey()
    {
        return EplIndividualPlanManager.getPermissionKeyView(EplIndividualPlanNonEduLoadUI.PERMISSION_KEY, _ppsActive);
    }

    // getters and setters

    public EntityHolder<EplIndividualPlan> getHolder()
    {
        return _holder;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public boolean isPpsActive()
    {
        return _ppsActive;
    }

    public void setPpsActive(boolean ppsActive)
    {
        _ppsActive = ppsActive;
    }
}
