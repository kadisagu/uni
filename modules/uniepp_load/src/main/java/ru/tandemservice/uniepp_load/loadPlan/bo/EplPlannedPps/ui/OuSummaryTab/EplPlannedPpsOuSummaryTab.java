/* $Id$ */
package ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.OuSummaryTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.logic.EplMaxAdditionalHoursFormatter;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.logic.EplPlannedPpsDSHandler;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.logic.EplRateFormatter;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps;

/**
 * @author nvankov
 * @since 1/27/15
 */
@Configuration
public class EplPlannedPpsOuSummaryTab extends BusinessComponentManager
{
    public static final String PPS_DS = "ppsDS";
    public static final String TIME_WORKERS_DS = "timeWorkerDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PPS_DS, ppsDSColumns(), ppsDSHandler()))
                .addDataSource(searchListDS(TIME_WORKERS_DS, timeWorkerDSColumns(), timeWorkerDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint ppsDSColumns()
    {
        return columnListExtPointBuilder(PPS_DS)
                .addColumn(textColumn(DataWrapper.TITLE, DataWrapper.TITLE).formatter(NewLineFormatter.NOBR_IN_LINES).required(true))
                .addColumn(textColumn(EplPlannedPpsDSHandler.VIEW_DEGREE, EplPlannedPpsDSHandler.VIEW_DEGREE).formatter(CollectionFormatter.COLLECTION_FORMATTER))
                .addColumn(textColumn(EplPlannedPpsDSHandler.VIEW_STATUS, EplPlannedPpsDSHandler.VIEW_STATUS).formatter(CollectionFormatter.COLLECTION_FORMATTER))
                .addColumn(textColumn(EplPlannedPps.L_POST, EplPlannedPps.post().title()))
                .addColumn(textColumn(EplPlannedPps.L_TYPE, EplPlannedPps.type().shortTitle()))
                .addColumn(textColumn(EplPlannedPps.L_PPS_ENTRY, EplPlannedPps.ppsEntry().titleFioInfoOrgUnit()))
                .addColumn(publisherColumn(EplPlannedPpsDSHandler.VIEW_EMPLOYEE_POSTS, EmployeePost.typedTitle())
                        .entityListProperty(EplPlannedPpsDSHandler.VIEW_EMPLOYEE_POSTS)
                        .formatter(CollectionFormatter.COLLECTION_FORMATTER))
                .addColumn(headerColumn(EplPlannedPpsDSHandler.VIEW_SUMMARY_DATA)
                .addSubColumn(textColumn(EplPlannedPps.P_STAFF_RATE_AS_DOUBLE, EplPlannedPps.P_STAFF_RATE_AS_DOUBLE).formatter(new EplRateFormatter()).width("40px").required(true).create())
                .addSubColumn(textColumn(EplPlannedPpsDSHandler.ADDITIONAL_TIME_AMOUNT, EplPlannedPpsDSHandler.ADDITIONAL_TIME_AMOUNT).formatter(EplMaxAdditionalHoursFormatter.INSTANCE).width("40px").required(true).create())
                .addSubColumn(headerColumn(EplPlannedPpsDSHandler.VIEW_TIME_AMOUNT)
                        .addSubColumn(textColumn(EplPlannedPpsDSHandler.VIEW_RATE, EplPlannedPpsDSHandler.VIEW_RATE).formatter(RawFormatter.INSTANCE).width("40px").required(true).create())
                        .addSubColumn(textColumn(EplPlannedPpsDSHandler.VIEW_OVERRATE, EplPlannedPpsDSHandler.VIEW_OVERRATE).formatter(RawFormatter.INSTANCE).width("40px").required(true).create())
                        .addSubColumn(textColumn(EplPlannedPpsDSHandler.VIEW_TOTAL, EplPlannedPpsDSHandler.VIEW_TOTAL).formatter(RawFormatter.INSTANCE).width("40px").required(true).create()).create()
                ))
                .addColumn(headerColumn(EplPlannedPpsDSHandler.VIEW_STUDENT_SUMMARY_DATA)
                .addSubColumn(textColumn(EplPlannedPpsDSHandler.STAFF_RATE_SUM, EplPlannedPpsDSHandler.STAFF_RATE_SUM).formatter(new EplRateFormatter()).width("40px").create())
                .addSubColumn(textColumn(EplPlannedPpsDSHandler.ADDITIONAL_TIME_AMOUNT_SUM, EplPlannedPpsDSHandler.ADDITIONAL_TIME_AMOUNT_SUM).formatter(EplMaxAdditionalHoursFormatter.INSTANCE).width("40px").create())
                .addSubColumn(headerColumn(EplPlannedPpsDSHandler.VIEW_TIME_AMOUNT)
                        .addSubColumn(textColumn(EplPlannedPpsDSHandler.VIEW_RATE_SUM, EplPlannedPpsDSHandler.VIEW_RATE_SUM).formatter(RawFormatter.INSTANCE).width("40px").create())
                        .addSubColumn(textColumn(EplPlannedPpsDSHandler.VIEW_OVERRATE_SUM, EplPlannedPpsDSHandler.VIEW_OVERRATE_SUM).formatter(RawFormatter.INSTANCE).width("40px").create())
                        .addSubColumn(textColumn(EplPlannedPpsDSHandler.VIEW_TOTAL_SUM, EplPlannedPpsDSHandler.VIEW_TOTAL_SUM).formatter(RawFormatter.INSTANCE).width("40px").create()).create()
                ))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEditPps").disabled("ui:disabled").permissionKey("eplOuSummaryPpsTabPpsEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("ppsDS.delete.alert", DataWrapper.TITLE)).disabled("ui:disabled").permissionKey("eplOuSummaryPpsTabPpsDelete"))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler ppsDSHandler()
    {
        return new EplPlannedPpsDSHandler(getName(), false);
    }

    @Bean
    public ColumnListExtPoint timeWorkerDSColumns()
    {
        return columnListExtPointBuilder(TIME_WORKERS_DS)
                .addColumn(textColumn(DataWrapper.TITLE, DataWrapper.TITLE).formatter(NewLineFormatter.NOBR_IN_LINES).required(true))
                .addColumn(textColumn(EplPlannedPpsDSHandler.VIEW_DEGREE, EplPlannedPpsDSHandler.VIEW_DEGREE).formatter(CollectionFormatter.COLLECTION_FORMATTER))
                .addColumn(textColumn(EplPlannedPpsDSHandler.VIEW_STATUS, EplPlannedPpsDSHandler.VIEW_STATUS).formatter(CollectionFormatter.COLLECTION_FORMATTER))
                .addColumn(textColumn(EplPlannedPps.L_TYPE, EplPlannedPps.type().shortTitle()))
                .addColumn(textColumn(EplPlannedPps.L_PPS_ENTRY, EplPlannedPps.ppsEntry().titleFioInfoOrgUnit()))
                .addColumn(publisherColumn(EplPlannedPpsDSHandler.VIEW_TIME_WORKERS, "titleInfo")
                        .entityListProperty(EplPlannedPpsDSHandler.VIEW_TIME_WORKERS)
                        .formatter(CollectionFormatter.COLLECTION_FORMATTER))
                .addColumn(textColumn(EplPlannedPps.P_MAX_TIME_AMOUNT, EplPlannedPps.P_MAX_TIME_AMOUNT).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).width("40px").required(true))
                .addColumn(textColumn(EplPlannedPpsDSHandler.VIEW_TIME_AMOUNT, EplPlannedPpsDSHandler.VIEW_TIME_AMOUNT).formatter(RawFormatter.INSTANCE).width("40px").required(true))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEditTimeWorker").disabled("ui:disabled").permissionKey("eplOuSummaryPpsTabTimeWorkerEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("timeWorkerDS.delete.alert", DataWrapper.TITLE)).disabled("ui:disabled").permissionKey("eplOuSummaryPpsTabTimeWorkerDelete"))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler timeWorkerDSHandler()
    {
        return new EplPlannedPpsDSHandler(getName(), true);
    }
}



    