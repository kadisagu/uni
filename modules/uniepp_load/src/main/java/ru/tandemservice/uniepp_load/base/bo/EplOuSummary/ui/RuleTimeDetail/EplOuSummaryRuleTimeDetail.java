/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.RuleTimeDetail;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;

import static ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager.DS_YEAR_PART;

/**
 * @author oleyba
 * @since 12/12/14
 */
@Configuration
public class EplOuSummaryRuleTimeDetail extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_YEAR_PART, getName(), YearDistributionPart.useSelectDSHandler(getName()))
                                       .treeable(true).valueStyleSource(EducationCatalogsManager.getYearPartValueStyleSource()))
                .create();
    }
}