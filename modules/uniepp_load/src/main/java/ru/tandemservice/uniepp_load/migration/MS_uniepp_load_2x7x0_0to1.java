/* $Id:$ */
package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author oleyba
 * @since 11/27/14
 */
public class MS_uniepp_load_2x7x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (!tool.tableExists("epl_time_rule_t")) return;
        tool.executeUpdate("delete from epl_time_rule_edugrp_base_t");
        tool.executeUpdate("delete from epl_time_rule_edugrp_simple_t");
		tool.executeUpdate("delete from epl_time_rule_t");
        tool.executeUpdate("delete from epl_time_category_t");
    }
}
