package ru.tandemservice.uniepp_load.base.entity.studentSummary;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.gen.EplGroupGen;

/**
 * Планируемая академ. группа
 */
public class EplGroup extends EplGroupGen
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId) {
        return new EntityComboDataSourceHandler(ownerId, EplGroup.class)
            .titleProperty(EplGroup.title().s())
            .filter(EplGroup.title())
            .order(EplGroup.title());
    }
}