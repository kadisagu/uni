/* $Id:$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.ui.SimpleAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemAddEditUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeItemVariantGrouping;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleSimple;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplTimeItemVariantGroupingCodes;

/**
 * @author oleyba
 * @since 11/29/14
 */
@Input({
    @Bind(key = "catalogItemId", binding = "holder.id"),
    @Bind(key = DefaultCatalogAddEditModel.CATALOG_CODE, binding = "catalogCode")
})
public class EplTimeRuleSimpleAddEditUI extends BaseCatalogItemAddEditUI<EplTimeRuleSimple>
{
    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        getProperties().remove(EplTimeRuleSimple.P_DESCRIPTION);
        getProperties().remove(EplTimeRuleSimple.P_PARAMETER_NAME);
    }

    @Override
    public String getAdditionalPropertiesPageName()
    {
        return getClass().getPackage().getName() + ".AdditProps";
    }

    @Override
    public void onClickApply()
    {
        if (!getCatalogItem().isParameterNeeded()) {
            getCatalogItem().setParameterName(null);
        }
        getCatalogItem().setGrouping(DataAccessServices.dao().getByCode(EplTimeItemVariantGrouping.class, EplTimeItemVariantGroupingCodes.BY_TIME_RULE));
        super.onClickApply();
    }
}