package ru.tandemservice.uniepp_load.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Предельные значения нагрузки преподавателей
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplSettingsPpsLoadGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad";
    public static final String ENTITY_NAME = "eplSettingsPpsLoad";
    public static final int VERSION_HASH = 1988481492;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_YEAR = "eduYear";
    public static final String L_POST = "post";
    public static final String P_HOURS_MIN_AS_LONG = "hoursMinAsLong";
    public static final String P_HOURS_MAX_AS_LONG = "hoursMaxAsLong";
    public static final String P_HOURS_ADDITIONAL_AS_LONG = "hoursAdditionalAsLong";
    public static final String P_STAFF_RATE_FACTOR = "staffRateFactor";
    public static final String P_STAFF_NUMBER_FACTOR = "staffNumberFactor";
    public static final String P_LEVEL = "level";
    public static final String P_ENTITY = "entity";
    public static final String P_HOURS_ADDITIONAL = "hoursAdditional";
    public static final String P_HOURS_MAX = "hoursMax";
    public static final String P_HOURS_MIN = "hoursMin";

    private EducationYear _eduYear;     // Учебный год
    private PostBoundedWithQGandQL _post;     // Должность
    private long _hoursMinAsLong;     // Минимальное число часов
    private long _hoursMaxAsLong;     // Максимальное число часов
    private long _hoursAdditionalAsLong;     // Максимальное число дополнительных часов почасовой нагрузки
    private boolean _staffRateFactor;     // Умножать на долю ставки
    private boolean _staffNumberFactor;     // Умножать на число сотрудников
    private long _level = 0;     // Уровень
    private Long _entity;     // Задано на

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    /**
     * @param eduYear Учебный год. Свойство не может быть null.
     */
    public void setEduYear(EducationYear eduYear)
    {
        dirty(_eduYear, eduYear);
        _eduYear = eduYear;
    }

    /**
     * @return Должность.
     */
    public PostBoundedWithQGandQL getPost()
    {
        return _post;
    }

    /**
     * @param post Должность.
     */
    public void setPost(PostBoundedWithQGandQL post)
    {
        dirty(_post, post);
        _post = post;
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Минимальное число часов. Свойство не может быть null.
     */
    @NotNull
    public long getHoursMinAsLong()
    {
        return _hoursMinAsLong;
    }

    /**
     * @param hoursMinAsLong Минимальное число часов. Свойство не может быть null.
     */
    public void setHoursMinAsLong(long hoursMinAsLong)
    {
        dirty(_hoursMinAsLong, hoursMinAsLong);
        _hoursMinAsLong = hoursMinAsLong;
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Максимальное число часов. Свойство не может быть null.
     */
    @NotNull
    public long getHoursMaxAsLong()
    {
        return _hoursMaxAsLong;
    }

    /**
     * @param hoursMaxAsLong Максимальное число часов. Свойство не может быть null.
     */
    public void setHoursMaxAsLong(long hoursMaxAsLong)
    {
        dirty(_hoursMaxAsLong, hoursMaxAsLong);
        _hoursMaxAsLong = hoursMaxAsLong;
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Максимальное число дополнительных часов почасовой нагрузки. Свойство не может быть null.
     */
    @NotNull
    public long getHoursAdditionalAsLong()
    {
        return _hoursAdditionalAsLong;
    }

    /**
     * @param hoursAdditionalAsLong Максимальное число дополнительных часов почасовой нагрузки. Свойство не может быть null.
     */
    public void setHoursAdditionalAsLong(long hoursAdditionalAsLong)
    {
        dirty(_hoursAdditionalAsLong, hoursAdditionalAsLong);
        _hoursAdditionalAsLong = hoursAdditionalAsLong;
    }

    /**
     * @return Умножать на долю ставки. Свойство не может быть null.
     */
    @NotNull
    public boolean isStaffRateFactor()
    {
        return _staffRateFactor;
    }

    /**
     * @param staffRateFactor Умножать на долю ставки. Свойство не может быть null.
     */
    public void setStaffRateFactor(boolean staffRateFactor)
    {
        dirty(_staffRateFactor, staffRateFactor);
        _staffRateFactor = staffRateFactor;
    }

    /**
     * @return Умножать на число сотрудников. Свойство не может быть null.
     */
    @NotNull
    public boolean isStaffNumberFactor()
    {
        return _staffNumberFactor;
    }

    /**
     * @param staffNumberFactor Умножать на число сотрудников. Свойство не может быть null.
     */
    public void setStaffNumberFactor(boolean staffNumberFactor)
    {
        dirty(_staffNumberFactor, staffNumberFactor);
        _staffNumberFactor = staffNumberFactor;
    }

    /**
     * Применяется настройка с самым высоким уровнем.
     *
     * @return Уровень. Свойство не может быть null.
     */
    @NotNull
    public long getLevel()
    {
        return _level;
    }

    /**
     * @param level Уровень. Свойство не может быть null.
     */
    public void setLevel(long level)
    {
        dirty(_level, level);
        _level = level;
    }

    /**
     * Id сводки или расчета на которые задана настройка
     *
     * @return Задано на.
     */
    public Long getEntity()
    {
        return _entity;
    }

    /**
     * @param entity Задано на.
     */
    public void setEntity(Long entity)
    {
        dirty(_entity, entity);
        _entity = entity;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EplSettingsPpsLoadGen)
        {
            setEduYear(((EplSettingsPpsLoad)another).getEduYear());
            setPost(((EplSettingsPpsLoad)another).getPost());
            setHoursMinAsLong(((EplSettingsPpsLoad)another).getHoursMinAsLong());
            setHoursMaxAsLong(((EplSettingsPpsLoad)another).getHoursMaxAsLong());
            setHoursAdditionalAsLong(((EplSettingsPpsLoad)another).getHoursAdditionalAsLong());
            setStaffRateFactor(((EplSettingsPpsLoad)another).isStaffRateFactor());
            setStaffNumberFactor(((EplSettingsPpsLoad)another).isStaffNumberFactor());
            setLevel(((EplSettingsPpsLoad)another).getLevel());
            setEntity(((EplSettingsPpsLoad)another).getEntity());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplSettingsPpsLoadGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplSettingsPpsLoad.class;
        }

        public T newInstance()
        {
            return (T) new EplSettingsPpsLoad();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduYear":
                    return obj.getEduYear();
                case "post":
                    return obj.getPost();
                case "hoursMinAsLong":
                    return obj.getHoursMinAsLong();
                case "hoursMaxAsLong":
                    return obj.getHoursMaxAsLong();
                case "hoursAdditionalAsLong":
                    return obj.getHoursAdditionalAsLong();
                case "staffRateFactor":
                    return obj.isStaffRateFactor();
                case "staffNumberFactor":
                    return obj.isStaffNumberFactor();
                case "level":
                    return obj.getLevel();
                case "entity":
                    return obj.getEntity();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduYear":
                    obj.setEduYear((EducationYear) value);
                    return;
                case "post":
                    obj.setPost((PostBoundedWithQGandQL) value);
                    return;
                case "hoursMinAsLong":
                    obj.setHoursMinAsLong((Long) value);
                    return;
                case "hoursMaxAsLong":
                    obj.setHoursMaxAsLong((Long) value);
                    return;
                case "hoursAdditionalAsLong":
                    obj.setHoursAdditionalAsLong((Long) value);
                    return;
                case "staffRateFactor":
                    obj.setStaffRateFactor((Boolean) value);
                    return;
                case "staffNumberFactor":
                    obj.setStaffNumberFactor((Boolean) value);
                    return;
                case "level":
                    obj.setLevel((Long) value);
                    return;
                case "entity":
                    obj.setEntity((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduYear":
                        return true;
                case "post":
                        return true;
                case "hoursMinAsLong":
                        return true;
                case "hoursMaxAsLong":
                        return true;
                case "hoursAdditionalAsLong":
                        return true;
                case "staffRateFactor":
                        return true;
                case "staffNumberFactor":
                        return true;
                case "level":
                        return true;
                case "entity":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduYear":
                    return true;
                case "post":
                    return true;
                case "hoursMinAsLong":
                    return true;
                case "hoursMaxAsLong":
                    return true;
                case "hoursAdditionalAsLong":
                    return true;
                case "staffRateFactor":
                    return true;
                case "staffNumberFactor":
                    return true;
                case "level":
                    return true;
                case "entity":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduYear":
                    return EducationYear.class;
                case "post":
                    return PostBoundedWithQGandQL.class;
                case "hoursMinAsLong":
                    return Long.class;
                case "hoursMaxAsLong":
                    return Long.class;
                case "hoursAdditionalAsLong":
                    return Long.class;
                case "staffRateFactor":
                    return Boolean.class;
                case "staffNumberFactor":
                    return Boolean.class;
                case "level":
                    return Long.class;
                case "entity":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplSettingsPpsLoad> _dslPath = new Path<EplSettingsPpsLoad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplSettingsPpsLoad");
    }
            

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getEduYear()
     */
    public static EducationYear.Path<EducationYear> eduYear()
    {
        return _dslPath.eduYear();
    }

    /**
     * @return Должность.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getPost()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> post()
    {
        return _dslPath.post();
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Минимальное число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getHoursMinAsLong()
     */
    public static PropertyPath<Long> hoursMinAsLong()
    {
        return _dslPath.hoursMinAsLong();
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Максимальное число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getHoursMaxAsLong()
     */
    public static PropertyPath<Long> hoursMaxAsLong()
    {
        return _dslPath.hoursMaxAsLong();
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Максимальное число дополнительных часов почасовой нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getHoursAdditionalAsLong()
     */
    public static PropertyPath<Long> hoursAdditionalAsLong()
    {
        return _dslPath.hoursAdditionalAsLong();
    }

    /**
     * @return Умножать на долю ставки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#isStaffRateFactor()
     */
    public static PropertyPath<Boolean> staffRateFactor()
    {
        return _dslPath.staffRateFactor();
    }

    /**
     * @return Умножать на число сотрудников. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#isStaffNumberFactor()
     */
    public static PropertyPath<Boolean> staffNumberFactor()
    {
        return _dslPath.staffNumberFactor();
    }

    /**
     * Применяется настройка с самым высоким уровнем.
     *
     * @return Уровень. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getLevel()
     */
    public static PropertyPath<Long> level()
    {
        return _dslPath.level();
    }

    /**
     * Id сводки или расчета на которые задана настройка
     *
     * @return Задано на.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getEntity()
     */
    public static PropertyPath<Long> entity()
    {
        return _dslPath.entity();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getHoursAdditional()
     */
    public static SupportedPropertyPath<Double> hoursAdditional()
    {
        return _dslPath.hoursAdditional();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getHoursMax()
     */
    public static SupportedPropertyPath<Double> hoursMax()
    {
        return _dslPath.hoursMax();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getHoursMin()
     */
    public static SupportedPropertyPath<Double> hoursMin()
    {
        return _dslPath.hoursMin();
    }

    public static class Path<E extends EplSettingsPpsLoad> extends EntityPath<E>
    {
        private EducationYear.Path<EducationYear> _eduYear;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _post;
        private PropertyPath<Long> _hoursMinAsLong;
        private PropertyPath<Long> _hoursMaxAsLong;
        private PropertyPath<Long> _hoursAdditionalAsLong;
        private PropertyPath<Boolean> _staffRateFactor;
        private PropertyPath<Boolean> _staffNumberFactor;
        private PropertyPath<Long> _level;
        private PropertyPath<Long> _entity;
        private SupportedPropertyPath<Double> _hoursAdditional;
        private SupportedPropertyPath<Double> _hoursMax;
        private SupportedPropertyPath<Double> _hoursMin;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getEduYear()
     */
        public EducationYear.Path<EducationYear> eduYear()
        {
            if(_eduYear == null )
                _eduYear = new EducationYear.Path<EducationYear>(L_EDU_YEAR, this);
            return _eduYear;
        }

    /**
     * @return Должность.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getPost()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> post()
        {
            if(_post == null )
                _post = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST, this);
            return _post;
        }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Минимальное число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getHoursMinAsLong()
     */
        public PropertyPath<Long> hoursMinAsLong()
        {
            if(_hoursMinAsLong == null )
                _hoursMinAsLong = new PropertyPath<Long>(EplSettingsPpsLoadGen.P_HOURS_MIN_AS_LONG, this);
            return _hoursMinAsLong;
        }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Максимальное число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getHoursMaxAsLong()
     */
        public PropertyPath<Long> hoursMaxAsLong()
        {
            if(_hoursMaxAsLong == null )
                _hoursMaxAsLong = new PropertyPath<Long>(EplSettingsPpsLoadGen.P_HOURS_MAX_AS_LONG, this);
            return _hoursMaxAsLong;
        }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Максимальное число дополнительных часов почасовой нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getHoursAdditionalAsLong()
     */
        public PropertyPath<Long> hoursAdditionalAsLong()
        {
            if(_hoursAdditionalAsLong == null )
                _hoursAdditionalAsLong = new PropertyPath<Long>(EplSettingsPpsLoadGen.P_HOURS_ADDITIONAL_AS_LONG, this);
            return _hoursAdditionalAsLong;
        }

    /**
     * @return Умножать на долю ставки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#isStaffRateFactor()
     */
        public PropertyPath<Boolean> staffRateFactor()
        {
            if(_staffRateFactor == null )
                _staffRateFactor = new PropertyPath<Boolean>(EplSettingsPpsLoadGen.P_STAFF_RATE_FACTOR, this);
            return _staffRateFactor;
        }

    /**
     * @return Умножать на число сотрудников. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#isStaffNumberFactor()
     */
        public PropertyPath<Boolean> staffNumberFactor()
        {
            if(_staffNumberFactor == null )
                _staffNumberFactor = new PropertyPath<Boolean>(EplSettingsPpsLoadGen.P_STAFF_NUMBER_FACTOR, this);
            return _staffNumberFactor;
        }

    /**
     * Применяется настройка с самым высоким уровнем.
     *
     * @return Уровень. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getLevel()
     */
        public PropertyPath<Long> level()
        {
            if(_level == null )
                _level = new PropertyPath<Long>(EplSettingsPpsLoadGen.P_LEVEL, this);
            return _level;
        }

    /**
     * Id сводки или расчета на которые задана настройка
     *
     * @return Задано на.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getEntity()
     */
        public PropertyPath<Long> entity()
        {
            if(_entity == null )
                _entity = new PropertyPath<Long>(EplSettingsPpsLoadGen.P_ENTITY, this);
            return _entity;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getHoursAdditional()
     */
        public SupportedPropertyPath<Double> hoursAdditional()
        {
            if(_hoursAdditional == null )
                _hoursAdditional = new SupportedPropertyPath<Double>(EplSettingsPpsLoadGen.P_HOURS_ADDITIONAL, this);
            return _hoursAdditional;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getHoursMax()
     */
        public SupportedPropertyPath<Double> hoursMax()
        {
            if(_hoursMax == null )
                _hoursMax = new SupportedPropertyPath<Double>(EplSettingsPpsLoadGen.P_HOURS_MAX, this);
            return _hoursMax;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad#getHoursMin()
     */
        public SupportedPropertyPath<Double> hoursMin()
        {
            if(_hoursMin == null )
                _hoursMin = new SupportedPropertyPath<Double>(EplSettingsPpsLoadGen.P_HOURS_MIN, this);
            return _hoursMin;
        }

        public Class getEntityClass()
        {
            return EplSettingsPpsLoad.class;
        }

        public String getEntityName()
        {
            return "eplSettingsPpsLoad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract double getHoursAdditional();

    public abstract double getHoursMax();

    public abstract double getHoursMin();
}
