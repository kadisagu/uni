/* $Id:$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.ui.EduGroupScriptList;

import org.tandemframework.common.catalog.CatalogDefines;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.ui.ScriptEdit.CommonScriptEdit;
import org.tandemframework.shared.commonbase.base.bo.Common.ui.ScriptEdit.CommonScriptEditUI;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.DynamicPub.CatalogDynamicPubUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uni.util.SafeSimpleColumn;
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.EplTimeRuleManager;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript;

/**
 * @author oleyba
 * @since 11/29/14
 */
@State({
    @Bind(key = DefaultCatalogPubModel.CATALOG_CODE, binding = "catalogCode")
})
public class EplTimeRuleEduGroupScriptListUI extends CatalogDynamicPubUI
{
    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        boolean hasCounter = ISharedBaseDao.instance.get().existsEntity(EplTimeRuleEduGroupScript.class, EplTimeRuleEduGroupScript.counterEnabled().s(), Boolean.TRUE);
        if (hasCounter)
            ContextLocal.getInfoCollector().add(getConfig().getProperty("ui.info"));
    }

    @Override
    protected void addActionColumns(DynamicListDataSource<ICatalogItem> dataSource)
    {
        int columnIndex = dataSource.getColumn(EplTimeRuleEduGroupScript.P_DESCRIPTION).getNumber();
        dataSource.addColumn(new SimpleColumn("Группировка часов", EplTimeRuleEduGroupScript.grouping().shortTitle().s()).setWidth("110px").setClickable(false), ++columnIndex);

        columnIndex = dataSource.getColumn(EplTimeRuleEduGroupScript.L_CATEGORY + ".title").getNumber();
        dataSource.addColumn(new SimpleColumn("Ограничить видом УГС", EplTimeRuleEduGroupScript.groupType().title().s()).setOrderable(false).setClickable(false), ++columnIndex);
        dataSource.addColumn(new SimpleColumn("Ограничить видом мероприятия", EplTimeRuleEduGroupScript.regStructure().shortTitle().s()).setOrderable(false).setClickable(false), ++columnIndex);

        dataSource.addColumn(new BooleanColumn("Пользовательский скрипт", EplTimeRuleEduGroupScript.hasUserScript()).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать скрипт", "edit-code", "onEditScript").setPermissionKey(getSec().getPermission("edit")));
        dataSource.addColumn(new ActionColumn("Вернуться к скрипту по умолчанию", "template_revert", "onRevertScript", "Вернуться к скрипту по умолчанию? Пользовательский скрипт будет удален.").setDisabledProperty(EplTimeRuleEduGroupScript.hasNoDefaultScript()).setPermissionKey(getSec().getPermission("edit")));
        super.addActionColumns(dataSource);

        columnIndex = dataSource.getColumn(CatalogDefines.P_ENABLED).getNumber();
        final IEntityHandler counterDisableHandler = entity -> !((EplTimeRuleEduGroupScript) entity).isEnabled();

        dataSource.addColumn(
                new ToggleColumn("Счетчик производительности", EplTimeRuleEduGroupScript.P_COUNTER_ENABLED)
                        .toggleOnListener("onClickChangeCounterEnabled").onLabel("Сделать неиспользуемым")
                        .toggleOffListener("onClickChangeCounterEnabled").offLabel("Сделать используемым")
                        .setDisableHandler(counterDisableHandler)
                        .setHeaderAlign("center")
                        .setOrderable(true)
                        .setWidth(1), ++columnIndex
        );

        dataSource.addColumn(new SafeSimpleColumn("Суммарное время выполнения (с)", EplTimeRuleEduGroupScript.P_TOTAL_TIME_WORK)
        {
            @Override
            public String getContent(final IEntity entity)
            {
                EplTimeRuleEduGroupScript script = (EplTimeRuleEduGroupScript) entity;
                double value = script.isCounterEnabled() ? (double) script.getTotalTimeWork() / 1000 / 1000 : 0;
                return value != 0 ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(value) : "";
            }
        }.setHeaderAlign("center").setOrderable(true).setClickable(false), ++columnIndex);

        dataSource.addColumn(new SafeSimpleColumn("Число вызовов", EplTimeRuleEduGroupScript.P_REQUEST_COUNT)
        {
            @Override
            public String getContent(final IEntity entity)
            {
                EplTimeRuleEduGroupScript script = (EplTimeRuleEduGroupScript) entity;
                int value = script.isCounterEnabled() ? script.getRequestCount() : 0;
                return value != 0 ? String.valueOf(value) : "";
            }
        }.setHeaderAlign("center").setOrderable(true).setClickable(false), ++columnIndex);

        dataSource.addColumn(new SimpleColumn("Среднее время выполнения (мкс)", EplTimeRuleEduGroupScript.P_AVG_TIME_WORK)
        {
            @Override
            public String getContent(final IEntity entity)
            {
                EplTimeRuleEduGroupScript script = (EplTimeRuleEduGroupScript) entity;
                double value = script.isCounterEnabled() && script.getRequestCount() > 0 ? script.getAvgTimeWork() : 0;
                return value != 0 ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(value) : "";
            }
        }.setHeaderAlign("center").setOrderable(false).setClickable(false), ++columnIndex);
    }

    public void onEditScript()
    {
        final EplTimeRuleEduGroupScript script = DataAccessServices.dao().get(EplTimeRuleEduGroupScript.class, getListenerParameterAsLong());
        _uiActivation.asDesktopRoot(CommonScriptEdit.class)
        .parameter(CommonScriptEditUI.PARAM_HOLDER, script.getScriptHolder())
        .activate();
    }

    public void onRevertScript()
    {
        final EplTimeRuleEduGroupScript script = DataAccessServices.dao().get(EplTimeRuleEduGroupScript.class, getListenerParameterAsLong());
        script.setUserScript(null);
        EplTimeRuleManager.instance().timeRuleDao().doSaveUserScript(script);
        ContextLocal.getDesktop().setRefreshScheduled(true);
    }

    public void onClickChangeCounterEnabled()
    {
        EplTimeRuleEduGroupScript script = getEntityByListenerParameterAsLong();
        script.setCounterEnabled(!script.isCounterEnabled());
        DataAccessServices.dao().update(script);
        ContextLocal.getDesktop().setRefreshScheduled(true);
    }

    public void onClickAllCountersActivate()
    {
        EplTimeRuleManager.instance().timeRuleDao().doAllCountersScriptActivate();
        ContextLocal.getDesktop().setRefreshScheduled(true);
    }

    public void onClickAllCountersDeactivate()
    {
        EplTimeRuleManager.instance().timeRuleDao().doAllCountersScriptDeactivate();
        ContextLocal.getDesktop().setRefreshScheduled(true);
    }

    public void onClickAllCountersClear()
    {
        EplTimeRuleManager.instance().timeRuleDao().doCountersScriptClear(null);
        ContextLocal.getDesktop().setRefreshScheduled(true);
    }

    @Override
    public void onClickActivityToggle()
    {
        super.onClickActivityToggle();
        EplTimeRuleEduGroupScript script = getEntityByListenerParameterAsLong();
        if (!script.isEnabled())
        {
            script.setCounterEnabled(false);
            DataAccessServices.dao().update(script);
        }
    }
}