/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.daemon;

import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author oleyba
 * @since 10/21/11
 */
public interface IEplStudentSummaryDaemonBean
{
    final SpringBeanCache<IEplStudentSummaryDaemonBean> instance = new SpringBeanCache<IEplStudentSummaryDaemonBean>(IEplStudentSummaryDaemonBean.class.getName());

    /**
     * Обновляет МСРП
     */
    void updateStudentData();
}
