/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Copy;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.*;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;

import java.util.Date;

/**
 * @author nvankov
 * @since 3/30/15
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id")
})
@Return({
        @Bind(key = "createdSummaryTitle", binding = "title")
})
public class EplStudentSummaryCopyUI extends UIPresenter
{
    private EntityHolder<EplStudentSummary> holder = new EntityHolder<EplStudentSummary>();
    private String _title;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        _title = getSummary().getTitle() + " (от " + DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date()) + ")";
    }

    public void onClickApply() {
        EplStudentSummaryManager.instance().dao().doCopySummary(_title, getSummary().getId(), false);
        deactivate();
    }

    public void onClickCancel()
    {
        _title = null;
        deactivate();
    }

    // getters and setters

    public EntityHolder<EplStudentSummary> getHolder()
    {
        return holder;
    }

    public EplStudentSummary getSummary()
    {
        return getHolder().getValue();
    }

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }
}
