package ru.tandemservice.uniepp_load.loadPlan.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplPlannedPpsType;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Планируемый ППС для распределения нагрузки
 *
 * Ограничения:
 * - dql check constraint "eplPlannedPps_config": Данный констрейнт учитывает только два состояния ppsEntry: когда класс либо относится к PpsEntryByEmployeePost, либо нет (здесь подразумевается,
 *   что ppsEntry хранит ссылку на PpsEntryByTimeworker). Если появится 3я сущность от PpsEntry, то этот констрейнт нужно будет изменить
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplPlannedPpsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps";
    public static final String ENTITY_NAME = "eplPlannedPps";
    public static final int VERSION_HASH = 1773716020;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT_SUMMARY = "orgUnitSummary";
    public static final String L_TYPE = "type";
    public static final String L_POST = "post";
    public static final String P_VACANCY = "vacancy";
    public static final String P_STAFF_RATE_AS_LONG = "staffRateAsLong";
    public static final String P_MAX_TIME_AMOUNT_AS_LONG = "maxTimeAmountAsLong";
    public static final String L_PPS_ENTRY = "ppsEntry";
    public static final String P_DISPLAYABLE_TITLE = "displayableTitle";
    public static final String P_MAX_TIME_AMOUNT = "maxTimeAmount";
    public static final String P_STAFF_RATE_AS_DOUBLE = "staffRateAsDouble";
    public static final String P_STAFF_RATE_AS_STRING = "staffRateAsString";

    private EplOrgUnitSummary _orgUnitSummary;     // Сводка
    private EplPlannedPpsType _type;     // Тип оформления
    private PostBoundedWithQGandQL _post;     // Должность
    private String _vacancy;     // Название вакансии
    private Long _staffRateAsLong;     // Доля ставки
    private long _maxTimeAmountAsLong;     // Ограничение на часы нагрузки
    private PpsEntry _ppsEntry;     // Преподаватель

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сводка. Свойство не может быть null.
     */
    @NotNull
    public EplOrgUnitSummary getOrgUnitSummary()
    {
        return _orgUnitSummary;
    }

    /**
     * @param orgUnitSummary Сводка. Свойство не может быть null.
     */
    public void setOrgUnitSummary(EplOrgUnitSummary orgUnitSummary)
    {
        dirty(_orgUnitSummary, orgUnitSummary);
        _orgUnitSummary = orgUnitSummary;
    }

    /**
     * @return Тип оформления. Свойство не может быть null.
     */
    @NotNull
    public EplPlannedPpsType getType()
    {
        return _type;
    }

    /**
     * @param type Тип оформления. Свойство не может быть null.
     */
    public void setType(EplPlannedPpsType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Должность.
     */
    public PostBoundedWithQGandQL getPost()
    {
        return _post;
    }

    /**
     * @param post Должность.
     */
    public void setPost(PostBoundedWithQGandQL post)
    {
        dirty(_post, post);
        _post = post;
    }

    /**
     * @return Название вакансии.
     */
    @Length(max=255)
    public String getVacancy()
    {
        return _vacancy;
    }

    /**
     * @param vacancy Название вакансии.
     */
    public void setVacancy(String vacancy)
    {
        dirty(_vacancy, vacancy);
        _vacancy = vacancy;
    }

    /**
     * Смещение на 4 знака для дробной части.
     *
     * @return Доля ставки.
     */
    public Long getStaffRateAsLong()
    {
        return _staffRateAsLong;
    }

    /**
     * @param staffRateAsLong Доля ставки.
     */
    public void setStaffRateAsLong(Long staffRateAsLong)
    {
        dirty(_staffRateAsLong, staffRateAsLong);
        _staffRateAsLong = staffRateAsLong;
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Ограничение на часы нагрузки. Свойство не может быть null.
     */
    @NotNull
    public long getMaxTimeAmountAsLong()
    {
        return _maxTimeAmountAsLong;
    }

    /**
     * @param maxTimeAmountAsLong Ограничение на часы нагрузки. Свойство не может быть null.
     */
    public void setMaxTimeAmountAsLong(long maxTimeAmountAsLong)
    {
        dirty(_maxTimeAmountAsLong, maxTimeAmountAsLong);
        _maxTimeAmountAsLong = maxTimeAmountAsLong;
    }

    /**
     * @return Преподаватель.
     */
    public PpsEntry getPpsEntry()
    {
        return _ppsEntry;
    }

    /**
     * @param ppsEntry Преподаватель.
     */
    public void setPpsEntry(PpsEntry ppsEntry)
    {
        dirty(_ppsEntry, ppsEntry);
        _ppsEntry = ppsEntry;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EplPlannedPpsGen)
        {
            setOrgUnitSummary(((EplPlannedPps)another).getOrgUnitSummary());
            setType(((EplPlannedPps)another).getType());
            setPost(((EplPlannedPps)another).getPost());
            setVacancy(((EplPlannedPps)another).getVacancy());
            setStaffRateAsLong(((EplPlannedPps)another).getStaffRateAsLong());
            setMaxTimeAmountAsLong(((EplPlannedPps)another).getMaxTimeAmountAsLong());
            setPpsEntry(((EplPlannedPps)another).getPpsEntry());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplPlannedPpsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplPlannedPps.class;
        }

        public T newInstance()
        {
            return (T) new EplPlannedPps();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnitSummary":
                    return obj.getOrgUnitSummary();
                case "type":
                    return obj.getType();
                case "post":
                    return obj.getPost();
                case "vacancy":
                    return obj.getVacancy();
                case "staffRateAsLong":
                    return obj.getStaffRateAsLong();
                case "maxTimeAmountAsLong":
                    return obj.getMaxTimeAmountAsLong();
                case "ppsEntry":
                    return obj.getPpsEntry();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnitSummary":
                    obj.setOrgUnitSummary((EplOrgUnitSummary) value);
                    return;
                case "type":
                    obj.setType((EplPlannedPpsType) value);
                    return;
                case "post":
                    obj.setPost((PostBoundedWithQGandQL) value);
                    return;
                case "vacancy":
                    obj.setVacancy((String) value);
                    return;
                case "staffRateAsLong":
                    obj.setStaffRateAsLong((Long) value);
                    return;
                case "maxTimeAmountAsLong":
                    obj.setMaxTimeAmountAsLong((Long) value);
                    return;
                case "ppsEntry":
                    obj.setPpsEntry((PpsEntry) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnitSummary":
                        return true;
                case "type":
                        return true;
                case "post":
                        return true;
                case "vacancy":
                        return true;
                case "staffRateAsLong":
                        return true;
                case "maxTimeAmountAsLong":
                        return true;
                case "ppsEntry":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnitSummary":
                    return true;
                case "type":
                    return true;
                case "post":
                    return true;
                case "vacancy":
                    return true;
                case "staffRateAsLong":
                    return true;
                case "maxTimeAmountAsLong":
                    return true;
                case "ppsEntry":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnitSummary":
                    return EplOrgUnitSummary.class;
                case "type":
                    return EplPlannedPpsType.class;
                case "post":
                    return PostBoundedWithQGandQL.class;
                case "vacancy":
                    return String.class;
                case "staffRateAsLong":
                    return Long.class;
                case "maxTimeAmountAsLong":
                    return Long.class;
                case "ppsEntry":
                    return PpsEntry.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplPlannedPps> _dslPath = new Path<EplPlannedPps>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplPlannedPps");
    }
            

    /**
     * @return Сводка. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getOrgUnitSummary()
     */
    public static EplOrgUnitSummary.Path<EplOrgUnitSummary> orgUnitSummary()
    {
        return _dslPath.orgUnitSummary();
    }

    /**
     * @return Тип оформления. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getType()
     */
    public static EplPlannedPpsType.Path<EplPlannedPpsType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Должность.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getPost()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> post()
    {
        return _dslPath.post();
    }

    /**
     * @return Название вакансии.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getVacancy()
     */
    public static PropertyPath<String> vacancy()
    {
        return _dslPath.vacancy();
    }

    /**
     * Смещение на 4 знака для дробной части.
     *
     * @return Доля ставки.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getStaffRateAsLong()
     */
    public static PropertyPath<Long> staffRateAsLong()
    {
        return _dslPath.staffRateAsLong();
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Ограничение на часы нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getMaxTimeAmountAsLong()
     */
    public static PropertyPath<Long> maxTimeAmountAsLong()
    {
        return _dslPath.maxTimeAmountAsLong();
    }

    /**
     * @return Преподаватель.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getPpsEntry()
     */
    public static PpsEntry.Path<PpsEntry> ppsEntry()
    {
        return _dslPath.ppsEntry();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getDisplayableTitle()
     */
    public static SupportedPropertyPath<String> displayableTitle()
    {
        return _dslPath.displayableTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getMaxTimeAmount()
     */
    public static SupportedPropertyPath<Double> maxTimeAmount()
    {
        return _dslPath.maxTimeAmount();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getStaffRateAsDouble()
     */
    public static SupportedPropertyPath<Double> staffRateAsDouble()
    {
        return _dslPath.staffRateAsDouble();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getStaffRateAsString()
     */
    public static SupportedPropertyPath<String> staffRateAsString()
    {
        return _dslPath.staffRateAsString();
    }

    public static class Path<E extends EplPlannedPps> extends EntityPath<E>
    {
        private EplOrgUnitSummary.Path<EplOrgUnitSummary> _orgUnitSummary;
        private EplPlannedPpsType.Path<EplPlannedPpsType> _type;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _post;
        private PropertyPath<String> _vacancy;
        private PropertyPath<Long> _staffRateAsLong;
        private PropertyPath<Long> _maxTimeAmountAsLong;
        private PpsEntry.Path<PpsEntry> _ppsEntry;
        private SupportedPropertyPath<String> _displayableTitle;
        private SupportedPropertyPath<Double> _maxTimeAmount;
        private SupportedPropertyPath<Double> _staffRateAsDouble;
        private SupportedPropertyPath<String> _staffRateAsString;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сводка. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getOrgUnitSummary()
     */
        public EplOrgUnitSummary.Path<EplOrgUnitSummary> orgUnitSummary()
        {
            if(_orgUnitSummary == null )
                _orgUnitSummary = new EplOrgUnitSummary.Path<EplOrgUnitSummary>(L_ORG_UNIT_SUMMARY, this);
            return _orgUnitSummary;
        }

    /**
     * @return Тип оформления. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getType()
     */
        public EplPlannedPpsType.Path<EplPlannedPpsType> type()
        {
            if(_type == null )
                _type = new EplPlannedPpsType.Path<EplPlannedPpsType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Должность.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getPost()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> post()
        {
            if(_post == null )
                _post = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST, this);
            return _post;
        }

    /**
     * @return Название вакансии.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getVacancy()
     */
        public PropertyPath<String> vacancy()
        {
            if(_vacancy == null )
                _vacancy = new PropertyPath<String>(EplPlannedPpsGen.P_VACANCY, this);
            return _vacancy;
        }

    /**
     * Смещение на 4 знака для дробной части.
     *
     * @return Доля ставки.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getStaffRateAsLong()
     */
        public PropertyPath<Long> staffRateAsLong()
        {
            if(_staffRateAsLong == null )
                _staffRateAsLong = new PropertyPath<Long>(EplPlannedPpsGen.P_STAFF_RATE_AS_LONG, this);
            return _staffRateAsLong;
        }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Ограничение на часы нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getMaxTimeAmountAsLong()
     */
        public PropertyPath<Long> maxTimeAmountAsLong()
        {
            if(_maxTimeAmountAsLong == null )
                _maxTimeAmountAsLong = new PropertyPath<Long>(EplPlannedPpsGen.P_MAX_TIME_AMOUNT_AS_LONG, this);
            return _maxTimeAmountAsLong;
        }

    /**
     * @return Преподаватель.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getPpsEntry()
     */
        public PpsEntry.Path<PpsEntry> ppsEntry()
        {
            if(_ppsEntry == null )
                _ppsEntry = new PpsEntry.Path<PpsEntry>(L_PPS_ENTRY, this);
            return _ppsEntry;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getDisplayableTitle()
     */
        public SupportedPropertyPath<String> displayableTitle()
        {
            if(_displayableTitle == null )
                _displayableTitle = new SupportedPropertyPath<String>(EplPlannedPpsGen.P_DISPLAYABLE_TITLE, this);
            return _displayableTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getMaxTimeAmount()
     */
        public SupportedPropertyPath<Double> maxTimeAmount()
        {
            if(_maxTimeAmount == null )
                _maxTimeAmount = new SupportedPropertyPath<Double>(EplPlannedPpsGen.P_MAX_TIME_AMOUNT, this);
            return _maxTimeAmount;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getStaffRateAsDouble()
     */
        public SupportedPropertyPath<Double> staffRateAsDouble()
        {
            if(_staffRateAsDouble == null )
                _staffRateAsDouble = new SupportedPropertyPath<Double>(EplPlannedPpsGen.P_STAFF_RATE_AS_DOUBLE, this);
            return _staffRateAsDouble;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps#getStaffRateAsString()
     */
        public SupportedPropertyPath<String> staffRateAsString()
        {
            if(_staffRateAsString == null )
                _staffRateAsString = new SupportedPropertyPath<String>(EplPlannedPpsGen.P_STAFF_RATE_AS_STRING, this);
            return _staffRateAsString;
        }

        public Class getEntityClass()
        {
            return EplPlannedPps.class;
        }

        public String getEntityName()
        {
            return "eplPlannedPps";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDisplayableTitle();

    public abstract Double getMaxTimeAmount();

    public abstract Double getStaffRateAsDouble();

    public abstract String getStaffRateAsString();
}
