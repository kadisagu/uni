package ru.tandemservice.uniepp_load.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Состояния сводки контингента"
 * Имя сущности : eplStudentSummaryState
 * Файл data.xml : uniepp_load.catalog.data.xml
 */
public interface EplStudentSummaryStateCodes
{
    /** Константа кода (code) элемента : Планирование контингента (title) */
    String CONTINGENT_PLANNING = "1";
    /** Константа кода (code) элемента : Привязка РУП (title) */
    String ATTACH_WORK_PLAN = "2";
    /** Константа кода (code) элемента : Планирование потоков (title) */
    String PLANNING_FLOW = "3";
    /** Константа кода (code) элемента : Расчет часов (title) */
    String TIME_ITEM = "4";
    /** Константа кода (code) элемента : Расчет согласован (title) */
    String ACCEPT = "5";

    Set<String> CODES = ImmutableSet.of(CONTINGENT_PLANNING, ATTACH_WORK_PLAN, PLANNING_FLOW, TIME_ITEM, ACCEPT);
}
