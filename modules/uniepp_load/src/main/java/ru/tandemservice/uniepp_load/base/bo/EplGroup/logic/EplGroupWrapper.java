/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplGroup.logic;

import com.google.common.collect.Lists;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;

import java.util.List;

/**
 * @author nvankov
 * @since 3/3/15
 */
public class EplGroupWrapper
{
    private EplGroup _group;

    private List<EplStudent> _students = Lists.newArrayList();
    private EplStudent _currentStudent;
    private String _groupTitle;

    public EplGroupWrapper(EplGroup group)
    {
        _group = group;
        _groupTitle = group.getTitle();
    }

    public List<EplStudent> getStudents()
    {
        return _students;
    }

    public EplGroup getGroup()
    {
        return _group;
    }

    public String getGroupTitle()
    {
        return _groupTitle;
    }

    public void setGroupTitle(String groupTitle)
    {
        _groupTitle = groupTitle;
    }

    public EplStudent getCurrentStudent()
    {
        return _currentStudent;
    }

    public void setCurrentStudent(EplStudent currentStudent)
    {
        _currentStudent = currentStudent;
    }

    public String getCurrentStudentTitle()
    {
        return getStudentTitle(_currentStudent);
    }

    public static String getStudentTitle(EplStudent student)
    {
        return student.getCount() + " - " + student.getEducationOrgUnit().getTitleWithFormAndCondition() + ", " + student.getDevelopGrid().getTitle() + ", " + student.getCompensationSource().getTitle();
    }
}
