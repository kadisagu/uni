/* $Id:$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.EplTimeRuleDao;
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleDao;

/**
 * @author oleyba
 * @since 11/28/14
 */
@Configuration
public class EplTimeRuleManager extends BusinessObjectManager
{
    public static EplTimeRuleManager instance()
    {
        return instance(EplTimeRuleManager.class);
    }

    @Bean
    public IEplTimeRuleDao timeRuleDao()
    {
        return new EplTimeRuleDao();
    }
}
