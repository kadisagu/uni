package ru.tandemservice.uniepp_load.catalog.entity;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uniepp_load.catalog.entity.gen.*;

/** @see ru.tandemservice.uniepp_load.catalog.entity.gen.EplTimeItemVariantGroupingGen */
public class EplTimeItemVariantGrouping extends EplTimeItemVariantGroupingGen implements IDynamicCatalogItem
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EplTimeItemVariantGrouping.class)
                .titleProperty(EplTimeItemVariantGrouping.title().s())
                .filter(EplTimeItemVariantGrouping.title())
                .order(EplTimeItemVariantGrouping.title());
    }
}