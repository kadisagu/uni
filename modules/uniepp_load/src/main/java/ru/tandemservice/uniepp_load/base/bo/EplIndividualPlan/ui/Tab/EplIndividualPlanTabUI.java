/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.Tab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitViewUI;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.EplIndividualPlanManager;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.AddEdit.EplIndividualPlanAddEdit;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.ext.OrgUnit.ui.View.OrgUnitViewExt;
import ru.tandemservice.uniepp_load.catalog.entity.EplPrintScript;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplPrintScriptCodes;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTab;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTabUI;
import ru.tandemservice.uniepp_load.util.UniEppLoadUtils;

import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 03.08.2016
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required = true),
        @Bind(key = EplIndividualPlanManager.PARAM_PPS_ACTIVE, binding = "ppsActive")
})
public class EplIndividualPlanTabUI extends UIPresenter
{
    public static final String PERMISSION_KEY = "eplIndividualPlanTab";

    private EntityHolder<EplIndividualPlan> _holder = new EntityHolder<>();
    private List<EplOrgUnitSummary> _ouSummaries;
    private EplOrgUnitSummary _ouSummary;
    private boolean _ppsActive;
    private long _eduLoad;
    private long _nonEduLoad;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();

        _ouSummaries = EplIndividualPlanManager.instance().dao().getOuSummaries(getIndPlan());
        _eduLoad = EplIndividualPlanManager.instance().dao().getPpsEntryEduLoad(getIndPlan());
        _nonEduLoad = EplIndividualPlanManager.instance().dao().getPpsEntryNonEduLoad(getIndPlan());
    }

    public void onClickEdit()
    {
        _uiActivation.asRegionDialog(EplIndividualPlanAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getIndPlan().getId())
                .activate();
    }

    public void onClickPrint()
    {
        EplPrintScript scriptItem = DataAccessServices.dao().getByCode(EplPrintScript.class, EplPrintScriptCodes.IND_PLAN_PPS);
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem, IScriptExecutor.TEMPLATE_VARIABLE, new byte[]{}, "indPlanId", getIndPlan().getId());
    }

    public ParametersMap getOrgUnitParams()
    {
        return new ParametersMap()
                .add(UIPresenter.PUBLISHER_ID, getIndPlan().getOrgUnit().getId())
                .add(OrgUnitViewUI.SELECTED_PAGE, OrgUnitViewExt.TAB_EPP_LOAD_ORG_UNIT)
                .add(EplOrgUnitTabUI.BIND_SELECTED_TAB, EplOrgUnitTab.TAB_IND_PLAN);
    }

    public String getIndPlanTabPermissionKey()
    {
        return EplIndividualPlanManager.getPermissionKeyView(PERMISSION_KEY, _ppsActive);
    }

    public String getIndPlanTabEditPermissionKey()
    {
        return EplIndividualPlanManager.getPermissionKey(PERMISSION_KEY, "edit", _ppsActive);
    }

    public String getIndPlanTabPrintPermissionKey()
    {
        return EplIndividualPlanManager.getPermissionKey(PERMISSION_KEY, "print", _ppsActive);
    }

    public EplIndividualPlan getIndPlan()
    {
        return getHolder().getValue();
    }

    public EntityHolder<EplIndividualPlan> getHolder()
    {
        return _holder;
    }

    public List<EplOrgUnitSummary> getOuSummaries()
    {
        return _ouSummaries;
    }

    public EplOrgUnitSummary getOuSummary()
    {
        return _ouSummary;
    }

    public void setOuSummary(EplOrgUnitSummary ouSummary)
    {
        _ouSummary = ouSummary;
    }

    public boolean isPpsActive()
    {
        return _ppsActive;
    }

    public void setPpsActive(boolean ppsActive)
    {
        _ppsActive = ppsActive;
    }

    public String getEduLoad()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(UniEppLoadUtils.wrap(_eduLoad));
    }

    public String getNonEduLoad()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(UniEppLoadUtils.wrap(_nonEduLoad));
    }

    public String getTotalLoad()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(UniEppLoadUtils.wrap(_eduLoad + _nonEduLoad));
    }
}
