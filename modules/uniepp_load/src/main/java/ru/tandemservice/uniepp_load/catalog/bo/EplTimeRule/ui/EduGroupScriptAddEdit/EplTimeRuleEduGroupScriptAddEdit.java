/* $Id:$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.ui.EduGroupScriptAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeItemVariantGrouping;

/**
 * @author oleyba
 * @since 11/29/14
 */
@Configuration
public class EplTimeRuleEduGroupScriptAddEdit extends BusinessComponentManager
{
    public static final String GROUP_TYPE_DS = "groupTypeDS";
    public static final String GROUPING_DS = "groupingDS";
    public static final String REG_STRUCTURE_DS = "regStructureDS";
    public static final String CATEGORY_DS = "categoryDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(GROUPING_DS, getName(), EplTimeItemVariantGrouping.defaultSelectDSHandler(getName())))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(GROUP_TYPE_DS, getName(), EppGroupType.defaultSelectDSHandler(getName())))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(REG_STRUCTURE_DS, getName(), EppRegistryStructure.defaultSelectDSHandler(getName())).treeable(true))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(CATEGORY_DS, getName(), EplTimeCategoryEduLoad.defaultSelectDSHandler(getName())))
                .create();
    }
}