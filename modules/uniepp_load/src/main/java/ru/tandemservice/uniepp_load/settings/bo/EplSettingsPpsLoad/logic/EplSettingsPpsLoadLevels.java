package ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.logic;

/**
 * Created by nsvetlov on 21.03.2017.
 */
public enum EplSettingsPpsLoadLevels
{
    BASIC(0L, "Настройка на уровне модуля"),
    SUMMARY(1L, "Настройка на уровне сводки"),
    OUSUMMARY(2L, "Настройка на уровне расчета");

    private Long _value;
    private EplSettingsPpsLoadLevels(Long val, String title){
        this._value = val;
    }

    public long getValue() {
        return this._value;
    }

    public boolean equals(Long val)
    {
        if (_value == null && val == null)
            return true;
        if (_value == null || val == null)
            return false;
        return _value == val;
    }
}
