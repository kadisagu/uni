/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EditCount;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.daemon.EplStudentSummaryDaemonBean;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Pub.EplStudentSummaryPub;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.StudentAdd.EplStudentSummaryStudentAdd;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.base.ext.OrgUnit.ui.View.OrgUnitViewExt;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTab;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTabUI;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 10/27/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "summaryHolder.id", required = true),
    @Bind(key = EplOrgUnitTabUI.BIND_ORG_UNIT_ID, binding = "orgUnitHolder.id")
})
public class EplStudentSummaryEditCountUI extends UIPresenter
{
    private EntityHolder<EplStudentSummary> summaryHolder = new EntityHolder<EplStudentSummary>();
    private EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<>();

    private List<EplStudent> rowList = new ArrayList<>();
    private EplStudent currentRow;

    @Override
    public void onComponentRefresh()
    {
        getSummaryHolder().refresh();
        getOrgUnitHolder().refresh();
        if (getOrgUnitHolder().getId() != null) {
            getSettings().set("formOu_cnt", getOrgUnitHolder().getValue());
        }
        onChangeFilterParams();
    }

    public void onChangeFilterParams() {
        saveSettings();

        OrgUnit formOu = getSettings().get("formOu_cnt");
        DevelopForm developForm = getSettings().get("developForm_cnt");

        setCurrentRow(null);
        setRowList(new ArrayList<EplStudent>());

        if (formOu == null || developForm == null) {
            return;
        }

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EplStudent.class, "s")
            .where(eq(property("s", EplStudent.group().summary()), value(getSummary())))
            .where(eq(property("s", EplStudent.educationOrgUnit().formativeOrgUnit()), value(formOu)))
            .where(eq(property("s", EplStudent.educationOrgUnit().developForm()), value(developForm)))
            .order(property("s", EplStudent.group().course().intValue()))
            .order(property("s", EplStudent.group().title()))
            .order(property("s", EplStudent.compensationSource().code()))
            ;

        FilterUtils.applySelectFilter(dql, "s", EplStudent.group().course(), getSettings().get("course_cnt"));
        FilterUtils.applySelectFilter(dql, "s", EplStudent.compensationSource(), getSettings().get("compensationSource_cnt"));

        setRowList(IUniBaseDao.instance.get().<EplStudent>getList(dql));
    }

    public void onClickApplyAndClose() {
        onClickApply();
        EplStudentSummaryDaemonBean.DAEMON.wakeUpDaemon();
        onClickClose();
    }

    public void onClickApply() {
        EplStudentSummaryManager.instance().dao().doSaveStudents(getRowList());
    }

    public void onClickClose() {
        if (getOrgUnitHolder().getId() != null) {
            _uiActivation
                .asDesktopRoot(OrgUnitView.class)
                .parameter("selectedPage", OrgUnitViewExt.TAB_EPP_LOAD_ORG_UNIT)
                .parameter("selectedSubPage", EplOrgUnitTab.TAB_STUDENT_SUMMARY)
                .parameter(PUBLISHER_ID, getOrgUnitHolder().getId())
                .activate();
        } else {
            _uiActivation
                .asDesktopRoot(EplStudentSummaryPub.class)
                .parameter("selectedTab", "groups")
                .parameter(PUBLISHER_ID, getSummary().getId())
                .activate();
        }
    }

    public boolean isShowGroupList() {
        OrgUnit formOu = getSettings().get("formOu_cnt");
        DevelopForm developForm = getSettings().get("developForm_cnt");
        return !(formOu == null || developForm == null);
    }

    // getters and setters

    public ISecured getSecurityObject() {
        return getOrgUnitHolder().getId() == null ? getSummary() : getOrgUnitHolder().getValue();
    }

    public String getEditPK() {
        if (getOrgUnitHolder().getId() == null) return "eplStudentSummaryEditGroups";
        return new OrgUnitSecModel(getOrgUnitHolder().getValue()).getPermission("editGroupsEplGroupTab");
    }

    public boolean isFormOuDisabled() {
        return getOrgUnitHolder().getId() != null;
    }

    public String getCountFieldId() {
        return "studentCount." + getCurrentRow().getId();
    }

    public EntityHolder<EplStudentSummary> getSummaryHolder()
    {
        return summaryHolder;
    }

    public EplStudentSummary getSummary()
    {
        return getSummaryHolder().getValue();
    }

    public EplStudent getCurrentRow()
    {
        return currentRow;
    }

    public void setCurrentRow(EplStudent currentRow)
    {
        this.currentRow = currentRow;
    }

    public List<EplStudent> getRowList()
    {
        return rowList;
    }

    public void setRowList(List<EplStudent> rowList)
    {
        this.rowList = rowList;
    }

    public EntityHolder<OrgUnit> getOrgUnitHolder()
    {
        return orgUnitHolder;
    }

    public void setOrgUnitHolder(EntityHolder<OrgUnit> orgUnitHolder)
    {
        this.orgUnitHolder = orgUnitHolder;
    }

    @Override
    public String getSettingsKey()
    {
        final String settingsKey = super.getSettingsKey();
        return getOrgUnitHolder().getId() != null ? settingsKey + getOrgUnitHolder().getId() : settingsKey;
    }
}