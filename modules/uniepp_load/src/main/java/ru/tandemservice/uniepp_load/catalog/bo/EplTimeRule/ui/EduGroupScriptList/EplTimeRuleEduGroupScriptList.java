/* $Id:$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.ui.EduGroupScriptList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author oleyba
 * @since 11/29/14
 */
@Configuration
public class EplTimeRuleEduGroupScriptList extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .create();
    }
}