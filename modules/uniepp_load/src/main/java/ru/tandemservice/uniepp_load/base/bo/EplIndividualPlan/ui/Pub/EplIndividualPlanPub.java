/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.EduLoad.EplIndividualPlanEduLoad;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.NonEduLoad.EplIndividualPlanNonEduLoad;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.Tab.EplIndividualPlanTab;

/**
 * @author Alexey Lopatin
 * @since 03.08.2016
 */
@Configuration
public class EplIndividualPlanPub extends BusinessComponentManager
{
    public static final String TAB_PANEL = "eplIndPlanPubTabPanel";

    public static final String TAB_IND_PLAN = "eplIndPlanTab";
    public static final String TAB_EDU_LOAD = "eplEduLoadTab";
    public static final String TAB_NON_EDU_LOAD = "eplNonEduLoadTab";

    @Bean
    public TabPanelExtPoint eplIndPlanPubTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(componentTab(TAB_IND_PLAN, EplIndividualPlanTab.class).permissionKey("ui:indPlanTabPermissionKey"))
                .addTab(componentTab(TAB_EDU_LOAD, EplIndividualPlanEduLoad.class).visible("ui:hasOuSummary").permissionKey("ui:indPlanEduLoadTabPermissionKey"))
                .addTab(componentTab(TAB_NON_EDU_LOAD, EplIndividualPlanNonEduLoad.class).permissionKey("ui:indPlanNonEduLoadTabPermissionKey"))
                .create();
    }
}