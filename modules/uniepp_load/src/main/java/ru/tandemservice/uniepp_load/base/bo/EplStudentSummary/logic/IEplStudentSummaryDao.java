/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic;

import com.sun.istack.NotNull;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentCount4SplitElement;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan;

import java.util.Collection;
import java.util.List;

/**
 * @author oleyba
 * @since 10/12/11
 */
public interface IEplStudentSummaryDao extends INeedPersistenceSupport
{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doSaveStudents(List<EplStudent> rowList);

    int fillSummary(EplStudentSummary summary, boolean fillCopyFirstCourse, boolean fillEduGroups);

    void doDeleteSummary(Long summaryId);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doAddStudent(EplStudentSummary summary, EplStudent student, String groupTitle, Course course);

    /**
     * Генерация названия План. потока. Получается перечислением через запятую отсортированных уникальных названий планируемых академических групп.
     *
     * @param eplEduGroupIds идентификаторы План. потоков
     * @return новое название План. потока
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    String generateEplEduGroupTitle(Collection<Long> eplEduGroupIds);

    /**
     * Объединение нескольких План. потоков в один на основе какой-то дисциплины.
     * Объединяемые потоки должны быть из одной сводки, одного вида нагрузки и одной части года.
     * @param eplEduGroups           объединяемые План. поток
     * @param newTitle               название нового План. потока
     * @param eppRegistryElementPart дисциплина реестра для нового План. потока
     * @param groupWithHours        План. поток, с которой необходимо перенести часы
     * @param summary
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void mergeEplEduGroups(Collection<EplEduGroup> eplEduGroups, String newTitle, EppRegistryElementPart eppRegistryElementPart, Long groupWithHours, EplOrgUnitSummary summary);

    /**
     * Разбивание План. потока на подгруппы указанного размера
     *
     * @param group        План. поток
     * @param subGroupSize количество студентов в подгруппе
     * @param splitByMaxCount
     * @return true - План. поток разделен, иначе - false
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    boolean splitEplEduGroup(EplEduGroup group, int subGroupSize, boolean splitByMaxCount);

    /**
     * Массовое разбивание План. потоков на подгруппы указанного размера
     *
     * @param groupIds     список идентификаторов План. потоков
     * @param subGroupSize количество студентов в подгруппе
     * @param splitByMaxCount признак разделения по максимальному числу студентов
     * @return кол-во раделенных План. потоков
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    int massSplitEplEduGroup(Collection<Long> groupIds, int subGroupSize, boolean splitByMaxCount);

    /**
     * Разбивание План. потока по признаку
     *
     * @param group            План. поток
     * @param splitTypeWrapper признак, по которому разбивается План. поток. Метод поддерживает разбиение по виду затрат, по параметрам обучения (НПП) и по дисциплине.
     * @return true - План. поток разделен, иначе - false
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    boolean splitEplEduGroup(EplEduGroup group, DataWrapper splitTypeWrapper);

    /**
     * Массовое разбивание План. потоков по признаку
     *
     * @param groupIds         список идентификаторов План. потоков
     * @param splitTypeWrapper признак, по которому разбивается План. поток. Метод поддерживает разбиение по виду затрат, по параметрам обучения (НПП) и по дисциплине.
     * @return кол-во раделенных План. потоков
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    int massSplitEplEduGroup(Collection<Long> groupIds, DataWrapper splitTypeWrapper);

    /**
     * Обновляет числа в План. потоке по сводке (в том числе, удаляет "опустевшие" План. поток), добавляет недостающие План. поток
     * @param summary сводка
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doRecalculateEduGroups(EplStudentSummary summary);

    List<EplTransferAffiliateWrapper> getEplTransferAffilationWrappers(Long summaryId, Course course, OrgUnit orgUnit);

    void doTransferFromAffiliate(List<EplTransferAffiliateWrapper> wrappers);

    /**
     * Создает копию сводки
     *
     * @param title     название сводки
     * @param summaryId id сводки
     * @param backup    резервная копия
     * @return созданная копия сводки
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    EplStudentSummary doCopySummary(String title, Long summaryId, boolean backup);

    /**
     * Возвращает начало учебного года следующее после выбранного
     */
    EppYearEducationProcess getNextAfterCurrentYearEducationProcess(EducationYear eduYear);

    List<String> getDisciplinesByGroupIds(Collection<Long> groupIds);

    /**
     * @param eplGroupIds ids План. потоков
     * @return Возвращает список способов деления План. потоков
     */
    List<DataWrapper> getSplitTypeList(Collection<Long> eplGroupIds);

    /**
     * Возвращает граничные значения суммы студентов по строкам План. потока,
     * где 1е и 2е значение - это, соответственно, минимальное и максимальное кол-во студентов
     *
     * @param eplGroupIds ids План. потоков
     * @return диапазон значений
     */
    PairKey<Integer, Integer> getEduGroupRowBoundaryCount(Collection<Long> eplGroupIds);

    /**
     * Объединяет сводки.
     * @param summaryBase основная сводка
     * @param summaryMerge присоединяемая сводка
     */
    void doMerge(EplStudentSummary summaryBase, EplStudentSummary summaryMerge);

    /**
     * Обновление сводки:
     *      - Производим обновление планируемых групп и студентов
     *      - Обновляем связи с РУП
     *
     * @param summary сводка
     * @param fillFirstCourse Заполнить первый курс копией текущего первого курса
     * @return кол-во пропущенных студентов
     */
    int doRefreshSummary(EplStudentSummary summary, boolean fillFirstCourse);

    /**
     * Обновление планируемых групп и студентов
     *
     * @param summary сводка
     * @param fillFirstCourse Заполнить первый курс копией текущего первого курса
     * @param useCorrectGroupTitle скорректировать название группы
     * @param yearDiff разница между учебным годом сводки и текущим учебным годом
     * @return кол-во пропущенных план. студентов
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    int doRefreshEduGroupsAndStudentData(EplStudentSummary summary, boolean fillFirstCourse, boolean useCorrectGroupTitle, int yearDiff);

    /**
     * Обновляем связей с РУП
     *
     * @param summary сводка
     * @param fillFirstCourse Заполнить первый курс копией текущего первого курса
     * @param useCorrectGroupTitle скорректировать название группы
     * @param yearDiff разница между учебным годом сводки и текущим учебным годом
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doRefreshWpRel(EplStudentSummary summary, boolean fillFirstCourse, boolean useCorrectGroupTitle, int yearDiff);

    /**
     * Возвращает билдер деления потоков, в зависимости от выбранного способа деления
     *
     * @param baseDql      базовый билдер отбора студентов в связи с РУП
     * @param splitAlias   алиас деления
     * @param splitVariant способ деления
     * @return элемент деления потоков
     */
    DQLSelectBuilder getStudent2WorkPlan4SplitBuilder(DQLSelectBuilder baseDql, String splitAlias, EppEduGroupSplitVariant splitVariant);

    /**
     * Сохранение элемента деления потоков
     *
     * @param splitVariant способ деления
     * @param splitValue   свойство, по которому производим деление
     * @return элемент деления потоков
     */
    EppEduGroupSplitBaseElement saveSplitElement(EppEduGroupSplitVariant splitVariant, IEntity splitValue);

    /**
     * Обновление План. потока
     *
     * @param summary сводка
     * @param fillEduGroups Заполнить План. поток по текущим УГС
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doRefreshEduGroups(EplStudentSummary summary, boolean fillEduGroups);

    /**
     * Возвращает билдер таких МСРП-ВН, в которых:
     * - есть МСРП-ВН такое, что число студентов в нем не совпадает с суммой чисел студентов в строках План. потока, ссылающихся на него. Если строк План. поток нет - сумма равна 0;
     *
     * @param summary сводка
     * @return билдер
     */
    DQLSelectBuilder getEplStudentWP2GTypeSlotErrorNonEqualCountBuilder(String alias, EplStudentSummary summary);

    /**
     * Возвращает билдер таких МСРП-ВН, в которых:
     * - есть МСРП-ВН такое, что число студентов в нем не совпадает с суммой чисел студентов в строках План. потока, ссылающихся на него. Если строк План. поток нет - сумма равна 0;
     * - есть МСРП-ВН на которое не ссылается ни одна строка ПУГС
     *
     * @param summary сводка
     * @return билдер
     */
    DQLSelectBuilder getEplStudentWP2GTypeSlotErrorWithoutSameSummaryCheckBuilder(String alias, EplStudentSummary summary);

    /**
     * Возвращает билдер таких МСРП-ВН, в которых:
     * - есть МСРП-ВН такое, что число студентов в нем не совпадает с суммой чисел студентов в строках План. потока, ссылающихся на него. Если строк План. поток нет - сумма равна 0;
     * - есть МСРП-ВН, полученных через строки План. потока, в которых сводка план. группы, отличается от текущей сводки.
     *
     * @param summary сводка
     * @return билдер
     */
    DQLSelectBuilder getEplStudentWP2GTypeSlotErrorBuilder(String alias, EplStudentSummary summary);

    /**
     * Осуществляет проверки по необходимости обновления План. потока, если есть хотя бы одна:
     * - План. поток без строк;
     * - МСРП-ВН из проверки {@link #getEplStudentWP2GTypeSlotErrorBuilder}
     *
     * @param summary сводка
     * @return Возвращает true если необходимо обновить План. поток, иначе - false.
     */
    boolean isNeedRefreshEduGroups(EplStudentSummary summary);

    /**
     * Дополнительное условие для вывода списка сводок, в зависимости от подразделения:
     * - если подр. формирующее, то есть хотя бы один планируемый студент в сводке с НПП с данным формирующим;
     * - если подр. читающее, то есть дисциплина из План. потока в сводке, для которой данное подразделение является ответственным;
     * - если подр. читающее, то есть дисциплина из МСРП в сводке, для нее есть строка План. поток, и для дисциплины данное подразделение является ответственным.
     */
    DQLSelectBuilder checkStudentSummaryByOrgUnit(String alias, DQLSelectBuilder dql, ExecutionContext context);

    /**
     * Возвращает DQLSelect запрос, который собирает уникальные пары (РУП для план. студента, элемент реестра), определенными следующими способами:
     * 1) по строкам данного РУП, в элементе реестра которых указан способ деления
     * 2) по строкам данного РУП, в элементе реестра которых указан не указан способ деления, но на него ссылкается как минимум
     * один элемент "Число план. студентов для элемента деления потоков". Это утверждение равноценно утверждению-условию
     * "по объектам «Число план. студентов для элемента деления потоков», ссылающимся на данный РУП"
     *
     * @param summary Сводка контингента. Необходимый параметр для фильтрации выбираемых уникальных пар.
     * @return Билдер
     */
    DQLSelectBuilder getWorkPlanAndRegElementUniquePairsDql(@NotNull String alias, EplStudentSummary summary);

    /**
     * Возвращает билдер таких пар (РУП, элемент реестра), которые удовлетворяют условию фильтра «Некорректно задано деление»:
     * - сумма чисел студентов для элементов деления не совпадает с числом студентов по РУП;
     * - способ деления элемента реестра не совпадает со способом деления элементов деления в нагрузке;
     * - у элементов деления разные способы деления.
     *
     * P.S. Пары могут быть неуникальны.
     *
     * @param studentSummary Сводка контингента
     * @return билдер
     */
    DQLSelectBuilder getIncorrectSplitPairDql(EplStudentSummary studentSummary);

    /**
     * Возвращает билдер уникальных пар (РУП, элемент реестра), которые удовлетворяют условию фильтра «Не задано деление»:
     * - для пары нет ни одного объекта «Число план. студентов для элемента деления потоков»
     *
     * @param studentSummary studentSummary Сводка контингента
     * @return билдер
     */
    DQLSelectBuilder getSplitEmptyPairDql(EplStudentSummary studentSummary);

    /**
     * Выполняет массовое удаление элементов "Число план. студентов для элемента деления потоков".
     * @param ids Коллекция идентификаторов сущностей на удаление.
     * @return Количество удаленных сущностей
     */
    int doSplitElementMassDelete(Collection<Long> ids);

    /**
     * Актуализирует новые данные "Число план. студентов для элемента деления потоков" для выбранной уникальной пары.
     * Мерджит новый список сформы редактирования с актуальным списком  и сохраняет новый смердженный список.
     * @param currentSplitElementList Новый список, пришедший с формы редактирования, который необходимо смерджить со актуалныи списком.
     * @param eppRegistryElement элемент реестра - необходим для выбора актуального списка "Число план. студентов для элемента деления потоков".
     * @param eplStudent2WorkPlan РУП для план. студента - необходим для выбора актуального списка "Число план. студентов для элемента деления потоков".
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void saveNewSplitRowList(List<EplStudentCount4SplitElement> currentSplitElementList, EppRegistryElement eppRegistryElement, EplStudent2WorkPlan eplStudent2WorkPlan);

    /**
     * Производит массовое удаление расчетов нагрузки по подразделениям.
     * @param selectedIds Коллекция идентификаторов расчетов нагрузки по подразделениям.
     * @return Кол-во удаленных элементов.
     */
    int doOrgUnitSummaryMassDelete(Collection<Long> selectedIds);

    /**
     * Генерит недостающие базовые элементы деления для переданного способа деления потоков.
     * @param splitVariant Способ деления потоков.
     */
    void doGenerateBaseSplitElements(EppEduGroupSplitVariant splitVariant);
}
