/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.TransferItem.logic;

import com.beust.jcommander.internal.Lists;
import com.beust.jcommander.internal.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.EplOuSummaryDAO;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.EplOuSummaryDAO.EplEduGroupTimeItemKey;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCommonRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic.EplErrorFormatter;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTransferOrgUnitEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.TransferItem.EplOuSummaryTransferItem.*;

/**
 * @author Alexey Lopatin
 * @since 01.10.2015
 */
public class EplOuSummaryTransferTimeItemDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String VIEW_ACADEMIC_GROUP = "viewAcademicGroup";
    public static final String VIEW_COURSE         = "viewCourse";
    public static final String VIEW_TERM           = "viewTerm";
    public static final String VIEW_YEAR_PART      = "viewYearPart";
    public static final String VIEW_AMOUNT         = "viewAmount";

    private boolean _possible;
    private Map<EplEduGroup, List<EplEduGroupRow>> _eduGroup2RowMap;

    public EplOuSummaryTransferTimeItemDSHandler(String ownerId, boolean possible)
    {
        super(ownerId);
        _possible = possible;
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EplOrgUnitSummary summary = context.get(BIND_ORG_UNIT_SUMMARY);
        EplCommonRowWrapper discOrRule = context.get(BIND_DISC_OR_RULE);
        Set<Long> categories = context.get(BIND_CATEGORIES);
        Set<Long> transferCategories = context.get(BIND_TRANSFER_CATEGORIES);
        Map<EplEduGroupTimeItemKey, EplTransferOrgUnitEduGroupTimeItem> transferTimeItemMap = context.get(BIND_TRANSFER_TIME_ITEM_MAP);

        if (null == summary || null == discOrRule)
            return ListOutputBuilder.get(input, Collections.emptyList()).pageable(false).build();

        EplCommonRowWrapper.DisciplineOrRulePairKey key = discOrRule.getKey();
        Long disciplineId = key.getDisciplineId();
        Long ruleId = key.getRuleId();

        List<EplEduGroupTimeItem> resultList = Lists.newArrayList();

        DQLSelectBuilder groupRowBuilder = new DQLSelectBuilder().fromEntity(EplEduGroupRow.class, "r").column(property("r"))
                .joinPath(DQLJoinType.inner, EplEduGroupRow.group().fromAlias("r"), "g")
                .joinPath(DQLJoinType.inner, EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().fromAlias("r"), "wpSlot")
                .joinPath(DQLJoinType.inner, EplStudentWPSlot.student().fromAlias("wpSlot"), "st");

        FilterUtils.applySelectFilter(groupRowBuilder, "g", EplEduGroup.registryElementPart().id(), disciplineId);
        if (null != ruleId)
            groupRowBuilder.where(exists(EplEduGroupTimeItem.class,
                                         EplEduGroupTimeItem.eduGroup().s(), property("g"),
                                         EplEduGroupTimeItem.timeRule().id().s(), ruleId)
            );

        _eduGroup2RowMap = Maps.newHashMap();
        for (EplEduGroupRow groupRow : groupRowBuilder.createStatement(context.getSession()).<EplEduGroupRow>list())
        {
            SafeMap.safeGet(_eduGroup2RowMap, groupRow.getGroup(), ArrayList.class).add(groupRow);
        }

        if (_possible)
        {
            final List<EplTransferOrgUnitEduGroupTimeItem> transferTimeList = Lists.newArrayList();

            if (CollectionUtils.isNotEmpty(transferCategories))
            {
                DQLSelectBuilder transferTimeDql = new DQLSelectBuilder().fromEntity(EplTransferOrgUnitEduGroupTimeItem.class, "e")
                        .where(eq(property("e", EplTransferOrgUnitEduGroupTimeItem.transferredFrom()), value(summary)));

                EplOuSummaryDAO.applyEduGroupTimeItemFilter(transferTimeDql, "e", key, transferCategories);

                transferTimeList.addAll(transferTimeDql.createStatement(context.getSession()).list());
            }

            DQLOrderDescriptionRegistry registry = new DQLOrderDescriptionRegistry(EplEduGroupTimeItem.class, "e");
            DQLSelectBuilder builder = registry.buildDQLSelectBuilder().column("e")
                    .where(eq(property("e", EplEduGroupTimeItem.summary()), value(summary)));

            EplOuSummaryDAO.applyEduGroupTimeItemFilter(builder, "e", key, categories);

            builder.where(exists(groupRowBuilder.where(eq(property("g.id"), property("e", EplEduGroupTimeItem.eduGroup().id()))).buildQuery()));

            FilterUtils.applySelectFilter(groupRowBuilder, "wpSlot", EplStudentWPSlot.yearPart(), context.get(BIND_YEAR_PART));
            FilterUtils.applySelectFilter(groupRowBuilder, "st", EplStudent.group().course(), context.get(BIND_COURSE));
            FilterUtils.applySelectFilter(groupRowBuilder, "st", EplStudent.group(), context.get(BIND_GROUP));

            registry.applyOrder(builder, input.getEntityOrder());

            List<MultiKey> keys = Lists.newArrayList();
            transferTimeItemMap.entrySet().forEach(entry -> {
                keys.add(entry.getKey().getKey());
                transferTimeList.remove(entry.getValue());
            });

            List<EplEduGroupTimeItem> timeItems = builder.createStatement(context.getSession()).list();
            timeItems.addAll(transferTimeList);

            for (EplEduGroupTimeItem timeItem : timeItems)
            {
                MultiKey timeItemKey = new EplEduGroupTimeItemKey(timeItem).getKey();

                if (keys.contains(timeItemKey)) continue;
                else keys.add(timeItemKey);
                resultList.add(timeItem);
            }
        }
        else
        {
            resultList.addAll(transferTimeItemMap.values());
            Comparator<EplEduGroupTimeItem> comparator = (t1, t2) -> {
                int result = CommonCollator.RUSSIAN_COLLATOR.compare(t1.getEduGroup().getTitle(), t2.getEduGroup().getTitle());
                if (result == 0)
                {
                    if (t1.getId() == null) return 1;
                    else if (t2.getId() == null) return -1;
                }
                return 0;
            };
            Collections.sort(resultList, comparator);
        }

        DSOutput output = ListOutputBuilder.get(input, resultList).pageable(false).build();
        return wrap(output, input, context);
    }

    protected DSOutput wrap(DSOutput output, DSInput input, ExecutionContext context)
    {
        long index = 0;

        for (DataWrapper wrapper : DataWrapper.wrap(output))
        {
            EplEduGroupTimeItem timeItem = wrapper.getWrapped();
            wrapper.setProperty(VIEW_AMOUNT, EplErrorFormatter.errorDiff(timeItem));

            EplEduGroup eduGroup = timeItem.getEduGroup();

            if (null == timeItem.getEduGroup()) continue;
            if (wrapper.getId() == null && timeItem instanceof EplTransferOrgUnitEduGroupTimeItem)
            {
                wrapper.setId(index++);
            }

            List<EplEduGroupRow> rows = _eduGroup2RowMap.get(eduGroup);
            if (CollectionUtils.isEmpty(rows)) continue;

            List<String> groups = com.google.common.collect.Lists.newArrayList();
            List<String> courses = com.google.common.collect.Lists.newArrayList();
            List<String> terms = com.google.common.collect.Lists.newArrayList();
            List<String> yearParts = com.google.common.collect.Lists.newArrayList();

            for (EplEduGroupRow row : rows)
            {
                EplStudentWPSlot slot = row.getStudentWP2GTypeSlot().getStudentWPSlot();
                EplGroup group = slot.getStudent().getGroup();

                if (!groups.contains(group.getTitle())) groups.add(group.getTitle());
                if (!courses.contains(group.getCourse().getTitle())) courses.add(group.getCourse().getTitle());
                if (!terms.contains(slot.getTerm().getTitle())) terms.add(slot.getTerm().getTitle());
                if (!yearParts.contains(slot.getYearPart().getTitle())) yearParts.add(slot.getYearPart().getTitle());
            }

            Collections.sort(groups, CommonCollator.RUSSIAN_COLLATOR::compare);
            Collections.sort(courses, CommonCollator.RUSSIAN_COLLATOR::compare);
            Collections.sort(terms, CommonCollator.RUSSIAN_COLLATOR::compare);
            Collections.sort(yearParts, CommonCollator.RUSSIAN_COLLATOR::compare);

            wrapper.setProperty(VIEW_ACADEMIC_GROUP, groups);
            wrapper.setProperty(VIEW_COURSE, courses);
            wrapper.setProperty(VIEW_TERM, terms);
            wrapper.setProperty(VIEW_YEAR_PART, yearParts);
        }
        return output;
    }
}