/* $Id$ */
package ru.tandemservice.uniepp_load.base.ext.EppState;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;
import ru.tandemservice.uniepp.base.bo.EppState.util.BaseEppStateConfig;
import ru.tandemservice.uniepp.base.bo.EppState.util.IEppStateConfig;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;

import java.util.List;
import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 17.02.2016
 */
@Configuration
public class EppStateExtManager extends BusinessObjectExtensionManager
{
    private static final Map<String, List<String>> POSSIBLE_TRANSITIONS_MAP = ImmutableMap.<String, List<String>>builder()
            .put(EppState.STATE_FORMATIVE, ImmutableList.of(EppState.STATE_ACCEPTABLE))
            .put(EppState.STATE_ACCEPTABLE, ImmutableList.of(EppState.STATE_REJECTED, EppState.STATE_ACCEPTED))
            .put(EppState.STATE_ACCEPTED, ImmutableList.of(EppState.STATE_REJECTED))
            .put(EppState.STATE_REJECTED, ImmutableList.of(EppState.STATE_FORMATIVE))
            .build();

    @Autowired
    private EppStateManager _manager;

    @Bean
    public ItemListExtension<IEppStateConfig> itemListExtension()
    {
        return itemListExtension(_manager.stateConfigExtPoint())
                .add(EplOrgUnitSummary.class.getSimpleName(), getStateConfig())
                .create();
    }

    private IEppStateConfig getStateConfig() {
        return new BaseEppStateConfig() {
            @Override public Map<String, List<String>> getPossibleTransitionsMap() { return POSSIBLE_TRANSITIONS_MAP; }
        };
    }
}
