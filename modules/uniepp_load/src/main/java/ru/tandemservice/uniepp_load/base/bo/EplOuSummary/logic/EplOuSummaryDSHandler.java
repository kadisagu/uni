/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;

/**
 * @author oleyba
 * @since 5/5/12
 */
public class EplOuSummaryDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PARAM_ORG_UNIT = "orgUnit";

    private final DQLOrderDescriptionRegistry registry = buildOrderRegistry();

    public EplOuSummaryDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EplOrgUnitSummary.class, "e");
        FilterUtils.applySelectFilter(dql, "e", EplOrgUnitSummary.studentSummary(), context.get(EplOrgUnitSummary.PARAM_STUDENT_SUMMARY));
        FilterUtils.applySelectFilter(dql, "e", EplOrgUnitSummary.orgUnit(), context.get(PARAM_ORG_UNIT));
        registry.applyOrder(dql, input.getEntityOrder());
        return DQLSelectOutputBuilder.get(input, dql, context.getSession()).build();
    }

    protected DQLOrderDescriptionRegistry buildOrderRegistry() {
        return new DQLOrderDescriptionRegistry(EplOrgUnitSummary.class, "e")
            .setOrders(EplOrgUnitSummary.studentSummary().title(), new OrderDescription(EplOrgUnitSummary.studentSummary().title()), new OrderDescription(EplOrgUnitSummary.studentSummary().eppYear().educationYear().intValue()))
            ;
    }
}