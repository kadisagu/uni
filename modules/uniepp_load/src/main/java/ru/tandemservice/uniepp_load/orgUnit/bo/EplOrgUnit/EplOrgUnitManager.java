/* $Id:$ */
package ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.logic.IEplOrgUnitDao;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.logic.EplOrgUnitDao;

/**
 * @author oleyba
 * @since 12/6/14
 */
@Configuration
public class EplOrgUnitManager extends BusinessObjectManager
{
    public static EplOrgUnitManager instance()
    {
        return instance(EplOrgUnitManager.class);
    }

    @Bean
    public IEplOrgUnitDao dao()
    {
        return new EplOrgUnitDao();
    }
}
