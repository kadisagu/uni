/* $Id:$ */
package ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.SummaryTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.EplOuSummaryDSHandler;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;

/**
 * @author oleyba
 * @since 5/5/12
 */
@Configuration
public class EplOrgUnitSummaryTab extends BusinessComponentManager
{
    public static final String DS_OU_SUMMARY = "eplOuSummaryDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(DS_OU_SUMMARY, eplOuSummaryDSColumns(), eplOuSummaryDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint eplOuSummaryDSColumns()
    {
        return columnListExtPointBuilder(DS_OU_SUMMARY)
                .addColumn(publisherColumn(EplOrgUnitSummary.L_STUDENT_SUMMARY, EplOrgUnitSummary.studentSummary().titleWithState()).order())
                .addColumn(textColumn(EplOrgUnitSummary.P_CREATION_DATE, EplOrgUnitSummary.creationDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(textColumn(EplStudentSummary.L_STATE, EplOrgUnitSummary.state().title()).order())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eplOuSummaryDSHandler()
    {
        return new EplOuSummaryDSHandler(getName());
    }
}
