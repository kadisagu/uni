/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.distribution;

import com.google.common.collect.Maps;
import org.tandemframework.caf.ui.support.IUISettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;

/**
 * Враппер подстроки для расчитанных часов План. потока (для форм распределения нагрузки по ППС)
 *
 * @author Alexey Lopatin
 * @since 05.04.2016
 */
public class EplDistributionSubRowWrapper extends EplDistributionCommonRowWrapper implements Comparable<EplDistributionSubRowWrapper>
{
    private EplDistributionCommonRowWrapper _parent;

    public EplDistributionSubRowWrapper(Long id, EplDistributionCommonRowWrapper parent)
    {
        super(id, null, null, null);
        _parent = parent;
    }

    public EplDistributionCommonRowWrapper getParent()
    {
        return _parent;
    }

    public void initTimeFromParent()
    {
        if (!_parent.isHasChildren())
        {
            setPps(_parent.getPps());
            setOvertime(_parent.isOvertime());
        }
        _parent.getDistributionTimeMap().forEach((k, v) -> getDistributionTimeMap().put(k, v));
        _parent.getChildren()
                .forEach(s -> s.getDistributionTimeMap().entrySet()
                        .stream()
                        .filter(e -> e.getValue() != null)
                        .forEach(e -> getDistributionTimeMap().put(e.getKey(), Math.max(0, getDistributionTimeMap().get(e.getKey()) - e.getValue()))));
    }

    public void updateSum()
    {
        timeSum = getDistributionTimeMap().values().stream().filter(v -> null != v).mapToLong(Long::longValue).sum();

        _parent.childrenTimeMap = Maps.newHashMap();
        _parent.getChildren()
                .forEach(s -> s.getDistributionTimeMap().entrySet().stream()
                        .filter(e -> null != e.getValue())
                        .forEach(e -> _parent.childrenTimeMap.put(e.getKey(), e.getValue() + _parent.childrenTimeMap.getOrDefault(e.getKey(), 0L))));

        _parent.childrenTimeSum = _parent.getChildren().stream().mapToLong(c -> c.timeSum).sum();
    }

    @Override
    public void updateVisibility(IUISettings settings)
    {
        visible = _parent.visible;
    }

    @Override
    public String getViewProperty(TitleColumnType columnType)
    {
        return "";
    }

    @Override
    public PairKey<String, ParametersMap> getLink(TitleColumn column) { return null; }

    @Override
    public int compareTo(EplDistributionSubRowWrapper o)
    {
        if (getPps() == null && o.getPps() == null) return 0;
        if (getPps() == null) return 1;
        if (o.getPps() == null) return -1;
        if (getPps().equals(o.getPps())) return java.lang.Boolean.compare(isOvertime(), o.isOvertime());
        return getPps().getDisplayableTitle().compareTo(o.getPps().getDisplayableTitle());
    }
}
