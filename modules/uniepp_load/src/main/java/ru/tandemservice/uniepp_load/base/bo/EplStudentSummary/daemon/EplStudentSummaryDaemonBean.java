/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.daemon;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections15.MultiMap;
import org.apache.commons.collections15.keyvalue.MultiKey;
import org.apache.commons.collections15.multimap.MultiHashMap;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.debug.SectionDebugItem;
import org.tandemframework.core.entity.dsl.EntityPath;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeALT;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentCount4SplitElement;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWP2GTypeSlot;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot;
import ru.tandemservice.uniepp_load.catalog.entity.EplStudentSummaryState;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplStudentSummaryStateCodes;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.sum;

/**
 * @author oleyba
 * @since 10/21/11
 */
public class EplStudentSummaryDaemonBean extends UniBaseDao implements IEplStudentSummaryDaemonBean
{
    public static final String GLOBAL_DAEMON_LOCK = EplStudentSummaryDaemonBean.class.getSimpleName() + ".lock";
    private static int statePriority;

    public static final SyncDaemon DAEMON = new SyncDaemon(EplStudentSummaryDaemonBean.class.getName(), 120, GLOBAL_DAEMON_LOCK)
    {
        @Override protected void main()
        {
            statePriority = DataAccessServices.dao().getByCode(EplStudentSummaryState.class, EplStudentSummaryStateCodes.ATTACH_WORK_PLAN).getPriority();
            IEplStudentSummaryDaemonBean.instance.get().updateStudentData();
        }
    };

    @Override
    protected void initDao()
    {
        // изменение сводки
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EplStudent.class, DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EplStudent.class, DAEMON.getAfterCompleteWakeUpListener());

        // изменение связей с РУП
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EplStudent2WorkPlan.class, DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EplStudent2WorkPlan.class, DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterDelete, EplStudent2WorkPlan.class, DAEMON.getAfterCompleteWakeUpListener());

        // изменение числа план. студентов для элемента деления потоков
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EplStudentCount4SplitElement.class, DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EplStudentCount4SplitElement.class, DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterDelete, EplStudentCount4SplitElement.class, DAEMON.getAfterCompleteWakeUpListener());
    }

    @Override
    public void updateStudentData()
    {
        Debug.begin("updateStudentData");
        try {

            NamedSyncInTransactionCheckLocker.register(getSession(), "eplSummaryStudentUpdate");

            doUpdateEplStudentCount4SplitElement();

            doGenerateStudentWPSlots();

            doGenerateStudentWp2GTypeSlots();

            doUpdateCount();
        }
        finally {
            SectionDebugItem end = Debug.end();
            if (null != end) {
                // System.out.println(end.toFullString());
            }
        }
    }

    /**
     * Обновляет число план. студентов для элемента деления потоков
     */
    private void doUpdateEplStudentCount4SplitElement()
    {
        Debug.begin("deleteEplStudentCount");
        try {
            DQLDeleteBuilder dqlDelete = new DQLDeleteBuilder(EplStudentCount4SplitElement.class)
                    .where(eq(property(EplStudentCount4SplitElement.studentCount()), value(0)));

            final int deleteCount = executeAndClear(dqlDelete);
            if (deleteCount > 0)
                Debug.message("deleted eplStudentCount: " + deleteCount);
        }
        finally {
            Debug.end();
        }
    }

    /**
     * Создает недостающие планируемые МСРП
     */
    private void doGenerateStudentWPSlots()
    {
        Debug.begin("deleteWrongWp");
        try {
            DQLDeleteBuilder dqlDelete = new DQLDeleteBuilder(EplStudent2WorkPlan.class)
                .where(ne(property(EplStudent2WorkPlan.cachedGridTerm().course()), property(EplStudent2WorkPlan.student().group().course())));

            final int deleteCount = executeAndClear(dqlDelete);
            if (deleteCount > 0)
                Debug.message("deleted rels: " + deleteCount);
        }
        finally {
            Debug.end();
        }

        Debug.begin("updateCachedCourse");
        try {
            DQLUpdateBuilder update = new DQLUpdateBuilder(EplStudentWPSlot.class)
                .fromDataSource(new DQLSelectBuilder()
                    .fromEntity(EplStudent2WorkPlan.class, "w")
                    .column(property("w", EplStudent2WorkPlan.student().id()), "s")
                    .column(property("w", EplStudent2WorkPlan.student().group().course().id()), "course")
                    .buildQuery(), "r")
                .where(eq(property(EplStudentWPSlot.student()), property("r.s")))
                .where(ne(property(EplStudentWPSlot.cachedCourse()), property("r.course")))
                .set(EplStudentWPSlot.cachedCourse().s(), property("r.course"));

            final int updateCount = executeAndClear(update);
            if (updateCount > 0) {
                Debug.message("updated cachedCourse: " + updateCount);
            }
        }
        finally {
            Debug.end();
        }


        Debug.begin("generateMissingStudentWPSlots");
        try {

            final Session session = getSession();

            // создать недостающие МСРП

            DQLSelectBuilder discDQL = getEplStudent2WorkPlanBuilder("s2wp")

                .joinPath(DQLJoinType.inner, EplStudent2WorkPlan.cachedGridTerm().fromAlias("s2wp"), "gt")
                .joinEntity("s2wp", DQLJoinType.inner, EppWorkPlanRegistryElementRow.class, "wpRow", and(
                    eq(property(EppWorkPlanRegistryElementRow.workPlan().fromAlias("wpRow")), property(EplStudent2WorkPlan.workPlan().fromAlias("s2wp"))),
                    neNullSafe(property("wpRow", EppWorkPlanRegistryElementRow.needRetake()), value(false))
                ))

                .joinEntity("wpRow", DQLJoinType.left, EplStudentWPSlot.class, "slot", andGreatFiveRel("slot", "s2wp", "wpRow"))
                .where(isNull("slot.id"))

                .column(property("s2wp", EplStudent2WorkPlan.student().id()))
                .column(property("wpRow", EppWorkPlanRegistryElementRow.registryElementPart().id()))
                .column(property("gt", DevelopGridTerm.part().id()))
                .column(property("gt", DevelopGridTerm.term().id()))
                .column(property("gt", DevelopGridTerm.course().id()))

                .order(property("s2wp", EplStudent2WorkPlan.student().id()))
                .order(property("wpRow", EppWorkPlanRegistryElementRow.registryElementPart().id()))
                .order(property("gt", DevelopGridTerm.part().id()))
                .order(property("gt", DevelopGridTerm.term().id()))
                .order(property("gt", DevelopGridTerm.course().id()))

                .predicate(DQLPredicateType.distinct);

            List<Object[]> discRows = discDQL.createStatement(session).list();

            final short entityCode = EntityRuntime.getMeta(EplStudentWPSlot.class).getEntityCode();
            Iterables.partition(discRows, DQL.MAX_VALUES_ROW_NUMBER).forEach(elements -> {
                DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(EplStudentWPSlot.class);

                elements.forEach(discRow -> {

                    insertBuilder.value(EplStudentWPSlot.P_ID, EntityIDGenerator.generateNewId(entityCode));
                    insertBuilder.value(EplStudentWPSlot.L_STUDENT, discRow[0]);
                    insertBuilder.value(EplStudentWPSlot.L_ACTIVITY_ELEMENT_PART, discRow[1]);
                    insertBuilder.value(EplStudentWPSlot.L_YEAR_PART, discRow[2]);
                    insertBuilder.value(EplStudentWPSlot.L_TERM, discRow[3]);
                    insertBuilder.value(EplStudentWPSlot.L_CACHED_COURSE, discRow[4]);

                    insertBuilder.addBatch();
                });
                insertBuilder.createStatement(session).execute();

                session.flush();
                session.clear();
            });

        }
        finally {
            Debug.end();
        }


        Debug.begin("deleteStudentWPSlots");
        try {
            final DQLDeleteBuilder delete = new DQLDeleteBuilder(EplStudentWPSlot.class)
                .fromDataSource(
                        getEplStudentWPSlotBuilder("slot")
                        .column(property("slot.id"), "slot_id")
                        .where(notExists(
                            new DQLSelectBuilder()
                                .fromEntity(EppWorkPlanRegistryElementRow.class, "wpRow")
                                .joinEntity("wpRow", DQLJoinType.inner, EplStudent2WorkPlan.class, "s2wp", and(
                                        eq(property(EplStudent2WorkPlan.workPlan().fromAlias("s2wp")), property(EppWorkPlanRegistryElementRow.workPlan().fromAlias("wpRow"))),
                                        neNullSafe(property("wpRow", EppWorkPlanRegistryElementRow.needRetake()), value(false))
                                ))
                                .column(property("wpRow.id"))
                                .where(andGreatFiveRel("slot", "s2wp", "wpRow"))
                                .buildQuery()
                        ))
                        .buildQuery(),
                    "xxx"
                ).where(eq(property("id"), property("xxx.slot_id")));

            final int deleteCount = executeAndClear(delete);
            if (deleteCount > 0)
                Debug.message("deleted rows: "+deleteCount);
        } finally {
            Debug.end();
        }
    }

    /**
     * Создает МСРП-ВН
     */
    private void doGenerateStudentWp2GTypeSlots()
    {
        doDeleteStudentWp2GTypeSlots4Split();

        doCreateStudentWp2GTypeSlots();

        doDeleteStudentWp2GTypeSlots();
    }

    private void doDeleteStudentWp2GTypeSlots4Split()
    {
        Debug.begin("deleteStudentWp2GTypeSlots4Split");
        try {
            // удаляем все МСРП-ВН с делением, которые перестали существовать (сменился способ деления, либо появилось ограничение)
            DQLDeleteBuilder dqlDelete = new DQLDeleteBuilder(EplStudentWP2GTypeSlot.class)
                    .fromDataSource(
                            getEplStudentWP2GTypeSlotBuilder("gtSlot")
                                    .column(property("gtSlot.id"), "id")
                                    .joinPath(DQLJoinType.inner, EplStudentWP2GTypeSlot.studentWPSlot().fromAlias("gtSlot"), "slot")
                                    .joinPath(DQLJoinType.inner, EplStudentWPSlot.activityElementPart().registryElement().fromAlias("slot"), "regEl")
                                    .where(isNotNull(property("gtSlot", EplStudentWP2GTypeSlot.splitElement())))
                                    .where(or(
                                            notExists(
                                                    new DQLSelectBuilder()
                                                            .column(property("sc.id"))
                                                            .fromEntity(EplStudentCount4SplitElement.class, "sc")
                                                            .joinPath(DQLJoinType.left, EplStudentCount4SplitElement.student2WorkPlan().fromAlias("sc"), "s2wp")
                                                            .joinPath(DQLJoinType.left, EplStudentCount4SplitElement.regElement().fromAlias("sc"), "el")
                                                            .where(and(
                                                                    andFantasticFourRel("slot", "s2wp"),
                                                                    eq(property("el"), property("regEl")),
                                                                    eq(property("gtSlot", EplStudentWP2GTypeSlot.splitElement()), property("sc", EplStudentCount4SplitElement.splitElement()))
                                                            )).buildQuery()
                                            ),
                                            and(
                                                    eq(property("regEl", EppRegistryElement.eduGroupSplitByType()), value(Boolean.TRUE)),
                                                    notExists(
                                                            new DQLSelectBuilder()
                                                                    .column(property("rel.id"))
                                                                    .fromEntity(EppRegElementSplitByTypeEduGroupRel.class, "rel")
                                                                    .where(eq(property("rel", EppRegElementSplitByTypeEduGroupRel.registryElement()), property("regEl")))
                                                                    .where(eq(property("rel", EppRegElementSplitByTypeEduGroupRel.groupType()), property("gtSlot", EplStudentWP2GTypeSlot.groupType())))
                                                                    .buildQuery()
                                                    ))
                                    )).buildQuery(), "xxx")
                    .where(eq(property("id"), property("xxx.id")));

            int deleteCount = executeAndClear(dqlDelete);
            if (deleteCount > 0)
                Debug.message("deleted rows for split element: " + deleteCount);

            // удаляем такие МСРП-ВН, для которых вместо split-null элемента, будут созданы МСРП-ВН с делением (учитывая при этом и ограничения по делению)
            dqlDelete = new DQLDeleteBuilder(EplStudentWP2GTypeSlot.class)
                    .fromDataSource(
                            getEplStudentWP2GTypeSlotBuilder("gtSlot")
                                    .column(property("gtSlot.id"), "id")
                                    .joinPath(DQLJoinType.inner, EplStudentWP2GTypeSlot.studentWPSlot().fromAlias("gtSlot"), "slot")
                                    .joinPath(DQLJoinType.inner, EplStudentWPSlot.activityElementPart().registryElement().fromAlias("slot"), "regEl")
                                    .where(isNull(property("gtSlot", EplStudentWP2GTypeSlot.splitElement())))
                                    .where(exists(
                                            new DQLSelectBuilder()
                                                    .column(property("sc.id"))
                                                    .fromEntity(EplStudentCount4SplitElement.class, "sc")
                                                    .joinPath(DQLJoinType.left, EplStudentCount4SplitElement.student2WorkPlan().fromAlias("sc"), "s2wp")
                                                    .joinPath(DQLJoinType.left, EplStudentCount4SplitElement.regElement().fromAlias("sc"), "el")

                                                    .where(or(
                                                            eq(property("el", EppRegistryElement.eduGroupSplitByType()), value(Boolean.FALSE)),
                                                            and(
                                                                    eq(property("el", EppRegistryElement.eduGroupSplitByType()), value(Boolean.TRUE)),
                                                                    exists(
                                                                            new DQLSelectBuilder()
                                                                                    .column(property("rel.id"))
                                                                                    .fromEntity(EppRegElementSplitByTypeEduGroupRel.class, "rel")
                                                                                    .where(eq(property("rel", EppRegElementSplitByTypeEduGroupRel.registryElement()), property("regEl")))
                                                                                    .where(eq(property("rel", EppRegElementSplitByTypeEduGroupRel.groupType()), property("gtSlot", EplStudentWP2GTypeSlot.groupType())))
                                                                                    .buildQuery()
                                                                    )
                                                            )
                                                    ))

                                                    .where(and(
                                                            andFantasticFourRel("slot", "s2wp"),
                                                            eq(property("el"), property("regEl"))
                                                    )).buildQuery()
                                    )).buildQuery(), "xxx")
                    .where(eq(property("id"), property("xxx.id")));

            deleteCount = executeAndClear(dqlDelete);
            if (deleteCount > 0)
                Debug.message("deleted rows for split-null element: " + deleteCount);
        }
        finally {
            Debug.end();
        }
    }

    private void doCreateStudentWp2GTypeSlots()
    {
        Debug.begin("generateStudentWp2GtypeSlots");
        try {
            final Session session = getSession();

            Map<Long, Long> slot2ElementMap = Maps.newHashMap();
            getEplStudentWPSlotBuilder("slot")
                    .column(property("slot.id"))
                    .column(property("el.id"))
                    .joinPath(DQLJoinType.inner, EplStudentWPSlot.activityElementPart().registryElement().fromAlias("slot"), "el")
                    .where(eq(property("el", EppRegistryElement.eduGroupSplitByType()), value(Boolean.TRUE)))
                    .createStatement(getSession()).<Object[]>list()
                    .forEach(item -> slot2ElementMap.put((Long) item[0], (Long) item[1]));

            MultiMap<Long, Long> regElement2GroupTypeMap = getList(EppRegElementSplitByTypeEduGroupRel.class)
                    .stream().collect(
                            MultiHashMap::new,
                            (map, item) -> map.put(item.getRegistryElement().getId(), item.getGroupType().getId()),
                            MultiHashMap::putAll
                    );

            List<MultiKey> gtSlotKeyList = Lists.newLinkedList();
            getEplStudentWP2GTypeSlotBuilder("gtSlot")
                    .joinPath(DQLJoinType.left, EplStudentWP2GTypeSlot.splitElement().fromAlias("gtSlot"), "split")
                    .column(property("gtSlot", EplStudentWP2GTypeSlot.studentWPSlot().id()))
                    .column(property("gtSlot", EplStudentWP2GTypeSlot.groupType().id()))
                    .column(property("split.id"))
                    .createStatement(getSession()).<Object[]>list()
                    .forEach(item -> gtSlotKeyList.add(new MultiKey<>(item[0], item[1], item[2])));

            List<Object[]> newGTypeSlotRows = getEplStudentWP2GTypeSlotList();
            Debug.message("creating slots: " + newGTypeSlotRows.size());

            // сохраняем то, что насобирали

            final short entityCode = EntityRuntime.getMeta(EplStudentWP2GTypeSlot.class).getEntityCode();
            Iterables.partition(newGTypeSlotRows, DQL.MAX_VALUES_ROW_NUMBER).forEach(elements -> {
                DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(EplStudentWP2GTypeSlot.class);
                final MutableBoolean save = new MutableBoolean(false);

                elements.forEach(loadRow -> {
                    Long slotId = (Long) loadRow[0];
                    Long groupTypeId = (Long) loadRow[1];
                    Long splitElementId = (Long) loadRow[2];

                    Long elementId = slot2ElementMap.get(slotId);
                    if (null != elementId && regElement2GroupTypeMap.containsKey(elementId))
                    {
                        Collection<Long> groupTypeIds = regElement2GroupTypeMap.get(elementId);
                        if (!groupTypeIds.contains(groupTypeId)) splitElementId = null;
                    }
                    MultiKey key = new MultiKey<>(slotId, groupTypeId, splitElementId);
                    if (gtSlotKeyList.contains(key)) return;
                    else gtSlotKeyList.add(key);

                    insertBuilder.value(EplStudentWP2GTypeSlot.P_ID, EntityIDGenerator.generateNewId(entityCode));
                    insertBuilder.value(EplStudentWP2GTypeSlot.L_STUDENT_W_P_SLOT, slotId);
                    insertBuilder.value(EplStudentWP2GTypeSlot.L_GROUP_TYPE, groupTypeId);
                    insertBuilder.value(EplStudentWP2GTypeSlot.L_SPLIT_ELEMENT, splitElementId);
                    insertBuilder.value(EplStudentWP2GTypeSlot.P_COUNT, 0);
                    insertBuilder.addBatch();
                    save.setValue(true);

                });
                if (save.booleanValue())
                    insertBuilder.createStatement(session).execute();

                session.flush();
                session.clear();
            });
        }
        finally {
            Debug.end();
        }
    }

    private List<Object[]> getEplStudentWP2GTypeSlotList()
    {
        DQLSelectBuilder builder = geEplStudentWP2GTypeSlottBuilder(true);
        builder.unionAll(geEplStudentWP2GTypeSlottBuilder(false).buildSelectRule());

        return new DQLSelectBuilder()
                .fromDataSource(builder.buildQuery(), "q")
                .joinEntity("q", DQLJoinType.left, EplStudentWP2GTypeSlot.class, "rel", and(
                        eq(property("rel", EplStudentWP2GTypeSlot.studentWPSlot()), property("q.slot")),
                        eq(property("rel", EplStudentWP2GTypeSlot.groupType()), property("q.gt")),
                        eqNullSafe(property("rel", EplStudentWP2GTypeSlot.splitElement()), property("q.split"))
                ))
                .where(isNull("rel.id"))
                .column("q.slot")
                .column("q.gt")
                .column("q.split")
                .createStatement(getSession()).list();
    }

    private DQLSelectBuilder geEplStudentWP2GTypeSlottBuilder(boolean aLoad)
    {
        DQLSelectBuilder builder = getEplStudentWPSlotBuilder("slot");
        joinSplitElement2StudentWPSlot("slot", builder);

        if (aLoad)
        {
            builder
                    .fromEntity(EppRegistryModuleALoad.class, "ml")
                    .fromEntity(EppRegistryElementPartModule.class, "m")
                    .joinPath(DQLJoinType.inner, EppRegistryModuleALoad.loadType().eppGroupType().fromAlias("ml"), "gt")
                    .where(eq(property("m", EppRegistryElementPartModule.module()), property("ml", EppRegistryModuleALoad.module())))
                    .where(eq(property("m", EppRegistryElementPartModule.part()), property("elPart")));
        }
        else
        {
            builder
                    .fromEntity(EppRegistryElementPartFControlAction.class, "ca")
                    .joinPath(DQLJoinType.inner, EppRegistryElementPartFControlAction.controlAction().eppGroupType().fromAlias("ca"), "gt")
                    .where(eq(property("ca", EppRegistryElementPartFControlAction.part()), property("slot", EplStudentWPSlot.activityElementPart())));
        }

        builder
                .where(exists(
                        new DQLSelectBuilder()
                                .fromEntity(EppWorkPlanRegistryElementRow.class, "wpRow")
                                .joinEntity("wpRow", DQLJoinType.inner, EplStudent2WorkPlan.class, "s2wp", eq(property("s2wp", EplStudent2WorkPlan.workPlan()), property("wpRow", EppWorkPlanRegistryElementRow.workPlan())))
                                .column(property("wpRow.id"))
                                .where(andGreatFiveRel("slot", "s2wp", "wpRow"))
                                .where(aLoad
                                               ? isNull(property("wpRow", EppWorkPlanRegistryElementRow.needRetake()))
                                               : neNullSafe(property("wpRow", EppWorkPlanRegistryElementRow.needRetake()), value(false))
                                )
                                .buildQuery()
                ))
                .column(property("slot.id"), "slot")
                .column(property("gt.id"), "gt")
                .column(property("xxx.split"), "split")
                .distinct();

        return builder;
    }

    private void joinSplitElement2StudentWPSlot(String slotAlias, DQLSelectBuilder dql)
    {
        joinSplitElement2StudentWPSlot(slotAlias, dql, false, false, null);
    }

    /**
     * Производим join числа план. студентов с элементами деления потоков
     */
    private void joinSplitElement2StudentWPSlot(String slotAlias, DQLSelectBuilder dql, boolean hasWpRow, boolean aLoad, String gtSlotAlias)
    {
        DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                .fromEntity(EplStudentCount4SplitElement.class, "sc")
                .joinPath(DQLJoinType.left, EplStudentCount4SplitElement.student2WorkPlan().fromAlias("sc"), "s2wp")
                .joinPath(DQLJoinType.left, EplStudentCount4SplitElement.regElement().fromAlias("sc"), "el")
                .joinPath(DQLJoinType.left, EplStudent2WorkPlan.cachedGridTerm().fromAlias("s2wp"), "dgt")

                .column(property("sc", EplStudentCount4SplitElement.splitElement().id()), "split")
                .column(property("sc", EplStudentCount4SplitElement.studentCount()), "studentCount")
                .column(property("s2wp", EplStudent2WorkPlan.student()), "st")
                .column(property("dgt", DevelopGridTerm.term()), "term")
                .column(property("dgt", DevelopGridTerm.course()), "course")
                .column(property("dgt", DevelopGridTerm.part()), "part")
                .column(property("el"), "el");

        if (hasWpRow)
        {
            subBuilder
                    .joinEntity("s2wp", DQLJoinType.inner, EppWorkPlanRegistryElementRow.class, "wpRow", eq(property("s2wp", EplStudent2WorkPlan.workPlan()), property("wpRow", EppWorkPlanRegistryElementRow.workPlan())))
                    .where(aLoad
                                   ? isNull(property("wpRow", EppWorkPlanRegistryElementRow.needRetake()))
                                   : neNullSafe(property("wpRow", EppWorkPlanRegistryElementRow.needRetake()), value(false))
                    )
                    .column(property("wpRow"), "wpRow");
        }

        dql.joinPath(DQLJoinType.inner, EplStudentWPSlot.activityElementPart().fromAlias(slotAlias), "elPart");
        dql.joinPath(DQLJoinType.inner, EppRegistryElementPart.registryElement().fromAlias("elPart"), "regEl");

        dql.joinDataSource(slotAlias, DQLJoinType.left, subBuilder.buildQuery(), "xxx", and(

                eq(property("xxx.st"), property(slotAlias, EplStudentWPSlot.student())),
                eq(property("xxx.term"), property(slotAlias, EplStudentWPSlot.term())),
                eq(property("xxx.course"), property(slotAlias, EplStudentWPSlot.cachedCourse())),
                eq(property("xxx.part"), property(slotAlias, EplStudentWPSlot.yearPart())),

                eq(property("xxx.el"), property("regEl")), null,

                hasWpRow ? and(
                        eq(property("xxx.wpRow", EppWorkPlanRegistryElementRow.registryElementPart()), property("elPart")),
                        eqNullSafe(property("xxx.split"), property(gtSlotAlias, EplStudentWP2GTypeSlot.splitElement()))
                ) : null
        ));
    }

    private void doDeleteStudentWp2GTypeSlots()
    {
        Debug.begin("deleteStudentWp2GtypeSlots");
        try {
            // для УГС-ВАН удаляем все МСРП-ВН, которых уже по факту не существуют
            DQLDeleteBuilder dqlDelete = new DQLDeleteBuilder(EplStudentWP2GTypeSlot.class)
                    .fromDataSource(
                            getEplStudentWP2GTypeSlotBuilder("gtSlot")
                                    .joinPath(DQLJoinType.inner, EplStudentWP2GTypeSlot.groupType().fromAlias("gtSlot"), "gt")
                                    .where(instanceOf("gt", EppGroupTypeALT.class))
                                    .column(property("gtSlot.id"), "slot_id")
                                    .where(notExists(
                                            new DQLSelectBuilder()
                                                    .column(property("slot.id"))
                                                    .fromEntity(EppRegistryElementPartModule.class, "m")
                                                    .fromEntity(EppWorkPlanRegistryElementRow.class, "wpRow")
                                                    .joinEntity("m", DQLJoinType.inner, EppRegistryModuleALoad.class, "ml", eq(property("m", EppRegistryElementPartModule.module()), property("ml", EppRegistryModuleALoad.module())))
                                                    .joinEntity("m", DQLJoinType.inner, EplStudentWPSlot.class, "slot", and(
                                                            eq(property("m", EppRegistryElementPartModule.part()), property("slot", EplStudentWPSlot.activityElementPart())),
                                                            eq(property("gtSlot", EplStudentWP2GTypeSlot.studentWPSlot().id()), property("slot.id"))
                                                    ))
                                                    .joinEntity("wpRow", DQLJoinType.inner, EplStudent2WorkPlan.class, "s2wp", and(
                                                            eq(property("s2wp", EplStudent2WorkPlan.workPlan()), property("wpRow", EppWorkPlanRegistryElementRow.workPlan())),
                                                            isNull(property("wpRow", EppWorkPlanRegistryElementRow.needRetake()))
                                                    ))
                                                    .where(eq(property("gt"), property("ml", EppRegistryModuleALoad.loadType().eppGroupType())))
                                                    .where(andGreatFiveRel("slot", "s2wp", "wpRow"))
                                                    .buildQuery()
                                    )).buildQuery(), "xxx")
                    .where(eq(property("id"), property("xxx.slot_id")));

            int deleteCount = executeAndClear(dqlDelete);
            if (deleteCount > 0)
                Debug.message("deleted rows alt: " + deleteCount);

            // для УГС-ФИК удаляем все МСРП-ВН, которых уже по факту не существуют
            dqlDelete = new DQLDeleteBuilder(EplStudentWP2GTypeSlot.class)
                    .fromDataSource(
                            getEplStudentWP2GTypeSlotBuilder("gtSlot")
                                    .joinPath(DQLJoinType.inner, EplStudentWP2GTypeSlot.groupType().fromAlias("gtSlot"), "gt")
                                    .where(instanceOf("gt", EppGroupTypeFCA.class))
                                    .column(property("gtSlot.id"), "slot_id")
                                    .where(notExists(
                                            new DQLSelectBuilder()
                                                    .fromEntity(EplStudentWPSlot.class, "slot")
                                                    .fromEntity(EppWorkPlanRegistryElementRow.class, "wpRow")
                                                    .joinEntity("slot", DQLJoinType.inner, EppRegistryElementPartFControlAction.class, "ca", and(
                                                            eq(property("slot", EplStudentWPSlot.activityElementPart()), property("ca", EppRegistryElementPartFControlAction.part())),
                                                            eq(property("gtSlot", EplStudentWP2GTypeSlot.studentWPSlot().id()), property("slot.id"))
                                                    ))
                                                    .joinEntity("wpRow", DQLJoinType.inner, EplStudent2WorkPlan.class, "s2wp", and(
                                                            eq(property("s2wp", EplStudent2WorkPlan.workPlan()), property("wpRow", EppWorkPlanRegistryElementRow.workPlan())),
                                                            neNullSafe(property("wpRow", EppWorkPlanRegistryElementRow.needRetake()), value(false))
                                                    ))
                                                    .where(eq(property("gt"), property(EppRegistryElementPartFControlAction.controlAction().eppGroupType().fromAlias("ca"))))
                                                    .where(andGreatFiveRel("slot", "s2wp", "wpRow"))
                                                    .buildQuery()
                                    ))
                                    .buildQuery(), "xxx"
                    ).where(eq(property("id"), property("xxx.slot_id")));

            deleteCount = executeAndClear(dqlDelete);
            if (deleteCount > 0)
                Debug.message("deleted rows fca: " + deleteCount);
        }
        finally {
            Debug.end();
        }
    }

    /**
     * Обновляет кэшируемые суммы
     */
    private void doUpdateCount()
    {
        Debug.begin("doUpdateCount.eplStudent.errorCountWithWp");
        try {
            DQLSelectBuilder countDQL = getEplStudentBuilder("si")
                .fromEntity(DevelopGridTerm.class, "term")
                .joinPath(DQLJoinType.inner, DevelopGridTerm.part().fromAlias("term"), "yp")
                .where(eq(property("term", DevelopGridTerm.developGrid()), property("si", EplStudent.developGrid())))
                .where(eq(property("term", DevelopGridTerm.course()), property("si", EplStudent.group().course())))
                .fromEntity(EplStudent2WorkPlan.class, "wpRel")
                .where(eq(property("wpRel", EplStudent2WorkPlan.student()), property("si")))
                .where(eq(property("wpRel", EplStudent2WorkPlan.cachedGridTerm().part()), property("yp")))
                .group(property("yp", YearDistributionPart.id()))
                .group(property("si", EplStudent.id()))
                .column(property("si", EplStudent.id()), "s_id")
                .column(sum(property("wpRel", EplStudent2WorkPlan.count())), "s_sum")
                ;

            DQLSelectBuilder errorDQL = getEplStudentBuilder("s")
                .column(property("s", EplStudent.id()), "s_id")
                .column(caseExpr(exists(new DQLSelectBuilder()
                        .fromDataSource(countDQL.buildQuery(), "c")
                        .where(eq(property("c.s_id"), property("s", EplStudent.id())))
                        .where(ne(property("c.s_sum"), property("s", EplStudent.count())))
                        .buildQuery()),
                    value(Boolean.TRUE),
                    value(Boolean.FALSE)), "wp_e")
                ;


            DQLUpdateBuilder updateCountDQL = new DQLUpdateBuilder(EplStudent.class)
                .fromDataSource(errorDQL.buildQuery(), "x")
                .where(eq(property("id"), property("x.s_id")))
                .where(ne(property(EplStudent.P_ERROR_COUNT_WITH_WP), property("x.wp_e")))
                .set(EplStudent.P_ERROR_COUNT_WITH_WP, property("x.wp_e"));

            int updated = executeAndClear(updateCountDQL);
            Debug.message("updated students: " + updated);

        }
        finally {
            Debug.end();
        }

        Debug.begin("doUpdateCount.eplStudent.noWp");
        try {
            DQLSelectBuilder noWpDQL = getEplStudentBuilder("si")
                .fromEntity(YearDistributionPart.class, "yp")
                .where(exists(new DQLSelectBuilder()
                        .fromEntity(DevelopGridTerm.class, "term").column("term.id")
                        .where(eq(property("term", DevelopGridTerm.part()), property("yp")))
                        .where(eq(property("term", DevelopGridTerm.developGrid()), property("si", EplStudent.developGrid())))
                        .where(eq(property("term", DevelopGridTerm.course()), property("si", EplStudent.group().course())))
                        .buildQuery()
                ))
                .where(notExists(new DQLSelectBuilder()
                    .fromEntity(EplStudent2WorkPlan.class, "wpRel")
                    .where(eq(property("wpRel", EplStudent2WorkPlan.student()), property("si")))
                    .where(eq(property("wpRel", EplStudent2WorkPlan.cachedGridTerm().part()), property("yp")))
                    .buildQuery()))
                .column(property("si", EplStudent.id()), "s_id")
                ;

            DQLSelectBuilder errorDQL = getEplStudentBuilder("s")
                .column(property("s", EplStudent.id()), "s_id")
                .column(caseExpr(exists(new DQLSelectBuilder()
                        .fromDataSource(noWpDQL.buildQuery(), "c")
                        .where(eq(property("c.s_id"), property("s", EplStudent.id())))
                        .buildQuery()),
                    value(Boolean.TRUE),
                    value(Boolean.FALSE)), "wp_e")
                ;

            DQLUpdateBuilder updateCountDQL = new DQLUpdateBuilder(EplStudent.class)
                .fromDataSource(errorDQL.buildQuery(), "x")
                .where(eq(property("id"), property("x.s_id")))
                .where(ne(property(EplStudent.P_NO_WP), property("x.wp_e")))
                .set(EplStudent.P_NO_WP, property("x.wp_e"));

            int updated = executeAndClear(updateCountDQL);
            Debug.message("updated students: " + updated);

        }
        finally {
            Debug.end();
        }

        Debug.begin("doUpdateCount.eplStudentWP2GTypeSlot.count [sum of eplStudent2WorkPlan.count]");
        try {
            DQLUpdateBuilder updateCountALoadDQL = new DQLUpdateBuilder(EplStudentWP2GTypeSlot.class)
                    .fromDataSource(getWP2GTypeSlotBuilder(true).buildQuery(), "x")
                    .where(eq(property("id"), property("x.s_id")))
                    .where(ne(property(EplStudentWP2GTypeSlot.P_COUNT), property("x.r_sum")))
                    .set(EplStudentWP2GTypeSlot.P_COUNT, property("x.r_sum"));

            int updateCount = executeAndClear(updateCountALoadDQL);
            if (updateCount > 0)
                Debug.message("update count aLoad: " + updateCount);

            DQLUpdateBuilder updateCountCaDQL = new DQLUpdateBuilder(EplStudentWP2GTypeSlot.class)
                    .fromDataSource(getWP2GTypeSlotBuilder(false).buildQuery(), "x")
                    .where(eq(property("id"), property("x.s_id")))
                    .where(ne(property(EplStudentWP2GTypeSlot.P_COUNT), property("x.r_sum")))
                    .set(EplStudentWP2GTypeSlot.P_COUNT, property("x.r_sum"));

            updateCount = executeAndClear(updateCountCaDQL);
            if (updateCount > 0)
                Debug.message("update count ca: " + updateCount);
        }
        finally {
            Debug.end();
        }

        Debug.begin("deleteEplStudentWP2GTypeSlot");
        try {
            DQLDeleteBuilder dqlDelete = new DQLDeleteBuilder(EplStudentWP2GTypeSlot.class)
                    .where(eq(property(EplStudentWP2GTypeSlot.count()), value(0)));

            int deleteCount = executeAndClear(dqlDelete);
            if (deleteCount > 0)
                Debug.message("deleted eplStudentWP2GTypeSlot: " + deleteCount);
        }
        finally {
            Debug.end();
        }
    }

    private DQLSelectBuilder getWP2GTypeSlotBuilder(boolean aLoad)
    {
        DQLSelectBuilder builder = getEplStudentWP2GTypeSlotBuilder("gtSlot")
                .joinPath(DQLJoinType.inner, EplStudentWP2GTypeSlot.studentWPSlot().fromAlias("gtSlot"), "slot")
                .joinPath(DQLJoinType.inner, EplStudentWP2GTypeSlot.groupType().fromAlias("gtSlot"), "gt")
                .fromEntity(EppWorkPlanRegistryElementRow.class, "wpRow")
                .joinEntity("wpRow", DQLJoinType.inner, EplStudent2WorkPlan.class, "s2wp", and(
                        eq(property("s2wp", EplStudent2WorkPlan.workPlan()), property("wpRow", EppWorkPlanRegistryElementRow.workPlan())),
                        aLoad
                                ? isNull(property("wpRow", EppWorkPlanRegistryElementRow.needRetake()))
                                : neNullSafe(property("wpRow", EppWorkPlanRegistryElementRow.needRetake()), value(false))
                ))
                .where(andGreatFiveRel("slot", "s2wp", "wpRow"))
                .where(isNull(property("gtSlot", EplStudentWP2GTypeSlot.splitElement())))
                .group(property("gtSlot", EplStudentWP2GTypeSlot.id()))
                .column(sum(property("s2wp", EplStudent2WorkPlan.count())), "r_sum")
                .column(property("gtSlot", EplStudentWP2GTypeSlot.id()), "s_id");

        joinGroupTypeExpr(builder, aLoad);

        DQLSelectBuilder builderWithSplit = getEplStudentWP2GTypeSlotBuilder("gtSlot")
                .joinPath(DQLJoinType.inner, EplStudentWP2GTypeSlot.studentWPSlot().fromAlias("gtSlot"), "slot")
                .joinPath(DQLJoinType.inner, EplStudentWP2GTypeSlot.groupType().fromAlias("gtSlot"), "gt")
                .where(isNotNull(property("gtSlot", EplStudentWP2GTypeSlot.splitElement())))
                .group(property("gtSlot", EplStudentWP2GTypeSlot.id()))
                .column(sum(property("xxx.studentCount")), "r_sum")
                .column(property("gtSlot", EplStudentWP2GTypeSlot.id()), "s_id");

        joinSplitElement2StudentWPSlot("slot", builderWithSplit, true, aLoad, "gtSlot");
        joinGroupTypeExpr(builderWithSplit, aLoad);

        builder.unionAll(builderWithSplit.buildSelectRule());

        return builder;
    }

    private void joinGroupTypeExpr(DQLSelectBuilder builder, boolean aLoad)
    {
        if (aLoad)
        {
            builder
                    .where(instanceOf("gt", EppGroupTypeALT.class))
                    .joinEntity("slot", DQLJoinType.inner, EppRegistryElementPartModule.class, "m", eq(property("slot", EplStudentWPSlot.activityElementPart()), property("m", EppRegistryElementPartModule.part())))
                    .joinEntity("m", DQLJoinType.inner, EppRegistryModuleALoad.class, "ml", eq(property("m", EppRegistryElementPartModule.module()), property("ml", EppRegistryModuleALoad.module())))
                    .where(eq(property("gt"), property("ml", EppRegistryModuleALoad.loadType().eppGroupType())));
        }
        else
        {
            builder
                    .where(instanceOf("gt", EppGroupTypeFCA.class))
                    .joinEntity("slot", DQLJoinType.inner, EppRegistryElementPartFControlAction.class, "ca", eq(property("slot", EplStudentWPSlot.activityElementPart()), property("ca", EppRegistryElementPartFControlAction.part())))
                    .where(eq(property("gt"), property("ca", EppRegistryElementPartFControlAction.controlAction().eppGroupType())));
        }
    }

    private DQLSelectBuilder getEplStudent2WorkPlanBuilder(String alias)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EplStudent2WorkPlan.class, alias);
        return getBaseBuilder(dql, EplStudent2WorkPlan.student().group().summary().fromAlias(alias));
    }

    private DQLSelectBuilder getEplStudentWPSlotBuilder(String alias)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EplStudentWPSlot.class, alias);
        return getBaseBuilder(dql, EplStudentWPSlot.student().group().summary().fromAlias(alias));
    }

    private DQLSelectBuilder getEplStudentWP2GTypeSlotBuilder(String alias)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EplStudentWP2GTypeSlot.class, alias);
        return getBaseBuilder(dql, EplStudentWP2GTypeSlot.studentWPSlot().student().group().summary().fromAlias(alias));
    }

    private DQLSelectBuilder getEplStudentBuilder(String alias)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EplStudent.class, alias);
        return getBaseBuilder(dql, EplStudent.group().summary().fromAlias(alias));
    }

    /**
     * Сводка контингента не должна быть архивной и состояние должно быть <= указанного приоритета
     */
    private DQLSelectBuilder getBaseBuilder(DQLSelectBuilder dql, EntityPath summaryPath)
    {
        return dql
                .joinPath(DQLJoinType.inner, summaryPath, "ss")
                .where(le(property("ss", EplStudentSummary.state().priority()), value(statePriority)))
                .where(eq(property("ss", EplStudentSummary.archival()), value(Boolean.FALSE)));
    }

    /**
     * "Фантастическая четверка" (студент, семестр, курс, часть года)
     */
    private IDQLExpression andFantasticFourRel(String slotAlias, String s2wpAlias)
    {
        return and(
                eq(property(slotAlias, EplStudentWPSlot.student()), property(s2wpAlias, EplStudent2WorkPlan.student())),
                eq(property(slotAlias, EplStudentWPSlot.term()), property(s2wpAlias, EplStudent2WorkPlan.cachedGridTerm().term())),
                eq(property(slotAlias, EplStudentWPSlot.cachedCourse()), property(s2wpAlias, EplStudent2WorkPlan.cachedGridTerm().course())),
                eq(property(slotAlias, EplStudentWPSlot.yearPart()), property(s2wpAlias, EplStudent2WorkPlan.cachedGridTerm().part()))
        );
    }

    /**
     * "Великолепная пятерка" (студент, семестр, курс, часть года, часть мероприятия реестра)
     */
    private IDQLExpression andGreatFiveRel(String slotAlias, String s2wpAlias, String wpRowAlias)
    {
        return and(andFantasticFourRel(slotAlias, s2wpAlias), eq(property(slotAlias, EplStudentWPSlot.activityElementPart()), property(wpRowAlias, EppWorkPlanRegistryElementRow.registryElementPart())));
    }
}

