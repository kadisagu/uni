/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EduGroupMerge;

import com.google.common.base.Preconditions;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;

/**
 * @author Nikolay Fedorovskih
 * @since 20.11.2014
 */
@Input(
        @Bind(key = EplStudentSummaryEduGroupMergeUI.BIND_PARAM_MERGED_GROUP_IDS, binding = "mergedGroupIds", required = true)
)
public class EplStudentSummaryEduGroupMergeUI extends UIPresenter
{
    public static final String BIND_PARAM_MERGED_GROUP_IDS = "mergedGroupIds";
    public static final String GROUP_DS = "groupDS";
    public static final String LIST = "list";

    private List<Long> _mergedGroupIds;
    private List<EplEduGroup> _mergedGroups;
    private Set<EppRegistryElementPart> _disciplineSet;
    private EppRegistryElementPart _discipline;
    private String _title;
    private boolean _moveHours = true;
    private boolean _hasHours = false;
    private ViewWrapper<EplEduGroup> _groupWithHours;
    private List<ViewWrapper<EplEduGroup>> _groupListWithHours;
    private EplOrgUnitSummary _summary;

    @Override
    public void onComponentRefresh()
    {
        Preconditions.checkArgument(_mergedGroupIds.size() > 1);

        _mergedGroups = DataAccessServices.dao().getList(EplEduGroup.class, _mergedGroupIds);
        _title = EplStudentSummaryManager.instance().dao().generateEplEduGroupTitle(_mergedGroupIds);
        _disciplineSet = CommonBaseEntityUtil.getPropertiesSet(_mergedGroups, EplEduGroup.L_REGISTRY_ELEMENT_PART);
        if (_disciplineSet.size() == 1)
            _discipline = _disciplineSet.iterator().next();
        refreshGroupsWithHours();
    }


    private void refreshGroupsWithHours()
    {
        List<Object[]> groupsWithHours = DataAccessServices.dao().getList(new DQLSelectBuilder()
                .fromEntity(EplEduGroup.class, "g").column("g.id")
                .fromEntity(EplPlannedPps.class, "pps").column("pps")
                .where(in(property("g", EplEduGroup.id()), _mergedGroupIds))
                .where(exists(new DQLSelectBuilder()
                        .fromEntity(EplPlannedPpsTimeItem.class, "pi")
                        .where(eq(property("pi", EplPlannedPpsTimeItem.pps()), property("pps")))
                        .fromEntity(EplEduGroupTimeItem.class, "gi")
                        .where(eq(property("pi", EplPlannedPpsTimeItem.timeItem()), property("gi")))
                        .where(eq(property("gi", EplEduGroupTimeItem.eduGroup()), property("g")))
                        .column("pi.id")
                        .buildQuery())));

        if (getDiscipline() != null && getDiscipline().getTutorOu() != null && _mergedGroups != null && !_mergedGroups.isEmpty())
        {
            final Set<EplStudentSummary> groupSummarySet = CommonBaseEntityUtil.getPropertiesSet(_mergedGroups, EplEduGroup.L_SUMMARY);

            List<EplOrgUnitSummary> list = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(EplOrgUnitSummary.class, "s")
                    .where(eq(property(EplOrgUnitSummary.orgUnit().id().fromAlias("s")), value(getDiscipline().getTutorOu().getId())))
                    .where(eq(property(EplOrgUnitSummary.studentSummary().id().fromAlias("s")), value(groupSummarySet.iterator().next())))
            );
            _summary = list.isEmpty() ? null : list.get(0);
        }
        else
        {
            _summary = null;
        }

        _hasHours = groupsWithHours != null && _summary != null && !groupsWithHours.isEmpty();

        if (_hasHours)
        {
            Set<Long> groupIdsWithHours = groupsWithHours.stream().map(o -> ((Long) o[0])).collect(Collectors.toSet());
            _mergedGroups.sort((g1, g2) -> (groupIdsWithHours.contains(g1.getId()) ? 1 : 0) - (groupIdsWithHours.contains(g2.getId()) ? 1 : 0));

            List<Object[]> toMove = DataAccessServices.dao().getList(new DQLSelectBuilder()
                    .fromEntity(EplEduGroup.class, "g")
                    .column(EplEduGroup.id().fromAlias("g").s())
                    .column(EplEduGroup.title().fromAlias("g").s())
                    .fromEntity(EplPlannedPps.class, "pps")
                    .where(in(property("g", EplEduGroup.id()), groupIdsWithHours))
                    .fromEntity(EplPlannedPpsTimeItem.class, "pi")
                    .column(DQLFunctions.sum(property(EplPlannedPpsTimeItem.timeAmountAsLong().fromAlias("pi"))))
                    .where(eq(property("pi", EplPlannedPpsTimeItem.pps()), property("pps")))
                    .fromEntity(EplEduGroupTimeItem.class, "gi")
                    .where(eq(property("pi", EplPlannedPpsTimeItem.timeItem()), property("gi")))
                    .where(eq(property("gi", EplEduGroupTimeItem.eduGroup()), property("g")))
                    .group(property("g",EplEduGroup.id()))
                    .group(property("g", EplEduGroup.title().s()))
                    .order(DQLFunctions.sum(property(EplPlannedPpsTimeItem.timeAmountAsLong().fromAlias("pi"))), OrderDirection.desc)
                    );

            setGroupListWithHours(toMove.stream().map(EplStudentSummaryEduGroupMergeUI::toViewWrapper).collect(Collectors.toList()));
            setGroupWithHours(getGroupListWithHours().iterator().next());
        }
    }

    // Передача параметров в хэндлеры
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.put(LIST, getGroupListWithHours());
        }
    }

    public static ViewWrapper<EplEduGroup> toViewWrapper(Object[] o) {
        EplEduGroup group = new EplEduGroup();
        group.setId((Long)o[0]);
        group.setTitle(String.format("%s (%.2f)", o[1], ((Long)o[2]).doubleValue() / 100.0));
        return new ViewWrapper<>(group);
    }

    public void onClickApply()
    {
        EplStudentSummaryManager.instance().dao().mergeEplEduGroups(_mergedGroups, getTitle(), getDiscipline(),
                _moveHours && getGroupWithHours() != null ? getGroupWithHours().getId() : null, _summary);
        deactivate();
    }

    public List<Long> getMergedGroupIds()
    {
        return _mergedGroupIds;
    }

    public void setMergedGroupIds(List<Long> mergedGroupIds)
    {
        _mergedGroupIds = mergedGroupIds;
    }

    public EppRegistryElementPart getDiscipline()
    {
        return _discipline;
    }

    public void setGroupWithHours(ViewWrapper<EplEduGroup> group)
    {
        _groupWithHours = group;
    }

    public ViewWrapper<EplEduGroup> getGroupWithHours()
    {
        return _groupWithHours;
    }

    public void setDiscipline(EppRegistryElementPart discipline)
    {
        _discipline = discipline;
        refreshGroupsWithHours();
    }

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }

    public Set<EppRegistryElementPart> getDisciplineSet()
    {
        return _disciplineSet;
    }


    public boolean getMoveHours()
    {
        return _moveHours;
    }

    public void setMoveHours(boolean moveHours)
    {
        this._moveHours = moveHours;
    }

    public boolean getHasHours()
    {
        return _hasHours;
    }

    public List<ViewWrapper<EplEduGroup>> getGroupListWithHours()
    {
        return _groupListWithHours;
    }

    public void setGroupListWithHours(List<ViewWrapper<EplEduGroup>> _groupListWithHours)
    {
        this._groupListWithHours = _groupListWithHours;
    }
}