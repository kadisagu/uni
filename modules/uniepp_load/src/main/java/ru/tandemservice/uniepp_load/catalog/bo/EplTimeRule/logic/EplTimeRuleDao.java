/* $Id:$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic;

import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import ru.tandemservice.uni.dao.UniScriptDao;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeALTCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupFormula;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 11/28/14
 */
public class EplTimeRuleDao extends UniScriptDao implements IEplTimeRuleDao
{
    public static IRuleResult getResult(final double hours, final String description) {
        return getResult(hours, description, 0);
    }

    private static IRuleResultEmpty getResult(long scriptTime) {
        return new IRuleResultEmpty() {
            @Override public double hours() { return 0; }
            @Override public String description() { return null; }
            @Override public long scriptTime() { return scriptTime; }
        };
    }

    private static IRuleResult getResult(final double hours, final String description, long scriptTime) {
        return new IRuleResult() {
            @Override public double hours() { return hours; }
            @Override public String description() { return description; }
            @Override public long scriptTime() { return scriptTime; }
        };
    }

    public static ILoadData wrapLoadData(final IEppRegElPartWrapper discData, final EplEduGroup eduGroup, final Map<EplStudent, Integer> groupContent, final List<EppIControlActionType> icaTypes)
    {
        return new ILoadData() {
            @Override public EplEduGroup eduGroup() { return eduGroup; }
            @Override public IEppRegElPartWrapper discData() { return discData; }
            @Override public Map<EplStudent, Integer> groupContent() { return groupContent; }
            @Override public List<EppIControlActionType> icaTypes() { return icaTypes; }
        };
    }

    @Override
    public Map<EplTimeRuleEduGroupScript, IEplTimeRuleScript> getScriptWrapper4RuleMap(List<EplTimeRuleEduGroupScript> rules)
    {
        Map<EplTimeRuleEduGroupScript, IEplTimeRuleScript> resultMap = Maps.newHashMap();
        if (CollectionUtils.isEmpty(rules)) return resultMap;

        for (EplTimeRuleEduGroupScript ruleScript : rules)
        {
            final String configKey = "time_rule_script_" + ruleScript.getCode();
            final String scriptTitle = "Скрипт " + ruleScript.getTitle();
            final String defScriptPath = EplTimeRuleDao.findScriptPath(ruleScript.getDefaultScriptPath());

            try
            {
                final IScriptWrapper<IEplTimeRuleScript, EplTimeRuleEduGroupScript> wrapper = this.buildScriptWrapper(ruleScript, IEplTimeRuleScript.class, configKey, defScriptPath, scriptTitle);
                resultMap.put(ruleScript, wrapper.getScript());
            }
            catch (Exception t)
            {
                registerScriptException(ruleScript, t);
                throw new ApplicationException(getScriptExceptionMessage(ruleScript), t);
            }
        }
        return resultMap;
    }

    @Override
    public IRuleResult count(EplTimeRuleEduGroupScript ruleScript, IEplTimeRuleScript script, ILoadData loadData)
    {
        try
        {
            EplEduGroup eduGroup = loadData.eduGroup();
            if (null != ruleScript.getGroupType() && !ruleScript.getGroupType().equals(eduGroup.getGroupType())) return null;
            if (null != ruleScript.getRegStructure() && !checkRegistryStructure(eduGroup.getRegistryElementPart().getRegistryElement().getParent(), ruleScript.getRegStructure())) return null;

            if (ruleScript.isCounterEnabled())
            {
                if (ruleScript.getRequestCount() == Integer.MAX_VALUE)
                {
                    ruleScript.setTotalTimeWork(0);
                    ruleScript.setRequestCount(0);
                    update(ruleScript);
                }

                long startTime = System.nanoTime();
                IRuleResult result = script.count(loadData);
                long scriptTime = (System.nanoTime() - startTime) / 1000;

                if (null == result) return getResult(scriptTime);
                return getResult(result.hours(), result.description(), scriptTime);
            }
            return script.count(loadData);
        }
        catch (Exception t)
        {
            registerScriptException(ruleScript, t);
            throw new ApplicationException(getScriptExceptionMessage(ruleScript), t);
        }
    }

    private void registerScriptException(EplTimeRuleEduGroupScript ruleScript, Exception t)
    {
        // регистрируем ошибку и помещаем ее в "Список ошибок". Если это не пользовательский скрипт, то еще выводим в лог.
        Debug.exception("Call " + (ruleScript.isHasUserScript() ? "user" : "default") + " script [id = " + ruleScript.getId() + ", code = " + ruleScript.getCode(), t);
        Debug.saveDebug();
    }

    private String getScriptExceptionMessage(EplTimeRuleEduGroupScript ruleScript)
    {
        return "Ошибка норм-скриптов. Проверьте список ошибок.";
    }

    @Override
    public IRuleResult count(EplTimeRuleEduGroupFormula timeRule, ILoadData loadData)
    {
        EplEduGroup eduGroup = loadData.eduGroup();
        EppGroupType groupType = eduGroup.getGroupType();
        boolean apply = groupType.equals(timeRule.getGroupType()) && checkRegistryStructure(eduGroup.getRegistryElementPart().getRegistryElement().getParent(), timeRule.getEppRegistryStructure());
        if (!apply) return null;

        Integer students = 0;
        Double load = 0d;
        double timeAmount = timeRule.getCoefficient();

        if (timeRule.isMultiplyStudents())
        {
            students = loadData.groupContent().values().stream().mapToInt(Integer::intValue).sum();
            timeAmount = timeAmount * students;
        }
        if (timeRule.isMultiplyHours())
        {
            String fullCode = FULL_CODES_CACHE.get(groupType.getCode());
            if (null == fullCode)
            {
                if (groupType instanceof EppGroupTypeALT)
                    fullCode = getNotNull(EppALoadType.class, EppALoadType.eppGroupType(), (EppGroupTypeALT) groupType).getFullCode();
                else if (groupType instanceof EppGroupTypeFCA)
                    fullCode = getNotNull(EppFControlActionType.class, EppFControlActionType.eppGroupType(), (EppGroupTypeFCA) groupType).getFullCode();
                else
                    throw new IllegalStateException();
            }
            load = loadData.discData().getLoadAsDouble(fullCode);
            timeAmount = timeAmount * load;
        }

        if (UniEppUtils.eq(timeAmount, 0)) return null;

        String description = "План. поток: "+ eduGroup.getTitle() + ", " + groupType.getShortTitle() + ", дисциплина: " + eduGroup.getRegistryElementPart().getTitleWithNumber() + ", нагрузка: " + load + ", студенты: " + students;
        return getResult(timeAmount, description);
    }

    @Override
    public void doSaveUserScript(EplTimeRuleEduGroupScript catalogItem)
    {
        if (StringUtils.isBlank(catalogItem.getDefaultScriptPath()) && StringUtils.isBlank(catalogItem.getUserScript())) {
            throw new ApplicationException("Для нормы времени не задан скрипт расчета.");
        }
        super.doSaveUserScript(catalogItem);
    }

    @Override
    public void doAllCountersScriptActivate()
    {
        new DQLUpdateBuilder(EplTimeRuleEduGroupScript.class)
                .where(eq(property(EplTimeRuleEduGroupScript.enabled()), value(Boolean.TRUE)))
                .set(EplTimeRuleEduGroupScript.counterEnabled().s(), value(Boolean.TRUE))
                .createStatement(getSession()).execute();
    }

    @Override
    public void doAllCountersScriptDeactivate()
    {
        new DQLUpdateBuilder(EplTimeRuleEduGroupScript.class)
                .set(EplTimeRuleEduGroupScript.counterEnabled().s(), value(Boolean.FALSE))
                .createStatement(getSession()).execute();
    }

    @Override
    public void doCountersScriptClear(Long ruleId)
    {
        new DQLUpdateBuilder(EplTimeRuleEduGroupScript.class)
                .set(EplTimeRuleEduGroupScript.totalTimeWork().s(), value(0))
                .set(EplTimeRuleEduGroupScript.requestCount().s(), value(0))
                .where(ruleId == null ? null : eq(property(EplTimeRuleEduGroupScript.id()), value(ruleId)))
                .createStatement(getSession()).execute();
    }

    @Override
    public String getDefaultScriptText(EplTimeRuleEduGroupScript timeRule) {
        if (StringUtils.isBlank(timeRule.getDefaultScriptPath())) {
            throw new ApplicationException("Для нормы времени не задан скрипт расчета.");
        }
        return UniScriptDao.getScriptText(UniScriptDao.findScriptPath(timeRule.getDefaultScriptPath()));
    }

    private boolean checkRegistryStructure(EppRegistryStructure elementStructure, EppRegistryStructure ruleStructure)
    {
        if (ruleStructure.equals(elementStructure))
            return true;
        if (null == elementStructure)
            return false;
        return checkRegistryStructure(elementStructure.getParent(), ruleStructure);
    }

    private static final Map<String, String> FULL_CODES_CACHE = new HashMap<>();
    static {
        FULL_CODES_CACHE.put(EppGroupTypeALTCodes.TYPE_LABS, EppALoadType.FULL_CODE_TOTAL_LABS);
        FULL_CODES_CACHE.put(EppGroupTypeALTCodes.TYPE_LECTURES, EppALoadType.FULL_CODE_TOTAL_LECTURES);
        FULL_CODES_CACHE.put(EppGroupTypeALTCodes.TYPE_PRACTICE, EppALoadType.FULL_CODE_TOTAL_PRACTICE);

        FULL_CODES_CACHE.put(EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_PROJECT, EppFControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_PROJECT);
        FULL_CODES_CACHE.put(EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_WORK, EppFControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_WORK);
        FULL_CODES_CACHE.put(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM, EppFControlActionType.FULL_CODE_CONTROL_ACTION_EXAM);
        FULL_CODES_CACHE.put(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM_ACCUM, EppFControlActionType.FULL_CODE_CONTROL_ACTION_EXAM_ACCUM);
        FULL_CODES_CACHE.put(EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF, EppFControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF);
        FULL_CODES_CACHE.put(EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF_DIFF, EppFControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF_DIFF);
        FULL_CODES_CACHE.put(EppGroupTypeFCACodes.CONTROL_ACTION_CONTROL_WORK, EppFControlActionType.FULL_CODE_CONTROL_ACTION_CONTROL_WORK);
    }
}