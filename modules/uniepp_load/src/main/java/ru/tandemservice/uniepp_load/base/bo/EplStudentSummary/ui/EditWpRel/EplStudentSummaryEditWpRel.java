/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EditWpRel;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.select.ISelectValueStyleSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.list.IViewSelectValueStyle;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.NonSelectableValueStyle;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.core.CoreStringUtils.getLikePredicate;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.coalesce;
import static org.tandemframework.hibsupport.dql.DQLFunctions.concat;

/**
 * @author Nikolay Fedorovskih
 * @since 17.11.2014
 */
@Configuration
public class EplStudentSummaryEditWpRel extends BusinessComponentManager
{
    public static final String WORK_PLAN_DS = "workPlanDS";
    public static final String PARAM_STUDENT = "student";
    public static final String PARAM_TERM = "term";
    public static final String WORK_PLAN = "workPlan";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(WORK_PLAN_DS, workPlanDSHandler()).valueStyleSource(new ISelectValueStyleSource(){
                    @Override
                    public IViewSelectValueStyle getSelectValueStyle(Object value)
                    {
                        if (value instanceof ViewWrapper) // показывать ранее выбранный РУП, даже если он более не доступен по логике работы формы серым
                            return NonSelectableValueStyle.INSTANCE;
                        return null;
                    }
                }))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> workPlanDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {

                final Term term = context.get(PARAM_TERM);
                final EplStudent student = context.get(PARAM_STUDENT);
                final EduProgramSubject programSubject = student != null ? student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject() : null;
                String filter = input.getComboFilterByValue();

                if (null == student || null == programSubject) {
                    return ListOutputBuilder.get(input, new ArrayList()).build();
                }

                List<IEntity> resultList = getResultList(input, term, student, programSubject);
                final EppWorkPlanBase currentWorkPlan = context.get(WORK_PLAN);
                boolean missCurrentValue = currentWorkPlan != null && resultList.stream().filter(w -> currentWorkPlan.getId().equals(w.getId())).count() == 0;
                if (missCurrentValue)
                    //создаем фейковый ранее выбранный РУП, даже если он более не доступен по логике работы формы
                    resultList.add(new ViewWrapper<>(currentWorkPlan));


                if (StringUtils.isNotEmpty(filter))
                    resultList = resultList.stream().filter(getLikePredicate(filter, w -> w.getProperty("title").toString())).collect(Collectors.toList());

                context.put(UIDefines.COMBO_OBJECT_LIST, resultList);

                DSOutput output = super.execute(input, context);
                output.setTotalSize(resultList.size());
                return output;
            }

            private List<IEntity> getResultList(DSInput input, Term term, EplStudent student, EduProgramSubject programSubject)
            {
                String alias = "wpb";
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppWorkPlanBase.class, alias).column(property(alias));

                FilterUtils.applySelectFilter(dql, alias, EppWorkPlanBase.id(), input.getPrimaryKeys());

                final DQLSelectBuilder wpDql = new DQLSelectBuilder().fromEntity(EppWorkPlan.class, "wpSub");
                wpDql.column(property("wpSub.id"));

                wpDql.where(eq(property("wpSub", EppWorkPlan.term()), value(term)));
                wpDql.where(eq(property("wpSub", EppWorkPlan.year()), value(student.getGroup().getSummary().getEppYear())));
                wpDql.joinPath(DQLJoinType.inner, EppWorkPlan.parent().eduPlanVersion().eduPlan().fromAlias("wpSub"), "plan");
                wpDql.joinEntity("plan", DQLJoinType.inner, EppEduPlanProf.class, "prof", eq(property("plan"), property("prof")));
                wpDql.where(eq(property("prof", EppEduPlanProf.programForm()), value(student.getEducationOrgUnit().getDevelopForm().getProgramForm())));
                wpDql.where(eq(property("prof", EppEduPlanProf.developGrid()), value(student.getDevelopGrid())));
                wpDql.where(eq(property("prof", EppEduPlanProf.developCondition()), value(student.getEducationOrgUnit().getDevelopCondition())));
                wpDql.where(eqNullSafe(property("prof", EppEduPlanProf.programTrait()), value(student.getEducationOrgUnit().getDevelopTech().getProgramTrait())));

                wpDql.where(eq(property("prof", EppEduPlanProf.programSubject()), value(programSubject)));

                if (IEppSettingsDAO.instance.get().getGlobalSettings().isCheckElementState())
                    dql.where(eq(property(alias, EppWorkPlanBase.state().code()), value(EppState.STATE_ACCEPTED)));

                dql.joinEntity(alias, DQLJoinType.left, EppWorkPlan.class, "wp", eq(property(alias, "id"), property("wp.id")));
                dql.joinEntity(alias, DQLJoinType.left, EppWorkPlanVersion.class, "ver", eq(property(alias, "id"), property("ver.id")));
                dql.joinPath(DQLJoinType.left, EppWorkPlanVersion.parent().fromAlias("ver"), "ver_p");

                // джойним семестр (либо к РУП либо к версии) для сортировки
                dql.joinEntity(alias, DQLJoinType.inner, Term.class, "t",
                               eq(coalesce(property("wp", EppWorkPlan.term()), property("ver_p", EppWorkPlan.term())), property("t"))
                );
                // джойним учебный год (либо к РУП либо к версии) для сортировки
                dql.joinEntity(alias, DQLJoinType.inner, EppYearEducationProcess.class, "y",
                               eq(coalesce(property("wp", EppWorkPlan.year()), property("ver_p", EppWorkPlan.year())), property("y"))
                );

                // применяем основные фильтры к РУПам
                dql.where(in(coalesce(property("wp.id"), property("ver_p")), wpDql.buildQuery()));

                // Сначала сортируем по номеру. Номер версии получается из номера РУПа и номера самой версии через точку
                dql.order(coalesce(property("wp", EppWorkPlan.number()), concat(property("ver_p", EppWorkPlan.number()), value("."), property("ver", EppWorkPlanVersion.number()))));

                dql.order(property("t", Term.intValue()));
                dql.order(property("y", EppYearEducationProcess.educationYear().intValue()));

                return dql.createStatement(getSession()).list();
            }
        };
    }
}