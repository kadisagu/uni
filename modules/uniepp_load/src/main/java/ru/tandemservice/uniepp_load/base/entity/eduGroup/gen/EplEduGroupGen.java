package ru.tandemservice.uniepp_load.base.entity.eduGroup.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * План. поток
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplEduGroupGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup";
    public static final String ENTITY_NAME = "eplEduGroup";
    public static final int VERSION_HASH = 1625312827;
    private static IEntityMeta ENTITY_META;

    public static final String L_SUMMARY = "summary";
    public static final String L_YEAR_PART = "yearPart";
    public static final String L_REGISTRY_ELEMENT_PART = "registryElementPart";
    public static final String L_GROUP_TYPE = "groupType";
    public static final String P_TITLE = "title";

    private EplStudentSummary _summary;     // Сводка
    private YearDistributionPart _yearPart;     // Часть года
    private EppRegistryElementPart _registryElementPart;     // Дисциплиночасть
    private EppGroupType _groupType;     // Вид учебной группы
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сводка. Свойство не может быть null.
     */
    @NotNull
    public EplStudentSummary getSummary()
    {
        return _summary;
    }

    /**
     * @param summary Сводка. Свойство не может быть null.
     */
    public void setSummary(EplStudentSummary summary)
    {
        dirty(_summary, summary);
        _summary = summary;
    }

    /**
     * @return Часть года. Свойство не может быть null.
     */
    @NotNull
    public YearDistributionPart getYearPart()
    {
        return _yearPart;
    }

    /**
     * @param yearPart Часть года. Свойство не может быть null.
     */
    public void setYearPart(YearDistributionPart yearPart)
    {
        dirty(_yearPart, yearPart);
        _yearPart = yearPart;
    }

    /**
     * @return Дисциплиночасть. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPart getRegistryElementPart()
    {
        return _registryElementPart;
    }

    /**
     * @param registryElementPart Дисциплиночасть. Свойство не может быть null.
     */
    public void setRegistryElementPart(EppRegistryElementPart registryElementPart)
    {
        dirty(_registryElementPart, registryElementPart);
        _registryElementPart = registryElementPart;
    }

    /**
     * @return Вид учебной группы. Свойство не может быть null.
     */
    @NotNull
    public EppGroupType getGroupType()
    {
        return _groupType;
    }

    /**
     * @param groupType Вид учебной группы. Свойство не может быть null.
     */
    public void setGroupType(EppGroupType groupType)
    {
        dirty(_groupType, groupType);
        _groupType = groupType;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EplEduGroupGen)
        {
            setSummary(((EplEduGroup)another).getSummary());
            setYearPart(((EplEduGroup)another).getYearPart());
            setRegistryElementPart(((EplEduGroup)another).getRegistryElementPart());
            setGroupType(((EplEduGroup)another).getGroupType());
            setTitle(((EplEduGroup)another).getTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplEduGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplEduGroup.class;
        }

        public T newInstance()
        {
            return (T) new EplEduGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "summary":
                    return obj.getSummary();
                case "yearPart":
                    return obj.getYearPart();
                case "registryElementPart":
                    return obj.getRegistryElementPart();
                case "groupType":
                    return obj.getGroupType();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "summary":
                    obj.setSummary((EplStudentSummary) value);
                    return;
                case "yearPart":
                    obj.setYearPart((YearDistributionPart) value);
                    return;
                case "registryElementPart":
                    obj.setRegistryElementPart((EppRegistryElementPart) value);
                    return;
                case "groupType":
                    obj.setGroupType((EppGroupType) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "summary":
                        return true;
                case "yearPart":
                        return true;
                case "registryElementPart":
                        return true;
                case "groupType":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "summary":
                    return true;
                case "yearPart":
                    return true;
                case "registryElementPart":
                    return true;
                case "groupType":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "summary":
                    return EplStudentSummary.class;
                case "yearPart":
                    return YearDistributionPart.class;
                case "registryElementPart":
                    return EppRegistryElementPart.class;
                case "groupType":
                    return EppGroupType.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplEduGroup> _dslPath = new Path<EplEduGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplEduGroup");
    }
            

    /**
     * @return Сводка. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup#getSummary()
     */
    public static EplStudentSummary.Path<EplStudentSummary> summary()
    {
        return _dslPath.summary();
    }

    /**
     * @return Часть года. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup#getYearPart()
     */
    public static YearDistributionPart.Path<YearDistributionPart> yearPart()
    {
        return _dslPath.yearPart();
    }

    /**
     * @return Дисциплиночасть. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup#getRegistryElementPart()
     */
    public static EppRegistryElementPart.Path<EppRegistryElementPart> registryElementPart()
    {
        return _dslPath.registryElementPart();
    }

    /**
     * @return Вид учебной группы. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup#getGroupType()
     */
    public static EppGroupType.Path<EppGroupType> groupType()
    {
        return _dslPath.groupType();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EplEduGroup> extends EntityPath<E>
    {
        private EplStudentSummary.Path<EplStudentSummary> _summary;
        private YearDistributionPart.Path<YearDistributionPart> _yearPart;
        private EppRegistryElementPart.Path<EppRegistryElementPart> _registryElementPart;
        private EppGroupType.Path<EppGroupType> _groupType;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сводка. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup#getSummary()
     */
        public EplStudentSummary.Path<EplStudentSummary> summary()
        {
            if(_summary == null )
                _summary = new EplStudentSummary.Path<EplStudentSummary>(L_SUMMARY, this);
            return _summary;
        }

    /**
     * @return Часть года. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup#getYearPart()
     */
        public YearDistributionPart.Path<YearDistributionPart> yearPart()
        {
            if(_yearPart == null )
                _yearPart = new YearDistributionPart.Path<YearDistributionPart>(L_YEAR_PART, this);
            return _yearPart;
        }

    /**
     * @return Дисциплиночасть. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup#getRegistryElementPart()
     */
        public EppRegistryElementPart.Path<EppRegistryElementPart> registryElementPart()
        {
            if(_registryElementPart == null )
                _registryElementPart = new EppRegistryElementPart.Path<EppRegistryElementPart>(L_REGISTRY_ELEMENT_PART, this);
            return _registryElementPart;
        }

    /**
     * @return Вид учебной группы. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup#getGroupType()
     */
        public EppGroupType.Path<EppGroupType> groupType()
        {
            if(_groupType == null )
                _groupType = new EppGroupType.Path<EppGroupType>(L_GROUP_TYPE, this);
            return _groupType;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EplEduGroupGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EplEduGroup.class;
        }

        public String getEntityName()
        {
            return "eplEduGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
