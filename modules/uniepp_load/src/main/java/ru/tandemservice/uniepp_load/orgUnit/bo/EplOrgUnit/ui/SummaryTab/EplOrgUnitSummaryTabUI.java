/* $Id:$ */
package ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.SummaryTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.EplOuSummaryDSHandler;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTabUI;

/**
 * @author oleyba
 * @since 5/5/12
 */
@Input({
    @Bind(key = EplOrgUnitTabUI.BIND_ORG_UNIT_ID, binding = "holder.id", required = true)
})
public class EplOrgUnitSummaryTabUI extends UIPresenter
{
    private EntityHolder<OrgUnit> holder = new EntityHolder<OrgUnit>();

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EplOuSummaryDSHandler.PARAM_ORG_UNIT, getHolder().getValue());
    }

    // getters and setters

    public EntityHolder<OrgUnit> getHolder()
    {
        return holder;
    }
}

