package ru.tandemservice.uniepp_load.migration;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_load_2x7x1_8to9 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Map<MultiKey, Long> ouSummaryByKey = new HashMap<>();
        Map<Long, Long> ouSummaryById = new HashMap<>();
        Map<Long, Long> studentSummaryMap = new HashMap<>();

        Statement stmt = tool.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery("select ous.id, ous.orgunit_id, gs.summary_id from epl_ou_summary_t ous join epl_edu_group_summary_t gs on ous.edugroupsummary_id=gs.id");
        while(rs.next())
        {
            Long ous_id = rs.getLong(1);
            Long ou_id = rs.getLong(2);
            Long ss_id = rs.getLong(3);
            MultiKey key = new MultiKey(ou_id, ss_id);
            Long candidate = ouSummaryByKey.get(key);
            if (null == candidate) {
                ouSummaryByKey.put(key, candidate = ous_id);
                studentSummaryMap.put(candidate, ss_id);
            }
            ouSummaryById.put(ous_id, candidate);
        }

        tool.dropConstraint("epl_ou_summary_t", "fk_edugroupsummary_abc1337f");
        tool.dropConstraint("epl_ou_summary_t", "uk_naturalid_eplorgunitsummary");

        for (Map.Entry<Long, Long> e : ouSummaryById.entrySet()) {
            Long id = e.getKey();
            Long candidate = e.getValue();

            if (!id.equals(candidate)) {
                tool.executeUpdate("update epl_time_item_t set summary_id = ? where summary_id = ?", candidate, id);
                tool.executeUpdate("delete from epl_ou_summary_t where id = ?", id);
            } else {
                tool.executeUpdate("update epl_ou_summary_t set edugroupsummary_id = ? where id = ?", studentSummaryMap.get(id), id);
            }
        }

        tool.renameColumn("epl_ou_summary_t", "edugroupsummary_id", "studentsummary_id");
    }
}