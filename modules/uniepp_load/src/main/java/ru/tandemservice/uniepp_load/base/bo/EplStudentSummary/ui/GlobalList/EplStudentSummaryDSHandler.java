/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GlobalList;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;

/**
 * @author oleyba
 * @since 10/6/11
 */
public class EplStudentSummaryDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{

    public EplStudentSummaryDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLOrderDescriptionRegistry registry = new DQLOrderDescriptionRegistry(EplStudentSummary.class, "e")
            .setOrders(EplStudentSummary.eppYear().title().s(), new OrderDescription(EplStudentSummary.eppYear().educationYear().intValue().s()));
        DQLSelectBuilder dql = registry.buildDQLSelectBuilder();
        FilterUtils.applySelectFilter(dql, "e", EplStudentSummary.eppYear().educationYear(), context.get("eduYear"));
        registry.applyOrder(dql, input.getEntityOrder());
        return DQLSelectOutputBuilder.get(input, dql, context.getSession()).build();
    }

}