/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplState.logic;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp_load.base.bo.EplState.EplStateManager;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplStudentSummaryState;

import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 08.02.2016
 */
public class EplStateDao extends UniBaseDao implements IEplStateDao
{
    @Override
    public boolean isCanChangeStateEppObject(EplStudentSummary summary, String newStateCode)
    {
        String oldStateCode = summary.getState().getCode();

        if (!EplStudentSummaryState.canChangeState(oldStateCode, newStateCode))
        {
            final EplStudentSummaryState newState = getByCode(EplStudentSummaryState.class, newStateCode);
            final List<String> fromStateCode = newState.getPossibleTransitions();
            final List<String> fromStateTitles = getPropertiesList(EplStudentSummaryState.class, EplStudentSummaryState.code(), fromStateCode, true, EplStudentSummaryState.title());
            String message = EplStateManager.instance().getProperty("error.cant-change-state-from-state", newState.getTitle(), fromStateTitles.stream().map(s -> "«" + s + "»").collect(Collectors.joining(" или ")));
            if (null != message)
                throw new ApplicationException(message);
        }
        return true;
    }

    @Override
    public void changeSummaryState(EplStudentSummary summary, String oldStateCode, String newStateCode)
    {
        String newCode = oldStateCode.equals(summary.getState().getCode()) ? newStateCode : oldStateCode;
        if (isCanChangeStateEppObject(summary, newCode))
        {
            EplStudentSummaryState newState = getByCode(EplStudentSummaryState.class, newCode);
            summary.setState(newState);
            update(summary);
        }
    }

    @Override
    public EplStudentSummaryStateWrapper getSummaryStateTitle(EplStudentSummary summary, String currentStateCode, boolean needNext)
    {
        EplStudentSummaryState summaryState = summary.getState();

        EplStudentSummaryState currentState = getByCode(EplStudentSummaryState.class, currentStateCode);
        String suffix = currentState.getPermissionSuffix();

        String move = needNext ? "next" : "back";
        String propertyTitle = "ui.state." + suffix + "." + move;

        String title = EplStateManager.instance().getProperty(propertyTitle);
        String alert = EplStateManager.instance().getProperty(propertyTitle + ".alert");

        EplStudentSummaryState nextState = get(EplStudentSummaryState.class, EplStudentSummaryState.priority(), currentState.getPriority() + 1);
        String permissionSuffix = needNext && nextState != null ? nextState.getPermissionSuffix() : suffix;
        boolean visible = summaryState.equals(currentState) || nextState != null && summaryState.equals(nextState);

        return new EplStudentSummaryStateWrapper(title, alert, getPermissionStr(permissionSuffix), visible);
    }

    @Override
    public boolean isCurrentStateMore(EplStudentSummaryState currentState, String stateCode)
    {
        EplStudentSummaryState state = getByCode(EplStudentSummaryState.class, stateCode);
        return currentState.getPriority() > state.getPriority();
    }

    @Override
    public boolean isAllOuSummaryEqStatus(EplStudentSummary summary, String stateCode, List<Long> eplOrgUnitSummaryIds)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EplOrgUnitSummary.class, "s")
                .column(property("s.id")).top(1)
                .where(ne(property("s", EplOrgUnitSummary.state().code()), value(stateCode)));
        if (summary != null) {
            dql.where(eq(property("s", EplOrgUnitSummary.studentSummary()), value(summary)));
        }
        if (CollectionUtils.isNotEmpty(eplOrgUnitSummaryIds)) {
            dql.where(in(property("s", EplOrgUnitSummary.id()), eplOrgUnitSummaryIds));
        }
        Long resultId = dql.createStatement(getSession()).uniqueResult();
        return resultId == null;
    }

    @Override
    public boolean isCanChangeStateOuSummary(EplOrgUnitSummary summary)
    {
        getSession().refresh(summary.getStudentSummary());
        return summary.getStudentSummary().getState().isTimeItem();
    }

    public class EplStudentSummaryStateWrapper
    {
        private String _title;
        private String _alert;
        private String _permissionKey;
        private boolean _visible;

        public EplStudentSummaryStateWrapper(String title, String alert, String permissionKey, boolean visible)
        {
            _title = title;
            _alert = alert;
            _permissionKey = permissionKey;
            _visible = visible;
        }

        public String getTitle() { return _title; }
        public String getAlert() { return _alert; }
        public String getPermissionKey() { return _permissionKey; }
        public boolean isVisible() { return _visible; }
    }

    public static String getPermissionStr(String suffix)
    {
        return "changeStateTo_" + suffix + "_eplStudentSummary";
    }
}
