/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Merge;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplStudentSummaryStateCodes;
import ru.tandemservice.uniepp_load.report.bo.EplReport.EplReportManager;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 17.08.15
 */
@Configuration
public class EplStudentSummaryMerge extends BusinessComponentManager
{
    public static final String DS_STUDENT_SUMMARY = "studentSummaryDS";
    public static final String DS_STUDENT_SUMMARY_MERGE = "studentSummaryMergeDS";

    public static final String PARAM_SUMMARY = "summary";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EplReportManager.instance().eduYearDSConfig())
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_STUDENT_SUMMARY, getName(), EplStudentSummary.defaultNotArchiveSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> dql.where(eq(property(alias, EplStudentSummary.state().code()), value(EplStudentSummaryStateCodes.CONTINGENT_PLANNING)))
                )))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_STUDENT_SUMMARY_MERGE, getName(), EplStudentSummary.defaultNotArchiveSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> {
                            EplStudentSummary summary = context.get(PARAM_SUMMARY);
                            if (null == summary)
                                return dql.where(nothing());

                            dql.where(eq(property(alias, EplStudentSummary.state().code()), value(EplStudentSummaryStateCodes.CONTINGENT_PLANNING)));
                            dql.where(ne(property(alias), value(summary)));
                            return dql;
                        })
                ))
                .create();
    }
}