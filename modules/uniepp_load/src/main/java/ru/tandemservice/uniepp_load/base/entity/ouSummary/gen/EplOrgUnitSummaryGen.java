package ru.tandemservice.uniepp_load.base.entity.ouSummary.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расчет учебной нагрузки на читающем подразделении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplOrgUnitSummaryGen extends EntityBase
 implements INaturalIdentifiable<EplOrgUnitSummaryGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary";
    public static final String ENTITY_NAME = "eplOrgUnitSummary";
    public static final int VERSION_HASH = -320918345;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_SUMMARY = "studentSummary";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_CREATION_DATE = "creationDate";
    public static final String L_STATE = "state";
    public static final String P_RATE_HOURS = "rateHours";
    public static final String P_HOURLY_FUND = "hourlyFund";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_TITLE = "title";

    private EplStudentSummary _studentSummary;     // Сводка контингента
    private OrgUnit _orgUnit;     // Читающее подразделение
    private Date _creationDate;     // Дата создания объекта
    private EppState _state;     // Состояние
    private int _rateHours;     // Объем учебной нагрузки на ставку
    private int _hourlyFund;     // Почасовой фонд

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сводка контингента. Свойство не может быть null.
     */
    @NotNull
    public EplStudentSummary getStudentSummary()
    {
        return _studentSummary;
    }

    /**
     * @param studentSummary Сводка контингента. Свойство не может быть null.
     */
    public void setStudentSummary(EplStudentSummary studentSummary)
    {
        dirty(_studentSummary, studentSummary);
        _studentSummary = studentSummary;
    }

    /**
     * @return Читающее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Читающее подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Дата создания объекта. Свойство не может быть null.
     */
    @NotNull
    public Date getCreationDate()
    {
        return _creationDate;
    }

    /**
     * @param creationDate Дата создания объекта. Свойство не может быть null.
     */
    public void setCreationDate(Date creationDate)
    {
        dirty(_creationDate, creationDate);
        _creationDate = creationDate;
    }

    /**
     * @return Состояние. Свойство не может быть null.
     */
    @NotNull
    public EppState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние. Свойство не может быть null.
     */
    public void setState(EppState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Объем учебной нагрузки на ставку. Свойство не может быть null.
     */
    @NotNull
    public int getRateHours()
    {
        return _rateHours;
    }

    /**
     * @param rateHours Объем учебной нагрузки на ставку. Свойство не может быть null.
     */
    public void setRateHours(int rateHours)
    {
        dirty(_rateHours, rateHours);
        _rateHours = rateHours;
    }

    /**
     * @return Почасовой фонд. Свойство не может быть null.
     */
    @NotNull
    public int getHourlyFund()
    {
        return _hourlyFund;
    }

    /**
     * @param hourlyFund Почасовой фонд. Свойство не может быть null.
     */
    public void setHourlyFund(int hourlyFund)
    {
        dirty(_hourlyFund, hourlyFund);
        _hourlyFund = hourlyFund;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EplOrgUnitSummaryGen)
        {
            if (withNaturalIdProperties)
            {
                setStudentSummary(((EplOrgUnitSummary)another).getStudentSummary());
                setOrgUnit(((EplOrgUnitSummary)another).getOrgUnit());
            }
            setCreationDate(((EplOrgUnitSummary)another).getCreationDate());
            setState(((EplOrgUnitSummary)another).getState());
            setRateHours(((EplOrgUnitSummary)another).getRateHours());
            setHourlyFund(((EplOrgUnitSummary)another).getHourlyFund());
        }
    }

    public INaturalId<EplOrgUnitSummaryGen> getNaturalId()
    {
        return new NaturalId(getStudentSummary(), getOrgUnit());
    }

    public static class NaturalId extends NaturalIdBase<EplOrgUnitSummaryGen>
    {
        private static final String PROXY_NAME = "EplOrgUnitSummaryNaturalProxy";

        private Long _studentSummary;
        private Long _orgUnit;

        public NaturalId()
        {}

        public NaturalId(EplStudentSummary studentSummary, OrgUnit orgUnit)
        {
            _studentSummary = ((IEntity) studentSummary).getId();
            _orgUnit = ((IEntity) orgUnit).getId();
        }

        public Long getStudentSummary()
        {
            return _studentSummary;
        }

        public void setStudentSummary(Long studentSummary)
        {
            _studentSummary = studentSummary;
        }

        public Long getOrgUnit()
        {
            return _orgUnit;
        }

        public void setOrgUnit(Long orgUnit)
        {
            _orgUnit = orgUnit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EplOrgUnitSummaryGen.NaturalId) ) return false;

            EplOrgUnitSummaryGen.NaturalId that = (NaturalId) o;

            if( !equals(getStudentSummary(), that.getStudentSummary()) ) return false;
            if( !equals(getOrgUnit(), that.getOrgUnit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStudentSummary());
            result = hashCode(result, getOrgUnit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStudentSummary());
            sb.append("/");
            sb.append(getOrgUnit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplOrgUnitSummaryGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplOrgUnitSummary.class;
        }

        public T newInstance()
        {
            return (T) new EplOrgUnitSummary();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentSummary":
                    return obj.getStudentSummary();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "creationDate":
                    return obj.getCreationDate();
                case "state":
                    return obj.getState();
                case "rateHours":
                    return obj.getRateHours();
                case "hourlyFund":
                    return obj.getHourlyFund();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentSummary":
                    obj.setStudentSummary((EplStudentSummary) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "creationDate":
                    obj.setCreationDate((Date) value);
                    return;
                case "state":
                    obj.setState((EppState) value);
                    return;
                case "rateHours":
                    obj.setRateHours((Integer) value);
                    return;
                case "hourlyFund":
                    obj.setHourlyFund((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentSummary":
                        return true;
                case "orgUnit":
                        return true;
                case "creationDate":
                        return true;
                case "state":
                        return true;
                case "rateHours":
                        return true;
                case "hourlyFund":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentSummary":
                    return true;
                case "orgUnit":
                    return true;
                case "creationDate":
                    return true;
                case "state":
                    return true;
                case "rateHours":
                    return true;
                case "hourlyFund":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentSummary":
                    return EplStudentSummary.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "creationDate":
                    return Date.class;
                case "state":
                    return EppState.class;
                case "rateHours":
                    return Integer.class;
                case "hourlyFund":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplOrgUnitSummary> _dslPath = new Path<EplOrgUnitSummary>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplOrgUnitSummary");
    }
            

    /**
     * @return Сводка контингента. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary#getStudentSummary()
     */
    public static EplStudentSummary.Path<EplStudentSummary> studentSummary()
    {
        return _dslPath.studentSummary();
    }

    /**
     * @return Читающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Дата создания объекта. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary#getCreationDate()
     */
    public static PropertyPath<Date> creationDate()
    {
        return _dslPath.creationDate();
    }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary#getState()
     */
    public static EppState.Path<EppState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Объем учебной нагрузки на ставку. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary#getRateHours()
     */
    public static PropertyPath<Integer> rateHours()
    {
        return _dslPath.rateHours();
    }

    /**
     * @return Почасовой фонд. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary#getHourlyFund()
     */
    public static PropertyPath<Integer> hourlyFund()
    {
        return _dslPath.hourlyFund();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary#getShortTitle()
     */
    public static SupportedPropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EplOrgUnitSummary> extends EntityPath<E>
    {
        private EplStudentSummary.Path<EplStudentSummary> _studentSummary;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<Date> _creationDate;
        private EppState.Path<EppState> _state;
        private PropertyPath<Integer> _rateHours;
        private PropertyPath<Integer> _hourlyFund;
        private SupportedPropertyPath<String> _shortTitle;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сводка контингента. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary#getStudentSummary()
     */
        public EplStudentSummary.Path<EplStudentSummary> studentSummary()
        {
            if(_studentSummary == null )
                _studentSummary = new EplStudentSummary.Path<EplStudentSummary>(L_STUDENT_SUMMARY, this);
            return _studentSummary;
        }

    /**
     * @return Читающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Дата создания объекта. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary#getCreationDate()
     */
        public PropertyPath<Date> creationDate()
        {
            if(_creationDate == null )
                _creationDate = new PropertyPath<Date>(EplOrgUnitSummaryGen.P_CREATION_DATE, this);
            return _creationDate;
        }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary#getState()
     */
        public EppState.Path<EppState> state()
        {
            if(_state == null )
                _state = new EppState.Path<EppState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Объем учебной нагрузки на ставку. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary#getRateHours()
     */
        public PropertyPath<Integer> rateHours()
        {
            if(_rateHours == null )
                _rateHours = new PropertyPath<Integer>(EplOrgUnitSummaryGen.P_RATE_HOURS, this);
            return _rateHours;
        }

    /**
     * @return Почасовой фонд. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary#getHourlyFund()
     */
        public PropertyPath<Integer> hourlyFund()
        {
            if(_hourlyFund == null )
                _hourlyFund = new PropertyPath<Integer>(EplOrgUnitSummaryGen.P_HOURLY_FUND, this);
            return _hourlyFund;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary#getShortTitle()
     */
        public SupportedPropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new SupportedPropertyPath<String>(EplOrgUnitSummaryGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EplOrgUnitSummaryGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EplOrgUnitSummary.class;
        }

        public String getEntityName()
        {
            return "eplOrgUnitSummary";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getShortTitle();

    public abstract String getTitle();
}
