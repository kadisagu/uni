/* $Id:$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeCategory.ui.EduLoadAddEdit;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemAddEditUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;

/**
 * @author oleyba
 * @since 11/28/14
 */
@Input({
    @Bind(key = "catalogItemId", binding = "holder.id"),
    @Bind(key = DefaultCatalogAddEditModel.CATALOG_CODE, binding = "catalogCode")
})
public class EplTimeCategoryEduLoadAddEditUI extends BaseCatalogItemAddEditUI<EplTimeCategoryEduLoad>
{
    public static final String PARAM_CATEGORY_ITEM_ID = "catalogItemId";

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PARAM_CATEGORY_ITEM_ID, getCatalogItem().getId());
    }

    @Override
    public String getAdditionalPropertiesPageName()
    {
        return getClass().getPackage().getName() + ".AdditProps";
    }

    @Override
    public void onClickApply()
    {
        if (isAddForm())
            getCatalogItem().setUserCode(null);
        super.onClickApply();
    }

    public boolean isParentDisabled()
    {
        return !isAddForm() && (
                IUniBaseDao.instance.get().existsEntity(EplTimeCategoryEduLoad.class, EplTimeCategoryEduLoad.parent().s(), getCatalogItem()) ||
                        !CatalogManager.instance().modifyDao().isCatalogItemPropertyEditable(getCatalogItem().getId(), EplTimeCategoryEduLoad.L_PARENT)
        );
    }
}