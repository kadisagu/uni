/* $Id:$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeCategory;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author oleyba
 * @since 11/28/14
 */
@Configuration
public class EplTimeCategoryManager extends BusinessObjectManager
{
    public static EplTimeCategoryManager instance()
    {
        return instance(EplTimeCategoryManager.class);
    }
}
