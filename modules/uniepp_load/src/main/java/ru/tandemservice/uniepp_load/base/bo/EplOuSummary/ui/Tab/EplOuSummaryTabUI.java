/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.Tab;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.log4j.Logger;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniepp.IUniEppDefines;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.EplOuSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.*;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplBaseCategoryDataWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplBaseTimeDataWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplCommonRowWrapperComparator;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.DisciplineTimeDetail.EplOuSummaryDisciplineTimeDetail;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.DisciplineTimeDetail.EplOuSummaryDisciplineTimeDetailUI;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.Edit.EplOuSummaryEdit;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.ItemAddEdit.EplOuSummaryItemAddEdit;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.ItemAddEdit.EplOuSummaryItemAddEditUI;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.RuleTimeDetail.EplOuSummaryRuleTimeDetail;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.TransferItem.EplOuSummaryTransferItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTransferOrgUnitEduGroupTimeItem;
import ru.tandemservice.uniepp_load.catalog.entity.EplPrintScript;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplPrintScriptCodes;

import java.util.*;

import static ru.tandemservice.uniepp_load.catalog.entity.codes.EplTimeItemVariantGroupingCodes.*;

/**
 * @author oleyba
 * @since 5/5/12
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required = true),
        @Bind(key = "selectedTab", binding = "selectedTab")
})
public class EplOuSummaryTabUI extends UIPresenter
{
    public static final String PARAM_CATEGORIES = "categories";
    public static final String PARAM_TRANSFER_CATEGORIES = "transferCategories";
    public static final String PARAM_DISC_OR_RULE_KEY = "discOrRuleKey";

    private EntityHolder<EplOrgUnitSummary> holder = new EntityHolder<>();
    private String selectedTab;

    private EplBaseTimeDataWrapper _dataWrapper;
    private List<EplDiscOrRuleRowWrapper> _tableDiscRows;
    private List<EplDiscOrRuleRowWrapper> _tableDiscByRuleRows;
    private List<EplSimpleRuleRowWrapper> _tableRuleRows;

    private EplCategoryWrapper _category;
    private EplDiscOrRuleRowWrapper _tableDiscRow;
    private EplSimpleRuleRowWrapper _tableRuleRow;
    private List<EplCategoryWrapper> _currentSubcategories;

    private List<Long> _activeRegElementPartIds;

    private boolean _hasIncorrectTime;
    private boolean _hasTimeData;
    private boolean _hasTransferTo;
    private boolean _isBlocked;


    protected static final Logger logger = Logger.getLogger(EplOuSummaryTabUI.class);

    // actions

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        _isBlocked = EplOuSummaryManager.instance().dao().isBlocked(getSummary());
        _hasIncorrectTime = EplOuSummaryManager.instance().dao().hasIncorrectTime(getSummary());
        _activeRegElementPartIds = EplOuSummaryManager.instance().dao().getRegElementPartIdsOnItemTime(getSummary());
        prepareTimeData();
    }

    private void prepareTimeData()
    {
        _tableDiscRows = Lists.newArrayList();
        _tableDiscByRuleRows = Lists.newArrayList();
        _tableRuleRows = Lists.newArrayList();

        _hasTransferTo = ISharedBaseDao.instance.get().existsEntity(EplTransferOrgUnitEduGroupTimeItem.class, EplTransferOrgUnitEduGroupTimeItem.summary().s(), getSummary());
        _dataWrapper = new EplBaseTimeDataWrapper(getSummary(), true, true).fillDataSource(null, true);

        if (!(_hasTimeData = null != _dataWrapper)) return;

        Collection<EplDiscOrRuleRowWrapper> rowsByRegElements = getDiscRowMap().getOrDefault(BY_REG_ELEMENT, Maps.newHashMap()).values();
        Collection<EplDiscOrRuleRowWrapper> rowsByRegElementsAndRule = getDiscRowMap().getOrDefault(BY_REG_ELEMENT_AND_TIME_RULE, Maps.newHashMap()).values();
        Collection<EplDiscOrRuleRowWrapper> rowsByRule = getDiscRowMap().getOrDefault(BY_TIME_RULE, Maps.newHashMap()).values();
        Collection<EplSimpleRuleRowWrapper> simpleRuleRows = getSimpleRuleRowMap().values();

        getTableDiscRows().addAll(rowsByRegElements);
        getTableDiscRows().addAll(rowsByRegElementsAndRule);
        getTableDiscByRuleRows().addAll(rowsByRule);
        getTableRuleRows().addAll(simpleRuleRows);

        Collections.sort(getTableDiscRows(), EplCommonRowWrapperComparator.INSTANCE);
        Collections.sort(getTableDiscByRuleRows(), EplCommonRowWrapperComparator.INSTANCE);
        Collections.sort(getTableRuleRows(), EplCommonRowWrapperComparator.INSTANCE);
    }

    public void onClickAddItem()
    {
        _uiActivation.asRegionDialog(EplOuSummaryItemAddEdit.class).parameter(EplOuSummaryItemAddEditUI.SUMMARY_ID, getSummary().getId()).activate();
        _uiSupport.setRefreshScheduled(true);
    }

    public void onClickTransferItem()
    {
        _uiActivation.asDesktopRoot(EplOuSummaryTransferItem.class).parameter(PUBLISHER_ID, getSummary().getId()).activate();
    }

    public void onClickDiscTransferItem()
    {
        _uiActivation.asDesktopRoot(EplOuSummaryTransferItem.class).parameters(getListenerParameter()).activate();
    }

    public void onClickRecalculate()
    {
        doRecalulate(EplRecalculateSummaryKind.FULL);
    }

    public void onClickCheck()
    {
        doRecalulate(EplRecalculateSummaryKind.CHECK);
    }

    private void doRecalulate(final EplRecalculateSummaryKind kind)
    {
        final String title = (kind == EplRecalculateSummaryKind.FULL ? "Обновление" : "Проверка")
                + " расчета";
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(ProcessState state)
            {
                EplOuSummaryManager.instance().dao().doRecalculateSummary(getSummary().getId(), kind, state);
                return new ProcessResult(title + " завершен" +
                        (kind == EplRecalculateSummaryKind.FULL ? "о." : "а."));
            }
        };

        new BackgroundProcessHolder().start(title, process, ProcessDisplayMode.unknown);
    }

    public void onClickPrint()
    {
        if (!isHasTimeData()) throw new ApplicationException("Нет данных по нагрузке для печати.");
        EplPrintScript scriptItem = DataAccessServices.dao().getByCode(EplPrintScript.class, EplPrintScriptCodes.ORG_UNIT_SUMMARY_A);
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem,
                                                                          IScriptExecutor.TEMPLATE_VARIABLE, new byte[]{},
                                                                          IScriptExecutor.OBJECT_VARIABLE, getSummary().getId());
    }

    public void onClickEduAssignmentPrint()
    {
        if (!isHasTimeData()) throw new ApplicationException("Нет данных по нагрузке для печати.");

        EplPrintScript scriptItem = DataAccessServices.dao().getByCode(EplPrintScript.class, EplPrintScriptCodes.ORG_UNIT_EDU_ASSIGNMENT);
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(
                scriptItem, IScriptExecutor.TEMPLATE_VARIABLE, new byte[]{},
                "ouSummaryId", getSummary().getId(), "yearPartIds", null, "developFormIds", null, "developConditionIds", null
        );
    }

    public void onClickEdit()
    {
        _uiActivation.asRegionDialog(EplOuSummaryEdit.class).parameter(PUBLISHER_ID, getSummary().getId()).activate();
    }

    public void onClickShowDiscDetails()
    {
        getActivationBuilder().asRegionDialog(EplOuSummaryDisciplineTimeDetail.class).parameters(getListenerParameter()).activate();
    }

    public void onClickShowRuleDetails()
    {
        getActivationBuilder().asRegionDialog(EplOuSummaryRuleTimeDetail.class).parameters(getListenerParameter()).activate();
    }

    // utils

    public ParametersMap getRuleParameters()
    {
        return new ParametersMap()
                .add(PUBLISHER_ID, getSummary().getId())
                .add(PARAM_DISC_OR_RULE_KEY, getTableRuleRow().getKey());
    }

    private ParametersMap getDiscParameters()
    {
        return new ParametersMap()
                .add(PUBLISHER_ID, getSummary().getId())
                .add(PARAM_DISC_OR_RULE_KEY, getTableDiscRow().getKey());
    }

    public ParametersMap getDiscTransferParameters()
    {
        Set<Long> categorySet = getTableDiscRow().getGroups().keySet();
        Set<Long> transferCategorySet = getTableDiscRow().getTransferGroups().keySet();

        return getDiscParameters()
                .add(PARAM_CATEGORIES, categorySet)
                .add(PARAM_TRANSFER_CATEGORIES, transferCategorySet);
    }

    public ParametersMap getDiscParameters(boolean transferTo)
    {
        Long categoryId = getCategory().getId();

        return getDiscParameters()
                .add(EplOuSummaryDisciplineTimeDetailUI.PARAM_CATEGORY, categoryId)
                .add(EplOuSummaryDisciplineTimeDetailUI.PARAM_TRANSFER_FORM, transferTo);
    }

    public List<String> getCurrentGroupingCodes()
    {
        List<String> discCodes = Lists.newArrayList(BY_REG_ELEMENT, BY_REG_ELEMENT_AND_TIME_RULE);
        List<String> discByRuleCodes = Lists.newArrayList(BY_TIME_RULE);

        String code = getTableDiscRow().getKey().getGroupingCode();
        if (discCodes.contains(code)) return discCodes;
        else if (discByRuleCodes.contains(code)) return discByRuleCodes;
        else
            throw new IllegalStateException("Unknown grouping for time item.");
    }

    public String getCategoryDiscTimeSum()
    {
        return getCategory().getDisciplineTimeSum(getCurrentGroupingCodes());
    }

    public EplOrgUnitSummary getSummary()
    {
        return getHolder().getValue();
    }

    public String getTotalTimeSum()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(_dataWrapper.getDisciplineTimeSum() + _dataWrapper.getRuleTimeSum());
    }

    public String getRuleTimeSum()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(_dataWrapper.getRuleTimeSum());
    }

    public String getDiscRowTimeSum()
    {
        Double result = _dataWrapper.getDiscTimeSum(false, getCurrentGroupingCodes());
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(result);
    }

    public String getTransferDiscRowTimeSum()
    {
        Double result = _dataWrapper.getDiscTimeSum(true, getCurrentGroupingCodes());
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(result);
    }

    public String getTotalDiscRowTimeSum()
    {
        Double discValue = _dataWrapper.getDiscTimeSum(false, getCurrentGroupingCodes());
        Double transferValue = _dataWrapper.getDiscTimeSum(true, getCurrentGroupingCodes());
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(discValue - transferValue);
    }

    public String getTransferTotalTimeSum()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(_dataWrapper.getTransferTotalRowTimeSum());
    }

    public String getTotalRuleTimeSum()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(_dataWrapper.getRuleTimeSum());
    }

    public String getTotalSum()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(_dataWrapper.getDisciplineTimeSum() + _dataWrapper.getRuleTimeSum() - (_dataWrapper.getTransferTotalRowTimeSum()));
    }

    public int getHeaderRowCount()
    {
        return getCategoryLevelCount() + 1;
    }

    public int getTableColumnCount()
    {
        int count = 0;
        if (isHasTransferTo()) count++;
        if (isTransferCategoriesNotEmpty()) count += getTransferBottomLevelCategories().size() + 2;
        return 4 + getBottomLevelCategories().size() + count;
    }

    public boolean isEditMode()
    {
        return getSummary().getState().getCode().equals(EppState.STATE_FORMATIVE);
    }

    public boolean isVisibleOuSummaryCheck()
    {
        String stateCode = getSummary().getState().getCode();
        return !stateCode.equals(EppState.STATE_ACCEPTED);
    }

    public boolean isTransferCategoriesNotEmpty()
    {
        return !getTransferBottomLevelCategories().isEmpty();
    }

    public boolean isTableDiscRowTransferToDisabled()
    {
        return !isEditMode()
                || (null != getCurrentTransferTo() && getCurrentTransferTo() != getSummary()
                || (null != getTableDiscRow().getKey().getDisciplineId() && !getActiveRegElementPartIds().contains(getTableDiscRow().getKey().getDisciplineId())));
    }

    public EplOrgUnitSummary getCurrentTransferTo()
    {
        return getTableDiscRow().getTransferTo();
    }

    public String getDisciplinePubComponentName()
    {
        EppRegistryElementPart discipline = getTableDiscRow().getDiscipline();
        if (null == discipline) return null;

        if (discipline.getRegistryElement() instanceof EppRegistryAttestation) return IUniEppDefines.EPP_ATTESTATION_REGISTRY_PUB;
        if (discipline.getRegistryElement() instanceof EppRegistryDiscipline) return IUniEppDefines.EPP_DISCIPLINE_REGISTRY_PUB;
        if (discipline.getRegistryElement() instanceof EppRegistryPractice) return IUniEppDefines.EPP_PRACTICE_REGISTRY_PUB;
        return "";
    }

    public String getDiscTimeDetailsId()
    {
        return "timeDetails_" + getTableDiscRow().getFieldId().hashCode() + "_" + getCategory().getId().hashCode();
    }

    public String getTransferDiscTimeDetailsId()
    {
        return "transfer_timeDetails_" + getTableDiscRow().getFieldId().hashCode() + "_" + getCategory().getId().hashCode();
    }

    public String getRuleTimeDetailsId()
    {
        return "timeDetails_" + getTableRuleRow().getId().hashCode() + "_" + getCategory().getId().hashCode();
    }

    public String getTransferRuleTimeDetailsId()
    {
        return "transfer_timeDetails_" + getTableRuleRow().getId().hashCode() + "_" + getCategory().getId().hashCode();
    }

    public int getCategoryRowCount()
    {
        return getCategory().getRowCount(getCategoryLevelCount());
    }

    public String getDiscCellTimeSum()
    {
        return getDiscCellTimeSum(false);
    }

    public String getTransferDiscCellTimeSum()
    {
        return getDiscCellTimeSum(true);
    }

    public String getDiscCellTimeSum(boolean transferTo)
    {
        return _dataWrapper.getDiscCellTimeSum(getCategory(), getTableDiscRow(), transferTo);
    }

    public String getRuleCellTimeSum()
    {
        return _dataWrapper.getRuleCellTimeSum(getCategory(), getTableRuleRow());
    }

    // getters and setters

    public Set<EplTimeCategoryEduLoad> getUsedCategories()
    {
        return getCategoryDataWrapper().getUsedCategories();
    }

    public List<EplCategoryWrapper> getTopLevelCategories()
    {
        return getCategoryDataWrapper().getTopLevelCategories();
    }

    public List<EplCategoryWrapper> getBottomLevelCategories()
    {
        return getCategoryDataWrapper().getBottomLevelCategories();
    }

    public List<List<EplCategoryWrapper>> getSubcategories()
    {
        return getCategoryDataWrapper().getSubcategories();
    }

    public Set<EplTimeCategoryEduLoad> getTransferUsedCategories()
    {
        return getCategoryDataWrapper().getTransferUsedCategories();
    }

    public List<EplCategoryWrapper> getTransferTopLevelCategories()
    {
        return getCategoryDataWrapper().getTransferTopLevelCategories();
    }

    public List<EplCategoryWrapper> getTransferBottomLevelCategories()
    {
        return getCategoryDataWrapper().getTransferBottomLevelCategories();
    }

    public List<List<EplCategoryWrapper>> getTransferSubcategories()
    {
        return getCategoryDataWrapper().getTransferSubcategories();
    }

    public EplCategoryWrapper getCategory()
    {
        return _category;
    }

    public void setCategory(EplCategoryWrapper category)
    {
        _category = category;
    }

    public List<EplCategoryWrapper> getCurrentSubcategories()
    {
        return _currentSubcategories;
    }

    public void setCurrentSubcategories(List<EplCategoryWrapper> currentSubcategories)
    {
        _currentSubcategories = currentSubcategories;
    }

    public EntityHolder<EplOrgUnitSummary> getHolder()
    {
        return holder;
    }

    public String getSelectedTab()
    {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        this.selectedTab = selectedTab;
    }

    public List<EplDiscOrRuleRowWrapper> getTableDiscRows()
    {
        return _tableDiscRows;
    }

    public List<EplDiscOrRuleRowWrapper> getTableDiscByRuleRows()
    {
        return _tableDiscByRuleRows;
    }

    public void setTableDiscRows(List<EplDiscOrRuleRowWrapper> tableDiscRows)
    {
        _tableDiscRows = tableDiscRows;
    }

    public EplDiscOrRuleRowWrapper getTableDiscRow()
    {
        return _tableDiscRow;
    }

    public void setTableDiscRow(EplDiscOrRuleRowWrapper tableDiscRow)
    {
        _tableDiscRow = tableDiscRow;
    }

    public int getCategoryLevelCount()
    {
        return getCategoryDataWrapper().getCategoryLevelCount();
    }

    public Map<String, Map<EplCommonRowWrapper.DisciplineOrRulePairKey, EplDiscOrRuleRowWrapper>> getDiscRowMap()
    {
        return _dataWrapper.getDiscRowMap();
    }

    public Map<EplCommonRowWrapper.DisciplineOrRulePairKey, EplSimpleRuleRowWrapper> getSimpleRuleRowMap()
    {
        return _dataWrapper.getSimpleRuleRowMap().getOrDefault(EplSimpleRuleRowWrapper.BY_SIMPLE_TIME_RULE, Maps.newHashMap());
    }

    public EplBaseTimeDataWrapper getDataWrapper()
    {
        return _dataWrapper;
    }

    public EplBaseCategoryDataWrapper getCategoryDataWrapper()
    {
        return _dataWrapper.getCategoryDataWrapper();
    }

    public boolean isHasIncorrectTime()
    {
        return _hasIncorrectTime;
    }

    public void setHasIncorrectTime(boolean hasIncorrectTime)
    {
        _hasIncorrectTime = hasIncorrectTime;
    }

    public List<Long> getActiveRegElementPartIds()
    {
        return _activeRegElementPartIds;
    }

    public EplSimpleRuleRowWrapper getTableRuleRow()
    {
        return _tableRuleRow;
    }

    public void setTableRuleRow(EplSimpleRuleRowWrapper tableRuleRow)
    {
        _tableRuleRow = tableRuleRow;
    }

    public List<EplSimpleRuleRowWrapper> getTableRuleRows()
    {
        return _tableRuleRows;
    }

    public void setTableRuleRows(List<EplSimpleRuleRowWrapper> tableRuleRows)
    {
        _tableRuleRows = tableRuleRows;
    }

    public boolean isHasTimeData()
    {
        return _hasTimeData;
    }

    public void setHasTimeData(boolean hasTimeData)
    {
        _hasTimeData = hasTimeData;
    }

    public boolean isHasTransferTo()
    {
        return _hasTransferTo;
    }

    public boolean isHasTableDiscRows()
    {
        return !getTableDiscRows().isEmpty();
    }

    public boolean isHasTableDiscByRuleRows()
    {
        return !getTableDiscByRuleRows().isEmpty();
    }

    public boolean isHasTableRuleRows()
    {
        return !getTableRuleRows().isEmpty();
    }

    public boolean isBlocked()
    {
        return _isBlocked;
    }
}
