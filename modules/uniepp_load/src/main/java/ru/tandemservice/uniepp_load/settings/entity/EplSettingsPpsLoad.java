package ru.tandemservice.uniepp_load.settings.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp_load.settings.entity.gen.*;
import ru.tandemservice.uniepp_load.util.UniEppLoadUtils;

/** @see ru.tandemservice.uniepp_load.settings.entity.gen.EplSettingsPpsLoadGen */
public class EplSettingsPpsLoad extends EplSettingsPpsLoadGen
{

    @EntityDSLSupport(parts = EplSettingsPpsLoad.P_HOURS_MIN_AS_LONG)
    public double getHoursMin()
    {
        return UniEppLoadUtils.wrap(getHoursMinAsLong());
    }

    public void setHoursMin(double hours) {
        setHoursMinAsLong(Math.round(100 * hours));
    }

    @EntityDSLSupport(parts = EplSettingsPpsLoad.P_HOURS_MAX_AS_LONG)
    public double getHoursMax()
    {
        return UniEppLoadUtils.wrap(getHoursMaxAsLong());
    }

    public void setHoursMax(double hours) {
        setHoursMaxAsLong(Math.round(100 * hours));
    }

    @EntityDSLSupport
    public double getHoursAdditional()
    {
        return UniEppLoadUtils.wrap(getHoursAdditionalAsLong());
    }

    public void setHoursAdditional(double hours) {
        setHoursAdditionalAsLong(Math.round(100 * hours));
    }

}