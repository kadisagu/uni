package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic;

import org.tandemframework.core.process.ProcessState;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplRecalculateSummaryKind;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleScript;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by nsvetlov on 30.01.2017.
 */
public class EplRecalculateSummaryData
{
    public final EplOrgUnitSummary ouSummary;
    public final EplRecalculateSummaryKind kind;
    public final ProcessState state;
    public final List<EplTimeRuleEduLoad> ruleList;
    public final Set<EplTimeRuleEduLoad> rulesInQueue;
    public final List<EppIControlActionType> icaTypes;
    public final Map<EplTimeRuleEduGroupScript, IEplTimeRuleScript> scriptWrapperMap;
    public final Set<EplTimeRuleEduLoad> disabledRules;

    public EplRecalculateSummaryData(final EplOrgUnitSummary ouSummary,
                                    final EplRecalculateSummaryKind kind,
                                    final ProcessState state,
                                    final List<EplTimeRuleEduLoad> ruleList,
                                    final Set<EplTimeRuleEduLoad> rulesInQueue,
                                    final List<EppIControlActionType> icaTypes,
                                    final Map<EplTimeRuleEduGroupScript, IEplTimeRuleScript> scriptWrapperMap,
                                    final Set<EplTimeRuleEduLoad> disabledRules)
    {
        this.ouSummary = ouSummary;
        this. kind = kind;
        this.state = state;
        this.ruleList = ruleList;
        this.rulesInQueue = rulesInQueue;
        this.icaTypes = icaTypes;
        this.scriptWrapperMap = scriptWrapperMap;
        this.disabledRules = disabledRules;
    }

    public EplRecalculateSummaryData withSummary(final EplOrgUnitSummary ouSummary)
    {
        return new EplRecalculateSummaryData(ouSummary, this.kind, this.state, this.ruleList, this.rulesInQueue, this.icaTypes, this.scriptWrapperMap, this.disabledRules);
    }

}
