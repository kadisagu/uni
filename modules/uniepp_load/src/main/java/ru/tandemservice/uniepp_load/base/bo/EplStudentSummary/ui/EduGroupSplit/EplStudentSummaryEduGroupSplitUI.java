/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EduGroupSplit;

import com.google.common.collect.Lists;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.info.InfoCollector;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic.EplEduGroupSplitType;

import java.util.Collection;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 29.11.2014
 */
@Input(
        @Bind(key = EplStudentSummaryEduGroupSplitUI.BIND_EPL_EDU_GROUP_IDS_PARAM, binding = "ids", required = true)
)
public class EplStudentSummaryEduGroupSplitUI extends UIPresenter
{
    public static final String BIND_EPL_EDU_GROUP_IDS_PARAM = "eplEduGroupIds";

    private DataWrapper _splitType;
    private List<DataWrapper> _splitTypeList;
    private Integer _subGroupSize;
    private Collection<Long> _ids;
    private List<String> _disciplines = Lists.newArrayList();
    private String _currentDiscipline;

    @Override
    public void onComponentRefresh()
    {
        _disciplines = EplStudentSummaryManager.instance().dao().getDisciplinesByGroupIds(_ids);
        _splitTypeList = EplStudentSummaryManager.instance().dao().getSplitTypeList(_ids);

        if (_splitTypeList.isEmpty())
        {
            ErrorCollector errors = UserContext.getInstance().getErrorCollector();
            errors.add("Для выбранных потоков нет доступных вариантов деления.");
            deactivate();
        }
    }

    // Listeners

    public void onClickApply()
    {
        int changeEduGroupCount = 0;
        if (isSplitByCount())
            changeEduGroupCount += EplStudentSummaryManager.instance().dao().massSplitEplEduGroup(getIds(), getSubGroupSize(), isSplitByMaxCount());
        else
            changeEduGroupCount += EplStudentSummaryManager.instance().dao().massSplitEplEduGroup(getIds(), getSplitType());

        InfoCollector info = UserContext.getInstance().getInfoCollector();
        info.add("Разделено потоков: " + changeEduGroupCount);

        deactivate();
    }

    // Getters & Setters

    public DataWrapper getSplitType()
    {
        return _splitType;
    }

    public void setSplitType(DataWrapper splitType)
    {
        _splitType = splitType;
    }

    public List<DataWrapper> getSplitTypeList()
    {
        return _splitTypeList;
    }

    public Integer getSubGroupSize()
    {
        return _subGroupSize;
    }

    public void setSubGroupSize(Integer subGroupSize)
    {
        _subGroupSize = subGroupSize;
    }

    public String getGroupSize()
    {
        PairKey<Integer, Integer> boundary = EplStudentSummaryManager.instance().dao().getEduGroupRowBoundaryCount(getIds());
        if (null == boundary) return "";

        if (boundary.getFirst().equals(boundary.getSecond()))
            return String.valueOf(boundary.getFirst());

        return "от " + boundary.getFirst() + " до " + boundary.getSecond();
    }

    public Collection<Long> getIds()
    {
        return _ids;
    }

    public void setIds(Collection<Long> ids)
    {
        _ids = ids;
    }

    public List<String> getDisciplines()
    {
        return _disciplines;
    }

    public String getCurrentDiscipline()
    {
        return _currentDiscipline;
    }

    public void setCurrentDiscipline(String currentDiscipline)
    {
        _currentDiscipline = currentDiscipline;
    }

    public boolean isSplitByCount()
    {
        return (null != getSplitType() && getSplitType().getId() == EplEduGroupSplitType.BY_COUNT.ordinal()) || isSplitByMaxCount();
    }

    public boolean isSplitByMaxCount()
    {
        return null != getSplitType() && getSplitType().getId() == EplEduGroupSplitType.BY_MAX_COUNT.ordinal();
    }

    public boolean isShowDisciplines()
    {
        return null != getSplitType() && getSplitType().getId() == EplEduGroupSplitType.BY_DISCIPLINE.ordinal();
    }
}