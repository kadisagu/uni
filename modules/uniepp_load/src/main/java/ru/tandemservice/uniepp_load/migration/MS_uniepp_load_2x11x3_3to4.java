package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_load_2x11x3_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eplSettingsPpsLoad

		// создано обязательное свойство hoursMinAsLong
		{
			// создать колонку
			tool.createColumn("epl_settings_pps_load_t", new DBColumn("hoursminaslong_p", DBType.LONG));

			// задать значение по умолчанию
			java.lang.Long defaultHoursMinAsLong = 0L;
			tool.executeUpdate("update epl_settings_pps_load_t set hoursminaslong_p=? where hoursminaslong_p is null", defaultHoursMinAsLong);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epl_settings_pps_load_t", "hoursminaslong_p", false);

		}

		// создано обязательное свойство hoursMaxAsLong
		{
			// создать колонку
			tool.createColumn("epl_settings_pps_load_t", new DBColumn("hoursmaxaslong_p", DBType.LONG));

			// задать значение по умолчанию
			java.lang.Long defaultHoursMaxAsLong = 0L;
			tool.executeUpdate("update epl_settings_pps_load_t set hoursmaxaslong_p=? where hoursmaxaslong_p is null", defaultHoursMaxAsLong);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epl_settings_pps_load_t", "hoursmaxaslong_p", false);

		}

		// создано обязательное свойство hoursAdditionalAsLong
		{
			// создать колонку
			tool.createColumn("epl_settings_pps_load_t", new DBColumn("hoursadditionalaslong_p", DBType.LONG));

			// задать значение по умолчанию
			java.lang.Long defaultHoursAdditionalAsLong = 0L;
			tool.executeUpdate("update epl_settings_pps_load_t set hoursadditionalaslong_p=? where hoursadditionalaslong_p is null", defaultHoursAdditionalAsLong);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epl_settings_pps_load_t", "hoursadditionalaslong_p", false);

		}

		tool.executeUpdate("update epl_settings_pps_load_t set hoursminaslong_p = hoursminlong_p, hoursmaxaslong_p = hoursmaxlong_p, hoursadditionalaslong_p = hoursadditionallong_p");

		// удалено свойство hoursMinLong
		{

			// удалить колонку
			tool.dropColumn("epl_settings_pps_load_t", "hoursminlong_p");

		}

		// удалено свойство hoursMaxLong
		{

			// удалить колонку
			tool.dropColumn("epl_settings_pps_load_t", "hoursmaxlong_p");

		}

		// удалено свойство hoursAdditionalLong
		{
			// удалить колонку
			tool.dropColumn("epl_settings_pps_load_t", "hoursadditionallong_p");

		}

    }
}