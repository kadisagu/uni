/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.ItemAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.EplOuSummaryManager;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplSimpleTimeItem;

/**
 * @author oleyba
 * @since 12/9/14
 */
@Input({
    @Bind(key = EplOuSummaryItemAddEditUI.SUMMARY_ID, binding = "summaryHolder.id"),
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "timeItem.id")
})
public class EplOuSummaryItemAddEditUI extends UIPresenter
{
    public static final String SUMMARY_ID = "summaryId";

    private EntityHolder<EplOrgUnitSummary> summaryHolder = new EntityHolder<>();
    private EplSimpleTimeItem timeItem = new EplSimpleTimeItem();

    @Override
    public void onComponentRefresh()
    {
        if (getTimeItem().getId() != null) {
            setTimeItem(DataAccessServices.dao().getNotNull(EplSimpleTimeItem.class, getTimeItem().getId()));
        } else {
            getSummaryHolder().refresh();
            getTimeItem().setSummary(getSummaryHolder().getValue());
        }
    }

    public void onClickApply() {
        EplOuSummaryManager.instance().dao().saveOrUpdateTimeItem(getTimeItem());
        deactivate();
    }

    // getters and setters

    public boolean isAddForm() {
        return getTimeItem().getId() == null;
    }

    public EntityHolder<EplOrgUnitSummary> getSummaryHolder()
    {
        return summaryHolder;
    }

    public EplSimpleTimeItem getTimeItem()
    {
        return timeItem;
    }

    public void setTimeItem(EplSimpleTimeItem timeItem)
    {
        this.timeItem = timeItem;
    }
}