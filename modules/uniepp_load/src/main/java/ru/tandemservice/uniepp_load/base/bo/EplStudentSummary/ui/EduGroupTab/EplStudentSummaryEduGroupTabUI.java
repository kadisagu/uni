/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EduGroupTab;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSettingsUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp_load.base.bo.EplState.EplStateManager;
import ru.tandemservice.uniepp_load.base.bo.EplState.logic.EplStateDao;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.daemon.EplStudentSummaryDaemonBean;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EduGroupEdit.EplStudentSummaryEduGroupEdit;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EduGroupMerge.EplStudentSummaryEduGroupMerge;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EduGroupMerge.EplStudentSummaryEduGroupMergeUI;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EduGroupSplit.EplStudentSummaryEduGroupSplit;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EduGroupSplit.EplStudentSummaryEduGroupSplitUI;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.InfoBlock.EplStudentSummaryInfoBlockUI;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplStudentSummaryState;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTabUI;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import static ru.tandemservice.uniepp_load.catalog.entity.codes.EplStudentSummaryStateCodes.*;

/**
 * @author oleyba
 * @since 10/24/11
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "summaryHolder.id"),
    @Bind(key = EplOrgUnitTabUI.BIND_ORG_UNIT_ID, binding = "orgUnitHolder.id")
})
public class EplStudentSummaryEduGroupTabUI extends UIPresenter
{
    private EntityHolder<EplStudentSummary> summaryHolder = new EntityHolder<>();
    private EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<>();

    private boolean summaryPub = true;
    private boolean _disabled;
    private boolean _visibleDaemon;
    private EplStateDao.EplStudentSummaryStateWrapper _stateWrapper;
    public static final String MERGE_CHECKBOX = "mergeCheckbox";

    @Override
    public void onComponentRefresh()
    {
        getOrgUnitHolder().refresh();
        if (getOrgUnitHolder().getId() != null) {
            setSummaryPub(false);
            if (getOrgUnitHolder().getId().equals(getSummaryHolder().getId())) {
                getSummaryHolder().setId(null);
                getSummaryHolder().setValue(null);
            }
            getSettings().set(EducationCatalogsManager.PARAM_FORMATIVE_ORGUNIT, getOrgUnitHolder().getValue());
            Long id = _uiSettings.<Long>get("summary." + getOrgUnitHolder().getId());
            final EplStudentSummary iEntity = id == null ? null : DataAccessServices.dao().get(id);
            if (id != null && null == iEntity)
            {
                _uiSettings.<Long>set("summary." + getOrgUnitHolder().getId(), null);
            }
            getSummaryHolder().setValue(iEntity);
            getSummaryHolder().refreshAllowNotExist();
            saveSettingsInternal();
        }
        getSummaryHolder().refresh();
        if (null != getSummary())
        {
            EplStudentSummaryInfoBlockUI.componentActivate(_uiSupport, _uiActivation, getSummary().getId());
            EplStudentSummaryState state = getSummary().getState();
            _stateWrapper = EplStateManager.instance().dao().getSummaryStateTitle(getSummary(), PLANNING_FLOW, state.isPlanningFlow());
            _disabled = EplStateManager.instance().dao().isCurrentStateMore(state, PLANNING_FLOW);
            _visibleDaemon = isOrgUnitEmpty() && !EplStateManager.instance().dao().isCurrentStateMore(state, ATTACH_WORK_PLAN);
        }
        onCheck();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EplStudentSummaryManager.PARAM_SUMMARY, getSummary());
        dataSource.put(EplStudentSummaryManager.PARAM_EPP_YEAR, getSummary() == null ? null : getSummary().getEppYear());
        dataSource.put(EplStudentSummaryManager.PARAM_YEAR_PART, getSettings().get(EplStudentSummaryManager.PARAM_YEAR_PART));
        dataSource.put(EplStudentSummaryManager.PARAM_GROUP_TYPE, getSettings().get(EplStudentSummaryManager.PARAM_GROUP_TYPE));
        dataSource.put(EplStudentSummaryManager.PARAM_SPLIT_VARIANT, getSettings().get(EplStudentSummaryManager.PARAM_SPLIT_VARIANT));
        dataSource.put(EplStudentSummaryManager.PARAM_SPLIT_ELEMENT, getSettings().get(EplStudentSummaryManager.PARAM_SPLIT_ELEMENT));
        dataSource.put(EplStudentSummaryManager.PARAM_SETTINGS, getSettings());
        if (EplStudentSummaryEduGroupTab.DS_EDU_HS.equals(dataSource.getName())) {
            dataSource.putAll(getSettings().getAsMap(EducationCatalogsManager.PARAM_FORMATIVE_ORGUNIT));
        } else if (EplStudentSummaryEduGroupTab.DS_SUMMARY.equals(dataSource.getName())) {
           dataSource.put(EplStudentSummaryManager.PARAM_FORMATIVE_OR_OWNER_ORG_UNIT, getOrgUnitHolder().getValue());
        } else if ("groupDS".equals(dataSource.getName()))
        {
            dataSource.putAll(getSettings().getAsMap("formativeOrgUnit", "eduHS", "developForm", "course"));
        } else if ("eplEduGroupDS".equals(dataSource.getName())) {
            dataSource.put(EplStudentSummaryManager.PARAM_FORMATIVE_OR_OWNER_ORG_UNIT, getOrgUnitHolder().getValue());
        }
    }

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        if (UIDefines.DIALOG_REGION_NAME.equals(childRegionName))
        {
            clearCheckBoxes();
        }
    }

    private void clearCheckBoxes()
    {
        final BaseSearchListDataSource dataSource = getConfig().<BaseSearchListDataSource>getDataSource(EplStudentSummaryEduGroupTab.DS_EPL_EDU_GROUP);
        if (null == dataSource.getLegacyDataSource().getColumn(MERGE_CHECKBOX))
            return;

        dataSource.getOptionColumnSelectedObjects(MERGE_CHECKBOX).clear();
    }

    public void onClickRefresh()
    {
        EplStudentSummaryDaemonBean.DAEMON.wakeUpDaemon();
        deactivate();
    }

    public void onClickMerge()
    {
        final BaseSearchListDataSource dataSource = getConfig().<BaseSearchListDataSource>getDataSource(EplStudentSummaryEduGroupTab.DS_EPL_EDU_GROUP);
        Collection<IEntity> selectedItems;
        if (null == dataSource.getLegacyDataSource().getColumn(MERGE_CHECKBOX) || (selectedItems = dataSource.getOptionColumnSelectedObjects(MERGE_CHECKBOX)).size() < 2)
        {
            ContextLocal.getErrorCollector().add("Для объединения необходимо выбрать хотя бы два потока.");
            onCheck();
            return;
        }

        getActivationBuilder().asRegionDialog(EplStudentSummaryEduGroupMerge.class)
                .parameter(EplStudentSummaryEduGroupMergeUI.BIND_PARAM_MERGED_GROUP_IDS, CommonBaseEntityUtil.getIdList(selectedItems))
                .activate();
    }

    public void onClickEdit()
    {
        getActivationBuilder().asRegionDialog(EplStudentSummaryEduGroupEdit.class)
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, getListenerParameterAsLong())
                .activate();
    }

    public void onClickSplit()
    {
        getActivationBuilder().asRegionDialog(EplStudentSummaryEduGroupSplit.class)
                .parameter(EplStudentSummaryEduGroupSplitUI.BIND_EPL_EDU_GROUP_IDS_PARAM, ImmutableList.of(getListenerParameterAsLong()))
                .activate();
    }

    public void onClickMassSplit()
    {
        final BaseSearchListDataSource dataSource = getConfig().<BaseSearchListDataSource>getDataSource(EplStudentSummaryEduGroupTab.DS_EPL_EDU_GROUP);
        Collection<IEntity> selectedItems;
        if (null == dataSource.getLegacyDataSource().getColumn(MERGE_CHECKBOX) || (selectedItems = dataSource.getOptionColumnSelectedObjects(MERGE_CHECKBOX)).size() < 1)
            throw new ApplicationException("Выберите потоки для разделения.");
        getActivationBuilder().asRegionDialog(EplStudentSummaryEduGroupSplit.class)
                .parameter(EplStudentSummaryEduGroupSplitUI.BIND_EPL_EDU_GROUP_IDS_PARAM, CommonBaseEntityUtil.getIdList(selectedItems))
                .activate();
    }

    public void onChangeSummary()
    {
        if (null != getSummary())
            EplStudentSummaryInfoBlockUI.componentActivate(_uiSupport, _uiActivation, getSummary().getId());
        saveSettings();
        ContextLocal.getDesktop().setRefreshScheduled(true);
    }

    private void saveSettingsInternal()
    {
        if (!isSummaryPub()) {
            getSettings().set("summary." + getOrgUnitHolder().getId(), getSummary() == null ? null : getSummary().getId());
        }
        if (getOrgUnitHolder().getId() != null) {
            getSettings().set(EducationCatalogsManager.PARAM_FORMATIVE_ORGUNIT, getOrgUnitHolder().getValue());
        }
        super.saveSettings();
    }

    @Override
    public void saveSettings()
    {
        clearCheckBoxes();
        saveSettingsInternal();
    }

    @Override
    public void clearSettings()
    {
        CommonBaseSettingsUtil.clearAndSaveSettingsExcept(getSettings(), EplStudentSummaryManager.PARAM_YEAR_PART, EplStudentSummaryManager.PARAM_GROUP_TYPE);
        if (getOrgUnitHolder().getId() != null) {
            getSettings().set(EducationCatalogsManager.PARAM_FORMATIVE_ORGUNIT, getOrgUnitHolder().getValue());
        }
        saveSettings();
    }

    public void onClickRecalculate()
    {
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(ProcessState state)
            {
                EplStudentSummaryDaemonBean.DAEMON.wakeUpAndWaitDaemon(24*60*60);
                EplStudentSummaryManager.instance().dao().doRecalculateEduGroups(getSummary());
                return new ProcessResult("Обновление планируемых потоков завершено.");
            }
        };
        new BackgroundProcessHolder().start("Обновление планируемых потоков", process, ProcessDisplayMode.unknown);
        deactivate();
    }

    public void onClickChangeState()
    {
        try {
            boolean allFormative = EplStateManager.instance().dao().isAllOuSummaryEqStatus(getSummary(), EppState.STATE_FORMATIVE, null);
            if (!allFormative && TIME_ITEM.equals(getSummary().getState().getCode()))
                throw new ApplicationException(getConfig().getProperty("ui.summary.accept.error"));

            EplStateManager.instance().dao().changeSummaryState(getSummary(), PLANNING_FLOW, TIME_ITEM);
            clearCheckBoxes();
        } finally {
            ContextLocal.getDesktop().setRefreshScheduled(true);
        }
    }

    // presenter

    public String getDaemonStatus()
    {
        final Long date = EplStudentSummaryDaemonBean.DAEMON.getCompleteStatus();
        if (null != date) {
            return DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date(date));
        }
        return "обновляется";
    }

    public boolean isShowSummaryTab() {
        Object yearPart = getSettings().get(EplStudentSummaryManager.PARAM_YEAR_PART);
        Object groupType = getSettings().get(EplStudentSummaryManager.PARAM_GROUP_TYPE);
        return getSummary() != null && yearPart != null && groupType != null;
    }

    public String getEditPK() {
        if (isSummaryPub()) return "eplStudentSummaryEditEduGroup";
        return new OrgUnitSecModel(getOrgUnitHolder().getValue()).getPermission("editEduGroupEplEduGroupTab");
    }

    public void onCheck()
    {

        final BaseSearchListDataSource dataSource = getConfig().<BaseSearchListDataSource>getDataSource(EplStudentSummaryEduGroupTab.DS_EPL_EDU_GROUP);
        Collection<IEntity> selectedItems;
        if (null != dataSource.getLegacyDataSource().getColumn(MERGE_CHECKBOX) && (selectedItems = dataSource.getOptionColumnSelectedObjects(MERGE_CHECKBOX)).size() > 0)
        {
            final int sum = selectedItems.stream().mapToInt(w -> {
                final Object property = w.getProperty(EplEduGroupDSHandler.COLUMN_STUDENT_COUNT);
                return property == null || property.toString().isEmpty() ? 0 : Integer.parseInt(property.toString());
            }).sum();
            ContextLocal.getInfoCollector().add(CommonBaseStringUtil.numberPostfixCase(sum, "Выбран ", "Выбрано ", "Выбрано ") +
                    CommonBaseStringUtil.numberWithPostfixCase(sum, "студент", "студента", "студентов" )+ " в " +
                    CommonBaseStringUtil.numberWithPostfixCase(selectedItems.size(), "потоке.", "потоках.", "потоках."));
            ContextLocal.getInfoCollector().setStyle("success");
        }
    }

    public boolean isCurrentEduGroupDisabled()
    {
        return _disabled;
    }


    // getters and setters

    public EplStudentSummary getSummary()
    {
        return getSummaryHolder().getValue();
    }

    public void setSummary(EplStudentSummary summary) {
        getSummaryHolder().setValue(summary);
    }

    public EplStateDao.EplStudentSummaryStateWrapper getStateWrapper()
    {
        return _stateWrapper;
    }

    public boolean isVisibleDaemon()
    {
        return _visibleDaemon;
    }

    public boolean isDisabled()
    {
        return _disabled;
    }

    public boolean isEditMode()
    {
        return !isDisabled();
    }

    public boolean isOrgUnitEmpty()
    {
        return null == orgUnitHolder.getId();
    }

    public boolean isChangeStateVisible()
    {
        return isOrgUnitEmpty() && null != _stateWrapper && _stateWrapper.isVisible();
    }

    public EntityHolder<EplStudentSummary> getSummaryHolder()
    {
        return summaryHolder;
    }

    public EntityHolder<OrgUnit> getOrgUnitHolder()
    {
        return orgUnitHolder;
    }

    public boolean isSummaryPub()
    {
        return summaryPub;
    }

    public void setSummaryPub(boolean summaryPub)
    {
        this.summaryPub = summaryPub;
    }
}
