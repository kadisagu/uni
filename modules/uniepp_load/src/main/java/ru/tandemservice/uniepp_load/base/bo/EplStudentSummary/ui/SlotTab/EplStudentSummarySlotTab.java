/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.SlotTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic.EplStudentSummarySplitElementDSHandler;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWP2GTypeSlot;
import ru.tandemservice.uniepp_load.catalog.entity.EplCompensationSource;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager.DS_YEAR_PART;
import static ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager.PARAM_SUMMARY;

/**
 * @author oleyba
 * @since 12/20/14
 */
@Configuration
public class EplStudentSummarySlotTab extends BusinessComponentManager
{
    public static final String DS_SUMMARY = "summaryDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(EplStudentSummaryManager.instance().groupTypeDSConfig())
            .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_YEAR_PART, getName(), YearDistributionPart.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) -> dql.where(exists(EplStudentWP2GTypeSlot.class,
                                                                             EplStudentWP2GTypeSlot.studentWPSlot().yearPart().id().s(), property(alias, "id"),
                                                                             EplStudentWP2GTypeSlot.studentWPSlot().student().group().summary().s(), context.<EplStudentSummary>get(PARAM_SUMMARY))
                ))).treeable(true).valueStyleSource(EducationCatalogsManager.getYearPartValueStyleSource()))
            .addDataSource(EplStudentSummaryManager.instance().orgUnitDSConfig())
            .addDataSource(selectDS("disciplineDS", EplStudentSummaryManager.instance().disciplineDSHandler()).addColumn(EppRegistryElementPart.P_TITLE_WITH_NUMBER))
            .addDataSource(selectDS("formOuDS").handler(UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler()).addColumn(OrgUnit.fullTitle().s()))
            .addDataSource(selectDS("eduHsDS", EducationCatalogsManager.instance().eduHsDSHandler()).addColumn(EducationLevelsHighSchool.fullTitleExtended().s()))
            .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
            .addDataSource(UniStudentManger.instance().courseDSConfig())
            .addDataSource(selectDS("groupDS", EplStudentSummaryManager.instance().groupComboDSHandler()))
            .addDataSource(selectDS("compSourceDS").handler(EplStudentSummaryManager.instance().compSourceDSHandler()).addColumn(EplCompensationSource.title().s()))
            .addDataSource(searchListDS("eplSlotDS", slotDSConfig(), slotDSHandler()))
            .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_SUMMARY, getName(), EplStudentSummary.defaultNotArchiveSelectDSHandler(getName())
                    .customize((alias, dql, context, filter) -> EplStudentSummaryManager.instance().dao().checkStudentSummaryByOrgUnit(alias, dql, context))))
            .addDataSource(CommonBaseStaticSelectDataSource.selectDS(EplStudentSummaryManager.SPLIT_VARIANT_DS, getName(), EppEduGroupSplitVariant.selectForEppRegistryElementDSHandler(getName())
                    .customize((alias, dql, context, filter) ->
                                       dql.where(exists(EplStudentWP2GTypeSlot.class,
                                                        EplStudentWP2GTypeSlot.splitElement().splitVariant().id().s(), property(alias, "id"),
                                                        EplStudentWP2GTypeSlot.studentWPSlot().student().group().summary().s(), context.<EplStudentSummary>get(PARAM_SUMMARY))
                                       ))))
            .addDataSource(selectDS(EplStudentSummaryManager.SPLIT_ELEMENT_DS, splitElementDSHandler()).addColumn(EppEduGroupSplitBaseElement.P_SPLIT_ELEMENT_TITLE))
            .create();
    }

    @Bean
    public ColumnListExtPoint slotDSConfig()
    {
        return columnListExtPointBuilder("eplSlotDS")
            .addColumn(textColumn("group", EplStudentWP2GTypeSlot.studentWPSlot().student().group().title()).order().create())
            .addColumn(textColumn("formOu", EplStudentWP2GTypeSlot.studentWPSlot().student().educationOrgUnit().formativeOrgUnit().shortTitle().s()).order().create())
            .addColumn(textColumn("terrOu", EplStudentWP2GTypeSlot.studentWPSlot().student().educationOrgUnit().territorialOrgUnit().shortTitle().s()).order().create())
            .addColumn(textColumn("eduOu", EplStudentWP2GTypeSlot.studentWPSlot().student().educationOrgUnit().educationLevelHighSchool().fullTitleExtended().s()).order().create())
            .addColumn(textColumn("compType", EplStudentWP2GTypeSlot.studentWPSlot().student().compensationSource().title().s()).order().create())
            .addColumn(textColumn("disc", EplStudentWP2GTypeSlot.studentWPSlot().activityElementPart().titleWithNumber().s()).order().create())
            .addColumn(textColumn("loadType", EplStudentWP2GTypeSlot.groupType().title().s()).order().create())
            .addColumn(textColumn("yearPart", EplStudentWP2GTypeSlot.studentWPSlot().yearPart().title().s()).order().create())
            .addColumn(textColumn("splitVariant", EplStudentWP2GTypeSlot.splitElement().splitVariant().shortTitle().s()))
            .addColumn(textColumn("splitElement", EplStudentWP2GTypeSlot.splitElement().splitElementShortTitle()))
            .addColumn(textColumn("slotCount", EplStudentWP2GTypeSlot.count().s()).order().create())
            .addColumn(textColumn("eduGroupCountSum", EplStudentSummarySlotDSHandler.VIEW_PROP_EDU_GROUP_COUNT_SUM).order().create())
            .addColumn(textColumn("groups", EplStudentSummarySlotDSHandler.VIEW_PROP_ROWS).order().create())
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> slotDSHandler()
    {
        return new EplStudentSummarySlotDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler splitElementDSHandler()
    {
        return new EplStudentSummarySplitElementDSHandler(getName())
        {
            @Override
            protected DQLSelectBuilder getBuilder(String alias, ExecutionContext context)
            {
                EplStudentSummary summary = context.get(PARAM_SUMMARY);

                DQLSelectBuilder builder = super.getBuilder(alias, context);
                builder.where(exists(EplStudentWP2GTypeSlot.class,
                                     EplStudentWP2GTypeSlot.splitElement().id().s(), property(alias, "id"),
                                     EplStudentWP2GTypeSlot.studentWPSlot().student().group().summary().s(), summary)
                );
                return builder;
            }
        };
    }
}