package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_load_2x8x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eplSettingsPpsLoad

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epl_settings_pps_load_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eplsettingsppsload"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("eduyear_id", DBType.LONG).setNullable(false), 
				new DBColumn("post_id", DBType.LONG), 
				new DBColumn("hoursmin_p", DBType.DOUBLE).setNullable(false), 
				new DBColumn("hoursmax_p", DBType.DOUBLE).setNullable(false), 
				new DBColumn("hoursadditional_p", DBType.DOUBLE).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eplSettingsPpsLoad");
		}
    }
}