/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.column.ITextColumnBuilder;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSettingsUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.catalog.bo.StudentCatalogs.StudentCatalogsManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp_load.base.bo.EplGroup.ui.EditTitles.EplGroupEditTitles;
import ru.tandemservice.uniepp_load.base.bo.EplState.EplStateManager;
import ru.tandemservice.uniepp_load.base.bo.EplState.logic.EplStateDao;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Edit.EplStudentSummaryEdit;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EditCount.EplStudentSummaryEditCount;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EditTitle.EplStudentSummaryEditTitle;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.logic.EplStudentFilter;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.logic.EplStudentSummaryGroupDSHandler;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.logic.IEplStudentSummaryGroupDAO;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.InfoBlock.EplStudentSummaryInfoBlockUI;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Refresh.EplStudentSummaryRefresh;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.TransferAffiliate.EplStudentSummaryTransferAffiliate;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplStudentSummaryState;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTabUI;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.caf.ui.config.BusinessComponentManagerBase.*;
import static ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.logic.EplStudentSummaryGroupDSHandler.PROP_COUNT;
import static ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.logic.EplStudentSummaryGroupDSHandler.PROP_COUNT_MESSAGE;
import static ru.tandemservice.uniepp_load.catalog.entity.codes.EplStudentSummaryStateCodes.ATTACH_WORK_PLAN;
import static ru.tandemservice.uniepp_load.catalog.entity.codes.EplStudentSummaryStateCodes.CONTINGENT_PLANNING;

/**
 * @author oleyba
 * @since 10/11/11
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "summaryHolder.id"),
    @Bind(key = EplOrgUnitTabUI.BIND_ORG_UNIT_ID, binding = "orgUnitHolder.id"),
    @Bind(key = EplStudentSummaryGroupTabUI.PARAM_MISS_EPL_STUDENTS, binding = "missEplStudents")
})
public class EplStudentSummaryGroupTabUI extends UIPresenter
{
	public static final String COLUMN_SELECT = "select";

    public static final String PARAM_MISS_EPL_STUDENTS = "missEplStudents";

	public static final String SETTINGS_FORMATIVE_ORGUNIT       = EducationCatalogsManager.PARAM_FORMATIVE_ORGUNIT;
	public static final String SETTINGS_EDU_HIGHSCHOOL_LEVEL    = EducationCatalogsManager.PARAM_EDU_HS;
	public static final String SETTINGS_DEVELOP_FORM            = EducationCatalogsManager.PARAM_DEVELOP_DORM;
	public static final String SETTINGS_COURSE                  = StudentCatalogsManager.PARAM_COURSE;
	public static final String SETTINGS_GROUP                   = "group";
	public static final String SETTINGS_COMPENSATION_SOURCE     = EplStudentSummaryManager.PARAM_COMPENSATION_SOURCE;

    private static final String SETTINGS_KEY_ITEMS = "eplStudentSummaryGroupTabItems";
    public static final String DS_EPL_STUDENT_SUMMARY_GROUP = "eplStudentSummaryGroupDS";

    private EntityHolder<EplStudentSummary> summaryHolder = new EntityHolder<>();
    private EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<>();
    private boolean summaryPub = true;
    private Integer _missEplStudents;

    private List<GroupingItemWrapper> prioritizedGroupingItemList = new ArrayList<>();
    private List<GroupingItemWrapper> actualGroupingItemList = new ArrayList<>();
    private GroupingItemWrapper currentItem;

    private PageableSearchListDataSource groupDS;
    private EplStateDao.EplStudentSummaryStateWrapper _stateWrapper;
    private boolean _disabled;

    // actions

    @Override
    public void onComponentRefresh()
    {
        getOrgUnitHolder().refresh();
        if (getOrgUnitHolder().getId() != null)
        {
            setSummaryPub(false);
            if (getOrgUnitHolder().getId().equals(getSummaryHolder().getId()))
            {
                getSummaryHolder().setId(null);
                getSummaryHolder().setValue(null);
            }
            getSettings().set(SETTINGS_FORMATIVE_ORGUNIT, getOrgUnitHolder().getValue());
            Long id = _uiSettings.<Long>get("summary." + getOrgUnitHolder().getId());
            final EplStudentSummary iEntity = id == null ? null : DataAccessServices.dao().get(id);
            if (id != null && null == iEntity)
            {
                _uiSettings.<Long>set("summary." + getOrgUnitHolder().getId(), null);
            }
            getSummaryHolder().setValue(iEntity);
            getSummaryHolder().refreshAllowNotExist();
            saveSettings();
        }
        getSummaryHolder().refresh();
	    prioritizedGroupingItemList.clear();
	    prioritizedGroupingItemList = getSettings().get(SETTINGS_KEY_ITEMS);
        final List<EplStudentSummaryGroupDSHandler.ColumnWrapper> columnList = EplStudentSummaryGroupDSHandler.getColumnList();
        if (prioritizedGroupingItemList == null || prioritizedGroupingItemList.size() != columnList.size())
            restoreDefaultGrouping(columnList);
        refreshGroupList();
        if (null != getSummary())
        {
            EplStudentSummaryInfoBlockUI.componentActivate(_uiSupport, _uiActivation, getSummary().getId());
            EplStudentSummaryState state = getSummary().getState();
            _stateWrapper = EplStateManager.instance().dao().getSummaryStateTitle(getSummary(), CONTINGENT_PLANNING, state.isContingentPlanning());
            _disabled = EplStateManager.instance().dao().isCurrentStateMore(state, CONTINGENT_PLANNING);
        }
    }

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		switch (dataSource.getName())
		{
			case DS_EPL_STUDENT_SUMMARY_GROUP:
				dataSource.put(EplStudentSummaryGroupDSHandler.PARAM_SELECTED_COLUMNS, getSelectedColumns());
				dataSource.put(EplStudentSummaryGroupDSHandler.PARAM_SUMMARY, getSummary());
				dataSource.put(EplStudentSummaryGroupDSHandler.PARAM_FILTERS, getFilters());
				break;
			case EplStudentSummaryGroupTab.DS_EDU_HS:
				dataSource.putAll(getSettings().getAsMap(EducationCatalogsManager.PARAM_FORMATIVE_ORGUNIT));
				break;
			case EplStudentSummaryGroupTab.DS_SUMMARY:
				dataSource.put(EplStudentSummaryManager.PARAM_FORMATIVE_ORG_UNIT, getOrgUnitHolder().getValue());
				break;
			case EplStudentSummaryGroupTab.DS_GROUP:
				dataSource.put(EplStudentSummaryGroupDSHandler.PARAM_SUMMARY, getSummary());
				dataSource.putAll(getSettings().getAsMap(SETTINGS_FORMATIVE_ORGUNIT, SETTINGS_EDU_HIGHSCHOOL_LEVEL, SETTINGS_DEVELOP_FORM, SETTINGS_COURSE));
				break;
		}
	}

    @Override
    public void saveSettings()
    {
        applySettings();
        refreshGroupList();
    }

    public void applySettings()
    {
        if (!isSummaryPub())
            getSettings().set("summary." + getOrgUnitHolder().getId(), getSummary() == null ? null : getSummary().getId());
        super.saveSettings();
    }

    @Override
    public void clearSettings()
    {
        applyClearSettings();
        refreshGroupList();
    }

    public void applyClearSettings()
    {
        CommonBaseSettingsUtil.clearAndSaveSettingsExcept(getSettings(), "summary." + getOrgUnitHolder().getId());
        if (getOrgUnitHolder().getId() != null)
            getSettings().set(EducationCatalogsManager.PARAM_FORMATIVE_ORGUNIT, getOrgUnitHolder().getValue());
    }

    public void saveGroupSettings()
    {
        _uiSettings.set(SETTINGS_KEY_ITEMS, prioritizedGroupingItemList);
        refreshGroupList();
    }

    public void clearGroupSettings()
    {
        restoreDefaultGrouping(EplStudentSummaryGroupDSHandler.getColumnList());
        saveGroupSettings();
    }

    public void onClickUp()
    {
	    String key = getListenerParameter();
        changeGroupingPriority(key, true);
	    saveSettings();
    }

    public void onClickDown()
    {
	    String key = getListenerParameter();
        changeGroupingPriority(key, false);
	    saveSettings();
    }

    public void onClickEdit()
    {
        _uiActivation.asRegion(EplStudentSummaryEditCount.class).top()
            .parameter(PUBLISHER_ID, getSummary().getId())
            .parameter(EplOrgUnitTabUI.BIND_ORG_UNIT_ID, getOrgUnitHolder().getId())
            .activate();
    }

    public void onClickEditContingent()
    {
        _uiActivation.asRegion(EplStudentSummaryEdit.class).top()
                .parameter(PUBLISHER_ID, getSummary().getId())
                .parameter(EplOrgUnitTabUI.BIND_ORG_UNIT_ID, getOrgUnitHolder().getId())
                .activate();
    }
    public void onClickEditTitle()
    {
        _uiActivation.asRegionDialog(EplStudentSummaryEditTitle.class).parameter(PUBLISHER_ID, getSummary().getId()).activate();
    }

    public void onClickRefresh()
    {
        ContextLocal.getDesktop().setRefreshScheduled(true);
        int currentEduYear = DataAccessServices.dao().getNotNull(EducationYear.class, EducationYear.current(), true).getIntValue();
        if (currentEduYear > getSummary().getEppYear().getEducationYear().getIntValue())
            throw new ApplicationException("Нельзя обновить сводку, т.к. текущий учебный год больше учебного года сводки.");

        _uiActivation.asRegionDialog(EplStudentSummaryRefresh.class).parameter(PUBLISHER_ID, getSummary().getId()).activate();
    }

    public void onGetZipLog()
    {
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().txt().fileName("Журнал обновления сводки.txt").document(getZipLog().getContent()), true);
    }

    public void onClickTransferAffiliate()
    {
        _uiActivation.asRegion(EplStudentSummaryTransferAffiliate.class).top()
                .parameter(PUBLISHER_ID, getSummary().getId())
                .activate();
    }

    public void onClickEditGroupTitles()
    {
        _uiActivation.asRegion(EplGroupEditTitles.class).top()
                .parameter(PUBLISHER_ID, getSummary().getId())
                .parameter(EplOrgUnitTabUI.BIND_ORG_UNIT_ID, getOrgUnitHolder().getId())
                .activate();
    }

	public void onClickDeleteSelectedEplStudents()
	{
		Collection<IEntity> selectedRows = getGroupDS().getOptionColumnSelectedObjects(COLUMN_SELECT);
		if (CollectionUtils.isEmpty(selectedRows))
			throw new ApplicationException(getConfig().getProperty("ui.deleteSelectedEplStudents.messageNotSelected"));

		EplStudentSummary summary = getSummary();
		EplStudentFilter filters = getFilters();
		IEplStudentSummaryGroupDAO dao = EplStudentSummaryManager.instance().groupDAO();
		selectedRows.forEach(row -> dao.deleteEplStudents(summary, filters, getProperties(row)));
	}

	/** Удалить план. студентов, соответствующих данной строке списка план. контингента. */
	public void onClickDeleteEplStudents()
	{
		List<DataWrapper> rows = getGroupDS().getRecords();
		DataWrapper rowToDelete = rows.stream().filter(w -> w.getId().equals(getListenerParameterAsLong())).findAny().get();
		EplStudentSummaryManager.instance().groupDAO().deleteEplStudents(getSummary(), getFilters(), getProperties(rowToDelete));
	}

	/** Получить значения в колонках для строки план. контингента (в формате "ключ-значение"). */
	private Collection<Pair<String, Object>> getProperties(IEntity eplStudent)
	{
		return getSelectedColumns().stream()
				.map(column -> new Pair<>(column.getPath() + "." + column.getTitleProperty(), eplStudent.getProperty(column.getKey())))
				.collect(Collectors.toList());
	}

    public void onClickChangeState()
    {
        try
        {
            EplStateManager.instance().dao().changeSummaryState(getSummary(), CONTINGENT_PLANNING, ATTACH_WORK_PLAN);
        }
        finally
        {
            ContextLocal.getDesktop().setRefreshScheduled(true);
        }
    }

    public void onChangeSummary()
    {
        if (null != getSummary())
            EplStudentSummaryInfoBlockUI.componentActivate(_uiSupport, _uiActivation, getSummary().getId());
        saveSettings();
        ContextLocal.getDesktop().setRefreshScheduled(true);
    }

	/** Получить объект фильтрации списка план.студентов, соответствующий выбранным в селектах значениям. */
	EplStudentFilter getFilters()
	{
		return new EplStudentFilter(getSettings());
	}

    // presenter

    public List<EplStudentSummaryGroupDSHandler.ColumnWrapper> getSelectedColumns()
    {
        List<EplStudentSummaryGroupDSHandler.ColumnWrapper> selected = new ArrayList<>();
        for (GroupingItemWrapper item : actualGroupingItemList)
            if (item.isSelected())
                selected.add(item.columnWrapper);
        return selected;
    }

    public EplStateDao.EplStudentSummaryStateWrapper getStateWrapper()
    {
        return _stateWrapper;
    }

    public boolean isDisabled()
    {
        return _disabled;
    }

    public boolean isEditMode()
    {
        return !isDisabled();
    }

    public boolean isChangeStateVisible()
    {
        return (null == orgUnitHolder.getId()) && (null != _stateWrapper) && _stateWrapper.isVisible();
    }

    public boolean isHasMissEplStudents()
    {
        return null != _missEplStudents && _missEplStudents > 0;
    }

    public boolean isShowSummaryTab()
    {
        return getSummary() != null;
    }

    public String getEditPK()
    {
        if (isSummaryPub()) return "eplStudentSummaryEditGroups";
        return new OrgUnitSecModel(getOrgUnitHolder().getValue()).getPermission("editGroupsEplGroupTab");
    }

    public String getEditContingentPK()
    {
        if (isSummaryPub()) return "eplStudentSummaryEditContingentGroups";
        return new OrgUnitSecModel(getOrgUnitHolder().getValue()).getPermission("editContingentGroupsEplGroupTab");
    }

    public String getTransferAffiliatePK()
    {
        if (isSummaryPub()) return "eplStudentSummaryTransferAffiliate";
        return new OrgUnitSecModel(getOrgUnitHolder().getValue()).getPermission("transferAffiliateEplGroupTab");
    }

    public String getEditGroupTitlesPK()
    {
        if (isSummaryPub()) return "eplStudentSummaryEditGroupTitles";
        return new OrgUnitSecModel(getOrgUnitHolder().getValue()).getPermission("editGroupTitlesEplGroupTab");
    }

	public String getDeleteEplStudentsPK()
	{
		if (isSummaryPub()) return "eplStudentSummaryDeleteEplStudents";
		return new OrgUnitSecModel(getOrgUnitHolder().getValue()).getPermission("deleteEplStudentsEplGroupTab");
	}

    public String getMissEplStudentsTitle()
    {
        return "Пропущено студентов с одинаковыми сетками: " + getMissEplStudents() + ". См.";
    }

    public boolean isActionsPermitted()
    {
        return checkKey(getEditContingentPK()) || checkKey(getEditPK()) || checkKey(getEditGroupTitlesPK()) || checkKey(getTransferAffiliatePK()) || checkKey(getDeleteEplStudentsPK());
    }

    public boolean isSummaryActionsPermitted()
    {
        return checkKey("eplStudentSummaryRefresh") || checkKey("eplStudentSummaryRefresh") || checkKey("eplStudentSummaryEditTitle") || checkKey("changeStateTo_attachWp_eplStudentSummary");
    }

    private boolean checkKey(String key)
    {
        return CoreServices.securityService().check(summaryHolder.getValue(), UserContext.getInstance().getPrincipalContext(), key);
    }
    // utils

    private void refreshGroupList()
    {
        actualGroupingItemList.clear();
        prioritizedGroupingItemList.forEach(i -> actualGroupingItemList.add(i.copy()));
        EplStudentSummaryGroupTab manager = _uiConfig.getManager();
        final IReadAggregateHandler dsHandler = manager.eplStudentSummaryGroupDSHandler();
	    groupDS = new PageableSearchListDataSource(this, DS_EPL_STUDENT_SUMMARY_GROUP);
        groupDS.setHandler(dsHandler);
	    createGroupDSColumns(groupDS, getSelectedColumns());
    }

	/**
	 * Сформировать колонки для датасурса списка и добавить их в датасурс. Добавляются следующие строки:
	 * <ol>
	 *     <li/> Колонка с чекбоксами (для массового выбора строк);
	 *     <li/> Колонки, выбранные пользователем для отображения и группировки;
	 *     <li/> Число план. студентов, соответствующих строке списка;
	 *     <li/> Удаление строки.
	 * <ol/>
	 * @param dataSource Датасурс списка.
	 * @param selectedColumns Колонки с данными план. студента, по которым нужна группировка.
	 */
	private static void createGroupDSColumns(PageableSearchListDataSource dataSource, List<EplStudentSummaryGroupDSHandler.ColumnWrapper> selectedColumns)
	{
		dataSource.addColumn(checkboxColumn(COLUMN_SELECT));
		selectedColumns.forEach(wrapper -> dataSource.addColumn(textColumn(wrapper.getKey(), wrapper.getKey())));
		ITextColumnBuilder countColumn = selectedColumns.isEmpty() ? textColumn(PROP_COUNT, PROP_COUNT) : textColumn(PROP_COUNT, PROP_COUNT).width("10px;");
		dataSource.addColumn(countColumn);
		dataSource.addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onClickDeleteEplStudents")
				.alert(BusinessComponentManager.alert("eplStudentSummaryGroupDS.delete.alert",  PROP_COUNT_MESSAGE ))
				.visible("ui:editMode")
				.permissionKey("ui:deleteEplStudentsPK")
				.width("1px;"));
	}

    private void restoreDefaultGrouping(List<EplStudentSummaryGroupDSHandler.ColumnWrapper> columnList)
    {
	    prioritizedGroupingItemList = new ArrayList<>();
        for (EplStudentSummaryGroupDSHandler.ColumnWrapper column : columnList)
        {
	        boolean selected = column.isRequired() || column.isEnabledByDefault();
	        prioritizedGroupingItemList.add(new GroupingItemWrapper(column, selected));
        }
    }

    private void changeGroupingPriority(String key, boolean up)
    {
        GroupingItemWrapper item = null;
        int index = -1;
        for (int i = 0; i < prioritizedGroupingItemList.size(); i++)
        {
            final GroupingItemWrapper current = prioritizedGroupingItemList.get(i);
	        if (current.getKey().equals(key))
            {
                item = current;
                index = i;
            }
        }
        if (null == item)
	        return;
	    int newIndex = up ? (index - 1) : (index + 1);
	    if (newIndex < 0 || newIndex >= prioritizedGroupingItemList.size())
		    return;
	    prioritizedGroupingItemList.remove(index);
	    prioritizedGroupingItemList.add(newIndex, item);
    }

    // getters and setters

    public EplStudentSummary getSummary()
    {
        return getSummaryHolder().getValue();
    }

    public void setSummary(EplStudentSummary summary)
    {
        getSummaryHolder().setValue(summary);
    }

    public EntityHolder<EplStudentSummary> getSummaryHolder()
    {
        return summaryHolder;
    }

    public DatabaseFile getZipLog()
    {
        return getSummary().getZipLog();
    }

    public List<GroupingItemWrapper> getPrioritizedGroupingItemList()
    {
        return prioritizedGroupingItemList;
    }

    public GroupingItemWrapper getCurrentItem()
    {
        return currentItem;
    }

    public void setCurrentItem(GroupingItemWrapper currentItem)
    {
        this.currentItem = currentItem;
    }

    public void setPrioritizedGroupingItemList(List<GroupingItemWrapper> prioritizedGroupingItemList)
    {
        this.prioritizedGroupingItemList = prioritizedGroupingItemList;
    }

    public PageableSearchListDataSource getGroupDS()
    {
        return groupDS;
    }

    public void setGroupDS(PageableSearchListDataSource groupDS)
    {
        this.groupDS = groupDS;
    }

    public EntityHolder<OrgUnit> getOrgUnitHolder()
    {
        return orgUnitHolder;
    }

    public boolean isSummaryPub()
    {
        return summaryPub;
    }

    public void setSummaryPub(boolean summaryPub)
    {
        this.summaryPub = summaryPub;
    }

    public Integer getMissEplStudents()
    {
        return _missEplStudents;
    }

    public void setMissEplStudents(Integer missEplStudents)
    {
        _missEplStudents = missEplStudents;
    }

    // inner classes

    public static class GroupingItemWrapper implements Serializable
    {
        private EplStudentSummaryGroupDSHandler.ColumnWrapper columnWrapper;
        private boolean selected;

        public GroupingItemWrapper(EplStudentSummaryGroupDSHandler.ColumnWrapper columnWrapper, boolean selected)
        {
            this.columnWrapper = columnWrapper;
            this.selected = selected;
        }

        public String getKey() { return columnWrapper.getKey(); }
        public String getTitle() { return columnWrapper.getTitle(); }
        public boolean isRequired() { return columnWrapper.isRequired(); }
        public boolean isSelected() { return selected; }
        public void setSelected(boolean selected)
        {
            this.selected = selected;
        }

        public GroupingItemWrapper copy()
        {
            return new GroupingItemWrapper(columnWrapper, selected);
        }
    }
}
