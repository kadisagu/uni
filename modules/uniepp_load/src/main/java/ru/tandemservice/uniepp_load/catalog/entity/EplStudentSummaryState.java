package ru.tandemservice.uniepp_load.catalog.entity;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.uniepp_load.catalog.entity.gen.EplStudentSummaryStateGen;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static ru.tandemservice.uniepp_load.catalog.entity.codes.EplStudentSummaryStateCodes.*;

/** @see ru.tandemservice.uniepp_load.catalog.entity.gen.EplStudentSummaryStateGen */
public class EplStudentSummaryState extends EplStudentSummaryStateGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{
    public boolean isContingentPlanning() { return CONTINGENT_PLANNING.equals(getCode()); }
    public boolean isAttachWp() { return ATTACH_WORK_PLAN.equals(getCode()); }
    public boolean isPlanningFlow() { return PLANNING_FLOW.equals(getCode()); }
    public boolean isTimeItem() { return TIME_ITEM.equals(getCode()); }
    public boolean isAccept() { return ACCEPT.equals(getCode()); }

    private static final Map<String, List<String>> POSSIBLE_TRANSITIONS_MAP = ImmutableMap.<String, List<String>>builder()
            .put(CONTINGENT_PLANNING, ImmutableList.of(ATTACH_WORK_PLAN))
            .put(ATTACH_WORK_PLAN, ImmutableList.of(CONTINGENT_PLANNING, PLANNING_FLOW))
            .put(PLANNING_FLOW, ImmutableList.of(ATTACH_WORK_PLAN, TIME_ITEM))
            .put(TIME_ITEM, ImmutableList.of(PLANNING_FLOW, ACCEPT))
            .put(ACCEPT, ImmutableList.of(TIME_ITEM))
            .build();

    /**
     * Проверка возможности перехода между состояниями
     */
    public static boolean canChangeState(String fromStateCode, String toStateCode)
    {
        return POSSIBLE_TRANSITIONS_MAP.get(fromStateCode).contains(toStateCode);
    }

    /**
     * @return коды состояний, в которые доступен переход из текущего
     */
    public List<String> getPossibleTransitions()
    {
        return POSSIBLE_TRANSITIONS_MAP.get(getCode());
    }

    private static List<String> HIDDEN_PROPERTIES = ImmutableList.of(P_PERMISSION_SUFFIX);

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public Collection<String> getHiddenFields() { return HIDDEN_PROPERTIES; }
        };
    }
}