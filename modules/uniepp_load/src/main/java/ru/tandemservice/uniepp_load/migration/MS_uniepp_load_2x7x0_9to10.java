package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_load_2x7x0_9to10 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (tool.tableExists("epl_time_rule_simple_t"))
        {
            if (tool.columnExists("epl_time_rule_simple_t", "basefieldname_p"))
                tool.renameColumn("epl_time_rule_simple_t", "basefieldname_p", "parametername_p");

            tool.createColumn("epl_time_rule_simple_t", new DBColumn("parameterneeded_p", DBType.BOOLEAN));
            tool.executeUpdate("update epl_time_rule_simple_t set parameterneeded_p=? where parameterneeded_p is null", Boolean.FALSE);
            tool.setColumnNullable("epl_time_rule_simple_t", "parameterneeded_p", false);
        }
    }
}