package ru.tandemservice.uniepp_load.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Конфигурация для скриптовой печати модуля «Нагрузка»"
 * Имя сущности : eplPrintScript
 * Файл data.xml : uniepp_load.catalog.data.xml
 */
public interface EplPrintScriptCodes
{
    /** Константа кода (code) элемента : Отчет «Учебная нагрузка читающего подразделения (форма A)» (title) */
    String ORG_UNIT_SUMMARY_A = "orgUnitSummary";
    /** Константа кода (code) элемента : Отчет «Учебная нагрузка читающего подразделения (форма B)» (title) */
    String ORG_UNIT_SUMMARY_B = "orgUnitEduLoadB";
    /** Константа кода (code) элемента : Отчет «Распределение почасового фонда» (title) */
    String HOURLY_FUND = "hourlyFund";
    /** Константа кода (code) элемента : Отчет «Расчет штата на основании учебной нагрузки» (title) */
    String STAFF_BY_LOAD_B = "staffByLoadB";
    /** Константа кода (code) элемента : Отчет «Проверка распределения учебной нагрузки по ППС читающего подразделения» (title) */
    String ORG_UNIT_LOAD_BY_PPS_CHECK = "orgUnitEduLoadByPpsCheck";
    /** Константа кода (code) элемента : Отчет «Учебное поручение читающему подразделению» (title) */
    String ORG_UNIT_EDU_ASSIGNMENT = "orgUnitEduAssignment";
    /** Константа кода (code) элемента : Отчет «Индивидуальный план преподавателя» (title) */
    String IND_PLAN_PPS = "indPlanPps";

    Set<String> CODES = ImmutableSet.of(ORG_UNIT_SUMMARY_A, ORG_UNIT_SUMMARY_B, HOURLY_FUND, STAFF_BY_LOAD_B, ORG_UNIT_LOAD_BY_PPS_CHECK, ORG_UNIT_EDU_ASSIGNMENT, IND_PLAN_PPS);
}
