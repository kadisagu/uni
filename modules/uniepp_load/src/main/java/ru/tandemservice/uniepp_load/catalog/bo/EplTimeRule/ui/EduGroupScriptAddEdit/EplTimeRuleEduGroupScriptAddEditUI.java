/* $Id:$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.ui.EduGroupScriptAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemAddEditUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.uni.dao.UniScriptDao;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript;

/**
 * @author oleyba
 * @since 11/29/14
 */
@Input({
    @Bind(key = "catalogItemId", binding = "holder.id"),
    @Bind(key = DefaultCatalogAddEditModel.CATALOG_CODE, binding = "catalogCode")
})
public class EplTimeRuleEduGroupScriptAddEditUI extends BaseCatalogItemAddEditUI<EplTimeRuleEduGroupScript>
{
    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        getProperties().remove(EplTimeRuleEduGroupScript.P_DESCRIPTION);
    }

    @Override
    public void onClickApply()
    {
        if (isAddForm())
        {
            getCatalogItem().setDefaultScriptPath(EplTimeRuleEduGroupScript.DEFAULT_SCRIPT_PATH);
            getCatalogItem().setUserScript(UniScriptDao.getScriptText(UniScriptDao.findScriptPath(EplTimeRuleEduGroupScript.DEFAULT_SCRIPT_PATH)));
        }
        super.onClickApply();
    }

    @Override
    public String getAdditionalPropertiesPageName()
    {
        return getClass().getPackage().getName() + ".AdditProps";
    }
}