/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 21.03.2016
 */
public class EplStudentSummarySplitElementDSHandler extends SimpleTitledComboDataSourceHandler
{
    public EplStudentSummarySplitElementDSHandler(String ownerId)
    {
        super(ownerId);
    }

    protected DQLSelectBuilder getBuilder(String alias, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEduGroupSplitBaseElement.class, "s");

        EppEduGroupSplitVariant splitVariant = context.get(EplStudentSummaryManager.PARAM_SPLIT_VARIANT);
        if (null == splitVariant)
            builder.where(nothing());
        else
            builder.where(eq(property("s", EppEduGroupSplitBaseElement.splitVariant()), value(splitVariant)));
        return builder;
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        String alias = "s";
        DQLSelectBuilder builder = getBuilder(alias, context);

        Set keys = input.getPrimaryKeys();
        if (CollectionUtils.isNotEmpty(keys))
            builder.where(in(property(alias, "id"), keys));

        String filter = input.getComboFilterByValue();
        List<EppEduGroupSplitBaseElement> resultList = builder.createStatement(context.getSession()).<EppEduGroupSplitBaseElement>list().stream()
                .filter(entity -> StringUtils.isEmpty(filter) || entity.getSplitElementTitle().toLowerCase().contains(filter.toLowerCase()))
                .sorted((o1, o2) -> o1.getSplitElementShortTitle().compareTo(o2.getSplitElementShortTitle()))
                .collect(Collectors.toList());

        return ListOutputBuilder.get(input, resultList).pageable(false).build();
    }
}
