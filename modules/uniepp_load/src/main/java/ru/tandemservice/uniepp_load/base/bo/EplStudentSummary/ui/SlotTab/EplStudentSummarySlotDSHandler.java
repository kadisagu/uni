/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.SlotTab;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.support.IUISettings;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.catalog.bo.StudentCatalogs.StudentCatalogsManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWP2GTypeSlot;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.sum;

/**
 * @author oleyba
 * @since 12/20/14
 */
public class EplStudentSummarySlotDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PARAM_STUDENT_SUMMARY = "summary";
    public static final String PARAM_HAS_ERRORS = "hasErrors";
    public static final String VIEW_PROP_ROWS = "rows";
    public static final String VIEW_PROP_EDU_GROUP_COUNT_SUM = "eduGroupCountSum";

    private final DQLOrderDescriptionRegistry registry = buildOrderRegistry();

    public EplStudentSummarySlotDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EplStudentSummary summary = context.get(EplStudentSummaryManager.PARAM_SUMMARY);
        if (null == summary) return new DSOutput(input);

        DQLSelectBuilder dql = registry.buildDQLSelectBuilder().column("s");
        dql.joinPath(DQLJoinType.inner, EplStudentWP2GTypeSlot.studentWPSlot().student().fromAlias("s"), "st");
        dql.where(eq(property("st", EplStudent.group().summary()), value(summary)));

        IUISettings settings = context.get(EplStudentSummaryManager.PARAM_SETTINGS);

        CommonBaseFilterUtil.applySelectFilter(dql, "s", EplStudentWP2GTypeSlot.studentWPSlot().activityElementPart(), settings.get(EplStudentSummaryManager.PARAM_DISCIPLINE));
        CommonBaseFilterUtil.applySelectFilter(dql, "s", EplStudentWP2GTypeSlot.studentWPSlot().activityElementPart().registryElement().owner(), settings.get("readingOrgUnit"));
        CommonBaseFilterUtil.applySelectFilter(dql, "s", EplStudentWP2GTypeSlot.studentWPSlot().yearPart(), context.get(EplStudentSummaryManager.PARAM_YEAR_PART));
        CommonBaseFilterUtil.applySelectFilter(dql, "s", EplStudentWP2GTypeSlot.groupType(), context.get(EplStudentSummaryManager.PARAM_GROUP_TYPE));
        CommonBaseFilterUtil.applySelectFilter(dql, "st", EplStudent.educationOrgUnit().formativeOrgUnit(), settings.get(EducationCatalogsManager.PARAM_FORMATIVE_ORGUNIT));
        CommonBaseFilterUtil.applySelectFilter(dql, "st", EplStudent.educationOrgUnit().educationLevelHighSchool(), settings.get(EducationCatalogsManager.PARAM_EDU_HS));
        CommonBaseFilterUtil.applySelectFilter(dql, "st", EplStudent.educationOrgUnit().developForm(), settings.get(EducationCatalogsManager.PARAM_DEVELOP_DORM));
        CommonBaseFilterUtil.applySelectFilter(dql, "st", EplStudent.group().course(), settings.get(StudentCatalogsManager.PARAM_COURSE));
        CommonBaseFilterUtil.applySelectFilter(dql, "st", EplStudent.compensationSource(), settings.get(EplStudentSummaryManager.PARAM_COMPENSATION_SOURCE));
        CommonBaseFilterUtil.applySelectFilter(dql, "s", EplStudentWP2GTypeSlot.studentWPSlot().student().group().summary(), context.get(PARAM_STUDENT_SUMMARY));
        CommonBaseFilterUtil.applySelectFilter(dql, "s", EplStudentWP2GTypeSlot.splitElement().splitVariant(), context.get(EplStudentSummaryManager.PARAM_SPLIT_VARIANT));
        CommonBaseFilterUtil.applySelectFilter(dql, "s", EplStudentWP2GTypeSlot.splitElement(), context.get(EplStudentSummaryManager.PARAM_SPLIT_ELEMENT));

        if (Boolean.TRUE.equals(settings.get(PARAM_HAS_ERRORS)))
        {
            DQLSelectBuilder errorBuilder = EplStudentSummaryManager.instance().dao().getEplStudentWP2GTypeSlotErrorWithoutSameSummaryCheckBuilder("slot", summary);
            dql.joinDataSource("s", DQLJoinType.inner, errorBuilder.buildQuery(), "slot", eq(property("s.id"), property("slot.id")));
        }

        DQLSelectBuilder slot2CountDql = new DQLSelectBuilder()
                .fromEntity(EplStudentWP2GTypeSlot.class, "s")
                .fromEntity(EplEduGroupRow.class, "row")
                .where(eq(property("s"), property("row", EplEduGroupRow.studentWP2GTypeSlot())))
                .where(eq(property("s", EplStudentWP2GTypeSlot.studentWPSlot().student().group().summary()), value(summary)))
                .group(property(EplStudentWP2GTypeSlot.id().fromAlias("s")))
                .column(property(EplStudentWP2GTypeSlot.id().fromAlias("s")), "s_id")
                .column(sum(property(EplEduGroupRow.count().fromAlias("row"))), "r_sum");

        List<Object[]> list = slot2CountDql.createStatement(context.getSession()).list();
        Map<Long, Long> slot2CountMap = list.stream().collect(Collectors.toMap(item -> (Long) item[0], item -> (Long) item[1]));

        String groupTitle = settings.get("group") != null ? ((DataWrapper) settings.get("group")).getTitle() : null;
        if(!StringUtils.isEmpty(groupTitle))
        {
            dql.where(eq(property("s", EplStudentWP2GTypeSlot.studentWPSlot().student().group().title()), value(groupTitle)));
        }

        registry.applyOrder(dql, input.getEntityOrder());
        DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).build();

        final List<DataWrapper> wrappers = DataWrapper.wrap(output);
        BatchUtils.execute(wrappers, 256, new BatchUtils.Action<DataWrapper>()
        {
            @Override
            public void execute(final Collection<DataWrapper> wrappers)
            {
                Collection<Long> ids = CommonDAO.ids(wrappers);

                final List<EplEduGroupRow> rows = new DQLSelectBuilder()
                    .fromEntity(EplEduGroupRow.class, "rel").column("rel")
                    .where(in(property(EplEduGroupRow.studentWP2GTypeSlot().id().fromAlias("rel")), ids))
                    .createStatement(context.getSession()).list();
                final Map<Long, List<EplEduGroupRow>> relMap = SafeMap.get(ArrayList.class);
                for (final EplEduGroupRow rel : rows)
                    relMap.get(rel.getStudentWP2GTypeSlot().getId()).add(rel);

                for (final DataWrapper wrapper : wrappers) {
                    List<EplEduGroupRow> wpList = relMap.get(wrapper.getId());
                    Collections.sort(wpList, ITitled.TITLED_COMPARATOR);

                    StrBuilder b = new StrBuilder();
                    for (EplEduGroupRow rel : wpList) {
                        b.appendSeparator("\n");
                        b.append(rel.getCount()).append(" — ").append(rel.getGroup().getTitle()).append(", ").append(rel.getGroup().getRegistryElementPart().getNumberWithAbbreviationWithPart());
                    }

                    wrapper.put(VIEW_PROP_ROWS, b.toString());
                    wrapper.put(VIEW_PROP_EDU_GROUP_COUNT_SUM, slot2CountMap.get(wrapper.getId()));
                }
            }
        });

        return output;
    }

    protected DQLOrderDescriptionRegistry buildOrderRegistry() {
        return new DQLOrderDescriptionRegistry(EplStudentWP2GTypeSlot.class, "s");
    }
}