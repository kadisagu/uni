package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_load_2x10x4_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.4")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eplReportOrgUnitEduAssignment

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epl_rep_ou_edu_assignment_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_b92fd6d0"),
				new DBColumn("eduyear_id", DBType.LONG).setNullable(false),
				new DBColumn("studentsummary_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("orgunit_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("yearpart_p", DBType.createVarchar(255)),
				new DBColumn("developform_p", DBType.createVarchar(255)),
				new DBColumn("developcondition_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eplReportOrgUnitEduAssignment");
		}
    }
}