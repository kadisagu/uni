/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.daemon;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.debug.SectionDebugItem;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.base.bo.DaemonQueue.DaemonQueueManager;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.EplOuSummaryManager;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupFormula;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleSimple;

import java.util.Iterator;

/**
 * @author Alexey Lopatin
 * @since 28.01.2016
 */
public class EplTimeItemDaemonBean extends UniBaseDao implements IEplTimeItemDaemonBean
{
    public static final String GLOBAL_DAEMON_LOCK = EplTimeItemDaemonBean.class.getSimpleName() + ".lock";

    public static final String DAEMON_NAME = "ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.daemon.EplTimeItemDaemonBean";
    public static final SyncDaemon DAEMON = new SyncDaemon(EplTimeItemDaemonBean.class.getName(), 1440, GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            Debug.begin("checkCorrectEplTimeItems");
            final IEventServiceLock eventLock = CoreServices.eventService().lock();

            try
            {
                DaemonQueueManager queueManager = DaemonQueueManager.instance();
                DaemonQueueManager.DaemonQueueItemIterator<EplEduGroup> groupIterator =
                        queueManager.getQueueIterator(DAEMON_NAME, EplEduGroup.class);
                DaemonQueueManager.DaemonQueueItemIterator<EplTimeRuleSimple> simpleRuleIterator =
                        queueManager.getQueueIterator(DAEMON_NAME, EplTimeRuleSimple.class);
                DaemonQueueManager.DaemonQueueItemIterator<EplTimeRuleEduGroupFormula> formulaRuleIterator =
                        queueManager.getQueueIterator(DAEMON_NAME, EplTimeRuleEduGroupFormula.class);
                DaemonQueueManager.DaemonQueueItemIterator<EplTimeRuleEduGroupScript> scriptRuleIterator =
                        queueManager.getQueueIterator(DAEMON_NAME, EplTimeRuleEduGroupScript.class);
                DaemonQueueManager.DaemonQueueItemIterator<EplOrgUnitSummary> summaryIterator =
                        queueManager.getQueueIterator(DAEMON_NAME, EplOrgUnitSummary.class);


                boolean success = ((groupIterator.isEmpty() && summaryIterator.isEmpty()) || !formulaRuleIterator.isEmpty() || !scriptRuleIterator.isEmpty()) ?
                    //EplRecalculateSummaryKind.CHECK
                    EplOuSummaryManager.instance().dao().doRefreshFull() :
                    //EplRecalculateSummaryKind.PARTIAL_CHECK;
                    EplOuSummaryManager.instance().dao().refreshPartial(groupIterator, summaryIterator, simpleRuleIterator);

                if (success)
                {
                    if (!groupIterator.isEmpty())
                        queueManager.endProcessQueue(DAEMON_NAME, EplEduGroup.class);
                    if (!simpleRuleIterator.isEmpty())
                        queueManager.endProcessQueue(DAEMON_NAME, EplTimeRuleSimple.class);
                    if (!formulaRuleIterator.isEmpty())
                        queueManager.endProcessQueue(DAEMON_NAME, EplTimeRuleEduGroupFormula.class);
                    if (!scriptRuleIterator.isEmpty())
                        queueManager.endProcessQueue(DAEMON_NAME, EplTimeRuleEduGroupScript.class);
                    if (!summaryIterator.isEmpty())
                        queueManager.endProcessQueue(DAEMON_NAME, EplOrgUnitSummary.class);
                }
            }
            finally
            {
                final SectionDebugItem end = Debug.end();

                // включаем системное логирование
                eventLock.release();
            }
        }
    };
}
