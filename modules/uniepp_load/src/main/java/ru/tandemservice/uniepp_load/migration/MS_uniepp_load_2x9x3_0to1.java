package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.util.List;

import static org.tandemframework.shared.commonbase.utils.MigrationUtils.BatchUpdater;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_load_2x9x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (!tool.tableExists("epl_student_wp_slot_t") || !tool.tableExists("epl_student_summary_t")) {
            return;
        }

        ////////////////////////////////////////////////////////////////////////////////
		// сущность eplStudentSummary

		// создано свойство zipLog
        tool.createColumn("epl_student_summary_t", new DBColumn("ziplog_id", DBType.LONG));

		////////////////////////////////////////////////////////////////////////////////
		// сущность eplStudentWP2GTypeSlot

		// свойство countByEduGroup переименовано в count
		{
			tool.renameColumn("epl_student_wp2gt_slot_t", "countbyedugroup_p", "count_p");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eplStudentWPSlot

        // создано обязательное свойство term
        {
            // создать колонки
            tool.createColumn("epl_student_wp_slot_t", new DBColumn("term_id", DBType.LONG));
            tool.createColumn("epl_student_wp_slot_t", new DBColumn("cachedcourse_id", DBType.LONG));

            // задать значения по умолчанию
            final List<Object[]> slots = tool.executeQuery(MigrationUtils.processor(3), "select slot.id, dgt.term_id, dgt.course_id from epl_student_wp_slot_t slot inner join developgridterm_t dgt on slot.cachedgridterm_id = dgt.id");
            final BatchUpdater updater = new BatchUpdater("update epl_student_wp_slot_t set term_id = ?, cachedcourse_id = ? where id = ?", DBType.LONG, DBType.LONG, DBType.LONG);

            for (Object[] row : slots)
            {
                Long slotId = (Long) row[0];
                Long termId = (Long) row[1];
                Long courseId = (Long) row[2];

                updater.addBatch(termId, courseId, slotId);
            }
            updater.executeUpdate(tool);

            // сделать колонки NOT NULL
            tool.setColumnNullable("epl_student_wp_slot_t", "term_id", false);
            tool.setColumnNullable("epl_student_wp_slot_t", "cachedcourse_id", false);
        }

		// удалено свойство cachedGridTerm
		{
			// удалить колонку
			tool.dropColumn("epl_student_wp_slot_t", "cachedgridterm_id");
		}
    }
}