/* $Id:$ */
package ru.tandemservice.uniepp_load.report.bo.EplReportBase.logic;

import org.tandemframework.caf.ui.config.BusinessComponentManager;

import java.util.List;

/**
 * @author oleyba
 * @since 5/14/14
 */
public interface IEplStorableReportDesc
{
    String getReportKey();
    Class<? extends IEplReport> getReportClass();

    List<String> getPubPropertyList();
    List<String> getListPropertyList();

    Class<? extends BusinessComponentManager> getAddFormComponent();
}
