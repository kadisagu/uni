/* $Id:$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeCategory.ui.EduLoadAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 11/28/14
 */
@Configuration
public class EplTimeCategoryEduLoadAddEdit extends BusinessComponentManager
{
    public static final String PARENT_DS = "parentDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(PARENT_DS, parentDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler parentDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EplTimeCategoryEduLoad.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(ne(property(alias, EplTimeCategoryEduLoad.id()), value(context.<Long>get(EplTimeCategoryEduLoadAddEditUI.PARAM_CATEGORY_ITEM_ID))));
                dql.where(isNull(property(alias, EplTimeCategoryEduLoad.parent())));
                dql.where(notExists(EplTimeRuleEduLoad.class, EplTimeRuleEduLoad.category().s(), property(alias)));
            }
        }
                .order(EplTimeCategoryEduLoad.title())
                .filter(EplTimeCategoryEduLoad.title());
    }
}