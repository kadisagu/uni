package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_load_2x9x3_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eplStudentCount4SplitElement

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epl_student_count_4se_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_8eb783a1"),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("student2workplan_id", DBType.LONG).setNullable(false),
				new DBColumn("regelement_id", DBType.LONG).setNullable(false),
				new DBColumn("splitelement_id", DBType.LONG).setNullable(false),
				new DBColumn("studentcount_p", DBType.INTEGER).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eplStudentCount4SplitElement");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eplStudentWP2GTypeSlot

		// создано свойство splitElement
		{
			// создать колонку
			tool.createColumn("epl_student_wp2gt_slot_t", new DBColumn("splitelement_id", DBType.LONG));
		}
    }
}