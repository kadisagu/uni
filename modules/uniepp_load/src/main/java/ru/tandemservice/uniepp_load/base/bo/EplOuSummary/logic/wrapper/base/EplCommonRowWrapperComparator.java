/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base;

import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCommonRowWrapper;

import java.util.Comparator;

/**
 * Сортировка строк:
 * 1) по части элемента реестра по выводимому названию, при этом null выводится перед значимыми строками
 * 2) по норме времени, при этом null выводится перед значимыми строками
 *
 * @author Alexey Lopatin
 * @since 06.04.2016
 */
public class EplCommonRowWrapperComparator<T extends EplCommonRowWrapper> implements Comparator<T>
{
    public static final EplCommonRowWrapperComparator<EplCommonRowWrapper> INSTANCE = new EplCommonRowWrapperComparator<>();

    public EplCommonRowWrapperComparator() {}

    @Override
    public int compare(T w1, T w2)
    {
        int result;
        EplCommonRowWrapper.DisciplineOrRulePairKey k1 = w1.getKey();
        EplCommonRowWrapper.DisciplineOrRulePairKey k2 = w2.getKey();

        result = Integer.compare(k1.getGroupPriority(), k2.getGroupPriority());
        if (result == 0)
        {
            Long d1 = k1.getDisciplineId();
            Long d2 = k2.getDisciplineId();

            if (null == d1 || null == d2)
                result = d1 == null ? (d2 == null ? 0 : 1) : -1;
            else
                result = k1.getDisciplineTitle().compareTo(k2.getDisciplineTitle());
        }
        if (result == 0)
        {
            Long r1 = k1.getRuleId();
            Long r2 = k2.getRuleId();

            if (null == r1 || null == r2)
                result = r1 == null ? (r2 == null ? 0 : -1) : 1;
            else
                result = k1.getRuleTitle().compareTo(k2.getRuleTitle());
        }
        return result;
    }
}
