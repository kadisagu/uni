package ru.tandemservice.uniepp_load.base.entity.studentWPSlot;

import org.tandemframework.core.common.IEntityDebugTitled;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.gen.EplStudentWPSlotGen;

/**
 * МСРП (план.)
 */
public class EplStudentWPSlot extends EplStudentWPSlotGen implements IEntityDebugTitled
{
    @Override
    public String getEntityDebugTitle()
    {
        final EplStudent student = getStudent();
        return UniStringUtils.joinWithSeparator(", ",
            student.getGroup().getTitle(),
            student.getGroup().getCourse().getTitle() + " курс",
            student.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableShortTitle(),
            student.getEducationOrgUnit().getDevelopOrgUnitShortTitle(),
            student.getEducationOrgUnit().getDevelopCombinationTitle(),
            student.getCompensationSource().getTitle(),
            getActivityElementPart().getNumberWithAbbreviationWithPart()
        );
    }
}