/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EditWpRel;

import com.google.common.base.Preconditions;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.common.INaturalId;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 17.11.2014
 */
@Input(
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id")
)
public class EplStudentSummaryEditWpRelUI extends UIPresenter
{
    private EntityHolder<EplStudent> holder = new EntityHolder<>();
    private Map<DevelopGridTerm, Map<Long, EplStudent2WorkPlan>> _rowTable;
    private Map<Term, DevelopGridTerm> _termMap;
    private DevelopGridTerm _currentTerm;
    private EplStudent2WorkPlan _currentRow;
    private boolean _hasWpFromOtherTerms;

    private static final String WP_FIELD_PREFIX = "wp_";
    private static final String COUNT_FIELD_PREFIX = "cnt_";

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();

        List<DevelopGridTerm> termList = IUniBaseDao.instance.get().getList(
                new DQLSelectBuilder().fromEntity(DevelopGridTerm.class, "g").column("g")
                        .where(eq(property("g", DevelopGridTerm.developGrid()), value(getStudent().getDevelopGrid())))
                        .where(eq(property("g", DevelopGridTerm.course()), value(getStudent().getGroup().getCourse())))
                        .order(property("g", DevelopGridTerm.term().intValue()))
        );
        _termMap = new LinkedHashMap<>();
        for (DevelopGridTerm developGridTerm : termList) {
            _termMap.put(developGridTerm.getTerm(), developGridTerm);
        }

        List<EplStudent2WorkPlan> list = IUniBaseDao.instance.get().getList(EplStudent2WorkPlan.class, EplStudent2WorkPlan.L_STUDENT, getHolder().getId());
        Collections.sort(list, EplStudent2WorkPlan.IN_ROW_COMPARATOR);
        _rowTable = SafeMap.get(LinkedHashMap.class);
        for (EplStudent2WorkPlan item : list) {
            DevelopGridTerm term = _termMap.get(item.getWorkPlan().getTerm());
            if (term != null)
                addRow(term, item.getWorkPlan(), item.getCount());
            else
                _hasWpFromOtherTerms = true;
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EplStudentSummaryEditWpRel.PARAM_STUDENT, getStudent());
        dataSource.put(EplStudentSummaryEditWpRel.PARAM_TERM, getCurrentTerm().getTerm());
        dataSource.put(EplStudentSummaryEditWpRel.WORK_PLAN, getCurrentRow().getWorkPlan());
    }

    // actions

    public void onClickAddRow()
    {
        DevelopGridTerm developGridTerm = (DevelopGridTerm) getSupport().getSession().load(DevelopGridTerm.class, getListenerParameterAsLong());
        addRow(developGridTerm, null, Math.max(0, getStudent().getCount() - calcSum(developGridTerm)));
    }

    public void onClickDeleteRow()
    {
        for (Map<Long, EplStudent2WorkPlan> termRow : _rowTable.values())
        {
            if (termRow.remove(getListenerParameterAsLong()) != null)
                return;
        }
        throw new IllegalStateException(); // Почему ничего не удалилось?
    }

    public void onClickApply()
    {
        if (!validate())
            return;

        IUniBaseDao.instance.get().doInTransaction(session -> {
            List<EplStudent2WorkPlan> list = IUniBaseDao.instance.get().getList(EplStudent2WorkPlan.class, EplStudent2WorkPlan.L_STUDENT, getHolder().getId());
            for (EplStudent2WorkPlan wp : getRowList()) {
                wp.setId(null);
            }

            new MergeAction.SessionMergeAction<INaturalId, EplStudent2WorkPlan>()
            {
                @Override protected INaturalId key(EplStudent2WorkPlan source) { return source.getNaturalId(); }

                @Override protected EplStudent2WorkPlan buildRow(EplStudent2WorkPlan source) { return source; }

                @Override protected void fill(EplStudent2WorkPlan target, EplStudent2WorkPlan source) {
                    target.update(source, false);
                    target.setCachedGridTerm(target.getWorkPlan().getGridTerm());
                }
            }.merge(list, getRowList());

            return null;
        });

        deactivate();
    }

    // utils

    private void addRow(DevelopGridTerm term, EppWorkPlanBase wp, int studentCount)
    {
        Preconditions.checkArgument(wp == null || wp.getTerm().equals(term.getTerm()));

        EplStudent2WorkPlan row = new EplStudent2WorkPlan();
        row.setStudent(getStudent());
        row.setWorkPlan(wp);
        row.setCount(studentCount);
        row.setId((long) System.identityHashCode(row));
        _rowTable.get(term).put(row.getId(), row);
    }

    private Collection<EplStudent2WorkPlan> getRows(DevelopGridTerm term)
    {
        return _rowTable.get(term).values();
    }


    private int calcSum(DevelopGridTerm term)
    {
        int sum = 0;
        for (EplStudent2WorkPlan row : getRows(term)) {
            sum += row.getCount();
        }
        return sum;
    }

    private boolean validate()
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();
        Set<EppWorkPlanBase> workPlans = new HashSet<>();

        for (EplStudent2WorkPlan row : getRowList())
        {
            if (!workPlans.add(row.getWorkPlan()))
                errorCollector.add("Рабочий план должен быть уникальным в рамках строки сводки.", WP_FIELD_PREFIX + row.getId());
        }

        for (DevelopGridTerm term : getTermList())
        {
            if (calcSum(term) != getStudent().getCount())
            {
                Collection<EplStudent2WorkPlan> rows = getRows(term);
                if (rows.isEmpty()) continue;

                String[] ids = new String[rows.size()];
                int i = 0;
                for (EplStudent2WorkPlan row : rows) {
                    ids[i++] = COUNT_FIELD_PREFIX + row.getId();
                }
                errorCollector.add("Сумма по рабочим планам за " + term.getPart().getTitle() + " должна быть равна количеству студентов строки сводки.", ids);
            }
        }

        return !errorCollector.hasErrors();
    }

    // getters and setters


    public boolean isHasWpFromOtherTerms()
    {
        return _hasWpFromOtherTerms;
    }

    public EplStudent getStudent()
    {
        return getHolder().getValue();
    }

    public EntityHolder<EplStudent> getHolder()
    {
        return holder;
    }

    public Collection<DevelopGridTerm> getTermList()
    {
        return _termMap.values();
    }

    public DevelopGridTerm getCurrentTerm()
    {
        return _currentTerm;
    }

    public void setCurrentTerm(DevelopGridTerm currentTerm)
    {
        _currentTerm = currentTerm;
    }

    public Collection<EplStudent2WorkPlan> getRowList()
    {
        Set<EplStudent2WorkPlan> set = new HashSet<>();
        for (Map<Long, EplStudent2WorkPlan> term : _rowTable.values()) {
            set.addAll(term.values());
        }
        return set;
    }

    public Collection<EplStudent2WorkPlan> getCurrentRowList()
    {
        return _rowTable.get(getCurrentTerm()).values();
    }

    public EplStudent2WorkPlan getCurrentRow()
    {
        return _currentRow;
    }

    public void setCurrentRow(EplStudent2WorkPlan currentRow)
    {
        _currentRow = currentRow;
    }

    public String getCurrentRowCountFieldId()
    {
        return COUNT_FIELD_PREFIX + getCurrentRow().getId();
    }

    public String getCurrentRowWpFieldId()
    {
        return WP_FIELD_PREFIX + getCurrentRow().getId();
    }

    public IEntity getCurrentRowWorkPlan()
    {
        return getCurrentRow().getWorkPlan();
    }

    public void setCurrentRowWorkPlan(IEntity value)
    {
        if (value instanceof ViewWrapper)
        {
            ViewWrapper<EppWorkPlanBase> wrapper = (ViewWrapper<EppWorkPlanBase>) value;
            getCurrentRow().setWorkPlan(wrapper.getEntity());
        }
        else
            getCurrentRow().setWorkPlan((EppWorkPlanBase) value);
    }
}