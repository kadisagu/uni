package ru.tandemservice.uniepp_load.loadPlan.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Часы нагрузки для планируемого ППС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplPlannedPpsTimeItemGen extends EntityBase
 implements INaturalIdentifiable<EplPlannedPpsTimeItemGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem";
    public static final String ENTITY_NAME = "eplPlannedPpsTimeItem";
    public static final int VERSION_HASH = -747541908;
    private static IEntityMeta ENTITY_META;

    public static final String L_PPS = "pps";
    public static final String L_TIME_ITEM = "timeItem";
    public static final String P_TIME_AMOUNT_AS_LONG = "timeAmountAsLong";
    public static final String P_OVER_TIME = "overTime";

    private EplPlannedPps _pps;     // ППС
    private EplTimeItem _timeItem;     // Часы нагрузки
    private long _timeAmountAsLong;     // Число часов
    private boolean _overTime;     // Сверх ставки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ППС. Свойство не может быть null.
     */
    @NotNull
    public EplPlannedPps getPps()
    {
        return _pps;
    }

    /**
     * @param pps ППС. Свойство не может быть null.
     */
    public void setPps(EplPlannedPps pps)
    {
        dirty(_pps, pps);
        _pps = pps;
    }

    /**
     * @return Часы нагрузки. Свойство не может быть null.
     */
    @NotNull
    public EplTimeItem getTimeItem()
    {
        return _timeItem;
    }

    /**
     * @param timeItem Часы нагрузки. Свойство не может быть null.
     */
    public void setTimeItem(EplTimeItem timeItem)
    {
        dirty(_timeItem, timeItem);
        _timeItem = timeItem;
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Число часов. Свойство не может быть null.
     */
    @NotNull
    public long getTimeAmountAsLong()
    {
        return _timeAmountAsLong;
    }

    /**
     * @param timeAmountAsLong Число часов. Свойство не может быть null.
     */
    public void setTimeAmountAsLong(long timeAmountAsLong)
    {
        dirty(_timeAmountAsLong, timeAmountAsLong);
        _timeAmountAsLong = timeAmountAsLong;
    }

    /**
     * @return Сверх ставки. Свойство не может быть null.
     */
    @NotNull
    public boolean isOverTime()
    {
        return _overTime;
    }

    /**
     * @param overTime Сверх ставки. Свойство не может быть null.
     */
    public void setOverTime(boolean overTime)
    {
        dirty(_overTime, overTime);
        _overTime = overTime;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EplPlannedPpsTimeItemGen)
        {
            if (withNaturalIdProperties)
            {
                setPps(((EplPlannedPpsTimeItem)another).getPps());
                setTimeItem(((EplPlannedPpsTimeItem)another).getTimeItem());
            }
            setTimeAmountAsLong(((EplPlannedPpsTimeItem)another).getTimeAmountAsLong());
            setOverTime(((EplPlannedPpsTimeItem)another).isOverTime());
        }
    }

    public INaturalId<EplPlannedPpsTimeItemGen> getNaturalId()
    {
        return new NaturalId(getPps(), getTimeItem());
    }

    public static class NaturalId extends NaturalIdBase<EplPlannedPpsTimeItemGen>
    {
        private static final String PROXY_NAME = "EplPlannedPpsTimeItemNaturalProxy";

        private Long _pps;
        private Long _timeItem;

        public NaturalId()
        {}

        public NaturalId(EplPlannedPps pps, EplTimeItem timeItem)
        {
            _pps = ((IEntity) pps).getId();
            _timeItem = ((IEntity) timeItem).getId();
        }

        public Long getPps()
        {
            return _pps;
        }

        public void setPps(Long pps)
        {
            _pps = pps;
        }

        public Long getTimeItem()
        {
            return _timeItem;
        }

        public void setTimeItem(Long timeItem)
        {
            _timeItem = timeItem;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EplPlannedPpsTimeItemGen.NaturalId) ) return false;

            EplPlannedPpsTimeItemGen.NaturalId that = (NaturalId) o;

            if( !equals(getPps(), that.getPps()) ) return false;
            if( !equals(getTimeItem(), that.getTimeItem()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getPps());
            result = hashCode(result, getTimeItem());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getPps());
            sb.append("/");
            sb.append(getTimeItem());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplPlannedPpsTimeItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplPlannedPpsTimeItem.class;
        }

        public T newInstance()
        {
            return (T) new EplPlannedPpsTimeItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "pps":
                    return obj.getPps();
                case "timeItem":
                    return obj.getTimeItem();
                case "timeAmountAsLong":
                    return obj.getTimeAmountAsLong();
                case "overTime":
                    return obj.isOverTime();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "pps":
                    obj.setPps((EplPlannedPps) value);
                    return;
                case "timeItem":
                    obj.setTimeItem((EplTimeItem) value);
                    return;
                case "timeAmountAsLong":
                    obj.setTimeAmountAsLong((Long) value);
                    return;
                case "overTime":
                    obj.setOverTime((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "pps":
                        return true;
                case "timeItem":
                        return true;
                case "timeAmountAsLong":
                        return true;
                case "overTime":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "pps":
                    return true;
                case "timeItem":
                    return true;
                case "timeAmountAsLong":
                    return true;
                case "overTime":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "pps":
                    return EplPlannedPps.class;
                case "timeItem":
                    return EplTimeItem.class;
                case "timeAmountAsLong":
                    return Long.class;
                case "overTime":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplPlannedPpsTimeItem> _dslPath = new Path<EplPlannedPpsTimeItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplPlannedPpsTimeItem");
    }
            

    /**
     * @return ППС. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem#getPps()
     */
    public static EplPlannedPps.Path<EplPlannedPps> pps()
    {
        return _dslPath.pps();
    }

    /**
     * @return Часы нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem#getTimeItem()
     */
    public static EplTimeItem.Path<EplTimeItem> timeItem()
    {
        return _dslPath.timeItem();
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem#getTimeAmountAsLong()
     */
    public static PropertyPath<Long> timeAmountAsLong()
    {
        return _dslPath.timeAmountAsLong();
    }

    /**
     * @return Сверх ставки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem#isOverTime()
     */
    public static PropertyPath<Boolean> overTime()
    {
        return _dslPath.overTime();
    }

    public static class Path<E extends EplPlannedPpsTimeItem> extends EntityPath<E>
    {
        private EplPlannedPps.Path<EplPlannedPps> _pps;
        private EplTimeItem.Path<EplTimeItem> _timeItem;
        private PropertyPath<Long> _timeAmountAsLong;
        private PropertyPath<Boolean> _overTime;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ППС. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem#getPps()
     */
        public EplPlannedPps.Path<EplPlannedPps> pps()
        {
            if(_pps == null )
                _pps = new EplPlannedPps.Path<EplPlannedPps>(L_PPS, this);
            return _pps;
        }

    /**
     * @return Часы нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem#getTimeItem()
     */
        public EplTimeItem.Path<EplTimeItem> timeItem()
        {
            if(_timeItem == null )
                _timeItem = new EplTimeItem.Path<EplTimeItem>(L_TIME_ITEM, this);
            return _timeItem;
        }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem#getTimeAmountAsLong()
     */
        public PropertyPath<Long> timeAmountAsLong()
        {
            if(_timeAmountAsLong == null )
                _timeAmountAsLong = new PropertyPath<Long>(EplPlannedPpsTimeItemGen.P_TIME_AMOUNT_AS_LONG, this);
            return _timeAmountAsLong;
        }

    /**
     * @return Сверх ставки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem#isOverTime()
     */
        public PropertyPath<Boolean> overTime()
        {
            if(_overTime == null )
                _overTime = new PropertyPath<Boolean>(EplPlannedPpsTimeItemGen.P_OVER_TIME, this);
            return _overTime;
        }

        public Class getEntityClass()
        {
            return EplPlannedPpsTimeItem.class;
        }

        public String getEntityName()
        {
            return "eplPlannedPpsTimeItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
