/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.caf.logic.handler.SimpleTitledComboDataSourceWithPopupSizeHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic.EplDisciplineDSHandler;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic.EplStudentSummaryDao;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic.IEplStudentSummaryDao;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.logic.EplStudentSummaryGroupDAO;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.logic.IEplStudentSummaryGroupDAO;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplCompensationSource;

import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EduGroupMerge.EplStudentSummaryEduGroupMergeUI.LIST;

/**
 * @author oleyba
 * @since 10/6/11
 */
@Configuration
public class EplStudentSummaryManager extends BusinessObjectManager
{
    public static final String DS_ORG_UNIT = "eplStudentSummaryOrgUnitDS";
    public static final String DS_YEAR_PART = "eplStudentSummaryYearPartDS";
    public static final String DS_LOAD_TYPE = "eplGroupTypeDS";
    public static final String SPLIT_VARIANT_DS = "splitVariantDS";
    public static final String SPLIT_ELEMENT_DS = "splitElementDS";

    public static final String PARAM_COMPENSATION_SOURCE = "compensationSource";
    public static final String PARAM_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String PARAM_FORMATIVE_OR_OWNER_ORG_UNIT = "formativeOrOwnerOrgUnit";
    public static final String PARAM_SPLIT_VARIANT = "splitVariant";
    public static final String PARAM_SPLIT_ELEMENT = "splitElement";

    public static final String PARAM_SETTINGS = "settings";
    public static final String PARAM_SUMMARY = "summary";
    public static final String PARAM_EPP_YEAR = "eppYear";
    public static final String PARAM_YEAR_PART = "yearPart";
    public static final String PARAM_GROUP_TYPE = "groupType";
    public static final String PARAM_READING_ORGUNIT = "readingOrgUnit";
    public static final String PARAM_DISCIPLINE = "discipline";

    public static EplStudentSummaryManager instance() {
        return instance(EplStudentSummaryManager.class);
    }

    @Bean
    public IEplStudentSummaryDao dao() {
        return new EplStudentSummaryDao();
    }

	@Bean
	public IEplStudentSummaryGroupDAO groupDAO()
	{
		return new EplStudentSummaryGroupDAO();
	}

    @Bean
    public UIDataSourceConfig orgUnitDSConfig()
    {
        return SelectDSConfig.with(DS_ORG_UNIT, this.getName())
            .dataSourceClass(SelectDataSource.class)
            .handler(this.orgUnitDSHandler())
            .addColumn(OrgUnit.P_FULL_TITLE)
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> orgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), OrgUnit.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(new DQLSelectBuilder()
                    .fromEntity(EppTutorOrgUnit.class, "t")
                    .where(eq(property("t", EppTutorOrgUnit.orgUnit()), property(alias)))
                    .buildQuery()
                ));
            }
        }
            .order(OrgUnit.title())
            .filter(OrgUnit.title())
            .pageable(true)
            ;
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> compSourceDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EplCompensationSource.class)
            .order(EplCompensationSource.title())
            .filter(EplCompensationSource.title())
            ;
    }

    @Bean
    public UIDataSourceConfig groupTypeDSConfig()
    {
        return SelectDSConfig.with(DS_LOAD_TYPE, this.getName())
            .dataSourceClass(SelectDataSource.class)
            .addColumn("title", EppGroupType.title().s())
            .handler(this.groupTypeDSHandler())
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> groupTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EppGroupType.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)  {
                super.applyWhereConditions(alias, dql, context);

                final Object summary = context.get(PARAM_SUMMARY);
                final Object yearPart = context.get(PARAM_YEAR_PART);

                if (null != summary && null != yearPart)
                    dql.where(in(
                        property(alias, "id"),
                        new DQLSelectBuilder()
                            .fromEntity(EplEduGroup.class, "g")
                            .where(eq(property(EplEduGroup.summary().fromAlias("g")), commonValue(summary)))
                            .where(eq(property(EplEduGroup.yearPart().fromAlias("g")), commonValue(yearPart)))
                            .column(property(EplEduGroup.groupType().id().fromAlias("g")), "id")
                            .predicate(DQLPredicateType.distinct)
                            .buildQuery()
                    ));

            }
        }
            .order(EppGroupType.priority())
            .filter(EppGroupType.title())
            ;
    }

    @Bean
    public EntityComboDataSourceHandler disciplineDSHandler()
    {
        return new EplDisciplineDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler groupComboDSHandler()
    {
        return new SimpleTitledComboDataSourceWithPopupSizeHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                context.put(UIDefines.COMBO_OBJECT_LIST, createStatement(getBuilder(input, context)).list());
                DSOutput output = super.execute(input, context);
                output.setTotalSize(((Number) getBuilder(input, context).createCountStatement(new DQLExecutionContext(context.getSession())).uniqueResult()).intValue());
                return output;
            }

            private DQLSelectBuilder getBuilder(DSInput input, ExecutionContext context)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EplGroup.class, "g")
                    .column(property("g", EplGroup.title()))
                    .distinct();

                EplStudentSummary summary = context.get("summary");
                if(summary != null)
                    builder.where(eq(property("g", EplGroup.summary()), commonValue(summary)));

                String filter = input.getComboFilterByValue();
                if(!StringUtils.isEmpty(filter))
                    builder.where(likeUpper(property("g", EplGroup.title()), value(CoreStringUtils.escapeLike(filter, true))));

                OrgUnit formativeOrgUnit = context.get("formativeOrgUnit");

                DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                    .fromEntity(EplStudent.class, "s")
                    .where(eq(property("s", EplStudent.group()), property("g")));


                if(formativeOrgUnit != null)
                    subBuilder.where(eq(property("s", EplStudent.educationOrgUnit().formativeOrgUnit()), commonValue(formativeOrgUnit)));

                EducationLevelsHighSchool eduHs = context.get("eduHS");
                if(eduHs != null)
                    subBuilder.where(eq(property("s", EplStudent.educationOrgUnit().educationLevelHighSchool()), commonValue(eduHs)));

                DevelopForm developForm = context.get("developForm");
                if(developForm != null)
                    subBuilder.where(eq(property("s", EplStudent.educationOrgUnit().developForm()), commonValue(developForm)));

                if(formativeOrgUnit != null || eduHs != null || developForm != null)
                    builder.where(exists(subBuilder.buildQuery()));

                Course course = context.get("course");
                if(course != null)
                    builder.where(eq(property("g", EplGroup.course()), commonValue(course)));

                builder.order(property("g", EplGroup.title()));

                return builder;
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> groupDSHandler()
    {
        return new SimpleTitledComboDataSourceWithPopupSizeHandler(this.getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                final String filter = input.getComboFilterByValue() == null ? null : input.getComboFilterByValue().toLowerCase();
                List<ViewWrapper<EplEduGroup>> groupListWithHours = context.get(LIST);
                groupListWithHours = groupListWithHours.stream().
                        filter(g -> g.getTitle().toLowerCase().contains(filter)).collect(Collectors.toList());
                context.put(UIDefines.COMBO_OBJECT_LIST, groupListWithHours);
                DSOutput output = super.execute(input, context);
                output.setTotalSize(groupListWithHours.size());
                return output;
            }
        };
    }
}
