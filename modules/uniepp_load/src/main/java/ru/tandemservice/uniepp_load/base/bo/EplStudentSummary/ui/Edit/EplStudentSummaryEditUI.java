/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Edit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.tapsupport.TapSupportUtils;
import org.tandemframework.tapsupport.confirm.ClickButtonAction;
import org.tandemframework.tapsupport.confirm.ConfirmInfo;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Pub.EplStudentSummaryPub;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.StudentAdd.EplStudentSummaryStudentAdd;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.StudentAdd.EplStudentSummaryStudentAddUI;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.base.ext.OrgUnit.ui.View.OrgUnitViewExt;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTab;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTabUI;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 10/27/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "summaryHolder.id", required = true),
    @Bind(key = EplOrgUnitTabUI.BIND_ORG_UNIT_ID, binding = "orgUnitHolder.id")
})
public class EplStudentSummaryEditUI extends UIPresenter
{
    private EntityHolder<EplStudentSummary> summaryHolder = new EntityHolder<EplStudentSummary>();
    private EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<>();

    private List<EplStudent> rowList = new ArrayList<>();
    private EplStudent currentRow;

    @Override
    public void onComponentRefresh()
    {
        getSummaryHolder().refresh();
        getOrgUnitHolder().refresh();
        if (getOrgUnitHolder().getId() != null) {
            getSettings().set("formOu", getOrgUnitHolder().getValue());
        }
        onChangeFilterParams();
    }

    public void onChangeFilterParams() {
        saveSettings();

        OrgUnit formOu = getFormOu();
        DevelopForm developForm = getSettings().get("developForm");

        setCurrentRow(null);
        setRowList(new ArrayList<EplStudent>());

        if (formOu == null || developForm == null) {
            return;
        }

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EplStudent.class, "s")
            .where(eq(property("s", EplStudent.group().summary()), value(getSummary())))
            .where(eq(property("s", EplStudent.educationOrgUnit().formativeOrgUnit()), value(formOu)))
            .where(eq(property("s", EplStudent.educationOrgUnit().developForm()), value(developForm)))
            .order(property("s", EplStudent.group().course().intValue()))
            .order(property("s", EplStudent.group().title()))
            .order(property("s", EplStudent.compensationSource().code()))
            ;

        FilterUtils.applySelectFilter(dql, "s", EplStudent.group().course(), getSettings().get("course"));
        FilterUtils.applySelectFilter(dql, "s", EplStudent.compensationSource(), getSettings().get("compensationSource"));

        setRowList(IUniBaseDao.instance.get().<EplStudent>getList(dql));
    }

    public void onClickAddGroup() {
        _uiActivation
            .asRegionDialog(EplStudentSummaryStudentAdd.class)
            .parameter(PUBLISHER_ID, getSummaryHolder().getId())
            .parameter(EplOrgUnitTabUI.BIND_ORG_UNIT_ID, getOrgUnitHolder().getId())
            .parameter(EplStudentSummaryStudentAddUI.OU_ID, getFormOu().getId())
            .activate();
    }

    public void onClickClose() {
        if (getOrgUnitHolder().getId() != null) {
            _uiActivation
                .asDesktopRoot(OrgUnitView.class)
                .parameter("selectedPage", OrgUnitViewExt.TAB_EPP_LOAD_ORG_UNIT)
                .parameter("selectedSubPage", EplOrgUnitTab.TAB_STUDENT_SUMMARY)
                .parameter(PUBLISHER_ID, getOrgUnitHolder().getId())
                .activate();
        } else {
            _uiActivation
                .asDesktopRoot(EplStudentSummaryPub.class)
                .parameter("selectedTab", "groups")
                .parameter(PUBLISHER_ID, getSummary().getId())
                .activate();
        }
    }

    public void onClickEditRow()
    {
        _uiActivation.asRegionDialog(EplStudentSummaryStudentAdd.class)
                .parameter(PUBLISHER_ID, getSummary().getId())
                .parameter(EplOrgUnitTabUI.BIND_ORG_UNIT_ID, getOrgUnitHolder().getId())
                .parameter(EplStudentSummaryStudentAddUI.STUDENT_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickDeleteRow()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
        onComponentRefresh();
    }

    public String getAlertMessage()
    {
        return "Удалить " +
                CommonBaseStringUtil.numberWithPostfixCase(currentRow.getCount(),
                        "планируемого студента?", "планируемых студентов?", "планируемых студентов?");
    }

    public boolean isShowGroupList() {
        DevelopForm developForm = getSettings().get("developForm");
        return !(getFormOu() == null || developForm == null);
    }

    private OrgUnit getFormOu()
    {
        return getSettings().get("formOu");
    }

    // getters and setters

    public ISecured getSecurityObject() {
        return getOrgUnitHolder().getId() == null ? getSummary() : getOrgUnitHolder().getValue();
    }

    public String getEditPK() {
        if (getOrgUnitHolder().getId() == null) return "eplStudentSummaryEditContingentGroups";
        return new OrgUnitSecModel(getOrgUnitHolder().getValue()).getPermission("editContingentGroupsEplGroupTab");
    }

    public boolean isFormOuDisabled() {
        return getOrgUnitHolder().getId() != null;
    }

    public String getCountFieldId() {
        return "studentCount." + getCurrentRow().getId();
    }

    public EntityHolder<EplStudentSummary> getSummaryHolder()
    {
        return summaryHolder;
    }

    public EplStudentSummary getSummary()
    {
        return getSummaryHolder().getValue();
    }

    public EplStudent getCurrentRow()
    {
        return currentRow;
    }

    public void setCurrentRow(EplStudent currentRow)
    {
        this.currentRow = currentRow;
    }

    public List<EplStudent> getRowList()
    {
        return rowList;
    }

    public void setRowList(List<EplStudent> rowList)
    {
        this.rowList = rowList;
    }

    public EntityHolder<OrgUnit> getOrgUnitHolder()
    {
        return orgUnitHolder;
    }

    public void setOrgUnitHolder(EntityHolder<OrgUnit> orgUnitHolder)
    {
        this.orgUnitHolder = orgUnitHolder;
    }

    @Override
    public String getSettingsKey()
    {
        final String settingsKey = super.getSettingsKey();
        return getOrgUnitHolder().getId() != null ? settingsKey + getOrgUnitHolder().getId() : settingsKey;
    }
}