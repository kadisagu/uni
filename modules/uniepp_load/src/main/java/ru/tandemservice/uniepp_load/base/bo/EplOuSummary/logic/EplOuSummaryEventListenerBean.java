/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteAction;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.EplOuSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplRecalculateSummaryKind;
import ru.tandemservice.uniepp_load.base.bo.EplState.EplStateManager;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * @author Alexey Lopatin
 * @since 01.03.2016
 */
public class EplOuSummaryEventListenerBean
{
    private static class EplOuSummaryStateCheckEventListener extends ParamTransactionCompleteAction<EplOrgUnitSummary, Void> implements IDSetEventListener
    {
        private static final EplOuSummaryStateCheckEventListener INSTANCE = new EplOuSummaryStateCheckEventListener();

        @Override
        public Void beforeCompletion(Session session, Collection<EplOrgUnitSummary> summaryList)
        {
            if (summaryList.isEmpty()) return null;
            try
            {
                final String title = "Проверка расчета";
                final IBackgroundProcess process = new BackgroundProcessBase()
                {
                    @Override
                    public ProcessResult run(ProcessState state)
                    {
                        EplOuSummaryManager.instance().dao().doRecalculateSummary(summaryList.iterator().next().getId(), EplRecalculateSummaryKind.CHECK, new ProcessState(ProcessDisplayMode.unknown));
                        return new ProcessResult(title + " завершена.");
                    }
                };
                new BackgroundProcessHolder().start(title, process, ProcessDisplayMode.unknown);
            }
            catch (Exception ex) {
                ex.printStackTrace();
                throw new ApplicationException("Произошла ошибка при изменении состояния расчета. " + ex.getMessage());
            }
            return null;
        }

        @Override
        public void onEvent(DSetEvent event)
        {
            if (EplOrgUnitSummary.class.isAssignableFrom(event.getMultitude().getEntityMeta().getEntityClass()))
            {
                final Set affectedProperties = event.getMultitude().getAffectedProperties();
                if (affectedProperties.contains(EplOrgUnitSummary.L_STATE))
                {
                    EplOrgUnitSummary summary = (EplOrgUnitSummary) event.getMultitude().getSingularEntity();
                    boolean canChange = EplStateManager.instance().dao().isCanChangeStateOuSummary(summary);
                    if (!canChange)
                        throw new ApplicationException("Нельзя изменить состояние расчета, т.к. состояние сводки контингента отлично от «Расчет часов».");

                    EppState state = summary.getState();
                    if (state.isAcceptable() || state.isAccepted())
                    {
                        register(event, Collections.singletonList(summary));
                    }
                }
            }
        }
    }

    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EplOrgUnitSummary.class, EplOuSummaryStateCheckEventListener.INSTANCE);
    }
}
