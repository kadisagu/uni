/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.assignment;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.utils.DQLSimple;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.EplOuSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.EplEduGroupType;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.IEplOuSummaryDAO;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCategoryWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCommonRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplDiscOrRuleRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplSimpleRuleRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplBaseCategoryDataWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplBaseTimeDataWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplCommonRowWrapperComparator;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplTimeItemVariantGroupingCodes;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Alexey Lopatin
 * @since 08.08.2016
 */
public class EplAssignmentCommonWrapper extends EplBaseTimeDataWrapper
{
    private EplIndividualPlan _plan;
    private Set<YearDistributionPart> _yearParts;

    private Map<EplEduGroup, EplEduGroupType> _eduGroupTypeMap;
    private IEplOuSummaryDAO.IDistributionEduGroupData _eduGroupData;

    protected EplAssignmentCommonWrapper(Collection<EplOrgUnitSummary> ouSummaries, boolean withTransferTimeItem, boolean withSimpleTimeItem)
    {
        super(ouSummaries, withTransferTimeItem, withSimpleTimeItem);
    }

    protected EplAssignmentCommonWrapper(Collection<EplOrgUnitSummary> ouSummaries, boolean withTransferTimeItem, boolean withSimpleTimeItem, EplIndividualPlan plan)
    {
        super(ouSummaries, withTransferTimeItem, withSimpleTimeItem);
        _plan = plan;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected <T extends EplBaseTimeDataWrapper> T fill(Set<String> propertyPathSet, boolean fillTimeItem, boolean select)
    {
        EplBaseTimeDataWrapper dataWrapper = super.fill(propertyPathSet, fillTimeItem, select);

        _eduGroupTypeMap = EplOuSummaryManager.instance().dao().getEduGroupTypeMap(getEduGroups());
        _eduGroupData = EplOuSummaryManager.instance().dao().prepareDistributionEduGroupData(getOuSummaries(), _plan == null ? null : _plan.getPps(), true);

        return (T) dataWrapper;
    }

    public Set<YearDistributionPart> getYearParts()
    {
        if (null != _yearParts) return _yearParts;

        _yearParts = Sets.newTreeSet(YEAR_PART_COMPARATOR);
        List<YearDistributionPart> allYearParts = new DQLSimple<>(YearDistributionPart.class).order(YearDistributionPart.yearDistribution().amount()).order(YearDistributionPart.number()).list();

        Set<EplEduGroup> eduGroups =
                Stream.concat(
                        getDiscRowMap().getOrDefault(EplTimeItemVariantGroupingCodes.BY_REG_ELEMENT, Maps.newHashMap()).values().stream(),
                        getDiscRowMap().getOrDefault(EplTimeItemVariantGroupingCodes.BY_REG_ELEMENT_AND_TIME_RULE, Maps.newHashMap()).values().stream()
                )
                .collect(Collectors.toList())
                .stream()
                        .map(EplDiscOrRuleRowWrapper::getEplGroups)
                        .flatMap(Set::stream)
                        .collect(Collectors.toSet());

        eduGroups.stream()
                .map(eduGroup -> eduGroup.getYearPart().getYearDistribution())
                .collect(Collectors.toSet())
                .forEach(dist -> _yearParts.addAll(
                        allYearParts.stream()
                                .filter(item -> dist.equals(item.getYearDistribution()))
                                .collect(Collectors.toList())
                ));
        // для определения практики
        _yearParts.add(null);

        return _yearParts;
    }

    public Map<YearDistributionPart, Data> getYearPartToDataMap()
    {
        Map<YearDistributionPart, Data> resultMap = Maps.newHashMap();

        for (YearDistributionPart yearPart : getYearParts())
        {
            List<String> regTypeCodes = yearPart != null
                    ? Collections.singletonList(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                    : Arrays.asList(EppRegistryStructureCodes.REGISTRY_PRACTICE, EppRegistryStructureCodes.REGISTRY_ATTESTATION);

            Map<String, Map<EplCommonRowWrapper.DisciplineOrRulePairKey, EplDiscOrRuleRowWrapper>> discRowMap = getDiscRowMap();

            List<EplDiscOrRuleRowWrapper> discRows = Lists.newArrayList();

            Collection<EplDiscOrRuleRowWrapper> regElementList = discRowMap.getOrDefault(EplTimeItemVariantGroupingCodes.BY_REG_ELEMENT, Maps.newHashMap()).values();
            Collection<EplDiscOrRuleRowWrapper> regElementAndTimeRuleList = discRowMap.getOrDefault(EplTimeItemVariantGroupingCodes.BY_REG_ELEMENT_AND_TIME_RULE, Maps.newHashMap()).values();

            for (EplDiscOrRuleRowWrapper row : regElementList)
            {
                regElementAndTimeRuleList.stream()
                        .filter(rowWithTimeRule -> row.getDiscipline().equals(rowWithTimeRule.getDiscipline()))
                        .forEach(rowWithTimeRule -> {
                            row.getEduGroupTimeItemList().addAll(rowWithTimeRule.getEduGroupTimeItemList());
                            row.getEplGroups().addAll(rowWithTimeRule.getEplGroups());
                        });
                discRows.add(row);
            }
            discRows.addAll(regElementAndTimeRuleList);
            Collections.sort(discRows, EplCommonRowWrapperComparator.INSTANCE);

            Set<EplTimeCategoryEduLoad> usedCategories = Sets.newHashSet();
            Map<PairKey<EplDiscOrRuleRowWrapper, YearDistributionPart>, List<EplEduGroupTimeItem>> rowsMap = Maps.newLinkedHashMap();

            // все дисциплины, совпадающие по части года
            for (EplDiscOrRuleRowWrapper row : discRows)
            {
                EppRegistryStructure regStructure = row.getDiscipline().getRegistryElement().getParent();
                if (!isRegTypeEquals(regStructure, regTypeCodes)) continue;

                List<EplEduGroupTimeItem> timeItems = row.getEduGroupTimeItemList();
                if (null == yearPart)
                    Collections.sort(timeItems, EDU_GROUP_TIME_ITEM_COMPARATOR);

                for (EplEduGroupTimeItem timeItem : timeItems)
                {
                    YearDistributionPart eduYearPart = timeItem.getEduGroup().getYearPart();
                    if (null != yearPart && !yearPart.equals(eduYearPart)) continue;

                    EplTimeCategoryEduLoad category = timeItem.getTimeRule().getCategory();
                    usedCategories.add(category);

                    // для практик группируем по части уч. года
                    SafeMap.safeGet(rowsMap, PairKey.create(row, yearPart != null ? null : eduYearPart), ArrayList.class).add(timeItem);
                }
            }
            EplBaseCategoryDataWrapper categoryDataWrapper = EplOuSummaryManager.instance().dao().getCategoryDataWrapper(usedCategories, Sets.newHashSet());

            List<EplAssignmentRowWrapper> rows = getRows(yearPart, categoryDataWrapper, rowsMap);
            resultMap.put(yearPart, new Data(categoryDataWrapper, rows));
        }
        return resultMap;
    }

    /**
     * Возвращает строки для частей учебного года и практик
     *
     * @param yearPart            часть учебного года (если null - тогда это "Практики и ИГА")
     * @param categoryDataWrapper враппер для вывода категорий норм времени
     * @param rowsMap             мап связи враппера рассчит. часов со списком рассчит часов
     * @return Возвращает строки планируемых часов нагрузки для ППС
     */
    public List<EplAssignmentRowWrapper> getRows(YearDistributionPart yearPart, EplBaseCategoryDataWrapper categoryDataWrapper, Map<PairKey<EplDiscOrRuleRowWrapper, YearDistributionPart>, List<EplEduGroupTimeItem>> rowsMap)
    {
        // поднимаем планируемые часы для ППС
        List<EplTimeItem> timeItems = rowsMap.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
        Map<EplTimeItem, List<EplPlannedPpsTimeItem>> timeItemToPlannedMap = getTimeItemToPlannedMap(timeItems);

        List<EplAssignmentRowWrapper> resultList = Lists.newLinkedList();

        EplAssignmentRowWrapper totalWrapper = new EplAssignmentRowWrapper("Всего", false, true);

        for (Map.Entry<PairKey<EplDiscOrRuleRowWrapper, YearDistributionPart>, List<EplEduGroupTimeItem>> entry : rowsMap.entrySet())
        {
            Set<EppGroupType> eppGroupTypes = Sets.newTreeSet(GROUP_TYPE_COMPARATOR);
            Map<EppGroupType, Set<EplEduGroup>> gt2EplEduGroupMap = Maps.newLinkedHashMap();
            Map<EppGroupType, Map<EplTimeCategoryEduLoad, Double>> gt2CategoryTimeMap = Maps.newHashMap();
            Map<EplTimeCategoryEduLoad, Double> category2TimeMap = Maps.newHashMap();

            for (EplEduGroupTimeItem timeItem : entry.getValue())
            {
                EplEduGroup eduGroup = timeItem.getEduGroup();
                EppGroupType type = eduGroup.getGroupType();
                EplTimeCategoryEduLoad category = timeItem.getTimeRule().getCategory();

                eppGroupTypes.add(type);
                SafeMap.safeGet(gt2EplEduGroupMap, type, HashSet.class).add(eduGroup);

                Map<EplTimeCategoryEduLoad, Double> timeMap = SafeMap.safeGet(gt2CategoryTimeMap, type, HashMap.class);

                for (EplPlannedPpsTimeItem plannedTimeItem : timeItemToPlannedMap.getOrDefault(timeItem, Lists.newArrayList()))
                {
                    timeMap.put(category, timeMap.getOrDefault(category, 0d) + plannedTimeItem.getTimeAmount());
                    category2TimeMap.put(category, category2TimeMap.getOrDefault(category, 0d) + plannedTimeItem.getTimeAmount());
                }
            }

            // подсчитываем параметры
            Map<EppGroupType, ParameterKey> gt2MultiKeyMap = getGroupType2ParameterKeyMap(gt2EplEduGroupMap, true, true);
            Set<ParameterKey> parameters = Sets.newHashSet(gt2MultiKeyMap.values());

            // добавляем строку дисциплиночасти
            if (!eppGroupTypes.isEmpty())
            {
                EplDiscOrRuleRowWrapper row = entry.getKey().getFirst();
                EplAssignmentRowWrapper wrapper = new EplAssignmentRowWrapper(row.getDiscipline().getTitleWithLabor());
                boolean yearPartVisible = yearPart == null;

                // если данные в УГС совпадают
                if (parameters.size() == 1)
                {
                    // по параметрам
                    setParametersRow(wrapper, parameters.iterator().next(), yearPartVisible);
                    // по категориям
                    setCategoriesRow(wrapper, categoryDataWrapper.getTopLevelCategories(), category2TimeMap);
                    // итоговый
                    setCategoriesRow(totalWrapper, categoryDataWrapper.getTopLevelCategories(), category2TimeMap);
                }
                else
                {
                    for (EppGroupType eppGroupType : eppGroupTypes)
                    {
                        ParameterKey key = gt2MultiKeyMap.get(eppGroupType);
                        EplAssignmentRowWrapper childWrapper = new EplAssignmentRowWrapper(eppGroupType.getTitle(), true, false);

                        // по параметрам
                        setParametersRow(childWrapper, key, yearPartVisible);
                        // по категориям
                        Map<EplTimeCategoryEduLoad, Double> timeMap = gt2CategoryTimeMap.get(eppGroupType);
                        setCategoriesRow(childWrapper, categoryDataWrapper.getTopLevelCategories(), timeMap);

                        wrapper.getChildren().add(childWrapper);
                        setCategoriesRow(wrapper, categoryDataWrapper.getTopLevelCategories(), timeMap);

                        setCategoriesRow(totalWrapper, categoryDataWrapper.getTopLevelCategories(), timeMap);
                    }
                }
                resultList.add(wrapper);
            }
        }
        resultList.add(totalWrapper);
        return resultList;
    }

    public Map<EppGroupType, ParameterKey> getGroupType2ParameterKeyMap(Map<EppGroupType, Set<EplEduGroup>> gt2EplEduGroupMap, boolean needEduGroupType, boolean needEplGroup)
    {
        Map<EppGroupType, ParameterKey> resultMap = Maps.newLinkedHashMap();

        for (EppGroupType eppGroupType : gt2EplEduGroupMap.keySet())
        {
            MutableInt flowCount = new MutableInt(0);
            MutableInt groupCount = new MutableInt(0);
            MutableInt subGroupCount = new MutableInt(0);
            MutableInt studentCount = new MutableInt(0);

            List<EplStudent> eplStudents = Lists.newArrayList();
            Set<YearDistributionPart> groupYearParts = Sets.newTreeSet(YEAR_PART_COMPARATOR);
            Set<EplEduGroup> eduGroups = gt2EplEduGroupMap.get(eppGroupType);

            eduGroups.forEach(eduGroup -> {
                groupYearParts.add(eduGroup.getYearPart());
                eplStudents.addAll(getGroupMap().getOrDefault(eduGroup, Collections.emptyMap()).keySet());
                studentCount.setValue(studentCount.intValue() + _eduGroupData.studentCountMap().getOrDefault(eduGroup.getId(), 0L));

                if (needEduGroupType)
                {
                    EplEduGroupType eduGroupType = _eduGroupTypeMap.get(eduGroup);
                    if (null != eduGroupType)
                    {
                        if (eduGroupType.equals(EplEduGroupType.FLOW))
                            flowCount.increment();
                        else if (eduGroupType.equals(EplEduGroupType.GROUP))
                            groupCount.increment();
                        else if (eduGroupType.equals(EplEduGroupType.SUB_GROUP))
                            subGroupCount.increment();
                    }
                }
            });

            List<String> groupYearPartList = Lists.newArrayList();
            groupYearParts.forEach(value -> groupYearPartList.add(value.getTitle()));
            String educationOrgUnitTitles = getEducationOrgUnitTitles(eplStudents);
            String courses = getCourses(eplStudents);
            String groupYearPartTitles = StringUtils.join(groupYearPartList, ", ");
            String eplGroups = needEplGroup ? getEplGroups(eplStudents) : null;

            resultMap.put(eppGroupType, new ParameterKey(educationOrgUnitTitles, courses, groupYearPartTitles, flowCount.intValue(), groupCount.intValue(), subGroupCount.intValue(), studentCount.intValue(), eplGroups));
        }
        return resultMap;
    }

    /**
     * Возвращает строки планируемых часов нагрузки для ППС по простым нормам и часы с группировкой "норма времени"
     */
    public List<EplAssignmentRowWrapper> getOtherRows()
    {
        List<EplSimpleRuleRowWrapper> ruleRows = Lists.newArrayList(getSimpleRuleRowMap().getOrDefault(EplSimpleRuleRowWrapper.BY_SIMPLE_TIME_RULE, Maps.newHashMap()).values());
        Collections.sort(ruleRows, ITitled.TITLED_COMPARATOR);

        List<EplDiscOrRuleRowWrapper> discRows = Lists.newArrayList(getDiscRowMap().getOrDefault(EplTimeItemVariantGroupingCodes.BY_TIME_RULE, Maps.newHashMap()).values());
        Collections.sort(discRows, EplCommonRowWrapperComparator.INSTANCE);

        // поднимаем планируемые часы для ППС
        List<EplTimeItem> timeItems = Lists.newArrayList();
        timeItems.addAll(ruleRows.stream().map(EplSimpleRuleRowWrapper::getSimpleTimeItemList).flatMap(List::stream).collect(Collectors.toList()));
        timeItems.addAll(discRows.stream().map(EplDiscOrRuleRowWrapper::getEduGroupTimeItemList).flatMap(List::stream).collect(Collectors.toList()));

        Map<EplTimeItem, List<EplPlannedPpsTimeItem>> timeItemToPlannedMap = getTimeItemToPlannedMap(timeItems);

        List<EplAssignmentRowWrapper> resultList = Lists.newLinkedList();
        EplAssignmentRowWrapper totalWrapper = new EplAssignmentRowWrapper("Всего", false, true);

        // простая норма
        for (EplSimpleRuleRowWrapper row : ruleRows)
        {
            EplAssignmentRowWrapper wrapper = new EplAssignmentRowWrapper(row.getRule().getTitle());

            double sum = row.getSimpleTimeItemList().stream()
                    .flatMap(timeItem -> timeItemToPlannedMap.getOrDefault(timeItem, Lists.newArrayList()).stream())
                    .mapToDouble(EplPlannedPpsTimeItem::getTimeAmount).sum();

            wrapper.getTimeByCategoryMap().put(null, sum);

            Map<EplTimeCategoryEduLoad, Double> totalTimeMap = totalWrapper.getTimeByCategoryMap();
            totalTimeMap.put(null, totalTimeMap.getOrDefault(null, 0d) + sum);

            resultList.add(wrapper);
        }

        // группировка - по норме времени
        for (EplDiscOrRuleRowWrapper row : discRows)
        {
            Set<EppGroupType> eppGroupTypes = Sets.newTreeSet(GROUP_TYPE_COMPARATOR);
            Map<EppGroupType, Set<EplEduGroup>> gt2EplEduGroupMap = Maps.newHashMap();
            Map<EppGroupType, Double> timeMap = Maps.newHashMap();

            for (EplEduGroupTimeItem timeItem : row.getEduGroupTimeItemList())
            {
                EplEduGroup eduGroup = timeItem.getEduGroup();
                EppGroupType type = eduGroup.getGroupType();

                eppGroupTypes.add(type);
                SafeMap.safeGet(gt2EplEduGroupMap, type, HashSet.class).add(eduGroup);

                for (EplPlannedPpsTimeItem plannedTimeItem : timeItemToPlannedMap.getOrDefault(timeItem, Lists.newArrayList()))
                {
                    timeMap.put(type, timeMap.getOrDefault(type, 0d) + plannedTimeItem.getTimeAmount());
                }
            }

            // подсчитываем параметры
            Map<EppGroupType, ParameterKey> gt2MultiKeyMap = getGroupType2ParameterKeyMap(gt2EplEduGroupMap, false, false);
            Set<ParameterKey> parameters = Sets.newHashSet(gt2MultiKeyMap.values());

            // добавляем строку дисциплиночасти
            if (!eppGroupTypes.isEmpty())
            {
                EplAssignmentRowWrapper wrapper = new EplAssignmentRowWrapper(row.getRule().getTitle());

                // если данные в УГС совпадают
                if (parameters.size() == 1)
                {
                    double sum = timeMap.values().stream().mapToDouble(item -> item).sum();

                    // по параметрам
                    setParametersRow(wrapper, parameters.iterator().next(), true);
                    // всего
                    wrapper.getTimeByCategoryMap().put(null, sum);
                    // итоговый
                    Map<EplTimeCategoryEduLoad, Double> totalTimeMap = totalWrapper.getTimeByCategoryMap();
                    totalTimeMap.put(null, totalTimeMap.getOrDefault(null, 0d) + sum);
                }
                else
                {
                    double sum = 0d;
                    for (EppGroupType eppGroupType : eppGroupTypes)
                    {
                        ParameterKey key = gt2MultiKeyMap.get(eppGroupType);
                        EplAssignmentRowWrapper childWrapper = new EplAssignmentRowWrapper(eppGroupType.getTitle(), true, false);
                        Double value = timeMap.get(eppGroupType);

                        // по параметрам
                        setParametersRow(childWrapper, key, true);
                        // по категориям
                        childWrapper.getTimeByCategoryMap().put(null, value);

                        wrapper.getChildren().add(childWrapper);
                        sum += value;
                    }
                    wrapper.getTimeByCategoryMap().put(null, sum);

                    // итоговый
                    Map<EplTimeCategoryEduLoad, Double> totalTimeMap = totalWrapper.getTimeByCategoryMap();
                    totalTimeMap.put(null, totalTimeMap.getOrDefault(null, 0d) + sum);
                }
                resultList.add(wrapper);
            }
        }
        resultList.add(totalWrapper);
        return resultList;
    }

    private Map<EplTimeItem, List<EplPlannedPpsTimeItem>> getTimeItemToPlannedMap(List<EplTimeItem> timeItems)
    {
        if (null == _plan) return Maps.newHashMap();

        List<EplPlannedPpsTimeItem> plannedTimeItems = new DQLSimple<>(EplPlannedPpsTimeItem.class)
                .where(EplPlannedPpsTimeItem.timeItem(), timeItems)
                .where(EplPlannedPpsTimeItem.pps().ppsEntry(), _plan.getPps())
                .list();
        return CommonBaseEntityUtil.groupByProperty(plannedTimeItems, EplPlannedPpsTimeItem.timeItem().s());
    }

    private void setParametersRow(EplAssignmentRowWrapper wrapper, ParameterKey key, boolean yearPartVisible)
    {
        wrapper.setEouTitles(key.getEouTitles());
        wrapper.setCourses(key.getCourses());

        if (yearPartVisible)
            wrapper.setGroupYearPartTitles(key.getGroupYearPartTitles());

        wrapper.setFlowCount(key.getFlowCount());
        wrapper.setGroupCount(key.getGroupCount());
        wrapper.setSubGroupCount(key.getSubGroupCount());
        wrapper.setStudentCount(key.getStudentCount());
        wrapper.setEplGroups(key.getEplGroups());
    }

    private void setCategoriesRow(EplAssignmentRowWrapper wrapper, List<EplCategoryWrapper> topCategories, Map<EplTimeCategoryEduLoad, Double> timeMap)
    {
        Map<EplTimeCategoryEduLoad, Double> timeByCategoryMap = wrapper.getTimeByCategoryMap();
        for (EplCategoryWrapper categoryWrapper : topCategories)
        {
            List<EplCategoryWrapper> children = categoryWrapper.getChildren();
            if (children.isEmpty())
                putCategoryTimeMap(categoryWrapper.category, timeByCategoryMap, timeMap);
            else
                children.forEach(sub -> putCategoryTimeMap(sub.category, timeByCategoryMap, timeMap));
        }
    }

    private void putCategoryTimeMap(EplTimeCategoryEduLoad category, Map<EplTimeCategoryEduLoad, Double> baseMap, Map<EplTimeCategoryEduLoad, Double> tempMap)
    {
        baseMap.put(category, baseMap.getOrDefault(category, 0d) + tempMap.getOrDefault(category, 0d));
    }

    public static boolean isRegTypeEquals(EppRegistryStructure regStructure, List<String> codes)
    {
        return codes.contains(regStructure.getCode()) || (null != regStructure.getParent() && codes.contains(regStructure.getParent().getCode()));
    }

    public static String getEducationOrgUnitTitles(List<EplStudent> eplStudents)
    {
        Set<String> temp = Sets.newHashSet();
        eplStudents.forEach(eplStudent -> temp.add(eplStudent.getEducationOrgUnit().getProgramSubjectShortExtendedTitleWithDevelopPeriod()));
        return joinTitles(temp, "\n");
    }

    public static String getCourses(List<EplStudent> eplStudents)
    {
        Set<String> temp = Sets.newHashSet();
        eplStudents.forEach(eplStudent -> temp.add(eplStudent.getGroup().getCourse().getTitle()));
        return joinTitles(temp, ", ");
    }

    public static String getEplGroups(List<EplStudent> eplStudents)
    {
        Set<String> temp = Sets.newHashSet();
        eplStudents.forEach(eplStudent -> temp.add(eplStudent.getGroup().getTitle()));
        return joinTitles(temp, ", ");
    }

    public static String joinTitles(Set<String> set, String separator)
    {
        List<String> titles = Lists.newArrayList(set);
        Collections.sort(titles);
        return StringUtils.join(titles, separator);
    }

    public static class ParameterKey extends MultiKey
    {
        private String _eouTitles;
        private String _courses;
        private String _groupYearPartTitles;
        private int _flowCount;
        private int _groupCount;
        private int _subGroupCount;
        private int _studentCount;
        private String _eplGroups;

        public ParameterKey(String eouTitles, String courses, String groupYearPartTitles, int flowCount, int groupCount, int subGroupCount, int studentCount, String eplGroups)
        {
            super(new Object[]{eouTitles, courses, groupYearPartTitles, flowCount, groupCount, subGroupCount, studentCount, eplGroups});

            _eouTitles = eouTitles;
            _courses = courses;
            _groupYearPartTitles = groupYearPartTitles;
            _flowCount = flowCount;
            _groupCount = groupCount;
            _subGroupCount = subGroupCount;
            _studentCount = studentCount;
            _eplGroups = eplGroups;
        }

        public String getEouTitles() { return _eouTitles; }
        public String getCourses() { return _courses; }
        public String getGroupYearPartTitles() { return _groupYearPartTitles; }
        public int getFlowCount() { return _flowCount; }
        public int getGroupCount() { return _groupCount; }
        public int getSubGroupCount() { return _subGroupCount; }
        public int getStudentCount() { return _studentCount; }
        public String getEplGroups() { return _eplGroups; }
    }

    public class Data
    {
        private EplBaseCategoryDataWrapper _categoryDataWrapper;
        private List<EplAssignmentRowWrapper> _rows;

        public Data(EplBaseCategoryDataWrapper categoryDataWrapper, List<EplAssignmentRowWrapper> rows)
        {
            _categoryDataWrapper = categoryDataWrapper;
            _rows = rows;
        }

        public EplBaseCategoryDataWrapper getCategoryDataWrapper()
        {
            return _categoryDataWrapper;
        }

        public List<EplAssignmentRowWrapper> getRows()
        {
            return _rows;
        }
    }

    public static class TitleColumn extends IdentifiableWrapper
    {
        public TitleColumn(Long id, String title) { super(id, title); }

        public TitleColumnType getType() {
            return TitleColumnType.values()[(int) (long) getId()];
        }

        public static List<TitleColumn> getList(boolean hasYearPart, boolean otherLoad) {
            long i = -1;
            List<TitleColumn> columns = Arrays.asList(
                    otherLoad ? new TitleColumn(++i, "Вид нагрузки") : new TitleColumn(++i, "Название мероприятия"),
                    new TitleColumn(++i, "Параметры обучения (направление подготовки, формирующее подр., характеристики)"),
                    new TitleColumn(++i, "Курс"),
                    hasYearPart ? new TitleColumn(++i, "Часть года") : null,
                    otherLoad ? null : new TitleColumn(++i, "Потоков"),
                    otherLoad ? null : new TitleColumn(++i, "Групп"),
                    otherLoad ? null : new TitleColumn(++i, "Подгрупп"),
                    new TitleColumn(++i, "Студентов"),
                    otherLoad ? null : new TitleColumn(++i, "Группа")
            );
            return columns.stream().filter(item -> item != null).collect(Collectors.toList());
        }
    }

    public enum TitleColumnType
    {
        TITLE, EDU_PARAMS, COURSE, YEAR_PART, FLOW_COUNT, GROUP_COUNT, SUB_GROUP_COUNT, STUDENT_COUNT, GROUP
    }

    public static Comparator<YearDistributionPart> YEAR_PART_COMPARATOR = (p1, p2) -> {
        int result = (null == p1 || null == p2) ? 1 : 0;
        if (0 == result) result = Integer.compare(p1.getYearDistribution().getAmount(), p2.getYearDistribution().getAmount());
        if (0 == result) result = Integer.compare(p1.getNumber(), p2.getNumber());
        return result;
    };

    public static Comparator<EppGroupType> GROUP_TYPE_COMPARATOR = (t1, t2) -> Integer.compare(t1.getPriority(), t2.getPriority());

    public static Comparator<EplEduGroupTimeItem> EDU_GROUP_TIME_ITEM_COMPARATOR = (o1, o2) -> {
        YearDistributionPart yp1 = o1.getEduGroup().getYearPart();
        YearDistributionPart yp2 = o2.getEduGroup().getYearPart();

        int result = Integer.compare(yp1.getYearDistribution().getAmount(), yp2.getYearDistribution().getAmount());
        if (0 == result) result = Integer.compare(yp1.getNumber(), yp2.getNumber());
        return result;
    };
}
