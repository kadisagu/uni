package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.MaxLoadTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.AddEdit.EplSettingsPpsLoadAddEdit;
import ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.List.EplSettingsPpsLoadList;

import static ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.logic.EplSettingsPpsLoadLevels.SUMMARY;
import static ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.AddEdit.EplSettingsPpsLoadAddEdit.ENTITY;
import static ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.AddEdit.EplSettingsPpsLoadAddEdit.LEVEL;
import static ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.List.EplSettingsPpsLoadListUI.setStaffNumber;
import static ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.List.EplSettingsPpsLoadListUI.setStaffRate;

@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required = true)
})
public class EplStudentSummaryMaxLoadTabUI extends UIPresenter
{
    private EntityHolder<IEntity> holder = new EntityHolder<>();

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(EplSettingsPpsLoadList.PPS_LOAD_DS))
        {
            dataSource.put(LEVEL, SUMMARY.getValue());
            dataSource.put(ENTITY, getHolder().getId());
        }
    }

    public void onClickEplSettingsPpsLoadAdd()
    {
        _uiActivation.asRegionDialog(EplSettingsPpsLoadAddEdit.class)
                .parameter(ENTITY, holder.getId())
                .parameter(LEVEL, SUMMARY.getValue()).activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(EplSettingsPpsLoadAddEdit.class).parameter(PUBLISHER_ID, getListenerParameter()).parameter(LEVEL, SUMMARY.getValue()).activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public EntityHolder<IEntity> getHolder()
    {
        return holder;
    }

    public void setHolder(EntityHolder<IEntity> holder)
    {
        this.holder = holder;
    }

    public void onStaffRate()
    {
        setStaffRate(true, getListenerParameterAsLong());
    }

    public void offStaffRate()
    {
        setStaffRate(false, getListenerParameterAsLong());
    }
    public void onStaffNumber()
    {
        setStaffNumber(true, getListenerParameterAsLong());
    }
    public void offStaffNumber()
    {
        setStaffNumber(false, getListenerParameterAsLong());
    }
}
