package ru.tandemservice.uniepp_load.loadPlan.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.LongAsDoubleFormatter;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplPlannedPpsTypeCodes;
import ru.tandemservice.uniepp_load.loadPlan.entity.gen.EplPlannedPpsGen;
import ru.tandemservice.uniepp_load.util.UniEppLoadUtils;

/** @see ru.tandemservice.uniepp_load.loadPlan.entity.gen.EplPlannedPpsGen */
public class EplPlannedPps extends EplPlannedPpsGen implements ITitled
{
    public static final LongAsDoubleFormatter DEFAULT_RATE_FORMATTER = new LongAsDoubleFormatter(4, 2);

    @EntityDSLSupport(parts = EplPlannedPps.P_STAFF_RATE_AS_LONG)
    @Override
    public String getStaffRateAsString() {
        return getStaffRateAsLong() == null ? "" : DEFAULT_RATE_FORMATTER.format(getStaffRateAsLong());
    }

    @EntityDSLSupport(parts = EplPlannedPps.P_STAFF_RATE_AS_LONG)
    @Override
    public Double getStaffRateAsDouble()
    {
        return UniEppLoadUtils.staffRateWrap(getStaffRateAsLong());
    }

    public void setStaffRateAsDouble(Double staffRateAsDouble)
    {
        setStaffRateAsLong(UniEppLoadUtils.staffRateUnwrap(staffRateAsDouble));
    }

    @Override
    @EntityDSLSupport(parts = EplPlannedPps.P_MAX_TIME_AMOUNT_AS_LONG)
    public Double getMaxTimeAmount()
    {
        return UniEppLoadUtils.wrap(getMaxTimeAmountAsLong());
    }

    public void setMaxTimeAmount(Double value)
    {
        setMaxTimeAmountAsLong(UniEppLoadUtils.unwrap(value));
    }

    @EntityDSLSupport
    @Override
    public String getDisplayableTitle()
    {
        StringBuilder builder = new StringBuilder(getTitle());
        if(getPost() != null || (null != getStaffRateAsDouble() && getStaffRateAsDouble() != 1))
        {
            builder.append(" (");
            if(getPost() != null)
            {
                builder.append(getPost().getTitle());
                if (getStaffRateAsLong() != 10000)
                    builder.append(", ");
            }
            if(getStaffRateAsLong() != 10000)
                builder.append(getStaffRateAsString()).append(" ст.");
            builder.append(")");
        }

        return builder.toString();
    }

    @Override
    public String getTitle()
    {
        if (null == getType())
            return getClass().getSimpleName();

        switch (getType().getCode())
        {
            case EplPlannedPpsTypeCodes.STAFF: return null == getPpsEntry() ? null : getPpsEntry().getPerson().getFullFio();
            case EplPlannedPpsTypeCodes.VACANCY_AS_STAFF: return getVacancy();
            case EplPlannedPpsTypeCodes.TIMEWORKER: return null == getPpsEntry() ? null : getPpsEntry().getPerson().getFullFio();
            case EplPlannedPpsTypeCodes.VACANCY_AS_TIMEWORKER: return getVacancy();
            default: return "";
        }
    }

    public Boolean isTimeWorker()
    {
        String code = getType().getCode();

        switch (code)
        {
            case EplPlannedPpsTypeCodes.STAFF:
            case EplPlannedPpsTypeCodes.VACANCY_AS_STAFF: return false;
            case EplPlannedPpsTypeCodes.TIMEWORKER:
            case EplPlannedPpsTypeCodes.VACANCY_AS_TIMEWORKER: return true;
            default: return null;
        }
    }
}