/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCategoryWrapper;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Враппер вывода списка категорий для расчитанных и переведенных часов План. потока
 *
 * Враппер формирует подсписки в виде пар, где
 *      первый элемент - список категорий по часам План. потока (расчитанные и переведенные)
 *      второй элемент - список категорий только по переведенным часам
 *
 * @author Alexey Lopatin
 * @since 06.04.2016
 */
public class EplBaseCategoryDataWrapper
{
    /** Показывает кол-во уровней **/
    private int _categoryLevelCount;

    /** Используемые категории времени (учебной нагрузки) **/
    private PairKey<Set<EplTimeCategoryEduLoad>, Set<EplTimeCategoryEduLoad>> _usedCategories = PairKey.create(Sets.newHashSet(), Sets.newHashSet());

    /** Список врапперов категорий верхнего (1) уровня **/
    private PairKey<List<EplCategoryWrapper>, List<EplCategoryWrapper>> _topLevelCategories = PairKey.create(Lists.newArrayList(), Lists.newArrayList());

    /** Список врапперов категорий нижнего (1, 2 и 3) уровня - все вместе **/
    private PairKey<List<EplCategoryWrapper>, List<EplCategoryWrapper>> _bottomLevelCategories = PairKey.create(Lists.newArrayList(), Lists.newArrayList());

    /** Список врапперов категорий нижнего (2 и 3) уровня - вложенной структурой **/
    private PairKey<List<List<EplCategoryWrapper>>, List<List<EplCategoryWrapper>>> _subcategories = PairKey.create(Lists.newArrayList(), Lists.newArrayList());

    /** Мап враперов, где ключ - id категория времени; значение - враппер категории **/
    private PairKey<Map<Long, EplCategoryWrapper>, Map<Long, EplCategoryWrapper>> _categoryMap = PairKey.create(Maps.newHashMap(), Maps.newHashMap());

    public EplBaseCategoryDataWrapper() {}

    public int getCategoryLevelCount()
    {
        return _categoryLevelCount;
    }

    public void setCategoryLevelCount(int categoryLevelCount)
    {
        _categoryLevelCount = categoryLevelCount;
    }

    public void setUsedCategories(PairKey<Set<EplTimeCategoryEduLoad>, Set<EplTimeCategoryEduLoad>> usedCategories)
    {
        _usedCategories = usedCategories;
    }

    public Map<Long, EplCategoryWrapper> getCategoryMap()
    {
        return _categoryMap.getFirst();
    }

    public Map<Long, EplCategoryWrapper> getTransferCategoryMap()
    {
        return _categoryMap.getSecond();
    }

    public Set<EplTimeCategoryEduLoad> getUsedCategories()
    {
        return _usedCategories.getFirst();
    }

    public List<EplCategoryWrapper> getTopLevelCategories()
    {
        return _topLevelCategories.getFirst();
    }

    public List<EplCategoryWrapper> getBottomLevelCategories()
    {
        return _bottomLevelCategories.getFirst();
    }

    public List<List<EplCategoryWrapper>> getSubcategories()
    {
        return _subcategories.getFirst();
    }

    // transfer category

    public Set<EplTimeCategoryEduLoad> getTransferUsedCategories()
    {
        return _usedCategories.getSecond();
    }

    public List<EplCategoryWrapper> getTransferTopLevelCategories()
    {
        return _topLevelCategories.getSecond();
    }

    public List<EplCategoryWrapper> getTransferBottomLevelCategories()
    {
        return _bottomLevelCategories.getSecond();
    }

    public List<List<EplCategoryWrapper>> getTransferSubcategories()
    {
        return _subcategories.getSecond();
    }
}
