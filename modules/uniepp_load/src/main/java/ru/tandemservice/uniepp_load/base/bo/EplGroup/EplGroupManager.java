/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplGroup;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniepp_load.base.bo.EplGroup.logic.EplGroupDao;
import ru.tandemservice.uniepp_load.base.bo.EplGroup.logic.IEplGroupDao;

/**
 * @author nvankov
 * @since 3/3/15
 */
@Configuration
public class EplGroupManager extends BusinessObjectManager
{
    public static EplGroupManager instance()
    {
        return instance(EplGroupManager.class);
    }

    @Bean
    public IEplGroupDao dao() {
        return new EplGroupDao();
    }
}



    