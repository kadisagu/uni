/* $Id$ */
package ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.logic;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.core.common.INaturalId;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import org.tandemframework.shared.commonbase.utils.DQLSimple;
import org.tandemframework.shared.employeebase.base.bo.EmployeePost.EmployeePostManager;
import org.tandemframework.shared.employeebase.base.bo.EmployeePost.logic.IEmployeeAdditionalInfo;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.pps.IPpsEntryDao;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem;
import ru.tandemservice.uniepp_load.catalog.entity.EplPlannedPpsType;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplPlannedPpsTypeCodes;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem;
import ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.EplSettingsPpsLoadManager;
import ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad;
import ru.tandemservice.uniepp_load.util.UniEppLoadUtils;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author nvankov
 * @since 1/28/15
 */
public class EplPlannedPpsDao extends UniBaseDao implements IEplPlannedPpsDao
{
    @Override
    public void doAddAllPlanedPps(EplOrgUnitSummary ouSummary)
    {
        List<EplPlannedPps> plannedPpsList = Lists.newLinkedList();
        Map<PpsEntryByEmployeePost, List<EmployeePost>> ppsEntry2EmployeePostMap = Maps.newHashMap();

        EplPlannedPpsType typeStaff = getByCode(EplPlannedPpsType.class, EplPlannedPpsTypeCodes.STAFF);

        // находим потенциальных ППС
        new DQLSelectBuilder()
                .fromEntity(PpsEntryByEmployeePost.class, "pps_ep")
                .where(eq(property("pps_ep", PpsEntryByEmployeePost.orgUnit()), value(ouSummary.getOrgUnit())))
                .where(isNull(property("pps_ep", PpsEntryByEmployeePost.removalDate())))
                .where(notExists(
                        new DQLSelectBuilder().fromEntity(EplPlannedPps.class, "pp")
                                .where(eq(property("pp", EplPlannedPps.ppsEntry().id()), property("pps_ep.id")))
                                .where(eq(property("pp", EplPlannedPps.type().code()), value(EplPlannedPpsTypeCodes.STAFF)))
                                .where(eq(property("pp", EplPlannedPps.orgUnitSummary()), value(ouSummary)))
                                .buildQuery()
                ))
                .column(property("pps_ep"))
                .createStatement(getSession()).<PpsEntryByEmployeePost>list()
                .forEach(ppsEntry -> {
                    EplPlannedPps planPps = new EplPlannedPps();
                    planPps.setOrgUnitSummary(ouSummary);
                    planPps.setType(typeStaff);
                    planPps.setPpsEntry(ppsEntry);
                    plannedPpsList.add(planPps);
                });

        List<Long> ppsEntryIds = plannedPpsList.stream().map(pps -> pps.getPpsEntry().getId()).collect(Collectors.toList());

        getList(EmployeePost4PpsEntry.class, EmployeePost4PpsEntry.ppsEntry().id(), ppsEntryIds)
                .forEach(item -> SafeMap.safeGet(ppsEntry2EmployeePostMap, item.getPpsEntry(), ArrayList.class).add(item.getEmployeePost()));

        Map<PostBoundedWithQGandQL, EplSettingsPpsLoad> postMap = EplSettingsPpsLoadManager.instance().dao().getPostMap(ouSummary.getId());
        Map<Long, Long> employeePost2StaffRateMap = EmployeePostManager.instance().employeeInfo().getRateMap(
                ppsEntry2EmployeePostMap.values()
                        .stream()
                        .flatMap(Collection::stream)
                        .map(EmployeePost::getId)
                        .collect(Collectors.toList())
        );
        for (EplPlannedPps pps : plannedPpsList)
        {
            PpsEntryByEmployeePost teacher = (PpsEntryByEmployeePost) pps.getPpsEntry();
            EplSettingsPpsLoad setting = postMap.get(teacher.getPost());
            final EplSettingsPpsLoad toAll = postMap.get(null);
            if (setting == null || (toAll != null && toAll.getLevel() > setting.getLevel()))
                setting = toAll;

            // посчитываем ставку - как сумму ставок группы
            Double staffRateAsDouble = 0d;
            for (EmployeePost empPost : ppsEntry2EmployeePostMap.get(teacher))
                staffRateAsDouble += employeePost2StaffRateMap.getOrDefault(empPost.getId(), 0L) / (double) IEmployeeAdditionalInfo.MULTIPLIER;

            if (0 == staffRateAsDouble) staffRateAsDouble = 1.0;

            pps.setStaffRateAsDouble(staffRateAsDouble);


            double maxValue = 0d;
            if (setting != null)
            {
                maxValue = setting.getHoursAdditional();
                if (setting.isStaffRateFactor() && pps != null && pps.getStaffRateAsDouble() != null)
                    maxValue *= pps.getStaffRateAsDouble();
                if (setting.isStaffNumberFactor() && pps != null && pps.getPpsEntry() != null)
                    maxValue *= pps.getPpsEntry() == null ? 1 : getEmployeePost4PpsEntryCount(pps.getPpsEntry().getId());

            }
            pps.setMaxTimeAmount(maxValue);

            savePlannedPps(pps);
        }
    }

    @Override
    public void doAddAllTimeWorker(EplOrgUnitSummary ouSummary)
    {
        EplPlannedPpsType typeTimeWorker = getByCode(EplPlannedPpsType.class, EplPlannedPpsTypeCodes.TIMEWORKER);
        EducationYear eduYear = ouSummary.getStudentSummary().getEppYear().getEducationYear();

        // находим потенциальных ППС
        IPpsEntryDao.instance.get().getPpsEntryByTimeworkerBuilder("pps_tw", ouSummary.getOrgUnit(), eduYear)
                .where(notExists(
                        new DQLSelectBuilder().fromEntity(EplPlannedPps.class, "pp")
                                .where(eq(property("pp", EplPlannedPps.ppsEntry().id()), property("pps_tw.id")))
                                .where(eq(property("pp", EplPlannedPps.type().code()), value(EplPlannedPpsTypeCodes.TIMEWORKER)))
                                .where(eq(property("pp", EplPlannedPps.orgUnitSummary()), value(ouSummary)))
                                .buildQuery()
                ))
                .column(property("pps_tw"))
                .createStatement(getSession()).<PpsEntry>list()
                .forEach(ppsEntry -> {
                    EplPlannedPps planPps = new EplPlannedPps();
                    planPps.setOrgUnitSummary(ouSummary);
                    planPps.setType(typeTimeWorker);
                    planPps.setPpsEntry(ppsEntry);
                    planPps.setMaxTimeAmountAsLong(ppsEntry.getTimeAmountAsLong());
                    savePlannedPps(planPps);
                });
    }

    @Override
    public boolean existPersonPpsForOU(Person person, OrgUnit orgUnit)
    {
        return existsEntity(PpsEntry.class,
                PpsEntry.person().s(), person,
                PpsEntry.orgUnit().s(), orgUnit
        );
    }

    @Override
    public double getSummaryHours(Long orgUnitSummaryId)
    {
        Long result = new DQLSelectBuilder().fromEntity(EplTimeItem.class, "ti")
                .column(DQLFunctions.sum(property("ti", EplTimeItem.timeAmountAsLong())))
                .where(eq(property("ti", EplTimeItem.summary().id()), value(orgUnitSummaryId))).createStatement(getSession()).uniqueResult();
        return result != null ? UniEppLoadUtils.wrap(result) : 0;
    }

    @Override
    public double getSummaryDistribHours(Long orgUnitSummaryId)
    {
        Long result = new DQLSelectBuilder().fromEntity(EplTimeItem.class, "ti")
                .column(DQLFunctions.sum(property("ti", EplTimeItem.timeAmountAsLong())))
                .where(exists(new DQLSelectBuilder().fromEntity(EplPlannedPpsTimeItem.class, "ppti").where(eq(property("ppti", EplPlannedPpsTimeItem.timeItem()), property("ti"))).buildQuery()))
                .where(eq(property("ti", EplTimeItem.summary().id()), value(orgUnitSummaryId))).createStatement(getSession()).uniqueResult();
        return result != null ? UniEppLoadUtils.wrap(result) : 0;
    }

    @Override
    public double getDistributionHours(Long orgUnitSummaryId)
    {
        Long result = new DQLSelectBuilder().fromEntity(EplPlannedPpsTimeItem.class, "pti")
                .column(DQLFunctions.sum(property("pti", EplPlannedPpsTimeItem.timeAmountAsLong())))
                .where(eq(property("pti", EplPlannedPpsTimeItem.timeItem().summary().id()), value(orgUnitSummaryId))).createStatement(getSession()).uniqueResult();

        return result != null ? UniEppLoadUtils.wrap(result) : 0;
    }

    @Override
    public void doRefreshDistribution(Long orgUnitSummaryId)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EplTimeItem.class, "ti")
                .joinEntity("ti", DQLJoinType.left, EplPlannedPpsTimeItem.class, "ppti", eq(property("ppti", EplPlannedPpsTimeItem.timeItem()), property("ti")))
                .where(eq(property("ti", EplTimeItem.summary().id()), value(orgUnitSummaryId)));

        Map<EplTimeItem, List<EplPlannedPpsTimeItem>> timeItemListMap = Maps.newHashMap();

        for(Object[] obj : builder.createStatement(getSession()).<Object[]>list())
        {
            EplTimeItem timeItem = (EplTimeItem) obj[0];
            EplPlannedPpsTimeItem ppsTimeItem = (EplPlannedPpsTimeItem) obj[1];
            if(!timeItemListMap.containsKey(timeItem))
                timeItemListMap.put(timeItem, Lists.<EplPlannedPpsTimeItem>newArrayList());
            List<EplPlannedPpsTimeItem> ppsTimeItems = timeItemListMap.get(timeItem);
            if(ppsTimeItem != null)
            {
                ppsTimeItems.add(ppsTimeItem);
            }
        }

        for(Map.Entry<EplTimeItem, List<EplPlannedPpsTimeItem>> entry : timeItemListMap.entrySet())
        {
            EplTimeItem timeItem = entry.getKey();
            List<EplPlannedPpsTimeItem> ppsTimeItems = entry.getValue();
            if(timeItem instanceof EplEduGroupTimeItem && ((EplEduGroupTimeItem) timeItem).getEduGroup() == null && !ppsTimeItems.isEmpty())
            {
                new DQLDeleteBuilder(EplPlannedPpsTimeItem.class).where(in(property(EplPlannedPps.id()), CommonBaseEntityUtil.getIdList(ppsTimeItems)));
                ppsTimeItems.clear();
            }
            else if(ppsTimeItems.size() == 1)
            {
                EplPlannedPpsTimeItem ppsTimeItem = ppsTimeItems.get(0);
                ppsTimeItem.setTimeAmount(timeItem.getTimeAmount());
                update(ppsTimeItem);
            }
            else if(ppsTimeItems.size() > 1) // распределяем пропорционально
            {
                Double sumTimeAmount = 0.0d;
                for(EplPlannedPpsTimeItem ppsTimeItem : ppsTimeItems)
                {
                    sumTimeAmount += ppsTimeItem.getTimeAmount();
                }

                if(!UniEppUtils.eq(sumTimeAmount, timeItem.getTimeAmount()))
                {
                    Double itemTimeAmount = timeItem.getTimeAmount();

                    for (EplPlannedPpsTimeItem ppsTimeItem : ppsTimeItems)
                    {
                        Double timeAmount = itemTimeAmount * ppsTimeItem.getTimeAmount() / sumTimeAmount;
                        ppsTimeItem.setTimeAmount(timeAmount);
                        update(ppsTimeItem);
                    }
                }
            }
        }
    }

    @Override
    public void doUpdateTimeDistribution(EplOrgUnitSummary summary, List<ITimeData> time)
    {
        new MergeAction.SessionMergeAction<INaturalId, EplPlannedPpsTimeItem>() {
            @Override
            protected EplPlannedPpsTimeItem buildRow(EplPlannedPpsTimeItem source)
            {
                return source;
            }

            @Override
            protected INaturalId key(EplPlannedPpsTimeItem source)
            {
                return source.getNaturalId();
            }

            @Override
            protected void fill(EplPlannedPpsTimeItem target, EplPlannedPpsTimeItem source)
            {
                target.update(source);
            }
        }.merge(
            getList(EplPlannedPpsTimeItem.class, EplPlannedPpsTimeItem.pps().orgUnitSummary(), summary),
            time.stream().map(EplPlannedPpsTimeItem::new).collect(Collectors.toList()));
    }

    @Zlo("Проверка, подключен ли М Почасовики")
    @Override
    public boolean isTimeWorkerExists()
    {
        return null != EntityRuntime.getMeta(UniEppLoadUtils.UNI_SNPPS_REL);
    }

    @Override
    public void savePlannedPps(EplPlannedPps pps)
    {
        PpsEntry ppsEntry = pps.getPpsEntry();
        EplPlannedPpsType type = pps.getType();

        Preconditions.checkNotNull(type);
        Preconditions.checkNotNull(pps.getType());

        if (type.isStaff())
        {
            pps.setPost(((PpsEntryByEmployeePost) ppsEntry).getPost());
            pps.setVacancy(null);
        }
        else if (type.isVacancyAsStaff())
        {
            if (null != ppsEntry && !(ppsEntry instanceof PpsEntryByEmployeePost))
            {
                pps.setPpsEntry(null);
            }
        }
        else if (type.isTimeWorker())
        {
            pps.setPost(null);
            pps.setStaffRateAsLong(null);
            pps.setVacancy(null);
        }
        else if (type.isVacancyAsTimeWorker())
        {
            pps.setPost(null);
            pps.setStaffRateAsLong(null);

            if (null != ppsEntry && ppsEntry instanceof PpsEntryByEmployeePost)
            {
                pps.setPpsEntry(null);
            }
        }
        else
            throw new IllegalStateException("Unknown type of plannedPps.");

        saveOrUpdate(pps);
    }

    public Double hasWrongStaffRates(EplOrgUnitSummary summary)
    {
        List<EplPlannedPps> ppsList = getList(new DQLSelectBuilder().fromEntity(EplPlannedPps.class, "pps")
                .where(eq(property("pps", EplPlannedPps.orgUnitSummary().id()), value(summary.getId())))
                .where(in(property("pps", EplPlannedPps.type().code()), Arrays.asList(EplPlannedPpsTypeCodes.STAFF, EplPlannedPpsTypeCodes.VACANCY_AS_STAFF))));
        for (EplPlannedPps pps : ppsList)
        {
            Double rate = EplRateFormatter.invalidStaffRate(pps.getStaffRateAsDouble());
            if (null != rate) {
                return rate;
            }
        }
        return null;
    }


    public int getEmployeePost4PpsEntryCount(Long ppsEntryId)
    {
        return new DQLSimple<>(EmployeePost4PpsEntry.class)
                .where(EmployeePost4PpsEntry.ppsEntry().id(), ppsEntryId)
                .where(EmployeePost4PpsEntry.removalDate(), null)
                .count();
    }

    public int getEplPlannedPpsCount(Long summaryId)
    {
        return new DQLSimple<>(EplPlannedPps.class)
                .where(EplPlannedPps.orgUnitSummary().id(), summaryId)
                .where(EplPlannedPps.type().code(), Arrays.asList(EplPlannedPpsTypeCodes.STAFF, EplPlannedPpsTypeCodes.VACANCY_AS_STAFF))
                .count();
    }
}
