/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.TransferAffiliate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;

/**
 * @author nvankov
 * @since 2/25/15
 */
@Configuration
public class EplStudentSummaryTransferAffiliate extends BusinessComponentManager
{
    public static final String COURSE_DS = "courseDS";
    public static final String ORG_UNIT_DS = "orgUnitDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(COURSE_DS, courseDSHandler()))
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler courseDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Course.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long studentSummaryId = context.getNotNull("studentSummaryId");

                dql.where(DQLExpressions.exists(
                        new DQLSelectBuilder().fromEntity(EplStudent.class, "s")
                                .where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.group().course()), DQLExpressions.property("e")))
                                .where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.group().summary().id()), DQLExpressions.value(studentSummaryId)))
                                .buildQuery()
                ));
            }
        }
                .filter(Course.title())
                .order(Course.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long studentSummaryId = context.getNotNull("studentSummaryId");
                Long courseId = context.get("course");

                if(courseId == null)
                {
                    dql.where(DQLExpressions.eq(DQLExpressions.value(0), DQLExpressions.value(1)));
                    return;
                }

                dql.where(DQLExpressions.exists(
                        new DQLSelectBuilder().fromEntity(EplStudent.class, "s")
                                .where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.educationOrgUnit().territorialOrgUnit()), DQLExpressions.property("e")))
                                .where(DQLExpressions.isNotNull(DQLExpressions.property("s", EplStudent.educationOrgUnit().territorialOrgUnit().parent())))
                                .where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.group().summary().id()), DQLExpressions.value(studentSummaryId)))
                                .where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.group().course().id()), DQLExpressions.value(courseId)))
                                .buildQuery()
                ));
            }
        }
                .filter(OrgUnit.title())
                .order(OrgUnit.title());
    }
}



    