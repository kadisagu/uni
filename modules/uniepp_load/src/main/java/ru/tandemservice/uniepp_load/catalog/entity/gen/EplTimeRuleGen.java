package ru.tandemservice.uniepp_load.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRule;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Норма времени
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplTimeRuleGen extends EntityBase
 implements INaturalIdentifiable<EplTimeRuleGen>, org.tandemframework.common.catalog.entity.ICatalogItem, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.catalog.entity.EplTimeRule";
    public static final String ENTITY_NAME = "eplTimeRule";
    public static final int VERSION_HASH = -2072947447;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_CATALOG_CODE = "catalogCode";
    public static final String P_DESCRIPTION = "description";
    public static final String P_DISABLED_DATE = "disabledDate";
    public static final String P_ENABLED = "enabled";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _catalogCode;     // Код справочника
    private String _description;     // Описание
    private Date _disabledDate;     // Дата запрещения
    
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Код справочника. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCatalogCode()
    {
        return _catalogCode;
    }

    /**
     * @param catalogCode Код справочника. Свойство не может быть null.
     */
    public void setCatalogCode(String catalogCode)
    {
        dirty(_catalogCode, catalogCode);
        _catalogCode = catalogCode;
    }

    /**
     * @return Описание.
     */
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Описание.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Дата запрещения.
     */
    public Date getDisabledDate()
    {
        return _disabledDate;
    }

    /**
     * @param disabledDate Дата запрещения.
     */
    public void setDisabledDate(Date disabledDate)
    {
        dirty(_disabledDate, disabledDate);
        _disabledDate = disabledDate;
    }

    public boolean isEnabled()
    {
        return getDisabledDate()==null;
    }

    public void setEnabled(boolean enabled)
    {
        if( !isInitLazyInProgress() && isEnabled()!=enabled )
        {
            setDisabledDate(enabled ? null : new Date());
        }
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EplTimeRuleGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EplTimeRule)another).getCode());
                setCatalogCode(((EplTimeRule)another).getCatalogCode());
            }
            setDescription(((EplTimeRule)another).getDescription());
            setDisabledDate(((EplTimeRule)another).getDisabledDate());
            setEnabled(((EplTimeRule)another).isEnabled());
            setTitle(((EplTimeRule)another).getTitle());
        }
    }

    public INaturalId<EplTimeRuleGen> getNaturalId()
    {
        return new NaturalId(getCode(), getCatalogCode());
    }

    public static class NaturalId extends NaturalIdBase<EplTimeRuleGen>
    {
        private static final String PROXY_NAME = "EplTimeRuleNaturalProxy";

        private String _code;
        private String _catalogCode;

        public NaturalId()
        {}

        public NaturalId(String code, String catalogCode)
        {
            _code = code;
            _catalogCode = catalogCode;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getCatalogCode()
        {
            return _catalogCode;
        }

        public void setCatalogCode(String catalogCode)
        {
            _catalogCode = catalogCode;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EplTimeRuleGen.NaturalId) ) return false;

            EplTimeRuleGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            if( !equals(getCatalogCode(), that.getCatalogCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            result = hashCode(result, getCatalogCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            sb.append("/");
            sb.append(getCatalogCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplTimeRuleGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplTimeRule.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EplTimeRule is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "catalogCode":
                    return obj.getCatalogCode();
                case "description":
                    return obj.getDescription();
                case "disabledDate":
                    return obj.getDisabledDate();
                case "enabled":
                    return obj.isEnabled();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "catalogCode":
                    obj.setCatalogCode((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "disabledDate":
                    obj.setDisabledDate((Date) value);
                    return;
                case "enabled":
                    obj.setEnabled((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "catalogCode":
                        return true;
                case "description":
                        return true;
                case "disabledDate":
                        return true;
                case "enabled":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "catalogCode":
                    return true;
                case "description":
                    return true;
                case "disabledDate":
                    return true;
                case "enabled":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "catalogCode":
                    return String.class;
                case "description":
                    return String.class;
                case "disabledDate":
                    return Date.class;
                case "enabled":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplTimeRule> _dslPath = new Path<EplTimeRule>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplTimeRule");
    }
            

    /**
     * @return Системный код. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRule#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Код справочника. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRule#getCatalogCode()
     */
    public static PropertyPath<String> catalogCode()
    {
        return _dslPath.catalogCode();
    }

    /**
     * @return Описание.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRule#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Дата запрещения.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRule#getDisabledDate()
     */
    public static PropertyPath<Date> disabledDate()
    {
        return _dslPath.disabledDate();
    }

    /**
     * @return Используется. Свойство не может быть null.
     *
     * Это формула "case when disabledDate is null then true else false end".
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRule#isEnabled()
     */
    public static PropertyPath<Boolean> enabled()
    {
        return _dslPath.enabled();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRule#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EplTimeRule> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _catalogCode;
        private PropertyPath<String> _description;
        private PropertyPath<Date> _disabledDate;
        private PropertyPath<Boolean> _enabled;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRule#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EplTimeRuleGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Код справочника. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRule#getCatalogCode()
     */
        public PropertyPath<String> catalogCode()
        {
            if(_catalogCode == null )
                _catalogCode = new PropertyPath<String>(EplTimeRuleGen.P_CATALOG_CODE, this);
            return _catalogCode;
        }

    /**
     * @return Описание.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRule#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(EplTimeRuleGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Дата запрещения.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRule#getDisabledDate()
     */
        public PropertyPath<Date> disabledDate()
        {
            if(_disabledDate == null )
                _disabledDate = new PropertyPath<Date>(EplTimeRuleGen.P_DISABLED_DATE, this);
            return _disabledDate;
        }

    /**
     * @return Используется. Свойство не может быть null.
     *
     * Это формула "case when disabledDate is null then true else false end".
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRule#isEnabled()
     */
        public PropertyPath<Boolean> enabled()
        {
            if(_enabled == null )
                _enabled = new PropertyPath<Boolean>(EplTimeRuleGen.P_ENABLED, this);
            return _enabled;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRule#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EplTimeRuleGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EplTimeRule.class;
        }

        public String getEntityName()
        {
            return "eplTimeRule";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
