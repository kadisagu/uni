/* $Id$ */
package ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.OuSummaryTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.EplPlannedPpsManager;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.AddEdit.EplPlannedPpsAddEdit;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.AddEdit.EplPlannedPpsAddEditUI;
import ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.EplSettingsPpsLoadManager;

import java.util.HashMap;
import java.util.Map;

/**
 * @author nvankov
 * @since 1/27/15
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required = true)})
public class EplPlannedPpsOuSummaryTabUI extends UIPresenter
{
    public static final String PARAM_SUMMARY_ID = "eplOUSummaryId";

    private EntityHolder<EplOrgUnitSummary> _holder = new EntityHolder<>();
    private boolean _moduleExists;
    private Double _wrongStaffRate;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        _moduleExists = EplPlannedPpsManager.instance().dao().isTimeWorkerExists();
        checkStaffRate();
    }

    private void checkStaffRate()
    {
        _wrongStaffRate = EplPlannedPpsManager.instance().dao().hasWrongStaffRates(getSummary());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PARAM_SUMMARY_ID, getSummary().getId());
    }

    public void onClickAddPps()
    {
        _uiActivation.asRegionDialog(EplPlannedPpsAddEdit.class)
                .parameter(EplPlannedPpsAddEditUI.PARAM_OU_SUMMARY_ID, getSummary().getId())
                .parameter(EplPlannedPpsAddEditUI.PARAM_TIME_WORKER, false)
                .activate();
        checkStaffRate();
    }

    public void onClickAddPpsAll()
    {
        EplPlannedPpsManager.instance().dao().doAddAllPlanedPps(getSummary());
        checkStaffRate();
    }

    public void onClickEditPps()
    {
        _uiActivation.asRegionDialog(EplPlannedPpsAddEdit.class)
                .parameter(EplPlannedPpsAddEditUI.PARAM_OU_SUMMARY_ID, getSummary().getId())
                .parameter(EplPlannedPpsAddEditUI.PARAM_TIME_WORKER, false)
                .parameter(EplPlannedPpsAddEditUI.PARAM_PLANNED_PPS_ID, getListenerParameterAsLong())
                .activate();
    }


    public void onClickAddTimeWorker()
    {
        _uiActivation.asRegionDialog(EplPlannedPpsAddEdit.class)
                .parameter(EplPlannedPpsAddEditUI.PARAM_OU_SUMMARY_ID, getSummary().getId())
                .parameter(EplPlannedPpsAddEditUI.PARAM_TIME_WORKER, true)
                .activate();
        checkStaffRate();
    }

    public void onClickAddTimeWorkerAll()
    {
        EplPlannedPpsManager.instance().dao().doAddAllTimeWorker(getSummary());
        checkStaffRate();
    }

    public void onClickEditTimeWorker()
    {
        _uiActivation.asRegionDialog(EplPlannedPpsAddEdit.class)
                .parameter(EplPlannedPpsAddEditUI.PARAM_OU_SUMMARY_ID, getSummary().getId())
                .parameter(EplPlannedPpsAddEditUI.PARAM_TIME_WORKER, true)
                .parameter(EplPlannedPpsAddEditUI.PARAM_PLANNED_PPS_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        checkStaffRate();
    }

    public boolean isEditMode()
    {
        return getSummary().getState().getCode().equals(EppState.STATE_FORMATIVE);
    }

    public boolean isTimeWorkerVisible()
    {
        return isEditMode() && isModuleExists();
    }

    public boolean isDisabled()
    {
        return !isEditMode();
    }

    public boolean isHasWarnings() {
        return isNotFoundSettings() || isHasWrongStaffRates();
    }

    public boolean isNotFoundSettings()
    {
        return EplSettingsPpsLoadManager.instance().dao().isNotFoundSettingsPpsLoad(getSummary().getId()) || isEmptyPpsList();
    }

    public boolean isHasWrongStaffRates()
    {
        return _wrongStaffRate != null;
    }

    public String getWrongStaffRateMessage()
    {
        return _wrongStaffRate == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(_wrongStaffRate);
    }

    public boolean isModuleExists()
    {
        return _moduleExists;
    }

    // getters and setters

    public EplOrgUnitSummary getSummary()
    {
        return getHolder().getValue();
    }

    public EntityHolder<EplOrgUnitSummary> getHolder()
    {
        return _holder;
    }

    public Map<String, Object> getOuSummaryLinkParams()
    {
        final HashMap<String, Object> params = new HashMap<>();
        params.put(UIPresenter.PUBLISHER_ID, getSummary().getId());
        params.put("selectedTab", "maxLoadTab");
        return params;
    }

    public Map<String, Object> getSummaryLinkParams()
    {
        final HashMap<String, Object> params = new HashMap<>();
        params.put(UIPresenter.PUBLISHER_ID, getSummary().getStudentSummary().getId());
        params.put("selectedTab", "maxLoadTab");
        return params;
    }

    public void setHolder(EntityHolder<EplOrgUnitSummary> holder)
    {
        _holder = holder;
    }

    public boolean isEmptyPpsList()
    {
        return EplPlannedPpsManager.instance().dao().getEplPlannedPpsCount(getSummary().getId()) < 1;
    }
}
