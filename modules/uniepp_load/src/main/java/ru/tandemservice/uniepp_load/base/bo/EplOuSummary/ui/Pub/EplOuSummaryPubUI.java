/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;

/**
 * @author oleyba
 * @since 5/5/12
 */
@State({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required = true),
    @Bind(key = "selectedTab", binding = "selectedTab")
})
public class EplOuSummaryPubUI extends UIPresenter
{
    private EntityHolder<EplOrgUnitSummary> holder = new EntityHolder<EplOrgUnitSummary>();
    private String selectedTab;

    // actions
    
    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
    }

    public EntityHolder<EplOrgUnitSummary> getHolder()
    {
        return holder;
    }

    public String getSelectedTab()
    {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        this.selectedTab = selectedTab;
    }

    public EplOrgUnitSummary getSummary()
    {
        return getHolder().getValue();
    }
}
