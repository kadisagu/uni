/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCommonRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplBaseTimeDataWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplCommonRowWrapperComparator;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.DistributionEditNew.EplPlannedPpsDistributionEditNew;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Alexey Lopatin
 * @since 28.04.2016
 */
public class EplTimeItemComboDSHandler extends EntityComboDataSourceHandler
{
    private boolean _withTransfer;
    private boolean _withSimpleTimeItem;

    public EplTimeItemComboDSHandler(String ownerId, boolean withTransfer, boolean withSimpleTimeItem)
    {
        super(ownerId, DataWrapper.class);
        _withTransfer = withTransfer;
        _withSimpleTimeItem = withSimpleTimeItem;
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EplOrgUnitSummary summary = context.get(EplPlannedPpsDistributionEditNew.BIND_ORG_UNIT_SUMMARY);
        List<EplCommonRowWrapper> resultList = new EplBaseTimeDataWrapper(summary, _withTransfer, _withSimpleTimeItem).fillSelect().getAllRows();

        Set keys = input.getPrimaryKeys();
        String filter = input.getComboFilterByValue();

        if (CollectionUtils.isNotEmpty(keys))
            resultList = resultList.stream()
                    .filter(g -> keys.contains(g.getFieldId()))
                    .collect(Collectors.toList());


        resultList = resultList.stream()
                .filter(row -> row.getTitle().toLowerCase().contains(filter.toLowerCase()))
                .distinct()
                .sorted(EplCommonRowWrapperComparator.INSTANCE)
                .collect(Collectors.toList());

        return ListOutputBuilder.get(input, resultList).pageable(false).build();
    }
}
