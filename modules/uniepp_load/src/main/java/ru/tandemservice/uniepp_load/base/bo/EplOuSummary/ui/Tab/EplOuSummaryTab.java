/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.Tab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.DistributionTab.EplPlannedPpsDistributionTab;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.OuSummaryTab.EplPlannedPpsOuSummaryTab;

/**
 * @author oleyba
 * @since 5/5/12
 */
@Configuration
public class EplOuSummaryTab extends BusinessComponentManager
{
}
