/* $Id: DAO.java 32113 2014-01-27 09:14:01Z hudson $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.LogTab;

import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWP2GTypeSlot;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot;

import java.util.Arrays;
import java.util.List;

/**
 * @author ekachanova
 */
public class DAO extends ru.tandemservice.uni.component.log.EntityLogViewBase.DAO
{
    private static final List<String> _entityClassNames = Arrays.asList(
        EplStudentSummary.ENTITY_CLASS,
        EplGroup.ENTITY_CLASS,
        EplStudent.ENTITY_CLASS,
        EplStudent2WorkPlan.ENTITY_CLASS,
        EplStudentWPSlot.ENTITY_CLASS,
        EplStudentWP2GTypeSlot.ENTITY_CLASS,
        EplEduGroup.ENTITY_CLASS,
        EplEduGroupRow.ENTITY_CLASS
    );

    @Override
    protected List<String> getEntityClassNames()
    {
        return _entityClassNames;
    }
}
