package ru.tandemservice.uniepp_load.catalog.entity;

import org.tandemframework.common.catalog.entity.IActiveCatalogItem;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uniepp_load.catalog.entity.gen.EplTimeRuleGen;

/** @see ru.tandemservice.uniepp_load.catalog.entity.gen.EplTimeRuleGen */
public abstract class EplTimeRule extends EplTimeRuleGen implements IDynamicCatalogItem, IActiveCatalogItem
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EplTimeRule.class)
                .where(EplTimeCategory.enabled(), Boolean.TRUE)
                .titleProperty(EplTimeCategory.title().s())
                .order(EplTimeCategory.title())
                .filter(EplTimeCategory.title());
    }

    @Override
    public String getUserCode()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setUserCode(String userCode)
    {
        throw new UnsupportedOperationException();
    }
}