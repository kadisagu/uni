package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_load_2x9x0_2to3 extends IndependentMigrationScript
{
	@Override
	public ScriptDependency[] getBoundaryDependencies()
	{
		return new ScriptDependency[]
			{
				new ScriptDependency("org.tandemframework", "1.6.18"),
				new ScriptDependency("org.tandemframework.shared", "1.9.0")
			};
	}

	@Override
	public void run(DBTool tool) throws Exception
	{
		////////////////////////////////////////////////////////////////////////////////
		// сущность eplPlannedPpsTimeItem

		tool.createColumn("epl_pl_pps_time_item_t", new DBColumn("timeamountaslong_p", DBType.LONG));
		tool.executeUpdate("update epl_pl_pps_time_item_t set timeamountaslong_p = (100 * timeamount_p)");
		tool.setColumnNullable("epl_pl_pps_time_item_t", "timeamountaslong_p", false);
		tool.dropColumn("epl_pl_pps_time_item_t", "timeamount_p");

		////////////////////////////////////////////////////////////////////////////////
		// сущность eplTimeItem

		tool.createColumn("epl_time_item_t", new DBColumn("timeamountaslong_p", DBType.LONG));
		tool.executeUpdate("update epl_time_item_t set timeamountaslong_p = (100 * timeamount_p)");
		tool.setColumnNullable("epl_time_item_t", "timeamountaslong_p", false);
		tool.dropColumn("epl_time_item_t", "timeamount_p");
	}
}