/* $Id: DataCorrectionExtManager.java 6852 2015-07-02 09:04:00Z oleyba $ */
package ru.tandemservice.uniepp_load.base.ext.DataCorrection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.DataCorrection.DataCorrectionManager;
import org.tandemframework.shared.commonbase.base.bo.DataCorrection.util.DataCorrectionAction;
import ru.tandemservice.uniepp_load.base.ext.DataCorrection.ui.Pub.DataCorrectionPubExt;

/**
 * @author nvankov
 * @since 5/21/14
 */
@Configuration
public class DataCorrectionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private DataCorrectionManager _dataCorrectionManager;

    @Bean
    public ItemListExtension<DataCorrectionAction> actionListExtension()
    {
        final IItemListExtensionBuilder<DataCorrectionAction> itemListExtensionBuilder = itemListExtension(_dataCorrectionManager.actionExtPoint());

        return itemListExtensionBuilder
            .add("eplSummaryMerge", new DataCorrectionAction("uniepp_load", "eplSummaryMerge", "onClickEplSummaryMerge", DataCorrectionPubExt.EPL_DATA_CORRECTION_PUB_ADDON_NAME))
            .create();
    }
}
