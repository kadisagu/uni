/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.SlotTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSettingsUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniepp_load.base.bo.EplState.EplStateManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.daemon.EplStudentSummaryDaemonBean;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EduGroupTab.EplStudentSummaryEduGroupTab;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.InfoBlock.EplStudentSummaryInfoBlockUI;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplStudentSummaryState;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTabUI;

import java.util.Date;

import static ru.tandemservice.uniepp_load.catalog.entity.codes.EplStudentSummaryStateCodes.ATTACH_WORK_PLAN;

/**
 * @author oleyba
 * @since 12/20/14
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "summaryHolder.id"),
    @Bind(key = EplOrgUnitTabUI.BIND_ORG_UNIT_ID, binding = "orgUnitHolder.id")
})
public class EplStudentSummarySlotTabUI extends UIPresenter
{
    private EntityHolder<EplStudentSummary> summaryHolder = new EntityHolder<EplStudentSummary>();
    private EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<>();

    private boolean summaryPub = true;
    private boolean _visibleDaemon;

    @Override
    public void onComponentRefresh()
    {
        getOrgUnitHolder().refresh();
        if (getOrgUnitHolder().getId() != null) {
            setSummaryPub(false);
            if (getOrgUnitHolder().getId().equals(getSummaryHolder().getId())) {
                getSummaryHolder().setId(null);
                getSummaryHolder().setValue(null);
            }
            getSettings().set(EducationCatalogsManager.PARAM_FORMATIVE_ORGUNIT, getOrgUnitHolder().getValue());
            getSummaryHolder().setId((Long) _uiSettings.get("summary." + getOrgUnitHolder().getId()));
            getSummaryHolder().refresh();
            saveSettings();
        }
        getSummaryHolder().refresh();
        if (null != getSummary())
        {
            EplStudentSummaryInfoBlockUI.componentActivate(_uiSupport, _uiActivation, getSummary().getId());
            EplStudentSummaryState state = getSummary().getState();
            _visibleDaemon = !EplStateManager.instance().dao().isCurrentStateMore(state, ATTACH_WORK_PLAN);
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EplStudentSummaryManager.PARAM_SUMMARY, getSummary());
        dataSource.put(EplStudentSummaryManager.PARAM_EPP_YEAR, getSummary() == null ? null : getSummary().getEppYear());
        dataSource.put(EplStudentSummaryManager.PARAM_YEAR_PART, getSettings().get(EplStudentSummaryManager.PARAM_YEAR_PART));
        dataSource.put(EplStudentSummaryManager.PARAM_GROUP_TYPE, getSettings().get(EplStudentSummaryManager.PARAM_GROUP_TYPE));
        dataSource.put(EplStudentSummaryManager.PARAM_SPLIT_VARIANT, getSettings().get(EplStudentSummaryManager.PARAM_SPLIT_VARIANT));
        dataSource.put(EplStudentSummaryManager.PARAM_SPLIT_ELEMENT, getSettings().get(EplStudentSummaryManager.PARAM_SPLIT_ELEMENT));
        dataSource.put(EplStudentSummaryManager.PARAM_SETTINGS, getSettings());
        if (EplStudentSummaryEduGroupTab.DS_EDU_HS.equals(dataSource.getName())) {
            dataSource.putAll(getSettings().getAsMap(EducationCatalogsManager.PARAM_FORMATIVE_ORGUNIT));
        } else if ("groupDS".equals(dataSource.getName()))
        {
            dataSource.putAll(getSettings().getAsMap("formativeOrgUnit", "eduHS", "developForm", "course"));
        }
        else if (EplStudentSummarySlotTab.DS_SUMMARY.equals(dataSource.getName())) {
           dataSource.put(EplStudentSummaryManager.PARAM_FORMATIVE_ORG_UNIT, getOrgUnitHolder().getValue());
        }
    }

    public void onClickRefresh()
    {
        EplStudentSummaryDaemonBean.DAEMON.wakeUpDaemon();
        deactivate();
    }

    public void onChangeSummary()
    {
        if (null != getSummary())
            EplStudentSummaryInfoBlockUI.componentActivate(_uiSupport, _uiActivation, getSummary().getId());
        saveSettings();
        ContextLocal.getDesktop().setRefreshScheduled(true);
    }

    @Override
    public void saveSettings()
    {
        if (!isSummaryPub()) {
            getSettings().set("summary." + getOrgUnitHolder().getId(), getSummary() == null ? null : getSummary().getId());
        }
        super.saveSettings();
    }

    @Override
    public void clearSettings()
    {
        CommonBaseSettingsUtil.clearAndSaveSettingsExcept(getSettings(), EplStudentSummaryManager.PARAM_YEAR_PART, EplStudentSummaryManager.PARAM_GROUP_TYPE);
    }

    // presenter

    public String getDaemonStatus()
    {
        final Long date = EplStudentSummaryDaemonBean.DAEMON.getCompleteStatus();
        if (null != date) {
            return DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date(date));
        }
        return "обновляется";
    }

    public boolean isShowSummaryTab() {
        return getSummary() != null;
    }

    // getters and setters

    public EplStudentSummary getSummary()
    {
        return getSummaryHolder().getValue();
    }

    public void setSummary(EplStudentSummary summary) {
        getSummaryHolder().setValue(summary);
    }

    public EntityHolder<EplStudentSummary> getSummaryHolder()
    {
        return summaryHolder;
    }

    public boolean isVisibleDaemon()
    {
        return _visibleDaemon;
    }

    public EntityHolder<OrgUnit> getOrgUnitHolder()
    {
        return orgUnitHolder;
    }

    public boolean isSummaryPub()
    {
        return summaryPub;
    }

    public void setSummaryPub(boolean summaryPub)
    {
        this.summaryPub = summaryPub;
    }
}
