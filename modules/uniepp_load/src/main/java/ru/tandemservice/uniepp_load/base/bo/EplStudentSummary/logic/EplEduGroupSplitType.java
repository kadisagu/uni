/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic;

/**
 * @author Nikolay Fedorovskih
 * @since 29.11.2014
 */
public enum EplEduGroupSplitType
{
    BY_DISCIPLINE("По мероприятию реестра"),
    BY_EDUCATION_ORGUNIT("По параметрам обучения"),
    BY_COMPENSATION_SOURCE("По источнику возмещения затрат"),
    BY_GROUP("По планируемой академической группе"),
    BY_ROW("По планируемым МСРП"),
    BY_COUNT("По числу студентов"),
    BY_SPLIT_VARIANT("По способам деления потоков"),
    BY_SPLIT_ELEMENT("По элементам деления потоков"),
    BY_MAX_COUNT("Равномерно с ограничением по числу студентов");

    private final String _title;

    public String getTitle()
    {
        return _title;
    }

    EplEduGroupSplitType(String title)
    {
        _title = title;
    }
}
