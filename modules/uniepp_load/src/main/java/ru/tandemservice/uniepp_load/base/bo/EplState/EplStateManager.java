/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplState;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp_load.base.bo.EplState.logic.EplStateDao;
import ru.tandemservice.uniepp_load.base.bo.EplState.logic.IEplStateDao;
import ru.tandemservice.uniepp_load.catalog.entity.EplStudentSummaryState;

/**
 * @author Alexey Lopatin
 * @since 08.02.2016
 */
@Configuration
public class EplStateManager extends BusinessObjectManager
{
    public static final String EPL_STATE_DS = "eplStateDS";

    public static EplStateManager instance()
    {
        return BusinessObjectManager.instance(EplStateManager.class);
    }

    @Bean
    public UIDataSourceConfig eppStateDSConfig()
    {
        return SelectDSConfig.with(EPL_STATE_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(eplStateDSHandler())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eplStateDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EplStudentSummaryState.class)
                .order(EplStudentSummaryState.priority())
                .filter(EplStudentSummaryState.title());
    }

    @Bean
    public IEplStateDao dao()
    {
        return new EplStateDao();
    }
}
