/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.logic.EplIndividualPlanDao;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.logic.IEplIndividualPlanDao;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.logic.EplPpsEntryDSHandler;

/**
 * @author Alexey Lopatin
 * @since 01.08.2016
 */
@Configuration
public class EplIndividualPlanManager extends BusinessObjectManager
{
    public static final String PARAM_PPS_ACTIVE = "ppsActive";

    public static final String DS_ORG_UNIT = "orgUnitDS";
    public static final String DS_TEACHER = "teacherDS";
    public static final String DS_STUDENT_SUMMARY = "studentSummaryDS";

    public static EplIndividualPlanManager instance()
    {
        return instance(EplIndividualPlanManager.class);
    }

    @Bean
    public IEplIndividualPlanDao dao()
    {
        return new EplIndividualPlanDao();
    }

    @Bean
    public UIDataSourceConfig teacherDSConfig()
    {
        return SelectDSConfig.with(DS_TEACHER, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(teacherDSHandler())
                .addColumn(PpsEntry.fio().s())
                .addColumn(PpsEntry.orgUnit().shortTitle().s())
                .addColumn(PpsEntry.titlePostOrTimeWorkerData().s())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler teacherDSHandler()
    {
        return new EplPpsEntryDSHandler(getName());
    }

    public static String getPermissionKeyView(String key, boolean ppsActive)
    {
        return getPermissionKey(key, "view", ppsActive);
    }

    public static String getPermissionKey(String key, String postfix, boolean ppsActive)
    {
        return ppsActive ? "" : key + "_" + postfix;
    }
}
