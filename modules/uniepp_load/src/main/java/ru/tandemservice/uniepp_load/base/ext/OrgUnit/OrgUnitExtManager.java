/* $Id:$ */
package ru.tandemservice.uniepp_load.base.ext.OrgUnit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author oleyba
 * @since 12/6/14
 */
@Configuration
public class OrgUnitExtManager extends BusinessObjectExtensionManager
{
}
