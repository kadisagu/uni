/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupSplitTab;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentCount4SplitElement;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.gen.EplStudentCount4SplitElementGen;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Katkov
 * @since 24.03.2016
 */
public class EplStudentSummaryGroupSplitDSHandler extends DefaultSearchDataSourceHandler
{
    protected EplStudentSummaryGroupSplitDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    public DSOutput execute(DSInput input, ExecutionContext context)
    {
        String es2wp = "s2wp";
        String es4se = "es4se";
        String row = "wpRow";

        EplStudentSummary studentSummary = context.get(EplStudentSummaryManager.PARAM_SUMMARY);
        EppRegistryElement discipline = context.get(EplStudentSummaryGroupSplitTab.DISCIPLINE);
        EppEduGroupSplitVariant splitVariant = context.get(EplStudentSummaryManager.PARAM_SPLIT_VARIANT);
        List<EppEduGroupSplitBaseElement> paramSplitElements = context.get(EplStudentSummaryManager.PARAM_SPLIT_ELEMENT);
        boolean hasIncorrectSplit = Boolean.TRUE.equals(context.get(EplStudentSummaryGroupSplitTab.INCORRECT_SPLIT));
        boolean hasNoSplit = Boolean.TRUE.equals(context.get(EplStudentSummaryGroupSplitTab.NO_SPLIT));

        Set<PairKey<Long, Long>> missPairKey = Sets.newHashSet();

        if (hasIncorrectSplit)
        {
            EplStudentSummaryManager.instance().dao().getIncorrectSplitPairDql(studentSummary)
                    .createStatement(getSession()).<Object[]>list().stream()
                    .forEach(item -> missPairKey.add(PairKey.create((Long) item[0], (Long) item[1])));
        }
        if (hasNoSplit)
        {
            EplStudentSummaryManager.instance().dao().getSplitEmptyPairDql(studentSummary)
                    .createStatement(getSession()).<Object[]>list().stream()
                    .forEach(item -> missPairKey.add(PairKey.create((Long) item[0], (Long) item[1])));
        }

        DQLSelectBuilder dql = EplStudentSummaryManager.instance().dao().getWorkPlanAndRegElementUniquePairsDql(row, studentSummary);

        // сортировка + сортировка по колонкам сущности в проперти EppRegistryElement: Мероприятие реестра и Способ деления
        EntityOrder order = input.getEntityOrder();
        OrderDirection direction = order.getDirection();
        switch (order.getColumnName())
        {
            case EplStudentSummaryGroupSplitTab.DISCIPLINE:
                dql.order(property(row, EppWorkPlanRegistryElementRow.registryElementPart().registryElement().title()), direction);
                break;
            case EppRegistryElement.L_EDU_GROUP_SPLIT_VARIANT:
                dql.order(property(row, EppWorkPlanRegistryElementRow.registryElementPart().registryElement().eduGroupSplitVariant().title()), direction);
                break;
            default:
                dql.order(property(es2wp, order.getKeyString()), direction);
                break;
        }

        //Фильтрация по дисциплине и делению потоков
        if (discipline != null) {
            dql.where(eq(property(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id().fromAlias(row)), value(discipline.getId())));
        }
        if (splitVariant != null) {
            dql.where(in(property(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().eduGroupSplitVariant().fromAlias(row)), value(splitVariant)));
        }

        List<Object[]> uniquePairList = dql.createStatement(getSession()).list();

        // формируем уникальную пару сочитания РУП акад группы и дисциплины
        List<PairKey<Long, Long>> uniquePairIdList = uniquePairList.stream()
                .map(item -> PairKey.create(((EplStudent2WorkPlan)item[0]).getId(), ((EppRegistryElement)item[1]).getId()))
                .collect(Collectors.toList());

        //собираем элементы деления
        Map<PairKey, List<EplStudentCount4SplitElement>> eplStudent2WorkPlanMap = Maps.newHashMap();

        new DQLSelectBuilder()
                .fromEntity(EplStudentCount4SplitElement.class, es4se)
                .where(in(property(EplStudentCount4SplitElement.student2WorkPlan().id().fromAlias(es4se)), uniquePairIdList.stream().map(PairKey::getFirst).collect(Collectors.toList())))
                .createStatement(getSession()).<EplStudentCount4SplitElement>list()
                .stream().forEach(o -> SafeMap.safeGet(eplStudent2WorkPlanMap, PairKey.create(o.getStudent2WorkPlan().getId(), o.getRegElement().getId()), ArrayList.class).add(o));

        //Начинаем формировать выходные данные
        List<DataWrapper> result = Lists.newArrayList();
        for (Object item[] : uniquePairList)
        {
            if (missPairKey.isEmpty() && (hasIncorrectSplit || hasNoSplit)) continue;

            EplStudent2WorkPlan s2wp = (EplStudent2WorkPlan) item[0];
            EppRegistryElement regElement = (EppRegistryElement) item[1];
            PairKey pairKey = PairKey.create(s2wp.getId(), regElement.getId());

            if (!missPairKey.isEmpty() && !missPairKey.contains(pairKey)) continue;

            List<EplStudentCount4SplitElement> splitElements = eplStudent2WorkPlanMap.get(pairKey);
            long splitVariantCount = 0;

            // фильтрация по элементам деления
            if (!CollectionUtils.isEmpty(paramSplitElements))
            {
                if (null == splitElements) continue;

                Set<EppEduGroupSplitBaseElement> splitBaseElementList = splitElements.stream().map(EplStudentCount4SplitElement::getSplitElement).collect(Collectors.toSet());
                if (Collections.disjoint(splitBaseElementList, paramSplitElements)) continue;
            }
            if (null != splitElements)
            {
                splitVariantCount = splitElements.stream()
                        .filter(o -> !o.getSplitElement().getSplitVariant().equals(regElement.getEduGroupSplitVariant()))
                        .count();
            }

            DataWrapper dw = new DataWrapper(s2wp);
            dw.setId((long) System.identityHashCode(pairKey));
            dw.setProperty(EplStudentSummaryGroupSplitTab.ENABLING, splitVariantCount == 0 && regElement.getEduGroupSplitVariant() != null);
            dw.setProperty(EplStudentSummaryGroupSplitTab.DISCIPLINE, regElement);
            dw.setProperty(EplStudentSummaryGroupSplitTab.GROUP_SPLIT_ELEMENT, eplStudent2WorkPlanMap.getOrDefault(pairKey, new ArrayList<>()).stream()
                    .sorted((o1, o2) -> o1.getSplitElement().getSplitElementShortTitle().toLowerCase().compareTo(o2.getSplitElement().getSplitElementShortTitle().toLowerCase()))
                    .map(o -> o.getStudentCount() + " - " + o.getSplitElement().getSplitElementShortTitle() + System.lineSeparator())
                    .collect(Collectors.toList()));
            dw.setProperty(EplStudentSummaryGroupSplitTab.SPLIT_ELEMENTS_ID, eplStudent2WorkPlanMap.getOrDefault(pairKey, new ArrayList<>()).stream().map(EntityBase::getId).collect(Collectors.toList()));
            dw.setProperty(EplStudentSummaryGroupSplitTab.GROUP_SPLIT_ELEMENT_COUNT, eplStudent2WorkPlanMap.getOrDefault(pairKey, new ArrayList<>()).stream().mapToInt(EplStudentCount4SplitElementGen::getStudentCount).sum());

            result.add(dw);
        }
        return ListOutputBuilder.get(input, result).pageable(true).build();
    }
}