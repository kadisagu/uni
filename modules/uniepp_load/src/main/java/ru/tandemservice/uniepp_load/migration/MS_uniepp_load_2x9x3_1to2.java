package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_load_2x9x3_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
		        new ScriptDependency("org.tandemframework", "1.6.18"),
		        new ScriptDependency("org.tandemframework.shared", "1.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность eplStudentSummaryState

        // создана новая сущность
        short entityCode;
        {
            // создать таблицу
            DBTable dbt = new DBTable("epl_student_summary_state_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eplstudentsummarystate"),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("priority_p", DBType.INTEGER).setNullable(false),
				new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            entityCode = tool.entityCodes().ensure("eplStudentSummaryState");
        }

        PreparedStatement insert = tool.prepareStatement("insert into epl_student_summary_state_t (id, discriminator, code_p, priority_p, title_p) values (?, ?, ?, ?, ?)");
        String[] titles = {"Планирование контингента", "Привязка РУП", "Планирование потоков", "Расчет часов", "Расчет согласован"};

        for (int i = 0; i < titles.length; i++)
        {
            int idx = i + 1;
            insert.setLong(1, EntityIDGenerator.generateNewId(entityCode));
            insert.setShort(2, entityCode);
            insert.setString(3, String.valueOf(idx));
            insert.setInt(4, idx);
            insert.setString(5, titles[i]);
            insert.executeUpdate();
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eplStudentSummary

        // создано обязательное свойство state
        {
            // создать колонку
            tool.createColumn("epl_student_summary_t", new DBColumn("state_id", DBType.LONG));

            // задать значение по умолчанию
            // выставляем всем сводкам статус «Планирование контингента»
            Number contingentPlanningId = (Number) tool.getUniqueResult("select id from epl_student_summary_state_t where code_p = ?", "1");
            tool.executeUpdate("update epl_student_summary_t set state_id=? where state_id is null", contingentPlanningId);

            // сделать колонку NOT NULL
            tool.setColumnNullable("epl_student_summary_t", "state_id", false);
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eplTimeItem

        // создано обязательное свойство controlTimeAmountAsLong
        {
            // создать колонку
            tool.createColumn("epl_time_item_t", new DBColumn("controltimeamountaslong_p", DBType.LONG));

            // задать значение по умолчанию
            tool.executeUpdate("update epl_time_item_t set controltimeamountaslong_p=? where controltimeamountaslong_p is null", 0);

            // сделать колонку NOT NULL
            tool.setColumnNullable("epl_time_item_t", "controltimeamountaslong_p", false);
        }

        // удаляем все рассчитанные часы с отрицательным числом часов

        String subQuery = "select id from epl_time_item_t where timeamountaslong_p < 0";

        tool.executeUpdate("delete from epl_t_ou_edu_group_time_item_t where id in (" + subQuery + ")");
        tool.executeUpdate("delete from epl_edu_group_time_item_t where id in (" + subQuery + ")");
        tool.executeUpdate("delete from epl_simple_time_item_t where id in (" + subQuery + ")");

        tool.executeUpdate("delete from epl_time_item_t where timeamountaslong_p < 0");
    }
}