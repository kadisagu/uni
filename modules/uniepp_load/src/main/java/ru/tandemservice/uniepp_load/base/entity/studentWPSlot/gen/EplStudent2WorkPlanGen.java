package ru.tandemservice.uniepp_load.base.entity.studentWPSlot.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * РУП план. студентов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplStudent2WorkPlanGen extends EntityBase
 implements INaturalIdentifiable<EplStudent2WorkPlanGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan";
    public static final String ENTITY_NAME = "eplStudent2WorkPlan";
    public static final int VERSION_HASH = 1108863767;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String L_WORK_PLAN = "workPlan";
    public static final String P_COUNT = "count";
    public static final String L_CACHED_GRID_TERM = "cachedGridTerm";

    private EplStudent _student;     // План. студент
    private EppWorkPlanBase _workPlan;     // РУП
    private int _count;     // Число студентов
    private DevelopGridTerm _cachedGridTerm;     // Семестр сетки (кэш для запросов)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return План. студент. Свойство не может быть null.
     */
    @NotNull
    public EplStudent getStudent()
    {
        return _student;
    }

    /**
     * @param student План. студент. Свойство не может быть null.
     */
    public void setStudent(EplStudent student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return РУП. Свойство не может быть null.
     */
    @NotNull
    public EppWorkPlanBase getWorkPlan()
    {
        return _workPlan;
    }

    /**
     * @param workPlan РУП. Свойство не может быть null.
     */
    public void setWorkPlan(EppWorkPlanBase workPlan)
    {
        dirty(_workPlan, workPlan);
        _workPlan = workPlan;
    }

    /**
     * @return Число студентов. Свойство не может быть null.
     */
    @NotNull
    public int getCount()
    {
        return _count;
    }

    /**
     * @param count Число студентов. Свойство не может быть null.
     */
    public void setCount(int count)
    {
        dirty(_count, count);
        _count = count;
    }

    /**
     * Семестр сетки (кэш для запросов)
     *
     * @return Семестр сетки (кэш для запросов). Свойство не может быть null.
     */
    @NotNull
    public DevelopGridTerm getCachedGridTerm()
    {
        return _cachedGridTerm;
    }

    /**
     * @param cachedGridTerm Семестр сетки (кэш для запросов). Свойство не может быть null.
     */
    public void setCachedGridTerm(DevelopGridTerm cachedGridTerm)
    {
        dirty(_cachedGridTerm, cachedGridTerm);
        _cachedGridTerm = cachedGridTerm;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EplStudent2WorkPlanGen)
        {
            if (withNaturalIdProperties)
            {
                setStudent(((EplStudent2WorkPlan)another).getStudent());
                setWorkPlan(((EplStudent2WorkPlan)another).getWorkPlan());
            }
            setCount(((EplStudent2WorkPlan)another).getCount());
            setCachedGridTerm(((EplStudent2WorkPlan)another).getCachedGridTerm());
        }
    }

    public INaturalId<EplStudent2WorkPlanGen> getNaturalId()
    {
        return new NaturalId(getStudent(), getWorkPlan());
    }

    public static class NaturalId extends NaturalIdBase<EplStudent2WorkPlanGen>
    {
        private static final String PROXY_NAME = "EplStudent2WorkPlanNaturalProxy";

        private Long _student;
        private Long _workPlan;

        public NaturalId()
        {}

        public NaturalId(EplStudent student, EppWorkPlanBase workPlan)
        {
            _student = ((IEntity) student).getId();
            _workPlan = ((IEntity) workPlan).getId();
        }

        public Long getStudent()
        {
            return _student;
        }

        public void setStudent(Long student)
        {
            _student = student;
        }

        public Long getWorkPlan()
        {
            return _workPlan;
        }

        public void setWorkPlan(Long workPlan)
        {
            _workPlan = workPlan;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EplStudent2WorkPlanGen.NaturalId) ) return false;

            EplStudent2WorkPlanGen.NaturalId that = (NaturalId) o;

            if( !equals(getStudent(), that.getStudent()) ) return false;
            if( !equals(getWorkPlan(), that.getWorkPlan()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStudent());
            result = hashCode(result, getWorkPlan());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStudent());
            sb.append("/");
            sb.append(getWorkPlan());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplStudent2WorkPlanGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplStudent2WorkPlan.class;
        }

        public T newInstance()
        {
            return (T) new EplStudent2WorkPlan();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "workPlan":
                    return obj.getWorkPlan();
                case "count":
                    return obj.getCount();
                case "cachedGridTerm":
                    return obj.getCachedGridTerm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((EplStudent) value);
                    return;
                case "workPlan":
                    obj.setWorkPlan((EppWorkPlanBase) value);
                    return;
                case "count":
                    obj.setCount((Integer) value);
                    return;
                case "cachedGridTerm":
                    obj.setCachedGridTerm((DevelopGridTerm) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "workPlan":
                        return true;
                case "count":
                        return true;
                case "cachedGridTerm":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "workPlan":
                    return true;
                case "count":
                    return true;
                case "cachedGridTerm":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return EplStudent.class;
                case "workPlan":
                    return EppWorkPlanBase.class;
                case "count":
                    return Integer.class;
                case "cachedGridTerm":
                    return DevelopGridTerm.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplStudent2WorkPlan> _dslPath = new Path<EplStudent2WorkPlan>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplStudent2WorkPlan");
    }
            

    /**
     * @return План. студент. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan#getStudent()
     */
    public static EplStudent.Path<EplStudent> student()
    {
        return _dslPath.student();
    }

    /**
     * @return РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan#getWorkPlan()
     */
    public static EppWorkPlanBase.Path<EppWorkPlanBase> workPlan()
    {
        return _dslPath.workPlan();
    }

    /**
     * @return Число студентов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan#getCount()
     */
    public static PropertyPath<Integer> count()
    {
        return _dslPath.count();
    }

    /**
     * Семестр сетки (кэш для запросов)
     *
     * @return Семестр сетки (кэш для запросов). Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan#getCachedGridTerm()
     */
    public static DevelopGridTerm.Path<DevelopGridTerm> cachedGridTerm()
    {
        return _dslPath.cachedGridTerm();
    }

    public static class Path<E extends EplStudent2WorkPlan> extends EntityPath<E>
    {
        private EplStudent.Path<EplStudent> _student;
        private EppWorkPlanBase.Path<EppWorkPlanBase> _workPlan;
        private PropertyPath<Integer> _count;
        private DevelopGridTerm.Path<DevelopGridTerm> _cachedGridTerm;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return План. студент. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan#getStudent()
     */
        public EplStudent.Path<EplStudent> student()
        {
            if(_student == null )
                _student = new EplStudent.Path<EplStudent>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan#getWorkPlan()
     */
        public EppWorkPlanBase.Path<EppWorkPlanBase> workPlan()
        {
            if(_workPlan == null )
                _workPlan = new EppWorkPlanBase.Path<EppWorkPlanBase>(L_WORK_PLAN, this);
            return _workPlan;
        }

    /**
     * @return Число студентов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan#getCount()
     */
        public PropertyPath<Integer> count()
        {
            if(_count == null )
                _count = new PropertyPath<Integer>(EplStudent2WorkPlanGen.P_COUNT, this);
            return _count;
        }

    /**
     * Семестр сетки (кэш для запросов)
     *
     * @return Семестр сетки (кэш для запросов). Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan#getCachedGridTerm()
     */
        public DevelopGridTerm.Path<DevelopGridTerm> cachedGridTerm()
        {
            if(_cachedGridTerm == null )
                _cachedGridTerm = new DevelopGridTerm.Path<DevelopGridTerm>(L_CACHED_GRID_TERM, this);
            return _cachedGridTerm;
        }

        public Class getEntityClass()
        {
            return EplStudent2WorkPlan.class;
        }

        public String getEntityName()
        {
            return "eplStudent2WorkPlan";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
