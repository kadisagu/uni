package ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.AddEdit.EplSettingsPpsLoadAddEdit;
import ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.EditMax.EplSettingsPpsLoadEditMax;
import ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad;
import ru.tandemservice.uniepp_load.util.EplDefines;

import static ru.tandemservice.uniepp_load.util.EplDefines.getMaxLoadSetting;

/**
 * @author Alexey Lopatin
 * @since 18.05.2015
 */
public class EplSettingsPpsLoadListUI extends UIPresenter
{
    public static final String PARAM_EDU_YEAR = "eduYear";

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(EplSettingsPpsLoadList.PPS_LOAD_DS))
        {
            dataSource.put(PARAM_EDU_YEAR, getSettings().get(PARAM_EDU_YEAR));
        }
    }

    public void onClickEplSettingsPpsLoadAdd()
    {
        _uiActivation.asRegionDialog(EplSettingsPpsLoadAddEdit.class).activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(EplSettingsPpsLoadAddEdit.class).parameter(PUBLISHER_ID, getListenerParameter()).activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public String getMaxLoad()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getMaxLoadSetting());
    }

    public void onClickEditMaxLoad()
    {
        _uiActivation.asRegionDialog(EplSettingsPpsLoadEditMax.class).activate();
    }

    public void onStaffRate()
    {
        setStaffRate(true, getListenerParameterAsLong());
    }

    public void offStaffRate()
    {
        setStaffRate(false, getListenerParameterAsLong());
    }
    public void onStaffNumber()
    {
        setStaffNumber(true, getListenerParameterAsLong());
    }
    public void offStaffNumber()
    {
        setStaffNumber(false, getListenerParameterAsLong());
    }

    public static void setStaffRate(boolean value, Long id)
    {
        final ICommonDAO dao = DataAccessServices.dao();
        final EplSettingsPpsLoad settingsPpsLoad = dao.get(id);
        settingsPpsLoad.setStaffRateFactor(value);
        dao.save(settingsPpsLoad);
    }

    public static void setStaffNumber(boolean value, Long id)
    {
        final ICommonDAO dao = DataAccessServices.dao();
        final EplSettingsPpsLoad settingsPpsLoad = dao.get(id);
        settingsPpsLoad.setStaffNumberFactor(value);
        dao.save(settingsPpsLoad);
    }
}
