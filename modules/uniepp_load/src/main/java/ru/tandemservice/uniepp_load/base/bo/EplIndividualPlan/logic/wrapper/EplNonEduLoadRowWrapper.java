/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.logic.wrapper;

import com.google.common.collect.Lists;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uniepp_load.util.UniEppLoadUtils;

import java.util.Date;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 09.09.2016
 */
public class EplNonEduLoadRowWrapper
{
    private Long _id;
    private Integer _number;
    private String _title;
    private String _paramName;
    private Double _paramValue;
    private long _timeAmountAsLong;
    private Date _dateFrom;
    private Date _dateTo;
    private String _yearPart;

    private List<EplNonEduLoadRowWrapper> _children = Lists.newLinkedList();
    private boolean _category;
    private boolean _total;

    public EplNonEduLoadRowWrapper(String title, boolean category, boolean total)
    {
        _title = title;
        _category = category;
        _total = total;
    }

    public String getDateFromStr() { return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()); }
    public String getDateToStr() { return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo()); }
    public double getTimeAmount() { return UniEppLoadUtils.wrap(getTimeAmountAsLong()); }

    public Long getId() { return _id; }
    public void setId(Long id) { _id = id; }
    public Integer getNumber() { return _number; }
    public void setNumber(Integer number) { _number = number; }
    public String getTitle() { return _title; }
    public void setTitle(String title) { _title = title; }
    public String getParamName() { return _paramName; }
    public void setParamName(String paramName) { _paramName = paramName; }
    public Double getParamValue() { return _paramValue; }
    public void setParamValue(Double paramValue) { _paramValue = paramValue; }
    public long getTimeAmountAsLong() { return _timeAmountAsLong; }
    public void setTimeAmountAsLong(long timeAmountAsLong) { _timeAmountAsLong = timeAmountAsLong; }
    public Date getDateFrom() { return _dateFrom; }
    public void setDateFrom(Date dateFrom) { _dateFrom = dateFrom; }
    public Date getDateTo() { return _dateTo; }
    public void setDateTo(Date dateTo) { _dateTo = dateTo; }
    public String getYearPart() { return _yearPart; }
    public void setYearPart(String yearPart) { _yearPart = yearPart; }
    public List<EplNonEduLoadRowWrapper> getChildren() { return _children; }
    public void setChildren(List<EplNonEduLoadRowWrapper> children) { _children = children; }
    public boolean isCategory() { return _category; }
    public void setCategory(boolean category) { _category = category; }
    public boolean isTotal() { return _total; }
    public void setTotal(boolean total) { _total = total; }
}
