/* $Id$ */
package ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.logic;

import com.google.common.collect.Maps;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps;
import ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad;

import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 20.05.2015
 */
public class EplSettingsPpsLoadDao extends UniBaseDao implements IEplSettingsPpsLoadDao
{
    public boolean isNotFoundSettingsPpsLoad(Long orgUnitSummaryId)
    {
        List<PostBoundedWithQGandQL> postList = CommonBaseUtil.getPropertiesList(DataAccessServices.dao().getList(EplPlannedPps.class, EplPlannedPps.orgUnitSummary().id(), orgUnitSummaryId), EplPlannedPps.L_POST);
        if (postList.isEmpty()) return false;

        Map<PostBoundedWithQGandQL, EplSettingsPpsLoad> settingMap = getPostMap(orgUnitSummaryId);
        if (settingMap.containsKey(null))
            return false;
        for (PostBoundedWithQGandQL post : postList)
        {
            if (settingMap.get(post) == null && settingMap.get(null) == null) return true;
        }
        return false;
    }

    public CoreCollectionUtils.Pair<String, Boolean> getTotalSummaryByPps(Map<PostBoundedWithQGandQL, EplSettingsPpsLoad> settingMap, EplPlannedPps plannedPps, Double ppsSummary)
    {
        if (null == ppsSummary)
            return new CoreCollectionUtils.Pair<>("", false);

        EplSettingsPpsLoad setting = settingMap.get(plannedPps.getPost());
        if (null == setting)
            setting = settingMap.get(null);

        boolean error = false;
        String ppsSummaryAsString = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(ppsSummary);
        Double staffRate = plannedPps.getStaffRateAsDouble();
        if (null != setting && null != staffRate)
        {
            double minStaffRate = setting.getHoursMin() * staffRate;
            double maxStaffRate = setting.getHoursMax() * staffRate;
            double additionStaffRate = setting.getHoursAdditional() * staffRate;

            if (ppsSummary < minStaffRate)
            {
                ppsSummaryAsString += " (меньше " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(minStaffRate) + ")";
                error = true;
            }
            else if (ppsSummary > maxStaffRate && ppsSummary <= maxStaffRate + additionStaffRate)
                ppsSummaryAsString += " (доп. почасовая: " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(ppsSummary - maxStaffRate) + ")";
            else if (ppsSummary > maxStaffRate + additionStaffRate)
            {
                ppsSummaryAsString += " (больше " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(maxStaffRate + additionStaffRate) + ")";
                error = true;
            }
        }
        return new CoreCollectionUtils.Pair<>(ppsSummaryAsString, error);
    }

    @Override
    public Map<PostBoundedWithQGandQL, EplSettingsPpsLoad> getPostMap(Long orgUnitSummaryId)
    {
        EplOrgUnitSummary summary = getNotNull(orgUnitSummaryId);
        EducationYear eduYear = summary.getStudentSummary().getEppYear().getEducationYear();

        Map<PostBoundedWithQGandQL, EplSettingsPpsLoad> settingMap = Maps.newHashMap();
        List<EplSettingsPpsLoad> settingList = new DQLSelectBuilder().fromEntity(EplSettingsPpsLoad.class, "s")
                .where(eq(property("s", EplSettingsPpsLoad.eduYear()), value(eduYear)))
                .where(or(eq(property("s", EplSettingsPpsLoad.level()), value(EplSettingsPpsLoadLevels.BASIC.getValue())), in(property("s", EplSettingsPpsLoad.entity()), new Object[] { summary.getStudentSummary().getId(), orgUnitSummaryId })))
                .order(EplSettingsPpsLoad.level().fromAlias("s").s())
                .createStatement(getSession()).list();

        settingList.forEach(s -> settingMap.put(s.getPost(), s));
        return settingMap;
    }
}
