/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.OuSummaryTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.EplOuSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.EplOuSummaryDSHandler;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.Add.EplOuSummaryAdd;
import ru.tandemservice.uniepp_load.base.bo.EplState.EplStateManager;
import ru.tandemservice.uniepp_load.base.bo.EplState.logic.EplStateDao;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.daemon.EplTimeItemDaemonBean;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.InfoBlock.EplStudentSummaryInfoBlockUI;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplStudentSummaryState;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplStudentSummaryStateCodes;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static ru.tandemservice.uniepp_load.catalog.entity.codes.EplStudentSummaryStateCodes.ACCEPT;
import static ru.tandemservice.uniepp_load.catalog.entity.codes.EplStudentSummaryStateCodes.TIME_ITEM;

/**
 * @author oleyba
 * @since 5/5/12
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required = true)
})
public class EplStudentSummaryOuSummaryTabUI extends UIPresenter
{
    private EntityHolder<EplStudentSummary> holder = new EntityHolder<>();
    private EplStateDao.EplStudentSummaryStateWrapper _stateWrapper;
    private boolean _disabled;

    @Override
    public void onComponentRefresh()
    {
        EplStudentSummaryInfoBlockUI.componentActivate(_uiSupport, _uiActivation, getSummary().getId());
        getHolder().refresh();
        _stateWrapper = EplStateManager.instance().dao().getSummaryStateTitle(getSummary(), TIME_ITEM, getState().isTimeItem());
        _disabled = EplStateManager.instance().dao().isCurrentStateMore(getState(), TIME_ITEM);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EplOrgUnitSummary.PARAM_STUDENT_SUMMARY, getSummary());
        dataSource.put(EplOuSummaryDSHandler.PARAM_ORG_UNIT, getSettings().get("tutorOu"));
    }

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        if (UIDefines.DIALOG_REGION_NAME.equals(childRegionName)) {
            Collection<IEntity> selected = ((BaseSearchListDataSource)getConfig().getDataSource(EplStudentSummaryOuSummaryTab.DS_OU_SUMMARY)).getOptionColumnSelectedObjects(EplStudentSummaryOuSummaryTab.CHECKBOX);
            selected.clear();
        }
    }

    public void onDeleteEntityFromList()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    public void onClickAddOrgUnit()
    {
        getActivationBuilder().asRegionDialog(EplOuSummaryAdd.class).parameter(UIPresenter.PUBLISHER_ID, getSummary().getId()).activate();
    }

    public void onClickAddAllMissing()
    {
        EplOuSummaryManager.instance().dao().createAllMissingSummaries(getSummary());
    }

    public void onClickRefresh()
    {
        EplTimeItemDaemonBean.DAEMON.wakeUpDaemon();
        deactivate();
    }

    public void onClickChangeState()
    {
        boolean allAccept = EplStateManager.instance().dao().isAllOuSummaryEqStatus(getSummary(), EppState.STATE_ACCEPTED, null);
        if (!allAccept && TIME_ITEM.equals(getSummary().getState().getCode()))
            throw new ApplicationException(getConfig().getProperty("ui.summary.accept.error"));

        try {
            EplStateManager.instance().dao().changeSummaryState(getSummary(), TIME_ITEM, ACCEPT);
        } finally {
            ContextLocal.getDesktop().setRefreshScheduled(true);
        }
    }

    // getters and setters

    public EplStudentSummary getSummary()
    {
        return getHolder().getValue();
    }

    public EntityHolder<EplStudentSummary> getHolder()
    {
        return holder;
    }

    public EplStudentSummaryState getState()
    {
        return getSummary().getState();
    }

    public EplStateDao.EplStudentSummaryStateWrapper getStateWrapper()
    {
        return _stateWrapper;
    }

    public boolean isDeleteDisabled()
    {
        if (_disabled) return true;
        EplOrgUnitSummary ouSummary = _uiConfig.getDataSource(EplStudentSummaryOuSummaryTab.DS_OU_SUMMARY).getCurrent();
        return !ouSummary.getState().getCode().equals(EppState.STATE_FORMATIVE);
    }

    public boolean isDisabled()
    {
        return _disabled;
    }

    public boolean isEditMode()
    {
        return !isDisabled();
    }

    public boolean isVisibleDaemon()
    {
        return !getSummary().getState().isAccept();
    }

    public String getDaemonStatus()
    {
        final Long date = EplTimeItemDaemonBean.DAEMON.getCompleteStatus();
        if (null != date)
            return DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date(date));
        return "обновляется";
    }

    public void onClickOrgUnitSummaryMassDelete(){
        BaseSearchListDataSource ds = getConfig().getDataSource(EplStudentSummaryOuSummaryTab.DS_OU_SUMMARY);
        CheckboxColumn checkboxColumn = (CheckboxColumn) ds.getLegacyDataSource().getColumn(EplStudentSummaryOuSummaryTab.CHECKBOX);
        List<Long> selectedIds = checkboxColumn.getSelectedObjects().stream().map(IIdentifiable::getId).collect(Collectors.toList());

        //проверка, что все расчеты в статусе "формируется"
        if (selectedIds.size() == 0) {
            throw new ApplicationException("Необходимо выбрать хотя бы один расчет.");
        } else if (!EplStateManager.instance().dao().isAllOuSummaryEqStatus(null, EppState.STATE_FORMATIVE, selectedIds)) {
            throw new ApplicationException("Удаление расчета возможно только в состоянии «Формируется».");
        }

        int itemsDeleted = EplStudentSummaryManager.instance().dao().doOrgUnitSummaryMassDelete(selectedIds);

        ContextLocal.getInfoCollector().add("Удалено расчетов: " + itemsDeleted + ".");
        checkboxColumn.getSelectedObjects().clear();
    }

    public boolean isOrgUnitSummaryMassDeleteEnable(){
        return !EplStateManager.instance().dao().isCurrentStateMore(getSummary().getState(), EplStudentSummaryStateCodes.TIME_ITEM);
    }
}

