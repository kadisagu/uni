/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.EduLoad;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.EplIndividualPlanManager;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.EplOuSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCategoryWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.assignment.EplAssignmentCommonWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.assignment.EplAssignmentRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplBaseCategoryDataWrapper;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Alexey Lopatin
 * @since 05.08.2016
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required = true),
        @Bind(key = EplIndividualPlanManager.PARAM_PPS_ACTIVE, binding = "ppsActive")
})
public class EplIndividualPlanEduLoadUI extends UIPresenter
{
    public static final String PERMISSION_KEY = "eplIndividualPlanEduLoadTab";

    private EntityHolder<EplIndividualPlan> _holder = new EntityHolder<>();
    private List<EplOrgUnitSummary> _ouSummaries;
    private EplOrgUnitSummary _ouSummary;
    private boolean _ppsActive;
    private boolean _hasErrors;

    private EplAssignmentCommonWrapper _dataWrapper;
    private Map<YearDistributionPart, EplAssignmentCommonWrapper.Data> _dataMap;

    private YearDistributionPart _currentYearPart;
    private EplAssignmentCommonWrapper.TitleColumn _currentTitleColumn;

    private EplCategoryWrapper _currentCategory;
    private List<EplCategoryWrapper> _currentSubcategories;
    private EplAssignmentRowWrapper _currentRow;

    private boolean _currentRowMainLoad;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        _ouSummaries = EplIndividualPlanManager.instance().dao().getOuSummaries(getIndPlan());
        _hasErrors = !EplOuSummaryManager.instance().dao().existsPlannedPpsTimeItemForPps(getIndPlan());
        if (!_hasErrors)
        {
            _dataWrapper = EplOuSummaryManager.instance().dao().getEplTimeDataWrapperForPps(getIndPlan());
            _dataMap = _dataWrapper.getYearPartToDataMap();
        }
    }

    public EplIndividualPlan getIndPlan()
    {
        return getHolder().getValue();
    }

    public List<EplOrgUnitSummary> getOuSummaries()
    {
        return _ouSummaries;
    }

    public EplOrgUnitSummary getOuSummary()
    {
        return _ouSummary;
    }

    public Set<YearDistributionPart> getYearParts()
    {
        return _dataWrapper.getYearParts();
    }

    public EplAssignmentCommonWrapper.Data getCurrentData()
    {
        return _dataMap.get(getCurrentYearPart());
    }

    public EplBaseCategoryDataWrapper getCategoryDataWrapper()
    {
        return getCurrentData().getCategoryDataWrapper();
    }

    public List<EplAssignmentCommonWrapper.TitleColumn> getTitleColumns()
    {
        return EplAssignmentCommonWrapper.TitleColumn.getList(getCurrentYearPart() == null, false);
    }

    public List<EplAssignmentCommonWrapper.TitleColumn> getOtherTitleColumns()
    {
        return EplAssignmentCommonWrapper.TitleColumn.getList(true, true);
    }

    public long getTableColumnCount()
    {
        return getCategoryDataWrapper().getUsedCategories().size() + getTitleColumns().size() + 2;
    }

    public int getCategoryLevelCount()
    {
        return getCategoryDataWrapper().getCategoryLevelCount() + 1;
    }

    public int getCategoryRowCount()
    {
        int count = getCurrentCategory().getRowCount(getCategoryLevelCount());
        return count == 1 ? count : --count;
    }

    public String getCurrentRowSum()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getCurrentRow().getTimeByCategorySum());
    }

    public String getCurrentCategoryValue()
    {
        Double value = getCurrentRow().getTimeByCategoryMap().get(getCurrentCategory().getCategory());
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(value);
    }

    public Integer getCurrentTitleColumnColSpan()
    {
        return getCurrentTitleColumn().getType().equals(EplAssignmentCommonWrapper.TitleColumnType.TITLE) ? 2 : 1;
    }

    public String getCurrentYearPartTitle()
    {
        _currentRowMainLoad = true;
        return null == getCurrentYearPart() ? "Практики и ГИА" : getCurrentYearPart().getTitle().substring(0, 1).toUpperCase() + getCurrentYearPart().getTitle().substring(1);
    }

    public String getOtherTitle()
    {
        _currentRowMainLoad = false;
        return "Другие виды нагрузки";
    }

    public String getCurrentRowStyle()
    {
        return getCurrentRow().isTotal() ? "font-weight: bold;" : "";
    }

    public boolean isCurrentRowMainLoad()
    {
        return _currentRowMainLoad;
    }

    public String getIndPlanEduLoadTabPermissionKey()
    {
        return EplIndividualPlanManager.getPermissionKeyView(PERMISSION_KEY, _ppsActive);
    }

    // getters and setters

    public EntityHolder<EplIndividualPlan> getHolder()
    {
        return _holder;
    }

    public boolean isPpsActive()
    {
        return _ppsActive;
    }

    public void setPpsActive(boolean ppsActive)
    {
        _ppsActive = ppsActive;
    }

    public boolean isHasErrors()
    {
        return _hasErrors;
    }

    public EplCategoryWrapper getCurrentCategory()
    {
        return _currentCategory;
    }

    public List<EplCategoryWrapper> getCurrentSubcategories()
    {
        return _currentSubcategories;
    }

    public YearDistributionPart getCurrentYearPart()
    {
        return _currentYearPart;
    }

    public EplAssignmentRowWrapper getCurrentRow()
    {
        return _currentRow;
    }

    public EplAssignmentCommonWrapper.TitleColumn getCurrentTitleColumn()
    {
        return _currentTitleColumn;
    }
}
