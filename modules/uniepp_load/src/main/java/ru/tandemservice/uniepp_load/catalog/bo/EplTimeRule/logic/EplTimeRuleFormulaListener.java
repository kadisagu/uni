package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.shared.commonbase.base.bo.DaemonQueue.DaemonQueueManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.daemon.EplTimeItemDaemonBean;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupFormula;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * Created by nsvetlov on 24.11.2016.
 */
public class EplTimeRuleFormulaListener implements IDSetEventListener
{
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.commitInsert, EplTimeRuleEduGroupFormula.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.commitUpdate, EplTimeRuleEduGroupFormula.class, this);
    }

    @Override
    public void onEvent(DSetEvent dSetEvent)
    {
        DaemonQueueManager.instance().addIds(new DQLSelectBuilder()
                .fromEntity(EplTimeRuleEduGroupFormula.class, "g")
                .column("g.id")
                .where(in(property("g.id"), dSetEvent.getMultitude().getInExpression()))
                .createStatement(dSetEvent.getContext())
                .list(), true);
        EplTimeItemDaemonBean.DAEMON.registerAfterCompleteWakeUp(dSetEvent);
    }
}
