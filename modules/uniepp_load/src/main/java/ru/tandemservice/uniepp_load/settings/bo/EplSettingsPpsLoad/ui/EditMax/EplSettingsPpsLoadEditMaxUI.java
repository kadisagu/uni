package ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.EditMax;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import ru.tandemservice.uniepp_load.util.EplDefines;

import static ru.tandemservice.uniepp_load.util.EplDefines.getMaxLoadSetting;

/**
 * Created by nsvetlov on 09.03.2017.
 */
public class EplSettingsPpsLoadEditMaxUI extends UIPresenter
{
    private Double _maxLoad;

    @Override
    public void onComponentRefresh()
    {
        setMaxLoad(getMaxLoadSetting());
    }

    public void onClickApply()
    {
        IDataSettings settings = getLoadSettings();
        settings.set(EplDefines.MAX_LOAD_SETING_NAME, getMaxLoad());
        DataSettingsFacade.saveSettings(settings);
        deactivate();
    }

    private IDataSettings getLoadSettings()
    {
        return DataSettingsFacade.getSettings("general", EplDefines.MAX_LOAD_SETTINGS_PREFIX);
    }

    public Double getMaxLoad()
    {
        return _maxLoad;
    }

    public void setMaxLoad(Double maxLoad)
    {
        this._maxLoad = maxLoad;
    }
}
