package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLQuery;
import org.tandemframework.hibsupport.dql.statement.AbstractDQLStatement;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.shared.commonbase.base.bo.DaemonQueue.DaemonQueueManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.daemon.EplTimeItemDaemonBean;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * Created by nsvetlov on 24.11.2016.
 */
public class EplTimeRuleScriptListener implements IDSetEventListener
{
    static HashSet<String> affectedProperties;
    static {
        affectedProperties = new HashSet<>();
        affectedProperties.add("userScript");
        affectedProperties.add("defaultScriptPath");
        affectedProperties.add("groupType");
        affectedProperties.add("regStructure");

        affectedProperties.add("category");
        affectedProperties.add("grouping");

        affectedProperties.add("code");
        affectedProperties.add("catalogCode");
        affectedProperties.add("description");
        affectedProperties.add("disabledDate");
        affectedProperties.add("title");
    }

    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.commitInsert, EplTimeRuleEduGroupScript.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.commitUpdate, EplTimeRuleEduGroupScript.class, this, affectedProperties);
    }

    @Override
    public void onEvent(DSetEvent dSetEvent)
    {
        DaemonQueueManager.instance().addIds(getIdsByInExpression(dSetEvent), true);
        EplTimeItemDaemonBean.DAEMON.registerAfterCompleteWakeUp(dSetEvent);
    }

    public static List<Long> getIdsByInExpression(DSetEvent dSetEvent)
    {
        return dSetEvent.getMultitude().isSingular() ? Arrays.asList(new Long(dSetEvent.getMultitude().getInExpression().getText()))
                : AbstractDQLStatement.createStatement(dSetEvent.getContext(), (IDQLQuery) dSetEvent.getMultitude().getInExpression()).list();
    }
}
