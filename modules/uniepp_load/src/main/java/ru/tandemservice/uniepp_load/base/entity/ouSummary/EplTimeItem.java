package ru.tandemservice.uniepp_load.base.entity.ouSummary;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.EplTimeItemComboDSHandler;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.gen.EplTimeItemGen;
import ru.tandemservice.uniepp_load.util.UniEppLoadUtils;

/**
 * Часы нагрузки
 */
public abstract class EplTimeItem extends EplTimeItemGen
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EplTimeItemComboDSHandler(ownerId, true, true);
    }

    public static EntityComboDataSourceHandler defaultSelectWithoutTransferDSHandler(String ownerId)
    {
        return new EplTimeItemComboDSHandler(ownerId, false, true);
    }

    public static EntityComboDataSourceHandler defaultSelectWithoutSimpleDSHandler(String ownerId)
    {
        return new EplTimeItemComboDSHandler(ownerId, true, false);
    }

    public double getTimeAmount()
    {
        return UniEppLoadUtils.wrap(getTimeAmountAsLong());
    }

    public void setTimeAmount(double timeAmount)
    {
        setTimeAmountAsLong(UniEppLoadUtils.unwrap(timeAmount));
    }

    public double getControlTimeAmount()
    {
        return UniEppLoadUtils.wrap(getControlTimeAmountAsLong());
    }

    public void setControlTimeAmount(double timeAmount)
    {
        setControlTimeAmountAsLong(UniEppLoadUtils.unwrap(timeAmount));
    }

    @EntityDSLSupport
    public String getTimeAmountStr() {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getTimeAmount());
    }
}