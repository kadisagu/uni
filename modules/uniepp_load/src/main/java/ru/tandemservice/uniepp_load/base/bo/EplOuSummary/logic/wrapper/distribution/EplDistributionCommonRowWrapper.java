/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.distribution;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.ObjectUtils;
import org.tandemframework.caf.ui.support.IUISettings;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCommonRowWrapper;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps;
import ru.tandemservice.uniepp_load.loadPlan.util.PpsTimeStatsUtil;
import ru.tandemservice.uniepp_load.util.UniEppLoadUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.DistributionEditNew.EplPlannedPpsDistributionEditNew.*;

/**
 * Базовый враппер строки (для форм распределения нагрузки по ППС)
 *
 * @author Alexey Lopatin
 * @since 05.04.2016
 */
public abstract class EplDistributionCommonRowWrapper extends EplCommonRowWrapper
{
    protected EplDistributionCommonRowWrapper(Long id, EppRegistryElementPart discipline, EplTimeRuleEduLoad rule, EplCommonRowWrapper.DisciplineOrRulePairKey key)
    {
        super(id, discipline, rule, key);
    }

    public Map<Long, Long> distributionTimeMap = Maps.newHashMap();
    public Map<Long, Long> childrenTimeMap = Maps.newHashMap();

    protected List<EplDistributionSubRowWrapper> children = Lists.newArrayList();
    private Map<Long, List<EplTimeItem>> timeItemMap = Maps.newHashMap();

    public long timeSum;
    public long childrenTimeSum;

    private EplPlannedPps pps;
    private boolean overtime;
    private boolean ppsDownVisible;
    protected boolean visible;

    public String getTimeSum() {
        if (!isHasChildren() || timeSum == childrenTimeSum) {
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getTimeSumAsDouble());
        }
        return "<div>" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getTimeSumAsDouble()) + "</div>" + PpsTimeStatsUtil.colorValue(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getChildrenTimeSumAsDouble()), true);
    }
    public double getTimeSumAsDouble() { return UniEppLoadUtils.wrap(timeSum); }
    public Long getTimeSumAsLong() { return timeSum; }
    public void setTimeSumAsLong(long timeSum) { this.timeSum = timeSum; }
    public double getChildrenTimeSumAsDouble() { return UniEppLoadUtils.wrap(childrenTimeSum); }
    public Map<Long, Long> getDistributionTimeMap() { return distributionTimeMap; }
    public void setDistributionTimeMap(Map<Long, Long> distributionTimeMap) { this.distributionTimeMap = distributionTimeMap; }
    public EplPlannedPps getPps() { return pps; }
    public void setPps(EplPlannedPps pps) { this.pps = pps; }
    public boolean isPpsDownVisible() { return ppsDownVisible; }
    public void setPpsDownVisible(boolean ppsDownVisible) { this.ppsDownVisible = ppsDownVisible; }
    public boolean isVisible() { return visible; }
    public boolean isOvertime() { return overtime; }
    public void setOvertime(boolean overtime) { this.overtime = overtime; }
    public List<EplDistributionSubRowWrapper> getChildren() { return children; }
    public boolean isHasChildren() {return !getChildren().isEmpty(); }
    public int getRowSpan() {
        return 1 + getChildren().size();
    }
    public boolean isSubRow() { return this instanceof EplDistributionSubRowWrapper; }
    public boolean isTopRow() { return !isSubRow(); }
    public Map<Long, List<EplTimeItem>> getTimeItemMap() { return timeItemMap; }

    public void addTime(EplTimeCategoryEduLoad category, long timeAmount)
    {
        getDistributionTimeMap().put(category.getId(), getDistributionTimeMap().getOrDefault(category.getId(), 0L) + timeAmount);
        timeSum += timeAmount;
    }

    public void addTime(EplTimeItem timeItem) {
        addTime(timeItem.getTimeRule().getCategory(), timeItem.getTimeAmountAsLong());
        timeItemMap.computeIfAbsent(timeItem.getTimeRule().getCategory().getId(), k -> Lists.newArrayList()).add(timeItem);
    }

    public boolean isShowOvertime()
    {
        return getPps() != null;
    }

    public boolean isOvertimeDisabled()
    {
        return null != getPps() && getPps().isTimeWorker();
    }

    public void updateVisibility(IUISettings settings)
    {
        Object ppsFilter = settings.get(BIND_PPS);
        boolean displayFreeLoad = Boolean.TRUE.equals(settings.get(BIND_DISPLAY_FREE_LOAD));

        if (ppsFilter == null && !displayFreeLoad) {
            visible = true;
            return;
        }

        Predicate<EplDistributionCommonRowWrapper> ppsMatch;
        if (ppsFilter != null)
            ppsMatch = displayFreeLoad ? r -> ( isHasFreeLoad() || ppsFilter.equals(r.getPps())) : r -> (ppsFilter.equals(r.getPps()));
        else
            ppsMatch = EplDistributionCommonRowWrapper::isHasFreeLoad;

        visible = (ppsMatch.test(this) || getChildren().stream().anyMatch(ppsMatch));
    }

    public boolean getDisciplineOrRulePairKeyVisible(IUISettings settings)
    {
        List<EplCommonRowWrapper> rowWrappers = settings.get(BIND_DISC_OR_RULE);
        boolean keyVisible = true;
        if (CollectionUtils.isNotEmpty(rowWrappers))
        {
            keyVisible = false;
            for (EplCommonRowWrapper wrapper : rowWrappers)
            {
                keyVisible = keyVisible || ObjectUtils.equals(getKey().getDisciplineId(), wrapper.getKey().getDisciplineId()) && ObjectUtils.equals(getKey().getRuleId(), wrapper.getKey().getRuleId());
            }
        }
        return keyVisible;
    }

    public boolean isHasFreeLoad()
    {
        return (isHasChildren() && timeSum != childrenTimeSum) || (!isHasChildren() && getPps() == null);
    }

    public abstract String getViewProperty(TitleColumnType columnType);
    public abstract PairKey<String, ParametersMap> getLink(TitleColumn column);

    public static class TitleColumn extends IdentifiableWrapper
    {
        private boolean visible = true;

        public TitleColumn(Long id, String title) { super(id, title); }

        public boolean isVisible() {
            return visible;
        }

        public void setVisible(boolean visible) {
            this.visible = visible;
        }

        public TitleColumnType getType() {
            return TitleColumnType.values()[(int) (long) getId()];
        }

        public void updateVisibility(IUISettings settings)
        {
            List<EplCommonRowWrapper> rowWrappers = settings.get(BIND_DISC_OR_RULE);

            switch (getType()) {
                case ROW_TITLE: setVisible(CollectionUtils.isEmpty(rowWrappers) || rowWrappers.size() > 1); break;
                case TRANSFER_TO: setVisible(BooleanUtils.isTrue(settings.get(BIND_HAS_TRANSFER_TO))); break;
                case STUDENT_COUNT: setVisible(false); break;
                case EDU_GROUP_TYPE: setVisible(true); break;
                case EDU_GROUP: setVisible(true); break;
                case GROUP: setVisible(true); break;
                case COURSE: setVisible(null == settings.get(BIND_COURSE)); break;
                case TERM: setVisible(true); break;
                case YEAR_PART: setVisible(null == settings.get(BIND_YEAR_PART)); break;
                case EDU_OU: setVisible(true); break;
                default: throw new IllegalStateException();
            }
        }

        public static List<TitleColumn> getList() {
            long i = -1;
            return Arrays.asList(
                    new TitleColumn(++i, "Мероприятие реестра, норма времени"),
                    new TitleColumn(++i, "Передано с"),
                    new TitleColumn(++i, "Число студентов"),
                    new TitleColumn(++i, "Вид УГС"),
                    new TitleColumn(++i, "План. поток"),
                    new TitleColumn(++i, "Группа"),
                    new TitleColumn(++i, "Курс"),
                    new TitleColumn(++i, "Сем."),
                    new TitleColumn(++i, "Часть года"),
                    new TitleColumn(++i, "НПП")
            );
        }
    }

    public enum TitleColumnType
    {
        ROW_TITLE, TRANSFER_TO, STUDENT_COUNT, EDU_GROUP_TYPE, EDU_GROUP, GROUP, COURSE, TERM, YEAR_PART, EDU_OU
    }
}
