package ru.tandemservice.uniepp_load.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип оформления планируемого ППС"
 * Имя сущности : eplPlannedPpsType
 * Файл data.xml : uniepp_load.catalog.data.xml
 */
public interface EplPlannedPpsTypeCodes
{
    /** Константа кода (code) элемента : Штат (title) */
    String STAFF = "staff";
    /** Константа кода (code) элемента : Вакансия (штат) (title) */
    String VACANCY_AS_STAFF = "vacancyAsStaff";
    /** Константа кода (code) элемента : Почасовик (title) */
    String TIMEWORKER = "timeWorker";
    /** Константа кода (code) элемента : Вакансия (почасовик) (title) */
    String VACANCY_AS_TIMEWORKER = "vacancyAsTimeWorker";

    Set<String> CODES = ImmutableSet.of(STAFF, VACANCY_AS_STAFF, TIMEWORKER, VACANCY_AS_TIMEWORKER);
}
