package ru.tandemservice.uniepp_load.base.entity.ouSummary.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplSimpleTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Часы нагрузки по простой норме
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplSimpleTimeItemGen extends EplTimeItem
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.base.entity.ouSummary.EplSimpleTimeItem";
    public static final String ENTITY_NAME = "eplSimpleTimeItem";
    public static final int VERSION_HASH = 477725704;
    private static IEntityMeta ENTITY_META;

    public static final String L_YEAR_PART = "yearPart";
    public static final String P_PARAMETER = "parameter";
    public static final String P_COUNT_AS_CONTRACT = "countAsContract";

    private YearDistributionPart _yearPart;     // Часть учебного года
    private double _parameter;     // Параметр
    private boolean _countAsContract;     // Отнести к возмещению затрат на договорной основе

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Часть учебного года.
     */
    public YearDistributionPart getYearPart()
    {
        return _yearPart;
    }

    /**
     * @param yearPart Часть учебного года.
     */
    public void setYearPart(YearDistributionPart yearPart)
    {
        dirty(_yearPart, yearPart);
        _yearPart = yearPart;
    }

    /**
     * @return Параметр. Свойство не может быть null.
     */
    @NotNull
    public double getParameter()
    {
        return _parameter;
    }

    /**
     * @param parameter Параметр. Свойство не может быть null.
     */
    public void setParameter(double parameter)
    {
        dirty(_parameter, parameter);
        _parameter = parameter;
    }

    /**
     * @return Отнести к возмещению затрат на договорной основе. Свойство не может быть null.
     */
    @NotNull
    public boolean isCountAsContract()
    {
        return _countAsContract;
    }

    /**
     * @param countAsContract Отнести к возмещению затрат на договорной основе. Свойство не может быть null.
     */
    public void setCountAsContract(boolean countAsContract)
    {
        dirty(_countAsContract, countAsContract);
        _countAsContract = countAsContract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EplSimpleTimeItemGen)
        {
            setYearPart(((EplSimpleTimeItem)another).getYearPart());
            setParameter(((EplSimpleTimeItem)another).getParameter());
            setCountAsContract(((EplSimpleTimeItem)another).isCountAsContract());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplSimpleTimeItemGen> extends EplTimeItem.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplSimpleTimeItem.class;
        }

        public T newInstance()
        {
            return (T) new EplSimpleTimeItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "yearPart":
                    return obj.getYearPart();
                case "parameter":
                    return obj.getParameter();
                case "countAsContract":
                    return obj.isCountAsContract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "yearPart":
                    obj.setYearPart((YearDistributionPart) value);
                    return;
                case "parameter":
                    obj.setParameter((Double) value);
                    return;
                case "countAsContract":
                    obj.setCountAsContract((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "yearPart":
                        return true;
                case "parameter":
                        return true;
                case "countAsContract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "yearPart":
                    return true;
                case "parameter":
                    return true;
                case "countAsContract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "yearPart":
                    return YearDistributionPart.class;
                case "parameter":
                    return Double.class;
                case "countAsContract":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplSimpleTimeItem> _dslPath = new Path<EplSimpleTimeItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplSimpleTimeItem");
    }
            

    /**
     * @return Часть учебного года.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplSimpleTimeItem#getYearPart()
     */
    public static YearDistributionPart.Path<YearDistributionPart> yearPart()
    {
        return _dslPath.yearPart();
    }

    /**
     * @return Параметр. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplSimpleTimeItem#getParameter()
     */
    public static PropertyPath<Double> parameter()
    {
        return _dslPath.parameter();
    }

    /**
     * @return Отнести к возмещению затрат на договорной основе. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplSimpleTimeItem#isCountAsContract()
     */
    public static PropertyPath<Boolean> countAsContract()
    {
        return _dslPath.countAsContract();
    }

    public static class Path<E extends EplSimpleTimeItem> extends EplTimeItem.Path<E>
    {
        private YearDistributionPart.Path<YearDistributionPart> _yearPart;
        private PropertyPath<Double> _parameter;
        private PropertyPath<Boolean> _countAsContract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Часть учебного года.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplSimpleTimeItem#getYearPart()
     */
        public YearDistributionPart.Path<YearDistributionPart> yearPart()
        {
            if(_yearPart == null )
                _yearPart = new YearDistributionPart.Path<YearDistributionPart>(L_YEAR_PART, this);
            return _yearPart;
        }

    /**
     * @return Параметр. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplSimpleTimeItem#getParameter()
     */
        public PropertyPath<Double> parameter()
        {
            if(_parameter == null )
                _parameter = new PropertyPath<Double>(EplSimpleTimeItemGen.P_PARAMETER, this);
            return _parameter;
        }

    /**
     * @return Отнести к возмещению затрат на договорной основе. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplSimpleTimeItem#isCountAsContract()
     */
        public PropertyPath<Boolean> countAsContract()
        {
            if(_countAsContract == null )
                _countAsContract = new PropertyPath<Boolean>(EplSimpleTimeItemGen.P_COUNT_AS_CONTRACT, this);
            return _countAsContract;
        }

        public Class getEntityClass()
        {
            return EplSimpleTimeItem.class;
        }

        public String getEntityName()
        {
            return "eplSimpleTimeItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
