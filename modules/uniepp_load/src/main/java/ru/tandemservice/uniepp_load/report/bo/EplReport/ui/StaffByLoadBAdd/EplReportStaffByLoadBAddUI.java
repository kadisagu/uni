/* $Id:$ */
package ru.tandemservice.uniepp_load.report.bo.EplReport.ui.StaffByLoadBAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplPrintScript;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplPrintScriptCodes;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.ui.Pub.EplReportBasePub;
import ru.tandemservice.uniepp_load.report.entity.EplReportStaffByLoadB;

import java.util.Date;
import java.util.Map;

/**
 * @author oleyba
 * @since 2/19/15
 */
public class EplReportStaffByLoadBAddUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
    }

    public void onClickApply() {
        getSettings().save();
        Long reportId = IUniBaseDao.instance.get().doInTransaction(session -> {
            EplStudentSummary summary = getSettings().get("studentSummary");

            EplPrintScript scriptItem = DataAccessServices.dao().getByCode(EplPrintScript.class, EplPrintScriptCodes.STAFF_BY_LOAD_B);
            Map<String,Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                IScriptExecutor.TEMPLATE_VARIABLE, new byte[]{},
                IScriptExecutor.OBJECT_VARIABLE, summary.getId());

            EplReportStaffByLoadB report = new EplReportStaffByLoadB();

            report.setFormingDate(new Date());
            report.setStudentSummary(summary.getTitle());
            report.setEduYear(summary.getEppYear().getEducationYear());

            DatabaseFile dbFile = new DatabaseFile();

            byte[] content = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
            String filename = (String) scriptResult.get(IScriptExecutor.FILE_NAME);

            if (null == content) {
                throw new ApplicationException("Скрипт печати не вернул содержимое файла. Обратитесь к администратору системы.");
            }
            if (null == filename || !filename.contains(".")) {
                throw new ApplicationException("Скрипт печати вернул некорректное имя файла. Обратитесь к администратору системы.");
            }

            dbFile.setContent(content);
            dbFile.setFilename(filename);
            dbFile.setContentType(CommonBaseUtil.getContentTypeByFileName(filename, DatabaseFile.CONTENT_TYPE_APPLICATION_RTF));
            session.save(dbFile);

            report.setContent(dbFile);
            session.save(report);

            return report.getId();
        });

        deactivate();
        _uiActivation.asDesktopRoot(EplReportBasePub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.putAll(getSettings().getAll(null));
    }

    // getters and setters
}