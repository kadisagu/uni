/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic;

import com.google.common.collect.Lists;
import org.tandemframework.core.common.ITitled;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.DevelopUtil;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;

import java.util.List;

/**
 * @author nvankov
 * @since 2/26/15
 */
public class EplTransferAffiliateWrapper implements ITitled
{
    private EducationOrgUnit _educationOrgUnit;

    private List<EducationOrgUnit> _educationOrgUnitList = Lists.newArrayList();
    private EducationOrgUnit _selectedEducationOrgUnit;
    private List<EplStudent> _students = Lists.newArrayList();
    private String _title;

    public EplTransferAffiliateWrapper(EducationOrgUnit educationOrgUnit)
    {
        _educationOrgUnit = educationOrgUnit;
    }

    public void fillData()
    {
        int studentCount = 0;
        for(EplStudent student : _students)
        {
            studentCount += student.getCount();
        }
        // "[Число студентов] - [НПП вариант 1 ], [ФУТС вариант 1 ], [сокр. название форм. подр.]",
        _title = studentCount + " - " + _educationOrgUnit.getTitle() + ", " + DevelopUtil.getTitle(_educationOrgUnit) + ", " + _educationOrgUnit.getFormativeOrgUnit().getShortTitle();
    }

    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    public List<EplStudent> getStudents()
    {
        return _students;
    }

    public List<EducationOrgUnit> getEducationOrgUnitList()
    {
        return _educationOrgUnitList;
    }

    public EducationOrgUnit getSelectedEducationOrgUnit()
    {
        return _selectedEducationOrgUnit;
    }

    public void setSelectedEducationOrgUnit(EducationOrgUnit selectedEducationOrgUnit)
    {
        _selectedEducationOrgUnit = selectedEducationOrgUnit;
    }

    public String getTitle()
    {
        return _title;
    }

    public String getRowId()
    {
        return String.valueOf(_educationOrgUnit.getId());
    }
}
