/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Refresh;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.hibsupport.log.UpdateEntityEvent;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.EplStudentSummaryGroupTabUI;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Pub.EplStudentSummaryPub;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;

import java.util.Date;

/**
 * @author oleyba
 * @since 19.08.15
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "summaryHolder.id")})
public class EplStudentSummaryRefreshUI extends UIPresenter
{
    private EntityHolder<EplStudentSummary> _summaryHolder = new EntityHolder<>();

    private EducationYear _eduYear;
    private boolean _refreshFirstCourse;
    private boolean _backupCopy = true;

    @Override
    public void onComponentRefresh()
    {
        getSummaryHolder().refresh();
        _eduYear = DataAccessServices.dao().getNotNull(EducationYear.class, EducationYear.current(), true);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EducationYear.PARAM_EDU_YEAR_ONLY_CURRENT, true);
    }

    public void onClickApply()
    {
        EplStudentSummary summaryCopy = null;
        if (isBackupCopy())
        {
            String title = getSummary().getTitle() + " (резервная копия от " + DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date()) + ")";
            summaryCopy = EplStudentSummaryManager.instance().dao().doCopySummary(title, getSummary().getId(), true);
        }

        DatabaseFile file = getSummary().getZipLog();
        if (null != file)
        {
            getSummary().setZipLog(null);
            DataAccessServices.dao().update(getSummary());
            DataAccessServices.dao().delete(file);
        }

        int missEplStudents = EplStudentSummaryManager.instance().dao().doRefreshSummary(getSummary(), _refreshFirstCourse);

        if (null != summaryCopy)
            ContextLocal.getInfoCollector().add("Создана сводка «" + summaryCopy.getTitleWithState() + "».");

        CoreServices.eventService().fireEvent(new UpdateEntityEvent("Сводка контингента обновлена", getSummary()));
        _uiActivation.asDesktopRoot(EplStudentSummaryPub.class)
                .parameter(PUBLISHER_ID, getSummary().getId())
                .parameter("selectedTab", "groupTab")
                .parameter(EplStudentSummaryGroupTabUI.PARAM_MISS_EPL_STUDENTS, missEplStudents)
                .activate();

        deactivate();
    }

    public boolean isYearsNotEquals()
    {
        return null != getEppYear() && !getEppYear().getEducationYear().equals(_eduYear);
    }

    // getters and setters

    public EplStudentSummary getSummary()
    {
        return getSummaryHolder().getValue();
    }

    public void setSummary(EplStudentSummary summary)
    {
        getSummaryHolder().setValue(summary);
    }

    public EntityHolder<EplStudentSummary> getSummaryHolder()
    {
        return _summaryHolder;
    }

    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    public void setEduYear(EducationYear eduYear)
    {
        _eduYear = eduYear;
    }

    public EppYearEducationProcess getEppYear()
    {
        return getSummary().getEppYear();
    }

    public boolean isRefreshFirstCourse()
    {
        return _refreshFirstCourse;
    }

    public void setRefreshFirstCourse(boolean refreshFirstCourse)
    {
        _refreshFirstCourse = refreshFirstCourse;
    }

    public boolean isBackupCopy()
    {
        return _backupCopy;
    }

    public void setBackupCopy(boolean backupCopy)
    {
        _backupCopy = backupCopy;
    }
}