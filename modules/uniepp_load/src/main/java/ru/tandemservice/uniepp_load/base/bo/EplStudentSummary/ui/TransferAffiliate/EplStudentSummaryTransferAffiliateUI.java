/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.TransferAffiliate;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic.EplTransferAffiliateWrapper;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTabUI;

import java.util.List;

/**
 * @author nvankov
 * @since 2/25/15
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "summaryHolder.id", required = true)

})
public class EplStudentSummaryTransferAffiliateUI extends UIPresenter
{
    private EntityHolder<EplStudentSummary> summaryHolder = new EntityHolder<EplStudentSummary>();

    private List<EplTransferAffiliateWrapper> _wrappers = Lists.newArrayList();
    private EplTransferAffiliateWrapper _currentWrapper;

    public void onComponentRefresh()
    {
        getSummaryHolder().refresh();
        onChangeFilterParams();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("studentSummaryId", getSummary().getId());
        if(EplStudentSummaryTransferAffiliate.ORG_UNIT_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(getSettings().getAsMap(true, "course"));
        }
    }

    public void onChangeFilterParams()
    {
        saveSettings();

        Course course = getSettings().get("course");
        OrgUnit orgUnit = getSettings().get("orgUnit");

        _wrappers.clear();

        if (course == null || orgUnit == null) {
            return;
        }
        _wrappers.addAll(EplStudentSummaryManager.instance().dao().getEplTransferAffilationWrappers(getSummary().getId(), course, orgUnit));

    }

    public void onClickSave()
    {
        apply();
        deactivate();
    }

    public void onClickApply()
    {
        apply();
        _uiSupport.doRefresh();
    }

    private void apply()
    {
        EplStudentSummaryManager.instance().dao().doTransferFromAffiliate(_wrappers);
    }

    // getters and setters
    //
    public EplStudentSummary getSummary()
    {
        return getSummaryHolder().getValue();
    }

    public EntityHolder<EplStudentSummary> getSummaryHolder()
    {
        return summaryHolder;
    }

    public boolean isShowSelectWarn()
    {
        Course course = getSettings().get("course");
        OrgUnit orgUnit = getSettings().get("orgUnit");

        return course == null || orgUnit == null;
    }

    //
    public List<EplTransferAffiliateWrapper> getWrappers()
    {
        return _wrappers;
    }

    public EplTransferAffiliateWrapper getCurrentWrapper()
    {
        return _currentWrapper;
    }

    public void setCurrentWrapper(EplTransferAffiliateWrapper currentWrapper)
    {
        _currentWrapper = currentWrapper;
    }
}
