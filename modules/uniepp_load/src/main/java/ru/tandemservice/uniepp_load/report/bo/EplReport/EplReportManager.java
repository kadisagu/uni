/* $Id:$ */
package ru.tandemservice.uniepp_load.report.bo.EplReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.config.itemList.IItemListExtPointBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.transformer.BaseOutputTransformer;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.logic.IEplStorableReportDesc;
import ru.tandemservice.uniepp_load.report.entity.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/16/15
 */
@Configuration
public class EplReportManager extends BusinessObjectManager
{
    public static final String DS_SUMMARY_EPP_YEAR = "summaryEppYearDS";

    public static EplReportManager instance()
    {
        return instance(EplReportManager.class);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> orgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EplOrgUnitSummary.class)
            .where(EplOrgUnitSummary.studentSummary(), "studentSummary", true)
            .order(EplOrgUnitSummary.orgUnit().fullTitle())
            .filter(EplOrgUnitSummary.orgUnit().fullTitle())
            .pageable(true);
    }


    private static Map<String, IEplStorableReportDesc> getStorableReportDescList() {
        Map<String, IEplStorableReportDesc> map = new HashMap<>();
        map.put(EplReportOrgUnitEduLoadA.DESC.getReportKey(), EplReportOrgUnitEduLoadA.DESC);
        map.put(EplReportOrgUnitEduLoadB.DESC.getReportKey(), EplReportOrgUnitEduLoadB.DESC);
        map.put(EplReportHourlyFund.DESC.getReportKey(), EplReportHourlyFund.DESC);
        map.put(EplReportStaffByLoadB.DESC.getReportKey(), EplReportStaffByLoadB.DESC);
        map.put(EplReportOrgUnitEduLoadByPpsCheck.DESC.getReportKey(), EplReportOrgUnitEduLoadByPpsCheck.DESC);
        map.put(EplReportOrgUnitEduAssignment.DESC.getReportKey(), EplReportOrgUnitEduAssignment.DESC);
        return map;
    }

    @Bean
    public ItemListExtPoint<IEplStorableReportDesc> storableReportDescExtPoint() {
        IItemListExtPointBuilder<IEplStorableReportDesc> items = itemList(IEplStorableReportDesc.class);
        for (Map.Entry<String, IEplStorableReportDesc> entry : getStorableReportDescList().entrySet())
        {
            items.add(entry.getKey(), entry.getValue());
        }
        return items.create();
    }

    @Bean
    public UIDataSourceConfig eduYearDSConfig()
    {
        return SelectDSConfig.with(EducationCatalogsManager.DS_EDU_YEAR, getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(eduYearDSHandler())
        .valueStyleSource(EducationCatalogsManager.getEduYearValueStyleSource())
        .create();
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eduYearDSHandler()
    {
        return EducationYear.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) ->
                                   dql.where(exists(new DQLSelectBuilder()
                                                    .fromEntity(EppYearEducationProcess.class, "p").column("p.id")
                                                    .where(eq(property("p", EppYearEducationProcess.educationYear().id()), property(alias + ".id")))
                                                    .buildQuery())))
                .outputTransformer(new BaseOutputTransformer<DSInput, DSOutput>(EducationCatalogsManager.DS_EDU_YEAR, this.getName())
                {
                    @Override
                    public void transformOutput(final DSInput input, final DSOutput output, final ExecutionContext context)
                    {
                        Collections.reverse(output.getRecordList());
                    }
                });
    }

    @Bean
    public EntityComboDataSourceHandler summaryEduYearDSHandler()
    {
        return EppYearEducationProcess.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) -> dql.where(exists(EplStudentSummary.class, EplStudentSummary.eppYear().s(), property(alias))));
    }
}
