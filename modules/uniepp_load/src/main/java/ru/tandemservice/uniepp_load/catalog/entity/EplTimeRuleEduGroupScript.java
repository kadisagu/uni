package ru.tandemservice.uniepp_load.catalog.entity;

import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.bo.Common.ui.ScriptEdit.ScriptHolder;
import org.tandemframework.shared.commonbase.base.util.IScriptOwner;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.DynamicButton;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import ru.tandemservice.uni.dao.UniScriptDao;
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.EplTimeRuleManager;
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.ui.EduGroupScriptAddEdit.EplTimeRuleEduGroupScriptAddEdit;
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.ui.EduGroupScriptPub.EplTimeRuleEduGroupScriptPub;
import ru.tandemservice.uniepp_load.catalog.entity.gen.EplTimeRuleEduGroupScriptGen;
import ru.tandemservice.uniepp_load.util.IScriptPerformanceCounter;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/** @see ru.tandemservice.uniepp_load.catalog.entity.gen.EplTimeRuleEduGroupScriptGen */
public class EplTimeRuleEduGroupScript extends EplTimeRuleEduGroupScriptGen implements IScriptOwner, IScriptPerformanceCounter
{
    public static final String DEFAULT_SCRIPT_PATH = "scripts/timeRule/DefaultTimeRuleScript.groovy";

    @Override
    @EntityDSLSupport
    public boolean isHasUserScript() {
        return StringUtils.isNotEmpty(getUserScript());
    }

    @EntityDSLSupport
    public boolean isHasNoDefaultScript() {
        return StringUtils.isEmpty(getDefaultScriptPath());
    }

    @Override
    @EntityDSLSupport
    public double getAvgTimeWork()
    {
        if (getRequestCount() <= 0) return 0;
        return (double) getTotalTimeWork() / getRequestCount();
    }

    private static Set<String> HIDDEN_PROPERTIES = ImmutableSet.of(
            L_GROUPING,
            L_GROUP_TYPE,
            L_REG_STRUCTURE,
            P_DEFAULT_SCRIPT_PATH,
            P_USER_SCRIPT,
            P_COUNTER_ENABLED,
            P_TOTAL_TIME_WORK,
            P_REQUEST_COUNT
    );
    private static final Set<String> HIDDEN_FIELDS_ITEM_PUB = ImmutableSet.of(
            P_DEFAULT_SCRIPT_PATH,
            P_USER_SCRIPT,
            P_TOTAL_TIME_WORK,
            P_REQUEST_COUNT
    );

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public String getViewComponentName() { return EplTimeRuleEduGroupScriptPub.class.getSimpleName(); }
            @Override public String getAddEditComponentName() { return EplTimeRuleEduGroupScriptAddEdit.class.getSimpleName(); }
            @Override public Collection<String> getHiddenFields() { return HIDDEN_PROPERTIES; }
            @Override public Collection<String> getHiddenFieldsItemPub() { return HIDDEN_FIELDS_ITEM_PUB; }
            @Override public void addAdditionalButtons(List<DynamicButton> buttonList)
            {
                buttonList.add(new DynamicButton("Включить все счетчики", "onClickAllCountersActivate", ""));
                buttonList.add(new DynamicButton("Выключить все счетчики", "onClickAllCountersDeactivate", ""));
                buttonList.add(new DynamicButton("Сбросить все счетчики", "onClickAllCountersClear", ""));
            }
        };
    }

    public ScriptHolder<EplTimeRuleEduGroupScript> getScriptHolder()
    {
        return new ScriptHolder<EplTimeRuleEduGroupScript>(this) {
            @Override public String getScriptText() {
                final String script = StringUtils.trimToNull(getValue().getUserScript());
                if (null != script) { return script; }
                return getDefaultScriptText();
            }

            @Override public void setScriptText(String userScript) {
                userScript = StringUtils.trimToNull(userScript);
                if (null != userScript && scriptEquals(userScript, getDefaultScriptText())) { userScript = null; }
                getValue().setUserScript(userScript);
            }

            @Override public String getDefaultScriptText() {
                if (!isHasNoDefaultScript()) { return StringUtils.trimToEmpty(EplTimeRuleManager.instance().timeRuleDao().getDefaultScriptText(EplTimeRuleEduGroupScript.this)); }
                return "";
            }

            @Override protected void internalSave() {
                EplTimeRuleManager.instance().timeRuleDao().doSaveUserScript(getValue());
            }
        };
    }

    @Override
    public String getScriptPath()
    {
        return UniScriptDao.findScriptPath(getDefaultScriptPath());
    }
}