package ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps;
import ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad;

import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 20.05.2015
 */
public interface IEplSettingsPpsLoadDao extends INeedPersistenceSupport
{
    /**
     * Проверяет наличие настройки для каждой должности выбранной сводки
     *
     * @param orgUnitSummaryId Сводка
     * @return true - настройка для должности отстутствует, иначе - false
     */
    boolean isNotFoundSettingsPpsLoad(Long orgUnitSummaryId);

    /**
     * Производит соответсвие общего кол-ва часов ППС с настройкой
     *
     * @param settingMap Мап настроки с ключом по должности
     * @param plannedPps Планируемый ППС
     * @param ppsSummary Всего часов нагрузки для ППС
     * @return <всего_часов_ППС, несоответствие_настройке>
     */
    CoreCollectionUtils.Pair<String, Boolean> getTotalSummaryByPps(Map<PostBoundedWithQGandQL, EplSettingsPpsLoad> settingMap, EplPlannedPps plannedPps, Double ppsSummary);

    /**
     * @param orgUnitSummaryId Сводка
     * @return Возвращает мап должностей на указанный уч. год в сводке
     */
    Map<PostBoundedWithQGandQL, EplSettingsPpsLoad> getPostMap(Long orgUnitSummaryId);
}
