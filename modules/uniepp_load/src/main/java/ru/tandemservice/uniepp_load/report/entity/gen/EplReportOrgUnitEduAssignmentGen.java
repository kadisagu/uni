package ru.tandemservice.uniepp_load.report.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduAssignment;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Учебное поручение читающему подразделению
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplReportOrgUnitEduAssignmentGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduAssignment";
    public static final String ENTITY_NAME = "eplReportOrgUnitEduAssignment";
    public static final int VERSION_HASH = -1596538813;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_YEAR = "eduYear";
    public static final String P_STUDENT_SUMMARY = "studentSummary";
    public static final String P_ORG_UNIT = "orgUnit";
    public static final String P_YEAR_PART = "yearPart";
    public static final String P_DEVELOP_FORM = "developForm";
    public static final String P_DEVELOP_CONDITION = "developCondition";

    private EducationYear _eduYear;     // Учебный год
    private String _studentSummary;     // Сводка контингента
    private String _orgUnit;     // Читающее подразделение
    private String _yearPart;     // Часть учебного года
    private String _developForm;     // Форма освоения
    private String _developCondition;     // Условие освоения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    /**
     * @param eduYear Учебный год. Свойство не может быть null.
     */
    public void setEduYear(EducationYear eduYear)
    {
        dirty(_eduYear, eduYear);
        _eduYear = eduYear;
    }

    /**
     * @return Сводка контингента. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStudentSummary()
    {
        return _studentSummary;
    }

    /**
     * @param studentSummary Сводка контингента. Свойство не может быть null.
     */
    public void setStudentSummary(String studentSummary)
    {
        dirty(_studentSummary, studentSummary);
        _studentSummary = studentSummary;
    }

    /**
     * @return Читающее подразделение. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Читающее подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(String orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Часть учебного года.
     */
    @Length(max=255)
    public String getYearPart()
    {
        return _yearPart;
    }

    /**
     * @param yearPart Часть учебного года.
     */
    public void setYearPart(String yearPart)
    {
        dirty(_yearPart, yearPart);
        _yearPart = yearPart;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(String developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения.
     */
    public void setDevelopCondition(String developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EplReportOrgUnitEduAssignmentGen)
        {
            setEduYear(((EplReportOrgUnitEduAssignment)another).getEduYear());
            setStudentSummary(((EplReportOrgUnitEduAssignment)another).getStudentSummary());
            setOrgUnit(((EplReportOrgUnitEduAssignment)another).getOrgUnit());
            setYearPart(((EplReportOrgUnitEduAssignment)another).getYearPart());
            setDevelopForm(((EplReportOrgUnitEduAssignment)another).getDevelopForm());
            setDevelopCondition(((EplReportOrgUnitEduAssignment)another).getDevelopCondition());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplReportOrgUnitEduAssignmentGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplReportOrgUnitEduAssignment.class;
        }

        public T newInstance()
        {
            return (T) new EplReportOrgUnitEduAssignment();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "eduYear":
                    return obj.getEduYear();
                case "studentSummary":
                    return obj.getStudentSummary();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "yearPart":
                    return obj.getYearPart();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "eduYear":
                    obj.setEduYear((EducationYear) value);
                    return;
                case "studentSummary":
                    obj.setStudentSummary((String) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((String) value);
                    return;
                case "yearPart":
                    obj.setYearPart((String) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((String) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eduYear":
                        return true;
                case "studentSummary":
                        return true;
                case "orgUnit":
                        return true;
                case "yearPart":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eduYear":
                    return true;
                case "studentSummary":
                    return true;
                case "orgUnit":
                    return true;
                case "yearPart":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "eduYear":
                    return EducationYear.class;
                case "studentSummary":
                    return String.class;
                case "orgUnit":
                    return String.class;
                case "yearPart":
                    return String.class;
                case "developForm":
                    return String.class;
                case "developCondition":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplReportOrgUnitEduAssignment> _dslPath = new Path<EplReportOrgUnitEduAssignment>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplReportOrgUnitEduAssignment");
    }
            

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduAssignment#getEduYear()
     */
    public static EducationYear.Path<EducationYear> eduYear()
    {
        return _dslPath.eduYear();
    }

    /**
     * @return Сводка контингента. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduAssignment#getStudentSummary()
     */
    public static PropertyPath<String> studentSummary()
    {
        return _dslPath.studentSummary();
    }

    /**
     * @return Читающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduAssignment#getOrgUnit()
     */
    public static PropertyPath<String> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Часть учебного года.
     * @see ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduAssignment#getYearPart()
     */
    public static PropertyPath<String> yearPart()
    {
        return _dslPath.yearPart();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduAssignment#getDevelopForm()
     */
    public static PropertyPath<String> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduAssignment#getDevelopCondition()
     */
    public static PropertyPath<String> developCondition()
    {
        return _dslPath.developCondition();
    }

    public static class Path<E extends EplReportOrgUnitEduAssignment> extends StorableReport.Path<E>
    {
        private EducationYear.Path<EducationYear> _eduYear;
        private PropertyPath<String> _studentSummary;
        private PropertyPath<String> _orgUnit;
        private PropertyPath<String> _yearPart;
        private PropertyPath<String> _developForm;
        private PropertyPath<String> _developCondition;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduAssignment#getEduYear()
     */
        public EducationYear.Path<EducationYear> eduYear()
        {
            if(_eduYear == null )
                _eduYear = new EducationYear.Path<EducationYear>(L_EDU_YEAR, this);
            return _eduYear;
        }

    /**
     * @return Сводка контингента. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduAssignment#getStudentSummary()
     */
        public PropertyPath<String> studentSummary()
        {
            if(_studentSummary == null )
                _studentSummary = new PropertyPath<String>(EplReportOrgUnitEduAssignmentGen.P_STUDENT_SUMMARY, this);
            return _studentSummary;
        }

    /**
     * @return Читающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduAssignment#getOrgUnit()
     */
        public PropertyPath<String> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new PropertyPath<String>(EplReportOrgUnitEduAssignmentGen.P_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Часть учебного года.
     * @see ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduAssignment#getYearPart()
     */
        public PropertyPath<String> yearPart()
        {
            if(_yearPart == null )
                _yearPart = new PropertyPath<String>(EplReportOrgUnitEduAssignmentGen.P_YEAR_PART, this);
            return _yearPart;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduAssignment#getDevelopForm()
     */
        public PropertyPath<String> developForm()
        {
            if(_developForm == null )
                _developForm = new PropertyPath<String>(EplReportOrgUnitEduAssignmentGen.P_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduAssignment#getDevelopCondition()
     */
        public PropertyPath<String> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new PropertyPath<String>(EplReportOrgUnitEduAssignmentGen.P_DEVELOP_CONDITION, this);
            return _developCondition;
        }

        public Class getEntityClass()
        {
            return EplReportOrgUnitEduAssignment.class;
        }

        public String getEntityName()
        {
            return "eplReportOrgUnitEduAssignment";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
