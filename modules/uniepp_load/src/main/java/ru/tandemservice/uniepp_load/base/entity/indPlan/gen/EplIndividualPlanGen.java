package ru.tandemservice.uniepp_load.base.entity.indPlan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Индивидуальный план преподавателя (ИПП)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplIndividualPlanGen extends EntityBase
 implements INaturalIdentifiable<EplIndividualPlanGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan";
    public static final String ENTITY_NAME = "eplIndividualPlan";
    public static final int VERSION_HASH = -477368995;
    private static IEntityMeta ENTITY_META;

    public static final String L_PPS = "pps";
    public static final String L_EPP_YEAR = "eppYear";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_TITLE = "title";

    private PpsEntry _pps;     // ППС
    private EppYearEducationProcess _eppYear;     // Учебный год
    private OrgUnit _orgUnit;     // Подразделение
    private Date _formingDate;     // Дата формирования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ППС. Свойство не может быть null.
     */
    @NotNull
    public PpsEntry getPps()
    {
        return _pps;
    }

    /**
     * @param pps ППС. Свойство не может быть null.
     */
    public void setPps(PpsEntry pps)
    {
        dirty(_pps, pps);
        _pps = pps;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EppYearEducationProcess getEppYear()
    {
        return _eppYear;
    }

    /**
     * @param eppYear Учебный год. Свойство не может быть null.
     */
    public void setEppYear(EppYearEducationProcess eppYear)
    {
        dirty(_eppYear, eppYear);
        _eppYear = eppYear;
    }

    /**
     * Подразделение, на котором создан ИПП, по нему находится подходящий расчет из сводки. Оно может не совпадать с подразделением из ППС.
     *
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EplIndividualPlanGen)
        {
            if (withNaturalIdProperties)
            {
                setPps(((EplIndividualPlan)another).getPps());
                setEppYear(((EplIndividualPlan)another).getEppYear());
                setOrgUnit(((EplIndividualPlan)another).getOrgUnit());
            }
            setFormingDate(((EplIndividualPlan)another).getFormingDate());
        }
    }

    public INaturalId<EplIndividualPlanGen> getNaturalId()
    {
        return new NaturalId(getPps(), getEppYear(), getOrgUnit());
    }

    public static class NaturalId extends NaturalIdBase<EplIndividualPlanGen>
    {
        private static final String PROXY_NAME = "EplIndividualPlanNaturalProxy";

        private Long _pps;
        private Long _eppYear;
        private Long _orgUnit;

        public NaturalId()
        {}

        public NaturalId(PpsEntry pps, EppYearEducationProcess eppYear, OrgUnit orgUnit)
        {
            _pps = ((IEntity) pps).getId();
            _eppYear = ((IEntity) eppYear).getId();
            _orgUnit = ((IEntity) orgUnit).getId();
        }

        public Long getPps()
        {
            return _pps;
        }

        public void setPps(Long pps)
        {
            _pps = pps;
        }

        public Long getEppYear()
        {
            return _eppYear;
        }

        public void setEppYear(Long eppYear)
        {
            _eppYear = eppYear;
        }

        public Long getOrgUnit()
        {
            return _orgUnit;
        }

        public void setOrgUnit(Long orgUnit)
        {
            _orgUnit = orgUnit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EplIndividualPlanGen.NaturalId) ) return false;

            EplIndividualPlanGen.NaturalId that = (NaturalId) o;

            if( !equals(getPps(), that.getPps()) ) return false;
            if( !equals(getEppYear(), that.getEppYear()) ) return false;
            if( !equals(getOrgUnit(), that.getOrgUnit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getPps());
            result = hashCode(result, getEppYear());
            result = hashCode(result, getOrgUnit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getPps());
            sb.append("/");
            sb.append(getEppYear());
            sb.append("/");
            sb.append(getOrgUnit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplIndividualPlanGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplIndividualPlan.class;
        }

        public T newInstance()
        {
            return (T) new EplIndividualPlan();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "pps":
                    return obj.getPps();
                case "eppYear":
                    return obj.getEppYear();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "formingDate":
                    return obj.getFormingDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "pps":
                    obj.setPps((PpsEntry) value);
                    return;
                case "eppYear":
                    obj.setEppYear((EppYearEducationProcess) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "pps":
                        return true;
                case "eppYear":
                        return true;
                case "orgUnit":
                        return true;
                case "formingDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "pps":
                    return true;
                case "eppYear":
                    return true;
                case "orgUnit":
                    return true;
                case "formingDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "pps":
                    return PpsEntry.class;
                case "eppYear":
                    return EppYearEducationProcess.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "formingDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplIndividualPlan> _dslPath = new Path<EplIndividualPlan>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplIndividualPlan");
    }
            

    /**
     * @return ППС. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan#getPps()
     */
    public static PpsEntry.Path<PpsEntry> pps()
    {
        return _dslPath.pps();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan#getEppYear()
     */
    public static EppYearEducationProcess.Path<EppYearEducationProcess> eppYear()
    {
        return _dslPath.eppYear();
    }

    /**
     * Подразделение, на котором создан ИПП, по нему находится подходящий расчет из сводки. Оно может не совпадать с подразделением из ППС.
     *
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EplIndividualPlan> extends EntityPath<E>
    {
        private PpsEntry.Path<PpsEntry> _pps;
        private EppYearEducationProcess.Path<EppYearEducationProcess> _eppYear;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<Date> _formingDate;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ППС. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan#getPps()
     */
        public PpsEntry.Path<PpsEntry> pps()
        {
            if(_pps == null )
                _pps = new PpsEntry.Path<PpsEntry>(L_PPS, this);
            return _pps;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan#getEppYear()
     */
        public EppYearEducationProcess.Path<EppYearEducationProcess> eppYear()
        {
            if(_eppYear == null )
                _eppYear = new EppYearEducationProcess.Path<EppYearEducationProcess>(L_EPP_YEAR, this);
            return _eppYear;
        }

    /**
     * Подразделение, на котором создан ИПП, по нему находится подходящий расчет из сводки. Оно может не совпадать с подразделением из ППС.
     *
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(EplIndividualPlanGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EplIndividualPlanGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EplIndividualPlan.class;
        }

        public String getEntityName()
        {
            return "eplIndividualPlan";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitle();
}
