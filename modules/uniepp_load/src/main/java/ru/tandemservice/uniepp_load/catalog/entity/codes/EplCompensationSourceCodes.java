package ru.tandemservice.uniepp_load.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Источник возмещения затрат"
 * Имя сущности : eplCompensationSource
 * Файл data.xml : uniepp_load.catalog.data.xml
 */
public interface EplCompensationSourceCodes
{
    /** Константа кода (code) элемента : Бюджет (title) */
    String COMPENSATION_TYPE_BUDGET = "1";
    /** Константа кода (code) элемента : По договору (title) */
    String COMPENSATION_TYPE_CONTRACT = "2";

    Set<String> CODES = ImmutableSet.of(COMPENSATION_TYPE_BUDGET, COMPENSATION_TYPE_CONTRACT);
}
