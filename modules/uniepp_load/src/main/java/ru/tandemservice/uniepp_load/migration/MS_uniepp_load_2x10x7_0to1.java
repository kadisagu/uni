package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_load_2x10x7_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.7")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eplIndPlan2OuSummaryRel

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epl_ind_plan_2_ou_summary_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eplindplan2ousummaryrel"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("indplan_id", DBType.LONG).setNullable(false), 
				new DBColumn("ousummary_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eplIndPlan2OuSummaryRel");

			// заполняем значениями из epl_ind_plan_t
			PreparedStatement insert = tool.prepareStatement("insert into epl_ind_plan_2_ou_summary_t (id, discriminator, indplan_id, ousummary_id) values (?, ?, ?, ?)");

			Statement stmt = tool.getConnection().createStatement();
			ResultSet rs = stmt.executeQuery("select id, ousummary_id from epl_ind_plan_t where ousummary_id is not null");
			while(rs.next())
			{
				Long indPlanId = rs.getLong(1);
				Long ouSummaryId = rs.getLong(2);

				insert.setLong(1, EntityIDGenerator.generateNewId(entityCode));
				insert.setShort(2, entityCode);
				insert.setLong(3, indPlanId);
				insert.setLong(4, ouSummaryId);
				insert.executeUpdate();
			}
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eplIndividualPlan

		// удалено свойство ouSummary
		{
			// удалить колонку
			tool.dropColumn("epl_ind_plan_t", "ousummary_id");
		}
    }
}