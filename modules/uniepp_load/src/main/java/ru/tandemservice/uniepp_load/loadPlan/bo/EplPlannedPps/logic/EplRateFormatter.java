package ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.logic;

import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.IRawFormatter;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uniepp_load.util.EplDefines;

import static ru.tandemservice.uniepp_load.util.EplDefines.getMaxLoadSetting;

/**
 * Created by nsvetlov on 02.02.2017.
 */
public class EplRateFormatter implements IRawFormatter
{
    @Override
    public String format(Object o)
    {
        String text = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(o);
        Double limit;
        final Double doubleVal = (Double) o;
        if (o instanceof  Double && (null !=  (limit = limitStaffRate(doubleVal)) || null !=  invalidStaffRate(doubleVal)))
            return "<span style=\"color:" + UniDefines.COLOR_CELL_ERROR + "\">" + text + (limit == null ? "" :
                    " (больше " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(limit) + ")") + " </span>";
        return text;
    }

    public static Double invalidStaffRate(Double staffRateAsDouble)
    {
        if (null == staffRateAsDouble)
            return null;
        IDataSettings settings = DataSettingsFacade.getSettings("general", "StaffRateStep_");//UniempDefines.STAFF_RATE_STEP_SETTINGS_PREFIX
        Double minStaffRate = settings.get("post");//UniempDefines.STAFF_RATE_STEP_POST_SETING_NAME
        if (null == minStaffRate)
            return null;
        Double reminder = Math.round((staffRateAsDouble % minStaffRate) * 100) / 100d;
        return  (reminder != 0 && !minStaffRate.equals(reminder)) ? minStaffRate : null;
    }

    public static Double limitStaffRate(Double staffRateAsDouble)
    {
        if (null == staffRateAsDouble)
            return null;
        Double maxStaffRate = getMaxLoadSetting();
        if (null == maxStaffRate)
            return null;
        return  maxStaffRate < staffRateAsDouble ? maxStaffRate : null;
    }
}
