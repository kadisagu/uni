package ru.tandemservice.uniepp_load.base.entity.studentSummary.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.catalog.entity.EplCompensationSource;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * План. студенты
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplStudentGen extends EntityBase
 implements INaturalIdentifiable<EplStudentGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent";
    public static final String ENTITY_NAME = "eplStudent";
    public static final int VERSION_HASH = -370145907;
    private static IEntityMeta ENTITY_META;

    public static final String L_GROUP = "group";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String L_COMPENSATION_SOURCE = "compensationSource";
    public static final String L_DEVELOP_GRID = "developGrid";
    public static final String P_COUNT = "count";
    public static final String P_ERROR_COUNT_WITH_WP = "errorCountWithWp";
    public static final String P_NO_WP = "noWp";

    private EplGroup _group;     // Группа
    private EducationOrgUnit _educationOrgUnit;     // НПП
    private EplCompensationSource _compensationSource;     // Источник возм. затрат
    private DevelopGrid _developGrid;     // Сетка
    private int _count;     // Число студентов
    private boolean _errorCountWithWp;     // Некорректно указаны РУП
    private boolean _noWp;     // Не указан РУП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public EplGroup getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(EplGroup group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return НПП. Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit НПП. Свойство не может быть null.
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * @return Источник возм. затрат. Свойство не может быть null.
     */
    @NotNull
    public EplCompensationSource getCompensationSource()
    {
        return _compensationSource;
    }

    /**
     * @param compensationSource Источник возм. затрат. Свойство не может быть null.
     */
    public void setCompensationSource(EplCompensationSource compensationSource)
    {
        dirty(_compensationSource, compensationSource);
        _compensationSource = compensationSource;
    }

    /**
     * @return Сетка. Свойство не может быть null.
     */
    @NotNull
    public DevelopGrid getDevelopGrid()
    {
        return _developGrid;
    }

    /**
     * @param developGrid Сетка. Свойство не может быть null.
     */
    public void setDevelopGrid(DevelopGrid developGrid)
    {
        dirty(_developGrid, developGrid);
        _developGrid = developGrid;
    }

    /**
     * @return Число студентов. Свойство не может быть null.
     */
    @NotNull
    public int getCount()
    {
        return _count;
    }

    /**
     * @param count Число студентов. Свойство не может быть null.
     */
    public void setCount(int count)
    {
        dirty(_count, count);
        _count = count;
    }

    /**
     * Есть набор связей с РУП в рамках части года, сумма по которому не соответствует числу студентов.
     *
     * @return Некорректно указаны РУП. Свойство не может быть null.
     */
    @NotNull
    public boolean isErrorCountWithWp()
    {
        return _errorCountWithWp;
    }

    /**
     * @param errorCountWithWp Некорректно указаны РУП. Свойство не может быть null.
     */
    public void setErrorCountWithWp(boolean errorCountWithWp)
    {
        dirty(_errorCountWithWp, errorCountWithWp);
        _errorCountWithWp = errorCountWithWp;
    }

    /**
     * Есть предусмотренная сеткой часть года, для которой не указан РУП.
     *
     * @return Не указан РУП. Свойство не может быть null.
     */
    @NotNull
    public boolean isNoWp()
    {
        return _noWp;
    }

    /**
     * @param noWp Не указан РУП. Свойство не может быть null.
     */
    public void setNoWp(boolean noWp)
    {
        dirty(_noWp, noWp);
        _noWp = noWp;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EplStudentGen)
        {
            if (withNaturalIdProperties)
            {
                setGroup(((EplStudent)another).getGroup());
                setEducationOrgUnit(((EplStudent)another).getEducationOrgUnit());
                setCompensationSource(((EplStudent)another).getCompensationSource());
            }
            setDevelopGrid(((EplStudent)another).getDevelopGrid());
            setCount(((EplStudent)another).getCount());
            setErrorCountWithWp(((EplStudent)another).isErrorCountWithWp());
            setNoWp(((EplStudent)another).isNoWp());
        }
    }

    public INaturalId<EplStudentGen> getNaturalId()
    {
        return new NaturalId(getGroup(), getEducationOrgUnit(), getCompensationSource());
    }

    public static class NaturalId extends NaturalIdBase<EplStudentGen>
    {
        private static final String PROXY_NAME = "EplStudentNaturalProxy";

        private Long _group;
        private Long _educationOrgUnit;
        private Long _compensationSource;

        public NaturalId()
        {}

        public NaturalId(EplGroup group, EducationOrgUnit educationOrgUnit, EplCompensationSource compensationSource)
        {
            _group = ((IEntity) group).getId();
            _educationOrgUnit = ((IEntity) educationOrgUnit).getId();
            _compensationSource = ((IEntity) compensationSource).getId();
        }

        public Long getGroup()
        {
            return _group;
        }

        public void setGroup(Long group)
        {
            _group = group;
        }

        public Long getEducationOrgUnit()
        {
            return _educationOrgUnit;
        }

        public void setEducationOrgUnit(Long educationOrgUnit)
        {
            _educationOrgUnit = educationOrgUnit;
        }

        public Long getCompensationSource()
        {
            return _compensationSource;
        }

        public void setCompensationSource(Long compensationSource)
        {
            _compensationSource = compensationSource;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EplStudentGen.NaturalId) ) return false;

            EplStudentGen.NaturalId that = (NaturalId) o;

            if( !equals(getGroup(), that.getGroup()) ) return false;
            if( !equals(getEducationOrgUnit(), that.getEducationOrgUnit()) ) return false;
            if( !equals(getCompensationSource(), that.getCompensationSource()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getGroup());
            result = hashCode(result, getEducationOrgUnit());
            result = hashCode(result, getCompensationSource());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getGroup());
            sb.append("/");
            sb.append(getEducationOrgUnit());
            sb.append("/");
            sb.append(getCompensationSource());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplStudentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplStudent.class;
        }

        public T newInstance()
        {
            return (T) new EplStudent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "group":
                    return obj.getGroup();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "compensationSource":
                    return obj.getCompensationSource();
                case "developGrid":
                    return obj.getDevelopGrid();
                case "count":
                    return obj.getCount();
                case "errorCountWithWp":
                    return obj.isErrorCountWithWp();
                case "noWp":
                    return obj.isNoWp();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "group":
                    obj.setGroup((EplGroup) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "compensationSource":
                    obj.setCompensationSource((EplCompensationSource) value);
                    return;
                case "developGrid":
                    obj.setDevelopGrid((DevelopGrid) value);
                    return;
                case "count":
                    obj.setCount((Integer) value);
                    return;
                case "errorCountWithWp":
                    obj.setErrorCountWithWp((Boolean) value);
                    return;
                case "noWp":
                    obj.setNoWp((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "group":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "compensationSource":
                        return true;
                case "developGrid":
                        return true;
                case "count":
                        return true;
                case "errorCountWithWp":
                        return true;
                case "noWp":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "group":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "compensationSource":
                    return true;
                case "developGrid":
                    return true;
                case "count":
                    return true;
                case "errorCountWithWp":
                    return true;
                case "noWp":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "group":
                    return EplGroup.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "compensationSource":
                    return EplCompensationSource.class;
                case "developGrid":
                    return DevelopGrid.class;
                case "count":
                    return Integer.class;
                case "errorCountWithWp":
                    return Boolean.class;
                case "noWp":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplStudent> _dslPath = new Path<EplStudent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplStudent");
    }
            

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent#getGroup()
     */
    public static EplGroup.Path<EplGroup> group()
    {
        return _dslPath.group();
    }

    /**
     * @return НПП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * @return Источник возм. затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent#getCompensationSource()
     */
    public static EplCompensationSource.Path<EplCompensationSource> compensationSource()
    {
        return _dslPath.compensationSource();
    }

    /**
     * @return Сетка. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent#getDevelopGrid()
     */
    public static DevelopGrid.Path<DevelopGrid> developGrid()
    {
        return _dslPath.developGrid();
    }

    /**
     * @return Число студентов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent#getCount()
     */
    public static PropertyPath<Integer> count()
    {
        return _dslPath.count();
    }

    /**
     * Есть набор связей с РУП в рамках части года, сумма по которому не соответствует числу студентов.
     *
     * @return Некорректно указаны РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent#isErrorCountWithWp()
     */
    public static PropertyPath<Boolean> errorCountWithWp()
    {
        return _dslPath.errorCountWithWp();
    }

    /**
     * Есть предусмотренная сеткой часть года, для которой не указан РУП.
     *
     * @return Не указан РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent#isNoWp()
     */
    public static PropertyPath<Boolean> noWp()
    {
        return _dslPath.noWp();
    }

    public static class Path<E extends EplStudent> extends EntityPath<E>
    {
        private EplGroup.Path<EplGroup> _group;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private EplCompensationSource.Path<EplCompensationSource> _compensationSource;
        private DevelopGrid.Path<DevelopGrid> _developGrid;
        private PropertyPath<Integer> _count;
        private PropertyPath<Boolean> _errorCountWithWp;
        private PropertyPath<Boolean> _noWp;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent#getGroup()
     */
        public EplGroup.Path<EplGroup> group()
        {
            if(_group == null )
                _group = new EplGroup.Path<EplGroup>(L_GROUP, this);
            return _group;
        }

    /**
     * @return НПП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * @return Источник возм. затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent#getCompensationSource()
     */
        public EplCompensationSource.Path<EplCompensationSource> compensationSource()
        {
            if(_compensationSource == null )
                _compensationSource = new EplCompensationSource.Path<EplCompensationSource>(L_COMPENSATION_SOURCE, this);
            return _compensationSource;
        }

    /**
     * @return Сетка. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent#getDevelopGrid()
     */
        public DevelopGrid.Path<DevelopGrid> developGrid()
        {
            if(_developGrid == null )
                _developGrid = new DevelopGrid.Path<DevelopGrid>(L_DEVELOP_GRID, this);
            return _developGrid;
        }

    /**
     * @return Число студентов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent#getCount()
     */
        public PropertyPath<Integer> count()
        {
            if(_count == null )
                _count = new PropertyPath<Integer>(EplStudentGen.P_COUNT, this);
            return _count;
        }

    /**
     * Есть набор связей с РУП в рамках части года, сумма по которому не соответствует числу студентов.
     *
     * @return Некорректно указаны РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent#isErrorCountWithWp()
     */
        public PropertyPath<Boolean> errorCountWithWp()
        {
            if(_errorCountWithWp == null )
                _errorCountWithWp = new PropertyPath<Boolean>(EplStudentGen.P_ERROR_COUNT_WITH_WP, this);
            return _errorCountWithWp;
        }

    /**
     * Есть предусмотренная сеткой часть года, для которой не указан РУП.
     *
     * @return Не указан РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent#isNoWp()
     */
        public PropertyPath<Boolean> noWp()
        {
            if(_noWp == null )
                _noWp = new PropertyPath<Boolean>(EplStudentGen.P_NO_WP, this);
            return _noWp;
        }

        public Class getEntityClass()
        {
            return EplStudent.class;
        }

        public String getEntityName()
        {
            return "eplStudent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
