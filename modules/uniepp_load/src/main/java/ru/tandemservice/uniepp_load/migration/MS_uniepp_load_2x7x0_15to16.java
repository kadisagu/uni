package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_load_2x7x0_15to16 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eplStudent
		{
            if(tool.columnExists("epl_student_t", "countwithwp_p"))
            {
                tool.dropColumn("epl_student_t", "countwithwp_p");
            }

            if(!tool.columnExists("epl_student_t", "errorcountwithwp_p"))
            {
                tool.createColumn("epl_student_t", new DBColumn("errorcountwithwp_p", DBType.BOOLEAN));

                tool.executeUpdate("update epl_student_t set errorcountwithwp_p=? where errorcountwithwp_p is null", (Boolean) false);

                tool.setColumnNullable("epl_student_t", "errorcountwithwp_p", false);
            }
		}
    }
}