/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.logic;

import com.google.common.collect.Lists;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndPlan2OuSummaryRel;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleNonEduLoad;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.sum;

/**
 * @author Alexey Lopatin
 * @since 03.08.2016
 */
public class EplIndividualPlanDao extends UniBaseDao implements IEplIndividualPlanDao
{
    @Override
    public void saveOrUpdateTimeItem(EplTimeItemNonEduLoad timeItem)
    {
        if (null == timeItem.getCreationDate())
            timeItem.setCreationDate(new Date());

        EplTimeRuleNonEduLoad timeRule = timeItem.getTimeRule();
        double result = timeRule.isParameterNeeded() ? timeRule.getCoefficient() * timeItem.getParameter() : timeRule.getCoefficient();

        timeItem.setTimeAmount(result);
        timeItem.setControlTimeAmount(result);
        saveOrUpdate(timeItem);
    }

    @Override
    public void saveOrUpdateOuSummaries(EplIndividualPlan indPlan, Collection<EplStudentSummary> studentSummaries)
    {
        List<EplIndPlan2OuSummaryRel> sourceList = getList(EplIndPlan2OuSummaryRel.class, EplIndPlan2OuSummaryRel.indPlan(), indPlan);
        List<EplIndPlan2OuSummaryRel> targetList = Lists.newArrayList();

        new DQLSelectBuilder().fromEntity(EplOrgUnitSummary.class, "e")
                .where(in(property("e", EplOrgUnitSummary.studentSummary().id()), ids(studentSummaries)))
                .where(eq(property("e", EplOrgUnitSummary.orgUnit()), value(indPlan.getOrgUnit())))
                .createStatement(getSession()).<EplOrgUnitSummary>list()
                .forEach(ouSummary -> targetList.add(new EplIndPlan2OuSummaryRel(indPlan, ouSummary)));

        new MergeAction.SessionMergeAction<EplIndPlan2OuSummaryRel.NaturalId, EplIndPlan2OuSummaryRel>() {
            @Override protected EplIndPlan2OuSummaryRel.NaturalId key(EplIndPlan2OuSummaryRel source) { return new EplIndPlan2OuSummaryRel.NaturalId(source.getIndPlan(), source.getOuSummary()); }
            @Override protected EplIndPlan2OuSummaryRel buildRow(EplIndPlan2OuSummaryRel source) { return new EplIndPlan2OuSummaryRel(source.getIndPlan(), source.getOuSummary()); }
            @Override protected void fill(EplIndPlan2OuSummaryRel target, EplIndPlan2OuSummaryRel source) { target.update(source, false); }
        }.merge(sourceList, targetList);
    }

    @Override
    public List<EplOrgUnitSummary> getOuSummaries(EplIndividualPlan indPlan)
    {
        return new DQLSelectBuilder().fromEntity(EplIndPlan2OuSummaryRel.class, "rel").column(property("ous"))
                .joinPath(DQLJoinType.inner, EplIndPlan2OuSummaryRel.ouSummary().fromAlias("rel"), "ous")
                .joinPath(DQLJoinType.inner, EplOrgUnitSummary.studentSummary().fromAlias("ous"), "ss")
                .where(eq(property("rel", EplIndPlan2OuSummaryRel.indPlan()), value(indPlan)))
                .order(property("ss", EplStudentSummary.eppYear().educationYear().intValue()), OrderDirection.desc)
                .order(property("ss", EplStudentSummary.title()))
                .createStatement(getSession()).list();
    }

    @Override
    public long getPpsEntryEduLoad(EplIndividualPlan indPlan)
    {
        if (null == indPlan) return 0L;

        Long result = new DQLSelectBuilder().fromEntity(EplPlannedPpsTimeItem.class, "pps")
                .column(sum(property("pps", EplPlannedPpsTimeItem.timeAmountAsLong())))
                .joinPath(DQLJoinType.inner,  EplPlannedPpsTimeItem.timeItem().fromAlias("pps"), "ti")
                .joinEntity("ti", DQLJoinType.inner, EplIndPlan2OuSummaryRel.class, "rel", and(
                        eq(property("ti", EplTimeItem.summary()), property("rel", EplIndPlan2OuSummaryRel.ouSummary())),
                        eq(property("rel", EplIndPlan2OuSummaryRel.indPlan().id()), value(indPlan.getId()))
                ))
                .where(eq(property("pps", EplPlannedPpsTimeItem.pps().ppsEntry()), value(indPlan.getPps())))
                .createStatement(getSession()).uniqueResult();

        return result == null ? 0L : result;
    }

    @Override
    public long getPpsEntryNonEduLoad(EplIndividualPlan indPlan)
    {
        Long result = new DQLSelectBuilder().fromEntity(EplTimeItemNonEduLoad.class, "e")
                .column(sum(property("e", EplTimeItemNonEduLoad.timeAmountAsLong())))
                .where(eq(property("e", EplTimeItemNonEduLoad.indPlan()), value(indPlan)))
                .createStatement(getSession()).uniqueResult();

        return result == null ? 0L : result;
    }
}
