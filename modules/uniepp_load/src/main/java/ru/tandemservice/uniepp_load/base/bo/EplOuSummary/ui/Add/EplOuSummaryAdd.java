/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;

/**
 * @author oleyba
 * @since 5/5/12
 */
@Configuration
public class EplOuSummaryAdd extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(EplStudentSummaryManager.instance().orgUnitDSConfig())
        .create();
    }
}
