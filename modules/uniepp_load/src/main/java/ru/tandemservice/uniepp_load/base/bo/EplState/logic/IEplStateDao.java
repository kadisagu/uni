package ru.tandemservice.uniepp_load.base.bo.EplState.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplStudentSummaryState;

import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 08.02.2016
 */
public interface IEplStateDao extends IUniBaseDao
{
    /**
     * Проверка возможности смены состояния сводки
     *
     * @param summary      сводка
     * @param newStateCode код нового состояния
     * @return true - возможно изменить состояние, иначе - false
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    boolean isCanChangeStateEppObject(EplStudentSummary summary, String newStateCode);

    /**
     * Изменение состояния сводки
     *
     * @param summary      сводка
     * @param oldStateCode код старого состояния
     * @param newStateCode код нового состояния
     */
    void changeSummaryState(EplStudentSummary summary, String oldStateCode, String newStateCode);

    /**
     * Возвращает враппер текущего состояния сводки
     *
     * @param summary          сводка
     * @param currentStateCode проверяемый код состояния
     * @param needNext         необходим переход к следующему состоянию, иначе - к предыдущему
     * @return враппер по текущему состоянию сводки
     */
    EplStateDao.EplStudentSummaryStateWrapper getSummaryStateTitle(EplStudentSummary summary, String currentStateCode, boolean needNext);

    /**
     * Приоритет текущего состояния больше указанного
     *
     * @param currentState текущиее состояние
     * @param stateCode    код заданного состояния
     * @return true - если приоритет больше, иначе - false
     */
    boolean isCurrentStateMore(EplStudentSummaryState currentState, String stateCode);

    /**
     * Все расчеты выбранной сводки находятся в выбранном состоянии или
     * все выбранные расчеты находятся в выбранном состоянии
     * @param summary   сводка
     * @param stateCode состояние
     * @param eplOrgUnitSummaryIds id выбранных расчетов
     * @return true - если все согласованы, иначе - false
     */
    boolean isAllOuSummaryEqStatus(EplStudentSummary summary, String stateCode, List<Long> eplOrgUnitSummaryIds);

    /**
     * Проверка возможности смены состояния расчета
     * @return true - возможно изменить состояние, иначе - false
     */
    boolean isCanChangeStateOuSummary(EplOrgUnitSummary summary);
}
