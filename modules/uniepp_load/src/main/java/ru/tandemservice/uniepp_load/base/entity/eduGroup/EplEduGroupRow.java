package ru.tandemservice.uniepp_load.base.entity.eduGroup;

import org.tandemframework.core.common.IEntityDebugTitled;
import org.tandemframework.core.common.ITitled;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.gen.EplEduGroupRowGen;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;


/**
 * Строка План. потока
 */
public class EplEduGroupRow extends EplEduGroupRowGen implements ITitled, IEntityDebugTitled
{
    @Override
    public String getTitle()
    {
        if (getStudentWP2GTypeSlot() == null) {
            return this.getClass().getSimpleName();
        }
        final EplStudent student = getStudentWP2GTypeSlot().getStudentWPSlot().getStudent();
        final EplGroup group = student.getGroup();
        final EducationOrgUnit eou = student.getEducationOrgUnit();
        final EppEduGroupSplitBaseElement splitElement = getStudentWP2GTypeSlot().getSplitElement();

        return UniStringUtils.joinWithSeparator(
                ", ",
                getCount() + " — " + group.getTitle(),
                group.getCourse().getTitle() + " курс",
                eou.getProgramSubjectShortExtendedTitle(),
                student.getCompensationSource().getTitle(),
                getStudentWP2GTypeSlot().getStudentWPSlot().getActivityElementPart().getShortTitle(),
                splitElement == null ? "" : splitElement.getTitle()
        );
    }

    @Override
    public String getEntityDebugTitle()
    {
        return "План. поток: " + getGroup().getEntityDebugTitle() + " | Строка: " + getTitle();
    }
}