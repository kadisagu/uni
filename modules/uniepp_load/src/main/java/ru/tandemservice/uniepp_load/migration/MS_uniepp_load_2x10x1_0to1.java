package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_load_2x10x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eplTimeRuleEduGroupScript

		// создано обязательное свойство counterEnabled
		{
			if (!tool.columnExists("epl_time_rule_edugrp_base_t", "counterenabled_p"))
			{
				// создать колонку
				tool.createColumn("epl_time_rule_edugrp_base_t", new DBColumn("counterenabled_p", DBType.BOOLEAN));
				// задать значение по умолчанию
				tool.executeUpdate("update epl_time_rule_edugrp_base_t set counterenabled_p=? where counterenabled_p is null", false);
				// сделать колонку NOT NULL
				tool.setColumnNullable("epl_time_rule_edugrp_base_t", "counterenabled_p", false);
			}
		}

		// создано обязательное свойство totalTimeWork
		{
			if (!tool.columnExists("epl_time_rule_edugrp_base_t", "totaltimework_p"))
			{
				// создать колонку
				tool.createColumn("epl_time_rule_edugrp_base_t", new DBColumn("totaltimework_p", DBType.LONG));
				// задать значение по умолчанию
				tool.executeUpdate("update epl_time_rule_edugrp_base_t set totaltimework_p=? where totaltimework_p is null", 0);
				// сделать колонку NOT NULL
				tool.setColumnNullable("epl_time_rule_edugrp_base_t", "totaltimework_p", false);
			}
		}

		// создано обязательное свойство requestCount
		{
			if (!tool.columnExists("epl_time_rule_edugrp_base_t", "requestcount_p"))
			{
				// создать колонку
				tool.createColumn("epl_time_rule_edugrp_base_t", new DBColumn("requestcount_p", DBType.INTEGER));
				// задать значение по умолчанию
				tool.executeUpdate("update epl_time_rule_edugrp_base_t set requestcount_p=? where requestcount_p is null", 0);
				// сделать колонку NOT NULL
				tool.setColumnNullable("epl_time_rule_edugrp_base_t", "requestcount_p", false);
			}
		}
    }
}