/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EduGroupTab;

import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.support.IUISettings;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.catalog.bo.StudentCatalogs.StudentCatalogsManager;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWP2GTypeSlot;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.sum;

/**
 * @author oleyba
 * @since 10/24/11
 */
public class EplEduGroupDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String COLUMN_ROWS = "rowColumn";
    public static final String COLUMN_PPS = "ppsColumn";
    public static final String COLUMN_SPLIT_VARIANT = "splitVariant";
    public static final String COLUMN_SPLIT_ELEMENT = "splitElement";
    public static final String COLUMN_STUDENT_COUNT = "studentCount";
    public static final String COLUMN_LOAD = "load";
    public static final String COLUMN_CONTROL = "control";
    public static final String PARAM_HAS_ERRORS = "hasErrors";
    public static final String ALOAD = "al";
    public static final String ICONTROL = "ic";
    public static final String FCONTROL = "fc";
    public static final String ICONTROL_TYPE = "ict";
    public static final String FCONTROL_TYPE = "fct";
    public static final String LOAD_TYPE = "lt";
    public static final String GROUP = "g";
    public static final String MODULE = "m";

    private static DQLOrderDescriptionRegistry orderReg = createOrderRegistry();

    public EplEduGroupDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final Session session = context.getSession();

        EplStudentSummary summary = context.get(EplStudentSummaryManager.PARAM_SUMMARY);
        YearDistributionPart part = context.get(EplStudentSummaryManager.PARAM_YEAR_PART);
        EppGroupType groupType = context.get(EplStudentSummaryManager.PARAM_GROUP_TYPE);
        IUISettings settings = context.get(EplStudentSummaryManager.PARAM_SETTINGS);
        boolean hasErrors = Boolean.TRUE.equals(settings.get(PARAM_HAS_ERRORS));

        if (null == summary || null == part || null == groupType)
            return new DSOutput(input);

        DQLSelectBuilder dql = orderReg.buildDQLSelectBuilder()
            .where(eq(property(EplEduGroup.summary().fromAlias(GROUP)), value(summary)))
            .where(eq(property(EplEduGroup.yearPart().fromAlias(GROUP)), value(part)))
            .where(eq(property(EplEduGroup.groupType().fromAlias(GROUP)), value(groupType)));


        DQLSelectBuilder rowDql = new DQLSelectBuilder().fromEntity(EplEduGroupRow.class, "row")
            .column(property("row.id"))
            .joinPath(DQLJoinType.inner, EplEduGroupRow.studentWP2GTypeSlot().fromAlias("row"), "wp2GTypeSlot")
            .joinPath(DQLJoinType.inner, EplStudentWP2GTypeSlot.studentWPSlot().fromAlias("wp2GTypeSlot"), "wpSlot")
            .joinPath(DQLJoinType.inner, EplStudentWPSlot.student().fromAlias("wpSlot"), "s")
            .where(eq(property("row", EplEduGroupRow.group()), property(GROUP)));

        boolean filterByRow = CommonBaseFilterUtil.applySelectFilter(rowDql, "s", EplStudent.educationOrgUnit().educationLevelHighSchool(), settings.get(EducationCatalogsManager.PARAM_EDU_HS));
        filterByRow |= CommonBaseFilterUtil.applySelectFilter(rowDql, "s", EplStudent.educationOrgUnit().developForm(), settings.get(EducationCatalogsManager.PARAM_DEVELOP_DORM));
        filterByRow |= CommonBaseFilterUtil.applySelectFilter(rowDql, "s", EplStudent.group().course(), settings.get(StudentCatalogsManager.PARAM_COURSE));
        filterByRow |= CommonBaseFilterUtil.applySelectFilter(rowDql, "s", EplStudent.compensationSource(), settings.get(EplStudentSummaryManager.PARAM_COMPENSATION_SOURCE));
        filterByRow |= CommonBaseFilterUtil.applySelectFilter(rowDql, "row", EplEduGroupRow.studentWP2GTypeSlot().splitElement().splitVariant(), context.get(EplStudentSummaryManager.PARAM_SPLIT_VARIANT));
        filterByRow |= CommonBaseFilterUtil.applySelectFilter(rowDql, "row", EplEduGroupRow.studentWP2GTypeSlot().splitElement(), context.get(EplStudentSummaryManager.PARAM_SPLIT_ELEMENT));

        IEntity discipline = settings.get(EplStudentSummaryManager.PARAM_DISCIPLINE);
        IEntity readingOrgUnit = settings.get("readingOrgUnit");
        if (null != discipline) {
            rowDql.where(or(
                eq(property("wpSlot", EplStudentWPSlot.activityElementPart()), value(discipline)),
                eq(property("row", EplEduGroupRow.group().registryElementPart()), value(discipline))
            ));
            filterByRow = true;
        } else if (null != readingOrgUnit) {
            rowDql.where(or(
                    exists(EppRegistryElementPart.class,
                           EppRegistryElementPart.id().s(), property("wpSlot", EplStudentWPSlot.activityElementPart()),
                           EppRegistryElementPart.registryElement().owner().s(), readingOrgUnit
                    ),
                    exists(EplEduGroup.class,
                           EplEduGroup.id().s(), property("row", EplEduGroupRow.group()),
                           EplEduGroup.registryElementPart().registryElement().owner().s(), readingOrgUnit
                    )
            ));
            filterByRow = true;
        }

        IEntity formativeOrgUnit = settings.get(EplStudentSummaryManager.PARAM_FORMATIVE_ORG_UNIT);
        IEntity formativeOrOwnerOrgUnit = context.get(EplStudentSummaryManager.PARAM_FORMATIVE_OR_OWNER_ORG_UNIT);
        if (null != formativeOrOwnerOrgUnit)
        {
            rowDql.where(or(
                    eq(property("wpSlot", EplStudentWPSlot.student().educationOrgUnit().formativeOrgUnit()), value(formativeOrOwnerOrgUnit)),
                    exists(EppRegistryElementPart.class,
                           EppRegistryElementPart.id().s(), property("wpSlot", EplStudentWPSlot.activityElementPart()),
                           EppRegistryElementPart.registryElement().owner().s(), formativeOrOwnerOrgUnit
                    ),
                    exists(EplEduGroup.class,
                           EplEduGroup.id().s(), property("row", EplEduGroupRow.group()),
                           EplEduGroup.registryElementPart().registryElement().owner().s(), formativeOrOwnerOrgUnit
                    )
            ));
            filterByRow = true;
        }
        else if (null != formativeOrgUnit)
        {
            CommonBaseFilterUtil.applySelectFilter(rowDql, "s", EplStudent.educationOrgUnit().formativeOrgUnit(), formativeOrgUnit);
            filterByRow = true;
        }

        String groupTitle = settings.get("group") != null ? ((DataWrapper) settings.get("group")).getTitle() : null;
        if(!StringUtils.isEmpty(groupTitle))
        {
            rowDql.where(eq(property("s", EplStudent.group().title()), value(groupTitle)));
            filterByRow |= true;
        }

        if (hasErrors)
        {
            DQLSelectBuilder errorBuilder = EplStudentSummaryManager.instance().dao().getEplStudentWP2GTypeSlotErrorNonEqualCountBuilder("slot", summary);
            rowDql.where(in(property("wp2GTypeSlot.id"), errorBuilder.buildQuery()));
            filterByRow |= true;
        }

        if (filterByRow)
        {
            dql.where(or(
                    hasErrors ? notExists(EplEduGroupRow.class, EplEduGroupRow.group().s(), property(GROUP)) : null,
                    exists(rowDql.buildQuery())
            ));
        }

        orderReg.applyOrder(dql, input.getEntityOrder());


        DSOutput output = DQLSelectOutputBuilder.get(input, dql, session).build();

        Iterables.partition(DataWrapper.wrap(output, "id", "title"), DQL.MAX_VALUES_ROW_NUMBER).forEach(wrappers -> {
            Collection<Long> ids = CommonDAO.ids(wrappers);

            final List<EplEduGroupRow> rows = new DQLSelectBuilder()
                .fromEntity(EplEduGroupRow.class, "rel").column("rel")
                .where(in(property(EplEduGroupRow.group().id().fromAlias("rel")), ids))
                .createStatement(session).list();

            final Map<Long, Set<EppEduGroupSplitBaseElement>> group2SplitElementMap = Maps.newHashMap();
            final Map<Long, Set<EppEduGroupSplitVariant>> group2SplitVariantMap = Maps.newHashMap();
            final Map<Long, Long> group2StudentNumber = Maps.newHashMap();
            rows.forEach(row -> {
                Long groupId = row.getGroup().getId();
                EppEduGroupSplitBaseElement splitElement = row.getStudentWP2GTypeSlot().getSplitElement();

                SafeMap.safeGet(group2SplitElementMap, groupId, HashSet.class).add(splitElement);
                SafeMap.safeGet(group2SplitVariantMap, groupId, HashSet.class).add(splitElement == null ? null : splitElement.getSplitVariant());
                Long sum = group2StudentNumber.get(groupId);
                group2StudentNumber.put(groupId, row.getCount() + (sum == null ? 0 : sum));
            });

            final List<Object[]> pps = new DQLSelectBuilder()
                .fromEntity(EplEduGroup.class, GROUP).column(GROUP + ".id")
                .fromEntity(EplPlannedPps.class, "pps").column("pps")
                .where(in(property(GROUP, EplEduGroup.id()), ids))
                .where(exists(new DQLSelectBuilder()
                    .fromEntity(EplPlannedPpsTimeItem.class, "pi")
                    .where(eq(property("pi", EplPlannedPpsTimeItem.pps()), property("pps")))
                    .fromEntity(EplEduGroupTimeItem.class, "gi")
                    .where(eq(property("pi", EplPlannedPpsTimeItem.timeItem()), property("gi")))
                    .where(eq(property("gi", EplEduGroupTimeItem.eduGroup()), property(GROUP)))
                    .column("pi.id")
                    .buildQuery()))
                .createStatement(session).list();

            final Map<Long, Long> loadMap = getLoadMap(session, ids);
            final Map<Long, String> controlMap = getControlMap(session, ids);

            for (final DataWrapper wrapper : wrappers) {

                wrapper.put(COLUMN_ROWS, rows.stream()
                    .filter(r -> wrapper.getId().equals(r.getGroup().getId()))
                    .sorted(ITitled.TITLED_COMPARATOR)
                    .map(EplEduGroupRow::getTitle)
                    .collect(Collectors.joining("\n")));
                wrapper.put(COLUMN_PPS, pps.stream()
                    .filter(r -> wrapper.getId().equals(r[0]))
                    .map(r -> ((EplPlannedPps) r[1]).getDisplayableTitle())
                    .sorted()
                    .collect(Collectors.joining("\n")));
                wrapper.put(COLUMN_SPLIT_VARIANT, rows.stream()
                    .filter(r -> {
                        if (!wrapper.getId().equals(r.getGroup().getId())) return false;
                        Set<EppEduGroupSplitVariant> variants = group2SplitVariantMap.get(r.getGroup().getId());
                        return !variants.contains(null) && variants.size() == 1;
                    })
                    .map(r -> group2SplitVariantMap.get(r.getGroup().getId()).iterator().next().getShortTitle())
                    .collect(Collectors.toSet()));
                wrapper.put(COLUMN_SPLIT_ELEMENT, rows.stream()
                    .filter(r -> {
                        if (!wrapper.getId().equals(r.getGroup().getId())) return false;
                        Set<EppEduGroupSplitBaseElement> elements = group2SplitElementMap.get(r.getGroup().getId());
                        return !elements.contains(null) && elements.size() == 1;
                    })
                    .map(r -> group2SplitElementMap.get(r.getGroup().getId()).iterator().next().getSplitElementShortTitle())
                    .collect(Collectors.toSet()));
                Long students = group2StudentNumber.get(wrapper.getId());
                if (students == null || students == 0)
                    students = null;
                wrapper.put(COLUMN_STUDENT_COUNT, students);

                wrapper.put(COLUMN_LOAD, loadMap.get(wrapper.getId()));
                wrapper.put(COLUMN_CONTROL, controlMap.get(wrapper.getId()));
            }
        });

        sortByLoad(input, output);
        return output;
    }

    private void sortByLoad(DSInput input, DSOutput output)
    {
        if (COLUMN_LOAD.equals(input.getEntityOrder().getColumnName()))
        {
            output.getRecordList().sort((a, b) -> {
                Long aVal = (Long)((DataWrapper)a).getProperty(COLUMN_LOAD);
                Long bVal = (Long)((DataWrapper)b).getProperty(COLUMN_LOAD);
                return (OrderDirection.asc == input.getEntityOrder().getDirection() ? 1 : -1) *
                        (aVal == null && bVal == null ? 0 : aVal == null ? -1 : aVal.compareTo(bVal));
            } );
        }
    }

    private Map<Long, String> getControlMap(Session session, Collection<Long> ids)
    {
        final HashMap<Long, String> map = new HashMap<>();
        final List<Object[]> byGroup =
                new DQLSelectBuilder()
                        .column(GROUP + ".id")
                        .column(property(ICONTROL, EppRegistryModuleIControlAction.controlAction().abbreviation()))

                        .fromEntity(EppRegistryModuleIControlAction.class, ICONTROL)
                        .joinEntity(ICONTROL, DQLJoinType.inner, EppIControlActionType.class, ICONTROL_TYPE, false, eq(property(ICONTROL, EppRegistryModuleIControlAction.controlAction().id()), property(ICONTROL_TYPE, EppIControlActionType.id())))
                        .joinEntity(ICONTROL_TYPE, DQLJoinType.inner, EplEduGroup.class, GROUP,
                                and(
                                        eq(property(ICONTROL_TYPE, EppIControlActionType.defaultGroupType().id()), property(GROUP, EplEduGroup.groupType().id())),
                                        in(property(GROUP, EplEduGroup.id()), ids)
                                ))
                        .joinEntity(GROUP, DQLJoinType.inner, EppRegistryElementPartModule.class, MODULE, false,
                                and(
                                        eq(property(GROUP, EplEduGroup.registryElementPart().id()), property(MODULE, EppRegistryElementPartModule.part().id())),
                                        eq(property(ICONTROL, EppRegistryModuleIControlAction.module().id()), property(MODULE, EppRegistryElementPartModule.module().id()))))
                        .unionAll(

                                ((DQLSelectFragmentBuilder) new DQLSelectFragmentBuilder()
                                        .column(GROUP + ".id")
                                        .column(property(FCONTROL_TYPE, EppFControlActionType.abbreviation()))

                                        .fromEntity(EppFControlActionType.class, FCONTROL_TYPE)
                                        .joinEntity(FCONTROL_TYPE, DQLJoinType.inner, EplEduGroup.class, GROUP,
                                                and(
                                                        eq(property(FCONTROL_TYPE, EppFControlActionType.eppGroupType().id()), property(GROUP, EplEduGroup.groupType().id())),
                                                        in(property(GROUP, EplEduGroup.id()), ids)
                                                ))
                                        .joinEntity(GROUP, DQLJoinType.inner, EppRegistryElementPartFControlAction.class, FCONTROL, false,
                                                and(
                                                        eq(property(GROUP, EplEduGroup.registryElementPart().id()), property(FCONTROL, EppRegistryElementPartFControlAction.part().id())),
                                                        eq(property(FCONTROL_TYPE, EppFControlActionType.id()), property(FCONTROL, EppRegistryElementPartFControlAction.controlAction().id()))))
                                ).buildSelectRule()

                        )
                        .createStatement(session).list();
        byGroup.forEach(objs -> {
            final Long id = (Long) objs[0];
            String text = map.get(id);
            final String value = objs[1].toString();
            map.put(id, text == null ? value : text + ", " + value);
        });
        return map;
    }

    private Map<Long, Long> getLoadMap(Session session, Collection<Long> ids)
    {
        final HashMap<Long, Long> map = new HashMap<>();
        final List<Object[]> byGroup = new DQLSelectBuilder().fromEntity(EppRegistryModuleALoad.class, ALOAD)
                .joinEntity(ALOAD, DQLJoinType.inner, EppALoadType.class, LOAD_TYPE, false, eq(property(ALOAD, EppRegistryModuleALoad.loadType()), property(LOAD_TYPE, EppALoadType.id())))
                .joinEntity(LOAD_TYPE, DQLJoinType.inner, EplEduGroup.class, GROUP,
                        and(
                                eq(property(LOAD_TYPE, EppALoadType.eppGroupType().id()), property(GROUP, EplEduGroup.groupType().id())),
                                in(property(GROUP, EplEduGroup.id()), ids)
                        ))
                .joinEntity(GROUP, DQLJoinType.inner, EppRegistryElementPartModule.class,  MODULE, false,
                        and(
                                eq(property(GROUP, EplEduGroup.registryElementPart().id()), property(MODULE, EppRegistryElementPartModule.part().id())),
                                eq(property(ALOAD, EppRegistryModuleALoad.module().id()), property(MODULE, EppRegistryElementPartModule.module().id()))))
                .column(GROUP + ".id")
                .column(sum(property(ALOAD, EppRegistryModuleALoad.load())))
                .group(GROUP + ".id").createStatement(session).list();
        byGroup.forEach(objs -> map.put((Long)objs[0], (Long)objs[1]));
        return map;
    }

    private static DQLOrderDescriptionRegistry createOrderRegistry()
    {
        return new DQLOrderDescriptionRegistry(EplEduGroup.class, GROUP)
            ;
    }
}
