package ru.tandemservice.uniepp_load.base.entity.studentSummary.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentCount4SplitElement;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Число план. студентов для элемента деления потоков
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplStudentCount4SplitElementGen extends EntityBase
 implements INaturalIdentifiable<EplStudentCount4SplitElementGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentCount4SplitElement";
    public static final String ENTITY_NAME = "eplStudentCount4SplitElement";
    public static final int VERSION_HASH = -392242980;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT2_WORK_PLAN = "student2WorkPlan";
    public static final String L_REG_ELEMENT = "regElement";
    public static final String L_SPLIT_ELEMENT = "splitElement";
    public static final String P_STUDENT_COUNT = "studentCount";

    private EplStudent2WorkPlan _student2WorkPlan;     // РУП для план студента
    private EppRegistryElement _regElement;     // Элемент реестра
    private EppEduGroupSplitBaseElement _splitElement;     // Элемент деления потоков
    private int _studentCount;     // Число студентов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return РУП для план студента. Свойство не может быть null.
     */
    @NotNull
    public EplStudent2WorkPlan getStudent2WorkPlan()
    {
        return _student2WorkPlan;
    }

    /**
     * @param student2WorkPlan РУП для план студента. Свойство не может быть null.
     */
    public void setStudent2WorkPlan(EplStudent2WorkPlan student2WorkPlan)
    {
        dirty(_student2WorkPlan, student2WorkPlan);
        _student2WorkPlan = student2WorkPlan;
    }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElement getRegElement()
    {
        return _regElement;
    }

    /**
     * @param regElement Элемент реестра. Свойство не может быть null.
     */
    public void setRegElement(EppRegistryElement regElement)
    {
        dirty(_regElement, regElement);
        _regElement = regElement;
    }

    /**
     * @return Элемент деления потоков. Свойство не может быть null.
     */
    @NotNull
    public EppEduGroupSplitBaseElement getSplitElement()
    {
        return _splitElement;
    }

    /**
     * @param splitElement Элемент деления потоков. Свойство не может быть null.
     */
    public void setSplitElement(EppEduGroupSplitBaseElement splitElement)
    {
        dirty(_splitElement, splitElement);
        _splitElement = splitElement;
    }

    /**
     * @return Число студентов. Свойство не может быть null.
     */
    @NotNull
    public int getStudentCount()
    {
        return _studentCount;
    }

    /**
     * @param studentCount Число студентов. Свойство не может быть null.
     */
    public void setStudentCount(int studentCount)
    {
        dirty(_studentCount, studentCount);
        _studentCount = studentCount;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EplStudentCount4SplitElementGen)
        {
            if (withNaturalIdProperties)
            {
                setStudent2WorkPlan(((EplStudentCount4SplitElement)another).getStudent2WorkPlan());
                setRegElement(((EplStudentCount4SplitElement)another).getRegElement());
                setSplitElement(((EplStudentCount4SplitElement)another).getSplitElement());
            }
            setStudentCount(((EplStudentCount4SplitElement)another).getStudentCount());
        }
    }

    public INaturalId<EplStudentCount4SplitElementGen> getNaturalId()
    {
        return new NaturalId(getStudent2WorkPlan(), getRegElement(), getSplitElement());
    }

    public static class NaturalId extends NaturalIdBase<EplStudentCount4SplitElementGen>
    {
        private static final String PROXY_NAME = "EplStudentCount4SplitElementNaturalProxy";

        private Long _student2WorkPlan;
        private Long _regElement;
        private Long _splitElement;

        public NaturalId()
        {}

        public NaturalId(EplStudent2WorkPlan student2WorkPlan, EppRegistryElement regElement, EppEduGroupSplitBaseElement splitElement)
        {
            _student2WorkPlan = ((IEntity) student2WorkPlan).getId();
            _regElement = ((IEntity) regElement).getId();
            _splitElement = ((IEntity) splitElement).getId();
        }

        public Long getStudent2WorkPlan()
        {
            return _student2WorkPlan;
        }

        public void setStudent2WorkPlan(Long student2WorkPlan)
        {
            _student2WorkPlan = student2WorkPlan;
        }

        public Long getRegElement()
        {
            return _regElement;
        }

        public void setRegElement(Long regElement)
        {
            _regElement = regElement;
        }

        public Long getSplitElement()
        {
            return _splitElement;
        }

        public void setSplitElement(Long splitElement)
        {
            _splitElement = splitElement;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EplStudentCount4SplitElementGen.NaturalId) ) return false;

            EplStudentCount4SplitElementGen.NaturalId that = (NaturalId) o;

            if( !equals(getStudent2WorkPlan(), that.getStudent2WorkPlan()) ) return false;
            if( !equals(getRegElement(), that.getRegElement()) ) return false;
            if( !equals(getSplitElement(), that.getSplitElement()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStudent2WorkPlan());
            result = hashCode(result, getRegElement());
            result = hashCode(result, getSplitElement());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStudent2WorkPlan());
            sb.append("/");
            sb.append(getRegElement());
            sb.append("/");
            sb.append(getSplitElement());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplStudentCount4SplitElementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplStudentCount4SplitElement.class;
        }

        public T newInstance()
        {
            return (T) new EplStudentCount4SplitElement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student2WorkPlan":
                    return obj.getStudent2WorkPlan();
                case "regElement":
                    return obj.getRegElement();
                case "splitElement":
                    return obj.getSplitElement();
                case "studentCount":
                    return obj.getStudentCount();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student2WorkPlan":
                    obj.setStudent2WorkPlan((EplStudent2WorkPlan) value);
                    return;
                case "regElement":
                    obj.setRegElement((EppRegistryElement) value);
                    return;
                case "splitElement":
                    obj.setSplitElement((EppEduGroupSplitBaseElement) value);
                    return;
                case "studentCount":
                    obj.setStudentCount((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student2WorkPlan":
                        return true;
                case "regElement":
                        return true;
                case "splitElement":
                        return true;
                case "studentCount":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student2WorkPlan":
                    return true;
                case "regElement":
                    return true;
                case "splitElement":
                    return true;
                case "studentCount":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student2WorkPlan":
                    return EplStudent2WorkPlan.class;
                case "regElement":
                    return EppRegistryElement.class;
                case "splitElement":
                    return EppEduGroupSplitBaseElement.class;
                case "studentCount":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplStudentCount4SplitElement> _dslPath = new Path<EplStudentCount4SplitElement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplStudentCount4SplitElement");
    }
            

    /**
     * @return РУП для план студента. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentCount4SplitElement#getStudent2WorkPlan()
     */
    public static EplStudent2WorkPlan.Path<EplStudent2WorkPlan> student2WorkPlan()
    {
        return _dslPath.student2WorkPlan();
    }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentCount4SplitElement#getRegElement()
     */
    public static EppRegistryElement.Path<EppRegistryElement> regElement()
    {
        return _dslPath.regElement();
    }

    /**
     * @return Элемент деления потоков. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentCount4SplitElement#getSplitElement()
     */
    public static EppEduGroupSplitBaseElement.Path<EppEduGroupSplitBaseElement> splitElement()
    {
        return _dslPath.splitElement();
    }

    /**
     * @return Число студентов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentCount4SplitElement#getStudentCount()
     */
    public static PropertyPath<Integer> studentCount()
    {
        return _dslPath.studentCount();
    }

    public static class Path<E extends EplStudentCount4SplitElement> extends EntityPath<E>
    {
        private EplStudent2WorkPlan.Path<EplStudent2WorkPlan> _student2WorkPlan;
        private EppRegistryElement.Path<EppRegistryElement> _regElement;
        private EppEduGroupSplitBaseElement.Path<EppEduGroupSplitBaseElement> _splitElement;
        private PropertyPath<Integer> _studentCount;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return РУП для план студента. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentCount4SplitElement#getStudent2WorkPlan()
     */
        public EplStudent2WorkPlan.Path<EplStudent2WorkPlan> student2WorkPlan()
        {
            if(_student2WorkPlan == null )
                _student2WorkPlan = new EplStudent2WorkPlan.Path<EplStudent2WorkPlan>(L_STUDENT2_WORK_PLAN, this);
            return _student2WorkPlan;
        }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentCount4SplitElement#getRegElement()
     */
        public EppRegistryElement.Path<EppRegistryElement> regElement()
        {
            if(_regElement == null )
                _regElement = new EppRegistryElement.Path<EppRegistryElement>(L_REG_ELEMENT, this);
            return _regElement;
        }

    /**
     * @return Элемент деления потоков. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentCount4SplitElement#getSplitElement()
     */
        public EppEduGroupSplitBaseElement.Path<EppEduGroupSplitBaseElement> splitElement()
        {
            if(_splitElement == null )
                _splitElement = new EppEduGroupSplitBaseElement.Path<EppEduGroupSplitBaseElement>(L_SPLIT_ELEMENT, this);
            return _splitElement;
        }

    /**
     * @return Число студентов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentCount4SplitElement#getStudentCount()
     */
        public PropertyPath<Integer> studentCount()
        {
            if(_studentCount == null )
                _studentCount = new PropertyPath<Integer>(EplStudentCount4SplitElementGen.P_STUDENT_COUNT, this);
            return _studentCount;
        }

        public Class getEntityClass()
        {
            return EplStudentCount4SplitElement.class;
        }

        public String getEntityName()
        {
            return "eplStudentCount4SplitElement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
