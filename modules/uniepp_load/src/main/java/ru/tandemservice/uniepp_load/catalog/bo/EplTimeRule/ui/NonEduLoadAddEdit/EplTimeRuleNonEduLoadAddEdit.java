/* $Id$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.ui.NonEduLoadAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryNonEduLoad;

/**
 * @author Alexey Lopatin
 * @since 22.07.2016
 */
@Configuration
public class EplTimeRuleNonEduLoadAddEdit extends BusinessComponentManager
{
    public static final String CATEGORY_DS = "categoryDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(CATEGORY_DS, getName(), EplTimeCategoryNonEduLoad.defaultSelectDSHandler(getName())))
                .create();
    }
}
