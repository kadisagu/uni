package ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 18.05.2015
 */
@Configuration
public class EplSettingsPpsLoadList extends BusinessComponentManager
{
    public static final String PPS_LOAD_DS = "ppsLoadDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PPS_LOAD_DS, ppsLoadDSColumnList(), ppsLoadDSHandler()))
                .addDataSource(EducationYearManager.instance().eduYearDSConfig())
                .create();
    }

    @Bean
    public ColumnListExtPoint ppsLoadDSColumnList()
    {
        return getColumnListExtPoint(getName(), true, "menuEplSettingsPpsLoadList");
    }

    public static ColumnListExtPoint getColumnListExtPoint(String name, boolean showYear, String editPermissionKey)
    {
        IFormatter<Long> alertFormatter = ppsLoadId -> {
            EplSettingsPpsLoad ppsLoad = DataAccessServices.dao().getNotNull(ppsLoadId);
            EducationYear year = ppsLoad.getEduYear();
            PostBoundedWithQGandQL post = ppsLoad.getPost();

            String postfix = post == null ? "без должности" : post.getTitle() + " (" + post.getPost().getEmployeeType().getShortTitle() + ")";
            return year.getTitle() + " - " + postfix;
        };

        final IColumnListExtPointBuilder builder = ColumnListExtPoint.with(name, PPS_LOAD_DS);
        return (showYear ? builder.addColumn(textColumn(EplSettingsPpsLoad.L_EDU_YEAR, EplSettingsPpsLoad.eduYear().title()).order())
                : builder)
                .addColumn(textColumn(EplSettingsPpsLoad.L_POST, EplSettingsPpsLoad.post().fullTitle().s()).order())
                .addColumn(textColumn(EplSettingsPpsLoad.P_HOURS_MIN, EplSettingsPpsLoad.hoursMin()).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).order())
                .addColumn(textColumn(EplSettingsPpsLoad.P_HOURS_MAX, EplSettingsPpsLoad.hoursMax()).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).order())
                .addColumn(headerColumn("load")
                        .addSubColumn(textColumn(EplSettingsPpsLoad.P_HOURS_ADDITIONAL, EplSettingsPpsLoad.hoursAdditional()).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).create())
                        .addSubColumn(toggleColumn(EplSettingsPpsLoad.P_STAFF_RATE_FACTOR, EplSettingsPpsLoad.staffRateFactor()).toggleOnListener("onStaffRate").toggleOffListener("offStaffRate").permissionKey(editPermissionKey).create())//getListenerParameterAsLong()
                        .addSubColumn(toggleColumn(EplSettingsPpsLoad.P_STAFF_NUMBER_FACTOR, EplSettingsPpsLoad.staffNumberFactor()).toggleOnListener("onStaffNumber").toggleOffListener("offStaffNumber").permissionKey(editPermissionKey).create()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey(editPermissionKey))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("ppsLoadDS.delete.alert", EplSettingsPpsLoad.id().s(), alertFormatter)).permissionKey(editPermissionKey))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler ppsLoadDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                EducationYear eduYear = context.get(EplSettingsPpsLoadListUI.PARAM_EDU_YEAR);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EplSettingsPpsLoad.class, "e").column("e")
                        .where(eq(property("e", EplSettingsPpsLoad.level()), value(0L)));
                if (eduYear != null)
                {
                    builder.where(eq(property("e", EplSettingsPpsLoad.eduYear()), value(eduYear)));
                }
                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().build();
            }
        };
    }
}
