/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.EduLoad;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Alexey Lopatin
 * @since 05.08.2016
 */
@Configuration
public class EplIndividualPlanEduLoad extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}
