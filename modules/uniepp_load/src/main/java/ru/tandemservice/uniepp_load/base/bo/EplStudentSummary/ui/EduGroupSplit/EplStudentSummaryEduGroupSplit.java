/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EduGroupSplit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Nikolay Fedorovskih
 * @since 29.11.2014
 */
@Configuration
public class EplStudentSummaryEduGroupSplit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .create();
    }
}