/* $Id$ */
package ru.tandemservice.uniepp_load.base.ext.Person.ui.Home;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.person.base.bo.Person.ui.Home.PersonHome;

/**
 * @author Alexey Lopatin
 * @since 11.08.2016
 */
@Configuration
public class PersonHomeExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "uniepp_load" + PersonHomeExtUI.class.getSimpleName();

    @Autowired
    private PersonHome _personHome;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_personHome.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, PersonHomeExtUI.class))
                .create();
    }

    @Bean
    public ButtonListExtension homeBLExtension()
    {
        return buttonListExtensionBuilder(_personHome.homeBLExtPoint())
                .addButton(submitButton("eplIndPlanPageButton", ADDON_NAME + ":onClickGoToIndPlanPage").visible("addon:" + ADDON_NAME + ".ppsFound").create())
                .create();
    }
}