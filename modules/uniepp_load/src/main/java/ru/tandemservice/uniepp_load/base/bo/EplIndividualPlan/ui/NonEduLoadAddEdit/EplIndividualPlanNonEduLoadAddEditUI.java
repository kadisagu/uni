/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.NonEduLoadAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.EplIndividualPlanManager;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryNonEduLoad;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleNonEduLoad;

/**
 * @author Alexey Lopatin
 * @since 03.08.2016
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "indPlan.id"),
        @Bind(key = EplIndividualPlanNonEduLoadAddEditUI.PARAM_TIME_ITEM_ID, binding = "timeItem.id")
})
public class EplIndividualPlanNonEduLoadAddEditUI extends UIPresenter
{
    public static final String PARAM_TIME_ITEM_ID = "timeItemId";

    public static final String PARAM_CATEGORY_ID = "categoryId";

    private EplIndividualPlan _indPlan = new EplIndividualPlan();
    private EplTimeItemNonEduLoad _timeItem = new EplTimeItemNonEduLoad();

    private EplTimeCategoryNonEduLoad _category;

    @Override
    public void onComponentRefresh()
    {
        _indPlan = DataAccessServices.dao().getNotNull(_indPlan.getId());

        if (isEditMode())
        {
            _timeItem = DataAccessServices.dao().getNotNull(_timeItem.getId());
            _category = _timeItem.getTimeRule().getCategory();
        }
        else
            _timeItem.setIndPlan(_indPlan);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(EplIndividualPlanNonEduLoadAddEdit.TIME_RULE_DS))
        {
            dataSource.put(PARAM_CATEGORY_ID, _category == null ? null : _category.getId());
        }
    }

    public boolean validate()
    {
        ErrorCollector errors = UserContext.getInstance().getErrorCollector();

        if (null != getTimeItem().getDateFrom() && null != getTimeItem().getDateTo() && getTimeItem().getDateTo().before(getTimeItem().getDateFrom()))
        {
            errors.add("Дата начала должна быть не позже даты окончания.", "dateFrom", "dateTo");
        }
        return errors.hasErrors();
    }

    public void onClickApply()
    {
        if (validate()) return;
        EplIndividualPlanManager.instance().dao().saveOrUpdateTimeItem(_timeItem);
        deactivate();
    }

    public boolean isEditMode()
    {
        return null != _timeItem.getId();
    }

    public boolean isParameterNeeded()
    {
        return getTimeRule() != null && getTimeRule().isParameterNeeded();
    }

    public EplTimeRuleNonEduLoad getTimeRule()
    {
        return _timeItem.getTimeRule();
    }

    // getters and setters

    public EplIndividualPlan getIndPlan()
    {
        return _indPlan;
    }

    public void setIndPlan(EplIndividualPlan indPlan)
    {
        _indPlan = indPlan;
    }

    public EplTimeItemNonEduLoad getTimeItem()
    {
        return _timeItem;
    }

    public void setTimeItem(EplTimeItemNonEduLoad timeItem)
    {
        _timeItem = timeItem;
    }

    public EplTimeCategoryNonEduLoad getCategory()
    {
        return _category;
    }

    public void setCategory(EplTimeCategoryNonEduLoad category)
    {
        _category = category;
    }
}
