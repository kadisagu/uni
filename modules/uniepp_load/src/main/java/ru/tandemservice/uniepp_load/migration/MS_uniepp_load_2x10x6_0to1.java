/* $Id$ */
package ru.tandemservice.uniepp_load.migration;

import com.google.common.collect.Maps;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_load_2x10x6_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
		        new ScriptDependency("org.tandemframework", "1.6.18"),
		        new ScriptDependency("org.tandemframework.shared", "1.10.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность eplTimeCategory

        // таблица сущности переименована
        {
            // переименовать таблицу
            tool.renameTable("epl_time_category_t", "epl_time_cat_t");
        }

        // создано свойство userCode
        {
            // создать колонку
            tool.createColumn("epl_time_cat_t", new DBColumn("usercode_p", DBType.createVarchar(255)));
        }

        // создано свойство disabledDate
        {
            // создать колонку
            tool.createColumn("epl_time_cat_t", new DBColumn("disableddate_p", DBType.TIMESTAMP));
        }

        //  свойство title стало обязательным
        {
            // для категорий с не уникальными сокр. названиями задавать сокр. название как "[title]"
            tool.executeUpdate("update epl_time_cat_t set shorttitle_p = title_p where shorttitle_p in (select shorttitle_p from epl_time_cat_t group by shorttitle_p having count(shorttitle_p) > 1)");

            // для категорий без названия заполнить значение по умолчанию как "code"
            tool.executeUpdate("update epl_time_cat_t set title_p = code_p where title_p is null");

            // сделать колонку NOT NULL
            tool.setColumnNullable("epl_time_cat_t", "title_p", false);
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eplTimeCategoryEduLoad

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("epl_time_cat_edu_load_t",
				      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_epltimecategoryeduload")
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("eplTimeCategoryEduLoad");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eplTimeCategoryNonEduLoad

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("epl_time_cat_non_edu_load_t",
				      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_epltimecategorynoneduload")
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("eplTimeCategoryNonEduLoad");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eplTimeRuleEduLoad

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("epl_time_rule_edu_load_t",
				      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_epltimeruleeduload"),
				      new DBColumn("category_id", DBType.LONG).setNullable(false),
				      new DBColumn("grouping_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("eplTimeRuleEduLoad");

            // заполняем значениями из epl_time_rule_t
            PreparedStatement insert = tool.prepareStatement("insert into epl_time_rule_edu_load_t (id, category_id, grouping_id) values (?, ?, ?)");

            Statement stmt = tool.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("select id, category_id, grouping_id from epl_time_rule_t");
            while(rs.next())
            {
                Long id = rs.getLong(1);
                Long categoryId = rs.getLong(2);
                Long groupingId = rs.getLong(3);

                insert.setLong(1, id);
                insert.setLong(2, categoryId);
                insert.setLong(3, groupingId);
                insert.executeUpdate();
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eplTimeRuleNonEduLoad

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("epl_time_rule_non_edu_load_t",
				      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_epltimerulenoneduload"),
				      new DBColumn("category_id", DBType.LONG).setNullable(false),
				      new DBColumn("coefficient_p", DBType.DOUBLE).setNullable(false),
				      new DBColumn("parameterneeded_p", DBType.BOOLEAN).setNullable(false),
				      new DBColumn("parametername_p", DBType.createVarchar(255))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("eplTimeRuleNonEduLoad");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eplTimeRule

        // для норм без названия, задать название как "[catalogCode] [code]".
        tool.executeUpdate("update epl_time_rule_t set title_p = " + tool.createStringConcatenationSQL("catalogcode_p", "' '", "code_p") + " where title_p is null");

        // для норм с одинаковыми названиями и кодом справочника задать название как "[title] [code]".
        Statement stmt = tool.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery("select title_p, catalogcode_p from epl_time_rule_t group by title_p, catalogcode_p having count(title_p) > 1");
        while(rs.next())
        {
            String title = rs.getString(1);
            String catalogCode = rs.getString(2);
            tool.executeUpdate("update epl_time_rule_t set title_p = " + tool.createStringConcatenationSQL("title_p", "' '", "code_p") + " where title_p = ? and catalogcode_p = ?", title, catalogCode);
        }

        // перебрасываем ссылки на категории
        tool.dropConstraint("epl_time_cat_t", "fk_parent_epltimecategory");
        tool.dropConstraint("epl_time_cat_t", "chk_class_epltimecategory");
        tool.dropConstraint("epl_time_rule_t", "fk_category_epltimerule");

        Map<Long, Long> parentOldToNewIds = Maps.newHashMap();

        String sqlPrefix = "select id, parent_id from epl_time_cat_t where parent_id ";
        changeTimeCategoryIds(tool, sqlPrefix + "is null", parentOldToNewIds);
        changeTimeCategoryIds(tool, sqlPrefix + "is not null", parentOldToNewIds);

        // удалено свойство category
        {
            // удалить колонку
            tool.dropColumn("epl_time_rule_t", "category_id");
        }

        // удалено свойство grouping
        {
            // удалить колонку
            tool.dropColumn("epl_time_rule_t", "grouping_id");
        }
    }

    private void changeTimeCategoryIds(DBTool tool, String query, Map<Long, Long> parentOldToNewIds) throws Exception
    {
        short entityCode = tool.entityCodes().ensure("eplTimeCategoryEduLoad");

        ResultSet rs = tool.getConnection().createStatement().executeQuery(query);
        while(rs.next())
        {
            Long oldId = rs.getLong(1);
            Long parentId = rs.getLong(2);
            Long newId = EntityIDGenerator.generateNewId(entityCode);

            tool.executeUpdate("insert into epl_time_cat_edu_load_t (id) values (?)", newId);

            if (0 == parentId)
            {
                parentOldToNewIds.put(oldId, newId);
                tool.executeUpdate("update epl_time_cat_t set id = ?, discriminator = ? where id = ?", newId, entityCode, oldId);
            }
            else
            {
                parentId = parentOldToNewIds.get(parentId);
                tool.executeUpdate("update epl_time_cat_t set id = ?, discriminator = ?, parent_id = ? where id = ?", newId, entityCode, parentId, oldId);
            }

            // меняем связь категории на новый id
            tool.executeUpdate("update epl_time_rule_edu_load_t set category_id = ? where category_id = ?", newId, oldId);
        }
    }
}