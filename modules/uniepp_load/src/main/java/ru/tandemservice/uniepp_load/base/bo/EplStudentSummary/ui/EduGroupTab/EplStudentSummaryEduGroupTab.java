/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EduGroupTab;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.BaseRawFormatter;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.LongAsDoubleFormatter;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic.EplStudentSummarySplitElementDSHandler;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplCompensationSource;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager.DS_YEAR_PART;
import static ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager.PARAM_SUMMARY;

/**
 * @author oleyba
 * @since 10/24/11
 */
@Configuration
public class EplStudentSummaryEduGroupTab extends BusinessComponentManager
{
    public static final String DS_EPL_EDU_GROUP = "eplEduGroupDS";
    public static final String DS_DISCIPLINE = "disciplineDS";
    public static final String DS_EDU_HS = "eduHsDS";
    public static final String DS_SUMMARY = "summaryDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .addDataSource(EplStudentSummaryManager.instance().groupTypeDSConfig())
            .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_YEAR_PART, getName(), YearDistributionPart.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) -> dql.where(exists(EplEduGroup.class,
                                                                             EplEduGroup.yearPart().id().s(), property(alias, "id"),
                                                                             EplEduGroup.summary().s(), context.<EplStudentSummary>get(PARAM_SUMMARY))
                ))).treeable(true).valueStyleSource(EducationCatalogsManager.getYearPartValueStyleSource()))
            .addDataSource(EplStudentSummaryManager.instance().orgUnitDSConfig())
            .addDataSource(selectDS(DS_DISCIPLINE, EplStudentSummaryManager.instance().disciplineDSHandler()).addColumn(EppRegistryElementPart.P_TITLE_WITH_NUMBER))
            .addDataSource(selectDS("formOuDS").handler(UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler()).addColumn(OrgUnit.fullTitle().s()))
            .addDataSource(selectDS(DS_EDU_HS, EducationCatalogsManager.instance().eduHsDSHandler()).addColumn(EducationLevelsHighSchool.fullTitleExtended().s()))
            .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
            .addDataSource(UniStudentManger.instance().courseDSConfig())
            .addDataSource(selectDS("groupDS", EplStudentSummaryManager.instance().groupComboDSHandler()))
            .addDataSource(selectDS("compSourceDS").handler(EplStudentSummaryManager.instance().compSourceDSHandler()).addColumn(EplCompensationSource.title().s()))
            .addDataSource(searchListDS(DS_EPL_EDU_GROUP, eplStudentSummaryEduGroupDSConfig(), eplStudentSummaryEduGroupDSHandler()))
            .addDataSource(CommonBaseStaticSelectDataSource.selectDS(EplStudentSummaryManager.SPLIT_VARIANT_DS, getName(), EppEduGroupSplitVariant.selectForEppRegistryElementDSHandler(getName())
                    .customize((alias, dql, context, filter) ->
                                       dql.where(exists(EplEduGroupRow.class,
                                                        EplEduGroupRow.studentWP2GTypeSlot().splitElement().splitVariant().id().s(), property(alias, "id"),
                                                        EplEduGroupRow.group().summary().s(), context.<EplStudentSummary>get(PARAM_SUMMARY))
                                       ))))
            .addDataSource(selectDS(EplStudentSummaryManager.SPLIT_ELEMENT_DS, splitElementDSHandler()).addColumn(EppEduGroupSplitBaseElement.P_SPLIT_ELEMENT_TITLE))
            .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_SUMMARY, getName(), EplStudentSummary.defaultNotArchiveSelectDSHandler(getName())
                    .customize((alias, dql, context, filter) -> EplStudentSummaryManager.instance().dao().checkStudentSummaryByOrgUnit(alias, dql, context))))
            .create();
    }

    @Bean
    public ColumnListExtPoint eplStudentSummaryEduGroupDSConfig()
    {
        return columnListExtPointBuilder(DS_EPL_EDU_GROUP)
                .addColumn(checkboxColumn("mergeCheckbox").visible("ui:editMode").disabled("ui:currentEduGroupDisabled").listener("onCheck").headerListener("onCheck"))
                .addColumn(textColumn("title").path(EplEduGroup.title().s()).order().create())
                .addColumn(publisherColumn("disc", EplEduGroup.registryElementPart().titleWithNumber().s())
                        .publisherLinkResolver(new DefaultPublisherLinkResolver()
                        {
                            @Override
                            public Object getParameters(final IEntity entity)
                            {
                                return new ParametersMap()
                                        .add(PublisherActivator.PUBLISHER_ID_KEY, ((EplEduGroup) ((DataWrapper) entity).getWrapped()).getRegistryElementPart().getRegistryElement().getId())
                                        .add("selectedTab", "disciplineRegistryElementTab")
                                        ;
                            }
                        }).order().create())
                .addColumn(textColumn(EplEduGroupDSHandler.COLUMN_LOAD, EplEduGroupDSHandler.COLUMN_LOAD).order().formatter(new LongAsDoubleFormatter(2)).create())
                .addColumn(textColumn(EplEduGroupDSHandler.COLUMN_CONTROL, EplEduGroupDSHandler.COLUMN_CONTROL).create())
                .addColumn(publisherColumn("owningUnit", EplEduGroup.registryElementPart().registryElement().owner().fullTitle())
                        .publisherLinkResolver(new DefaultPublisherLinkResolver()
                        {
                            @Override
                            public Object getParameters(final IEntity entity)
                            {
                                EplEduGroup group = (EplEduGroup) ((DataWrapper) entity).getWrapped();
                                OrgUnit orgUnit = group != null ? group.getRegistryElementPart().getRegistryElement().getOwner() : null;
                                return new ParametersMap()
                                        .add(PublisherActivator.PUBLISHER_ID_KEY, orgUnit != null ? orgUnit.getId() : null)
                                        .add("selectedPage", "eppLoadOrgUnitTab")
                                        .add("selectedSubPage", "eduGroupTab")
                                        ;
                            }
                        })
                        .order().create())
                .addColumn(textColumn(EplEduGroupDSHandler.COLUMN_PPS, EplEduGroupDSHandler.COLUMN_PPS).formatter(NewLineFormatter.NOBR_IN_LINES).create())
                .addColumn(textColumn(EplEduGroupDSHandler.COLUMN_SPLIT_VARIANT, EplEduGroupDSHandler.COLUMN_SPLIT_VARIANT).formatter(CollectionFormatter.ROW_FORMATTER).create())
                .addColumn(textColumn(EplEduGroupDSHandler.COLUMN_SPLIT_ELEMENT, EplEduGroupDSHandler.COLUMN_SPLIT_ELEMENT).formatter(CollectionFormatter.ROW_FORMATTER).create())
                .addColumn(textColumn(EplEduGroupDSHandler.COLUMN_STUDENT_COUNT, EplEduGroupDSHandler.COLUMN_STUDENT_COUNT).create())
                .addColumn(textColumn(EplEduGroupDSHandler.COLUMN_ROWS).path(EplEduGroupDSHandler.COLUMN_ROWS).formatter(
                        new BaseRawFormatter<String>()
                        {
                            @Override
                            public String format(String source)
                            {
                                // TODO Эксперимертальный костыльный способ выравнивания строк, для количества < 10 студентов и >= 10. Возможно, лучше его вообще убрать.

                                if (StringUtils.isEmpty(source))
                                {
                                    return "";
                                }

                                source = BaseRawFormatter.encode(source.trim());
                                final StringBuilder sb = new StringBuilder();

                                final String[] lines = source.split("\n");
                                for (final String line : lines)
                                {
                                    if (StringUtils.isEmpty(line))
                                    {
                                        sb.append("<div>&nbsp;</div>");
                                    } else
                                    {
                                        sb.append("<div><nobr>");
                                        if (line.indexOf(' ') == 1)
                                            sb.append("&nbsp;&nbsp;");
                                        sb.append(line);
                                        sb.append("</nobr></div>");
                                    }
                                }

                                return sb.toString();
                            }
                        }
                ).create())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEdit").disabled("ui:disabled").permissionKey("ui:editPK"))
                .addColumn(actionColumn("split", new Icon("separate", "Разделить"), "onClickSplit").disabled("ui:currentEduGroupDisabled").permissionKey("ui:editPK"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eplStudentSummaryEduGroupDSHandler()
    {
        return new EplEduGroupDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler splitElementDSHandler()
    {
        return new EplStudentSummarySplitElementDSHandler(getName())
        {
            @Override
            protected DQLSelectBuilder getBuilder(String alias, ExecutionContext context)
            {
                EplStudentSummary summary = context.get(PARAM_SUMMARY);

                DQLSelectBuilder builder = super.getBuilder(alias, context);
                builder.where(exists(EplEduGroupRow.class,
                                 EplEduGroupRow.studentWP2GTypeSlot().splitElement().id().s(), property(alias, "id"),
                                 EplEduGroupRow.group().summary().s(), summary)
                );
                return builder;
            }
        };
    }
}
