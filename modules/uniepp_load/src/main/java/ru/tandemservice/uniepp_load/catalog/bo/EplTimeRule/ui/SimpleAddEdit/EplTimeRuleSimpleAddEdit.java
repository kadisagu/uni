/* $Id:$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.ui.SimpleAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;

/**
 * @author oleyba
 * @since 11/29/14
 */
@Configuration
public class EplTimeRuleSimpleAddEdit extends BusinessComponentManager
{
    public static final String CATEGORY_DS = "categoryDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(CATEGORY_DS, getName(), EplTimeCategoryEduLoad.defaultSelectDSHandler(getName())))
                .create();
    }
}