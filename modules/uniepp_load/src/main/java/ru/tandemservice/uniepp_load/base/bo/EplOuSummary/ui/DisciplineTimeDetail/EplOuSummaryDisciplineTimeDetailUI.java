/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.DisciplineTimeDetail;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.EplOuSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCommonRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.Tab.EplOuSummaryTabUI;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTransferOrgUnitEduGroupTimeItem;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uniepp_load.catalog.entity.codes.EplTimeItemVariantGroupingCodes.BY_REG_ELEMENT;
import static ru.tandemservice.uniepp_load.catalog.entity.codes.EplTimeItemVariantGroupingCodes.BY_TIME_RULE;

/**
 * @author oleyba
 * @since 5/14/12
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "summary.id", required = true),
    @Bind(key = EplOuSummaryTabUI.PARAM_DISC_OR_RULE_KEY, binding = "discOrRuleKey", required = true),
    @Bind(key = EplOuSummaryDisciplineTimeDetailUI.PARAM_CATEGORY, binding = "category.id", required = true),
    @Bind(key = EplOuSummaryDisciplineTimeDetailUI.PARAM_TRANSFER_FORM, binding = "transferForm", required = true)
})
public class EplOuSummaryDisciplineTimeDetailUI extends UIPresenter
{
    public static final String PARAM_CATEGORY = "category";
    public static final String PARAM_TRANSFER_FORM = "transferForm";

    private EplOrgUnitSummary _summary = new EplOrgUnitSummary();
    private EppRegistryElementPart _discipline;
    private EplTimeRuleEduLoad _rule;

    private EplCommonRowWrapper.DisciplineOrRulePairKey _discOrRuleKey;
    private EplTimeCategoryEduLoad _category = new EplTimeCategoryEduLoad();
    private boolean _transferForm;

    @Override
    public void onComponentRefresh()
    {
        setSummary(IUniBaseDao.instance.get().get(EplOrgUnitSummary.class, getSummary().getId()));
        setCategory(IUniBaseDao.instance.get().get(EplTimeCategoryEduLoad.class, getCategory().getId()));

        Long disciplineId = getDiscOrRuleKey().getDisciplineId();
        Long ruleId = getDiscOrRuleKey().getRuleId();

        if (null != disciplineId)
            _discipline = IUniBaseDao.instance.get().get(EppRegistryElementPart.class, disciplineId);
        if (null != ruleId)
            _rule = IUniBaseDao.instance.get().get(EplTimeRuleEduLoad.class, ruleId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EplOuSummaryDisciplineTimeDetail.DS_DISCIPLINE_TIME_DETAIL.equals(dataSource.getName()))
        {
            dataSource.put(EplOuSummaryDisciplineTimeDetailDSHandler.PARAM_OU_SUMMARY, getSummary());
            dataSource.put(EplOuSummaryDisciplineTimeDetailDSHandler.PARAM_DISC_OR_RULE_KEY, getDiscOrRuleKey());
            dataSource.put(EplOuSummaryDisciplineTimeDetailDSHandler.PARAM_CATEGORY, getCategory());
            dataSource.put(EplOuSummaryDisciplineTimeDetailDSHandler.PARAM_TRANSFER, isTransferForm());
        }
    }

    public void onDeleteEntityFromList()
    {
        EplOuSummaryManager.instance().dao().deleteEduGroupTimeItem(getListenerParameterAsLong());
        int countRecord = _uiConfig.getDataSource(EplOuSummaryDisciplineTimeDetail.DS_DISCIPLINE_TIME_DETAIL).getCountRecord();
        if (countRecord == 1) deactivate();
    }

    // accessors

    public String getDisciplineTimeDetailDeleteAlert()
    {
        DataWrapper wrapper = getConfig().getDataSourceCurrentRecord(EplOuSummaryDisciplineTimeDetail.DS_DISCIPLINE_TIME_DETAIL);
        EplEduGroupTimeItem timeItem = wrapper.getWrapped();

        String postfix = "";
        if (timeItem instanceof EplTransferOrgUnitEduGroupTimeItem)
        {
            EplOrgUnitSummary transferTo = ((EplTransferOrgUnitEduGroupTimeItem) timeItem).getTransferredFrom();
            if (null != transferTo && !getSummary().equals(transferTo))
                postfix = ", переданные с подразделения «" + transferTo.getOrgUnit().getFullTitle() + "»";
            else if (!getSummary().equals(timeItem.getSummary()))
                postfix = ", переданные на подразделение «"+ timeItem.getSummary().getOrgUnit().getFullTitle() +"»";
        }
        return "Удалить часы нагрузки «" + timeItem.getDescription() + "»" + postfix + "?";
    }

    public boolean isHasDiscipline()
    {
        return getGroupingCode().equals(BY_REG_ELEMENT);
    }

    public boolean isHasTimeRule()
    {
        return getGroupingCode().equals(BY_TIME_RULE);
    }

    public boolean isHasTransferRow()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EplTransferOrgUnitEduGroupTimeItem.class, "e")
                .column(property("e.id")).top(1)
                .where(eq(property("e", EplEduGroupTimeItem.timeRule().category()), value(_category)))
                .where(eq(property("e", EplTransferOrgUnitEduGroupTimeItem.transferredFrom()), value(_summary)));

        FilterUtils.applySelectFilter(dql, "e", EplEduGroupTimeItem.registryElementPart(), _discipline);
        FilterUtils.applySelectFilter(dql, "e", EplEduGroupTimeItem.timeRule(), _rule);

        return ISharedBaseDao.instance.get().existsEntity(dql.buildQuery());
    }

    public String getGroupingCode()
    {
        return getDiscOrRuleKey().getGroupingCode();
    }

    public EplCommonRowWrapper.DisciplineOrRulePairKey getDiscOrRuleKey()
    {
        return _discOrRuleKey;
    }

    public void setDiscOrRuleKey(EplCommonRowWrapper.DisciplineOrRulePairKey discOrRuleKey)
    {
        _discOrRuleKey = discOrRuleKey;
    }

    public EplOrgUnitSummary getTransferTo()
    {
        return EplOuSummaryManager.instance().dao().getSummaryTransferTo(getSummary(), getDiscOrRuleKey(), getCategory());
    }

    public boolean isDeleteDisabled()
    {
        return !getSummary().getState().getCode().equals(EppState.STATE_FORMATIVE);
    }

    public EplOrgUnitSummary getSummary()
    {
        return _summary;
    }

    public void setSummary(EplOrgUnitSummary summary)
    {
        _summary = summary;
    }

    public EppRegistryElementPart getDiscipline()
    {
        return _discipline;
    }

    public EplTimeRuleEduLoad getRule()
    {
        return _rule;
    }

    public EplTimeCategoryEduLoad getCategory()
    {
        return _category;
    }

    public void setCategory(EplTimeCategoryEduLoad category)
    {
        _category = category;
    }

    public boolean isTransferForm()
    {
        return _transferForm;
    }

    public void setTransferForm(boolean transferForm)
    {
        _transferForm = transferForm;
    }
}
