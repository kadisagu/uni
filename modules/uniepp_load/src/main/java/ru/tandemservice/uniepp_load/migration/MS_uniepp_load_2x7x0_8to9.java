package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_load_2x7x0_8to9 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (tool.entityCodes().get("eplSimpleTimeRule") != null)
            tool.entityCodes().rename("eplSimpleTimeRule", "eplTimeRuleSimple");
        if (tool.entityCodes().get("eplEduGroupSimpleTimeRule") != null)
            tool.entityCodes().rename("eplEduGroupSimpleTimeRule", "eplTimeRuleEduGroupFormula");
        if (tool.entityCodes().get("eplEduGroupBaseTimeRule") != null)
            tool.entityCodes().rename("eplEduGroupBaseTimeRule", "eplTimeRuleEduGroupScript");
    }
}