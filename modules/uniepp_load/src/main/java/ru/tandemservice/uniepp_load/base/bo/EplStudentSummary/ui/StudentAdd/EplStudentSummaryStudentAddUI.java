/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.StudentAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTabUI;

/**
 * @author oleyba
 * @since 10/27/14
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "summaryHolder.id", required = true),
    @Bind(key = EplOrgUnitTabUI.BIND_ORG_UNIT_ID, binding = "orgUnitHolder.id"),
    @Bind(key = EplStudentSummaryStudentAddUI.STUDENT_ID, binding = "studentId"),
    @Bind(key = EplStudentSummaryStudentAddUI.OU_ID, binding = "ouId")

})
public class EplStudentSummaryStudentAddUI extends UIPresenter
{
    public static final String STUDENT_ID = "studentId";
    public static final String OU_ID = "ouId";
    private EntityHolder<EplStudentSummary> summaryHolder = new EntityHolder<>();
    private EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<>();

    private EplStudent student = new EplStudent();
    private Long _studentId;
    private Long _ouId;
    private String groupTitle;
    private Course course;
    private boolean _formOuDisabled;

    @Override
    public void onComponentActivate()
    {
        _formOuDisabled = getOrgUnitHolder().getId() != null;
    }

    @Override
    public void onComponentRefresh()
    {
        getSummaryHolder().refresh();
        getOrgUnitHolder().refresh();
        if (isEditMode())
        {
            student = ICommonDAO.DAO_CACHE.get().get(_studentId);
            final EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
            orgUnitHolder.setValue(educationOrgUnit.getFormativeOrgUnit());
            getSettings().set("f.territorialOrgUnit", educationOrgUnit.getTerritorialOrgUnit());
            getSettings().set("f.educationLevelHighSchool", educationOrgUnit.getEducationLevelHighSchool());
        }
        else
        {
            getSettings().set("f.territorialOrgUnit", null);
            getSettings().set("f.educationLevelHighSchool", null);
        }
        if (getOrgUnitHolder().getId() != null)
            getSettings().set("f.formativeOrgUnit", getOrgUnitHolder().getValue());
        else if (getOuId() != null)
            getSettings().set("f.formativeOrgUnit", ICommonDAO.DAO_CACHE.get().get(getOuId()));
        saveSettings();
    }

    public void onClickApply()
    {
        if (isEditMode())
            ICommonDAO.DAO_CACHE.get().saveOrUpdate(student);
        else
            EplStudentSummaryManager.instance().dao().doAddStudent(getSummaryHolder().getValue(), getStudent(), getGroupTitle(), getCourse());
        deactivate();
    }

    // presenter

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.putAll(getSettings().getAll("f."));
        dataSource.put("developPeriod", getStudent().getEducationOrgUnit() == null ? null : getStudent().getEducationOrgUnit().getDevelopPeriod());
    }

    public boolean isFormOuDisabled() {
        return _formOuDisabled;
    }

    // getters and setters


    public EplStudent getStudent()
    {
        return student;
    }

    public void setStudent(EplStudent student)
    {
        this.student = student;
    }

    public Course getCourse()
    {
        if (isEditMode())
            return getStudent().getGroup().getCourse();
        else
            return course;
    }

    public void setCourse(Course course)
    {
        if (isEditMode())
            getStudent().getGroup().setCourse(course);
        else
            this.course = course;
    }

    public String getGroupTitle()
    {
        if (isEditMode())
            return getStudent().getGroup().getTitle();
        else
            return groupTitle;
    }

    public void setGroupTitle(String groupTitle)
    {
        if (isEditMode())
            getStudent().getGroup().setTitle(groupTitle);
        else
            this.groupTitle = groupTitle;
    }

    public EntityHolder<EplStudentSummary> getSummaryHolder()
    {
        return summaryHolder;
    }

    public EntityHolder<OrgUnit> getOrgUnitHolder()
    {
        return orgUnitHolder;
    }

    public boolean isEditMode()
    {
        return _studentId != null;
    }

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        this._studentId = studentId;
    }

    public Long getOuId()
    {
        return _ouId;
    }

    public void setOuId(Long _ouId)
    {
        this._ouId = _ouId;
    }
}