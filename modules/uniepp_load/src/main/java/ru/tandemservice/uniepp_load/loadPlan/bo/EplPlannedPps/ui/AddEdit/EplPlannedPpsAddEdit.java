/* $Id$ */
package ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import ru.tandemservice.uni.base.bo.PpsEntry.PpsEntryManager;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp_load.catalog.entity.EplPlannedPpsType;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplPlannedPpsTypeCodes;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 1/28/15
 */
@Configuration
public class EplPlannedPpsAddEdit extends BusinessComponentManager
{
    public static final String DS_TYPE = "typeDS";
    public static final String DS_POST = "postDS";
    public static final String DS_TEACHER_EMPLOYEE_POST = "teacherEpDS";
    public static final String DS_TEACHER_TIME_WORKER = "teacherTwDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_TYPE, getName(), EplPlannedPpsType.defaultSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> {
                           boolean timeWorker = context.getNotNull(EplPlannedPpsAddEditUI.PARAM_TIME_WORKER);
                           boolean moduleExists = context.getBoolean(EplPlannedPpsAddEditUI.PARAM_MODULE_EXISTS, false);

                           List<String> typeCodes = timeWorker
                                   ? moduleExists
                                        ? Arrays.asList(EplPlannedPpsTypeCodes.TIMEWORKER, EplPlannedPpsTypeCodes.VACANCY_AS_TIMEWORKER)
                                        : Collections.singletonList(EplPlannedPpsTypeCodes.VACANCY_AS_TIMEWORKER)
                                   : Arrays.asList(EplPlannedPpsTypeCodes.STAFF, EplPlannedPpsTypeCodes.VACANCY_AS_STAFF);

                           dql.where(in(property(alias, EplPlannedPpsType.code()), typeCodes));
                           return dql;
                       })
                ))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_POST, getName(), PostBoundedWithQGandQL.defaultSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> {
                               dql.where(eq(property(alias, PostBoundedWithQGandQL.post().employeeType().code()), value(EmployeeTypeCodes.EDU_STAFF)));
                               dql.where(eq(property(alias, PostBoundedWithQGandQL.enabled()), value(Boolean.TRUE)));
                               return dql;
                        })
                ))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_TEACHER_EMPLOYEE_POST, getName(), PpsEntry.defaultSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> {
                            boolean onlyOuPps = context.getBoolean(EplPlannedPpsAddEditUI.PARAM_EP_ONLY_OU, false);

                            dql.where(instanceOf(alias, PpsEntryByEmployeePost.class));
                            dql.where(isNull(property(alias, PpsEntry.removalDate())));

                            if (onlyOuPps)
                                FilterUtils.applySelectFilter(dql, alias, PpsEntry.orgUnit().id(), context.get(EplPlannedPpsAddEditUI.PARAM_EP_ORG_UNIT_ID));

                            Long postId = context.get(EplPlannedPpsAddEditUI.PARAM_EP_POST_ID);
                            if (null != postId)
                                dql.where(exists(PpsEntryByEmployeePost.class, PpsEntryByEmployeePost.id().s(), property(alias), PpsEntryByEmployeePost.post().id().s(), value(postId)));

                            return dql;
                        })
                )
                       .addColumn(PpsEntry.fio().s())
                       .addColumn(PpsEntry.orgUnit().shortTitle().s())
                       .addColumn(PpsEntry.titlePostOrTimeWorkerData().s()))
                .addDataSource(
                        selectDS(DS_TEACHER_TIME_WORKER, PpsEntryManager.instance().timeWorkerDSHandler())
                                .addColumn(PpsEntry.fio().s())
                                .addColumn(PpsEntry.orgUnit().shortTitle().s())
                                .addColumn(PpsEntry.titlePostOrTimeWorkerData().s())
                )
                .create();
    }
}



    