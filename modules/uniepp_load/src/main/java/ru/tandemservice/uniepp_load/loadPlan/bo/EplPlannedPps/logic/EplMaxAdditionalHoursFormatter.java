package ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.logic;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.IRawFormatter;
import ru.tandemservice.uni.UniDefines;

/**
 * Created by nsvetlov on 15.03.2017.
 */
public class EplMaxAdditionalHoursFormatter implements IRawFormatter
{
    public static final EplMaxAdditionalHoursFormatter INSTANCE = new EplMaxAdditionalHoursFormatter();
    @Override
    public String format(Object source)
    {
        if (source instanceof Double[])
        {
            final Double[] values = (Double[]) source;
            final String text = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(values[0]);
            final Double limit = values[1];
            // 2 поля если мы выводим подстроку (больше)
            if (values.length == 2 && limit != null && limit != 0  && values[0] - limit > 0 )
                return "<span style=\"color:" + UniDefines.COLOR_CELL_ERROR + "\">" + text +
                        " (больше " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(limit) + ")</span>";
            // 3 поля если мы просто красим красным поле
            else if (values.length == 3 && values[2] != null && values[2] > 0)
                return "<span style=\"color:" + UniDefines.COLOR_CELL_ERROR + "\">" + text + "</span>";
            return text;
        }
        return null;
    }
}
