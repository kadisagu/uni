package ru.tandemservice.uniepp_load.base.entity.ouSummary.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTransferOrgUnitEduGroupTimeItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Переданные с другого подразделения часы нагрузки для план. потока
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplTransferOrgUnitEduGroupTimeItemGen extends EplEduGroupTimeItem
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTransferOrgUnitEduGroupTimeItem";
    public static final String ENTITY_NAME = "eplTransferOrgUnitEduGroupTimeItem";
    public static final int VERSION_HASH = 2008908032;
    private static IEntityMeta ENTITY_META;

    public static final String L_TRANSFERRED_FROM = "transferredFrom";

    private EplOrgUnitSummary _transferredFrom;     // Передано с

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Передано с. Свойство не может быть null.
     */
    @NotNull
    public EplOrgUnitSummary getTransferredFrom()
    {
        return _transferredFrom;
    }

    /**
     * @param transferredFrom Передано с. Свойство не может быть null.
     */
    public void setTransferredFrom(EplOrgUnitSummary transferredFrom)
    {
        dirty(_transferredFrom, transferredFrom);
        _transferredFrom = transferredFrom;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EplTransferOrgUnitEduGroupTimeItemGen)
        {
            setTransferredFrom(((EplTransferOrgUnitEduGroupTimeItem)another).getTransferredFrom());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplTransferOrgUnitEduGroupTimeItemGen> extends EplEduGroupTimeItem.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplTransferOrgUnitEduGroupTimeItem.class;
        }

        public T newInstance()
        {
            return (T) new EplTransferOrgUnitEduGroupTimeItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "transferredFrom":
                    return obj.getTransferredFrom();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "transferredFrom":
                    obj.setTransferredFrom((EplOrgUnitSummary) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "transferredFrom":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "transferredFrom":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "transferredFrom":
                    return EplOrgUnitSummary.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplTransferOrgUnitEduGroupTimeItem> _dslPath = new Path<EplTransferOrgUnitEduGroupTimeItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplTransferOrgUnitEduGroupTimeItem");
    }
            

    /**
     * @return Передано с. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTransferOrgUnitEduGroupTimeItem#getTransferredFrom()
     */
    public static EplOrgUnitSummary.Path<EplOrgUnitSummary> transferredFrom()
    {
        return _dslPath.transferredFrom();
    }

    public static class Path<E extends EplTransferOrgUnitEduGroupTimeItem> extends EplEduGroupTimeItem.Path<E>
    {
        private EplOrgUnitSummary.Path<EplOrgUnitSummary> _transferredFrom;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Передано с. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTransferOrgUnitEduGroupTimeItem#getTransferredFrom()
     */
        public EplOrgUnitSummary.Path<EplOrgUnitSummary> transferredFrom()
        {
            if(_transferredFrom == null )
                _transferredFrom = new EplOrgUnitSummary.Path<EplOrgUnitSummary>(L_TRANSFERRED_FROM, this);
            return _transferredFrom;
        }

        public Class getEntityClass()
        {
            return EplTransferOrgUnitEduGroupTimeItem.class;
        }

        public String getEntityName()
        {
            return "eplTransferOrgUnitEduGroupTimeItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
