/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupSplitTab;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp_load.base.bo.EplState.EplStateManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupSplitEdit.EplStudentSummaryGroupSplitEdit;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.InfoBlock.EplStudentSummaryInfoBlockUI;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplStudentSummaryStateCodes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Denis Katkov
 * @since 23.03.2016
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "summaryHolder.id"),
})
public class EplStudentSummaryGroupSplitTabUI extends UIPresenter
{
    private EntityHolder<EplStudentSummary> summaryHolder = new EntityHolder<>();
    private boolean editSplitFormEnable;

    @Override
    public void onComponentRefresh()
    {
        EplStudentSummaryInfoBlockUI.componentActivate(_uiSupport, _uiActivation, getSummary().getId());
        getSummaryHolder().refresh();
        editSplitFormEnable = EplStateManager.instance().dao().isCurrentStateMore(getSummary().getState(), EplStudentSummaryStateCodes.ATTACH_WORK_PLAN);
    }

    public void onClickEdit()
    {
        DataWrapper dw = getConfig().getDataSources().get(EplStudentSummaryGroupSplitTab.EPL_STUDENT_GROUP_SPLIT_DS).getRecordById(getListenerParameterAsLong());
        boolean enable = (boolean) dw.getProperty(EplStudentSummaryGroupSplitTab.ENABLING);

        if (!enable) {
            ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();
            errorCollector.add("Способ деления потоков мероприятия реестра не соответствует элементам деления потоков.");
            return;
        }
        EppRegistryElement discipline = ((EppRegistryElement) dw.getProperty(EplStudentSummaryGroupSplitTab.DISCIPLINE));
        EplStudentSummaryManager.instance().dao().doGenerateBaseSplitElements(discipline.getEduGroupSplitVariant());
        getActivationBuilder().asRegionDialog(EplStudentSummaryGroupSplitEdit.class)
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, ((EplStudent2WorkPlan) dw.getWrapped()).getId())
                .parameter(EplStudentSummaryGroupSplitTab.DISCIPLINE, discipline.getId())
                .activate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EplStudentSummaryManager.PARAM_SUMMARY, getSummary());
        if (dataSource.getName().equals(EplStudentSummaryGroupSplitTab.EPL_STUDENT_GROUP_SPLIT_DS)) {
            Map<String, Object> settingsMap = getSettings().getAsMap(
                    EplStudentSummaryGroupSplitTab.DISCIPLINE,
                    EplStudentSummaryManager.PARAM_SPLIT_VARIANT,
                    EplStudentSummaryManager.PARAM_SPLIT_ELEMENT,
                    EplStudentSummaryGroupSplitTab.INCORRECT_SPLIT,
                    EplStudentSummaryGroupSplitTab.NO_SPLIT
            );
            dataSource.putAll(settingsMap);
        } else {
            dataSource.put(EplStudentSummaryManager.PARAM_SPLIT_VARIANT, getSettings().get(EplStudentSummaryManager.PARAM_SPLIT_VARIANT));
        }
    }

    public void onClickSplitElementMassDelete()
    {
        BaseSearchListDataSource ds = getConfig().getDataSource(EplStudentSummaryGroupSplitTab.EPL_STUDENT_GROUP_SPLIT_DS);
        CheckboxColumn checkboxColumn = (CheckboxColumn) ds.getLegacyDataSource().getColumn(EplStudentSummaryGroupSplitTab.CHECKBOX);

        if (checkboxColumn.getSelectedObjects().size() == 0) {
            throw new ApplicationException("Необходимо выбрать хотя бы один объект из списка.");
        }
        List<Long> selectedIds = new ArrayList<>();
        checkboxColumn.getSelectedObjects().stream()
                .map(entity -> (ArrayList) entity.getProperty(EplStudentSummaryGroupSplitTab.SPLIT_ELEMENTS_ID)).collect(Collectors.toList())
                .stream().forEach(selectedIds::addAll);

        int deleteCount = EplStudentSummaryManager.instance().dao().doSplitElementMassDelete(selectedIds);
        ContextLocal.getInfoCollector().add("Удалено элементов деления: " + deleteCount + ".");
        checkboxColumn.getSelectedObjects().clear();
    }

    @SuppressWarnings("unchecked")
    public void onClickDeleteSplitElements()
    {
        DataWrapper currentWrapper = getConfig().getDataSource(EplStudentSummaryGroupSplitTab.EPL_STUDENT_GROUP_SPLIT_DS).getRecordById(getListenerParameterAsLong());
        int deleteCount = EplStudentSummaryManager.instance().dao().doSplitElementMassDelete((List<Long>)currentWrapper.getProperty(EplStudentSummaryGroupSplitTab.SPLIT_ELEMENTS_ID));
        ContextLocal.getInfoCollector().add("Удалено элементов деления: " + deleteCount + ".");
    }

    public EplStudentSummary getSummary()
    {
        return getSummaryHolder().getValue();
    }

    public void setSummary(EplStudentSummary summary)
    {
        getSummaryHolder().setValue(summary);
    }

    public EntityHolder<EplStudentSummary> getSummaryHolder()
    {
        return summaryHolder;
    }

    public boolean isEditSplitFormEnable()
    {
        return editSplitFormEnable;
    }
}