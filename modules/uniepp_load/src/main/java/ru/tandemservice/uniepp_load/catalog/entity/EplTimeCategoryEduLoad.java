package ru.tandemservice.uniepp_load.catalog.entity;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogHierarchyItem;
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeCategory.ui.EduLoadAddEdit.EplTimeCategoryEduLoadAddEdit;
import ru.tandemservice.uniepp_load.catalog.entity.gen.EplTimeCategoryEduLoadGen;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.instanceOf;

/** @see ru.tandemservice.uniepp_load.catalog.entity.gen.EplTimeCategoryEduLoadGen */
public class EplTimeCategoryEduLoad extends EplTimeCategoryEduLoadGen implements IPrioritizedCatalogHierarchyItem, IHierarchyItem
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return EplTimeCategory.defaultSelectDSHandler(ownerId).customize((alias, dql, context, filter) -> dql.where(instanceOf(alias, EplTimeCategoryEduLoad.class)));
    }

    public static final String CODE_LECTURES = "cat_lectures";
    public static final String CODE_PRACTICES = "cat_practices";
    public static final String CODE_LABS = "cat_labs";

    private static List<String> HIDDEN_PROPERTIES = ImmutableList.of(L_PARENT, P_USER_CODE);

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public Collection<String> getHiddenFields() { return HIDDEN_PROPERTIES; }
            @Override public String getAddEditComponentName() { return EplTimeCategoryEduLoadAddEdit.class.getSimpleName(); }
        };
    }

    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return (IHierarchyItem) getParent();
    }
}