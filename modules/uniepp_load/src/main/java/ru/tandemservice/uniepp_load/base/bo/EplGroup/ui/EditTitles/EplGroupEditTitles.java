/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplGroup.ui.EditTitles;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.catalog.entity.EplCompensationSource;

/**
 * @author nvankov
 * @since 3/3/15
 */
@Configuration
public class EplGroupEditTitles extends BusinessComponentManager
{
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String DEVELOP_FORM_DS = "developFormDS";
    public static final String COURSE_DS = "courseDS";
    public static final String COMPENSATION_SOURCE_DS = "compensationSourceDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(DEVELOP_FORM_DS, developFormDSHandler()))
                .addDataSource(selectDS(COURSE_DS, courseDSHandler()))
                .addDataSource(selectDS(COMPENSATION_SOURCE_DS, compensationSourceDSHandler()))
                .create();
    }



    @Bean
    public IDefaultComboDataSourceHandler formativeOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long studentSummaryId = context.getNotNull("studentSummaryId");

                dql.where(DQLExpressions.exists(
                        new DQLSelectBuilder().fromEntity(EplStudent.class, "s")
                                .where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.educationOrgUnit().formativeOrgUnit()), DQLExpressions.property("e")))
                                .where(DQLExpressions.isNotNull(DQLExpressions.property("s", EplStudent.educationOrgUnit().formativeOrgUnit().parent())))
                                .where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.group().summary().id()), DQLExpressions.value(studentSummaryId)))
                                .buildQuery()
                ));
            }
        }
                .filter(OrgUnit.title())
                .order(OrgUnit.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler developFormDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopForm.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long studentSummaryId = context.getNotNull("studentSummaryId");
                Long formativeOrgUnitId = context.get("formativeOrgUnit");

                if(formativeOrgUnitId == null)
                {
                    dql.where(DQLExpressions.eq(DQLExpressions.value(0), DQLExpressions.value(1)));
                    return;
                }

                dql.where(DQLExpressions.exists(
                        new DQLSelectBuilder().fromEntity(EplStudent.class, "s")
                                .where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.educationOrgUnit().developForm()), DQLExpressions.property("e")))
                                .where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.group().summary().id()), DQLExpressions.value(studentSummaryId)))
                                .where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.educationOrgUnit().formativeOrgUnit().id()), DQLExpressions.value(formativeOrgUnitId))).buildQuery()
                ));
            }
        }
                .filter(DevelopForm.title())
                .order(DevelopForm.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler courseDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Course.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long studentSummaryId = context.getNotNull("studentSummaryId");
                Long formativeOrgUnitId = context.get("formativeOrgUnit");
                Long developFormId = context.get("developForm_title");

                if(formativeOrgUnitId == null || developFormId == null)
                {
                    dql.where(DQLExpressions.eq(DQLExpressions.value(0), DQLExpressions.value(1)));
                    return;
                }

                dql.where(DQLExpressions.exists(
                        new DQLSelectBuilder().fromEntity(EplStudent.class, "s")
                                .where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.group().course()), DQLExpressions.property("e")))
                                .where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.group().summary().id()), DQLExpressions.value(studentSummaryId)))
                                .where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.educationOrgUnit().formativeOrgUnit().id()), DQLExpressions.value(formativeOrgUnitId)))
                                .where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.educationOrgUnit().developForm().id()), DQLExpressions.value(developFormId)))
                                .buildQuery()
                ));
            }
        }
                .filter(Course.title())
                .order(Course.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler compensationSourceDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EplCompensationSource.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long studentSummaryId = context.getNotNull("studentSummaryId");
                Long formativeOrgUnitId = context.get("formativeOrgUnit");
                Long developFormId = context.get("developForm_title");
                Long courseId = context.get("course_title");

                if(formativeOrgUnitId == null || developFormId == null)
                {
                    dql.where(DQLExpressions.eq(DQLExpressions.value(0), DQLExpressions.value(1)));
                    return;
                }

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EplStudent.class, "s")
                        .where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.compensationSource()), DQLExpressions.property("e")))
                        .where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.group().summary().id()), DQLExpressions.value(studentSummaryId)))
                        .where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.educationOrgUnit().formativeOrgUnit().id()), DQLExpressions.value(formativeOrgUnitId)))
                        .where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.educationOrgUnit().developForm().id()), DQLExpressions.value(developFormId)));

                if(courseId != null)
                    builder.where(DQLExpressions.eq(DQLExpressions.property("s", EplStudent.group().course().id()), DQLExpressions.value(courseId)));

                dql.where(DQLExpressions.exists(
                        builder.buildQuery()
                ));
            }
        }
                .filter(EplCompensationSource.title())
                .order(EplCompensationSource.title());
    }
}



    