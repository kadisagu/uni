/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EduGroupEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;

/**
 * @author Nikolay Fedorovskih
 * @since 29.11.2014
 */
@Input(
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id", required = true)
)
public class EplStudentSummaryEduGroupEditUI extends UIPresenter
{
    private EntityHolder<EplEduGroup> _holder = new EntityHolder<>();

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
    }

    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(getGroup());
        deactivate();
    }

    public EplEduGroup getGroup()
    {
        return getHolder().getValue();
    }

    public EntityHolder<EplEduGroup> getHolder()
    {
        return _holder;
    }
}