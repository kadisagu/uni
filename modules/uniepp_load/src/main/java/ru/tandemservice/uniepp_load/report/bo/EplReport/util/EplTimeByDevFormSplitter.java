/* $Id:$ */
package ru.tandemservice.uniepp_load.report.bo.EplReport.util;

import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplCompensationSourceCodes;

import java.util.Collection;

/**
 * @author oleyba
 * @since 2/17/15
 */
public class EplTimeByDevFormSplitter extends EplTimeSplitter
{
    public EplTimeByDevFormSplitter(Long summaryId)
    {
        super(summaryId);
    }

    protected Collection getPropertyValueList()
    {
        return DevelopFormCodes.CODES;
    }

    protected String getPropertyPath()
    {
        return EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().educationOrgUnit().developForm().code().s();
    }
}
