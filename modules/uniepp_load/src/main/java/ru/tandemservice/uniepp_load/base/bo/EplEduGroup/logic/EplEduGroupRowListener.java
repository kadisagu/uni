package ru.tandemservice.uniepp_load.base.bo.EplEduGroup.logic;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.shared.commonbase.base.bo.DaemonQueue.DaemonQueueManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.daemon.EplTimeItemDaemonBean;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Created by nsvetlov on 24.11.2016.
 */
public class EplEduGroupRowListener implements IDSetEventListener
{

    public static final String SUMMARY = "s";
    public static final String ORG_UNIT = "ou";
    public static final String GROUP = "g";
    public static final String ROW = "r";

    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.commitInsert, EplEduGroupRow.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.commitUpdate, EplEduGroupRow.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, EplEduGroupRow.class, this);
    }

    @Override
    public void onEvent(DSetEvent dSetEvent)
    {
        DaemonQueueManager.instance().addIds(new DQLSelectBuilder()
                .fromEntity(EplEduGroupRow.class, ROW)
                .column(EplEduGroupRow.group().id().fromAlias(ROW).s())
                .where(in(property(ROW + ".id"), dSetEvent.getMultitude().getInExpression()))
                .createStatement(dSetEvent.getContext())
                .list(), DSetEventType.beforeDelete != dSetEvent.getEventType());
        EplTimeItemDaemonBean.DAEMON.registerAfterCompleteWakeUp(dSetEvent);
    }
}
