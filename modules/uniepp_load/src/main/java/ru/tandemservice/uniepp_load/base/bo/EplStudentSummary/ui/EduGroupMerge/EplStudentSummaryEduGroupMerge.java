/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EduGroupMerge;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;

import static ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EduGroupMerge.EplStudentSummaryEduGroupMergeUI.GROUP_DS;

/**
 * @author Nikolay Fedorovskih
 * @since 20.11.2014
 */
@Configuration
public class EplStudentSummaryEduGroupMerge extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(GROUP_DS, EplStudentSummaryManager.instance().groupDSHandler()))
                .create();
    }
}