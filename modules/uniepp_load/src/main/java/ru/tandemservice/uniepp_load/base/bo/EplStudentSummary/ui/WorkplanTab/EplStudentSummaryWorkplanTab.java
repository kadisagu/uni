/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.WorkplanTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplCompensationSource;

/**
 * @author oleyba
 * @since 10/13/11
 */
@Configuration
public class EplStudentSummaryWorkplanTab extends BusinessComponentManager
{
    public static final String DS_EPL_STUDENT_WORKPLAN = "eplStudentWorkplanDS";
    public static final String DS_EDU_HS = "eduHsDS";
    public static final String DS_SUMMARY = "summaryDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS("formOuDS").handler(UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler()).addColumn(OrgUnit.fullTitle().s()))
            .addDataSource(selectDS(DS_EDU_HS, EducationCatalogsManager.instance().eduHsDSHandler()).addColumn(EducationLevelsHighSchool.fullTitleExtended().s()))
            .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
            .addDataSource(UniStudentManger.instance().courseDSConfig())
            .addDataSource(selectDS("groupDS", EplStudentSummaryManager.instance().groupComboDSHandler()))
            .addDataSource(selectDS("compSourceDS").handler(EplStudentSummaryManager.instance().compSourceDSHandler()).addColumn(EplCompensationSource.title().s()))
            .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_SUMMARY, getName(), EplStudentSummary.defaultNotArchiveSelectDSHandler(getName())
                    .customize((alias, dql, context, filter) -> EplStudentSummaryManager.instance().dao().checkStudentSummaryByOrgUnit(alias, dql, context))))
            .addDataSource(this.searchListDS(DS_EPL_STUDENT_WORKPLAN, eplStudentWorkplanDSColumns(), eplStudentWorkplanDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint eplStudentWorkplanDSColumns() {
        return this.columnListExtPointBuilder(DS_EPL_STUDENT_WORKPLAN)
            .addColumn(textColumn("course", EplStudent.group().course().title().s()).order().create())
            .addColumn(textColumn("group", EplStudent.group().title().s()).order().create())
            .addColumn(textColumn("eduHS", EplStudent.educationOrgUnit().educationLevelHighSchool().fullTitleExtended().s()).order().create())
            .addColumn(textColumn("developForm", EplStudent.educationOrgUnit().developForm().title().s()).order().create())
            .addColumn(textColumn("developCondition", EplStudent.educationOrgUnit().developCondition().shortTitle().s()).order().create())
            .addColumn(textColumn("developTech", EplStudent.educationOrgUnit().developTech().title().s()).order().create())
            .addColumn(textColumn("developPeriod", EplStudent.developGrid().title().s()).order().create())
            .addColumn(textColumn("compSource", EplStudent.compensationSource().title().s()).order().create())
            .addColumn(textColumn("count", EplStudentWorkplanDSHandler.COLUMN_COUNT).formatter(RawFormatter.INSTANCE).order().create())
            .addColumn(publisherColumn("workplan", "title").entityListProperty(EplStudentWorkplanDSHandler.COLUMN_WORKPLAN).create())
            .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).disabled("ui:disabled").permissionKey("ui:editPK"))
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eplStudentWorkplanDSHandler() {
        return new EplStudentWorkplanDSHandler(this.getName());
    }
}
