/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplGroup.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author nvankov
 * @since 3/3/15
 */
public class EplGroupDao extends UniBaseDao implements IEplGroupDao
{
    @Override
    public List<EplGroupWrapper> getEplGroupWrappers(Long studentSummaryId, Map<String, Object> filtersMap)
    {
        if(studentSummaryId == null) throw new NullPointerException();

        Long formativeOrgUnitId = (Long) filtersMap.get("formativeOrgUnit");
        Long developFormId = (Long) filtersMap.get("developForm_title");
        Long courseId = (Long) filtersMap.get("course_title");
        Long compensationSourceId = (Long) filtersMap.get("compensationSource_title");

        if(formativeOrgUnitId == null || developFormId == null)
            return Collections.emptyList();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EplStudent.class, "s")
                .where(eq(property("s", EplStudent.group().summary().id()), value(studentSummaryId)))
                .where(eq(property("s", EplStudent.educationOrgUnit().formativeOrgUnit().id()), value(formativeOrgUnitId)))
                .where(eq(property("s", EplStudent.educationOrgUnit().developForm().id()), value(developFormId)));

        if(courseId != null)
            builder.where(eq(property("s", EplStudent.group().course().id()), value(courseId)));

        if(compensationSourceId != null)
            builder.where(eq(property("s", EplStudent.compensationSource().id()), value(compensationSourceId)));

        Map<Long, List<EplStudent>> studentsMap = Maps.newHashMap();

        List<EplGroupWrapper> wrappers = Lists.newArrayList();

        for (EplStudent student : createStatement(builder).<EplStudent>list())
        {
            SafeMap.safeGet(studentsMap, student.getGroup().getId(), ArrayList.class).add(student);
        }
        for (Map.Entry<Long, List<EplStudent>> entry : studentsMap.entrySet())
        {
            EplGroup group = entry.getValue().get(0).getGroup();

            EplGroupWrapper wrapper = new EplGroupWrapper(group);
            List<EplStudent> students = entry.getValue();
            Collections.sort(students, (o1, o2) -> CommonCollator.RUSSIAN_COLLATOR.compare(EplGroupWrapper.getStudentTitle(o1), EplGroupWrapper.getStudentTitle(o2)));
            wrapper.getStudents().addAll(students);

            wrappers.add(wrapper);
        }

        Collections.sort(wrappers, (o1, o2) -> {
            int result = CommonCollator.RUSSIAN_COLLATOR.compare(o1.getGroupTitle(), o2.getGroupTitle());
            if (0 == result)
                result = Integer.compare(o1.getGroup().getCourse().getIntValue(), o2.getGroup().getCourse().getIntValue());
            return result;
        });
        return wrappers;
    }

    @Override
    public void doUpdateEplGroups(Long studentSummaryId, List<EplGroupWrapper> wrappers)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), "eplStudentGroupsUpdate_" + studentSummaryId);

        for (EplGroupWrapper wrapper : wrappers)
        {
            String title = wrapper.getGroupTitle();
            wrapper.getStudents().stream()
                    .filter(student -> !title.equals(student.getGroup().getTitle()))
                    .forEach(student -> {
                        EplGroup group = student.getGroup();
                        group.setTitle(title);
                        update(group);
                    });
        }
    }
}
