/* $Id$ */
package ru.tandemservice.uniepp_load.base.ext.Person.ui.Home;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.sec.IPrincipalContext;
import ru.tandemservice.uni.dao.pps.IPpsEntryDao;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.EplIndividualPlanManager;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.List.EplIndividualPlanList;

/**
 * @author Alexey Lopatin
 * @since 11.08.2016
 */
public class PersonHomeExtUI extends UIAddon
{
    public PersonHomeExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickGoToIndPlanPage()
    {
        getPresenter().getActivationBuilder()
                .asDesktopRoot(EplIndividualPlanList.class)
                .parameter(EplIndividualPlanManager.PARAM_PPS_ACTIVE, true)
                .activate();
    }

    public boolean isPpsFound()
    {
        IPrincipalContext principalContext = getPresenter().getConfig().getUserContext().getPrincipalContext();
        return !IPpsEntryDao.instance.get().getPpsList(principalContext).isEmpty();
    }
}