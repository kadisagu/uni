/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.EplOuSummaryDAO;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.IEplOuSummaryDAO;

/**
 * @author oleyba
 * @since 5/5/12
 */
@Configuration
public class EplOuSummaryManager extends BusinessObjectManager
{
    public static EplOuSummaryManager instance() {
        return instance(EplOuSummaryManager.class);
    }

    @Bean
    public IEplOuSummaryDAO dao() {
        return new EplOuSummaryDAO();
    }

}
