package ru.tandemservice.uniepp_load.catalog.entity;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.ui.NonEduLoadAddEdit.EplTimeRuleNonEduLoadAddEdit;
import ru.tandemservice.uniepp_load.catalog.entity.gen.EplTimeRuleNonEduLoadGen;

import static org.tandemframework.hibsupport.dql.DQLExpressions.instanceOf;

/** @see ru.tandemservice.uniepp_load.catalog.entity.gen.EplTimeRuleNonEduLoadGen */
public class EplTimeRuleNonEduLoad extends EplTimeRuleNonEduLoadGen
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return EplTimeRule.defaultSelectDSHandler(ownerId).customize((alias, dql, context, filter) -> dql.where(instanceOf(alias, EplTimeRuleNonEduLoad.class)));
    }

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public String getAddEditComponentName() { return EplTimeRuleNonEduLoadAddEdit.class.getSimpleName(); }
        };
    }
}