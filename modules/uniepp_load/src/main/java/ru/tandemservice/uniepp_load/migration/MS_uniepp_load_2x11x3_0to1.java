package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import ru.tandemservice.uniepp_load.util.EplDefines;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_load_2x11x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eplSettingsPpsLoad

		tool.executeUpdate("update epl_settings_pps_load_t set hoursMax_p = 0 where hoursMax_p < 0");
		tool.executeUpdate("update epl_settings_pps_load_t set hoursMin_p = 0 where hoursMin_p < 0");
		tool.executeUpdate("update epl_settings_pps_load_t set hoursAdditional_p = 0 where hoursAdditional_p < 0");

		tool.executeUpdate("update epl_settings_pps_load_t set hoursMax_p = 0,  hoursMin_p = 0  where hoursMin_p > hoursMax_p");

		// создано обязательное свойство staffRateFactor
		{
			// создать колонку
			tool.createColumn("epl_settings_pps_load_t", new DBColumn("staffratefactor_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultStaffRateFactor = false;
			tool.executeUpdate("update epl_settings_pps_load_t set staffratefactor_p=? where staffratefactor_p is null", defaultStaffRateFactor);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epl_settings_pps_load_t", "staffratefactor_p", false);

		}

		// создано обязательное свойство staffNumberFactor
		{
			// создать колонку
			tool.createColumn("epl_settings_pps_load_t", new DBColumn("staffnumberfactor_p", DBType.BOOLEAN));


			// задать значение по умолчанию
			java.lang.Boolean defaultStaffNumberFactor = false;
			tool.executeUpdate("update epl_settings_pps_load_t set staffnumberfactor_p=? where staffnumberfactor_p is null", defaultStaffNumberFactor);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epl_settings_pps_load_t", "staffnumberfactor_p", false);

		}

		tool.executeUpdate("insert into settings_s (owner_p, key_p, value_p) values ('general', ?, ?)", EplDefines.MAX_LOAD_SETTINGS_PREFIX + EplDefines.MAX_LOAD_SETING_NAME, Double.class.getName() + "-1.5");

		////////////////////////////////////////////////////////////////////////////////
		// сущность eplSettingsPpsLoad

		// создано обязательное свойство level
		{
			// создать колонку
			tool.createColumn("epl_settings_pps_load_t", new DBColumn("level_p", DBType.LONG));

			// задать значение по умолчанию
			Long defaultLevel = 0l;
			tool.executeUpdate("update epl_settings_pps_load_t set level_p=? where level_p is null", defaultLevel);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epl_settings_pps_load_t", "level_p", false);

		}

		// создано свойство entity
		{
			// создать колонку
			tool.createColumn("epl_settings_pps_load_t", new DBColumn("entity_p", DBType.LONG));

		}


    }
}