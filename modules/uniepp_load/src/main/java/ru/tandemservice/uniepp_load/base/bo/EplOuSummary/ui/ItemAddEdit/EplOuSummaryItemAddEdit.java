/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.ItemAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleSimple;

import static ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager.DS_YEAR_PART;

/**
 * @author oleyba
 * @since 12/9/14
 */
@Configuration
public class EplOuSummaryItemAddEdit extends BusinessComponentManager
{
    public static final String DS_TIME_RULE = "timeRuleDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(DS_TIME_RULE, timeRuleDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_YEAR_PART, getName(), YearDistributionPart.useSelectDSHandler(getName()))
                                       .treeable(true).valueStyleSource(EducationCatalogsManager.getYearPartValueStyleSource()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler timeRuleDSHandler() {
        return new EntityComboDataSourceHandler(getName(), EplTimeRuleSimple.class)
            .where(EplTimeRuleSimple.enabled(), Boolean.TRUE)
            .order(EplTimeRuleSimple.title())
            .filter(EplTimeRuleSimple.title())
            ;
    }
}