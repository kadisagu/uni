package ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 18.05.2015
 */
@Configuration
public class EplSettingsPpsLoadAddEdit extends BusinessComponentManager
{
    public static final String POST_DS = "postDS";
    public static final String ENTITY = "entity";
    public static final String LEVEL = "level";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EducationYearManager.instance().eduYearDSConfig())
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(POST_DS, getName(), PostBoundedWithQGandQL.defaultSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> {
                                dql.where(eq(property(alias, PostBoundedWithQGandQL.post().employeeType().code()), value(EmployeeTypeCodes.EDU_STAFF)));
                                dql.where(eq(property(alias, PostBoundedWithQGandQL.enabled()), value(Boolean.TRUE)));
                                return dql;
                        })
                ))
                .create();
    }
}
