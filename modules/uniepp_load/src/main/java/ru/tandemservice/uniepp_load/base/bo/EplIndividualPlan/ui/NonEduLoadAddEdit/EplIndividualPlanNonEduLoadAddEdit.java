/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.NonEduLoadAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryNonEduLoad;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleNonEduLoad;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager.DS_YEAR_PART;

/**
 * @author Alexey Lopatin
 * @since 03.08.2016
 */
@Configuration
public class EplIndividualPlanNonEduLoadAddEdit extends BusinessComponentManager
{
    public static final String CATEGORY_DS = "categoryDS";
    public static final String TIME_RULE_DS = "timeRuleDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(CATEGORY_DS, getName(), EplTimeCategoryNonEduLoad.defaultSelectDSHandler(getName())))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(TIME_RULE_DS, getName(), EplTimeRuleNonEduLoad.defaultSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> {
                            Long categoryId = context.get(EplIndividualPlanNonEduLoadAddEditUI.PARAM_CATEGORY_ID);
                            if (null == categoryId) return dql.where(nothing());
                            else
                                return dql
                                        .joinEntity(alias, DQLJoinType.inner, EplTimeRuleNonEduLoad.class, "n", eq(property(alias, "id"), property("n.id")))
                                        .where(eq(property("n", EplTimeRuleNonEduLoad.category()), value(categoryId)));
                        })))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_YEAR_PART, getName(), YearDistributionPart.useSelectDSHandler(getName()))
                                       .treeable(true).valueStyleSource(EducationCatalogsManager.getYearPartValueStyleSource()))
                .create();
    }
}