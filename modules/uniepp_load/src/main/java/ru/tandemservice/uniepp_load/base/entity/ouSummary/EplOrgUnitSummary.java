package ru.tandemservice.uniepp_load.base.entity.ouSummary;

import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.gen.EplOrgUnitSummaryGen;

import java.util.Collection;
import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/** @see ru.tandemservice.uniepp_load.base.entity.ouSummary.gen.EplOrgUnitSummaryGen */
public class EplOrgUnitSummary extends EplOrgUnitSummaryGen implements ITitled, ISecLocalEntityOwner, IEppStateObject
{
    public static final String PARAM_STUDENT_SUMMARY = "studentSummary";

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EplOrgUnitSummary.class)
                .where(EplOrgUnitSummary.studentSummary(), PARAM_STUDENT_SUMMARY)
                .titleProperty(EplOrgUnitSummary.orgUnit().fullTitle().s())
                .order(EplOrgUnitSummary.orgUnit().title())
                .filter(EplOrgUnitSummary.orgUnit().title())
                .filter(EplOrgUnitSummary.orgUnit().orgUnitType().title());
    }

    public static EntityComboDataSourceHandler readingOrgUnitDSHandler(String name)
    {
        return new EntityComboDataSourceHandler(name, OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EplOrgUnitSummary.class, "s");
                builder.where(eq(property("s", EplOrgUnitSummary.orgUnit()), property(alias)));
                FilterUtils.applySelectFilter(builder, "s", EplOrgUnitSummary.studentSummary(), context.get(PARAM_STUDENT_SUMMARY));

                dql.where(exists(builder.buildQuery()));
            }
        }
                .titleProperty(OrgUnit.fullTitle().s())
                .order(OrgUnit.title())
                .filter(OrgUnit.title())
                .filter(OrgUnit.orgUnitType().title());
    }

    @Override
    @EntityDSLSupport(parts = P_CREATION_DATE)
    public String getTitle()
    {
        if (getOrgUnit() == null) return this.getClass().getSimpleName();
        return "Расчет на " + getShortTitle() + " сводки «" + getStudentSummary().getTitleWithState() + "»";
    }

    @Override
    @EntityDSLSupport(parts = P_CREATION_DATE)
    public String getShortTitle()
    {
        if (getOrgUnit() == null) return this.getClass().getSimpleName();
        String stateTitle = getState().getTitle().toLowerCase();
        return getOrgUnit().getShortTitle() + " (" + stateTitle + ")";
    }

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return Collections.singleton((IEntity) getOrgUnit());
    }
}