/* $Id:$ */
package ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.List.EplIndividualPlanList;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EduGroupTab.EplStudentSummaryEduGroupTab;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.EplStudentSummaryGroupTab;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.WorkplanTab.EplStudentSummaryWorkplanTab;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.EplOrgUnitManager;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.SummaryTab.EplOrgUnitSummaryTab;

/**
 * @author oleyba
 * @since 12/6/14
 */
@Configuration
public class EplOrgUnitTab extends BusinessComponentManager
{
    public static final String EPP_LOAD_TAB_PANEL = "eppLoadTabPanel";
    public static final String TAB_STUDENT_SUMMARY = "eppLoadStudentSummaryTab";
    public static final String TAB_WORKPLAN = "eppLoadStudentWorkplanTab";
    public static final String TAB_EDU_GROUP = "eppLoadStudentEduGroupTab";
    public static final String TAB_OU_SUMMARY = "eppLoadStudentOuSummaryTab";
    public static final String TAB_IND_PLAN = "eplIndPlanTab";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }

    @Bean
    public TabPanelExtPoint enr14TabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(EPP_LOAD_TAB_PANEL)
            .addTab(componentTab(TAB_STUDENT_SUMMARY, EplStudentSummaryGroupTab.class)
                .permissionKey("ui:secModel.viewEplGroupTab")
                .securedObject("ui:orgUnit")
                .visible("ognl:@" + EplOrgUnitTab.class.getName() + "@showTabForming(presenter.orgUnit)")
            )
            .addTab(componentTab(TAB_WORKPLAN, EplStudentSummaryWorkplanTab.class)
                .permissionKey("ui:secModel.viewEplWpTab")
                .securedObject("ui:orgUnit")
                .visible("ognl:@" + EplOrgUnitTab.class.getName() + "@showTabForming(presenter.orgUnit)")
            )
            .addTab(componentTab(TAB_EDU_GROUP, EplStudentSummaryEduGroupTab.class)
                .permissionKey("ui:secModel.viewEplEduGroupTab")
                .securedObject("ui:orgUnit")
                .visible("ognl:@" + EplOrgUnitTab.class.getName() + "@showTabFormingOrOwner(presenter.orgUnit)")
            )
            .addTab(componentTab(TAB_OU_SUMMARY, EplOrgUnitSummaryTab.class)
                .permissionKey("ui:secModel.viewEplOuSummaryTab")
                .securedObject("ui:orgUnit")
                .visible("ognl:@" + EplOrgUnitTab.class.getName() + "@showTabOwner(presenter.orgUnit)")
            )
            .addTab(componentTab(TAB_IND_PLAN, EplIndividualPlanList.class)
                .permissionKey("ui:secModel.viewEplIndPlanTab")
                .securedObject("ui:orgUnit")
                .visible("ognl:@" + EplOrgUnitTab.class.getName() + "@showTabOwner(presenter.orgUnit)")
            )
            .create()
            ;
    }

    public static boolean showTabForming(final OrgUnit orgUnit) {
        return EplOrgUnitManager.instance().dao().isOrgUnitForming(orgUnit);
    }

    public static boolean showTabOwner(final OrgUnit orgUnit) {
        return EplOrgUnitManager.instance().dao().isOrgUnitOwner(orgUnit);
    }

    public static boolean showTabFormingOrOwner(final OrgUnit orgUnit) {
        return EplOrgUnitManager.instance().dao().isOrgUnitForming(orgUnit) || EplOrgUnitManager.instance().dao().isOrgUnitOwner(orgUnit);
    }

}