/* $Id$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.ui.EduGroupFormulaList;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.DynamicPub.CatalogDynamicPubUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupFormula;

/**
 * @author Alexey Lopatin
 * @since 31.03.2016
 */
@State({@Bind(key = DefaultCatalogPubModel.CATALOG_CODE, binding = "catalogCode")})
public class EplTimeRuleEduGroupFormulaListUI extends CatalogDynamicPubUI
{
    @Override
    protected void addActionColumns(DynamicListDataSource<ICatalogItem> dataSource)
    {
        super.addActionColumns(dataSource);

        int columnIndex = dataSource.getColumn(EplTimeRuleEduGroupFormula.P_DESCRIPTION).getNumber();
        dataSource.addColumn(new SimpleColumn("Группировка часов", EplTimeRuleEduGroupFormula.grouping().shortTitle().s()).setWidth("110px").setClickable(false), ++columnIndex);

        columnIndex = dataSource.getColumn(EplTimeRuleEduGroupFormula.L_GROUP_TYPE + ".title").getNumber();
        dataSource.addColumn(new SimpleColumn("Вид мероприятия реестра", EplTimeRuleEduGroupFormula.eppRegistryStructure().shortTitle().s()).setClickable(false), ++columnIndex);
    }
}
