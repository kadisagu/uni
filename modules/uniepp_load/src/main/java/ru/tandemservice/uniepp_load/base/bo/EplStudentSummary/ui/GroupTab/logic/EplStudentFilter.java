package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.logic;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.support.IUISettings;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.EplStudentSummaryGroupTabUI;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;

import java.util.ArrayList;
import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Набор фильтров планируемых студентов. Нужен для согласованного применения фильтров план. студента при выводе в списке план.контингента и при удалении элемента списка.
 * Строка списка соответствует не одному объекту, а нескольким план.студентам, соответствующих фильтрам, и сгруппированных по указанным признакам.
 * Если в фильтрах выбран 3 курс, а группировки по курсам нет, то чтобы не удалить лишнего, нужно гарантированно применить те же фильтры, что и при показе списка.
 * @author avedernikov
 * @since 16.12.2016
 */
public class EplStudentFilter
{
	/** Список всех возможных для применения фильтров: форм.подразделение, направление (специальность), форма освоения, курс, группа, источник возм. затрат. */
	private static final ImmutableList<FilterItem> items = ImmutableList.of(
			new FilterItem(EplStudentSummaryGroupTabUI.SETTINGS_FORMATIVE_ORGUNIT, EplStudent.educationOrgUnit().formativeOrgUnit()),
			new FilterItem(EplStudentSummaryGroupTabUI.SETTINGS_EDU_HIGHSCHOOL_LEVEL, EplStudent.educationOrgUnit().educationLevelHighSchool()),
			new FilterItem(EplStudentSummaryGroupTabUI.SETTINGS_DEVELOP_FORM, EplStudent.educationOrgUnit().developForm()),
			new FilterItem(EplStudentSummaryGroupTabUI.SETTINGS_COURSE, EplStudent.group().course()),
			new FilterItem(EplStudentSummaryGroupTabUI.SETTINGS_GROUP, EplStudent.group().title(), Group.P_TITLE),
			new FilterItem(EplStudentSummaryGroupTabUI.SETTINGS_COMPENSATION_SOURCE, EplStudent.compensationSource())
	);

	/** Выбранные значения для каждого фильтра. Фильтры сейчас только с одиночным выбором (без мультиселектов). */
	private final Collection<Pair<FilterItem, Object>> filterValues;

	/**
	 * Инициализировать значения в наборе фильтров из сеттингов.
	 * @param settings {@link UIPresenter#getSettings()}.
	 */
	public EplStudentFilter(IUISettings settings)
	{
		filterValues = new ArrayList<>();
		for (FilterItem item : items)
		{
			Object value = settings.get(item.getKey());
			if (value != null)
				filterValues.add(new Pair<>(item, value));
		}
	}

	/**
	 * Применить фильтры к запросу к план.студентам ({@link EplStudent}).
	 * @param eplStudentDql Запрос к план. студентам.
	 * @param eplStudentAlias Алиас план. студента.
	 */
	public void applyFilters(DQLSelectBuilder eplStudentDql, final String eplStudentAlias)
	{
		filterValues.forEach(pair -> pair.getX().applyFilter(eplStudentDql, eplStudentAlias, pair.getY()));
	}

	/**
	 * Описание фильтра, применяемого к план.студентам: ключ, по которому в сеттингах сохраняется значение, путь к отражаемому в фильтре свойству план.студента,
	 * а также, если фильтрация идет не по самому объекту, выбираемому в селекте, а по его параметру (например, если в селекте выбираются не сущности, а врапперы), то путь к этому параметру.
	 */
	private static class FilterItem
	{
		private final String key;
		private final MetaDSLPath path;
		private final String valueWrapperProp;

		/**
		 * Описание фильтра, когда в селекте выбирается тот объект, по которому должна быть фильтрация. Фильтрация вида {@code alias.path=settings.get(key)}.
		 * @param key Ключ, по которому выбранное значение сохраняется в сеттинге.
		 * @param path Путь к фильтруемому свойству план. студента.
		 */
		public FilterItem(String key, MetaDSLPath path)
		{
			this.key = key;
			this.path = path;
			this.valueWrapperProp = null;
		}

		/**
		 * Описание фильтра, когда фильтрация проводится по одному из параметров выбираемого в селекте объекта. Фильтрация вида {@code alias.path=settings.get(key).getProperty(valueWrapperProp)}.
		 * @param key Ключ, по которому выбранное значение сохраняется в сеттинге.
		 * @param path Путь к фильтруемому свойству план. студента.
		 * @param valueWrapperProp Параметр выбранного в селекте объекта.
		 */
		public FilterItem(String key, MetaDSLPath path, String valueWrapperProp)
		{
			this.key = key;
			this.path = path;
			this.valueWrapperProp = valueWrapperProp;
		}

		public String getKey()
		{
			return key;
		}

		/** Применить к запросу к план. студенту фильтрацию по выбранному в селекте значению. */
		public void applyFilter(DQLSelectBuilder eplStudentDql, final String eplStudentAlias, Object value)
		{
			Object actualValue = (valueWrapperProp == null) ? value : ((IEntity)value).getProperty(valueWrapperProp);
			eplStudentDql.where(eq(property(eplStudentAlias, path), commonValue(actualValue)));
		}
	}
}
