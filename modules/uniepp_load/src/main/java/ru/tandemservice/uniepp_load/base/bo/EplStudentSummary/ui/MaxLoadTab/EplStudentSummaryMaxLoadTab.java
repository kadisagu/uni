package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.MaxLoadTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.AddEdit.EplSettingsPpsLoadAddEdit.ENTITY;
import static ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.AddEdit.EplSettingsPpsLoadAddEdit.LEVEL;
import static ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.List.EplSettingsPpsLoadList.PPS_LOAD_DS;
import static ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.List.EplSettingsPpsLoadList.getColumnListExtPoint;

/**
 * @author Alexey Lopatin
 * @since 18.05.2015
 */
@Configuration
public class EplStudentSummaryMaxLoadTab extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PPS_LOAD_DS, ppsLoadDSColumnList(), ppsLoadDSHandler()))
                .addDataSource(EducationYearManager.instance().eduYearDSConfig())
                .create();
    }

    @Bean
    public ColumnListExtPoint ppsLoadDSColumnList()
    {
        return getColumnListExtPoint(getName(), false, "eplStudentSummaryMaxLoadTabEdit");
    }


    @Bean
    public IDefaultSearchDataSourceHandler ppsLoadDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Long level = context.get(LEVEL);
                Long entityId = context.get(ENTITY);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EplSettingsPpsLoad.class, "e").column("e")
                        .where(eq(property("e", EplSettingsPpsLoad.level()), value(level)))
                        .where(eq(property("e", EplSettingsPpsLoad.entity()), value(entityId)));

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().build();
            }
        };
    }
}
