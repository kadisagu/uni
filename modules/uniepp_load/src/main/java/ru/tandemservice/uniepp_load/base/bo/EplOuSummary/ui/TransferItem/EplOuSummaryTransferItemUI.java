/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.TransferItem;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.EplOuSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.EplOuSummaryDAO;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.EplOuSummaryDAO.EplEduGroupTimeItemKey;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.IEplOuSummaryDAO;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCategoryWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCommonRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplDiscOrRuleRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplBaseCategoryDataWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.Tab.EplOuSummaryTabUI;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic.EplErrorFormatter;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTransferOrgUnitEduGroupTimeItem;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.TransferItem.EplOuSummaryTransferItem.*;
import static ru.tandemservice.uniepp_load.catalog.entity.codes.EplTimeItemVariantGroupingCodes.BY_REG_ELEMENT;
import static ru.tandemservice.uniepp_load.catalog.entity.codes.EplTimeItemVariantGroupingCodes.BY_TIME_RULE;

/**
 * @author Alexey Lopatin
 * @since 30.09.2015
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "summary.id", required = true),
        @Bind(key = EplOuSummaryTabUI.PARAM_DISC_OR_RULE_KEY, binding = "discOrRuleKey"),
        @Bind(key = EplOuSummaryTabUI.PARAM_CATEGORIES, binding = "categories"),
        @Bind(key = EplOuSummaryTabUI.PARAM_TRANSFER_CATEGORIES, binding = "transferCategories")
})
public class EplOuSummaryTransferItemUI extends UIPresenter
{
    private EplOrgUnitSummary _summary = new EplOrgUnitSummary();
    private EplBaseCategoryDataWrapper _categoryDataWrapper;
    private EplCommonRowWrapper _discOrRule;
    private EplCommonRowWrapper.DisciplineOrRulePairKey _discOrRuleKey;
    private Set<Long> _categories;
    private Set<Long> _transferCategories;

    private List<EplDiscOrRuleRowWrapper> _tableDiscRows;
    private EplDiscOrRuleRowWrapper _currentDiscRow;

    private EplCategoryWrapper _category;
    private List<EplCategoryWrapper> _currentSubcategories;

    private Set<EplEduGroup> _eplGroups = Sets.newHashSet();

    private Map<EplEduGroupTimeItemKey, EplTransferOrgUnitEduGroupTimeItem> _transferTimeItemMap = Maps.newLinkedHashMap();

    private boolean _hasTimeData;

    @Override
    public void onComponentRefresh()
    {
        _summary = DataAccessServices.dao().getNotNull(_summary.getId());
        if (!_hasTimeData)
        {
            _categoryDataWrapper = EplOuSummaryManager.instance().dao().getCategoryDataWrapper(Collections.singleton(getSummary()));
            _hasTimeData = null != _categoryDataWrapper;
        }

        if (null != _discOrRuleKey)
        {
            _discOrRule = new EplDiscOrRuleRowWrapper(null, null, null, _discOrRuleKey);
            onChangeDiscOrRule();
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(BIND_ORG_UNIT_SUMMARY, _summary);
        dataSource.put(BIND_DISC_OR_RULE, _discOrRule);
        dataSource.put(BIND_CATEGORIES, _categories);
        dataSource.put(BIND_TRANSFER_CATEGORIES, _transferCategories);
        dataSource.put(BIND_TRANSFER_TIME_ITEM_MAP, _transferTimeItemMap);
        dataSource.putAll(getSettings().getAsMap(BIND_YEAR_PART, BIND_COURSE, BIND_GROUP));
    }

    public void onChangeDiscOrRule()
    {
        changeCurrentTransferData();
        onDiscTableTimeRefresh();
    }

    public void onClickTransfer()
    {
        final Collection<IEntity> selected = _uiConfig.<BaseSearchListDataSource>getDataSource(DS_POSSIBLE_TRANSFER_TIME_ITEM).getOptionColumnSelectedObjects("select");
        if (selected.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы один объект из списка.");

        Map<EplEduGroupTimeItemKey, EplTransferOrgUnitEduGroupTimeItem> items = EplOuSummaryManager.instance().dao().getTransferTimeItems(_summary, UniBaseDao.ids(selected), getSettings().get(BIND_READING_ORG_UNIT));
        _transferTimeItemMap.putAll(items);
        onDiscTableTimeRefresh();
    }

    public void onClickDeleteTransferRow()
    {
        long index = 0;
        Long deleteId = getListenerParameterAsLong();
        IEntityMeta meta = EntityRuntime.getMeta(deleteId);

        if (null != meta)
        {
            EplTransferOrgUnitEduGroupTimeItem deleteTransferTimeItem = DataAccessServices.dao().get(deleteId);
            if (null != deleteTransferTimeItem && !deleteTransferTimeItem.getSummary().getState().isFormative())
            {
                throw new ApplicationException("Состояние расчета на подразделении, на которое переданы часы, отлично от «Формируется».");
            }
        }

        for (Iterator<Map.Entry<EplEduGroupTimeItemKey, EplTransferOrgUnitEduGroupTimeItem>> it = _transferTimeItemMap.entrySet().iterator(); it.hasNext(); )
        {
            Long timeItemId = it.next().getValue().getId();
            long rowId = null != timeItemId ? timeItemId : index++;
            if (deleteId.equals(rowId)) it.remove();
        }
        onDiscTableTimeRefresh();
    }

    public void onClickSave()
    {
        onClickApply();
        deactivate();
    }

    public void onClickApply()
    {
        if (null != _discOrRule)
        {
            EplOuSummaryManager.instance().dao().doUpdateTransferTimeItems(_summary, _discOrRule.getKey(), _transferTimeItemMap.values(), _transferCategories);
        }
        changeCurrentTransferData();
        onDiscTableTimeRefresh();
    }

    public void onDiscTableTimeRefresh()
    {
        if (!isTableVisible()) return;
        fillTimeDataRows();
    }

    private void changeCurrentTransferData()
    {
        if (null != _discOrRule)
        {
            IEplOuSummaryDAO.IDiscOrRuleData data = EplOuSummaryManager.instance().dao().getCurrentDiscOrRuleKeyCategories(_summary, _discOrRule.getKey());
            _categories = data.categories();
            _transferCategories = data.transferCategories();

            _transferTimeItemMap = EplOuSummaryManager.instance().dao().getCurrentTransferTimeItems(_summary, _discOrRule.getKey(), _transferCategories);
        }
    }

    private void fillTimeDataRows()
    {
        EplDiscOrRuleRowWrapper calcWrapper = new EplDiscOrRuleRowWrapper(0L, getDiscOrRule().getDiscipline(), getDiscOrRule().getRule(), null);
        calcWrapper.setTitle("Рассчитано");
        calcWrapper.init();

        EplDiscOrRuleRowWrapper transferWrapper = new EplDiscOrRuleRowWrapper(1L, getDiscOrRule().getDiscipline(), getDiscOrRule().getRule(), null);
        transferWrapper.setTitle("Передано");
        transferWrapper.init();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EplEduGroupTimeItem.class, "t")
                .where(or(
                        eq(property("t", EplEduGroupTimeItem.summary()), value(getSummary())),
                        exists(EplTransferOrgUnitEduGroupTimeItem.class,
                               EplTransferOrgUnitEduGroupTimeItem.id().s(), property("t.id"),
                               EplTransferOrgUnitEduGroupTimeItem.transferredFrom().s(), getSummary())
                ));

        EplOuSummaryDAO.applyEduGroupTimeItemFilter(builder, "t", getDiscOrRule().getKey(), _categories);

        for (boolean transferTo : new boolean[]{false, true})
        {
            Collection<? extends EplEduGroupTimeItem> timeItems = transferTo ? _transferTimeItemMap.values() : DataAccessServices.dao().getList(builder);
            EplDiscOrRuleRowWrapper rowWrapper = transferTo ? transferWrapper : calcWrapper;

            for (EplEduGroupTimeItem timeItem : timeItems)
            {
                EplTimeRuleEduLoad timeRule = timeItem.getTimeRule();
                Long categoryId = timeRule.getCategory().getId();
                Map<PairKey<Long, Boolean>, EplCommonRowWrapper.ControlTime> timeMap = rowWrapper.getTimeMap();

                PairKey<Long, Boolean> key = PairKey.create(categoryId, transferTo);
                EplCommonRowWrapper.ControlTime timeSum = timeMap.get(key);
                if (null == timeSum)
                {
                    timeSum = new EplCommonRowWrapper.ControlTime();
                    timeMap.put(key, timeSum);
                }
                if (timeItem.getTimeAmount() != timeItem.getControlTimeAmount())
                    timeSum.hasDifference = true;

                timeSum.time += timeItem.getTimeAmount();
                timeSum.controlTime += timeItem.getControlTimeAmount();

                rowWrapper._timeSum += timeItem.getTimeAmount();

                if (null != timeItem.getEduGroup())
                {
                    rowWrapper.getEplGroups().add(timeItem.getEduGroup());
                    rowWrapper.getGroups().get(categoryId).add(timeItem.getEduGroup().getId());
                }
            }
        }
        _tableDiscRows = Arrays.asList(calcWrapper, transferWrapper);
    }

    public boolean isHasDiscipline()
    {
        return _discOrRule != null && _discOrRule.getKey().getGroupingCode().equals(BY_REG_ELEMENT);
    }

    public boolean isHasTimeRule()
    {
        return _discOrRule != null && _discOrRule.getKey().getGroupingCode().equals(BY_TIME_RULE);
    }

    public boolean isCurrentTransferTimeItemDisabled()
    {
        DataWrapper wrapper = _uiSupport.getDataSourceCurrentValue(EplOuSummaryTransferItem.DS_POSSIBLE_TRANSFER_TIME_ITEM);
        EplEduGroupTimeItem timeItem = wrapper.getWrapped();
        return null != timeItem && timeItem instanceof EplTransferOrgUnitEduGroupTimeItem;
    }

    public boolean isTableVisible()
    {
        return null != getDiscOrRule() && isHasTimeData();
    }

    public int getHeaderRowCount()
    {
        return getCategoryLevelCount();
    }

    public int getTableColumnCount()
    {
        return getBottomLevelCategories().size() + 3;
    }

    public String getCalcCellTimeSum()
    {
        Long categoryId = getCategory().getId();
        boolean transferTo = _currentDiscRow.getId() != 0;
        PairKey<Long, Boolean> categoryKey = PairKey.create(categoryId, transferTo);

        EplCommonRowWrapper.ControlTime timeSum = _currentDiscRow.getTimeMap().get(categoryKey);

        String value = timeSum == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(timeSum.time);
        String controlValue = timeSum == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(timeSum.controlTime);

        String categoryCode = getCategory().getCategory().getCode();

        if (timeSum != null && (EplTimeCategoryEduLoad.CODE_LECTURES.equals(categoryCode) || EplTimeCategoryEduLoad.CODE_PRACTICES.equals(categoryCode) || EplTimeCategoryEduLoad.CODE_LABS.equals(categoryCode)))
        {
            int count = _currentDiscRow.getGroups().get(categoryId).size();
            value = count + ": " + value;
            controlValue = count + ": " + controlValue;
        }
        return EplErrorFormatter.errorDiffSum(value, controlValue, timeSum != null && timeSum.hasDifference);
    }

    public String getGroupingCode()
    {
        return getDiscOrRuleKey().getGroupingCode();
    }

    public EplCommonRowWrapper getDiscOrRule()
    {
        return _discOrRule;
    }

    public void setDiscOrRule(EplCommonRowWrapper discOrRule)
    {
        _discOrRule = discOrRule;
    }

    public EplBaseCategoryDataWrapper getCategoryDataWrapper()
    {
        return _categoryDataWrapper;
    }

    public void setCategoryDataWrapper(EplBaseCategoryDataWrapper categoryDataWrapper)
    {
        _categoryDataWrapper = categoryDataWrapper;
    }

    public EplCommonRowWrapper.DisciplineOrRulePairKey getDiscOrRuleKey()
    {
        return _discOrRuleKey;
    }

    public void setDiscOrRuleKey(EplCommonRowWrapper.DisciplineOrRulePairKey discOrRuleKey)
    {
        _discOrRuleKey = discOrRuleKey;
    }

    public Set<Long> getCategories()
    {
        return _categories;
    }

    public void setCategories(Set<Long> categories)
    {
        _categories = categories;
    }

    public Set<Long> getTransferCategories()
    {
        return _transferCategories;
    }

    public void setTransferCategories(Set<Long> transferCategories)
    {
        _transferCategories = transferCategories;
    }

    public List<EplDiscOrRuleRowWrapper> getTableDiscRows()
    {
        return _tableDiscRows;
    }

    public EplOrgUnitSummary getSummary()
    {
        return _summary;
    }

    public void setSummary(EplOrgUnitSummary summary)
    {
        _summary = summary;
    }

    public boolean isHasTimeData()
    {
        return _hasTimeData;
    }

    public void setHasTimeData(boolean hasTimeData)
    {
        _hasTimeData = hasTimeData;
    }

    public int getCategoryRowCount()
    {
        return getCategory().getRowCount(getCategoryLevelCount());
    }

    public Set<EplTimeCategoryEduLoad> getUsedCategories()
    {
        return getCategoryDataWrapper().getUsedCategories();
    }

    public List<EplCategoryWrapper> getTopLevelCategories()
    {
        return getCategoryDataWrapper().getTopLevelCategories();
    }

    public List<EplCategoryWrapper> getBottomLevelCategories()
    {
        return getCategoryDataWrapper().getBottomLevelCategories();
    }

    public List<List<EplCategoryWrapper>> getSubcategories()
    {
        return getCategoryDataWrapper().getSubcategories();
    }

    public List<EplCategoryWrapper> getTopLevelCategoryList()
    {
        return getCategoryDataWrapper().getTopLevelCategories();
    }

    public Map<Long, EplCategoryWrapper> getCategoryMap()
    {
        return getCategoryDataWrapper().getCategoryMap();
    }

    public EplCategoryWrapper getCategory()
    {
        return _category;
    }

    public void setCategory(EplCategoryWrapper category)
    {
        _category = category;
    }

    public List<EplCategoryWrapper> getCurrentSubcategories()
    {
        return _currentSubcategories;
    }

    public void setCurrentSubcategories(List<EplCategoryWrapper> currentSubcategories)
    {
        _currentSubcategories = currentSubcategories;
    }

    public int getCategoryLevelCount()
    {
        return getCategoryDataWrapper().getCategoryLevelCount();
    }

    public Set<EplEduGroup> getEplGroups()
    {
        return _eplGroups;
    }

    public void setEplGroups(Set<EplEduGroup> eplGroups)
    {
        _eplGroups = eplGroups;
    }

    public EplDiscOrRuleRowWrapper getCurrentDiscRow()
    {
        return _currentDiscRow;
    }

    public void setCurrentDiscRow(EplDiscOrRuleRowWrapper currentDiscRow)
    {
        _currentDiscRow = currentDiscRow;
    }

    public Map<EplEduGroupTimeItemKey, EplTransferOrgUnitEduGroupTimeItem> getTransferTimeItemMap()
    {
        return _transferTimeItemMap;
    }
}
