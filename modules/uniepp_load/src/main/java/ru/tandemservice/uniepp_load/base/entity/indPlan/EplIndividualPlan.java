package ru.tandemservice.uniepp_load.base.entity.indPlan;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp_load.base.entity.indPlan.gen.*;

import java.util.Collection;
import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/** @see ru.tandemservice.uniepp_load.base.entity.indPlan.gen.EplIndividualPlanGen */
public class EplIndividualPlan extends EplIndividualPlanGen implements ISecLocalEntityOwner
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return OrgUnit.defaultComboDataSourceHandler(ownerId)
                .customize((alias, dql, context, filter) -> dql.where(exists(EplIndividualPlan.class, EplIndividualPlan.orgUnit().s(), property(alias))));
    }

    @Override
    @EntityDSLSupport(parts = P_FORMING_DATE)
    public String getTitle()
    {
        if (getEppYear() == null) return this.getClass().getSimpleName();
        return "Индивидуальный план на " + getEppYear().getTitle() + " уч. год. Преподаватель: " + getPps().getFullFio();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return Collections.singleton(getOrgUnit());
    }
}