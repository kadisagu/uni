/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.EplIndividualPlanManager;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.logic.EplPpsEntryDSHandler;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTabUI;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Alexey Lopatin
 * @since 02.08.2016
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "indPlan.id"),
        @Bind(key = EplOrgUnitTabUI.BIND_ORG_UNIT_ID, binding = "orgUnitId"),
        @Bind(key = EplIndividualPlanManager.PARAM_PPS_ACTIVE, binding = "ppsActive")
})
public class EplIndividualPlanAddEditUI extends UIPresenter
{
    public static final String PARAM_EPP_YEAR = "eppYear";
    public static final String PARAM_ORG_UNIT = "orgUnit";
    public static final String PARAM_PPS = "pps";

    private EplIndividualPlan _indPlan = new EplIndividualPlan();
    private Long _orgUnitId;
    private boolean _ppsActive;

    private boolean _onlyOuPps = true;
    private List<EplStudentSummary> _studentSummaries;

    @Override
    public void onComponentRefresh()
    {
        if (isEditMode())
        {
            _onlyOuPps = false;
            _indPlan = DataAccessServices.dao().getNotNull(_indPlan.getId());
            _studentSummaries = EplIndividualPlanManager.instance().dao().getOuSummaries(getIndPlan())
                    .stream().map(EplOrgUnitSummary::getStudentSummary).collect(Collectors.toList());
        }
        else
        {
            if (null != getOrgUnitId())
                _indPlan.setOrgUnit(DataAccessServices.dao().getNotNull(_orgUnitId));
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        Long orgUnitId = _indPlan.getOrgUnit() == null ? null : _indPlan.getOrgUnit().getId();
        EducationYear eduYear = _indPlan.getEppYear() == null ? null : _indPlan.getEppYear().getEducationYear();
        Person ppsPerson = isPpsActive() ? PersonManager.instance().dao().getPerson(_uiConfig.getUserContext().getPrincipalContext()) : null;

        switch (dataSource.getName())
        {
            case EplIndividualPlanManager.DS_TEACHER:
                dataSource.put(EplPpsEntryDSHandler.PARAM_ONLY_OU_PPS, _onlyOuPps);
                dataSource.put(EplPpsEntryDSHandler.PARAM_ORG_UNIT_ID, orgUnitId);
                dataSource.put(EplPpsEntryDSHandler.PARAM_PERSON_ID, ppsPerson == null ? null : ppsPerson.getId());
                dataSource.put(EplPpsEntryDSHandler.PARAM_EDU_YEAR, eduYear);
                break;
            case EplIndividualPlanManager.DS_STUDENT_SUMMARY:
                dataSource.put(PARAM_EPP_YEAR, _indPlan.getEppYear());
                dataSource.put(PARAM_ORG_UNIT, _indPlan.getOrgUnit());
                dataSource.put(PARAM_PPS, _indPlan.getPps());
                break;
        }
    }

    public void onClickApply()
    {
        if (!isEditMode())
            _indPlan.setFormingDate(new Date());

        DataAccessServices.dao().saveOrUpdate(_indPlan);
        EplIndividualPlanManager.instance().dao().saveOrUpdateOuSummaries(_indPlan, _studentSummaries);
        deactivate();
    }

    public boolean isEditMode()
    {
        return null != _indPlan.getId();
    }

    public boolean isOrgUnitDisabled()
    {
        return isEditMode() || null != getOrgUnitId();
    }

    // getters and setters

    public EplIndividualPlan getIndPlan()
    {
        return _indPlan;
    }

    public void setIndPlan(EplIndividualPlan indPlan)
    {
        _indPlan = indPlan;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public boolean isPpsActive()
    {
        return _ppsActive;
    }

    public void setPpsActive(boolean ppsActive)
    {
        _ppsActive = ppsActive;
    }

    public boolean isOnlyOuPps()
    {
        return _onlyOuPps;
    }

    public void setOnlyOuPps(boolean onlyOuPps)
    {
        _onlyOuPps = onlyOuPps;
    }

    public List<EplStudentSummary> getStudentSummaries()
    {
        return _studentSummaries;
    }

    public void setStudentSummaries(List<EplStudentSummary> studentSummaries)
    {
        _studentSummaries = studentSummaries;
    }
}
