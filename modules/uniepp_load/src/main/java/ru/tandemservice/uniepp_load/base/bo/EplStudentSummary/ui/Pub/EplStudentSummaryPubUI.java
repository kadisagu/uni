/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;

/**
 * @author oleyba
 * @since 10/11/11
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required = true),
        @Bind(key = EplStudentSummaryPubUI.SELECTED_TAB, binding = "selectedTab")
})
public class EplStudentSummaryPubUI extends UIPresenter
{
    public static final String SELECTED_TAB = "selectedTab";

    private EntityHolder<EplStudentSummary> holder = new EntityHolder<>();
    private String selectedTab;
    private boolean _groupSplitTabVisible;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        _groupSplitTabVisible = ISharedBaseDao.instance.get().existsEntity(EplStudentSummaryManager.instance().dao().getWorkPlanAndRegElementUniquePairsDql("wpRow", getSummary()).buildQuery());
    }

    public EplStudentSummary getSummary()
    {
        return getHolder().getValue();
    }

    public EntityHolder<EplStudentSummary> getHolder()
    {
        return holder;
    }

    public String getSelectedTab()
    {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        this.selectedTab = selectedTab;
    }

    public boolean isGroupSplitTabVisible()
    {
        return _groupSplitTabVisible;
    }
}
