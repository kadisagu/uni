package ru.tandemservice.uniepp_load.base.entity.studentWPSlot;

import com.google.common.collect.Ordering;
import com.google.common.primitives.Ints;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.gen.EplStudent2WorkPlanGen;

import java.util.Comparator;

/**
 * РУП для план. академ. группы
 */
public class EplStudent2WorkPlan extends EplStudent2WorkPlanGen
{
    public static final Comparator<EplStudent2WorkPlan> IN_ROW_COMPARATOR = (o1, o2) -> {
        int ret = o1.getWorkPlan().getTerm().getIntValue() - o2.getWorkPlan().getTerm().getIntValue();
        if (ret != 0) return ret;
        return Ordering.natural().nullsFirst().compare(
                Ints.tryParse(o1.getWorkPlan().getWorkPlan().getNumber()),
                Ints.tryParse(o2.getWorkPlan().getWorkPlan().getNumber())
        );
    };
}