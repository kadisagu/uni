/* $Id$ */
package ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.commonbase.utils.DQLSimple;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import ru.tandemservice.uni.dao.pps.IPpsEntryDao;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplPlannedPpsTypeCodes;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.OuSummaryTab.EplPlannedPpsOuSummaryTabUI;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem;
import ru.tandemservice.uniepp_load.loadPlan.util.PpsTimeStatsUtil;
import ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.EplSettingsPpsLoadManager;
import ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.logic.EplSettingsPpsLoadLevels;
import ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.logic.IEplSettingsPpsLoadDao;
import ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uniepp_load.loadPlan.util.PpsTimeStatsUtil.getEplSettingsPpsLoadByPost;

/**
 * @author nvankov
 * @since 1/28/15
 */
public class EplPlannedPpsDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String VIEW_DEGREE = "degree";
    public static final String VIEW_STATUS = "status";
    public static final String VIEW_EMPLOYEE_POSTS = "employeePosts";
    public static final String VIEW_TIME_WORKERS = "timeWorkers";

    public static final String VIEW_TIME_AMOUNT = "timeAmount";
    public static final String VIEW_SUMMARY_DATA = "summaryData";
    public static final String VIEW_STUDENT_SUMMARY_DATA = "studentSummaryData";
    public static final String VIEW_RATE = "rate";
    public static final String VIEW_OVERRATE = "overrate";
    public static final String VIEW_TOTAL = "total";
    public static final String ADDITIONAL_TIME_AMOUNT = "additionalTimeAmount";
    public static final String ADDITIONAL_TIME_AMOUNT_SUM = "additionalTimeAmountSum";
    public static final String STAFF_RATE_SUM = "staffRateSum";
    public static final String VIEW_RATE_SUM = "rateSum";
    public static final String VIEW_OVERRATE_SUM = "overrateSum";
    public static final String VIEW_TOTAL_SUM = "totalSum";

    private boolean _isTimeWorker;

    public EplPlannedPpsDSHandler(String ownerId, boolean isTimeWorker)
    {
        super(ownerId);
        _isTimeWorker = isTimeWorker;
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Map<Person, Set<String>> statusMap = Maps.newHashMap();
        Map<Person, Set<String>> degreeMap = Maps.newHashMap();
        Map<PpsEntry, List<EmployeePost>> employeePostMap = Maps.newHashMap();
        Map<PpsEntry, List<IEntity>> timeWorkerMap = Maps.newHashMap();
        Map<Person, List<EplPlannedPps>> ppsByPerson = Maps.newHashMap();

        Map<EplPlannedPps, Set<EplPlannedPpsTimeItem>> ppsTimeMap = Maps.newHashMap();
        Map<EplPlannedPps, Set<EplPlannedPpsTimeItem>> summaryPpsTimeMap = Maps.newHashMap();

        Long summaryId = context.getNotNull(EplPlannedPpsOuSummaryTabUI.PARAM_SUMMARY_ID);
        EplOrgUnitSummary summary = DataAccessServices.dao().get(summaryId);
        List<String> typeCodes = _isTimeWorker
                ? Arrays.asList(EplPlannedPpsTypeCodes.TIMEWORKER, EplPlannedPpsTypeCodes.VACANCY_AS_TIMEWORKER)
                : Arrays.asList(EplPlannedPpsTypeCodes.STAFF, EplPlannedPpsTypeCodes.VACANCY_AS_STAFF);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EplPlannedPps.class, "pps")
                .where(eq(property("pps", EplPlannedPps.orgUnitSummary().id()), value(summaryId)))
                .where(in(property("pps", EplPlannedPps.type().code()), typeCodes));

        List<EplPlannedPps> ppsList = builder.createStatement(getSession()).<EplPlannedPps>list().stream()
                .sorted(PLANNED_PPS_COMPARATOR)
                .collect(Collectors.toList());

        List<Long> personIds = ppsList.stream()
                .filter(pps -> null != pps.getPpsEntry())
                .map(pps -> pps.getPpsEntry().getPerson().getId())
                .distinct()
                .collect(Collectors.toList());

        List<Long> ppsEntryIds = ppsList.stream()
                .filter(pps -> null != pps.getPpsEntry())
                .map(pps -> pps.getPpsEntry().getId())
                .distinct()
                .collect(Collectors.toList());

        new DQLSimple<>(PersonAcademicDegree.class)
                .where(PersonAcademicDegree.person().id(), personIds)
                .list().forEach(item -> SafeMap.safeGet(degreeMap, item.getPerson(), HashSet.class).add(item.getAcademicDegree().getShortTitle()));

        new DQLSimple<>(PersonAcademicStatus.class)
                .where(PersonAcademicStatus.person().id(), personIds)
                .list().forEach(item -> SafeMap.safeGet(statusMap, item.getPerson(), HashSet.class).add(item.getAcademicStatus().getShortTitle()));

        if (_isTimeWorker)
            timeWorkerMap = IPpsEntryDao.instance.get().getPpsEntry2TimeWorkerMap(ppsEntryIds);
        else
        {
            new DQLSimple<>(EmployeePost4PpsEntry.class)
                    .where(EmployeePost4PpsEntry.ppsEntry().id(), ppsEntryIds)
                    .where(EmployeePost4PpsEntry.removalDate(), null)
                    .list().forEach(item -> SafeMap.safeGet(employeePostMap, item.getPpsEntry(), ArrayList.class).add(item.getEmployeePost()));
        }

        new DQLSelectBuilder().fromEntity(EplPlannedPps.class, "pps")
                .where(eq(property("pps", EplPlannedPps.orgUnitSummary().id()), value(summaryId)))
                .joinEntity("pps", DQLJoinType.inner, EplPlannedPpsTimeItem.class, "ti", eq(property("ti", EplPlannedPpsTimeItem.pps()), property("pps")))
                .column(property("pps"))
                .column(property("ti"))
                .createStatement(getSession()).<Object[]>list()
                .forEach(row -> SafeMap.safeGet(ppsTimeMap, (EplPlannedPps) row[0], HashSet.class).add((EplPlannedPpsTimeItem) row[1]));

        new DQLSelectBuilder().fromEntity(EplPlannedPps.class, "pps")
                .where(eq(property("pps", EplPlannedPps.orgUnitSummary().studentSummary().id()), value(summary.getStudentSummary().getId())))
                .joinEntity("pps", DQLJoinType.inner, EplPlannedPpsTimeItem.class, "ti", eq(property("ti", EplPlannedPpsTimeItem.pps()), property("pps")))
                .column(property("pps"))
                .column(property("ti"))
                .createStatement(getSession()).<Object[]>list()
                .forEach(row -> SafeMap.safeGet(summaryPpsTimeMap, (EplPlannedPps) row[0], HashSet.class).add((EplPlannedPpsTimeItem) row[1]));

        final IEplSettingsPpsLoadDao dao = EplSettingsPpsLoadManager.instance().dao();
        final Map<PostBoundedWithQGandQL, EplSettingsPpsLoad> postMap = dao.getPostMap(summaryId);
        Map<EplPlannedPps, PpsTimeStatsUtil.ResultRow> rateMap = PpsTimeStatsUtil.getPpsTimeStatsRows(ppsTimeMap, postMap);
        Map<EplPlannedPps, PpsTimeStatsUtil.ResultRow> summaryRateMap = PpsTimeStatsUtil.getPpsTimeStatsRowsBySummary(summaryPpsTimeMap, getSettinsMapForStudentSummary(summary));

        new DQLSelectBuilder().fromEntity(EplPlannedPps.class, "pps")
                .where(eq(property("pps", EplPlannedPps.orgUnitSummary().studentSummary().id()),  value(summary.getStudentSummary().getId())))
                .where(in(property("pps", EplPlannedPps.type().code()), typeCodes))
                .createStatement(getSession()).<EplPlannedPps>list()
                .stream()
                .forEach(e -> {
                    if(e.getPpsEntry() != null)
                        SafeMap.safeGet(ppsByPerson, e.getPpsEntry().getPerson(), ArrayList.class).add(e);
                });

        List<DataWrapper> resultList = Lists.newLinkedList();

        for (EplPlannedPps pps : ppsList)
        {
            PpsEntry ppsEntry = pps.getPpsEntry();
            Person person = ppsEntry == null ? null : ppsEntry.getPerson();
            PpsTimeStatsUtil.ResultRow rate = rateMap.get(pps);

            DataWrapper wrapper = new DataWrapper(pps);
            wrapper.setProperty(VIEW_DEGREE, degreeMap.get(person));
            wrapper.setProperty(VIEW_STATUS, statusMap.get(person));

            if (_isTimeWorker)
            {
                wrapper.setProperty(VIEW_TIME_WORKERS, timeWorkerMap.get(pps.getPpsEntry()));
                if (null != rate)
                {
                    wrapper.setProperty(VIEW_TIME_AMOUNT, rate.getTotalMessageColor());
                }
            }
            else
            {
                wrapper.setProperty(VIEW_EMPLOYEE_POSTS, employeePostMap.get(pps.getPpsEntry()));

                if (null != rate)
                {
                    wrapper.setProperty(VIEW_RATE, getNonZero(rate.getRateMessageColor()));
                    wrapper.setProperty(VIEW_OVERRATE, getNonZero(rate.getOverrateMessageColor()));
                    wrapper.setProperty(VIEW_TOTAL, getNonZero(rate.getTotalMessageColor()));
                }
            }
            wrapper.setProperty(ADDITIONAL_TIME_AMOUNT, new Double[] {pps.getMaxTimeAmount(),
                    getLimit(pps, postMap, employeePostMap)});

            if (ppsEntry != null && ppsByPerson.containsKey(ppsEntry.getPerson()))
            {
                wrapper.setProperty(EplPlannedPpsDSHandler.STAFF_RATE_SUM, ppsByPerson.get(ppsEntry.getPerson()).stream()
                        .filter(e -> null != e.getStaffRateAsDouble())
                        .mapToDouble(EplPlannedPps::getStaffRateAsDouble).sum());

                double additionalHoursSum = 0, additionalHoursSumLimit = 0;
                for (EplPlannedPps item : ppsByPerson.get(ppsEntry.getPerson()))
                {
                    if (item.getMaxTimeAmount() != null)
                        additionalHoursSum += item.getMaxTimeAmount();
                    Double limit = getLimit(item, dao.getPostMap(item.getOrgUnitSummary().getId()), employeePostMap);
                    if (null != limit && item.getMaxTimeAmount() != null && item.getMaxTimeAmount() > limit)
                        additionalHoursSumLimit += limit;
                }

                wrapper.setProperty(ADDITIONAL_TIME_AMOUNT_SUM, new Double[] {additionalHoursSum, null, additionalHoursSumLimit });

                PpsTimeStatsUtil.ResultRow result = new PpsTimeStatsUtil.ResultRow();

                ppsByPerson.get(ppsEntry.getPerson()).stream().map(e -> summaryRateMap.get(e)).forEach(e ->result.append(e));

                wrapper.setProperty(VIEW_RATE_SUM, getNonZero(result.getRateMessageColor()));
                wrapper.setProperty(VIEW_OVERRATE_SUM, getNonZero(result.getOverrateMessageColor()));
                wrapper.setProperty(VIEW_TOTAL_SUM, getNonZero(result.getTotalMessageColor()));
            }

            resultList.add(wrapper);
        }

        return ListOutputBuilder.get(input, resultList).build();
    }

    private Map<MultiKey, EplSettingsPpsLoad> getSettinsMapForStudentSummary(EplOrgUnitSummary summary)
    {
        final Map<MultiKey, EplSettingsPpsLoad> settingMap = Maps.newHashMap();
        final List<Object> ouIds = new DQLSelectBuilder()
                .fromEntity(EplOrgUnitSummary.class, "ou")
                .where(eq(property("ou", EplOrgUnitSummary.studentSummary()), value(summary.getStudentSummary().getId())))
                .createStatement(getSession()).idList();
        List<EplSettingsPpsLoad> settingList = new DQLSelectBuilder().fromEntity(EplSettingsPpsLoad.class, "s")
                .where(eq(property("s", EplSettingsPpsLoad.eduYear()), value(summary.getStudentSummary().getEppYear().getEducationYear())))
                .where(or(eq(property("s", EplSettingsPpsLoad.level()), value(EplSettingsPpsLoadLevels.BASIC.getValue())),
                        eq(property("s", EplSettingsPpsLoad.entity()), value(summary.getStudentSummary().getId())),
                        in(property("s", EplSettingsPpsLoad.entity()), ouIds)))
                .order(EplSettingsPpsLoad.level().fromAlias("s").s())
                .createStatement(getSession()).list();

        settingList.forEach(s ->
        {
            if (EplSettingsPpsLoadLevels.OUSUMMARY.equals(s.getLevel()))
                settingMap.put(new MultiKey(s.getEntity(), s.getPost()), s);
            else
                ouIds.stream().forEach(id -> settingMap.put(new MultiKey(id, s.getPost()), s));
        });
        return settingMap;
    }

    private static Double getLimit(EplPlannedPps pps, Map<PostBoundedWithQGandQL, EplSettingsPpsLoad> postMap, Map<PpsEntry, List<EmployeePost>> employeePostMap)
    {
        Double maxValue = null;
        final EplSettingsPpsLoad eplSettingsPpsLoad = getEplSettingsPpsLoadByPost(postMap, pps.getPost());
        if (eplSettingsPpsLoad != null)
        {
            maxValue = eplSettingsPpsLoad.getHoursAdditional();
            if (eplSettingsPpsLoad.isStaffRateFactor() && pps != null && pps.getStaffRateAsDouble() != null)
                maxValue *= pps.getStaffRateAsDouble();
            if (eplSettingsPpsLoad.isStaffNumberFactor() && pps != null && pps.getPpsEntry() != null && employeePostMap.containsKey(pps.getPpsEntry()))
                maxValue *= pps.getPpsEntry() == null ? 1 : employeePostMap.get(pps.getPpsEntry()).size();
        }
        return maxValue;
    }

    private static final String ZERO = "0";
    static String getNonZero(String value)
    {
        return  ZERO.equals(value) ? "" : value;
    }

    private static final Comparator<EplPlannedPps> PLANNED_PPS_COMPARATOR = (p1, p2) -> {
        int result = Integer.compare(p1.getType().getPriority(), p2.getType().getPriority());
        if (0 == result)
            result = CommonCollator.RUSSIAN_COLLATOR.compare(p1.getTitle(), p2.getTitle());
        if (0 == result)
        {
            String pe1 = p1.getPpsEntry() == null ? null : p1.getPpsEntry().getTitle();
            String pe2 = p2.getPpsEntry() == null ? null : p2.getPpsEntry().getTitle();

            if (null != pe1 || null != pe2)
            {
                if (null != pe1 && null != pe2)
                    result = CommonCollator.RUSSIAN_COLLATOR.compare(pe1, pe2);
                else
                    result = pe1 == null ? 1 : -1;
            }
        }
        return result;
    };
}
