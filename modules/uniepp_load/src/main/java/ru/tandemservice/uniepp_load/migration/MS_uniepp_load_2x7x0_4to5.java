package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_load_2x7x0_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (tool.tableExists("epl_time_rule_edugrp_base_t"))
        {
            tool.dropColumn("epl_time_rule_edugrp_base_t", "javacodebean_p");
            tool.createColumn("epl_time_rule_edugrp_base_t", new DBColumn("userscript_p", DBType.TEXT));
            tool.createColumn("epl_time_rule_edugrp_base_t", new DBColumn("defaultscriptpath_p", DBType.createVarchar(255)));
        }
    }
}