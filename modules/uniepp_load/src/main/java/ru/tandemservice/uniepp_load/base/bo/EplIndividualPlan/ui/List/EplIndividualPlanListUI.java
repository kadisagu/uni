/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.EplIndividualPlanManager;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.AddEdit.EplIndividualPlanAddEdit;
import ru.tandemservice.uniepp_load.catalog.entity.EplPrintScript;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplPrintScriptCodes;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTabUI;

import java.text.MessageFormat;

/**
 * @author Alexey Lopatin
 * @since 01.08.2016
 */
@State({
        @Bind(key = EplOrgUnitTabUI.BIND_ORG_UNIT_ID, binding = "orgUnitId"),
        @Bind(key = EplIndividualPlanManager.PARAM_PPS_ACTIVE, binding = "ppsActive")
})
@Output({
        @Bind(key = EplIndividualPlanManager.PARAM_PPS_ACTIVE, binding = "ppsActive")
})
public class EplIndividualPlanListUI extends UIPresenter
{
    public static final String PARAM_EPP_YEAR = "eppYear";
    public static final String PARAM_ORG_UNIT = "orgUnit";
    public static final String PARAM_PPS_LIST = "ppsList";
    public static final String PARAM_STUDENT_SUMMARY = "studentSummary";

    public static final String PARAM_PPS_PERSON = "ppsPerson";

    private Long _orgUnitId;
    private CommonPostfixPermissionModel _secModel;
    private Person _ppsPerson;
    private boolean _ppsActive;

    @Override
    public void onComponentRefresh()
    {
        if (isOrgUnitActive())
        {
            getSettings().set(PARAM_ORG_UNIT, null);
            saveSettings();

            OrgUnit orgUnit = DataAccessServices.dao().getNotNull(getOrgUnitId());
            setSecModel(new OrgUnitSecModel(orgUnit));
        }
        if (isPpsActive())
            _ppsPerson = PersonManager.instance().dao().getPerson(_uiConfig.getUserContext().getPrincipalContext());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.putAll(getSettings().getAll(null));
        dataSource.put(EplOrgUnitTabUI.BIND_ORG_UNIT_ID, _orgUnitId);
        dataSource.put(PARAM_PPS_PERSON, _ppsPerson);
    }

    public void onClickAdd()
    {
        _uiActivation.asRegionDialog(EplIndividualPlanAddEdit.class)
                .parameter(EplOrgUnitTabUI.BIND_ORG_UNIT_ID, _orgUnitId)
                .parameter(EplIndividualPlanManager.PARAM_PPS_ACTIVE, _ppsActive)
                .activate();
    }

    public void onClickPrint()
    {
        EplPrintScript scriptItem = DataAccessServices.dao().getByCode(EplPrintScript.class, EplPrintScriptCodes.IND_PLAN_PPS);
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem, IScriptExecutor.TEMPLATE_VARIABLE, new byte[]{}, "indPlanId", getListenerParameterAsLong());
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(EplIndividualPlanAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .parameter(EplOrgUnitTabUI.BIND_ORG_UNIT_ID, _orgUnitId)
                .parameter(EplIndividualPlanManager.PARAM_PPS_ACTIVE, _ppsActive)
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public String getStickerPps()
    {
        return _ppsPerson == null ? "" : MessageFormat.format(getConfig().getProperty("ui.stickerPps"), getPpsPerson().getFullFio());
    }

    public boolean isMainVisible()
    {
        return isOrgUnitVisible() && isPpsVisible();
    }

    public boolean isOrgUnitActive()
    {
        return null != _orgUnitId;
    }

    public boolean isOrgUnitVisible()
    {
        return !isOrgUnitActive();
    }

    public boolean isPpsVisible()
    {
        return !isPpsActive();
    }

    public String getAddPermissionKey()
    {
        return getPermissionKey("add");
    }

    public String getPrintPermissionKey()
    {
        return getPermissionKey("print");
    }

    public String getEditPermissionKey()
    {
        return getPermissionKey("edit");
    }

    public String getDeletePermissionKey()
    {
        return getPermissionKey("delete");
    }

    private String getPermissionKey(String postfix)
    {
        return isOrgUnitActive() ? getSecModel().getPermission("viewEplIndPlanTab_" + postfix) : isPpsActive() ? null : "eplIndividualPlanList_" + postfix;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public boolean isPpsActive()
    {
        return _ppsActive;
    }

    public void setPpsActive(boolean ppsActive)
    {
        _ppsActive = ppsActive;
    }

    public Person getPpsPerson()
    {
        return _ppsPerson;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }
}
