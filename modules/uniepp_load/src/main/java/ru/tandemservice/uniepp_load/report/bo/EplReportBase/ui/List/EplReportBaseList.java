/* $Id:$ */
package ru.tandemservice.uniepp_load.report.bo.EplReportBase.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 5/14/14
 */
@Configuration
public class EplReportBaseList extends BusinessComponentManager
{
    public static final String ORG_UNIT_DS = "orgUnitDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EducationCatalogsManager.instance().eduYearDSConfig())
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler()).addColumn(OrgUnit.fullTitle().s()))
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                dql.where(exists(EplOrgUnitSummary.class, EplOrgUnitSummary.L_ORG_UNIT, property(alias)));
            }
        }
                .order(OrgUnit.title())
                .filter(OrgUnit.title());
    }
}