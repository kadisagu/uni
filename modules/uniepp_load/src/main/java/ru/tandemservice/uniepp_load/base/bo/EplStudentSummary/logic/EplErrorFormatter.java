/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem;

/**
 * @author oleyba
 * @since 12/4/14
 */
public class EplErrorFormatter
{
    public static String errorDiff(final EplTimeItem timeItem)
    {
        if (timeItem.getTimeAmount() == timeItem.getControlTimeAmount())
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(timeItem.getTimeAmount());

        return "<span style=\"color:" + UniDefines.COLOR_CELL_ERROR + "\"><nobr>" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(timeItem.getTimeAmount()) +
                " (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(timeItem.getControlTimeAmount()) + ")</nobr></span>";
    }

    public static String errorDiffSum(String value, String controlValue)
    {
        return errorDiffSum(value, controlValue, !value.equals(controlValue));
    }

    public static String errorDiffSum(String value, String controlValue, Boolean hasDifference)
    {
        return (hasDifference == null || !hasDifference) ? value : "<span style=\"color:" + UniDefines.COLOR_CELL_ERROR + "\">" + value + " (" + controlValue + ")</span>";
    }
}
