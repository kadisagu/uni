/* $Id:$ */
package ru.tandemservice.uniepp_load.report.bo.EplReport.util;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;

import java.util.Collection;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.sum;

/**
 * @author oleyba
 * @since 2/17/15
 */
public abstract class EplTimeSplitter
{
    private final Long summaryId;
    private Map<MultiKey, Integer> studentCountMap = SafeMap.get(key -> 0);

    public EplTimeSplitter(Long summaryId)
    {
        this.summaryId = summaryId;
        initStudentCountMap();
    }

    public double c(EplEduGroupTimeItem item, Object key)
    {
        if (item.getEduGroup() == null) {
            throw new ApplicationException("В сводке присутствуют неактуальные часы нагрузки (" + item.getDescription() + "), распределить их по видам возмещения затрат невозможно.");
        }
        int sum = 0;
        for (Object k : getPropertyValueList()) {
            sum += studentCountMap.get(new MultiKey(item.getEduGroup().getId(), k));
        }
        if (0 == sum) {
            throw new ApplicationException("В планируемой учебной группе " + item.getEduGroup().getTitle() + " нет студентов, распределить часы по видам возмещения затрат невозможно.");
        }
        return ((double) studentCountMap.get(new MultiKey(item.getEduGroup().getId(), key))) / sum;
    }

    private void initStudentCountMap()
    {
        for (Object propValue : getPropertyValueList()) {
            String s = getPropertyPath();

            DQLSelectBuilder subDQL = new DQLSelectBuilder()
                .fromEntity(EplEduGroupTimeItem.class, "i")
                .where(eq(property("i", EplEduGroupTimeItem.eduGroup()), property("r", EplEduGroupRow.group())));

            IEntity summary = DataAccessServices.dao().get(summaryId);
            if (summary instanceof EplOrgUnitSummary) {
                subDQL.where(eq(property("i", EplEduGroupTimeItem.summary().id()), value(summaryId)));
            } else if (summary instanceof EplStudentSummary) {
                subDQL.where(eq(property("i", EplEduGroupTimeItem.summary().studentSummary().id()), value(summaryId)));
            } else {
                throw new IllegalArgumentException();
            }

            DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EplEduGroupRow.class, "r")
                .where(eq(property("r", s), commonValue(propValue)))
                .where(exists(subDQL
                    .buildQuery()))
                .column(property("r", EplEduGroupRow.group().id()), "g_id")
                .group(property("r", EplEduGroupRow.group().id()))
                .column(sum(property("r", EplEduGroupRow.count())));


            for (Object[] row : DataAccessServices.dao().<Object[]>getList(dql)) {
                studentCountMap.put(new MultiKey(row[0], propValue), ((Number) row[1]).intValue());
            }
        }
    }

    protected abstract Collection getPropertyValueList();
    protected abstract String getPropertyPath();
}

