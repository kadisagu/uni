/* $Id$ */
package ru.tandemservice.uniepp_load.base.ext.Person;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Alexey Lopatin
 * @since 11.08.2016
 */
@Configuration
public class PersonExtManager extends BusinessObjectExtensionManager
{
}