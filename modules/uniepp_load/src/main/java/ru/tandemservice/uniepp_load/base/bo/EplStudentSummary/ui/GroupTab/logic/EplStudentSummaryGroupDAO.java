package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.logic;

import com.google.common.collect.Iterables;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author avedernikov
 * @since 16.12.2016
 */
public class EplStudentSummaryGroupDAO extends SharedBaseDao implements IEplStudentSummaryGroupDAO
{
	@Override
	public void deleteEplStudents(EplStudentSummary summary, EplStudentFilter filters, Collection<Pair<String, Object>> eplStudentProperties)
	{
		final String studentAlias = "eplStudent";
		List<Long> studentsToDelete = getEplStudentDql(summary, filters, eplStudentProperties, studentAlias)
				.column(property(studentAlias, EplStudent.id()))
				.createStatement(getSession()).list();
		List<Long> groupsOfStudents = getEplStudentDql(summary, filters, eplStudentProperties, studentAlias)
				.column(property(studentAlias, EplStudent.group().id()))
				.distinct()
				.createStatement(getSession()).list();

		Iterables.partition(studentsToDelete, DQL.MAX_VALUES_ROW_NUMBER).forEach(ids -> deleteEntities(EplStudent.class, ids));
		List<Long> emptyGroups = getEmptyGroups(groupsOfStudents);
		deleteEntities(EplGroup.class, emptyGroups);
	}

	/**
	 * Построить запрос к план. студентам.
	 * @param summary Сводка контингента.
	 * @param filters Фильтрация, которая должна примениться к запросу согласно выбранным в селектах значениям.
	 * @param eplStudentProperties Значения строки план. контингента, по которой строится запрос (в формате "ключ-значение").
	 * @param studentAlias Алиас план. студента.
	 */
	private DQLSelectBuilder getEplStudentDql(EplStudentSummary summary, EplStudentFilter filters, Collection<Pair<String, Object>> eplStudentProperties, String studentAlias)
	{
		DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EplStudent.class, studentAlias)
				.where(eq(property(studentAlias, EplStudent.group().summary()), value(summary)));
		filters.applyFilters(dql, studentAlias);
		eplStudentProperties.forEach(propPair -> dql.where(eq(property(studentAlias, propPair.getX()), commonValue(propPair.getY()))));
		return dql;
	}

	/** Удалить сущности определенного класса по списку их id. */
	private void deleteEntities(Class entityClass, List<Long> ids)
	{
		new DQLDeleteBuilder(entityClass)
				.where(in(property("id"), ids))
				.createStatement(getSession()).execute();
	}

	/**
	 * Получить список id тех групп, для которых больше нет план. студентов.
	 * @param possibleGroupIds Список id групп-кандидатов (проверяются только группы из этого списка).
	 */
	private List<Long> getEmptyGroups(List<Long> possibleGroupIds)
	{
		final String groupAlias = "eplGroup";
		final String studentAlias = "eplStudent";
		DQLSelectBuilder studentsOfGroup = new DQLSelectBuilder().fromEntity(EplStudent.class, studentAlias)
				.where(eq(property(studentAlias, EplStudent.group()), property(groupAlias)));
		return new DQLSelectBuilder().fromEntity(EplGroup.class, groupAlias)
				.where(in(property(groupAlias, EplGroup.id()), possibleGroupIds))
				.where(notExists(studentsOfGroup.buildQuery()))
				.createStatement(getSession()).list();
	}
}
