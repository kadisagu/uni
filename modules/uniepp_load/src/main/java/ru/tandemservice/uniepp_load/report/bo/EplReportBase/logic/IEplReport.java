/* $Id:$ */
package ru.tandemservice.uniepp_load.report.bo.EplReportBase.logic;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.IStorableReport;

import java.util.Date;

/**
 * @author oleyba
 * @since 5/14/14
 */
public interface IEplReport extends IStorableReport, IEntity
{
    public static final String P_FORMING_DATE = "formingDate";
    public static final String L_EDU_YEAR = "eduYear"; // должно быть персистентным

    IEplStorableReportDesc getDesc();

    Date getFormingDate();
}
