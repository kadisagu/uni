/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.logic;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.column.ITextColumnBuilder;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniepp_load.catalog.entity.EplCompensationSource;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 10/11/11
 */
public class EplStudentSummaryGroupDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PARAM_SELECTED_COLUMNS = "selectedColumns";
    public static final String PARAM_SUMMARY = "summary";
    public static final String PARAM_FILTERS = "settings";

	public static final String PROP_COUNT = "count";
	public static final String PROP_COUNT_MESSAGE = "count_postfix";

    public static List<ColumnWrapper> columnList = Arrays.asList(
        new ColumnWrapper("course", "Курс", EplStudent.group().course().s()).order(Course.intValue().s()).enabledByDefault(),
        new ColumnWrapper("formOu", "Формирующее подразделение", EplStudent.educationOrgUnit().formativeOrgUnit().s()).title(OrgUnit.shortTitle().s()).enabledByDefault(),
        new ColumnWrapper("terrOu", "Территориальное подразделение", EplStudent.educationOrgUnit().territorialOrgUnit().s()).title(OrgUnit.territorialShortTitle().s()),
        new ColumnWrapper("group", "Группа", EplStudent.group().s()).enabledByDefault(),
        new ColumnWrapper("eduLevel", "Направление (специальность)", EplStudent.educationOrgUnit().educationLevelHighSchool().s()).title(EducationLevelsHighSchool.fullTitleExtended().s()).enabledByDefault(),
        new ColumnWrapper("developForm", "Форма освоения", EplStudent.educationOrgUnit().developForm().s()).order(DevelopForm.code().s()).enabledByDefault(),
        new ColumnWrapper("developCondition", "Условие освоения", EplStudent.educationOrgUnit().developCondition().s()).title(DevelopCondition.shortTitle().s()).order(DevelopCondition.code().s()),
        new ColumnWrapper("developTech", "Технология освоения", EplStudent.educationOrgUnit().developTech().s()).order(DevelopTech.code().s()),
        new ColumnWrapper("developPeriod", "Срок освоения", EplStudent.developGrid().s()).order(DevelopGrid.developPeriod().priority().s()),
        new ColumnWrapper("compSource", "Источник возм. затрат", EplStudent.compensationSource().s()).order(EplCompensationSource.code().s()).enabledByDefault()
    );

    public EplStudentSummaryGroupDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context)
    {
        final List<ColumnWrapper> selectedColumns = context.get(PARAM_SELECTED_COLUMNS);
	    final EplStudentSummary summary = context.get(PARAM_SUMMARY);
	    final EplStudentFilter filters = context.get(PARAM_FILTERS);

        final int columnCount = selectedColumns.size();

        Number count = new DQLSelectBuilder()
            .fromDataSource(getDqlSelectBuilder("eplStu", selectedColumns, summary, filters).buildQuery(), "a")
            .createCountStatement(new DQLExecutionContext(context.getSession())).uniqueResult();

	    if (count.intValue() == 0)
		    return new DSOutput(dsInput);

        final DSOutput output = new DSOutput(dsInput);
        output.setTotalSize(count.intValue());

	    final String eplStudentAlias = "eplStudent";
        DQLSelectBuilder dql = getDqlSelectBuilder(eplStudentAlias, selectedColumns, summary, filters);
	    dql.column(DQLFunctions.sum(property(eplStudentAlias, EplStudent.count())));
        for (ColumnWrapper column : selectedColumns)
            dql.order(column.getOrderDQLProperty(eplStudentAlias));

	    if (columnCount == 0)
	    {
		    final List<Long> counts = DataAccessServices.dao().getList(dql, output.getStartRecord(), output.getCountRecord());
		    List<Object> records = wrapCollapsedRow(counts);
		    output.setRecordList(records);
	    }
	    else
	    {
		    final List<Object[]> rows = DataAccessServices.dao().getList(dql, output.getStartRecord(), output.getCountRecord());
		    List<Object> records = wrapNormalRows(rows, selectedColumns);
		    output.setRecordList(records);
	    }
        return output;
    }

    private DQLSelectBuilder getDqlSelectBuilder(String eplStudentAlias, List<ColumnWrapper> selectedColumns, EplStudentSummary summary, EplStudentFilter filters)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EplStudent.class, eplStudentAlias)
            .where(eq(property(EplStudent.group().summary().fromAlias(eplStudentAlias)), value(summary)));

	    filters.applyFilters(dql, eplStudentAlias);

        for (ColumnWrapper column : selectedColumns)
        {
            dql.column(column.getIdDQLProperty(eplStudentAlias));
            dql.group(column.getIdDQLProperty(eplStudentAlias));
        }

        for (ColumnWrapper column : selectedColumns)
        {
            if (column.isSupportedTitle()) {
                dql.column(column.getIdDQLProperty(eplStudentAlias));
            } else {
                dql.column(column.getTitleDQLProperty(eplStudentAlias));
                dql.group(column.getTitleDQLProperty(eplStudentAlias));
            }
        }

	    // TODO: А эти-то колонки нафига нужны?
        for (ColumnWrapper column : selectedColumns)
        {
            dql.column(column.getOrderDQLProperty(eplStudentAlias));
            dql.group(column.getOrderDQLProperty(eplStudentAlias));
        }
        return dql;
    }

	/** Построить список элементов серчлиста для случая, когда есть группировка хотя бы по одному параметру (и, соответственно, хотя бы одна колонка со свойством план. студента). */
	private static List<Object> wrapNormalRows(List<Object[]> rows, List<ColumnWrapper> selectedColumns)
	{
		final int columnCount = selectedColumns.size();
		int currIndex = 1;
		List<Object> result = new ArrayList<>();
		for (Object[] row : rows)
		{
			DataWrapper wrapper = new DataWrapper();
			HashCodeBuilder hasher = new HashCodeBuilder();
			wrapper.setProperty(PROP_COUNT, row[3 * columnCount]);
            wrapper.put(PROP_COUNT_MESSAGE, getPostfix((Long)row[3 * columnCount]));
			for (int i = 0; i < columnCount; i++)
			{
				ColumnWrapper columnWrapper = selectedColumns.get(i);
				Object propValue = getPropValue(row, columnWrapper, i, columnCount);
				wrapper.setProperty(columnWrapper.getKey(), propValue);
				hasher.append(propValue);
			}
			wrapper.setId(buildId(currIndex++, hasher.hashCode()));
			result.add(wrapper);
		}
		return result;
	}

	/**
	 * Получить из строки значений то, которое соответствует нужной колонке.
	 * @param row Строка значений.
	 * @param columnWrapper Колонка, для которой нужно выбрать значение.
	 * @param columnIndex Индекс колонки в списке колонок.
	 * @param columnCount Общее число колонок.
	 */
	private static Object getPropValue(Object[] row, ColumnWrapper columnWrapper, int columnIndex, int columnCount)
	{
		if (!columnWrapper.isSupportedTitle())
			return row[columnIndex + columnCount];
		long entityId = (Long)row[columnIndex];
		IEntity entity = DataAccessServices.dao().get(entityId);
		return entity.getProperty(columnWrapper.getTitleProperty());
	}

	/**
	 * Получить список элементов серчлиста (вообще-то там должна быть только одна строка) для случая, когда ни одна колонка не выбрана,
	 * т.е. группировки нет, и нужно вывести только общее количество план. студентов.
	 * @param counts Список (по идее из одного элемента) количества план. студентов.
	 */
	private static List<Object> wrapCollapsedRow(List<Long> counts)
	{
		int currIndex = 1;
		List<Object> result = new ArrayList<>();
		for (long count : counts)
		{
			DataWrapper wrapper = new DataWrapper();
			wrapper.setId(buildId(currIndex++, (int)count));
			wrapper.put(PROP_COUNT, count);
			wrapper.put(PROP_COUNT_MESSAGE, getPostfix(count));
			result.add(wrapper);
		}
		return result;
	}

    private static String getPostfix(long count)
    {
        return CommonBaseStringUtil.numberWithPostfixCase(count, " планируемого студента?", " планируемых студентов?", " планируемых студентов?" );
    }

    /** Получить суррогатный id строки план. контингента по индексу строки в списке и хешу, полученному на основе значений к колонках строки. */
	private static long buildId(int index, int hash)
	{
		long firstPart = (long)index << 32;
		long secondPart = hash & 0xFFFFFFFFL;
		return firstPart | secondPart;
	}

    public static class ColumnWrapper implements Serializable
    {
        private String key;
        private String title;
        private boolean required;
        private String path;
        private String titleProperty = "title";
        private boolean supportedTitle = false;
        private String orderProperty = "title";
        private boolean enabledByDefault;

        public ColumnWrapper(String key, String title, String entityPath)
        {
            this(key, title, false, entityPath);
        }

        public ColumnWrapper(String key, String title, boolean required, String entityPath)
        {
            this.key = key;
            this.title = title;
            this.required = required;
            this.path = entityPath;
        }

        public ColumnWrapper order(String property)
        {
            this.orderProperty = property;
            return this;
        }

        public ColumnWrapper title(String property)
        {
            this.titleProperty = property;
            this.orderProperty = property;
            return this;
        }

        public ColumnWrapper supportedTitle() {
            this.supportedTitle = true;
            return this;
        }

        public ColumnWrapper enabledByDefault() {
            enabledByDefault = true;
            return this;
        }

        public String getTitle() { return title; }
        public boolean isSupportedTitle() { return supportedTitle; }
        public boolean isRequired() { return required; }
        public String getKey() { return key; }
        public String getPath() { return path; }
        public String getTitleProperty() { return titleProperty; }
        public String getOrderProperty() { return orderProperty; }
        public boolean isEnabledByDefault() { return enabledByDefault; }

        public IDQLExpression getIdDQLProperty(String alias)
        {
            return property(alias + "." + getPath() + ".id");
        }

        public IDQLExpression getOrderDQLProperty(String alias)
        {
            return property(alias + "." + getPath() + "." + getOrderProperty());
        }

        public IDQLExpression getTitleDQLProperty(String alias)
        {
            return property(alias + "." + getPath() + "." + getTitleProperty());
        }
    }

    public static List<ColumnWrapper> getColumnList()
    {
        return columnList;
    }
}
