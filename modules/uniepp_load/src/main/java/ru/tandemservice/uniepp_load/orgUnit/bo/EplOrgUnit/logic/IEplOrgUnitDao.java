/* $Id:$ */
package ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author oleyba
 * @since 12/6/14
 */
public interface IEplOrgUnitDao extends INeedPersistenceSupport
{
    boolean isOrgUnitForming(OrgUnit orgUnit);

    boolean isOrgUnitOwner(OrgUnit orgUnit);
}