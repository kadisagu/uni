package ru.tandemservice.uniepp_load.base.entity.indPlan;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniepp_load.base.entity.indPlan.gen.*;
import ru.tandemservice.uniepp_load.util.UniEppLoadUtils;

/** @see ru.tandemservice.uniepp_load.base.entity.indPlan.gen.EplTimeItemNonEduLoadGen */
public class EplTimeItemNonEduLoad extends EplTimeItemNonEduLoadGen
{
    public double getTimeAmount()
    {
        return UniEppLoadUtils.wrap(getTimeAmountAsLong());
    }

    public void setTimeAmount(double timeAmount)
    {
        setTimeAmountAsLong(UniEppLoadUtils.unwrap(timeAmount));
    }

    public double getControlTimeAmount()
    {
        return UniEppLoadUtils.wrap(getControlTimeAmountAsLong());
    }

    public void setControlTimeAmount(double timeAmount)
    {
        setControlTimeAmountAsLong(UniEppLoadUtils.unwrap(timeAmount));
    }

    public String getTimeAmountStr()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(getTimeAmount());
    }
}