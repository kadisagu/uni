/* $Id:$ */
package ru.tandemservice.uniepp_load.loadPlan.util;

import com.google.common.collect.Maps;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem;
import ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.EplSettingsPpsLoadManager;
import ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad;

import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 12.10.2015
 */
public class PpsTimeStatsUtil
{
    /**
     * @param summaryId Сводка контингента
     * @param input     Входной map, в котором к каждому планируемому ППС прилинкованы часы нагрузки для планируемого ППС, которые необходимо обработать.
     * @return Метод возвращает map, в котором содержатся планируемые ППС для распределения нагрузки и строка с часами нагрузки.
     * map содержит в себе только планируемые ппс, для которых определены распределенные часы нагрузки для ППС.
     */
    public static Map<EplPlannedPps, ResultRow> getPpsTimeStatsRows(Long summaryId, Map<EplPlannedPps, Set<EplPlannedPpsTimeItem>> input)
    {
        final Map<PostBoundedWithQGandQL, EplSettingsPpsLoad> postMap = EplSettingsPpsLoadManager.instance().dao().getPostMap(summaryId);
        return getPpsTimeStatsRows(input, postMap);
    }

    public static Map<EplPlannedPps, ResultRow> getPpsTimeStatsRows(Map<EplPlannedPps, Set<EplPlannedPpsTimeItem>> input, final Map<PostBoundedWithQGandQL, EplSettingsPpsLoad> postMap)
    {
        final Map<EplPlannedPps, ResultRow> result = Maps.newHashMap();

        input.forEach((pps, timeItems) ->
        {
            result.put(pps, new ResultRow(getEplSettingsPpsLoadByPost(postMap, pps.getPost()), timeItems, pps));
        });
        return result;
    }

    public static EplSettingsPpsLoad getEplSettingsPpsLoadByPost(Map<PostBoundedWithQGandQL, EplSettingsPpsLoad> postMap, PostBoundedWithQGandQL post)
    {
        final EplSettingsPpsLoad toAll = postMap.get(null);
        final EplSettingsPpsLoad toPost = postMap.get(post);
        return (toPost != null && (toAll == null || toPost.getLevel() >= toAll.getLevel())) ? toPost : toAll;
    }

    public static Map<EplPlannedPps, ResultRow> getPpsTimeStatsRowsBySummary(Map<EplPlannedPps, Set<EplPlannedPpsTimeItem>> input, Map<MultiKey, EplSettingsPpsLoad> postMap)
    {
        final Map<EplPlannedPps, ResultRow> result = Maps.newHashMap();

        input.forEach((pps, timeItems) ->
        {
            final MultiKey key = new MultiKey(pps.getOrgUnitSummary().getId(), pps.getPost());
            final EplSettingsPpsLoad toAll = postMap.get(new MultiKey(pps.getOrgUnitSummary().getId(), null));
            final EplSettingsPpsLoad toPost = postMap.get(key);
            final EplSettingsPpsLoad settings = (toPost != null && (toAll == null || toPost.getLevel() >= toAll.getLevel())) ? toPost : toAll;
            result.put(pps, new ResultRow(settings, timeItems, pps));
        });
        return result;
    }

    /**
     * Обрабатывает все распределенные часы нагрузки в рамках сводки
     *
     * @param summaryId Сводка контингента
     * @return Метод возвращает map, в котором содержатся планируемые ППС для распределения нагрузки и строка с часами нагрузки.
     * map содержит в себе только планируемые ппс, для которых определены распределенные часы нагрузки для ППС.
     */
    public static Map<EplPlannedPps, ResultRow> getPpsTimeStatsRows(Long summaryId)
    {
        final List<Object[]> resultList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(EplPlannedPps.class, "pps")
                        .where(eq(property("pps", EplPlannedPps.orgUnitSummary().id()), value(summaryId)))
                        .joinEntity("pps", DQLJoinType.inner, EplPlannedPpsTimeItem.class, "ti", eq(property("ti", EplPlannedPpsTimeItem.pps()), property("pps")))
                        .column(property("pps"))
                        .column(property("ti"))
        );

        final Map<EplPlannedPps, Set<EplPlannedPpsTimeItem>> ppsTimeMap = Maps.newHashMap();
        resultList.forEach(row -> SafeMap.safeGet(ppsTimeMap, (EplPlannedPps) row[0], HashSet.class).add((EplPlannedPpsTimeItem) row[1]));

        return getPpsTimeStatsRows(summaryId, ppsTimeMap);
    }

    /**
     * Утильный класс, строка, которая содержит в себе часы нагрузки в требуемом формате (ставка/сверх_ставки/всего)
     * может возвращать как числовые значение, так и сообщения в формате «[значение] (больше/меньше [предел])»
     */
    public static class ResultRow
    {
        private EplPlannedPps _pps;
        private double rate;
        private double overrate;

        private double minStaffRate;
        private double maxStaffRate;
        private double additionStaffRate;

        private boolean errorInRate;
        private boolean errorInOverate;
        private boolean errorInTotal;

        private boolean integral;

        private DoubleFormatter format = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS;

        /**
         * @param settings  настройки предельных значений учебной нагрузки на ставку
         * @param timeItems часы нагрузки для планируемого ППС, которые необходимо обработать
         * @param pps       планируемый ППС
         */
        public ResultRow(EplSettingsPpsLoad settings, @NotNull Set<EplPlannedPpsTimeItem> timeItems, EplPlannedPps pps)
        {
            _pps = pps;
            for (EplPlannedPpsTimeItem timeItem : timeItems)
            {
                if (timeItem.isOverTime())
                    overrate += timeItem.getTimeAmount();
                else
                    rate += timeItem.getTimeAmount();
            }
            onCheckErrors(settings, pps.getStaffRateAsDouble());
        }

        public ResultRow(EplSettingsPpsLoad settings, double r, double o, EplPlannedPps pps)
        {
            _pps = pps;
            rate = r;
            overrate = o;
            onCheckErrors(settings, pps.getStaffRateAsDouble());
        }

        public ResultRow()
        {
        }

        private void onCheckErrors(EplSettingsPpsLoad settings, Double staffRate)
        {
            if (_pps.isTimeWorker())
            {
                minStaffRate = 0;
                maxStaffRate = _pps.getMaxTimeAmount();
            }
            else
            {
                if (null == settings || null == staffRate) return;

                minStaffRate = settings.getHoursMin() * staffRate;
                maxStaffRate = settings.getHoursMax() * staffRate;
            }
            additionStaffRate = _pps.getMaxTimeAmount();

            errorInRate = minStaffRate > rate || (maxStaffRate != 0 && maxStaffRate < rate);
            errorInOverate = additionStaffRate != 0 && additionStaffRate < overrate;
            errorInTotal = _pps.isTimeWorker() ? (maxStaffRate != 0 && maxStaffRate < rate) : errorInRate || errorInOverate;
        }

        /**
         * @return форматированное сообщение для колонки «Ставка» в формате «[значение] (больше/меньше [предел])»
         */
        public String getRateMessage()
        {
            StringBuilder builder = new StringBuilder().append(format.format(rate));
            if (isErrorInRate() && !integral)
            {
                if (minStaffRate > rate) builder.append(" (меньше ").append(format.format(minStaffRate)).append(")");
                else builder.append(" (больше ").append(format.format(maxStaffRate)).append(")");
            }
            return builder.toString();
        }

        public String getRateMessageColor()
        {
            return colorValue(getRateMessage(), isErrorInRate());
        }

        /**
         * @return форматированное сообщение для колонки «Сверх ставки» в формате «[значение] (больше/меньше [предел])»
         */
        public String getOverrateMessage()
        {
            StringBuilder builder = new StringBuilder().append(format.format(overrate));
            if (isErrorInOverrate() && !integral)
                builder.append(" (больше ").append(format.format(additionStaffRate)).append(")");
            return builder.toString();
        }

        public String getOverrateMessageColor()
        {
            return colorValue(getOverrateMessage(), isErrorInOverrate());
        }

        /**
         * @return форматированное сообщение для колонки «Всего» в формате «[значение] (больше/меньше [предел])»
         */
        public String getTotalMessage()
        {
            if (_pps == null  || !_pps.isTimeWorker()) return format.format(getTotal());

            StringBuilder builder = new StringBuilder().append(format.format(rate));
            if (isErrorInTotal() && !integral)
                builder.append(" (больше ").append(format.format(maxStaffRate)).append(")");
            return builder.toString();
        }

        public String getTotalMessageColor()
        {
            return colorValue(getTotalMessage(), isErrorInTotal());
        }

        public boolean isErrorInRate() { return this.errorInRate; }
        public boolean isErrorInOverrate() { return this.errorInOverate; }
        public boolean isErrorInTotal() { return errorInTotal; }

        public double getRate() {
            return rate;
        }

        public double getOverrate() {
            return overrate;
        }

        public double getTotal() {
            return rate + overrate;
        }

        public void append(ResultRow e)
        {
            if (e == null)
                return;
            rate += e.rate;
            overrate += e.overrate;

            _pps = e._pps;

            errorInRate = errorInRate || e.errorInRate;
            errorInOverate = errorInOverate || e.errorInOverate;
            errorInTotal = errorInTotal || e.errorInTotal;

            integral = true;
        }
    }

    /**
     * @return форматированное сообщение для вывода в html raw формате.
     */
    public static String colorValue(final String value, boolean error)
    {
        return error ? "<span style=\"color:" + UniDefines.COLOR_CELL_ERROR + "\">" + value + "</span>" : value;
    }
}
