package ru.tandemservice.uniepp_load.base.entity.indPlan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleNonEduLoad;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Рассчитанные часы внеучебной нагрузки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplTimeItemNonEduLoadGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad";
    public static final String ENTITY_NAME = "eplTimeItemNonEduLoad";
    public static final int VERSION_HASH = -600726447;
    private static IEntityMeta ENTITY_META;

    public static final String L_IND_PLAN = "indPlan";
    public static final String L_TIME_RULE = "timeRule";
    public static final String P_TIME_AMOUNT_AS_LONG = "timeAmountAsLong";
    public static final String P_CONTROL_TIME_AMOUNT_AS_LONG = "controlTimeAmountAsLong";
    public static final String P_PARAMETER = "parameter";
    public static final String L_YEAR_PART = "yearPart";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_CREATION_DATE = "creationDate";

    private EplIndividualPlan _indPlan;     // Индивидуальный план преподавателя
    private EplTimeRuleNonEduLoad _timeRule;     // Норма времени внеучебной нагрузки
    private long _timeAmountAsLong;     // Число часов
    private long _controlTimeAmountAsLong;     // Контрольное число часов
    private Double _parameter;     // Параметр
    private YearDistributionPart _yearPart;     // Часть учебного года
    private Date _dateFrom;     // Дата с
    private Date _dateTo;     // Дата по
    private Date _creationDate;     // Дата создания объекта

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Индивидуальный план преподавателя. Свойство не может быть null.
     */
    @NotNull
    public EplIndividualPlan getIndPlan()
    {
        return _indPlan;
    }

    /**
     * @param indPlan Индивидуальный план преподавателя. Свойство не может быть null.
     */
    public void setIndPlan(EplIndividualPlan indPlan)
    {
        dirty(_indPlan, indPlan);
        _indPlan = indPlan;
    }

    /**
     * @return Норма времени внеучебной нагрузки. Свойство не может быть null.
     */
    @NotNull
    public EplTimeRuleNonEduLoad getTimeRule()
    {
        return _timeRule;
    }

    /**
     * @param timeRule Норма времени внеучебной нагрузки. Свойство не может быть null.
     */
    public void setTimeRule(EplTimeRuleNonEduLoad timeRule)
    {
        dirty(_timeRule, timeRule);
        _timeRule = timeRule;
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Число часов. Свойство не может быть null.
     */
    @NotNull
    public long getTimeAmountAsLong()
    {
        return _timeAmountAsLong;
    }

    /**
     * @param timeAmountAsLong Число часов. Свойство не может быть null.
     */
    public void setTimeAmountAsLong(long timeAmountAsLong)
    {
        dirty(_timeAmountAsLong, timeAmountAsLong);
        _timeAmountAsLong = timeAmountAsLong;
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Контрольное число часов. Свойство не может быть null.
     */
    @NotNull
    public long getControlTimeAmountAsLong()
    {
        return _controlTimeAmountAsLong;
    }

    /**
     * @param controlTimeAmountAsLong Контрольное число часов. Свойство не может быть null.
     */
    public void setControlTimeAmountAsLong(long controlTimeAmountAsLong)
    {
        dirty(_controlTimeAmountAsLong, controlTimeAmountAsLong);
        _controlTimeAmountAsLong = controlTimeAmountAsLong;
    }

    /**
     * @return Параметр.
     */
    public Double getParameter()
    {
        return _parameter;
    }

    /**
     * @param parameter Параметр.
     */
    public void setParameter(Double parameter)
    {
        dirty(_parameter, parameter);
        _parameter = parameter;
    }

    /**
     * @return Часть учебного года.
     */
    public YearDistributionPart getYearPart()
    {
        return _yearPart;
    }

    /**
     * @param yearPart Часть учебного года.
     */
    public void setYearPart(YearDistributionPart yearPart)
    {
        dirty(_yearPart, yearPart);
        _yearPart = yearPart;
    }

    /**
     * @return Дата с.
     */
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Дата с.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Дата по.
     */
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Дата по.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Дата создания объекта. Свойство не может быть null.
     */
    @NotNull
    public Date getCreationDate()
    {
        return _creationDate;
    }

    /**
     * @param creationDate Дата создания объекта. Свойство не может быть null.
     */
    public void setCreationDate(Date creationDate)
    {
        dirty(_creationDate, creationDate);
        _creationDate = creationDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EplTimeItemNonEduLoadGen)
        {
            setIndPlan(((EplTimeItemNonEduLoad)another).getIndPlan());
            setTimeRule(((EplTimeItemNonEduLoad)another).getTimeRule());
            setTimeAmountAsLong(((EplTimeItemNonEduLoad)another).getTimeAmountAsLong());
            setControlTimeAmountAsLong(((EplTimeItemNonEduLoad)another).getControlTimeAmountAsLong());
            setParameter(((EplTimeItemNonEduLoad)another).getParameter());
            setYearPart(((EplTimeItemNonEduLoad)another).getYearPart());
            setDateFrom(((EplTimeItemNonEduLoad)another).getDateFrom());
            setDateTo(((EplTimeItemNonEduLoad)another).getDateTo());
            setCreationDate(((EplTimeItemNonEduLoad)another).getCreationDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplTimeItemNonEduLoadGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplTimeItemNonEduLoad.class;
        }

        public T newInstance()
        {
            return (T) new EplTimeItemNonEduLoad();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "indPlan":
                    return obj.getIndPlan();
                case "timeRule":
                    return obj.getTimeRule();
                case "timeAmountAsLong":
                    return obj.getTimeAmountAsLong();
                case "controlTimeAmountAsLong":
                    return obj.getControlTimeAmountAsLong();
                case "parameter":
                    return obj.getParameter();
                case "yearPart":
                    return obj.getYearPart();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "creationDate":
                    return obj.getCreationDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "indPlan":
                    obj.setIndPlan((EplIndividualPlan) value);
                    return;
                case "timeRule":
                    obj.setTimeRule((EplTimeRuleNonEduLoad) value);
                    return;
                case "timeAmountAsLong":
                    obj.setTimeAmountAsLong((Long) value);
                    return;
                case "controlTimeAmountAsLong":
                    obj.setControlTimeAmountAsLong((Long) value);
                    return;
                case "parameter":
                    obj.setParameter((Double) value);
                    return;
                case "yearPart":
                    obj.setYearPart((YearDistributionPart) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "creationDate":
                    obj.setCreationDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "indPlan":
                        return true;
                case "timeRule":
                        return true;
                case "timeAmountAsLong":
                        return true;
                case "controlTimeAmountAsLong":
                        return true;
                case "parameter":
                        return true;
                case "yearPart":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "creationDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "indPlan":
                    return true;
                case "timeRule":
                    return true;
                case "timeAmountAsLong":
                    return true;
                case "controlTimeAmountAsLong":
                    return true;
                case "parameter":
                    return true;
                case "yearPart":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "creationDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "indPlan":
                    return EplIndividualPlan.class;
                case "timeRule":
                    return EplTimeRuleNonEduLoad.class;
                case "timeAmountAsLong":
                    return Long.class;
                case "controlTimeAmountAsLong":
                    return Long.class;
                case "parameter":
                    return Double.class;
                case "yearPart":
                    return YearDistributionPart.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "creationDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplTimeItemNonEduLoad> _dslPath = new Path<EplTimeItemNonEduLoad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplTimeItemNonEduLoad");
    }
            

    /**
     * @return Индивидуальный план преподавателя. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad#getIndPlan()
     */
    public static EplIndividualPlan.Path<EplIndividualPlan> indPlan()
    {
        return _dslPath.indPlan();
    }

    /**
     * @return Норма времени внеучебной нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad#getTimeRule()
     */
    public static EplTimeRuleNonEduLoad.Path<EplTimeRuleNonEduLoad> timeRule()
    {
        return _dslPath.timeRule();
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad#getTimeAmountAsLong()
     */
    public static PropertyPath<Long> timeAmountAsLong()
    {
        return _dslPath.timeAmountAsLong();
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Контрольное число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad#getControlTimeAmountAsLong()
     */
    public static PropertyPath<Long> controlTimeAmountAsLong()
    {
        return _dslPath.controlTimeAmountAsLong();
    }

    /**
     * @return Параметр.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad#getParameter()
     */
    public static PropertyPath<Double> parameter()
    {
        return _dslPath.parameter();
    }

    /**
     * @return Часть учебного года.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad#getYearPart()
     */
    public static YearDistributionPart.Path<YearDistributionPart> yearPart()
    {
        return _dslPath.yearPart();
    }

    /**
     * @return Дата с.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Дата по.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Дата создания объекта. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad#getCreationDate()
     */
    public static PropertyPath<Date> creationDate()
    {
        return _dslPath.creationDate();
    }

    public static class Path<E extends EplTimeItemNonEduLoad> extends EntityPath<E>
    {
        private EplIndividualPlan.Path<EplIndividualPlan> _indPlan;
        private EplTimeRuleNonEduLoad.Path<EplTimeRuleNonEduLoad> _timeRule;
        private PropertyPath<Long> _timeAmountAsLong;
        private PropertyPath<Long> _controlTimeAmountAsLong;
        private PropertyPath<Double> _parameter;
        private YearDistributionPart.Path<YearDistributionPart> _yearPart;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<Date> _creationDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Индивидуальный план преподавателя. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad#getIndPlan()
     */
        public EplIndividualPlan.Path<EplIndividualPlan> indPlan()
        {
            if(_indPlan == null )
                _indPlan = new EplIndividualPlan.Path<EplIndividualPlan>(L_IND_PLAN, this);
            return _indPlan;
        }

    /**
     * @return Норма времени внеучебной нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad#getTimeRule()
     */
        public EplTimeRuleNonEduLoad.Path<EplTimeRuleNonEduLoad> timeRule()
        {
            if(_timeRule == null )
                _timeRule = new EplTimeRuleNonEduLoad.Path<EplTimeRuleNonEduLoad>(L_TIME_RULE, this);
            return _timeRule;
        }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad#getTimeAmountAsLong()
     */
        public PropertyPath<Long> timeAmountAsLong()
        {
            if(_timeAmountAsLong == null )
                _timeAmountAsLong = new PropertyPath<Long>(EplTimeItemNonEduLoadGen.P_TIME_AMOUNT_AS_LONG, this);
            return _timeAmountAsLong;
        }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Контрольное число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad#getControlTimeAmountAsLong()
     */
        public PropertyPath<Long> controlTimeAmountAsLong()
        {
            if(_controlTimeAmountAsLong == null )
                _controlTimeAmountAsLong = new PropertyPath<Long>(EplTimeItemNonEduLoadGen.P_CONTROL_TIME_AMOUNT_AS_LONG, this);
            return _controlTimeAmountAsLong;
        }

    /**
     * @return Параметр.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad#getParameter()
     */
        public PropertyPath<Double> parameter()
        {
            if(_parameter == null )
                _parameter = new PropertyPath<Double>(EplTimeItemNonEduLoadGen.P_PARAMETER, this);
            return _parameter;
        }

    /**
     * @return Часть учебного года.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad#getYearPart()
     */
        public YearDistributionPart.Path<YearDistributionPart> yearPart()
        {
            if(_yearPart == null )
                _yearPart = new YearDistributionPart.Path<YearDistributionPart>(L_YEAR_PART, this);
            return _yearPart;
        }

    /**
     * @return Дата с.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EplTimeItemNonEduLoadGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Дата по.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EplTimeItemNonEduLoadGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Дата создания объекта. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad#getCreationDate()
     */
        public PropertyPath<Date> creationDate()
        {
            if(_creationDate == null )
                _creationDate = new PropertyPath<Date>(EplTimeItemNonEduLoadGen.P_CREATION_DATE, this);
            return _creationDate;
        }

        public Class getEntityClass()
        {
            return EplTimeItemNonEduLoad.class;
        }

        public String getEntityName()
        {
            return "eplTimeItemNonEduLoad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
