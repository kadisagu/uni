/* $Id:$ */
package ru.tandemservice.uniepp_load.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp_load.base.bo.EplState.logic.EplStateDao;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplStudentSummaryState;

import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 12/19/14
 */
public class EplOrgUnitPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(final Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        final SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uniepp_load");
        config.setName("uniepp_load-ou-sec-config");
        config.setTitle("");

        final ModuleGlobalGroupMeta mggOrgstruct = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
        final ModuleLocalGroupMeta mlgOrgstruct = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");

        final ModuleLocalGroupMeta mlgTraining = PermissionMetaUtil.createModuleLocalGroup(config, "eplLocal", "Модуль «Нагрузка»");
        final ClassGroupMeta lcgJournal = PermissionMetaUtil.createClassGroup(mlgTraining, "eplOrgUnitSummaryLocalClass", "Объект «Расчет учебной нагрузки на читающем подразделении»", EplOrgUnitSummary.class.getName());
        PermissionMetaUtil.createGroupRelation(config, "eplOrgUnitSummaryPG", lcgJournal.getName());

        final ClassGroupMeta indPlanPg = PermissionMetaUtil.createClassGroup(mlgTraining, "eplIndividualPlanLocalClass", "Объект «Индивидуальный план преподавателя»", EplIndividualPlan.class.getName());
        PermissionMetaUtil.createGroupRelation(config, "eplIndividualPlanPG", indPlanPg.getName());

        for (final OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            String code = description.getCode();
            final ClassGroupMeta gcg = PermissionMetaUtil.createClassGroup(mggOrgstruct, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
            final ClassGroupMeta lcg = PermissionMetaUtil.createClassGroup(mlgOrgstruct, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

            PermissionGroupMeta pgEplTab = PermissionMetaUtil.createPermissionGroup(config, code + "EplTabPG", "Вкладка «Нагрузка»");
            PermissionMetaUtil.createGroupRelation(config, pgEplTab.getName(), gcg.getName());
            PermissionMetaUtil.createGroupRelation(config, pgEplTab.getName(), lcg.getName());
            PermissionMetaUtil.createPermission(pgEplTab, "viewEplTab_" + code, "Просмотр");

            PermissionGroupMeta pgGroupTab = PermissionMetaUtil.createPermissionGroup(pgEplTab, code + "EplGroupTabPG", "Вкладка «Контингент»");
            PermissionMetaUtil.createPermission(pgGroupTab, "viewEplGroupTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgGroupTab, "editGroupsEplGroupTab_" + code, "Редактирование числа студентов");
            PermissionMetaUtil.createPermission(pgGroupTab, "editContingentGroupsEplGroupTab_" + code, "Редактирование контингента");
            PermissionMetaUtil.createPermission(pgGroupTab, "transferAffiliateEplGroupTab_" + code, "Перевод из филиала");
            PermissionMetaUtil.createPermission(pgGroupTab, "editGroupTitlesEplGroupTab_" + code, "Редактирование названий групп");
	        PermissionMetaUtil.createPermission(pgGroupTab, "deleteEplStudentsEplGroupTab_" + code, "Удаление планируемых студентов");



            PermissionGroupMeta pgWpTab = PermissionMetaUtil.createPermissionGroup(pgEplTab, code + "EplWpTabPG", "Вкладка «РУП»");
            PermissionMetaUtil.createPermission(pgWpTab, "viewEplWpTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgWpTab, "editWpEplWpTab_" + code, "Указание РУП");

            PermissionGroupMeta pgEduGroupTab = PermissionMetaUtil.createPermissionGroup(pgEplTab, code + "EplEduGroupTabPG", "Вкладка «Потоки»");
            PermissionMetaUtil.createPermission(pgEduGroupTab, "viewEplEduGroupTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgEduGroupTab, "editEduGroupEplEduGroupTab_" + code, "Редактирование план. потоков");

            PermissionGroupMeta pgOuSummaryTab = PermissionMetaUtil.createPermissionGroup(pgEplTab, code + "EplOuSummaryTabPG", "Вкладка «Учебная нагрузка»");
            PermissionMetaUtil.createPermission(pgOuSummaryTab, "viewEplOuSummaryTab_" + code, "Просмотр");

            PermissionGroupMeta pgIndPlanTab = PermissionMetaUtil.createPermissionGroup(pgEplTab, code + "EplIndPlanTabPG", "Вкладка «ИПП»");
            PermissionMetaUtil.createPermission(pgIndPlanTab, "viewEplIndPlanTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgIndPlanTab, "viewEplIndPlanTab_add_" + code, "Добавление ИПП");
            PermissionMetaUtil.createPermission(pgIndPlanTab, "viewEplIndPlanTab_print_" + code, "Печать ИПП");
            PermissionMetaUtil.createPermission(pgIndPlanTab, "viewEplIndPlanTab_edit_" + code, "Редактирование ИПП");
            PermissionMetaUtil.createPermission(pgIndPlanTab, "viewEplIndPlanTab_delete_" + code, "Удаление ИПП");
        }

        final List<EplStudentSummaryState> stateList = UniDaoFacade.getCoreDao().getList(EplStudentSummaryState.class);

        final PermissionGroupMeta pg = PermissionMetaUtil.createPermissionGroup(config, "eplStudentSummaryPG", "Объект «Сводка контингента»");
        for (final EplStudentSummaryState state : stateList)
        {
            PermissionMetaUtil.createPermission(pg, EplStateDao.getPermissionStr(state.getPermissionSuffix()), "Перевод в состояние «" + state.getTitle() + "»");
        }

        securityConfigMetaMap.put(config.getName(), config);
    }
}

