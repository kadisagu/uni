package ru.tandemservice.uniepp_load.base.entity.indPlan;

import ru.tandemservice.uniepp_load.base.entity.indPlan.gen.*;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;

/** @see ru.tandemservice.uniepp_load.base.entity.indPlan.gen.EplIndPlan2OuSummaryRelGen */
public class EplIndPlan2OuSummaryRel extends EplIndPlan2OuSummaryRelGen
{
    public EplIndPlan2OuSummaryRel() {}

    public EplIndPlan2OuSummaryRel(EplIndividualPlan indPlan, EplOrgUnitSummary ouSummary)
    {
        setIndPlan(indPlan);
        setOuSummary(ouSummary);
    }
}