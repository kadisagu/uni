package ru.tandemservice.uniepp_load.base.entity.ouSummary;

import org.tandemframework.core.common.IEntityDebugTitled;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.gen.EplEduGroupTimeItemGen;

/**
 * Часы нагрузки для план. потока
 */
public class EplEduGroupTimeItem extends EplEduGroupTimeItemGen implements IEntityDebugTitled
{
    @Override
    public String getEntityDebugTitle()
    {
        return getDescription();
    }
}