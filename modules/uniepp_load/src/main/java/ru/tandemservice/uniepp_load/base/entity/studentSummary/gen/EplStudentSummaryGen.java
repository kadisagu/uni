package ru.tandemservice.uniepp_load.base.entity.studentSummary.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplStudentSummaryState;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сводка контингента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplStudentSummaryGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary";
    public static final String ENTITY_NAME = "eplStudentSummary";
    public static final int VERSION_HASH = -1840279677;
    private static IEntityMeta ENTITY_META;

    public static final String L_EPP_YEAR = "eppYear";
    public static final String P_TITLE = "title";
    public static final String P_CREATION_DATE = "creationDate";
    public static final String P_ARCHIVAL = "archival";
    public static final String L_STATE = "state";
    public static final String L_ZIP_LOG = "zipLog";
    public static final String P_CREATION_DATE_STR = "creationDateStr";
    public static final String P_TITLE_WITH_STATE = "titleWithState";

    private EppYearEducationProcess _eppYear;     // Учебный год
    private String _title;     // Название
    private Date _creationDate;     // Дата создания объекта
    private boolean _archival;     // В архиве
    private EplStudentSummaryState _state;     // Состояние
    private DatabaseFile _zipLog;     // ZIP: журнал

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EppYearEducationProcess getEppYear()
    {
        return _eppYear;
    }

    /**
     * @param eppYear Учебный год. Свойство не может быть null.
     */
    public void setEppYear(EppYearEducationProcess eppYear)
    {
        dirty(_eppYear, eppYear);
        _eppYear = eppYear;
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Дата создания объекта. Свойство не может быть null.
     */
    @NotNull
    public Date getCreationDate()
    {
        return _creationDate;
    }

    /**
     * @param creationDate Дата создания объекта. Свойство не может быть null.
     */
    public void setCreationDate(Date creationDate)
    {
        dirty(_creationDate, creationDate);
        _creationDate = creationDate;
    }

    /**
     * @return В архиве. Свойство не может быть null.
     */
    @NotNull
    public boolean isArchival()
    {
        return _archival;
    }

    /**
     * @param archival В архиве. Свойство не может быть null.
     */
    public void setArchival(boolean archival)
    {
        dirty(_archival, archival);
        _archival = archival;
    }

    /**
     * @return Состояние. Свойство не может быть null.
     */
    @NotNull
    public EplStudentSummaryState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние. Свойство не может быть null.
     */
    public void setState(EplStudentSummaryState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * Текстовый журнал, содержащий информацию об обновлении сводки.
     *
     * @return ZIP: журнал.
     */
    public DatabaseFile getZipLog()
    {
        return _zipLog;
    }

    /**
     * @param zipLog ZIP: журнал.
     */
    public void setZipLog(DatabaseFile zipLog)
    {
        dirty(_zipLog, zipLog);
        _zipLog = zipLog;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EplStudentSummaryGen)
        {
            setEppYear(((EplStudentSummary)another).getEppYear());
            setTitle(((EplStudentSummary)another).getTitle());
            setCreationDate(((EplStudentSummary)another).getCreationDate());
            setArchival(((EplStudentSummary)another).isArchival());
            setState(((EplStudentSummary)another).getState());
            setZipLog(((EplStudentSummary)another).getZipLog());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplStudentSummaryGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplStudentSummary.class;
        }

        public T newInstance()
        {
            return (T) new EplStudentSummary();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eppYear":
                    return obj.getEppYear();
                case "title":
                    return obj.getTitle();
                case "creationDate":
                    return obj.getCreationDate();
                case "archival":
                    return obj.isArchival();
                case "state":
                    return obj.getState();
                case "zipLog":
                    return obj.getZipLog();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eppYear":
                    obj.setEppYear((EppYearEducationProcess) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "creationDate":
                    obj.setCreationDate((Date) value);
                    return;
                case "archival":
                    obj.setArchival((Boolean) value);
                    return;
                case "state":
                    obj.setState((EplStudentSummaryState) value);
                    return;
                case "zipLog":
                    obj.setZipLog((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eppYear":
                        return true;
                case "title":
                        return true;
                case "creationDate":
                        return true;
                case "archival":
                        return true;
                case "state":
                        return true;
                case "zipLog":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eppYear":
                    return true;
                case "title":
                    return true;
                case "creationDate":
                    return true;
                case "archival":
                    return true;
                case "state":
                    return true;
                case "zipLog":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eppYear":
                    return EppYearEducationProcess.class;
                case "title":
                    return String.class;
                case "creationDate":
                    return Date.class;
                case "archival":
                    return Boolean.class;
                case "state":
                    return EplStudentSummaryState.class;
                case "zipLog":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplStudentSummary> _dslPath = new Path<EplStudentSummary>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplStudentSummary");
    }
            

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary#getEppYear()
     */
    public static EppYearEducationProcess.Path<EppYearEducationProcess> eppYear()
    {
        return _dslPath.eppYear();
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Дата создания объекта. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary#getCreationDate()
     */
    public static PropertyPath<Date> creationDate()
    {
        return _dslPath.creationDate();
    }

    /**
     * @return В архиве. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary#isArchival()
     */
    public static PropertyPath<Boolean> archival()
    {
        return _dslPath.archival();
    }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary#getState()
     */
    public static EplStudentSummaryState.Path<EplStudentSummaryState> state()
    {
        return _dslPath.state();
    }

    /**
     * Текстовый журнал, содержащий информацию об обновлении сводки.
     *
     * @return ZIP: журнал.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary#getZipLog()
     */
    public static DatabaseFile.Path<DatabaseFile> zipLog()
    {
        return _dslPath.zipLog();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary#getCreationDateStr()
     */
    public static SupportedPropertyPath<String> creationDateStr()
    {
        return _dslPath.creationDateStr();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary#getTitleWithState()
     */
    public static SupportedPropertyPath<String> titleWithState()
    {
        return _dslPath.titleWithState();
    }

    public static class Path<E extends EplStudentSummary> extends EntityPath<E>
    {
        private EppYearEducationProcess.Path<EppYearEducationProcess> _eppYear;
        private PropertyPath<String> _title;
        private PropertyPath<Date> _creationDate;
        private PropertyPath<Boolean> _archival;
        private EplStudentSummaryState.Path<EplStudentSummaryState> _state;
        private DatabaseFile.Path<DatabaseFile> _zipLog;
        private SupportedPropertyPath<String> _creationDateStr;
        private SupportedPropertyPath<String> _titleWithState;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary#getEppYear()
     */
        public EppYearEducationProcess.Path<EppYearEducationProcess> eppYear()
        {
            if(_eppYear == null )
                _eppYear = new EppYearEducationProcess.Path<EppYearEducationProcess>(L_EPP_YEAR, this);
            return _eppYear;
        }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EplStudentSummaryGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Дата создания объекта. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary#getCreationDate()
     */
        public PropertyPath<Date> creationDate()
        {
            if(_creationDate == null )
                _creationDate = new PropertyPath<Date>(EplStudentSummaryGen.P_CREATION_DATE, this);
            return _creationDate;
        }

    /**
     * @return В архиве. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary#isArchival()
     */
        public PropertyPath<Boolean> archival()
        {
            if(_archival == null )
                _archival = new PropertyPath<Boolean>(EplStudentSummaryGen.P_ARCHIVAL, this);
            return _archival;
        }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary#getState()
     */
        public EplStudentSummaryState.Path<EplStudentSummaryState> state()
        {
            if(_state == null )
                _state = new EplStudentSummaryState.Path<EplStudentSummaryState>(L_STATE, this);
            return _state;
        }

    /**
     * Текстовый журнал, содержащий информацию об обновлении сводки.
     *
     * @return ZIP: журнал.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary#getZipLog()
     */
        public DatabaseFile.Path<DatabaseFile> zipLog()
        {
            if(_zipLog == null )
                _zipLog = new DatabaseFile.Path<DatabaseFile>(L_ZIP_LOG, this);
            return _zipLog;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary#getCreationDateStr()
     */
        public SupportedPropertyPath<String> creationDateStr()
        {
            if(_creationDateStr == null )
                _creationDateStr = new SupportedPropertyPath<String>(EplStudentSummaryGen.P_CREATION_DATE_STR, this);
            return _creationDateStr;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary#getTitleWithState()
     */
        public SupportedPropertyPath<String> titleWithState()
        {
            if(_titleWithState == null )
                _titleWithState = new SupportedPropertyPath<String>(EplStudentSummaryGen.P_TITLE_WITH_STATE, this);
            return _titleWithState;
        }

        public Class getEntityClass()
        {
            return EplStudentSummary.class;
        }

        public String getEntityName()
        {
            return "eplStudentSummary";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getCreationDateStr();

    public abstract String getTitleWithState();
}
