package ru.tandemservice.uniepp_load.report.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.uniepp_load.report.bo.EplReport.ui.HourlyFundAdd.EplReportHourlyFundAdd;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.logic.IEplReport;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.logic.IEplStorableReportDesc;
import ru.tandemservice.uniepp_load.report.entity.gen.EplReportHourlyFundGen;

import java.util.Arrays;
import java.util.List;

/** @see ru.tandemservice.uniepp_load.report.entity.gen.EplReportHourlyFundGen */
public class EplReportHourlyFund extends EplReportHourlyFundGen implements IEplReport
{
    private static final List<String> VIEW_PROP_LIST = Arrays.asList(EplReportHourlyFundGen.L_EDU_YEAR, P_STUDENT_SUMMARY);

    public static final IEplStorableReportDesc DESC = new IEplStorableReportDesc() {
        @Override public String getReportKey() { return EplReportHourlyFund.class.getSimpleName(); }
        @Override public Class<? extends IEplReport> getReportClass() { return EplReportHourlyFund.class; }
        @Override public List<String> getPubPropertyList() { return VIEW_PROP_LIST; }
        @Override public List<String> getListPropertyList() { return VIEW_PROP_LIST; }
        @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return EplReportHourlyFundAdd.class; }
    };

    @Override
    public IEplStorableReportDesc getDesc()
    {
        return DESC;
    }
}