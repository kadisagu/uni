/* $Id$ */
package ru.tandemservice.uniepp_load.report.bo.EplReport.ui.OrgUnitEduAssignmentAdd;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplPrintScript;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplPrintScriptCodes;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.ui.Pub.EplReportBasePub;
import ru.tandemservice.uniepp_load.report.entity.EplReportOrgUnitEduAssignment;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 25.06.2016
 */
public class EplReportOrgUnitEduAssignmentAddUI extends UIPresenter
{
    public static final String PARAM_STUDENT_SUMMARY = EplOrgUnitSummary.PARAM_STUDENT_SUMMARY;
    public static final String PARAM_OU_SUMMARY = "ouSummary";
    public static final String PARAM_EPP_YEAR = "eppYear";
    public static final String PARAM_YEAR_PART_LIST = "yearPartList";
    public static final String PARAM_DEVELOP_FORM_LIST = "developFormList";
    public static final String PARAM_DEVELOP_CONDITION_LIST = "developConditionList";

    public void onClickApply()
    {
        getSettings().save();
        Long reportId = IUniBaseDao.instance.get().doInTransaction(session -> {
            EplOrgUnitSummary ouSummary = getSettings().get(PARAM_OU_SUMMARY);
            List<YearDistributionPart> yearPartList = getSettings().get(PARAM_YEAR_PART_LIST);
            List<DevelopForm> developFormList = getSettings().get(PARAM_DEVELOP_FORM_LIST);
            List<DevelopCondition> developConditionList = getSettings().get(PARAM_DEVELOP_CONDITION_LIST);

            EplReportOrgUnitEduAssignment report = new EplReportOrgUnitEduAssignment();

            report.setFormingDate(new Date());
            report.setEduYear(ouSummary.getStudentSummary().getEppYear().getEducationYear());
            report.setStudentSummary(ouSummary.getStudentSummary().getTitle());
            report.setOrgUnit(ouSummary.getOrgUnit().getFullTitle());

            Collection<Long> yearPartIds = Lists.newArrayList();
            Collection<Long> developFormIds = Lists.newArrayList();
            Collection<Long> developConditionIds = Lists.newArrayList();

            if (CollectionUtils.isNotEmpty(yearPartList))
            {
                report.setYearPart(StringUtils.join(yearPartList, ", "));
                yearPartIds = UniBaseDao.ids(yearPartList);
            }
            if (CollectionUtils.isNotEmpty(developFormList))
            {
                report.setDevelopForm(StringUtils.join(developFormList, ", "));
                developFormIds = UniBaseDao.ids(developFormList);
            }
            if (CollectionUtils.isNotEmpty(developConditionList))
            {
                report.setDevelopCondition(StringUtils.join(developConditionList, ", "));
                developConditionIds = UniBaseDao.ids(developConditionList);
            }

            EplPrintScript scriptItem = DataAccessServices.dao().getByCode(EplPrintScript.class, EplPrintScriptCodes.ORG_UNIT_EDU_ASSIGNMENT);
            Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(
                    scriptItem, IScriptExecutor.TEMPLATE_VARIABLE, new byte[]{},
                    "ouSummaryId", ouSummary.getId(), "yearPartIds", yearPartIds, "developFormIds", developFormIds, "developConditionIds", developConditionIds
            );

            DatabaseFile dbFile = new DatabaseFile();

            byte[] content = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
            String filename = (String) scriptResult.get(IScriptExecutor.FILE_NAME);

            if (null == content)
                throw new ApplicationException("Скрипт печати не вернул содержимое файла. Обратитесь к администратору системы.");

            if (null == filename || !filename.contains("."))
                throw new ApplicationException("Скрипт печати вернул некорректное имя файла. Обратитесь к администратору системы.");

            dbFile.setContent(content);
            dbFile.setFilename(filename);
            dbFile.setContentType(CommonBaseUtil.getContentTypeByFileName(filename, DatabaseFile.CONTENT_TYPE_APPLICATION_RTF));
            session.save(dbFile);

            report.setContent(dbFile);
            session.save(report);

            return report.getId();
        });

        deactivate();
        _uiActivation.asDesktopRoot(EplReportBasePub.class).parameter(PUBLISHER_ID, reportId).activate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.putAll(getSettings().getAll(null));
    }
}
