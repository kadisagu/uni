/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic;

import com.google.common.collect.SetMultimap;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.process.ProcessState;
import org.tandemframework.shared.commonbase.base.bo.DaemonQueue.DaemonQueueManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.EplOuSummaryDAO.EplEduGroupTimeItemKey;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCommonRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplRecalculateSummaryKind;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.assignment.EplAssignmentCommonWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplBaseCategoryDataWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplBaseTimeDataWrapper;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplSimpleTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTransferOrgUnitEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleScript;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleSimple;

import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * @author oleyba
 * @since 5/5/12
 */
public interface IEplOuSummaryDAO extends INeedPersistenceSupport
{
    /**
     * Создание расчета нагрузки на подразделение
     *
     * @param orgUnit        подразделение
     * @param studentSummary сводка контингента
     * @param state
     * @return расчет нагзруки
     */
    EplOrgUnitSummary createSummary(OrgUnit orgUnit, EplStudentSummary studentSummary, ProcessState state);

    /**
     * Создание расчета нагрузки на подразделение
     *
     * @param studentSummary сводка контингента
     * @param orgUnit        подразделение
     * @return расчет нагрузки
     */
    EplOrgUnitSummary createSummaryInternal(EplStudentSummary studentSummary, OrgUnit orgUnit, ProcessState state);

    /**
     * Массовое создание расчета нагрузки на подразделения, для которых есть хотя бы одна План. поток и еще нет расчета
     *
     * @param studentSummary сводка контингента
     */
    void createAllMissingSummaries(EplStudentSummary studentSummary);

    /**
     * @param ouSummaries  расчеты нагрузки
     * @param transferTo учитывать дополнительно переведенные часы
     * @param groups учитывать только План. поток из этого множества
     * @return Возвращает мап План. потоков, в каждом из которых расчитано кол-во студентов на план. студента
     */
    Map<EplEduGroup, Map<EplStudent, Integer>> prepareGroupMap(Collection<EplOrgUnitSummary> ouSummaries, boolean transferTo, Set<EplEduGroup> groups);

    /**
     * Осуществляет проверку контрольных часов (и обновляет часы) расчета нагрузки на подразделении
     * @param summaryId  расчет нагрузки
     * @param kind способ перерасчета
     * @param state
     */
    void doRecalculateSummary(Long summaryId, EplRecalculateSummaryKind kind, ProcessState state);

    /**
     * Проверяет есть ли в расчете учебной нагрузки на читающем подразделении хотя бы одни часы, которые не соответсвуют значениям в формулах/скриптах
     *
     * @param summary расчет
     * @return true - найдено несоответствие, иначе - false
     */
    boolean hasIncorrectTime(EplOrgUnitSummary summary);

    /**
     * Проверяет есть ли блокировка на расчет
     *
     * @param summary расчет
     * @return true - блокировка есть, иначе - false
     */
    boolean isBlocked(EplOrgUnitSummary summary);

    /**
     * Сохраняет рассчит. часы нагрузки часы по простой норме
     *
     * @param timeItem часы нагрузки
     */
    void saveOrUpdateTimeItem(EplSimpleTimeItem timeItem);

    /**
     * Удаляет часы нагрузки для План. потока
     * Если часы План. потока оказались переведенными, то перед удалением они воссоздаются в исходном расчете
     *
     * @param deleteId удаляемые часы
     */
    void deleteEduGroupTimeItem(Long deleteId);

    /**
     * Возвращает уникальный набор ids дисциплиночастей через План. поток рассчитанных часов (исключая переведенные), в рамках расчета на подразделении.
     *
     * @param summary расчет нагрузки
     * @return ids дисциплиночастей
     */
    List<Long> getRegElementPartIdsOnItemTime(EplOrgUnitSummary summary);

    /**
     * Возвращает текущий набор, переданных с другого подразделения, часов нагрузки для План. потока
     *
     * @param summary            расчет учебной нагрузки на читающем подразделении
     * @param discOrRulePairKey  ключ пары: часть элемента реестра и норма времени
     * @param transferCategories используемые категории
     */
    Map<EplEduGroupTimeItemKey, EplTransferOrgUnitEduGroupTimeItem> getCurrentTransferTimeItems(EplOrgUnitSummary summary, EplCommonRowWrapper.DisciplineOrRulePairKey discOrRulePairKey, Set<Long> transferCategories);

    /**
     * @param summary           расчет учебной нагрузки на читающем подразделении
     * @param discOrRulePairKey ключ пары: часть элемента реестра и норма времени
     * @return Возвращает список текущих категорий
     */
    IDiscOrRuleData getCurrentDiscOrRuleKeyCategories(EplOrgUnitSummary summary, EplCommonRowWrapper.DisciplineOrRulePairKey discOrRulePairKey);

    @Transactional(propagation=Propagation.SUPPORTS)
    boolean doRefreshFull();

    @Transactional(propagation=Propagation.SUPPORTS)
    boolean refreshPartial(DaemonQueueManager.DaemonQueueItemIterator<EplEduGroup> groupIterator, Iterator<EplOrgUnitSummary> summaryIterator, Iterator<EplTimeRuleSimple> simpleRuleIterator);

    @Transactional(propagation=Propagation.REQUIRES_NEW)
    boolean doRecalculateSummary(final EplRecalculateSummaryData rData,
                              DaemonQueueManager.DaemonQueueItemIterator<EplEduGroup> groups);

    @Transactional(propagation=Propagation.REQUIRES_NEW)
    void updateTimeRules(Map<EplTimeRuleEduGroupScript, Integer> scriptRequestCountMap, Map<EplTimeRuleEduGroupScript, Long> scriptTotalTimeMap);

    interface IDiscOrRuleData {
        Set<Long> categories();
        Set<Long> transferCategories();
    }

    /**
     * Формирует список переданных часов с другого подразделения
     * на основе рассчитанных часов нагрузки для План. потока
     *
     * @param summary        читающее подразделение, с которого была передана нагрузка
     * @param timeItemIds    список возможных для передачи часов нагрузки
     * @param readingOrgUnit новое читающее подразделение
     */
    Map<EplEduGroupTimeItemKey, EplTransferOrgUnitEduGroupTimeItem> getTransferTimeItems(EplOrgUnitSummary summary, Collection<Long> timeItemIds, OrgUnit readingOrgUnit);

    /**
     * Сохраняет переданные с другого подразделения часы нагрузки для План. потока
     *
     * @param summary            расчет учебной нагрузки на читающем подразделении
     * @param discOrRulePairKey  часть элемента реестра
     * @param currTimeItems      текущий набор переданных часов нагрузки
     * @param transferCategories используемые категории
     */
    void doUpdateTransferTimeItems(EplOrgUnitSummary summary, EplCommonRowWrapper.DisciplineOrRulePairKey discOrRulePairKey, Collection<EplTransferOrgUnitEduGroupTimeItem> currTimeItems, Set<Long> transferCategories);

    /**
     * Возвращает читающее подразделение, с которого были переданы часы
     * на основе рассчитанных часов нагрузки для План. потока
     *
     * @param summary           читающее подразделение, с которого была передана нагрузка
     * @param discOrRulePairKey ключ пары: часть элемента реестра и норма времени
     * @param category          категория времени в расчете нагрузки
     */
    EplOrgUnitSummary getSummaryTransferTo(EplOrgUnitSummary summary, EplCommonRowWrapper.DisciplineOrRulePairKey discOrRulePairKey, EplTimeCategoryEduLoad category);

    /**
     * Заполняет датасорс по рассчитанным часам расчета нагрузки
     * Группировка элементов по парам:
     * - (часть элемента реестра, null)
     * - (часть элемента реестра, норма времени)
     * - (null, норма времени)
     *
     * @param dataWrapper датасорс
     */
    <T extends EplBaseTimeDataWrapper> T fillSummaryTimeDataWrapper(T dataWrapper);

    /**
     * Формирует набор данных, необходимых для заполнения строк распределения нагрузки по ППС
     * - семестры План. потока
     * - части учебного года План. потока
     * - кол-во студентов в строках План. потока
     *
     * @param ouSummaries расчет нагрузки на подразделении
     * @param ppsEntry    ППС
     * @param transferTo  учитывать дополнительно переведенные часы
     * @return набор данных
     */
    IDistributionEduGroupData prepareDistributionEduGroupData(Collection<EplOrgUnitSummary> ouSummaries, PpsEntry ppsEntry, boolean transferTo);

    interface IDistributionEduGroupData {
        SetMultimap<Long, Term> termMap();
        SetMultimap<Long, YearDistributionPart> partMap();
        Map<Long, Long> studentCountMap();
    }

    /**
     * Возвращает враппер по актуальным категориям времени
     *
     * @param ouSummaries расчеты
     * @return враппер категорий времени
     */
    EplBaseCategoryDataWrapper getCategoryDataWrapper(Collection<EplOrgUnitSummary> ouSummaries);

    /**
     * Возвращает враппер категорий времени по заданным категориям
     *
     * @param uCategories  список категорий для обычных часов
     * @param tuCategories список категорий для переведенных часов
     * @return враппер категорий времени
     */
    EplBaseCategoryDataWrapper getCategoryDataWrapper(Set<EplTimeCategoryEduLoad> uCategories, Set<EplTimeCategoryEduLoad> tuCategories);

    /**
     * Возвращает враппер списка рассчитанных часов для отчета "Учебное поручение читающему подразделению"
     *
     * @param ouSummaryId         нагрузки на читающем подразделении
     * @param yearPartIds         ids частей учебного года
     * @param developFormIds      ids форм освоения
     * @param developConditionIds ids условий освоения
     * @return враппер рассчитанных часов
     */
    EplAssignmentCommonWrapper getEplTimeDataWrapperForEduAssignmentPrint(@NotNull Long ouSummaryId, List<Long> yearPartIds, List<Long> developFormIds, List<Long> developConditionIds);

    /**
     * Возвращает враппер списка рассчитанных часов для ИПП
     *
     * @param plan Индивидуальный план преподавателя
     * @return враппер рассчитанных часов
     */
    EplAssignmentCommonWrapper getEplTimeDataWrapperForPps(@NotNull EplIndividualPlan plan);

    /**
     * Проверяет, существует ли распределенные часы для ППС по ИПП
     * @param plan ИПП
     * @return true - если есть хотя бы одни часы, иначе - false
     */
    boolean existsPlannedPpsTimeItemForPps(@NotNull EplIndividualPlan plan);

    /**
     * Возвращает связь План. потока с типом План. потока, по соответствующему правилу определения.
     * Если в строках План. потока, ссылающихся на один и тот же План. поток, план. студенты имеют:
     * 1) разные план. академ. группы. Тогда План. поток - "поток";
     * 2) общую план. академ. группу, при этом у всех МСРП-ВН, полученных по строкам План. потока, ровно одна строка. Тогда План. поток - "группа";
     * 3) иначе - "подгруппа".
     *
     * @param eduGroups список План. потоков
     * @return мап связи План. потока и его типа
     */
    Map<EplEduGroup, EplEduGroupType> getEduGroupTypeMap(Set<EplEduGroup> eduGroups);

    @Transactional(propagation=Propagation.REQUIRES_NEW)
    void deleteOrUpdateTimeItem(final EplRecalculateSummaryData rData);

    @Transactional(propagation=Propagation.REQUIRES_NEW)
    void refreshHoursBySimpleRules(final EplRecalculateSummaryData rData);

    @Transactional(propagation=Propagation.REQUIRES_NEW)
    Set<Long> checkTimeItemSummaryHours(final EplRecalculateSummaryData rData,
                                        Map<EplEduGroup, Map<EplStudent, Integer>> groupMap,
                                        ITimeItemData data,
                                        Map<MultiKey, Boolean> transferredHours,
                                        boolean useTransfered,
                                        final Map<EplTimeRuleEduGroupScript, Integer> scriptRequestCountMap,
                                        final Map<EplTimeRuleEduGroupScript, Long> scriptTotalTimeMap);

    @Transactional(propagation=Propagation.REQUIRES_NEW)
    boolean recalculateTransferred(final EplRecalculateSummaryData rData,
                                IEplOuSummaryDAO ouSummaryDAO,
                                Map<MultiKey, Boolean> transferredHours,
                                Map<MultiKey, EplOrgUnitSummary> transferredTo,
                                Map<EplTimeRuleEduGroupScript, Integer> scriptRequestCountMap,
                                Map<EplTimeRuleEduGroupScript, Long> scriptTotalTimeMap,
                                Date fillDate,
                                DaemonQueueManager.DaemonQueueItemIterator<EplEduGroup> groupIterator);

    @Transactional(propagation=Propagation.REQUIRES_NEW)
    void updateOrDelete(Set<Long> toClearSet, boolean delete);

    interface ITimeItemData {
        Date fillDate();
        Map<EplTimeRuleEduGroupScript, IEplTimeRuleScript> scriptWrapperMap();
        List<EplTimeRuleEduLoad> ruleList();
        List<EppIControlActionType> icaTypes();
        Map<Long, IEppRegElPartWrapper> registryData();
        Map<MultiKey, EplEduGroupTimeItem> itemMap();
        Map<MultiKey, EplTransferOrgUnitEduGroupTimeItem> transferItemMap();
    }
}
