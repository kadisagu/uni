package ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.AddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad;
import ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoadOuSummary;
import ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoadSummary;

import static ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.logic.EplSettingsPpsLoadLevels.OUSUMMARY;
import static ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.logic.EplSettingsPpsLoadLevels.SUMMARY;
import static ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.AddEdit.EplSettingsPpsLoadAddEdit.ENTITY;
import static ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.ui.AddEdit.EplSettingsPpsLoadAddEdit.LEVEL;

/**
 * @author Alexey Lopatin
 * @since 18.05.2015
 */
@Input({
        @Bind(key = IUIPresenter.PUBLISHER_ID, binding = "settingId"),
        @Bind(key = ENTITY, binding = "entityId"),
        @Bind(key = LEVEL, binding = "level"),
})
public class EplSettingsPpsLoadAddEditUI extends UIPresenter
{
    private Long _settingId;
    private EplSettingsPpsLoad _setting;
    private Long _entityId;
    private Long _level;
    private boolean _fixedYear;

    @Override
    public void onComponentRefresh()
    {
        if (isAddForm())
        {
            IEntity entity;
            EducationYear eduYear = null;
            EplStudentSummary summary = null;
            EplOrgUnitSummary ouSummary = null;

            if (_entityId != null && null != (entity = DataAccessServices.dao().get(_entityId)))
            {
                if (entity instanceof EplStudentSummary)
                {
                    summary = (EplStudentSummary) entity;
                    eduYear = summary.getEppYear().getEducationYear();
                } else if (entity instanceof EplOrgUnitSummary)
                {
                    ouSummary = (EplOrgUnitSummary) entity;
                    eduYear = ouSummary.getStudentSummary().getEppYear().getEducationYear();
                }
            }
            _fixedYear = eduYear != null;
            if (!_fixedYear)
                eduYear = DataAccessServices.dao().getNotNull(EducationYear.class, EducationYear.current(), Boolean.TRUE);

            if (SUMMARY.equals(_level))
            {
                final EplSettingsPpsLoadSummary eplSettingsPpsLoadSummary = new EplSettingsPpsLoadSummary();
                eplSettingsPpsLoadSummary.setSummary(summary);
                _setting = eplSettingsPpsLoadSummary;
            }
            else if (OUSUMMARY.equals(_level))
            {
                final EplSettingsPpsLoadOuSummary eplSettingsPpsLoadOuSummary = new EplSettingsPpsLoadOuSummary();
                eplSettingsPpsLoadOuSummary.setSummary(ouSummary);
                _setting = eplSettingsPpsLoadOuSummary;
            }
            else
                _setting = new EplSettingsPpsLoad();
            _setting.setLevel(_level == null ? 0 : _level);
            _setting.setEduYear(eduYear);
        }
        else
        {
            _setting = DataAccessServices.dao().getNotNull(_settingId);
            _fixedYear = _setting.getLevel() > 0;
        }
    }

    public void onClickApply()
    {
        _setting.setLevel(getLevel() == null ? 0 : getLevel());
        if (getEntityId() != null)
            _setting.setEntity(getEntityId());


        DataAccessServices.dao().saveOrUpdate((IEntity) DataAccessServices.dao().getComponentSession().merge(_setting));
        deactivate();
    }

    public boolean isAddForm()
    {
        return _settingId == null;
    }

    public Long getSettingId()
    {
        return _settingId;
    }

    public void setSettingId(Long settingId)
    {
        _settingId = settingId;
    }

    public EplSettingsPpsLoad getSetting()
    {
        return _setting;
    }

    public void setSetting(EplSettingsPpsLoad setting)
    {
        _setting = setting;
    }

    public Long getEntityId()
    {
        return _entityId;
    }

    public void setEntityId(Long _entityId)
    {
        this._entityId = _entityId;
    }

    public Long getLevel()
    {
        return _level;
    }

    public void setLevel(Long _level)
    {
        this._level = _level;
    }

    public boolean isFixedYear()
    {
        return !_fixedYear;
    }
}
