/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.logic.EplStudentSummaryGroupDSHandler;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplCompensationSource;

/**
 * @author oleyba
 * @since 10/11/11
 */
@Configuration
public class EplStudentSummaryGroupTab extends BusinessComponentManager
{
    public static final String DS_EDU_HS = "eduHsDS";
	public static final String DS_GROUP = "groupDS";
    public static final String DS_SUMMARY = "summaryDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS("formOuDS").handler(UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler()).addColumn(OrgUnit.fullTitle().s()))
            .addDataSource(selectDS(DS_EDU_HS, EducationCatalogsManager.instance().eduHsDSHandler()).addColumn(EducationLevelsHighSchool.fullTitleExtended().s()))
            .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
            .addDataSource(UniStudentManger.instance().courseDSConfig())
            .addDataSource(selectDS(DS_GROUP, EplStudentSummaryManager.instance().groupComboDSHandler()))
            .addDataSource(selectDS("compSourceDS").handler(EplStudentSummaryManager.instance().compSourceDSHandler()).addColumn(EplCompensationSource.title().s()))
            .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_SUMMARY, getName(), EplStudentSummary.defaultNotArchiveSelectDSHandler(getName())
                .customize((alias, dql, context, filter) -> EplStudentSummaryManager.instance().dao().checkStudentSummaryByOrgUnit(alias, dql, context))))
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eplStudentSummaryGroupDSHandler()
    {
        return new EplStudentSummaryGroupDSHandler(getName());
    }
}
