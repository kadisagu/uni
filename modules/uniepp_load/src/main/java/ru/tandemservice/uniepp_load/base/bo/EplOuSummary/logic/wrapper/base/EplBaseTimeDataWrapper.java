/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.EplOuSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCategoryWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCommonRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplDiscOrRuleRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplSimpleRuleRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic.EplErrorFormatter;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplSimpleTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTransferOrgUnitEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Базовый враппер для вывода списка расчитанных и переведенных часов План. потока
 *
 * @author Alexey Lopatin
 * @since 04.04.2016
 */
public class EplBaseTimeDataWrapper
{
    private Collection<EplOrgUnitSummary> _ouSummaries;
    private EplBaseCategoryDataWrapper _categoryDataWrapper;

    // параметры для построения датасорса
    private Set<String> _propertyPathSet;
    private boolean _withTransferTimeItem;
    private boolean _withSimpleTimeItem;
    private boolean _fillTimeItem;
    private boolean _select;

    private double _ruleTimeSum;
    private Map<String, Map<EplCommonRowWrapper.DisciplineOrRulePairKey, EplDiscOrRuleRowWrapper>> _discRowMap = Maps.newHashMap();
    private Map<String, Map<EplCommonRowWrapper.DisciplineOrRulePairKey, EplSimpleRuleRowWrapper>> _simpleRuleRowMap = Maps.newHashMap();
    private Map<EplEduGroup, Map<EplStudent, Integer>> _groupMap = Maps.newHashMap();
    private Set<EplEduGroup> _eduGroups = Sets.newHashSet();

    private DQLSelectBuilder _eduGroupDql;
    private DQLSelectBuilder _simpleDql;

    public PairKey<Map<String, Double>, Map<String, Double>> _discTotalRowTimeSumMap = PairKey.create(Maps.newHashMap(), Maps.newHashMap());

    public EplBaseTimeDataWrapper(EplOrgUnitSummary ouSummary, boolean withTransferTimeItem, boolean withSimpleTimeItem)
    {
        this(Collections.singleton(ouSummary), withTransferTimeItem, withSimpleTimeItem);
    }

    /**
     * @param ouSummaries          расчеты
     * @param withTransferTimeItem учитывать переданные часы
     * @param withSimpleTimeItem   учитывать часы с простыми нормами
     */
    public EplBaseTimeDataWrapper(Collection<EplOrgUnitSummary> ouSummaries, boolean withTransferTimeItem, boolean withSimpleTimeItem)
    {
        _ouSummaries = ouSummaries;
        _withTransferTimeItem = withTransferTimeItem;
        _withSimpleTimeItem = withSimpleTimeItem;

        Collection<Long> ouSummaryIds = CommonDAO.ids(_ouSummaries);

        String eduGroupAlias = "eti";
        DQLSelectBuilder eduGroupDql = new DQLSelectBuilder().fromEntity(EplEduGroupTimeItem.class, eduGroupAlias).column(property(eduGroupAlias))
                .fetchPath(DQLJoinType.left, EplEduGroupTimeItem.registryElementPart().registryElement().fromAlias(eduGroupAlias), "regEl")
                .fetchPath(DQLJoinType.left, EplEduGroupTimeItem.timeRule().fromAlias(eduGroupAlias), "rule")
                .where(_withTransferTimeItem
                               ? or(in(property(eduGroupAlias, EplEduGroupTimeItem.summary()), ouSummaryIds),
                                    exists(EplTransferOrgUnitEduGroupTimeItem.class,
                                           EplTransferOrgUnitEduGroupTimeItem.id().s(), property(eduGroupAlias, "id"),
                                           EplTransferOrgUnitEduGroupTimeItem.transferredFrom().s(), ouSummaryIds))
                               : in(property(eduGroupAlias, EplEduGroupTimeItem.summary()), ouSummaryIds));

        applyWhereConditionsEduGroup(eduGroupAlias, eduGroupDql);
        _eduGroupDql = eduGroupDql;

        String simpleAlias = "sti";
        DQLSelectBuilder simpleDql = new DQLSelectBuilder().fromEntity(EplSimpleTimeItem.class, simpleAlias).column(property(simpleAlias))
                .where(in(property(simpleAlias, EplSimpleTimeItem.summary()), ouSummaryIds));
        applyWhereConditionsSimple(simpleAlias, simpleDql);
        _simpleDql = simpleDql;
    }

    protected void applyWhereConditionsEduGroup(final String alias, final DQLSelectBuilder dql)
    {
    }

    protected void applyWhereConditionsSimple(final String alias, final DQLSelectBuilder dql)
    {
    }

    public <T extends EplBaseTimeDataWrapper> T fillDataSource(Set<String> propertyPathSet, boolean fillTimeItem)
    {
        return fill(propertyPathSet, fillTimeItem, false);
    }

    public <T extends EplBaseTimeDataWrapper> T fillSelect()
    {
        return fill(null, false, true);
    }

    /**
     * Заполняет датасорс
     *
     * @param propertyPathSet доп. проперти для группировки рассчитанных часов нагрузки
     * @param fillTimeItem    заполнять значение часов нагрузки
     * @param select          используется как селект
     */
    @SuppressWarnings("unchecked")
    protected <T extends EplBaseTimeDataWrapper> T fill(Set<String> propertyPathSet, boolean fillTimeItem, boolean select)
    {
        _propertyPathSet = propertyPathSet;
        _fillTimeItem = fillTimeItem;
        _select = select;

        return EplOuSummaryManager.instance().dao().fillSummaryTimeDataWrapper((T) this);
    }

    /** Выводит часы нагрузки, согласно запросу **/
    public List<EplEduGroupTimeItem> getEduGroupTimeItems()
    {
        return IUniBaseDao.instance.get().getList(_eduGroupDql);
    }

    public List<EplSimpleTimeItem> getSimpleTimeItems()
    {
        return IUniBaseDao.instance.get().getList(_simpleDql);
    }

    /** мап расчитанных часов нагрузки, сгруппированных согласно вариантам группировки рассчитанных часов */
    public Map<String, Map<EplCommonRowWrapper.DisciplineOrRulePairKey, EplDiscOrRuleRowWrapper>> getDiscRowMap()
    {
        return _discRowMap;
    }

    /** мап расчитанных часов нагрузки по простой норме */
    public Map<String, Map<EplCommonRowWrapper.DisciplineOrRulePairKey, EplSimpleRuleRowWrapper>> getSimpleRuleRowMap()
    {
        return _simpleRuleRowMap;
    }

    public List<EplCommonRowWrapper> getAllRows()
    {
        return getAllRowMap().values().stream()
                .flatMap(row -> row.values().stream())
                .collect(Collectors.toList());
    }

    private Map<String, Map<EplCommonRowWrapper.DisciplineOrRulePairKey, ? extends EplCommonRowWrapper>> getAllRowMap()
    {
        Map<String, Map<EplCommonRowWrapper.DisciplineOrRulePairKey, ? extends EplCommonRowWrapper>> resultMap = Maps.newHashMap();
        resultMap.putAll(_discRowMap);
        resultMap.putAll(_simpleRuleRowMap);
        return resultMap;
    }

    /** результат в ячейке дисциплин **/
    public String getDiscCellTimeSum(EplCategoryWrapper category, EplDiscOrRuleRowWrapper discRow, boolean transferTo)
    {
        Long categoryId = category.getId();
        String categoryCode = category.getCategory().getCode();
        EplCommonRowWrapper.DisciplineOrRulePairKey key = discRow.getKey();

        EplDiscOrRuleRowWrapper rowWrapper = getDiscRowMap().get(key.getGroupingCode()).get(key);
        PairKey<Long, Boolean> categoryKey = PairKey.create(categoryId, transferTo);

        EplCommonRowWrapper.ControlTime timeSum = rowWrapper.getTimeMap().get(categoryKey);
        String value = timeSum == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(timeSum.time);
        String controlValue = timeSum == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(timeSum.controlTime);

        if (timeSum != null && (EplTimeCategoryEduLoad.CODE_LECTURES.equals(categoryCode) || EplTimeCategoryEduLoad.CODE_PRACTICES.equals(categoryCode) || EplTimeCategoryEduLoad.CODE_LABS.equals(categoryCode)))
        {
            int count = transferTo ? rowWrapper.getTransferGroups().get(categoryId).size() : rowWrapper.getGroups().get(categoryId).size();
            value = count + ": " + value;
            controlValue = count + ": " + controlValue;
        }
        return EplErrorFormatter.errorDiffSum(value, controlValue, timeSum != null && timeSum.hasDifference);
    }

    /** результат в ячейке норм **/
    public String getRuleCellTimeSum(EplCategoryWrapper category, EplSimpleRuleRowWrapper ruleRow)
    {
        Long categoryId = category.getId();
        PairKey<Long, Boolean> categoryKey = PairKey.create(categoryId, false);

        EplSimpleRuleRowWrapper rowWrapper = getSimpleRuleRowMap().get(EplSimpleRuleRowWrapper.BY_SIMPLE_TIME_RULE).get(ruleRow.getKey());
        EplCommonRowWrapper.ControlTime timeSum = rowWrapper.getTimeMap().get(categoryKey);

        String value = timeSum == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(timeSum.time);
        String controlValue = timeSum == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(timeSum.controlTime);

        return EplErrorFormatter.errorDiffSum(value, controlValue);
    }

    /** сумма всех часов для определенной группы **/
    public double getDiscTimeSum(boolean transferTo, List<String> groupingCodes)
    {
        Map<String, Double> discTimeSumMap = transferTo ? getDiscTotalRowTimeSumMap().getSecond() : getDiscTotalRowTimeSumMap().getFirst();
        double result = .0;
        for (String code : groupingCodes)
            result += discTimeSumMap.getOrDefault(code, 0.0);
        return result;
    }

    /** сумма всех распределенных часов **/
    public double getDisciplineTimeSum()
    {
        Collection<Double> values = getDiscTotalRowTimeSumMap().getFirst().values();
        return values.stream().mapToDouble(Double::doubleValue).sum();
    }

    /** сумма всех переведенных часов **/
    public double getTransferTotalRowTimeSum()
    {
        Collection<Double> values = getDiscTotalRowTimeSumMap().getSecond().values();
        return values.stream().mapToDouble(Double::doubleValue).sum();
    }

    public Collection<EplOrgUnitSummary> getOuSummaries() { return _ouSummaries; }
    public EplBaseCategoryDataWrapper getCategoryDataWrapper() { return _categoryDataWrapper; }
    public void setCategoryDataWrapper(EplBaseCategoryDataWrapper categoryDataWrapper) { _categoryDataWrapper = categoryDataWrapper; }
    public double getRuleTimeSum() { return _ruleTimeSum; }
    public void setRuleTimeSum(double ruleTimeSum) { _ruleTimeSum = ruleTimeSum; }
    public Map<EplEduGroup, Map<EplStudent, Integer>> getGroupMap() { return _groupMap; }
    public void setGroupMap(Map<EplEduGroup, Map<EplStudent, Integer>> groupMap) { _groupMap = groupMap; }
    public Set<EplEduGroup> getEduGroups() { return _eduGroups; }
    public PairKey<Map<String, Double>, Map<String, Double>> getDiscTotalRowTimeSumMap() { return _discTotalRowTimeSumMap; }
    public void setDiscTotalRowTimeSumMap(PairKey<Map<String, Double>, Map<String, Double>> discTotalRowTimeSumMap) { _discTotalRowTimeSumMap = discTotalRowTimeSumMap; }
    public Set<String> getPropertyPathSet() { return _propertyPathSet; }
    public boolean isWithTransferTimeItem() { return _withTransferTimeItem; }
    public boolean isWithSimpleTimeItem() { return _withSimpleTimeItem; }
    public boolean isFillTimeItem() { return _fillTimeItem; }
    public boolean isSelect() { return _select; }
}
