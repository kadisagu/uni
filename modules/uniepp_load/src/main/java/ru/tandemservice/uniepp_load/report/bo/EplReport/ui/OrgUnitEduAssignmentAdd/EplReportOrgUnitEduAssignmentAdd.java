/* $Id$ */
package ru.tandemservice.uniepp_load.report.bo.EplReport.ui.OrgUnitEduAssignmentAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.report.bo.EplReport.EplReportManager;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager.DS_YEAR_PART;
import static ru.tandemservice.uniepp_load.report.bo.EplReport.ui.OrgUnitEduAssignmentAdd.EplReportOrgUnitEduAssignmentAddUI.*;

/**
 * @author Alexey Lopatin
 * @since 25.06.2016
 */
@Configuration
public class EplReportOrgUnitEduAssignmentAdd extends BusinessComponentManager
{
    public static final String DS_STUDENT_SUMMARY = "studentSummaryDS";
    public static final String DS_OU_SUMMARY = "ouSummaryDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(EplReportManager.DS_SUMMARY_EPP_YEAR, getName(), EplReportManager.instance().summaryEduYearDSHandler())
                                       .valueStyleSource(EducationCatalogsManager.getEduYearValueStyleSource()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_STUDENT_SUMMARY, getName(), EplStudentSummary.defaultSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> {
                            EppYearEducationProcess eppYear = context.get(PARAM_EPP_YEAR);
                            return dql.where(eppYear == null ? nothing() : eq(property(alias, EplStudentSummary.eppYear()), value(eppYear)));
                        })
                ))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_OU_SUMMARY, getName(), EplOrgUnitSummary.defaultSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> {
                            EplStudentSummary studentSummary = context.get(PARAM_STUDENT_SUMMARY);
                            return dql.where(studentSummary == null ? nothing() : eq(property(alias, EplOrgUnitSummary.studentSummary()), value(studentSummary)));
                        })
                ))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_YEAR_PART, getName(), YearDistributionPart.defaultSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> {
                            EplOrgUnitSummary ouSummary = context.get(PARAM_OU_SUMMARY);
                            if (null == ouSummary)
                                return dql.where(nothing());
                            else
                                return dql.where(exists(
                                        new DQLSelectBuilder().fromEntity(EplEduGroup.class, "g").column(property("g.id")).top(1)
                                                .where(eq(property("g", EplEduGroup.yearPart()), property(alias)))
                                                .where(eq(property("g", EplEduGroup.summary()), value(context.<EplStudentSummary>get(PARAM_STUDENT_SUMMARY))))
                                                .where(eq(property("g", EplEduGroup.registryElementPart().registryElement().owner()), value(ouSummary.getOrgUnit())))
                                                .buildQuery()));
                        })).treeable(true).valueStyleSource(EducationCatalogsManager.getYearPartValueStyleSource()))
                .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developConditionDSConfig())
                .create();
    }
}
