/* $Id$ */
package ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.logic.EplPlannedPpsDao;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.logic.IEplPlannedPpsDao;

/**
 * @author nvankov
 * @since 1/28/15
 */
@Configuration
public class EplPlannedPpsManager extends BusinessObjectManager
{
    public static EplPlannedPpsManager instance()
    {
        return instance(EplPlannedPpsManager.class);
    }

    @Bean
    public IEplPlannedPpsDao dao()
    {
        return new EplPlannedPpsDao();
    }
}



    