package ru.tandemservice.uniepp_load.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad;
import ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoadSummary;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Предельные значения нагрузки преподавателей (сводка)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplSettingsPpsLoadSummaryGen extends EplSettingsPpsLoad
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoadSummary";
    public static final String ENTITY_NAME = "eplSettingsPpsLoadSummary";
    public static final int VERSION_HASH = 1160803455;
    private static IEntityMeta ENTITY_META;

    public static final String L_SUMMARY = "summary";

    private EplStudentSummary _summary;     // Сводка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сводка. Свойство не может быть null.
     */
    @NotNull
    public EplStudentSummary getSummary()
    {
        return _summary;
    }

    /**
     * @param summary Сводка. Свойство не может быть null.
     */
    public void setSummary(EplStudentSummary summary)
    {
        dirty(_summary, summary);
        _summary = summary;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EplSettingsPpsLoadSummaryGen)
        {
            setSummary(((EplSettingsPpsLoadSummary)another).getSummary());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplSettingsPpsLoadSummaryGen> extends EplSettingsPpsLoad.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplSettingsPpsLoadSummary.class;
        }

        public T newInstance()
        {
            return (T) new EplSettingsPpsLoadSummary();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "summary":
                    return obj.getSummary();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "summary":
                    obj.setSummary((EplStudentSummary) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "summary":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "summary":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "summary":
                    return EplStudentSummary.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplSettingsPpsLoadSummary> _dslPath = new Path<EplSettingsPpsLoadSummary>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplSettingsPpsLoadSummary");
    }
            

    /**
     * @return Сводка. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoadSummary#getSummary()
     */
    public static EplStudentSummary.Path<EplStudentSummary> summary()
    {
        return _dslPath.summary();
    }

    public static class Path<E extends EplSettingsPpsLoadSummary> extends EplSettingsPpsLoad.Path<E>
    {
        private EplStudentSummary.Path<EplStudentSummary> _summary;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сводка. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoadSummary#getSummary()
     */
        public EplStudentSummary.Path<EplStudentSummary> summary()
        {
            if(_summary == null )
                _summary = new EplStudentSummary.Path<EplStudentSummary>(L_SUMMARY, this);
            return _summary;
        }

        public Class getEntityClass()
        {
            return EplSettingsPpsLoadSummary.class;
        }

        public String getEntityName()
        {
            return "eplSettingsPpsLoadSummary";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
