package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_load_2x11x3_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eplSettingsPpsLoad

		// создано обязательное свойство hoursMinLong
		{
			// создать колонку
			tool.createColumn("epl_settings_pps_load_t", new DBColumn("hoursminlong_p", DBType.LONG));

			// задать значение по умолчанию
			java.lang.Long defaultHoursMinLong = 0L;
			tool.executeUpdate("update epl_settings_pps_load_t set hoursminlong_p=? where hoursminlong_p is null", defaultHoursMinLong);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epl_settings_pps_load_t", "hoursminlong_p", false);

		}

		// создано обязательное свойство hoursMaxLong
		{
			// создать колонку
			tool.createColumn("epl_settings_pps_load_t", new DBColumn("hoursmaxlong_p", DBType.LONG));

			// задать значение по умолчанию
			java.lang.Long defaultHoursMaxLong = 0L;
			tool.executeUpdate("update epl_settings_pps_load_t set hoursmaxlong_p=? where hoursmaxlong_p is null", defaultHoursMaxLong);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epl_settings_pps_load_t", "hoursmaxlong_p", false);

		}

		// создано обязательное свойство hoursAdditionalLong
		{
			// создать колонку
			tool.createColumn("epl_settings_pps_load_t", new DBColumn("hoursadditionallong_p", DBType.LONG));

			// задать значение по умолчанию
			java.lang.Long defaultHoursAdditionalLong = 0L;
			tool.executeUpdate("update epl_settings_pps_load_t set hoursadditionallong_p=? where hoursadditionallong_p is null", defaultHoursAdditionalLong);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epl_settings_pps_load_t", "hoursadditionallong_p", false);

		}

		tool.executeUpdate("update epl_settings_pps_load_t set hoursminlong_p = 100 * hoursmin_p, hoursmaxlong_p = 100 * hoursmax_p, hoursadditionallong_p = 100 * hoursadditional_p");

		// удалено свойство hoursMin
		{

			// удалить колонку
			tool.dropColumn("epl_settings_pps_load_t", "hoursmin_p");

		}

		// удалено свойство hoursMax
		{
			// удалить колонку
			tool.dropColumn("epl_settings_pps_load_t", "hoursmax_p");

		}

		// удалено свойство hoursAdditional
		{
			// удалить колонку
			tool.dropColumn("epl_settings_pps_load_t", "hoursadditional_p");

		}
    }
}