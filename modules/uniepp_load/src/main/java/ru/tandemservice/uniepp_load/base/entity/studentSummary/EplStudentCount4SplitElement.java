package ru.tandemservice.uniepp_load.base.entity.studentSummary;

import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.gen.*;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan;

/** @see ru.tandemservice.uniepp_load.base.entity.studentSummary.gen.EplStudentCount4SplitElementGen */
public class EplStudentCount4SplitElement extends EplStudentCount4SplitElementGen
{
    public EplStudentCount4SplitElement()
    {
    }

    public EplStudentCount4SplitElement(EplStudent2WorkPlan student2WorkPlan, EppRegistryElement regElement, EppEduGroupSplitBaseElement splitElement)
    {
        setStudent2WorkPlan(student2WorkPlan);
        setRegElement(regElement);
        setSplitElement(splitElement);
    }
}