/* $Id:$ */
package ru.tandemservice.uniepp_load.report.bo.EplReportBase;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author oleyba
 * @since 2/16/15
 */
@Configuration
public class EplReportBaseManager extends BusinessObjectManager
{
    public static EplReportBaseManager instance()
    {
        return instance(EplReportBaseManager.class);
    }
}
