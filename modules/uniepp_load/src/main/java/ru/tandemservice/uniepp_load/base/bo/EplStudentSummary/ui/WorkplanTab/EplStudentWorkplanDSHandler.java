/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.WorkplanTab;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.support.IUISettings;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.catalog.bo.StudentCatalogs.StudentCatalogsManager;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 10/13/11
 */
public class EplStudentWorkplanDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PARAM_SUMMARY = "summary";
    public static final String PARAM_SETTINGS = "settings";
    public static final String COLUMN_WORKPLAN = "workplan";
    public static final String COLUMN_COUNT = "countView";

    private static DQLOrderDescriptionRegistry orderReg = createOrderRegistry();

    public EplStudentWorkplanDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final Session session = context.getSession();

        EplStudentSummary summary = context.get(PARAM_SUMMARY);
        IUISettings settings = context.get(PARAM_SETTINGS);

        DQLSelectBuilder dql = orderReg.buildDQLSelectBuilder()
            .where(eq(property(EplStudent.group().summary().fromAlias("s")), value(summary)))
            ;

        FilterUtils.applySelectFilter(dql, "s", EplStudent.educationOrgUnit().formativeOrgUnit(), settings.get(EducationCatalogsManager.PARAM_FORMATIVE_ORGUNIT));
        FilterUtils.applySelectFilter(dql, "s", EplStudent.educationOrgUnit().educationLevelHighSchool(), settings.get(EducationCatalogsManager.PARAM_EDU_HS));
        FilterUtils.applySelectFilter(dql, "s", EplStudent.educationOrgUnit().developForm(), settings.get(EducationCatalogsManager.PARAM_DEVELOP_DORM));
        FilterUtils.applySelectFilter(dql, "s", EplStudent.group().course(), settings.get(StudentCatalogsManager.PARAM_COURSE));
        FilterUtils.applySelectFilter(dql, "s", EplStudent.compensationSource(), settings.get(EplStudentSummaryManager.PARAM_COMPENSATION_SOURCE));

        boolean hasErrors = Boolean.TRUE.equals(settings.get(EplStudentSummaryWorkplanTabUI.PARAM_HAS_ERRORS));
        boolean noWp = Boolean.TRUE.equals(settings.get(EplStudentSummaryWorkplanTabUI.PARAM_NO_WP));

        if (hasErrors && noWp)
        {
            dql.where(or(
                    eq(property("s", EplStudent.errorCountWithWp()), value(Boolean.TRUE)),
                    eq(property("s", EplStudent.noWp()), value(Boolean.TRUE))
            ));
        }
        else
        {
            if (hasErrors)
                dql.where(eq(property("s", EplStudent.errorCountWithWp()), value(Boolean.TRUE)));
            if (noWp)
                dql.where(eq(property("s", EplStudent.noWp()), value(Boolean.TRUE)));
        }

        String groupTitle = settings.get("group") != null ? ((DataWrapper) settings.get("group")).getTitle() : null;
        if(!StringUtils.isEmpty(groupTitle))
        {
            dql.where(eq(property("s", EplStudent.group().title()), value(groupTitle)));
        }

        orderReg.applyOrder(dql, input.getEntityOrder());
        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, session).build();

        final List<DataWrapper> wrappers = DataWrapper.wrap(output, "id", "count");
        BatchUtils.execute(wrappers, 256, new BatchUtils.Action<DataWrapper>()
        {
            @Override
            public void execute(final Collection<DataWrapper> wrappers)
            {
                Collection<Long> ids = CommonDAO.ids(wrappers);

                final List<EplStudent2WorkPlan> rows = new DQLSelectBuilder()
                    .fromEntity(EplStudent2WorkPlan.class, "rel").column("rel")
                    .where(in(property(EplStudent2WorkPlan.student().id().fromAlias("rel")), ids))
                    .fetchPath(DQLJoinType.inner, EplStudent2WorkPlan.student().fromAlias("rel"), "s")
                    .fetchPath(DQLJoinType.inner, EplStudent2WorkPlan.workPlan().fromAlias("rel"), "w")
                    .createStatement(session).list();
                final Map<Long, List<EplStudent2WorkPlan>> relMap = SafeMap.get(ArrayList.class);
                for (final EplStudent2WorkPlan rel : rows)
                    relMap.get(rel.getStudent().getId()).add(rel);

                for (final DataWrapper wrapper : wrappers) {
                    EplStudent student = wrapper.getWrapped();

                    List<EplStudent2WorkPlan> wpList = relMap.get(wrapper.getId());
                    Collections.sort(wpList, EplStudent2WorkPlan.IN_ROW_COMPARATOR);

                    List<DataWrapper> wpWrappers = new ArrayList<>(wpList.size());
                    for (EplStudent2WorkPlan rel : wpList) {
                        wpWrappers.add(new DataWrapper(rel.getWorkPlan().getId(), rel.getCount() + ": " + rel.getWorkPlan().getTitle()));
                    }

                    wrapper.put(COLUMN_WORKPLAN, wpWrappers);
                    wrapper.put(COLUMN_COUNT, student.isErrorCountWithWp() || student.isNoWp() ? "<span style=\"color:" + UniDefines.COLOR_CELL_ERROR + "\">" + student.getCount() + "</span>" : String.valueOf(student.getCount()));
                }
            }
        });

        return output;
    }

    private static DQLOrderDescriptionRegistry createOrderRegistry()
    {
        OrderDescription byCourse = new OrderDescription(EplStudent.group().course().intValue());
        OrderDescription byGroupTitle = new OrderDescription(EplStudent.group().title());
        return new DQLOrderDescriptionRegistry(EplStudent.class, "s")
                .setOrders(EplStudent.group().course().title(), byCourse, byGroupTitle)
                .setOrders(EplStudent.group().title(), byGroupTitle, byCourse)
                .setOrders(EplStudent.educationOrgUnit().educationLevelHighSchool().displayableTitle(), new OrderDescription(EplStudent.educationOrgUnit().educationLevelHighSchool().displayableTitle()), byCourse, byGroupTitle)
                .setOrders(EplStudent.educationOrgUnit().developForm().title(), new OrderDescription(EplStudent.educationOrgUnit().developForm().code()), byCourse, byGroupTitle)
                .setOrders(EplStudent.educationOrgUnit().developCondition().shortTitle(), new OrderDescription(EplStudent.educationOrgUnit().developCondition().code()), byCourse, byGroupTitle)
                .setOrders(EplStudent.educationOrgUnit().developTech().title(), new OrderDescription(EplStudent.educationOrgUnit().developTech().code()), byCourse, byGroupTitle)
                .setOrders(EplStudent.developGrid().title(), new OrderDescription(EplStudent.developGrid().developPeriod().priority()), byCourse, byGroupTitle)
                .setOrders(EplStudent.compensationSource().title(), new OrderDescription(EplStudent.compensationSource().code()), byCourse, byGroupTitle)
                .setOrders(COLUMN_COUNT, new OrderDescription("s", EplStudent.count()), byCourse, byGroupTitle)
                ;
    }
}
