/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.DisciplineTimeDetail;

import com.beust.jcommander.internal.Maps;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.EplOuSummaryDAO;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCommonRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic.EplErrorFormatter;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTransferOrgUnitEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 5/14/12
 */
public class EplOuSummaryDisciplineTimeDetailDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PARAM_OU_SUMMARY = "summary";
    public static final String PARAM_DISC_OR_RULE_KEY = "discOrRuleKey";
    public static final String PARAM_CATEGORY = "category";
    public static final String PARAM_TRANSFER = "transfer";

    public static final String VIEW_ACADEMIC_GROUP = "viewAcademicGroup";
    public static final String VIEW_COURSE         = "viewCourse";
    public static final String VIEW_TERM           = "viewTerm";
    public static final String VIEW_YEAR_PART      = "viewYearPart";
    public static final String VIEW_AMOUNT         = "viewAmount";
    public static final String VIEW_TRANSFER_FROM  = "viewTransferFrom";

    public EplOuSummaryDisciplineTimeDetailDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EplOrgUnitSummary summary = context.get(PARAM_OU_SUMMARY);
        EplCommonRowWrapper.DisciplineOrRulePairKey key = context.get(PARAM_DISC_OR_RULE_KEY);
        EplTimeCategoryEduLoad category = context.get(PARAM_CATEGORY);
        Boolean transfer = context.get(PARAM_TRANSFER);

        Long disciplineId = key.getDisciplineId();
        Long ruleId = key.getRuleId();

        if (null == summary || (null == disciplineId && null == ruleId) || null == category || null == transfer)
            return ListOutputBuilder.get(input, Collections.emptyList()).pageable(false).build();

        DQLSelectBuilder groupRowBuilder = new DQLSelectBuilder().fromEntity(EplEduGroupRow.class, "r").column(property("r"))
                .joinPath(DQLJoinType.inner, EplEduGroupRow.group().fromAlias("r"), "g")
                .joinPath(DQLJoinType.inner, EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().fromAlias("r"), "wpSlot")
                .joinPath(DQLJoinType.inner, EplStudentWPSlot.student().fromAlias("wpSlot"), "st");

        FilterUtils.applySelectFilter(groupRowBuilder, "g", EplEduGroup.registryElementPart().id(), disciplineId);
        if (null != ruleId)
            groupRowBuilder.where(exists(EplEduGroupTimeItem.class,
                                         EplEduGroupTimeItem.eduGroup().s(), property("g"),
                                         EplEduGroupTimeItem.timeRule().id().s(), ruleId)
            );

        Map<EplEduGroup, List<EplEduGroupRow>> eduGroup2RowMap = Maps.newHashMap();
        for (EplEduGroupRow groupRow : groupRowBuilder.createStatement(context.getSession()).<EplEduGroupRow>list())
        {
            SafeMap.safeGet(eduGroup2RowMap, groupRow.getGroup(), ArrayList.class).add(groupRow);
        }

        Class<? extends EplEduGroupTimeItem> clazz = transfer ? EplTransferOrgUnitEduGroupTimeItem.class : EplEduGroupTimeItem.class;
        DQLOrderDescriptionRegistry registry = new DQLOrderDescriptionRegistry(clazz, "e");

        DQLSelectBuilder dql = registry.buildDQLSelectBuilder().column(property("e"))
                .fetchPath(DQLJoinType.inner, EplEduGroupTimeItem.summary().orgUnit().fromAlias("e"), "ou")
                .where(eq(property("e", EplEduGroupTimeItem.timeRule().category()), value(category)));

        EplOuSummaryDAO.applyEduGroupTimeItemFilter(dql, "e", key, null);

        if (transfer)
            dql.where(eq(property("e", EplTransferOrgUnitEduGroupTimeItem.transferredFrom()), value(summary)));
        else
        {
            DQLSelectBuilder transferTimeItemDql = new DQLSelectBuilder()
                    .fromEntity(EplTransferOrgUnitEduGroupTimeItem.class, "t").column(property("t.id"))
                    .where(eq(property("t", EplTransferOrgUnitEduGroupTimeItem.transferredFrom()), value(summary)))
                    .where(eq(property("t", EplTransferOrgUnitEduGroupTimeItem.timeRule().category()), value(category)));

            EplOuSummaryDAO.applyEduGroupTimeItemFilter(transferTimeItemDql, "t", key, null);

            dql.where(or(
                    eq(property("e", EplEduGroupTimeItem.summary()), value(summary)),
                    in(property("e.id"), transferTimeItemDql.buildQuery())
            ));
        }

        EntityOrder entityOrder = input.getEntityOrder();
        String orderColumnName = entityOrder.getColumnName();
        OrderDirection orderDirection = entityOrder.getDirection();

        if (VIEW_AMOUNT.equals(orderColumnName))
            dql.order(property("e", EplEduGroupTimeItem.timeAmountAsLong()), orderDirection);
        else if (EplOuSummaryDisciplineTimeDetail.BIND_REG_ELEMENT_PART.equals(orderColumnName))
        {
            dql.order(property("e", EplEduGroupTimeItem.registryElementPart().registryElement().title()), orderDirection);
            dql.order(property("e", EplEduGroupTimeItem.registryElementPart().registryElement().number()), orderDirection);
            dql.order(property("e", EplEduGroupTimeItem.registryElementPart().number()), orderDirection);
        }
        else
            registry.applyOrderWithLeftJoins(dql, entityOrder);
        List<EplEduGroupTimeItem> timeItems = dql.createStatement(context.getSession()).list();

        if (VIEW_TRANSFER_FROM.equals(orderColumnName))
        {
            final OrgUnit summaryOrgUnit = summary.getOrgUnit();
            Collections.sort(timeItems, (t1, t2) -> {
                OrgUnit ou1 = t1.getSummary().getOrgUnit();
                OrgUnit ou2 = t2.getSummary().getOrgUnit();

                String title1 = summaryOrgUnit.equals(ou1) ? "" : ou1.getShortTitle();
                String title2 = summaryOrgUnit.equals(ou2) ? "" : ou2.getShortTitle();
                return title1.compareTo(title2);
            });
            if (orderDirection == OrderDirection.desc) Collections.reverse(timeItems);
        }

        DSOutput output = ListOutputBuilder.get(input, timeItems).pageable(false).build();

        for (DataWrapper wrapper : DataWrapper.wrap(output))
        {
            EplEduGroupTimeItem timeItem = wrapper.getWrapped();
            wrapper.setProperty(VIEW_AMOUNT, EplErrorFormatter.errorDiff(timeItem));
            wrapper.setProperty(VIEW_TRANSFER_FROM, timeItem instanceof EplTransferOrgUnitEduGroupTimeItem ? timeItem : null);

            EplEduGroup eduGroup = timeItem.getEduGroup();

            if (null == timeItem.getEduGroup()) continue;

            List<EplEduGroupRow> rows = eduGroup2RowMap.get(eduGroup);
            if (CollectionUtils.isEmpty(rows)) continue;

            List<String> groups = Lists.newArrayList();
            List<String> courses = Lists.newArrayList();
            List<String> terms = Lists.newArrayList();
            List<String> yearParts = Lists.newArrayList();

            for (EplEduGroupRow row : rows)
            {
                EplStudentWPSlot slot = row.getStudentWP2GTypeSlot().getStudentWPSlot();
                EplGroup group = slot.getStudent().getGroup();

                if (!groups.contains(group.getTitle())) groups.add(group.getTitle());
                if (!courses.contains(group.getCourse().getTitle())) courses.add(group.getCourse().getTitle());
                if (!terms.contains(slot.getTerm().getTitle())) terms.add(slot.getTerm().getTitle());
                if (!yearParts.contains(slot.getYearPart().getTitle())) yearParts.add(slot.getYearPart().getTitle());
            }

            Collections.sort(groups, CommonCollator.RUSSIAN_COLLATOR::compare);
            Collections.sort(courses, CommonCollator.RUSSIAN_COLLATOR::compare);
            Collections.sort(terms, CommonCollator.RUSSIAN_COLLATOR::compare);
            Collections.sort(yearParts, CommonCollator.RUSSIAN_COLLATOR::compare);

            wrapper.setProperty(VIEW_ACADEMIC_GROUP, groups);
            wrapper.setProperty(VIEW_COURSE, courses);
            wrapper.setProperty(VIEW_TERM, terms);
            wrapper.setProperty(VIEW_YEAR_PART, yearParts);
        }
        return output;
    }
}
