/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.distribution;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.support.IUISettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.IEplOuSummaryDAO;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplDiscOrRuleRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.Pub.EplOuSummaryPub;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.DistributionEditNew.EplPlannedPpsDistributionEditNew.*;

/**
 * Враппер строки для расчитанных часов План. потока (для форм распределения нагрузки по ППС)
 *
 * @author Alexey Lopatin
 * @since 05.04.2016
 */
public class EplDistributionDiscRowWrapper extends EplDistributionCommonRowWrapper
{
    private EplDiscOrRuleRowWrapper _discRowWrapper;
    private EplEduGroup _eduGroup;
    private EplOrgUnitSummary _transferTo;

    private Set<EplStudent> _students;
    private Set<Term> _terms;
    private Set<YearDistributionPart> _parts;
    private int _minCourse;

    private Map<TitleColumnType, String> viewProperties = new HashMap<>();

    public EplDistributionDiscRowWrapper(EplDiscOrRuleRowWrapper wrapper)
    {
        super(wrapper.getId(), wrapper.getDiscipline(), wrapper.getRule(), wrapper.getKey());
        _discRowWrapper = wrapper;
        setTitle(wrapper.getTitle());
    }

    public EplDiscOrRuleRowWrapper getDiscRowWrapper() { return _discRowWrapper; }
    public EplEduGroup getEduGroup() { return _eduGroup; }
    public EplOrgUnitSummary getTransferTo() { return _transferTo; }
    public Set<EplStudent> getStudents() { return _students; }
    public Set<Term> getTerms() { return _terms; }
    public Set<YearDistributionPart> getParts() { return _parts; }
    public int getMinCourse() { return _minCourse; }

    public void init(EplEduGroup eduGroup, IEplOuSummaryDAO.IDistributionEduGroupData eduGroupData, Set<EplStudent> students)
    {
        _eduGroup = eduGroup;

        Set<Term> terms = eduGroupData.termMap().get(eduGroup.getId());
        Set<YearDistributionPart> parts = eduGroupData.partMap().get(eduGroup.getId());
        Long studentCount = eduGroupData.studentCountMap().get(eduGroup.getId());

        _eduGroup = getDiscRowWrapper().getEplGroups().iterator().next();
        _transferTo = getDiscRowWrapper().getTransferTo();

        _students = students;
        _terms = terms;
        _parts = parts;

        viewProperties.put(TitleColumnType.ROW_TITLE, getTitle());
        if (null != _transferTo)
            viewProperties.put(TitleColumnType.TRANSFER_TO, _transferTo.getOrgUnit().getShortTitle());

        if (null == studentCount)
            studentCount = 0L;

        viewProperties.put(TitleColumnType.STUDENT_COUNT, String.valueOf(studentCount));
        viewProperties.put(TitleColumnType.EDU_GROUP_TYPE, _eduGroup.getGroupType().getAbbreviation());
        viewProperties.put(TitleColumnType.EDU_GROUP, _eduGroup.getTitle());

        viewProperties.put(TitleColumnType.GROUP, RowCollectionFormatter.INSTANCE.format(students.stream()
                                                                                                 .map(s -> s.getGroup().getTitle())
                                                                                                 .distinct().sorted().collect(Collectors.toList())));
        viewProperties.put(TitleColumnType.COURSE, students.stream()
                .map(s -> s.getGroup().getCourse().getTitle())
                .distinct().sorted().collect(Collectors.joining(", ")));
        viewProperties.put(TitleColumnType.EDU_OU, getDiscRowWrapper().getEducationOrgUnitTitles());

        viewProperties.put(TitleColumnType.TERM, terms.stream()
                .map(Term::getTitle)
                .distinct().sorted().collect(Collectors.joining(", ")));
        viewProperties.put(TitleColumnType.YEAR_PART, parts.stream()
                .map(YearDistributionPart::getTitle)
                .distinct().sorted().collect(Collectors.joining(", ")));

        _minCourse = students.stream().mapToInt(s -> s.getGroup().getCourse().getIntValue()).min().orElse(0);
        getDiscRowWrapper().getEduGroupTimeItemList().forEach(this::addTime);
    }

    @Override
    public String getViewProperty(TitleColumnType columnType)
    {
        return viewProperties.get(columnType);
    }

    @Override
    public PairKey<String, ParametersMap> getLink(TitleColumn column)
    {
        if (column.getType().equals(TitleColumnType.TRANSFER_TO))
        {
            if (null != getTransferTo())
                return PairKey.create(EplOuSummaryPub.class.getSimpleName(), ParametersMap.createWith(UIPresenter.PUBLISHER_ID, getTransferTo().getId()));
        }
        return null;
    }

    @Override
    public void updateVisibility(IUISettings settings)
    {
        super.updateVisibility(settings);
        if (!visible) return;

        Object group = settings.get(BIND_GROUP);
        Object course = settings.get(BIND_COURSE);
        Object yearPart = settings.get(BIND_YEAR_PART);

        visible = (getDisciplineOrRulePairKeyVisible(settings))
                && (group == null || getStudents().stream().anyMatch(s -> s.getGroup().equals(group)))
                && (course == null || getStudents().stream().anyMatch(s -> s.getGroup().getCourse().equals(course)))
                && (yearPart == null || getParts().stream().anyMatch(p -> p.equals(yearPart)));
    }
}