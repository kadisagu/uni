/* $Id$ */
package ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem;

import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 2/17/15
 */
public class EplEduGroupTimeItemWrapper
{
    private EplEduGroupTimeItem _timeItem;
    private Map<EplPlannedPps, EplPlannedPpsTimeItem> _amountTimeMap = Maps.newHashMap();

    private List<EplPlannedPps> _ppsList = Lists.newArrayList();
    private EplPlannedPps _currentPps;

    public EplEduGroupTimeItemWrapper(EplEduGroupTimeItem timeItem, Map<EplPlannedPps, EplPlannedPpsTimeItem> amountTimeMap, List<EplPlannedPps> ppsList)
    {
        _timeItem = timeItem;
        _amountTimeMap = amountTimeMap;

        _ppsList = ppsList;
    }

    public EplEduGroupTimeItem getTimeItem()
    {
        return _timeItem;
    }


    public Map<EplPlannedPps, EplPlannedPpsTimeItem> getAmountTimeMap()
    {
        return _amountTimeMap;
    }

    public List<EplPlannedPps> getPpsList()
    {
        return _ppsList;
    }

    public EplPlannedPps getCurrentPps()
    {
        return _currentPps;
    }

    public void setCurrentPps(EplPlannedPps currentPps)
    {
        _currentPps = currentPps;
    }

    public String getTimeAmount()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(_timeItem.getTimeAmount());
    }

    public Double getPpsTimeAmount()
    {
        return _amountTimeMap.get(_currentPps).getTimeAmount();
    }

    public void setPpsTimeAmount(Double value)
    {
        if(value != null)
            _amountTimeMap.get(_currentPps).setTimeAmount(value);
        else
            _amountTimeMap.get(_currentPps).setTimeAmount(0L);
    }

    public String getFieldId()
    {
        return String.valueOf(_timeItem.getId()) + "_" + String.valueOf(_currentPps.getId());
    }
}
