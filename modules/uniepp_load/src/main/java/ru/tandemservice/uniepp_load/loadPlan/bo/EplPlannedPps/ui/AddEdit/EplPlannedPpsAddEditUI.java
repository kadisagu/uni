/* $Id$ */
package ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.base.bo.EmployeePost.EmployeePostManager;
import org.tandemframework.shared.employeebase.base.bo.EmployeePost.logic.IEmployeeAdditionalInfo;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uni.base.bo.PpsEntry.PpsEntryManager;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplPlannedPpsType;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.EplPlannedPpsManager;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.logic.EplPpsEntryDSHandler;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps;
import ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.EplSettingsPpsLoadManager;
import ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.logic.EplRateFormatter.invalidStaffRate;

/**
 * @author nvankov
 * @since 1/28/15
 */
@State({
        @Bind(key = EplPlannedPpsAddEditUI.PARAM_OU_SUMMARY_ID, binding = "ouSummary.id", required = true),
        @Bind(key = EplPlannedPpsAddEditUI.PARAM_TIME_WORKER, binding = "timeWorker", required = true),
        @Bind(key = EplPlannedPpsAddEditUI.PARAM_PLANNED_PPS_ID, binding = "plannedPps.id")
})
public class EplPlannedPpsAddEditUI extends UIPresenter
{
    public static final String PARAM_OU_SUMMARY_ID = "orgUnitSummaryId";
    public static final String PARAM_PLANNED_PPS_ID = "plannedPpsId";
    public static final String PARAM_TIME_WORKER = EplPpsEntryDSHandler.PARAM_TIME_WORKER;
    public static final String PARAM_MODULE_EXISTS = "moduleExists";

    // employeePost
    public static final String PARAM_EP_ONLY_OU = EplPpsEntryDSHandler.PARAM_ONLY_OU_PPS;
    public static final String PARAM_EP_ORG_UNIT_ID = EplPpsEntryDSHandler.PARAM_ORG_UNIT_ID;
    public static final String PARAM_EP_POST_ID = "postId";

    // timeWorker
    public static final String PARAM_TW_ONLY_OU = PpsEntryManager.PARAM_TIME_WORKER_ONLY_OU;
    public static final String PARAM_TW_ORG_UNIT_ID = PpsEntryManager.PARAM_TIME_WORKER_ORG_UNIT_ID;
    public static final String PARAM_TW_EDU_YEAR_ID = PpsEntryManager.PARAM_TIME_WORKER_EDU_YEAR_ID;

    private EplOrgUnitSummary _ouSummary = new EplOrgUnitSummary();
    private boolean _timeWorker;

    private EplPlannedPps _plannedPps = new EplPlannedPps();
    private boolean _onlyOuPps;
    private boolean _onlyOuTimeWorker;
    private boolean _moduleExists;

    @Override
    public void onComponentRefresh()
    {
        _ouSummary = DataAccessServices.dao().getNotNull(_ouSummary.getId());

        if (isPps())
            _onlyOuPps = true;
        else
            _onlyOuTimeWorker = true;

        if (isEditForm())
        {
            _plannedPps = DataAccessServices.dao().getNotNull(_plannedPps.getId());
            PpsEntry ppsEntry = _plannedPps.getPpsEntry();
            if (null != ppsEntry && !ppsEntry.getOrgUnit().equals(_ouSummary.getOrgUnit()))
            {
                _onlyOuPps = false;
                _onlyOuTimeWorker = false;
            }
        }
        else
        {
            _plannedPps.setOrgUnitSummary(_ouSummary);
            _plannedPps.setStaffRateAsLong(0L);
        }

        _moduleExists = EplPlannedPpsManager.instance().dao().isTimeWorkerExists();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PARAM_EP_ONLY_OU, isOnlyOuPps());
        dataSource.put(PARAM_EP_ORG_UNIT_ID, _ouSummary.getOrgUnit().getId());
        dataSource.put(PARAM_EP_POST_ID, isHasType() && getType().isVacancyAsStaff() && null != getPlannedPps().getPost() ? getPlannedPps().getPost().getId() : null);

        dataSource.put(PARAM_TW_ONLY_OU, isOnlyOuTimeWorker());
        dataSource.put(PARAM_TW_ORG_UNIT_ID, _ouSummary.getOrgUnit().getId());
        dataSource.put(PARAM_TW_EDU_YEAR_ID, _ouSummary.getStudentSummary().getEppYear().getEducationYear().getId());

        dataSource.put(PARAM_TIME_WORKER, isTimeWorker());
        dataSource.put(PARAM_MODULE_EXISTS, isModuleExists());
    }

    public void onChangeStaffRate()
    {
        Long staffRate = (long) (IEmployeeAdditionalInfo.MULTIPLIER);
        PpsEntry teacher = _plannedPps.getPpsEntry();
        if (null != teacher && teacher instanceof PpsEntryByEmployeePost)
        {
            List<Long> employeePostIds = DataAccessServices.dao().getList(EmployeePost4PpsEntry.class, EmployeePost4PpsEntry.ppsEntry(), (PpsEntryByEmployeePost) teacher)
                    .stream().map(item -> item.getEmployeePost().getId())
                    .collect(Collectors.toList());

            staffRate = EmployeePostManager.instance().employeeInfo().getRateMap(employeePostIds).values().stream().mapToLong(Long::longValue).sum();
        }
        _plannedPps.setStaffRateAsLong(staffRate);

        onChangeMaxTimeAmount();
    }

    public void onChangeMaxTimeAmount()
    {
        PpsEntry teacher = _plannedPps.getPpsEntry();

        if (isTimeWorker())
            _plannedPps.setMaxTimeAmountAsLong(teacher == null || teacher.getRemovalDate() != null ? 0L : teacher.getTimeAmountAsLong());
        else
        {
            Map<PostBoundedWithQGandQL, EplSettingsPpsLoad> postMap = EplSettingsPpsLoadManager.instance().dao().getPostMap(_ouSummary.getId());

            PostBoundedWithQGandQL post = null != teacher && teacher instanceof PpsEntryByEmployeePost ? ((PpsEntryByEmployeePost) teacher).getPost() : _plannedPps.getPost();
            EplSettingsPpsLoad setting = postMap.get(post);
            final EplSettingsPpsLoad toAll = postMap.get(null);
            if (setting == null || (toAll != null && toAll.getLevel() > setting.getLevel()))
                setting = toAll;

            double maxValue = 0d;
            if (setting != null)
            {
                maxValue = setting.getHoursAdditional();
                if (setting.isStaffRateFactor() && _plannedPps != null && _plannedPps.getStaffRateAsDouble() != null)
                    maxValue *= _plannedPps.getStaffRateAsDouble();
                if (setting.isStaffNumberFactor() && _plannedPps != null && _plannedPps.getPpsEntry() != null)
                    maxValue *= _plannedPps.getPpsEntry() == null ? 1 : EplPlannedPpsManager.instance().dao().getEmployeePost4PpsEntryCount(_plannedPps.getPpsEntry().getId());

            }
            _plannedPps.setMaxTimeAmount(maxValue);
        }
    }

    public void onClickApply()
    {
        EplPlannedPps plannedPps = getPlannedPps();
        Double staffRateAsDouble = plannedPps.getStaffRateAsDouble();
        Double minStaffRate;
        if ((minStaffRate = invalidStaffRate(staffRateAsDouble)) != null)
        {
            UserContext.getInstance().getErrorCollector().add("Доля ставки должна быть кратна " + minStaffRate + ".", "staffRate");
            return;
        }
        EplPlannedPpsManager.instance().dao().savePlannedPps(plannedPps);
        deactivate();
    }

    public boolean isDisabled()
    {
        return !getOuSummary().getState().getCode().equals(EppState.STATE_FORMATIVE);
    }

    public boolean isOnlyOuPpsDisabled()
    {
        return isPps() && !getOuSummary().getState().getCode().equals(EppState.STATE_FORMATIVE);
    }

    public boolean isEditForm()
    {
        return null != _plannedPps.getId();
    }

    public boolean isHasType()
    {
        return null != getType();
    }

    public boolean isTeacherEpRequired()
    {
        return isHasType() && getType().isStaff();
    }

    public boolean isTeacherTwRequired()
    {
        return isHasType() && getType().isTimeWorker();
    }

    public String getSticker()
    {
        return (isEditForm() ? "Редактирование" : "Добавление") + (isTimeWorker() ? " планируемого почасовика" : " планируемого ППС");
    }

    // Getters & Setters

    public EplOrgUnitSummary getOuSummary()
    {
        return _ouSummary;
    }

    public void setOuSummary(EplOrgUnitSummary ouSummary)
    {
        _ouSummary = ouSummary;
    }

    public EplPlannedPps getPlannedPps()
    {
        return _plannedPps;
    }

    public void setPlannedPps(EplPlannedPps plannedPps)
    {
        _plannedPps = plannedPps;
    }

    public EplPlannedPpsType getType()
    {
        return getPlannedPps().getType();
    }

    public boolean isTimeWorker()
    {
        return _timeWorker;
    }

    public void setTimeWorker(boolean timeWorker)
    {
        _timeWorker = timeWorker;
    }

    public boolean isOnlyOuPps()
    {
        return _onlyOuPps;
    }

    public void setOnlyOuPps(boolean onlyOuPps)
    {
        _onlyOuPps = onlyOuPps;
    }

    public boolean isOnlyOuTimeWorker()
    {
        return _onlyOuTimeWorker;
    }

    public void setOnlyOuTimeWorker(boolean onlyOuTimeWorker)
    {
        _onlyOuTimeWorker = onlyOuTimeWorker;
    }

    public boolean isPps()
    {
        return !isTimeWorker();
    }

    public boolean isShowStaffRate()
    {
        return _plannedPps.getPost() != null;
    }

    public boolean isModuleExists()
    {
        return _moduleExists;
    }

    public boolean isCantEditStaffRate() {
        return disabled("eplPlannedPpsAddEdit_StaffRate_edit");
    }

    public boolean isCantEditMaxTimeAmount() {
        return disabled("eplPlannedPpsAddEdit_MaxTimeAmount_edit");
    }
    public boolean isCantEditMaxTimeAmountTw() {
        return disabled("eplPlannedPpsAddEditTw_maxTimeAmount_edit");
    }

    private boolean disabled(String key)
    {
        return !CoreServices.securityService().check(_ouSummary.getOrgUnit(), ContextLocal.getUserContext().getPrincipalContext(), key);
    }
}
