/* $Id:$ */
package ru.tandemservice.uniepp_load.catalog.ext.Catalog;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import ru.tandemservice.uniepp_load.catalog.entity.*;

/**
 * @author oleyba
 * @since 11/28/14
 */
@Configuration
public class CatalogExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CatalogManager catalogManager;

    @Bean
    public ItemListExtension<IDynamicCatalogDesc> itemListExtension()
    {
        return itemListExtension(catalogManager.dynamicCatalogsExtPoint())
            .add(StringUtils.uncapitalize(EplTimeCategoryEduLoad.class.getSimpleName()), EplTimeCategoryEduLoad.getUiDesc())
            .add(StringUtils.uncapitalize(EplTimeCategoryNonEduLoad.class.getSimpleName()), EplTimeCategoryNonEduLoad.getUiDesc())
            .add(StringUtils.uncapitalize(EplTimeRuleSimple.class.getSimpleName()), EplTimeRuleSimple.getUiDesc())
            .add(StringUtils.uncapitalize(EplTimeRuleEduGroupScript.class.getSimpleName()), EplTimeRuleEduGroupScript.getUiDesc())
            .add(StringUtils.uncapitalize(EplTimeRuleEduGroupFormula.class.getSimpleName()), EplTimeRuleEduGroupFormula.getUiDesc())
            .add(StringUtils.uncapitalize(EplTimeRuleNonEduLoad.class.getSimpleName()), EplTimeRuleNonEduLoad.getUiDesc())
            .add(StringUtils.uncapitalize(EplStudentSummaryState.class.getSimpleName()), EplStudentSummaryState.getUiDesc())
            .create();
    }
}
