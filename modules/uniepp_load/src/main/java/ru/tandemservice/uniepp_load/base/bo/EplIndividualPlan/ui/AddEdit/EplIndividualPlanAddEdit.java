/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniepp.base.bo.EppOrgUnit.EppOrgUnitManager;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.EplIndividualPlanManager;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps;
import ru.tandemservice.uniepp_load.report.bo.EplReport.EplReportManager;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.EplIndividualPlanManager.DS_ORG_UNIT;
import static ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.EplIndividualPlanManager.DS_STUDENT_SUMMARY;
import static ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.AddEdit.EplIndividualPlanAddEditUI.*;

/**
 * @author Alexey Lopatin
 * @since 02.08.2016
 */
@Configuration
public class EplIndividualPlanAddEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(EplReportManager.DS_SUMMARY_EPP_YEAR, getName(), EplReportManager.instance().summaryEduYearDSHandler())
                                       .valueStyleSource(EducationCatalogsManager.getEduYearValueStyleSource()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_ORG_UNIT, getName(), EppOrgUnitManager.instance().eppOrgUnitDSHandlerForAddEditForm(getName())))
                .addDataSource(EplIndividualPlanManager.instance().teacherDSConfig())
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_STUDENT_SUMMARY, getName(), EplStudentSummary.defaultNotArchiveSelectDSHandler(getName())
                        .where(EplStudentSummary.eppYear(), PARAM_EPP_YEAR)
                        .customize((alias, dql, context, filter) -> dql.where(and(
                                exists(EplOrgUnitSummary.class,
                                       EplOrgUnitSummary.studentSummary().s(), property(alias),
                                       EplOrgUnitSummary.orgUnit().s(), context.<OrgUnit>get(PARAM_ORG_UNIT)
                                ),
                                exists(new DQLSelectBuilder().fromEntity(EplPlannedPps.class, "pps").column(value(1)).top(1)
                                               .where(eq(property("pps", EplPlannedPps.orgUnitSummary().studentSummary()), property(alias)))
                                               .where(eq(property("pps", EplPlannedPps.orgUnitSummary().orgUnit()), value(context.<EplStudentSummary>get(PARAM_ORG_UNIT))))
                                               .where(eq(property("pps", EplPlannedPps.ppsEntry()), value(context.<EplStudentSummary>get(PARAM_PPS))))
                                               .buildQuery()
                                )
                        )))
                ))
                .create();
    }
}
