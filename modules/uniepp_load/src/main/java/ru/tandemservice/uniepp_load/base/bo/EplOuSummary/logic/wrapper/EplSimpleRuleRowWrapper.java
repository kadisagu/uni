/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper;

import com.google.common.collect.Lists;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplSimpleTimeItem;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleSimple;

import java.util.List;

/**
 * Враппер строки для расчитанных часов по простой норме
 *
 * @author Alexey Lopatin
 * @since 04.04.2016
 */
public class EplSimpleRuleRowWrapper extends EplCommonRowWrapper
{
    private List<EplSimpleTimeItem> _simpleTimeItemList;

    public static final String BY_SIMPLE_TIME_RULE = "bySimpleTimeRule";

    public EplSimpleRuleRowWrapper(Long id, EplTimeRuleSimple rule, EplCommonRowWrapper.DisciplineOrRulePairKey key)
    {
        super(id, null, rule, key);
    }

    public void init()
    {
        _simpleTimeItemList = Lists.newArrayList();
    }

    public List<EplSimpleTimeItem> getSimpleTimeItemList() { return _simpleTimeItemList; }
}
