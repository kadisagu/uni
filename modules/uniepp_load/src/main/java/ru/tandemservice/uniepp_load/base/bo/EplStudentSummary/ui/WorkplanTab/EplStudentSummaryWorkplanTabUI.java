/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.WorkplanTab;

import org.hibernate.Session;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.process.*;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSettingsUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniepp_load.base.bo.EplState.EplStateManager;
import ru.tandemservice.uniepp_load.base.bo.EplState.logic.EplStateDao;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.daemon.EplStudentSummaryDaemonBean;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EditWpRel.EplStudentSummaryEditWpRel;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.InfoBlock.EplStudentSummaryInfoBlockUI;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplStudentSummaryState;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTabUI;

import static ru.tandemservice.uniepp_load.catalog.entity.codes.EplStudentSummaryStateCodes.ATTACH_WORK_PLAN;
import static ru.tandemservice.uniepp_load.catalog.entity.codes.EplStudentSummaryStateCodes.PLANNING_FLOW;

/**
 * @author oleyba
 * @since 10/13/11
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "summaryHolder.id"),
    @Bind(key = EplOrgUnitTabUI.BIND_ORG_UNIT_ID, binding = "orgUnitHolder.id")
})
public class EplStudentSummaryWorkplanTabUI extends UIPresenter
{
    public static final String PARAM_HAS_ERRORS = "hasErrors";
    public static final String PARAM_NO_WP = "noWp";

    private EntityHolder<EplStudentSummary> summaryHolder = new EntityHolder<>();
    private EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<>();
    private EplStateDao.EplStudentSummaryStateWrapper _stateWrapper;

    private boolean summaryPub = true;
    private boolean _disabled;

    @Override
    public void onComponentRefresh()
    {
        getOrgUnitHolder().refresh();
        if (getOrgUnitHolder().getId() != null) {
            setSummaryPub(false);
            if (getOrgUnitHolder().getId().equals(getSummaryHolder().getId())) {
                getSummaryHolder().setId(null);
                getSummaryHolder().setValue(null);
            }
            getSettings().set(EducationCatalogsManager.PARAM_FORMATIVE_ORGUNIT, getOrgUnitHolder().getValue());
            Long id = _uiSettings.<Long>get("summary." + getOrgUnitHolder().getId());
            final EplStudentSummary iEntity = id == null ? null : DataAccessServices.dao().get(id);
            if (id != null && null == iEntity)
            {
                _uiSettings.<Long>set("summary." + getOrgUnitHolder().getId(), null);
            }
            getSummaryHolder().setValue(iEntity);
            getSummaryHolder().refreshAllowNotExist();
            saveSettings();
        }
        getSummaryHolder().refresh();
        if (null != getSummary())
        {
            EplStudentSummaryInfoBlockUI.componentActivate(_uiSupport, _uiActivation, getSummary().getId());
            EplStudentSummaryState state = getSummary().getState();
            _stateWrapper = EplStateManager.instance().dao().getSummaryStateTitle(getSummary(), ATTACH_WORK_PLAN, state.isAttachWp());
            _disabled = EplStateManager.instance().dao().isCurrentStateMore(state, ATTACH_WORK_PLAN);
        }
    }

    @Override
    public void onComponentRender()
    {
        Session session = getSupport().getSession();
        session.flush();
        session.clear();
    }

    public void onEditEntityFromList()
    {
        getActivationBuilder()
            .asRegionDialog(EplStudentSummaryEditWpRel.class)
            .parameter(PublisherActivator.PUBLISHER_ID_KEY, getListenerParameterAsLong())
            .activate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EplStudentSummaryWorkplanTab.DS_EPL_STUDENT_WORKPLAN.equals(dataSource.getName())) {
            dataSource.put(EplStudentWorkplanDSHandler.PARAM_SUMMARY, getSummary());
            dataSource.put(EplStudentWorkplanDSHandler.PARAM_SETTINGS, getSettings());
        } else if (EplStudentSummaryWorkplanTab.DS_EDU_HS.equals(dataSource.getName())) {
            dataSource.putAll(getSettings().getAsMap(EducationCatalogsManager.PARAM_FORMATIVE_ORGUNIT));
        } else if ("groupDS".equals(dataSource.getName()))
        {
            dataSource.put(EplStudentWorkplanDSHandler.PARAM_SUMMARY, getSummary());
            dataSource.putAll(getSettings().getAsMap("formativeOrgUnit", "eduHS", "developForm", "course"));
        } else if (EplStudentSummaryWorkplanTab.DS_SUMMARY.equals(dataSource.getName())) {
            dataSource.put(EplStudentSummaryManager.PARAM_FORMATIVE_ORG_UNIT, getOrgUnitHolder().getValue());
        }
    }

    public void onChangeSummary()
    {
        if (null != getSummary())
            EplStudentSummaryInfoBlockUI.componentActivate(_uiSupport, _uiActivation, getSummary().getId());
        saveSettings();
        ContextLocal.getDesktop().setRefreshScheduled(true);
    }

    @Override
    public void saveSettings()
    {
        if (!isSummaryPub()) {
            getSettings().set("summary." + getOrgUnitHolder().getId(), getSummary() == null ? null : getSummary().getId());
        }
        super.saveSettings();
    }

    @Override
    public void clearSettings()
    {
        CommonBaseSettingsUtil.clearAndSaveSettingsExcept(getSettings(), "summary." + getOrgUnitHolder().getId());
        if (getOrgUnitHolder().getId() != null) {
            getSettings().set(EducationCatalogsManager.PARAM_FORMATIVE_ORGUNIT, getOrgUnitHolder().getValue());
        }
        super.saveSettings();
    }

    public String getEditPK() {
        if (isSummaryPub()) return "eplStudentSummaryEditWp";
        return new OrgUnitSecModel(getOrgUnitHolder().getValue()).getPermission("editWpEplWpTab");
    }

    public void onClickChangeState()
    {
        try {
            if (getSummary().getState().getCode().equals(ATTACH_WORK_PLAN))
            {
                final IBackgroundProcess process = new BackgroundProcessBase()
                {
                    @Override
                    public ProcessResult run(ProcessState state)
                    {
                        EplStudentSummaryDaemonBean.DAEMON.wakeUpAndWaitDaemon(24*60*60);
                        return null;
                    }
                };
                new BackgroundProcessHolder().start("Обновление МСРП", process, ProcessDisplayMode.unknown);
            }
            EplStateManager.instance().dao().changeSummaryState(getSummary(), ATTACH_WORK_PLAN, PLANNING_FLOW);
        } finally {
            ContextLocal.getDesktop().setRefreshScheduled(true);
        }
    }

    // getters and setters

    public EplStateDao.EplStudentSummaryStateWrapper getStateWrapper()
    {
        return _stateWrapper;
    }

    public boolean isDisabled()
    {
        return _disabled;
    }

    public boolean isChangeStateVisible()
    {
        return null == orgUnitHolder.getId() && null != _stateWrapper && _stateWrapper.isVisible();
    }

    public boolean isShowSummaryTab() {
        return getSummary() != null;
    }

    public EplStudentSummary getSummary()
    {
        return getSummaryHolder().getValue();
    }

    public void setSummary(EplStudentSummary summary) {
        getSummaryHolder().setValue(summary);
    }

    public EntityHolder<EplStudentSummary> getSummaryHolder()
    {
        return summaryHolder;
    }

    public EntityHolder<OrgUnit> getOrgUnitHolder()
    {
        return orgUnitHolder;
    }

    public boolean isSummaryPub()
    {
        return summaryPub;
    }

    public void setSummaryPub(boolean summaryPub)
    {
        this.summaryPub = summaryPub;
    }

}
