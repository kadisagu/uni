/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.distribution;

import org.tandemframework.caf.ui.support.IUISettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplSimpleRuleRowWrapper;

/**
 * Враппер строки для расчитанных часов по простой норме (для форм распределения нагрузки по ППС)
 *
 * @author Alexey Lopatin
 * @since 05.04.2016
 */
public class EplDistributionSimpleRuleRowWrapper extends EplDistributionCommonRowWrapper
{
    public EplDistributionSimpleRuleRowWrapper(EplSimpleRuleRowWrapper wrapper)
    {
        super(wrapper.getId(), null, wrapper.getRule(), wrapper.getKey());
        setTitle(wrapper.getTitle());
    }

    @Override
    public String getViewProperty(TitleColumnType columnType)
    {
        if (TitleColumnType.ROW_TITLE.equals(columnType)) return getTitle();
        return "";
    }

    @Override
    public PairKey<String, ParametersMap> getLink(TitleColumn column) { return null; }

    @Override
    public void updateVisibility(IUISettings settings)
    {
        super.updateVisibility(settings);
        if (!visible) return;
        visible = getDisciplineOrRulePairKeyVisible(settings);
    }
}