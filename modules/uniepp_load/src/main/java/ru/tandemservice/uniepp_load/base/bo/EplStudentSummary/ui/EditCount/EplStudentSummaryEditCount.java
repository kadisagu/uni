/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EditCount;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.catalog.entity.EplCompensationSource;

/**
 * @author oleyba
 * @since 10/27/14
 */
@Configuration
public class EplStudentSummaryEditCount extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS("formOuDS").handler(UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler()).addColumn(OrgUnit.fullTitle().s()))
            .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
            .addDataSource(UniStudentManger.instance().courseDSConfig())
            .addDataSource(selectDS("compSourceDS").handler(EplStudentSummaryManager.instance().compSourceDSHandler()).addColumn(EplCompensationSource.title().s()))
            .create();
    }
}