/* $Id$ */
package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Alexey Lopatin
 * @since 03.02.2016
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_load_2x9x3_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.3")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // заполнить контрольные часы значением по умолчанию для активных норм, иначе 0
        tool.executeUpdate("update epl_time_item_t set controltimeamountaslong_p = timeamountaslong_p where id in " +
                                   "(select item.id from epl_time_item_t item inner join epl_time_rule_t t_rule on item.timerule_id = t_rule.id where t_rule.disableddate_p is null)");
    }
}
