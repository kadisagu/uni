package ru.tandemservice.uniepp_load.report.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.uniepp_load.report.bo.EplReport.ui.OrgUnitEduAssignmentAdd.EplReportOrgUnitEduAssignmentAdd;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.logic.IEplReport;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.logic.IEplStorableReportDesc;
import ru.tandemservice.uniepp_load.report.entity.gen.EplReportOrgUnitEduAssignmentGen;

import java.util.Arrays;
import java.util.List;

/** @see ru.tandemservice.uniepp_load.report.entity.gen.EplReportOrgUnitEduAssignmentGen */
public class EplReportOrgUnitEduAssignment extends EplReportOrgUnitEduAssignmentGen implements IEplReport
{
    private static final List<String> VIEW_PROP_LIST = Arrays.asList(EplReportOrgUnitEduAssignmentGen.L_EDU_YEAR, P_STUDENT_SUMMARY, P_ORG_UNIT, P_YEAR_PART, P_DEVELOP_FORM, P_DEVELOP_CONDITION);

    public static final IEplStorableReportDesc DESC = new IEplStorableReportDesc() {
        @Override public String getReportKey() { return EplReportOrgUnitEduAssignment.class.getSimpleName(); }
        @Override public Class<? extends IEplReport> getReportClass() { return EplReportOrgUnitEduAssignment.class; }
        @Override public List<String> getPubPropertyList() { return VIEW_PROP_LIST; }
        @Override public List<String> getListPropertyList() { return VIEW_PROP_LIST; }
        @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return EplReportOrgUnitEduAssignmentAdd.class; }
    };

    @Override
    public IEplStorableReportDesc getDesc()
    {
        return DESC;
    }
}