/* $Id:$ */
package ru.tandemservice.uniepp_load.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.EplOrgUnitManager;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.logic.IEplOrgUnitDao;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTab;

/**
 * @author oleyba
 * @since 12/6/14
 */
@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager
{
    public static final String TAB_EPP_LOAD_ORG_UNIT = "eppLoadOrgUnitTab";

    @Autowired
    public OrgUnitView _orgUnitView;

    @Bean
    public TabPanelExtension tabPanelExtension() {
        return this.tabPanelExtensionBuilder(this._orgUnitView.orgUnitTabPanelExtPoint())
        .addTab(
            componentTab(TAB_EPP_LOAD_ORG_UNIT, EplOrgUnitTab.class)
            .permissionKey("ui:secModel.viewEplTab")
            .parameters("mvel:['publisherId':presenter.orgUnit.id]")
            .visible("ognl:@"+OrgUnitViewExt.class.getName()+"@showEppLoadOrgUnitTab(presenter.orgUnit)")
        )
        .create();
    }

    public static boolean showEppLoadOrgUnitTab(final OrgUnit orgUnit) {
        IEplOrgUnitDao dao = EplOrgUnitManager.instance().dao();
        return dao.isOrgUnitForming(orgUnit) || dao.isOrgUnitOwner(orgUnit);
    }

}
