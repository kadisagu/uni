package ru.tandemservice.uniepp_load.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleSimple;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Норма времени учебной нагрузки (простая)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplTimeRuleSimpleGen extends EplTimeRuleEduLoad
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleSimple";
    public static final String ENTITY_NAME = "eplTimeRuleSimple";
    public static final int VERSION_HASH = -525807778;
    private static IEntityMeta ENTITY_META;

    public static final String P_COEFFICIENT = "coefficient";
    public static final String P_PARAMETER_NEEDED = "parameterNeeded";
    public static final String P_PARAMETER_NAME = "parameterName";

    private double _coefficient;     // Коэффициент
    private boolean _parameterNeeded;     // Необходим ввод базового параметра
    private String _parameterName;     // Наименование базового параметра

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Коэффициент. Свойство не может быть null.
     */
    @NotNull
    public double getCoefficient()
    {
        return _coefficient;
    }

    /**
     * @param coefficient Коэффициент. Свойство не может быть null.
     */
    public void setCoefficient(double coefficient)
    {
        dirty(_coefficient, coefficient);
        _coefficient = coefficient;
    }

    /**
     * @return Необходим ввод базового параметра. Свойство не может быть null.
     */
    @NotNull
    public boolean isParameterNeeded()
    {
        return _parameterNeeded;
    }

    /**
     * @param parameterNeeded Необходим ввод базового параметра. Свойство не может быть null.
     */
    public void setParameterNeeded(boolean parameterNeeded)
    {
        dirty(_parameterNeeded, parameterNeeded);
        _parameterNeeded = parameterNeeded;
    }

    /**
     * @return Наименование базового параметра.
     */
    @Length(max=255)
    public String getParameterName()
    {
        return _parameterName;
    }

    /**
     * @param parameterName Наименование базового параметра.
     */
    public void setParameterName(String parameterName)
    {
        dirty(_parameterName, parameterName);
        _parameterName = parameterName;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EplTimeRuleSimpleGen)
        {
            setCoefficient(((EplTimeRuleSimple)another).getCoefficient());
            setParameterNeeded(((EplTimeRuleSimple)another).isParameterNeeded());
            setParameterName(((EplTimeRuleSimple)another).getParameterName());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplTimeRuleSimpleGen> extends EplTimeRuleEduLoad.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplTimeRuleSimple.class;
        }

        public T newInstance()
        {
            return (T) new EplTimeRuleSimple();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "coefficient":
                    return obj.getCoefficient();
                case "parameterNeeded":
                    return obj.isParameterNeeded();
                case "parameterName":
                    return obj.getParameterName();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "coefficient":
                    obj.setCoefficient((Double) value);
                    return;
                case "parameterNeeded":
                    obj.setParameterNeeded((Boolean) value);
                    return;
                case "parameterName":
                    obj.setParameterName((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "coefficient":
                        return true;
                case "parameterNeeded":
                        return true;
                case "parameterName":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "coefficient":
                    return true;
                case "parameterNeeded":
                    return true;
                case "parameterName":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "coefficient":
                    return Double.class;
                case "parameterNeeded":
                    return Boolean.class;
                case "parameterName":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplTimeRuleSimple> _dslPath = new Path<EplTimeRuleSimple>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplTimeRuleSimple");
    }
            

    /**
     * @return Коэффициент. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleSimple#getCoefficient()
     */
    public static PropertyPath<Double> coefficient()
    {
        return _dslPath.coefficient();
    }

    /**
     * @return Необходим ввод базового параметра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleSimple#isParameterNeeded()
     */
    public static PropertyPath<Boolean> parameterNeeded()
    {
        return _dslPath.parameterNeeded();
    }

    /**
     * @return Наименование базового параметра.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleSimple#getParameterName()
     */
    public static PropertyPath<String> parameterName()
    {
        return _dslPath.parameterName();
    }

    public static class Path<E extends EplTimeRuleSimple> extends EplTimeRuleEduLoad.Path<E>
    {
        private PropertyPath<Double> _coefficient;
        private PropertyPath<Boolean> _parameterNeeded;
        private PropertyPath<String> _parameterName;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Коэффициент. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleSimple#getCoefficient()
     */
        public PropertyPath<Double> coefficient()
        {
            if(_coefficient == null )
                _coefficient = new PropertyPath<Double>(EplTimeRuleSimpleGen.P_COEFFICIENT, this);
            return _coefficient;
        }

    /**
     * @return Необходим ввод базового параметра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleSimple#isParameterNeeded()
     */
        public PropertyPath<Boolean> parameterNeeded()
        {
            if(_parameterNeeded == null )
                _parameterNeeded = new PropertyPath<Boolean>(EplTimeRuleSimpleGen.P_PARAMETER_NEEDED, this);
            return _parameterNeeded;
        }

    /**
     * @return Наименование базового параметра.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleSimple#getParameterName()
     */
        public PropertyPath<String> parameterName()
        {
            if(_parameterName == null )
                _parameterName = new PropertyPath<String>(EplTimeRuleSimpleGen.P_PARAMETER_NAME, this);
            return _parameterName;
        }

        public Class getEntityClass()
        {
            return EplTimeRuleSimple.class;
        }

        public String getEntityName()
        {
            return "eplTimeRuleSimple";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
