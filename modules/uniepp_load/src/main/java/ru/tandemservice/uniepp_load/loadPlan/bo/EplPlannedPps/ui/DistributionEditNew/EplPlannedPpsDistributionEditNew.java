/* $Id:$ */
package ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.DistributionEditNew;

import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.tapsupport.component.selection.DefaultSelectValueStyle;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCommonParentWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCommonRowWrapper;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager.DS_YEAR_PART;

/**
 * @author oleyba
 * @since 21.08.15
 */
@Configuration
public class EplPlannedPpsDistributionEditNew extends BusinessComponentManager
{
    public static final String DS_DISC_OR_RULE = "discOrRuleDS";

    public static final String BIND_DISC_OR_RULE = "discOrRule";
    public static final String BIND_HAS_TRANSFER_TO = "hasTransferTo";
    public static final String BIND_GROUP = "group";
    public static final String BIND_COURSE = "course";
    public static final String BIND_YEAR_PART = "yearPart";
    public static final String BIND_PPS = "pps";
    public static final String BIND_DISPLAY_FREE_LOAD = "displayFreeLoad";
    public static final String BIND_ORG_UNIT_SUMMARY = "orgUnitSummary";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_DISC_OR_RULE, getName(), EplTimeItem.defaultSelectWithoutTransferDSHandler(getName()))
                                       .primaryKeyProperty("fieldId")
                                       .treeable(true)
                                       .valueStyleSource(value -> {
                                           final boolean disabled = value instanceof EplCommonParentWrapper;
                                           return new DefaultSelectValueStyle()
                                           {
                                               @Override public boolean isDisabled() { return disabled; }
                                               @Override public String getRowStyle() { return isDisabled() ? "color:gray;" : null; }
                                               @Override public boolean isSelectable() { return !isDisabled(); }
                                           };
                                       }))

            .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_YEAR_PART, getName(), YearDistributionPart.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) -> {
                    DQLSelectBuilder from = new DQLSelectBuilder()
                        .fromEntity(EplEduGroupRow.class, "r")
                        .fromEntity(EplEduGroupTimeItem.class, "i")
                        .where(eq(property("r", EplEduGroupRow.group()), property("i", EplEduGroupTimeItem.eduGroup())))
                        .where(eq(property("i", EplEduGroupTimeItem.summary()), value(context.<IEntity>get(BIND_ORG_UNIT_SUMMARY))))
                        .column(property("r", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().yearPart().id()));

                    Set<Long> disciplineIds = getDisciplineIds(context);
                    if (!disciplineIds.isEmpty())
                        FilterUtils.applySelectFilter(from, "i", EplEduGroupTimeItem.registryElementPart().id(), disciplineIds);
                    return dql.where(in(property(alias, "id"), from.buildQuery()));
                })).treeable(true).valueStyleSource(EducationCatalogsManager.getYearPartValueStyleSource()))

            .addDataSource(CommonBaseStaticSelectDataSource.selectDS("courseDS", getName(), Course.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) -> {
                    DQLSelectBuilder from = new DQLSelectBuilder()
                        .fromEntity(EplEduGroupRow.class, "r")
                        .fromEntity(EplEduGroupTimeItem.class, "i")
                        .where(eq(property("r", EplEduGroupRow.group()), property("i", EplEduGroupTimeItem.eduGroup())))
                        .where(eq(property("i", EplEduGroupTimeItem.summary()), value(context.<IEntity>get(BIND_ORG_UNIT_SUMMARY))))
                        .column(property("r", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().group().course().id()));

                    Set<Long> disciplineIds = getDisciplineIds(context);
                    if (!disciplineIds.isEmpty())
                        FilterUtils.applySelectFilter(from, "i", EplEduGroupTimeItem.registryElementPart().id(), disciplineIds);
                    FilterUtils.applySelectFilter(from, "r", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().yearPart(), context.get(BIND_YEAR_PART));
                    return dql.where(in(property(alias, "id"), from.buildQuery()));
                })))

            .addDataSource(CommonBaseStaticSelectDataSource.selectDS("groupDS", getName(), EplGroup.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) -> {
                    DQLSelectBuilder from = new DQLSelectBuilder()
                        .fromEntity(EplEduGroupRow.class, "r")
                        .fromEntity(EplEduGroupTimeItem.class, "i")
                        .where(eq(property("r", EplEduGroupRow.group()), property("i", EplEduGroupTimeItem.eduGroup())))
                        .where(eq(property("i", EplEduGroupTimeItem.summary()), value(context.<IEntity>get(BIND_ORG_UNIT_SUMMARY))))
                        .column(property("r", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().group().id()));

                    Set<Long> disciplineIds = getDisciplineIds(context);
                    if (!disciplineIds.isEmpty())
                        FilterUtils.applySelectFilter(from, "i", EplEduGroupTimeItem.registryElementPart().id(), disciplineIds);

                    FilterUtils.applySelectFilter(from, "r", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().yearPart(), context.get(BIND_YEAR_PART));
                    FilterUtils.applySelectFilter(from, "r", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().group().course(), context.get(BIND_COURSE));
                    return dql.where(in(property(alias, "id"), from.buildQuery()));
                }).pageable(false)
            ))

            .create();
    }

    private Set<Long> getDisciplineIds(ExecutionContext context)
    {
        List<EplCommonRowWrapper> rowWrappers = context.get(BIND_DISC_OR_RULE);
        Set<Long> disciplineIds = Sets.newHashSet();
        if (CollectionUtils.isNotEmpty(rowWrappers))
        {
            disciplineIds = rowWrappers.stream()
                    .map(row -> row.getKey().getDisciplineId())
                    .collect(Collectors.toSet());
        }
        return disciplineIds;
    }
}