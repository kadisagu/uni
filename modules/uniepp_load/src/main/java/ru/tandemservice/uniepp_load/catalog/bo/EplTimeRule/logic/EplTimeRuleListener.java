package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.shared.commonbase.base.bo.DaemonQueue.DaemonQueueManager;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRule;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * Created by nsvetlov on 24.11.2016.
 */
public class EplTimeRuleListener implements IDSetEventListener
{
    public void init()
    {
    }

    @Override
    public void onEvent(DSetEvent dSetEvent)
    {
    }
}
