/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic;

import com.google.common.base.Preconditions;
import com.google.common.collect.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.mutable.MutableInt;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.debug.DebugBlock;
import org.tandemframework.core.debug.SectionDebugItem;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.dql.util.DQLCanDeleteExpressionBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.hibsupport.log.InsertEntityEvent;
import org.tandemframework.hibsupport.log.UpdateEntityEvent;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.events.CommonbaseImmutableNaturalIdListener;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.util.GroupTitleAlgorithm;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.codes.EppEduGroupSplitVariantCodes;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.daemon.IEplStudentSummaryDaemonBean;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.*;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentCount4SplitElement;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWP2GTypeSlot;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot;
import ru.tandemservice.uniepp_load.catalog.entity.EplCompensationSource;
import ru.tandemservice.uniepp_load.catalog.entity.EplStudentSummaryState;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplCompensationSourceCodes;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplStudentSummaryStateCodes;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.*;

/**
 * @author oleyba
 * @since 10/12/11
 */
public class EplStudentSummaryDao extends UniBaseDao implements IEplStudentSummaryDao
{
    public static final String NO_GROUP = "Вне групп";

    protected static final Logger logger = Logger.getLogger(EplStudentSummaryDao.class);

    private final Pattern coursePattern = Pattern.compile("^-?\\d+$");

    @Override
    public int fillSummary(EplStudentSummary summary, boolean fillCopyFirstCourse, boolean fillEduGroups)
    {
        // выключаем системное логирование, чтобы не тормозило
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        int missEplStudents = 0;

        Debug.begin("doAddSummary");
        try {
            // Заполняем сводку
            try (DebugBlock ignored = Debug.block("doFillStudentSummary"))
            {
                missEplStudents = EplStudentSummaryManager.instance().dao().doRefreshSummary(summary, fillCopyFirstCourse);
            }
            // Обновляем МСРП
            try (DebugBlock ignored = Debug.block("updateStudentData"))
            {
                IEplStudentSummaryDaemonBean.instance.get().updateStudentData();
            }
            // Обновляем План. потоки
            try (DebugBlock ignored = Debug.block("doRecalculateEduGroups"))
            {
                EplStudentSummaryManager.instance().dao().doRefreshEduGroups(summary, fillEduGroups);
            }
        } finally {
            final SectionDebugItem end = Debug.end();
            if (end != null && logger.isInfoEnabled())
                logger.info(end.toFullString());

            // включаем системное логирование
            eventLock.release();
        }
        CoreServices.eventService().fireEvent(new InsertEntityEvent("Создание объекта", summary));
        return missEplStudents;
    }

    @Override
    public void doDeleteSummary(Long summaryId)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), "eplChangeSummary-" + summaryId, 120);
        delete(summaryId);
    }

    @Override
    public void doSaveStudents(List<EplStudent> rowList)
    {
        for (EplStudent student : rowList) {
            if (student.getCount() == 0) {
                delete(student);
            } else {
                update(student);
            }
        }

        new DQLDeleteBuilder(EplGroup.class)
            .where(notExists(new DQLSelectBuilder()
                .fromEntity(EplStudent.class, "s")
                .where(eq(property("id"), property("s", EplStudent.group().id())))
                .buildQuery()))
            .createStatement(getSession()).execute();
    }

    @Override
    public void doAddStudent(EplStudentSummary summary, EplStudent student, String groupTitle, Course course)
    {
        EplGroup group = new DQLSelectBuilder()
            .fromEntity(EplGroup.class, "g")
            .where(eq(property("g", EplGroup.summary()), value(summary)))
            .where(eq(property("g", EplGroup.title()), value(groupTitle)))
            .where(eq(property("g", EplGroup.course()), value(course)))
            .createStatement(getSession()).uniqueResult();

        if (null == group) {
            group = new EplGroup();
            group.setSummary(summary);
            group.setCourse(course);
            group.setTitle(groupTitle);
            save(group);
        } else {
            List<EplStudent> existing = new DQLSelectBuilder()
                .fromEntity(EplStudent.class, "s")
                .where(eq(property("s", EplStudent.group()), value(group)))
                .where(eq(property("s", EplStudent.educationOrgUnit()), value(student.getEducationOrgUnit())))
                .where(eq(property("s", EplStudent.compensationSource()), value(student.getCompensationSource())))
                .createStatement(getSession()).list();
            if (!existing.isEmpty()) {
                throw new ApplicationException("В сводку уже добавлены студенты с такими параметрами обучения.");
            }
        }

        student.setGroup(group);
        save(student);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String generateEplEduGroupTitle(Collection<Long> eplEduGroupIds)
    {
        final List<String> titles = new DQLSelectBuilder().fromEntity(EplEduGroupRow.class, "row")
            .predicate(DQLPredicateType.distinct)
            .column(property("g", EplGroup.title()))
            .joinPath(DQLJoinType.inner, EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().group().fromAlias("row"), "g")
            .where(in(property("row", EplEduGroupRow.group()), eplEduGroupIds))
            .order(property("g", EplGroup.title()))
            .createStatement(getSession()).list()
            ;

        return CommonBaseStringUtil.joinNotEmpty(titles, ", ");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mergeEplEduGroups(Collection<EplEduGroup> eplEduGroups, String newTitle, EppRegistryElementPart eppRegistryElementPart, Long groupWithHours, EplOrgUnitSummary summary)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), "eplSummaryStudentUpdate", 300);

        Preconditions.checkArgument(eplEduGroups.size() > 1);

        // Вид УГС должен совпадать для всех объединяемых групп
        final Set<EppGroupType> groupTypeSet = CommonBaseEntityUtil.getPropertiesSet(eplEduGroups, EplEduGroup.L_GROUP_TYPE);
        Preconditions.checkArgument(groupTypeSet.size() == 1);

        // Часть года должна совпадать для всех объединяемых групп
        final Set<YearDistributionPart> yearPartSet = CommonBaseEntityUtil.getPropertiesSet(eplEduGroups, EplEduGroup.L_YEAR_PART);
        Preconditions.checkArgument(yearPartSet.size() == 1);

        // Сводка должна совпадать для всех объединяемых групп
        final Set<EplStudentSummary> groupSummarySet = CommonBaseEntityUtil.getPropertiesSet(eplEduGroups, EplEduGroup.L_SUMMARY);
        Preconditions.checkArgument(groupSummarySet.size() == 1);

        // Создаем новую группу
        final EplEduGroup newGroup = new EplEduGroup();
        newGroup.setRegistryElementPart(eppRegistryElementPart);
        newGroup.setTitle(newTitle);
        newGroup.setYearPart(yearPartSet.iterator().next());
        newGroup.setGroupType(groupTypeSet.iterator().next());
        newGroup.setSummary(groupSummarySet.iterator().next());
        save(newGroup);

        // Получаем строки. Студенты для одинковых слотов суммируются.
        final List<Object[]> items = new DQLSelectBuilder().fromEntity(EplEduGroupRow.class, "row")
            .column(property("row", EplEduGroupRow.studentWP2GTypeSlot().id()))
            .column(DQLFunctions.sum(property("row", EplEduGroupRow.count())))
            .where(in(property("row", EplEduGroupRow.group()), eplEduGroups))
            .group(property("row", EplEduGroupRow.studentWP2GTypeSlot()))
            .createStatement(getSession()).list();

        for (Object[] item : items)
        {
            final Long slotId = (Long) item[0];
            final Number count = (Number) item[1];

            final EplEduGroupRow row = new EplEduGroupRow();
            row.setGroup(newGroup);
            row.setStudentWP2GTypeSlot((EplStudentWP2GTypeSlot) getSession().load(EplStudentWP2GTypeSlot.class, slotId));
            row.setCount(count.intValue());
            save(row);
        }

        if (groupWithHours != null) // Переносим все часы с группы (в т.ч. переданные) на результурующую План. потоки вместе с распределенными часами по ППС
        {
            EplEduGroup oldGroup = eplEduGroups.stream().filter(g -> groupWithHours.equals(g.getId())).findFirst().get();
            OrgUnit newTutorOu = summary.getOrgUnit();

            List<EplEduGroupTimeItem> hours = getList(EplEduGroupTimeItem.class, EplEduGroupTimeItem.eduGroup().id(), groupWithHours);
            for(EplEduGroupTimeItem item: hours)
            {
                EplOrgUnitSummary itemSummary = item.getSummary();
                // если исходная дисциплина в переносимых часах отличается от выбранной дисциплины на форме,
                if (!eppRegistryElementPart.equals(item.getRegistryElementPart())) {
                    // то обновить дисциплину в часах
                    item.setRegistryElementPart(eppRegistryElementPart);
                }
                //if (!itemSummary.equals(eppRegistryElementPart.get))

                // если расчет, с которого передали часы (поле «transferredFrom»), в переносимых переданных часах совпадает с расчетом для дисциплины на форме
                if (item instanceof EplTransferOrgUnitEduGroupTimeItem)
                {
                    EplTransferOrgUnitEduGroupTimeItem transferred = (EplTransferOrgUnitEduGroupTimeItem) item;
                    if (transferred.getSummary().equals(summary))
                    {
                        // то конвертируем переданные часы в обычные часы, иначе корректируем им ссылку на новое читающее
                        EplEduGroupTimeItem newItem = new EplEduGroupTimeItem();
                        newItem.update(item);
                        newItem.setSummary(summary);
                        item = moveHoursToNewItem(item, newItem);
                    }
                }
                else
                {
                    // если исходный расчет в переносимых обычных часах отличается от расчета для дисциплины на форме,
                    if (!itemSummary.equals(summary))
                    {
                        // то конвертируем часы в переданные часы с нового читающего
                        EplTransferOrgUnitEduGroupTimeItem newItem = new EplTransferOrgUnitEduGroupTimeItem();
                        newItem.update(item);
                        newItem.setSummary(itemSummary);
                        newItem.setTransferredFrom(summary);
                        item = moveHoursToNewItem(item, newItem);
                    }
                }

                item.setEduGroup(newGroup);
                saveOrUpdate(item);
            }
        }

        // Удаляем старые группы (вместе со строками)
        new DQLDeleteBuilder(EplEduGroup.class)
            .where(in("id", eplEduGroups))
            .createStatement(getSession())
            .execute();
    }

    private EplEduGroupTimeItem moveHoursToNewItem(EplEduGroupTimeItem item, EplEduGroupTimeItem newItem)
    {
        saveOrUpdate(newItem);
        for(EplPlannedPpsTimeItem planned : getList(EplPlannedPpsTimeItem.class, EplPlannedPpsTimeItem.timeItem().id(), item.getId()))
        {
            planned.setTimeItem(newItem);
            saveOrUpdate(planned);
        }
        delete(item);
        executeFlush();
        return newItem;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int massSplitEplEduGroup(Collection<Long> groupIds, int subGroupSize, boolean splitByMaxCount)
    {
        int changeEduGroupCount = 0;
        NamedSyncInTransactionCheckLocker.register(getSession(), "eplSummaryStudentUpdate", 300);

        for (Long id : groupIds)
        {
            EplEduGroup group = getNotNull(EplEduGroup.class, id);
            boolean change = splitEplEduGroup(group, subGroupSize, splitByMaxCount);
            if (change) changeEduGroupCount++;
        }
        return changeEduGroupCount;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean splitEplEduGroup(EplEduGroup srcGroup, int subGroupSize, boolean splitByMaxCount)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), "eplSummaryStudentUpdate", 300);

        Preconditions.checkArgument(subGroupSize > 0);
        final List<EplEduGroupRow> rowList = getList(EplEduGroupRow.class, EplEduGroupRow.L_GROUP, srcGroup);
        if (rowList == null  || rowList.isEmpty())
            return false;

        int allSum = 0;
        for (EplEduGroupRow row : rowList) {
            allSum += row.getCount();
        }

        if (allSum <= subGroupSize) return false;

        int subGroupNum;
        int remainder = 0;
        if (splitByMaxCount)
        {
            subGroupNum = (allSum % subGroupSize) == 0 ? allSum / subGroupSize : (allSum / subGroupSize) + 1;
            remainder = allSum % subGroupNum;
            subGroupSize = allSum / subGroupNum;
        }

        // Сортируем строки по числу студентов и разрезаем получившееся множество на равные части (возможно, с остатком),
        Collections.sort(rowList, (o1, o2) -> o2.getCount() - o1.getCount());
        final Iterator<EplEduGroupRow> iterator = rowList.iterator();
        EplEduGroupRow srcRow = iterator.next();
        do
        {
            final EplEduGroup newGroup = new EplEduGroup();
            newGroup.update(srcGroup);
            save(newGroup);

            int groupSum = subGroupSize;
            while (groupSum > 0 && allSum > 0)
            {
                final EplEduGroupRow newRow = new EplEduGroupRow();
                newRow.update(srcRow, true);
                newRow.setGroup(newGroup);
                int newCount = Math.min(groupSum, srcRow.getCount());
                if (splitByMaxCount && remainder > 0)
                {
                    newCount++;
                    remainder--;
                }
                newRow.setCount(newCount);
                save(newRow);

                groupSum -= newRow.getCount();
                allSum -= newRow.getCount();
                srcRow.setCount(srcRow.getCount() - newRow.getCount());
                if (srcRow.getCount() <= 0 && iterator.hasNext())
                    srcRow = iterator.next();
            }

            // Теперь нужно сгенерировать новое название на основе строк, которые попали в групу
            executeFlush();
            newGroup.setTitle(generateEplEduGroupTitle(ImmutableList.of(newGroup.getId())));
            update(newGroup);
        }
        while (allSum > 0);

        delete(srcGroup);
        return true;
    }

    @Override
    public int massSplitEplEduGroup(Collection<Long> groupIds, DataWrapper splitTypeWrapper)
    {
        int changeEduGroupCount = 0;
        NamedSyncInTransactionCheckLocker.register(getSession(), "eplSummaryStudentUpdate", 300);

        for (Long id : groupIds)
        {
            EplEduGroup group = getNotNull(EplEduGroup.class, id);
            boolean change = splitEplEduGroup(group, splitTypeWrapper);
            if (change) changeEduGroupCount++;
        }
        return changeEduGroupCount;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean splitEplEduGroup(EplEduGroup srcGroup, DataWrapper splitTypeWrapper)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), "eplSummaryStudentUpdate", 300);

        Object wrapper = splitTypeWrapper.getWrapped();

        EplEduGroupSplitType splitType = null;
        final String splitProperty;

        if (wrapper instanceof EppEduGroupSplitVariant)
        {
            splitProperty = EplEduGroupRow.studentWP2GTypeSlot().splitElement().s();
        }
        else if (wrapper instanceof EplEduGroupSplitType)
        {
            splitType = (EplEduGroupSplitType) wrapper;
            switch (splitType)
            {
                case BY_EDUCATION_ORGUNIT:
                    splitProperty = EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().educationOrgUnit().s();
                    break;
                case BY_COMPENSATION_SOURCE:
                    splitProperty = EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().compensationSource().s();
                    break;
                case BY_DISCIPLINE:
                    splitProperty = EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().activityElementPart().s();
                    break;
                case BY_GROUP:
                    splitProperty = EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().group().s();
                    break;
                case BY_ROW:
                    splitProperty = EplEduGroupRow.studentWP2GTypeSlot().s();
                    break;
                case BY_SPLIT_VARIANT:
                    splitProperty = EplEduGroupRow.studentWP2GTypeSlot().splitElement().splitVariant().s();
                    break;
                case BY_SPLIT_ELEMENT:
                    splitProperty = EplEduGroupRow.studentWP2GTypeSlot().splitElement().s();
                    break;
                default:
                    throw new IllegalArgumentException();
            }
        }
        else
            throw new IllegalStateException("Unknown split variant.");

        final List<EplEduGroupRow> rowList = getList(EplEduGroupRow.class, EplEduGroupRow.L_GROUP, srcGroup);
        if (rowList == null  || rowList.isEmpty())
            return false;

        final Map<Object, List<EplEduGroupRow>> rowMap = CommonBaseEntityUtil.groupByProperty(rowList, splitProperty, new LinkedHashMap<>());

        if (rowMap.size() == 1) return false;

        // Создаем новые группы и строки
        for (Map.Entry<Object, List<EplEduGroupRow>> entry : rowMap.entrySet())
        {
            Object key = entry.getKey();

            final EplEduGroup newGroup = new EplEduGroup();
            newGroup.update(srcGroup);

            if (null != splitType)
            {
                if (splitType == EplEduGroupSplitType.BY_DISCIPLINE)
                    newGroup.setRegistryElementPart((EppRegistryElementPart) key);
                else if (splitType == EplEduGroupSplitType.BY_ROW)
                    newGroup.setRegistryElementPart(((EplStudentWP2GTypeSlot) key).getStudentWPSlot().getActivityElementPart());
            }
            save(newGroup);

            for (EplEduGroupRow srcRow : entry.getValue())
            {
                final EplEduGroupRow newRow = new EplEduGroupRow();
                newRow.update(srcRow, true);
                newRow.setGroup(newGroup);
                save(newRow);
            }

            // Теперь нужно сгенерировать новое название на основе строк, которые попали в групу
            executeFlush();

            String title = generateEplEduGroupTitle(ImmutableList.of(newGroup.getId()));
            if (key instanceof EppEduGroupSplitBaseElement)
            {
                title += " (" + ((EppEduGroupSplitBaseElement) key).getSplitElementShortTitle() + ")";
            }
            newGroup.setTitle(title);
            update(newGroup);
        }

        // Удаляем старую группу (удалится вместе со строками)
        delete(srcGroup);
        return true;
    }

    @Override
    public void doRecalculateEduGroups(EplStudentSummary summary)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), "eplChangeSummary-" + summary.getId());
        Debug.begin("doRecalculateEduGroups");

        // выключаем системное логирование, чтобы не тормозило
        try (IEventServiceLock ignored = CoreServices.eventService().lock()) {
            doRefreshEduGroups(summary, false);
        }
        finally {
            final SectionDebugItem end = Debug.end();
            if (end != null && logger.isInfoEnabled())
                logger.info(end.toFullString());
        }

        CoreServices.eventService().fireEvent(new UpdateEntityEvent("План. потоки обновлены", summary));
    }



    @Override
    public List<EplTransferAffiliateWrapper> getEplTransferAffilationWrappers(Long summaryId, Course course, OrgUnit orgUnit)
    {
        List<Object[]> students = createStatement(
            new DQLSelectBuilder().fromEntity(EplStudent.class, "s")
                .joinPath(DQLJoinType.inner, EplStudent.educationOrgUnit().fromAlias("s"), "steduou", true)
                .joinEntity("s", DQLJoinType.left, EducationOrgUnit.class, "eduou", and(
                    eq(property("eduou", EducationOrgUnit.educationLevelHighSchool()), property("steduou", EducationOrgUnit.educationLevelHighSchool())),
                    eq(property("eduou", EducationOrgUnit.developForm()), property("steduou", EducationOrgUnit.developForm())),
                    eq(property("eduou", EducationOrgUnit.developCondition()), property("steduou", EducationOrgUnit.developCondition())),
                    eq(property("eduou", EducationOrgUnit.developPeriod()), property("steduou", EducationOrgUnit.developPeriod())),
                    eq(property("eduou", EducationOrgUnit.developTech()), property("steduou", EducationOrgUnit.developTech())),
                    ne(property("eduou"), property("s", EplStudent.educationOrgUnit())),
                    exists(new DQLSelectBuilder().fromEntity(TopOrgUnit.class, "tou")
                        .where(eq(property("tou", TopOrgUnit.id()), property("eduou", EducationOrgUnit.territorialOrgUnit().id())))
                        .buildQuery())
                ))
                .fetchPath(DQLJoinType.inner, EplStudent.educationOrgUnit().developCondition().fromAlias("s"), "eoudc", true)
                .fetchPath(DQLJoinType.inner, EplStudent.educationOrgUnit().developForm().fromAlias("s"), "eoudf", true)
                .fetchPath(DQLJoinType.inner, EplStudent.educationOrgUnit().developTech().fromAlias("s"), "eoudt", true)
                .fetchPath(DQLJoinType.inner, EplStudent.educationOrgUnit().developPeriod().fromAlias("s"), "eoudp", true)
                .fetchPath(DQLJoinType.inner, EplStudent.educationOrgUnit().formativeOrgUnit().fromAlias("s"), "eouf", true)
                .column(property("s"))
                .column(property("eduou"))
                .where(eq(property("s", EplStudent.group().summary().id()), value(summaryId)))
                .where(eq(property("s", EplStudent.group().course()), value(course)))
                .where(eq(property("s", EplStudent.educationOrgUnit().territorialOrgUnit()), value(orgUnit)))

        ).list();

        Map<EducationOrgUnit, EplTransferAffiliateWrapper> wrappersMap = Maps.newHashMap();

        for(Object[] obj : students)
        {
            EplStudent student = (EplStudent) obj[0];
            EducationOrgUnit educationOrgUnitForSelect = (EducationOrgUnit) obj[1];

            EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
            if(!wrappersMap.containsKey(educationOrgUnit))
                wrappersMap.put(educationOrgUnit, new EplTransferAffiliateWrapper(educationOrgUnit));

            EplTransferAffiliateWrapper wrapper = wrappersMap.get(educationOrgUnit);

            if(!wrapper.getStudents().contains(student))
                wrapper.getStudents().add(student);

            if(educationOrgUnitForSelect != null && !wrapper.getEducationOrgUnitList().contains(educationOrgUnitForSelect))
                wrapper.getEducationOrgUnitList().add(educationOrgUnitForSelect);
        }

        List<EplTransferAffiliateWrapper> wrappers = Lists.newArrayList(wrappersMap.values());

        for(EplTransferAffiliateWrapper wrapper : wrappersMap.values())
        {
            wrapper.fillData();
            wrapper.getEducationOrgUnitList().sort(CommonCollator.comparing(eou -> eou.getFormativeOrgUnit().getTitle(), true));
        }

        wrappers.sort(CommonCollator.TITLED_WITH_ID_COMPARATOR);

        return wrappers;
    }

    @Override
    public void doTransferFromAffiliate(List<EplTransferAffiliateWrapper> wrappers)
    {
        for(EplTransferAffiliateWrapper wrapper : wrappers)
        {
            if(wrapper.getSelectedEducationOrgUnit() != null)
            {
                for (EplStudent student : wrapper.getStudents())
                {
                    student.setEducationOrgUnit(wrapper.getSelectedEducationOrgUnit());
                    update(student);
                }
            }
        }
    }

    @Override
    public EplStudentSummary doCopySummary(String title, Long summaryId, boolean backup)
    {
        final Session session = getSession();

        // выключаем системное логирование, чтобы не тормозило
        final IEventServiceLock eventLock = CoreServices.eventService().lock();

        Debug.begin("doCopyEplStudentSummary");

        EplStudentSummaryState state = getByCode(EplStudentSummaryState.class, EplStudentSummaryStateCodes.CONTINGENT_PLANNING);
        EplStudentSummary newSummary = new EplStudentSummary(state);
        try
        {
            EplStudentSummary summary = getNotNull(summaryId);
            if (backup)
                newSummary.setState(summary.getState());

            List<EplGroup> groups = getList(EplGroup.class, EplGroup.summary().id(), summary.getId());
            List<EplStudent> students = getList(EplStudent.class, EplStudent.group().summary().id(), summary.getId());
            List<EplEduGroup> eduGroups = getList(EplEduGroup.class, EplEduGroup.summary().id(), summary.getId());
            List<EplEduGroupRow> eduGroupRows = getList(EplEduGroupRow.class, EplEduGroupRow.group().summary().id(), summary.getId());
            List<EplStudentWPSlot> studentWPSlots = getList(EplStudentWPSlot.class, EplStudentWPSlot.student().group().summary().id(), summary.getId());
            List<EplStudentWP2GTypeSlot> studentWP2GTypeSlots = getList(EplStudentWP2GTypeSlot.class, EplStudentWP2GTypeSlot.studentWPSlot().student().group().summary().id(), summary.getId());
            List<EplStudent2WorkPlan> student2WorkPlans = getList(EplStudent2WorkPlan.class, EplStudent2WorkPlan.student().group().summary().id(), summary.getId());
            List<EplOrgUnitSummary> orgUnitSummaries = getList(EplOrgUnitSummary.class, EplOrgUnitSummary.studentSummary().id(), summary.getId());
            List<EplEduGroupTimeItem> eduGroupTimeItems = getList(EplEduGroupTimeItem.class, EplEduGroupTimeItem.summary().studentSummary().id(), summary.getId());
            List<EplSimpleTimeItem> simpleTimeItems = getList(EplSimpleTimeItem.class, EplSimpleTimeItem.summary().studentSummary().id(), summary.getId());
            List<EplPlannedPps> plannedPpses = getList(EplPlannedPps.class, EplPlannedPps.orgUnitSummary().studentSummary().id(), summary.getId());
            List<EplPlannedPpsTimeItem> plannedPpsTimeItems = getList(EplPlannedPpsTimeItem.class, EplPlannedPpsTimeItem.pps().orgUnitSummary().studentSummary().id(), summary.getId());

            Map<EplGroup, EplGroup> groupMap = Maps.newHashMap();
            Map<EplStudent, EplStudent> studentMap = Maps.newHashMap();
            Map<EplEduGroup, EplEduGroup> eduGroupMap = Maps.newHashMap();
            Map<EplStudentWPSlot, EplStudentWPSlot> studentWPSlotMap = Maps.newHashMap();
            Map<EplStudentWP2GTypeSlot, EplStudentWP2GTypeSlot> studentWP2GTypeSlotMap = Maps.newHashMap();
            Map<EplOrgUnitSummary, EplOrgUnitSummary> orgUnitSummaryMap = Maps.newHashMap();
            Map<EplEduGroupTimeItem, EplEduGroupTimeItem> eduGroupTimeItemMap = Maps.newHashMap();
            Map<EplSimpleTimeItem, EplSimpleTimeItem> simpleTimeItemMap = Maps.newHashMap();
            Map<EplPlannedPps, EplPlannedPps> plannedPpsMap = Maps.newHashMap();

            Date currentDate = new Date();

            newSummary.setTitle(title);
            newSummary.setArchival(backup);
            newSummary.setEppYear(summary.getEppYear());
            newSummary.setCreationDate(currentDate);
            session.save(newSummary);

            for (EplGroup group : groups)
            {
                EplGroup newGroup = new EplGroup();
                newGroup.update(group);
                newGroup.setSummary(newSummary);
                session.save(newGroup);
                groupMap.put(group, newGroup);
            }

            for (EplStudent student : students)
            {
                EplStudent newStudent = new EplStudent();
                newStudent.update(student);
                newStudent.setGroup(groupMap.get(student.getGroup()));
                session.save(newStudent);
                studentMap.put(student, newStudent);
            }

            for (EplEduGroup eduGroup : eduGroups)
            {
                EplEduGroup newEduGroup = new EplEduGroup();
                newEduGroup.update(eduGroup);
                newEduGroup.setSummary(newSummary);
                session.save(newEduGroup);
                eduGroupMap.put(eduGroup, newEduGroup);
            }

            for (EplStudentWPSlot studentWPSlot : studentWPSlots)
            {
                EplStudentWPSlot newStudentWPSlot = new EplStudentWPSlot();
                newStudentWPSlot.update(studentWPSlot);
                newStudentWPSlot.setStudent(studentMap.get(studentWPSlot.getStudent()));
                session.save(newStudentWPSlot);
                studentWPSlotMap.put(studentWPSlot, newStudentWPSlot);
            }

            for (EplStudentWP2GTypeSlot studentWP2GTypeSlot : studentWP2GTypeSlots)
            {
                EplStudentWP2GTypeSlot newStudentWP2GTypeSlot = new EplStudentWP2GTypeSlot();
                newStudentWP2GTypeSlot.update(studentWP2GTypeSlot);
                newStudentWP2GTypeSlot.setStudentWPSlot(studentWPSlotMap.get(studentWP2GTypeSlot.getStudentWPSlot()));
                session.save(newStudentWP2GTypeSlot);
                studentWP2GTypeSlotMap.put(studentWP2GTypeSlot, newStudentWP2GTypeSlot);
            }

            for (EplEduGroupRow eduGroupRow : eduGroupRows)
            {
                EplEduGroupRow newEduGroupRow = new EplEduGroupRow();
                newEduGroupRow.update(eduGroupRow);
                newEduGroupRow.setStudentWP2GTypeSlot(studentWP2GTypeSlotMap.get(eduGroupRow.getStudentWP2GTypeSlot()));
                newEduGroupRow.setGroup(eduGroupMap.get(eduGroupRow.getGroup()));
                session.save(newEduGroupRow);
            }

            for (EplStudent2WorkPlan student2WorkPlan : student2WorkPlans)
            {
                EplStudent2WorkPlan newStudent2WorkPlan = new EplStudent2WorkPlan();
                newStudent2WorkPlan.update(student2WorkPlan);
                newStudent2WorkPlan.setStudent(studentMap.get(student2WorkPlan.getStudent()));
                session.save(newStudent2WorkPlan);
            }

            EppState ouState = getByCode(EppState.class, EppState.STATE_FORMATIVE);

            for (EplOrgUnitSummary orgUnitSummary : orgUnitSummaries)
            {
                EplOrgUnitSummary newOrgUnitSummary = new EplOrgUnitSummary();
                newOrgUnitSummary.update(orgUnitSummary);
                newOrgUnitSummary.setStudentSummary(newSummary);
                newOrgUnitSummary.setCreationDate(currentDate);
                if (!backup)
                    newOrgUnitSummary.setState(ouState);
                session.save(newOrgUnitSummary);
                orgUnitSummaryMap.put(orgUnitSummary, newOrgUnitSummary);
            }

            for (EplEduGroupTimeItem timeItem : eduGroupTimeItems)
            {
                if (timeItem instanceof EplTransferOrgUnitEduGroupTimeItem)
                {
                    EplTransferOrgUnitEduGroupTimeItem transferTimeItem = (EplTransferOrgUnitEduGroupTimeItem) timeItem;
                    EplTransferOrgUnitEduGroupTimeItem newTransferTimeItem = new EplTransferOrgUnitEduGroupTimeItem();
                    newTransferTimeItem.update(transferTimeItem);
                    newTransferTimeItem.setTransferredFrom(orgUnitSummaryMap.get(transferTimeItem.getTransferredFrom()));
                    newTransferTimeItem.setSummary(orgUnitSummaryMap.get(timeItem.getSummary()));
                    newTransferTimeItem.setEduGroup(eduGroupMap.get(timeItem.getEduGroup()));
                    newTransferTimeItem.setCreationDate(currentDate);
                    session.save(newTransferTimeItem);
                    eduGroupTimeItemMap.put(timeItem, newTransferTimeItem);
                }
                else
                {
                    EplEduGroupTimeItem newTimeItem = new EplEduGroupTimeItem();
                    newTimeItem.update(timeItem);
                    newTimeItem.setSummary(orgUnitSummaryMap.get(timeItem.getSummary()));
                    newTimeItem.setEduGroup(eduGroupMap.get(timeItem.getEduGroup()));
                    newTimeItem.setCreationDate(currentDate);
                    session.save(newTimeItem);
                    eduGroupTimeItemMap.put(timeItem, newTimeItem);
                }
            }

            for (EplSimpleTimeItem simpleTimeItem : simpleTimeItems)
            {
                EplSimpleTimeItem newSimpleTimeItem = new EplSimpleTimeItem();
                newSimpleTimeItem.update(simpleTimeItem);
                newSimpleTimeItem.setSummary(orgUnitSummaryMap.get(simpleTimeItem.getSummary()));
                newSimpleTimeItem.setCreationDate(currentDate);
                session.save(newSimpleTimeItem);
                simpleTimeItemMap.put(simpleTimeItem, newSimpleTimeItem);
            }

            for (EplPlannedPps plannedPps : plannedPpses)
            {
                EplPlannedPps newPlannedPps = new EplPlannedPps();
                newPlannedPps.update(plannedPps);
                newPlannedPps.setOrgUnitSummary(orgUnitSummaryMap.get(plannedPps.getOrgUnitSummary()));
                session.save(newPlannedPps);
                plannedPpsMap.put(plannedPps, newPlannedPps);
            }

            for (EplPlannedPpsTimeItem plannedPpsTimeItem : plannedPpsTimeItems)
            {
                EplPlannedPpsTimeItem newPlannedPpsTimeItem = new EplPlannedPpsTimeItem();
                newPlannedPpsTimeItem.update(plannedPpsTimeItem);
                newPlannedPpsTimeItem.setPps(plannedPpsMap.get(plannedPpsTimeItem.getPps()));

                EplTimeItem timeItem = plannedPpsTimeItem.getTimeItem() instanceof EplEduGroupTimeItem
                    ? eduGroupTimeItemMap.get(plannedPpsTimeItem.getTimeItem())
                    : (plannedPpsTimeItem.getTimeItem() instanceof EplSimpleTimeItem
                    ? simpleTimeItemMap.get(plannedPpsTimeItem.getTimeItem())
                    : null);
                newPlannedPpsTimeItem.setTimeItem(timeItem);

                session.save(newPlannedPpsTimeItem);
            }

            session.flush();
        }
        finally
        {
            final SectionDebugItem end = Debug.end();
            if (end != null && logger.isInfoEnabled())
                logger.info(end.toFullString());

            // включаем системное логирование
            eventLock.release();
        }

        CoreServices.eventService().fireEvent(new InsertEntityEvent("Создание объекта", newSummary));
        return newSummary;
    }

    @Override
    public EppYearEducationProcess getNextAfterCurrentYearEducationProcess(EducationYear eduYear)
    {
        if(null == eduYear)
        {
            return new DQLSelectBuilder().fromEntity(EppYearEducationProcess.class, "p")
                    .order(property("p", EppYearEducationProcess.educationYear().intValue()))
                    .createStatement(getSession()).uniqueResult();
        }
        return get(EppYearEducationProcess.class, EppYearEducationProcess.educationYear().intValue(), eduYear.getIntValue() + 1);
    }

    @Override
    public List<String> getDisciplinesByGroupIds(Collection<Long> groupIds)
    {
        final Set<EppRegistryElementPart> registryElementParts = Sets.newHashSet();
        BatchUtils.execute(groupIds, 100, elements -> registryElementParts.addAll(createStatement(new DQLSelectBuilder().fromEntity(EplEduGroupRow.class, "gr")
                .column(property("gr", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().activityElementPart()))
                .where(in(property("gr", EplEduGroupRow.group().id()), elements))
                .distinct()
        ).<EppRegistryElementPart>list()));

        return registryElementParts.stream()
                .map(EppRegistryElementPart::getTitleWithNumber)
                .sorted(CommonCollator.RUSSIAN_COLLATOR)
                .collect(Collectors.toList());
    }

    @Override
    public List<DataWrapper> getSplitTypeList(Collection<Long> eplGroupIds)
    {
        List<DataWrapper> resultList = Lists.newArrayList();
        if (CollectionUtils.isEmpty(eplGroupIds)) return resultList;

        Map<Long, Set<Long>> disciplineMap = Maps.newHashMap();
        Map<Long, Set<Long>> eouMap = Maps.newHashMap();
        Map<Long, Set<Long>> compSourceMap = Maps.newHashMap();
        Map<Long, Set<Long>> eplGroupMap = Maps.newHashMap();
        Map<Long, Set<Long>> slotMap = Maps.newHashMap();
        Map<Long, Set<Long>> splitElementMap = Maps.newHashMap();
        Map<Long, Set<Long>> splitVariantMap = Maps.newHashMap();

        new DQLSelectBuilder().fromEntity(EplEduGroupRow.class, "gr")
                .column(property("gr", EplEduGroupRow.group().id()))
                .column(property("gtSlot", EplStudentWP2GTypeSlot.studentWPSlot().activityElementPart().id()))
                .column(property("gtSlot", EplStudentWP2GTypeSlot.studentWPSlot().student().educationOrgUnit().id()))
                .column(property("gtSlot", EplStudentWP2GTypeSlot.studentWPSlot().student().compensationSource().id()))
                .column(property("gtSlot", EplStudentWP2GTypeSlot.studentWPSlot().student().group().id()))
                .column(property("gtSlot", EplStudentWP2GTypeSlot.id()))
                .column(property("split"))
                .joinPath(DQLJoinType.inner, EplEduGroupRow.studentWP2GTypeSlot().fromAlias("gr"), "gtSlot")
                .joinPath(DQLJoinType.left, EplStudentWP2GTypeSlot.splitElement().fromAlias("gtSlot"), "split")
                .joinPath(DQLJoinType.left, EppEduGroupSplitBaseElement.splitVariant().fromAlias("split"), "splitVariant")
                .where(in(property("gr", EplEduGroupRow.group().id()), eplGroupIds))
                .createStatement(getSession()).<Object[]>list()
                .forEach(row -> {
                    Long groupId = (Long) row[0];
                    SafeMap.safeGet(disciplineMap, groupId, HashSet.class).add((Long) row[1]);
                    SafeMap.safeGet(eouMap, groupId, HashSet.class).add((Long) row[2]);
                    SafeMap.safeGet(compSourceMap, groupId, HashSet.class).add((Long) row[3]);
                    SafeMap.safeGet(eplGroupMap, groupId, HashSet.class).add((Long) row[4]);
                    SafeMap.safeGet(slotMap, groupId, HashSet.class).add((Long) row[5]);

                    EppEduGroupSplitBaseElement splitElement = (EppEduGroupSplitBaseElement) row[6];
                    SafeMap.safeGet(splitElementMap, groupId, HashSet.class).add(splitElement == null ? null : splitElement.getId());
                    SafeMap.safeGet(splitVariantMap, groupId, HashSet.class).add(splitElement == null ? null : splitElement.getSplitVariant().getId());
                });

        PairKey<Integer, Integer> boundary = getEduGroupRowBoundaryCount(eplGroupIds);

        List<Long> splitVariantList = new DQLSelectBuilder().fromEntity(EplEduGroupRow.class, "row")
                .column(property("spEl", EppEduGroupSplitBaseElement.splitVariant().id())).distinct()
                .joinPath(DQLJoinType.left, EplEduGroupRow.studentWP2GTypeSlot().splitElement().fromAlias("row"), "spEl")
                .where(in(property("row", EplEduGroupRow.group()), eplGroupIds))
                .createStatement(getSession()).list();

        boolean hasRowWhereNotEqualsRegElPart = existsEntity(
                new DQLSelectBuilder()
                        .fromEntity(EplEduGroupRow.class, "r")
                        .joinPath(DQLJoinType.inner, EplEduGroupRow.group().fromAlias("r"), "g")
                        .where(in(property("g.id"), eplGroupIds))
                        .where(ne(property("r", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().activityElementPart()), property("g", EplEduGroup.registryElementPart())))
                        .column(property("r.id"))
                        .buildQuery()
        );
        boolean hasCount = boundary != null && boundary.getSecond() > 1;
        boolean hasSomeSplit = splitVariantList.size() > 1;

        long index = 0;
        for (EplEduGroupSplitType splitType : EplEduGroupSplitType.values())
        {
            switch (splitType)
            {
                case BY_DISCIPLINE:
                    if (!isHasValuesByGroupIds(disciplineMap)) continue;
                    break;
                case BY_EDUCATION_ORGUNIT:
                    if (!isHasValuesByGroupIds(eouMap)) continue;
                    break;
                case BY_COMPENSATION_SOURCE:
                    if (!isHasValuesByGroupIds(compSourceMap)) continue;
                    break;
                case BY_GROUP:
                    if (!isHasValuesByGroupIds(eplGroupMap)) continue;
                    break;
                case BY_ROW:
                    if (!isHasValuesByGroupIds(slotMap) || !hasRowWhereNotEqualsRegElPart) continue;
                    break;
                case BY_COUNT:
                    if (!hasCount) continue;
                    break;
                case BY_SPLIT_VARIANT:
                    if (!isHasValuesByGroupIds(splitVariantMap)) continue;
                    break;
                case BY_SPLIT_ELEMENT:
                    if (!isHasValuesByGroupIds(splitElementMap) || !hasSomeSplit) continue;
                    break;
                case BY_MAX_COUNT:
                    if (!hasCount) continue;
                    break;
                default:
                    throw new IllegalArgumentException();
            }
            index = splitType.ordinal();
            resultList.add(new DataWrapper(index, splitType.getTitle(), splitType));
        }
        if (isHasValuesByGroupIds(splitElementMap) && !hasSomeSplit && null != splitVariantList.get(0))
        {
            EppEduGroupSplitVariant splitVariant = getNotNull(splitVariantList.get(0));
            resultList.add(new DataWrapper(++index, splitVariant.getTitle(), splitVariant));
        }
        return resultList;
    }

    private boolean isHasValuesByGroupIds(Map<Long, Set<Long>> groupMap)
    {
        Set<Long> set = groupMap.values()
                .stream()
                .filter(row -> row.size() > 1)
                .findAny().orElse(null);
        return set != null;
    }

    @Override
    public PairKey<Integer, Integer> getEduGroupRowBoundaryCount(Collection<Long> eplGroupIds)
    {
        DQLSelectBuilder subDql = new DQLSelectBuilder()
                .fromEntity(EplEduGroupRow.class, "row")
                .column(sum(property("row", EplEduGroupRow.count())), "sum_c")
                .group(property("row", EplEduGroupRow.group()))
                .where(in(property("row", EplEduGroupRow.group()), eplGroupIds));

        List<Object[]> items = new DQLSelectBuilder().fromDataSource(subDql.buildQuery(), "e")
                .column(min(property("e", "sum_c")))
                .column(max(property("e", "sum_c")))
                .createStatement(getSession()).list();

        if (items.size() > 1) throw new IllegalStateException();
        if (items.size() < 1) return null;

        Number minSize = (Number) items.get(0)[0];
        Number maxSize = (Number) items.get(0)[1];

        return minSize == null || maxSize == null ? null :
                PairKey.create(minSize.intValue(), maxSize.intValue());
    }

    @Override
    public void doMerge(EplStudentSummary summaryBase, EplStudentSummary summaryMerge)
    {
        try (EventListenerLocker.Lock ignored = CommonbaseImmutableNaturalIdListener.IMMUTABLE_NATURAL_ID_LOCKER.lock(EventListenerLocker.noopHandler()))
        {
            Object[] row = new DQLSelectBuilder().fromEntity(EplPlannedPps.class, "p").top(1)
                    .column(property("ous", EplOrgUnitSummary.studentSummary().id()))
                    .column(property("ous", EplOrgUnitSummary.orgUnit().id()))
                    .joinPath(DQLJoinType.left, EplPlannedPps.ppsEntry().fromAlias("p"), "pe")
                    .joinPath(DQLJoinType.inner, EplPlannedPps.orgUnitSummary().fromAlias("p"), "ous")
                    .where(or(
                            eq(property("ous", EplOrgUnitSummary.studentSummary().id()), value(summaryBase)),
                            eq(property("ous", EplOrgUnitSummary.studentSummary().id()), value(summaryMerge))
                    ))
                    .group(property("ous", EplOrgUnitSummary.studentSummary().id()))
                    .group(property("ous", EplOrgUnitSummary.orgUnit().id()))
                    .group(property("pe.id"))
                    .group(property("p", EplPlannedPps.type().id()))
                    .group(property("p", EplPlannedPps.post().id()))
                    .group(property("p", EplPlannedPps.vacancy()))
                    .having(gt(count(property("p.id")), value(1)))
                    .createStatement(getSession()).uniqueResult();

            if (null != row)
            {
                EplStudentSummary summary = getNotNull((Long) row[0]);
                OrgUnit ou = getNotNull((Long) row[1]);
                throw new ApplicationException("Нельзя объединить сводки, т.к. в сводке «" + summary.getTitle() + "» в расчете на подразделении «" + ou.getFullTitle() + "» добавлены планируемые ППС, отличающиеся только долей ставки.");
            }

            new DQLUpdateBuilder(EplEduGroup.class)
                .set(EplEduGroup.summary().s(), value(summaryBase))
                .where(eq(property(EplEduGroup.summary()), value(summaryMerge)))
                .createStatement(getSession()).execute();

            new DQLUpdateBuilder(EplGroup.class)
                .set(EplGroup.summary().s(), value(summaryBase))
                .where(eq(property(EplGroup.summary()), value(summaryMerge)))
                .createStatement(getSession()).execute();

            DQLSelectBuilder eqSummaryBuilder = new DQLSelectBuilder()
                    .fromEntity(EplOrgUnitSummary.class, "bo")
                    .where(eq(property("bo", EplOrgUnitSummary.studentSummary()), value(summaryBase)))
                    .fromEntity(EplOrgUnitSummary.class, "mo")
                    .where(eq(property("mo", EplOrgUnitSummary.studentSummary()), value(summaryMerge)))
                    .where(eq(property("bo", EplOrgUnitSummary.orgUnit()), property("mo", EplOrgUnitSummary.orgUnit())));

            // меняем сводку контингента 2й сводки для тех подразделений, которых нет в 1й сводке
            new DQLUpdateBuilder(EplOrgUnitSummary.class)
                    .set(EplOrgUnitSummary.studentSummary().s(), value(summaryBase))
                    .where(eq(property(EplOrgUnitSummary.studentSummary()), value(summaryMerge)))
                    .where(notIn(property(EplOrgUnitSummary.orgUnit().id()), eqSummaryBuilder.column(property("mo", EplOrgUnitSummary.orgUnit().id())).buildQuery()))
                    .createStatement(getSession()).execute();

            new DQLUpdateBuilder(EplTimeItem.class)
                .fromDataSource(eqSummaryBuilder
                    .column("bo.id", "bs")
                    .column("mo.id", "ms")
                    .buildQuery(), "ds")
                .set(EplTimeItem.summary().s(), property("ds.bs"))
                .where(eq(property(EplTimeItem.summary()), property("ds.ms")))
                .where(notIn(EplTimeItem.id(),
                             new DQLSelectBuilder().fromEntity(EplTransferOrgUnitEduGroupTimeItem.class, "t").column(property("t.id"))
                                     .where(eq(property("t", EplTransferOrgUnitEduGroupTimeItem.transferredFrom().studentSummary()), value(summaryMerge)))
                                     .buildQuery()
                ))
                .createStatement(getSession()).execute();

            // находим такие переведенные часы, которые по прежнему ссылаются на подразделения в рамках своего расчета 2й сводки
            // для них мы должны найти аналогичные подразделения в новом расчете нагрузки 1й сводки
            {
                DQLSelectBuilder base2TransferOuSummaryBuilder = new DQLSelectBuilder()
                        .fromEntity(EplOrgUnitSummary.class, "bo").distinct()
                        .column("bo.id", "bs")
                        .column("ds.mt.id", "mt")
                        .fromDataSource(
                                new DQLSelectBuilder()
                                        .column(property("mo", EplTransferOrgUnitEduGroupTimeItem.transferredFrom()), "mt")
                                        .fromEntity(EplTransferOrgUnitEduGroupTimeItem.class, "mo")
                                        .where(eq(property("mo", EplTransferOrgUnitEduGroupTimeItem.transferredFrom().studentSummary()), value(summaryMerge)))
                                        .buildQuery(), "ds")
                        .where(eq(property("bo", EplOrgUnitSummary.studentSummary()), value(summaryBase)))
                        .where(ne(property("bo", EplOrgUnitSummary.studentSummary()), property("ds.mt", EplOrgUnitSummary.studentSummary())))
                        .where(eq(property("bo", EplOrgUnitSummary.orgUnit()), property("ds.mt", EplOrgUnitSummary.orgUnit())));

                // для часов, ссылающихся на подразделения, которые есть в 1й сводке
                new DQLUpdateBuilder(EplTransferOrgUnitEduGroupTimeItem.class)
                        .fromDataSource(eqSummaryBuilder.column("bo.id", "bs").column("mo.id", "ms").buildQuery(), "summ")
                        .fromDataSource(base2TransferOuSummaryBuilder.buildQuery(), "tds")
                        .set(EplTransferOrgUnitEduGroupTimeItem.transferredFrom().s(), property("tds.bs"))
                        .set(EplTransferOrgUnitEduGroupTimeItem.summary().s(), property("summ.bs"))
                        .where(eq(property(EplTransferOrgUnitEduGroupTimeItem.transferredFrom()), property("tds.mt")))
                        .where(eq(property(EplTransferOrgUnitEduGroupTimeItem.summary()), property("summ.ms")))
                        .where(eq(property(EplTransferOrgUnitEduGroupTimeItem.transferredFrom().studentSummary()), value(summaryMerge)))
                        .createStatement(getSession()).execute();

                // для оставшихся (новых связи, новые подразделения)
                new DQLUpdateBuilder(EplTransferOrgUnitEduGroupTimeItem.class)
                        .fromDataSource(base2TransferOuSummaryBuilder.buildQuery(), "tds")
                        .set(EplTransferOrgUnitEduGroupTimeItem.transferredFrom().s(), property("tds.bs"))
                        .where(eq(property(EplTransferOrgUnitEduGroupTimeItem.transferredFrom()), property("tds.mt")))
                        .where(eq(property(EplTransferOrgUnitEduGroupTimeItem.transferredFrom().studentSummary()), value(summaryMerge)))
                        .createStatement(getSession()).execute();
            }

            // суммируем ставку у одинаковых ППС, разных сводок
            new DQLUpdateBuilder(EplPlannedPps.class)
                .fromDataSource(new DQLSelectBuilder()
                    .fromEntity(EplPlannedPps.class, "bp")
                    .where(eq(property("bp", EplPlannedPps.orgUnitSummary().studentSummary()), value(summaryBase)))
                    .fromEntity(EplPlannedPps.class, "mp")
                    .where(eq(property("mp", EplPlannedPps.orgUnitSummary().studentSummary()), value(summaryMerge)))
                    .where(eq(property("bp", EplPlannedPps.orgUnitSummary().orgUnit()), property("mp", EplPlannedPps.orgUnitSummary().orgUnit())))
                    .where(eqNullSafe(property("bp", EplPlannedPps.ppsEntry()), property("mp", EplPlannedPps.ppsEntry())))
                    .where(eqNullSafe(property("bp", EplPlannedPps.type()), property("mp", EplPlannedPps.type())))
                    .where(eqNullSafe(property("bp", EplPlannedPps.post()), property("mp", EplPlannedPps.post())))
                    .where(eqNullSafe(property("bp", EplPlannedPps.vacancy()), property("mp", EplPlannedPps.vacancy())))
                    .column("bp.id", "bpId")
                    .column("mp.id", "mpId")
                    .column(plus(property("bp", EplPlannedPps.staffRateAsLong()), property("mp", EplPlannedPps.staffRateAsLong())), "sum")
                    .buildQuery(), "ds")
                .set(EplPlannedPps.staffRateAsLong().s(), property("ds.sum"))
                .where(or(
                        eq(property(EplPlannedPps.id()), property("ds.bpId")),
                        eq(property(EplPlannedPps.id()), property("ds.mpId"))
                ))
                .createStatement(getSession()).execute();

            new DQLUpdateBuilder(EplPlannedPpsTimeItem.class)
                .fromDataSource(new DQLSelectBuilder()
                    .fromEntity(EplPlannedPps.class, "bp")
                    .where(eq(property("bp", EplPlannedPps.orgUnitSummary().studentSummary()), value(summaryBase)))
                    .fromEntity(EplPlannedPps.class, "mp")
                    .where(eq(property("mp", EplPlannedPps.orgUnitSummary().studentSummary()), value(summaryMerge)))
                    .where(eq(property("bp", EplPlannedPps.orgUnitSummary().orgUnit()), property("mp", EplPlannedPps.orgUnitSummary().orgUnit())))
                    .where(eqNullSafe(property("bp", EplPlannedPps.ppsEntry()), property("mp", EplPlannedPps.ppsEntry())))
                    .where(eqNullSafe(property("bp", EplPlannedPps.type()), property("mp", EplPlannedPps.type())))
                    .where(eqNullSafe(property("bp", EplPlannedPps.post()), property("mp", EplPlannedPps.post())))
                    .where(eqNullSafe(property("bp", EplPlannedPps.vacancy()), property("mp", EplPlannedPps.vacancy())))
                    .where(eqNullSafe(property("bp", EplPlannedPps.staffRateAsLong()), property("mp", EplPlannedPps.staffRateAsLong())))
                    .column("bp.id", "bpps")
                    .column("mp.id", "mpps")
                    .buildQuery(), "ds")
                .set(EplPlannedPpsTimeItem.pps().s(), property("ds.bpps"))
                .where(eq(property(EplPlannedPpsTimeItem.pps()), property("ds.mpps")))
                .createStatement(getSession()).execute();

            DQLSelectBuilder foundOuSummaryBuilder = new DQLSelectBuilder()
                    .column(property("mp", EplPlannedPps.orgUnitSummary().orgUnit().id())).distinct()
                    .fromEntity(EplPlannedPps.class, "bp")
                    .where(eq(property("bp", EplPlannedPps.orgUnitSummary().studentSummary()), value(summaryBase)))
                    .fromEntity(EplPlannedPps.class, "mp")
                    .where(eq(property("mp", EplPlannedPps.orgUnitSummary().studentSummary()), value(summaryMerge)))
                    .where(eq(property("bp", EplPlannedPps.orgUnitSummary().orgUnit()), property("mp", EplPlannedPps.orgUnitSummary().orgUnit())));

            // находим такие ППС, которые по прежнему остаются во 2й сводке,
            // для них мы должны найти аналогичные подразделения в новом расчете нагрузки 1й сводки
            new DQLUpdateBuilder(EplPlannedPps.class)
                    .fromDataSource(
                        new DQLSelectBuilder()
                            .fromEntity(EplOrgUnitSummary.class, "bo").distinct()
                            .column("bo.id", "bs")
                            .column("pds.ps.id", "ps")
                            .fromDataSource(
                                    new DQLSelectBuilder().fromEntity(EplPlannedPps.class, "p")
                                            .column(property("p", EplPlannedPps.orgUnitSummary()), "ps")
                                            .where(eq(property("p", EplPlannedPps.orgUnitSummary().studentSummary()), value(summaryMerge)))
                                            .where(notIn(property("p", EplPlannedPps.orgUnitSummary().orgUnit().id()), foundOuSummaryBuilder.buildQuery()))
                                            .buildQuery(), "pds")
                            .where(eq(property("bo", EplOrgUnitSummary.studentSummary()), value(summaryBase)))
                            .where(ne(property("bo", EplOrgUnitSummary.studentSummary()), property("pds.ps", EplOrgUnitSummary.studentSummary())))
                            .where(eq(property("bo", EplOrgUnitSummary.orgUnit()), property("pds.ps", EplOrgUnitSummary.orgUnit())))
                            .buildQuery(), "pds")
                    .set(EplPlannedPps.orgUnitSummary().s(), property("pds.bs"))
                    .where(eq(property(EplPlannedPps.orgUnitSummary()), property("pds.ps")))
                    .createStatement(getSession()).execute();

            delete(summaryMerge);
        }
    }

    @Override
    public int doRefreshSummary(EplStudentSummary summary, boolean fillFirstCourse)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), "eplChangeSummary-" + summary.getId());

        int currEduYear = EducationYearManager.instance().getCurrentRequired().getIntValue();
        int eppYear = summary.getEppYear().getEducationYear().getIntValue();
        int yearDiff = eppYear - currEduYear;

        if (yearDiff < 0)
            throw new ApplicationException("Нельзя обновить сводку, т.к. текущий учебный год больше учебного года сводки.");
        if (yearDiff > 1)
            throw new ApplicationException("Нельзя обновить сводку, т.к. разница между учебным годом сводки и текущим учебным годом превышает 1 год.");

        GroupTitleAlgorithm currentAlg = getUnique(GroupTitleAlgorithm.class, GroupTitleAlgorithm.current().s(), true);
        boolean useCorrectGroupTitle = yearDiff > 0 && null != currentAlg && ("groupTitleAlgorithmSample3".equals(currentAlg.getDaoName()) || "groupTitleAlgorithmSample5".equals(currentAlg.getDaoName()));

        int missEplStudents = doRefreshEduGroupsAndStudentData(summary, fillFirstCourse, useCorrectGroupTitle, yearDiff);
        doRefreshWpRel(summary, fillFirstCourse, useCorrectGroupTitle, yearDiff);

        return missEplStudents;
    }

    @Override
    public int doRefreshEduGroupsAndStudentData(EplStudentSummary summary, boolean fillFirstCourse, boolean useCorrectGroupTitle, int yearDiff)
    {
        // выставляем число студентов для план. студента
        new DQLUpdateBuilder(EplStudent.class)
                .fromDataSource(getStudent2EpvFullBuilder(fillFirstCourse, useCorrectGroupTitle, yearDiff).buildQuery(), "ds")
                .set(EplStudent.count().s(), property("ds.count"))

                .where(eq(property(EplStudent.group().summary().id()), value(summary.getId())))
                .where(eq(property(EplStudent.developGrid().id()), property("ds.grid")))
                .where(eq(property(EplStudent.educationOrgUnit().id()), property("ds.eou")))
                .where(eq(property(EplStudent.compensationSource().code()), property("ds.ct")))
                .where(eq(property(EplStudent.group().title()), property("ds.group")))
                .where(eq(property(EplStudent.group().course().intValue()), property("ds.course")))
                .createStatement(getSession()).execute();

        // ставим 0 тем, кого нет в контингенте
        new DQLUpdateBuilder(EplStudent.class)
                .fromDataSource(
                    new DQLSelectBuilder()
                        .fromEntity(EplStudent.class, "s").where(eq(property("s", EplStudent.group().summary().id()), value(summary.getId())))
                        .column(property("s.id"), "id")
                        .column(property("s", EplStudent.group().title()), "group")
                        .column(property("s", EplStudent.group().course().intValue()), "course")
                        .column(property("s", EplStudent.educationOrgUnit().id()), "eou")
                        .column(property("s", EplStudent.compensationSource().code()), "ct")
                        .column(property("s", EplStudent.developGrid().id()), "grid")
                        .buildQuery(), "o")
                .joinDataSource("o", DQLJoinType.left, getStudent2EpvFullBuilder(fillFirstCourse, useCorrectGroupTitle, yearDiff).buildQuery(), "ds", and(
                        eq(property("o.grid"), property("ds.grid")),
                        eq(property("o.eou"), property("ds.eou")),
                        eq(property("o.ct"), property("ds.ct")),
                        eq(property("o.group"), property("ds.group")),
                        eq(property("o.course"), property("ds.course"))
                ))
                .where(fillFirstCourse ? null : gt(property(EplStudent.group().course().intValue()), value(yearDiff)))
                .where(eq(property(EplStudent.group().summary().id()), value(summary.getId())))
                .where(eq(property("id"), property("o.id")))
                .where(isNull("ds.group"))
                .set(EplStudent.count().s(), value(0))
                .createStatement(getSession()).execute();

        // удаляем план. студентов с нулевым числом студентов
        new DQLDeleteBuilder(EplStudent.class)
                .where(eq(property(EplStudent.count()), value(0)))
                .where(eq(property(EplStudent.group().summary().id()), value(summary.getId())))
                .createStatement(getSession()).execute();

        // удаляем план. группы без план. студентов
        new DQLDeleteBuilder(EplGroup.class)
                .where(notExists(EplStudent.class, EplStudent.group().id().s(), property("id")))
                .where(eq(property(EplGroup.summary().id()), value(summary.getId())))
                .createStatement(getSession()).execute();

        // создаем недостающих план. студентов

        // собираем данные о существующих план. группах - по ключу уникальности
        Map<MultiKey, Long> groupIdMap = Maps.newHashMap();
        new DQLSelectBuilder()
                .fromEntity(EplGroup.class, "g")
                .column(property("g", EplGroup.course().intValue()))
                .column(property("g", EplGroup.title()))
                .column("g.id")
                .where(eq(property("g", EplGroup.summary().id()), value(summary.getId())))
                .createStatement(getSession()).<Object[]>list()
                .forEach(row -> groupIdMap.put(new MultiKey(row[0], row[1]), (Long) row[2]));

        // собираем данные о существующих план. студентах - по ключу уникальности
        Map<MultiKey, Long> studentIdMap = Maps.newHashMap();
        new DQLSelectBuilder()
                .fromEntity(EplStudent.class, "s")
                .column(property("s", EplStudent.group().id()))
                .column(property("s", EplStudent.educationOrgUnit().id()))
                .column(property("s", EplStudent.compensationSource().code()))
                .column(property("s.id"))
                .where(eq(property("s", EplStudent.group().summary().id()), value(summary.getId())))
                .createStatement(getSession()).<Object[]>list()
                .forEach(row -> studentIdMap.put(new MultiKey(row[0], row[1], row[2]), (Long) row[3]));

        Map<String, EplCompensationSource> compSourceMap = Maps.newHashMap();
        getList(EplCompensationSource.class).forEach(s -> compSourceMap.put(s.getCode(), s));

        Map<Long, DevelopGrid> developGridMap = Maps.newHashMap();
        getList(DevelopGrid.class).forEach(dg -> developGridMap.put(dg.getId(), dg));

        Map<Integer, Course> courseMap = Maps.newHashMap();
        getList(Course.class).forEach(c -> courseMap.put(c.getIntValue(), c));

        final short groupEntityCode = EntityRuntime.getMeta(EplGroup.class).getEntityCode();
        final short studentEntityCode = EntityRuntime.getMeta(EplStudent.class).getEntityCode();

        DQLSelectBuilder eplStudentBuilder = new DQLSelectBuilder().fromEntity(EplStudent.class, "s")
                .column(property("s", EplStudent.group().title()), "group")
                .column(property("s", EplStudent.group().course().id()), "course")
                .column(property("s", EplStudent.educationOrgUnit().id()), "eou")
                .column(property("s", EplStudent.compensationSource().code()), "ct")
                .column(property("s", EplStudent.developGrid().id()), "grid")
                .where(eq(property("s", EplStudent.group().summary().id()), value(summary.getId())));

        DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                new DQLSelectBuilder().fromDataSource(getStudent2EpvFullBuilder(fillFirstCourse, useCorrectGroupTitle, yearDiff).buildQuery(), "ds")
                        .joinDataSource("ds", DQLJoinType.left, eplStudentBuilder.buildQuery(), "st", and(
                                eq(property("st.group"), property("ds.group")),
                                eq(property("st.eou"), property("ds.eou")),
                                eq(property("st.ct"), property("ds.ct")),
                                eq(property("st.grid"), property("ds.grid")),
                                eq(property("st.course"), property("ds.course"))
                        ))
                        .where(isNull("st.group"))
                .order(property("ds.course"))
                .order(property("ds.group"))
                .order(property("ds.eou"))
                .order(property("ds.ct"))
                .order(property("ds.grid"))
                .order(property("ds.count"), OrderDirection.desc)
        );

        final int course_col = dql.column(property("ds.course"));
        final int group_col = dql.column(property("ds.group"));
        final int eou_col = dql.column(property("ds.eou"));
        final int ct_col = dql.column(property("ds.ct"));
        final int grid_col = dql.column(property("ds.grid"));
        final int count_col = dql.column(property("ds.count"));

        final MutableInt missed = new MutableInt();
        final Set<MultiKey> studentUniqueKeyCheck = Sets.newHashSet();
        final ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try (PrintWriter p = new PrintWriter(stream))
        {
            Iterables.partition(dql.getDql().createStatement(getSession()).<Object[]>list(), DQL.MAX_VALUES_ROW_NUMBER).forEach(rows -> {
                DQLInsertValuesBuilder studentBuilder = new DQLInsertValuesBuilder(EplStudent.class);
                DQLInsertValuesBuilder groupBuilder = new DQLInsertValuesBuilder(EplGroup.class);

                boolean saveGroups = false;
                boolean saveStudents = false;

                for (Object[] row : rows)
                {
                    EplCompensationSource compSource = compSourceMap.get((String) row[ct_col]);
                    Integer course = (Integer) row[course_col];
                    String groupTitle = (String) row[group_col];
                    Long count = (Long) row[count_col];

                    MultiKey groupKey = new MultiKey(course, groupTitle);
                    Long group = groupIdMap.get(groupKey);
                    if (null == group)
                    {
                        group = EntityIDGenerator.generateNewId(groupEntityCode);
                        groupIdMap.put(groupKey, group);

                        groupBuilder.value(EplGroup.P_ID, group);
                        groupBuilder.value(EplGroup.L_SUMMARY, summary.getId());
                        groupBuilder.value(EplGroup.L_COURSE, courseMap.get(course));
                        groupBuilder.value(EplGroup.P_TITLE, groupTitle);
                        groupBuilder.addBatch();
                        saveGroups = true;
                    }

                    MultiKey studentKey = new MultiKey(group, row[eou_col], row[ct_col]);
                    if (studentUniqueKeyCheck.contains(studentKey))
                    {
                        EducationOrgUnit eou = getNotNull((Long) row[eou_col]);
                        DevelopGrid dg = developGridMap.get((Long) row[grid_col]);

                        p.println("В группе " + groupTitle +
                                " объединены студенты с разными сетками, но одинаковыми источником возмещения затрат (" + compSource.getTitle() + ")" +
                                " и параметрами обучения (" + eou.getProgramSubjectShortExtendedTitle() + ")." +
                                " Пропущено студентов с сеткой «" + dg.getTitle() + "»: " + count + ".");
                        missed.add(count);
                        continue;
                    }
                    else
                        studentUniqueKeyCheck.add(studentKey);

                    Long student = studentIdMap.get(studentKey);
                    if (null == student)
                    {
                        studentBuilder.value(EplStudent.P_ID, EntityIDGenerator.generateNewId(studentEntityCode));
                        studentBuilder.value(EplStudent.L_GROUP, group);
                        studentBuilder.value(EplStudent.L_EDUCATION_ORG_UNIT, row[eou_col]);
                        studentBuilder.value(EplStudent.L_COMPENSATION_SOURCE, compSource.getId());
                        studentBuilder.value(EplStudent.L_DEVELOP_GRID, row[grid_col]);
                        studentBuilder.value(EplStudent.P_COUNT, count);
                        studentBuilder.value(EplStudent.P_NO_WP, false);
                        studentBuilder.value(EplStudent.P_ERROR_COUNT_WITH_WP, false);
                        studentBuilder.addBatch();
                        saveStudents = true;
                    }
                }

                if (saveGroups) groupBuilder.createStatement(getSession()).execute();
                if (saveStudents) studentBuilder.createStatement(getSession()).execute();
            });
        }

        if (stream.toByteArray().length != 0)
        {
            DatabaseFile zipLog = new DatabaseFile();
            zipLog.setContent(stream.toByteArray());
            save(zipLog);

            summary.setZipLog(zipLog);
            update(summary);
        }
        return missed.intValue();
    }

    private DQLSelectBuilder getStudent2EpvFullBuilder(boolean fillFirstCourse, boolean useCorrectGroupTitle, int yearDiff)
    {
        DQLSelectBuilder builder = getStudent2EpvBuilder(false, useCorrectGroupTitle, yearDiff);
        if (fillFirstCourse)
            builder.unionAll(getStudent2EpvBuilder(true, false, yearDiff).buildSelectRule());
        return builder;
    }

    private DQLSelectBuilder getStudent2EpvBuilder(boolean fillFirstCourse, boolean useCorrectGroupTitle, int yearDiff)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppStudent2EduPlanVersion.class, "r")
                .where(isNull(property("r", EppStudent2EduPlanVersion.removalDate())))
                .joinPath(DQLJoinType.inner, EppStudent2EduPlanVersion.student().fromAlias("r"), "s")
                .joinPath(DQLJoinType.left, Student.group().fromAlias("s"), "g")
                .joinPath(DQLJoinType.left, Group.course().fromAlias("g"), "course")
                .joinPath(DQLJoinType.inner, EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().developGrid().fromAlias("r"), "dg")
                .where(eq(property("s", Student.archival()), value(Boolean.FALSE)))
                .where(eq(property("s", Student.status().active()), value(Boolean.TRUE)))

                .where(exists(getDevelopGridTermBuilder("course", fillFirstCourse, yearDiff).buildQuery()))

                .group(property("dg.id"))
                .group(correctCourse("course", "s", fillFirstCourse, yearDiff))
                .group(property("s", Student.educationOrgUnit().id()))
                .group(property("s", Student.compensationType().code()))
                .group(correctGroupTitle("g", useCorrectGroupTitle, yearDiff))

                .column(property("dg.id"), "grid")
                .column(correctCourse("course", "s", fillFirstCourse, yearDiff), "course")
                .column(property("s", Student.educationOrgUnit().id()), "eou")
                .column(property("s", Student.compensationType().code()), "ct")
                .column(correctGroupTitle("g", useCorrectGroupTitle, yearDiff), "group")

                .column(count(property("s.id")), "count");

        if (fillFirstCourse)
        {
            builder.where(
                    le(caseExpr(isNotNull(property("course")),
                                property("course", Course.intValue()),
                                property("s", Student.course().intValue())),
                       value(yearDiff)
                    ));
        }
        return builder;
    }

    private DQLSelectBuilder getDevelopGridTermBuilder(String courseAlias, boolean fillFirstCourse, int yearDiff)
    {
        return new DQLSelectBuilder()
                .fromEntity(DevelopGridTerm.class, "gt")
                .where(or(
                        isNull(property(courseAlias)),
                        and(
                                eq(property("gt", DevelopGridTerm.developGrid()), property("dg")),
                                eq(property("gt", DevelopGridTerm.course().intValue()),
                                   fillFirstCourse
                                           ? property(courseAlias, Course.intValue())
                                           : plus(property(courseAlias, Course.intValue()), value(yearDiff))
                                ))
                ));
    }

    @Override
    public void doRefreshWpRel(EplStudentSummary summary, boolean fillFirstCourse, boolean useCorrectGroupTitle, int yearDiff)
    {
        // выставляем число студентов в связи с РУП план. студента
        new DQLUpdateBuilder(EplStudent2WorkPlan.class)
                .fromDataSource(getStudent2WpFullBuilder(summary, fillFirstCourse, useCorrectGroupTitle, yearDiff).buildQuery(), "ds")
                .set(EplStudent2WorkPlan.count().s(), property("ds.count"))

                .where(eq(property(EplStudent2WorkPlan.student().group().summary().id()), value(summary.getId())))
                .where(eq(property(EplStudent2WorkPlan.student().developGrid().id()), property("ds.grid")))
                .where(eq(property(EplStudent2WorkPlan.student().educationOrgUnit().id()), property("ds.eou")))
                .where(eq(property(EplStudent2WorkPlan.student().compensationSource().code()), property("ds.ct")))
                .where(eq(property(EplStudent2WorkPlan.student().group().title()), property("ds.group")))
                .where(eq(property(EplStudent2WorkPlan.workPlan().id()), property("ds.wp")))
                .where(eq(property(EplStudent2WorkPlan.student().group().course().intValue()), property("ds.course")))
                .createStatement(getSession()).execute();

        // ставим 0 тем, кого не нашли
        new DQLUpdateBuilder(EplStudent2WorkPlan.class)
                .fromDataSource(
                        new DQLSelectBuilder()
                                .fromEntity(EplStudent2WorkPlan.class, "wp").where(eq(property("wp", EplStudent2WorkPlan.student().group().summary().id()), value(summary.getId())))
                                .column(property("wp.id"), "id")
                                .column(property("wp", EplStudent2WorkPlan.student().group().title()), "group")
                                .column(property("wp", EplStudent2WorkPlan.student().group().course().intValue()), "course")
                                .column(property("wp", EplStudent2WorkPlan.student().educationOrgUnit().id()), "eou")
                                .column(property("wp", EplStudent2WorkPlan.student().compensationSource().code()), "ct")
                                .column(property("wp", EplStudent2WorkPlan.student().developGrid().id()), "grid")
                                .column(property("wp", EplStudent2WorkPlan.workPlan().id()), "wp")
                                .buildQuery(), "o")
                .joinDataSource("o", DQLJoinType.left, getStudent2WpFullBuilder(summary, fillFirstCourse, useCorrectGroupTitle, yearDiff).buildQuery(), "ds", and(
                        eq(property("o.grid"), property("ds.grid")),
                        eq(property("o.eou"), property("ds.eou")),
                        eq(property("o.ct"), property("ds.ct")),
                        eq(property("o.group"), property("ds.group")),
                        eq(property("o.wp"), property("ds.wp")),
                        eq(property("o.course"), property("ds.course"))
                ))
                .where(fillFirstCourse ? null : gt(property(EplStudent2WorkPlan.student().group().course().intValue()), value(yearDiff)))
                .where(eq(property(EplStudent2WorkPlan.student().group().summary().id()), value(summary.getId())))
                .where(eq(property("id"), property("o.id")))
                .where(isNull("ds.group"))
                .set(EplStudent2WorkPlan.count().s(), value(0))
                .createStatement(getSession()).execute();

        // удаляем связи с РУП план. студента с нулевым числом студентов
        new DQLDeleteBuilder(EplStudent2WorkPlan.class)
                .where(eq(property(EplStudent2WorkPlan.count()), value(0)))
                .where(eq(property(EplStudent2WorkPlan.student().group().summary().id()), value(summary.getId())))
                .createStatement(getSession()).execute();

        Map<MultiKey, Long> groupIdMap = Maps.newHashMap();
        new DQLSelectBuilder()
                .fromEntity(EplGroup.class, "g")
                .column(property("g", EplGroup.course().id()))
                .column(property("g", EplGroup.title()))
                .column("g.id")
                .where(eq(property("g", EplGroup.summary().id()), value(summary.getId())))
                .createStatement(getSession()).<Object[]>list()
                .forEach(row -> groupIdMap.put(new MultiKey(row[0], row[1]), (Long) row[2]));

        // собираем данные о существующих РУП для план. академ. групп - по ключу уникальности
        Map<MultiKey, Long> wpIdMap = Maps.newHashMap();
        new DQLSelectBuilder()
                .fromEntity(EplStudent2WorkPlan.class, "wp")
                .column(property("wp", EplStudent2WorkPlan.student().id()))
                .column(property("wp", EplStudent2WorkPlan.workPlan().id()))
                .column(property("wp.id"))
                .where(eq(property("wp", EplStudent2WorkPlan.student().group().summary().id()), value(summary.getId())))
                .createStatement(getSession()).<Object[]>list()
                .forEach(row -> wpIdMap.put(new MultiKey(row[0], row[1]), (Long) row[2]));

        // собираем данные о существующих план. студентах сводки - по ключу уникальности
        Map<StudentKey, Long> eplStudentIds = getSummaryEplStudentIds(summary);

        final Map<String, String> compTypeMap = getCompSourceMap();
        Map<String, EplCompensationSource> compSourceMap = Maps.newHashMap();
        getList(EplCompensationSource.class).forEach(s -> compSourceMap.put(s.getCode(), s));

        final short wpEntityCode = EntityRuntime.getMeta(EplStudent2WorkPlan.class).getEntityCode();

        DQLSelectBuilder eplStudentBuilder = new DQLSelectBuilder().fromEntity(EplStudent2WorkPlan.class, "eplWp")
                .column(property("s", EplStudent.group().title()), "group")
                .column(property("s", EplStudent.group().course().id()), "course")
                .column(property("s", EplStudent.educationOrgUnit().id()), "eou")
                .column(property("s", EplStudent.compensationSource().code()), "ct")
                .column(property("eplWp", EplStudent2WorkPlan.cachedGridTerm().id()), "grid")
                .column(property("eplWp", EplStudent2WorkPlan.workPlan().id()), "wp")
                .joinPath(DQLJoinType.inner, EplStudent2WorkPlan.student().fromAlias("eplWp"), "s")
                .where(eq(property("s", EplStudent.group().summary().id()), value(summary.getId())));

        DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                new DQLSelectBuilder().fromDataSource(getStudent2WpFullBuilder(summary, fillFirstCourse, useCorrectGroupTitle, yearDiff).buildQuery(), "ds")
                        .joinDataSource("ds", DQLJoinType.left, eplStudentBuilder.buildQuery(), "st", and(
                                eq(property("st.group"), property("ds.group")),
                                eq(property("st.eou"), property("ds.eou")),
                                eq(property("st.ct"), property("ds.ct")),
                                eq(property("st.grid"), property("ds.grid")),
                                eq(property("st.wp"), property("ds.wp")),
                                eq(property("st.course"), property("ds.course"))
                        ))
                        .where(isNull("st.group"))
        );

        final int course_col = dql.column(property("ds.course"));
        final int group_col = dql.column(property("ds.group"));
        final int eou_col = dql.column(property("ds.eou"));
        final int ct_col = dql.column(property("ds.ct"));
        final int wp_col = dql.column(property("ds.wp"));
        final int count_col = dql.column(property("ds.count"));
        final int gridTerm_col = dql.column(property("ds.gridTerm"));

        Iterables.partition(dql.getDql().createStatement(getSession()).<Object[]>list(), DQL.MAX_VALUES_ROW_NUMBER).forEach(rows -> {
            DQLInsertValuesBuilder wpBuilder = new DQLInsertValuesBuilder(EplStudent2WorkPlan.class);
            boolean saveWp = false;
            for (Object[] row : rows)
            {
                StudentKey studentKey = new StudentKey((Integer) row[course_col], (String) row[group_col], (Long) row[eou_col], compTypeMap.get((String) row[ct_col]));
                Long student = eplStudentIds.get(studentKey);
                if (null == student) continue;

                MultiKey wpKey = new MultiKey(student, row[wp_col]);
                Long wp = wpIdMap.get(wpKey);
                if (null == wp)
                {
                    wpBuilder.value(EplStudent2WorkPlan.P_ID, EntityIDGenerator.generateNewId(wpEntityCode));
                    wpBuilder.value(EplStudent2WorkPlan.L_STUDENT, student);
                    wpBuilder.value(EplStudent2WorkPlan.L_WORK_PLAN, row[wp_col]);
                    wpBuilder.value(EplStudent2WorkPlan.L_CACHED_GRID_TERM, row[gridTerm_col]);
                    wpBuilder.value(EplStudent2WorkPlan.P_COUNT, row[count_col]);
                    wpBuilder.addBatch();
                    saveWp = true;
                }
            }
            if (saveWp) wpBuilder.createStatement(getSession()).execute();
        });

        doRefreshSplitElement4WorkPlan(summary, fillFirstCourse, useCorrectGroupTitle, yearDiff);
    }

    private void doRefreshSplitElement4WorkPlan(EplStudentSummary summary, boolean fillFirstCourse, boolean useCorrectGroupTitle, int yearDiff)
    {
        // зануляем число студентов для всех план. студентов по элементу деления потоков
        new DQLUpdateBuilder(EplStudentCount4SplitElement.class)
                .fromDataSource(
                        new DQLSelectBuilder().fromEntity(EplStudentCount4SplitElement.class, "s")
                                .where(eq(property("s", EplStudentCount4SplitElement.student2WorkPlan().student().group().summary()), value(summary)))
                                .column(property("s.id"), "id")
                                .buildQuery(), "x"
                )
                .where(eq(property(EplStudentCount4SplitElement.id()), property("x.id")))
                .set(EplStudentCount4SplitElement.studentCount().s(), value(0))
                .createStatement(getSession()).execute();

        // собираем данные о существующих план. студентах сводки - по ключу уникальности
        final Map<StudentKey, Long> eplStudentIds = getSummaryEplStudentIds(summary);
        final Map<EppEduGroupSplitVariant, Map<Long, EppEduGroupSplitBaseElement>> split4ElementMap = Maps.newHashMap();
        for (EppEduGroupSplitBaseElement element : getList(EppEduGroupSplitBaseElement.class))
        {
            SafeMap.safeGet(split4ElementMap, element.getSplitVariant(), HashMap.class).put(element.getSplitElementId(), element);
        }

        // поднимаем текущие РУП план. студентов
        Set<Long> workPlanSet = Sets.newHashSet();
        Map<MultiKey, EplStudent2WorkPlan> student2WorkPlanMap = new DQLSelectBuilder().fromEntity(EplStudent2WorkPlan.class, "eplWp")
                .where(eq(property("eplWp", EplStudent2WorkPlan.student().group().summary().id()), value(summary.getId())))
                .createStatement(getSession()).<EplStudent2WorkPlan>list()
                .stream()
                .collect(Collectors.toMap(item -> {
                    Long wpId = item.getWorkPlan().getId();
                    workPlanSet.add(wpId);
                    return new MultiKey(item.getStudent().getId(), wpId);
                }, item -> item));

        final Map<Long, IEppWorkPlanWrapper> wpDataMap = IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(workPlanSet);

        // находим все элементы реестра, где есть распределение потоков
        Map<EppEduGroupSplitVariant, Map<Long, List<EppRegistryElement>>> wp2SplitMap = Maps.newHashMap();
        for (Map.Entry<Long, IEppWorkPlanWrapper> entry : wpDataMap.entrySet())
        {
            Long wpId = entry.getKey();
            IEppWorkPlanWrapper wrapper = entry.getValue();

            for (IEppWorkPlanRowWrapper rowWrapper : wrapper.getRowMap().values())
            {
                IEppRegElPartWrapper regElementPart = rowWrapper.getRegistryElementPart();
                if (null != regElementPart && null != regElementPart.getItem())
                {
                    EppRegistryElement regElement = regElementPart.getItem().getRegistryElement();
                    EppEduGroupSplitVariant splitVariant = regElement.getEduGroupSplitVariant();
                    if (null == splitVariant) continue;

                    Map<Long, List<EppRegistryElement>> currentMap = SafeMap.safeGet(wp2SplitMap, splitVariant, HashMap.class);
                    SafeMap.safeGet(currentMap, wpId, ArrayList.class).add(regElement);
                }
            }
        }

        final String splitAlias = "split";
        List<EplStudentCount4SplitElement> newEplStudentCountList = Lists.newArrayList();
        for (Map.Entry<EppEduGroupSplitVariant, Map<Long, List<EppRegistryElement>>> entry : wp2SplitMap.entrySet())
        {
            EppEduGroupSplitVariant splitVariant = entry.getKey();
            Map<Long, List<EppRegistryElement>> split2RegElementsMap = entry.getValue();
            Set<Long> workPlan4SplitIds = split2RegElementsMap.keySet();
            Map<Long, EppEduGroupSplitBaseElement> splitElementMap = split4ElementMap.getOrDefault(splitVariant, Maps.newHashMap());

            DQLSelectBuilder resultDql = getStudent2WpFullSplitBuilder(summary, fillFirstCourse, useCorrectGroupTitle, yearDiff, workPlan4SplitIds, splitVariant, splitAlias);
            List<EplStudentCount4SplitElement> resultList = getNewEplStudentCountList(splitVariant, resultDql, splitAlias, splitElementMap, split2RegElementsMap, student2WorkPlanMap, eplStudentIds);

            if (!resultList.isEmpty())
                newEplStudentCountList.addAll(resultList);
        }

        // сохраняем/обновляем число план. студентов для эл. деления потоков
        final MergeAction.SessionMergeAction<EplStudentCount4SplitElement.NaturalId, EplStudentCount4SplitElement> mergeAction = new MergeAction.SessionMergeAction<EplStudentCount4SplitElement.NaturalId, EplStudentCount4SplitElement>() {
            @Override protected EplStudentCount4SplitElement.NaturalId key(EplStudentCount4SplitElement source) { return new EplStudentCount4SplitElement.NaturalId(source.getStudent2WorkPlan(), source.getRegElement(), source.getSplitElement()); }
            @Override protected EplStudentCount4SplitElement buildRow(EplStudentCount4SplitElement source) { return new EplStudentCount4SplitElement(source.getStudent2WorkPlan(), source.getRegElement(), source.getSplitElement()); }
            @Override protected void fill(EplStudentCount4SplitElement target, EplStudentCount4SplitElement source) { target.update(source, false); }
        };

        mergeAction.merge(
                getList(EplStudentCount4SplitElement.class, EplStudentCount4SplitElement.student2WorkPlan().student().group().summary().id(), summary.getId()),
                newEplStudentCountList
        );

        // удаляем число план. студентов для эл. деления потоков с нулевым числом студентов
        new DQLDeleteBuilder(EplStudentCount4SplitElement.class)
                .fromDataSource(
                        new DQLSelectBuilder().fromEntity(EplStudentCount4SplitElement.class, "s")
                                .where(eq(property("s", EplStudentCount4SplitElement.studentCount()), value(0)))
                                .where(eq(property("s", EplStudentCount4SplitElement.student2WorkPlan().student().group().summary()), value(summary)))
                                .column(property("s.id"), "id")
                                .buildQuery(), "x"
                )
                .where(eq(property(EplStudentCount4SplitElement.id()), property("x.id")))
                .createStatement(getSession()).execute();
    }

    private List<EplStudentCount4SplitElement> getNewEplStudentCountList(EppEduGroupSplitVariant splitVariant, DQLSelectBuilder dql, String splitAlias, Map<Long, EppEduGroupSplitBaseElement> splitElementMap, Map<Long, List<EppRegistryElement>> split2RegElementsMap, Map<MultiKey, EplStudent2WorkPlan> student2WorkPlanMap, Map<StudentKey, Long> eplStudentIds)
    {
        final Map<String, String> compTypeMap = getCompSourceMap();
        DQLSelectColumnNumerator splitDql = new DQLSelectColumnNumerator(new DQLSelectBuilder().fromDataSource(dql.buildQuery(), "ds"));

        final int course_col = splitDql.column(property("ds.course"));
        final int group_col = splitDql.column(property("ds.group"));
        final int eou_col = splitDql.column(property("ds.eou"));
        final int ct_col = splitDql.column(property("ds.ct"));
        final int wp_col = splitDql.column(property("ds.wp"));
        final int count_col = splitDql.column(property("ds.count"));
        final int split_col = splitDql.column(property("ds." + splitAlias));

        Map<PairKey<MultiKey, IEntity>, Integer> resultMap = Maps.newHashMap();
        // получаем из запроса нужное количество обучающихся по каждому параметру распределения
        Iterables.partition(splitDql.getDql().createStatement(getSession()).<Object[]>list(), DQL.MAX_VALUES_ROW_NUMBER)
                .forEach(rows -> rows
                        .forEach(row -> {
                            StudentKey studentKey = new StudentKey((Integer) row[course_col], (String) row[group_col], (Long) row[eou_col], compTypeMap.get((String) row[ct_col]));
                            Long eplStudentId = eplStudentIds.get(studentKey);
                            if (null != eplStudentId)
                            {
                                MultiKey multiKey = new MultiKey(eplStudentId, (Long) row[wp_col]);
                                PairKey<MultiKey, IEntity> key = PairKey.create(multiKey, (IEntity) row[split_col]);
                                resultMap.put(key, ((Long) row[count_col]).intValue());
                            }
                        }));

        List<EplStudentCount4SplitElement> resultList = Lists.newArrayList();
        for (Map.Entry<PairKey<MultiKey, IEntity>, Integer> relEntry : resultMap.entrySet())
        {
            PairKey<MultiKey, IEntity> key = relEntry.getKey();
            Integer studentCount = relEntry.getValue();

            EplStudent2WorkPlan student2Wp = student2WorkPlanMap.get(key.getFirst());
            List<EppRegistryElement> regElements = split2RegElementsMap.get(student2Wp.getWorkPlan().getId());
            IEntity split = key.getSecond();

            EppEduGroupSplitBaseElement splitElement = splitElementMap.get(split.getId());
            if (null == splitElement)
            {
                splitElement = saveSplitElement(splitVariant, split);
                splitElementMap.put(split.getId(), splitElement);
            }

            for (EppRegistryElement regElement : regElements)
            {
                EplStudentCount4SplitElement item = new EplStudentCount4SplitElement(student2Wp, regElement, splitElement);
                item.setStudentCount(studentCount);
                resultList.add(item);
            }
        }
        return resultList;
    }

    @Override
    public DQLSelectBuilder getStudent2WorkPlan4SplitBuilder(DQLSelectBuilder dql, String splitAlias, EppEduGroupSplitVariant splitVariant)
    {
        String splitCode = splitVariant.getCode();
        switch (splitCode)
        {
            case EppEduGroupSplitVariantCodes.PO_POLU:
                return dql
                        .joinPath(DQLJoinType.inner, Student.person().identityCard().fromAlias("s"), "iCard")
                        .group(property("iCard", IdentityCard.sex()))
                        .column(property("iCard", IdentityCard.sex()), splitAlias);
            case EppEduGroupSplitVariantCodes.PO_INOSTRANNOMU_YAZYKU:
                return dql
                        .joinEntity("s", DQLJoinType.inner, PersonForeignLanguage.class, "pfl", eq(property("s", Student.person()), property("pfl", PersonForeignLanguage.person())))
                        .where(eq(property("pfl", PersonForeignLanguage.main()), value(Boolean.TRUE)))
                        .group(property("pfl", PersonForeignLanguage.language()))
                        .column(property("pfl", PersonForeignLanguage.language()), splitAlias);
            default:
                throw new IllegalStateException("Unknown split variant.");
        }
    }

    @Override
    public EppEduGroupSplitBaseElement saveSplitElement(EppEduGroupSplitVariant splitVariant, IEntity splitValue)
    {
        String splitCode = splitVariant.getCode();
        EppEduGroupSplitBaseElement splitElement = null;

        if (splitCode.equals(EppEduGroupSplitVariantCodes.PO_POLU))
        {
            splitElement = new EppEduGroupSplitBySexElement(splitVariant, (Sex) splitValue);
            save(splitElement);
        }
        else if (splitCode.equals(EppEduGroupSplitVariantCodes.PO_INOSTRANNOMU_YAZYKU))
        {
            splitElement = new EppEduGroupSplitByLangElement(splitVariant, (ForeignLanguage) splitValue);
            save(splitElement);
        }

        if (null == splitElement)
            throw new IllegalStateException("Unknown split variant.");
        else
            return splitElement;
    }

    private DQLSelectBuilder getStudent2WpFullBuilder(EplStudentSummary summary, boolean fillFirstCourse, boolean useCorrectGroupTitle, int yearDiff)
    {
        DQLSelectBuilder builder = getStudent2WpBuilder(summary, false, useCorrectGroupTitle, yearDiff);
        if (fillFirstCourse)
            builder.unionAll(getStudent2WpBuilder(summary, true, false, yearDiff).buildSelectRule());
        return builder;
    }

    private DQLSelectBuilder getStudent2WpFullSplitBuilder(EplStudentSummary summary, boolean fillFirstCourse, boolean useCorrectGroupTitle, int yearDiff, Set<Long> workPlan4SplitIds, EppEduGroupSplitVariant splitVariant, String splitAlias)
    {
        DQLSelectBuilder builder = getStudent2WorkPlan4SplitBuilder(getStudent2WpBuilder(summary, false, useCorrectGroupTitle, yearDiff), splitAlias, splitVariant)
                .where(in(property("r", EppStudent2WorkPlan.workPlan().id()), workPlan4SplitIds))
                .having(gt(count(property("s.id")), value(0)));

        if (fillFirstCourse)
        {
            DQLSelectBuilder unionDql = getStudent2WorkPlan4SplitBuilder(getStudent2WpBuilder(summary, true, false, yearDiff), splitAlias, splitVariant)
                    .where(in(property("r", EppStudent2WorkPlan.workPlan().id()), workPlan4SplitIds))
                    .having(gt(count(property("s.id")), value(0)));

            builder.unionAll(unionDql.buildSelectRule());
        }
        return builder;
    }

    private DQLSelectBuilder getStudent2WpBuilder(EplStudentSummary summary, boolean fillFirstCourse, boolean useCorrectGroupTitle, int yearDiff)
    {
        boolean checkElementState = IEppSettingsDAO.instance.get().getGlobalSettings().isCheckElementState();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppStudent2WorkPlan.class, "r")
                .where(isNull(property("r", EppStudent2WorkPlan.removalDate())))
                .joinPath(DQLJoinType.inner, EppStudent2WorkPlan.studentEduPlanVersion().student().fromAlias("r"), "s")
                .joinPath(DQLJoinType.left, Student.group().fromAlias("s"), "g")
                .joinPath(DQLJoinType.left, Group.course().fromAlias("g"), "course")
                .joinPath(DQLJoinType.inner, EppStudent2WorkPlan.cachedGridTerm().developGrid().fromAlias("r"), "dg")
                .where(eq(property("s", Student.archival()), value(Boolean.FALSE)))
                .where(eq(property("s", Student.status().active()), value(Boolean.TRUE)))
                .where(eq(property("r", EppStudent2WorkPlan.cachedEppYear()), value(summary.getEppYear())))
                .where(checkElementState ? eq(property("r", EppStudent2WorkPlan.workPlan().state().code()), value(EppState.STATE_ACCEPTED)) : null)

                .where(eq(property("r", EppStudent2WorkPlan.cachedGridTerm().course().intValue()), correctCourse("course", "s", fillFirstCourse, yearDiff)))

                .group(property("dg.id"))
                .group(correctCourse("course", "s", fillFirstCourse, yearDiff))
                .group(property("s", Student.educationOrgUnit().id()))
                .group(property("s", Student.compensationType().code()))
                .group(correctGroupTitle("g", useCorrectGroupTitle, yearDiff))
                .group(property("r", EppStudent2WorkPlan.workPlan().id()))
                .group(property("r", EppStudent2WorkPlan.cachedGridTerm().id()))

                .column(property("dg.id"), "grid")
                .column(correctCourse("course", "s", fillFirstCourse, yearDiff), "course")
                .column(property("s", Student.educationOrgUnit().id()), "eou")
                .column(property("s", Student.compensationType().code()), "ct")
                .column(correctGroupTitle("g", useCorrectGroupTitle, yearDiff), "group")
                .column(property("r", EppStudent2WorkPlan.workPlan().id()), "wp")
                .column(property("r", EppStudent2WorkPlan.cachedGridTerm().id()), "gridTerm")

                .column(count(property("s.id")), "count");

        if (fillFirstCourse)
        {
            builder.where(
                    le(caseExpr(isNotNull(property("course")),
                                property("course", Course.intValue()),
                                property("s", Student.course().intValue())),
                       value(yearDiff)
                    ));
        }
        return builder;
    }

    private IDQLExpression correctCourse(String courseAlias, String studentAlias, boolean fillFirstCourse, int yearDiff)
    {
        String courseTitle = courseAlias + "." + Course.intValue();
        String studentCourseTitle = studentAlias + "." + Student.course().intValue();

        return caseExpr(isNotNull(courseAlias),
                        fillFirstCourse ? property(courseTitle) : plus(property(courseTitle), value(yearDiff)),
                        fillFirstCourse ? property(studentCourseTitle) : plus(property(studentCourseTitle), value(yearDiff))
        );
    }

    /**
     * Если в настройке «Механизм формирования названия групп» выбран 3 или 5 алгоритм и <сдвиг> больше нуля,
     * то используем скорректированное наименование группы студента — берем номер курса группы (первая цифра после дефиса) и прибавляем к нему <сдвиг>.
     *
     * Например, если <сдвиг> равен 1, то скорректированное название для группы «Оархит-301» будет «Оархит-401».
     *
     * @param useCorrectGroupTitle используется механизм формирования названий для групп
     * @param yearDiff             сдвиг
     * @return скорректированное название группы
     */
    private IDQLExpression correctGroupTitle(String groupAlias, boolean useCorrectGroupTitle, int yearDiff)
    {
        String groupTitle = groupAlias + "." + Group.title().s();
        if (!useCorrectGroupTitle || yearDiff <= 0) return coalesce(property(groupTitle), value(NO_GROUP));

        // если группы нет, либо дефис не найден
        return caseExpr(or(isNull(groupTitle), eq(getHyphenPos(groupTitle), value(0))),
                        // то выводим тек. название группы
                        coalesce(property(groupTitle), value(NO_GROUP)),
                        // иначе собираем строку
                        concat(
                                // начало строки, включая дефис
                                substring(property(groupTitle), value(1), getHyphenPos(groupTitle)),
                                // если символ после дефиса - число, тогда прибавляем сдвиг, иначе возвращаем старый символ
                                caseExpr(eq(isNumeric(getCharAfterHyphen(groupTitle)), value(true)), cast(plus(cast(getCharAfterHyphen(groupTitle), PropertyType.INT), value(yearDiff)), PropertyType.STRING), getCharAfterHyphen(groupTitle)),
                                // конец строки
                                substring(property(groupTitle), plus(getHyphenPos(groupTitle), value(2)), length(property(groupTitle)))
                        ));
    }

    private IDQLExpression getHyphenPos(String groupTitle)
    {
        return new IDQLExpression[]{position(property(groupTitle), value("-"))}[0];
    }

    private IDQLExpression getCharAfterHyphen(String groupTitle)
    {
        return new IDQLExpression[]{substring(property(groupTitle), plus(getHyphenPos(groupTitle), value(1)), value(1))}[0];
    }

    // --------------------------------------------------------------------------------------

    private Map<StudentKey, Long> getSummaryEplStudentIds(EplStudentSummary summary)
    {
        // данные о существующих план. студентах сводки - по ключу уникальности
        Map<StudentKey, Long> studentIdMap = Maps.newHashMap();
        new DQLSelectBuilder()
                .fromEntity(EplStudent.class, "s")
                /*0*/.column(property("s", EplStudent.group().course().intValue()))
                /*1*/.column(property("s", EplStudent.group().title()))
                /*2*/.column(property("s", EplStudent.educationOrgUnit().id()))
                /*3*/.column(property("s", EplStudent.compensationSource().code()))
                /*4*/.column("s.id")
                .where(eq(property("s", EplStudent.group().summary().id()), value(summary.getId())))
                .createStatement(getSession()).<Object[]>list()
                .forEach(row -> studentIdMap.put(new StudentKey((Integer) row[0], (String) row[1], (Long) row[2], (String) row[3]), (Long) row[4]));

        return studentIdMap;
    }

    private void doCreateEduGroups(EppYearEducationProcess year, EplStudentSummary summary)
    {
        Debug.begin("createEduGroups");
        try {

            final Session session = getSession();

            Map<Long, Long> eplStudentByStudent = Maps.newHashMap();
            final Map<String, String> compTypeMap = getCompSourceMap();
            Map<StudentKey, Long> eplStudentIds = getSummaryEplStudentIds(summary);

            DQLSelectBuilder eplStudentBuilder = new DQLSelectBuilder().fromEntity(EplStudent2WorkPlan.class, "eplWp")
                    .column(property("s", EplStudent.group().title()), "group")
                    .column(property("s", EplStudent.group().course().id()), "course")
                    .column(property("s", EplStudent.educationOrgUnit().id()), "eou")
                    .column(property("s", EplStudent.compensationSource().code()), "ct")
                    .column(property("eplWp", EplStudent2WorkPlan.cachedGridTerm().id()), "grid")
                    .column(property("eplWp", EplStudent2WorkPlan.workPlan().id()), "wp")
                    .joinPath(DQLJoinType.inner, EplStudent2WorkPlan.student().fromAlias("eplWp"), "s")
                    .where(eq(property("s", EplStudent.group().summary().id()), value(summary.getId())));

            DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                    new DQLSelectBuilder().fromDataSource(
                            getStudent2WpFullBuilder(summary, false, false, 0)
                                    .group(property("s.id"))
                                    .column(property("s.id"), "studentId")
                                    .buildQuery(), "ds")
                            .joinDataSource("ds", DQLJoinType.left, eplStudentBuilder.buildQuery(), "st", and(
                                    eq(property("st.group"), property("ds.group")),
                                    eq(property("st.course"), property("ds.course")),
                                    eq(property("st.eou"), property("ds.eou")),
                                    eq(property("st.ct"), property("ds.ct")),
                                    eq(property("st.grid"), property("ds.grid")),
                                    eq(property("st.wp"), property("ds.wp"))
                            ))
            );

            final int course_col = dql.column(property("ds.course"));
            final int group_col = dql.column(property("ds.group"));
            final int eou_col = dql.column(property("ds.eou"));
            final int ct_col = dql.column(property("ds.ct"));
            final int studentId_col = dql.column(property("ds.studentId"));

            Iterables.partition(dql.getDql().createStatement(session).<Object[]>list(), DQL.MAX_VALUES_ROW_NUMBER).forEach(rows -> {
                for (Object[] row : rows)
                {
                    StudentKey studentKey = new StudentKey((Integer) row[course_col], (String) row[group_col], (Long) row[eou_col], compTypeMap.get((String) row[ct_col]));
                    Long eplStudent = eplStudentIds.get(studentKey);
                    if (null == eplStudent) continue;
                    eplStudentByStudent.put((Long) row[studentId_col], eplStudent);
                }
            });

            final Map<MultiKey, Long> eplSlotMap = new HashMap<>();
            DQLSelectBuilder slotDQL = new DQLSelectBuilder()
                .fromEntity(EplStudentWP2GTypeSlot.class, "s")
                .where(eq(property(EplStudentWP2GTypeSlot.studentWPSlot().student().group().summary().fromAlias("s")), value(summary)))
                .column(property(EplStudentWP2GTypeSlot.studentWPSlot().student().id().fromAlias("s")))
                .column(property(EplStudentWP2GTypeSlot.studentWPSlot().yearPart().id().fromAlias("s")))
                .column(property(EplStudentWP2GTypeSlot.studentWPSlot().activityElementPart().id().fromAlias("s")))
                .column(property(EplStudentWP2GTypeSlot.groupType().id().fromAlias("s")))
                .column(property(EplStudentWP2GTypeSlot.id().fromAlias("s")));

            final List<Object[]> rows = slotDQL.createStatement(session).list();

            for (Object[] row : rows) {
                eplSlotMap.put(eplSlotKey(row[0], row[1], row[2], row[3]), (Long) row[4]);
            }

            for (EppYearPart eppYearPart : getList(EppYearPart.class, EppYearPart.year().s(), year)) {

                final Long yearPartId = eppYearPart.getPart().getId();


                DQLSelectBuilder eppEduGroupRowDQL = new DQLSelectBuilder()
                    .fromEntity(EppRealEduGroupRow.class, "rel")
                    .where(eq(property(EppRealEduGroupRow.group().summary().yearPart().fromAlias("rel")), value(eppYearPart)))
                    .column(property(EppRealEduGroupRow.group().type().id().fromAlias("rel")), "type_id")
                    .column(property(EppRealEduGroupRow.group().activityPart().id().fromAlias("rel")), "regelPart_id")
                    .column(property(EppRealEduGroupRow.group().title().fromAlias("rel")), "eduGroup_title")
                    .column(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().id().fromAlias("rel")), "student_id")
                    .order(property(EppRealEduGroupRow.group().type().priority().fromAlias("rel")))
                    .order(property(EppRealEduGroupRow.group().type().id().fromAlias("rel")))
                    .order(property(EppRealEduGroupRow.group().activityPart().id().fromAlias("rel")))
                    ;

                final Map<MultiKey, EplEduGroup> eplGroupMap = new HashMap<>();
                final Map<PairKey<EplEduGroup, Long>, MutableInt> eplRowMap = SafeMap.get(MutableInt.class);

                BatchUtils.execute(eppEduGroupRowDQL.createStatement(session).<Object[]>list(), 256, elements -> {
                    final Map<Long, ? extends EppGroupType> groupTypeCache = getLoadCacheMap(EppGroupType.class);
                    final Map<Long, EppRegistryElementPart> discCache = getLoadCacheMap(EppRegistryElementPart.class);

                    for (Object[] row : elements) {
                        final Long groupType = (Long) row[0];
                        final Long disc = (Long) row[1];
                        final String groupTitle = (String) row[2];
                        final Long studentId = (Long) row[3];

                        MultiKey groupKey = new MultiKey(groupType, disc, groupTitle);
                        EplEduGroup group = eplGroupMap.get(groupKey);
                        if (null == group) {
                            group = new EplEduGroup();
                            group.setSummary(summary);
                            group.setRegistryElementPart(discCache.get(disc));
                            group.setGroupType(groupTypeCache.get(groupType));
                            group.setYearPart(eppYearPart.getPart());
                            group.setTitle(groupTitle);
                            save(group);
                            eplGroupMap.put(groupKey, group);
                        }

                        MultiKey eplSlotKey = eplSlotKey(eplStudentByStudent.get(studentId), yearPartId, disc, groupType);
                        eplRowMap.get(new PairKey<>(group, eplSlotMap.get(eplSlotKey))).increment();
                    }

                    session.flush();
                    session.clear();
                });

                final MutableInt missed = new MutableInt();
                final MutableInt created = new MutableInt();

                BatchUtils.execute(eplRowMap.entrySet(), 256, elements -> {
                    for (Map.Entry<PairKey<EplEduGroup, Long>, MutableInt> eplRowEntry : elements) {
                        final Long eplSlot = eplRowEntry.getKey().getSecond();
                        if (null == eplSlot) { missed.increment(); continue; }
                        created.increment();
                        EplEduGroupRow row = new EplEduGroupRow();
                        row.setGroup(eplRowEntry.getKey().getFirst());
                        row.setStudentWP2GTypeSlot((EplStudentWP2GTypeSlot) session.load(EplStudentWP2GTypeSlot.class, eplSlot));
                        row.setCount(eplRowEntry.getValue().intValue());
                        save(row);
                    }

                    session.flush();
                    session.clear();
                });

                Debug.message("eplRows created: " + created);
                Debug.message("eplRows missed: " + missed);
            }
        }
        finally {
            Debug.end();
        }
    }

    @Override
    public void doRefreshEduGroups(EplStudentSummary summary, boolean fillEduGroups)
    {
        final Session session = getSession();

        // удаляем все строки План. потока, для которых сводка, полученная по МСРП-ВН (план.), отличается от сводки в План. поток
        int deleteCount = new DQLDeleteBuilder(EplEduGroupRow.class)
                .fromDataSource(
                        new DQLSelectBuilder()
                                .fromEntity(EplEduGroupRow.class, "r").column(property("r.id"), "id")
                                .joinPath(DQLJoinType.inner, EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().group().fromAlias("r"), "g")
                                .where(ne(property("r", EplEduGroupRow.group().summary()), property("g", EplGroup.summary())))
                                .where(eq(property("r", EplEduGroupRow.group().summary()), value(summary)))
                                .buildQuery(), "xxx"
                ).where(eq(property("id"), property("xxx.id")))
                .createStatement(getSession()).execute();

        if (fillEduGroups)
        {
            // формируем План. поток по текущим УГС
            doCreateEduGroups(summary.getEppYear(), summary);
            return;
        }

        // сначала присоединяем к План. поток то, что можно присоединить - по совпадению части года, дисциплиночасти, вида нагрузки, академической группе, элемента деления

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EplStudentWP2GTypeSlot.class, "gtSlot")
            .joinPath(DQLJoinType.inner, EplStudentWP2GTypeSlot.studentWPSlot().fromAlias("gtSlot"), "slot")
            .joinPath(DQLJoinType.inner, EplStudentWPSlot.student().group().summary().fromAlias("slot"), "ss")

            .where(eq(property("ss.id"), value(summary.getId())))
            .where(eq(property("ss", EplStudentSummary.archival()), value(Boolean.FALSE)))

            .where(notExists(EplEduGroupRow.class, EplEduGroupRow.studentWP2GTypeSlot().s(), property("gtSlot"))) // todo join

            .fromEntity(EplEduGroupRow.class, "baseRow")
            .joinPath(DQLJoinType.inner, EplEduGroupRow.studentWP2GTypeSlot().fromAlias("baseRow"), "baseSlot")
            .joinPath(DQLJoinType.inner, EplEduGroupRow.group().fromAlias("baseRow"), "baseGroup")

            .where(eq(property("baseSlot", EplStudentWP2GTypeSlot.studentWPSlot().yearPart()), property("slot", EplStudentWPSlot.yearPart())))
            .where(eq(property("baseSlot", EplStudentWP2GTypeSlot.studentWPSlot().student().group()), property("slot", EplStudentWPSlot.student().group())))
            .where(eq(property("baseGroup", EplEduGroup.registryElementPart()), property("slot", EplStudentWPSlot.activityElementPart())))
            .where(eq(property("baseGroup", EplEduGroup.groupType()), property("gtSlot", EplStudentWP2GTypeSlot.groupType())))

            .column(property("gtSlot", EplStudentWP2GTypeSlot.id()))
            .column(property("baseRow", EplEduGroupRow.group().id()))
            .column(property("gtSlot", EplStudentWP2GTypeSlot.count()))
            .distinct();

        List<Object[]> rows = dql.createStatement(session).list();

        Debug.message("creating slots: " + rows.size());

        final short rowEntityCode = EntityRuntime.getMeta(EplEduGroupRow.class).getEntityCode();
        Iterables.partition(rows, DQL.MAX_VALUES_ROW_NUMBER).forEach(elements -> {
            DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(EplEduGroupRow.class);
            elements.forEach(caRow -> {
                insertBuilder.value(EplEduGroupRow.P_ID, EntityIDGenerator.generateNewId(rowEntityCode));
                insertBuilder.value(EplEduGroupRow.L_STUDENT_W_P2_G_TYPE_SLOT, caRow[0]);
                insertBuilder.value(EplEduGroupRow.L_GROUP, caRow[1]);
                insertBuilder.value(EplEduGroupRow.P_COUNT, caRow[2]);

                insertBuilder.addBatch();
            });
            insertBuilder.createStatement(session).execute();
        });

        // теперь для того, что осталось, нам придется создать новые План. потоки - будем группировать по части года, дисциплиночасти, виду нагрузки, академической группе

        dql = new DQLSelectBuilder()
            .fromEntity(EplStudentWP2GTypeSlot.class, "slot")

            .where(eq(property("slot", EplStudentWP2GTypeSlot.studentWPSlot().student().group().summary()), value(summary)))
            .where(eq(property(EplStudentWP2GTypeSlot.studentWPSlot().student().group().summary().archival().fromAlias("slot")), value(Boolean.FALSE)))

            .where(notExists(EplEduGroupRow.class, EplEduGroupRow.studentWP2GTypeSlot().s(), property("slot"))) // todo join

            .column(property("slot", EplStudentWP2GTypeSlot.id()))
            .column(property("slot", EplStudentWP2GTypeSlot.studentWPSlot().student().group().summary().id()))
            .column(property("slot", EplStudentWP2GTypeSlot.studentWPSlot().yearPart().id()))
            .column(property("slot", EplStudentWP2GTypeSlot.studentWPSlot().activityElementPart().id()))
            .column(property("slot", EplStudentWP2GTypeSlot.groupType().id()))
            .column(property("slot", EplStudentWP2GTypeSlot.studentWPSlot().student().group().title()))

            .column(property(EplStudentWP2GTypeSlot.count().fromAlias("slot")))
            .predicate(DQLPredicateType.distinct);

        rows = dql.createStatement(session).list();

        Debug.message("creating slots: " + rows.size());

        final Map<EplEduGroupKey, Long> groupMap = new HashMap<>();
        final short groupEntityCode = EntityRuntime.getMeta(EplEduGroup.class).getEntityCode();

        Iterables.partition(rows, DQL.MAX_VALUES_ROW_NUMBER).forEach(elements -> {
            DQLInsertValuesBuilder rowInsertDql = new DQLInsertValuesBuilder(EplEduGroupRow.class);
            DQLInsertValuesBuilder groupInsertDql = new DQLInsertValuesBuilder(EplEduGroup.class);

            elements.forEach(row -> {

                Long studentSummaryId = (Long) row[1];
                Long yearPartId = (Long) row[2];
                Long registryElementPart = (Long) row[3];
                Long groupType = (Long) row[4];
                String groupTitle = (String) row[5];

                EplEduGroupKey groupKey = new EplEduGroupKey(yearPartId, registryElementPart, groupType, groupTitle);
                Long groupId = groupMap.get(groupKey);
                if (null == groupId) {

                    groupMap.put(groupKey, groupId = EntityIDGenerator.generateNewId(groupEntityCode));

                    groupInsertDql.value(EplEduGroup.P_ID, groupId);
                    groupInsertDql.value(EplEduGroup.L_SUMMARY, studentSummaryId);
                    groupInsertDql.value(EplEduGroup.L_YEAR_PART, yearPartId);
                    groupInsertDql.value(EplEduGroup.L_REGISTRY_ELEMENT_PART, registryElementPart);
                    groupInsertDql.value(EplEduGroup.L_GROUP_TYPE, groupType);
                    groupInsertDql.value(EplEduGroup.P_TITLE, groupTitle);
                    groupInsertDql.addBatch();
                }

                rowInsertDql.value(EplEduGroupRow.P_ID, EntityIDGenerator.generateNewId(rowEntityCode));
                rowInsertDql.value(EplEduGroupRow.L_STUDENT_W_P2_G_TYPE_SLOT, row[0]);
                rowInsertDql.value(EplEduGroupRow.L_GROUP, groupId);
                rowInsertDql.value(EplEduGroupRow.P_COUNT, row[6]);
                rowInsertDql.addBatch();
            });

            groupInsertDql.createStatement(session).execute();
            rowInsertDql.createStatement(session).execute();
        });

        Map<EplStudentWP2GTypeSlot, List<EplEduGroupRow>> rowMap = new HashMap<>();

        DQLSelectBuilder errorBuilder = EplStudentSummaryManager.instance().dao().getEplStudentWP2GTypeSlotErrorBuilder("slot", summary);
        dql = new DQLSelectBuilder().fromEntity(EplEduGroupRow.class, "row")
                .column(property("row"))
                .joinDataSource("row", DQLJoinType.inner, errorBuilder.buildQuery(), "slot", eq(property("row", EplEduGroupRow.studentWP2GTypeSlot().id()), property("slot.id")))
                .order(property("row", EplEduGroupRow.count()), OrderDirection.desc);

        for (EplEduGroupRow row : dql.createStatement(session).<EplEduGroupRow>list())
        {
            SafeMap.safeGet(rowMap, row.getStudentWP2GTypeSlot(), ArrayList.class).add(row);
        }

        for (Map.Entry<EplStudentWP2GTypeSlot, List<EplEduGroupRow>> entry : rowMap.entrySet())
        {
            int rest = entry.getKey().getCount();
            EplEduGroupRow last = entry.getValue().get(entry.getValue().size() - 1);
            for (EplEduGroupRow row : entry.getValue())
            {
                rest = rest - row.getCount();
                if (rest > 0 && row.equals(last))
                {
                    row.setCount(row.getCount() + rest);
                    continue;
                }
                if (rest <= 0)
                {
                    row.setCount(row.getCount() + rest);
                    session.update(row);
                }
                if (row.getCount() <= 0)
                {
                    session.delete(row);
                }
            }
        }
        session.flush();

        // удаляем все План. потоки, для которых нет ни одной строки План. поток, в рамках сводки
        DQLDeleteBuilder dqlDelete = new DQLDeleteBuilder(EplEduGroup.class)
                .fromDataSource(
                        new DQLSelectBuilder()
                                .fromEntity(EplEduGroup.class, "grp")
                                .column(property("grp.id"), "id")
                                .where(eq(property("grp", EplEduGroup.summary()), value(summary)))
                                .joinEntity("grp", DQLJoinType.left, EplEduGroupRow.class, "row", eq(property("row", EplEduGroupRow.group()), property("grp")))
                                .where(isNull(property("row.id")))
                                .where(notExists(new DQLSelectBuilder().fromEntity(EplPlannedPpsTimeItem.class, "pti")
                                        .fromEntity(EplEduGroupTimeItem.class, "ti")
                                        .where(eq(property("pti", EplPlannedPpsTimeItem.timeItem()), property("ti", EplEduGroupTimeItem.id())))
                                        .where(eq(property("ti", EplEduGroupTimeItem.eduGroup()), property("grp", EplEduGroup.id()))).buildQuery()))
                                .buildQuery(), "xxx"
                ).where(eq(property("id"), property("xxx.id")));

        int updateCount = this.executeAndClear(dqlDelete);
        if (updateCount > 0)
        {
            Debug.message("deleted rows: " + updateCount);
        }
    }


    @Override
    public DQLSelectBuilder getEplStudentWP2GTypeSlotErrorNonEqualCountBuilder(String alias, EplStudentSummary summary)
    {
        //есть МСРП-ВН такое, что число студентов в нем не совпадает с суммой чисел студентов в строках ПУГС, ссылающихся на него
        return new DQLSelectBuilder()
                .fromEntity(EplStudentWP2GTypeSlot.class, alias).column(property(alias + ".id"))
                .fromDataSource(
                        new DQLSelectBuilder()
                                .fromEntity(EplStudentWP2GTypeSlot.class, "slot")
                                .joinEntity("slot", DQLJoinType.left, EplEduGroupRow.class, "r", eq(property("r", EplEduGroupRow.studentWP2GTypeSlot().id()), property("slot.id")))
                                .where(eq(property("slot", EplStudentWP2GTypeSlot.studentWPSlot().student().group().summary()), value(summary)))
                                .group(property("slot.id"))
                                .column(property("slot.id"), "slot_id")
                                .column(sum(coalesce(property("r", EplEduGroupRow.count()), value(0))), "r_sum")
                                .buildQuery(),
                        "x")
                .where(eq(property(alias, "id"), property("x.slot_id")))
                .where(ne(property(alias, EplStudentWP2GTypeSlot.count()), property("x.r_sum")));
    }

    @Override
    public DQLSelectBuilder getEplStudentWP2GTypeSlotErrorWithoutSameSummaryCheckBuilder(String alias, EplStudentSummary summary)
    {
        //есть МСРП-ВН такое, что число студентов в нем не совпадает с суммой чисел студентов в строках ПУГС, ссылающихся на него
        DQLSelectBuilder builder = getEplStudentWP2GTypeSlotErrorNonEqualCountBuilder(alias, summary);
        //есть МСРП-ВН на которое не ссылается ни одна строка ПУГС
        builder.unionAll(
                new DQLSelectBuilder()
                        .fromEntity(EplStudentWP2GTypeSlot.class, alias).column(property(alias+ ".id"))
                        .where(notExistsByExpr(EplEduGroupRow.class, "r", eq(property("r", EplEduGroupRow.studentWP2GTypeSlot().id()), property(alias+ ".id"))))
                        .buildSelectRule()
        );

        return builder;
    }

    @Override
    public DQLSelectBuilder getEplStudentWP2GTypeSlotErrorBuilder(String alias, EplStudentSummary summary)
    {
        //есть МСРП-ВН такое, что число студентов в нем не совпадает с суммой чисел студентов в строках ПУГС, ссылающихся на него
        DQLSelectBuilder builder = getEplStudentWP2GTypeSlotErrorNonEqualCountBuilder(alias, summary);

        builder.unionAll(
                new DQLSelectBuilder()
                        .fromEntity(EplEduGroupRow.class, "r").column(property(alias + ".id"))
                        .joinPath(DQLJoinType.inner, EplEduGroupRow.studentWP2GTypeSlot().fromAlias("r"), alias)
                        .where(eq(property("r", EplEduGroupRow.group().summary()), value(summary)))
                        .where(ne(property(alias, EplStudentWP2GTypeSlot.studentWPSlot().student().group().summary()), value(summary)))
                        .buildSelectRule()
        );

        builder.unionAll(
                new DQLSelectBuilder()
                        .fromEntity(EplStudentWP2GTypeSlot.class, alias).column(property(alias+ ".id"))
                        .joinEntity(alias, DQLJoinType.inner, EplEduGroupRow.class, "r", eq(property("r", EplEduGroupRow.studentWP2GTypeSlot()), property(alias)))
                        .where(eq(property(alias, EplStudentWP2GTypeSlot.studentWPSlot().student().group().summary()), value(summary)))
                        .where(ne(property("r", EplEduGroupRow.group().summary()), value(summary)))
                        .buildSelectRule()
        );

        return builder;
    }

    @Override
    public boolean isNeedRefreshEduGroups(EplStudentSummary summary)
    {
        //есть ПУГС без строк
        boolean hasErrors = null != new DQLSelectBuilder()
                .fromEntity(EplEduGroup.class, "g")
                .where(eq(property("g", EplEduGroup.summary()), value(summary)))
                .where(notExists(EplEduGroupRow.class, EplEduGroupRow.group().s(), property("g")))
                .column(property("g.id")).top(1)
                .createStatement(getSession())
                .uniqueResult();

        if (!hasErrors)
        {
            // TODO: поскольку "select 1 where exists (...)" тормозит при пустом значении запроса
            hasErrors = 0 != getCount(getEplStudentWP2GTypeSlotErrorWithoutSameSummaryCheckBuilder("slot", summary));
        }
        return hasErrors;
    }

    @Override
    public DQLSelectBuilder checkStudentSummaryByOrgUnit(String alias, DQLSelectBuilder dql, ExecutionContext context)
    {
        OrgUnit formativeOrgUnit = context.get(EplStudentSummaryManager.PARAM_FORMATIVE_ORG_UNIT);
        OrgUnit formativeOrOwnerOrgUnit = context.get(EplStudentSummaryManager.PARAM_FORMATIVE_OR_OWNER_ORG_UNIT);

        if (null != formativeOrOwnerOrgUnit)
            dql.where(exists(
                    new DQLSelectBuilder()
                            .fromEntity(EplEduGroupRow.class, "row").column(value(1)).top(1)
                            .joinPath(DQLJoinType.inner, EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().fromAlias("row"), "slot")
                            .where(or(
                                    eq(property("slot", EplStudentWPSlot.student().educationOrgUnit().formativeOrgUnit()), value(formativeOrOwnerOrgUnit)),
                                    eq(property("slot", EplStudentWPSlot.activityElementPart().registryElement().owner()), value(formativeOrOwnerOrgUnit)),
                                    eq(property("row", EplEduGroupRow.group().registryElementPart().registryElement().owner()), value(formativeOrOwnerOrgUnit))
                            ))
                            .where(eq(property("slot", EplStudentWPSlot.student().group().summary()), property(alias)))
                            .buildQuery()
            ));
        else if (null != formativeOrgUnit)
            dql.where(exists(
                    new DQLSelectBuilder()
                            .fromEntity(EplStudent.class, "s")
                            .where(eq(property("s", EplStudent.educationOrgUnit().formativeOrgUnit()), value(formativeOrgUnit)))
                            .where(eq(property("s", EplStudent.group().summary()), property(alias)))
                            .buildQuery()
            ));
        else
            dql.where(nothing());
        return dql;
    }

    private MultiKey eplSlotKey(Object student, Object yearPart, Object disc, Object groupType)
    {
        return new MultiKey(student, yearPart, disc, groupType);
    }

    private Map<String, String> getCompSourceMap()
    {
        // todo нормальное заполнение справочников, настройка соответствия
        return ImmutableMap.of(
                CompensationTypeCodes.COMPENSATION_TYPE_BUDGET, EplCompensationSourceCodes.COMPENSATION_TYPE_BUDGET,
                CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT, EplCompensationSourceCodes.COMPENSATION_TYPE_CONTRACT
        );
    }

    @Override
    public DQLSelectBuilder getWorkPlanAndRegElementUniquePairsDql(String rowAlias, EplStudentSummary summary)
    {
        return new DQLSelectBuilder()
                .fromEntity(EplStudent2WorkPlan.class, "s2wp")
                .column(property("s2wp")).column(property("rl"))
                .joinEntity("s2wp", DQLJoinType.inner, EppWorkPlanRegistryElementRow.class, rowAlias, eq(property("s2wp", EplStudent2WorkPlan.workPlan().id()), property(rowAlias, EppWorkPlanRegistryElementRow.workPlan().id())))
                .joinPath(DQLJoinType.inner, EppWorkPlanRegistryElementRow.registryElementPart().registryElement().fromAlias(rowAlias), "rl")
                .where(or(
                        isNotNull(property("rl", EppRegistryElement.eduGroupSplitVariant())),
                        and(
                                isNull(property("rl", EppRegistryElement.eduGroupSplitVariant())),
                                exists(EplStudentCount4SplitElement.class, EplStudentCount4SplitElement.regElement().id().s(), property("rl.id"))
                        )
                ))
                .where(eq(property("s2wp", EplStudent2WorkPlan.student().group().summary().id()), value(summary.getId())));
    }

    @Override
    public DQLSelectBuilder getIncorrectSplitPairDql(EplStudentSummary studentSummary)
    {
        DQLSelectBuilder basePairBuilder = getWorkPlanAndRegElementUniquePairsDql("row", studentSummary)
                .column(property("s2wp.id"), "s2wp_id")
                .column(property("rl.id"), "rl_id");

        // сумма чисел студентов для элементов деления не совпадает с числом студентов по РУП
        DQLSelectBuilder dql1 = new DQLSelectBuilder().fromDataSource(basePairBuilder.buildQuery(), "xxx")
                .joinEntity("xxx", DQLJoinType.left, EplStudent2WorkPlan.class, "s2wp", eq(property("s2wp.id"), property("xxx", "s2wp_id")))
                .joinEntity("xxx", DQLJoinType.inner, EplStudentCount4SplitElement.class, "se", and(
                        eq(property("se", EplStudentCount4SplitElement.student2WorkPlan().id()), property("xxx", "s2wp_id")),
                        eq(property("se", EplStudentCount4SplitElement.regElement().id()), property("xxx", "rl_id"))
                ))
                .column(property("xxx", "s2wp_id"))
                .column(property("xxx", "rl_id"))
                .group(property("xxx", "s2wp_id"))
                .group(property("xxx", "rl_id"))
                .group(property("s2wp", EplStudent2WorkPlan.P_COUNT))
                .having(ne(sum(property("se", EplStudentCount4SplitElement.studentCount())), property("s2wp", EplStudent2WorkPlan.P_COUNT)));

        // способ деления элемента реестра не совпадает со способом деления элементов деления в нагрузке
        DQLSelectBuilder dql2 = new DQLSelectBuilder().fromDataSource(basePairBuilder.buildQuery(), "xxx")
                .joinEntity("xxx", DQLJoinType.inner, EplStudentCount4SplitElement.class, "se", and(
                        eq(property("se", EplStudentCount4SplitElement.student2WorkPlan().id()), property("xxx", "s2wp_id")),
                        eq(property("se", EplStudentCount4SplitElement.regElement().id()), property("xxx", "rl_id"))
                ))
                .where(ne(property("se", EplStudentCount4SplitElement.regElement().eduGroupSplitVariant()), property("se", EplStudentCount4SplitElement.splitElement().splitVariant())))
                .column(property("xxx", "s2wp_id")).column(property("xxx", "rl_id")).distinct();

        // у элементов деления разные способы деления
        DQLSelectBuilder dql3 = new DQLSelectBuilder().fromDataSource(
                new DQLSelectBuilder().fromDataSource(basePairBuilder.buildQuery(), "xxx")
                        .column(property("xxx", "s2wp_id"), "s2wp_id")
                        .column(property("xxx", "rl_id"), "rl_id")
                        .column(property("se", EplStudentCount4SplitElement.splitElement().splitVariant().id()), "split_var")
                        .distinct()
                        .joinEntity("xxx", DQLJoinType.inner, EplStudentCount4SplitElement.class, "se", and(
                                eq(property("se", EplStudentCount4SplitElement.student2WorkPlan().id()), property("xxx", "s2wp_id")),
                                eq(property("se", EplStudentCount4SplitElement.regElement().id()), property("xxx", "rl_id"))
                        ))
                        .buildQuery(), "yyy")
                .column(property("yyy", "s2wp_id"))
                .column(property("yyy", "rl_id"))
                .group(property("yyy", "s2wp_id"))
                .group(property("yyy", "rl_id"))
                .having(ne(count(property("yyy", "split_var")), value(1)));

        dql1.unionAll(dql2.buildSelectRule());
        dql1.unionAll(dql3.buildSelectRule());

        return dql1;
    }

    @Override
    public DQLSelectBuilder getSplitEmptyPairDql(EplStudentSummary studentSummary)
    {
        return new DQLSelectBuilder().fromDataSource(
                getWorkPlanAndRegElementUniquePairsDql("row", studentSummary)
                        .column(property("s2wp.id"), "s2wp_id")
                        .column(property("rl.id"), "rl_id")
                        .buildQuery(), "xxx")
                .column(property("xxx", "s2wp_id"))
                .column(property("xxx", "rl_id"))
                .joinEntity("xxx", DQLJoinType.left, EplStudentCount4SplitElement.class, "se", and(
                        eq(property("se", EplStudentCount4SplitElement.student2WorkPlan().id()), property("xxx", "s2wp_id")),
                        eq(property("se", EplStudentCount4SplitElement.regElement().id()), property("xxx", "rl_id"))
                ))
                .where(isNull(property("se")));
    }

    @Override
    public int doSplitElementMassDelete(Collection<Long> selectedIds)
    {
        int count = 0;
        for (List<Long> ids : Iterables.partition(selectedIds, DQL.MAX_VALUES_ROW_NUMBER)) {
            count += new DQLDeleteBuilder(EplStudentCount4SplitElement.class)
                    .where(in(property(EplStudentCount4SplitElement.id()), ids))
                    .where(new DQLCanDeleteExpressionBuilder(EplStudentCount4SplitElement.class, "id").getExpression())
                    .createStatement(getSession()).execute();
        }
        return count;
    }

    @Override
    public void saveNewSplitRowList(List<EplStudentCount4SplitElement> currentSplitElementList, EppRegistryElement eppRegistryElement, EplStudent2WorkPlan eplStudent2WorkPlan){
        getSession().clear();
        final MergeAction.SessionMergeAction<EplStudentCount4SplitElement.NaturalId, EplStudentCount4SplitElement> mergeAction = new MergeAction.SessionMergeAction<EplStudentCount4SplitElement.NaturalId, EplStudentCount4SplitElement>() {
            @Override protected EplStudentCount4SplitElement.NaturalId key(EplStudentCount4SplitElement source) { return new EplStudentCount4SplitElement.NaturalId(source.getStudent2WorkPlan(), source.getRegElement(), source.getSplitElement()); }
            @Override protected EplStudentCount4SplitElement buildRow(EplStudentCount4SplitElement source) { return new EplStudentCount4SplitElement(source.getStudent2WorkPlan(), source.getRegElement(), source.getSplitElement()); }
            @Override protected void fill(EplStudentCount4SplitElement target, EplStudentCount4SplitElement source) { target.update(source, false); }
        };

        String es4se = "es4se";
        DQLSelectBuilder dqlEs4se = new DQLSelectBuilder()
                .fromEntity(EplStudentCount4SplitElement.class, es4se)
                .where(eq(property(EplStudentCount4SplitElement.student2WorkPlan().id().fromAlias(es4se)), value(eplStudent2WorkPlan.getId())))
                .where(eq(property(EplStudentCount4SplitElement.regElement().id().fromAlias(es4se)), value(eppRegistryElement.getId())));
        List<EplStudentCount4SplitElement> databaseSplitElementList = dqlEs4se.createStatement(getSession()).list();

        mergeAction.merge(databaseSplitElementList,currentSplitElementList);
    }


    @Override
    public int doOrgUnitSummaryMassDelete(Collection<Long> selectedIds)
    {
        int count = 0;
        for (List<Long> ids : Iterables.partition(selectedIds, DQL.MAX_VALUES_ROW_NUMBER)) {
            count += new DQLDeleteBuilder(EplOrgUnitSummary.class)
                    .where(in(property(EplOrgUnitSummary.id()), ids))
                    .where(new DQLCanDeleteExpressionBuilder(EplOrgUnitSummary.class, "id").getExpression())
                    .createStatement(getSession()).execute();
        }
        return count;
    }

    public void doGenerateBaseSplitElements(EppEduGroupSplitVariant splitVariant)
    {
        //генерация происходит тольуо для несуществующих базовых элементов и только для переданного типа деления
        Class typeClass;
        IDQLExpression intermediateClassCheck;
        String splitCode = splitVariant.getCode();
        String alias = "alias";
        switch (splitCode) {
            case EppEduGroupSplitVariantCodes.PO_POLU:
                typeClass = Sex.class;
                intermediateClassCheck = notExists(EppEduGroupSplitBySexElement.class, EppEduGroupSplitBySexElement.sex().id().s(), property(alias, "id"));
                break;
            case EppEduGroupSplitVariantCodes.PO_INOSTRANNOMU_YAZYKU:
                typeClass = ForeignLanguage.class;
                intermediateClassCheck = notExists(EppEduGroupSplitByLangElement.class, EppEduGroupSplitByLangElement.foreignLanguage().id().s(), property(alias, "id"));
                break;
            default:
                throw new IllegalStateException("There is no appropriate class for this split variant.");
        }
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(typeClass, alias).where(intermediateClassCheck);
        List<IEntity> typeList = dql.createStatement(getSession()).list();
        for (IEntity entity : typeList) {
            saveSplitElement(splitVariant, entity);
        }
    }

    private static class StudentKey
    {
        Integer course;
        String group;
        Long eduOu;
        String eplCompSource;

        private StudentKey(Integer course, String group, Long eduOu, String eplCompSource)
        {
            this.course = course;
            this.group = group;
            this.eduOu = eduOu;
            this.eplCompSource = eplCompSource;
        }

        private MultiKey key() {return new MultiKey(course, group, eduOu, eplCompSource);}

        @Override
        public int hashCode()
        {
            return key().hashCode();
        }

        @Override
        public boolean equals(Object obj)
        {
            if (null == obj || !obj.getClass().isAssignableFrom(getClass()))
                return false;
            return key().equals(((StudentKey)obj).key());
        }
    }

    private static class EplEduGroupKey
    {
        Long yearPart;
        Long registryElementPart;
        Long groupType;
        String group;
        MultiKey key;

        private EplEduGroupKey(Long yearPart, Long registryElementPart, Long groupType, String group)
        {
            this.yearPart = yearPart;
            this.registryElementPart = registryElementPart;
            this.groupType = groupType;
            this.group = group;
            key = new MultiKey(yearPart, registryElementPart, groupType, group);
        }

        @Override public int hashCode() { return key.hashCode(); }

        @Override public boolean equals(Object obj) {
            if (null == obj || !obj.getClass().isAssignableFrom(EplEduGroupKey.class))
                return false;
            return key.equals(((EplEduGroupKey) obj).key);
        }
    }

}