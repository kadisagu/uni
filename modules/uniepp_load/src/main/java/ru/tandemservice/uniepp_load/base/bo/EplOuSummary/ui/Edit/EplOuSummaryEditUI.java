/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.Edit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;

/**
 * @author oleyba
 * @since 11/10/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id")
})
public class EplOuSummaryEditUI extends UIPresenter
{
    private EntityHolder<EplOrgUnitSummary> holder = new EntityHolder<EplOrgUnitSummary>();

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
    }

    public void onClickApply() {
        IUniBaseDao.instance.get().saveOrUpdate(getSummary());
        deactivate();
    }

    // getters and setters

    public EntityHolder<EplOrgUnitSummary> getHolder()
    {
        return holder;
    }

    public EplOrgUnitSummary getSummary()
    {
        return getHolder().getValue();
    }
}