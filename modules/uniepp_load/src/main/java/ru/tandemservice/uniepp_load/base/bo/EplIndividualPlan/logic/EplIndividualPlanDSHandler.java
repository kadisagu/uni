/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.logic;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.mutable.MutableLong;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.EplIndividualPlanManager;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.List.EplIndividualPlanList;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.List.EplIndividualPlanListUI;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndPlan2OuSummaryRel;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem;
import ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab.EplOrgUnitTabUI;
import ru.tandemservice.uniepp_load.util.UniEppLoadUtils;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dao.CommonDAO.ids;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.sum;

/**
 * @author Alexey Lopatin
 * @since 01.08.2016
 */
public class EplIndividualPlanDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public EplIndividualPlanDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EplStudentSummary studentSummary = context.get(EplIndividualPlanListUI.PARAM_STUDENT_SUMMARY);

        DQLOrderDescriptionRegistry registry = new DQLOrderDescriptionRegistry(EplIndividualPlan.class, "e");
        DQLSelectBuilder dql = registry.buildDQLSelectBuilder();

        Person ppsPerson = context.get(EplIndividualPlanListUI.PARAM_PPS_PERSON);
        if (null != ppsPerson)
            FilterUtils.applySelectFilter(dql, "e", EplIndividualPlan.pps().person(), ppsPerson);
        else
            FilterUtils.applySelectFilter(dql, "e", EplIndividualPlan.pps(), context.get(EplIndividualPlanListUI.PARAM_PPS_LIST));

        FilterUtils.applySelectFilter(dql, "e", EplIndividualPlan.orgUnit().id(), context.get(EplOrgUnitTabUI.BIND_ORG_UNIT_ID));

        FilterUtils.applySelectFilter(dql, "e", EplIndividualPlan.eppYear(), context.get(EplIndividualPlanListUI.PARAM_EPP_YEAR));
        FilterUtils.applySelectFilter(dql, "e", EplIndividualPlan.orgUnit(), context.get(EplIndividualPlanListUI.PARAM_ORG_UNIT));

        if (null != studentSummary)
        {
            dql.where(exists(EplIndPlan2OuSummaryRel.class,
                             EplIndPlan2OuSummaryRel.ouSummary().studentSummary().s(), studentSummary,
                             EplIndPlan2OuSummaryRel.indPlan().id().s(), property("e.id")
            ));
        }

        registry.applyOrder(dql, input.getEntityOrder());

        DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).build();

        Iterables.partition(DataWrapper.wrap(output), DQL.MAX_VALUES_ROW_NUMBER).forEach(wrappers -> {
            Collection<Long> ids = ids(wrappers);

            Map<Long, List<EplOrgUnitSummary>> indPlan2OuSummaryMap = Maps.newHashMap();
            new DQLSelectBuilder().fromEntity(EplIndPlan2OuSummaryRel.class, "rel")
                    .joinPath(DQLJoinType.inner, EplIndPlan2OuSummaryRel.ouSummary().studentSummary().fromAlias("rel"), "ss")
                    .where(in(property("rel", EplIndPlan2OuSummaryRel.indPlan().id()), ids))
                    .order(property("rel", EplIndPlan2OuSummaryRel.indPlan().id()))
                    .order(property("ss", EplStudentSummary.eppYear().educationYear().intValue()), OrderDirection.desc)
                    .order(property("ss", EplStudentSummary.title()))
                    .column(property("rel"))
                    .createStatement(context.getSession()).<EplIndPlan2OuSummaryRel>list()
                    .forEach(rel -> SafeMap.safeGet(indPlan2OuSummaryMap, rel.getIndPlan().getId(), LinkedList.class).add(rel.getOuSummary()));

            Set<EplOrgUnitSummary> ouSummaries = indPlan2OuSummaryMap.values()
                    .stream().flatMap(Collection::stream)
                    .collect(Collectors.toSet());

            Map<PairKey, Long> pps2CountMap = Maps.newHashMap();
            Map<Long, Long> indPlan2NonEduLoadMap = Maps.newHashMap();

            new DQLSelectBuilder().fromEntity(EplPlannedPpsTimeItem.class, "pps")
                    .joinPath(DQLJoinType.inner, EplPlannedPpsTimeItem.timeItem().fromAlias("pps"), "ti")
                    .where(in(property("ti", EplTimeItem.summary().id()), ids(ouSummaries)))
                    .group(property("ti", EplTimeItem.summary().id()))
                    .group(property("pps", EplPlannedPpsTimeItem.pps().ppsEntry().id()))
                    .column(property("ti", EplTimeItem.summary().id()))
                    .column(property("pps", EplPlannedPpsTimeItem.pps().ppsEntry().id()))
                    .column(sum(property("pps", EplPlannedPpsTimeItem.timeAmountAsLong())))
                    .createStatement(context.getSession()).<Object[]>list()
                    .forEach(row -> pps2CountMap.put(PairKey.create((Long) row[0], (Long) row[1]), (Long) row[2]));

            new DQLSelectBuilder().fromEntity(EplTimeItemNonEduLoad.class, "item")
                    .joinPath(DQLJoinType.inner, EplTimeItemNonEduLoad.indPlan().fromAlias("item"), "i")
                    .where(in(property("i.id"), ids))
                    .group(property("i.id"))
                    .column(property("i.id"))
                    .column(sum(property("item", EplTimeItemNonEduLoad.timeAmountAsLong())))
                    .createStatement(context.getSession()).<Object[]>list()
                    .forEach(row -> indPlan2NonEduLoadMap.put((Long) row[0], (Long) row[1]));

            for (DataWrapper wrapper : wrappers)
            {
                EplIndividualPlan plan = wrapper.getWrapped();

                MutableLong eduLoad = new MutableLong(0);
                List<EplOrgUnitSummary> summaries = indPlan2OuSummaryMap.getOrDefault(plan.getId(), Lists.newArrayList());
                summaries.forEach(ouSummary -> eduLoad.add(pps2CountMap.getOrDefault(PairKey.create(ouSummary.getId(), plan.getPps().getId()), 0L)));
                long nonEduLoad = indPlan2NonEduLoadMap.getOrDefault(plan.getId(), 0L);

                wrapper.put(EplIndividualPlanList.PARAM_STUDENT_SUMMARY, CommonBaseEntityUtil.getPropertiesSet(indPlan2OuSummaryMap.get(plan.getId()), EplOrgUnitSummary.studentSummary().title()));
                wrapper.put(EplIndividualPlanList.PARAM_EDU_LOAD, UniEppLoadUtils.wrap(eduLoad.longValue()));
                wrapper.put(EplIndividualPlanList.PARAM_NON_EDU_LOAD, UniEppLoadUtils.wrap(nonEduLoad));

                ParametersMap params = new ParametersMap();
                params.add(UIPresenter.PUBLISHER_ID, wrapper.getId());
                params.add(EplIndividualPlanManager.PARAM_PPS_ACTIVE, null != ppsPerson);

                wrapper.put(EplIndividualPlanList.PARAM_PUB_PARAMETERS, params);
            }
        });
        return output;
    }
}