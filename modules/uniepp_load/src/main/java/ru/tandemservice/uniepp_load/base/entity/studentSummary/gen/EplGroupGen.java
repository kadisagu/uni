package ru.tandemservice.uniepp_load.base.entity.studentSummary.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * План. группа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplGroupGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup";
    public static final String ENTITY_NAME = "eplGroup";
    public static final int VERSION_HASH = -1967898115;
    private static IEntityMeta ENTITY_META;

    public static final String L_SUMMARY = "summary";
    public static final String L_COURSE = "course";
    public static final String P_TITLE = "title";

    private EplStudentSummary _summary;     // Сводка
    private Course _course;     // Курс
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сводка. Свойство не может быть null.
     */
    @NotNull
    public EplStudentSummary getSummary()
    {
        return _summary;
    }

    /**
     * @param summary Сводка. Свойство не может быть null.
     */
    public void setSummary(EplStudentSummary summary)
    {
        dirty(_summary, summary);
        _summary = summary;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EplGroupGen)
        {
            setSummary(((EplGroup)another).getSummary());
            setCourse(((EplGroup)another).getCourse());
            setTitle(((EplGroup)another).getTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplGroup.class;
        }

        public T newInstance()
        {
            return (T) new EplGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "summary":
                    return obj.getSummary();
                case "course":
                    return obj.getCourse();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "summary":
                    obj.setSummary((EplStudentSummary) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "summary":
                        return true;
                case "course":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "summary":
                    return true;
                case "course":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "summary":
                    return EplStudentSummary.class;
                case "course":
                    return Course.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplGroup> _dslPath = new Path<EplGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplGroup");
    }
            

    /**
     * @return Сводка. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup#getSummary()
     */
    public static EplStudentSummary.Path<EplStudentSummary> summary()
    {
        return _dslPath.summary();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EplGroup> extends EntityPath<E>
    {
        private EplStudentSummary.Path<EplStudentSummary> _summary;
        private Course.Path<Course> _course;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сводка. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup#getSummary()
     */
        public EplStudentSummary.Path<EplStudentSummary> summary()
        {
            if(_summary == null )
                _summary = new EplStudentSummary.Path<EplStudentSummary>(L_SUMMARY, this);
            return _summary;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EplGroupGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EplGroup.class;
        }

        public String getEntityName()
        {
            return "eplGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
