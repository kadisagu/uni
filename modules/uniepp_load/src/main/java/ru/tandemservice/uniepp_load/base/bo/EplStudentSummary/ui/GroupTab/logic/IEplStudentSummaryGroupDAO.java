package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;

import java.util.Collection;

/**
 * @author avedernikov
 * @since 16.12.2016
 */
public interface IEplStudentSummaryGroupDAO extends ISharedBaseDao
{
	/**
	 * Удалить план. студентов нужной сводки, соответствующих строке списка план. контингента.
	 * @param summary Сводка контингента.
	 * @param filters Фильтры, примененные к списку план. контингента.
	 * @param eplStudentProperties Параметры строки списка в формате "ключ-значение".
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	void deleteEplStudents(EplStudentSummary summary, EplStudentFilter filters, Collection<Pair<String, Object>> eplStudentProperties);
}
