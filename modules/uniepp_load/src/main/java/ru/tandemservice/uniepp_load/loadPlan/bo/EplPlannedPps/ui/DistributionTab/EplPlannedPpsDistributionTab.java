/* $Id$ */
package ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.DistributionTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplSimpleTimeItem;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author nvankov
 * @since 2/2/15
 */
@Configuration
public class EplPlannedPpsDistributionTab extends BusinessComponentManager
{
    public static final String PPS_DS = "ppsDS";
    public static final String LOAD_TYPE_DS = "loadTypeDS";
    public static final String COURSE_DS = "courseDS";
    public static final String EDU_OU_DS = "educationOrgUnitDS";
    public static final String DEV_FORM_DS = "developFormDS";
    public static final String TIME_RULE_DS = "timeRuleDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(LOAD_TYPE_DS, loadTypeDSHandler()))
                .addDataSource(selectDS(COURSE_DS, courseDSHandler()))
                .addDataSource(selectDS(EDU_OU_DS, educationOrgUnitDSHandler()).addColumn(EducationOrgUnit.titleWithFormAndCondition().s()))
                .addDataSource(selectDS(DEV_FORM_DS, developFormDSHandler()))
                .addDataSource(selectDS(TIME_RULE_DS, timeRuleDSHandler()))
                .create();
    }



    @Bean
    public IDefaultComboDataSourceHandler loadTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppGroupType.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long orgUnitSummaryId = context.getNotNull("orgUnitSummaryId");
                Long ppsId = context.get("pps");
                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EplEduGroupTimeItem.class, "ti")
                        .column(property("ti", EplEduGroupTimeItem.eduGroup().groupType().id()))
                        .where(eq(property("ti", EplEduGroupTimeItem.summary().id()), value(orgUnitSummaryId)));
                if(ppsId != null)
                    subBuilder.joinEntity("ti", DQLJoinType.inner, EplPlannedPpsTimeItem.class, "pti", eq(property("pti", EplPlannedPpsTimeItem.timeItem()), property("ti")))
                            .where(eq(property("pti", EplPlannedPpsTimeItem.pps().id()), value(ppsId)));

                dql.where(in(property("e", EppGroupType.id()), subBuilder.buildQuery()));
            }
        }
            .filter(EppGroupType.title())
            .order(EppGroupType.priority());
    }

    @Bean
    public IDefaultComboDataSourceHandler courseDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Course.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long orgUnitSummaryId = context.getNotNull("orgUnitSummaryId");
                Long ppsId = context.get("pps");
                Long loadTypeId = context.get("loadType");

                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EplEduGroupRow.class, "gr")
                        .column(property("gr", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().group().course().id()))
                        .joinPath(DQLJoinType.inner, EplEduGroupRow.group().summary().fromAlias("gr"), "sum")
                        .joinEntity("gr", DQLJoinType.inner, EplOrgUnitSummary.class, "ous", eq(property("ous", EplOrgUnitSummary.studentSummary()), property("sum")))
                        .where(eq(property("ous", EplOrgUnitSummary.id()), value(orgUnitSummaryId)))
                        .where(in(property("gr", EplEduGroupRow.group().id()), new DQLSelectBuilder().fromEntity(EplEduGroupTimeItem.class, "gti")
                                .where(eq(property("gti", EplEduGroupTimeItem.summary().id()), value(orgUnitSummaryId)))
                                .column(property("gti", EplEduGroupTimeItem.eduGroup().id())).buildQuery()));
//
//
                if(ppsId != null)
                {
                    DQLSelectBuilder subSubBuilder = new DQLSelectBuilder().fromEntity(EplEduGroupTimeItem.class, "ti")
                            .joinEntity("ti", DQLJoinType.inner, EplPlannedPpsTimeItem.class, "pti", eq(property("pti", EplPlannedPpsTimeItem.timeItem()), property("ti")))
                            .where(eq(property("ti", EplEduGroupTimeItem.summary().id()), value(orgUnitSummaryId)))
                            .where(eq(property("gr", EplEduGroupRow.group()), property("ti", EplEduGroupTimeItem.eduGroup())))
                            .where(eq(property("pti", EplPlannedPpsTimeItem.pps().id()), value(ppsId)));
                    subBuilder.where(exists(subSubBuilder.buildQuery()));
                }

                if(loadTypeId != null)
                    subBuilder.where(eq(property("gr", EplEduGroupRow.group().groupType().id()), value(loadTypeId)));

                dql.where(in(property("e", Course.id()), subBuilder.buildQuery()));
            }
        }
                .filter(Course.title())
                .order(Course.intValue());
    }

    @Bean
    public IDefaultComboDataSourceHandler developFormDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopForm.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long orgUnitSummaryId = context.getNotNull("orgUnitSummaryId");
                Long ppsId = context.get("pps");
                Long loadTypeId = context.get("loadType");
                Long courseId = context.get("course");

                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EplEduGroupRow.class, "gr")
                        .column(property("gr", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().educationOrgUnit().developForm().id()))
                        .joinPath(DQLJoinType.inner, EplEduGroupRow.group().summary().fromAlias("gr"), "sum")
                        .joinEntity("gr", DQLJoinType.inner, EplOrgUnitSummary.class, "ous", eq(property("ous", EplOrgUnitSummary.studentSummary()), property("sum")))
                        .where(eq(property("ous", EplOrgUnitSummary.id()), value(orgUnitSummaryId)))
                        .where(in(property("gr", EplEduGroupRow.group().id()), new DQLSelectBuilder().fromEntity(EplEduGroupTimeItem.class, "gti")
                                .where(eq(property("gti", EplEduGroupTimeItem.summary().id()), value(orgUnitSummaryId)))
                                .column(property("gti", EplEduGroupTimeItem.eduGroup().id()))
                                .buildQuery()));
//
//
                if(ppsId != null)
                {
                    DQLSelectBuilder subSubBuilder = new DQLSelectBuilder().fromEntity(EplEduGroupTimeItem.class, "ti")
                            .joinEntity("ti", DQLJoinType.inner, EplPlannedPpsTimeItem.class, "pti", eq(property("pti", EplPlannedPpsTimeItem.timeItem()), property("ti")))
                            .where(eq(property("ti", EplEduGroupTimeItem.summary().id()), value(orgUnitSummaryId)))
                            .where(eq(property("gr", EplEduGroupRow.group()), property("ti", EplEduGroupTimeItem.eduGroup())))
                            .where(eq(property("pti", EplPlannedPpsTimeItem.pps().id()), value(ppsId)));
                    subBuilder.where(exists(subSubBuilder.buildQuery()));
                }

                if(loadTypeId != null)
                    subBuilder.where(eq(property("gr", EplEduGroupRow.group().groupType().id()), value(loadTypeId)));

                if(courseId != null)
                    subBuilder.where(eq(property("gr", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().group().course().id()), value(courseId)));

                dql.where(in(property("e", DevelopForm.id()), subBuilder.buildQuery()));
            }
        }
                .filter(DevelopForm.title())
                .order(DevelopForm.code());
    }

    @Bean
    public IDefaultComboDataSourceHandler educationOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EducationOrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long orgUnitSummaryId = context.getNotNull("orgUnitSummaryId");
                Long ppsId = context.get("pps");
                Long loadTypeId = context.get("loadType");
                Long courseId = context.get("course");
                Long developFormId = context.get("developForm");

                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EplEduGroupRow.class, "gr")
                        .column(property("gr", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().educationOrgUnit().id()))
                        .joinPath(DQLJoinType.inner, EplEduGroupRow.group().summary().fromAlias("gr"), "sum")
                        .joinEntity("gr", DQLJoinType.inner, EplOrgUnitSummary.class, "ous", eq(property("ous", EplOrgUnitSummary.studentSummary()), property("sum")))
                        .where(eq(property("ous", EplOrgUnitSummary.id()), value(orgUnitSummaryId)))
                        .where(in(property("gr", EplEduGroupRow.group().id()), new DQLSelectBuilder().fromEntity(EplEduGroupTimeItem.class, "gti")
                                .where(eq(property("gti", EplEduGroupTimeItem.summary().id()), value(orgUnitSummaryId)))
                                .column(property("gti", EplEduGroupTimeItem.eduGroup().id()))
                                .buildQuery()));
//
                if(ppsId != null)
                {
                    DQLSelectBuilder subSubBuilder = new DQLSelectBuilder().fromEntity(EplEduGroupTimeItem.class, "ti")
                            .joinEntity("ti", DQLJoinType.inner, EplPlannedPpsTimeItem.class, "pti", eq(property("pti", EplPlannedPpsTimeItem.timeItem()), property("ti")))
                            .where(eq(property("ti", EplEduGroupTimeItem.summary().id()), value(orgUnitSummaryId)))
                            .where(eq(property("gr", EplEduGroupRow.group()), property("ti", EplEduGroupTimeItem.eduGroup())))
                            .where(eq(property("pti", EplPlannedPpsTimeItem.pps().id()), value(ppsId)));
                    subBuilder.where(exists(subSubBuilder.buildQuery()));
                }

                if(loadTypeId != null)
                    subBuilder.where(eq(property("gr", EplEduGroupRow.group().groupType().id()), value(loadTypeId)));

                if(courseId != null)
                    subBuilder.where(eq(property("gr", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().group().course().id()), value(courseId)));

                if(developFormId != null)
                    subBuilder.where(eq(property("gr", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().educationOrgUnit().developForm().id()), value(developFormId)));

                dql.where(in(property("e", EducationOrgUnit.id()), subBuilder.buildQuery()));
            }
        }
                .filter(EducationOrgUnit.educationLevelHighSchool().fullTitle())
                .order(EducationOrgUnit.educationLevelHighSchool().fullTitle())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler timeRuleDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EplTimeRuleEduLoad.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long orgUnitSummaryId = context.getNotNull("orgUnitSummaryId");
                Long ppsId = context.get("pps");
                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EplSimpleTimeItem.class, "ti")
                        .column(property("ti", EplSimpleTimeItem.timeRule().id()))
                        .where(eq(property("ti", EplSimpleTimeItem.summary().id()), value(orgUnitSummaryId)));
                if(ppsId != null)
                    subBuilder.joinEntity("ti", DQLJoinType.inner, EplPlannedPpsTimeItem.class, "pti", eq(property("pti", EplPlannedPpsTimeItem.timeItem()), property("ti")))
                            .where(eq(property("pti", EplPlannedPpsTimeItem.pps().id()), value(ppsId)));

                dql.where(in(property("e", EplTimeRuleEduLoad.id()), subBuilder.buildQuery()));
            }
        }
                .filter(EplTimeRuleEduLoad.title())
                .order(EplTimeRuleEduLoad.title());
    }

}



