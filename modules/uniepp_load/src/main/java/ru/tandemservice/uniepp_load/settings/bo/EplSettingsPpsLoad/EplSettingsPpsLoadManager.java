package ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.logic.EplSettingsPpsLoadDao;
import ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.logic.IEplSettingsPpsLoadDao;

/**
 * @author Alexey Lopatin
 * @since 18.05.2015
 */
@Configuration
public class EplSettingsPpsLoadManager extends BusinessObjectManager
{
    public static EplSettingsPpsLoadManager instance()
    {
        return instance(EplSettingsPpsLoadManager.class);
    }

    @Bean
    public IEplSettingsPpsLoadDao dao()
    {
        return new EplSettingsPpsLoadDao();
    }
}
