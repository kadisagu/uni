/* $Id$ */
package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.ResultSet;

/**
 * @author Ekaterina Zvereva
 * @since 18.08.2016
 */
public class MS_uniepp_load_2x10x6_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.6")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        short entityCode = tool.entityCodes().ensure("eplTimeCategoryNonEduLoad");
        tool.executeUpdate("update epl_time_cat_t set code_p=" + tool.createStringConcatenationSQL("'non_edu.'", "code_p") +
                                       "where discriminator = ? and code_p in('1','2','3','4','5')", entityCode);

        entityCode = tool.entityCodes().ensure("eplTimeRuleNonEduLoad");
        tool.executeUpdate("update epl_time_rule_t set code_p=" + tool.createStringConcatenationSQL("'nonEduRule.'", "code_p") +
                                       "where discriminator = ? and code_p in('1','2','3','4','5')", entityCode);
    }
}