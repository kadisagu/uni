/* $Id:$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic;

import ru.tandemservice.uni.util.groovy.IScriptInstance;

/**
 * @author oleyba
 * @since 11/28/14
 */
public interface IEplTimeRuleScript extends IScriptInstance
{
    IEplTimeRuleDao.IRuleResult count(IEplTimeRuleDao.ILoadData loadData);
}
