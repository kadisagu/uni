package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;

import static ru.tandemservice.uniepp_load.catalog.entity.codes.EplTimeItemVariantGroupingCodes.*;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_load_2x10x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eplTimeItemVariantGrouping

		short entityCode;
		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epl_time_item_variant_group_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_epltimeitemvariantgrouping"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("shorttitle_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			entityCode = tool.entityCodes().ensure("eplTimeItemVariantGrouping");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eplTimeRule

		// создано обязательное свойство grouping
		{
			// создать колонку
			tool.createColumn("epl_time_rule_t", new DBColumn("grouping_id", DBType.LONG));

			// задать значение по умолчанию
			PreparedStatement insert = tool.prepareStatement("insert into epl_time_item_variant_group_t (id, discriminator, code_p, title_p, shorttitle_p) values (?, ?, ?, ?, ?)");

			String[][] columns = {
					{BY_REG_ELEMENT, "По мероприятию реестра", "по мероприятию"},
					{BY_TIME_RULE, "По норме времени", "по норме"},
					{BY_REG_ELEMENT_AND_TIME_RULE, "По мероприятию реестра и норме времени", "по мероприятию и норме"}
			};

			Long byTimeRuleId = null;
			Long byRegElementId = null;

			for (String[] column : columns)
			{
				Long id = EntityIDGenerator.generateNewId(entityCode);
				String code = column[0];

				if (code.equals(BY_REG_ELEMENT))
					byRegElementId = id;
				else if (code.equals(BY_TIME_RULE))
					byTimeRuleId = id;

				insert.setLong(1, id);
				insert.setShort(2, entityCode);
				insert.setString(3, code);
				insert.setString(4, column[1]);
				insert.setString(5, column[2]);
				insert.executeUpdate();
			}

			tool.executeUpdate("update epl_time_rule_t set grouping_id = ? where catalogcode_p = ?", byTimeRuleId, "eplTimeRuleSimple");
			tool.executeUpdate("update epl_time_rule_t set grouping_id = ? where catalogcode_p <> ?", byRegElementId, "eplTimeRuleSimple");

			// сделать колонку NOT NULL
			tool.setColumnNullable("epl_time_rule_t", "grouping_id", false);
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eplTimeRuleEduGroupScript

		// создано свойство regStructure
		{
			// создать колонку
			tool.createColumn("epl_time_rule_edugrp_base_t", new DBColumn("regstructure_id", DBType.LONG));
		}
    }
}