/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.NonEduLoad;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.EplIndividualPlanManager;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.logic.wrapper.EplNonEduLoadCommonWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.logic.wrapper.EplNonEduLoadRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.NonEduLoadAddEdit.EplIndividualPlanNonEduLoadAddEdit;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.ui.NonEduLoadAddEdit.EplIndividualPlanNonEduLoadAddEditUI;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan;

/**
 * @author Alexey Lopatin
 * @since 03.08.2016
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required = true),
        @Bind(key = EplIndividualPlanManager.PARAM_PPS_ACTIVE, binding = "ppsActive")
})
public class EplIndividualPlanNonEduLoadUI extends UIPresenter
{
    public static final String PERMISSION_KEY = "eplIndividualPlanNonEduLoadTab";

    private EntityHolder<EplIndividualPlan> _holder = new EntityHolder<>();
    private boolean _ppsActive;

    private EplNonEduLoadCommonWrapper _dataWrapper;
    private EplNonEduLoadCommonWrapper.TitleColumn _currentColumn;
    private EplNonEduLoadRowWrapper _currentRow;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        refreshData();
    }

    private void refreshData()
    {
        _dataWrapper = new EplNonEduLoadCommonWrapper(getIndPlan());
    }

    public void onClickAdd()
    {
        _uiActivation.asRegionDialog(EplIndividualPlanNonEduLoadAddEdit.class).parameter(UIPresenter.PUBLISHER_ID, getIndPlan().getId()).activate();
    }

    public void onClickEdit()
    {
        _uiActivation.asRegionDialog(EplIndividualPlanNonEduLoadAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getIndPlan().getId())
                .parameter(EplIndividualPlanNonEduLoadAddEditUI.PARAM_TIME_ITEM_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        refreshData();
    }

    public EplIndividualPlan getIndPlan()
    {
        return getHolder().getValue();
    }

    public String getNonEduLoadDeleteAlert()
    {
        return "Удалить нагрузку «" + getCurrentRow().getTitle() + "»?";
    }

    public String getCurrentRowParamValue()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getCurrentRow().getParamValue());
    }

    public String getCurrentRowTimeAmount()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(getCurrentRow().getTimeAmount());
    }

    public String getIndPlanNonEduLoadTabPermissionKey()
    {
        return EplIndividualPlanManager.getPermissionKeyView(PERMISSION_KEY, _ppsActive);
    }

    public String getIndPlanNonEduLoadTabAddPermissionKey()
    {
        return EplIndividualPlanManager.getPermissionKey(PERMISSION_KEY, "add", _ppsActive);
    }

    public String getIndPlanNonEduLoadTabEditPermissionKey()
    {
        return EplIndividualPlanManager.getPermissionKey(PERMISSION_KEY, "edit", _ppsActive);
    }

    public String getIndPlanNonEduLoadTabDeletePermissionKey()
    {
        return EplIndividualPlanManager.getPermissionKey(PERMISSION_KEY, "delete", _ppsActive);
    }

    public boolean isHasActionColumns()
    {
        return !getCurrentRow().isCategory() && !getCurrentRow().isTotal();
    }

    // getters and setters

    public EntityHolder<EplIndividualPlan> getHolder()
    {
        return _holder;
    }

    public boolean isPpsActive()
    {
        return _ppsActive;
    }

    public void setPpsActive(boolean ppsActive)
    {
        _ppsActive = ppsActive;
    }

    public EplNonEduLoadCommonWrapper getDataWrapper()
    {
        return _dataWrapper;
    }

    public EplNonEduLoadCommonWrapper.TitleColumn getCurrentColumn()
    {
        return _currentColumn;
    }

    public EplNonEduLoadRowWrapper getCurrentRow()
    {
        return _currentRow;
    }
}
