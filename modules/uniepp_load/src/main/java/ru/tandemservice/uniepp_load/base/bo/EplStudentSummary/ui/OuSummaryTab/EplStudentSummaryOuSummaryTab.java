/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.OuSummaryTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.EplOuSummaryDSHandler;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;

/**
 * @author oleyba
 * @since 5/5/12
 */
@Configuration
public class EplStudentSummaryOuSummaryTab extends BusinessComponentManager
{
    public static final String DS_OU_SUMMARY = "eplOuSummaryDS";
    public static final String CHECKBOX = "checkbox";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(DS_OU_SUMMARY, eplOuSummaryDSColumns(), eplOuSummaryDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS("readingOrgUnitDS", getName(), EplOrgUnitSummary.readingOrgUnitDSHandler(getName())))
                .create();
    }

    @Bean
    public ColumnListExtPoint eplOuSummaryDSColumns()
    {
        return columnListExtPointBuilder(DS_OU_SUMMARY)
                .addColumn(checkboxColumn(CHECKBOX))
                .addColumn(publisherColumn(EplOrgUnitSummary.L_ORG_UNIT, EplOrgUnitSummary.orgUnit().title()).order())
                .addColumn(textColumn(EplOrgUnitSummary.P_CREATION_DATE, EplOrgUnitSummary.creationDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(textColumn(EplOrgUnitSummary.L_STATE, EplOrgUnitSummary.state().title()).order())
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
                                   .alert(FormattedMessage.with().template("eplOuSummaryDS.deleteAlert").parameter(EplOrgUnitSummary.title()).create())
                                   .disabled("ui:deleteDisabled")
                                   .permissionKey("eplStudentSummaryPubOuSummaryTabDeleteSummary"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eplOuSummaryDSHandler()
    {
        return new EplOuSummaryDSHandler(getName());
    }
}
