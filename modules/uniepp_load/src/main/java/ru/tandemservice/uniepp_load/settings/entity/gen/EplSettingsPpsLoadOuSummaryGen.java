package ru.tandemservice.uniepp_load.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad;
import ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoadOuSummary;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Предельные значения нагрузки преподавателей (расчет)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplSettingsPpsLoadOuSummaryGen extends EplSettingsPpsLoad
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoadOuSummary";
    public static final String ENTITY_NAME = "eplSettingsPpsLoadOuSummary";
    public static final int VERSION_HASH = -370948878;
    private static IEntityMeta ENTITY_META;

    public static final String L_SUMMARY = "summary";

    private EplOrgUnitSummary _summary;     // Расчет на читающем подразделении

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Расчет на читающем подразделении. Свойство не может быть null.
     */
    @NotNull
    public EplOrgUnitSummary getSummary()
    {
        return _summary;
    }

    /**
     * @param summary Расчет на читающем подразделении. Свойство не может быть null.
     */
    public void setSummary(EplOrgUnitSummary summary)
    {
        dirty(_summary, summary);
        _summary = summary;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EplSettingsPpsLoadOuSummaryGen)
        {
            setSummary(((EplSettingsPpsLoadOuSummary)another).getSummary());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplSettingsPpsLoadOuSummaryGen> extends EplSettingsPpsLoad.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplSettingsPpsLoadOuSummary.class;
        }

        public T newInstance()
        {
            return (T) new EplSettingsPpsLoadOuSummary();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "summary":
                    return obj.getSummary();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "summary":
                    obj.setSummary((EplOrgUnitSummary) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "summary":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "summary":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "summary":
                    return EplOrgUnitSummary.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplSettingsPpsLoadOuSummary> _dslPath = new Path<EplSettingsPpsLoadOuSummary>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplSettingsPpsLoadOuSummary");
    }
            

    /**
     * @return Расчет на читающем подразделении. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoadOuSummary#getSummary()
     */
    public static EplOrgUnitSummary.Path<EplOrgUnitSummary> summary()
    {
        return _dslPath.summary();
    }

    public static class Path<E extends EplSettingsPpsLoadOuSummary> extends EplSettingsPpsLoad.Path<E>
    {
        private EplOrgUnitSummary.Path<EplOrgUnitSummary> _summary;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Расчет на читающем подразделении. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoadOuSummary#getSummary()
     */
        public EplOrgUnitSummary.Path<EplOrgUnitSummary> summary()
        {
            if(_summary == null )
                _summary = new EplOrgUnitSummary.Path<EplOrgUnitSummary>(L_SUMMARY, this);
            return _summary;
        }

        public Class getEntityClass()
        {
            return EplSettingsPpsLoadOuSummary.class;
        }

        public String getEntityName()
        {
            return "eplSettingsPpsLoadOuSummary";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
