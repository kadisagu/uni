/* $Id$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.ui.SimpleList;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.DynamicPub.CatalogDynamicPubUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleSimple;

/**
 * @author Alexey Lopatin
 * @since 31.03.2016
 */
@State({@Bind(key = DefaultCatalogPubModel.CATALOG_CODE, binding = "catalogCode")})
public class EplTimeRuleSimpleListUI extends CatalogDynamicPubUI
{
    @Override
    protected void addActionColumns(DynamicListDataSource<ICatalogItem> dataSource)
    {
        super.addActionColumns(dataSource);

        int columnIndex = dataSource.getColumn(EplTimeRuleSimple.P_DESCRIPTION).getNumber();
        dataSource.addColumn(new SimpleColumn("Группировка часов", EplTimeRuleSimple.grouping().shortTitle().s()).setWidth("110px").setClickable(false), ++columnIndex);
    }
}
