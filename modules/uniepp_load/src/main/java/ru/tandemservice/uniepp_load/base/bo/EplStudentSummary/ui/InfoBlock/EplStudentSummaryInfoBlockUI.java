/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.InfoBlock;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.support.IUIActivation;
import org.tandemframework.caf.ui.support.IUISupport;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Pub.EplStudentSummaryPub;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Pub.EplStudentSummaryPubUI;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 12.07.2016
 */
@Input({@Bind(key = EplStudentSummaryInfoBlockUI.PARAM_STUDENT_SUMMARY, binding = "studentSummary.id")})
public class EplStudentSummaryInfoBlockUI extends UIPresenter
{
    public static final String STUDENT_SUMMARY_INFO_BLOCK_REGION = "eplStudentSummaryInfoBlockRegion";

    public static final String PARAM_STUDENT_SUMMARY = "studentSummary";

    private EplStudentSummary _studentSummary = new EplStudentSummary();

    private boolean _wpCheck;
    private boolean _splitCheck;
    private boolean _eduGroupsCheck;

    @Override
    public void onComponentRefresh()
    {
        _studentSummary = _studentSummary == null ? null : DataAccessServices.dao().get(_studentSummary.getId());

        if (_studentSummary == null)
        {
            _wpCheck = false;
            _splitCheck = false;
            _eduGroupsCheck = false;
            return;
        }
        _wpCheck = ISharedBaseDao.instance.get().existsEntity(
                new DQLSelectBuilder().fromEntity(EplStudent.class, "s")
                        .where(eq(property("s", EplStudent.group().summary()), value(_studentSummary.getId())))
                        .where(or(
                                eq(property("s", EplStudent.errorCountWithWp()), value(Boolean.TRUE)),
                                eq(property("s", EplStudent.noWp()), value(Boolean.TRUE))
                        )).buildQuery()
        );

        _splitCheck = ISharedBaseDao.instance.get().existsEntity(EplStudentSummaryManager.instance().dao().getIncorrectSplitPairDql(_studentSummary).buildQuery());
        _splitCheck |= ISharedBaseDao.instance.get().existsEntity(EplStudentSummaryManager.instance().dao().getSplitEmptyPairDql(_studentSummary).buildQuery());

        _eduGroupsCheck = EplStudentSummaryManager.instance().dao().isNeedRefreshEduGroups(_studentSummary);
    }

    public void onChangeSummary(Long studentSummaryId)
    {
        _studentSummary = DataAccessServices.dao().getNotNull(studentSummaryId);
    }

    public static void componentActivate(IUISupport uiSupport, IUIActivation uiActivation, Long studentSummaryId)
    {
        EplStudentSummaryInfoBlockUI infoBlock = uiSupport.getChildUI(STUDENT_SUMMARY_INFO_BLOCK_REGION);
        if (null == infoBlock)
        {
            uiActivation.asRegion(EplStudentSummaryInfoBlock.class, STUDENT_SUMMARY_INFO_BLOCK_REGION)
                    .parameter(EplStudentSummaryInfoBlockUI.PARAM_STUDENT_SUMMARY, studentSummaryId)
                    .activate();
        }
        else
            infoBlock.onChangeSummary(studentSummaryId);
    }

    public EplStudentSummary getStudentSummary()
    {
        return _studentSummary;
    }

    public void setStudentSummary(EplStudentSummary studentSummary)
    {
        _studentSummary = studentSummary;
    }

    public String getComponentName()
    {
        return EplStudentSummaryPub.class.getSimpleName();
    }

    public ParametersMap getParameters(String tabName)
    {
        return new ParametersMap()
                .add(UIPresenter.PUBLISHER_ID, _studentSummary.getId())
                .add(EplStudentSummaryPubUI.SELECTED_TAB, tabName);
    }

    public boolean isHasErrors()
    {
        return isWpCheck() || isSplitCheck() || isEduGroupsCheck();
    }

    public boolean isWpCheck()
    {
        return _wpCheck;
    }

    public boolean isSplitCheck()
    {
        return _splitCheck;
    }

    public boolean isEduGroupsCheck()
    {
        return _eduGroupsCheck;
    }
}
