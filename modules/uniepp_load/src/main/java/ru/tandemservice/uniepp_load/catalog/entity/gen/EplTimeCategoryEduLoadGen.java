package ru.tandemservice.uniepp_load.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategory;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Категория времени учебной нагрузки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplTimeCategoryEduLoadGen extends EplTimeCategory
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad";
    public static final String ENTITY_NAME = "eplTimeCategoryEduLoad";
    public static final int VERSION_HASH = 1901541130;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EplTimeCategoryEduLoadGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplTimeCategoryEduLoadGen> extends EplTimeCategory.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplTimeCategoryEduLoad.class;
        }

        public T newInstance()
        {
            return (T) new EplTimeCategoryEduLoad();
        }
    }
    private static final Path<EplTimeCategoryEduLoad> _dslPath = new Path<EplTimeCategoryEduLoad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplTimeCategoryEduLoad");
    }
            

    public static class Path<E extends EplTimeCategoryEduLoad> extends EplTimeCategory.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EplTimeCategoryEduLoad.class;
        }

        public String getEntityName()
        {
            return "eplTimeCategoryEduLoad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
