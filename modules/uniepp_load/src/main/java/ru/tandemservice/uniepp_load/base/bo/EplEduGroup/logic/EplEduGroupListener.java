package ru.tandemservice.uniepp_load.base.bo.EplEduGroup.logic;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.bo.DaemonQueue.DaemonQueueManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.daemon.EplTimeItemDaemonBean;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;

import javax.transaction.Synchronization;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Created by nsvetlov on 24.11.2016.
 */
public class EplEduGroupListener implements IDSetEventListener
{

    public static final String SUMMARY = "s";
    public static final String ORG_UNIT = "ou";
    public static final String GROUP = "g";

    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, EplEduGroup.class, this);
    }

    @Override
    public void onEvent(DSetEvent dSetEvent)
    {
        final List<Long> ids = new DQLSelectBuilder()
                .fromEntity(EplOrgUnitSummary.class, SUMMARY)
                .joinPath(DQLJoinType.inner, EplOrgUnitSummary.orgUnit().fromAlias(SUMMARY), ORG_UNIT)
                .joinEntity("s", DQLJoinType.inner, EplEduGroup.class, GROUP,
                        eq(property(SUMMARY, EplOrgUnitSummary.studentSummary()), property(GROUP, EplEduGroup.summary())))
                .where(in(property(GROUP + ".id"), dSetEvent.getMultitude().getInExpression()))
                .where(eq(property(ORG_UNIT), property(GROUP, EplEduGroup.registryElementPart().registryElement().owner())))
                .column(SUMMARY + ".id")
                .distinct()
                .createStatement(dSetEvent.getContext())
                .list();
        Synchronization sync = new Synchronization()
        {
            @SuppressWarnings("unchecked")
            @Override
            public void beforeCompletion()
            {
                DaemonQueueManager.instance().addIds(ids, true);
                EplTimeItemDaemonBean.DAEMON.registerAfterCompleteWakeUp(dSetEvent);
            }

            @Override
            public void afterCompletion(int status)
            {
            }
        };
        dSetEvent.getContext().getTransaction().registerSynchronization(sync);
    }
}
