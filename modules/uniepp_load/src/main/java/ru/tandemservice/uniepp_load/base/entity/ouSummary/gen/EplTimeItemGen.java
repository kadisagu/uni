package ru.tandemservice.uniepp_load.base.entity.ouSummary.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Часы нагрузки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplTimeItemGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem";
    public static final String ENTITY_NAME = "eplTimeItem";
    public static final int VERSION_HASH = -1499622682;
    private static IEntityMeta ENTITY_META;

    public static final String L_SUMMARY = "summary";
    public static final String L_TIME_RULE = "timeRule";
    public static final String P_TIME_AMOUNT_AS_LONG = "timeAmountAsLong";
    public static final String P_CONTROL_TIME_AMOUNT_AS_LONG = "controlTimeAmountAsLong";
    public static final String P_CREATION_DATE = "creationDate";
    public static final String P_DESCRIPTION = "description";
    public static final String P_TIME_AMOUNT_STR = "timeAmountStr";

    private EplOrgUnitSummary _summary;     // Расчет на читающем подразделении
    private EplTimeRuleEduLoad _timeRule;     // Норма времени
    private long _timeAmountAsLong;     // Число часов
    private long _controlTimeAmountAsLong;     // Контрольное число часов
    private Date _creationDate;     // Дата создания объекта
    private String _description;     // Описание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Расчет на читающем подразделении. Свойство не может быть null.
     */
    @NotNull
    public EplOrgUnitSummary getSummary()
    {
        return _summary;
    }

    /**
     * @param summary Расчет на читающем подразделении. Свойство не может быть null.
     */
    public void setSummary(EplOrgUnitSummary summary)
    {
        dirty(_summary, summary);
        _summary = summary;
    }

    /**
     * @return Норма времени. Свойство не может быть null.
     */
    @NotNull
    public EplTimeRuleEduLoad getTimeRule()
    {
        return _timeRule;
    }

    /**
     * @param timeRule Норма времени. Свойство не может быть null.
     */
    public void setTimeRule(EplTimeRuleEduLoad timeRule)
    {
        dirty(_timeRule, timeRule);
        _timeRule = timeRule;
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Число часов. Свойство не может быть null.
     */
    @NotNull
    public long getTimeAmountAsLong()
    {
        return _timeAmountAsLong;
    }

    /**
     * @param timeAmountAsLong Число часов. Свойство не может быть null.
     */
    public void setTimeAmountAsLong(long timeAmountAsLong)
    {
        dirty(_timeAmountAsLong, timeAmountAsLong);
        _timeAmountAsLong = timeAmountAsLong;
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Контрольное число часов. Свойство не может быть null.
     */
    @NotNull
    public long getControlTimeAmountAsLong()
    {
        return _controlTimeAmountAsLong;
    }

    /**
     * @param controlTimeAmountAsLong Контрольное число часов. Свойство не может быть null.
     */
    public void setControlTimeAmountAsLong(long controlTimeAmountAsLong)
    {
        dirty(_controlTimeAmountAsLong, controlTimeAmountAsLong);
        _controlTimeAmountAsLong = controlTimeAmountAsLong;
    }

    /**
     * @return Дата создания объекта. Свойство не может быть null.
     */
    @NotNull
    public Date getCreationDate()
    {
        return _creationDate;
    }

    /**
     * @param creationDate Дата создания объекта. Свойство не может быть null.
     */
    public void setCreationDate(Date creationDate)
    {
        dirty(_creationDate, creationDate);
        _creationDate = creationDate;
    }

    /**
     * @return Описание.
     */
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Описание.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EplTimeItemGen)
        {
            setSummary(((EplTimeItem)another).getSummary());
            setTimeRule(((EplTimeItem)another).getTimeRule());
            setTimeAmountAsLong(((EplTimeItem)another).getTimeAmountAsLong());
            setControlTimeAmountAsLong(((EplTimeItem)another).getControlTimeAmountAsLong());
            setCreationDate(((EplTimeItem)another).getCreationDate());
            setDescription(((EplTimeItem)another).getDescription());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplTimeItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplTimeItem.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EplTimeItem is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "summary":
                    return obj.getSummary();
                case "timeRule":
                    return obj.getTimeRule();
                case "timeAmountAsLong":
                    return obj.getTimeAmountAsLong();
                case "controlTimeAmountAsLong":
                    return obj.getControlTimeAmountAsLong();
                case "creationDate":
                    return obj.getCreationDate();
                case "description":
                    return obj.getDescription();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "summary":
                    obj.setSummary((EplOrgUnitSummary) value);
                    return;
                case "timeRule":
                    obj.setTimeRule((EplTimeRuleEduLoad) value);
                    return;
                case "timeAmountAsLong":
                    obj.setTimeAmountAsLong((Long) value);
                    return;
                case "controlTimeAmountAsLong":
                    obj.setControlTimeAmountAsLong((Long) value);
                    return;
                case "creationDate":
                    obj.setCreationDate((Date) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "summary":
                        return true;
                case "timeRule":
                        return true;
                case "timeAmountAsLong":
                        return true;
                case "controlTimeAmountAsLong":
                        return true;
                case "creationDate":
                        return true;
                case "description":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "summary":
                    return true;
                case "timeRule":
                    return true;
                case "timeAmountAsLong":
                    return true;
                case "controlTimeAmountAsLong":
                    return true;
                case "creationDate":
                    return true;
                case "description":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "summary":
                    return EplOrgUnitSummary.class;
                case "timeRule":
                    return EplTimeRuleEduLoad.class;
                case "timeAmountAsLong":
                    return Long.class;
                case "controlTimeAmountAsLong":
                    return Long.class;
                case "creationDate":
                    return Date.class;
                case "description":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplTimeItem> _dslPath = new Path<EplTimeItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplTimeItem");
    }
            

    /**
     * @return Расчет на читающем подразделении. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem#getSummary()
     */
    public static EplOrgUnitSummary.Path<EplOrgUnitSummary> summary()
    {
        return _dslPath.summary();
    }

    /**
     * @return Норма времени. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem#getTimeRule()
     */
    public static EplTimeRuleEduLoad.Path<EplTimeRuleEduLoad> timeRule()
    {
        return _dslPath.timeRule();
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem#getTimeAmountAsLong()
     */
    public static PropertyPath<Long> timeAmountAsLong()
    {
        return _dslPath.timeAmountAsLong();
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Контрольное число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem#getControlTimeAmountAsLong()
     */
    public static PropertyPath<Long> controlTimeAmountAsLong()
    {
        return _dslPath.controlTimeAmountAsLong();
    }

    /**
     * @return Дата создания объекта. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem#getCreationDate()
     */
    public static PropertyPath<Date> creationDate()
    {
        return _dslPath.creationDate();
    }

    /**
     * @return Описание.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem#getTimeAmountStr()
     */
    public static SupportedPropertyPath<String> timeAmountStr()
    {
        return _dslPath.timeAmountStr();
    }

    public static class Path<E extends EplTimeItem> extends EntityPath<E>
    {
        private EplOrgUnitSummary.Path<EplOrgUnitSummary> _summary;
        private EplTimeRuleEduLoad.Path<EplTimeRuleEduLoad> _timeRule;
        private PropertyPath<Long> _timeAmountAsLong;
        private PropertyPath<Long> _controlTimeAmountAsLong;
        private PropertyPath<Date> _creationDate;
        private PropertyPath<String> _description;
        private SupportedPropertyPath<String> _timeAmountStr;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Расчет на читающем подразделении. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem#getSummary()
     */
        public EplOrgUnitSummary.Path<EplOrgUnitSummary> summary()
        {
            if(_summary == null )
                _summary = new EplOrgUnitSummary.Path<EplOrgUnitSummary>(L_SUMMARY, this);
            return _summary;
        }

    /**
     * @return Норма времени. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem#getTimeRule()
     */
        public EplTimeRuleEduLoad.Path<EplTimeRuleEduLoad> timeRule()
        {
            if(_timeRule == null )
                _timeRule = new EplTimeRuleEduLoad.Path<EplTimeRuleEduLoad>(L_TIME_RULE, this);
            return _timeRule;
        }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem#getTimeAmountAsLong()
     */
        public PropertyPath<Long> timeAmountAsLong()
        {
            if(_timeAmountAsLong == null )
                _timeAmountAsLong = new PropertyPath<Long>(EplTimeItemGen.P_TIME_AMOUNT_AS_LONG, this);
            return _timeAmountAsLong;
        }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Контрольное число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem#getControlTimeAmountAsLong()
     */
        public PropertyPath<Long> controlTimeAmountAsLong()
        {
            if(_controlTimeAmountAsLong == null )
                _controlTimeAmountAsLong = new PropertyPath<Long>(EplTimeItemGen.P_CONTROL_TIME_AMOUNT_AS_LONG, this);
            return _controlTimeAmountAsLong;
        }

    /**
     * @return Дата создания объекта. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem#getCreationDate()
     */
        public PropertyPath<Date> creationDate()
        {
            if(_creationDate == null )
                _creationDate = new PropertyPath<Date>(EplTimeItemGen.P_CREATION_DATE, this);
            return _creationDate;
        }

    /**
     * @return Описание.
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(EplTimeItemGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem#getTimeAmountStr()
     */
        public SupportedPropertyPath<String> timeAmountStr()
        {
            if(_timeAmountStr == null )
                _timeAmountStr = new SupportedPropertyPath<String>(EplTimeItemGen.P_TIME_AMOUNT_STR, this);
            return _timeAmountStr;
        }

        public Class getEntityClass()
        {
            return EplTimeItem.class;
        }

        public String getEntityName()
        {
            return "eplTimeItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTimeAmountStr();
}
