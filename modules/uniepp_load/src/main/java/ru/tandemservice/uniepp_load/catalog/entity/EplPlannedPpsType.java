package ru.tandemservice.uniepp_load.catalog.entity;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplPlannedPpsTypeCodes;
import ru.tandemservice.uniepp_load.catalog.entity.gen.*;

/** @see ru.tandemservice.uniepp_load.catalog.entity.gen.EplPlannedPpsTypeGen */
public class EplPlannedPpsType extends EplPlannedPpsTypeGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{
    public boolean isStaff() { return EplPlannedPpsTypeCodes.STAFF.equals(getCode()); }
    public boolean isVacancyAsStaff() { return EplPlannedPpsTypeCodes.VACANCY_AS_STAFF.equals(getCode()); }
    public boolean isTimeWorker() { return EplPlannedPpsTypeCodes.TIMEWORKER.equals(getCode()); }
    public boolean isVacancyAsTimeWorker() { return EplPlannedPpsTypeCodes.VACANCY_AS_TIMEWORKER.equals(getCode()); }

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EplPlannedPpsType.class)
                .titleProperty(EplPlannedPpsType.title().s())
                .order(EplPlannedPpsType.priority())
                .filter(EplPlannedPpsType.title());
    }
}