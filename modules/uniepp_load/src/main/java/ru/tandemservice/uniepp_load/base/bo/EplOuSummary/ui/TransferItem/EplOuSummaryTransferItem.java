/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.TransferItem;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCommonParentWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCommonRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.TransferItem.logic.EplOuSummaryTransferTimeItemDSHandler;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTransferOrgUnitEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.TransferItem.logic.EplOuSummaryTransferTimeItemDSHandler.*;

/**
 * @author Alexey Lopatin
 * @since 30.09.2015
 */
@Configuration
public class EplOuSummaryTransferItem extends BusinessComponentManager
{
    public static final String DS_POSSIBLE_TRANSFER_TIME_ITEM = "possibleTransferTimeItemDS";
    public static final String DS_TRANSFER_TIME_ITEM = "transferTimeItemDS";

    public static final String DS_REG_ELEMENT_PART_OR_RULE = "regElementPartOrRuleDS";
    public static final String DS_YEAR_PART = "yearPartDS";
    public static final String DS_COURSE = "courseDS";
    public static final String DS_GROUP = "groupDS";
    public static final String DS_READING_ORG_UNIT = "readingOrgUnitDS";

    public static final String BIND_TRANSFER_TIME_ITEM_MAP = "transferTimeItemMap";
    public static final String BIND_DISC_OR_RULE = "discOrRule";
    public static final String BIND_CATEGORIES = "categories";
    public static final String BIND_TRANSFER_CATEGORIES = "transferCategories";
    public static final String BIND_ORG_UNIT_SUMMARY = "orgUnitSummary";
    public static final String BIND_YEAR_PART = "yearPart";
    public static final String BIND_COURSE = "course";
    public static final String BIND_GROUP = "group";
    public static final String BIND_READING_ORG_UNIT = "readingOrgUnit";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_REG_ELEMENT_PART_OR_RULE, getName(), EplTimeItem.defaultSelectWithoutSimpleDSHandler(getName()))
                                       .primaryKeyProperty("fieldId")
                                       .treeable(true)
                                       .valueStyleSource(value -> {
                                           final boolean disabled = value instanceof EplCommonParentWrapper;
                                           return EducationCatalogsManager.getDefaultValueStyleSource(disabled);
                                       }))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_YEAR_PART, getName(), YearDistributionPart.defaultSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> {
                            DQLSelectBuilder from = createBuilder(context).column(property("r", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().yearPart().id()));

                            EplCommonRowWrapper discOrRule = context.get(BIND_DISC_OR_RULE);
                            if (null != discOrRule)
                            {
                                FilterUtils.applySelectFilter(from, "i", EplEduGroupTimeItem.registryElementPart().id(), discOrRule.getKey().getDisciplineId());
                                FilterUtils.applySelectFilter(from, "i", EplEduGroupTimeItem.timeRule().id().s(), discOrRule.getKey().getRuleId());
                            }
                            return dql.where(in(property(alias, "id"), from.buildQuery()));
                        })).treeable(true).valueStyleSource(EducationCatalogsManager.getYearPartValueStyleSource()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_COURSE, getName(), Course.defaultSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> {
                            DQLSelectBuilder from = createBuilder(context).column(property("r", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().group().course().id()));

                            EplCommonRowWrapper discOrRule = context.get(BIND_DISC_OR_RULE);
                            if (null != discOrRule)
                            {
                                FilterUtils.applySelectFilter(from, "i", EplEduGroupTimeItem.registryElementPart().id(), discOrRule.getKey().getDisciplineId());
                                FilterUtils.applySelectFilter(from, "i", EplEduGroupTimeItem.timeRule().id().s(), discOrRule.getKey().getRuleId());
                            }
                            FilterUtils.applySelectFilter(from, "r", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().yearPart(), context.get(BIND_YEAR_PART));
                            return dql.where(in(property(alias, "id"), from.buildQuery()));
                        })))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_GROUP, getName(), EplGroup.defaultSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> {
                            DQLSelectBuilder from = createBuilder(context).column(property("r", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().group().id()));

                            EplCommonRowWrapper discOrRule = context.get(BIND_DISC_OR_RULE);
                            if (null != discOrRule)
                            {
                                FilterUtils.applySelectFilter(from, "i", EplEduGroupTimeItem.registryElementPart().id(), discOrRule.getKey().getDisciplineId());
                                FilterUtils.applySelectFilter(from, "i", EplEduGroupTimeItem.timeRule().id().s(), discOrRule.getKey().getRuleId());
                            }
                            FilterUtils.applySelectFilter(from, "r", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().yearPart(), context.get(BIND_YEAR_PART));
                            FilterUtils.applySelectFilter(from, "r", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().group().course(), context.get(BIND_COURSE));
                            return dql.where(in(property(alias, "id"), from.buildQuery()));
                        })))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_READING_ORG_UNIT, getName(), EplOrgUnitSummary.readingOrgUnitDSHandler(getName())
                        .customize((alias, dql, context, filter) -> {
                            DQLSelectBuilder from = createBuilder(context).column(property("i.id"));
                            EplOrgUnitSummary summary = context.get(BIND_ORG_UNIT_SUMMARY);
                            EplCommonRowWrapper discOrRule = context.get(BIND_DISC_OR_RULE);

                            if (null != discOrRule)
                            {
                                FilterUtils.applySelectFilter(from, "i", EplEduGroupTimeItem.registryElementPart().id(), discOrRule.getKey().getDisciplineId());
                                FilterUtils.applySelectFilter(from, "i", EplEduGroupTimeItem.timeRule().id().s(), discOrRule.getKey().getRuleId());
                            }
                            FilterUtils.applySelectFilter(from, "r", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().yearPart(), context.get(BIND_YEAR_PART));
                            FilterUtils.applySelectFilter(from, "r", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().group().course(), context.get(BIND_COURSE));
                            FilterUtils.applySelectFilter(from, "r", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().group(), context.get(BIND_GROUP));

                            DQLSelectBuilder builder = new DQLSelectBuilder()
                                    .column(property("s", EplOrgUnitSummary.orgUnit().id())).distinct()
                                    .fromEntity(EplOrgUnitSummary.class, "s")
                                    .fromEntity(EplEduGroupTimeItem.class, "ti")
                                    .where(ne(property("s", EplOrgUnitSummary.orgUnit().id()), value(summary.getOrgUnit().getId())))
                                    .where(eq(property("s", EplOrgUnitSummary.studentSummary().id()), property("ti", EplEduGroupTimeItem.summary().studentSummary().id())))
                                    .where(eq(property("s", EplOrgUnitSummary.state().code()), value(EppState.STATE_FORMATIVE)))
                                    .where(in(property("ti.id"), from.buildQuery()));

                            return dql.where(in(property(alias, "id"), builder.buildQuery()));
                        })))
                .addDataSource(searchListDS(DS_POSSIBLE_TRANSFER_TIME_ITEM, possibleTransferTimeItemDSColumns(), possibleTransferTimeItemDSHandler()))
                .addDataSource(searchListDS(DS_TRANSFER_TIME_ITEM, transferTimeItemDSColumns(), transferTimeItemDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint possibleTransferTimeItemDSColumns()
    {
        return columnListExtPointBuilder(DS_POSSIBLE_TRANSFER_TIME_ITEM)
                .addColumn(checkboxColumn("select").disabled("ui:currentTransferTimeItemDisabled"))
                .addColumn(textColumn(EplEduGroupTimeItem.eduGroup().groupType().s(), EplEduGroupTimeItem.eduGroup().groupType().abbreviation()).order())
                .addColumn(textColumn(EplEduGroupTimeItem.L_EDU_GROUP, EplEduGroupTimeItem.eduGroup().title()).order())
                .addColumn(textColumn(VIEW_ACADEMIC_GROUP, VIEW_ACADEMIC_GROUP).formatter(CollectionFormatter.ROW_FORMATTER))
                .addColumn(textColumn(VIEW_COURSE, VIEW_COURSE).formatter(CollectionFormatter.ROW_FORMATTER))
                .addColumn(textColumn(VIEW_TERM, VIEW_TERM).formatter(CollectionFormatter.ROW_FORMATTER))
                .addColumn(textColumn(VIEW_YEAR_PART, VIEW_YEAR_PART).formatter(CollectionFormatter.ROW_FORMATTER))
                .addColumn(textColumn(VIEW_AMOUNT, VIEW_AMOUNT).formatter(RawFormatter.INSTANCE))
                .addColumn(textColumn(EplEduGroupTimeItem.L_REGISTRY_ELEMENT_PART, EplEduGroupTimeItem.registryElementPart().shortTitle()).visible("ui:hasTimeRule"))
                .addColumn(textColumn(EplEduGroupTimeItem.timeRule().category().s(), EplEduGroupTimeItem.timeRule().category().shortTitle()))
                .addColumn(textColumn(EplEduGroupTimeItem.timeRule().title().s(), EplEduGroupTimeItem.timeRule().title()).visible("ui:hasDiscipline"))
                .addColumn(textColumn(EplEduGroupTimeItem.timeRule().description().s(), EplEduGroupTimeItem.timeRule().description()).visible("ui:hasDiscipline").width("400px"))
                .addColumn(textColumn(EplEduGroupTimeItem.P_DESCRIPTION, EplEduGroupTimeItem.description()).width("400px"))
                .addColumn(textColumn(EplEduGroupTimeItem.P_CREATION_DATE, EplEduGroupTimeItem.creationDate()).order().formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .create();
    }

    @Bean
    public ColumnListExtPoint transferTimeItemDSColumns()
    {
        return columnListExtPointBuilder(DS_TRANSFER_TIME_ITEM)
                .addColumn(textColumn(EplEduGroupTimeItem.eduGroup().groupType().s(), EplEduGroupTimeItem.eduGroup().groupType().abbreviation()))
                .addColumn(textColumn(EplEduGroupTimeItem.L_EDU_GROUP, EplEduGroupTimeItem.eduGroup().title()).order())
                .addColumn(textColumn(VIEW_ACADEMIC_GROUP, VIEW_ACADEMIC_GROUP).formatter(CollectionFormatter.ROW_FORMATTER))
                .addColumn(textColumn(VIEW_COURSE, VIEW_COURSE).formatter(CollectionFormatter.ROW_FORMATTER))
                .addColumn(textColumn(VIEW_TERM, VIEW_TERM).formatter(CollectionFormatter.ROW_FORMATTER))
                .addColumn(textColumn(VIEW_YEAR_PART, VIEW_YEAR_PART).formatter(CollectionFormatter.ROW_FORMATTER))
                .addColumn(textColumn(VIEW_AMOUNT, VIEW_AMOUNT).formatter(RawFormatter.INSTANCE))
                .addColumn(textColumn(EplEduGroupTimeItem.L_REGISTRY_ELEMENT_PART, EplEduGroupTimeItem.registryElementPart().shortTitle()).visible("ui:hasTimeRule"))
                .addColumn(textColumn(EplEduGroupTimeItem.timeRule().category().s(), EplEduGroupTimeItem.timeRule().category().shortTitle()))
                .addColumn(textColumn(EplEduGroupTimeItem.timeRule().title().s(), EplEduGroupTimeItem.timeRule().title()).visible("ui:hasDiscipline"))
                .addColumn(textColumn(EplEduGroupTimeItem.timeRule().description().s(), EplEduGroupTimeItem.timeRule().description()).visible("ui:hasDiscipline").width("400px"))
                .addColumn(textColumn(EplEduGroupTimeItem.P_DESCRIPTION, EplEduGroupTimeItem.description()).width("400px"))
                .addColumn(textColumn(EplEduGroupTimeItem.P_CREATION_DATE, EplEduGroupTimeItem.creationDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .addColumn(textColumn(EplEduGroupTimeItem.L_SUMMARY, EplTransferOrgUnitEduGroupTimeItem.summary().orgUnit().shortTitle()))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDeleteTransferRow").alert(FormattedMessage.with().template(DS_TRANSFER_TIME_ITEM + ".delete.alert").create()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> possibleTransferTimeItemDSHandler()
    {
        return new EplOuSummaryTransferTimeItemDSHandler(getName(), true);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> transferTimeItemDSHandler()
    {
        return new EplOuSummaryTransferTimeItemDSHandler(getName(), false);
    }

    private DQLSelectBuilder createBuilder(ExecutionContext context)
    {
        return new DQLSelectBuilder()
                .fromEntity(EplEduGroupRow.class, "r")
                .fromEntity(EplEduGroupTimeItem.class, "i")
                .where(eq(property("r", EplEduGroupRow.group()), property("i", EplEduGroupTimeItem.eduGroup())))
                .where(eq(property("i", EplEduGroupTimeItem.summary()), value(context.<IEntity>get(BIND_ORG_UNIT_SUMMARY))));
    }
}
