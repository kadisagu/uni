package ru.tandemservice.uniepp_load.catalog.entity;

import com.google.common.collect.ImmutableSet;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.ui.SimpleAddEdit.EplTimeRuleSimpleAddEdit;
import ru.tandemservice.uniepp_load.catalog.entity.gen.EplTimeRuleSimpleGen;

import java.util.Collection;
import java.util.Set;

/** @see ru.tandemservice.uniepp_load.catalog.entity.gen.EplTimeRuleSimpleGen */
public class EplTimeRuleSimple extends EplTimeRuleSimpleGen
{
    private static Set<String> HIDDEN_PROPERTIES = ImmutableSet.of(L_GROUPING);

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public String getAddEditComponentName() { return EplTimeRuleSimpleAddEdit.class.getSimpleName(); }
            @Override public Collection<String> getHiddenFields() { return HIDDEN_PROPERTIES; }
        };
    }
}