package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.daemon;

import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author Alexey Lopatin
 * @since 28.01.2016
 */
public interface IEplTimeItemDaemonBean
{
    final SpringBeanCache<IEplTimeItemDaemonBean> instance = new SpringBeanCache<>(IEplTimeItemDaemonBean.class.getName());

    /**
     * Проставляем контрольное число часов всем объектам расчета, вычисляя значения через формулы/скрипты
     * Если значение было получено, но рассчитанных часов нагрузки для План. потокаа нет, то создаем такие часы, иначе - обновляем текущие
     */
    //void doGenerateControlTimeAmount();
}
