/* $Id:$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic;

import org.tandemframework.core.debug.NoDebugSection;
import ru.tandemservice.uni.dao.IUniScriptDao;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupFormula;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript;

import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 11/28/14
 */
public interface IEplTimeRuleDao extends IUniScriptDao
{
    void doSaveUserScript(EplTimeRuleEduGroupScript catalogItem);

    /** Включает все счетчики скриптов **/
    void doAllCountersScriptActivate();

    /** Выключает все счетчики скриптов **/
    void doAllCountersScriptDeactivate();

    /**
     * Сбрасывает счетчик скрипта.
     * Если скрипт не указан явно, то сбрасываются все счетчики.
     * @param ruleId id скрипта
     */
    void doCountersScriptClear(Long ruleId);

    String getDefaultScriptText(EplTimeRuleEduGroupScript timeRule);

    interface IRuleResult {
        double hours();
        String description();
        long scriptTime();
    }

    interface IRuleResultEmpty extends IRuleResult {}

    interface ILoadData {
        EplEduGroup eduGroup();
        IEppRegElPartWrapper discData();
        Map<EplStudent, Integer> groupContent();
        List<EppIControlActionType> icaTypes();
    }

    @NoDebugSection
    IRuleResult count(EplTimeRuleEduGroupScript rule, IEplTimeRuleScript script, ILoadData loadData);

    @NoDebugSection
    IRuleResult count(EplTimeRuleEduGroupFormula timeRule, ILoadData loadData);

    /**
     * @param rule список норм времени План. потока (скрипт)
     * @return Возвращает набор скриптов по нормам времени
     */
    Map<EplTimeRuleEduGroupScript, IEplTimeRuleScript> getScriptWrapper4RuleMap(List<EplTimeRuleEduGroupScript> rule);
}