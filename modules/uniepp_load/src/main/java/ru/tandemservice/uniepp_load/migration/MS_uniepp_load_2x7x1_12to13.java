package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_load_2x7x1_12to13 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (tool.tableExists("epl_rep_hourly_fund_t")) return;

		////////////////////////////////////////////////////////////////////////////////
		// сущность eplReportHourlyFund

		// создана новая сущность
		{
            if(!tool.tableExists("epl_rep_hourly_fund_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("epl_rep_hourly_fund_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                        new DBColumn("studentsummary_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);
            }

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eplReportHourlyFund");

		}


    }
}