package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_load_2x7x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (tool.columnExists("epl_simple_time_item_t", "countascontract_p"))
            return;

		////////////////////////////////////////////////////////////////////////////////
		// сущность eplSimpleTimeItem

		// создано обязательное свойство countAsContract
		{
			// создать колонку
			tool.createColumn("epl_simple_time_item_t", new DBColumn("countascontract_p", DBType.BOOLEAN));

			// задать значение по умолчанию
            tool.executeUpdate("update epl_simple_time_item_t set countascontract_p=? where countascontract_p is null", Boolean.FALSE);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epl_simple_time_item_t", "countascontract_p", false);
		}
    }
}