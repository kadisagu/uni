/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupSplitEdit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.catalog.codes.EppEduGroupSplitVariantCodes;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitByLangElement;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBySexElement;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic.EplStudentSummarySplitElementDSHandler;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentCount4SplitElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Katkov
 * @since 06.04.2016
 */
@Configuration
public class EplStudentSummaryGroupSplitEdit extends BusinessComponentManager
{
    public static final String GROUP_SPLIT_VARIANT_DS = "groupSplitVariantDS";
    public static final String PARAM_STUDENT_2_WORK_PLAN = "student2WorkPlan";
    public static final String PARAM_SPLIT_VARIANT = "splitVariant";
    public static final String PARAM_SPLIT_ELEMENTS = "splitElements";
    public static final String PARAM_CURRENT_SPLIT_ELEMENT = "currentSplitElement";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(GROUP_SPLIT_VARIANT_DS, groupSplitVariantDSHandler()).addColumn(EppEduGroupSplitBaseElement.P_SPLIT_ELEMENT_TITLE))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler groupSplitVariantDSHandler()
    {
        return new EplStudentSummarySplitElementDSHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                String alias = "s";
                DQLSelectBuilder builder = getBuilder(alias, context);

                List<EplStudentCount4SplitElement> splitElements = new ArrayList<>(context.get(PARAM_SPLIT_ELEMENTS));
                EplStudentCount4SplitElement currentSplitElement = context.get(PARAM_CURRENT_SPLIT_ELEMENT);
                EppEduGroupSplitVariant splitVariant = context.get(PARAM_SPLIT_VARIANT);
                splitElements.remove(currentSplitElement);

                Set keys = input.getPrimaryKeys();
                if (CollectionUtils.isNotEmpty(keys)) {
                    builder.where(in(property(alias, "id"), keys));
                } else if (CollectionUtils.isNotEmpty(splitElements)) {
                    List<Long> splitElementIds = splitElements.stream().filter(o -> o.getSplitElement() != null).map(o -> o.getSplitElement().getId()).collect(Collectors.toList());
                    builder.where(notIn(property(alias, "id"), splitElementIds));
                }

                String filter = input.getComboFilterByValue();
                List<EppEduGroupSplitBaseElement> resultList = builder.createStatement(context.getSession()).<EppEduGroupSplitBaseElement>list().stream()
                        .filter(o -> {
                            boolean enabled = false;
                            if (splitVariant.getCode().equals(EppEduGroupSplitVariantCodes.PO_POLU)) {
                                enabled = ((EppEduGroupSplitBySexElement) o).getSex().isEnabled();
                            } else if (splitVariant.getCode().equals(EppEduGroupSplitVariantCodes.PO_INOSTRANNOMU_YAZYKU)) {
                                enabled = ((EppEduGroupSplitByLangElement) o).getForeignLanguage().isEnabled();
                            }
                            return enabled && (StringUtils.isEmpty(filter) || o.getSplitElementTitle().toLowerCase().contains(filter.toLowerCase()));
                        })
                        .sorted((o1, o2) -> o1.getSplitElementTitle().compareTo(o2.getSplitElementTitle()))
                        .collect(Collectors.toList());

                return ListOutputBuilder.get(input, resultList).pageable(false).build();
            }
        };
    }
}