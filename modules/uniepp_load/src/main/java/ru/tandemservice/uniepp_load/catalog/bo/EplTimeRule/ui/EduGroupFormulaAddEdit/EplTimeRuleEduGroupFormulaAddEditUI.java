/* $Id:$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.ui.EduGroupFormulaAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemAddEditUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupFormula;

/**
 * @author oleyba
 * @since 12/2/14
 */
@Input({
    @Bind(key = "catalogItemId", binding = "holder.id"),
    @Bind(key = DefaultCatalogAddEditModel.CATALOG_CODE, binding = "catalogCode")
})
public class EplTimeRuleEduGroupFormulaAddEditUI extends BaseCatalogItemAddEditUI<EplTimeRuleEduGroupFormula>
{
    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        getProperties().remove(EplTimeRuleEduGroupFormula.P_DESCRIPTION);
    }

    @Override
    public String getAdditionalPropertiesPageName()
    {
        return getClass().getPackage().getName() + ".AdditProps";
    }
}