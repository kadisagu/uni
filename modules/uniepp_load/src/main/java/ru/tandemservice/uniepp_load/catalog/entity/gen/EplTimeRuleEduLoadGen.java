package ru.tandemservice.uniepp_load.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeItemVariantGrouping;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRule;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Норма времени учебной нагрузки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplTimeRuleEduLoadGen extends EplTimeRule
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad";
    public static final String ENTITY_NAME = "eplTimeRuleEduLoad";
    public static final int VERSION_HASH = -721414615;
    private static IEntityMeta ENTITY_META;

    public static final String L_CATEGORY = "category";
    public static final String L_GROUPING = "grouping";

    private EplTimeCategoryEduLoad _category;     // Категория
    private EplTimeItemVariantGrouping _grouping;     // Вариант группировки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Категория. Свойство не может быть null.
     */
    @NotNull
    public EplTimeCategoryEduLoad getCategory()
    {
        return _category;
    }

    /**
     * @param category Категория. Свойство не может быть null.
     */
    public void setCategory(EplTimeCategoryEduLoad category)
    {
        dirty(_category, category);
        _category = category;
    }

    /**
     * @return Вариант группировки. Свойство не может быть null.
     */
    @NotNull
    public EplTimeItemVariantGrouping getGrouping()
    {
        return _grouping;
    }

    /**
     * @param grouping Вариант группировки. Свойство не может быть null.
     */
    public void setGrouping(EplTimeItemVariantGrouping grouping)
    {
        dirty(_grouping, grouping);
        _grouping = grouping;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EplTimeRuleEduLoadGen)
        {
            setCategory(((EplTimeRuleEduLoad)another).getCategory());
            setGrouping(((EplTimeRuleEduLoad)another).getGrouping());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplTimeRuleEduLoadGen> extends EplTimeRule.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplTimeRuleEduLoad.class;
        }

        public T newInstance()
        {
            return (T) new EplTimeRuleEduLoad();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "category":
                    return obj.getCategory();
                case "grouping":
                    return obj.getGrouping();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "category":
                    obj.setCategory((EplTimeCategoryEduLoad) value);
                    return;
                case "grouping":
                    obj.setGrouping((EplTimeItemVariantGrouping) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "category":
                        return true;
                case "grouping":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "category":
                    return true;
                case "grouping":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "category":
                    return EplTimeCategoryEduLoad.class;
                case "grouping":
                    return EplTimeItemVariantGrouping.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplTimeRuleEduLoad> _dslPath = new Path<EplTimeRuleEduLoad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplTimeRuleEduLoad");
    }
            

    /**
     * @return Категория. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad#getCategory()
     */
    public static EplTimeCategoryEduLoad.Path<EplTimeCategoryEduLoad> category()
    {
        return _dslPath.category();
    }

    /**
     * @return Вариант группировки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad#getGrouping()
     */
    public static EplTimeItemVariantGrouping.Path<EplTimeItemVariantGrouping> grouping()
    {
        return _dslPath.grouping();
    }

    public static class Path<E extends EplTimeRuleEduLoad> extends EplTimeRule.Path<E>
    {
        private EplTimeCategoryEduLoad.Path<EplTimeCategoryEduLoad> _category;
        private EplTimeItemVariantGrouping.Path<EplTimeItemVariantGrouping> _grouping;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Категория. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad#getCategory()
     */
        public EplTimeCategoryEduLoad.Path<EplTimeCategoryEduLoad> category()
        {
            if(_category == null )
                _category = new EplTimeCategoryEduLoad.Path<EplTimeCategoryEduLoad>(L_CATEGORY, this);
            return _category;
        }

    /**
     * @return Вариант группировки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad#getGrouping()
     */
        public EplTimeItemVariantGrouping.Path<EplTimeItemVariantGrouping> grouping()
        {
            if(_grouping == null )
                _grouping = new EplTimeItemVariantGrouping.Path<EplTimeItemVariantGrouping>(L_GROUPING, this);
            return _grouping;
        }

        public Class getEntityClass()
        {
            return EplTimeRuleEduLoad.class;
        }

        public String getEntityName()
        {
            return "eplTimeRuleEduLoad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
