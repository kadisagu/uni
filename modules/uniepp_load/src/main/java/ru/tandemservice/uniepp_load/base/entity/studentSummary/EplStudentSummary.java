package ru.tandemservice.uniepp_load.base.entity.studentSummary;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.gen.EplStudentSummaryGen;
import ru.tandemservice.uniepp_load.catalog.entity.EplStudentSummaryState;

/**
 * Сводка контингента
 */
public class EplStudentSummary extends EplStudentSummaryGen implements ITitled
{
    public static final String PARAM_EDU_YEAR = "eduYear";

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EplStudentSummary.class)
                .where(EplStudentSummary.eppYear().educationYear(), PARAM_EDU_YEAR)
                .titleProperty(EplStudentSummary.P_TITLE_WITH_STATE)
                .filter(EplStudentSummary.title())
                .order(EplStudentSummary.eppYear().educationYear().intValue(), OrderDirection.desc)
                .order(EplStudentSummary.title());
    }

    public static EntityComboDataSourceHandler defaultNotArchiveSelectDSHandler(String ownerId)
    {
        return defaultSelectDSHandler(ownerId).where(EplStudentSummary.archival(), Boolean.FALSE);
    }

    public EplStudentSummary()
    {
    }

    public EplStudentSummary(final EplStudentSummaryState state)
    {
        setState(state);
    }

    @EntityDSLSupport(parts = P_CREATION_DATE)
    @Override
    public String getCreationDateStr() {
        return DateFormatter.DATE_FORMATTER_WITH_TIME.format(getCreationDate());
    }

    @EntityDSLSupport(parts = {P_TITLE, L_STATE + EplStudentSummaryState.P_TITLE, P_ARCHIVAL})
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=32474950")
    @Override
    public String getTitleWithState()
    {
        return getTitle() + " (" + getState().getTitle() + (isArchival() ? ", в архиве" : "") + ")";
    }
}