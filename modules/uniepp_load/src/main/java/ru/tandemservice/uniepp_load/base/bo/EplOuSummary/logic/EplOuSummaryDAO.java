/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic;

import com.google.common.collect.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.mutable.MutableLong;
import org.hibernate.Session;
import org.springframework.beans.InvalidPropertyException;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.debug.SectionDebugItem;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.dql.ast.dntree.DNSelectRule;
import org.tandemframework.hibsupport.log.InsertEntityEvent;
import org.tandemframework.hibsupport.log.UpdateEntityEvent;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;
import org.tandemframework.shared.commonbase.base.bo.DaemonQueue.DaemonQueueManager;
import org.tandemframework.shared.commonbase.base.entity.DaemonQueueItem;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.EplIndividualPlanManager;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.EplOuSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.*;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.assignment.EplAssignmentCommonWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplBaseCategoryDataWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplBaseTimeDataWrapper;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.*;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot;
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.EplTimeRuleManager;
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.EplTimeRuleDao;
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleDao;
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.logic.IEplTimeRuleScript;
import ru.tandemservice.uniepp_load.catalog.entity.*;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplStudentSummaryStateCodes;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uniepp.entity.catalog.EppState.STATE_ACCEPTED;
import static ru.tandemservice.uniepp_load.catalog.entity.codes.EplTimeItemVariantGroupingCodes.*;


/**
 * @author oleyba
 * @since 5/5/12
 */
public class EplOuSummaryDAO extends UniBaseDao implements IEplOuSummaryDAO
{
    public static final String SUMMARY = "s";
    public static final String ORG_UNIT = "ou";
    public static final String GROUP = "g";
    public static final int GROUP_CHUNK_SIZE = 100;
    public static final String DAEMON_QUEUE_ITEM = "dqi";
    public static final String TIME_ITEM = "t";
    public static final String TIME_RULE = "r";
    public static final String VALUES = "v";

    @Override
    public void createAllMissingSummaries(EplStudentSummary studentSummary)
    {
        // выключаем системное логирование, чтобы не тормозило
        final IEventServiceLock eventLock = CoreServices.eventService().lock();

        Debug.begin("doAddSummary");
        List<EplOrgUnitSummary> createdSummaries = Lists.newArrayList();

        try {
            final IBackgroundProcess process = new BackgroundProcessBase()
            {
                @Override
                public ProcessResult run(ProcessState processState)
                {
                    processState.setCurrentValue(1);
                    int i = 1;
                    Set<OrgUnit> existingOrgUnit = Sets.newHashSet(CommonBaseUtil.getPropertiesList(getList(EplOrgUnitSummary.class, EplOrgUnitSummary.studentSummary().s(), studentSummary), EplOrgUnitSummary.orgUnit()));
                    Map<OrgUnit, Map<EplEduGroup, Map<EplStudent, Integer>>> groupOuMap = prepareGroupMap(studentSummary, existingOrgUnit);

                    int size = groupOuMap.size();

                    for (Map.Entry<OrgUnit, Map<EplEduGroup, Map<EplStudent, Integer>>> entry : groupOuMap.entrySet())
                    {
                        ProcessState state = new ProcessState(ProcessDisplayMode.unknown);
                        EplOrgUnitSummary summary = EplOuSummaryManager.instance().dao().createSummaryInternal(studentSummary, entry.getKey(), state);
                        EplOuSummaryManager.instance().dao().doRecalculateSummary(summary.getId(), EplRecalculateSummaryKind.FULL, state);

                        createdSummaries.add(summary);

                        processState.setCurrentValue((int) Math.round(100 * (i++) / (double) size));
                    }
                    processState.setCurrentValue(100);
                    return new ProcessResult("Расчеты нагрузки по читающим подразделениям успешно добавлены.");
                }
            };
            new BackgroundProcessHolder().start("Добавление расчетов нагрузки по читающим подразделениям", process, ProcessDisplayMode.percent);
        }
        finally {
            final SectionDebugItem end = Debug.end();
            if (end != null && logger.isInfoEnabled())
                logger.info(end.toFullString());

            // включаем системное логирование
            eventLock.release();
        }

        for(EplOrgUnitSummary summary : createdSummaries)
        {
            CoreServices.eventService().fireEvent(new InsertEntityEvent("Создание объекта", summary));
        }
    }

    @Override
    public EplOrgUnitSummary createSummaryInternal(EplStudentSummary studentSummary, OrgUnit orgUnit, ProcessState processState )
    {
        EppState state = getByCode(EppState.class, EppState.STATE_FORMATIVE);

        EplOrgUnitSummary summary = new EplOrgUnitSummary();
        summary.setCreationDate(new Date());
        summary.setStudentSummary(studentSummary);
        summary.setOrgUnit(orgUnit);
        summary.setState(state);
        summary.setRateHours(800);
        getSession().save(summary);
        flushClearAndRefresh();
        return summary;
    }

    @Override
    public EplOrgUnitSummary createSummary(OrgUnit orgUnit, EplStudentSummary studentSummary, ProcessState state)
    {
        Debug.begin("doAddSummary");
        EplOrgUnitSummary summary;
        try
        {
            summary = createSummaryInternal(studentSummary, orgUnit, state);
        }
        finally {
            final SectionDebugItem end = Debug.end();
            if (end != null && logger.isInfoEnabled())
                logger.info(end.toFullString());

        }
        CoreServices.eventService().fireEvent(new InsertEntityEvent("Создание объекта", summary));
        return summary;
    }

    @Override
    public Map<EplEduGroup, Map<EplStudent, Integer>> prepareGroupMap(Collection<EplOrgUnitSummary> ouSummaries, boolean transferTo, Set<EplEduGroup> groups)
    {
        Map<EplEduGroup, Map<EplStudent, Integer>> resultMap = Maps.newHashMap();
        for (EplOrgUnitSummary ouSummary : ouSummaries)
        {
            resultMap.putAll(prepareGroupMap(ouSummary.getStudentSummary(), ouSummary.getOrgUnit(), ouSummary, transferTo, groups));
        }
        return resultMap;
    }

    private Map<EplEduGroup, Map<EplStudent, Integer>> prepareGroupMap(EplStudentSummary summary, OrgUnit orgUnit, EplOrgUnitSummary ouSummary, boolean transferTo, Set<EplEduGroup> groups)
    {
        Map<EplEduGroup, Map<EplStudent, Integer>> groupMap = Maps.newHashMap();
        DQLSelectBuilder dql = getEduGroupRowBuilder(summary);
        if (groups != null)
            dql = dql.where(in(property(EplEduGroupRow.group().fromAlias("row")), groups));

        if (transferTo && ouSummary != null)
        {
            Set<EplEduGroup> transferEduGroups = getList(EplTransferOrgUnitEduGroupTimeItem.class, EplTransferOrgUnitEduGroupTimeItem.summary(), ouSummary)
                    .stream().map(EplEduGroupTimeItem::getEduGroup).collect(Collectors.toSet());

            dql.where(or(
                    eq(property("r", EppRegistryElement.owner()), value(orgUnit)),
                    in(property("row", EplEduGroupRow.group()), transferEduGroups)
            ));
        }
        else
            dql.where(eq(property("r", EppRegistryElement.owner()), value(orgUnit)));

        createMapByDql(groupMap, dql);

        return groupMap;
    }

    private void createMapByDql(Map<EplEduGroup, Map<EplStudent, Integer>> groupMap, DQLSelectBuilder dql)
    {
        for (Object[] row : dql.createStatement(getSession()).<Object[]>list()) {
            EplEduGroup group = (EplEduGroup) row[0];
            EplStudent student = (EplStudent) row[1];
            Integer count = (Integer) row[2];
            Map<EplStudent, Integer> studentMap = SafeMap.safeGet(groupMap, group, HashMap.class);
            studentMap.put(student, count + SafeMap.safeGet(studentMap, student, key -> 0));
        }
    }

    private Map<EplEduGroup, Map<EplStudent, Integer>> prepareGroupMap(Set<EplEduGroup> groups)
    {
        Map<Long, EplEduGroup> sourceGroups = groups.stream().collect(Collectors.toMap(g -> g.getId(), g -> g));
        Map<EplEduGroup, Map<EplStudent, Integer>> groupMap = Maps.newHashMap();
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EplEduGroupRow.class, "row")
                .joinPath(DQLJoinType.inner, EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().fromAlias("row"), "s")
                .column(property(EplEduGroupRow.group().id().fromAlias("row")))
                .column(property("s"))
                .column(property("row", EplEduGroupRow.count()))
                .where(in(property(EplEduGroupRow.group().fromAlias("row")), groups));

        for (Object[] row : dql.createStatement(getSession()).<Object[]>list()) {
            Long groupId = (Long) row[0];
            EplEduGroup group = sourceGroups.get(groupId);
            EplStudent student = (EplStudent) row[1];
            Integer count = (Integer) row[2];
            Map<EplStudent, Integer> studentMap = SafeMap.safeGet(groupMap, group, HashMap.class);
            studentMap.put(student, count + SafeMap.safeGet(studentMap, student, key -> 0));
        }
        return groupMap;
    }


    private Map<OrgUnit, Map<EplEduGroup, Map<EplStudent, Integer>>> prepareGroupMap(EplStudentSummary studentSummary, Set<OrgUnit> existingOrgUnit)
    {
        Map<OrgUnit, Map<EplEduGroup, Map<EplStudent, Integer>>> groupMap = Maps.newHashMap();
        DQLSelectBuilder dql = getEduGroupRowBuilder(studentSummary);

        if (CollectionUtils.isNotEmpty(existingOrgUnit))
            dql.where(notIn(property("r", EppRegistryElement.owner()), existingOrgUnit));

        for (Object[] row : dql.createStatement(getSession()).<Object[]>list()) {
            EplEduGroup group = (EplEduGroup) row[0];
            EplStudent student = (EplStudent) row[1];
            Integer count = (Integer) row[2];
            OrgUnit key = group.getRegistryElementPart().getTutorOu();
            Map<EplEduGroup, Map<EplStudent, Integer>> currentMap = SafeMap.safeGet(groupMap, key, HashMap.class);
            Map<EplStudent, Integer> studentMap = SafeMap.safeGet(currentMap, group, HashMap.class);
            studentMap.put(student, count + SafeMap.safeGet(studentMap, student, key1 -> 0));
        }
        return groupMap;
    }

    private DQLSelectBuilder getEduGroupRowBuilder(EplStudentSummary summary)
    {
        return new DQLSelectBuilder()
                .fromEntity(EplEduGroupRow.class, "row")
                .joinPath(DQLJoinType.inner, EplEduGroupRow.group().fromAlias("row"), "g")
                .joinPath(DQLJoinType.inner, EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().fromAlias("row"), "s")
                .joinPath(DQLJoinType.inner, EplEduGroup.registryElementPart().registryElement().fromAlias("g"), "r")
                .fetchPath(DQLJoinType.inner, EplStudent.educationOrgUnit().fromAlias("s"), "eo")
                .fetchPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().fromAlias("eo"), "ehs")
                .fetchPath(DQLJoinType.inner, EducationLevelsHighSchool.educationLevel().fromAlias("ehs"), "el")
                .fetchPath(DQLJoinType.left, EducationLevels.eduProgramSubject().fromAlias("el"), "ps")
                .fetchPath(DQLJoinType.left, EducationLevels.eduProgramSpecialization().fromAlias("el"), "spec")
                .column(property("g"))
                .column(property("s"))
                .column(property("row", EplEduGroupRow.count()))
                .where(eq(property("row", EplEduGroupRow.group().summary()), value(summary)));
    }

    private DQLSelectBuilder getEduGroupRowBuilder(String alias, Collection<EplOrgUnitSummary> ouSummaries, boolean transferTo)
    {
        Set<Long> studentSummaryIds = Sets.newHashSet();
        Set<Long> orgUnitIds = Sets.newHashSet();
        Set<Long> ouSummaryIds = Sets.newHashSet();

        ouSummaries.forEach(item -> {
            studentSummaryIds.add(item.getStudentSummary().getId());
            orgUnitIds.add(item.getOrgUnit().getId());
            ouSummaryIds.add(item.getId());
        });

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EplEduGroupRow.class, alias)
                .where(in(property(alias, EplEduGroupRow.group().summary()), studentSummaryIds));

        if (transferTo)
        {
            builder.where(or(
                    in(property(alias, EplEduGroupRow.group().registryElementPart().registryElement().owner().id()), orgUnitIds),
                    in(property(alias, EplEduGroupRow.group()),
                       new DQLSelectBuilder()
                               .fromEntity(EplTransferOrgUnitEduGroupTimeItem.class, TIME_ITEM)
                               .column(property(TIME_ITEM, EplTransferOrgUnitEduGroupTimeItem.eduGroup().id())).distinct()
                               .where(in(property(TIME_ITEM, EplTransferOrgUnitEduGroupTimeItem.summary().id()), ouSummaryIds))
                               .buildQuery()
                    )
            ));
        }
        else
            builder.where(in(property("r", EplEduGroupRow.group().registryElementPart().registryElement().owner().id()), orgUnitIds));

        return builder;
    }

    @Override
    public void doRecalculateSummary(Long summaryId, EplRecalculateSummaryKind kind, ProcessState state) {
        flushClearAndRefresh();
        EplRecalculateSummaryData rData = prepareRecalculateData(kind, state, get(summaryId), null);
        EplOuSummaryManager.instance().dao().doRecalculateSummary(rData, null);
        reportErrors(state, rData.disabledRules);
    }

    private EplRecalculateSummaryData prepareRecalculateData(EplRecalculateSummaryKind kind, ProcessState state, EplOrgUnitSummary ouSummary, Set<EplTimeRuleEduLoad> rulesInQueue)
    {
        // берем активные формулы и скрипты
        List<EplTimeRuleEduGroupScript> ruleScripts = getList(EplTimeRuleEduGroupScript.class);
        List<EplTimeRuleEduLoad> ruleList = Lists.newArrayList(ruleScripts);
        ruleList.addAll(getList(EplTimeRuleEduGroupFormula.class));

        // и формы тек.контроля
        final List<EppIControlActionType> icaTypes = getList(EppIControlActionType.class, EppIControlActionType.userCode().s());
        Map<EplTimeRuleEduGroupScript, IEplTimeRuleScript> scriptWrapperMap = EplTimeRuleManager.instance().timeRuleDao().getScriptWrapper4RuleMap(ruleScripts);
        return new EplRecalculateSummaryData(ouSummary, kind, state,  ruleList,null, icaTypes, scriptWrapperMap, new HashSet<>());
    }


    @Override
    public boolean doRecalculateSummary(final EplRecalculateSummaryData rData, DaemonQueueManager.DaemonQueueItemIterator<EplEduGroup> groupIterator)
    {
        // Устанавливается блокировка на расчет eplSummaryOUTimeItemRefresh_<ID расчета> с ожиданием ее освобождения не более 5 минут.
        showMessage(rData, "Расчет заблокирован другим процессом. Ожидание разблокировки (не более 5 минут).");
        try
        {
            NamedSyncInTransactionCheckLocker.register(getSession(), getLockName(rData.ouSummary), 5 * 60);
        }
        catch (RuntimeTimeoutException ex)
        {
            showMessage(rData, "Расчет заблокирован другим процессом. Повторите попытку позже.");
            Debug.exception("Превышен лимит ожидания блокировки расчета (5 минут). Проверьте производительность норм-скриптов.", ex);
            Debug.saveDebug();
            return false;
        }

        Session session = getSession();
        // выключаем системное логирование, чтобы не тормозило
        final IEventServiceLock eventLock = CoreServices.eventService().lock();

        try
        {
            final IEplOuSummaryDAO ouSummaryDAO = EplOuSummaryManager.instance().dao();
            // Вызывается метод предварительной очистки расчета (на входе ID расчета):
            showMessage(rData, "Предварительная очистка расчета.");
            ouSummaryDAO.deleteOrUpdateTimeItem(rData);

            // Вызывается метод обновления рассчитанных часов по простой норме расчета (на входе ID расчета):
            showMessage(rData, "Обновление часов по простым нормам.");
            ouSummaryDAO.refreshHoursBySimpleRules(rData);

            showMessage(rData, "Обновление часов по нормам-скриптам и нормам-формулам.");

            Map<MultiKey, Boolean> transferredHours = Maps.newHashMap();
            Map<MultiKey, EplOrgUnitSummary> transferredTo = Maps.newHashMap();

            /**
             * Берутся все объекты Переданные с другого подразделения часы нагрузки для План. потока, ссылающиеся на данный расчет,
             * по ним вычисляются ключи (ИД План. потока, ИД нормы) и складываются в массив (ключ, признак необходимости пересчета)
             */

            List<EplTransferOrgUnitEduGroupTimeItem> transferTimeItems = new DQLSelectBuilder()
                    .fromEntity(EplTransferOrgUnitEduGroupTimeItem.class, TIME_ITEM)
                    .where(eq(property(TIME_ITEM, EplTransferOrgUnitEduGroupTimeItem.transferredFrom()), value(rData.ouSummary)))
                    .where(isNotNull(property(TIME_ITEM, EplTransferOrgUnitEduGroupTimeItem.eduGroup())))
                    .createStatement(getSession()).list();

            for (EplTransferOrgUnitEduGroupTimeItem item : transferTimeItems)
            {
                MultiKey key = new MultiKey(item.getEduGroup().getId(), item.getTimeRule().getId());
                Boolean bNeedRecalculate = null;
                if (!STATE_ACCEPTED.equals(item.getSummary().getState().getCode()) && !STATE_ACCEPTED.equals(item.getTransferredFrom().getState().getCode()))
                {
                    bNeedRecalculate = false;
                    transferredTo.put(key, item.getSummary());
                }
                transferredHours.put(key, bNeedRecalculate);
            }

            final Map<EplTimeRuleEduGroupScript, Integer> scriptRequestCountMap = SafeMap.get(key -> 0);
            final Map<EplTimeRuleEduGroupScript, Long> scriptTotalTimeMap = SafeMap.get(key -> 0L);

            Date fillDate = new Date();

            Iterator<Object> iterator = getGroupIterator(rData.ouSummary, groupIterator);
            iterateByChunk(rData, ouSummaryDAO, transferredHours, false, scriptRequestCountMap, scriptTotalTimeMap, fillDate, iterator);

            if (!ouSummaryDAO.recalculateTransferred(rData, ouSummaryDAO, transferredHours, transferredTo, scriptRequestCountMap, scriptTotalTimeMap, fillDate, groupIterator))
                return false;

            ouSummaryDAO.updateTimeRules(scriptRequestCountMap, scriptTotalTimeMap);

            session.flush();
        }
        finally
        {
            // включаем системное логирование
            eventLock.release();
        }

        if (rData.kind == EplRecalculateSummaryKind.FULL)
        {
            CoreServices.eventService().fireEvent(new UpdateEntityEvent("Расчет нагрузки обновлен.", rData.ouSummary));
        }
        return true;
    }

    private void reportErrors(ProcessState state, Set<EplTimeRuleEduLoad> disabledRules)
    {
        if (disabledRules.size() > 0)
        {
            throw new ApplicationException("Ошибка норм-скриптов. Проверьте список ошибок.", new Throwable());
        }
    }

    public boolean recalculateTransferred(final EplRecalculateSummaryData rData,
                                          IEplOuSummaryDAO ouSummaryDAO,
                                          Map<MultiKey, Boolean> transferredHours,
                                          Map<MultiKey, EplOrgUnitSummary> transferredTo,
                                          Map<EplTimeRuleEduGroupScript, Integer> scriptRequestCountMap,
                                          Map<EplTimeRuleEduGroupScript, Long> scriptTotalTimeMap,
                                          Date fillDate,
                                          DaemonQueueManager.DaemonQueueItemIterator<EplEduGroup> groupIterator)
    {
        //Устанавливается блокировка на сводку eplSummaryTransferredTimeItemRefresh_<ID сводки> с ожиданием ее освобождения не более 5 минут.
        showMessage(rData, "Расчет заблокирован другим процессом. Ожидание разблокировки (не более 5 минут).");
        try
        {
            NamedSyncInTransactionCheckLocker.register(getSession(), "eplSummaryTransferredTimeItemRefresh_" + rData.ouSummary.getStudentSummary().getId(), 5 * 60);
        }
        catch (RuntimeTimeoutException ex)
        {
            showMessage(rData, "Расчет заблокирован другим процессом. Повторите попытку позже.");
            Debug.exception("Превышен лимит ожидания блокировки расчета (5 минут). Проверьте производительность норм-скриптов.", ex);
            Debug.saveDebug();
            return false;
        }

        showMessage(rData, "Обновление переданных часов.");

        //Из массива ключей с переданными часами последовательно берутся ключи группами по N=10 элементов из расчетов в состоянии отличном от «Согласован» и помеченные признаком необходимости пересчета,
        // затем складываются в массив (ключ, признак необходимости очистки), к которому применяются пункты 5.c. — 5.f. алгоритма
        List<EplOrgUnitSummary> summaryList = transferredTo.values().stream().distinct().collect(Collectors.toList());
        for(EplOrgUnitSummary summary: summaryList)
        {
            Set<Long> groupIds = transferredTo.entrySet().stream().filter(e -> transferredHours.containsKey(e.getKey()) && transferredHours.get(e.getKey()) && summary.getId().equals(e.getValue().getId())).map(e -> (Long) e.getKey().getKey(0)).collect(Collectors.toSet());
            Iterator<Object> iterator = new DQLSelectBuilder()
                    .fromEntity(EplEduGroup.class, GROUP)
                    .where(in(property(GROUP + ".id"), groupIds))
                    .createStatement(getSession()).iterate();

            iterateByChunk(rData.withSummary(summary), ouSummaryDAO, transferredHours, true,
                    scriptRequestCountMap, scriptTotalTimeMap, fillDate, iterator);
        }

        Map<MultiKey, Boolean> transferredToMe = Maps.newHashMap();
        HashSet<Object> eplEduGroups = new HashSet<>();

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EplTransferOrgUnitEduGroupTimeItem.class, TIME_ITEM)
                .column(TIME_ITEM)
                .where(eq(property(TIME_ITEM, EplTransferOrgUnitEduGroupTimeItem.summary()), value(rData.ouSummary)))
                .where(ne(property(TIME_ITEM, EplTransferOrgUnitEduGroupTimeItem.transferredFrom().state().code()), value(STATE_ACCEPTED)));

        if (groupIterator != null && groupIterator.hasNext())
        {
            dql = dql.joinEntity(TIME_ITEM, DQLJoinType.inner, DaemonQueueItem.class, DAEMON_QUEUE_ITEM, false,
                    and(eq(property(TIME_ITEM, EplTransferOrgUnitEduGroupTimeItem.eduGroup().id()), property(DAEMON_QUEUE_ITEM, DaemonQueueItem.entity())),
                            eq(property(DAEMON_QUEUE_ITEM, DaemonQueueItem.queue()), value(groupIterator.getQueueId())),
                            le(property(DAEMON_QUEUE_ITEM, DaemonQueueItem.id()), value(groupIterator.getLastItemId()))));
        }

        Iterator<Object> iterate = dql.createStatement(getSession()).iterate();
        while (iterate.hasNext())
        {
            EplTransferOrgUnitEduGroupTimeItem timeItem = (EplTransferOrgUnitEduGroupTimeItem)iterate.next();
            if (timeItem == null || timeItem.getEduGroup() == null || timeItem.getEduGroup().getId() == null || timeItem.getTimeRule() == null || timeItem.getTimeRule().getId() == null)
                continue;
            boolean needsRecalulate = !STATE_ACCEPTED.equals(timeItem.getTransferredFrom().getState().getCode()) && !STATE_ACCEPTED.equals(timeItem.getSummary().getState().getCode());
            transferredToMe.put(new MultiKey(timeItem.getEduGroup().getId(), timeItem.getTimeRule().getId()), needsRecalulate);
            eplEduGroups.add(timeItem.getEduGroup());
        }

        iterateByChunk(rData, ouSummaryDAO, transferredToMe, true,
                scriptRequestCountMap, scriptTotalTimeMap, fillDate, eplEduGroups.iterator());
        return true;
    }

    private void iterateByChunk(final EplRecalculateSummaryData rData,
                                IEplOuSummaryDAO ouSummaryDAO,
                                Map<MultiKey, Boolean> transferredHours,
                                boolean useTransfered,
                                Map<EplTimeRuleEduGroupScript, Integer> scriptRequestCountMap,
                                Map<EplTimeRuleEduGroupScript, Long> scriptTotalTimeMap,
                                Date fillDate, Iterator<Object> iterator)
    {
        Set<EplEduGroup> groups = new HashSet<>(GROUP_CHUNK_SIZE);
        while (iterator.hasNext())
        {
            groups.add((EplEduGroup)iterator.next());
            if (iterator.hasNext() && groups.size() < GROUP_CHUNK_SIZE)
                continue;

            recalculateGroupChunk(rData, ouSummaryDAO, transferredHours, useTransfered, scriptRequestCountMap, scriptTotalTimeMap, groups, fillDate);
            groups.clear();
        }
    }

    private void recalculateGroupChunk(final EplRecalculateSummaryData rData,
                                       IEplOuSummaryDAO ouSummaryDAO,
                                       Map<MultiKey, Boolean> transferredHours,
                                       boolean useTransfered,
                                       Map<EplTimeRuleEduGroupScript, Integer> scriptRequestCountMap,
                                       Map<EplTimeRuleEduGroupScript, Long> scriptTotalTimeMap,
                                       Set<EplEduGroup> groups,
                                       Date fillDate)
    {
        Set<Long> regElementParts = Sets.newHashSet();
        Map<MultiKey, EplEduGroupTimeItem> itemMap = Maps.newHashMap();
        List<EplEduGroupTimeItem> timeItems = new DQLSelectBuilder()
                .fromEntity(EplEduGroupTimeItem.class, "t")
                .where(eq(property("t", EplEduGroupTimeItem.summary()), value(rData.ouSummary)))
                .where(rData.kind == EplRecalculateSummaryKind.PARTIAL_CHECK ?
                        in(property("t", EplEduGroupTimeItem.eduGroup()), groups)
                        : isNotNull(property("t", EplEduGroupTimeItem.eduGroup())))
                .createStatement(getSession()).list();

        timeItems.forEach(item -> itemMap.put(new MultiKey(item.getEduGroup().getId(), item.getTimeRule().getId()), item));


        Map<EplEduGroup, Map<EplStudent, Integer>> groupMap = prepareGroupMap(groups);
        groupMap.keySet().forEach(eduGroup -> regElementParts.add(eduGroup.getRegistryElementPart().getId()));
        // поднимаем обертки для указанных дисциплиночастей
        final Map<Long, IEppRegElPartWrapper> registryData = IEppRegistryDAO.instance.get().getRegistryElementPartDataMap(regElementParts);
        ITimeItemData data = wrapTimeItem(fillDate, rData.scriptWrapperMap, rData.ruleList, rData.icaTypes, registryData, itemMap, null);

        Set<Long> toClear = ouSummaryDAO.checkTimeItemSummaryHours(rData, groupMap, data, transferredHours, useTransfered, scriptRequestCountMap, scriptTotalTimeMap);
        //Удаляются объекты Часы нагрузки, соответствующие ключам, которые помечены признаком необходимости очистки в отдельной транзакци
        if (toClear.size() > 0)
            ouSummaryDAO.updateOrDelete(toClear, rData.kind == EplRecalculateSummaryKind.FULL);
    }

    private Iterator<Object> getGroupIterator(EplOrgUnitSummary ouSummary, DaemonQueueManager.DaemonQueueItemIterator<EplEduGroup> groupIterator)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .distinct()
                .fromEntity(EplEduGroup.class, GROUP)
                .where(eq(property("g", EplEduGroup.summary().id()), value(ouSummary.getStudentSummary().getId())))
                .where(eq(property("g", EplEduGroup.registryElementPart().registryElement().owner()), value(ouSummary.getOrgUnit())))
                .column(GROUP);

        if (groupIterator != null && !groupIterator.isEmpty())
        {
            dql = dql.joinEntity(GROUP, DQLJoinType.inner, DaemonQueueItem.class, DAEMON_QUEUE_ITEM, false,
                    and(eq(property(GROUP, EplEduGroup.id()), property(DAEMON_QUEUE_ITEM, DaemonQueueItem.entity())),
                            eq(property(DAEMON_QUEUE_ITEM, DaemonQueueItem.queue()), value(groupIterator.getQueueId())),
                            le(property(DAEMON_QUEUE_ITEM, DaemonQueueItem.id()), value(groupIterator.getLastItemId()))));
        }

        return dql.createStatement(getSession()).iterate();
    }

    private void showMessage(EplRecalculateSummaryData rData, String message)
    {
        rData.state.setMessage(message);
    }

    @Override
    public void refreshHoursBySimpleRules(final EplRecalculateSummaryData rData)
    {
        if (rData.kind == EplRecalculateSummaryKind.PARTIAL_CHECK && rData.rulesInQueue == null)
            return;

        // Поднимаем все часы нагрузки по простой форме, данного расчета
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EplSimpleTimeItem.class, TIME_ITEM)
                .column(TIME_ITEM)
                .column(EplTimeRuleSimple.coefficient().fromAlias(TIME_RULE).s())
                .column(EplTimeRuleSimple.enabled().fromAlias(TIME_RULE).s())
                .column(EplTimeRuleSimple.parameterNeeded().fromAlias(TIME_RULE).s())
                .joinEntity(TIME_ITEM, DQLJoinType.inner, EplTimeRuleSimple.class, TIME_RULE, true,
                            eq(property(TIME_ITEM, EplSimpleTimeItem.timeRule().id()), property(TIME_RULE, EplTimeRuleSimple.id()))
                )
                .where(eq(property(TIME_ITEM, EplSimpleTimeItem.summary()), value(rData.ouSummary)));

        dql.createStatement(getSession()).<Object[]>list().forEach(
                row ->
                {
                    EplSimpleTimeItem item = (EplSimpleTimeItem) row[0];
                    Double coefficient = (Double) row[1];
                    Boolean timeRuleEnabled = (Boolean) row[2];
                    Boolean parameterNeeded = (Boolean) row[3];
                    double result = parameterNeeded ? coefficient * item.getParameter(): coefficient;

                    if (rData.kind != EplRecalculateSummaryKind.FULL)
                    {
                        if (result != item.getControlTimeAmount() && timeRuleEnabled)
                        {
                            item.setControlTimeAmount(result);
                            getSession().saveOrUpdate(item);
                        }
                    } else
                    {
                        if (result != item.getTimeAmount() || result != item.getControlTimeAmount())
                        {
                            item.setTimeAmount(result);
                            item.setControlTimeAmount(result);
                            getSession().saveOrUpdate(item);
                        }
                    }
                });
    }

    @Override
    public void deleteOrUpdateTimeItem(EplRecalculateSummaryData rData)
    {
        DQLRestrictedBuilder dql = (rData.kind == EplRecalculateSummaryKind.FULL) ?
                new DQLDeleteBuilder(EplEduGroupTimeItem.class) :
                new DQLUpdateBuilder(EplEduGroupTimeItem.class).set(EplEduGroupTimeItem.controlTimeAmountAsLong().s(), value(0));

        // Без План. потока
        // С План. потоком без строк
        DQLSelectBuilder subquery = new DQLSelectBuilder().fromEntity(EplEduGroupTimeItem.class, TIME_ITEM)
                .where(notInstanceOf(TIME_ITEM, EplTransferOrgUnitEduGroupTimeItem.class))
                .column(property(TIME_ITEM + ".id"), "item_id")
                .joinPath(DQLJoinType.left, EplEduGroupTimeItem.eduGroup().fromAlias(TIME_ITEM), GROUP, false)
                .joinEntity(GROUP, DQLJoinType.left, EplEduGroupRow.class, "row", false, eq(property(GROUP), property("row", EplEduGroupRow.group())))
                .where(or(isNull(property(GROUP)), isNull(property("row"))))
                 .where(eq(property(TIME_ITEM, EplEduGroupTimeItem.summary()), value(rData.ouSummary)));

        //у которых данное подразделение не является читающим для дисциплины из План. потока
        DNSelectRule subRule = new DQLSelectBuilder().fromEntity(EplEduGroupTimeItem.class, TIME_ITEM)
                .where(notInstanceOf(TIME_ITEM, EplTransferOrgUnitEduGroupTimeItem.class))
                .column(property(TIME_ITEM + ".id"), "item_id")
                .where(is(TIME_ITEM, EplEduGroupTimeItem.class))
                .where(ne(property(TIME_ITEM, EplEduGroupTimeItem.eduGroup().registryElementPart().registryElement().owner()), value(rData.ouSummary.getOrgUnit())))
                .where(eq(property(TIME_ITEM, EplEduGroupTimeItem.summary()), value(rData.ouSummary))).buildSelectRule();

        dql = dql.fromDataSource(
                        subquery.union(subRule)
                                .buildQuery(), "x"
                )
                .where(eq(property(EplTimeItem.id()), property("x.item_id")));

        ((DQLExecutableQueryBuilder)dql).createStatement(getSession()).execute();

        dql = (rData.kind == EplRecalculateSummaryKind.FULL) ?
                new DQLDeleteBuilder(EplTransferOrgUnitEduGroupTimeItem.class) :
                new DQLUpdateBuilder(EplTransferOrgUnitEduGroupTimeItem.class).set(EplEduGroupTimeItem.controlTimeAmountAsLong().s(), value(0));
        //Удаляются часы без План. потока или с План. потоком без строк:
       // Переданные с другого подразделения часы нагрузки для План. потока, переданные с данного подразделения (поле "transferredFrom" ссылается на данный расчет), у которых расчет (поле "summary") в состоянии отличном от «Согласован»
        subquery = new DQLSelectBuilder().fromEntity(EplTransferOrgUnitEduGroupTimeItem.class, TIME_ITEM)
                .column(property(TIME_ITEM + ".id"), "item_id")
                .joinPath(DQLJoinType.left, EplTransferOrgUnitEduGroupTimeItem.eduGroup().fromAlias(TIME_ITEM), GROUP, false)
                .joinEntity(GROUP, DQLJoinType.left, EplEduGroupRow.class, "row", false, eq(property(GROUP), property("row", EplEduGroupRow.group())))
                .where(or(isNull(property(GROUP)), isNull(property("row"))))
                .where(eq(property(TIME_ITEM, EplTransferOrgUnitEduGroupTimeItem.transferredFrom()), value(rData.ouSummary)))
                .where(ne(property(TIME_ITEM, EplTransferOrgUnitEduGroupTimeItem.transferredFrom().state().code()), value(STATE_ACCEPTED)))
                .where(ne(property(TIME_ITEM, EplTransferOrgUnitEduGroupTimeItem.summary().state().code()), value(STATE_ACCEPTED)));

        //Переданные с другого подразделения часы нагрузки для План. потока данного подразделения, ссылающиеся (поле "transferredFrom") на расчеты в состояниях, отличных от «Согласован»
        subRule = new DQLSelectBuilder().fromEntity(EplTransferOrgUnitEduGroupTimeItem.class, TIME_ITEM)
                .column(property(TIME_ITEM + ".id"), "item_id")
                .joinPath(DQLJoinType.left, EplTransferOrgUnitEduGroupTimeItem.eduGroup().fromAlias(TIME_ITEM), GROUP, false)
                .joinEntity(GROUP, DQLJoinType.left, EplEduGroupRow.class, "row", false, eq(property(GROUP), property("row", EplEduGroupRow.group())))
                .where(or(isNull(property(GROUP)), isNull(property("row"))))
                .where(eq(property(TIME_ITEM, EplTransferOrgUnitEduGroupTimeItem.summary()), value(rData.ouSummary)))
                .where(ne(property(TIME_ITEM, EplTransferOrgUnitEduGroupTimeItem.transferredFrom().state().code()), value(STATE_ACCEPTED)))
                .where(ne(property(TIME_ITEM, EplTransferOrgUnitEduGroupTimeItem.summary().state().code()), value(STATE_ACCEPTED))).buildSelectRule();

        dql = dql.fromDataSource(
                subquery.union(subRule)
                        .buildQuery(), "x"
        )
                .where(eq(property(EplTransferOrgUnitEduGroupTimeItem.id()), property("x.item_id")));

        ((DQLExecutableQueryBuilder)dql).createStatement(getSession()).execute();
    }

    private DQLSelectBuilder getOrgUnitSummaryBuilder()
    {
        return new DQLSelectBuilder()
                .fromEntity(EplOrgUnitSummary.class, "ous")
                .joinPath(DQLJoinType.inner, EplOrgUnitSummary.studentSummary().fromAlias("ous"), "ss")
                .where(ne(property("ous", EplOrgUnitSummary.state().code()), value(STATE_ACCEPTED)))
                .where(eq(property("ss", EplStudentSummary.archival()), value(Boolean.FALSE)))
                .where(ne(property("ss", EplStudentSummary.state().code()), value(EplStudentSummaryStateCodes.ACCEPT)))
                .column(property("ous.id"), "ous_id");
    }

    private IEplTimeRuleDao.IRuleResult getiRuleResult(ITimeItemData data, Map<EplTimeRuleEduGroupScript, Integer> scriptRequestCountMap, Map<EplTimeRuleEduGroupScript, Long> scriptTotalTimeMap, IEplTimeRuleDao timeRuleDao, IEplTimeRuleDao.ILoadData loadData, EplTimeRuleEduLoad timeRule, IEplTimeRuleDao.IRuleResult ruleResult)
    {
        if (timeRule instanceof EplTimeRuleEduGroupFormula) ruleResult = timeRuleDao.count((EplTimeRuleEduGroupFormula) timeRule, loadData);
        else if (timeRule instanceof EplTimeRuleEduGroupScript) {
            EplTimeRuleEduGroupScript timeRuleScript = (EplTimeRuleEduGroupScript) timeRule;
            ruleResult = timeRuleDao.count(timeRuleScript, data.scriptWrapperMap().get(timeRule), loadData);
            ruleResult = updateCounters(scriptRequestCountMap, scriptTotalTimeMap, ruleResult, timeRuleScript);
        }
        return ruleResult;
    }

    public Set<Long> checkTimeItemSummaryHours(final EplRecalculateSummaryData rData,
                                               Map<EplEduGroup, Map<EplStudent, Integer>> groupMap,
                                               ITimeItemData data,
                                               Map<MultiKey, Boolean> transferredHours,
                                               boolean useTransfered,
                                               final Map<EplTimeRuleEduGroupScript, Integer> scriptRequestCountMap,
                                               final Map<EplTimeRuleEduGroupScript, Long> scriptTotalTimeMap)
    {
        getSession().merge(rData.ouSummary);
        Set<Long> toClear = new HashSet<>();

        final IEplTimeRuleDao timeRuleDao = EplTimeRuleManager.instance().timeRuleDao();

        for (Map.Entry<EplEduGroup, Map<EplStudent, Integer>> entry : groupMap.entrySet())
        {
            EplEduGroup eduGroup = entry.getKey();
            EppRegistryElementPart registryElementPart = eduGroup.getRegistryElementPart();
            IEppRegElPartWrapper regElPartWrapper = data.registryData().get(registryElementPart.getId());
            IEplTimeRuleDao.ILoadData loadData = EplTimeRuleDao.wrapLoadData(regElPartWrapper, eduGroup, entry.getValue(), data.icaTypes());

            for (EplTimeRuleEduLoad timeRule : data.ruleList())
            {
                MultiKey key = new MultiKey(eduGroup.getId(), timeRule.getId());
                //Если План. поток есть в массиве ключей с переданными часами, то ключу с такой План. поток устанавливается признак необходимости пересчета (такие элементы будут обновлены в п. 7)
                if (!useTransfered)
                {
                    if (null != transferredHours && transferredHours.containsKey(key))
                    {
                        transferredHours.put(key, transferredHours.get(key) != null);
                        continue;
                    }
                }
                else
                {
                    if (transferredHours != null && transferredHours.containsKey(key))
                    {
                        Boolean needTransfer = transferredHours.get(key);
                        if (null == needTransfer || !needTransfer)
                            continue;
                    } else
                        continue;
                }

                //норма времени вычеркнута
                //норма времени не используется
                EplEduGroupTimeItem timeItem = data.itemMap().get(key);

                if (rData.disabledRules.contains(timeRule) || !timeRule.isEnabled())
                {
                    addToClear(toClear, timeItem);
                    continue;
                }

                IEplTimeRuleDao.IRuleResult ruleResult = null;
                try
                {
                    ruleResult = getiRuleResult(data, scriptRequestCountMap, scriptTotalTimeMap, timeRuleDao, loadData, timeRule, ruleResult);
                }
                catch (Exception ex)
                {
                    rData.disabledRules.add(timeRule);
                    addToClear(toClear, timeItem);
                    continue;
                }

                //Null или число <=0, то ключ помечается признаком необходимости очистки
                if (null == ruleResult || ruleResult.hours() <= 0)
                {
                    addToClear(toClear, timeItem);
                    continue;
                }

                if (null == timeItem)
                {
                        timeItem = new EplEduGroupTimeItem();
                        timeItem.setEduGroup(eduGroup);
                        timeItem.setRegistryElementPart(registryElementPart);
                        timeItem.setSummary(rData.ouSummary);
                        timeItem.setTimeRule(timeRule);
                        timeItem.setCreationDate(data.fillDate());
                        timeItem.setDescription(ruleResult.description());
                        timeItem.setTimeAmount(0);
                } else
                    timeItem = (EplEduGroupTimeItem)getSession().merge(timeItem);

                if (rData.kind != EplRecalculateSummaryKind.FULL)
                {
                    if (ruleResult.hours() == timeItem.getControlTimeAmount())
                        continue;
                }
                else
                {
                    if (ruleResult.hours() == timeItem.getTimeAmount() && ruleResult.hours() == timeItem.getControlTimeAmount() && ruleResult.description().equals(timeItem.getDescription()))
                        continue;
                }

                if (rData.kind == EplRecalculateSummaryKind.FULL)
                    timeItem.setTimeAmount(ruleResult.hours());

                timeItem.setControlTimeAmount(ruleResult.hours());
                timeItem.setDescription(ruleResult.description());
                saveOrUpdate(timeItem);
            }
        }

        return toClear;
    }

    private void addToClear(Set<Long> toClear, EplEduGroupTimeItem timeItem)
    {
        if (timeItem != null)
            toClear.add(timeItem.getId());
    }

    public void updateOrDelete(Set<Long> toClearSet, boolean delete)
    {
        if (delete)
        {
            new DQLDeleteBuilder(EplEduGroupTimeItem.class)
                    .where(in(property("id"), toClearSet))
                    .createStatement(getSession()).execute();
        } else
        {
            new DQLUpdateBuilder(EplEduGroupTimeItem.class)
                    .set(EplEduGroupTimeItem.controlTimeAmountAsLong().s(), value(0))
                    .where(in(property("id"), toClearSet))
                    .createStatement(getSession()).execute();
        }
    }

    private IEplTimeRuleDao.IRuleResult updateCounters(Map<EplTimeRuleEduGroupScript, Integer> scriptRequestCountMap, Map<EplTimeRuleEduGroupScript, Long> scriptTotalTimeMap, IEplTimeRuleDao.IRuleResult ruleResult, EplTimeRuleEduGroupScript timeRuleScript)
    {
        if (null != ruleResult && timeRuleScript.isCounterEnabled())
        {
            scriptRequestCountMap.put(timeRuleScript, 1 + scriptRequestCountMap.get(timeRuleScript));
            scriptTotalTimeMap.put(timeRuleScript, ruleResult.scriptTime() + scriptTotalTimeMap.get(timeRuleScript));

            if (ruleResult instanceof IEplTimeRuleDao.IRuleResultEmpty) ruleResult = null;
        }
        return ruleResult;
    }

    @Override
    public void updateTimeRules(Map<EplTimeRuleEduGroupScript, Integer> scriptRequestCountMap, Map<EplTimeRuleEduGroupScript, Long> scriptTotalTimeMap)
    {
        if (scriptRequestCountMap.size() == 0)
            return;
        DQLValuesBuilder toUpdate = new DQLValuesBuilder(TIME_RULE, "time", "req");
        for (Map.Entry<EplTimeRuleEduGroupScript, Integer> entry : scriptRequestCountMap.entrySet())
        {
            EplTimeRuleEduGroupScript rule = entry.getKey();
            int requestCount = entry.getValue();
            Long totalTime = scriptTotalTimeMap.get(rule);
            toUpdate.rowValues(rule.getId(), totalTime, requestCount);
        }

        new DQLUpdateBuilder(EplTimeRuleEduGroupScript.class)
                .fromDataSource(toUpdate.build(), VALUES)
                .where(eq(property(VALUES, TIME_RULE), property(EplTimeRuleEduGroupScript.id())))
                .set(EplTimeRuleEduGroupScript.totalTimeWork().s(), plus(property(EplTimeRuleEduGroupScript.totalTimeWork().s()), property(VALUES, "time")))
                .set(EplTimeRuleEduGroupScript.requestCount().s(), plus(property(EplTimeRuleEduGroupScript.requestCount().s()), property(VALUES, "req")))
                .createStatement(getSession()).execute();
        executeFlush();
    }

    @Override
    public EplAssignmentCommonWrapper getEplTimeDataWrapperForEduAssignmentPrint(Long ouSummaryId, List<Long> yearPartIds, List<Long> developFormIds, List<Long> developConditionIds)
    {
        EplOrgUnitSummary ouSummary = getNotNull(ouSummaryId);
        return new EplAssignmentCommonWrapper(Collections.singleton(ouSummary), false, true)
        {
            @Override
            protected void applyWhereConditionsEduGroup(String alias, DQLSelectBuilder dql)
            {
                DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                        .fromEntity(EplEduGroupRow.class, "gr").column("gr.id").top(1)
                        .where(eq(property("gr", EplEduGroupRow.group()), property(alias, EplEduGroupTimeItem.eduGroup())));

                if (CollectionUtils.isNotEmpty(developFormIds))
                {
                    subBuilder.where(in(property("gr", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().educationOrgUnit().developForm().id()), developFormIds));
                }
                if (CollectionUtils.isNotEmpty(developConditionIds))
                {
                    subBuilder.where(in(property("gr", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().educationOrgUnit().developCondition().id()), developConditionIds));
                }
                if (CollectionUtils.isNotEmpty(yearPartIds))
                {
                    dql.where(in(property(alias, EplEduGroupTimeItem.eduGroup().yearPart().id()), yearPartIds));
                }
                dql.where(isNotNull(property(alias, EplEduGroupTimeItem.eduGroup())));
                dql.where(exists(subBuilder.buildQuery()));
            }
        }.fillDataSource(null, true);
    }

    @Override
    public EplAssignmentCommonWrapper getEplTimeDataWrapperForPps(EplIndividualPlan plan)
    {
        List<EplOrgUnitSummary> ouSummaries = EplIndividualPlanManager.instance().dao().getOuSummaries(plan);

        return new EplAssignmentCommonWrapper(ouSummaries, true, true, plan)
        {
            @Override
            protected void applyWhereConditionsEduGroup(String alias, DQLSelectBuilder dql)
            {
                dql.joinDataSource(alias, DQLJoinType.inner, getPlannedPpsTimeItemDql(plan).buildQuery(), "ppsTime", eq(property("ppsTime.timeItemId"), property(alias, "id")));
                dql.where(isNotNull(property(alias, EplEduGroupTimeItem.eduGroup())));
            }

            @Override
            protected void applyWhereConditionsSimple(String alias, DQLSelectBuilder dql)
            {
                dql.joinDataSource(alias, DQLJoinType.inner, getPlannedPpsTimeItemDql(plan).buildQuery(), "ppsTime", eq(property("ppsTime.timeItemId"), property(alias, "id")));
            }
        }.fillDataSource(null, true);
    }

    @Override
    public boolean existsPlannedPpsTimeItemForPps(EplIndividualPlan plan)
    {
        return ISharedBaseDao.instance.get().existsEntity(getPlannedPpsTimeItemDql(plan).top(1).buildQuery());
    }

    private DQLSelectBuilder getPlannedPpsTimeItemDql(EplIndividualPlan plan)
    {
        List<EplOrgUnitSummary> ouSummaries = EplIndividualPlanManager.instance().dao().getOuSummaries(plan);

        return new DQLSelectBuilder().fromEntity(EplPlannedPpsTimeItem.class, "ppsTime")
                .column(property("ppsTime", EplPlannedPpsTimeItem.timeItem().id()), "timeItemId")
                .joinPath(DQLJoinType.inner, EplPlannedPpsTimeItem.pps().fromAlias("ppsTime"), "pps")
                .where(ne(property("ppsTime", EplPlannedPpsTimeItem.timeAmountAsLong()), value(0)))
                .where(in(property("pps", EplPlannedPps.orgUnitSummary()), ids(ouSummaries)))
                .where(eq(property("pps", EplPlannedPps.ppsEntry()), value(plan.getPps())));
    }

    @Override
    public Map<EplEduGroup, EplEduGroupType> getEduGroupTypeMap(Set<EplEduGroup> eduGroups)
    {
        Map<EplEduGroup, EplEduGroupType> resultMap = Maps.newHashMap();

        Map<EplEduGroup, Set<EplEduGroupRow>> eduGroup2RowMap = Maps.newHashMap();
        Map<EplEduGroup, Set<Long>> eduGroup2EplGroupMap = Maps.newHashMap();
        Map<MultiKey, Set<Long>> key2RowIdMap = Maps.newHashMap();

        List<EplEduGroupRow> eduGroupRows = getList(EplEduGroupRow.class, EplEduGroupRow.group(), eduGroups);

        for (EplEduGroupRow row : eduGroupRows)
        {
            EplEduGroup eduGroup = row.getGroup();
            Long eplGroupId = row.getStudentWP2GTypeSlot().getStudentWPSlot().getStudent().getGroup().getId();
            MultiKey key = new MultiKey(eplGroupId, eduGroup.getGroupType().getId(), eduGroup.getYearPart().getId(), eduGroup.getRegistryElementPart().getId());

            SafeMap.safeGet(eduGroup2RowMap, eduGroup, HashSet.class).add(row);
            SafeMap.safeGet(eduGroup2EplGroupMap, eduGroup, HashSet.class).add(eplGroupId);
            SafeMap.safeGet(key2RowIdMap, key, HashSet.class).add(row.getGroup().getId());
        }

        for (Map.Entry<EplEduGroup, Set<EplEduGroupRow>> entry : eduGroup2RowMap.entrySet())
        {
            EplEduGroup eduGroup = entry.getKey();
            boolean group = true;

            for (EplEduGroupRow row : entry.getValue())
            {
                Long eplGroupId = row.getStudentWP2GTypeSlot().getStudentWPSlot().getStudent().getGroup().getId();
                MultiKey key = new MultiKey(eplGroupId, eduGroup.getGroupType().getId(), eduGroup.getYearPart().getId(), eduGroup.getRegistryElementPart().getId());
                if (key2RowIdMap.get(key).size() > 1) group = false;
            }

            EplEduGroupType type;
            Set<Long> eplGroups = eduGroup2EplGroupMap.get(eduGroup);

            if (eplGroups.size() > 1)
                type = EplEduGroupType.FLOW;
            else if (eplGroups.size() == 1 && group)
                type = EplEduGroupType.GROUP;
            else
                type = EplEduGroupType.SUB_GROUP;

            resultMap.put(eduGroup, type);
        }
        return resultMap;
    }

    @Override
    public boolean isBlocked(EplOrgUnitSummary summary)
    {
        return NamedSyncInTransactionCheckLocker.hasLock(getLockName(summary));
    }

    private String getLockName(EplOrgUnitSummary summary)
    {
        return "eplSummaryOUTimeItemRefresh_" + summary.getId();
    }

    @Override
    public boolean hasIncorrectTime(EplOrgUnitSummary summary)
    {
        Long result = new DQLSelectBuilder().fromEntity(EplTimeItem.class, "t")
                .column(property("t.id")).top(1)
                .where(eq(property("t", EplTimeItem.summary()), value(summary)))
                .where(ne(property("t", EplTimeItem.timeAmountAsLong()), property("t", EplTimeItem.controlTimeAmountAsLong())))
                .createStatement(getSession()).uniqueResult();

        if (null != result) return true;

        result = new DQLSelectBuilder().fromEntity(EplTransferOrgUnitEduGroupTimeItem.class, "t")
                .column(property("t.id")).top(1)
                .where(eq(property("t", EplTransferOrgUnitEduGroupTimeItem.transferredFrom()), value(summary)))
                .where(ne(property("t", EplTransferOrgUnitEduGroupTimeItem.timeAmountAsLong()), property("t", EplTransferOrgUnitEduGroupTimeItem.controlTimeAmountAsLong())))
                .createStatement(getSession()).uniqueResult();

        return null != result;
    }

    @Override
    public void saveOrUpdateTimeItem(EplSimpleTimeItem timeItem)
    {
        if (!(timeItem.getTimeRule() instanceof EplTimeRuleSimple)) {
            throw new IllegalArgumentException();
        }
        if (timeItem.getCreationDate() == null) {
            timeItem.setCreationDate(new Date());
        }
        EplTimeRuleSimple timeRule = (EplTimeRuleSimple) timeItem.getTimeRule();
        double result = timeRule.isParameterNeeded() ? timeRule.getCoefficient() * timeItem.getParameter() : timeRule.getCoefficient();

        timeItem.setTimeAmount(result);
        timeItem.setControlTimeAmount(result);
        saveOrUpdate(timeItem);
    }

    @Override
    public void deleteEduGroupTimeItem(Long deleteId)
    {
        EplEduGroupTimeItem deleteTimeItem = getNotNull(deleteId);

        if (deleteTimeItem instanceof EplTransferOrgUnitEduGroupTimeItem)
        {
            getSession().flush();
            getSession().clear();

            EplTransferOrgUnitEduGroupTimeItem deleteTransferTimeItem = (EplTransferOrgUnitEduGroupTimeItem) deleteTimeItem;
            EplOrgUnitSummary transferTo = (EplOrgUnitSummary) getSession().load(EplOrgUnitSummary.class, deleteTransferTimeItem.getTransferredFrom().getId());
            EplOrgUnitSummary transferSummary = (EplOrgUnitSummary) getSession().load(EplOrgUnitSummary.class, deleteTransferTimeItem.getSummary().getId());

            if (!transferTo.getState().isFormative())
                throw new ApplicationException("Состояние расчета на подразделении, с которого переданы часы, отлично от «Формируется».");

            if (!transferSummary.getState().isFormative())
                throw new ApplicationException("Состояние расчета на подразделении, куда переданы часы, отлично от «Формируется».");

            EplEduGroupTimeItem timeItem = new EplEduGroupTimeItem();
            timeItem.update(deleteTransferTimeItem);
            timeItem.setSummary(transferTo);
            saveOrUpdate(timeItem);
            delete(deleteTimeItem);
        }
        else
        {
            deleteTimeItem.setTimeAmountAsLong(0);
            saveOrUpdate(deleteTimeItem);
        }
    }

    @Override
    public List<Long> getRegElementPartIdsOnItemTime(EplOrgUnitSummary summary)
    {
        return new DQLSelectBuilder().fromEntity(EplEduGroupTimeItem.class, "t")
                .column(property("t", EplEduGroupTimeItem.eduGroup().registryElementPart().id())).distinct()
                .where(notInstanceOf("t", EplTransferOrgUnitEduGroupTimeItem.class))
                .where(eq(property("t", EplEduGroupTimeItem.summary()), value(summary)))
                .createStatement(getSession()).list();
    }

    @Override
    public Map<EplEduGroupTimeItemKey, EplTransferOrgUnitEduGroupTimeItem> getCurrentTransferTimeItems(EplOrgUnitSummary summary, EplCommonRowWrapper.DisciplineOrRulePairKey key, Set<Long> transferCategories)
    {
        Map<EplEduGroupTimeItemKey, EplTransferOrgUnitEduGroupTimeItem> resultMap = Maps.newLinkedHashMap();
        if (null == summary || null == key) return resultMap;

        Long disciplineId = key.getDisciplineId();
        Long ruleId = key.getRuleId();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EplTransferOrgUnitEduGroupTimeItem.class, "e").column("e")
                .where(eq(property("e", EplTransferOrgUnitEduGroupTimeItem.transferredFrom()), value(summary)))
                .order(property("e", EplEduGroupTimeItem.eduGroup().title()));

        applyEduGroupTimeItemFilter(builder, "e", key, transferCategories);

        DQLSelectBuilder groupRowBuilder = new DQLSelectBuilder().fromEntity(EplEduGroupRow.class, "r").column(property("r"))
                .joinPath(DQLJoinType.inner, EplEduGroupRow.group().fromAlias("r"), "g")
                .joinPath(DQLJoinType.inner, EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().fromAlias("r"), "wpSlot")
                .joinPath(DQLJoinType.inner, EplStudentWPSlot.student().fromAlias("wpSlot"), "st");

        FilterUtils.applySelectFilter(groupRowBuilder, "g", EplEduGroup.registryElementPart().id(), disciplineId);
        if (null != ruleId)
            groupRowBuilder.where(exists(EplEduGroupTimeItem.class,
                                         EplEduGroupTimeItem.eduGroup().s(), property("g"),
                                         EplEduGroupTimeItem.timeRule().id().s(), ruleId)
            );

        builder.where(exists(groupRowBuilder.where(eq(property("g.id"), property("e", EplEduGroupTimeItem.eduGroup().id()))).buildQuery()));

        for (EplTransferOrgUnitEduGroupTimeItem transferTimeItem : builder.createStatement(getSession()).<EplTransferOrgUnitEduGroupTimeItem>list())
        {
            resultMap.put(new EplEduGroupTimeItemKey(transferTimeItem, summary), transferTimeItem);
        }
        return resultMap;
    }

    @Override
    public Map<EplEduGroupTimeItemKey, EplTransferOrgUnitEduGroupTimeItem> getTransferTimeItems(EplOrgUnitSummary summary, Collection<Long> timeItemIds, OrgUnit readingOrgUnit)
    {
        Map<EplEduGroupTimeItemKey, EplTransferOrgUnitEduGroupTimeItem> resultMap = Maps.newLinkedHashMap();
        List<EplEduGroupTimeItem> timeItems = getList(EplEduGroupTimeItem.class, timeItemIds, EplEduGroupTimeItem.eduGroup().title().s());

        EplOrgUnitSummary newOuSummary = new DQLSelectBuilder()
                .fromEntity(EplOrgUnitSummary.class, "s").fromEntity(EplEduGroupTimeItem.class, "ti")
                .column(property("s")).top(1)
                .where(eq(property("s", EplOrgUnitSummary.studentSummary().id()), property("ti", EplEduGroupTimeItem.summary().studentSummary().id())))
                .where(eq(property("s", EplOrgUnitSummary.orgUnit().id()), value(readingOrgUnit)))
                .where(in(property("ti.id"), timeItemIds))
                .createStatement(getSession()).uniqueResult();

        for (EplEduGroupTimeItem timeItem : timeItems)
        {
            EplTransferOrgUnitEduGroupTimeItem transferTimeItem = new EplTransferOrgUnitEduGroupTimeItem();
            transferTimeItem.update(timeItem);
            transferTimeItem.setSummary(newOuSummary);
            transferTimeItem.setTransferredFrom(summary);

            resultMap.put(new EplEduGroupTimeItemKey(transferTimeItem, summary), transferTimeItem);
        }
        return resultMap;
    }

    @Override
    public void doUpdateTransferTimeItems(EplOrgUnitSummary summary, EplCommonRowWrapper.DisciplineOrRulePairKey discOrRulePairKey, Collection<EplTransferOrgUnitEduGroupTimeItem> currTimeItems, Set<Long> transferCategories)
    {
        Collection<EplTransferOrgUnitEduGroupTimeItem> oldTimeItems = getCurrentTransferTimeItems(summary, discOrRulePairKey, transferCategories).values();
        DQLSelectBuilder timeItemsDql = new DQLSelectBuilder().fromEntity(EplEduGroupTimeItem.class, "e").column("e")
                .where(eq(property("e", EplEduGroupTimeItem.summary()), value(summary)));

        applyEduGroupTimeItemFilter(timeItemsDql, "e", discOrRulePairKey, null);

        List<EplEduGroupTimeItem> timeItems = timeItemsDql.createStatement(getSession()).list();

        new MergeAction.SessionMergeAction<MultiKey, EplTransferOrgUnitEduGroupTimeItem>() {
            @Override protected EplTransferOrgUnitEduGroupTimeItem buildRow(EplTransferOrgUnitEduGroupTimeItem source) { return source; }
            @Override protected MultiKey key(EplTransferOrgUnitEduGroupTimeItem source) { return new EplEduGroupTimeItemKey(source).getKey(); }
            @Override protected void fill(EplTransferOrgUnitEduGroupTimeItem target, EplTransferOrgUnitEduGroupTimeItem source) { target.update(source); }

            @Override
            protected void doDeleteRecord(EplTransferOrgUnitEduGroupTimeItem databaseRecord)
            {
                EplEduGroupTimeItem timeItem = new EplEduGroupTimeItem();
                timeItem.update(databaseRecord);
                timeItem.setSummary(databaseRecord.getTransferredFrom());
                save(timeItem);

                super.doDeleteRecord(databaseRecord);
            }

            @Override
            protected void doSaveRecord(EplTransferOrgUnitEduGroupTimeItem databaseRecord)
            {
                EplEduGroupTimeItemKey timeItemKey = new EplEduGroupTimeItemKey(databaseRecord, databaseRecord.getTransferredFrom());
                MultiKey key = timeItemKey.getKey();

                for (EplEduGroupTimeItem timeItem : timeItems)
                {
                    if (key.equals(new EplEduGroupTimeItemKey(timeItem).getKey()))
                        delete(timeItem);
                }

                super.doSaveRecord(databaseRecord);
            }
        }.merge(oldTimeItems, currTimeItems);
    }

    public EplOrgUnitSummary getSummaryTransferTo(EplOrgUnitSummary summary, EplCommonRowWrapper.DisciplineOrRulePairKey discOrRuleKey, EplTimeCategoryEduLoad category)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EplTransferOrgUnitEduGroupTimeItem.class, "t")
                .column(property("t", EplTransferOrgUnitEduGroupTimeItem.transferredFrom())).top(1)
                .where(eq(property("t", EplTransferOrgUnitEduGroupTimeItem.summary()), value(summary)))
                .where(eq(property("t", EplTransferOrgUnitEduGroupTimeItem.timeRule().category()), value(category)));

        applyEduGroupTimeItemFilter(builder, "t", discOrRuleKey, null);

        return builder.createStatement(getSession()).uniqueResult();
    }

    @Override
    public IDiscOrRuleData getCurrentDiscOrRuleKeyCategories(EplOrgUnitSummary summary, EplCommonRowWrapper.DisciplineOrRulePairKey discOrRulePairKey)
    {
        Set<Long> categories = Sets.newHashSet();
        Set<Long> transferCategories = Sets.newHashSet();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EplEduGroupTimeItem.class, "t")
                .fetchPath(DQLJoinType.left, EplEduGroupTimeItem.registryElementPart().registryElement().fromAlias("t"), "regEl")
                .fetchPath(DQLJoinType.left, EplEduGroupTimeItem.timeRule().fromAlias("t"), "rule")
                .where(or(
                        eq(property("t", EplEduGroupTimeItem.summary()), value(summary)),
                        exists(EplTransferOrgUnitEduGroupTimeItem.class,
                               EplTransferOrgUnitEduGroupTimeItem.id().s(), property("t.id"),
                               EplTransferOrgUnitEduGroupTimeItem.transferredFrom().s(), summary)
                ));

        List<EplEduGroupTimeItem> allTimeItems = builder.createStatement(getSession()).list();
        for (EplEduGroupTimeItem timeItem : allTimeItems)
        {
            EppRegistryElementPart discipline = timeItem.getRegistryElementPart();
            EplTimeRuleEduLoad rule = timeItem.getTimeRule();
            String groupingCode = rule.getGrouping().getCode();
            Long categoryId = rule.getCategory().getId();

            EplCommonRowWrapper.DisciplineOrRulePairKey groupingKey = EplCommonRowWrapper.getKey(discipline, rule, groupingCode);
            if (Arrays.equals(discOrRulePairKey.getKeys(), groupingKey.getKeys()))
            {
                categories.add(categoryId);
                if (timeItem instanceof EplTransferOrgUnitEduGroupTimeItem)
                {
                    EplOrgUnitSummary transferTimeItem = ((EplTransferOrgUnitEduGroupTimeItem) timeItem).getTransferredFrom();
                    if (summary.equals(transferTimeItem))
                        transferCategories.add(categoryId);
                }
            }
        }
        return new IDiscOrRuleData()
        {
            @Override public Set<Long> categories() { return categories; }
            @Override public Set<Long> transferCategories() { return transferCategories; }
        };
    }

    @Override
    public boolean doRefreshFull()
    {
        return refreshPartial(null, null, null);
    }

    @Override
    public boolean refreshPartial(DaemonQueueManager.DaemonQueueItemIterator<EplEduGroup> groupIterator, Iterator<EplOrgUnitSummary> summaryIterator, Iterator<EplTimeRuleSimple> simpleRuleIterator)
    {
        final EplRecalculateSummaryKind kind = (groupIterator == null && simpleRuleIterator == null) ? EplRecalculateSummaryKind.CHECK : EplRecalculateSummaryKind.PARTIAL_CHECK;
        final Set<EplTimeRuleEduLoad> rules = new HashSet<>();
        HashSet<EplOrgUnitSummary> summaries = new HashSet<>();
        if (kind == EplRecalculateSummaryKind.PARTIAL_CHECK)
        {
            simpleRuleIterator.forEachRemaining(rules::add);

            if (summaryIterator != null)
                while(summaryIterator.hasNext())
                    summaries.add(summaryIterator.next());
        }

        if ((groupIterator == null || groupIterator.isEmpty()) && summaries.isEmpty())
        {
            Iterator<Object> iterator = getOrgUnitSummaryBuilder().distinct().createStatement(getSession()).iterate();

            while(iterator.hasNext())
                summaries.add(get(EplOrgUnitSummary.class, (Long)iterator.next()));
        }
        else
        {
            new DQLSelectBuilder()
                    .distinct()
                    .fromEntity(EplOrgUnitSummary.class, SUMMARY)
                    .joinPath(DQLJoinType.inner, EplOrgUnitSummary.orgUnit().fromAlias(SUMMARY), ORG_UNIT, false)
                    .joinEntity(SUMMARY, DQLJoinType.inner, EplEduGroup.class, GROUP, false,
                            eq(property(SUMMARY, EplOrgUnitSummary.studentSummary()), property(GROUP, EplEduGroup.summary())))
                    .joinEntity(GROUP, DQLJoinType.inner, DaemonQueueItem.class, DAEMON_QUEUE_ITEM, false,
                            and(eq(property(GROUP, EplEduGroup.id()), property(DAEMON_QUEUE_ITEM, DaemonQueueItem.entity())),
                            eq(property(DAEMON_QUEUE_ITEM, DaemonQueueItem.queue()), value(groupIterator.getQueueId())),
                            le(property(DAEMON_QUEUE_ITEM, DaemonQueueItem.id()), value(groupIterator.getLastItemId()))))
                    .where(eq(property(ORG_UNIT), property(GROUP, EplEduGroup.registryElementPart().registryElement().owner())))
                    .column(SUMMARY)
                    .createStatement(getSession())
                    .list().stream().forEach((summary) -> summaries.add((EplOrgUnitSummary) summary));
        }

        IEplOuSummaryDAO dao = EplOuSummaryManager.instance().dao();
        Iterator<EplOrgUnitSummary> iterator = summaries.stream().filter(s -> s != null).sorted(Comparator.comparing(EntityBase::getId)).iterator();

        ExecutorService executorService = Executors.newFixedThreadPool(4);
        EplRecalculateSummaryData rData = prepareRecalculateData(kind,  new ProcessState(ProcessDisplayMode.unknown) , null, rules);

        while (iterator.hasNext())
        {
            EplOrgUnitSummary summary = iterator.next();
            executorService.submit(new Callable<Void>()
            {
                @Override
                public Void call() throws Exception
                {
                    ContextLocal.begin(null);
                    if (!dao.doRecalculateSummary(rData.withSummary(summary), groupIterator))
                        throw new InterruptedException();
                    ContextLocal.end();
                    return null;
                }
            });
        }
        executorService.shutdown();
        try
        {
            executorService.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e)
        {
            return false;
        }
        return true;
    }

    @Override
    public EplBaseCategoryDataWrapper getCategoryDataWrapper(Collection<EplOrgUnitSummary> ouSummaries)
    {
        Collection<Long> ouSummaryIds = ids(ouSummaries);

        // заполняем категории
        Set<EplTimeCategoryEduLoad> usedCategories =
                new DQLSelectBuilder()
                        .fromEntity(EplTimeCategoryEduLoad.class, "c").column("c")
                        .where(exists(EplTimeItem.class,
                                      EplTimeItem.summary().s(), ouSummaryIds,
                                      EplTimeItem.timeRule().category().s(), property("c")))
                        .createStatement(getSession()).<EplTimeCategoryEduLoad>list()
                        .stream().collect(Collectors.toSet());

        Set<EplTimeCategoryEduLoad> transferUsedCategories =
                new DQLSelectBuilder()
                        .fromEntity(EplTimeCategoryEduLoad.class, "c").column("c")
                        .where(exists(EplTransferOrgUnitEduGroupTimeItem.class,
                                      EplTransferOrgUnitEduGroupTimeItem.transferredFrom().s(), ouSummaryIds,
                                      EplTransferOrgUnitEduGroupTimeItem.timeRule().category().s(), property("c")))
                        .createStatement(getSession()).<EplTimeCategoryEduLoad>list()
                        .stream().collect(Collectors.toSet());

        usedCategories.addAll(transferUsedCategories);
        if (CollectionUtils.isEmpty(usedCategories)) return null;

        return getCategoryDataWrapper(usedCategories, transferUsedCategories);
    }

    @Override
    public IDistributionEduGroupData prepareDistributionEduGroupData(Collection<EplOrgUnitSummary> ouSummaries, PpsEntry ppsEntry, boolean transferTo)
    {
        final SetMultimap<Long, Term> termMap = HashMultimap.create();
        final SetMultimap<Long, YearDistributionPart> partMap = HashMultimap.create();
        final Map<Long, Long> studentCountMap = Maps.newHashMap();

        DQLSelectBuilder eduGroupRowDql1 = getEduGroupRowBuilder("r", ouSummaries, transferTo);
        DQLSelectBuilder eduGroupRowDql2 = getEduGroupRowBuilder("r", ouSummaries, transferTo);

        if (null != ppsEntry)
        {
            DQLSelectBuilder ppsBuilder = new DQLSelectBuilder().fromEntity(EplPlannedPpsTimeItem.class, "ppsTime")
                    .column(property("eduTI", EplEduGroupTimeItem.eduGroup().id()), "eduGroupId").distinct()
                    .joinEntity("ppsTime", DQLJoinType.inner, EplEduGroupTimeItem.class, "eduTI", eq(property("ppsTime", EplPlannedPpsTimeItem.timeItem()), property("eduTI")))
                    .joinPath(DQLJoinType.inner, EplPlannedPpsTimeItem.pps().fromAlias("ppsTime"), "pps")
                    .where(ne(property("ppsTime", EplPlannedPpsTimeItem.timeAmountAsLong()), value(0)))
                    .where(in(property("pps", EplPlannedPps.orgUnitSummary()), ids(ouSummaries)))
                    .where(eq(property("pps", EplPlannedPps.ppsEntry()), value(ppsEntry)));

            eduGroupRowDql1.joinDataSource("r", DQLJoinType.inner, ppsBuilder.buildQuery(), "ppsTime", eq(property("ppsTime.eduGroupId"), property("r", EplEduGroupRow.group().id())));
            eduGroupRowDql2.joinDataSource("r", DQLJoinType.inner, ppsBuilder.buildQuery(), "ppsTime", eq(property("ppsTime.eduGroupId"), property("r", EplEduGroupRow.group().id())));
        }

        eduGroupRowDql1
                .group(property("r", EplEduGroupRow.group().id()))
                .column(property("r", EplEduGroupRow.group().id()))
                .column(DQLFunctions.sum(property("r", EplEduGroupRow.count())))
                .createStatement(getSession()).<Object[]>list()
                .forEach(row -> studentCountMap.put((Long) row[0], (Long) row[1]));

        eduGroupRowDql2
                .column(property("r", EplEduGroupRow.group().id()))
                .column(property("r", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().term()))
                .column(property("r", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().yearPart()))
                .createStatement(getSession()).<Object[]>list()
                .forEach(row -> {
                    termMap.put((Long) row[0], (Term) row[1]);
                    partMap.put((Long) row[0], (YearDistributionPart) row[2]);
                });

        return new IDistributionEduGroupData()
        {
            @Override public SetMultimap<Long, Term> termMap() { return termMap; }
            @Override public SetMultimap<Long, YearDistributionPart> partMap() { return partMap; }
            @Override public Map<Long, Long> studentCountMap() { return studentCountMap; }
        };
    }

    @Override
    public <T extends EplBaseTimeDataWrapper> T fillSummaryTimeDataWrapper(T dataWrapper)
    {
        Collection<EplOrgUnitSummary> ouSummaries = dataWrapper.getOuSummaries();
        Set<String> propertyPathSet = dataWrapper.getPropertyPathSet();
        boolean withSimpleTimeItem = dataWrapper.isWithSimpleTimeItem();
        boolean fillTimeItem = dataWrapper.isFillTimeItem();
        boolean select = dataWrapper.isSelect();

        EplBaseCategoryDataWrapper categoryDataWrapper = null;
        if (!select)
        {
            categoryDataWrapper = getCategoryDataWrapper(ouSummaries);
            if (null == categoryDataWrapper) return null;
        }
        dataWrapper.setCategoryDataWrapper(categoryDataWrapper);

        long parentId = 0L;
        EplCommonParentWrapper parentRegElementWrapper = new EplCommonParentWrapper(++parentId, BY_REG_ELEMENT);
        EplCommonParentWrapper parentTimeRuleWrapper = new EplCommonParentWrapper(++parentId, BY_TIME_RULE);
        EplCommonParentWrapper parentSimpleTimeRuleWrapper = new EplCommonParentWrapper(++parentId, EplSimpleRuleRowWrapper.BY_SIMPLE_TIME_RULE);

        // заполняем часы для План. потока
        MutableLong rowIdCounter = new MutableLong(0);
        List<EplEduGroupTimeItem> eduGroupTimeItems = dataWrapper.getEduGroupTimeItems();
        for (EplEduGroupTimeItem timeItem : eduGroupTimeItems)
        {
            EppRegistryElementPart discipline = timeItem.getRegistryElementPart();
            EplTimeRuleEduLoad rule = timeItem.getTimeRule();
            String groupingCode = rule.getGrouping().getCode();
            Long categoryId = rule.getCategory().getId();

            Map<EplCommonRowWrapper.DisciplineOrRulePairKey, EplDiscOrRuleRowWrapper> discRowMapByGroup = dataWrapper.getDiscRowMap().getOrDefault(groupingCode, Maps.newHashMap());
            if (discRowMapByGroup.isEmpty())
                dataWrapper.getDiscRowMap().put(groupingCode, discRowMapByGroup);

            // отбираем доп. значения для ключа (по полям рассчитанных часов)
            Object[] groupProperties = getTimeItemAdditionalGroupKey(timeItem, propertyPathSet);

            EplCommonRowWrapper.DisciplineOrRulePairKey groupingKey = EplCommonRowWrapper.getKey(discipline, rule, groupingCode, groupProperties);

            // если необходимо заполнить датасорс данными для одной строки
            // if (null != discOrRuleKey && !Arrays.equals(discOrRuleKey.getKeys(), groupingKey.getKeys())) continue;

            EplDiscOrRuleRowWrapper rowWrapper = discRowMapByGroup.get(groupingKey);
            if (null == rowWrapper)
            {
                rowIdCounter.decrement();
                Long id = rowIdCounter.longValue();

                discipline = groupingKey.getDisciplineId() == null ? null : discipline;
                rule = groupingKey.getRuleId() == null ? null : rule;
                String title = EplCommonRowWrapper.getTitle(discipline, rule, groupingCode);

                if (select)
                {
                    discipline = null;
                    rule = null;
                }
                discRowMapByGroup.put(groupingKey, rowWrapper = new EplDiscOrRuleRowWrapper(id, discipline, rule, groupingKey));
                rowWrapper.setTitle(title);

                if (groupingCode.equals(BY_REG_ELEMENT) || groupingCode.equals(BY_REG_ELEMENT_AND_TIME_RULE))
                    rowWrapper.setHierarchyParent(parentRegElementWrapper);
                else if (groupingCode.equals(BY_TIME_RULE))
                    rowWrapper.setHierarchyParent(parentTimeRuleWrapper);

                if (!select) rowWrapper.init();
            }

            if (select) continue;

            EplEduGroup eduGroup = timeItem.getEduGroup();
            rowWrapper.getEduGroupTimeItemList().add(timeItem);
            if (null != eduGroup)
            {
                rowWrapper.getGroups().get(categoryId).add(eduGroup.getId());
                rowWrapper.getEplGroups().add(eduGroup);
                dataWrapper.getEduGroups().add(eduGroup);
            }

            if (timeItem instanceof EplTransferOrgUnitEduGroupTimeItem)
            {
                EplOrgUnitSummary transferTimeItem = ((EplTransferOrgUnitEduGroupTimeItem) timeItem).getTransferredFrom();
                if (!ouSummaries.contains(transferTimeItem))
                    rowWrapper.setTransferTo(transferTimeItem);
                else
                {
                    if (categoryDataWrapper.getTransferCategoryMap().containsKey(categoryId))
                    {
                        fillDiscTimeItemMap(dataWrapper, timeItem, rowWrapper, true);
                        if (null != eduGroup)
                            rowWrapper.getTransferGroups().get(categoryId).add(eduGroup.getId());
                    }
                }
            }

            if (fillTimeItem)
                fillDiscTimeItemMap(dataWrapper, timeItem, rowWrapper, false);
        }

        if (!select)
        {
            Map<EplEduGroup, Map<EplStudent, Integer>> groupMap;
            dataWrapper.setGroupMap(groupMap = prepareGroupMap(ouSummaries, true, null));

            if (!dataWrapper.getDiscRowMap().isEmpty())
            {
                // Формруем список названий НПП студентов из План. потоков, попавших в каждую строку
                for (Map.Entry<String, Map<EplCommonRowWrapper.DisciplineOrRulePairKey, EplDiscOrRuleRowWrapper>> entry : dataWrapper.getDiscRowMap().entrySet())
                {
                    for (EplDiscOrRuleRowWrapper rowWrapper : entry.getValue().values())
                    {
                        List<String> titles = rowWrapper.getEplGroups().stream()
                                .flatMap(g -> groupMap.getOrDefault(g, Collections.emptyMap()).keySet().stream())
                                .map(EplStudent::getEducationOrgUnit)
                                .map(EducationOrgUnit::getProgramSubjectShortExtendedTitle)
                                .distinct()
                                .sorted()
                                .collect(Collectors.toList());
                        rowWrapper.setEducationOrgUnitTitles(RowCollectionFormatter.INSTANCE.format(titles));
                    }
                }
            }
        }

        if (withSimpleTimeItem)
        {
            // заполняем часы по простой норме
            List<EplSimpleTimeItem> simpleTimeItems = dataWrapper.getSimpleTimeItems();
            for (EplSimpleTimeItem timeItem : simpleTimeItems)
            {
                EplTimeRuleSimple rule = (EplTimeRuleSimple) timeItem.getTimeRule();
                String groupingCodeKey = EplSimpleRuleRowWrapper.BY_SIMPLE_TIME_RULE;
                String groupingCode = rule.getGrouping().getCode();

                // отбираем доп. значения для ключа (по полям рассчитанных часов)
                Object[] groupProperties = getTimeItemAdditionalGroupKey(timeItem, propertyPathSet);
                EplCommonRowWrapper.DisciplineOrRulePairKey groupingKey = EplCommonRowWrapper.getKey(null, rule, groupingCodeKey, groupProperties);

                // если необходимо заполнить датасорс данными для одной строки
                // if (null != discOrRuleKey && !Arrays.equals(discOrRuleKey.getKeys(), groupingKey.getKeys())) continue;

                Map<EplCommonRowWrapper.DisciplineOrRulePairKey, EplSimpleRuleRowWrapper> ruleRowMapByGroup = dataWrapper.getSimpleRuleRowMap().getOrDefault(groupingCodeKey, Maps.newHashMap());
                if (ruleRowMapByGroup.isEmpty())
                    dataWrapper.getSimpleRuleRowMap().put(groupingCodeKey, ruleRowMapByGroup);

                EplSimpleRuleRowWrapper rowWrapper = ruleRowMapByGroup.get(groupingKey);
                if (null == rowWrapper)
                {
                    rowIdCounter.decrement();
                    Long id = rowIdCounter.longValue();

                    String title = EplCommonRowWrapper.getTitle(null, rule, groupingCode);
                    if (select) rule = null;
                    ruleRowMapByGroup.put(groupingKey, rowWrapper = new EplSimpleRuleRowWrapper(id, rule, groupingKey));
                    rowWrapper.setTitle(title);
                    rowWrapper.setHierarchyParent(parentSimpleTimeRuleWrapper);

                    if (!select) rowWrapper.init();
                }

                if (select) continue;
                rowWrapper.getSimpleTimeItemList().add(timeItem);

                if (fillTimeItem)
                {
                    Long categoryId = rule.getCategory().getId();

                    putTimeSum4OneCategory(timeItem, rowWrapper, categoryId, false);
                    EplCategoryWrapper categoryWrapper = categoryDataWrapper.getCategoryMap().get(categoryId);

                    categoryWrapper.ruleTimeSum += timeItem.getTimeAmount();
                    dataWrapper.setRuleTimeSum(dataWrapper.getRuleTimeSum() + timeItem.getTimeAmount());
                }
            }
        }
        return dataWrapper;
    }

    private Object[] getTimeItemAdditionalGroupKey(EplTimeItem timeItem, Set<String> propertyPathSet)
    {
        Object[] groupProperties = null;
        if (CollectionUtils.isNotEmpty(propertyPathSet))
        {
            List<Object> tempProperties = Lists.newArrayList();
            for (String groupProperty : propertyPathSet)
            {
                Object property = null;
                try
                {
                    property = timeItem.getProperty(groupProperty);
                }
                catch (InvalidPropertyException ignored) {}

                if (property instanceof IEntity)
                {
                    property = ((IEntity) property).getId();
                }
                tempProperties.add(property);
            }
            groupProperties = tempProperties.toArray();
        }
        return groupProperties;
    }

    private void putTimeSum4OneCategory(EplTimeItem timeItem, EplCommonRowWrapper rowWrapper, Long categoryId, boolean transferTo)
    {
        PairKey<Long, Boolean> categoryKey = PairKey.create(categoryId, transferTo);
        EplCommonRowWrapper.ControlTime timeSum = rowWrapper.getTimeMap().get(categoryKey);
        if (timeSum == null)
        {
            timeSum = new EplCommonRowWrapper.ControlTime();
            rowWrapper.getTimeMap().put(categoryKey, timeSum);
        }

        if (timeItem.getControlTimeAmount() !=timeItem.getTimeAmount())
            timeSum.hasDifference = true;
        timeSum.time += timeItem.getTimeAmount();
        timeSum.controlTime += timeItem.getControlTimeAmount();

        if (transferTo)
            rowWrapper._transferTimeSum += timeItem.getTimeAmount();
        else
            rowWrapper._timeSum += timeItem.getTimeAmount();
    }

    @Override
    public EplBaseCategoryDataWrapper getCategoryDataWrapper(Set<EplTimeCategoryEduLoad> uCategories, Set<EplTimeCategoryEduLoad> tuCategories)
    {
        EplBaseCategoryDataWrapper dataWrapper = new EplBaseCategoryDataWrapper();
        dataWrapper.setUsedCategories(PairKey.create(uCategories, tuCategories));
        dataWrapper.setCategoryLevelCount(1);

        Comparator<EplCategoryWrapper> categoryComparator = (o1, o2) -> o1.getCategory().getPriority() - o2.getCategory().getPriority();

        for (boolean transfer : new boolean[]{false, true})
        {
            Set<EplTimeCategoryEduLoad> usedCategories;
            List<EplCategoryWrapper> topLevelCategoryList;
            List<EplCategoryWrapper> bottomLevelCategoryList;
            List<List<EplCategoryWrapper>> subCategories;
            Map<Long, EplCategoryWrapper> categoryMap;

            usedCategories          = transfer ? dataWrapper.getTransferUsedCategories() : dataWrapper.getUsedCategories();
            topLevelCategoryList    = transfer ? dataWrapper.getTransferTopLevelCategories() : dataWrapper.getTopLevelCategories();
            bottomLevelCategoryList = transfer ? dataWrapper.getTransferBottomLevelCategories() : dataWrapper.getBottomLevelCategories();
            subCategories           = transfer ? dataWrapper.getTransferSubcategories() : dataWrapper.getSubcategories();
            categoryMap             = transfer ? dataWrapper.getTransferCategoryMap() : dataWrapper.getCategoryMap();

            Set<EplTimeCategoryEduLoad> allCategories = Sets.newHashSet();
            allCategories.addAll(usedCategories);
            for (EplTimeCategoryEduLoad category : usedCategories)
            {
                EplTimeCategoryEduLoad parent = (EplTimeCategoryEduLoad) category.getParent();
                while (null != parent)
                {
                    allCategories.add(parent);
                    parent = (EplTimeCategoryEduLoad) parent.getParent();
                }
            }

            for (EplTimeCategoryEduLoad category : allCategories)
            {
                EplCategoryWrapper wrapper = new EplCategoryWrapper(category);
                categoryMap.put(category.getId(), wrapper);
                int level = 0;
                EplTimeCategoryEduLoad parent = (EplTimeCategoryEduLoad) category.getParent();
                while (null != parent)
                {
                    level++;
                    parent = (EplTimeCategoryEduLoad) parent.getParent();
                }
                dataWrapper.setCategoryLevelCount(Math.max(dataWrapper.getCategoryLevelCount(), 1 + level));
                wrapper.setLevel(level);
                if (0 == level)
                    topLevelCategoryList.add(wrapper);
            }

            categoryMap.values().stream()
                    .filter(wrapper -> wrapper.category.getParent() != null)
                    .forEach(wrapper -> categoryMap.get(wrapper.category.getParent().getId()).getChildren().add(wrapper));

            for (EplCategoryWrapper wrapper : categoryMap.values())
                Collections.sort(wrapper.getChildren(), categoryComparator);

            Collections.sort(topLevelCategoryList, categoryComparator);

            for (EplCategoryWrapper wrapper : topLevelCategoryList)
            {
                if (wrapper.getChildCount() == 0)
                    bottomLevelCategoryList.add(wrapper);
                else
                    bottomLevelCategoryList.addAll(wrapper.getChildren());
            }

            List<EplCategoryWrapper> parents = Lists.newArrayList();
            parents.addAll(topLevelCategoryList);
            while (!parents.isEmpty())
            {
                List<EplCategoryWrapper> children = Lists.newArrayList();
                for (EplCategoryWrapper wrapper : parents)
                    children.addAll(wrapper.getChildren());
                parents.clear();
                if (!children.isEmpty())
                {
                    subCategories.add(children);
                    parents.addAll(children);
                }
            }
        }
        return dataWrapper;
    }

    private void fillDiscTimeItemMap(EplBaseTimeDataWrapper dataWrapper, EplEduGroupTimeItem timeItem, EplDiscOrRuleRowWrapper rowWrapper, boolean transferTo)
    {
        Long categoryId = timeItem.getTimeRule().getCategory().getId();
        String groupingCode = rowWrapper.getKey().getGroupingCode();
        putTimeSum4OneCategory(timeItem, rowWrapper, categoryId, transferTo);

        double timeAmount = timeItem.getTimeAmount();
        EplBaseCategoryDataWrapper categoryDataWrapper = dataWrapper.getCategoryDataWrapper();
        dataWrapper.getDiscTotalRowTimeSumMap();

        EplCategoryWrapper categoryWrapper = transferTo ? categoryDataWrapper.getTransferCategoryMap().get(categoryId) : categoryDataWrapper.getCategoryMap().get(categoryId);
        Map<String, Double> discTimeSumMap = transferTo ? dataWrapper.getDiscTotalRowTimeSumMap().getSecond() : dataWrapper.getDiscTotalRowTimeSumMap().getFirst();

        Map<String, Double> categoryDiscTimeSumMap = categoryWrapper.getDiscTimeSumMap();
        categoryDiscTimeSumMap.put(groupingCode, categoryDiscTimeSumMap.getOrDefault(groupingCode, 0.0) + timeAmount);
        discTimeSumMap.put(groupingCode, discTimeSumMap.getOrDefault(groupingCode, 0.0) + timeAmount);
    }

    public static void applyEduGroupTimeItemFilter(DQLSelectBuilder dql, String alias, EplCommonRowWrapper.DisciplineOrRulePairKey key, Set<Long> categories)
    {
        FilterUtils.applySelectFilter(dql, alias, EplEduGroupTimeItem.registryElementPart().id(), key.getDisciplineId());
        FilterUtils.applySelectFilter(dql, alias, EplEduGroupTimeItem.timeRule().id(), key.getRuleId());
        FilterUtils.applySelectFilter(dql, alias, EplEduGroupTimeItem.timeRule().grouping().code(), key.getGroupingCode());
        FilterUtils.applySelectFilter(dql, alias, EplEduGroupTimeItem.timeRule().category().id(), categories);
    }

    public static class EplEduGroupTimeItemKey
    {
        MultiKey _key;

        public EplEduGroupTimeItemKey(EplEduGroupTimeItem timeItem)
        {
            this(timeItem, timeItem.getSummary());
        }

        public EplEduGroupTimeItemKey(EplEduGroupTimeItem timeItem, EplOrgUnitSummary summary)
        {
            Long eduGroupId = timeItem.getEduGroup() == null ? null : timeItem.getEduGroup().getId();
            _key = new MultiKey(summary.getId(), timeItem.getRegistryElementPart().getId(), eduGroupId, timeItem.getTimeRule().getId());
        }

        public MultiKey getKey() { return _key; }
    }

    public static ITimeItemData wrapTimeItem(
            final Date fillDate,
            final Map<EplTimeRuleEduGroupScript, IEplTimeRuleScript> scriptWrapperMap,
            final List<EplTimeRuleEduLoad> ruleList,
            final List<EppIControlActionType> icaTypes,
            final Map<Long, IEppRegElPartWrapper> registryData,
            final Map<MultiKey, EplEduGroupTimeItem> itemMap,
            final Map<MultiKey, EplTransferOrgUnitEduGroupTimeItem> transferItemMap)
    {
        return new ITimeItemData() {
            @Override public Date fillDate() { return fillDate; }
            @Override public Map<EplTimeRuleEduGroupScript, IEplTimeRuleScript> scriptWrapperMap() { return scriptWrapperMap; }
            @Override public List<EplTimeRuleEduLoad> ruleList() { return ruleList; }
            @Override public List<EppIControlActionType> icaTypes() { return icaTypes; }
            @Override public Map<Long, IEppRegElPartWrapper> registryData() { return registryData; }
            @Override public Map<MultiKey, EplEduGroupTimeItem> itemMap() { return itemMap; }
            @Override public Map<MultiKey, EplTransferOrgUnitEduGroupTimeItem> transferItemMap() { return transferItemMap; }
        };
    }
}
