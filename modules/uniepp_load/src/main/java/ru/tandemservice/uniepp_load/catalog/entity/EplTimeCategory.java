package ru.tandemservice.uniepp_load.catalog.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uniepp_load.catalog.entity.gen.EplTimeCategoryGen;

import static org.tandemframework.hibsupport.dql.DQLExpressions.notExists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/** @see ru.tandemservice.uniepp_load.catalog.entity.gen.EplTimeCategoryGen */
public abstract class EplTimeCategory extends EplTimeCategoryGen implements IDynamicCatalogItem, ITitled
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EplTimeCategory.class)
                .customize((alias, dql, context, filter) -> dql.where(notExists(EplTimeCategory.class, EplTimeCategory.parent().s(), property(alias))))
                .where(EplTimeCategory.enabled(), Boolean.TRUE)
                .titleProperty(EplTimeCategory.title().s())
                .order(EplTimeCategory.title())
                .filter(EplTimeCategory.title());
    }
}