/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupSplitTab;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic.EplStudentSummarySplitElementDSHandler;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentCount4SplitElement;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager.PARAM_SUMMARY;

/**
 * @author Denis Katkov
 * @since 23.03.2016
 */
@Configuration
public class EplStudentSummaryGroupSplitTab extends BusinessComponentManager
{
    public static final String EPL_STUDENT_GROUP_SPLIT_DS = "eplStudentGroupSplitDS";
    public static final String DISCIPLINE = "discipline";
    public static final String ENABLING = "enabling";
    public static final String GROUP_SPLIT_ELEMENT = "groupSplitElement";
    public static final String GROUP_SPLIT_ELEMENT_COUNT = "groupSplitElementCount";
    public static final String DISCIPLINE_DS = "disciplineDS";
    public static final String CHECKBOX = "checkbox";
    public static final String INCORRECT_SPLIT = "incorrectSplit";
    public static final String NO_SPLIT = "noSplit";
    public static final String SPLIT_ELEMENTS_ID = "splitElementsId";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EPL_STUDENT_GROUP_SPLIT_DS, groupSplitDSColumns(), groupSplitDSHandler()))
                .addDataSource(selectDS(DISCIPLINE_DS, disciplineDSHandler()).addColumn(EppRegistryElement.P_EDUCATION_ELEMENT_TITLE))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(EplStudentSummaryManager.SPLIT_VARIANT_DS, getName(), EppEduGroupSplitVariant.selectForEppRegistryElementDSHandler(getName())
                        .customize((alias, dql, context, filter) ->
                                dql.where(exists(EplStudentSummaryManager.instance().dao().getWorkPlanAndRegElementUniquePairsDql("wpRow", context.get(PARAM_SUMMARY))
                                        .where(eq(property(alias, "id"), property("wpRow", EppWorkPlanRegistryElementRow.registryElementPart().registryElement().eduGroupSplitVariant().id())))
                                        .buildQuery()))
                )))
                .addDataSource(selectDS(EplStudentSummaryManager.SPLIT_ELEMENT_DS, splitElementDSHandler()).addColumn(EppEduGroupSplitBaseElement.P_SPLIT_ELEMENT_TITLE))
                .create();
    }

    @Bean
    public ColumnListExtPoint groupSplitDSColumns()
    {
        return columnListExtPointBuilder(EPL_STUDENT_GROUP_SPLIT_DS)
                .addColumn(checkboxColumn(CHECKBOX))
                .addColumn(textColumn(EplStudent2WorkPlan.student().group().course().s(), EplStudent2WorkPlan.student().group().course().title()).order())
                .addColumn(textColumn(EplStudent2WorkPlan.student().group().s(), EplStudent2WorkPlan.student().group().title()).order())
                .addColumn(textColumn(EplStudent2WorkPlan.student().educationOrgUnit().formativeOrgUnit().s(), EplStudent2WorkPlan.student().educationOrgUnit().formativeOrgUnit().shortTitle()).order())
                .addColumn(textColumn(EplStudent2WorkPlan.student().educationOrgUnit().territorialOrgUnit().s(), EplStudent2WorkPlan.student().educationOrgUnit().territorialOrgUnit().territorialShortTitle()).order())
                .addColumn(textColumn(EplStudent2WorkPlan.student().educationOrgUnit().educationLevelHighSchool().s(), EplStudent2WorkPlan.student().educationOrgUnit().educationLevelHighSchool().fullTitle()).order())
                .addColumn(textColumn(EplStudent2WorkPlan.student().compensationSource().s(), EplStudent2WorkPlan.student().compensationSource().title()).order())
                .addColumn(textColumn(DISCIPLINE, DISCIPLINE).formatter(o -> ((EppRegistryElement) o).getEducationElementTitle()).order())
                .addColumn(textColumn(EppRegistryElement.eduGroupSplitVariant().s(), DISCIPLINE).formatter(o -> ((EppRegistryElement) o).getTitleWithSplitVariant()).order())
                .addColumn(textColumn(EplStudent2WorkPlan.workPlan().s(), EplStudent2WorkPlan.workPlan().title()))
                .addColumn(textColumn(EplStudent2WorkPlan.count().s(), EplStudent2WorkPlan.count()).order())
                .addColumn(textColumn(GROUP_SPLIT_ELEMENT, GROUP_SPLIT_ELEMENT).formatter(RowCollectionFormatter.INSTANCE)
                        .styleResolver(iEntity -> {
                            if (CollectionUtils.isEmpty((List) iEntity.getProperty(GROUP_SPLIT_ELEMENT))) {
                                return "";
                            }
                            return !(iEntity.getProperty(GROUP_SPLIT_ELEMENT_COUNT).equals(((EplStudent2WorkPlan) ((DataWrapper) iEntity).getWrapped()).getCount())) || !(boolean) iEntity.getProperty(ENABLING) ? "color: #DB0000" : "";
                        }))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEdit").disabled("ui:editSplitFormEnable").permissionKey("eplStudentSummaryGroupSplitTabEdit"))
                .addColumn(actionColumn("deleteSplitElements", new Icon("cancel_red"), "onClickDeleteSplitElements")
                        .permissionKey("eplStudentSummaryGroupSplitTabDelete")
                        .disabled("ui:editSplitFormEnable")
                        .alert(FormattedMessage.with().template(EPL_STUDENT_GROUP_SPLIT_DS + ".deleteAlert").parameter(DISCIPLINE, o1 -> ((EppRegistryElement) o1).getEducationElementTitle()).create()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler disciplineDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppRegistryElement.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                dql.where(exists(
                        EplStudentSummaryManager.instance().dao().getWorkPlanAndRegElementUniquePairsDql("wpRow", context.get(PARAM_SUMMARY))
                                .where(eq(property(alias, EppRegistryElement.id()), property("wpRow", EppWorkPlanRegistryElementRow.registryElementPart().registryElement())))
                                .buildQuery()
                ));
            }
        }
                .order(EppRegistryElement.title())
                .order(EppRegistryElement.number())
                .filter(EppRegistryElement.title())
                .filter(EppRegistryElement.shortTitle())
                .filter(EppRegistryElement.fullTitle())
                .filter(EppRegistryElement.number());
    }

    @Bean
    public IDefaultSearchDataSourceHandler groupSplitDSHandler()
    {
        return new EplStudentSummaryGroupSplitDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler splitElementDSHandler()
    {
        return new EplStudentSummarySplitElementDSHandler(getName()){
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                String alias = "s";
                DQLSelectBuilder builder = getBuilder(alias, context);

                Set keys = input.getPrimaryKeys();
                if (CollectionUtils.isNotEmpty(keys)) {
                    builder.where(in(property(alias, "id"), keys));
                } else {
                    builder.where(exists(EplStudentCount4SplitElement.class, EplStudentCount4SplitElement.splitElement().id().s(), property(alias, "id")));
                }

                String filter = input.getComboFilterByValue();
                List<EppEduGroupSplitBaseElement> resultList = builder.createStatement(context.getSession()).<EppEduGroupSplitBaseElement>list().stream()
                        .filter(entity -> StringUtils.isEmpty(filter) || entity.getSplitElementTitle().toLowerCase().contains(filter.toLowerCase()))
                        .sorted((o1, o2) -> o1.getSplitElementShortTitle().compareTo(o2.getSplitElementShortTitle()))
                        .collect(Collectors.toList());

                return ListOutputBuilder.get(input, resultList).pageable(false).build();
            }
        };
    }
}