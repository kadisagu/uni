package ru.tandemservice.uniepp_load.util;

/**
 * Поддерживает подсчет производительности
 *
 * @author Alexey Lopatin
 * @since 28.03.2016
 */
public interface IScriptPerformanceCounter
{
    /**
     * @return Если счетчик производительности включен - true, иначе - false
     */
    boolean isCounterEnabled();

    /**
     * @return Суммарное время работы (мкс)
     */
    long getTotalTimeWork();

    /**
     * @return Число вызовов
     */
    int getRequestCount();
}
