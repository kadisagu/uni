/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper;

import static ru.tandemservice.uniepp_load.catalog.entity.codes.EplTimeItemVariantGroupingCodes.*;

/**
 * Родительский враппер, необходим для построения селектов в виде дерева
 *
 * @author Alexey Lopatin
 * @since 27.04.2016
 */
public class EplCommonParentWrapper extends EplCommonRowWrapper
{
    private String _groupingCode;

    public EplCommonParentWrapper(long id, String groupingCode)
    {
        super(id, null, null, null);
        _groupingCode = groupingCode;
    }

    public String getFieldId()
    {
        return "parent_" + getId();
    }

    @Override
    public String getTitle()
    {
        switch (getGroupingCode())
        {
            case BY_REG_ELEMENT:
            case BY_REG_ELEMENT_AND_TIME_RULE: return "Мероприятие реестра";
            case BY_TIME_RULE: return "Норма времени";
            case EplSimpleRuleRowWrapper.BY_SIMPLE_TIME_RULE: return "Простая норма времени";
            default: throw new IllegalStateException("Unknown grouping for time item.");
        }
    }

    public String getGroupingCode()
    {
        return _groupingCode;
    }
}
