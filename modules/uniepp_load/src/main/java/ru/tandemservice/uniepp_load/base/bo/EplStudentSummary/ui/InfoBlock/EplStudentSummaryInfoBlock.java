/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.InfoBlock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Alexey Lopatin
 * @since 12.07.2016
 */
@Configuration
public class EplStudentSummaryInfoBlock extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}
