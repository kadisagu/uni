package ru.tandemservice.uniepp_load.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вариант группировки рассчитанных часов"
 * Имя сущности : eplTimeItemVariantGrouping
 * Файл data.xml : uniepp_load.catalog.data.xml
 */
public interface EplTimeItemVariantGroupingCodes
{
    /** Константа кода (code) элемента : По мероприятию реестра (title) */
    String BY_REG_ELEMENT = "byRegElement";
    /** Константа кода (code) элемента : По норме времени (title) */
    String BY_TIME_RULE = "byTimeRule";
    /** Константа кода (code) элемента : По мероприятию реестра и норме времени (title) */
    String BY_REG_ELEMENT_AND_TIME_RULE = "byRegElementAndTimeRule";

    Set<String> CODES = ImmutableSet.of(BY_REG_ELEMENT, BY_TIME_RULE, BY_REG_ELEMENT_AND_TIME_RULE);
}
