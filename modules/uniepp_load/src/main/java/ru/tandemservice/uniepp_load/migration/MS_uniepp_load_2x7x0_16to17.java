package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_load_2x7x0_16to17 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eplStudent

		// создано обязательное свойство noWp
		{
            if(!tool.columnExists("epl_student_t", "nowp_p"))
            {
                // создать колонку
                tool.createColumn("epl_student_t", new DBColumn("nowp_p", DBType.BOOLEAN));
                // задать значение по умолчанию
                tool.executeUpdate("update epl_student_t set nowp_p=? where nowp_p is null", (Boolean) false);
                // сделать колонку NOT NULL
                tool.setColumnNullable("epl_student_t", "nowp_p", false);
            }
		}
    }
}