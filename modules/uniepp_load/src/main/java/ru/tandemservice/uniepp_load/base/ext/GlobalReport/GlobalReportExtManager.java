/* $Id:$ */
package ru.tandemservice.uniepp_load.base.ext.GlobalReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.ui.List.EplReportBaseList;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.ui.List.EplReportBaseListUI;
import ru.tandemservice.uniepp_load.report.entity.*;

/**
 * @author oleyba
 * @since 12/11/13
 */
@Configuration
public class GlobalReportExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private GlobalReportManager _globalReportManager;

    @Bean
    public ItemListExtension<GlobalReportDefinition> reportListExtension()
    {
        return itemListExtension(_globalReportManager.reportListExtPoint())
            .add(EplReportOrgUnitEduLoadA.DESC.getReportKey(), def(EplReportOrgUnitEduLoadA.DESC.getReportKey()))
            .add(EplReportOrgUnitEduLoadB.DESC.getReportKey(), def(EplReportOrgUnitEduLoadB.DESC.getReportKey()))
            .add(EplReportHourlyFund.DESC.getReportKey(), def(EplReportHourlyFund.DESC.getReportKey()))
            .add(EplReportStaffByLoadB.DESC.getReportKey(), def(EplReportStaffByLoadB.DESC.getReportKey()))
            .add(EplReportOrgUnitEduLoadByPpsCheck.DESC.getReportKey(), def(EplReportOrgUnitEduLoadByPpsCheck.DESC.getReportKey()))
            .add(EplReportOrgUnitEduAssignment.DESC.getReportKey(), def(EplReportOrgUnitEduAssignment.DESC.getReportKey()))
            .create();
    }

    private static GlobalReportDefinition def(String reportKey)
    {
        return new GlobalReportDefinition("uniepp_load", reportKey, EplReportBaseList.class.getSimpleName(),
            new ParametersMap().add(EplReportBaseListUI.BIND_REPORT_KEY, reportKey));
    }
}
