/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EditTitle;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;

/**
 * @author oleyba
 * @since 11/10/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id")
})
public class EplStudentSummaryEditTitleUI extends UIPresenter
{
    private EntityHolder<EplStudentSummary> holder = new EntityHolder<EplStudentSummary>();

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
    }

    public void onClickApply() {
        IUniBaseDao.instance.get().saveOrUpdate(getSummary());
        deactivate();
    }

    // getters and setters

    public EntityHolder<EplStudentSummary> getHolder()
    {
        return holder;
    }

    public EplStudentSummary getSummary()
    {
        return getHolder().getValue();
    }
}