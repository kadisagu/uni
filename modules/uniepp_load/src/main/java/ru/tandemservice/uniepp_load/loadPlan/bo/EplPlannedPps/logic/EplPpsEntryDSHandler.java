/* $Id$ */
package ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.logic;

import com.beust.jcommander.internal.Maps;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import ru.tandemservice.uni.base.bo.PpsEntry.logic.BasePpsEntryDSHandler;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.tandemframework.hibsupport.dao.CommonDAO.ids;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 20.06.2016
 */
public class EplPpsEntryDSHandler extends BasePpsEntryDSHandler
{
    public static final String PARAM_ONLY_OU_PPS = "onlyOuPps";
    public static final String PARAM_TIME_WORKER = "timeWorker";
    public static final String PARAM_ORG_UNIT_ID = "orgUnitId";
    public static final String PARAM_PERSON_ID = "personId";
    public static final String PARAM_EDU_YEAR = "eduYear";
    public static final String PARAM_POST = "post";

    public EplPpsEntryDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
    {
        super.applyWhereConditions(alias, dql, context);

        boolean timeWorker = context.getBoolean(PARAM_TIME_WORKER, false);
        Post post = context.get(PARAM_POST);

        FilterUtils.applySelectFilter(dql, alias, PpsEntry.person().id(), context.get(PARAM_PERSON_ID));
        dql.where(isNull(property(alias, PpsEntry.removalDate())));

        dql.joinEntity(alias, DQLJoinType.left, PpsEntryByEmployeePost.class, "pps", eq(property(alias, "id"), property("pps.id")));
        dql.joinEntity("pps", DQLJoinType.left, EmployeePost4PpsEntry.class, "ep4pe", eq(property("pps.id"), property("ep4pe", EmployeePost4PpsEntry.ppsEntry().id())));
        dql.joinPath(DQLJoinType.left, EmployeePost4PpsEntry.employeePost().fromAlias("ep4pe"), "ep");
        dql.joinPath(DQLJoinType.left, EmployeePost.employee().fromAlias("ep"), "emp");
        dql.joinPath(DQLJoinType.left, EmployeePost.postStatus().fromAlias("ep"), "ps");
        dql.where(or(isNull("pps"), and(
                eq(property("emp", Employee.archival()), value(Boolean.FALSE)),
                eq(property("ps", EmployeePostStatus.active()), value(Boolean.TRUE))
        )));

        if (!timeWorker && null != post)
        {
            boolean onlyOuPps = context.getBoolean(PARAM_ONLY_OU_PPS, false);

            dql.where(eq(property("ep", EmployeePost.postRelation().postBoundedWithQGandQL().post()), value(post)));
            if (onlyOuPps)
            {
                Long orgUnitId = context.get(PARAM_ORG_UNIT_ID);
                if (null == orgUnitId) dql.where(nothing());
                else
                    dql.where(eq(property("pps", PpsEntryByEmployeePost.orgUnit().id()), value(orgUnitId)));
            }
        }
    }

    @Override
    protected DSOutput execute(final DSInput input, final ExecutionContext context)
    {
        Set keys = input.getPrimaryKeys();
        if (CollectionUtils.isNotEmpty(keys))
        {
            String filter = input.getComboFilterByValue();
            String alias = getOrderRegistry().getRootAlias();
            DQLSelectBuilder builder = query(alias, StringUtils.trimToNull(filter));
            applyWhereConditions(alias, builder, context);

            List<PpsEntry> baseEntries = builder.column(property(alias)).createStatement(context.getSession()).list();
            List<PpsEntry> resultList = getPpsResultList(baseEntries, context);
            if (resultList.isEmpty())
            {
                keys.clear();
            }
        }
        return super.execute(input, context);
    }

    @Override
    protected void execute(DQLSelectBuilder builder, String alias, DSOutput output, ExecutionContext context)
    {
        List<PpsEntry> baseEntries = builder.column(property(alias)).createStatement(context.getSession()).list();

        // hardcode: сделано так, поскольку у unisnpps не подключен uniepp_load
        List<PpsEntry> resultList = getPpsResultList(baseEntries, context);

        int totalSize = resultList.size();
        output.setTotalSize(totalSize);
        output.addRecords(totalSize > output.getCountRecord() ? resultList.subList(0, output.getCountRecord()) : resultList);
    }

    private List<PpsEntry> getPpsResultList(List<PpsEntry> baseEntries, ExecutionContext context)
    {
        List<PpsEntry> resultList = Lists.newArrayList();
        EducationYear summaryEduYear = context.get(PARAM_EDU_YEAR);
        if (null == summaryEduYear) return resultList;

        Map<Long, List<EmployeePost>> employeePostMap = Maps.newHashMap();
        DataAccessServices.dao().getList(EmployeePost4PpsEntry.class, EmployeePost4PpsEntry.ppsEntry().id(), ids(baseEntries))
                .forEach(item -> SafeMap.safeGet(employeePostMap, item.getPpsEntry().getId(), ArrayList.class).add(item.getEmployeePost()));

        for (PpsEntry ppsEntry : baseEntries)
        {
            if (ppsEntry instanceof PpsEntryByEmployeePost)
            {
                List<EmployeePost> employeePosts = employeePostMap.getOrDefault(ppsEntry.getId(), Lists.newArrayList());
                for (EmployeePost ep : employeePosts)
                {
                    if (!ep.getEmployee().isArchival() && ep.getPostStatus().isActive() && isAllOrWithOrgUnit(ppsEntry, context))
                    {
                        resultList.add(ppsEntry);
                        break;
                    }
                }
            }
            else if ((null == ppsEntry.getEduYear() || ppsEntry.getEduYear().equals(summaryEduYear)) && isAllOrWithOrgUnit(ppsEntry, context))
            {
                resultList.add(ppsEntry);
            }
        }
        return resultList;
    }

    /**
     * Если не указан параметр выбора из ППС данного подразделения, то берутся все преподаватели. Иначе только те, у кого совпадает подразделение.
     */
    private boolean isAllOrWithOrgUnit(PpsEntry ppsEntry, ExecutionContext context)
    {
        boolean onlyOuPps = context.getBoolean(PARAM_ONLY_OU_PPS, false);
        Long orgUnitId = context.get(PARAM_ORG_UNIT_ID);

        return !onlyOuPps || orgUnitId == null || orgUnitId.equals(ppsEntry.getOrgUnit().getId());
    }
}
