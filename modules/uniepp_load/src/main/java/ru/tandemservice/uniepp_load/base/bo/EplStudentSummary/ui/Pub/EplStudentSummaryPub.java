/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.EduGroupTab.EplStudentSummaryEduGroupTab;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupSplitTab.EplStudentSummaryGroupSplitTab;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.EplStudentSummaryGroupTab;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.LogTab.EplStudentSummaryLogTab;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.MaxLoadTab.EplStudentSummaryMaxLoadTab;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.OuSummaryTab.EplStudentSummaryOuSummaryTab;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.SlotTab.EplStudentSummarySlotTab;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.WorkplanTab.EplStudentSummaryWorkplanTab;

/**
 * @author oleyba
 * @since 10/11/11
 */
@Configuration
public class EplStudentSummaryPub extends BusinessComponentManager
{
    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder("tabPanel")
                .addTab(componentTab("groupTab", EplStudentSummaryGroupTab.class).permissionKey("eplStudentSummaryPubGroupTabView"))
                .addTab(componentTab("wpTab", EplStudentSummaryWorkplanTab.class).permissionKey("eplStudentSummaryPubWpTabView"))
                .addTab(componentTab("groupSplitTab", EplStudentSummaryGroupSplitTab.class).visible("ui:groupSplitTabVisible").permissionKey("eplStudentSummaryGroupSplitTabView"))
                .addTab(componentTab("slotTab", EplStudentSummarySlotTab.class).permissionKey("eplStudentSummaryPubSlotTabView"))
                .addTab(componentTab("eduGroupTab", EplStudentSummaryEduGroupTab.class).permissionKey("eplStudentSummaryPubEduGroupTabView"))
                .addTab(componentTab("ouSummaryTab", EplStudentSummaryOuSummaryTab.class).permissionKey("eplStudentSummaryPubOuSummaryTabView"))
                .addTab(componentTab("maxLoadTab", EplStudentSummaryMaxLoadTab.class).permissionKey("eplStudentSummaryMaxLoadTabView"))
                .addTab(componentTab("logTab", EplStudentSummaryLogTab.class).permissionKey("eplStudentSummaryLogTabView"))
                .create();
    }
}
