package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.catalog.CCheckConstraint;
import org.tandemframework.dbsupport.ddl.catalog.CForeignKey;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_load_2x9x0_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		for (CCheckConstraint ck : tool.table("epl_edu_group_t").checkConstraints()) {
			ck.drop();
		}
		for (CForeignKey fk : tool.table("epl_edu_group_t").foreignKeys()) {
			fk.drop();
		}

		tool.createColumn("epl_edu_group_t", new DBColumn("yearpart_id", DBType.LONG));
		tool.executeUpdate("update epl_edu_group_t set yearpart_id = (select distinct(s.yearpart_id) from epl_edu_group_summary_t s where epl_edu_group_t.summary_id = s.id)");
		tool.setColumnNullable("epl_edu_group_t", "yearpart_id", false);

		tool.executeUpdate("update epl_edu_group_t set summary_id = (select distinct(s.summary_id) from epl_edu_group_summary_t s where epl_edu_group_t.summary_id = s.id)");

		tool.dropTable("epl_edu_group_summary_t", false /* - не удалять, если есть ссылающиеся таблицы */);
		tool.entityCodes().delete("eplEduGroupSummary");

		tool.createColumn("epl_student2workplan_t", new DBColumn("cachedgridterm_id", DBType.LONG));
		tool.executeUpdate("update epl_student2workplan_t set cachedgridterm_id = (" +
			" select distinct(w.cachedGridTerm_id) from " +
			"  epp_workplan_version_t v" +
			"  inner join epp_workplan_t w on v.parent_id = w.id" +
			" where v.id = epl_student2workplan_t.workPlan_id)");
		tool.executeUpdate("update epl_student2workplan_t " +
			" set cachedgridterm_id = (select distinct(w.cachedGridTerm_id) from epp_workplan_t w where w.id = epl_student2workplan_t.workPlan_id) " +
			" where exists (select w.id from epp_workplan_t w where w.id = epl_student2workplan_t.workPlan_id)");
		tool.setColumnNullable("epl_student2workplan_t", "cachedgridterm_id", false);

		tool.createColumn("epl_student_wp_slot_t", new DBColumn("cachedgridterm_id", DBType.LONG));
		tool.executeUpdate("update epl_student_wp_slot_t set cachedgridterm_id=(" +
			"select distinct(t.id) from " +
			"  developgridterm_t t, " +
			"  epp_year_part_t eyp, " +
			"  epl_student_t s " +
			"  inner join epl_group_t g on g.id = s.group_id" +
			" where" +
			"  t.part_id = eyp.part_id and" +
			"  t.course_id = g.course_id and" +
			"  t.developGrid_id = s.developGrid_id and" +
			"  epl_student_wp_slot_t.cachedeppyearpart_id = eyp.id and" +
			"  epl_student_wp_slot_t.student_id = s.id" +
			")");
		tool.setColumnNullable("epl_student_wp_slot_t", "cachedgridterm_id", false);


		tool.dropColumn("epl_student2workplan_t", "cachedyearpart_id");
		tool.dropColumn("epl_student_wp_slot_t", "cachedeppyearpart_id");
    }
}