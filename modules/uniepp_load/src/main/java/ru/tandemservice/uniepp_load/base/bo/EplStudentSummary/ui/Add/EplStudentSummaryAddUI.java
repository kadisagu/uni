/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GroupTab.EplStudentSummaryGroupTabUI;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Pub.EplStudentSummaryPub;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplStudentSummaryState;
import ru.tandemservice.uniepp_load.catalog.entity.codes.EplStudentSummaryStateCodes;

import java.util.Date;

/**
 * @author oleyba
 * @since 12/3/14
 */
public class EplStudentSummaryAddUI extends UIPresenter
{
    public static final String PARAM_EDU_YEAR = "eduYear";

    private EducationYear _eduYear;
    private EppYearEducationProcess _eppYear;
    private String _title;

    private boolean _fillSummary = true;
    private boolean _fillCopyFirstCourse;
    private boolean _fillEduGroups;

    @Override
    public void onComponentRefresh()
    {
        _eduYear = DataAccessServices.dao().getNotNull(EducationYear.class, EducationYear.current(), true);
        _eppYear = EplStudentSummaryManager.instance().dao().getNextAfterCurrentYearEducationProcess(_eduYear);
        onChangeEppYear();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EducationYear.PARAM_EDU_YEAR_ONLY_CURRENT, true);
        dataSource.put(PARAM_EDU_YEAR, getEduYear());
    }

    public void onChangeEppYear()
    {
        if (null != _eppYear)
            _title = _eppYear.getTitle();
    }

    public void onClickApply()
    {
        EplStudentSummaryState state = DataAccessServices.dao().getByCode(EplStudentSummaryState.class, EplStudentSummaryStateCodes.CONTINGENT_PLANNING);

        EplStudentSummary summary = new EplStudentSummary(state);
        summary.setCreationDate(new Date());
        summary.setEppYear(getEppYear());
        summary.setTitle(getTitle());
        DataAccessServices.dao().save(summary);

        Integer missEplStudents = null;
        if (_fillSummary)
            missEplStudents = EplStudentSummaryManager.instance().dao().fillSummary(summary, _fillCopyFirstCourse, _fillEduGroups);

        _uiActivation.asDesktopRoot(EplStudentSummaryPub.class)
                .parameter(PUBLISHER_ID, summary.getId())
                .parameter("selectedTab", "groupTab")
                .parameter(EplStudentSummaryGroupTabUI.PARAM_MISS_EPL_STUDENTS, missEplStudents)
                .activate();

        deactivate();
    }

    public boolean isYearsEquals()
    {
        return null != _eppYear && _eppYear.getEducationYear().equals(_eduYear);
    }

    // getters and setters

    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    public void setEduYear(EducationYear eduYear)
    {
        _eduYear = eduYear;
    }

    public EppYearEducationProcess getEppYear()
    {
        return _eppYear;
    }

    public void setEppYear(EppYearEducationProcess eppYear)
    {
        _eppYear = eppYear;
    }

    public boolean isFillSummary()
    {
        return _fillSummary;
    }

    public void setFillSummary(boolean fillSummary)
    {
        _fillSummary = fillSummary;
    }

    public boolean isFillCopyFirstCourse()
    {
        return _fillCopyFirstCourse;
    }

    public void setFillCopyFirstCourse(boolean fillCopyFirstCourse)
    {
        _fillCopyFirstCourse = fillCopyFirstCourse;
    }

    public boolean isFillEduGroups()
    {
        return _fillEduGroups;
    }

    public void setFillEduGroups(boolean fillEduGroups)
    {
        _fillEduGroups = fillEduGroups;
    }

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }
}