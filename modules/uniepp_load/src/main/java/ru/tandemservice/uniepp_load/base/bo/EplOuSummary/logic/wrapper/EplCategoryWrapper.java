/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;

import java.util.List;
import java.util.Map;

/**
 * Враппер категорий
 *
 * @author Alexey Lopatin
 * @since 04.04.2016
 */
public class EplCategoryWrapper extends IdentifiableWrapper<EplTimeCategoryEduLoad>
{
    private int level;
    private List<EplCategoryWrapper> children = Lists.newArrayList();

    public EplTimeCategoryEduLoad category;
    public Map<String, Double> _discTimeSumMap = Maps.newHashMap();
    public double ruleTimeSum;

    public EplCategoryWrapper(EplTimeCategoryEduLoad i) throws ClassCastException
    {
        super(i.getId(), i.getShortTitle());
        category = i;
    }

    public int getColumnCount()
    {
        return Math.max(1, getChildCount());
    }

    public int getRowCount(int categoryLevelCount)
    {
        if (getChildCount() == 0)
            return categoryLevelCount - getLevel();
        else
            return 1;
    }

    public EplTimeCategoryEduLoad getCategory() { return category; }
    public int getLevel() { return level; }
    public void setLevel(int level) { this.level = level; }
    public int getChildCount() { return getChildren().size(); }
    public List<EplCategoryWrapper> getChildren() { return children; }

    public Map<String, Double> getDiscTimeSumMap()
    {
        return _discTimeSumMap;
    }

    public void setDiscTimeSumMap(Map<String, Double> discTimeSumMap)
    {
        _discTimeSumMap = discTimeSumMap;
    }

    public String getRuleTimeSum() { return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(ruleTimeSum); }
    public String getDisciplineTimeSum(List<String> groupingCodes)
    {
        double result = .0;
        for (String code : groupingCodes)
            result += _discTimeSumMap.getOrDefault(code, 0.0);
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(result);
    }

    public String getTimeSum()
    {
        Double totalSum = _discTimeSumMap.values().stream().mapToDouble(Double::doubleValue).sum();
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(totalSum + ruleTimeSum);
    }
}
