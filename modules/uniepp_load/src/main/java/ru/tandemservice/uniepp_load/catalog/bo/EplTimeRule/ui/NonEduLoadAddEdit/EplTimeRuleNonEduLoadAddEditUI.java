/* $Id$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.ui.NonEduLoadAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemAddEditUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleNonEduLoad;

/**
 * @author Alexey Lopatin
 * @since 22.07.2016
 */
@Input({
        @Bind(key = "catalogItemId", binding = "holder.id"),
        @Bind(key = DefaultCatalogAddEditModel.CATALOG_CODE, binding = "catalogCode")
})
public class EplTimeRuleNonEduLoadAddEditUI extends BaseCatalogItemAddEditUI<EplTimeRuleNonEduLoad>
{
    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        getProperties().remove(EplTimeRuleNonEduLoad.P_DESCRIPTION);
        getProperties().remove(EplTimeRuleNonEduLoad.P_PARAMETER_NAME);
    }

    @Override
    public String getAdditionalPropertiesPageName()
    {
        return getClass().getPackage().getName() + ".AdditProps";
    }

    @Override
    public void onClickApply()
    {
        if (!getCatalogItem().isParameterNeeded())
        {
            getCatalogItem().setParameterName(null);
        }
        super.onClickApply();
    }
}
