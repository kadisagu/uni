/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper;

import com.google.common.collect.Maps;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.ArrayUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad;

import java.util.Map;

import static ru.tandemservice.uniepp_load.catalog.entity.codes.EplTimeItemVariantGroupingCodes.*;

/**
 * Базовый враппер строки
 *
 * @author Alexey Lopatin
 * @since 04.04.2016
 */
public abstract class EplCommonRowWrapper extends DataWrapper
{
    public static class ControlTime
    {
        public double controlTime;
        public double time;
        public boolean hasDifference;
    }

    private DisciplineOrRulePairKey _key;
    private EppRegistryElementPart _discipline;
    private EplTimeRuleEduLoad _rule;

    public double _timeSum;
    public double _transferTimeSum;

    // <<categoryId, true-fullCategory/false-transferCategory>, timeValue>
    private Map<PairKey<Long, Boolean>, ControlTime> _timeMap = Maps.newHashMap();

    protected EplCommonRowWrapper(Long id, EppRegistryElementPart discipline, EplTimeRuleEduLoad rule, DisciplineOrRulePairKey key)
    {
        _id = id;
        _discipline = discipline;
        _rule = rule;
        _key = key;
    }

    public DisciplineOrRulePairKey getKey() { return _key; }
    public EppRegistryElementPart getDiscipline() { return _discipline; }
    public EplTimeRuleEduLoad getRule() { return _rule; }

    public String getTimeSum() { return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(_timeSum); }
    public String getTransferTimeSum() { return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(_transferTimeSum); }
    public String getTotalTimeSum() { return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(_timeSum - _transferTimeSum); }

    public Map<PairKey<Long, Boolean>, ControlTime> getTimeMap() { return _timeMap; }

    public String getFieldId()
    {
        return getKey().getDisciplineId() + "_" + getKey().getRuleId();
    }

    public static DisciplineOrRulePairKey getKey(EppRegistryElementPart discipline, EplTimeRuleEduLoad rule, String groupingCode)
    {
        return getKey(discipline, rule, groupingCode, null);
    }

    public static DisciplineOrRulePairKey getKey(EppRegistryElementPart discipline, EplTimeRuleEduLoad rule, String groupingCode, Object... groupKeys)
    {
        Object[] keys;
        String disciplineTitle = null;
        String ruleTitle = null;

        switch (groupingCode)
        {
            case BY_REG_ELEMENT:
                keys = new Object[]{discipline.getId(), null};
                disciplineTitle = discipline.getTitleWithNumber();
                break;
            case BY_REG_ELEMENT_AND_TIME_RULE:
                keys = new Object[]{discipline.getId(), rule.getId()};
                disciplineTitle = discipline.getTitleWithNumber();
                ruleTitle = rule.getTitle();
                break;
            case BY_TIME_RULE:
                keys = new Object[]{null, rule.getId()};
                ruleTitle = rule.getTitle();
                break;
            case EplSimpleRuleRowWrapper.BY_SIMPLE_TIME_RULE:
                keys = new Object[]{null, rule.getId()};
                ruleTitle = rule.getTitle();
                break;
            default: throw new IllegalStateException("Unknown grouping for time item.");
        }

        keys = groupKeys == null ? keys : ArrayUtils.addAll(keys, groupKeys);
        return new DisciplineOrRulePairKey(keys, groupingCode, disciplineTitle, ruleTitle, false);
    }

    public static String getTitle(EppRegistryElementPart discipline, EplTimeRuleEduLoad rule, String groupingCode)
    {
        switch (groupingCode)
        {
            case BY_REG_ELEMENT: return discipline.getTitleWithNumber();
            case BY_REG_ELEMENT_AND_TIME_RULE: return rule.getTitle() + " (" + discipline.getShortTitle() + ")";
            case BY_TIME_RULE: return rule.getTitle();
            case EplSimpleRuleRowWrapper.BY_SIMPLE_TIME_RULE: return rule.getTitle();
            default: throw new IllegalStateException("Unknown grouping for time item.");
        }
    }

    public static class DisciplineOrRulePairKey extends MultiKey
    {
        private String _groupingCode;
        private int _groupPriority;
        private String _disciplineTitle;
        private String _ruleTitle;

        public DisciplineOrRulePairKey(Object[] keys, String groupingCode, String disciplineTitle, String ruleTitle, boolean makeClone)
        {
            super(keys, makeClone);

            _groupingCode = groupingCode;
            _disciplineTitle = disciplineTitle;
            _ruleTitle = ruleTitle;

            switch (groupingCode)
            {
                case BY_REG_ELEMENT:
                case BY_REG_ELEMENT_AND_TIME_RULE: _groupPriority = 0; break;
                case BY_TIME_RULE: _groupPriority = 1; break;
                case EplSimpleRuleRowWrapper.BY_SIMPLE_TIME_RULE: _groupPriority = 2; break;
            }
        }

        public Long getDisciplineId(){ return (Long) getKey(0); }
        public Long getRuleId() { return (Long) getKey(1); }

        public int getGroupPriority() { return _groupPriority; }
        public String getGroupingCode(){ return _groupingCode; }
        public String getDisciplineTitle() { return _disciplineTitle; }
        public String getRuleTitle() { return _ruleTitle; }
    }
}
