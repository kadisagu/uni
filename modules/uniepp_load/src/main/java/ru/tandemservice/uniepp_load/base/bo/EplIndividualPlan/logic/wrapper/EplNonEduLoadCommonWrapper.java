/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.logic.wrapper;

import com.google.common.collect.Maps;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.commonbase.utils.DQLSimple;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryNonEduLoad;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Alexey Lopatin
 * @since 09.09.2016
 */
public class EplNonEduLoadCommonWrapper
{
    private EplIndividualPlan _indPlan;
    private List<EplNonEduLoadRowWrapper> _rows;

    public EplNonEduLoadCommonWrapper(EplIndividualPlan indPlan)
    {
        _indPlan = indPlan;
        init();
    }

    private void init()
    {
        Map<EplTimeCategoryNonEduLoad, List<EplTimeItemNonEduLoad>> category2TimeItemMap = Maps.newLinkedHashMap();
        new DQLSimple<>(EplTimeItemNonEduLoad.class)
                .where(EplTimeItemNonEduLoad.indPlan(), _indPlan)
                .list().stream()
                .sorted(TIME_ITEM_NON_EDU_LOAD_COMPARATOR)
                .forEach(item -> SafeMap.safeGet(category2TimeItemMap, item.getTimeRule().getCategory(), ArrayList.class).add(item));

        _rows = category2TimeItemMap.entrySet().stream()
                .map(entry -> {
                    EplTimeCategoryNonEduLoad category = entry.getKey();
                    EplNonEduLoadRowWrapper row = new EplNonEduLoadRowWrapper(category.getTitle(), true, false);
                    row.setId(category.getId());

                    int index = 0;
                    for (EplTimeItemNonEduLoad item : entry.getValue())
                    {
                        EplNonEduLoadRowWrapper child = new EplNonEduLoadRowWrapper(item.getTimeRule().getTitle(), false, false);
                        child.setId(item.getId());
                        child.setNumber(++index);
                        child.setParamName(item.getTimeRule().getParameterName());
                        child.setParamValue(item.getParameter());
                        child.setTimeAmountAsLong(item.getTimeAmountAsLong());
                        child.setDateFrom(item.getDateFrom());
                        child.setDateTo(item.getDateTo());

                        if (null != item.getYearPart())
                        {
                            child.setYearPart(item.getYearPart().getTitle());
                        }

                        row.getChildren().add(child);
                    }
                    long sum = row.getChildren().stream().mapToLong(EplNonEduLoadRowWrapper::getTimeAmountAsLong).sum();
                    row.setTimeAmountAsLong(sum);

                    return row;
                })
                .collect(Collectors.toList());

        long totalSum = _rows.stream()
                .flatMap(item -> item.getChildren().stream())
                .mapToLong(EplNonEduLoadRowWrapper::getTimeAmountAsLong)
                .sum();

        EplNonEduLoadRowWrapper totalWrapper = new EplNonEduLoadRowWrapper("Всего", false, true);
        totalWrapper.setTimeAmountAsLong(totalSum);
        _rows.add(totalWrapper);
    }

    public static class TitleColumn extends IdentifiableWrapper
    {
        public TitleColumn(Long id, String title) { super(id, title); }

        public TitleColumnType getType() {
            return TitleColumnType.values()[(int) (long) getId()];
        }

        public static List<TitleColumn> getList() {
            long i = -1;
            List<TitleColumn> columns = Arrays.asList(
                    new TitleColumn(++i, "№"),
                    new TitleColumn(++i, "Название"),
                    new TitleColumn(++i, "Параметр"),
                    new TitleColumn(++i, "Значение"),
                    new TitleColumn(++i, "Число часов"),
                    new TitleColumn(++i, "Дата с"),
                    new TitleColumn(++i, "Дата по"),
                    new TitleColumn(++i, "Часть года")
            );
            return columns.stream().filter(item -> item != null).collect(Collectors.toList());
        }
    }

    public enum TitleColumnType
    {
        NUMBER, TITLE, TIME_AMOUNT, DATE_FROM, DATE_TO, YEAR_PART, PARAM_NAME, PARAM_VALUE
    }

    private Comparator<EplTimeItemNonEduLoad> TIME_ITEM_NON_EDU_LOAD_COMPARATOR = (t1, t2) -> {
        int result = Integer.compare(t1.getTimeRule().getCategory().getPriority(), t2.getTimeRule().getCategory().getPriority());
        if (0 == result)
            result = t1.getTimeRule().getTitle().compareTo(t2.getTimeRule().getTitle());
        if (0 == result)
        {
            YearDistributionPart p1 = t1.getYearPart();
            YearDistributionPart p2 = t2.getYearPart();

            if (null != p1 || null != p2)
            {
                if (null != p1 && null != p2)
                {
                    result = Integer.compare(p1.getYearDistribution().getAmount(), p2.getYearDistribution().getAmount());
                    if (0 == result) result = Integer.compare(p1.getNumber(), p2.getNumber());
                }
                else
                    result = p1 == null ? -1 : 1;
            }
        }
        return result;
    };

    public List<TitleColumn> getColumns()
    {
        return TitleColumn.getList();
    }

    public List<EplNonEduLoadRowWrapper> getRows()
    {
        return _rows;
    }
}
