/* $Id:$ */
package ru.tandemservice.uniepp_load.report.bo.EplReportBase.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.meta.entity.IPropertyMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DefaultObjectFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp_load.report.bo.EplReport.EplReportManager;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.logic.IEplReport;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.logic.IEplStorableReportDesc;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.ui.Pub.EplReportBasePub;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 5/14/14
 */
@State({
    @Bind(key = EplReportBaseListUI.BIND_REPORT_KEY, binding = "reportKey")
})
public class EplReportBaseListUI<T extends IEplReport> extends UIPresenter
{
    public static final String BIND_REPORT_KEY = "reportKey";

    private String reportKey;
    private IEplStorableReportDesc reportDesc;
    private IEntityMeta entityMeta;

    private DynamicListDataSource<T> reportDS;

    @Override
    public void onComponentRefresh()
    {
        setReportDesc(EplReportManager.instance().storableReportDescExtPoint().getItem(getReportKey()));
        setEntityMeta(EntityRuntime.getMeta(getReportDesc().getReportClass()));
        prepareReportDS();
    }

    public void onClickAddReport() {
        _uiActivation.asRegion(getReportDesc().getAddFormComponent()).top().activate();
    }

    public void onClickDeleteReport() {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    public void onClickPrint() {
        BusinessComponentUtils.downloadDocument(UniReportUtils.createRenderer(getListenerParameterAsLong()), true);
    }

    public void onClickSearch() {
        getReportDS().refresh();
        DataSettingsFacade.saveSettings(getSettings());
    }

    public void onClickClear() {
        getSettings().clear();
        onClickSearch();
    }

    // presenter

    public String getSettingsKey() {
        return getReportDesc().getReportClass().getSimpleName() + "List.";
    }

    public String getPermissionKey() { return getReportDesc().getReportKey(); }

    // util

    private void prepareReportDS()
    {
        if (getReportDS() != null) return;

        DynamicListDataSource<T> dataSource = new DynamicListDataSource<>(this, component -> {
            final DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(getReportDesc().getReportClass(), "d");
            final DQLSelectBuilder dql = orderDescriptionRegistry.buildDQLSelectBuilder().column(property("d"));
            orderDescriptionRegistry.applyOrder(dql, getReportDS().getEntityOrder());
            FilterUtils.applySelectFilter(dql, "d", IEplReport.L_EDU_YEAR, getSettings().get("eduYear"));

            if (isHasOrgUnitFilter())
            {
                OrgUnit orgUnit = getSettings().get("orgUnit");
                if (null != orgUnit) {
                    FilterUtils.applySimpleLikeFilter(dql, "d", "orgUnit", orgUnit.getTitle());
                }
            }

            UniBaseUtils.createPage(getReportDS(), dql, _uiSupport.getSession());
        });
        dataSource.addColumn(new IndicatorColumn("Иконка", null).defaultIndicator(new IndicatorColumn.Item("report", "Отчет")).setOrderable(false).setClickable(false).setRequired(true));
        dataSource.addColumn(new PublisherLinkColumn("Дата формирования", IEplReport.P_FORMING_DATE)
            .setResolver(new DefaultPublisherLinkResolver()
            {
                @Override
                public String getComponentName(IEntity entity)
                {
                    return EplReportBasePub.class.getSimpleName();
                }
            })
            .setFormatter(DateFormatter.DATE_FORMATTER_WITH_TIME)

        );
        for (String property : getReportDesc().getListPropertyList()) {
            IPropertyMeta propertyMeta = getEntityMeta().getProperty(property);
            dataSource.addColumn(new SimpleColumn(propertyMeta.getTitle(), property, new DefaultObjectFormatter()).setClickable(false));
        }

        dataSource.addColumn(new IndicatorColumn("Печать", null, "onClickPrint")
            .defaultIndicator(new IndicatorColumn.Item("printer", "Печать"))
            .setImageHeader(false)
            .setDisableSecondSubmit(false)
            .setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteReport", "Удалить отчет от «{0}»?", IEplReport.P_FORMING_DATE)
            .setPermissionKey("deleteGlobalStoredReport"));
        dataSource.setOrder(IEplReport.P_FORMING_DATE, OrderDirection.desc);
        setReportDS(dataSource);
    }

    // getters and setters

    public boolean isHasOrgUnitFilter()
    {
        return getReportDesc().getListPropertyList().contains(OrgUnit.ENTITY_NAME);
    }

    public IEplStorableReportDesc getReportDesc()
    {
        return reportDesc;
    }

    public DynamicListDataSource<T> getReportDS()
    {
        return reportDS;
    }

    public void setReportDS(DynamicListDataSource<T> reportDS)
    {
        this.reportDS = reportDS;
    }

    public void setReportDesc(IEplStorableReportDesc reportDesc)
    {
        this.reportDesc = reportDesc;
    }

    public String getReportKey()
    {
        return reportKey;
    }

    public void setReportKey(String reportKey)
    {
        this.reportKey = reportKey;
    }

    public IEntityMeta getEntityMeta()
    {
        return entityMeta;
    }

    public void setEntityMeta(IEntityMeta entityMeta)
    {
        this.entityMeta = entityMeta;
    }
}