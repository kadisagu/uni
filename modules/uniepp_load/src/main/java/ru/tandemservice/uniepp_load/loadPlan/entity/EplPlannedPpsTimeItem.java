package ru.tandemservice.uniepp_load.loadPlan.entity;

import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.logic.IEplPlannedPpsDao;
import ru.tandemservice.uniepp_load.loadPlan.entity.gen.*;
import ru.tandemservice.uniepp_load.util.UniEppLoadUtils;

/** @see ru.tandemservice.uniepp_load.loadPlan.entity.gen.EplPlannedPpsTimeItemGen */
public class EplPlannedPpsTimeItem extends EplPlannedPpsTimeItemGen
{
    public EplPlannedPpsTimeItem()
    {
    }

    public EplPlannedPpsTimeItem(IEplPlannedPpsDao.ITimeData t)
    {
        setTimeItem(t.timeItem());
        setPps(t.pps());
        setTimeAmountAsLong(t.timeAmountAsLong());
        setOverTime(t.overtime());
    }

    public double getTimeAmount() {
        return UniEppLoadUtils.wrap(getTimeAmountAsLong());
    }

    public void setTimeAmount(double timeAmount) {
        setTimeAmountAsLong(Math.round(100 * timeAmount));
    }
}