/* $Id:$ */
package ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.ui.Tab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;

/**
 * @author oleyba
 * @since 12/6/14
 */
@State({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "orgUnitId", required = true),
    @Bind(key = EplOrgUnitTabUI.BIND_SELECTED_TAB, binding = "selectedTab")
})
@Output({
    @Bind(key = EplOrgUnitTabUI.BIND_ORG_UNIT_ID, binding = "orgUnitId")
})
public class EplOrgUnitTabUI extends UIPresenter
{
    public static final String BIND_ORG_UNIT_ID = "eplOrgUnitTabOrgUnitId";
    public static final String BIND_SELECTED_TAB = "selectedTab";

    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private CommonPostfixPermissionModel _secModel;
    private String _selectedTab;

    @Override
    public void onComponentRefresh()
    {
        setOrgUnit(DataAccessServices.dao().get(OrgUnit.class, getOrgUnitId()));
        setSecModel(new OrgUnitSecModel(getOrgUnit()));
    }

    // getters and setters

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public Long getOrgUnitId() { return _orgUnitId; }

    public void setOrgUnitId(Long orgUnitId) { _orgUnitId = orgUnitId; }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }
}