package ru.tandemservice.uniepp_load.catalog.entity;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeCategory.ui.NonEduLoadAddEdit.EplTimeCategoryNonEduLoadAddEdit;
import ru.tandemservice.uniepp_load.catalog.entity.gen.EplTimeCategoryNonEduLoadGen;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.instanceOf;

/** @see ru.tandemservice.uniepp_load.catalog.entity.gen.EplTimeCategoryNonEduLoadGen */
public class EplTimeCategoryNonEduLoad extends EplTimeCategoryNonEduLoadGen implements IPrioritizedCatalogItem
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return EplTimeCategory.defaultSelectDSHandler(ownerId).customize((alias, dql, context, filter) -> dql.where(instanceOf(alias, EplTimeCategoryNonEduLoad.class)));
    }

    private static List<String> HIDDEN_PROPERTIES = ImmutableList.of(L_PARENT, P_USER_CODE);

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public Collection<String> getHiddenFields() { return HIDDEN_PROPERTIES; }
            @Override public Collection<String> getHiddenFieldsAddEditForm() { return ImmutableSet.of(P_USER_CODE); }
            @Override public String getAddEditComponentName() { return EplTimeCategoryNonEduLoadAddEdit.class.getSimpleName(); }
        };
    }
}