/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.DisciplineTimeDetail;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTransferOrgUnitEduGroupTimeItem;

import static ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.DisciplineTimeDetail.EplOuSummaryDisciplineTimeDetailDSHandler.*;

/**
 * @author oleyba
 * @since 5/14/12
 */
@Configuration
public class EplOuSummaryDisciplineTimeDetail extends BusinessComponentManager
{
    public static final String BIND_GROUP_TYPE = "groupType";
    public static final String BIND_EDU_GROUP = "eduGroup";
    public static final String BIND_REG_ELEMENT_PART = "registryElementPart";
    public static final String BIND_TIME_RULE = "timeRule";
    public static final String BIND_TIME_RULE_DESCRIPTION = "timeRuleDesc";
    public static final String BIND_DESCRIPTION = "description";
    public static final String BIND_CREATION_DATE = "creationDate";

    public static final String DS_DISCIPLINE_TIME_DETAIL = "disciplineTimeDetailDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(DS_DISCIPLINE_TIME_DETAIL, disciplineTimeDetailDSColumns(), disciplineTimeDetailDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint disciplineTimeDetailDSColumns()
    {
        return columnListExtPointBuilder(DS_DISCIPLINE_TIME_DETAIL)
                .addColumn(textColumn(BIND_GROUP_TYPE, EplEduGroupTimeItem.eduGroup().groupType().abbreviation()).order())
                .addColumn(textColumn(BIND_EDU_GROUP, EplEduGroupTimeItem.eduGroup().title()).order())
                .addColumn(textColumn(VIEW_ACADEMIC_GROUP, VIEW_ACADEMIC_GROUP).formatter(CollectionFormatter.ROW_FORMATTER))
                .addColumn(textColumn(VIEW_COURSE, VIEW_COURSE).formatter(CollectionFormatter.ROW_FORMATTER))
                .addColumn(textColumn(VIEW_TERM, VIEW_TERM).formatter(CollectionFormatter.ROW_FORMATTER))
                .addColumn(textColumn(VIEW_YEAR_PART, VIEW_YEAR_PART).formatter(CollectionFormatter.ROW_FORMATTER))
                .addColumn(textColumn(VIEW_AMOUNT, VIEW_AMOUNT).formatter(RawFormatter.INSTANCE).order())
                .addColumn(publisherColumn(BIND_REG_ELEMENT_PART, EplEduGroupTimeItem.registryElementPart().shortTitle())
                                   .publisherLinkResolver(new SimplePublisherLinkResolver(EplEduGroupTimeItem.registryElementPart().registryElement().id()))
                                   .visible("ui:hasTimeRule").order())
                .addColumn(publisherColumn(BIND_TIME_RULE, EplEduGroupTimeItem.timeRule().title())
                                   .publisherLinkResolver(new SimplePublisherLinkResolver(EplEduGroupTimeItem.timeRule().id()))
                                   .visible("ui:hasDiscipline").order())
                .addColumn(textColumn(BIND_TIME_RULE_DESCRIPTION, EplEduGroupTimeItem.timeRule().description()).visible("ui:hasDiscipline"))
                .addColumn(textColumn(BIND_DESCRIPTION, EplEduGroupTimeItem.description()))
                .addColumn(textColumn(BIND_CREATION_DATE, EplEduGroupTimeItem.creationDate()).order().formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .addColumn(publisherColumn(VIEW_TRANSFER_FROM, VIEW_TRANSFER_FROM)
                                   .formatter(item -> item == null ? null : ((EplTransferOrgUnitEduGroupTimeItem) item).getSummary().getShortTitle())
                                   .publisherLinkResolver(new SimplePublisherLinkResolver(EplTransferOrgUnitEduGroupTimeItem.summary().id()))
                                   .visible("ui:hasTransferRow"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).permissionKey("eplOrgUnitSummaryPubEditParameters")
                                   .alert("ui:disciplineTimeDetailDeleteAlert").disabled("ui:deleteDisabled"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> disciplineTimeDetailDSHandler()
    {
        return new EplOuSummaryDisciplineTimeDetailDSHandler(getName());
    }
}
