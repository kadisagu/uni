package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_load_2x7x1_9to10 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eplReportOrgUnitEduLoadA

		// создана новая сущность
		{
            if(!tool.tableExists("epl_rep_ou_edu_load_a_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("epl_rep_ou_edu_load_a_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                        new DBColumn("studentsummary_id", DBType.LONG).setNullable(false),
                        new DBColumn("orgunit_p", DBType.createVarchar(255))
                );
                tool.createTable(dbt);
            }

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eplReportOrgUnitEduLoadA");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eplSimpleTimeItem

		// создано обязательное свойство countAsContract
        if (!tool.columnExists("epl_simple_time_item_t", "countascontract_p"))
        {
            // создать колонку
            tool.createColumn("epl_simple_time_item_t", new DBColumn("countascontract_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            tool.executeUpdate("update epl_simple_time_item_t set countascontract_p=? where countascontract_p is null", Boolean.FALSE);

            // сделать колонку NOT NULL
            tool.setColumnNullable("epl_simple_time_item_t", "countascontract_p", false);
        }
    }
}