package ru.tandemservice.uniepp_load.base.entity.indPlan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndPlan2OuSummaryRel;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Учебная нагрузка для ИПП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplIndPlan2OuSummaryRelGen extends EntityBase
 implements INaturalIdentifiable<EplIndPlan2OuSummaryRelGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndPlan2OuSummaryRel";
    public static final String ENTITY_NAME = "eplIndPlan2OuSummaryRel";
    public static final int VERSION_HASH = -746860886;
    private static IEntityMeta ENTITY_META;

    public static final String L_IND_PLAN = "indPlan";
    public static final String L_OU_SUMMARY = "ouSummary";

    private EplIndividualPlan _indPlan;     // Индивидуальный план
    private EplOrgUnitSummary _ouSummary;     // Расчет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Индивидуальный план. Свойство не может быть null.
     */
    @NotNull
    public EplIndividualPlan getIndPlan()
    {
        return _indPlan;
    }

    /**
     * @param indPlan Индивидуальный план. Свойство не может быть null.
     */
    public void setIndPlan(EplIndividualPlan indPlan)
    {
        dirty(_indPlan, indPlan);
        _indPlan = indPlan;
    }

    /**
     * @return Расчет. Свойство не может быть null.
     */
    @NotNull
    public EplOrgUnitSummary getOuSummary()
    {
        return _ouSummary;
    }

    /**
     * @param ouSummary Расчет. Свойство не может быть null.
     */
    public void setOuSummary(EplOrgUnitSummary ouSummary)
    {
        dirty(_ouSummary, ouSummary);
        _ouSummary = ouSummary;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EplIndPlan2OuSummaryRelGen)
        {
            if (withNaturalIdProperties)
            {
                setIndPlan(((EplIndPlan2OuSummaryRel)another).getIndPlan());
                setOuSummary(((EplIndPlan2OuSummaryRel)another).getOuSummary());
            }
        }
    }

    public INaturalId<EplIndPlan2OuSummaryRelGen> getNaturalId()
    {
        return new NaturalId(getIndPlan(), getOuSummary());
    }

    public static class NaturalId extends NaturalIdBase<EplIndPlan2OuSummaryRelGen>
    {
        private static final String PROXY_NAME = "EplIndPlan2OuSummaryRelNaturalProxy";

        private Long _indPlan;
        private Long _ouSummary;

        public NaturalId()
        {}

        public NaturalId(EplIndividualPlan indPlan, EplOrgUnitSummary ouSummary)
        {
            _indPlan = ((IEntity) indPlan).getId();
            _ouSummary = ((IEntity) ouSummary).getId();
        }

        public Long getIndPlan()
        {
            return _indPlan;
        }

        public void setIndPlan(Long indPlan)
        {
            _indPlan = indPlan;
        }

        public Long getOuSummary()
        {
            return _ouSummary;
        }

        public void setOuSummary(Long ouSummary)
        {
            _ouSummary = ouSummary;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EplIndPlan2OuSummaryRelGen.NaturalId) ) return false;

            EplIndPlan2OuSummaryRelGen.NaturalId that = (NaturalId) o;

            if( !equals(getIndPlan(), that.getIndPlan()) ) return false;
            if( !equals(getOuSummary(), that.getOuSummary()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getIndPlan());
            result = hashCode(result, getOuSummary());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getIndPlan());
            sb.append("/");
            sb.append(getOuSummary());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplIndPlan2OuSummaryRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplIndPlan2OuSummaryRel.class;
        }

        public T newInstance()
        {
            return (T) new EplIndPlan2OuSummaryRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "indPlan":
                    return obj.getIndPlan();
                case "ouSummary":
                    return obj.getOuSummary();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "indPlan":
                    obj.setIndPlan((EplIndividualPlan) value);
                    return;
                case "ouSummary":
                    obj.setOuSummary((EplOrgUnitSummary) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "indPlan":
                        return true;
                case "ouSummary":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "indPlan":
                    return true;
                case "ouSummary":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "indPlan":
                    return EplIndividualPlan.class;
                case "ouSummary":
                    return EplOrgUnitSummary.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplIndPlan2OuSummaryRel> _dslPath = new Path<EplIndPlan2OuSummaryRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplIndPlan2OuSummaryRel");
    }
            

    /**
     * @return Индивидуальный план. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndPlan2OuSummaryRel#getIndPlan()
     */
    public static EplIndividualPlan.Path<EplIndividualPlan> indPlan()
    {
        return _dslPath.indPlan();
    }

    /**
     * @return Расчет. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndPlan2OuSummaryRel#getOuSummary()
     */
    public static EplOrgUnitSummary.Path<EplOrgUnitSummary> ouSummary()
    {
        return _dslPath.ouSummary();
    }

    public static class Path<E extends EplIndPlan2OuSummaryRel> extends EntityPath<E>
    {
        private EplIndividualPlan.Path<EplIndividualPlan> _indPlan;
        private EplOrgUnitSummary.Path<EplOrgUnitSummary> _ouSummary;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Индивидуальный план. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndPlan2OuSummaryRel#getIndPlan()
     */
        public EplIndividualPlan.Path<EplIndividualPlan> indPlan()
        {
            if(_indPlan == null )
                _indPlan = new EplIndividualPlan.Path<EplIndividualPlan>(L_IND_PLAN, this);
            return _indPlan;
        }

    /**
     * @return Расчет. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndPlan2OuSummaryRel#getOuSummary()
     */
        public EplOrgUnitSummary.Path<EplOrgUnitSummary> ouSummary()
        {
            if(_ouSummary == null )
                _ouSummary = new EplOrgUnitSummary.Path<EplOrgUnitSummary>(L_OU_SUMMARY, this);
            return _ouSummary;
        }

        public Class getEntityClass()
        {
            return EplIndPlan2OuSummaryRel.class;
        }

        public String getEntityName()
        {
            return "eplIndPlan2OuSummaryRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
