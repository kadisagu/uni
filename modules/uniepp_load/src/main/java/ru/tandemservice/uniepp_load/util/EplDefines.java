package ru.tandemservice.uniepp_load.util;

import org.tandemframework.core.settings.DataSettingsFacade;

/**
 * Created by nsvetlov on 09.03.2017.
 */
public interface EplDefines
{
    String MAX_LOAD_SETTINGS_PREFIX = "MAX_LOAD_";
    String MAX_LOAD_SETING_NAME = "value";

    public static Double getMaxLoadSetting()
    {
        final Double value = DataSettingsFacade.getSettings("general", EplDefines.MAX_LOAD_SETTINGS_PREFIX).get(EplDefines.MAX_LOAD_SETING_NAME);
        return value == null ? new Double(1.5) : value;
    }
}

