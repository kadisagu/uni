package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_load_2x10x6_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eplIndividualPlan

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epl_ind_plan_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eplindividualplan"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("pps_id", DBType.LONG).setNullable(false), 
				new DBColumn("eppyear_id", DBType.LONG).setNullable(false),
				new DBColumn("orgunit_id", DBType.LONG).setNullable(false), 
				new DBColumn("ousummary_id", DBType.LONG), 
				new DBColumn("formingdate_p", DBType.TIMESTAMP).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eplIndividualPlan");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eplTimeItemNonEduLoad

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epl_time_item_non_edu_load_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_epltimeitemnoneduload"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("indplan_id", DBType.LONG).setNullable(false), 
				new DBColumn("timerule_id", DBType.LONG).setNullable(false), 
				new DBColumn("timeamountaslong_p", DBType.LONG).setNullable(false), 
				new DBColumn("controltimeamountaslong_p", DBType.LONG).setNullable(false), 
				new DBColumn("parameter_p", DBType.DOUBLE), 
				new DBColumn("yearpart_id", DBType.LONG), 
				new DBColumn("datefrom_p", DBType.DATE), 
				new DBColumn("dateto_p", DBType.DATE),
				new DBColumn("creationdate_p", DBType.TIMESTAMP).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eplTimeItemNonEduLoad");
		}
    }
}