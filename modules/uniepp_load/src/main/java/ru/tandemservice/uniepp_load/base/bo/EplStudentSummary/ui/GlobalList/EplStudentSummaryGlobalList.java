/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.GlobalList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;

/**
 * @author oleyba
 * @since 10/6/11
 */
@Configuration
public class EplStudentSummaryGlobalList extends BusinessComponentManager
{
    public static final String EPL_STUDENT_SUMMARY_GLOBAL_DS = "eplStudentSummaryGlobalDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(EducationCatalogsManager.instance().eduYearDSConfig())
            .addDataSource(this.searchListDS(EPL_STUDENT_SUMMARY_GLOBAL_DS, this.eplStudentSummaryGlobalDSColumns(), this.eplStudentSummaryGlobalDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint eplStudentSummaryGlobalDSColumns()
    {
        return columnListExtPointBuilder(EPL_STUDENT_SUMMARY_GLOBAL_DS)
            .addColumn(textColumn("year", EplStudentSummary.eppYear().title()).order().create())
            .addColumn(publisherColumn("title", EplStudentSummary.P_TITLE).order().create())
            .addColumn(textColumn("creationDate", EplStudentSummary.creationDate()).order().formatter(DateFormatter.DEFAULT_DATE_FORMATTER).create())
            .addColumn(textColumn(EplStudentSummary.L_STATE, EplStudentSummary.state().title()).order())
            .addColumn(toggleColumn(EplStudentSummary.P_ARCHIVAL, "ui:notArchival")
                .toggleOnListener("onClickChangeArchival").toggleOnListenerAlert(alert("eplStudentSummaryGlobalDS.archival.no.alert", EplStudentSummary.P_TITLE_WITH_STATE)).toggleOnLabel("eplStudentSummaryGlobalDS.archival.no")
                .toggleOffListener("onClickChangeArchival").toggleOffListenerAlert(alert("eplStudentSummaryGlobalDS.archival.yes.alert", EplStudentSummary.P_TITLE_WITH_STATE)).toggleOffLabel("eplStudentSummaryGlobalDS.archival.yes")
                .permissionKey("eplStudentSummaryListArchival"))
            .addColumn(actionColumn("copy", new Icon("plus_plus"), "onClickCopy")
                .permissionKey("eplStudentSummaryListAdd"))
            .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEditTitle")
                .permissionKey("eplStudentSummaryListEdit"))
            .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
                .alert(FormattedMessage.with().template("eplStudentSummaryGlobalDS.deleteAlert").parameter(EplStudentSummary.P_TITLE_WITH_STATE).create())
                .permissionKey("eplStudentSummaryListDelete")
                .disabled("ui:summaryDeleteDisabled")
                .create())
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eplStudentSummaryGlobalDSHandler()
    {
        return new EplStudentSummaryDSHandler(getName());
    }
}