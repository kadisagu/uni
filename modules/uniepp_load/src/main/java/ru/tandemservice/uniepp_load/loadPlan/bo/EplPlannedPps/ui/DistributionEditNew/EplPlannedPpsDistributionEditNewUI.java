/* $Id:$ */
package ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.DistributionEditNew;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.mutable.MutableLong;
import org.apache.log4j.Logger;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.debug.SectionDebugItem;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.commonbase.utils.DQLSimple;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.EplOuSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.IEplOuSummaryDAO;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCategoryWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCommonRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplDiscOrRuleRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplSimpleRuleRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplBaseCategoryDataWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplBaseTimeDataWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.base.EplCommonRowWrapperComparator;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.distribution.EplDistributionCommonRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.distribution.EplDistributionDiscRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.distribution.EplDistributionSimpleRuleRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.distribution.EplDistributionSubRowWrapper;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.*;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.gen.EplTimeItemGen;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.EplPlannedPpsManager;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.logic.IEplPlannedPpsDao;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPps;
import ru.tandemservice.uniepp_load.loadPlan.entity.EplPlannedPpsTimeItem;
import ru.tandemservice.uniepp_load.loadPlan.util.PpsTimeStatsUtil;
import ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.EplSettingsPpsLoadManager;
import ru.tandemservice.uniepp_load.settings.entity.EplSettingsPpsLoad;
import ru.tandemservice.uniepp_load.util.UniEppLoadUtils;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.DistributionEditNew.EplPlannedPpsDistributionEditNew.*;

/**
 * @author oleyba
 * @since 21.08.15
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id"),
    @Bind(key = EplPlannedPpsDistributionEditNewUI.VIEW_MODE, binding = "viewMode")
})
public class EplPlannedPpsDistributionEditNewUI extends UIPresenter
{
    private EntityHolder<EplOrgUnitSummary> holder = new EntityHolder<>();

    private EplCategoryWrapper _category;
    private List<EplCategoryWrapper> _currentSubcategories;

    public static final String VIEW_MODE = "viewMode";

    private boolean viewMode;

    private List<EplDistributionCommonRowWrapper> tableRows;
    private Map<Long, EplDistributionCommonRowWrapper> rowMap;
    private EplDistributionCommonRowWrapper tableRow;
    private EplDistributionSubRowWrapper tableSubRow;

    private Map<MultiKey, List<Long>> ppsDownKeys;
    private Map<Long, PpsTimeStatsUtil.ResultRow> ppsTimeStats;

    private EplBaseTimeDataWrapper _dataWrapper;

    private List<EplDistributionCommonRowWrapper.TitleColumn> titleColumnList = EplDistributionCommonRowWrapper.TitleColumn.getList();
    private EplDistributionCommonRowWrapper.TitleColumn titleColumn;

    private int rowNum;
    private MutableLong rowIdCounter;
    private boolean _hasTimeData;

    private static final Logger logger = Logger.getLogger(EplPlannedPpsDistributionEditNewUI.class);

    private ISelectModel ppsModel;

    // actions

    @Override
    public void onComponentRefresh()
    {
        Debug.begin("EplPlannedPpsDistributionEditUI.onComponentRefresh");
        try {
            getHolder().refresh();

            Set<String> propertyPathSet = Sets.newHashSet(EplEduGroupTimeItem.L_EDU_GROUP);
            _dataWrapper = new EplBaseTimeDataWrapper(getSummary(), false, true).fillDataSource(propertyPathSet, false);
            if (!(_hasTimeData = null != _dataWrapper)) return;

            Debug.begin("prepareTimeData");
            prepareTimeData();
            Debug.end();

            List<EplPlannedPps> planPpsList = new DQLSimple<>(EplPlannedPps.class)
                    .where(EplPlannedPps.orgUnitSummary().id(), getSummary().getId())
                    .list().stream()
                    .sorted((p1, p2) -> {
                        int result = CommonCollator.RUSSIAN_COLLATOR.compare(p1.getTitle(), p2.getTitle());
                        if (0 == result)
                            result = Integer.compare(p1.getType().getPriority(), p2.getType().getPriority());
                        return result;
                    })
                    .collect(Collectors.toList());

            ppsModel = new LazySimpleSelectModel<>(planPpsList, EplPlannedPps.displayableTitle().s())
                .setSortProperty(EplPlannedPps.displayableTitle().s())
                .setSearchProperty(EplPlannedPps.displayableTitle().s());

            onChangeFilterParams();
        } finally {
            final SectionDebugItem end = Debug.end();
            if (end != null && logger.isInfoEnabled()) {
                logger.info(end.toFullString());
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(BIND_ORG_UNIT_SUMMARY, getSummary());
        dataSource.putAll(getSettings().getAsMap(BIND_DISC_OR_RULE, BIND_YEAR_PART, BIND_COURSE));
    }

    public void onChangeFilterParams() {
        rowNum = 1;
        getSettings().set(BIND_HAS_TRANSFER_TO, isHasTransferTo());
        getSettings().save();
        getTableRows().forEach(r -> r.updateVisibility(getSettings()));
        getTitleColumnList().forEach(c -> c.updateVisibility(getSettings()));

        fillPpsVisible();
    }

    public void onClearFilters() {
        clearSettings();
        onChangeFilterParams();
    }

    public void onChangePps() {
        rowNum = 1;
        refreshPpsTimeStats();
        getTableRows().forEach(r -> r.updateVisibility(getSettings()));
    }

    public void onChangeTime() {
        rowNum = 1;
        ((EplDistributionSubRowWrapper) rowMap.get(getListenerParameterAsLong())).updateSum();
        refreshPpsTimeStats();
    }

    public void onClickFillPpsDown()
    {
        rowNum = 1;
        EplDistributionCommonRowWrapper currentRow = rowMap.get(getListenerParameterAsLong());
        if (currentRow.isPpsDownVisible())
        {
            PairKey<EplPlannedPps, Boolean> firstPairKey = null;
            List<Long> rowIds = ppsDownKeys.get(getPpsDownKey(currentRow));

            for (Long rowId : rowIds)
            {
                EplDistributionCommonRowWrapper row = rowMap.get(rowId);
                PairKey<EplPlannedPps, Boolean> pairKey = PairKey.create(row.getPps(), row.isOvertime());

                if (null == firstPairKey) firstPairKey = pairKey;
                else
                {
                    row.setPps(firstPairKey.getFirst());
                    row.setOvertime(firstPairKey.getSecond());
                    row.getChildren().clear();
                }
            }
            refreshPpsTimeStats();
        }
    }

    public void onClickAddSubRow() {
        rowNum = 1;
        EplDistributionCommonRowWrapper row = rowMap.get(getListenerParameterAsLong());
        row.setPpsDownVisible(false);

        rowIdCounter.decrement();
        long newId = rowIdCounter.longValue();
        EplDistributionSubRowWrapper newRow = new EplDistributionSubRowWrapper(newId, row);
        newRow.initTimeFromParent();
        row.getChildren().add(newRow);
        newRow.updateSum();
        rowMap.put(newId, newRow);
    }

    public void onClickDeleteSubRow() {
        rowNum = 1;
        EplDistributionSubRowWrapper row = (EplDistributionSubRowWrapper) rowMap.remove(getListenerParameterAsLong());
        EplDistributionCommonRowWrapper parent = row.getParent();
        parent.getChildren().remove(row);
        parent.setPps(row.getPps());
        parent.setOvertime(row.isOvertime());
        parent.getChildren().forEach(EplDistributionSubRowWrapper::updateSum);
        refreshPpsTimeStats();

        // проверяем, нужно ли выставить видимость кнопки ППС
        if (parent.getChildren().isEmpty())
        {
            MultiKey key = getPpsDownKey(parent);
            List<Long> rowIds = ppsDownKeys.get(key);

            if (rowIds.size() > 1)
            {
                boolean hasChildren = false;
                for (Long rowId : ppsDownKeys.get(key))
                {
                    if (!rowMap.get(rowId).getChildren().isEmpty())
                        hasChildren = true;
                }
                if (!hasChildren)
                {
                    Long firstRowId = ppsDownKeys.get(key).get(0);
                    rowMap.get(firstRowId).setPpsDownVisible(true);
                }
            }
        }
    }

    public void onClickSave()
    {
        if (updateDistribution()) {
            deactivate();
        }
    }

    public void onClickApply()
    {
        updateDistribution();
        rowNum = 1;
        // _uiSupport.doRefresh();
    }

    // presenter

    public EplOrgUnitSummary getSummary()
    {
        return getHolder().getValue();
    }

    public EplBaseCategoryDataWrapper getCategoryDataWrapper()
    {
        return _dataWrapper.getCategoryDataWrapper();
    }

    public Set<EplTimeCategoryEduLoad> getUsedCategories()
    {
        return getCategoryDataWrapper().getUsedCategories();
    }

    public List<EplCategoryWrapper> getTopLevelCategories()
    {
        return getCategoryDataWrapper().getTopLevelCategories();
    }

    public List<EplCategoryWrapper> getBottomLevelCategories()
    {
        return getCategoryDataWrapper().getBottomLevelCategories();
    }

    public List<List<EplCategoryWrapper>> getSubcategories()
    {
        return getCategoryDataWrapper().getSubcategories();
    }

    public long getTableColumnCount()
    {
        return getCategoryDataWrapper().getUsedCategories().size() + getTitleColumnList().stream().filter(EplDistributionCommonRowWrapper.TitleColumn::isVisible).count() + (isViewMode() ? 7 : 9);
    }

    public String getCellTimeSum()
    {
        EplDistributionCommonRowWrapper rowWrapper = rowMap.get(getTableRow().getId());
        Double time = UniEppLoadUtils.wrap(rowWrapper.getDistributionTimeMap().get(getCategory().getId()));
        Double childrenTime = UniEppLoadUtils.wrap(rowWrapper.childrenTimeMap.get(getCategory().getId()));

        if (!rowWrapper.isHasChildren() || time == null || childrenTime == null || time.longValue() == childrenTime.longValue()) {
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(time);
        }
        return "<div>" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(time) + "</div>" + PpsTimeStatsUtil.colorValue(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(childrenTime), true);
    }

    public Long getSubRowCellTimeSum()
    {
        EplDistributionCommonRowWrapper rowWrapper = rowMap.get(getTableSubRow().getId());
        return rowWrapper.getDistributionTimeMap().get(getCategory().getId());
    }

    public boolean isHasTransferTo()
    {
        List<EplCommonRowWrapper> rowWrappers = getSettings().get(BIND_DISC_OR_RULE);
        Set<EppRegistryElementPart> disciplines = Sets.newHashSet();
        if (CollectionUtils.isNotEmpty(rowWrappers))
        {
            disciplines = rowWrappers.stream()
                    .map(EplCommonRowWrapper::getDiscipline)
                    .collect(Collectors.toSet());
        }

        if (disciplines.isEmpty())
            return ISharedBaseDao.instance.get().existsEntity(EplTransferOrgUnitEduGroupTimeItem.class, EplTransferOrgUnitEduGroupTimeItem.summary().s(), getSummary());
        else
            return ISharedBaseDao.instance.get().existsEntity(EplTransferOrgUnitEduGroupTimeItem.class,
                                                              EplTransferOrgUnitEduGroupTimeItem.summary().s(), getSummary(),
                                                              EplTransferOrgUnitEduGroupTimeItem.registryElementPart().s(), disciplines);
    }

    public void setSubRowCellTimeSum(Long value)
    {
        EplDistributionCommonRowWrapper rowWrapper = rowMap.get(getTableSubRow().getId());
        rowWrapper.getDistributionTimeMap().put(getCategory().getId(), value);
    }

    public String getSubRowCellTimeSumStr()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(UniEppLoadUtils.wrap(getSubRowCellTimeSum()));
    }

    public boolean isSubRowCellTimeVisible() {
        return getTableSubRow().getParent().getDistributionTimeMap().get(getCategory().getId()) != null;
    }

    public String getSubRowCellTimeSumId() {
        return "ct." + getTableSubRow().getId();
    }

    public String getTitleColumnCellValue()
    {
        return rowMap.get(getTableRow().getId()).getViewProperty(getTitleColumn().getType());
    }

    public boolean isTitleColumnLink()
    {
        return isViewMode() && rowMap.get(getTableRow().getId()).getLink(getTitleColumn()) != null;
    }

    public String getTitleColumnLinkComponentName()
    {
        PairKey<String, ParametersMap> parameters = rowMap.get(getTableRow().getId()).getLink(getTitleColumn());
        return parameters == null ? null : parameters.getFirst();
    }

    public ParametersMap getTitleColumnLinkParameters()
    {
        PairKey<String, ParametersMap> parameters = rowMap.get(getTableRow().getId()).getLink(getTitleColumn());
        return parameters == null ? null : parameters.getSecond();
    }

    public int getRowNum()
    {
        return rowNum++;
    }

    public String getPpsFieldId()
    {
        return getPpsFieldId(true);
    }

    public String getPpsFieldSubId()
    {
        return getPpsFieldId(false);
    }

    public String getPpsFieldId(boolean top)
    {
        return "ppsField." + (top ? getTableRow().getId() : getTableSubRow().getId());
    }

    public String getOvertimeFieldId()
    {
        return getOvertimeFieldId(true);
    }

    public String getOvertimeFieldSubId()
    {
        return getOvertimeFieldId(false);
    }

    public String getOvertimeFieldId(boolean top)
    {
        return "overtimeField." + (top ? getTableRow().getId() : getTableSubRow().getId());
    }

    public String getAddSubRowButtonId() {
        return "addSub." + getTableRow().getId();
    }

    public String getDeleteSubRowButtonId() {
        return "deleteSub." + getTableSubRow().getId();
    }

    public String getPpsTimeOnRate()
    {
        return getPpsTime(true, 0);
    }

    public String getPpsTimeOnOverrate()
    {
        return getPpsTime(true, 1);
    }

    public String getPpsTimeOnTotal()
    {
        return getPpsTime(true, 2);
    }

    public String getPpsTimeSubOnRate()
    {
        return getPpsTime(false, 0);
    }

    public String getPpsTimeSubOnOverrate()
    {
        return getPpsTime(false, 1);
    }

    public String getPpsTimeSubOnTotal()
    {
        return getPpsTime(false, 2);
    }

    public String getPpsTime(boolean top, int index)
    {
        EplDistributionCommonRowWrapper row = top ? getTableRow() : getTableSubRow();
        if (row == null || row.getPps() == null || row.isHasChildren()) return "";
        PpsTimeStatsUtil.ResultRow resultRow = ppsTimeStats.getOrDefault(row.getPps().getId(), null);
        if (null == resultRow) return "";
        switch (index)
        {
            case 0 : return resultRow.getRateMessageColor();
            case 1 : return resultRow.getOverrateMessageColor();
            case 2 : return resultRow.getTotalMessageColor();
            default: return "";
        }
    }

    public List<EplDistributionCommonRowWrapper> getVisibleTableRows() {
        return getTableRows().stream().filter(EplDistributionCommonRowWrapper::isVisible).collect(Collectors.toList());
    }

    public List<EplDistributionCommonRowWrapper.TitleColumn> getVisibleTitleColumnList() {
        return getTitleColumnList().stream().filter(EplDistributionCommonRowWrapper.TitleColumn::isVisible).collect(Collectors.toList());
    }

    public String getPermissionKey() {
        return isViewMode() ? "eplPlannedPpsDistributionTabView" : "eplPlannedPpsDistributionTabEdit";
    }

    public boolean isEditMode() {
        return !isViewMode();
    }

    public boolean isNotHasTableRowChildren()
    {
        return !getTableRow().isHasChildren();
    }

    public boolean isOvertimeVisible()
    {
        return getTableRow().isShowOvertime() && isNotHasTableRowChildren();
    }

    // data

    private void prepareTimeData()
    {
        rowIdCounter = new MutableLong(0);

        // данные о контингенте для отображения в таблице
        IEplOuSummaryDAO.IDistributionEduGroupData eduGroupData = EplOuSummaryManager.instance().dao().prepareDistributionEduGroupData(Collections.singleton(getSummary()), null, isHasTransferTo());

        // загружаем расчетные данные, которые надо распределить
        rowMap = Maps.newHashMap();
        Map<MultiKey, EplDistributionCommonRowWrapper> rowByKeyMap = Maps.newHashMap();
        // сначала по дисциплинам
        List<EplDistributionDiscRowWrapper> discRows = Lists.newArrayList();

        _dataWrapper.getDiscRowMap().values().forEach(entry -> {
            for (Map.Entry<EplCommonRowWrapper.DisciplineOrRulePairKey, EplDiscOrRuleRowWrapper> rowEntry : entry.entrySet())
            {
                MultiKey groupingKey = rowEntry.getKey();
                EplDiscOrRuleRowWrapper wrapper = rowEntry.getValue();

                if (CollectionUtils.isEmpty(wrapper.getEplGroups())) continue;
                if (wrapper.getEplGroups().size() > 1)
                    throw new IllegalStateException("EplGroup must be contain only one item.");

                EplEduGroup eduGroup = wrapper.getEplGroups().iterator().next();
                if (null == eduGroup) continue;

                EplDistributionDiscRowWrapper rowWrapper = new EplDistributionDiscRowWrapper(wrapper);
                Long id = wrapper.getId();

                rowMap.put(id, rowWrapper);
                rowByKeyMap.put(groupingKey, rowWrapper);
                discRows.add(rowWrapper);

                Set<EplStudent> students = _dataWrapper.getGroupMap().getOrDefault(eduGroup, Collections.emptyMap()).keySet();
                rowWrapper.init(eduGroup, eduGroupData, students);

                rowIdCounter.setValue(id < rowIdCounter.longValue() ? id : rowIdCounter.longValue());
            }
        });

        // добавляем простые норм
        List<EplDistributionSimpleRuleRowWrapper> ruleRows = Lists.newArrayList();
        _dataWrapper.getSimpleRuleRowMap().values().forEach(entry -> {
            for (Map.Entry<EplCommonRowWrapper.DisciplineOrRulePairKey, EplSimpleRuleRowWrapper> rowEntry : entry.entrySet())
            {
                MultiKey groupingKey = rowEntry.getKey();
                EplSimpleRuleRowWrapper wrapper = rowEntry.getValue();

                EplDistributionSimpleRuleRowWrapper rowWrapper = new EplDistributionSimpleRuleRowWrapper(wrapper);
                Long id = wrapper.getId();

                rowMap.put(id, rowWrapper);
                wrapper.getSimpleTimeItemList().forEach(rowWrapper::addTime);

                rowByKeyMap.put(groupingKey, rowWrapper);
                ruleRows.add(rowWrapper);
                rowIdCounter.setValue(id < rowIdCounter.longValue() ? id : rowIdCounter.longValue());
            }
        });

        Collections.sort(discRows, (d1, d2) ->
                ComparisonChain.start()
                        .compare(d1, d2, EplCommonRowWrapperComparator.INSTANCE)
                        .compare(d1.getMinCourse(), d2.getMinCourse())
                        .compare(d1.getEduGroup().getTitle(), d2.getEduGroup().getTitle())
                        .compare(d1.getEduGroup().getGroupType().getPriority(), d2.getEduGroup().getGroupType().getPriority())
                        .result()
        );
        Collections.sort(ruleRows, ITitled.TITLED_COMPARATOR);

        tableRows = Lists.newArrayList();
        tableRows.addAll(discRows);
        tableRows.addAll(ruleRows);

        prepareDistributionCurrent(rowByKeyMap);
        refreshPpsTimeStats();
        fillPpsVisible();
    }

    private void fillPpsVisible()
    {
        if (isViewMode()) return;

        ppsDownKeys = Maps.newHashMap();
        getVisibleTableRows().forEach(row -> {
            MultiKey key = getPpsDownKey(row);
            row.setPpsDownVisible(!row.isHasChildren() && !ppsDownKeys.containsKey(key));
            SafeMap.safeGet(ppsDownKeys, key, LinkedList.class).add(row.getId());
        });
        ppsDownKeys.entrySet().stream()
                .filter(item -> item.getValue().size() == 1)
                .forEach(item -> rowMap.get(item.getValue().get(0)).setPpsDownVisible(false));
    }

    private MultiKey getPpsDownKey(EplDistributionCommonRowWrapper row)
    {
        EplCommonRowWrapper.DisciplineOrRulePairKey rowKey = row.getKey();
        String groupTitle = row.getViewProperty(EplDistributionCommonRowWrapper.TitleColumnType.GROUP);
        return new MultiKey(rowKey.getDisciplineId(), rowKey.getRuleId(), groupTitle);
    }

    public String getPpsDownAlert()
    {
        EplDistributionCommonRowWrapper currentRow = getTableRow();
        if (currentRow.isPpsDownVisible())
        {
            PairKey firstPairKey = null;
            List<Long> rowIds = ppsDownKeys.get(getPpsDownKey(currentRow));

            boolean needAlert = false;
            for (Long rowId : rowIds)
            {
                if (needAlert) continue;
                EplDistributionCommonRowWrapper row = rowMap.get(rowId);
                PairKey<EplPlannedPps, Boolean> pairKey = PairKey.create(row.getPps(), row.isOvertime());

                if (null == firstPairKey)
                    firstPairKey = pairKey;
                else if (!row.getChildren().isEmpty() || (null != row.getPps() && !firstPairKey.equals(pairKey)))
                {
                    needAlert = true;
                }
            }
            if (needAlert)
                return "В строках ниже будут изменены ППС и признак «Сверх ставки». Продолжить?";
        }
        return "";
    }

    private void prepareDistributionCurrent(Map<MultiKey, EplDistributionCommonRowWrapper> rowByKeyMap)
    {
        // загружаем текущее распределение времени
        for (EplPlannedPpsTimeItem ppsTimeItem : new DQLSimple<>(EplPlannedPpsTimeItem.class).where(EplPlannedPpsTimeItem.pps().orgUnitSummary(), getSummary()).list())
        {
            EplTimeItem timeItem = ppsTimeItem.getTimeItem();
            EplTimeRuleEduLoad rule = timeItem.getTimeRule();
            String groupingCode = rule.getGrouping().getCode();
            EppRegistryElementPart discipline = null;
            EplEduGroup eduGroup = null;

            EplDistributionCommonRowWrapper rowWrapper;
            if (timeItem instanceof EplEduGroupTimeItem)
            {
                EplEduGroupTimeItem eduGroupTimeItem = (EplEduGroupTimeItem) timeItem;
                if (null == eduGroupTimeItem.getEduGroup()) continue;

                discipline = eduGroupTimeItem.getRegistryElementPart();
                eduGroup = eduGroupTimeItem.getEduGroup();
            }

            if (timeItem instanceof EplSimpleTimeItem)
                groupingCode = EplSimpleRuleRowWrapper.BY_SIMPLE_TIME_RULE;

            Object[] groupKeys = new Object[]{eduGroup == null ? null : eduGroup.getId()};
            MultiKey rowKey = EplDistributionCommonRowWrapper.getKey(discipline, rule, groupingCode, groupKeys);
            rowWrapper = rowByKeyMap.get(rowKey);

            // сохраняем в подстроки - вдруг распределили только часть времени, или по разным ППС
            EplDistributionSubRowWrapper subRow = rowWrapper.getChildren().stream()
                    .filter(ex -> ppsTimeItem.getPps().equals(ex.getPps()) && ObjectUtils.equals(ppsTimeItem.isOverTime(), ex.isOvertime())).findFirst()
                    .orElseGet(() -> {
                        rowIdCounter.decrement();
                        return new EplDistributionSubRowWrapper(rowIdCounter.longValue(), rowWrapper);
                    });
            subRow.addTime(timeItem.getTimeRule().getCategory(), ppsTimeItem.getTimeAmountAsLong());
            subRow.setPps(ppsTimeItem.getPps());
            subRow.setOvertime(ppsTimeItem.isOverTime());
            if (!rowWrapper.getChildren().contains(subRow)) {
                rowWrapper.getChildren().add(subRow);
            }
        }

        // теперь удалим лишние подстроки
        for (EplDistributionCommonRowWrapper r : tableRows)
        {
            // если подстрока единственная, и в нее попало все время - просто удаляем подстроку и переносим данные ППС и overtime в строку
            if (r.getChildren().size() == 1 && r.timeSum == r.getChildren().get(0).timeSum)
            {
                EplDistributionSubRowWrapper s = r.getChildren().get(0);
                r.setPps(s.getPps());
                r.setOvertime(s.isOvertime());
                r.getChildren().clear();
            }
            // и обновим проверочное время
            r.childrenTimeMap.clear();
            r.childrenTimeSum = 0;
            for (EplDistributionSubRowWrapper s : r.getChildren())
            {
                s.updateSum();
                rowMap.put(s.getId(), s);
            }
        }
    }

    private void refreshPpsTimeStats()
    {
        Map<EplPlannedPps, Double[]> timeData = Maps.newHashMap();
        Consumer<EplDistributionCommonRowWrapper> accountFor = row -> {
            if (row.getPps() != null)
            {
                double timeAmount = UniEppLoadUtils.wrap(row.timeSum);
                Double[] time = timeData.computeIfAbsent(row.getPps(), pps -> new Double[]{0d, 0d});
                time[row.isOvertime() ? 1 : 0] += timeAmount;
            }
        };
        getTableRows().forEach(row -> {
            if (row.isHasChildren()) {
                row.getChildren().forEach(accountFor);
            } else {
                accountFor.accept(row);
            }
        });

        ppsTimeStats = new HashMap<>();
        final Map<PostBoundedWithQGandQL, EplSettingsPpsLoad> postMap = EplSettingsPpsLoadManager.instance().dao().getPostMap(getSummary().getId());
        timeData.forEach((pps, time) -> {
            final EplSettingsPpsLoad settings = postMap.containsKey(pps.getPost()) ? postMap.get(pps.getPost()) : postMap.get(null);
            ppsTimeStats.put(pps.getId(), new PpsTimeStatsUtil.ResultRow(settings, time[0], time[1], pps));
        });
    }

    private boolean updateDistribution()
    {
        // Нельзя сохранять подстроку без ППС или с нулевой суммой часов —
        // выводить сообщение «Необходимо распределить нагрузку на ППС.»
        // и подкрашивать поля для ввода в колонках «Часы» и селект «ППС» красным.
        for (EplDistributionCommonRowWrapper r : tableRows) {
            for (EplDistributionSubRowWrapper s : r.getChildren()) {
                if (s.getPps() == null || s.timeSum == 0) {
                    _uiConfig.getUserContext().getErrorCollector().add("Необходимо распределить нагрузку на ППС.", "ppsField." + s.getId());
                    onChangeFilterParams();
                    return false;
                }
            }
        }

        // проверяем уникальность выбора ППС в рамках строки и категории времени
        // «Часы нагрузки для одного ППС в рамках одной категории могут быть указаны только в одной подстроке распределения.»
        // и подкрашивать поля для ввода в колонках «Часы» и селект «ППС» красным.
        for (EplDistributionCommonRowWrapper r : tableRows) {
            Set<MultiKey> check = new HashSet<>();
            for (EplDistributionSubRowWrapper s : r.getChildren()) {
                for (Map.Entry<Long, Long> e : s.getDistributionTimeMap().entrySet()) {
                    if (e.getValue() != null && e.getValue() > 0 && !check.add(new MultiKey(e.getKey(), s.getPps().getId()))) {
                        _uiConfig.getUserContext().getErrorCollector().add("Часы нагрузки для одного ППС в рамках одной категории могут быть указаны только в одной подстроке распределения.", "ppsField." + s.getId());
                        onChangeFilterParams();
                        return false;
                    }
                }
            }
        }

        List<IEplPlannedPpsDao.ITimeData> timeData = new ArrayList<>();
        EntityComparator<EplTimeItem> timeItemComparator = new EntityComparator<>(new EntityOrder(EplTimeItem.timeAmountAsLong().s(), OrderDirection.desc));


        for (EplDistributionCommonRowWrapper row : tableRows) {
            if (!row.isHasChildren())
            {
                row.getTimeItemMap().values().stream()
                        .filter(s -> row.getPps() != null)
                        .forEach(s -> timeData.addAll(
                                s.stream()
                                        .map(ti -> new TimeData(row.getPps(), ti, ti.getTimeAmountAsLong(), row.isOvertime()))
                                        .collect(Collectors.toList())
                        ));
                continue;
            }
            for (EplDistributionSubRowWrapper s : row.getChildren()) {
                for (Map.Entry<Long, List<EplTimeItem>> e : row.getTimeItemMap().entrySet()) {
                    List<EplTimeItem> timeItems = e.getValue();
                    Collections.sort(timeItems, timeItemComparator);
                    Long time = s.getDistributionTimeMap().get(e.getKey());
                    if (time == null || time == 0L) continue;
                    double timeSum = timeItems.stream().mapToLong(EplTimeItemGen::getTimeAmountAsLong).sum();
                    long distrSum = 0L;
                    for (int i = 0; i < timeItems.size(); i++) {
                        EplTimeItem ti = timeItems.get(i);
                        long itemTime;
                        if (i == timeItems.size() - 1) {
                            itemTime = time - distrSum;
                        } else {
                            itemTime = Math.round(time * ti.getTimeAmountAsLong() / timeSum);
                            distrSum += itemTime;
                        }
                        timeData.add(new TimeData(s.getPps(), ti, itemTime, s.isOvertime()));
                    }
                }
            }
            row.getChildren().sort(Comparator.<EplDistributionSubRowWrapper>naturalOrder());
        }
        EplPlannedPpsManager.instance().dao().doUpdateTimeDistribution(getSummary(), timeData);

        return true;
    }

    // getters and setters

    public EntityHolder<EplOrgUnitSummary> getHolder()
    {
        return holder;
    }

    public int getHeaderRowCount()
    {
        return getCategoryLevelCount() + 1;
    }

    public List<EplCategoryWrapper> getCurrentSubcategories()
    {
        return _currentSubcategories;
    }

    public void setCurrentSubcategories(List<EplCategoryWrapper> currentSubcategories)
    {
        _currentSubcategories = currentSubcategories;
    }

    public int getCategoryRowCount()
    {
        return getCategory().getRowCount(getCategoryLevelCount());
    }

    public int getCategoryLevelCount()
    {
        return getCategoryDataWrapper().getCategoryLevelCount();
    }

    public EplCategoryWrapper getCategory()
    {
        return _category;
    }

    public List<EplDistributionCommonRowWrapper> getTableRows()
    {
        return tableRows;
    }

    public boolean isHasTimeData()
    {
        return _hasTimeData;
    }

    public void setHasTimeData(boolean hasTimeData)
    {
        _hasTimeData = hasTimeData;
    }

    public List<EplDistributionCommonRowWrapper.TitleColumn> getTitleColumnList()
    {
        return titleColumnList;
    }

    public EplDistributionCommonRowWrapper.TitleColumn getTitleColumn()
    {
        return titleColumn;
    }

    public void setTitleColumn(EplDistributionCommonRowWrapper.TitleColumn titleColumn)
    {
        this.titleColumn = titleColumn;
    }

    public EplDistributionCommonRowWrapper getTableRow()
    {
        return tableRow;
    }

    public void setTableRow(EplDistributionCommonRowWrapper tableRow)
    {
        this.tableRow = tableRow;
    }

    public ISelectModel getPpsModel()
    {
        return ppsModel;
    }

    public EplDistributionSubRowWrapper getTableSubRow()
    {
        return tableSubRow;
    }

    public void setTableSubRow(EplDistributionSubRowWrapper tableSubRow)
    {
        this.tableSubRow = tableSubRow;
    }

    public boolean isViewMode()
    {
        return viewMode;
    }

    public void setViewMode(boolean viewMode)
    {
        this.viewMode = viewMode;
    }

    private static class TimeData implements IEplPlannedPpsDao.ITimeData {

        private EplTimeItem timeItem;
        private EplPlannedPps pps;
        private boolean overtime;
        private long timeAmountAsLong;

        public TimeData(EplPlannedPps pps, EplTimeItem timeItem, long timeAmountAsLong, boolean overtime)
        {
            this.overtime = overtime;
            this.pps = pps;
            this.timeAmountAsLong = timeAmountAsLong;
            this.timeItem = timeItem;
        }

        @Override
        public boolean overtime()
        {
            return overtime;
        }

        @Override
        public long timeAmountAsLong()
        {
            return timeAmountAsLong;
        }

        @Override
        public EplTimeItem timeItem() {
            return timeItem;
        }

        @Override
        public EplPlannedPps pps() {
            return pps;
        }
    }
}
