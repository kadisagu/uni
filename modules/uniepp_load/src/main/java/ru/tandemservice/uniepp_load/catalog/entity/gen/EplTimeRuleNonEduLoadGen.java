package ru.tandemservice.uniepp_load.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryNonEduLoad;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRule;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleNonEduLoad;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Норма времени внеучебной нагрузки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplTimeRuleNonEduLoadGen extends EplTimeRule
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleNonEduLoad";
    public static final String ENTITY_NAME = "eplTimeRuleNonEduLoad";
    public static final int VERSION_HASH = 49222845;
    private static IEntityMeta ENTITY_META;

    public static final String L_CATEGORY = "category";
    public static final String P_COEFFICIENT = "coefficient";
    public static final String P_PARAMETER_NEEDED = "parameterNeeded";
    public static final String P_PARAMETER_NAME = "parameterName";

    private EplTimeCategoryNonEduLoad _category;     // Категория
    private double _coefficient;     // Коэффициент
    private boolean _parameterNeeded;     // Необходим ввод базового параметра
    private String _parameterName;     // Наименование базового параметра

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Категория. Свойство не может быть null.
     */
    @NotNull
    public EplTimeCategoryNonEduLoad getCategory()
    {
        return _category;
    }

    /**
     * @param category Категория. Свойство не может быть null.
     */
    public void setCategory(EplTimeCategoryNonEduLoad category)
    {
        dirty(_category, category);
        _category = category;
    }

    /**
     * @return Коэффициент. Свойство не может быть null.
     */
    @NotNull
    public double getCoefficient()
    {
        return _coefficient;
    }

    /**
     * @param coefficient Коэффициент. Свойство не может быть null.
     */
    public void setCoefficient(double coefficient)
    {
        dirty(_coefficient, coefficient);
        _coefficient = coefficient;
    }

    /**
     * @return Необходим ввод базового параметра. Свойство не может быть null.
     */
    @NotNull
    public boolean isParameterNeeded()
    {
        return _parameterNeeded;
    }

    /**
     * @param parameterNeeded Необходим ввод базового параметра. Свойство не может быть null.
     */
    public void setParameterNeeded(boolean parameterNeeded)
    {
        dirty(_parameterNeeded, parameterNeeded);
        _parameterNeeded = parameterNeeded;
    }

    /**
     * @return Наименование базового параметра.
     */
    @Length(max=255)
    public String getParameterName()
    {
        return _parameterName;
    }

    /**
     * @param parameterName Наименование базового параметра.
     */
    public void setParameterName(String parameterName)
    {
        dirty(_parameterName, parameterName);
        _parameterName = parameterName;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EplTimeRuleNonEduLoadGen)
        {
            setCategory(((EplTimeRuleNonEduLoad)another).getCategory());
            setCoefficient(((EplTimeRuleNonEduLoad)another).getCoefficient());
            setParameterNeeded(((EplTimeRuleNonEduLoad)another).isParameterNeeded());
            setParameterName(((EplTimeRuleNonEduLoad)another).getParameterName());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplTimeRuleNonEduLoadGen> extends EplTimeRule.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplTimeRuleNonEduLoad.class;
        }

        public T newInstance()
        {
            return (T) new EplTimeRuleNonEduLoad();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "category":
                    return obj.getCategory();
                case "coefficient":
                    return obj.getCoefficient();
                case "parameterNeeded":
                    return obj.isParameterNeeded();
                case "parameterName":
                    return obj.getParameterName();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "category":
                    obj.setCategory((EplTimeCategoryNonEduLoad) value);
                    return;
                case "coefficient":
                    obj.setCoefficient((Double) value);
                    return;
                case "parameterNeeded":
                    obj.setParameterNeeded((Boolean) value);
                    return;
                case "parameterName":
                    obj.setParameterName((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "category":
                        return true;
                case "coefficient":
                        return true;
                case "parameterNeeded":
                        return true;
                case "parameterName":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "category":
                    return true;
                case "coefficient":
                    return true;
                case "parameterNeeded":
                    return true;
                case "parameterName":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "category":
                    return EplTimeCategoryNonEduLoad.class;
                case "coefficient":
                    return Double.class;
                case "parameterNeeded":
                    return Boolean.class;
                case "parameterName":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplTimeRuleNonEduLoad> _dslPath = new Path<EplTimeRuleNonEduLoad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplTimeRuleNonEduLoad");
    }
            

    /**
     * @return Категория. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleNonEduLoad#getCategory()
     */
    public static EplTimeCategoryNonEduLoad.Path<EplTimeCategoryNonEduLoad> category()
    {
        return _dslPath.category();
    }

    /**
     * @return Коэффициент. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleNonEduLoad#getCoefficient()
     */
    public static PropertyPath<Double> coefficient()
    {
        return _dslPath.coefficient();
    }

    /**
     * @return Необходим ввод базового параметра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleNonEduLoad#isParameterNeeded()
     */
    public static PropertyPath<Boolean> parameterNeeded()
    {
        return _dslPath.parameterNeeded();
    }

    /**
     * @return Наименование базового параметра.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleNonEduLoad#getParameterName()
     */
    public static PropertyPath<String> parameterName()
    {
        return _dslPath.parameterName();
    }

    public static class Path<E extends EplTimeRuleNonEduLoad> extends EplTimeRule.Path<E>
    {
        private EplTimeCategoryNonEduLoad.Path<EplTimeCategoryNonEduLoad> _category;
        private PropertyPath<Double> _coefficient;
        private PropertyPath<Boolean> _parameterNeeded;
        private PropertyPath<String> _parameterName;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Категория. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleNonEduLoad#getCategory()
     */
        public EplTimeCategoryNonEduLoad.Path<EplTimeCategoryNonEduLoad> category()
        {
            if(_category == null )
                _category = new EplTimeCategoryNonEduLoad.Path<EplTimeCategoryNonEduLoad>(L_CATEGORY, this);
            return _category;
        }

    /**
     * @return Коэффициент. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleNonEduLoad#getCoefficient()
     */
        public PropertyPath<Double> coefficient()
        {
            if(_coefficient == null )
                _coefficient = new PropertyPath<Double>(EplTimeRuleNonEduLoadGen.P_COEFFICIENT, this);
            return _coefficient;
        }

    /**
     * @return Необходим ввод базового параметра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleNonEduLoad#isParameterNeeded()
     */
        public PropertyPath<Boolean> parameterNeeded()
        {
            if(_parameterNeeded == null )
                _parameterNeeded = new PropertyPath<Boolean>(EplTimeRuleNonEduLoadGen.P_PARAMETER_NEEDED, this);
            return _parameterNeeded;
        }

    /**
     * @return Наименование базового параметра.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleNonEduLoad#getParameterName()
     */
        public PropertyPath<String> parameterName()
        {
            if(_parameterName == null )
                _parameterName = new PropertyPath<String>(EplTimeRuleNonEduLoadGen.P_PARAMETER_NAME, this);
            return _parameterName;
        }

        public Class getEntityClass()
        {
            return EplTimeRuleNonEduLoad.class;
        }

        public String getEntityName()
        {
            return "eplTimeRuleNonEduLoad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
