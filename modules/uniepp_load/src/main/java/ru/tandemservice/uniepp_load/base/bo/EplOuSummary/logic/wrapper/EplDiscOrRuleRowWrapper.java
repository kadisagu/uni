/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroup;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Враппер строки для расчитанных часов План. потока
 *
 * @author Alexey Lopatin
 * @since 04.04.2016
 */
public class EplDiscOrRuleRowWrapper extends EplCommonRowWrapper
{
    private String _educationOrgUnitTitles;
    private List<EplEduGroupTimeItem> _eduGroupTimeItemList;
    private Set<EplEduGroup> _eplGroups;
    private EplOrgUnitSummary _transferTo;

    // <<categoryId, eduGroups>
    private Map<Long, Set<Long>> _groups;
    private Map<Long, Set<Long>> _transferGroups;

    public EplDiscOrRuleRowWrapper(Long id, EppRegistryElementPart discipline, EplTimeRuleEduLoad rule, EplCommonRowWrapper.DisciplineOrRulePairKey key)
    {
        super(id, discipline, rule, key);
    }

    public void init()
    {
        _eduGroupTimeItemList = Lists.newArrayList();
        _groups = SafeMap.get(HashSet.class);
        _transferGroups = SafeMap.get(HashSet.class);
        _eplGroups = Sets.newHashSet();
    }

    public List<EplEduGroupTimeItem> getEduGroupTimeItemList() { return _eduGroupTimeItemList; }
    public Map<Long, Set<Long>> getGroups() { return _groups; }
    public Map<Long, Set<Long>> getTransferGroups() { return _transferGroups; }
    public Set<EplEduGroup> getEplGroups() { return _eplGroups; }
    public String getEducationOrgUnitTitles() { return _educationOrgUnitTitles; }
    public void setEducationOrgUnitTitles(String educationOrgUnitTitles) { _educationOrgUnitTitles = educationOrgUnitTitles; }
    public EplOrgUnitSummary getTransferTo() { return _transferTo; }
    public void setTransferTo(EplOrgUnitSummary transferTo) { _transferTo = transferTo; }
}
