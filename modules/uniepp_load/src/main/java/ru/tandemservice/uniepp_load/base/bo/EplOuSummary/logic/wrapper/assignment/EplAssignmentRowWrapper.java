/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.assignment;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;

import java.util.List;
import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 09.08.2016
 */
public class EplAssignmentRowWrapper
{
    private String _title;
    private String _eouTitles;
    private String _courses;
    private String _groupYearPartTitles;
    private Integer _flowCount;
    private Integer _groupCount;
    private Integer _subGroupCount;
    private Integer _studentCount;
    private String _eplGroups;
    private Map<EplTimeCategoryEduLoad, Double> _timeByCategoryMap = Maps.newLinkedHashMap();

    private List<EplAssignmentRowWrapper> _children = Lists.newArrayList();

    private boolean _child;
    private boolean _total;

    public EplAssignmentRowWrapper(String title)
    {
        _title = title;
    }

    public EplAssignmentRowWrapper(String title, boolean child,  boolean total)
    {
        _title = title;
        _child = child;
        _total = total;
    }

    public String getTitle() { return _title; }
    public String getEouTitles() { return _eouTitles; }
    public void setEouTitles(String eouTitles) { _eouTitles = eouTitles; }
    public String getCourses() { return _courses; }
    public void setCourses(String courses) { _courses = courses; }
    public String getGroupYearPartTitles() { return _groupYearPartTitles; }
    public void setGroupYearPartTitles(String groupYearPartTitles) { _groupYearPartTitles = groupYearPartTitles; }
    public Integer getFlowCount() { return _flowCount; }
    public void setFlowCount(int flowCount) { _flowCount = flowCount; }
    public Integer getGroupCount() { return _groupCount; }
    public void setGroupCount(int groupCount) { _groupCount = groupCount; }
    public Integer getSubGroupCount() { return _subGroupCount; }
    public void setSubGroupCount(int subGroupCount) { _subGroupCount = subGroupCount; }
    public Integer getStudentCount() { return _studentCount; }
    public void setStudentCount(int studentCount) { _studentCount = studentCount; }
    public String getEplGroups() { return _eplGroups; }
    public void setEplGroups(String eplGroups) { _eplGroups = eplGroups; }
    public Map<EplTimeCategoryEduLoad, Double> getTimeByCategoryMap() { return _timeByCategoryMap; }
    public Double getTimeByCategorySum() { return getTimeByCategoryMap().values().stream().mapToDouble(item -> item).sum(); }
    public List<EplAssignmentRowWrapper> getChildren() { return _children; }
    public boolean isChild() { return _child; }
    public boolean isTotal() { return _total; }
}
