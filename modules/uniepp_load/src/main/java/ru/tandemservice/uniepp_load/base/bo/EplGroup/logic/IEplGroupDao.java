/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplGroup.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 3/3/15
 */
public interface IEplGroupDao extends INeedPersistenceSupport
{
    List<EplGroupWrapper> getEplGroupWrappers(Long studentSummaryId, Map<String, Object> filtersMap);

    void doUpdateEplGroups(Long studentSummaryId, List<EplGroupWrapper> wrappers);
}
