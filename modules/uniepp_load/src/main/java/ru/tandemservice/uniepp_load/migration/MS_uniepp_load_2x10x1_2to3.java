/* $Id$ */
package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import ru.tandemservice.uni.dao.UniScriptDao;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript;

/**
 * @author Alexey Lopatin
 * @since 07.06.2016
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_load_2x10x1_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.10.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        String defaultScriptText = UniScriptDao.getScriptText("uniepp_load/" + EplTimeRuleEduGroupScript.DEFAULT_SCRIPT_PATH);

        tool.executeUpdate("update epl_time_rule_edugrp_base_t set userscript_p = ? where defaultscriptpath_p is null and userscript_p is null", defaultScriptText);
        tool.executeUpdate("update epl_time_rule_edugrp_base_t set defaultscriptpath_p = ? where defaultscriptpath_p is null", EplTimeRuleEduGroupScript.DEFAULT_SCRIPT_PATH);
    }
}
