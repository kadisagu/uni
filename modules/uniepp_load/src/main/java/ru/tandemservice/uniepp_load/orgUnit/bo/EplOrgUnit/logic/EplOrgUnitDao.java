/* $Id:$ */
package ru.tandemservice.uniepp_load.orgUnit.bo.EplOrgUnit.logic;

import org.tandemframework.hibsupport.transaction.DaoCacheFacade;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * @author oleyba
 * @since 12/6/14
 */
public class EplOrgUnitDao extends UniBaseDao implements IEplOrgUnitDao
{
    private final DaoCacheFacade.CacheEntryDefinition<Long, Boolean> CACHE_IS_ORG_UNIT_FORMING = new DaoCacheFacade.CacheEntryDefinition<Long, Boolean>("EplOrgUnitDao.forming", 128, false) {
        @Override public void fill(final Map<Long, Boolean> cache, final Collection<Long> ids) {

            for (final Long id: ids) {
                cache.put(id, this.isOrgUnitFormingOrEnrOrgUnit(id));
            }
        }

        private Boolean isOrgUnitFormingOrEnrOrgUnit(Long id)
        {
            return existsEntity(
                OrgUnitToKindRelation.class,
                OrgUnitToKindRelation.L_ORG_UNIT, id,
                OrgUnitToKindRelation.orgUnitKind().code().s(), Arrays.asList(UniDefines.CATALOG_ORGUNIT_KIND_FORMING)
            );
        }
    };

    private final DaoCacheFacade.CacheEntryDefinition<Long, Boolean> CACHE_IS_ORG_UNIT_OWNER = new DaoCacheFacade.CacheEntryDefinition<Long, Boolean>("EplOrgUnitDao.forming", 128, false) {
        @Override public void fill(final Map<Long, Boolean> cache, final Collection<Long> ids) {
            for (final Long id: ids) {
                cache.put(id, this.isOrgUnitFormingOrEnrOrgUnit(id));
            }
        }

        private Boolean isOrgUnitFormingOrEnrOrgUnit(Long id)
        {
            return existsEntity(EppTutorOrgUnit.class, EppTutorOrgUnit.L_ORG_UNIT, id);
        }
    };

    @Override
    public boolean isOrgUnitForming(OrgUnit orgUnit)
    {
        return DaoCacheFacade.getEntry(CACHE_IS_ORG_UNIT_FORMING).getRecords(Collections.singleton(orgUnit.getId())).get(orgUnit.getId());
    }

    @Override
    public boolean isOrgUnitOwner(OrgUnit orgUnit)
    {
        return DaoCacheFacade.getEntry(CACHE_IS_ORG_UNIT_OWNER).getRecords(Collections.singleton(orgUnit.getId())).get(orgUnit.getId());

    }
}