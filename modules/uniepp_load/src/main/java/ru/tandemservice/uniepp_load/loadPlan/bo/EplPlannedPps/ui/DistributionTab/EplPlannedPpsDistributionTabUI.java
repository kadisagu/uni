/* $Id$ */
package ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.DistributionTab;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.EplOuSummaryManager;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplTimeItem;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeCategoryEduLoad;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.EplPlannedPpsManager;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.DistributionEditNew.EplPlannedPpsDistributionEditNew;
import ru.tandemservice.uniepp_load.loadPlan.bo.EplPlannedPps.ui.DistributionEditNew.EplPlannedPpsDistributionEditNewUI;
import ru.tandemservice.uniepp_load.settings.bo.EplSettingsPpsLoad.EplSettingsPpsLoadManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 2/2/15
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required = true)
})
public class EplPlannedPpsDistributionTabUI extends UIPresenter
{
    private EntityHolder<EplOrgUnitSummary> holder = new EntityHolder<>();

    private boolean _hasIncorrectTime;
    private boolean _hasTimeData;

    private double _summaryHours;
    private double _summaryDistribHours;
    private double _distributionHours;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        _summaryHours = EplPlannedPpsManager.instance().dao().getSummaryHours(getSummaryId());
        _summaryDistribHours = EplPlannedPpsManager.instance().dao().getSummaryDistribHours(getSummaryId());
        _distributionHours = EplPlannedPpsManager.instance().dao().getDistributionHours(getSummaryId());

        List<EplTimeCategoryEduLoad> usedCategories = DataAccessServices.dao().getList(new DQLSelectBuilder()
                .fromEntity(EplTimeCategoryEduLoad.class, "c").column("c")
                .where(exists(new DQLSelectBuilder()
                    .fromEntity(EplTimeItem.class, "i")
                    .where(eq(property("i", EplTimeItem.summary()), value(getSummary())))
                    .where(eq(property("i", EplTimeItem.timeRule().category()), property("c")))
                    .buildQuery()))
        );

        setHasIncorrectTime(EplOuSummaryManager.instance().dao().hasIncorrectTime(getSummary()));
        setHasTimeData(!CollectionUtils.isEmpty(usedCategories));
    }

    public void onClickEdit()
    {
        _uiActivation.asDesktopRoot(EplPlannedPpsDistributionEditNew.class).parameter(PUBLISHER_ID, getSummaryId()).activate();
    }

    // presenter

    public Map getInnerViewComponentParams() {
        return ParametersMap.createWith(PUBLISHER_ID, getSummaryId()).add(EplPlannedPpsDistributionEditNewUI.VIEW_MODE, true);
    }

    // getters and setters

    public EntityHolder<EplOrgUnitSummary> getHolder()
    {
        return holder;
    }

    public EplOrgUnitSummary getSummary()
    {
        return getHolder().getValue();
    }

    public Long getSummaryId()
    {
        return getSummary().getId();
    }

    public double getSummaryHours()
    {
        return _summaryHours;
    }

    public void setSummaryHours(double summaryHours)
    {
        _summaryHours = summaryHours;
    }

    public double getDistributionHours()
    {
        return _distributionHours;
    }

    public void setDistributionHours(double distributionHours)
    {
        _distributionHours = distributionHours;
    }

    public boolean isHasIncorrectTime()
    {
        return _hasIncorrectTime;
    }

    public void setHasIncorrectTime(boolean hasIncorrectTime)
    {
        _hasIncorrectTime = hasIncorrectTime;
    }

    public boolean isHasTimeData()
    {
        return _hasTimeData;
    }

    public void setHasTimeData(boolean hasTimeData)
    {
        _hasTimeData = hasTimeData;
    }

    public double getSummaryDistribHours()
    {
        return _summaryDistribHours;
    }

    public void setSummaryDistribHours(double summaryDistribHours)
    {
        _summaryDistribHours = summaryDistribHours;
    }

    public boolean isEditMode()
    {
        return _hasTimeData && getSummary().getState().getCode().equals(EppState.STATE_FORMATIVE);
    }

    public boolean isNotFoundSettings()
    {
        return EplSettingsPpsLoadManager.instance().dao().isNotFoundSettingsPpsLoad(getSummaryId());
    }

    public Map<String, Object> getOuSummaryLinkParams()
    {
        final HashMap<String, Object> params = new HashMap<>();
        params.put(UIPresenter.PUBLISHER_ID, getSummary().getId());
        params.put("selectedTab", "maxLoadTab");
        return params;
    }

    public Map<String, Object> getSummaryLinkParams()
    {
        final HashMap<String, Object> params = new HashMap<>();
        params.put(UIPresenter.PUBLISHER_ID, getSummary().getStudentSummary().getId());
        params.put("selectedTab", "maxLoadTab");
        return params;
    }
}