/* $Id$ */
package ru.tandemservice.uniepp_load.base.bo.EplEduGroup;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

import ru.tandemservice.uniepp_load.base.bo.EplEduGroup.logic.EplEduGroupDao;
import ru.tandemservice.uniepp_load.base.bo.EplEduGroup.logic.IEplEduGroupDao;

/**
 * @author oleyba
 * @since 10/25/11
 */
@Configuration
public class EplEduGroupManager extends BusinessObjectManager
{
    public static EplEduGroupManager instance() {
        return instance(EplEduGroupManager.class);
    }

    @Bean
    public IEplEduGroupDao dao() {
        return new EplEduGroupDao();
    }
}
