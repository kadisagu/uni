package ru.tandemservice.uniepp_load.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupFormula;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduLoad;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Норма времени учебной нагрузки (формула)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EplTimeRuleEduGroupFormulaGen extends EplTimeRuleEduLoad
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupFormula";
    public static final String ENTITY_NAME = "eplTimeRuleEduGroupFormula";
    public static final int VERSION_HASH = 277843417;
    private static IEntityMeta ENTITY_META;

    public static final String P_COEFFICIENT = "coefficient";
    public static final String P_MULTIPLY_HOURS = "multiplyHours";
    public static final String P_MULTIPLY_STUDENTS = "multiplyStudents";
    public static final String L_GROUP_TYPE = "groupType";
    public static final String L_EPP_REGISTRY_STRUCTURE = "eppRegistryStructure";

    private double _coefficient;     // Коэффициент
    private boolean _multiplyHours;     // Умножать на часы
    private boolean _multiplyStudents;     // Умножать на число студентов
    private EppGroupType _groupType;     // Вид УГС
    private EppRegistryStructure _eppRegistryStructure;     // Вид мероприятия реестра

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Коэффициент. Свойство не может быть null.
     */
    @NotNull
    public double getCoefficient()
    {
        return _coefficient;
    }

    /**
     * @param coefficient Коэффициент. Свойство не может быть null.
     */
    public void setCoefficient(double coefficient)
    {
        dirty(_coefficient, coefficient);
        _coefficient = coefficient;
    }

    /**
     * @return Умножать на часы. Свойство не может быть null.
     */
    @NotNull
    public boolean isMultiplyHours()
    {
        return _multiplyHours;
    }

    /**
     * @param multiplyHours Умножать на часы. Свойство не может быть null.
     */
    public void setMultiplyHours(boolean multiplyHours)
    {
        dirty(_multiplyHours, multiplyHours);
        _multiplyHours = multiplyHours;
    }

    /**
     * @return Умножать на число студентов. Свойство не может быть null.
     */
    @NotNull
    public boolean isMultiplyStudents()
    {
        return _multiplyStudents;
    }

    /**
     * @param multiplyStudents Умножать на число студентов. Свойство не может быть null.
     */
    public void setMultiplyStudents(boolean multiplyStudents)
    {
        dirty(_multiplyStudents, multiplyStudents);
        _multiplyStudents = multiplyStudents;
    }

    /**
     * @return Вид УГС. Свойство не может быть null.
     */
    @NotNull
    public EppGroupType getGroupType()
    {
        return _groupType;
    }

    /**
     * @param groupType Вид УГС. Свойство не может быть null.
     */
    public void setGroupType(EppGroupType groupType)
    {
        dirty(_groupType, groupType);
        _groupType = groupType;
    }

    /**
     * @return Вид мероприятия реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryStructure getEppRegistryStructure()
    {
        return _eppRegistryStructure;
    }

    /**
     * @param eppRegistryStructure Вид мероприятия реестра. Свойство не может быть null.
     */
    public void setEppRegistryStructure(EppRegistryStructure eppRegistryStructure)
    {
        dirty(_eppRegistryStructure, eppRegistryStructure);
        _eppRegistryStructure = eppRegistryStructure;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EplTimeRuleEduGroupFormulaGen)
        {
            setCoefficient(((EplTimeRuleEduGroupFormula)another).getCoefficient());
            setMultiplyHours(((EplTimeRuleEduGroupFormula)another).isMultiplyHours());
            setMultiplyStudents(((EplTimeRuleEduGroupFormula)another).isMultiplyStudents());
            setGroupType(((EplTimeRuleEduGroupFormula)another).getGroupType());
            setEppRegistryStructure(((EplTimeRuleEduGroupFormula)another).getEppRegistryStructure());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EplTimeRuleEduGroupFormulaGen> extends EplTimeRuleEduLoad.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EplTimeRuleEduGroupFormula.class;
        }

        public T newInstance()
        {
            return (T) new EplTimeRuleEduGroupFormula();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "coefficient":
                    return obj.getCoefficient();
                case "multiplyHours":
                    return obj.isMultiplyHours();
                case "multiplyStudents":
                    return obj.isMultiplyStudents();
                case "groupType":
                    return obj.getGroupType();
                case "eppRegistryStructure":
                    return obj.getEppRegistryStructure();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "coefficient":
                    obj.setCoefficient((Double) value);
                    return;
                case "multiplyHours":
                    obj.setMultiplyHours((Boolean) value);
                    return;
                case "multiplyStudents":
                    obj.setMultiplyStudents((Boolean) value);
                    return;
                case "groupType":
                    obj.setGroupType((EppGroupType) value);
                    return;
                case "eppRegistryStructure":
                    obj.setEppRegistryStructure((EppRegistryStructure) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "coefficient":
                        return true;
                case "multiplyHours":
                        return true;
                case "multiplyStudents":
                        return true;
                case "groupType":
                        return true;
                case "eppRegistryStructure":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "coefficient":
                    return true;
                case "multiplyHours":
                    return true;
                case "multiplyStudents":
                    return true;
                case "groupType":
                    return true;
                case "eppRegistryStructure":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "coefficient":
                    return Double.class;
                case "multiplyHours":
                    return Boolean.class;
                case "multiplyStudents":
                    return Boolean.class;
                case "groupType":
                    return EppGroupType.class;
                case "eppRegistryStructure":
                    return EppRegistryStructure.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EplTimeRuleEduGroupFormula> _dslPath = new Path<EplTimeRuleEduGroupFormula>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EplTimeRuleEduGroupFormula");
    }
            

    /**
     * @return Коэффициент. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupFormula#getCoefficient()
     */
    public static PropertyPath<Double> coefficient()
    {
        return _dslPath.coefficient();
    }

    /**
     * @return Умножать на часы. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupFormula#isMultiplyHours()
     */
    public static PropertyPath<Boolean> multiplyHours()
    {
        return _dslPath.multiplyHours();
    }

    /**
     * @return Умножать на число студентов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupFormula#isMultiplyStudents()
     */
    public static PropertyPath<Boolean> multiplyStudents()
    {
        return _dslPath.multiplyStudents();
    }

    /**
     * @return Вид УГС. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupFormula#getGroupType()
     */
    public static EppGroupType.Path<EppGroupType> groupType()
    {
        return _dslPath.groupType();
    }

    /**
     * @return Вид мероприятия реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupFormula#getEppRegistryStructure()
     */
    public static EppRegistryStructure.Path<EppRegistryStructure> eppRegistryStructure()
    {
        return _dslPath.eppRegistryStructure();
    }

    public static class Path<E extends EplTimeRuleEduGroupFormula> extends EplTimeRuleEduLoad.Path<E>
    {
        private PropertyPath<Double> _coefficient;
        private PropertyPath<Boolean> _multiplyHours;
        private PropertyPath<Boolean> _multiplyStudents;
        private EppGroupType.Path<EppGroupType> _groupType;
        private EppRegistryStructure.Path<EppRegistryStructure> _eppRegistryStructure;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Коэффициент. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupFormula#getCoefficient()
     */
        public PropertyPath<Double> coefficient()
        {
            if(_coefficient == null )
                _coefficient = new PropertyPath<Double>(EplTimeRuleEduGroupFormulaGen.P_COEFFICIENT, this);
            return _coefficient;
        }

    /**
     * @return Умножать на часы. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupFormula#isMultiplyHours()
     */
        public PropertyPath<Boolean> multiplyHours()
        {
            if(_multiplyHours == null )
                _multiplyHours = new PropertyPath<Boolean>(EplTimeRuleEduGroupFormulaGen.P_MULTIPLY_HOURS, this);
            return _multiplyHours;
        }

    /**
     * @return Умножать на число студентов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupFormula#isMultiplyStudents()
     */
        public PropertyPath<Boolean> multiplyStudents()
        {
            if(_multiplyStudents == null )
                _multiplyStudents = new PropertyPath<Boolean>(EplTimeRuleEduGroupFormulaGen.P_MULTIPLY_STUDENTS, this);
            return _multiplyStudents;
        }

    /**
     * @return Вид УГС. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupFormula#getGroupType()
     */
        public EppGroupType.Path<EppGroupType> groupType()
        {
            if(_groupType == null )
                _groupType = new EppGroupType.Path<EppGroupType>(L_GROUP_TYPE, this);
            return _groupType;
        }

    /**
     * @return Вид мероприятия реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupFormula#getEppRegistryStructure()
     */
        public EppRegistryStructure.Path<EppRegistryStructure> eppRegistryStructure()
        {
            if(_eppRegistryStructure == null )
                _eppRegistryStructure = new EppRegistryStructure.Path<EppRegistryStructure>(L_EPP_REGISTRY_STRUCTURE, this);
            return _eppRegistryStructure;
        }

        public Class getEntityClass()
        {
            return EplTimeRuleEduGroupFormula.class;
        }

        public String getEntityName()
        {
            return "eplTimeRuleEduGroupFormula";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
