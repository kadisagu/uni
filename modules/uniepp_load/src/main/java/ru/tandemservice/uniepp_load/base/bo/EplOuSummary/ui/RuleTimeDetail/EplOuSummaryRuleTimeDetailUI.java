/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.RuleTimeDetail;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.EplOuSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplCommonRowWrapper;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.Tab.EplOuSummaryTabUI;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic.EplErrorFormatter;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplSimpleTimeItem;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleSimple;

import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 12/12/14
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "summary.id", required = true),
        @Bind(key = EplOuSummaryTabUI.PARAM_DISC_OR_RULE_KEY, binding = "discOrRuleKey", required = true)
})
public class EplOuSummaryRuleTimeDetailUI extends UIPresenter
{
    public static final String VIEW_AMOUNT = "viewAmount";

    private EplOrgUnitSummary summary = new EplOrgUnitSummary();
    private EplTimeRuleSimple _rule;
    private StaticListDataSource<DataWrapper> timeDS;
    private EplSimpleTimeItem _currentEditSimpleTimeItem;

    private EplCommonRowWrapper.DisciplineOrRulePairKey _discOrRuleKey;

    @Override
    public void onComponentRefresh()
    {
        _currentEditSimpleTimeItem = null;

        setSummary(IUniBaseDao.instance.get().get(EplOrgUnitSummary.class, getSummary().getId()));
        setRule(IUniBaseDao.instance.get().get(EplTimeRuleSimple.class, getDiscOrRuleKey().getRuleId()));

        setTimeDS(new StaticListDataSource<>());

        getTimeDS().addColumn(new SimpleColumn("Часы", VIEW_AMOUNT).setFormatter(RawFormatter.INSTANCE).setClickable(false).setOrderable(false));
        if (getRule().isParameterNeeded()) {
            getTimeDS().addColumn(new BlockColumn("parameter", getRule().getParameterName()));
        }
        getTimeDS().addColumn(new BlockColumn(EplSimpleTimeItem.P_COUNT_AS_CONTRACT, "По договору"));
        getTimeDS().addColumn(new BlockColumn(EplSimpleTimeItem.L_YEAR_PART, "Часть учебного года"));

        BlockColumn editBlockColumn = new BlockColumn("edit", "");
        editBlockColumn.setRequired(true).setWidth("1px").setHint("Редактировать");

        BlockColumn deleteBlockColumn = new BlockColumn("delete", "");
        deleteBlockColumn.setRequired(true).setWidth("1px").setHint("Удалить");

        getTimeDS().addColumn(editBlockColumn);
        getTimeDS().addColumn(deleteBlockColumn);

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EplSimpleTimeItem.class, "i").column("i")
            .where(eq(property("i", EplSimpleTimeItem.summary()), value(getSummary())))
            .where(eq(property("i", EplSimpleTimeItem.timeRule()), value(getRule())))
            .order(property("i", EplSimpleTimeItem.timeAmountAsLong().s()), OrderDirection.desc)
            ;
        List<EplSimpleTimeItem> timeItems = DataAccessServices.dao().<EplSimpleTimeItem>getList(dql);

        List<DataWrapper> resultList = Lists.newArrayList();
        for (EplSimpleTimeItem timeItem : timeItems)
        {
            DataWrapper wrapper = new DataWrapper(timeItem);
            wrapper.setProperty(VIEW_AMOUNT, EplErrorFormatter.errorDiff(timeItem));
            resultList.add(wrapper);
        }

        getTimeDS().setupRows(resultList);

        getTimeDS().setCountRow(getTimeDS().getRowList().size());
        getTimeDS().setMinCountRow(1);
    }

    public void onClickSaveRow()
    {
        EplOuSummaryManager.instance().dao().saveOrUpdateTimeItem(getCurrentEditSimpleTimeItem());
        _currentEditSimpleTimeItem = null;
        _uiSupport.setRefreshScheduled(true);
    }

    public void onClickCancelEdit()
    {
        _currentEditSimpleTimeItem = null;
        _uiSupport.setRefreshScheduled(true);
    }

    public void onClickEditItem()
    {
        _currentEditSimpleTimeItem = getTimeItemMap().get(getListenerParameterAsLong());
    }

    public void onChangeTimeItemContract()
    {
        EplSimpleTimeItem timeItem = getTimeItemMap().get(getListenerParameterAsLong());
        timeItem.setCountAsContract(!timeItem.isCountAsContract());
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        _uiSupport.setRefreshScheduled(true);
    }

    public EplSimpleTimeItem getCurrentSimpleTimeItem()
    {
        return (EplSimpleTimeItem) ((DataWrapper) getTimeDS().getCurrentValueEntity()).getWrapped();
    }

    public String getCurrentSimpleTimeItemParameter()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getCurrentSimpleTimeItem().getParameter());
    }

    public boolean isSimpleTimeItemInEditMode()
    {
        return null != getCurrentEditSimpleTimeItem() && getCurrentSimpleTimeItem().getId().equals(getCurrentEditSimpleTimeItem().getId());
    }

    public boolean isEditMode()
    {
        return null != getCurrentEditSimpleTimeItem();
    }

    private Map<Long, EplSimpleTimeItem> getTimeItemMap()
    {
        Map<Long, EplSimpleTimeItem> timeItemMap = Maps.newHashMap();
        for (DataWrapper wrapper : getTimeDS().getRowList())
        {
            EplSimpleTimeItem timeItem = wrapper.getWrapped();
            timeItemMap.put(timeItem.getId(), timeItem);
        }
        return timeItemMap;
    }

    // accessors

    public EplOrgUnitSummary getSummary()
    {
        return summary;
    }

    public void setSummary(EplOrgUnitSummary summary)
    {
        this.summary = summary;
    }

    public EplCommonRowWrapper.DisciplineOrRulePairKey getDiscOrRuleKey()
    {
        return _discOrRuleKey;
    }

    public void setDiscOrRuleKey(EplCommonRowWrapper.DisciplineOrRulePairKey discOrRuleKey)
    {
        _discOrRuleKey = discOrRuleKey;
    }

    public EplTimeRuleSimple getRule()
    {
        return _rule;
    }

    public void setRule(EplTimeRuleSimple rule)
    {
        _rule = rule;
    }

    public StaticListDataSource<DataWrapper> getTimeDS()
    {
        return timeDS;
    }

    public void setTimeDS(StaticListDataSource<DataWrapper> timeDS)
    {
        this.timeDS = timeDS;
    }

    public EplSimpleTimeItem getCurrentEditSimpleTimeItem()
    {
        return _currentEditSimpleTimeItem;
    }

    public void setCurrentEditSimpleTimeItem(EplSimpleTimeItem currentEditSimpleTimeItem)
    {
        _currentEditSimpleTimeItem = currentEditSimpleTimeItem;
    }
}