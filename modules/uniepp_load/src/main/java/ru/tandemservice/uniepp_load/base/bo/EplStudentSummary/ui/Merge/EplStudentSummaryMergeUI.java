/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Merge;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;

/**
 * @author oleyba
 * @since 17.08.15
 */
public class EplStudentSummaryMergeUI extends UIPresenter
{
    private EducationYear eduYear;
    private EplStudentSummary summaryBase;
    private EplStudentSummary summaryMerge;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EplStudentSummary.PARAM_EDU_YEAR, getEduYear());
        dataSource.put(EplStudentSummaryMerge.PARAM_SUMMARY, getSummaryBase());
    }

    public void onClickApply() {
        EplStudentSummaryManager.instance().dao().doMerge(getSummaryBase(), getSummaryMerge());
        deactivate();
    }

    // getters and setters

    public EducationYear getEduYear()
    {
        return eduYear;
    }

    public void setEduYear(EducationYear eduYear)
    {
        this.eduYear = eduYear;
    }

    public EplStudentSummary getSummaryBase()
    {
        return summaryBase;
    }

    public void setSummaryBase(EplStudentSummary summaryBase)
    {
        this.summaryBase = summaryBase;
    }

    public EplStudentSummary getSummaryMerge()
    {
        return summaryMerge;
    }

    public void setSummaryMerge(EplStudentSummary summaryMerge)
    {
        this.summaryMerge = summaryMerge;
    }
}