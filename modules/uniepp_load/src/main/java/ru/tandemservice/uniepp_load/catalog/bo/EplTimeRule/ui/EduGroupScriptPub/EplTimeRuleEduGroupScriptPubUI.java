/* $Id:$ */
package ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.ui.EduGroupScriptPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.commonbase.base.bo.Common.ui.ScriptEdit.CommonScriptEdit;
import org.tandemframework.shared.commonbase.base.bo.Common.ui.ScriptEdit.CommonScriptEditUI;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.DynamicItemPub.CatalogDynamicItemPubUI;
import ru.tandemservice.uniepp_load.catalog.bo.EplTimeRule.EplTimeRuleManager;
import ru.tandemservice.uniepp_load.catalog.entity.EplTimeRuleEduGroupScript;

/**
 * @author oleyba
 * @since 11/29/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id")
})
public class EplTimeRuleEduGroupScriptPubUI extends CatalogDynamicItemPubUI
{
    public void onEditScript()
    {
        _uiActivation.asDesktopRoot(CommonScriptEdit.class)
        .parameter(CommonScriptEditUI.PARAM_HOLDER, ((EplTimeRuleEduGroupScript) getCatalogItem()).getScriptHolder())
        .activate();
    }

    public void onRevertScript()
    {
        final EplTimeRuleEduGroupScript script = (EplTimeRuleEduGroupScript) getCatalogItem();
        script.setUserScript(null);
        EplTimeRuleManager.instance().timeRuleDao().doSaveUserScript(script);
    }

    @Override
    public String getAdditionalButtonsPageName()
    {
        return getClass().getPackage().getName() + ".AdditButtons";
    }
}