/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.EplStudentSummaryManager;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 12/3/14
 */
@Configuration
public class EplStudentSummaryAdd extends BusinessComponentManager
{
    public static final String DS_EDU_YEAR = "eduYearDS";
    public static final String DS_EPP_YEAR = "eppYearDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_EDU_YEAR, getName(), EducationYear.defaultSelectDSHandler(getName())))
                .addDataSource(selectDS(DS_EPP_YEAR, eppYearDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eppYearDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppYearEducationProcess.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                EducationYear currentYear = context.getNotNull(EplStudentSummaryAddUI.PARAM_EDU_YEAR);
                EppYearEducationProcess nextYear = EplStudentSummaryManager.instance().dao().getNextAfterCurrentYearEducationProcess(currentYear);

                dql.where(or(
                        eq(property(alias, EppYearEducationProcess.educationYear()), value(currentYear)),
                        nextYear == null ? null : eq(property(alias, "id"), value(nextYear.getId()))
                ));
            }
        }
                .order(EppYearEducationProcess.educationYear().intValue())
                .filter(EppYearEducationProcess.title());
    }
}