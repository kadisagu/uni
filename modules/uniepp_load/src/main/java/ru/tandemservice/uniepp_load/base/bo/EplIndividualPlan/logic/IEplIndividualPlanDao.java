package ru.tandemservice.uniepp_load.base.bo.EplIndividualPlan.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplIndividualPlan;
import ru.tandemservice.uniepp_load.base.entity.indPlan.EplTimeItemNonEduLoad;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;

import java.util.Collection;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 03.08.2016
 */
public interface IEplIndividualPlanDao extends INeedPersistenceSupport
{
    /**
     * Сохраняет рассчит. часы внеучебной нагрузки
     *
     * @param timeItem часы нагрузки
     */
    void saveOrUpdateTimeItem(EplTimeItemNonEduLoad timeItem);

    /**
     * Сохраняет расчеты для ИПП
     *
     * @param indPlan          ИПП
     * @param studentSummaries сводки
     */
    void saveOrUpdateOuSummaries(EplIndividualPlan indPlan, Collection<EplStudentSummary> studentSummaries);

    /**
     * Возвращает набор расчетов по ИПП
     *
     * @param indPlan ИПП
     * @return список расчетов ИПП
     */
    List<EplOrgUnitSummary> getOuSummaries(EplIndividualPlan indPlan);

    /**
     * Возвращает сумму рассчитанных часов на ППС учебной нагрузки ИПП
     *
     * @param indPlan ИПП
     * @return кол-во часов учебной нагрузки
     */
    long getPpsEntryEduLoad(EplIndividualPlan indPlan);

    /**
     * Возвращает сумму часов внеучебной нагрузки ИПП
     *
     * @param indPlan ИПП
     * @return кол-во часов внеучебной нагрузки
     */
    long getPpsEntryNonEduLoad(EplIndividualPlan indPlan);
}
