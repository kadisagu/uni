/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.ui.Refresh;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;

/**
 * @author oleyba
 * @since 19.08.15
 */
@Configuration
public class EplStudentSummaryRefresh extends BusinessComponentManager
{
    public static final String DS_EDU_YEAR = "eduYearDS";
    public static final String DS_EPP_YEAR = "eppYearDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_EDU_YEAR, getName(), EducationYear.defaultSelectDSHandler(getName())))
                .addDataSource(selectDS(DS_EPP_YEAR, eppYearDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eppYearDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppYearEducationProcess.class)
                .order(EppYearEducationProcess.educationYear().intValue())
                .filter(EppYearEducationProcess.title());
    }
}