package ru.tandemservice.uniepp_load.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_load_2x7x0_13to14 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		// создано обязательное свойство countByEduGroup
		{
			// создать колонку
			tool.createColumn("epl_student_wp2gt_slot_t", new DBColumn("countbyedugroup_p", DBType.INTEGER));
            tool.executeUpdate("update epl_student_wp2gt_slot_t set countbyedugroup_p=? where countbyedugroup_p is null", (Integer) 0);
			tool.setColumnNullable("epl_student_wp2gt_slot_t", "countbyedugroup_p", false);
		}

        tool.renameColumn("epl_student_wp_slot_t", "count_p", "countbywp_p");
   }
}