/* $Id:$ */
package ru.tandemservice.uniepp_load.base.bo.EplOuSummary.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.process.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.EplOuSummaryManager;
import ru.tandemservice.uniepp_load.base.bo.EplOuSummary.logic.wrapper.EplRecalculateSummaryKind;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;

/**
 * @author oleyba
 * @since 5/5/12
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "summary.id", required = true)
})
public class EplOuSummaryAddUI extends UIPresenter
{
    private EplStudentSummary summary = new EplStudentSummary();
    private OrgUnit orgUnit;

    @Override
    public void onComponentRefresh() {
        setSummary(IUniBaseDao.instance.get().get(EplStudentSummary.class, getSummary().getId()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
    }

    public void onClickApply() {
        ProcessState state = new ProcessState(ProcessDisplayMode.unknown);
        EplOrgUnitSummary summary = EplOuSummaryManager.instance().dao().createSummary(getOrgUnit(), getSummary(), state);
        EplOuSummaryManager.instance().dao().doRecalculateSummary(summary.getId(), EplRecalculateSummaryKind.FULL, state);
        deactivate();
    }

    // accessors

    public EplStudentSummary getSummary()
    {
        return summary;
    }

    public void setSummary(EplStudentSummary summary)
    {
        this.summary = summary;
    }

    public OrgUnit getOrgUnit()
    {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this.orgUnit = orgUnit;
    }
}

