/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.PlaceList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.util.DQLCanDeleteExpressionBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.codes.OrgUnitTypeCodes;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceCondition;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;
import ru.tandemservice.uniplaces.util.PlacesUtil;
import ru.tandemservice.uniplaces.util.UniplacesClassroomTypeSelectModel;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author agolubenko
 * @since Aug 23, 2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    protected static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("place");

    @Override
    public void prepare(final Model model)
    {
        model.setPurposeList(new LazySimpleSelectModel<>(PlacesUtil.sortByHierarchy(getList(UniplacesPlacePurpose.class))));
        model.setConditionList(getCatalogItemListOrderByCode(UniplacesPlaceCondition.class));

//        final Session session = getSession();
        model.setBuildingList(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(UniplacesBuilding.ENTITY_CLASS, "building");

                MQBuilder sub = new MQBuilder(UniplacesPlace.ENTITY_CLASS, "place");
                sub.add(MQExpression.eqProperty("place", UniplacesPlace.floor().unit().building().id().s(), "building", UniplacesBuilding.id().s()));

                builder.add(MQExpression.exists(sub));

                if (StringUtils.isNotBlank(filter))
                    builder.add(MQExpression.like("building", UniplacesBuilding.title().s(), CoreStringUtils.escapeLike(filter)));

                builder.addOrder("building", UniplacesUnit.title().s());
                return new ListResult<>(builder.<UniplacesUnit>getResultList(getSession(), 0, 100), builder.getResultCount(getSession()));
            }
        });
        model.setUnitList(new FullCheckSelectModel()
        {
            @Override
            public ListResult<UniplacesUnit> findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(UniplacesUnit.ENTITY_CLASS, "unit");
                if(model.getBuilding() != null)
                    builder.add(MQExpression.eq("unit", UniplacesUnit.building().s(), model.getBuilding()));

                MQBuilder sub = new MQBuilder(UniplacesPlace.ENTITY_CLASS, "place");
                sub.add(MQExpression.eqProperty("place", UniplacesPlace.floor().unit().id().s(), "unit", UniplacesUnit.id().s()));

                builder.add(MQExpression.exists(sub));

                if (StringUtils.isNotBlank(filter))
                    builder.add(MQExpression.like("unit", UniplacesUnit.title().s(), CoreStringUtils.escapeLike(filter)));

                builder.addOrder("unit", UniplacesUnit.title().s());
                return new ListResult<>(builder.<UniplacesUnit>getResultList(getSession(), 0, 100), builder.getResultCount(getSession()));
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {

                return ((UniplacesUnit)value).getTitleExtended();
            }
        });
        model.setFloorList(new FullCheckSelectModel()
        {
            @Override
            public ListResult<UniplacesFloor> findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(UniplacesFloor.ENTITY_CLASS, "floor");
                if(model.getBuilding() != null)
                    builder.add(MQExpression.eq("floor", UniplacesFloor.unit().building().s(), model.getBuilding()));
                if(model.getUnit() != null)
                    builder.add(MQExpression.eq("floor", UniplacesFloor.unit().s(), model.getUnit()));

                MQBuilder sub = new MQBuilder(UniplacesPlace.ENTITY_CLASS, "place");
                sub.add(MQExpression.eqProperty("place", UniplacesPlace.floor().id().s(), "floor", UniplacesFloor.id().s()));

                builder.add(MQExpression.exists(sub));

                if (StringUtils.isNotBlank(filter))
                    builder.add(MQExpression.like("floor", UniplacesFloor.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder("floor", UniplacesFloor.title().s());
                return new ListResult<>(builder.<UniplacesFloor>getResultList(getSession(), 0, 100), builder.getResultCount(getSession()));
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((UniplacesFloor) value).getTitleExtended();
            }
        });
        model.setPlaceList(new FullCheckSelectModel()
        {
            @Override
            public ListResult<UniplacesPlace> findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(UniplacesPlace.ENTITY_CLASS, "place");
                if(model.getBuilding() != null)
                    builder.add(MQExpression.eq("place", UniplacesPlace.floor().unit().building().s(), model.getBuilding()));
                if(model.getUnit() != null)
                    builder.add(MQExpression.eq("place", UniplacesPlace.floor().unit().s(), model.getUnit()));
                if(model.getFloor() != null)
                    builder.add(MQExpression.eq("place", UniplacesPlace.floor().s(), model.getFloor()));

                if (StringUtils.isNotBlank(filter))
                    builder.add(MQExpression.like("place", UniplacesPlace.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder("place", UniplacesPlace.title().s());
                return new ListResult<>(builder.<UniplacesPlace>getResultList(getSession(), 0, 100), builder.getResultCount(getSession()));
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((UniplacesPlace) value).getTitleWithLocation();
            }
        });

        List<OrgUnit> orgUnits = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou").add(MQExpression.notEq("ou", OrgUnit.orgUnitType().code().s(), OrgUnitTypeCodes.TOP)).<OrgUnit>getResultList(getSession());
        model.setOrgUnitList(new LazySimpleSelectModel<>(orgUnits, "fullTitle").setSearchFromStart(false).setSearchProperty("fullTitle"));
        model.setClassroomTypeModel(new UniplacesClassroomTypeSelectModel());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<UniplacesPlace> dataSource = model.getDataSource();

        MQBuilder builder = new MQBuilder(UniplacesPlace.ENTITY_CLASS, "place");
        builder.addJoinFetch("place", UniplacesPlace.floor().s(), "floor");
        builder.addJoinFetch("floor", UniplacesFloor.unit().s(), "unit");
        builder.addJoinFetch("unit", UniplacesUnit.building().s(), "building");

        List<UniplacesPlacePurpose> purposes = model.getPurposes();
        if (purposes != null && !purposes.isEmpty())
        {
            builder.add(MQExpression.in("place", UniplacesPlace.purpose().s(), purposes));
        }

        List<UniplacesClassroomType> classroomTypes = model.getClassroomTypes();
        if(classroomTypes != null && !classroomTypes.isEmpty())
        {
            builder.add(MQExpression.in("place", UniplacesPlace.classroomType().s(), classroomTypes));
        }

        UniplacesPlaceCondition condition = model.getCondition();
        if (condition != null)
        {
            builder.add(MQExpression.eq("place", UniplacesPlace.condition().s(), condition));
        }

        if (model.getPlace() != null)
            builder.add(MQExpression.eq("place", "id", model.getPlace().getId()));
        if (model.getFloor() != null)
            builder.add(MQExpression.eq("place", UniplacesPlace.floor().s(), model.getFloor()));
        if (model.getUnit() != null)
            builder.add(MQExpression.eq("floor", UniplacesFloor.unit().s(), model.getUnit()));
        if (model.getBuilding() != null)
            builder.add(MQExpression.eq("unit", UniplacesUnit.building().s(), model.getBuilding()));

        if (model.getOrgUnit() != null)
            builder.add(MQExpression.eq("place", UniplacesPlace.responsibleOrgUnit().s(), model.getOrgUnit()));
        if (model.getAreaMax() != null)
            builder.add(MQExpression.lessOrEq("place", UniplacesPlace.area().s(), model.getAreaMax()));
        if (model.getAreaMin() != null)
            builder.add(MQExpression.greatOrEq("place", UniplacesPlace.area().s(), model.getAreaMin()));
        if (model.getLengthMax()!= null)
            builder.add(MQExpression.lessOrEq("place", UniplacesPlace.length().s(), model.getLengthMax()));
        if (model.getLengthMin()!= null)
            builder.add(MQExpression.greatOrEq("place", UniplacesPlace.length().s(), model.getLengthMin()));
        if (model.getWidthMax()!= null)
            builder.add(MQExpression.lessOrEq("place", UniplacesPlace.width().s(), model.getWidthMax()));
        if (model.getWidthMin()!= null)
            builder.add(MQExpression.greatOrEq("place", UniplacesPlace.width().s(), model.getWidthMin()));
        if (model.getHeightMax()!= null)
            builder.add(MQExpression.lessOrEq("place", UniplacesPlace.height().s(), model.getHeightMax()));
        if (model.getHeightMin()!= null)
            builder.add(MQExpression.greatOrEq("place", UniplacesPlace.height().s(), model.getHeightMin()));

        _orderSettings.applyOrder(builder, dataSource.getEntityOrder());
        UniBaseUtils.createPage(dataSource, builder, getSession());
    }

    @Override
    public int deletePlacesList(Collection<Long> ids)
    {
        DQLDeleteBuilder delBuilder = new DQLDeleteBuilder(UniplacesPlace.class)
                .where(in(property(UniplacesPlace.id()), ids));
        delBuilder.where(new DQLCanDeleteExpressionBuilder(UniplacesPlace.class, "id").getExpression());

        return delBuilder.createStatement(getSession()).execute();
    }
}
