package ru.tandemservice.uniplaces.entity.place;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uniplaces.entity.place.gen.UniplacesDocumentGen;

/**
 * Документ
 */
public abstract class UniplacesDocument extends UniplacesDocumentGen
{
    public static final String P_FULL_TITLE = "fullTitle";

    public String getFullTitle()
    {
        StringBuilder title = new StringBuilder(getKind().getTitle());
        if (getNumber() != null)
            title.append(" №").append(getNumber());
        if (getIssuanceDate() != null)
            title.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getIssuanceDate()));
        return title.toString();
    }
}