package ru.tandemservice.uniplaces.entity.place;

import org.apache.commons.lang.StringUtils;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.archive.catalog.entity.ArchiveType;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uniplaces.entity.place.gen.UniplacesUnitGen;

/**
 * Блок
 */
public class UniplacesUnit extends UniplacesUnitGen
{
    public static final String P_FRACTIONAL_AREA = "fractionalArea";

    public String getDisplayableTitle()
    {
        if (StringUtils.isEmpty(getIdentifier()))
            return getTitle();
        return String.format("%s (%s)", getTitle(), getIdentifier());
    }

    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=32473371")
    @EntityDSLSupport
    public String getTitleExtended()
    {
        return getDisplayableTitle() + " (" + getBuilding().getShortTitle() + ")";
    }

    public Double getFractionalArea()
    {
        Long area = getArea();
        return (area != null) ? area / 100.0 : null;
    }

    public void setFractionalArea(Double area)
    {
        setArea((area != null) ? Math.round(area * 100.0d) : null);
    }
}