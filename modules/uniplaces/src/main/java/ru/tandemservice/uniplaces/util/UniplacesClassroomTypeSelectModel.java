/* $Id$ */
package ru.tandemservice.uniplaces.util;

import org.tandemframework.core.entity.dsl.IPropertyPath;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author nvankov
 * @since 11/10/14
 */
public class UniplacesClassroomTypeSelectModel extends DQLFullCheckSelectModel
{

    public UniplacesClassroomTypeSelectModel() {
        this(UniplacesClassroomType.title());
    }

    public UniplacesClassroomTypeSelectModel(final IPropertyPath... labelProperties) {
        super(labelProperties);
    }

    @Override
    protected DQLSelectBuilder query(final String alias, final String filter) {
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(UniplacesClassroomType.class, alias);
        if (null != filter) {
            dql.where(like(UniplacesClassroomType.title().fromAlias(alias), filter));
        }
        dql.order(property(UniplacesClassroomType.title().fromAlias(alias)));
        return dql;
    }

}
