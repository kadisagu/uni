/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniplaces.component.place.FloorAddEdit;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

/**
 * @author Julia Oschepkova
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        UniplacesUnit unit = getNotNull(UniplacesUnit.class, model.getUnit().getId());
        model.setUnit(unit);
        if (model.isEditForm())
        {
            model.setFloor(getNotNull(UniplacesFloor.class, model.getFloor().getId()));
        }
    }

    @Override
    public void update(Model model)
    {
        //добавление этажа
        if (null == model.getFloor().getUnit())
        {
            model.getFloor().setUnit(model.getUnit());
            model.getFloor().setResponsibleOrgUnit(model.getUnit().getResponsibleOrgUnit());

            //нашли макс номер
            Criteria criteria = getSession().createCriteria(UniplacesFloor.class);
            criteria.setProjection(Projections.max(UniplacesFloor.P_NUMBER));
            criteria.add(Restrictions.eq(UniplacesFloor.L_UNIT, model.getUnit()));

            Number result = (Number) criteria.uniqueResult();

            int max = (null == result) ? 0 : result.intValue() + 1;
            model.getFloor().setNumber(max);

            getSession().save(model.getFloor());
        }
        //обновление этажа
        else
        {
            getSession().update(model.getFloor());
        }
    }
}
