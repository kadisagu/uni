package ru.tandemservice.uniplaces.entity.place.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniplaces.entity.place.UniplacesRegistryRecord;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись реестра
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniplacesRegistryRecordGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplaces.entity.place.UniplacesRegistryRecord";
    public static final String ENTITY_NAME = "uniplacesRegistryRecord";
    public static final int VERSION_HASH = 1630551759;
    private static IEntityMeta ENTITY_META;

    public static final String L_RESPONSIBLE_ORG_UNIT = "responsibleOrgUnit";
    public static final String P_COMMENT = "comment";
    public static final String P_CADASTRE_RECORD_NUMBER = "cadastreRecordNumber";

    private OrgUnit _responsibleOrgUnit;     // Ответственное подразделение
    private String _comment;     // Комментарий
    private String _cadastreRecordNumber;     // Номер кадастровой записи

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Ответственное подразделение.
     */
    public OrgUnit getResponsibleOrgUnit()
    {
        return _responsibleOrgUnit;
    }

    /**
     * @param responsibleOrgUnit Ответственное подразделение.
     */
    public void setResponsibleOrgUnit(OrgUnit responsibleOrgUnit)
    {
        dirty(_responsibleOrgUnit, responsibleOrgUnit);
        _responsibleOrgUnit = responsibleOrgUnit;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=500)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Номер кадастровой записи.
     */
    @Length(max=255)
    public String getCadastreRecordNumber()
    {
        return _cadastreRecordNumber;
    }

    /**
     * @param cadastreRecordNumber Номер кадастровой записи.
     */
    public void setCadastreRecordNumber(String cadastreRecordNumber)
    {
        dirty(_cadastreRecordNumber, cadastreRecordNumber);
        _cadastreRecordNumber = cadastreRecordNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniplacesRegistryRecordGen)
        {
            setResponsibleOrgUnit(((UniplacesRegistryRecord)another).getResponsibleOrgUnit());
            setComment(((UniplacesRegistryRecord)another).getComment());
            setCadastreRecordNumber(((UniplacesRegistryRecord)another).getCadastreRecordNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniplacesRegistryRecordGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniplacesRegistryRecord.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("UniplacesRegistryRecord is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "responsibleOrgUnit":
                    return obj.getResponsibleOrgUnit();
                case "comment":
                    return obj.getComment();
                case "cadastreRecordNumber":
                    return obj.getCadastreRecordNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "responsibleOrgUnit":
                    obj.setResponsibleOrgUnit((OrgUnit) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "cadastreRecordNumber":
                    obj.setCadastreRecordNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "responsibleOrgUnit":
                        return true;
                case "comment":
                        return true;
                case "cadastreRecordNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "responsibleOrgUnit":
                    return true;
                case "comment":
                    return true;
                case "cadastreRecordNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "responsibleOrgUnit":
                    return OrgUnit.class;
                case "comment":
                    return String.class;
                case "cadastreRecordNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniplacesRegistryRecord> _dslPath = new Path<UniplacesRegistryRecord>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniplacesRegistryRecord");
    }
            

    /**
     * @return Ответственное подразделение.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesRegistryRecord#getResponsibleOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> responsibleOrgUnit()
    {
        return _dslPath.responsibleOrgUnit();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesRegistryRecord#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Номер кадастровой записи.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesRegistryRecord#getCadastreRecordNumber()
     */
    public static PropertyPath<String> cadastreRecordNumber()
    {
        return _dslPath.cadastreRecordNumber();
    }

    public static class Path<E extends UniplacesRegistryRecord> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _responsibleOrgUnit;
        private PropertyPath<String> _comment;
        private PropertyPath<String> _cadastreRecordNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Ответственное подразделение.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesRegistryRecord#getResponsibleOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> responsibleOrgUnit()
        {
            if(_responsibleOrgUnit == null )
                _responsibleOrgUnit = new OrgUnit.Path<OrgUnit>(L_RESPONSIBLE_ORG_UNIT, this);
            return _responsibleOrgUnit;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesRegistryRecord#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(UniplacesRegistryRecordGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Номер кадастровой записи.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesRegistryRecord#getCadastreRecordNumber()
     */
        public PropertyPath<String> cadastreRecordNumber()
        {
            if(_cadastreRecordNumber == null )
                _cadastreRecordNumber = new PropertyPath<String>(UniplacesRegistryRecordGen.P_CADASTRE_RECORD_NUMBER, this);
            return _cadastreRecordNumber;
        }

        public Class getEntityClass()
        {
            return UniplacesRegistryRecord.class;
        }

        public String getEntityName()
        {
            return "uniplacesRegistryRecord";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
