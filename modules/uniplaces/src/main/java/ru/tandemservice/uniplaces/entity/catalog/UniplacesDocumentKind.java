package ru.tandemservice.uniplaces.entity.catalog;

import org.tandemframework.core.tool.tree.IHierarchyItem;

import ru.tandemservice.uniplaces.entity.catalog.gen.UniplacesDocumentKindGen;
import ru.tandemservice.uniplaces.entity.place.UniplacesDocument;

/**
 * Виды документов
 */
public class UniplacesDocumentKind extends UniplacesDocumentKindGen implements IHierarchyItem
{
    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return getParent();
    }

    public String getSafeDocumentClassName()
    {
        for (UniplacesDocumentKind documentKind = this; documentKind != null; documentKind = documentKind.getParent())
        {
            String documentClassName = getDocumentClassName();
            if (documentClassName != null)
            {
                return documentClassName;
            }
        }
        return null;
    }

    public UniplacesDocument createDocument()
    {
        try
        {
            return (UniplacesDocument) Class.forName(getSafeDocumentClassName()).newInstance();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
}