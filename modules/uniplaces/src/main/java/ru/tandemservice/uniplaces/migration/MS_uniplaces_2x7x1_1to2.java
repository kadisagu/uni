package ru.tandemservice.uniplaces.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.SQLException;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniplaces_2x7x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
                new ScriptDependency("org.tandemframework", "1.6.16"),
                new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность uniplacesRegistryRecord

		// создано свойство cadastreRecordNumber
		if (!tool.columnExists("places_registry_record","cadastrerecordnumber_p"))
        {
			// создать колонку
			tool.createColumn("places_registry_record", new DBColumn("cadastrerecordnumber_p", DBType.createVarchar(255)));

		}



		////////////////////////////////////////////////////////////////////////////////
		// сущность uniplacesGround

		// удалено свойство cadastreRecordNumber
		if (tool.columnExists("places_ground", "cadastrerecordnumber_p"))
        {
            updateCadastreRecordNumber(tool);
			// удалить колонку
			tool.dropColumn("places_ground", "cadastrerecordnumber_p");

		}


    }

    private void updateCadastreRecordNumber(DBTool tool) throws SQLException
    {
        tool.getStatement().executeUpdate("update places_registry_record  set cadastreRecordNumber_p = (select ground.cadastreRecordNumber_p from places_ground ground where places_registry_record.id=ground.id)");
    }
}