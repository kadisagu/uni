/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.UnitPub;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniplaces.component.place.AbstractPlaceObjectPub.AbstractPlaceObjectPubDAO;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

/**
 * @author agolubenko
 * @since Aug 20, 2010
 */
public class DAO extends AbstractPlaceObjectPubDAO<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setUnit(getNotNull(UniplacesUnit.class, model.getUnit().getId()));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        super.prepareListDataSource(model);
        MQBuilder builder = new MQBuilder(UniplacesFloor.ENTITY_CLASS, "floor");
        builder.add(MQExpression.eq("floor", UniplacesFloor.unit().s(), model.getUnit()));
        builder.addOrder("floor", UniplacesFloor.number().s(), OrderDirection.asc);
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void updateFloorUpOrDown(Model model, Long id, boolean up)
    {
        Session session = getSession();
        //нашли объект который надо сделать выше
        UniplacesFloor floor1 = get(UniplacesFloor.class, id);
        int low = floor1.getNumber();

        if (up && floor1.getNumber() == 0)
            return;

        //нашли макс значение в поле номер
        Criteria criteria = getSession().createCriteria(UniplacesFloor.class);
        criteria.setProjection(Projections.max(UniplacesFloor.P_NUMBER));
        criteria.add(Restrictions.eq(UniplacesFloor.L_UNIT, model.getUnit()));

        Number result = (Number) criteria.uniqueResult();

        int max = (null == result) ?  0  : result.intValue() +  1;

        if (!up && floor1.getNumber() == max) return;

        //нашли объект который надо сделать ниже
        criteria = session.createCriteria(UniplacesFloor.class);
        criteria.add(Restrictions.eq(UniplacesFloor.L_UNIT, model.getUnit()));

        //если двигаем вверх то ищем первый с меньшим номером
        if (up)
        {
            criteria.add(Restrictions.lt(UniplacesFloor.number().s(), floor1.getNumber()));
            criteria.addOrder(Order.desc(UniplacesFloor.number().s()));
        }
        //если вниз, то первый с большим номером
        else
        {
            criteria.add(Restrictions.gt(UniplacesFloor.number().s(), floor1.getNumber()));
            criteria.addOrder(Order.asc(UniplacesFloor.number().s()));
        }

        criteria.setMaxResults(1);
        List<UniplacesFloor> floors = criteria.list();
        UniplacesFloor floor2 = floors.get(0);
        int high = floor2.getNumber();

        floor1.setNumber(max + 1);
        session.update(floor1);
        session.flush();

        floor2.setNumber(low);
        session.update(floor2);
        session.flush();

        floor1.setNumber(high);
        session.update(floor1);
    }

}
