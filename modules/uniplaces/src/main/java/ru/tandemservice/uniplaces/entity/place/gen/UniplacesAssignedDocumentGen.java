package ru.tandemservice.uniplaces.entity.place.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentPurpose;
import ru.tandemservice.uniplaces.entity.place.UniplacesAssignedDocument;
import ru.tandemservice.uniplaces.entity.place.UniplacesDocument;
import ru.tandemservice.uniplaces.entity.place.UniplacesRegistryRecord;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Назначенный документ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniplacesAssignedDocumentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplaces.entity.place.UniplacesAssignedDocument";
    public static final String ENTITY_NAME = "uniplacesAssignedDocument";
    public static final int VERSION_HASH = -313847659;
    private static IEntityMeta ENTITY_META;

    public static final String L_REGISTRY_RECORD = "registryRecord";
    public static final String L_DOCUMENT = "document";
    public static final String L_PURPOSE = "purpose";

    private UniplacesRegistryRecord _registryRecord;     // Запись реестра
    private UniplacesDocument _document;     // Документ (здания и помещения)
    private UniplacesDocumentPurpose _purpose;     // Назначение документа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Запись реестра. Свойство не может быть null.
     */
    @NotNull
    public UniplacesRegistryRecord getRegistryRecord()
    {
        return _registryRecord;
    }

    /**
     * @param registryRecord Запись реестра. Свойство не может быть null.
     */
    public void setRegistryRecord(UniplacesRegistryRecord registryRecord)
    {
        dirty(_registryRecord, registryRecord);
        _registryRecord = registryRecord;
    }

    /**
     * @return Документ (здания и помещения). Свойство не может быть null.
     */
    @NotNull
    public UniplacesDocument getDocument()
    {
        return _document;
    }

    /**
     * @param document Документ (здания и помещения). Свойство не может быть null.
     */
    public void setDocument(UniplacesDocument document)
    {
        dirty(_document, document);
        _document = document;
    }

    /**
     * @return Назначение документа. Свойство не может быть null.
     */
    @NotNull
    public UniplacesDocumentPurpose getPurpose()
    {
        return _purpose;
    }

    /**
     * @param purpose Назначение документа. Свойство не может быть null.
     */
    public void setPurpose(UniplacesDocumentPurpose purpose)
    {
        dirty(_purpose, purpose);
        _purpose = purpose;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniplacesAssignedDocumentGen)
        {
            setRegistryRecord(((UniplacesAssignedDocument)another).getRegistryRecord());
            setDocument(((UniplacesAssignedDocument)another).getDocument());
            setPurpose(((UniplacesAssignedDocument)another).getPurpose());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniplacesAssignedDocumentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniplacesAssignedDocument.class;
        }

        public T newInstance()
        {
            return (T) new UniplacesAssignedDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "registryRecord":
                    return obj.getRegistryRecord();
                case "document":
                    return obj.getDocument();
                case "purpose":
                    return obj.getPurpose();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "registryRecord":
                    obj.setRegistryRecord((UniplacesRegistryRecord) value);
                    return;
                case "document":
                    obj.setDocument((UniplacesDocument) value);
                    return;
                case "purpose":
                    obj.setPurpose((UniplacesDocumentPurpose) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "registryRecord":
                        return true;
                case "document":
                        return true;
                case "purpose":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "registryRecord":
                    return true;
                case "document":
                    return true;
                case "purpose":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "registryRecord":
                    return UniplacesRegistryRecord.class;
                case "document":
                    return UniplacesDocument.class;
                case "purpose":
                    return UniplacesDocumentPurpose.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniplacesAssignedDocument> _dslPath = new Path<UniplacesAssignedDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniplacesAssignedDocument");
    }
            

    /**
     * @return Запись реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesAssignedDocument#getRegistryRecord()
     */
    public static UniplacesRegistryRecord.Path<UniplacesRegistryRecord> registryRecord()
    {
        return _dslPath.registryRecord();
    }

    /**
     * @return Документ (здания и помещения). Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesAssignedDocument#getDocument()
     */
    public static UniplacesDocument.Path<UniplacesDocument> document()
    {
        return _dslPath.document();
    }

    /**
     * @return Назначение документа. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesAssignedDocument#getPurpose()
     */
    public static UniplacesDocumentPurpose.Path<UniplacesDocumentPurpose> purpose()
    {
        return _dslPath.purpose();
    }

    public static class Path<E extends UniplacesAssignedDocument> extends EntityPath<E>
    {
        private UniplacesRegistryRecord.Path<UniplacesRegistryRecord> _registryRecord;
        private UniplacesDocument.Path<UniplacesDocument> _document;
        private UniplacesDocumentPurpose.Path<UniplacesDocumentPurpose> _purpose;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Запись реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesAssignedDocument#getRegistryRecord()
     */
        public UniplacesRegistryRecord.Path<UniplacesRegistryRecord> registryRecord()
        {
            if(_registryRecord == null )
                _registryRecord = new UniplacesRegistryRecord.Path<UniplacesRegistryRecord>(L_REGISTRY_RECORD, this);
            return _registryRecord;
        }

    /**
     * @return Документ (здания и помещения). Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesAssignedDocument#getDocument()
     */
        public UniplacesDocument.Path<UniplacesDocument> document()
        {
            if(_document == null )
                _document = new UniplacesDocument.Path<UniplacesDocument>(L_DOCUMENT, this);
            return _document;
        }

    /**
     * @return Назначение документа. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesAssignedDocument#getPurpose()
     */
        public UniplacesDocumentPurpose.Path<UniplacesDocumentPurpose> purpose()
        {
            if(_purpose == null )
                _purpose = new UniplacesDocumentPurpose.Path<UniplacesDocumentPurpose>(L_PURPOSE, this);
            return _purpose;
        }

        public Class getEntityClass()
        {
            return UniplacesAssignedDocument.class;
        }

        public String getEntityName()
        {
            return "uniplacesAssignedDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
