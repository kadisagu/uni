/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.PlaceRangeAdd;

import org.apache.commons.lang.StringUtils;

import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniplaces.dao.IUniplacesDAO;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceCondition;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;
import ru.tandemservice.uniplaces.util.UniplacesClassroomTypeSelectModel;

/**
 * @author agolubenko
 * @since Sep 1, 2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        UniplacesFloor floor = getNotNull(UniplacesFloor.class, model.getFloor().getId());
        UniplacesUnit unit = floor.getUnit();
        UniplacesBuilding building = unit.getBuilding();

        model.setFloor(floor);
        model.setPrefix(StringUtils.defaultString(building.getIdentifier()) + StringUtils.defaultString(unit.getIdentifier()) + floor.getTitle());

        model.setPlaceConditionList(getCatalogItemListOrderByCode(UniplacesPlaceCondition.class));
        model.setPlacePurposeList(HierarchyUtil.listHierarchyNodesWithParents(getCatalogItemListOrderByCode(UniplacesPlacePurpose.class), true));
        model.setResponsibleOrgUnitList(IUniplacesDAO.instance.get().getResponsibleOrgUnitModel());
        model.setClassroomTypeModel(new UniplacesClassroomTypeSelectModel());
    }

    @Override
    public void update(Model model)
    {
        for (int i = model.getFrom(); i <= model.getTo(); i++)
        {
            UniplacesPlace place = new UniplacesPlace();
            place.setFloor(model.getFloor());
            final String number = StringUtils.defaultString(model.getPrefix()) + String.format("%0" + model.getDigitsNumber() + "d", i) + StringUtils.defaultString(model.getPostfix());
            place.setNumber(number);
            place.setTitle(number);
            place.setPurpose(model.getPlacePurpose());
            place.setCondition(model.getPlaceCondition());
            place.setResponsibleOrgUnit(model.getResponsibleOrgUnit());
            place.setClassroomType(model.getClassroomType());

            save(place);

            boolean habitable = IUniplacesDAO.instance.get().getHabitablePlacePurposeList().contains(place.getPurpose());
            UniplacesHabitablePlace habitablePlace = get(UniplacesHabitablePlace.class, UniplacesHabitablePlace.place().s(), place);

            if (habitable)
            {
                if (habitablePlace == null)
                {
                    habitablePlace = new UniplacesHabitablePlace();
                    habitablePlace.setPlace(place);
                    habitablePlace.setBedNumber(0);
                    save(habitablePlace);
                }
            }
        }
    }
}
