package ru.tandemservice.uniplaces.entity.catalog;

import org.tandemframework.core.tool.tree.IHierarchyItem;

import ru.tandemservice.uniplaces.entity.catalog.gen.UniplacesPlacePurposeGen;

/**
 * Назначение помещения
 */
public class UniplacesPlacePurpose extends UniplacesPlacePurposeGen implements IHierarchyItem
{
    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return getParent();
    }
}