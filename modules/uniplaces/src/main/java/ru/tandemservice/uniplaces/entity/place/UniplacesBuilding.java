package ru.tandemservice.uniplaces.entity.place;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uniplaces.entity.place.gen.UniplacesBuildingGen;

/**
 * Здание
 */
public class UniplacesBuilding extends UniplacesBuildingGen
{
    public static final String P_FRACTIONAL_AREA = "fractionalArea";

    public Double getFractionalArea()
    {
        Long area = getArea();
        return (area != null) ? area / 100.0 : null;
    }

    public void setFractionalArea(Double area)
    {
        setArea((area != null) ? Math.round(area * 100.0d) : null);
    }

    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=15012852")
    @EntityDSLSupport
    public String getTitleWithAddress()
    {
        return getTitle() + (StringUtils.isEmpty(getAddress().getShortTitleWithSettlement()) ? "" : " (" + getAddress().getShortTitleWithSettlement() + ")");
    }
}