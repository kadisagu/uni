/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.GroundAddEdit;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniplaces.UniplacesDefines;
import ru.tandemservice.uniplaces.dao.IUniplacesDAO;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDisposalRight;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentKind;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentPurpose;
import ru.tandemservice.uniplaces.entity.place.UniplacesAssignedDocument;
import ru.tandemservice.uniplaces.entity.place.UniplacesDocument;
import ru.tandemservice.uniplaces.entity.place.UniplacesGround;
import ru.tandemservice.uniplaces.settings.CadastreRecordNamingSettings;
import ru.tandemservice.uniplaces.util.PlacesUtil;

import java.util.Date;
import java.util.List;

/**
 * @author agolubenko
 * @since Aug 2, 2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        UniplacesDocumentPurpose documentPurpose = getCatalogItem(UniplacesDocumentPurpose.class, UniplacesDefines.CATALOG_DOCUMENT_PURPOSE_GROUND_DISPOSAL_RIGHT);

        // если форма редактирования, то получаем объекты
        if (model.isEditForm())
        {
            UniplacesGround ground = get(UniplacesGround.class, model.getGround().getId());
            model.setGround(ground);
            model.setAssignedDocument(getAssignedDocument(ground, documentPurpose));
        }

        // если документ не назначен, то создаем заготовку
        if (model.getAssignedDocument() == null)
        {
            UniplacesAssignedDocument assignedDocument = new UniplacesAssignedDocument();
            assignedDocument.setRegistryRecord(model.getGround());
            assignedDocument.setPurpose(documentPurpose);
            assignedDocument.setDocument(documentPurpose.getDocumentKind().createDocument());

            model.setAssignedDocument(assignedDocument);
        }

        // фильтруем виды документов по назначению
        List<UniplacesDocumentKind> documentKinds = PlacesUtil.getFilteredDocumentKinds(getCatalogItemList(UniplacesDocumentKind.class), documentPurpose);
        model.setDocumentKindList(HierarchyUtil.listHierarchyNodesWithParents(documentKinds, CommonCatalogUtil.CATALOG_CODE_COMPARATOR, false));

        // заполняем остальные селекты
        model.setDisposalRightList(getCatalogItemListOrderByCode(UniplacesDisposalRight.class));
        model.setOrgUnitList(IUniplacesDAO.instance.get().getResponsibleOrgUnitModel());
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (model.getGround().getRightStartDate() != null && model.getGround().getRightStartDate().after(new Date()))
            errors.add("Дата установления права распоряжения участком должна быть не позже сегодняшней.", "rightStartDate");
    }

    @Override
    public void update(Model model)
    {
        Session session = getSession();

        // получаем настройки
        CadastreRecordNamingSettings settings = DataSettingsFacade.getSettings("", UniplacesDefines.MODULE_SETTINGS_KEY).get(UniplacesDefines.SETTINGS_CADASTRE_RECORD_NAMING);
        if (settings == null)
        {
            settings = new CadastreRecordNamingSettings();
        }

        // генерируем кадастровый номер, если форма добавления 
        UniplacesGround ground = model.getGround();
        if (!model.isEditForm())
        {
            Integer defaultDigitsNumber = settings.getDigitsNumber();
            Integer defaultStartNumber = settings.getStartNumber();
            int number = getMaxGroundNumber(defaultStartNumber);

            ground.setNumber(number);
//            ground.setCadastreRecordNumber(getCadastreRecordNumber(settings.getPrefix(), defaultDigitsNumber, number));
        }
        session.saveOrUpdate(ground);

        // обновляем или удаляем назначенный документ
        UniplacesAssignedDocument assignedDocument = model.getAssignedDocument();
        UniplacesDocument document = assignedDocument.getDocument();
        if (document.getKind() != null || !StringUtils.isBlank(document.getNumber()) || document.getIssuanceDate() != null)
        {
            session.saveOrUpdate(document);
            session.saveOrUpdate(assignedDocument);
        }
        else if (assignedDocument.getId() != null)
        {
            session.delete(assignedDocument);
            session.delete(document);
        }
    }

    /**
     * @param defaultValue номер по умолчанию
     * @return максимальный номер земельного участка
     */
    private int getMaxGroundNumber(int defaultValue)
    {
        Criteria criteria = getSession().createCriteria(UniplacesGround.class);
        criteria.setProjection(Projections.max(UniplacesGround.number().s()));

        Number result = (Number) criteria.uniqueResult();
        return (result != null) ? result.intValue() + 1 : defaultValue;
    }

    /**
     * @param prefix префикс
     * @param digitsNumber количество цифр в номере
     * @param number порядковый номер
     * @return номер кадастровой записи
     */
    private String getCadastreRecordNumber(String prefix, int digitsNumber, int number)
    {
        return String.format("%s%0" + digitsNumber + "d", StringUtils.defaultString(prefix), number);
    }

    /**
     * @param ground земельный участок
     * @param documentPurpose назначение документа
     * @return назначенный документ
     */
    private UniplacesAssignedDocument getAssignedDocument(UniplacesGround ground, UniplacesDocumentPurpose documentPurpose)
    {
        Criteria criteria = getSession().createCriteria(UniplacesAssignedDocument.class);
        criteria.add(Restrictions.eq(UniplacesAssignedDocument.registryRecord().s(), ground));
        criteria.add(Restrictions.eq(UniplacesAssignedDocument.purpose().s(), documentPurpose));
        return (UniplacesAssignedDocument) criteria.uniqueResult();
    }
}
