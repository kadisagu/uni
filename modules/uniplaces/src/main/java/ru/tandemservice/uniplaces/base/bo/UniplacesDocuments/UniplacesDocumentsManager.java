/* $Id$ */
package ru.tandemservice.uniplaces.base.bo.UniplacesDocuments;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniplaces.base.bo.UniplacesDocuments.logic.IUniplacesDocumentsDAO;
import ru.tandemservice.uniplaces.base.bo.UniplacesDocuments.logic.UniplacesDocumentsDAO;

/**
 * @author Ekaterina Zvereva
 * @since 12.02.2015
 */
@Configuration
public class UniplacesDocumentsManager extends BusinessObjectManager
{
    public static UniplacesDocumentsManager instance() {return instance(UniplacesDocumentsManager.class);}

    @Bean
    public IUniplacesDocumentsDAO dao()
    {
        return new UniplacesDocumentsDAO();
    }
}