/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.UnitPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniplaces.UniplacesComponents;
import ru.tandemservice.uniplaces.component.place.AbstractPlaceObjectPub.AbstractPlaceObjectPubController;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;

/**
 * @author agolubenko
 * @since Aug 20, 2010
 */
public class Controller extends AbstractPlaceObjectPubController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        prepareListDataSource(component);
        getDao().prepare(getModel(component));
    }


    private void prepareListDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<UniplacesFloor> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Этаж", UniplacesFloor.title().s()).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Вверх", CommonBaseDefine.ICO_UP, "onClickFloorUp").setPermissionKey("changeFloorNumber_unit"));
        dataSource.addColumn(new ActionColumn("Вниз", CommonBaseDefine.ICO_DOWN, "onClickFloorDown").setPermissionKey("changeFloorNumber_unit"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditFloor").setPermissionKey("editFloor_unit"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteFloor", "Удалить этаж {0}?", UniplacesFloor.title().s()).setPermissionKey("deleteFloor_unit"));

        getModel(component).setDataSource(dataSource);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                UniplacesComponents.UNIT_ADD_EDIT
        ));
    }

    public void onClickAddFloor(IBusinessComponent component)
    {

        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                UniplacesComponents.FLOOR_ADD_EDIT, new ParametersMap().add("floorId", null)
        ));
    }

    public void onClickEditFloor(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                UniplacesComponents.FLOOR_ADD_EDIT, new ParametersMap().add("floorId", component.getListenerParameter())
        ));
    }

    public void onClickDeleteFloor(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }

    public synchronized void onClickFloorUp(IBusinessComponent component)
    {
        getDao().updateFloorUpOrDown(getModel(component), (Long) component.getListenerParameter(), true);
    }

    public synchronized void onClickFloorDown(IBusinessComponent component)
    {
        getDao().updateFloorUpOrDown(getModel(component), (Long) component.getListenerParameter(), false);
    }

    public synchronized void onClickAddFloorRange(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                UniplacesComponents.FLOOR_RANGE_ADD
        ));
    }
}
