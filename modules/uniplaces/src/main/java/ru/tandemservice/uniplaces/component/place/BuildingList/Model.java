/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.BuildingList;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;

/**
 * @author agolubenko
 * @since Aug 10, 2010
 */
public class Model
{
    private DynamicListDataSource<UniplacesBuilding> _dataSource;

    public DynamicListDataSource<UniplacesBuilding> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<UniplacesBuilding> dataSource)
    {
        _dataSource = dataSource;
    }
}
