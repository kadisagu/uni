/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.GroundList;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniplaces.entity.place.UniplacesGround;

/**
 * @author agolubenko
 * @since Aug 2, 2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("ground");
    static
    {
        _orderSettings.setOrders(UniplacesGround.responsibleOrgUnit().fullTitle().s(), new OrderDescription("orgUnitType", OrgUnitType.title().s()), new OrderDescription("orgUnit", OrgUnit.title().s()));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<UniplacesGround> dataSource = model.getDataSource();

        MQBuilder builder = new MQBuilder(UniplacesGround.ENTITY_CLASS, "ground");
        builder.addLeftJoinFetch("ground", UniplacesGround.responsibleOrgUnit().s(), "orgUnit");
        builder.addLeftJoin("orgUnit", OrgUnit.orgUnitType().s(), "orgUnitType");

        _orderSettings.applyOrder(builder, dataSource.getEntityOrder());
        UniBaseUtils.createPage(dataSource, builder, getSession());
    }
}
