package ru.tandemservice.uniplaces.entity.place.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesBuildingPurpose;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceNumberingType;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesRegistryRecord;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Блок
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniplacesUnitGen extends UniplacesRegistryRecord
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplaces.entity.place.UniplacesUnit";
    public static final String ENTITY_NAME = "uniplacesUnit";
    public static final int VERSION_HASH = -727543817;
    private static IEntityMeta ENTITY_META;

    public static final String L_BUILDING = "building";
    public static final String L_PURPOSE = "purpose";
    public static final String L_PLACE_NUMBERING_TYPE = "placeNumberingType";
    public static final String P_TITLE = "title";
    public static final String P_IDENTIFIER = "identifier";
    public static final String P_FLOOR_NUMBER = "floorNumber";
    public static final String P_AREA = "area";
    public static final String P_TITLE_EXTENDED = "titleExtended";

    private UniplacesBuilding _building;     // Здание
    private UniplacesBuildingPurpose _purpose;     // Назначение здания
    private UniplacesPlaceNumberingType _placeNumberingType;     // Тип нумерации помещений
    private String _title;     // Название
    private String _identifier;     // Идентификатор
    private Integer _floorNumber;     // Этажность
    private Long _area;     // Площадь кв. м

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Здание. Свойство не может быть null.
     */
    @NotNull
    public UniplacesBuilding getBuilding()
    {
        return _building;
    }

    /**
     * @param building Здание. Свойство не может быть null.
     */
    public void setBuilding(UniplacesBuilding building)
    {
        dirty(_building, building);
        _building = building;
    }

    /**
     * @return Назначение здания. Свойство не может быть null.
     */
    @NotNull
    public UniplacesBuildingPurpose getPurpose()
    {
        return _purpose;
    }

    /**
     * @param purpose Назначение здания. Свойство не может быть null.
     */
    public void setPurpose(UniplacesBuildingPurpose purpose)
    {
        dirty(_purpose, purpose);
        _purpose = purpose;
    }

    /**
     * @return Тип нумерации помещений. Свойство не может быть null.
     */
    @NotNull
    public UniplacesPlaceNumberingType getPlaceNumberingType()
    {
        return _placeNumberingType;
    }

    /**
     * @param placeNumberingType Тип нумерации помещений. Свойство не может быть null.
     */
    public void setPlaceNumberingType(UniplacesPlaceNumberingType placeNumberingType)
    {
        dirty(_placeNumberingType, placeNumberingType);
        _placeNumberingType = placeNumberingType;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Идентификатор.
     */
    @Length(max=255)
    public String getIdentifier()
    {
        return _identifier;
    }

    /**
     * @param identifier Идентификатор.
     */
    public void setIdentifier(String identifier)
    {
        dirty(_identifier, identifier);
        _identifier = identifier;
    }

    /**
     * @return Этажность.
     */
    public Integer getFloorNumber()
    {
        return _floorNumber;
    }

    /**
     * @param floorNumber Этажность.
     */
    public void setFloorNumber(Integer floorNumber)
    {
        dirty(_floorNumber, floorNumber);
        _floorNumber = floorNumber;
    }

    /**
     * @return Площадь кв. м.
     */
    public Long getArea()
    {
        return _area;
    }

    /**
     * @param area Площадь кв. м.
     */
    public void setArea(Long area)
    {
        dirty(_area, area);
        _area = area;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UniplacesUnitGen)
        {
            setBuilding(((UniplacesUnit)another).getBuilding());
            setPurpose(((UniplacesUnit)another).getPurpose());
            setPlaceNumberingType(((UniplacesUnit)another).getPlaceNumberingType());
            setTitle(((UniplacesUnit)another).getTitle());
            setIdentifier(((UniplacesUnit)another).getIdentifier());
            setFloorNumber(((UniplacesUnit)another).getFloorNumber());
            setArea(((UniplacesUnit)another).getArea());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniplacesUnitGen> extends UniplacesRegistryRecord.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniplacesUnit.class;
        }

        public T newInstance()
        {
            return (T) new UniplacesUnit();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "building":
                    return obj.getBuilding();
                case "purpose":
                    return obj.getPurpose();
                case "placeNumberingType":
                    return obj.getPlaceNumberingType();
                case "title":
                    return obj.getTitle();
                case "identifier":
                    return obj.getIdentifier();
                case "floorNumber":
                    return obj.getFloorNumber();
                case "area":
                    return obj.getArea();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "building":
                    obj.setBuilding((UniplacesBuilding) value);
                    return;
                case "purpose":
                    obj.setPurpose((UniplacesBuildingPurpose) value);
                    return;
                case "placeNumberingType":
                    obj.setPlaceNumberingType((UniplacesPlaceNumberingType) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "identifier":
                    obj.setIdentifier((String) value);
                    return;
                case "floorNumber":
                    obj.setFloorNumber((Integer) value);
                    return;
                case "area":
                    obj.setArea((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "building":
                        return true;
                case "purpose":
                        return true;
                case "placeNumberingType":
                        return true;
                case "title":
                        return true;
                case "identifier":
                        return true;
                case "floorNumber":
                        return true;
                case "area":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "building":
                    return true;
                case "purpose":
                    return true;
                case "placeNumberingType":
                    return true;
                case "title":
                    return true;
                case "identifier":
                    return true;
                case "floorNumber":
                    return true;
                case "area":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "building":
                    return UniplacesBuilding.class;
                case "purpose":
                    return UniplacesBuildingPurpose.class;
                case "placeNumberingType":
                    return UniplacesPlaceNumberingType.class;
                case "title":
                    return String.class;
                case "identifier":
                    return String.class;
                case "floorNumber":
                    return Integer.class;
                case "area":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniplacesUnit> _dslPath = new Path<UniplacesUnit>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniplacesUnit");
    }
            

    /**
     * @return Здание. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesUnit#getBuilding()
     */
    public static UniplacesBuilding.Path<UniplacesBuilding> building()
    {
        return _dslPath.building();
    }

    /**
     * @return Назначение здания. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesUnit#getPurpose()
     */
    public static UniplacesBuildingPurpose.Path<UniplacesBuildingPurpose> purpose()
    {
        return _dslPath.purpose();
    }

    /**
     * @return Тип нумерации помещений. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesUnit#getPlaceNumberingType()
     */
    public static UniplacesPlaceNumberingType.Path<UniplacesPlaceNumberingType> placeNumberingType()
    {
        return _dslPath.placeNumberingType();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesUnit#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Идентификатор.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesUnit#getIdentifier()
     */
    public static PropertyPath<String> identifier()
    {
        return _dslPath.identifier();
    }

    /**
     * @return Этажность.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesUnit#getFloorNumber()
     */
    public static PropertyPath<Integer> floorNumber()
    {
        return _dslPath.floorNumber();
    }

    /**
     * @return Площадь кв. м.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesUnit#getArea()
     */
    public static PropertyPath<Long> area()
    {
        return _dslPath.area();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesUnit#getTitleExtended()
     */
    public static SupportedPropertyPath<String> titleExtended()
    {
        return _dslPath.titleExtended();
    }

    public static class Path<E extends UniplacesUnit> extends UniplacesRegistryRecord.Path<E>
    {
        private UniplacesBuilding.Path<UniplacesBuilding> _building;
        private UniplacesBuildingPurpose.Path<UniplacesBuildingPurpose> _purpose;
        private UniplacesPlaceNumberingType.Path<UniplacesPlaceNumberingType> _placeNumberingType;
        private PropertyPath<String> _title;
        private PropertyPath<String> _identifier;
        private PropertyPath<Integer> _floorNumber;
        private PropertyPath<Long> _area;
        private SupportedPropertyPath<String> _titleExtended;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Здание. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesUnit#getBuilding()
     */
        public UniplacesBuilding.Path<UniplacesBuilding> building()
        {
            if(_building == null )
                _building = new UniplacesBuilding.Path<UniplacesBuilding>(L_BUILDING, this);
            return _building;
        }

    /**
     * @return Назначение здания. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesUnit#getPurpose()
     */
        public UniplacesBuildingPurpose.Path<UniplacesBuildingPurpose> purpose()
        {
            if(_purpose == null )
                _purpose = new UniplacesBuildingPurpose.Path<UniplacesBuildingPurpose>(L_PURPOSE, this);
            return _purpose;
        }

    /**
     * @return Тип нумерации помещений. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesUnit#getPlaceNumberingType()
     */
        public UniplacesPlaceNumberingType.Path<UniplacesPlaceNumberingType> placeNumberingType()
        {
            if(_placeNumberingType == null )
                _placeNumberingType = new UniplacesPlaceNumberingType.Path<UniplacesPlaceNumberingType>(L_PLACE_NUMBERING_TYPE, this);
            return _placeNumberingType;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesUnit#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(UniplacesUnitGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Идентификатор.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesUnit#getIdentifier()
     */
        public PropertyPath<String> identifier()
        {
            if(_identifier == null )
                _identifier = new PropertyPath<String>(UniplacesUnitGen.P_IDENTIFIER, this);
            return _identifier;
        }

    /**
     * @return Этажность.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesUnit#getFloorNumber()
     */
        public PropertyPath<Integer> floorNumber()
        {
            if(_floorNumber == null )
                _floorNumber = new PropertyPath<Integer>(UniplacesUnitGen.P_FLOOR_NUMBER, this);
            return _floorNumber;
        }

    /**
     * @return Площадь кв. м.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesUnit#getArea()
     */
        public PropertyPath<Long> area()
        {
            if(_area == null )
                _area = new PropertyPath<Long>(UniplacesUnitGen.P_AREA, this);
            return _area;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesUnit#getTitleExtended()
     */
        public SupportedPropertyPath<String> titleExtended()
        {
            if(_titleExtended == null )
                _titleExtended = new SupportedPropertyPath<String>(UniplacesUnitGen.P_TITLE_EXTENDED, this);
            return _titleExtended;
        }

        public Class getEntityClass()
        {
            return UniplacesUnit.class;
        }

        public String getEntityName()
        {
            return "uniplacesUnit";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitleExtended();
}
