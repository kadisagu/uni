/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.FloorPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniplaces.component.place.AbstractPlaceObjectPub.AbstractPlaceObjectPubModel;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;

/**
 * @author agolubenko
 * @since Aug 24, 2010
 */
@State(@Bind(key = "publisherId", binding = "floor.id"))
@Output(@Bind(key = "unitId", binding = "floor.unit.id"))
public class Model extends AbstractPlaceObjectPubModel
{
    private UniplacesFloor _floor = new UniplacesFloor();
    private DynamicListDataSource<UniplacesPlace> _dataSource;

    public String getTitle()
    {
        return String.format("Помещения здания №%s, блока: %s, этаж %s", getFloor().getUnit().getBuilding().getNumber(), getFloor().getUnit().getDisplayableTitle(), getFloor().getTitle());
    }

    public UniplacesFloor getFloor()
    {
        return _floor;
    }

    public void setFloor(UniplacesFloor floor)
    {
        _floor = floor;
    }

    public DynamicListDataSource<UniplacesPlace> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<UniplacesPlace> dataSource)
    {
        _dataSource = dataSource;
    }

    @Override
    public Long getObjectID()
    {
        return _floor.getId();
    }

    @Override
    public String initPostfix()
    {
        return "floor";
    }


}
