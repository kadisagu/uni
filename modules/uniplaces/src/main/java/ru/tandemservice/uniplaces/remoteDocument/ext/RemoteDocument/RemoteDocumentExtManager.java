/* $Id$ */
package ru.tandemservice.uniplaces.remoteDocument.ext.RemoteDocument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.remoteDocument.bo.RemoteDocument.RemoteDocumentManager;
import ru.tandemservice.uniplaces.base.bo.UniplacesDocuments.logic.UniplacesDocumentsDAO;

import java.util.Arrays;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 13.02.2015
 */
@Configuration
public class RemoteDocumentExtManager extends BusinessObjectExtensionManager
{
    public static final String UNIPLACES_DOCS_SERVICE_NAME = "uniplacesDocs";

    @Autowired
    private RemoteDocumentManager _remoteDocumentManager;

    @Bean
    public ItemListExtension<List<String>> remoteDocumentModuleExtension()
    {
        return itemListExtension(_remoteDocumentManager.moduleExtPoint())
                .add(UNIPLACES_DOCS_SERVICE_NAME, Arrays.asList(UniplacesDocumentsDAO.REMOTE_DOCUMENT_MODULE))
                .create();
    }

    @Bean
    @SuppressWarnings("unchecked")
    public ItemListExtension<String> remoteDocumentTaskExtension()
    {
        return itemListExtension(_remoteDocumentManager.tasksExtPoint()).add(UNIPLACES_DOCS_SERVICE_NAME, "Скан-копии документов зданий и помещений").create();
    }
}