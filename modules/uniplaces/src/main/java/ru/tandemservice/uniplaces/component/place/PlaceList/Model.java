/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.PlaceList;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceCondition;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

import java.util.List;

/**
 * @author agolubenko
 * @since Aug 23, 2010
 */
public class Model
{
    private IDataSettings _settings;

    private ISelectModel _buildingList;
    private ISelectModel _unitList;
    private ISelectModel _floorList;
    private ISelectModel _placeList;
    private ISelectModel _purposeList;
    private ISelectModel _orgUnitList;
    private List<UniplacesPlaceCondition> _conditionList;
    private ISelectModel _classroomTypeModel;

    private DynamicListDataSource<UniplacesPlace> _dataSource;

    @SuppressWarnings("unchecked")
    public List<UniplacesPlacePurpose> getPurposes()
    {
        return (List<UniplacesPlacePurpose>) getSettings().get("purposes");
    }

    @SuppressWarnings("unchecked")
    public List<UniplacesClassroomType> getClassroomTypes()
    {
        return (List<UniplacesClassroomType>) getSettings().get("classroomTypes");
    }

    public UniplacesPlaceCondition getCondition()
    {
        return (UniplacesPlaceCondition) getSettings().get("condition");
    }

    public UniplacesBuilding getBuilding()
    {
        return (UniplacesBuilding) getSettings().get("building");
    }

    public UniplacesUnit getUnit()
    {
        return (UniplacesUnit) getSettings().get("unit");
    }

    public UniplacesFloor getFloor()
    {
        return (UniplacesFloor) getSettings().get("floor");
    }

    public UniplacesPlace getPlace()
    {
        return (UniplacesPlace) getSettings().get("place");
    }

    public OrgUnit getOrgUnit()
    {
        return (OrgUnit) getSettings().get("orgUnit");
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public ISelectModel getPurposeList()
    {
        return _purposeList;
    }

    public void setPurposeList(ISelectModel purposeList)
    {
        _purposeList = purposeList;
    }

    public List<UniplacesPlaceCondition> getConditionList()
    {
        return _conditionList;
    }

    public void setConditionList(List<UniplacesPlaceCondition> conditionList)
    {
        _conditionList = conditionList;
    }

    public ISelectModel getBuildingList()
    {
        return _buildingList;
    }

    public void setBuildingList(ISelectModel buildingList)
    {
        _buildingList = buildingList;
    }

    public ISelectModel getUnitList()
    {
        return _unitList;
    }

    public void setUnitList(ISelectModel unitList)
    {
        _unitList = unitList;
    }

    public ISelectModel getFloorList()
    {
        return _floorList;
    }

    public void setFloorList(ISelectModel floorList)
    {
        _floorList = floorList;
    }

    public ISelectModel getPlaceList()
    {
        return _placeList;
    }

    public void setPlaceList(ISelectModel placeList)
    {
        _placeList = placeList;
    }

    public ISelectModel getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(ISelectModel orgUnitList)
    {
        _orgUnitList = orgUnitList;
    }

    public DynamicListDataSource<UniplacesPlace> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<UniplacesPlace> dataSource)
    {
        _dataSource = dataSource;
    }

    public ISelectModel getClassroomTypeModel()
    {
        return _classroomTypeModel;
    }

    public void setClassroomTypeModel(ISelectModel classroomTypeModel)
    {
        _classroomTypeModel = classroomTypeModel;
    }

    public Long getLengthMin()
    {
        return getSettings().get("lengthMin")!=null?  Math.round((Long)getSettings().get("lengthMin") * 1000.0d) : null;
    }

    public Long getLengthMax()
    {
        return  getSettings().get("lengthMax")!=null? Math.round((Long) getSettings().get("lengthMax")*1000.0d):null;
    }

    public Long getWidthMin()
    {
        return  getSettings().get("widthMin")!=null? Math.round((Long) getSettings().get("widthMin")*1000.0d):null;
    }

    public Long getWidthMax()
    {
        return  getSettings().get("widthMax")!=null? Math.round((Long) getSettings().get("widthMax")*1000.0d):null;
    }

    public Long getHeightMin()
    {
        return  getSettings().get("heightMin")!=null? Math.round((Long) getSettings().get("heightMin")*1000.0d):null;
    }

    public Long getHeightMax()
    {
        return  getSettings().get("heightMax")!=null? Math.round((Long) getSettings().get("heightMax")*1000.0d):null;
    }

    public Long getAreaMin()
    {
        return  getSettings().get("areaMin")!=null? Math.round((Long) getSettings().get("areaMin")*100.0d):null;
    }

    public Long getAreaMax()
    {
        return  getSettings().get("areaMax")!=null? Math.round((Long) getSettings().get("areaMax")*100.0d):null;
    }
}
