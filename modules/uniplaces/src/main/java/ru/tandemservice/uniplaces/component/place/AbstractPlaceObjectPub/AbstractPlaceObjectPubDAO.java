/* $Id$ */
package ru.tandemservice.uniplaces.component.place.AbstractPlaceObjectPub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 16.02.2015
 */
public abstract class AbstractPlaceObjectPubDAO<Model extends AbstractPlaceObjectPubModel> extends UniDao<Model>
{


    public void prepareDocumentDataSource(Model model)
    {
        DynamicListDataSource<UniplacesObjectToDocumentRelation> dataSource = model.getDocumentDataSource();
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UniplacesObjectToDocumentRelation.class, "doc")
                .where(eq(property("doc", UniplacesObjectToDocumentRelation.object().id()), value(model.getObjectID())));
        DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(UniplacesObjectToDocumentRelation.class, "doc");
        order.applyOrder(builder,dataSource.getEntityOrder());
        Long count = builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
        dataSource.setCountRow(count < 3 ? 3:count);
        UniBaseUtils.createPage(dataSource, builder, getSession());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        prepareDocumentDataSource(model);
    }
}