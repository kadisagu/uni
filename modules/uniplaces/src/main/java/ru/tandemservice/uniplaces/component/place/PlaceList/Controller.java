/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.PlaceList;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniplaces.UniplacesComponents;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;

import java.util.Collection;

/**
 * @author agolubenko
 * @since Aug 23, 2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);
        model.setSettings(component.getSettings());

        prepareDataSource(component);
    }

    protected void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<UniplacesPlace> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new CheckboxColumn("select"));
        dataSource.addColumn(new SimpleColumn("Номер", UniplacesPlace.number().s()));
        dataSource.addColumn(new SimpleColumn("Название", UniplacesPlace.title().s()));
        dataSource.addColumn(new SimpleColumn("Кадастровый номер", UniplacesPlace.cadastreRecordNumber().s()));
        dataSource.addColumn(new SimpleColumn("Площадь, кв. м", UniplacesPlace.P_FRACTIONAL_AREA, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Высота, м", UniplacesPlace.P_FRACTIONAL_HEIGHT, new DoubleFormatter(3, true)).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Длина, м", UniplacesPlace.P_FRACTIONAL_LENGTH, new DoubleFormatter(3, true)).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Ширина, м",  UniplacesPlace.P_FRACTIONAL_WIDTH, new DoubleFormatter(3, true)).setClickable(false));
        dataSource.addColumn(new PublisherLinkColumn("Этаж", "title", UniplacesPlace.floor()));
        dataSource.addColumn(new PublisherLinkColumn("Блок", "title",  UniplacesPlace.floor().unit()));
        dataSource.addColumn(new PublisherLinkColumn("Здание", "title", UniplacesPlace.floor().unit().building()));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditPlace").setPermissionKey("editPlace_list"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeletePlace", "Удалить помещение {0}?", UniplacesPlace.title().s()).setPermissionKey("deletePlace_list"));

        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        component.saveSettings();
        DynamicListDataSource<UniplacesPlace> dataSource = getModel(component).getDataSource();
        dataSource.showFirstPage();
        dataSource.refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        component.clearSettings();
        onClickSearch(component);
    }

    public void onClickEditPlace(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                UniplacesComponents.PLACE_ADD_EDIT, new ParametersMap().add("placeId", component.getListenerParameter()).add("floorId", null)
        ));
    }

    public void onClickDeletePlace(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }

    public void onClickMassEditPlace(IBusinessComponent component)
    {
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try  {
            final Model model = this.getModel(component);
            final CheckboxColumn checkboxColumn = (CheckboxColumn)model.getDataSource().getColumn("select");
            final Collection<Long> ids = UniBaseDao.ids(checkboxColumn.getSelectedObjects());
            if (ids.isEmpty()) { throw new ApplicationException("Не выбрано ни одно помещение."); }
                ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                    UniplacesComponents.PLACE_MASS_EDIT, new ParametersMap().add("placeIds", ids)
                ));
        } finally {
            eventLock.release();
        }
    }

    public void onClickMassDeletePlace(IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final CheckboxColumn checkboxColumn = (CheckboxColumn)model.getDataSource().getColumn("select");
        final Collection<Long> ids = UniBaseDao.ids(checkboxColumn.getSelectedObjects());
        if (ids.isEmpty()) { throw new ApplicationException("Не выбрано ни одно помещение."); }
        int itemsDeleted = getDao().deletePlacesList(ids);
        ContextLocal.getInfoCollector().add("Из выбранных помещений ("+ids.size()+") удалено: " + itemsDeleted + ", невозможно удалить: "+(ids.size() - itemsDeleted)+".");
        checkboxColumn.getSelectedObjects().clear();
    }
}
