/* $Id$ */
package ru.tandemservice.uniplaces.base.bo.UniplacesDocuments.ui.AddEdit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Ekaterina Zvereva
 * @since 12.02.2015
 */
@Configuration
public class UniplacesDocumentsAddEdit extends BusinessComponentManager
{
}