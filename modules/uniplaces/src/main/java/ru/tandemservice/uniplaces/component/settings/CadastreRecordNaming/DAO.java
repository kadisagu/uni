/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.settings.CadastreRecordNaming;

import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniplaces.UniplacesDefines;
import ru.tandemservice.uniplaces.entity.place.UniplacesGround;
import ru.tandemservice.uniplaces.settings.CadastreRecordNamingSettings;

/**
 * @author agolubenko
 * @since Aug 4, 2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        IDataSettings settings = DataSettingsFacade.getSettings("", UniplacesDefines.MODULE_SETTINGS_KEY);
        model.setNamingSettings((CadastreRecordNamingSettings) settings.get(UniplacesDefines.SETTINGS_CADASTRE_RECORD_NAMING));
        if (null == model.getNamingSettings())
            model.setNamingSettings(new CadastreRecordNamingSettings());
        model.setDisabled(existsEntity(UniplacesGround.class));
    }

    @Override
    public void update(Model model)
    {
        IDataSettings settings = DataSettingsFacade.getSettings("", UniplacesDefines.MODULE_SETTINGS_KEY);
        settings.set(UniplacesDefines.SETTINGS_CADASTRE_RECORD_NAMING, model.getNamingSettings());
        DataSettingsFacade.saveSettings(settings);
    }
}
