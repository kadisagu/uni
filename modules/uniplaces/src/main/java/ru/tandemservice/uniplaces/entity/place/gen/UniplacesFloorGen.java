package ru.tandemservice.uniplaces.entity.place.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesRegistryRecord;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Этаж
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniplacesFloorGen extends UniplacesRegistryRecord
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplaces.entity.place.UniplacesFloor";
    public static final String ENTITY_NAME = "uniplacesFloor";
    public static final int VERSION_HASH = -164836420;
    private static IEntityMeta ENTITY_META;

    public static final String L_UNIT = "unit";
    public static final String P_TITLE = "title";
    public static final String P_NUMBER = "number";
    public static final String P_TITLE_EXTENDED = "titleExtended";

    private UniplacesUnit _unit;     // Блок
    private String _title;     // Название
    private int _number;     // Порядковый номер

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Блок. Свойство не может быть null.
     */
    @NotNull
    public UniplacesUnit getUnit()
    {
        return _unit;
    }

    /**
     * @param unit Блок. Свойство не может быть null.
     */
    public void setUnit(UniplacesUnit unit)
    {
        dirty(_unit, unit);
        _unit = unit;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Порядковый номер. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Порядковый номер. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UniplacesFloorGen)
        {
            setUnit(((UniplacesFloor)another).getUnit());
            setTitle(((UniplacesFloor)another).getTitle());
            setNumber(((UniplacesFloor)another).getNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniplacesFloorGen> extends UniplacesRegistryRecord.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniplacesFloor.class;
        }

        public T newInstance()
        {
            return (T) new UniplacesFloor();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "unit":
                    return obj.getUnit();
                case "title":
                    return obj.getTitle();
                case "number":
                    return obj.getNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "unit":
                    obj.setUnit((UniplacesUnit) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "unit":
                        return true;
                case "title":
                        return true;
                case "number":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "unit":
                    return true;
                case "title":
                    return true;
                case "number":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "unit":
                    return UniplacesUnit.class;
                case "title":
                    return String.class;
                case "number":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniplacesFloor> _dslPath = new Path<UniplacesFloor>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniplacesFloor");
    }
            

    /**
     * @return Блок. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesFloor#getUnit()
     */
    public static UniplacesUnit.Path<UniplacesUnit> unit()
    {
        return _dslPath.unit();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesFloor#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Порядковый номер. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesFloor#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesFloor#getTitleExtended()
     */
    public static SupportedPropertyPath<String> titleExtended()
    {
        return _dslPath.titleExtended();
    }

    public static class Path<E extends UniplacesFloor> extends UniplacesRegistryRecord.Path<E>
    {
        private UniplacesUnit.Path<UniplacesUnit> _unit;
        private PropertyPath<String> _title;
        private PropertyPath<Integer> _number;
        private SupportedPropertyPath<String> _titleExtended;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Блок. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesFloor#getUnit()
     */
        public UniplacesUnit.Path<UniplacesUnit> unit()
        {
            if(_unit == null )
                _unit = new UniplacesUnit.Path<UniplacesUnit>(L_UNIT, this);
            return _unit;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesFloor#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(UniplacesFloorGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Порядковый номер. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesFloor#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(UniplacesFloorGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesFloor#getTitleExtended()
     */
        public SupportedPropertyPath<String> titleExtended()
        {
            if(_titleExtended == null )
                _titleExtended = new SupportedPropertyPath<String>(UniplacesFloorGen.P_TITLE_EXTENDED, this);
            return _titleExtended;
        }

        public Class getEntityClass()
        {
            return UniplacesFloor.class;
        }

        public String getEntityName()
        {
            return "uniplacesFloor";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitleExtended();
}
