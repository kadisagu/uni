/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.PlaceAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniplaces.dao.IUniplacesDAO;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceCondition;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.uniplaces.util.UniplacesClassroomTypeSelectModel;

/**
 * @author agolubenko
 * @since Aug 23, 2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        UniplacesPlace place = model.getPlace();

        if (model.isEditForm())
        {
            place = getNotNull(UniplacesPlace.class, place.getId());
            model.setPlace(place);
        }
        else
        {
            place.setFloor(getNotNull(UniplacesFloor.class, model.getFloorId()));
        }

        model.setOrgUnitList(IUniplacesDAO.instance.get().getResponsibleOrgUnitModel());
        model.setClassroomTypeModel(new UniplacesClassroomTypeSelectModel());
        model.setPurposeList(HierarchyUtil.listHierarchyNodesWithParents(getCatalogItemListOrderByCode(UniplacesPlacePurpose.class), true));
        model.setConditionList(getCatalogItemListOrderByCode(UniplacesPlaceCondition.class));
    }

    @Override
    public void update(Model model)
    {
        UniplacesPlace place = model.getPlace();
        getSession().saveOrUpdate(place);

        boolean habitable = IUniplacesDAO.instance.get().getHabitablePlacePurposeList().contains(place.getPurpose());
        UniplacesHabitablePlace habitablePlace = get(UniplacesHabitablePlace.class, UniplacesHabitablePlace.place().s(), place);

        if (habitable)
        {
            if (habitablePlace == null)
            {
                habitablePlace = new UniplacesHabitablePlace();
                habitablePlace.setPlace(place);
                habitablePlace.setBedNumber(0);
                save(habitablePlace);
            }
        }
        else if (habitablePlace != null)
        {
            // TODO переместить в архив
            delete(habitablePlace);
        }
    }
}