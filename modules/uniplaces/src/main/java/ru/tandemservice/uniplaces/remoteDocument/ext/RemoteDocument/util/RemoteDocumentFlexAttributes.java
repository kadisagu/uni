/* $Id$ */
package ru.tandemservice.uniplaces.remoteDocument.ext.RemoteDocument.util;

/**
 * @author Ekaterina Zvereva
 * @since 13.02.2015
 */
public interface RemoteDocumentFlexAttributes
{
    public static final String UNI_PLACES_DOCUMENT_OWNER_OBJECT = "uniplacesDocumentOwnerObject";
    public static final String UNI_PLACES_DOCUMENT_TITLE = "uniplacesDocumentTitle";
    public static final String UNI_PLACES_DOCUMENT_START_DATE = "uniplacesDocumentStartDate";
    public static final String UNI_PLACES_DOCUMENT_END_DATE = "uniplacesDocumentEndDate";
    public static final String UNI_PLACES_DOCUMENT_COMMENT = "uniplacesDocumentComment";
    public static final String UNI_PLACES_DOCUMENT_ACTIVE = "uniplacesDocumentActive";


}