package ru.tandemservice.uniplaces.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Назначение помещения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniplacesPlacePurposeGen extends EntityBase
 implements INaturalIdentifiable<UniplacesPlacePurposeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose";
    public static final String ENTITY_NAME = "uniplacesPlacePurpose";
    public static final int VERSION_HASH = -1450995037;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_PARENT = "parent";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_CHILDS_ALLOWED = "childsAllowed";
    public static final String P_COUNT_BED_NUMBER = "countBedNumber";
    public static final String L_SEX = "sex";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private UniplacesPlacePurpose _parent;     // Назначение помещения
    private String _shortTitle;     // Сокращенное название
    private boolean _childsAllowed;     // Разрешено добавлять дочерние элементы
    private boolean _countBedNumber;     // Использовать койкоместа при расчете заполненности
    private Sex _sex;     // Пол
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Назначение помещения.
     */
    public UniplacesPlacePurpose getParent()
    {
        return _parent;
    }

    /**
     * @param parent Назначение помещения.
     */
    public void setParent(UniplacesPlacePurpose parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Разрешено добавлять дочерние элементы. Свойство не может быть null.
     */
    @NotNull
    public boolean isChildsAllowed()
    {
        return _childsAllowed;
    }

    /**
     * @param childsAllowed Разрешено добавлять дочерние элементы. Свойство не может быть null.
     */
    public void setChildsAllowed(boolean childsAllowed)
    {
        dirty(_childsAllowed, childsAllowed);
        _childsAllowed = childsAllowed;
    }

    /**
     * @return Использовать койкоместа при расчете заполненности. Свойство не может быть null.
     */
    @NotNull
    public boolean isCountBedNumber()
    {
        return _countBedNumber;
    }

    /**
     * @param countBedNumber Использовать койкоместа при расчете заполненности. Свойство не может быть null.
     */
    public void setCountBedNumber(boolean countBedNumber)
    {
        dirty(_countBedNumber, countBedNumber);
        _countBedNumber = countBedNumber;
    }

    /**
     * @return Пол.
     */
    public Sex getSex()
    {
        return _sex;
    }

    /**
     * @param sex Пол.
     */
    public void setSex(Sex sex)
    {
        dirty(_sex, sex);
        _sex = sex;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniplacesPlacePurposeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((UniplacesPlacePurpose)another).getCode());
            }
            setParent(((UniplacesPlacePurpose)another).getParent());
            setShortTitle(((UniplacesPlacePurpose)another).getShortTitle());
            setChildsAllowed(((UniplacesPlacePurpose)another).isChildsAllowed());
            setCountBedNumber(((UniplacesPlacePurpose)another).isCountBedNumber());
            setSex(((UniplacesPlacePurpose)another).getSex());
            setTitle(((UniplacesPlacePurpose)another).getTitle());
        }
    }

    public INaturalId<UniplacesPlacePurposeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<UniplacesPlacePurposeGen>
    {
        private static final String PROXY_NAME = "UniplacesPlacePurposeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UniplacesPlacePurposeGen.NaturalId) ) return false;

            UniplacesPlacePurposeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniplacesPlacePurposeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniplacesPlacePurpose.class;
        }

        public T newInstance()
        {
            return (T) new UniplacesPlacePurpose();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "parent":
                    return obj.getParent();
                case "shortTitle":
                    return obj.getShortTitle();
                case "childsAllowed":
                    return obj.isChildsAllowed();
                case "countBedNumber":
                    return obj.isCountBedNumber();
                case "sex":
                    return obj.getSex();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "parent":
                    obj.setParent((UniplacesPlacePurpose) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "childsAllowed":
                    obj.setChildsAllowed((Boolean) value);
                    return;
                case "countBedNumber":
                    obj.setCountBedNumber((Boolean) value);
                    return;
                case "sex":
                    obj.setSex((Sex) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "parent":
                        return true;
                case "shortTitle":
                        return true;
                case "childsAllowed":
                        return true;
                case "countBedNumber":
                        return true;
                case "sex":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "parent":
                    return true;
                case "shortTitle":
                    return true;
                case "childsAllowed":
                    return true;
                case "countBedNumber":
                    return true;
                case "sex":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "parent":
                    return UniplacesPlacePurpose.class;
                case "shortTitle":
                    return String.class;
                case "childsAllowed":
                    return Boolean.class;
                case "countBedNumber":
                    return Boolean.class;
                case "sex":
                    return Sex.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniplacesPlacePurpose> _dslPath = new Path<UniplacesPlacePurpose>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniplacesPlacePurpose");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Назначение помещения.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose#getParent()
     */
    public static UniplacesPlacePurpose.Path<UniplacesPlacePurpose> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Разрешено добавлять дочерние элементы. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose#isChildsAllowed()
     */
    public static PropertyPath<Boolean> childsAllowed()
    {
        return _dslPath.childsAllowed();
    }

    /**
     * @return Использовать койкоместа при расчете заполненности. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose#isCountBedNumber()
     */
    public static PropertyPath<Boolean> countBedNumber()
    {
        return _dslPath.countBedNumber();
    }

    /**
     * @return Пол.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose#getSex()
     */
    public static Sex.Path<Sex> sex()
    {
        return _dslPath.sex();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends UniplacesPlacePurpose> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private UniplacesPlacePurpose.Path<UniplacesPlacePurpose> _parent;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<Boolean> _childsAllowed;
        private PropertyPath<Boolean> _countBedNumber;
        private Sex.Path<Sex> _sex;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(UniplacesPlacePurposeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Назначение помещения.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose#getParent()
     */
        public UniplacesPlacePurpose.Path<UniplacesPlacePurpose> parent()
        {
            if(_parent == null )
                _parent = new UniplacesPlacePurpose.Path<UniplacesPlacePurpose>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(UniplacesPlacePurposeGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Разрешено добавлять дочерние элементы. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose#isChildsAllowed()
     */
        public PropertyPath<Boolean> childsAllowed()
        {
            if(_childsAllowed == null )
                _childsAllowed = new PropertyPath<Boolean>(UniplacesPlacePurposeGen.P_CHILDS_ALLOWED, this);
            return _childsAllowed;
        }

    /**
     * @return Использовать койкоместа при расчете заполненности. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose#isCountBedNumber()
     */
        public PropertyPath<Boolean> countBedNumber()
        {
            if(_countBedNumber == null )
                _countBedNumber = new PropertyPath<Boolean>(UniplacesPlacePurposeGen.P_COUNT_BED_NUMBER, this);
            return _countBedNumber;
        }

    /**
     * @return Пол.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose#getSex()
     */
        public Sex.Path<Sex> sex()
        {
            if(_sex == null )
                _sex = new Sex.Path<Sex>(L_SEX, this);
            return _sex;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(UniplacesPlacePurposeGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return UniplacesPlacePurpose.class;
        }

        public String getEntityName()
        {
            return "uniplacesPlacePurpose";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
