/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.catalog.uniplacesPlacePurpose.UniplacesPlacePurposeAddEdit;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;

import java.util.List;

/**
 * @author agolubenko
 * @since Sep 1, 2010
 */
public class DAO extends DefaultCatalogAddEditDAO<UniplacesPlacePurpose, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setPlacePurposeList(HierarchyUtil.listHierarchyNodesWithParents(getPlacePurposes(), CommonCatalogUtil.CATALOG_CODE_COMPARATOR, false));
    }

    @SuppressWarnings("unchecked")
    private List<UniplacesPlacePurpose> getPlacePurposes()
    {
        Criteria criteria = getSession().createCriteria(UniplacesPlacePurpose.class);
        criteria.add(Restrictions.eq(UniplacesPlacePurpose.childsAllowed().s(), Boolean.TRUE));
        return criteria.list();
    }

    @Override
    public void update(Model model)
    {
        final UniplacesPlacePurpose purpose = model.getCatalogItem();
        if (purpose.getParent() == null)
        {
            purpose.setCountBedNumber(false);
            purpose.setSex(null);
        }
        else
        {
            purpose.setCountBedNumber(purpose.getParent().isCountBedNumber());
            purpose.setSex(purpose.getParent().getSex());
        }
        super.update(model);
    }
}
