/* $Id$ */
package ru.tandemservice.uniplaces.base.bo.UniplacesDocuments.ui.AddEdit;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniplaces.base.bo.UniplacesDocuments.UniplacesDocumentsManager;
import ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation;
import ru.tandemservice.uniplaces.entity.place.UniplacesRegistryRecord;

/**
 * @author Ekaterina Zvereva
 * @since 12.02.2015
 */
@Input(
        {@Bind(key = "objectId", binding = "objectId", required = true),
       @Bind(key="documentId", binding = "documentId")})
public class UniplacesDocumentsAddEditUI extends UIPresenter
{
    private Long _objectId;
    private Long _documentId;
    private UniplacesObjectToDocumentRelation _document;
    private IUploadFile _uploadFile;
    private String _fileName;

    public String getFileName()
    {
        return _fileName;
    }

    public void setFileName(String fileName)
    {
        _fileName = fileName;
    }

    public Long getDocumentId()
    {
        return _documentId;
    }

    public void setDocumentId(Long documentId)
    {
        _documentId = documentId;
    }

    public Long getObjectId()
    {
        return _objectId;
    }

    public void setObjectId(Long objectId)
    {
        _objectId = objectId;
    }

    public UniplacesObjectToDocumentRelation getDocument()
    {
        return _document;
    }

    public void setDocument(UniplacesObjectToDocumentRelation document)
    {
        _document = document;
    }

    public IUploadFile getUploadFile()
    {
        return _uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile)
    {
        _uploadFile = uploadFile;
    }

    public boolean isEditForm()
    {
        return _documentId != null;
    }

    public void onClickApply()
    {
        UniplacesDocumentsManager.instance().dao().saveOrUpdateDocument(getDocument(), getUploadFile());
        deactivate();
    }

    @Override
    public void onComponentRefresh()
    {
        UniplacesRegistryRecord object = DataAccessServices.dao().getNotNull(_objectId);
        if (_documentId !=null)
        {
            _document = DataAccessServices.dao().get(UniplacesObjectToDocumentRelation.class, _documentId);
            if (_document != null && _document.getDocument() != null)
                _fileName = UniplacesDocumentsManager.instance().dao().getDocument(_document.getDocument()).getFileName();

        }
        else
        {
            _document = new UniplacesObjectToDocumentRelation();
            _document.setObject(object);
        }
    }
}