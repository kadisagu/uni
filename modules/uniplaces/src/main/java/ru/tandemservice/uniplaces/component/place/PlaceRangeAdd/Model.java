/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.PlaceRangeAdd;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceCondition;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;

/**
 * @author agolubenko
 * @since Sep 1, 2010
 */
@Input(@Bind(key = "floorId", binding = "floor.id"))
public class Model
{
    private UniplacesFloor _floor = new UniplacesFloor();

    private String _prefix;
    private String _postfix;
    private int _digitsNumber = 3;
    private int _from;
    private int _to;

    private UniplacesPlacePurpose _placePurpose;
    private List<HSelectOption> _placePurposeList;

    private UniplacesPlaceCondition _placeCondition;
    private List<UniplacesPlaceCondition> _placeConditionList;

    private OrgUnit _responsibleOrgUnit;
    private ISelectModel _responsibleOrgUnitList;

    private UniplacesClassroomType _classroomType;
    private ISelectModel _classroomTypeModel;

    public UniplacesFloor getFloor()
    {
        return _floor;
    }

    public void setFloor(UniplacesFloor floor)
    {
        _floor = floor;
    }

    public String getPrefix()
    {
        return _prefix;
    }

    public void setPrefix(String prefix)
    {
        _prefix = prefix;
    }

    public String getPostfix()
    {
        return _postfix;
    }

    public void setPostfix(String postfix)
    {
        _postfix = postfix;
    }

    public int getDigitsNumber()
    {
        return _digitsNumber;
    }

    public void setDigitsNumber(int digitsNumber)
    {
        _digitsNumber = digitsNumber;
    }

    public int getFrom()
    {
        return _from;
    }

    public void setFrom(int from)
    {
        _from = from;
    }

    public int getTo()
    {
        return _to;
    }

    public void setTo(int to)
    {
        _to = to;
    }

    public UniplacesPlacePurpose getPlacePurpose()
    {
        return _placePurpose;
    }

    public void setPlacePurpose(UniplacesPlacePurpose placePurpose)
    {
        _placePurpose = placePurpose;
    }

    public List<HSelectOption> getPlacePurposeList()
    {
        return _placePurposeList;
    }

    public void setPlacePurposeList(List<HSelectOption> placePurposeList)
    {
        _placePurposeList = placePurposeList;
    }

    public UniplacesPlaceCondition getPlaceCondition()
    {
        return _placeCondition;
    }

    public void setPlaceCondition(UniplacesPlaceCondition placeCondition)
    {
        _placeCondition = placeCondition;
    }

    public List<UniplacesPlaceCondition> getPlaceConditionList()
    {
        return _placeConditionList;
    }

    public void setPlaceConditionList(List<UniplacesPlaceCondition> placeConditionList)
    {
        _placeConditionList = placeConditionList;
    }

    public OrgUnit getResponsibleOrgUnit()
    {
        return _responsibleOrgUnit;
    }

    public void setResponsibleOrgUnit(OrgUnit responsibleOrgUnit)
    {
        _responsibleOrgUnit = responsibleOrgUnit;
    }

    public ISelectModel getResponsibleOrgUnitList()
    {
        return _responsibleOrgUnitList;
    }

    public void setResponsibleOrgUnitList(ISelectModel responsibleOrgUnitList)
    {
        _responsibleOrgUnitList = responsibleOrgUnitList;
    }

    public UniplacesClassroomType getClassroomType()
    {
        return _classroomType;
    }

    public void setClassroomType(UniplacesClassroomType classroomType)
    {
        _classroomType = classroomType;
    }

    public ISelectModel getClassroomTypeModel()
    {
        return _classroomTypeModel;
    }

    public void setClassroomTypeModel(ISelectModel classroomTypeModel)
    {
        _classroomTypeModel = classroomTypeModel;
    }
}
