/* $Id$ */
package ru.tandemservice.uniplaces.component.place.AbstractPlaceObjectPub;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Ekaterina Zvereva
 * @since 16.02.2015
 */
public interface IAbstractPlaceObjectPubDAO<Model extends AbstractPlaceObjectPubModel> extends IUniDao<Model>
{
}