/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.settings.BuildingNaming;

import ru.tandemservice.uniplaces.settings.BuildingNamingSettings;

/**
 * @author agolubenko
 * @since Aug 17, 2010
 */
public class Model
{
    private BuildingNamingSettings _namingSettings;
    private boolean _disabled;

    public boolean isDisabled()
    {
        return _disabled;
    }

    public void setDisabled(boolean disabled)
    {
        _disabled = disabled;
    }

    public BuildingNamingSettings getNamingSettings()
    {
        return _namingSettings;
    }

    public void setNamingSettings(BuildingNamingSettings namingSettings)
    {
        _namingSettings = namingSettings;
    }
}
