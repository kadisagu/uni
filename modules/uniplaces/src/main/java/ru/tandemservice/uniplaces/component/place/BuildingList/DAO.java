/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.BuildingList;

import java.util.Collections;
import java.util.List;

import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import org.tandemframework.shared.commonbase.base.util.DelegatePropertyComparator;
import org.tandemframework.core.util.NumberAsStringComparator;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;

/**
 * @author agolubenko
 * @since Aug 10, 2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("building");
    static
    {
        _orderSettings.setOrders(UniplacesBuilding.responsibleOrgUnit().fullTitle().s(), new OrderDescription("orgUnitType", OrgUnitType.title().s()), new OrderDescription("orgUnit", OrgUnit.title().s()));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<UniplacesBuilding> dataSource = model.getDataSource();

        MQBuilder builder = new MQBuilder(UniplacesBuilding.ENTITY_CLASS, "building");
        builder.addLeftJoinFetch("building", UniplacesBuilding.responsibleOrgUnit().s(), "orgUnit");
        builder.addLeftJoinFetch("building", UniplacesBuilding.address().s(), "address");
        builder.addLeftJoin("orgUnit", OrgUnit.orgUnitType().s(), "orgUnitType");

        EntityOrder entityOrder = dataSource.getEntityOrder();
        if (entityOrder.getKey().equals(UniplacesBuilding.title().s()))
        {
            List<UniplacesBuilding> result = builder.getResultList(getSession());
            Collections.sort(result, new DelegatePropertyComparator(NumberAsStringComparator.INSTANCE, UniplacesBuilding.title().s()));
            if (entityOrder.getDirection() == OrderDirection.desc)
            {
                Collections.reverse(result);
            }
            UniBaseUtils.createPage(model.getDataSource(), result);
        }
        else
        {
            _orderSettings.applyOrder(builder, entityOrder);
            UniBaseUtils.createPage(dataSource, builder, getSession());
        }

    }
}
