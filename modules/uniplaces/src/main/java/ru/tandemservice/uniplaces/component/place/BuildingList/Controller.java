/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.BuildingList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniplaces.UniplacesComponents;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;

/**
 * @author agolubenko
 * @since Aug 10, 2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        prepareDataSource(component);
    }

    public void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<UniplacesBuilding> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Номер", UniplacesBuilding.fullNumber().s()));
        dataSource.addColumn(new SimpleColumn("Идентификатор", UniplacesBuilding.identifier().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Корпус", UniplacesBuilding.unit().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Название", UniplacesBuilding.title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Кадастровый номер", UniplacesBuilding.cadastreRecordNumber().s()));
        dataSource.addColumn(new SimpleColumn("Адрес расположения", UniplacesBuilding.address().titleWithFlat().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Право распоряжения", UniplacesBuilding.disposalRight().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Площадь, кв. м", UniplacesBuilding.P_FRACTIONAL_AREA, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED).setClickable(false));
        dataSource.addColumn(new PublisherLinkColumn("Ответственное подразделение", "fullTitle", UniplacesBuilding.responsibleOrgUnit()));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditBuilding").setPermissionKey("editBuilding_list"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteBuilding", "Удалить здание №{0}?", UniplacesBuilding.fullNumber().s()).setPermissionKey("deleteBuilding_list"));
        model.setDataSource(dataSource);
    }

    public void onClickAddBuilding(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(UniplacesComponents.BUILDING_ADD_EDIT, new ParametersMap().add("buildingId", null)));
    }

    public void onClickEditBuilding(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(UniplacesComponents.BUILDING_ADD_EDIT, new ParametersMap().add("buildingId", component.getListenerParameter())));
    }

    public void onClickDeleteBuilding(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }
}
