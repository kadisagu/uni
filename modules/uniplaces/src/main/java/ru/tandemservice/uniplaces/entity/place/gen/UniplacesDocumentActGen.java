package ru.tandemservice.uniplaces.entity.place.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniplaces.entity.place.UniplacesDocument;
import ru.tandemservice.uniplaces.entity.place.UniplacesDocumentAct;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Документ-акт
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniplacesDocumentActGen extends UniplacesDocument
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplaces.entity.place.UniplacesDocumentAct";
    public static final String ENTITY_NAME = "uniplacesDocumentAct";
    public static final int VERSION_HASH = 2044802282;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UniplacesDocumentActGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniplacesDocumentActGen> extends UniplacesDocument.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniplacesDocumentAct.class;
        }

        public T newInstance()
        {
            return (T) new UniplacesDocumentAct();
        }
    }
    private static final Path<UniplacesDocumentAct> _dslPath = new Path<UniplacesDocumentAct>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniplacesDocumentAct");
    }
            

    public static class Path<E extends UniplacesDocumentAct> extends UniplacesDocument.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return UniplacesDocumentAct.class;
        }

        public String getEntityName()
        {
            return "uniplacesDocumentAct";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
