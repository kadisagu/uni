package ru.tandemservice.uniplaces.entity.place;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.uniplaces.entity.place.gen.UniplacesHabitablePlaceGen;

/**
 * Жилое помещение
 */
public class UniplacesHabitablePlace extends UniplacesHabitablePlaceGen implements ITitled
{
    public static final String P_ADDRESS_TITLE = "addressTitle";

    @Override
    public String getTitle()
    {
        if (getPlace() == null) {
            return this.getClass().getSimpleName();
        }
        return getAddressTitle();
    }

    @Override
    @EntityDSLSupport
    public String getAddressTitle()
    {
        return getPlace().getFloor().getUnit().getBuilding().getAddress().getTitle() + ", пом. №" + getPlace().getNumber();  
    }

    @EntityDSLSupport
    public void setAddressTitle(String title)
    {
    }
}