/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.BuildingPub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniplaces.component.place.AbstractPlaceObjectPub.AbstractPlaceObjectPubDAO;
import ru.tandemservice.uniplaces.component.place.AbstractPlaceObjectPub.AbstractPlaceObjectPubModel;
import ru.tandemservice.uniplaces.entity.place.*;

import java.util.List;

/**
 * @author agolubenko
 * @since Aug 16, 2010
 */
public class DAO extends AbstractPlaceObjectPubDAO<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        UniplacesBuilding building = getNotNull(UniplacesBuilding.class, ((Model)model).getBuilding().getId());

        model.setBuilding(building);
        model.setAssignedDocuments(getList(UniplacesAssignedDocument.class, UniplacesAssignedDocument.registryRecord().s(), building));

        model.setUnitArea(.0);
        MQBuilder unitAreas = new MQBuilder(UniplacesUnit.ENTITY_CLASS, "unit");
        unitAreas.add(MQExpression.isNotNull("unit", UniplacesUnit.area()));
        unitAreas.add(MQExpression.eq("unit", UniplacesUnit.building(), model.getBuilding()));
        for (UniplacesUnit unit : unitAreas.<UniplacesUnit>getResultList(getSession()))
            model.setUnitArea(model.getUnitArea() + unit.getFractionalArea());

        model.setPlacesArea(.0);
        MQBuilder placeAreas = new MQBuilder(UniplacesPlace.ENTITY_CLASS, "place");
        placeAreas.add(MQExpression.isNotNull("place", UniplacesPlace.area()));
        placeAreas.add(MQExpression.eq("place", UniplacesPlace.floor().unit().building(), model.getBuilding()));
        for (UniplacesPlace place : placeAreas.<UniplacesPlace>getResultList(getSession()))
            model.setPlacesArea(model.getPlacesArea() + place.getFractionalArea());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        super.prepareListDataSource(model);
        MQBuilder builder = new MQBuilder(UniplacesUnit.ENTITY_CLASS, "t");
        builder.add(MQExpression.eq("t", UniplacesUnit.L_BUILDING, model.getBuilding()));
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

}
