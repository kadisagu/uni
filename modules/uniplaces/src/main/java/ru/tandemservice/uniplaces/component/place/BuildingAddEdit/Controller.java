/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.BuildingAddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInline;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineConfig;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineUI;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;

/**
 * @author agolubenko
 * @since Aug 16, 2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    public static final String REGION_ADDRESS = "addressRegion";

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        AddressBaseEditInlineConfig addressConfig = new AddressBaseEditInlineConfig();
        addressConfig.setEntityId(model.getBuilding().getId() == null ? null : model.getBuilding().getId());
        addressConfig.setDetailedOnly(true);
        addressConfig.setDetailLevel(4);
        addressConfig.setAreaVisible(true);
        addressConfig.setInline(true);
        addressConfig.setFieldSetTitle("Адрес здания");
        addressConfig.setAddressProperty(UniplacesBuilding.L_ADDRESS);
        component.createChildRegion(REGION_ADDRESS, new ComponentActivator(AddressBaseEditInline.class.getSimpleName(), new ParametersMap().add(AddressBaseEditInlineUI.BIND_CONFIG, addressConfig)));

    }

    public synchronized void onClickApply(IBusinessComponent component)
    {
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        Model model = getModel(component);
        getDao().validate(model, errors);
        if (errors.hasErrors())
            return;

        AddressBaseEditInlineUI addressBaseEditInlineUI = (AddressBaseEditInlineUI) component.getChildRegion(REGION_ADDRESS).getActiveComponent().getPresenter();
        if(addressBaseEditInlineUI.getResult() == null) throw new RuntimeException("Адрес обязателен");
        if(!(addressBaseEditInlineUI.getResult() instanceof AddressDetailed)) throw new RuntimeException("Некорректный адрес");
        AddressDetailed address = (AddressDetailed) addressBaseEditInlineUI.getResult();

        model.setAddress(address);


        getDao().update(model);
        deactivate(component);
    }
}
