/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.UnitPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniplaces.component.place.AbstractPlaceObjectPub.AbstractPlaceObjectPubModel;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

/**
 * @author agolubenko
 * @since Aug 20, 2010
 */
@State(@Bind(key = "publisherId", binding = "unit.id"))
@Output(@Bind(key = "unitId", binding = "unit.id"))
public class Model extends AbstractPlaceObjectPubModel
{
    private UniplacesUnit _unit = new UniplacesUnit();
    private DynamicListDataSource<UniplacesFloor> _dataSource;

    //Getters & Setters
    public DynamicListDataSource<UniplacesFloor> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<UniplacesFloor> dataSource)
    {
        _dataSource = dataSource;
    }

    public UniplacesUnit getUnit()
    {
        return _unit;
    }

    public void setUnit(UniplacesUnit unit)
    {
        _unit = unit;
    }

    @Override
    public Long getObjectID()
    {
        return _unit.getId();
    }

    @Override
    public String initPostfix()
    {
        return "unit";
    }

}
