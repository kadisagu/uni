/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.dao;

import java.util.List;
import java.util.Map;

import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;

/**
 * @author agolubenko
 * @since Aug 16, 2010
 */
public interface IUniplacesDAO
{
    final SpringBeanCache<IUniplacesDAO> instance = new SpringBeanCache<IUniplacesDAO>(IUniplacesDAO.class.getName());

    ISelectModel getResponsibleOrgUnitModel();

    <T extends IHierarchyItem> Map<T, List<T>> getTree(Class<T> clazz);

    <T extends IHierarchyItem> List<T> getSubTree(T node);

    List<UniplacesPlacePurpose> getHabitablePlacePurposeList();

    List<HSelectOption> getHabitablePlacePurposeOptionList();
}
