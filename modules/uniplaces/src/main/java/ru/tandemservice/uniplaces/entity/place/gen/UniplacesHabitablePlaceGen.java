package ru.tandemservice.uniplaces.entity.place.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Жилое помещение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniplacesHabitablePlaceGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace";
    public static final String ENTITY_NAME = "uniplacesHabitablePlace";
    public static final int VERSION_HASH = -508560727;
    private static IEntityMeta ENTITY_META;

    public static final String L_PLACE = "place";
    public static final String P_BED_NUMBER = "bedNumber";
    public static final String P_ADDRESS_TITLE = "addressTitle";

    private UniplacesPlace _place;     // Помещение
    private int _bedNumber;     // Число койко-мест

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Помещение. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public UniplacesPlace getPlace()
    {
        return _place;
    }

    /**
     * @param place Помещение. Свойство не может быть null и должно быть уникальным.
     */
    public void setPlace(UniplacesPlace place)
    {
        dirty(_place, place);
        _place = place;
    }

    /**
     * @return Число койко-мест. Свойство не может быть null.
     */
    @NotNull
    public int getBedNumber()
    {
        return _bedNumber;
    }

    /**
     * @param bedNumber Число койко-мест. Свойство не может быть null.
     */
    public void setBedNumber(int bedNumber)
    {
        dirty(_bedNumber, bedNumber);
        _bedNumber = bedNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniplacesHabitablePlaceGen)
        {
            setPlace(((UniplacesHabitablePlace)another).getPlace());
            setBedNumber(((UniplacesHabitablePlace)another).getBedNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniplacesHabitablePlaceGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniplacesHabitablePlace.class;
        }

        public T newInstance()
        {
            return (T) new UniplacesHabitablePlace();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "place":
                    return obj.getPlace();
                case "bedNumber":
                    return obj.getBedNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "place":
                    obj.setPlace((UniplacesPlace) value);
                    return;
                case "bedNumber":
                    obj.setBedNumber((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "place":
                        return true;
                case "bedNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "place":
                    return true;
                case "bedNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "place":
                    return UniplacesPlace.class;
                case "bedNumber":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniplacesHabitablePlace> _dslPath = new Path<UniplacesHabitablePlace>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniplacesHabitablePlace");
    }
            

    /**
     * @return Помещение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace#getPlace()
     */
    public static UniplacesPlace.Path<UniplacesPlace> place()
    {
        return _dslPath.place();
    }

    /**
     * @return Число койко-мест. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace#getBedNumber()
     */
    public static PropertyPath<Integer> bedNumber()
    {
        return _dslPath.bedNumber();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace#getAddressTitle()
     */
    public static SupportedPropertyPath<String> addressTitle()
    {
        return _dslPath.addressTitle();
    }

    public static class Path<E extends UniplacesHabitablePlace> extends EntityPath<E>
    {
        private UniplacesPlace.Path<UniplacesPlace> _place;
        private PropertyPath<Integer> _bedNumber;
        private SupportedPropertyPath<String> _addressTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Помещение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace#getPlace()
     */
        public UniplacesPlace.Path<UniplacesPlace> place()
        {
            if(_place == null )
                _place = new UniplacesPlace.Path<UniplacesPlace>(L_PLACE, this);
            return _place;
        }

    /**
     * @return Число койко-мест. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace#getBedNumber()
     */
        public PropertyPath<Integer> bedNumber()
        {
            if(_bedNumber == null )
                _bedNumber = new PropertyPath<Integer>(UniplacesHabitablePlaceGen.P_BED_NUMBER, this);
            return _bedNumber;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace#getAddressTitle()
     */
        public SupportedPropertyPath<String> addressTitle()
        {
            if(_addressTitle == null )
                _addressTitle = new SupportedPropertyPath<String>(UniplacesHabitablePlaceGen.P_ADDRESS_TITLE, this);
            return _addressTitle;
        }

        public Class getEntityClass()
        {
            return UniplacesHabitablePlace.class;
        }

        public String getEntityName()
        {
            return "uniplacesHabitablePlace";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getAddressTitle();
}
