/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.GroundPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.State;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniplaces.component.place.AbstractPlaceObjectPub.AbstractPlaceObjectPubModel;
import ru.tandemservice.uniplaces.entity.place.UniplacesAssignedDocument;
import ru.tandemservice.uniplaces.entity.place.UniplacesGround;
import ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation;

/**
 * @author agolubenko
 * @since Aug 3, 2010
 */
@State(@Bind(key = "publisherId", binding = "ground.id"))
@Output(@Bind(key = "groundId", binding = "ground.id"))
public class Model extends AbstractPlaceObjectPubModel
{
    private UniplacesGround _ground = new UniplacesGround();
    private UniplacesAssignedDocument _assignedDocument;
    private DynamicListDataSource<UniplacesObjectToDocumentRelation> _dataSource;

    public DynamicListDataSource<UniplacesObjectToDocumentRelation> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<UniplacesObjectToDocumentRelation> dataSource)
    {
        _dataSource = dataSource;
    }

    public UniplacesGround getGround()
    {
        return _ground;
    }

    public void setGround(UniplacesGround ground)
    {
        _ground = ground;
    }

    public UniplacesAssignedDocument getAssignedDocument()
    {
        return _assignedDocument;
    }

    public void setAssignedDocument(UniplacesAssignedDocument assignedDocument)
    {
        _assignedDocument = assignedDocument;
    }

    @Override
    public Long getObjectID()
    {
        return _ground.getId();
    }

    @Override
    public String initPostfix()
    {
        return "ground";
    }

}
