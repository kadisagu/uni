// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.PlaceMassEdit;

import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniplaces.dao.IUniplacesDAO;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceCondition;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.uniplaces.util.UniplacesClassroomTypeSelectModel;

import java.util.List;

/**
 * @author oleyba
 * @since 24.09.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("place");
    static
    {
        _orderSettings.setOrders(UniplacesPlace.P_FRACTIONAL_AREA, new OrderDescription("place", UniplacesPlace.P_AREA));
    }


    @Override
    public void prepare(final Model model)
    {
        final Session session = getSession();
        model.setPurposeList(HierarchyUtil.listHierarchyNodesWithParents(getCatalogItemListOrderByCode(UniplacesPlacePurpose.class), true));
        model.setConditionList(new LazySimpleSelectModel<UniplacesPlaceCondition>(UniplacesPlaceCondition.class));
        List<OrgUnit> orgUnits = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou").add(MQExpression.notEq("ou", OrgUnit.orgUnitType().code().s(), OrgUnitTypeCodes.TOP)).<OrgUnit>getResultList(session);
        model.setOrgUnitList(IUniplacesDAO.instance.get().getResponsibleOrgUnitModel());
        model.setClassroomTypeModel(new UniplacesClassroomTypeSelectModel());
    }

    @Override
    public void update(Model model)
    {
        for (Long id : model.getPlaceIds())
        {
            UniplacesPlace place = get(UniplacesPlace.class, id);
            if (model.getNewCondition() != null)
                place.setCondition(model.getNewCondition());
            if (model.getNewPurpose() != null)
                place.setPurpose(model.getNewPurpose());
            if (model.getNewOrgUnit() != null)
                place.setResponsibleOrgUnit(model.getNewOrgUnit());
            if(model.getClassroomType() != null)
                place.setClassroomType(model.getClassroomType());
            update(place);

            boolean habitable = IUniplacesDAO.instance.get().getHabitablePlacePurposeList().contains(place.getPurpose());
            UniplacesHabitablePlace habitablePlace = get(UniplacesHabitablePlace.class, UniplacesHabitablePlace.place().s(), place);

            if (habitable)
            {
                if (habitablePlace == null)
                {
                    habitablePlace = new UniplacesHabitablePlace();
                    habitablePlace.setPlace(place);
                    habitablePlace.setBedNumber(0);
                    save(habitablePlace);
                }
            }
            else if (habitablePlace != null)
            {
                // TODO переместить в архив
                delete(habitablePlace);
            }
        }
    }
}
