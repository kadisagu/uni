package ru.tandemservice.uniplaces.entity.place;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uniplaces.entity.place.gen.UniplacesFloorGen;

/**
 * Этаж
 */
public class UniplacesFloor extends UniplacesFloorGen
{
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=32473371")
    @EntityDSLSupport
    public String getTitleExtended()
    {
        return getTitle() + " (" + getUnit().getBuilding().getShortTitle() + ", блок: " + getUnit().getTitle() +  ")";
    }

}