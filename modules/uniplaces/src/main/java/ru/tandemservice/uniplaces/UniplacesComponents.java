/* $Id$ */
/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces;

/**
 * @author agolubenko
 * @since Aug 19, 2010
 */
public interface UniplacesComponents
{
    String GROUND_ADD_EDIT = "ru.tandemservice.uniplaces.component.place.GroundAddEdit";
    String BUILDING_ADD_EDIT = "ru.tandemservice.uniplaces.component.place.BuildingAddEdit";
    String UNIT_ADD_EDIT = "ru.tandemservice.uniplaces.component.place.UnitAddEdit";
    String FLOOR_ADD_EDIT = "ru.tandemservice.uniplaces.component.place.FloorAddEdit";
    String PLACE_ADD_EDIT = "ru.tandemservice.uniplaces.component.place.PlaceAddEdit";
    String FLOOR_RANGE_ADD = "ru.tandemservice.uniplaces.component.place.FloorRangeAdd";
    String PLACE_RANGE_ADD = "ru.tandemservice.uniplaces.component.place.PlaceRangeAdd";
    String PLACE_MASS_EDIT = "ru.tandemservice.uniplaces.component.place.PlaceMassEdit";
}
