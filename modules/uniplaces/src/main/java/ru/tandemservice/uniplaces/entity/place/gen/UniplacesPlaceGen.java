package ru.tandemservice.uniplaces.entity.place.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceCondition;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesRegistryRecord;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Помещение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniplacesPlaceGen extends UniplacesRegistryRecord
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplaces.entity.place.UniplacesPlace";
    public static final String ENTITY_NAME = "uniplacesPlace";
    public static final int VERSION_HASH = 1828327500;
    private static IEntityMeta ENTITY_META;

    public static final String L_FLOOR = "floor";
    public static final String L_PURPOSE = "purpose";
    public static final String L_CONDITION = "condition";
    public static final String L_CLASSROOM_TYPE = "classroomType";
    public static final String P_NUMBER = "number";
    public static final String P_TITLE = "title";
    public static final String P_WINDOW_NUMBER = "windowNumber";
    public static final String P_AREA = "area";
    public static final String P_CAPACITY = "capacity";
    public static final String P_HEIGHT = "height";
    public static final String P_LENGTH = "length";
    public static final String P_WIDTH = "width";
    public static final String P_DISPLAYABLE_TITLE = "displayableTitle";
    public static final String P_FRACTIONAL_AREA = "fractionalArea";
    public static final String P_FULL_LOCATION_INFO = "fullLocationInfo";
    public static final String P_TITLE_WITH_LOCATION = "titleWithLocation";

    private UniplacesFloor _floor;     // Этаж
    private UniplacesPlacePurpose _purpose;     // Назначение помещения
    private UniplacesPlaceCondition _condition;     // Состояние помещения
    private UniplacesClassroomType _classroomType;     // Тип учебного помещения
    private String _number;     // Номер
    private String _title;     // Название
    private Integer _windowNumber;     // Число окон
    private Long _area;     // Площадь кв. м
    private Integer _capacity;     // Число посадочных мест
    private long _height = 0l;     // Высота помещения
    private long _length = 0l;     // Длина помещения
    private long _width = 0l;     // Ширина помещения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Этаж. Свойство не может быть null.
     */
    @NotNull
    public UniplacesFloor getFloor()
    {
        return _floor;
    }

    /**
     * @param floor Этаж. Свойство не может быть null.
     */
    public void setFloor(UniplacesFloor floor)
    {
        dirty(_floor, floor);
        _floor = floor;
    }

    /**
     * @return Назначение помещения.
     */
    public UniplacesPlacePurpose getPurpose()
    {
        return _purpose;
    }

    /**
     * @param purpose Назначение помещения.
     */
    public void setPurpose(UniplacesPlacePurpose purpose)
    {
        dirty(_purpose, purpose);
        _purpose = purpose;
    }

    /**
     * @return Состояние помещения.
     */
    public UniplacesPlaceCondition getCondition()
    {
        return _condition;
    }

    /**
     * @param condition Состояние помещения.
     */
    public void setCondition(UniplacesPlaceCondition condition)
    {
        dirty(_condition, condition);
        _condition = condition;
    }

    /**
     * @return Тип учебного помещения.
     */
    public UniplacesClassroomType getClassroomType()
    {
        return _classroomType;
    }

    /**
     * @param classroomType Тип учебного помещения.
     */
    public void setClassroomType(UniplacesClassroomType classroomType)
    {
        dirty(_classroomType, classroomType);
        _classroomType = classroomType;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Название.
     */
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Число окон.
     */
    public Integer getWindowNumber()
    {
        return _windowNumber;
    }

    /**
     * @param windowNumber Число окон.
     */
    public void setWindowNumber(Integer windowNumber)
    {
        dirty(_windowNumber, windowNumber);
        _windowNumber = windowNumber;
    }

    /**
     * @return Площадь кв. м.
     */
    public Long getArea()
    {
        return _area;
    }

    /**
     * @param area Площадь кв. м.
     */
    public void setArea(Long area)
    {
        dirty(_area, area);
        _area = area;
    }

    /**
     * @return Число посадочных мест.
     */
    public Integer getCapacity()
    {
        return _capacity;
    }

    /**
     * @param capacity Число посадочных мест.
     */
    public void setCapacity(Integer capacity)
    {
        dirty(_capacity, capacity);
        _capacity = capacity;
    }

    /**
     * Хранится со сдвигом на 3 символа дробной части
     *
     * @return Высота помещения. Свойство не может быть null.
     */
    @NotNull
    public long getHeight()
    {
        return _height;
    }

    /**
     * @param height Высота помещения. Свойство не может быть null.
     */
    public void setHeight(long height)
    {
        dirty(_height, height);
        _height = height;
    }

    /**
     * Хранится со сдвигом на 3 символа дробной части
     *
     * @return Длина помещения. Свойство не может быть null.
     */
    @NotNull
    public long getLength()
    {
        return _length;
    }

    /**
     * @param length Длина помещения. Свойство не может быть null.
     */
    public void setLength(long length)
    {
        dirty(_length, length);
        _length = length;
    }

    /**
     * Хранится со сдвигом на 3 символа дробной части
     *
     * @return Ширина помещения. Свойство не может быть null.
     */
    @NotNull
    public long getWidth()
    {
        return _width;
    }

    /**
     * @param width Ширина помещения. Свойство не может быть null.
     */
    public void setWidth(long width)
    {
        dirty(_width, width);
        _width = width;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UniplacesPlaceGen)
        {
            setFloor(((UniplacesPlace)another).getFloor());
            setPurpose(((UniplacesPlace)another).getPurpose());
            setCondition(((UniplacesPlace)another).getCondition());
            setClassroomType(((UniplacesPlace)another).getClassroomType());
            setNumber(((UniplacesPlace)another).getNumber());
            setTitle(((UniplacesPlace)another).getTitle());
            setWindowNumber(((UniplacesPlace)another).getWindowNumber());
            setArea(((UniplacesPlace)another).getArea());
            setCapacity(((UniplacesPlace)another).getCapacity());
            setHeight(((UniplacesPlace)another).getHeight());
            setLength(((UniplacesPlace)another).getLength());
            setWidth(((UniplacesPlace)another).getWidth());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniplacesPlaceGen> extends UniplacesRegistryRecord.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniplacesPlace.class;
        }

        public T newInstance()
        {
            return (T) new UniplacesPlace();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "floor":
                    return obj.getFloor();
                case "purpose":
                    return obj.getPurpose();
                case "condition":
                    return obj.getCondition();
                case "classroomType":
                    return obj.getClassroomType();
                case "number":
                    return obj.getNumber();
                case "title":
                    return obj.getTitle();
                case "windowNumber":
                    return obj.getWindowNumber();
                case "area":
                    return obj.getArea();
                case "capacity":
                    return obj.getCapacity();
                case "height":
                    return obj.getHeight();
                case "length":
                    return obj.getLength();
                case "width":
                    return obj.getWidth();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "floor":
                    obj.setFloor((UniplacesFloor) value);
                    return;
                case "purpose":
                    obj.setPurpose((UniplacesPlacePurpose) value);
                    return;
                case "condition":
                    obj.setCondition((UniplacesPlaceCondition) value);
                    return;
                case "classroomType":
                    obj.setClassroomType((UniplacesClassroomType) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "windowNumber":
                    obj.setWindowNumber((Integer) value);
                    return;
                case "area":
                    obj.setArea((Long) value);
                    return;
                case "capacity":
                    obj.setCapacity((Integer) value);
                    return;
                case "height":
                    obj.setHeight((Long) value);
                    return;
                case "length":
                    obj.setLength((Long) value);
                    return;
                case "width":
                    obj.setWidth((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "floor":
                        return true;
                case "purpose":
                        return true;
                case "condition":
                        return true;
                case "classroomType":
                        return true;
                case "number":
                        return true;
                case "title":
                        return true;
                case "windowNumber":
                        return true;
                case "area":
                        return true;
                case "capacity":
                        return true;
                case "height":
                        return true;
                case "length":
                        return true;
                case "width":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "floor":
                    return true;
                case "purpose":
                    return true;
                case "condition":
                    return true;
                case "classroomType":
                    return true;
                case "number":
                    return true;
                case "title":
                    return true;
                case "windowNumber":
                    return true;
                case "area":
                    return true;
                case "capacity":
                    return true;
                case "height":
                    return true;
                case "length":
                    return true;
                case "width":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "floor":
                    return UniplacesFloor.class;
                case "purpose":
                    return UniplacesPlacePurpose.class;
                case "condition":
                    return UniplacesPlaceCondition.class;
                case "classroomType":
                    return UniplacesClassroomType.class;
                case "number":
                    return String.class;
                case "title":
                    return String.class;
                case "windowNumber":
                    return Integer.class;
                case "area":
                    return Long.class;
                case "capacity":
                    return Integer.class;
                case "height":
                    return Long.class;
                case "length":
                    return Long.class;
                case "width":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniplacesPlace> _dslPath = new Path<UniplacesPlace>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniplacesPlace");
    }
            

    /**
     * @return Этаж. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getFloor()
     */
    public static UniplacesFloor.Path<UniplacesFloor> floor()
    {
        return _dslPath.floor();
    }

    /**
     * @return Назначение помещения.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getPurpose()
     */
    public static UniplacesPlacePurpose.Path<UniplacesPlacePurpose> purpose()
    {
        return _dslPath.purpose();
    }

    /**
     * @return Состояние помещения.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getCondition()
     */
    public static UniplacesPlaceCondition.Path<UniplacesPlaceCondition> condition()
    {
        return _dslPath.condition();
    }

    /**
     * @return Тип учебного помещения.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getClassroomType()
     */
    public static UniplacesClassroomType.Path<UniplacesClassroomType> classroomType()
    {
        return _dslPath.classroomType();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Число окон.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getWindowNumber()
     */
    public static PropertyPath<Integer> windowNumber()
    {
        return _dslPath.windowNumber();
    }

    /**
     * @return Площадь кв. м.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getArea()
     */
    public static PropertyPath<Long> area()
    {
        return _dslPath.area();
    }

    /**
     * @return Число посадочных мест.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getCapacity()
     */
    public static PropertyPath<Integer> capacity()
    {
        return _dslPath.capacity();
    }

    /**
     * Хранится со сдвигом на 3 символа дробной части
     *
     * @return Высота помещения. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getHeight()
     */
    public static PropertyPath<Long> height()
    {
        return _dslPath.height();
    }

    /**
     * Хранится со сдвигом на 3 символа дробной части
     *
     * @return Длина помещения. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getLength()
     */
    public static PropertyPath<Long> length()
    {
        return _dslPath.length();
    }

    /**
     * Хранится со сдвигом на 3 символа дробной части
     *
     * @return Ширина помещения. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getWidth()
     */
    public static PropertyPath<Long> width()
    {
        return _dslPath.width();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getDisplayableTitle()
     */
    public static SupportedPropertyPath<String> displayableTitle()
    {
        return _dslPath.displayableTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getFractionalArea()
     */
    public static SupportedPropertyPath<Double> fractionalArea()
    {
        return _dslPath.fractionalArea();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getFullLocationInfo()
     */
    public static SupportedPropertyPath<String> fullLocationInfo()
    {
        return _dslPath.fullLocationInfo();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getTitleWithLocation()
     */
    public static SupportedPropertyPath<String> titleWithLocation()
    {
        return _dslPath.titleWithLocation();
    }

    public static class Path<E extends UniplacesPlace> extends UniplacesRegistryRecord.Path<E>
    {
        private UniplacesFloor.Path<UniplacesFloor> _floor;
        private UniplacesPlacePurpose.Path<UniplacesPlacePurpose> _purpose;
        private UniplacesPlaceCondition.Path<UniplacesPlaceCondition> _condition;
        private UniplacesClassroomType.Path<UniplacesClassroomType> _classroomType;
        private PropertyPath<String> _number;
        private PropertyPath<String> _title;
        private PropertyPath<Integer> _windowNumber;
        private PropertyPath<Long> _area;
        private PropertyPath<Integer> _capacity;
        private PropertyPath<Long> _height;
        private PropertyPath<Long> _length;
        private PropertyPath<Long> _width;
        private SupportedPropertyPath<String> _displayableTitle;
        private SupportedPropertyPath<Double> _fractionalArea;
        private SupportedPropertyPath<String> _fullLocationInfo;
        private SupportedPropertyPath<String> _titleWithLocation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Этаж. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getFloor()
     */
        public UniplacesFloor.Path<UniplacesFloor> floor()
        {
            if(_floor == null )
                _floor = new UniplacesFloor.Path<UniplacesFloor>(L_FLOOR, this);
            return _floor;
        }

    /**
     * @return Назначение помещения.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getPurpose()
     */
        public UniplacesPlacePurpose.Path<UniplacesPlacePurpose> purpose()
        {
            if(_purpose == null )
                _purpose = new UniplacesPlacePurpose.Path<UniplacesPlacePurpose>(L_PURPOSE, this);
            return _purpose;
        }

    /**
     * @return Состояние помещения.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getCondition()
     */
        public UniplacesPlaceCondition.Path<UniplacesPlaceCondition> condition()
        {
            if(_condition == null )
                _condition = new UniplacesPlaceCondition.Path<UniplacesPlaceCondition>(L_CONDITION, this);
            return _condition;
        }

    /**
     * @return Тип учебного помещения.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getClassroomType()
     */
        public UniplacesClassroomType.Path<UniplacesClassroomType> classroomType()
        {
            if(_classroomType == null )
                _classroomType = new UniplacesClassroomType.Path<UniplacesClassroomType>(L_CLASSROOM_TYPE, this);
            return _classroomType;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(UniplacesPlaceGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(UniplacesPlaceGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Число окон.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getWindowNumber()
     */
        public PropertyPath<Integer> windowNumber()
        {
            if(_windowNumber == null )
                _windowNumber = new PropertyPath<Integer>(UniplacesPlaceGen.P_WINDOW_NUMBER, this);
            return _windowNumber;
        }

    /**
     * @return Площадь кв. м.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getArea()
     */
        public PropertyPath<Long> area()
        {
            if(_area == null )
                _area = new PropertyPath<Long>(UniplacesPlaceGen.P_AREA, this);
            return _area;
        }

    /**
     * @return Число посадочных мест.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getCapacity()
     */
        public PropertyPath<Integer> capacity()
        {
            if(_capacity == null )
                _capacity = new PropertyPath<Integer>(UniplacesPlaceGen.P_CAPACITY, this);
            return _capacity;
        }

    /**
     * Хранится со сдвигом на 3 символа дробной части
     *
     * @return Высота помещения. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getHeight()
     */
        public PropertyPath<Long> height()
        {
            if(_height == null )
                _height = new PropertyPath<Long>(UniplacesPlaceGen.P_HEIGHT, this);
            return _height;
        }

    /**
     * Хранится со сдвигом на 3 символа дробной части
     *
     * @return Длина помещения. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getLength()
     */
        public PropertyPath<Long> length()
        {
            if(_length == null )
                _length = new PropertyPath<Long>(UniplacesPlaceGen.P_LENGTH, this);
            return _length;
        }

    /**
     * Хранится со сдвигом на 3 символа дробной части
     *
     * @return Ширина помещения. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getWidth()
     */
        public PropertyPath<Long> width()
        {
            if(_width == null )
                _width = new PropertyPath<Long>(UniplacesPlaceGen.P_WIDTH, this);
            return _width;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getDisplayableTitle()
     */
        public SupportedPropertyPath<String> displayableTitle()
        {
            if(_displayableTitle == null )
                _displayableTitle = new SupportedPropertyPath<String>(UniplacesPlaceGen.P_DISPLAYABLE_TITLE, this);
            return _displayableTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getFractionalArea()
     */
        public SupportedPropertyPath<Double> fractionalArea()
        {
            if(_fractionalArea == null )
                _fractionalArea = new SupportedPropertyPath<Double>(UniplacesPlaceGen.P_FRACTIONAL_AREA, this);
            return _fractionalArea;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getFullLocationInfo()
     */
        public SupportedPropertyPath<String> fullLocationInfo()
        {
            if(_fullLocationInfo == null )
                _fullLocationInfo = new SupportedPropertyPath<String>(UniplacesPlaceGen.P_FULL_LOCATION_INFO, this);
            return _fullLocationInfo;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesPlace#getTitleWithLocation()
     */
        public SupportedPropertyPath<String> titleWithLocation()
        {
            if(_titleWithLocation == null )
                _titleWithLocation = new SupportedPropertyPath<String>(UniplacesPlaceGen.P_TITLE_WITH_LOCATION, this);
            return _titleWithLocation;
        }

        public Class getEntityClass()
        {
            return UniplacesPlace.class;
        }

        public String getEntityName()
        {
            return "uniplacesPlace";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDisplayableTitle();

    public abstract Double getFractionalArea();

    public abstract String getFullLocationInfo();

    public abstract String getTitleWithLocation();
}
