/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.settings;

import java.io.Serializable;

/**
 * @author agolubenko
 * @since Aug 17, 2010
 */
public class BuildingNamingSettings implements Serializable
{
    private static final long serialVersionUID = -4002875638010702572L;

    private String _prefix;
    private Integer _digitsNumber = 3;
    private int _startNumber = 1;

    public String getPrefix()
    {
        return _prefix;
    }

    public void setPrefix(String prefix)
    {
        _prefix = prefix;
    }

    public Integer getDigitsNumber()
    {
        return _digitsNumber;
    }

    public void setDigitsNumber(Integer digitsNumber)
    {
        _digitsNumber = digitsNumber;
    }

    public int getStartNumber()
    {
        return _startNumber;
    }

    public void setStartNumber(int startNumber)
    {
        _startNumber = startNumber;
    }
}
