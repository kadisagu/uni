/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.UnitAddEdit;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import ru.tandemservice.uniplaces.entity.catalog.UniplacesBuildingPurpose;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceNumberingType;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

/**
 * @author agolubenko
 * @since Aug 19, 2010
 */
@Input( { @Bind(key = "unitId", binding = "unit.id"), @Bind(key = "buildingId", binding = "building.id") })
public class Model
{
    private UniplacesUnit _unit = new UniplacesUnit();
    private UniplacesBuilding _building = new UniplacesBuilding();
    
    private List<UniplacesBuildingPurpose> _buildingPurposeList;
    private List<UniplacesPlaceNumberingType> _placeNumberingTypeList;

    public boolean isEditForm()
    {
        return (getUnit().getId() != null);
    }

    public UniplacesUnit getUnit()
    {
        return _unit;
    }

    public void setUnit(UniplacesUnit unit)
    {
        _unit = unit;
    }

    public UniplacesBuilding getBuilding()
    {
        return _building;
    }

    public void setBuilding(UniplacesBuilding building)
    {
        _building = building;
    }

    public List<UniplacesBuildingPurpose> getBuildingPurposeList()
    {
        return _buildingPurposeList;
    }

    public void setBuildingPurposeList(List<UniplacesBuildingPurpose> buildingPurposeList)
    {
        _buildingPurposeList = buildingPurposeList;
    }

    public List<UniplacesPlaceNumberingType> getPlaceNumberingTypeList()
    {
        return _placeNumberingTypeList;
    }

    public void setPlaceNumberingTypeList(List<UniplacesPlaceNumberingType> placeNumberingTypeList)
    {
        _placeNumberingTypeList = placeNumberingTypeList;
    }
}
