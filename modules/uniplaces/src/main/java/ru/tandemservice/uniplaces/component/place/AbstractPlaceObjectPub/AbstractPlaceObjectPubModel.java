/* $Id$ */
package ru.tandemservice.uniplaces.component.place.AbstractPlaceObjectPub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation;

/**
 * @author Ekaterina Zvereva
 * @since 16.02.2015
 */
public abstract class AbstractPlaceObjectPubModel
{
    private DynamicListDataSource<UniplacesObjectToDocumentRelation> _documentDataSource;
    private String _postfix = initPostfix();

    public DynamicListDataSource<UniplacesObjectToDocumentRelation> getDocumentDataSource()
    {
        return _documentDataSource;
    }

    public void setDocumentDataSource(DynamicListDataSource<UniplacesObjectToDocumentRelation> documentDataSource)
    {
        _documentDataSource = documentDataSource;
    }

    public abstract Long getObjectID();

    public String getPostfix()
    {
        return _postfix;
    }

    public abstract String initPostfix();
}