// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.PlaceMassEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceCondition;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;

import java.util.ArrayList;
import java.util.List;

/**
 * @author oleyba
 * @since 24.09.2010
 */
@Input({@Bind(key = "placeIds")})
public class Model
{
    private List<HSelectOption> _purposeList;
    private ISelectModel _conditionList;
    private ISelectModel _orgUnitList;
    private List<Long> placeIds = new ArrayList<Long>();

    private UniplacesPlacePurpose _newPurpose;
    private UniplacesPlaceCondition _newCondition;
    private OrgUnit _newOrgUnit;

    private UniplacesClassroomType _classroomType;
    private ISelectModel _classroomTypeModel;

    public List<HSelectOption> getPurposeList()
    {
        return _purposeList;
    }

    public void setPurposeList(List<HSelectOption> purposeList)
    {
        _purposeList = purposeList;
    }

    public ISelectModel getConditionList()
    {
        return _conditionList;
    }

    public void setConditionList(ISelectModel conditionList)
    {
        _conditionList = conditionList;
    }

    public ISelectModel getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(ISelectModel orgUnitList)
    {
        _orgUnitList = orgUnitList;
    }

    public List<Long> getPlaceIds()
    {
        return placeIds;
    }

    public void setPlaceIds(List<Long> placeIds)
    {
        this.placeIds = placeIds;
    }

    public UniplacesPlacePurpose getNewPurpose()
    {
        return _newPurpose;
    }

    public void setNewPurpose(UniplacesPlacePurpose newPurpose)
    {
        _newPurpose = newPurpose;
    }

    public UniplacesPlaceCondition getNewCondition()
    {
        return _newCondition;
    }

    public void setNewCondition(UniplacesPlaceCondition newCondition)
    {
        _newCondition = newCondition;
    }

    public OrgUnit getNewOrgUnit()
    {
        return _newOrgUnit;
    }

    public void setNewOrgUnit(OrgUnit newOrgUnit)
    {
        _newOrgUnit = newOrgUnit;
    }

    public UniplacesClassroomType getClassroomType()
    {
        return _classroomType;
    }

    public void setClassroomType(UniplacesClassroomType classroomType)
    {
        _classroomType = classroomType;
    }

    public ISelectModel getClassroomTypeModel()
    {
        return _classroomTypeModel;
    }

    public void setClassroomTypeModel(ISelectModel classroomTypeModel)
    {
        _classroomTypeModel = classroomTypeModel;
    }
}
