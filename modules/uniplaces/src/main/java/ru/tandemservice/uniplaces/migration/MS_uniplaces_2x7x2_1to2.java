package ru.tandemservice.uniplaces.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.IResultSetExt;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import ru.tandemservice.uniplaces.entity.catalog.codes.UniplacesPlacePurposeCodes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniplaces_2x7x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность uniplacesPlacePurpose

		// выставить признак countBedNumber = true для всех вложенных "Мужское" "Женское" типов помещений
        {
            ISQLTranslator translator = tool.getDialect().getSQLTranslator();

            SQLSelectQuery selectPurposesQuery = new SQLSelectQuery().from(SQLFrom.table("places_place_purpose", "ppp"))
                    .column("ppp.id", "id")
                    .column("ppp.parent_id", "pid")
                    .where("ppp.parent_id is not null");

            Statement purposesStatement = tool.getConnection().createStatement();
            purposesStatement.execute(translator.toSql(selectPurposesQuery));

            IResultSetExt purposesResultSet = (IResultSetExt) purposesStatement.getResultSet();
            while (purposesResultSet.next())
            {
                Long id = purposesResultSet.getLongObject(1);
                Long parentId = purposesResultSet.getLongObject(2);

                boolean isMaleOrFemale = checkMaleOrFemale(parentId, tool, translator);
                if(isMaleOrFemale)
                {
                    tool.executeUpdate("update places_place_purpose set countbednumber_p=? where id=?", true, id);
                }
            }
        }
    }

    private boolean checkMaleOrFemale(Long parentId, DBTool tool, ISQLTranslator translator) throws SQLException
    {
        boolean maleOrFemale = false;

        while(parentId != null)
        {
            Statement statement = tool.getConnection().createStatement();
            statement.execute(translator.toSql(getQuery().where("ppp.id=" + parentId)));

            IResultSetExt resultSet = (IResultSetExt) statement.getResultSet();
            while (resultSet.next())
            {
                Long id = resultSet.getLongObject(1);
                String code = resultSet.getString(3);

                if(UniplacesPlacePurposeCodes.MUJSKOE.equals(code) || UniplacesPlacePurposeCodes.JENSKOE.equals(code))
                {
                    maleOrFemale = true;
                    break;
                }
                parentId = resultSet.getLongObject(2);
            }
            if(maleOrFemale) break;
        }
        return maleOrFemale;
    }

    public SQLSelectQuery getQuery()
    {
        return new SQLSelectQuery().from(SQLFrom.table("places_place_purpose", "ppp"))
                .column("ppp.id", "id")
                .column("ppp.parent_id", "pid")
                .column("ppp.code_p", "code");
    }
}