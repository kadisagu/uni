package ru.tandemservice.uniplaces.entity.place.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesBuildingPurpose;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDisposalRight;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesGround;
import ru.tandemservice.uniplaces.entity.place.UniplacesRegistryRecord;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Здание
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniplacesBuildingGen extends UniplacesRegistryRecord
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplaces.entity.place.UniplacesBuilding";
    public static final String ENTITY_NAME = "uniplacesBuilding";
    public static final int VERSION_HASH = 1480868555;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISPOSAL_RIGHT = "disposalRight";
    public static final String L_GROUND = "ground";
    public static final String L_PURPOSE = "purpose";
    public static final String L_ADDRESS = "address";
    public static final String P_IDENTIFIER = "identifier";
    public static final String P_UNIT = "unit";
    public static final String P_FULL_NUMBER = "fullNumber";
    public static final String P_NUMBER = "number";
    public static final String P_TITLE = "title";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_AREA = "area";
    public static final String P_RIGHT_START_DATE = "rightStartDate";
    public static final String P_TITLE_WITH_ADDRESS = "titleWithAddress";

    private UniplacesDisposalRight _disposalRight;     // Право распоряжения
    private UniplacesGround _ground;     // Земельный участок
    private UniplacesBuildingPurpose _purpose;     // Назначение здания
    private AddressDetailed _address;     // Адрес (детальный)
    private String _identifier;     // Идентификатор
    private String _unit;     // Корпус
    private String _fullNumber;     // Номер
    private int _number;     // Порядковый номер
    private String _title;     // Название
    private String _shortTitle;     // Сокращенное название
    private Long _area;     // Площадь кв. м
    private Date _rightStartDate;     // Дата начала распоряжения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Право распоряжения. Свойство не может быть null.
     */
    @NotNull
    public UniplacesDisposalRight getDisposalRight()
    {
        return _disposalRight;
    }

    /**
     * @param disposalRight Право распоряжения. Свойство не может быть null.
     */
    public void setDisposalRight(UniplacesDisposalRight disposalRight)
    {
        dirty(_disposalRight, disposalRight);
        _disposalRight = disposalRight;
    }

    /**
     * @return Земельный участок.
     */
    public UniplacesGround getGround()
    {
        return _ground;
    }

    /**
     * @param ground Земельный участок.
     */
    public void setGround(UniplacesGround ground)
    {
        dirty(_ground, ground);
        _ground = ground;
    }

    /**
     * @return Назначение здания. Свойство не может быть null.
     */
    @NotNull
    public UniplacesBuildingPurpose getPurpose()
    {
        return _purpose;
    }

    /**
     * @param purpose Назначение здания. Свойство не может быть null.
     */
    public void setPurpose(UniplacesBuildingPurpose purpose)
    {
        dirty(_purpose, purpose);
        _purpose = purpose;
    }

    /**
     * @return Адрес (детальный). Свойство не может быть null.
     */
    @NotNull
    public AddressDetailed getAddress()
    {
        return _address;
    }

    /**
     * @param address Адрес (детальный). Свойство не может быть null.
     */
    public void setAddress(AddressDetailed address)
    {
        dirty(_address, address);
        _address = address;
    }

    /**
     * @return Идентификатор.
     */
    @Length(max=255)
    public String getIdentifier()
    {
        return _identifier;
    }

    /**
     * @param identifier Идентификатор.
     */
    public void setIdentifier(String identifier)
    {
        dirty(_identifier, identifier);
        _identifier = identifier;
    }

    /**
     * @return Корпус.
     */
    @Length(max=255)
    public String getUnit()
    {
        return _unit;
    }

    /**
     * @param unit Корпус.
     */
    public void setUnit(String unit)
    {
        dirty(_unit, unit);
        _unit = unit;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFullNumber()
    {
        return _fullNumber;
    }

    /**
     * @param fullNumber Номер. Свойство не может быть null.
     */
    public void setFullNumber(String fullNumber)
    {
        dirty(_fullNumber, fullNumber);
        _fullNumber = fullNumber;
    }

    /**
     * @return Порядковый номер. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Порядковый номер. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Площадь кв. м.
     */
    public Long getArea()
    {
        return _area;
    }

    /**
     * @param area Площадь кв. м.
     */
    public void setArea(Long area)
    {
        dirty(_area, area);
        _area = area;
    }

    /**
     * @return Дата начала распоряжения.
     */
    public Date getRightStartDate()
    {
        return _rightStartDate;
    }

    /**
     * @param rightStartDate Дата начала распоряжения.
     */
    public void setRightStartDate(Date rightStartDate)
    {
        dirty(_rightStartDate, rightStartDate);
        _rightStartDate = rightStartDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UniplacesBuildingGen)
        {
            setDisposalRight(((UniplacesBuilding)another).getDisposalRight());
            setGround(((UniplacesBuilding)another).getGround());
            setPurpose(((UniplacesBuilding)another).getPurpose());
            setAddress(((UniplacesBuilding)another).getAddress());
            setIdentifier(((UniplacesBuilding)another).getIdentifier());
            setUnit(((UniplacesBuilding)another).getUnit());
            setFullNumber(((UniplacesBuilding)another).getFullNumber());
            setNumber(((UniplacesBuilding)another).getNumber());
            setTitle(((UniplacesBuilding)another).getTitle());
            setShortTitle(((UniplacesBuilding)another).getShortTitle());
            setArea(((UniplacesBuilding)another).getArea());
            setRightStartDate(((UniplacesBuilding)another).getRightStartDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniplacesBuildingGen> extends UniplacesRegistryRecord.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniplacesBuilding.class;
        }

        public T newInstance()
        {
            return (T) new UniplacesBuilding();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "disposalRight":
                    return obj.getDisposalRight();
                case "ground":
                    return obj.getGround();
                case "purpose":
                    return obj.getPurpose();
                case "address":
                    return obj.getAddress();
                case "identifier":
                    return obj.getIdentifier();
                case "unit":
                    return obj.getUnit();
                case "fullNumber":
                    return obj.getFullNumber();
                case "number":
                    return obj.getNumber();
                case "title":
                    return obj.getTitle();
                case "shortTitle":
                    return obj.getShortTitle();
                case "area":
                    return obj.getArea();
                case "rightStartDate":
                    return obj.getRightStartDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "disposalRight":
                    obj.setDisposalRight((UniplacesDisposalRight) value);
                    return;
                case "ground":
                    obj.setGround((UniplacesGround) value);
                    return;
                case "purpose":
                    obj.setPurpose((UniplacesBuildingPurpose) value);
                    return;
                case "address":
                    obj.setAddress((AddressDetailed) value);
                    return;
                case "identifier":
                    obj.setIdentifier((String) value);
                    return;
                case "unit":
                    obj.setUnit((String) value);
                    return;
                case "fullNumber":
                    obj.setFullNumber((String) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "area":
                    obj.setArea((Long) value);
                    return;
                case "rightStartDate":
                    obj.setRightStartDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "disposalRight":
                        return true;
                case "ground":
                        return true;
                case "purpose":
                        return true;
                case "address":
                        return true;
                case "identifier":
                        return true;
                case "unit":
                        return true;
                case "fullNumber":
                        return true;
                case "number":
                        return true;
                case "title":
                        return true;
                case "shortTitle":
                        return true;
                case "area":
                        return true;
                case "rightStartDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "disposalRight":
                    return true;
                case "ground":
                    return true;
                case "purpose":
                    return true;
                case "address":
                    return true;
                case "identifier":
                    return true;
                case "unit":
                    return true;
                case "fullNumber":
                    return true;
                case "number":
                    return true;
                case "title":
                    return true;
                case "shortTitle":
                    return true;
                case "area":
                    return true;
                case "rightStartDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "disposalRight":
                    return UniplacesDisposalRight.class;
                case "ground":
                    return UniplacesGround.class;
                case "purpose":
                    return UniplacesBuildingPurpose.class;
                case "address":
                    return AddressDetailed.class;
                case "identifier":
                    return String.class;
                case "unit":
                    return String.class;
                case "fullNumber":
                    return String.class;
                case "number":
                    return Integer.class;
                case "title":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "area":
                    return Long.class;
                case "rightStartDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniplacesBuilding> _dslPath = new Path<UniplacesBuilding>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniplacesBuilding");
    }
            

    /**
     * @return Право распоряжения. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getDisposalRight()
     */
    public static UniplacesDisposalRight.Path<UniplacesDisposalRight> disposalRight()
    {
        return _dslPath.disposalRight();
    }

    /**
     * @return Земельный участок.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getGround()
     */
    public static UniplacesGround.Path<UniplacesGround> ground()
    {
        return _dslPath.ground();
    }

    /**
     * @return Назначение здания. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getPurpose()
     */
    public static UniplacesBuildingPurpose.Path<UniplacesBuildingPurpose> purpose()
    {
        return _dslPath.purpose();
    }

    /**
     * @return Адрес (детальный). Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getAddress()
     */
    public static AddressDetailed.Path<AddressDetailed> address()
    {
        return _dslPath.address();
    }

    /**
     * @return Идентификатор.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getIdentifier()
     */
    public static PropertyPath<String> identifier()
    {
        return _dslPath.identifier();
    }

    /**
     * @return Корпус.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getUnit()
     */
    public static PropertyPath<String> unit()
    {
        return _dslPath.unit();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getFullNumber()
     */
    public static PropertyPath<String> fullNumber()
    {
        return _dslPath.fullNumber();
    }

    /**
     * @return Порядковый номер. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Площадь кв. м.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getArea()
     */
    public static PropertyPath<Long> area()
    {
        return _dslPath.area();
    }

    /**
     * @return Дата начала распоряжения.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getRightStartDate()
     */
    public static PropertyPath<Date> rightStartDate()
    {
        return _dslPath.rightStartDate();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getTitleWithAddress()
     */
    public static SupportedPropertyPath<String> titleWithAddress()
    {
        return _dslPath.titleWithAddress();
    }

    public static class Path<E extends UniplacesBuilding> extends UniplacesRegistryRecord.Path<E>
    {
        private UniplacesDisposalRight.Path<UniplacesDisposalRight> _disposalRight;
        private UniplacesGround.Path<UniplacesGround> _ground;
        private UniplacesBuildingPurpose.Path<UniplacesBuildingPurpose> _purpose;
        private AddressDetailed.Path<AddressDetailed> _address;
        private PropertyPath<String> _identifier;
        private PropertyPath<String> _unit;
        private PropertyPath<String> _fullNumber;
        private PropertyPath<Integer> _number;
        private PropertyPath<String> _title;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<Long> _area;
        private PropertyPath<Date> _rightStartDate;
        private SupportedPropertyPath<String> _titleWithAddress;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Право распоряжения. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getDisposalRight()
     */
        public UniplacesDisposalRight.Path<UniplacesDisposalRight> disposalRight()
        {
            if(_disposalRight == null )
                _disposalRight = new UniplacesDisposalRight.Path<UniplacesDisposalRight>(L_DISPOSAL_RIGHT, this);
            return _disposalRight;
        }

    /**
     * @return Земельный участок.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getGround()
     */
        public UniplacesGround.Path<UniplacesGround> ground()
        {
            if(_ground == null )
                _ground = new UniplacesGround.Path<UniplacesGround>(L_GROUND, this);
            return _ground;
        }

    /**
     * @return Назначение здания. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getPurpose()
     */
        public UniplacesBuildingPurpose.Path<UniplacesBuildingPurpose> purpose()
        {
            if(_purpose == null )
                _purpose = new UniplacesBuildingPurpose.Path<UniplacesBuildingPurpose>(L_PURPOSE, this);
            return _purpose;
        }

    /**
     * @return Адрес (детальный). Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getAddress()
     */
        public AddressDetailed.Path<AddressDetailed> address()
        {
            if(_address == null )
                _address = new AddressDetailed.Path<AddressDetailed>(L_ADDRESS, this);
            return _address;
        }

    /**
     * @return Идентификатор.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getIdentifier()
     */
        public PropertyPath<String> identifier()
        {
            if(_identifier == null )
                _identifier = new PropertyPath<String>(UniplacesBuildingGen.P_IDENTIFIER, this);
            return _identifier;
        }

    /**
     * @return Корпус.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getUnit()
     */
        public PropertyPath<String> unit()
        {
            if(_unit == null )
                _unit = new PropertyPath<String>(UniplacesBuildingGen.P_UNIT, this);
            return _unit;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getFullNumber()
     */
        public PropertyPath<String> fullNumber()
        {
            if(_fullNumber == null )
                _fullNumber = new PropertyPath<String>(UniplacesBuildingGen.P_FULL_NUMBER, this);
            return _fullNumber;
        }

    /**
     * @return Порядковый номер. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(UniplacesBuildingGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(UniplacesBuildingGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(UniplacesBuildingGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Площадь кв. м.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getArea()
     */
        public PropertyPath<Long> area()
        {
            if(_area == null )
                _area = new PropertyPath<Long>(UniplacesBuildingGen.P_AREA, this);
            return _area;
        }

    /**
     * @return Дата начала распоряжения.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getRightStartDate()
     */
        public PropertyPath<Date> rightStartDate()
        {
            if(_rightStartDate == null )
                _rightStartDate = new PropertyPath<Date>(UniplacesBuildingGen.P_RIGHT_START_DATE, this);
            return _rightStartDate;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesBuilding#getTitleWithAddress()
     */
        public SupportedPropertyPath<String> titleWithAddress()
        {
            if(_titleWithAddress == null )
                _titleWithAddress = new SupportedPropertyPath<String>(UniplacesBuildingGen.P_TITLE_WITH_ADDRESS, this);
            return _titleWithAddress;
        }

        public Class getEntityClass()
        {
            return UniplacesBuilding.class;
        }

        public String getEntityName()
        {
            return "uniplacesBuilding";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitleWithAddress();
}
