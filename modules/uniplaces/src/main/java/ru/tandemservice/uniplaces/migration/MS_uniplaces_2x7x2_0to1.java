package ru.tandemservice.uniplaces.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniplaces_2x7x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность uniplacesObjectToDocumentRelation

		// создана новая сущность
		if (!tool.tableExists("nplcsobjcttdcmntrltn_t"))
        {
			// создать таблицу
			DBTable dbt = new DBTable("nplcsobjcttdcmntrltn_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("object_id", DBType.LONG).setNullable(false), 
				new DBColumn("document_p", DBType.LONG), 
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("startdate_p", DBType.DATE), 
				new DBColumn("enddate_p", DBType.DATE), 
				new DBColumn("comment_p", DBType.createVarchar(255)), 
				new DBColumn("active_p", DBType.BOOLEAN).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("uniplacesObjectToDocumentRelation");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность uniplacesPlace

		// создано обязательное свойство height
		if (!tool.columnExists("places_place", "height_p"))
        {
			// создать колонку
			tool.createColumn("places_place", new DBColumn("height_p", DBType.LONG));

			// задать значение по умолчанию
			java.lang.Long defaultHeight = 0l;
			tool.executeUpdate("update places_place set height_p=? where height_p is null", defaultHeight);

			// сделать колонку NOT NULL
			tool.setColumnNullable("places_place", "height_p", false);

		}

		// создано обязательное свойство length
        if (!tool.columnExists("places_place", "length_p"))
        {
			// создать колонку
			tool.createColumn("places_place", new DBColumn("length_p", DBType.LONG));

			// задать значение по умолчанию
			java.lang.Long defaultLength = 0l;
			tool.executeUpdate("update places_place set length_p=? where length_p is null", defaultLength);

			// сделать колонку NOT NULL
			tool.setColumnNullable("places_place", "length_p", false);

		}

		// создано обязательное свойство width
        if (!tool.columnExists("places_place", "width_p"))
        {
			// создать колонку
			tool.createColumn("places_place", new DBColumn("width_p", DBType.LONG));

			// задать значение по умолчанию
			java.lang.Long defaultWidth = 0l;
			tool.executeUpdate("update places_place set width_p=? where width_p is null", defaultWidth);

			// сделать колонку NOT NULL
			tool.setColumnNullable("places_place", "width_p", false);

		}


    }
}