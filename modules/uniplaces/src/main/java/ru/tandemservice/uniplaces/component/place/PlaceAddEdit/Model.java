/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.PlaceAddEdit;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceCondition;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;

/**
 * @author agolubenko
 * @since Aug 23, 2010
 */
@Input({@Bind(key = "placeId", binding = "place.id"), @Bind(key = "floorId")})
public class Model
{
    private UniplacesPlace _place = new UniplacesPlace();
    private Long _floorId;

    private List<HSelectOption> _purposeList;
    private List<UniplacesPlaceCondition> _conditionList;
    private ISelectModel _orgUnitList;
    private ISelectModel _classroomTypeModel;

    public boolean isEditForm()
    {
        return (getPlace().getId() != null);
    }

    public UniplacesPlace getPlace()
    {
        return _place;
    }

    public void setPlace(UniplacesPlace place)
    {
        _place = place;
    }

    public Long getFloorId()
    {
        return _floorId;
    }

    public void setFloorId(Long floorId)
    {
        _floorId = floorId;
    }

    public List<HSelectOption> getPurposeList()
    {
        return _purposeList;
    }

    public void setPurposeList(List<HSelectOption> purposeList)
    {
        _purposeList = purposeList;
    }

    public List<UniplacesPlaceCondition> getConditionList()
    {
        return _conditionList;
    }

    public void setConditionList(List<UniplacesPlaceCondition> conditionList)
    {
        _conditionList = conditionList;
    }

    public ISelectModel getOrgUnitList()
    {

        return _orgUnitList;
    }

    public void setOrgUnitList(ISelectModel orgUnitList)
    {
        _orgUnitList = orgUnitList;
    }

    public ISelectModel getClassroomTypeModel()
    {
        return _classroomTypeModel;
    }

    public void setClassroomTypeModel(ISelectModel classroomTypeModel)
    {
        _classroomTypeModel = classroomTypeModel;
    }
}
