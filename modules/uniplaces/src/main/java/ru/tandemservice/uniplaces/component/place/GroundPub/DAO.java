/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.GroundPub;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniplaces.UniplacesDefines;
import ru.tandemservice.uniplaces.component.place.AbstractPlaceObjectPub.AbstractPlaceObjectPubDAO;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentPurpose;
import ru.tandemservice.uniplaces.entity.place.UniplacesAssignedDocument;
import ru.tandemservice.uniplaces.entity.place.UniplacesGround;
import ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation;

import java.util.List;

/**
 * @author agolubenko
 * @since Aug 3, 2010
 */
public class DAO extends AbstractPlaceObjectPubDAO<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setGround(getNotNull(UniplacesGround.class, model.getGround().getId()));
        model.setAssignedDocument(getAssignedDocument(model.getGround(), getCatalogItem(UniplacesDocumentPurpose.class, UniplacesDefines.CATALOG_DOCUMENT_PURPOSE_GROUND_DISPOSAL_RIGHT)));
    }

    /**
     * @param ground земельный участок
     * @param documentPurpose назначение документа
     * @return назначенный документ
     */
    private UniplacesAssignedDocument getAssignedDocument(UniplacesGround ground, UniplacesDocumentPurpose documentPurpose)
    {
        Criteria criteria = getSession().createCriteria(UniplacesAssignedDocument.class);
        criteria.add(Restrictions.eq(UniplacesAssignedDocument.registryRecord().s(), ground));
        criteria.add(Restrictions.eq(UniplacesAssignedDocument.purpose().s(), documentPurpose));
        return (UniplacesAssignedDocument) criteria.uniqueResult();
    }
}
