/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.UnitAddEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniplaces.UniplacesDefines;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesBuildingPurpose;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceNumberingType;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

/**
 * @author agolubenko
 * @since Aug 19, 2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (model.isEditForm())
        {
            UniplacesUnit unit = getNotNull(UniplacesUnit.class, model.getUnit().getId());
            model.setUnit(unit);
            model.setBuilding(unit.getBuilding());
        }
        else
        {
            UniplacesBuilding building = getNotNull(UniplacesBuilding.class, model.getBuilding().getId());
            model.setBuilding(building);

            UniplacesUnit unit = model.getUnit();
            unit.setTitle("Основной");
            unit.setBuilding(building);
            unit.setPurpose(building.getPurpose());
            unit.setPlaceNumberingType(getCatalogItem(UniplacesPlaceNumberingType.class, UniplacesDefines.CATALOG_PLACE_NUMBERING_TYPE_BY_FLOOR));
        }

        model.setBuildingPurposeList(getCatalogItemListOrderByCode(UniplacesBuildingPurpose.class));
        model.setPlaceNumberingTypeList(getCatalogItemListOrderByCode(UniplacesPlaceNumberingType.class));
    }

    @Override
    public void update(Model model)
    {
        getSession().saveOrUpdate(model.getUnit());
    }
}
