/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.BuildingAddEdit;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniplaces.UniplacesDefines;
import ru.tandemservice.uniplaces.dao.IUniplacesDAO;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesBuildingPurpose;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDisposalRight;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentKind;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentPurpose;
import ru.tandemservice.uniplaces.entity.place.UniplacesAssignedDocument;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesDocument;
import ru.tandemservice.uniplaces.entity.place.UniplacesGround;
import ru.tandemservice.uniplaces.settings.BuildingNamingSettings;
import ru.tandemservice.uniplaces.util.PlacesUtil;

import java.util.*;

/**
 * @author agolubenko
 * @since Aug 16, 2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        UniplacesBuilding building = model.getBuilding();

        if (model.isEditForm())
        {
            building = getNotNull(UniplacesBuilding.class, model.getBuilding().getId());
            model.setBuilding(building);
        }

        Long groundId = model.getGroundId();
        if (groundId != null)
        {
            building.setGround(getNotNull(UniplacesGround.class, groundId));
        }

        // назначенные документы
        List<UniplacesAssignedDocument> assignedDocuments = new ArrayList<>();

        // получаем назначения и назначенные документы
        List<UniplacesDocumentPurpose> documentPurposes = getDocumentPurposes();
        Map<UniplacesDocumentPurpose, UniplacesAssignedDocument> purpose2AssignedDocuments = getPurpose2AssignedDocuments(building, documentPurposes);

        // для каждого назначения
        for (UniplacesDocumentPurpose documentPurpose : documentPurposes)
        {
            // если документа нет, со создаем новую заготовку
            UniplacesAssignedDocument assignedDocument = purpose2AssignedDocuments.get(documentPurpose);
            if (assignedDocument == null)
            {
                assignedDocument = new UniplacesAssignedDocument();
                assignedDocument.setRegistryRecord(model.getBuilding());
                assignedDocument.setPurpose(documentPurpose);
                assignedDocument.setDocument(documentPurpose.getDocumentKind().createDocument());
            }

            assignedDocuments.add(assignedDocument);
        }
        model.setAssignedDocuments(assignedDocuments);

        // фильтруем виды документов по назначению
        List<UniplacesDocumentKind> documentKinds = getCatalogItemList(UniplacesDocumentKind.class);

        Map<UniplacesDocumentPurpose, List<HSelectOption>> documentPurpose2KindList = new HashMap<>();
        for (UniplacesDocumentPurpose documentPurpose : UniBaseUtils.<UniplacesDocumentPurpose> getPropertiesSet(assignedDocuments, UniplacesAssignedDocument.purpose().s()))
        {
            documentPurpose2KindList.put(documentPurpose, HierarchyUtil.listHierarchyNodesWithParents(PlacesUtil.getFilteredDocumentKinds(documentKinds, documentPurpose), CommonCatalogUtil.CATALOG_CODE_COMPARATOR, false));
        }
        model.setDocumentPurpose2KindList(documentPurpose2KindList);

        // заполняем остальные селекты
        model.setOrgUnitList(IUniplacesDAO.instance.get().getResponsibleOrgUnitModel());
        model.setPurposeList(getCatalogItemListOrderByCode(UniplacesBuildingPurpose.class));
        model.setDisposalRightList(getCatalogItemListOrderByCode(UniplacesDisposalRight.class));
        model.setGroundList(new LazySimpleSelectModel<>(UniplacesGround.class, UniplacesGround.P_FULL_TITLE).setSortProperty(UniplacesGround.P_FULL_TITLE));
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (model.getBuilding().getRightStartDate() != null && model.getBuilding().getRightStartDate().after(new Date()))
            errors.add("Дата установления права распоряжения зданием должна быть не позже сегодняшней.", "rightStartDate");
    }

    @Override
    public void update(Model model)
    {
        Session session = getSession();

        // обновляем адрес
        AddressDetailed address = model.getAddress();

        AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(model.getBuilding(), address, UniplacesBuilding.L_ADDRESS);

        // получаем настройки
        BuildingNamingSettings settings = DataSettingsFacade.getSettings("", UniplacesDefines.MODULE_SETTINGS_KEY).get(UniplacesDefines.SETTINGS_BUILDING_NAMING);
        if (settings == null)
        {
            settings = new BuildingNamingSettings();
        }

        // генерируем номер, если форма добавления
        UniplacesBuilding building = model.getBuilding();
        if (!model.isEditForm())
        {
            Integer defaultDigitsNumber = settings.getDigitsNumber();
            Integer defaultStartNumber = settings.getStartNumber();
            int number = getMaxBuildingNumber(defaultStartNumber);

            building.setNumber(number);
            building.setFullNumber(getBuildingNumber((settings != null) ? settings.getPrefix() : null, defaultDigitsNumber, number));
        }

        session.saveOrUpdate(building);

        // назначенные документы
        for (UniplacesAssignedDocument assignedDocument : model.getAssignedDocuments())
        {
            // обновляем или удаляем
            UniplacesDocument document = assignedDocument.getDocument();
            if (document.getKind() != null || !StringUtils.isBlank(document.getNumber()) || document.getIssuanceDate() != null)
            {
                session.saveOrUpdate(document);
                session.saveOrUpdate(assignedDocument);
            }
            else if (assignedDocument.getId() != null)
            {
                session.delete(assignedDocument);
                session.delete(document);
            }
        }
    }

    /**
     * @param defaultValue номер по умолчанию
     * @return максимальный номер здания
     */
    private int getMaxBuildingNumber(int defaultValue)
    {
        Criteria criteria = getSession().createCriteria(UniplacesBuilding.class);
        criteria.setProjection(Projections.max(UniplacesBuilding.number().s()));

        Number result = (Number) criteria.uniqueResult();
        return (result != null) ? result.intValue() + 1 : defaultValue;
    }

    /**
     * @param prefix префикс
     * @param digitsNumber количество цифр в номере
     * @param number порядковый номер
     * @return полный номер здания
     */
    private String getBuildingNumber(String prefix, int digitsNumber, int number)
    {
        return String.format("%s%0" + digitsNumber + "d", StringUtils.defaultString(prefix), number);
    }

    /**
     * @return список назначений документов
     */
    private List<UniplacesDocumentPurpose> getDocumentPurposes()
    {
        List<String> purposeCodes = new ArrayList<>();
        purposeCodes.add(UniplacesDefines.CATALOG_DOCUMENT_PURPOSE_BUILDING_DISPOSAL_RIGHT);
        purposeCodes.add(UniplacesDefines.CATALOG_DOCUMENT_PURPOSE_BUILDING_OPERATION_START);
        purposeCodes.add(UniplacesDefines.CATALOG_DOCUMENT_PURPOSE_BUILDING_SANITARIAL_OPERATION_START);
        purposeCodes.add(UniplacesDefines.CATALOG_DOCUMENT_PURPOSE_BUILDING_FIRE_PREVENTION_OPERATION_START);

        return getCatalogItemList(UniplacesDocumentPurpose.class, UniplacesDocumentPurpose.code().s(), purposeCodes);
    }

    /**
     * @param building здание
     * @param documentPurposes назначения документов
     * @return назначение - документ для данного здания
     */
    @SuppressWarnings("unchecked")
    private Map<UniplacesDocumentPurpose, UniplacesAssignedDocument> getPurpose2AssignedDocuments(UniplacesBuilding building, Collection<UniplacesDocumentPurpose> documentPurposes)
    {
        if (documentPurposes.isEmpty() || building.getId() == null)
        {
            return Collections.emptyMap();
        }

        Criteria criteria = getSession().createCriteria(UniplacesAssignedDocument.class);
        criteria.add(Restrictions.eq(UniplacesAssignedDocument.registryRecord().s(), building));
        criteria.add(Restrictions.in(UniplacesAssignedDocument.purpose().s(), documentPurposes));

        Map<UniplacesDocumentPurpose, UniplacesAssignedDocument> result = new HashMap<>();
        for (UniplacesAssignedDocument assignedDocument : (List<UniplacesAssignedDocument>) criteria.list())
        {
            result.put(assignedDocument.getPurpose(), assignedDocument);
        }
        return result;
    }
}
