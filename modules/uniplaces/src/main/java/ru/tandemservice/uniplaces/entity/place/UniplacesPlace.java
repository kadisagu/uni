package ru.tandemservice.uniplaces.entity.place;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uniplaces.entity.place.gen.UniplacesPlaceGen;

/**
 * Помещение
 */
public class UniplacesPlace extends UniplacesPlaceGen
{
    public static final String P_FRACTIONAL_AREA = "fractionalArea";
    public static final String P_FRACTIONAL_LENGTH = "fractionalLength";
    public static final String P_FRACTIONAL_WIDTH = "fractionalWidth";
    public static final String P_FRACTIONAL_HEIGHT = "fractionalHeight";


    @Override
    @EntityDSLSupport(parts = {P_AREA})
    public Double getFractionalArea()
    {
        Long area = getArea();
        return (area != null) ? 0.01d * area : null;
    }

    public void setFractionalArea(Double area)
    {
        setArea((area != null) ? Math.round(area * 100.0d) : null);
    }


    public Double getFractionalLength()
    {
        long length = getLength();
        return 0.001d * length;
    }

    public void setFractionalLength(Double length)
    {
        setLength((length != null) ? Math.round(length * 1000.0d) : 0);
    }

    public Double getFractionalWidth()
    {
        long width = getWidth();
        return 0.001d * width;
    }

    public void setFractionalWidth(Double width)
    {
        setWidth((width != null) ? Math.round(width * 1000.0d) : 0);
    }

    public Double getFractionalHeight()
    {
        long height = getHeight();
        return 0.001d * height;
    }

    public void setFractionalHeight(Double height)
    {
        setHeight((height != null) ? Math.round(height * 1000.0d) : 0);
    }


    @Override
    @EntityDSLSupport(parts = {P_TITLE})
    public String getDisplayableTitle()
    {
        return getPurpose() == null || getPurpose().getShortTitle() == null ? getTitle() == null ? "" : getTitle() : getPurpose().getShortTitle() + (getTitle() == null ? "" : ". " + getTitle());
    }

    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1672148")
    @EntityDSLSupport
    public String getTitleWithLocation()
    {
        return StringUtils.trimToEmpty(getTitle()) + " (" + getFloor().getUnit().getBuilding().getTitle() + ", блок: " + getFloor().getUnit().getTitle() + ", " + getFloor().getTitle() + " этаж)";
    }

    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1672148")
    @EntityDSLSupport
    public String getFullLocationInfo()
    {
        return getFloor().getUnit().getBuilding().getShortTitle() + (StringUtils.isEmpty(getFloor().getUnit().getBuilding().getAddress().getShortTitleWithSettlement()) ? "" : " (" + getFloor().getUnit().getBuilding().getAddress().getShortTitleWithSettlement() + ")") + ", блок: " + getFloor().getUnit().getTitle() + ",  этаж: " + getFloor().getTitle();
    }
}