/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.BuildingAddEdit;

import java.util.List;
import java.util.Map;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uniplaces.entity.catalog.UniplacesBuildingPurpose;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDisposalRight;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentPurpose;
import ru.tandemservice.uniplaces.entity.place.UniplacesAssignedDocument;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;

/**
 * @author agolubenko
 * @since Aug 16, 2010
 */
@Input( { @Bind(key = "buildingId", binding = "building.id"), @Bind(key = "groundId") })
public class Model
{
    private UniplacesBuilding _building = new UniplacesBuilding();
    private UniplacesAssignedDocument _currentAssignedDocument;
    private List<UniplacesAssignedDocument> _assignedDocuments;

    private Long _groundId;
    private ISelectModel _groundList;
    private ISelectModel _orgUnitList;
    private Map<UniplacesDocumentPurpose, List<HSelectOption>> _documentPurpose2KindList;
    private List<UniplacesBuildingPurpose> _purposeList;
    private List<UniplacesDisposalRight> _disposalRightList;

    private AddressDetailed _address;

    public boolean isEditForm()
    {
        return (getBuilding().getId() != null);
    }

    public List<HSelectOption> getCurrentDocumentKindList()
    {
        return getDocumentPurpose2KindList().get(getCurrentAssignedDocument().getPurpose());
    }

    public UniplacesBuilding getBuilding()
    {
        return _building;
    }

    public void setBuilding(UniplacesBuilding building)
    {
        _building = building;
    }

    public UniplacesAssignedDocument getCurrentAssignedDocument()
    {
        return _currentAssignedDocument;
    }

    public void setCurrentAssignedDocument(UniplacesAssignedDocument currentAssignedDocument)
    {
        _currentAssignedDocument = currentAssignedDocument;
    }

    public List<UniplacesAssignedDocument> getAssignedDocuments()
    {
        return _assignedDocuments;
    }

    public void setAssignedDocuments(List<UniplacesAssignedDocument> assignedDocuments)
    {
        _assignedDocuments = assignedDocuments;
    }

    public Long getGroundId()
    {
        return _groundId;
    }

    public void setGroundId(Long groundId)
    {
        _groundId = groundId;
    }

    public Map<UniplacesDocumentPurpose, List<HSelectOption>> getDocumentPurpose2KindList()
    {
        return _documentPurpose2KindList;
    }

    public void setDocumentPurpose2KindList(Map<UniplacesDocumentPurpose, List<HSelectOption>> documentPurpose2KindList)
    {
        _documentPurpose2KindList = documentPurpose2KindList;
    }

    public ISelectModel getGroundList()
    {
        return _groundList;
    }

    public void setGroundList(ISelectModel groundList)
    {
        _groundList = groundList;
    }

    public ISelectModel getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(ISelectModel orgUnitList)
    {
        _orgUnitList = orgUnitList;
    }

    public List<UniplacesBuildingPurpose> getPurposeList()
    {
        return _purposeList;
    }

    public void setPurposeList(List<UniplacesBuildingPurpose> purposeList)
    {
        _purposeList = purposeList;
    }

    public List<UniplacesDisposalRight> getDisposalRightList()
    {
        return _disposalRightList;
    }

    public void setDisposalRightList(List<UniplacesDisposalRight> disposalRightList)
    {
        _disposalRightList = disposalRightList;
    }

    public AddressDetailed getAddress()
    {
        return _address;
    }

    public void setAddress(AddressDetailed address)
    {
        _address = address;
    }
}
