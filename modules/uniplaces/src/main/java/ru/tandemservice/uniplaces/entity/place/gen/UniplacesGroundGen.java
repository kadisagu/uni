package ru.tandemservice.uniplaces.entity.place.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDisposalRight;
import ru.tandemservice.uniplaces.entity.place.UniplacesGround;
import ru.tandemservice.uniplaces.entity.place.UniplacesRegistryRecord;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Земельный участок
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniplacesGroundGen extends UniplacesRegistryRecord
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplaces.entity.place.UniplacesGround";
    public static final String ENTITY_NAME = "uniplacesGround";
    public static final int VERSION_HASH = 762355584;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISPOSAL_RIGHT = "disposalRight";
    public static final String P_NUMBER = "number";
    public static final String P_LOCATION = "location";
    public static final String P_AREA = "area";
    public static final String P_BALANCE_VALUE = "balanceValue";
    public static final String P_RIGHT_START_DATE = "rightStartDate";
    public static final String P_RIGHT_END_DATE = "rightEndDate";
    public static final String P_FRACTIONAL_AREA = "fractionalArea";

    private UniplacesDisposalRight _disposalRight;     // Право распоряжения
    private int _number;     // Порядковый номер
    private String _location;     // Местоположение
    private Long _area;     // Площадь кв. м
    private Long _balanceValue;     // Балансовая стоимость
    private Date _rightStartDate;     // Дата начала распоряжения
    private Date _rightEndDate;     // Дата завершения распоряжения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Право распоряжения. Свойство не может быть null.
     */
    @NotNull
    public UniplacesDisposalRight getDisposalRight()
    {
        return _disposalRight;
    }

    /**
     * @param disposalRight Право распоряжения. Свойство не может быть null.
     */
    public void setDisposalRight(UniplacesDisposalRight disposalRight)
    {
        dirty(_disposalRight, disposalRight);
        _disposalRight = disposalRight;
    }

    /**
     * @return Порядковый номер. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Порядковый номер. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Местоположение. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLocation()
    {
        return _location;
    }

    /**
     * @param location Местоположение. Свойство не может быть null.
     */
    public void setLocation(String location)
    {
        dirty(_location, location);
        _location = location;
    }

    /**
     * @return Площадь кв. м.
     */
    public Long getArea()
    {
        return _area;
    }

    /**
     * @param area Площадь кв. м.
     */
    public void setArea(Long area)
    {
        dirty(_area, area);
        _area = area;
    }

    /**
     * @return Балансовая стоимость.
     */
    public Long getBalanceValue()
    {
        return _balanceValue;
    }

    /**
     * @param balanceValue Балансовая стоимость.
     */
    public void setBalanceValue(Long balanceValue)
    {
        dirty(_balanceValue, balanceValue);
        _balanceValue = balanceValue;
    }

    /**
     * @return Дата начала распоряжения.
     */
    public Date getRightStartDate()
    {
        return _rightStartDate;
    }

    /**
     * @param rightStartDate Дата начала распоряжения.
     */
    public void setRightStartDate(Date rightStartDate)
    {
        dirty(_rightStartDate, rightStartDate);
        _rightStartDate = rightStartDate;
    }

    /**
     * @return Дата завершения распоряжения.
     */
    public Date getRightEndDate()
    {
        return _rightEndDate;
    }

    /**
     * @param rightEndDate Дата завершения распоряжения.
     */
    public void setRightEndDate(Date rightEndDate)
    {
        dirty(_rightEndDate, rightEndDate);
        _rightEndDate = rightEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UniplacesGroundGen)
        {
            setDisposalRight(((UniplacesGround)another).getDisposalRight());
            setNumber(((UniplacesGround)another).getNumber());
            setLocation(((UniplacesGround)another).getLocation());
            setArea(((UniplacesGround)another).getArea());
            setBalanceValue(((UniplacesGround)another).getBalanceValue());
            setRightStartDate(((UniplacesGround)another).getRightStartDate());
            setRightEndDate(((UniplacesGround)another).getRightEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniplacesGroundGen> extends UniplacesRegistryRecord.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniplacesGround.class;
        }

        public T newInstance()
        {
            return (T) new UniplacesGround();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "disposalRight":
                    return obj.getDisposalRight();
                case "number":
                    return obj.getNumber();
                case "location":
                    return obj.getLocation();
                case "area":
                    return obj.getArea();
                case "balanceValue":
                    return obj.getBalanceValue();
                case "rightStartDate":
                    return obj.getRightStartDate();
                case "rightEndDate":
                    return obj.getRightEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "disposalRight":
                    obj.setDisposalRight((UniplacesDisposalRight) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "location":
                    obj.setLocation((String) value);
                    return;
                case "area":
                    obj.setArea((Long) value);
                    return;
                case "balanceValue":
                    obj.setBalanceValue((Long) value);
                    return;
                case "rightStartDate":
                    obj.setRightStartDate((Date) value);
                    return;
                case "rightEndDate":
                    obj.setRightEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "disposalRight":
                        return true;
                case "number":
                        return true;
                case "location":
                        return true;
                case "area":
                        return true;
                case "balanceValue":
                        return true;
                case "rightStartDate":
                        return true;
                case "rightEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "disposalRight":
                    return true;
                case "number":
                    return true;
                case "location":
                    return true;
                case "area":
                    return true;
                case "balanceValue":
                    return true;
                case "rightStartDate":
                    return true;
                case "rightEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "disposalRight":
                    return UniplacesDisposalRight.class;
                case "number":
                    return Integer.class;
                case "location":
                    return String.class;
                case "area":
                    return Long.class;
                case "balanceValue":
                    return Long.class;
                case "rightStartDate":
                    return Date.class;
                case "rightEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniplacesGround> _dslPath = new Path<UniplacesGround>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniplacesGround");
    }
            

    /**
     * @return Право распоряжения. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesGround#getDisposalRight()
     */
    public static UniplacesDisposalRight.Path<UniplacesDisposalRight> disposalRight()
    {
        return _dslPath.disposalRight();
    }

    /**
     * @return Порядковый номер. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesGround#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Местоположение. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesGround#getLocation()
     */
    public static PropertyPath<String> location()
    {
        return _dslPath.location();
    }

    /**
     * @return Площадь кв. м.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesGround#getArea()
     */
    public static PropertyPath<Long> area()
    {
        return _dslPath.area();
    }

    /**
     * @return Балансовая стоимость.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesGround#getBalanceValue()
     */
    public static PropertyPath<Long> balanceValue()
    {
        return _dslPath.balanceValue();
    }

    /**
     * @return Дата начала распоряжения.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesGround#getRightStartDate()
     */
    public static PropertyPath<Date> rightStartDate()
    {
        return _dslPath.rightStartDate();
    }

    /**
     * @return Дата завершения распоряжения.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesGround#getRightEndDate()
     */
    public static PropertyPath<Date> rightEndDate()
    {
        return _dslPath.rightEndDate();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesGround#getFractionalArea()
     */
    public static SupportedPropertyPath<Double> fractionalArea()
    {
        return _dslPath.fractionalArea();
    }

    public static class Path<E extends UniplacesGround> extends UniplacesRegistryRecord.Path<E>
    {
        private UniplacesDisposalRight.Path<UniplacesDisposalRight> _disposalRight;
        private PropertyPath<Integer> _number;
        private PropertyPath<String> _location;
        private PropertyPath<Long> _area;
        private PropertyPath<Long> _balanceValue;
        private PropertyPath<Date> _rightStartDate;
        private PropertyPath<Date> _rightEndDate;
        private SupportedPropertyPath<Double> _fractionalArea;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Право распоряжения. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesGround#getDisposalRight()
     */
        public UniplacesDisposalRight.Path<UniplacesDisposalRight> disposalRight()
        {
            if(_disposalRight == null )
                _disposalRight = new UniplacesDisposalRight.Path<UniplacesDisposalRight>(L_DISPOSAL_RIGHT, this);
            return _disposalRight;
        }

    /**
     * @return Порядковый номер. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesGround#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(UniplacesGroundGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Местоположение. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesGround#getLocation()
     */
        public PropertyPath<String> location()
        {
            if(_location == null )
                _location = new PropertyPath<String>(UniplacesGroundGen.P_LOCATION, this);
            return _location;
        }

    /**
     * @return Площадь кв. м.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesGround#getArea()
     */
        public PropertyPath<Long> area()
        {
            if(_area == null )
                _area = new PropertyPath<Long>(UniplacesGroundGen.P_AREA, this);
            return _area;
        }

    /**
     * @return Балансовая стоимость.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesGround#getBalanceValue()
     */
        public PropertyPath<Long> balanceValue()
        {
            if(_balanceValue == null )
                _balanceValue = new PropertyPath<Long>(UniplacesGroundGen.P_BALANCE_VALUE, this);
            return _balanceValue;
        }

    /**
     * @return Дата начала распоряжения.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesGround#getRightStartDate()
     */
        public PropertyPath<Date> rightStartDate()
        {
            if(_rightStartDate == null )
                _rightStartDate = new PropertyPath<Date>(UniplacesGroundGen.P_RIGHT_START_DATE, this);
            return _rightStartDate;
        }

    /**
     * @return Дата завершения распоряжения.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesGround#getRightEndDate()
     */
        public PropertyPath<Date> rightEndDate()
        {
            if(_rightEndDate == null )
                _rightEndDate = new PropertyPath<Date>(UniplacesGroundGen.P_RIGHT_END_DATE, this);
            return _rightEndDate;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesGround#getFractionalArea()
     */
        public SupportedPropertyPath<Double> fractionalArea()
        {
            if(_fractionalArea == null )
                _fractionalArea = new SupportedPropertyPath<Double>(UniplacesGroundGen.P_FRACTIONAL_AREA, this);
            return _fractionalArea;
        }

        public Class getEntityClass()
        {
            return UniplacesGround.class;
        }

        public String getEntityName()
        {
            return "uniplacesGround";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getFractionalArea();
}
