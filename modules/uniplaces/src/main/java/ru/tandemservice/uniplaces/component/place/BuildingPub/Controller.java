/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.BuildingPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniplaces.UniplacesComponents;
import ru.tandemservice.uniplaces.component.place.AbstractPlaceObjectPub.AbstractPlaceObjectPubController;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

/**
 * @author agolubenko
 * @since Aug 16, 2010
 */
public class Controller extends AbstractPlaceObjectPubController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        super.onRefreshComponent(component);
        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<UniplacesUnit> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Название", UniplacesUnit.title().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Этажность", UniplacesUnit.floorNumber().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Площадь, кв. м", UniplacesUnit.P_FRACTIONAL_AREA, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditUnit").setPermissionKey("editUnit_building"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteUnit", "Удалить блок {0}?", UniplacesUnit.title().s()).setPermissionKey("deleteUnit_building"));

        dataSource.setCountRow(5);
        getModel(component).setDataSource(dataSource);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(UniplacesComponents.BUILDING_ADD_EDIT));
    }

    public void onClickEditUnit(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                UniplacesComponents.UNIT_ADD_EDIT, new ParametersMap().add("unitId", component.getListenerParameter())
        ));
    }


    public void onClickAddUnit(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                UniplacesComponents.UNIT_ADD_EDIT, new ParametersMap().add("unitId", null)
        ));
    }

    public void onClickDeleteUnit(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }
}
