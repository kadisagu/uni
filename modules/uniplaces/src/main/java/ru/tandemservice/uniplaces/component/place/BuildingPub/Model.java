/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.BuildingPub;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniplaces.component.place.AbstractPlaceObjectPub.AbstractPlaceObjectPubModel;
import ru.tandemservice.uniplaces.entity.place.UniplacesAssignedDocument;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

/**
 * @author agolubenko
 * @since Aug 16, 2010
 */
@State(@Bind(key = "publisherId", binding = "building.id"))
@Output(@Bind(key = "buildingId", binding = "building.id"))
public class Model extends AbstractPlaceObjectPubModel
{
    private UniplacesBuilding _building = new UniplacesBuilding();
    private List<UniplacesAssignedDocument> _assignedDocuments;
    private UniplacesAssignedDocument _currentAssignedDocument;
    DynamicListDataSource<UniplacesUnit> _dataSource;
    private Double unitArea = .0;
    private Double placesArea = .0;

    public DynamicListDataSource<UniplacesUnit> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<UniplacesUnit> dataSource)
    {
        _dataSource = dataSource;
    }

    public UniplacesBuilding getBuilding()
    {
        return _building;
    }

    public void setBuilding(UniplacesBuilding building)
    {
        _building = building;
    }

    public List<UniplacesAssignedDocument> getAssignedDocuments()
    {
        return _assignedDocuments;
    }

    public void setAssignedDocuments(List<UniplacesAssignedDocument> assignedDocuments)
    {
        _assignedDocuments = assignedDocuments;
    }

    public UniplacesAssignedDocument getCurrentAssignedDocument()
    {
        return _currentAssignedDocument;
    }

    public void setCurrentAssignedDocument(UniplacesAssignedDocument currentAssignedDocument)
    {
        _currentAssignedDocument = currentAssignedDocument;
    }

    public Double getUnitArea()
    {
        return unitArea;
    }

    public void setUnitArea(Double unitArea)
    {
        this.unitArea = unitArea;
    }

    public Double getPlacesArea()
    {
        return placesArea;
    }

    public void setPlacesArea(Double placesArea)
    {
        this.placesArea = placesArea;
    }

    @Override
    public Long getObjectID()
    {
        return _building.getId();
    }

    @Override
    public String initPostfix()
    {
        return "building";
    }


}

