package ru.tandemservice.uniplaces.entity.place.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniplaces.entity.place.UniplacesDocument;
import ru.tandemservice.uniplaces.entity.place.UniplacesDocumentRight;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Документ-право
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniplacesDocumentRightGen extends UniplacesDocument
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplaces.entity.place.UniplacesDocumentRight";
    public static final String ENTITY_NAME = "uniplacesDocumentRight";
    public static final int VERSION_HASH = -309825986;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UniplacesDocumentRightGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniplacesDocumentRightGen> extends UniplacesDocument.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniplacesDocumentRight.class;
        }

        public T newInstance()
        {
            return (T) new UniplacesDocumentRight();
        }
    }
    private static final Path<UniplacesDocumentRight> _dslPath = new Path<UniplacesDocumentRight>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniplacesDocumentRight");
    }
            

    public static class Path<E extends UniplacesDocumentRight> extends UniplacesDocument.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return UniplacesDocumentRight.class;
        }

        public String getEntityName()
        {
            return "uniplacesDocumentRight";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
