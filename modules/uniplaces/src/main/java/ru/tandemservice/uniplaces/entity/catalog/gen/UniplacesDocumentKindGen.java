package ru.tandemservice.uniplaces.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentKind;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид документа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniplacesDocumentKindGen extends EntityBase
 implements INaturalIdentifiable<UniplacesDocumentKindGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentKind";
    public static final String ENTITY_NAME = "uniplacesDocumentKind";
    public static final int VERSION_HASH = 733555109;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_PARENT = "parent";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_DOCUMENT_CLASS_NAME = "documentClassName";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private UniplacesDocumentKind _parent;     // Вид документа
    private String _shortTitle;     // Сокращенное название
    private String _documentClassName;     // Имя класса документа
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Вид документа.
     */
    public UniplacesDocumentKind getParent()
    {
        return _parent;
    }

    /**
     * @param parent Вид документа.
     */
    public void setParent(UniplacesDocumentKind parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Имя класса документа.
     */
    @Length(max=255)
    public String getDocumentClassName()
    {
        return _documentClassName;
    }

    /**
     * @param documentClassName Имя класса документа.
     */
    public void setDocumentClassName(String documentClassName)
    {
        dirty(_documentClassName, documentClassName);
        _documentClassName = documentClassName;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniplacesDocumentKindGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((UniplacesDocumentKind)another).getCode());
            }
            setParent(((UniplacesDocumentKind)another).getParent());
            setShortTitle(((UniplacesDocumentKind)another).getShortTitle());
            setDocumentClassName(((UniplacesDocumentKind)another).getDocumentClassName());
            setTitle(((UniplacesDocumentKind)another).getTitle());
        }
    }

    public INaturalId<UniplacesDocumentKindGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<UniplacesDocumentKindGen>
    {
        private static final String PROXY_NAME = "UniplacesDocumentKindNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UniplacesDocumentKindGen.NaturalId) ) return false;

            UniplacesDocumentKindGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniplacesDocumentKindGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniplacesDocumentKind.class;
        }

        public T newInstance()
        {
            return (T) new UniplacesDocumentKind();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "parent":
                    return obj.getParent();
                case "shortTitle":
                    return obj.getShortTitle();
                case "documentClassName":
                    return obj.getDocumentClassName();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "parent":
                    obj.setParent((UniplacesDocumentKind) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "documentClassName":
                    obj.setDocumentClassName((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "parent":
                        return true;
                case "shortTitle":
                        return true;
                case "documentClassName":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "parent":
                    return true;
                case "shortTitle":
                    return true;
                case "documentClassName":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "parent":
                    return UniplacesDocumentKind.class;
                case "shortTitle":
                    return String.class;
                case "documentClassName":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniplacesDocumentKind> _dslPath = new Path<UniplacesDocumentKind>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniplacesDocumentKind");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentKind#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Вид документа.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentKind#getParent()
     */
    public static UniplacesDocumentKind.Path<UniplacesDocumentKind> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentKind#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Имя класса документа.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentKind#getDocumentClassName()
     */
    public static PropertyPath<String> documentClassName()
    {
        return _dslPath.documentClassName();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentKind#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends UniplacesDocumentKind> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private UniplacesDocumentKind.Path<UniplacesDocumentKind> _parent;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _documentClassName;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentKind#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(UniplacesDocumentKindGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Вид документа.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentKind#getParent()
     */
        public UniplacesDocumentKind.Path<UniplacesDocumentKind> parent()
        {
            if(_parent == null )
                _parent = new UniplacesDocumentKind.Path<UniplacesDocumentKind>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentKind#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(UniplacesDocumentKindGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Имя класса документа.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentKind#getDocumentClassName()
     */
        public PropertyPath<String> documentClassName()
        {
            if(_documentClassName == null )
                _documentClassName = new PropertyPath<String>(UniplacesDocumentKindGen.P_DOCUMENT_CLASS_NAME, this);
            return _documentClassName;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentKind#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(UniplacesDocumentKindGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return UniplacesDocumentKind.class;
        }

        public String getEntityName()
        {
            return "uniplacesDocumentKind";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
