package ru.tandemservice.uniplaces.entity.place;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.uniplaces.entity.place.gen.UniplacesGroundGen;

/**
 * Земельный участок
 */
public class UniplacesGround extends UniplacesGroundGen
{
    public static final String P_FRACTIONAL_AREA = "fractionalArea";

    @Override
    @EntityDSLSupport(parts = {P_AREA})
    public Double getFractionalArea()
    {
        Long area = getArea();
        return (area != null) ? area / 100.0 : null;
    }

    @EntityDSLSupport(parts = {P_AREA})
    public void setFractionalArea(Double area)
    {
        setArea((area != null) ? Math.round(area * 100.0d) : null);
    }

    public static final String P_FRACTIONAL_BALANCE_VALUE = "fractionalBalanceValue";

    public Double getFractionalBalanceValue()
    {
        Long balanceValue = getBalanceValue();
        return (balanceValue != null) ? balanceValue / 100.0 : null;
    }

    public void setFractionalBalanceValue(Double balanceValue)
    {
        setBalanceValue((balanceValue != null) ? new Double(balanceValue * 100).longValue() : null);
    }
    
    public static final String P_FULL_TITLE = "fullTitle";
    
    public String getFullTitle()
    {
        return String.format("№%s (%s)", getCadastreRecordNumber(), getLocation()) + (getFractionalArea() == null ? "" : String.format(" %02.2f кв. м", getFractionalArea()));
    }
}