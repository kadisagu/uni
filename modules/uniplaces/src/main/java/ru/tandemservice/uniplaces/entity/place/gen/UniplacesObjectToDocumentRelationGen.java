package ru.tandemservice.uniplaces.entity.place.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation;
import ru.tandemservice.uniplaces.entity.place.UniplacesRegistryRecord;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь объекта модуля Здания и Помещения с приложенным документом
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniplacesObjectToDocumentRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation";
    public static final String ENTITY_NAME = "uniplacesObjectToDocumentRelation";
    public static final int VERSION_HASH = -687142969;
    private static IEntityMeta ENTITY_META;

    public static final String L_OBJECT = "object";
    public static final String P_DOCUMENT = "document";
    public static final String P_TITLE = "title";
    public static final String P_START_DATE = "startDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_COMMENT = "comment";
    public static final String P_ACTIVE = "active";

    private UniplacesRegistryRecord _object;     // Объект, к которому приложен документ
    private Long _document;     // Файл скан-копии
    private String _title;     // Наименование
    private Date _startDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private String _comment;     // Примечание
    private boolean _active = true;     // Признак архивности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Объект, к которому приложен документ. Свойство не может быть null.
     */
    @NotNull
    public UniplacesRegistryRecord getObject()
    {
        return _object;
    }

    /**
     * @param object Объект, к которому приложен документ. Свойство не может быть null.
     */
    public void setObject(UniplacesRegistryRecord object)
    {
        dirty(_object, object);
        _object = object;
    }

    /**
     * @return Файл скан-копии.
     */
    public Long getDocument()
    {
        return _document;
    }

    /**
     * @param document Файл скан-копии.
     */
    public void setDocument(Long document)
    {
        dirty(_document, document);
        _document = document;
    }

    /**
     * @return Наименование. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Наименование. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Дата начала.
     */
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Дата начала.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Примечание.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Примечание.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Признак архивности. Свойство не может быть null.
     */
    @NotNull
    public boolean isActive()
    {
        return _active;
    }

    /**
     * @param active Признак архивности. Свойство не может быть null.
     */
    public void setActive(boolean active)
    {
        dirty(_active, active);
        _active = active;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniplacesObjectToDocumentRelationGen)
        {
            setObject(((UniplacesObjectToDocumentRelation)another).getObject());
            setDocument(((UniplacesObjectToDocumentRelation)another).getDocument());
            setTitle(((UniplacesObjectToDocumentRelation)another).getTitle());
            setStartDate(((UniplacesObjectToDocumentRelation)another).getStartDate());
            setEndDate(((UniplacesObjectToDocumentRelation)another).getEndDate());
            setComment(((UniplacesObjectToDocumentRelation)another).getComment());
            setActive(((UniplacesObjectToDocumentRelation)another).isActive());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniplacesObjectToDocumentRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniplacesObjectToDocumentRelation.class;
        }

        public T newInstance()
        {
            return (T) new UniplacesObjectToDocumentRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "object":
                    return obj.getObject();
                case "document":
                    return obj.getDocument();
                case "title":
                    return obj.getTitle();
                case "startDate":
                    return obj.getStartDate();
                case "endDate":
                    return obj.getEndDate();
                case "comment":
                    return obj.getComment();
                case "active":
                    return obj.isActive();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "object":
                    obj.setObject((UniplacesRegistryRecord) value);
                    return;
                case "document":
                    obj.setDocument((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "active":
                    obj.setActive((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "object":
                        return true;
                case "document":
                        return true;
                case "title":
                        return true;
                case "startDate":
                        return true;
                case "endDate":
                        return true;
                case "comment":
                        return true;
                case "active":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "object":
                    return true;
                case "document":
                    return true;
                case "title":
                    return true;
                case "startDate":
                    return true;
                case "endDate":
                    return true;
                case "comment":
                    return true;
                case "active":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "object":
                    return UniplacesRegistryRecord.class;
                case "document":
                    return Long.class;
                case "title":
                    return String.class;
                case "startDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "comment":
                    return String.class;
                case "active":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniplacesObjectToDocumentRelation> _dslPath = new Path<UniplacesObjectToDocumentRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniplacesObjectToDocumentRelation");
    }
            

    /**
     * @return Объект, к которому приложен документ. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation#getObject()
     */
    public static UniplacesRegistryRecord.Path<UniplacesRegistryRecord> object()
    {
        return _dslPath.object();
    }

    /**
     * @return Файл скан-копии.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation#getDocument()
     */
    public static PropertyPath<Long> document()
    {
        return _dslPath.document();
    }

    /**
     * @return Наименование. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Дата начала.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Примечание.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Признак архивности. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    public static class Path<E extends UniplacesObjectToDocumentRelation> extends EntityPath<E>
    {
        private UniplacesRegistryRecord.Path<UniplacesRegistryRecord> _object;
        private PropertyPath<Long> _document;
        private PropertyPath<String> _title;
        private PropertyPath<Date> _startDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<String> _comment;
        private PropertyPath<Boolean> _active;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Объект, к которому приложен документ. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation#getObject()
     */
        public UniplacesRegistryRecord.Path<UniplacesRegistryRecord> object()
        {
            if(_object == null )
                _object = new UniplacesRegistryRecord.Path<UniplacesRegistryRecord>(L_OBJECT, this);
            return _object;
        }

    /**
     * @return Файл скан-копии.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation#getDocument()
     */
        public PropertyPath<Long> document()
        {
            if(_document == null )
                _document = new PropertyPath<Long>(UniplacesObjectToDocumentRelationGen.P_DOCUMENT, this);
            return _document;
        }

    /**
     * @return Наименование. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(UniplacesObjectToDocumentRelationGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Дата начала.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(UniplacesObjectToDocumentRelationGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(UniplacesObjectToDocumentRelationGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Примечание.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(UniplacesObjectToDocumentRelationGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Признак архивности. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(UniplacesObjectToDocumentRelationGen.P_ACTIVE, this);
            return _active;
        }

        public Class getEntityClass()
        {
            return UniplacesObjectToDocumentRelation.class;
        }

        public String getEntityName()
        {
            return "uniplacesObjectToDocumentRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
