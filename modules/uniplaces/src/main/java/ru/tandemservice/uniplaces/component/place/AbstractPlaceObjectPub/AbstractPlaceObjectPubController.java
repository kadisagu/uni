/* $Id$ */
package ru.tandemservice.uniplaces.component.place.AbstractPlaceObjectPub;

import org.tandemframework.caf.ui.activator.impl.DialogComponentActivationBuilder;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniplaces.base.bo.UniplacesDocuments.UniplacesDocumentsManager;
import ru.tandemservice.uniplaces.base.bo.UniplacesDocuments.ui.AddEdit.UniplacesDocumentsAddEdit;
import ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation;


/**
 * @author Ekaterina Zvereva
 * @since 16.02.2015
 */
public abstract class AbstractPlaceObjectPubController<T extends IAbstractPlaceObjectPubDAO, Model extends AbstractPlaceObjectPubModel>  extends AbstractBusinessController<T, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        prepareDocumentDataSource(component);
    }

    protected void prepareDocumentDataSource(IBusinessComponent component)
    {
        if (getModel(component).getDocumentDataSource() != null) return;

        DynamicListDataSource<UniplacesObjectToDocumentRelation> documentDataSource = UniBaseUtils.createDataSource(component, getDao());
        documentDataSource.addColumn(new PublisherLinkColumn("Название", UniplacesObjectToDocumentRelation.title()));
        documentDataSource.addColumn(new SimpleColumn("Дата начала", UniplacesObjectToDocumentRelation.startDate()).setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER));
        documentDataSource.addColumn(new SimpleColumn("Дата окончания", UniplacesObjectToDocumentRelation.endDate()).setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER));
        documentDataSource.addColumn(new SimpleColumn("Примечание", UniplacesObjectToDocumentRelation.comment()).setOrderable(false));
        documentDataSource.addColumn(new ToggleColumn("Активный", UniplacesObjectToDocumentRelation.active()).toggleOnListener("onClickChangeArchiveProperty")
                                             .toggleOffListener("onClickChangeArchiveProperty").setPermissionKey("changeActiveDocument_"+ getModel(component).getPostfix()).setClickable(true));
        documentDataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrintDocument", "Печать").setPermissionKey("printPlaceDocument_"+ getModel(component).getPostfix()));
        documentDataSource.addColumn(new ActionColumn("edit", ActionColumn.EDIT, "onClickEditDocument").setPermissionKey("editPlaceDocument_"+ getModel(component).getPostfix()));
        documentDataSource.addColumn(new ActionColumn("delete", ActionColumn.DELETE, "onClickDeleteDocument").setPermissionKey("deletePlaceDocument_"+ getModel(component).getPostfix()));

        getModel(component).setDocumentDataSource(documentDataSource);
    }

    public void onClickPrintDocument(IBusinessComponent component)
    {
        final UniplacesObjectToDocumentRelation document = getDao().getNotNull(UniplacesObjectToDocumentRelation.class, (Long) component.getListenerParameter());
        UniplacesDocumentsManager.instance().dao().printDocumentScanCopy(document);
    }

    public void onClickDeleteDocument(IBusinessComponent component)
    {
        UniplacesDocumentsManager.instance().dao().deleteDocument((Long)component.getListenerParameter());
    }

    public void onClickChangeArchiveProperty(IBusinessComponent component)
    {
        UniplacesDocumentsManager.instance().dao().changeActiveProperty((Long) component.getListenerParameter());
    }

    public void onClickAddDocument(IBusinessComponent component)
    {
        new DialogComponentActivationBuilder(UniplacesDocumentsAddEdit.class)
                .parameter("objectId", getModel(component).getObjectID())
                .activate();
    }

    public void onClickEditDocument(IBusinessComponent component)
    {
        new DialogComponentActivationBuilder(UniplacesDocumentsAddEdit.class).parameter("documentId", component.getListenerParameter())
                .parameter("objectId", getModel(component).getObjectID())
                .activate();
    }
}