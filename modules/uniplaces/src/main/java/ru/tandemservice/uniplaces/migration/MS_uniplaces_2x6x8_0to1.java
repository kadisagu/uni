package ru.tandemservice.uniplaces.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniplaces_2x6x8_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность uniplacesClassroomType

		// создана новая сущность
		{
			if(!tool.tableExists("places_classroom_type"))
			{
				// создать таблицу
				DBTable dbt = new DBTable("places_classroom_type",
						new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
						new DBColumn("discriminator", DBType.SHORT).setNullable(false),
						new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
						new DBColumn("shorttitle_p", DBType.createVarchar(255)).setNullable(false),
						new DBColumn("title_p", DBType.createVarchar(1200))
				);
				tool.createTable(dbt);
			}

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("uniplacesClassroomType");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность uniplacesPlace

		// создано свойство classroomType
		if(tool.tableExists("places_place") && !tool.columnExists("places_place", "classroomtype_id"))
		{
			// создать колонку
			tool.createColumn("places_place", new DBColumn("classroomtype_id", DBType.LONG));

		}

		// создано свойство capacity
		if(tool.tableExists("places_place") && !tool.columnExists("places_place", "capacity_p"))
		{
			// создать колонку
			tool.createColumn("places_place", new DBColumn("capacity_p", DBType.INTEGER));

		}
    }
}