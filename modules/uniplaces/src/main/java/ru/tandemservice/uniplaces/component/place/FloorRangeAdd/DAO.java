/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniplaces.component.place.FloorRangeAdd;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.info.ErrorCollector;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

/**
 * @author Julia Oschepkova
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        UniplacesUnit unit = getNotNull(UniplacesUnit.class, model.getUnit().getId());
        model.setUnit(unit);
    }

    @Override
    public void update(Model model)
    {
        //нашли макс номер
        Criteria criteria = getSession().createCriteria(UniplacesFloor.class);
        criteria.setProjection(Projections.max(UniplacesFloor.P_NUMBER));
        criteria.add(Restrictions.eq(UniplacesFloor.L_UNIT, model.getUnit()));

        Number result = (Number) criteria.uniqueResult();

        int max = (null == result) ? 0 : result.intValue() + 1;

        for (int i = model.getFrom(); i <= model.getTo(); i++)
        {
            UniplacesFloor floor = new UniplacesFloor();

            floor.setUnit(model.getUnit());
            floor.setResponsibleOrgUnit(model.getUnit().getResponsibleOrgUnit());
            floor.setTitle(Integer.toString(i));
            floor.setNumber(max);
            getSession().save(floor);
            max++;
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (model.getFrom() > model.getTo()) errors.add("Диапазон значений задан неверно", "from", "to");
    }
}

