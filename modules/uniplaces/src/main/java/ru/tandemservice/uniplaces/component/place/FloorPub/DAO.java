/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.FloorPub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniplaces.component.place.AbstractPlaceObjectPub.AbstractPlaceObjectPubDAO;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

/**
 * @author agolubenko
 * @since Aug 24, 2010
 */
public class DAO extends AbstractPlaceObjectPubDAO<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("place");

    @Override
    public void prepare(Model model)
    {
        model.setFloor(getNotNull(UniplacesFloor.class, model.getFloor().getId()));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        super.prepareListDataSource(model);
        DynamicListDataSource<UniplacesPlace> dataSource = model.getDataSource();

        MQBuilder builder = new MQBuilder(UniplacesPlace.ENTITY_CLASS, "place");
        builder.addJoinFetch("place", UniplacesPlace.floor().s(), "floor");
        builder.addJoinFetch("floor", UniplacesFloor.unit().s(), "unit");
        builder.addJoinFetch("unit", UniplacesUnit.building().s(), "building");
        builder.add(MQExpression.eq("place", UniplacesPlace.floor().s(), model.getFloor()));

        _orderSettings.applyOrder(builder, dataSource.getEntityOrder());
        UniBaseUtils.createPage(dataSource, builder, getSession());
    }
}
