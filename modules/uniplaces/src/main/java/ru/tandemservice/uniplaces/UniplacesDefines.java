/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces;

/**
 * @author agolubenko
 * @since Aug 3, 2010
 */
public interface UniplacesDefines
{
    String MODULE_SETTINGS_KEY = "uniplaces.";
    String SETTINGS_BUILDING_NAMING = "buildingNaming";
    String SETTINGS_CADASTRE_RECORD_NAMING = "cadastreRecordNaming";

    String CATALOG_DOCUMENT_PURPOSE_GROUND_DISPOSAL_RIGHT = "1";
    String CATALOG_DOCUMENT_PURPOSE_BUILDING_DISPOSAL_RIGHT = "2";
    String CATALOG_DOCUMENT_PURPOSE_BUILDING_OPERATION_START = "3";
    String CATALOG_DOCUMENT_PURPOSE_BUILDING_SANITARIAL_OPERATION_START = "4";
    String CATALOG_DOCUMENT_PURPOSE_BUILDING_FIRE_PREVENTION_OPERATION_START = "5";

    String CATALOG_PLACE_NUMBERING_TYPE_BY_FLOOR = "1";
    String CATALOG_PLACE_NUMBERING_TYPE_CONTINUOUS = "2";
    
    String CATALOG_PLACE_PURPOSE_HABITABLE = "12";
    String CATALOG_PLACE_PURPOSE_FAMILY = "26";
}
