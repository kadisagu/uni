package ru.tandemservice.uniplaces.entity.place.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentKind;
import ru.tandemservice.uniplaces.entity.place.UniplacesDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Документ (здания и помещения)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniplacesDocumentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplaces.entity.place.UniplacesDocument";
    public static final String ENTITY_NAME = "uniplacesDocument";
    public static final int VERSION_HASH = -1271047855;
    private static IEntityMeta ENTITY_META;

    public static final String L_KIND = "kind";
    public static final String P_NUMBER = "number";
    public static final String P_ISSUANCE_DATE = "issuanceDate";
    public static final String P_EXPIRY_DATE = "expiryDate";

    private UniplacesDocumentKind _kind;     // Вид документа
    private String _number;     // Номер
    private Date _issuanceDate;     // Дата выдачи
    private Date _expiryDate;     // Дата действия документа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид документа. Свойство не может быть null.
     */
    @NotNull
    public UniplacesDocumentKind getKind()
    {
        return _kind;
    }

    /**
     * @param kind Вид документа. Свойство не может быть null.
     */
    public void setKind(UniplacesDocumentKind kind)
    {
        dirty(_kind, kind);
        _kind = kind;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата выдачи.
     */
    public Date getIssuanceDate()
    {
        return _issuanceDate;
    }

    /**
     * @param issuanceDate Дата выдачи.
     */
    public void setIssuanceDate(Date issuanceDate)
    {
        dirty(_issuanceDate, issuanceDate);
        _issuanceDate = issuanceDate;
    }

    /**
     * @return Дата действия документа.
     */
    public Date getExpiryDate()
    {
        return _expiryDate;
    }

    /**
     * @param expiryDate Дата действия документа.
     */
    public void setExpiryDate(Date expiryDate)
    {
        dirty(_expiryDate, expiryDate);
        _expiryDate = expiryDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniplacesDocumentGen)
        {
            setKind(((UniplacesDocument)another).getKind());
            setNumber(((UniplacesDocument)another).getNumber());
            setIssuanceDate(((UniplacesDocument)another).getIssuanceDate());
            setExpiryDate(((UniplacesDocument)another).getExpiryDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniplacesDocumentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniplacesDocument.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("UniplacesDocument is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "kind":
                    return obj.getKind();
                case "number":
                    return obj.getNumber();
                case "issuanceDate":
                    return obj.getIssuanceDate();
                case "expiryDate":
                    return obj.getExpiryDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "kind":
                    obj.setKind((UniplacesDocumentKind) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "issuanceDate":
                    obj.setIssuanceDate((Date) value);
                    return;
                case "expiryDate":
                    obj.setExpiryDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "kind":
                        return true;
                case "number":
                        return true;
                case "issuanceDate":
                        return true;
                case "expiryDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "kind":
                    return true;
                case "number":
                    return true;
                case "issuanceDate":
                    return true;
                case "expiryDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "kind":
                    return UniplacesDocumentKind.class;
                case "number":
                    return String.class;
                case "issuanceDate":
                    return Date.class;
                case "expiryDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniplacesDocument> _dslPath = new Path<UniplacesDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniplacesDocument");
    }
            

    /**
     * @return Вид документа. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesDocument#getKind()
     */
    public static UniplacesDocumentKind.Path<UniplacesDocumentKind> kind()
    {
        return _dslPath.kind();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesDocument#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата выдачи.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesDocument#getIssuanceDate()
     */
    public static PropertyPath<Date> issuanceDate()
    {
        return _dslPath.issuanceDate();
    }

    /**
     * @return Дата действия документа.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesDocument#getExpiryDate()
     */
    public static PropertyPath<Date> expiryDate()
    {
        return _dslPath.expiryDate();
    }

    public static class Path<E extends UniplacesDocument> extends EntityPath<E>
    {
        private UniplacesDocumentKind.Path<UniplacesDocumentKind> _kind;
        private PropertyPath<String> _number;
        private PropertyPath<Date> _issuanceDate;
        private PropertyPath<Date> _expiryDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид документа. Свойство не может быть null.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesDocument#getKind()
     */
        public UniplacesDocumentKind.Path<UniplacesDocumentKind> kind()
        {
            if(_kind == null )
                _kind = new UniplacesDocumentKind.Path<UniplacesDocumentKind>(L_KIND, this);
            return _kind;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesDocument#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(UniplacesDocumentGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата выдачи.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesDocument#getIssuanceDate()
     */
        public PropertyPath<Date> issuanceDate()
        {
            if(_issuanceDate == null )
                _issuanceDate = new PropertyPath<Date>(UniplacesDocumentGen.P_ISSUANCE_DATE, this);
            return _issuanceDate;
        }

    /**
     * @return Дата действия документа.
     * @see ru.tandemservice.uniplaces.entity.place.UniplacesDocument#getExpiryDate()
     */
        public PropertyPath<Date> expiryDate()
        {
            if(_expiryDate == null )
                _expiryDate = new PropertyPath<Date>(UniplacesDocumentGen.P_EXPIRY_DATE, this);
            return _expiryDate;
        }

        public Class getEntityClass()
        {
            return UniplacesDocument.class;
        }

        public String getEntityName()
        {
            return "uniplacesDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
