/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.FloorPub;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniplaces.UniplacesComponents;
import ru.tandemservice.uniplaces.component.place.AbstractPlaceObjectPub.AbstractPlaceObjectPubController;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;

import java.util.Collection;

/**
 * @author agolubenko
 * @since Aug 24, 2010
 */
public class Controller extends AbstractPlaceObjectPubController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        getDao().prepare(getModel(component));
        prepareDataSource(component);
    }

    public void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<UniplacesPlace> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new CheckboxColumn("select"));
        dataSource.addColumn(new SimpleColumn("Номер", UniplacesPlace.number().s()));
        dataSource.addColumn(new SimpleColumn("Площадь, кв. м", UniplacesPlace.P_FRACTIONAL_AREA, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditPlace").setPermissionKey("editPlace_floor"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeletePlace", "Удалить помещение {0}?", UniplacesPlace.title().s()).setPermissionKey("deletePlace_floor"));

        model.setDataSource(dataSource);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                UniplacesComponents.FLOOR_ADD_EDIT, new ParametersMap().add("floorId", getModel(component).getFloor().getId())
        ));
    }

    public void onClickAddPlace(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                UniplacesComponents.PLACE_ADD_EDIT, new ParametersMap().add("floorId", getModel(component).getFloor().getId())
        ));
    }
    
    public void onClickAddPlaceRange(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(UniplacesComponents.PLACE_RANGE_ADD, new ParametersMap().add("floorId", getModel(component).getFloor().getId())));
    }

    public void onClickEditPlace(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                UniplacesComponents.PLACE_ADD_EDIT, new ParametersMap().add("placeId", component.getListenerParameter()).add("floorId", null)
        ));
    }

    public void onClickDeletePlace(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }

    public void onClickMassEditPlace(IBusinessComponent component)
    {
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try  {
            final Model model = this.getModel(component);
            final CheckboxColumn checkboxColumn = (CheckboxColumn)model.getDataSource().getColumn("select");
            final Collection<Long> ids = UniBaseDao.ids(checkboxColumn.getSelectedObjects());
            if (ids.isEmpty()) { throw new ApplicationException("Не выбрано ни одно помещение."); }
                ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                    UniplacesComponents.PLACE_MASS_EDIT, new ParametersMap().add("placeIds", ids)
                ));
        } finally {
            eventLock.release();
        }
    }

    public void onClickMassDeletePlace(IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final CheckboxColumn checkboxColumn = (CheckboxColumn)model.getDataSource().getColumn("select");
        final Collection<Long> ids = UniBaseDao.ids(checkboxColumn.getSelectedObjects());
        if (ids.isEmpty()) { throw new ApplicationException("Не выбрано ни одно помещение."); }
        int itemsDeleted = 0;
        for (Long id : ids)
        {
            try
            {
                UniDaoFacade.getCoreDao().delete(id);
                itemsDeleted++;
            }
            catch (Throwable e){}
        }
        ContextLocal.getInfoCollector().add("Из выбранных помещений ("+ids.size()+") удалено: " + itemsDeleted + ", невозможно удалить: "+(ids.size() - itemsDeleted)+".");
        checkboxColumn.getSelectedObjects().clear();
    }
}
