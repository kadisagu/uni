/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.util.cache.SafeMap;

import ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentKind;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentPurpose;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;

/**
 * @author agolubenko
 * @since Aug 16, 2010
 */
public class PlacesUtil
{
    /**
     * @param documentKinds виды документов
     * @param documentPurpose назначение документа
     * @return типы документов
     */
    public static List<UniplacesDocumentKind> getFilteredDocumentKinds(List<UniplacesDocumentKind> documentKinds, UniplacesDocumentPurpose documentPurpose)
    {
        Set<UniplacesDocumentKind> result = new HashSet<UniplacesDocumentKind>();

        // для каждого типа документа
        for (UniplacesDocumentKind documentKind : documentKinds)
        {
            // будем проверять цепочку родителей 
            List<UniplacesDocumentKind> filteredDocumentKinds = new ArrayList<UniplacesDocumentKind>();
            for (UniplacesDocumentKind parent = documentKind; parent != null; parent = parent.getParent())
            {
                // если соответствует виду назначения, или уже обрабатывался, то добавляем в набор
                if (parent.equals(documentPurpose.getDocumentKind()) || result.contains(parent))
                {
                    result.addAll(filteredDocumentKinds);
                }
                filteredDocumentKinds.add(parent);
            }
        }
        return new ArrayList<UniplacesDocumentKind>(result);
    }

    public static List<UniplacesPlacePurpose> sortByHierarchy(List<UniplacesPlacePurpose> source)
    {
        Map<UniplacesPlacePurpose, Set<UniplacesPlacePurpose>> graphMap = new HashMap<UniplacesPlacePurpose, Set<UniplacesPlacePurpose>>();
        List<UniplacesPlacePurpose> topLevel = new ArrayList<UniplacesPlacePurpose>();
        for (UniplacesPlacePurpose element : source)
        {
            UniplacesPlacePurpose parent = element.getParent();
            if (null == parent || !source.contains(parent))
                topLevel.add(element);
            else
                SafeMap.safeGet(graphMap, parent, HashSet.class).add(element);
        }
        Collections.sort(topLevel, ITitled.TITLED_COMPARATOR);
        List<UniplacesPlacePurpose> result = new ArrayList<UniplacesPlacePurpose>();
        for (UniplacesPlacePurpose element : topLevel)
            add(result, element, graphMap);
        return result;
    }

    private static void add(List<UniplacesPlacePurpose> result, UniplacesPlacePurpose element, Map<UniplacesPlacePurpose, Set<UniplacesPlacePurpose>> graphMap)
    {
        result.add(element);
        Set<UniplacesPlacePurpose> set = graphMap.get(element);
        if (null == set || set.isEmpty())
            return;
        List<UniplacesPlacePurpose> list = new ArrayList<UniplacesPlacePurpose>(set);
        Collections.sort(list, ITitled.TITLED_COMPARATOR);
        for (UniplacesPlacePurpose child : list)
            add(result, child, graphMap);
    }
}
