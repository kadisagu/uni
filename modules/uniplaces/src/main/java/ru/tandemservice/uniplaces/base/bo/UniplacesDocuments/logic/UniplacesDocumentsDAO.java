/* $Id$ */
package ru.tandemservice.uniplaces.base.bo.UniplacesDocuments.logic;

import com.google.common.collect.Maps;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.file.FileNotFoundApplicationException;
import org.tandemframework.shared.commonbase.remoteDocument.dto.RemoteDocumentDTO;
import org.tandemframework.shared.commonbase.remoteDocument.service.ICommonRemoteDocumentService;
import org.tandemframework.shared.commonbase.remoteDocument.service.IRemoteDocumentProvider;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation;
import ru.tandemservice.uniplaces.remoteDocument.ext.RemoteDocument.RemoteDocumentExtManager;
import ru.tandemservice.uniplaces.remoteDocument.ext.RemoteDocument.util.RemoteDocumentFlexAttributes;

import java.io.IOException;
import java.util.Calendar;
import java.util.Map;


/**
 * @author Ekaterina Zvereva
 * @since 12.02.2015
 */
public class UniplacesDocumentsDAO extends UniBaseDao implements IUniplacesDocumentsDAO
{
    public static final String REMOTE_DOCUMENT_MODULE = "uniplacesdocument";

    @Override
    public void saveOrUpdateDocument(UniplacesObjectToDocumentRelation document, IUploadFile file)
    {
        saveOrUpdate(document);
        if(document.getDocument() != null)
        {
            updateRemoteDocument(document, file);
        }
        else if(file != null)
        {
            createRemoteDocument(document, file);
        }
    }

    @Override
    public void printDocumentScanCopy(UniplacesObjectToDocumentRelation document)
    {
        if(document.getDocument() != null)
        {
            try (ICommonRemoteDocumentService service = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.UNIPLACES_DOCS_SERVICE_NAME))
            {

                RemoteDocumentDTO scanCopy = service.get(document.getDocument());
                if (scanCopy != null)
                {
                    BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(scanCopy.getFileType()).fileName(scanCopy.getFileName()).document(scanCopy.getContent()), true);
                }
            }
        }
    }

    @Override
    public void changeActiveProperty(Long documentId)
    {
        UniplacesObjectToDocumentRelation document = get(UniplacesObjectToDocumentRelation.class,documentId);
        document.setActive(!document.isActive());
        update(document);
    }

    private void updateRemoteDocument(UniplacesObjectToDocumentRelation document, IUploadFile scanCopy)
    {
        try (ICommonRemoteDocumentService documentService = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.UNIPLACES_DOCS_SERVICE_NAME))
        {
            RemoteDocumentDTO documentDTO = documentService.get(document.getDocument());

            if(documentDTO == null)
            {
                createRemoteDocument(document, scanCopy);
                return;
            }

            if(scanCopy != null)
                documentDTO.setFileContent(scanCopy.getFileName(), IOUtils.toByteArray(scanCopy.getStream()));

            documentDTO.setFlexMap(fillFlexMap(document));

            documentService.update(documentDTO);
        }
        catch (IOException e)
        {
            throw CoreExceptionUtils.getRuntimeException(e);
        }
    }

    private void createRemoteDocument(UniplacesObjectToDocumentRelation document, IUploadFile scanCopy)
    {
        try (ICommonRemoteDocumentService documentService = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.UNIPLACES_DOCS_SERVICE_NAME))
        {
            RemoteDocumentDTO documentDTO = new RemoteDocumentDTO("Скан-копия документа" + document.getTitle(), REMOTE_DOCUMENT_MODULE, RemoteDocumentExtManager.UNIPLACES_DOCS_SERVICE_NAME);
            documentDTO.setOwnerId(document.getId());

            if(scanCopy != null)
                documentDTO.setFileContent(scanCopy.getFileName(), IOUtils.toByteArray(scanCopy.getStream()));

            documentDTO.setFlexMap(fillFlexMap(document));

            Long attachmentId = documentService.insert(documentDTO);

            document.setDocument(attachmentId);
            update(document);
        }
        catch (IOException e) {
            throw CoreExceptionUtils.getRuntimeException(e);
        }
    }

    private Map<String, Object> fillFlexMap(UniplacesObjectToDocumentRelation document)
    {
        final Map<String, Object> flexMap = Maps.newHashMap();
        flexMap.put(RemoteDocumentFlexAttributes.UNI_PLACES_DOCUMENT_OWNER_OBJECT, document.getObject().getId());
        flexMap.put(RemoteDocumentFlexAttributes.UNI_PLACES_DOCUMENT_TITLE, document.getTitle());
        if (document.getStartDate() != null)
        {
            Calendar startDate = Calendar.getInstance();
            startDate.setTime(document.getStartDate());
            flexMap.put(RemoteDocumentFlexAttributes.UNI_PLACES_DOCUMENT_START_DATE, startDate);
        }

        if (document.getEndDate() != null)
        {
            Calendar endDate = Calendar.getInstance();
            endDate.setTime(document.getStartDate());
            flexMap.put(RemoteDocumentFlexAttributes.UNI_PLACES_DOCUMENT_END_DATE, endDate);
        }
        if (!StringUtils.isEmpty(document.getComment()))
            flexMap.put(RemoteDocumentFlexAttributes.UNI_PLACES_DOCUMENT_COMMENT, document.getComment());
        flexMap.put(RemoteDocumentFlexAttributes.UNI_PLACES_DOCUMENT_ACTIVE, document.isActive());
        return flexMap;
    }


    @Override
    public void deleteDocument(Long documentId)
    {
        UniplacesObjectToDocumentRelation document = getNotNull(documentId);
        if (document.getDocument() != null)
        {
            try (ICommonRemoteDocumentService documentService = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.UNIPLACES_DOCS_SERVICE_NAME))
            {

                RemoteDocumentDTO documentDTO = null;
                try {
                    documentDTO = documentService.get(document.getDocument());
                } catch (FileNotFoundApplicationException ignored) {
                }
                if (documentDTO != null)
                {
                    documentService.delete(documentDTO);
                }
            }
        }
        delete(documentId);
    }

    @Override
    public RemoteDocumentDTO getDocument(Long documentId)
    {
        try (ICommonRemoteDocumentService documentService = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.UNIPLACES_DOCS_SERVICE_NAME))
        {
            return documentService.get(documentId);
        }
    }
}