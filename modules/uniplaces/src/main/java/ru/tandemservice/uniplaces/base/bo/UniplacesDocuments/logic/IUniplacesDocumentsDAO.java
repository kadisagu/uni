/* $Id$ */
package ru.tandemservice.uniplaces.base.bo.UniplacesDocuments.logic;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.shared.commonbase.remoteDocument.dto.RemoteDocumentDTO;
import ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation;

/**
 * @author Ekaterina Zvereva
 * @since 12.02.2015
 */
public interface IUniplacesDocumentsDAO
{
    void saveOrUpdateDocument(UniplacesObjectToDocumentRelation document, IUploadFile file);

    void printDocumentScanCopy(UniplacesObjectToDocumentRelation document);

    void changeActiveProperty(Long documentId);

    void deleteDocument(Long documentId);

    RemoteDocumentDTO getDocument(Long documentId);
}