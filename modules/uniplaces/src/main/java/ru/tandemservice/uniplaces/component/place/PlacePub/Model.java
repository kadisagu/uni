/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.PlacePub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;

import ru.tandemservice.uniplaces.component.place.AbstractPlaceObjectPub.AbstractPlaceObjectPubModel;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;

/**
 * @author agolubenko
 * @since Aug 23, 2010
 */
@State(@Bind(key = "publisherId", binding = "place.id"))
public class Model extends AbstractPlaceObjectPubModel
{
    private UniplacesPlace _place = new UniplacesPlace();
    
    public String getWindowNumber()
    {
        Integer result = getPlace().getWindowNumber();
        return (result != null && result > 0) ? result.toString() : "Без окон";
    }

    public String getCapacity()
    {
        Integer result = _place.getCapacity();
        return (result != null && result > 0) ? result.toString() : "Не указана";
    }

    public UniplacesPlace getPlace()
    {
        return _place;
    }

    public void setPlace(UniplacesPlace place)
    {
        _place = place;
    }

    @Override
    public Long getObjectID()
    {
        return _place.getId();
    }

    @Override
    public String initPostfix()
    {
        return "place";
    }
}
