/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.core.tool.tree.PlaneTree;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniplaces.UniplacesDefines;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;

/**
 * @author agolubenko
 * @since Aug 16, 2010
 */
public class UniplacesDAO extends UniBaseDao implements IUniplacesDAO
{
    @Override
    public ISelectModel getResponsibleOrgUnitModel()
    {
        return new UniQueryFullCheckSelectModel(OrgUnit.fullTitle().s())
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, alias);
                builder.add(MQExpression.eq(alias, OrgUnit.archival(), false));
                if (StringUtils.isNotBlank(filter))
                    builder.add(MQExpression.like(alias, OrgUnit.fullTitle(), CoreStringUtils.escapeLike(filter.toLowerCase(), true), true));
                return builder;
            }
        }.setPageSize(100);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IHierarchyItem> Map<T, List<T>> getTree(Class<T> clazz)
    {
        Map<T, List<T>> result = new HashMap<T, List<T>>();
        SafeMap.Callback<T, List<T>> callback = SafeMap.createCallback(ArrayList.class);

        for (T node : getList(clazz))
        {
            SafeMap.safeGet(result, (T) node.getHierarhyParent(), callback).add(node);
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IHierarchyItem> List<T> getSubTree(T node)
    {
        List<T> result = new ArrayList<T>();
        result.add(node);
        walk(node, result, getTree((Class<T>) EntityRuntime.getMeta((IEntity) node).getEntityClass()));
        return result;
    }

    @Override
    public List<UniplacesPlacePurpose> getHabitablePlacePurposeList()
    {
        return getSubTree(getCatalogItem(UniplacesPlacePurpose.class, UniplacesDefines.CATALOG_PLACE_PURPOSE_HABITABLE));
    }

    @Override
    public List<HSelectOption> getHabitablePlacePurposeOptionList()
    {
        List<UniplacesPlacePurpose> nodes = getHabitablePlacePurposeList();
        PlaneTree flatTree = new PlaneTree(nodes.toArray(new IHierarchyItem[nodes.size()]));

        IHierarchyItem[] hierarchyNodes = flatTree.getFlatTreeObjects();
        int[] levels = flatTree.getLevels();

        List<HSelectOption> result = new ArrayList<HSelectOption>();
        for (int i = 0; i < hierarchyNodes.length; i++)
        {
            result.add(new HSelectOption(hierarchyNodes[i], levels[i], i != 0));
        }
        return result;
    }

    private static <T> void walk(T node, List<T> nodes, Map<T, List<T>> tree)
    {
        List<T> children = tree.get(node);
        if (children == null)
        {
            return;
        }

        nodes.addAll(children);

        for (T child : children)
        {
            walk(child, nodes, tree);
        }
    }
}
