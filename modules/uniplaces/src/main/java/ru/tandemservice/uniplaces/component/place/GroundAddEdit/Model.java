/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniplaces.component.place.GroundAddEdit;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uniplaces.entity.catalog.UniplacesDisposalRight;
import ru.tandemservice.uniplaces.entity.place.UniplacesAssignedDocument;
import ru.tandemservice.uniplaces.entity.place.UniplacesGround;

/**
 * @author agolubenko
 * @since Aug 2, 2010
 */
@Input(@Bind(key = "groundId", binding = "ground.id"))
public class Model
{
    private UniplacesGround _ground = new UniplacesGround();
    private UniplacesAssignedDocument _assignedDocument;

    private ISelectModel _orgUnitList;
    private List<HSelectOption> _documentKindList;
    private List<UniplacesDisposalRight> _disposalRightList;

    public boolean isEditForm()
    {
        return (getGround().getId() != null);
    }

    public UniplacesGround getGround()
    {
        return _ground;
    }

    public void setGround(UniplacesGround ground)
    {
        _ground = ground;
    }

    public ISelectModel getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(ISelectModel orgUnitList)
    {
        _orgUnitList = orgUnitList;
    }

    public UniplacesAssignedDocument getAssignedDocument()
    {
        return _assignedDocument;
    }

    public void setAssignedDocument(UniplacesAssignedDocument assignedDocument)
    {
        _assignedDocument = assignedDocument;
    }

    public List<HSelectOption> getDocumentKindList()
    {
        return _documentKindList;
    }

    public void setDocumentKindList(List<HSelectOption> documentKindList)
    {
        _documentKindList = documentKindList;
    }

    public List<UniplacesDisposalRight> getDisposalRightList()
    {
        return _disposalRightList;
    }

    public void setDisposalRightList(List<UniplacesDisposalRight> disposalRightList)
    {
        _disposalRightList = disposalRightList;
    }
}
