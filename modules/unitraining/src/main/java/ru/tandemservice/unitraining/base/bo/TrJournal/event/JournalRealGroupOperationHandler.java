package ru.tandemservice.unitraining.base.bo.TrJournal.event;

import com.google.common.collect.Iterables;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.dao.group.IEppRealGroupOperationHandler;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow.IEppRealEduGroupRowDescription;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class JournalRealGroupOperationHandler extends UniBaseDao implements IEppRealGroupOperationHandler<EppRealEduGroup> {

    @Override public boolean canHandle(final Class<EppRealEduGroup> groupClass) {
        return true;
    }

    /** { journal.id -> { group.id }} */
    protected Map<Long, Set<Long>> getCoveredGroupIds(final Collection<EppRealEduGroup> groups) {
        final Map<Long, Set<Long>> result = new HashMap<>();
        for (List<EppRealEduGroup> groupsPart : Iterables.partition(groups, DQL.MAX_VALUES_ROW_NUMBER)) {
            {
                // по связям с УГС
                final List<Object[]> rows = new DQLSelectBuilder()
                        .fromEntity(TrJournalGroup.class, "jg")
                        .where(in(property(TrJournalGroup.group().fromAlias("jg")), groupsPart))
                        .column(property(TrJournalGroup.journal().id().fromAlias("jg")))
                        .column(property(TrJournalGroup.group().id().fromAlias("jg")))
                        .predicate(DQLPredicateType.distinct)
                        .createStatement(JournalRealGroupOperationHandler.this.getSession()).<Object[]>list();

                for (final Object[] row: rows) {
                    final Long journalId = (Long)row[0];
                    final Long groupId = (Long)row[1];
                    SafeMap.safeGet(result, journalId, HashSet.class).add(groupId);
                }
            }

                // не надо считать группы привязанными, если есть только события - это могут быть неактуальные оценки,
                // т.к. они остаются, когда группы уже отцеплены
//                {
//                    // по событиям
//                    final List<Object[]> rows = new DQLSelectBuilder()
//                    .fromEntity(TrEduGroupEvent.class, "ge")
//                    .where(in(property(TrEduGroupEvent.group().fromAlias("ge")), groups))
//                    .column(property(TrEduGroupEvent.journalEvent().journalModule().journal().id().fromAlias("ge")))
//                    .column(property(TrEduGroupEvent.group().id().fromAlias("ge")))
//                    .predicate(DQLPredicateType.distinct)
//                    .createStatement(JournalRealGroupOperationHandler.this.getSession()).<Object[]>list();
//
//                    for (final Object[] row: rows) {
//                        final Long journalId = (Long)row[0];
//                        final Long groupId = (Long)row[1];
//                        SafeMap.safeGet(result, journalId, HashSet.class).add(groupId);
//                    }
//                }

        }
        return result;
    }

    protected void error(final Long id) {
        // final EppRealEduGroup group = this.get(EppRealEduGroup.class, id);
        // throw new ApplicationException("Учебная группа «"+group.getTitle()+" "+group.getActivityPart().getTitleWithNumber()+", "+group.getType().getTitle()+"» заблокирована (существует журнал).");
    }

    @Override public void checkLocked(final Collection<EppRealEduGroup> groups) throws ApplicationException {
//        final Map<Long, Set<Long>> coveredMap = this.getCoveredGroupIds(groups);
//        if (coveredMap.isEmpty()) { return; }
//
//        // если что-то есть, то ошибка
//        final Set<Long> firstJournal = coveredMap.values().iterator().next();
//        this.error(firstJournal.iterator().next());
    }

    @Override public Collection<Long> getJoinCandidate(final Collection<EppRealEduGroupRow> rows) throws ApplicationException {
        final Set<EppRealEduGroup> groups = new HashSet<>();
        for (final EppRealEduGroupRow row: rows) { groups.add(row.getGroup()); }

        final Map<Long, Set<Long>> coveredMap = this.getCoveredGroupIds(groups);

        if (coveredMap.isEmpty()) { return null; /* мне все равно */ }
        if (coveredMap.size() > 1) {
            // более одной УГС с журналом - ругаемся матом
            throw new ApplicationException("Нельзя объединить учебные группы, привязаные к разным журналам.");
        }

        // если журнал один - берем любую группу из него
        return coveredMap.values().iterator().next();
    }

    @Override public void move(final EppRealEduGroup group, final Map<EppRealEduGroupRow, EppRealEduGroupRow> rowMap) throws ApplicationException {
        // ничего не делаем
    }

    @Override public void checkAutoDelete(final IEppRealEduGroupRowDescription relation, final DQLSelectBuilder dql, final String alias) {
        // удалить УГС можно, только если на неё нет журналов
        dql.where(notExists(TrJournalGroup.class, TrJournalGroup.L_GROUP, property(alias)));
    }
}
