package ru.tandemservice.unitraining.base.bo.TrOrgUnit.logic;

import org.hibernate.Session;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeALT;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeALTCodes;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalDao;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.GroupsJournalGen.GroupsJournalGenOption;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class TrOrgUnitDao extends UniBaseDao implements ITrOrgUnitDao {

    @Override
    public boolean isTrOrgUnitTabVisible(final Long orgUnitId) {
        return IEppSettingsDAO.instance.get().isTutorOrgUnit(orgUnitId);
    }

    @Override
    public int doGenerateJournals(OrgUnit orgUnit, EppYearPart yearPart, Boolean controlOnlyMode)
    {
        Session session = lock("tj."+orgUnit.getId()+"."+yearPart.getId());

        // берем все строки РУП (и их версий) из указанной годо-части,
        // для которых текущее подразделение является читающим

        final boolean checkState = IEppSettingsDAO.instance.get().getGlobalSettings().isCheckElementState();
        List<EppRegistryElementPart> regElParts = getList(
            EppRegistryElementPart.class,
            new DQLSelectBuilder()
            .fromEntity(EppWorkPlanRegistryElementRow.class, "row")
            .joinPath(DQLJoinType.inner, EppWorkPlanRegistryElementRow.registryElementPart().fromAlias("row"), "rep")
            .where(eq(property(EppRegistryElementPart.registryElement().owner().fromAlias("rep")), value(orgUnit)))

            // не должно быть журналов
            .joinEntity("rep", DQLJoinType.left, TrJournal.class, "j", and(
                eq(property(TrJournal.registryElementPart().fromAlias("j")), property("rep")),
                eq(property(TrJournal.yearPart().fromAlias("j")), value(yearPart))
            ))
            .where(isNull(property("j.id")))

            // должны быть либо РУП, либо версия РУП
            .where(or(
                exists(
                    new DQLSelectBuilder()
                    .fromEntity(EppWorkPlan.class, "wp").column(property("wp.id"))
                    .where(eq(property("wp.id"), property(EppWorkPlanRegistryElementRow.workPlan().fromAlias("row"))))
                    .where(or(
                            eq(value(checkState), value(Boolean.FALSE)),
                            eq(property(EppWorkPlan.state().code().fromAlias("wp")), value(EppState.STATE_ACCEPTED))
                    ))
                    .where(eq(property(EppWorkPlan.year().fromAlias("wp")), value(yearPart.getYear())))
                    .where(eq(property(EppWorkPlan.cachedGridTerm().part().fromAlias("wp")), value(yearPart.getPart())))

                    .buildQuery()
                ),
                exists(
                    new DQLSelectBuilder()
                    .fromEntity(EppWorkPlanVersion.class, "wpv").column(property("wpv.id"))
                    .where(eq(property("wpv.id"), property(EppWorkPlanRegistryElementRow.workPlan().fromAlias("row"))))
                    .joinPath(DQLJoinType.inner, EppWorkPlanVersion.parent().fromAlias("wpv"), "wp")
                    .where(or(
                            eq(value(checkState), value(Boolean.FALSE)),
                            eq(property(EppWorkPlanVersion.state().code().fromAlias("wpv")), value(EppState.STATE_ACCEPTED))
                    ))
                    .where(eq(property(EppWorkPlan.year().fromAlias("wp")), value(yearPart.getYear())))
                    .where(eq(property(EppWorkPlan.cachedGridTerm().part().fromAlias("wp")), value(yearPart.getPart())))
                    .buildQuery()
                )
            ))
            .predicate(DQLPredicateType.distinct)
            .column(property("rep.id"))
            .createStatement(session).<Long>list()
        );

        ITrJournalDao dao = TrJournalManager.instance().dao();
        for (EppRegistryElementPart regElPart: regElParts) {
            dao.doCreateTrJournal(regElPart, yearPart, controlOnlyMode);
        }

        return regElParts.size();
    }

    @Override
    public void doAutoLinkEduGroups(OrgUnit orgUnit, EppYearPart yearPart)
    {
        if (null == orgUnit)
            throw new ApplicationException("Для автоматического прикрепления групп к журналам выберите подразделение.");

        if (null == yearPart)
            throw new ApplicationException("Для автоматического прикрепления групп к журналам выберите часть учебного года.");

        Session session = lock("tj."+orgUnit.getId()+"."+yearPart.getId());

        DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EppRealEduGroup4LoadType.class, "e")
        .where(eq(property(EppRealEduGroup4LoadType.activityPart().registryElement().owner().fromAlias("e")), value(orgUnit)))
        .where(eq(property(EppRealEduGroup4LoadType.summary().yearPart().fromAlias("e")), value(yearPart)))
        .joinEntity("e", DQLJoinType.left, TrJournalGroup.class, "rel", eq(property(TrJournalGroup.group().fromAlias("rel")), property("e")))
        .where(isNull(property("rel.id")))
        .joinEntity("e", DQLJoinType.inner, TrJournal.class, "j", eq(property(EppRealEduGroup4LoadType.activityPart().fromAlias("e")), property(TrJournal.registryElementPart().fromAlias("j"))))
        .where(eq(property(TrJournal.yearPart().fromAlias("j")), value(yearPart)))
        .where(eq(property(TrJournal.state().code().fromAlias("j")), value(EppState.STATE_ACCEPTED)))
        .column("e")
        .column("j")
        ;

        Map<EppRealEduGroup4LoadType, Set<TrJournal>> map = SafeMap.get(HashSet.class);

        for (Object[] row : dql.createStatement(session).<Object[]>list())
            map.get((EppRealEduGroup4LoadType) row[0]).add((TrJournal) row[1]);

        int created = 0; int skipped = 0;
        for (Map.Entry<EppRealEduGroup4LoadType, Set<TrJournal>> entry : map.entrySet())
        {
            if (entry.getValue().size() > 1)
            {
                skipped++;
                continue;
            }
            TrJournalGroup rel = new TrJournalGroup();
            rel.setGroup(entry.getKey());
            rel.setJournal(entry.getValue().iterator().next());
            session.save(rel);
            created++;
        }

        ContextLocal.getInfoCollector().add("Групп прикреплено: " + created);
        ContextLocal.getInfoCollector().add("Групп пропущено (найдено более одной реализации дисциплины): " + skipped);
    }

    @Override
    public void doGroupsJournalGen(GroupsJournalGenOption option, Collection<Long> groupIdList, Boolean controlOnlyMode)
    {
        if (GroupsJournalGenOption.journalOnDisc.equals(option))
            doJournalOnDiscGen(groupIdList, controlOnlyMode);
        else if (GroupsJournalGenOption.journalOnLead.equals(option))
            doJournalOnLeadGen(groupIdList, controlOnlyMode);
        else if (GroupsJournalGenOption.journalOnGroup.equals(option))
            doJournalOnGroupGen(groupIdList, controlOnlyMode);
        else
            throw new NullPointerException("'option' is null.");
    }

    /**
     * На все уникальные дисциплины УГС будет сформирована новая реализация, после чего УГС по данной дисциплине будут прикреплены к этой реализации.
     */
    protected void doJournalOnDiscGen(Collection<Long> groupIdList, Boolean controlOnlyMode)
    {
        final List<EppRealEduGroup> groupList = new DQLSelectBuilder().fromEntity(EppRealEduGroup.class, "grp").column(property("grp"))
        .where(in(property(EppRealEduGroup.id().fromAlias("grp")), groupIdList))
        .order(property(EppRealEduGroup.type().priority().fromAlias("grp")))
        .order(property(EppRealEduGroup.title().fromAlias("grp")))
        .createStatement(getSession()).list();

        final Map<EppRegistryElementPart, List<EppRealEduGroup>> elementPartGroupsMap = SafeMap.get(ArrayList.class);
        for (EppRealEduGroup group : groupList)
            elementPartGroupsMap.get(group.getActivityPart())
            .add(group);

        for (Map.Entry<EppRegistryElementPart, List<EppRealEduGroup>> entry : elementPartGroupsMap.entrySet())
        {
            final EppRegistryElementPart registryElementPart = entry.getKey();
            final List<EppRealEduGroup> groupLoadList = entry.getValue();

            final EppYearPart yearPart = groupLoadList.get(0).getSummary().getYearPart();

            final TrJournal journal = new TrJournal(registryElementPart, yearPart);
            TrJournalManager.instance().dao().doCreateTrJournal(journal, controlOnlyMode);
            TrJournalManager.instance().dao().doLinkEduGroups(journal, groupLoadList);
        }
    }

    /**
     * По каждой дисциплине на ведущие УГС (по наиболее приоритетному виду нагрузки в порядке: лекции-практики-лабораторные) будут сформированы реализации, по одной на каждую ведущую УГС.
     * К сформированным реализациям будет прикреплена ведущая УГС, а затем все входящие УГС (т.е. те, которые по составу студентов полностью входят в ведущую).
     */
    protected void doJournalOnLeadGen(Collection<Long> groupIdList, Boolean controlOnlyMode)
    {
        final List<EppRealEduGroup> groupList = new DQLSelectBuilder().fromEntity(EppRealEduGroup.class, "grp").column(property("grp"))
            .where(in(property(EppRealEduGroup.id().fromAlias("grp")), groupIdList))
            .order(property(EppRealEduGroup.type().priority().fromAlias("grp")))
            .order(property(EppRealEduGroup.title().fromAlias("grp")))
            .createStatement(getSession()).list();

        final Map<EppRegistryElementPart, List<EppRealEduGroup>> elementPartGroupsMap = SafeMap.get(ArrayList.class);
        for (EppRealEduGroup group : groupList)
            elementPartGroupsMap.get(group.getActivityPart())
            .add(group);

        for (Map.Entry<EppRegistryElementPart, List<EppRealEduGroup>> entry : elementPartGroupsMap.entrySet())
        {
            final EppGroupTypeALT leadLoadType = getLeadLoadType(CommonBaseEntityUtil.<EppGroupTypeALT>getPropertiesSet(entry.getValue(), EppRealEduGroup.type()));
            final List<EppRealEduGroup> leadGroupList = getLeadGroupList(entry.getValue(), leadLoadType);

            final EppYearPart yearPart = entry.getValue().get(0).getSummary().getYearPart();

            for (EppRealEduGroup leadGroup : leadGroupList)
            {
                final TrJournal journal = new TrJournal(leadGroup.getActivityPart(), yearPart);
                TrJournalManager.instance().dao().doCreateTrJournal(journal, controlOnlyMode);
                // к сформированным реализациям будет прикреплена ведущая УГС
                TrJournalManager.instance().dao().doLinkEduGroups(journal, Collections.singletonList(leadGroup));
                // а затем все входящие УГС (т.е. те, которые по составу студентов полностью входят в ведущую)
                TrJournalManager.instance().dao().doLinkEduGroups(journal, groupsAsStudentSubset(leadGroup, entry.getValue()));
            }
        }
    }

    /**
     * На каждую УГС будет сформирована новая реализация, затем УГС будет к ней прикреплена.
     */
    protected void doJournalOnGroupGen(Collection<Long> groupIdList, Boolean controlOnlyMode)
    {
        final List<EppRealEduGroup> groupList = new DQLSelectBuilder().fromEntity(EppRealEduGroup.class, "grp").column(property("grp"))
        .where(in(property(EppRealEduGroup.id().fromAlias("grp")), groupIdList))
        .order(property(EppRealEduGroup.type().priority().fromAlias("grp")))
        .order(property(EppRealEduGroup.title().fromAlias("grp")))
        .createStatement(getSession()).list();

        for (EppRealEduGroup group : groupList)
        {
            final TrJournal journal = new TrJournal(group.getActivityPart(), group.getSummary().getYearPart());
            TrJournalManager.instance().dao().doCreateTrJournal(journal, controlOnlyMode);
            TrJournalManager.instance().dao().doLinkEduGroups(journal, Collections.singletonList(group));
        }
    }

    /**
     * Вычисляет список УГС входящие в ведущюю УГС (т.е. те, которые по составу студентов полностью входят в ведущую).
     * @param leadGroup ведущая УГС
     * @param groupList список УГС
     * @return входящие УГС
     */
    protected List<EppRealEduGroup> groupsAsStudentSubset(EppRealEduGroup leadGroup, List<EppRealEduGroup> groupList)
    {
        final List<EppRealEduGroup> list = new ArrayList<>(groupList);
        list.remove(leadGroup);

        final List<Object[]> studentList = new DQLSelectBuilder().fromEntity(EppRealEduGroupRow.class, "rel")
        .column(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().id().fromAlias("rel")))
        .column(property(EppRealEduGroupRow.group().id().fromAlias("rel")))
        .where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel"))))
        .where(or(
            eq(property(EppRealEduGroupRow.group().fromAlias("rel")), value(leadGroup)),
            in(property(EppRealEduGroupRow.group().fromAlias("rel")), list)))
            .createStatement(getSession()).list();

        final Map<Long, List<Long>> gropStudentsMap = SafeMap.get(ArrayList.class);
        for (Object[] row : studentList)
        {
            final Long studentId = (Long) row[0];
            final Long groupId = (Long) row[1];

            gropStudentsMap.get(groupId)
            .add(studentId);
        }

        final List<EppRealEduGroup> resultGroupList = new ArrayList<>();
        final List<Long> leadStudentIdList = gropStudentsMap.get(leadGroup.getId());
        for (EppRealEduGroup group : list)
        {
            final List<Long> studentIdList = gropStudentsMap.get(group.getId());

            if (leadStudentIdList.containsAll(studentIdList))
                resultGroupList.add(group);
        }

        return resultGroupList;
    }

    /**
     * Определяет ведущий вид УГС ВАН по наиболее приоритетному виду нагрузки в порядке: лекции-практики-лабораторные.
     * @param groupTypeList список ВАН
     * @return ведущий ВАН
     */
    protected EppGroupTypeALT getLeadLoadType(Collection<EppGroupTypeALT> groupTypeList)
    {
        final Set<String> codeList = CommonBaseEntityUtil.getPropertiesSet(groupTypeList, EppGroupType.code().s());

        if (codeList.contains(EppGroupTypeALTCodes.TYPE_LECTURES))
            return getCatalogItem(EppGroupTypeALT.class, EppGroupTypeALTCodes.TYPE_LECTURES);

        if (codeList.contains(EppGroupTypeALTCodes.TYPE_PRACTICE))
            return getCatalogItem(EppGroupTypeALT.class, EppGroupTypeALTCodes.TYPE_PRACTICE);

        if (codeList.contains(EppGroupTypeALTCodes.TYPE_LABS))
            return getCatalogItem(EppGroupTypeALT.class, EppGroupTypeALTCodes.TYPE_LABS);

        return null;
    }

    /**
     * Определяет ведущую УГС с таким ВАН.
     * @param groupList список из которых надо выбрать
     * @param leadGroupType ведущий вид УГС ВАН
     * @return ведущаие УГС
     */
    protected List<EppRealEduGroup> getLeadGroupList(List<EppRealEduGroup> groupList, EppGroupTypeALT leadGroupType)
    {
        final List<EppRealEduGroup> resultList = new ArrayList<>();
        for (EppRealEduGroup group : groupList)
        {
            if (group.getType().equals(leadGroupType))
                resultList.add(group);
        }

        return resultList;
    }
}
