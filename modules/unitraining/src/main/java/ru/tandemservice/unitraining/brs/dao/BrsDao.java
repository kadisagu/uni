/* $Id:$ */
package ru.tandemservice.unitraining.brs.dao;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniScriptDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGradeScaleCodes;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlotAdditionalData;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionCurrentMarkScale;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.codes.SessionCurrentMarkScaleCodes;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSlotRatingData;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalEventDao;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.*;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils;
import ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;
import ru.tandemservice.unitraining.brs.entity.catalogs.IBrsCoefficientDef;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unisession.entity.catalog.codes.SessionAttestationMarkCatalogItemCodes.MARK_NO_PASSED;
import static ru.tandemservice.unisession.entity.catalog.codes.SessionAttestationMarkCatalogItemCodes.MARK_PASSED;

/**
 * @author oleyba
 * @since 8/24/12
 */
public class BrsDao extends UniBaseDao implements IBrsDao
{
    public static String getDefaultScriptText() {
        return BrsScriptDao.getScriptText(UniScriptDao.findScriptPath(BrsScriptDao.DEFAULT_GLOBAL_RATING_SCRIPT));
    }

    @Override
    public EppYearPart getEppYearPart(EducationYear educationYear, YearDistributionPart yearDistributionPart)
    {
        if (educationYear == null || yearDistributionPart == null) return null;
        EppYearEducationProcess eduYear = getByNaturalId(new EppYearEducationProcess.NaturalId(educationYear));
        return eduYear != null ? this.<EppYearPart>getByNaturalId(new EppYearPart.NaturalId(eduYear, yearDistributionPart)) : null;
    }

    @Override
    public EppYearPart getFirstEppYearPartForCurrentEduYear()
    {
        return new DQLSelectBuilder().fromEntity(EppYearPart.class, "e")
                .top(1)
                .column(property("e"))
                .where(eq(property("e", EppYearPart.year().educationYear().current()), value(true)))
                .order(property("e", EppYearPart.part().number()))
                .createStatement(getSession()).uniqueResult();
    }

    @Override
    public void doSaveUserScript(TrOrgUnitSettings settings)
    {
        IBrsScriptDao.instance.get().doSaveUserScript(settings);
    }

    @Override
    public TrOrgUnitSettings getOuSettingsForLastYearPart(Long orgUnitId)
    {
        return new DQLSelectBuilder().fromEntity(TrOrgUnitSettings.class, "s")
                .top(1)
                .column(property("s"))
                .where(eq(property("s", TrOrgUnitSettings.orgUnit()), value(orgUnitId)))
                .order(property("s", TrOrgUnitSettings.eduYearPart().year().educationYear().intValue()), OrderDirection.desc)
                .order(property("s", TrOrgUnitSettings.eduYearPart().part().number()), OrderDirection.desc)
                .createStatement(getSession()).uniqueResult();
    }

    @Override
    public void validateOnAcceptance(TrJournal journal)
    {
        IBrsJournalScriptFunctions script = IBrsScriptDao.instance.get().getJournalScript(journal).getScript();
        script.validateOnJournalAcceptance(
            ContextLocal.getErrorCollector(),
            getPreparedJournalContent(journal),
                getPreparedBrsSettings(journal));
    }

    @Override
    public void validateJournalMark(EppStudentWorkPlanElement slot, TrEduGroupEvent event, ITrJournalEventDao.IJournalMarkData mark)
    {
        TrJournal journal = event.getJournalEvent().getJournalModule().getJournal();
        IBrsJournalGroupScriptFunctions script = IBrsScriptDao.instance.get().getJournalGroupScript(journal, slot).getScript();

        script.validateMark(
            ContextLocal.getErrorCollector(),
            slot.getStudent(),
            event,
            mark,
            getPreparedBrsSettings(journal, slot.getStudent()));
    }

    @Override
    public List<TrBrsCoefficientDef> getFilteredCoefficientDefList(IBrsSettingsOwner owner, TrBrsCoefficientOwnerType ownerType)
    {
        List<TrBrsCoefficientDef> defList = getList(TrBrsCoefficientDef.class, TrBrsCoefficientDef.ownerType(), ownerType, TrBrsCoefficientDef.title().s());
        if (owner instanceof TrJournalEvent) {
            // для событий журнала будем фильтровать коэффициенты в скрипте,
            // чтобы можно было иметь разные наборы в зависимости от типа и признаков события
            TrJournalEvent event = (TrJournalEvent) owner;
            TrJournal journal = event.getJournalModule().getJournal();
            IBrsJournalScriptFunctions script = IBrsScriptDao.instance.get().getJournalScript(journal).getScript();
            return script.filterCoefficientDefListForEvent(event, defList);
        }
        else if (owner instanceof TrJournal || owner instanceof TrJournalModule) {
            // для журнала и модуля фильтруем по определенным в скрипте коэффициентам
            // чтобы можно было иметь разные наборы на разных кафедрах
            TrJournal journal = owner instanceof TrJournal ? (TrJournal) owner : ((TrJournalModule) owner).getJournal();
            IBrsJournalScriptFunctions script = IBrsScriptDao.instance.get().getJournalScript(journal).getScript();
            List<IBrsCoefficientDef> coefFromScript = script.getCoefficientDefList();
            List<TrBrsCoefficientDef> result = new ArrayList<>();
            for (TrBrsCoefficientDef def : defList) {
                for (IBrsCoefficientDef defFromScript : coefFromScript)
                    if (defFromScript.getUserCode().equals(def.getUserCode()))
                        result.add(def);
            }
            return result;
        }
        return defList;
    }

    @Override
    public ICurrentRatingCalc getCalculatedRating(TrJournal journal)
    {
        return getCalculatedRating(journal, getPreparedBrsSettings(journal));
    }

    @Override
    public IJournalEventStatCalc getCalculatedStat(TrJournal journal)
    {
        return getStatCalc(journal, null, getPreparedBrsSettings(journal));
    }

    @Override
    public IJournalEventStatCalc getCalculatedStat(TrJournalGroup journalGroup)
    {
        TrJournal journal = journalGroup.getJournal();
        return getStatCalc(journal, journalGroup, getPreparedBrsSettings(journal));
    }

    @Override
    public TrOrgUnitSettings getOrgUnitBrsSettings(EppYearPart eppYearPart, OrgUnit tutorOu, OrgUnit groupOu)
    {
        return IBrsScriptDao.instance.get().getOrgUnitBrsSettings(eppYearPart, tutorOu, groupOu);
    }
    
    @Override
    public void doFillAttestationMarkBasedOnCurrentRating(SessionAttestationBulletin bulletin)
    {
        List<SessionAttestationSlot> attSlots = getList(SessionAttestationSlot.class, SessionAttestationSlot.bulletin(), bulletin);
        List<EppStudentWorkPlanElement> studentSlots = new ArrayList<>();
        for (SessionAttestationSlot attSlot : attSlots)
            studentSlots.add(attSlot.getStudentWpe());

        Map<EppStudentWorkPlanElement, Map<EppFControlActionGroup, TrJournal>> journalMap;
        try {
            journalMap = TrJournalManager.instance().dao().fillJournalMap(studentSlots, bulletin.getRegistryElementPart());
        }
        catch (IllegalStateException e) {
            throw new ApplicationException(e.getMessage());
        }
        
        Set<TrJournal> journals = new HashSet<>();
        for (Map.Entry<EppStudentWorkPlanElement, Map<EppFControlActionGroup, TrJournal>> entry : journalMap.entrySet()) {
            journals.addAll(entry.getValue().values());
        }
        
        Map<TrJournal, IBrsPreparedSettings> brsSettingsMap = new HashMap<>();
        for (TrJournal journal : journals)
            brsSettingsMap.put(journal, getPreparedBrsSettings(journal));

        Map<TrJournal, IBrsDao.ICurrentRatingCalc> ratingCalcMap = new HashMap<>();
        for (TrJournal journal: journals) {
            ratingCalcMap.put(journal, getCalculatedRating(journal, brsSettingsMap.get(journal)));
        }

        EppFControlActionGroup exam = getCatalogItem(EppFControlActionGroup.class, EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM);

        Map<SessionAttestationSlot, SessionAttestationSlotAdditionalData> additDataMap = new HashMap<>();
        for (SessionAttestationSlotAdditionalData data : getList(SessionAttestationSlotAdditionalData.class, SessionAttestationSlotAdditionalData.slot().bulletin(), bulletin))
            additDataMap.put(data.getSlot(), data);

        for (SessionAttestationSlot slot: attSlots) {

            SessionAttestationSlotAdditionalData prevData = additDataMap.get(slot);

            // берем единственный журнал, или журнал по экзамену, если их несколько, и вычисленный по этому журналу тек. рейтинг
            Map<EppFControlActionGroup, TrJournal> studentJournals = journalMap.get(slot.getStudentWpe());
            TrJournal journal = studentJournals == null ? null : (studentJournals.values().size() == 1 ? studentJournals.values().iterator().next() : studentJournals.get(exam));
            IBrsDao.ICurrentRatingCalc ratingCalc = ratingCalcMap.get(journal);

            // не нашли никакого журнала/вычисления рейтинга - удаляем старые данные
            if (null == ratingCalc || null == journal) {
                slot.setFixedRating(null);
                slot.setMark(null);
                saveOrUpdate(slot);
                if (null != prevData)
                    delete(prevData);
                continue;
            }

            IStudentCurrentRatingData studentCurrentRatingData = ratingCalc.getCurrentRating(slot.getStudentWpe());

            IBrsAttestationScriptFunctions script = IBrsScriptDao.instance.get().getAttestationScript(slot.getStudentWpe()).getScript();

            IStudentAttestationData studentAttestationData = script.calculateAttestation(slot, studentCurrentRatingData, brsSettingsMap.get(journal));

            if (studentAttestationData instanceof IStudentAttestationMarkData) {
                slot.setMark(((IStudentAttestationMarkData) studentAttestationData).mark());
            } else {
                SessionCurrentMarkScale scale = getCatalogItem(SessionCurrentMarkScale.class, SessionCurrentMarkScaleCodes.MARK_STATE);
                if (!scale.isActive()) {
                    throw new ApplicationException(
                            "Активная шкала оценок текущего контроля отлична от \"Атт./неатт.\", " +
                                    "поэтому метод calculateAttestation() скрипта БРС должен возвращать результат типа IBrsDao.IStudentAttestationMarkData.\n" +
                                    "см. http://www.tandemservice.ru/javadoc/university/ru/tandemservice/unitraining/brs/bo/TrBrsCoefficient/logic/scriptByScope/IBrsAttestationScriptFunctions.html");
                }

                Boolean passed = studentAttestationData.passed();
                if (passed == null)
                    slot.setMark(null);
                else
                    slot.setMark(getCatalogItem(SessionAttestationMarkCatalogItem.class, passed ? MARK_PASSED : MARK_NO_PASSED));
            }

            Double ratingValue = studentCurrentRatingData.getRatingValue().getValue();
            slot.setFixedRating(ratingValue == null ? 0 : ratingValue);

            save(slot);

            if (prevData == null && studentAttestationData.getAdditionalData() != null) {
                save(studentAttestationData.getAdditionalData());
            }
            if (prevData != null && studentAttestationData.getAdditionalData() != null) {
                prevData.update(studentAttestationData.getAdditionalData(), false);
                save(prevData);
            }
            if (prevData != null && studentAttestationData.getAdditionalData() == null) {
                delete(prevData);
            }
        }
    }

    @Override
    public Collection<ISessionMarkCalc> getCalculatedSessionMark(TrJournalGroup group)
    {
        TrJournal journal = group.getJournal();
        final EppYearPart yearPart = journal.getYearPart();
        
        final EppRegistryElementPart discipline = journal.getRegistryElementPart();
        final OrgUnit tutorOu = discipline.getTutorOu();

        final EppFControlActionGroup fcaGroup = TrBrsCoefficientManager.instance().coefDao().getFcaGroup(journal);
        final EppGradeScale scale;
        if (journal.getGradeScale() != null)
            scale = journal.getGradeScale();
        else if (EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM.equals(fcaGroup.getCode()))
            scale = getByCode(EppGradeScale.class, EppGradeScaleCodes.SCALE_5);
        else
            scale = getByCode(EppGradeScale.class, EppGradeScaleCodes.SCALE_2);

        Map<OrgUnit, IBrsScriptDao.IBrsScriptWrapper<IBrsSessionScriptFunctions>> sessionScriptByGroupOu = SafeMap.get(key -> IBrsScriptDao.instance.get().getSessionScript(yearPart, key, tutorOu));
        Map<IBrsScriptDao.IBrsScriptWrapper<IBrsSessionScriptFunctions>,  Map<EppStudentWorkPlanElement, Double>> studentByScript = new HashMap<>();
        Map<OrgUnit, ISessionBrsDao.ISessionRatingSettings> settingsByGroupOuMap = new HashMap<>(); 
        Map<ISessionBrsDao.ISessionRatingSettings, List<EppStudentWorkPlanElement>> settingsMap = new HashMap<>();
        
        for (EppStudentWorkPlanElement student: TrJournalManager.instance().dao().getStudentList(group)) {
            OrgUnit groupOrgUnit = student.getStudentCurrentGroupOu();
            if (groupOrgUnit == null) {
                throw new ApplicationException("Расчет рейтинга невозможен: не указан деканат для направления подготовки (специальности) «" + student.getStudent().getEducationOrgUnit().getTitleWithFormAndCondition() + "» студента " + student.getStudent().getFio() + ".");
            }
            IBrsScriptDao.IBrsScriptWrapper<IBrsSessionScriptFunctions> script = sessionScriptByGroupOu.get(groupOrgUnit);
            SafeMap.safeGet(studentByScript, script, HashMap.class).put(student, .0);
            if (!settingsByGroupOuMap.containsKey(groupOrgUnit)) {
                ISessionBrsDao.ISessionRatingSettingsKey key = ISessionBrsDao.instance.get().key(scale, fcaGroup, discipline, journal.getYearPart(), groupOrgUnit);
                ISessionBrsDao.ISessionRatingSettings sessionRatingSettings = getRatingSettings(script.getScript(), key);
                settingsByGroupOuMap.put(groupOrgUnit, sessionRatingSettings);
            }
            ISessionBrsDao.ISessionRatingSettings settings = settingsByGroupOuMap.get(groupOrgUnit);
            SafeMap.safeGet(settingsMap, settings, ArrayList.class).add(student);
        }

        // todo DEV-1166
        /*
        На самом деле, в общем случае нифига не будет работать с деканатом = null
        Ключ обязательно надо считать с учетом деканата
        Но тогда хз, как мерджить вычисление в части строки ртф,
        тем более, что оно мерджится в вызываемом методе
        Вложенный метод хотя бы знает про наличие разных скриптов, а этот - нет
        Скорее всего, надо пересмотреть концепцию деления на методы
         */

        return studentByScript.entrySet().stream().map(entry -> getCalculatedSessionMark(entry.getKey(), settingsMap, entry.getValue(), null)).collect(Collectors.toList());
    }

    @Override
    public ISessionBrsDao.ISessionRatingSettings getRatingSettings(ISessionBrsDao.ISessionRatingSettingsKey key)
    {
        IBrsSessionScriptFunctions script = IBrsScriptDao.instance.get().getSessionScript(key.getYearPart(), key.getGroupOu(), key.getDiscipline().getTutorOu()).getScript();
        return getRatingSettings(script, key);
    }

    public ISessionBrsDao.ISessionRatingSettings getRatingSettings(IBrsSessionScriptFunctions script, ISessionBrsDao.ISessionRatingSettingsKey key)
    {
        OrgUnit groupOu = key.getGroupOu();
        OrgUnit tutorOu = key.getDiscipline().getTutorOu();

        EppYearPart yearPart = key.getYearPart();

        TrOrgUnitSettings orgUnitSettings = getOrgUnitBrsSettings(yearPart, tutorOu, groupOu);
        boolean useRating = orgUnitSettings != null && orgUnitSettings.isUseCurrentRatingInSession();

        return script.getRatingSettings(key, getPreparedBrsSettings(yearPart, groupOu, tutorOu), useRating);
    }

    @Override
    public Map<String, ISessionBrsDao.IRatingAdditParamDef> getRatingAdditionalParamDefinitions(ISessionBrsDao.ISessionRatingSettingsKey key) {
        OrgUnit groupOu = key.getGroupOu();
        OrgUnit tutorOu = key.getDiscipline().getTutorOu();

        IBrsSessionScriptFunctions script = IBrsScriptDao.instance.get().getSessionScript(key.getYearPart(), groupOu, tutorOu).getScript();
        return script.getRatingAdditionalParamDefinitions();
    }

    @Override
    public Map<String, ISessionBrsDao.IRatingAdditParamDef> getRatingAdditionalParamDefinitions(TrJournal journal) {
        IBrsJournalScriptFunctions script = IBrsScriptDao.instance.get().getJournalScript(journal).getScript();
        return script.getRatingAdditionalParamDefinitions();
    }

    @Override
    public List<String> getDisplayableAdditParams(TrJournal journal) {
        IBrsJournalScriptFunctions script = IBrsScriptDao.instance.get().getJournalScript(journal).getScript();
        return script.getDisplayableAdditParams();
    }

    @Override
    public void saveCurrentRating(SessionDocument document, boolean silent)
    {
        final Map<EppStudentWorkPlanElement, SessionDocumentSlot> slotMap = new HashMap<>();
        final Map<ISessionBrsDao.ISessionRatingSettings, Map<EppStudentWorkPlanElement, Double>> studentSessionMarkDataMap = new HashMap<>();

        Map<ISessionBrsDao.ISessionRatingSettingsKey, ISessionBrsDao.ISessionRatingSettings> ratingSettings = ISessionBrsDao.instance.get().getRatingSettings(document);

        for (SessionDocumentSlot slot : getList(SessionDocumentSlot.class, SessionDocumentSlot.document(), document)) {
            ISessionBrsDao.ISessionRatingSettings settings = ratingSettings.get(ISessionBrsDao.instance.get().key(slot));
            SafeMap.safeGet(studentSessionMarkDataMap, settings, HashMap.class).put(slot.getStudentWpeCAction().getStudentWpe(), .0);
            slotMap.put(slot.getStudentWpeCAction().getStudentWpe(), slot);
        }
        
        for (Map.Entry<ISessionBrsDao.ISessionRatingSettings, Map<EppStudentWorkPlanElement, Double>> entry : studentSessionMarkDataMap.entrySet()) {
            ISessionBrsDao.ISessionRatingSettings settings = entry.getKey();
            Map<EppStudentWorkPlanElement, Double> studentSessionMarkData = entry.getValue();
            Map<EppStudentWorkPlanElement, IStudentCurrentRatingData> ratingCalcMap = calculateCurrentRating(settings, studentSessionMarkData);
            for (EppStudentWorkPlanElement slot : studentSessionMarkData.keySet()) {
                IStudentCurrentRatingData studentCurrentRatingData = ratingCalcMap.get(slot);

                if (null == studentCurrentRatingData) {
                    if (silent)
                        continue;
                    else
                        throw new ApplicationException("Заполнить текущий рейтинг в документе не удалось: не найден журнал для вычисления текущего рейтинга для студента " + slot.getStudent().getFio() + ".", true);
                }
                if (studentCurrentRatingData.getRatingValue().getValue() == null) {
                    if (silent)
                        continue;
                    else
                        throw new ApplicationException(studentCurrentRatingData.getRatingValue().getMessage() != null ? studentCurrentRatingData.getRatingValue().getMessage() : "Заполнить текущий рейтинг в документе не удалось: ошибка при вычислении текущего рейтинга для студента " + slot.getStudent().getFio() + ".", true);
                }

                ISessionBrsDao.IRatingValue currentRating = studentCurrentRatingData.getRatingValue();

                SessionDocumentSlot docSlot = slotMap.get(slot);
                SessionSlotRatingData ratingData = get(SessionSlotRatingData.class, SessionSlotRatingData.slot(), docSlot);
    
                if (null == ratingData) {
                    ratingData = new SessionSlotRatingData();
                    ratingData.setSlot(docSlot);
                }

                ratingData.setFixedCurrentRating(currentRating.getValue());
                ratingData.setAllowed(studentCurrentRatingData.isAllowed());

                saveOrUpdate(ratingData);
            }
        }
    }

    @Override
    public ISessionMarkDAO.MarkData calculateMarkData(final Date perfomDate, SessionDocumentSlot documentSlot, Double points)
    {
        EppStudentWorkPlanElement epvSlot = documentSlot.getStudentWpeCAction().getStudentWpe();

        ISessionBrsDao.ISessionRatingSettings ratingSettings = ISessionBrsDao.instance.get().getRatingSettings(ISessionBrsDao.instance.get().key(documentSlot));
        
        HashMap<EppStudentWorkPlanElement, ISessionBrsDao.IStudentCurrentRatingData> fixedCurrentRatingData = new HashMap<>();
        if (ratingSettings.useCurrentRating()) {
            SessionSlotRatingData ratingData = get(SessionSlotRatingData.class, SessionSlotRatingData.slot(), documentSlot);
            if (null == ratingData) {
                saveCurrentRating(documentSlot.getDocument(), false);
                getSession().flush();
                ratingData = get(SessionSlotRatingData.class, SessionSlotRatingData.slot(), documentSlot);
            }
            fixedCurrentRatingData.put(epvSlot, ratingData.getRatingData());
        }

        Map<EppStudentWorkPlanElement, Double> pointsMap = new HashMap<>();
        pointsMap.put(epvSlot, points);

        IBrsScriptDao.IBrsScriptWrapper<IBrsSessionScriptFunctions> sessionScript = IBrsScriptDao.instance.get().getSessionScript(epvSlot);
        Map<ISessionBrsDao.ISessionRatingSettings, List<EppStudentWorkPlanElement>> sessionRatingSettings = ImmutableMap.of(ratingSettings, Arrays.asList(epvSlot));
        ISessionMarkCalc sessionMarkCalc = getCalculatedSessionMark(sessionScript, sessionRatingSettings, pointsMap, fixedCurrentRatingData);
        final IStudentSessionMarkData studentSessionMark = sessionMarkCalc.getSessionMark(epvSlot);

        final ISessionBrsDao.IRatingValue ratingValue = studentSessionMark.getRatingValue();
        if (ratingValue.getMessage() != null && ratingValue.getValue() == null)
            throw new ApplicationException(ratingValue.getMessage());
        
        return new ISessionMarkDAO.MarkData()
        {
            @Override public Date getPerformDate() { return perfomDate; }
            @Override public Double getPoints() { return ratingValue.getValue(); }
            @Override public SessionMarkCatalogItem getMarkValue() { return studentSessionMark.getSessionMark(); }
            @Override public String getComment() { return ratingValue.getMessage(); }
        };
    }

    //------------------------------------------------------

    private Map<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> getPreparedMarkData(TrJournal journal, TrJournalGroup group)
    {
        HashMap<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> markMap = new HashMap<>();

        Collection<EppStudentWorkPlanElement> actualStudentList = group != null ? TrJournalManager.instance().dao().getStudentList(group) : TrJournalManager.instance().dao().getActualStudentList(journal);
        for (EppStudentWorkPlanElement slot : actualStudentList) {
            markMap.put(slot, new HashMap<>());
        }

        DQLSelectBuilder markDQL = new DQLSelectBuilder()
            .fromEntity(TrEduGroupEventStudent.class, "e").column("e")
            .where(eq(property(TrEduGroupEventStudent.event().journalEvent().journalModule().journal().fromAlias("e")), value(journal)))
            .where(isNull(property(TrEduGroupEventStudent.transferDate().fromAlias("e"))))
            .where(or(
                isNotNull(property(TrEduGroupEventStudent.absent().fromAlias("e"))),
                isNotNull(property(TrEduGroupEventStudent.gradeAsLong().fromAlias("e")))))
            .fetchPath(DQLJoinType.inner, TrEduGroupEventStudent.event().fromAlias("e"), "ge")
            .fetchPath(DQLJoinType.left, TrEduGroupEvent.scheduleEvent().fromAlias("ge"), "sche")
            .fetchPath(DQLJoinType.inner, TrEduGroupEvent.journalEvent().fromAlias("ge"), "je")
            .fetchPath(DQLJoinType.inner, TrJournalEvent.journalModule().fromAlias("je"), "m");

        List<TrEduGroupEventStudent> elList = markDQL.createStatement(getSession()).list();

        for (final TrEduGroupEventStudent el : elList) {

            EppStudentWorkPlanElement student = el.getStudentWpe();

            if (group != null && !markMap.keySet().contains(student)) {
                continue;
            }

            SafeMap.safeGet(markMap, student, HashMap.class).put(el.getEvent().getJournalEvent(), new ITrJournalEventDao.IJournalMarkData()
            {
                @Override public Boolean isAbsent() { return el.getAbsent(); }
                @Override public Double getGrade() { return el.getGrade(); }
                @Override public String getComment() { return el.getComment(); }
                @Override public Date getScheduledDate() { return el.getEvent().getScheduleEvent() == null ? null : el.getEvent().getScheduleEvent().getDurationBegin(); }
                @Override public Date getActualDate() { return null; }
            });

        }

        return markMap;
    }

    private ICurrentRatingCalc getCalculatedRating(TrJournal journal, IBrsPreparedSettings preparedBrsSettings)
    {

        IBrsJournalScriptFunctions script = IBrsScriptDao.instance.get().getJournalScript(journal).getScript();

//        System.out.println("finding script " + (new Date().getTime() - time)); time = new Date().getTime();

        HashMap<TrJournalModule, List<TrJournalEvent>> preparedJournalContent = getPreparedJournalContent(journal);

//        System.out.println("prep journal data " + (new Date().getTime() - time)); time = new Date().getTime();

        Map<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> preparedMarkData = getPreparedMarkData(journal, null);

//        System.out.println("prep mark data " + (new Date().getTime() - time)); time = new Date().getTime();

        ICurrentRatingCalc ratingCalc = script.calculateCurrentRating(journal, preparedJournalContent, preparedMarkData, preparedBrsSettings);

//        System.out.println("calculating rating " + (new Date().getTime() - time)); time = new Date().getTime();

        return ratingCalc;

    }

    private IJournalEventStatCalc getStatCalc(final TrJournal journal, final TrJournalGroup journalGroup, IBrsPreparedSettings settings)
    {
        IBrsJournalScriptFunctions script = IBrsScriptDao.instance.get().getJournalScript(journal).getScript();

        if (script instanceof IBrsJournalEventStatScriptFunctions) {
            IBrsJournalEventStatScriptFunctions statScript = (IBrsJournalEventStatScriptFunctions) script;

            HashMap<TrJournalModule, List<TrJournalEvent>> preparedJournalContent = getPreparedJournalContent(journal);

            Map<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> preparedMarkData = getPreparedMarkData(journal, journalGroup);

            IJournalEventStatCalc statCalc = statScript.calculateEventStat(journal, journalGroup, preparedJournalContent, preparedMarkData, settings);

            return statCalc;
        }

        return new IJournalEventStatCalc()
        {
            @Override public TrJournal getJournal() { return journal; }
            @Override public TrJournalGroup getJournalGroup() { return journalGroup; }
            @Override public Set<EppStudentWorkPlanElement> getIncludedStudents() { return Collections.emptySet(); }
            @Override public List<ISessionBrsDao.IRatingAdditParamDef> getStatParamDefinitions() { return Collections.emptyList(); }
            @Override public ISessionBrsDao.IRatingValue getStatValue(TrJournalEvent event, String statKey) { return null; }
            @Override public IBrsDao.IAggregatedRatingValue getStatValueAggregated(String statKey, String columnKey) { return BrsScriptUtils.getNotApplicableValue(); }
        };
    }

    private HashMap<TrJournalModule, List<TrJournalEvent>> getPreparedJournalContent(TrJournal journal)
    {
        HashMap<TrJournalModule, List<TrJournalEvent>> map = new HashMap<>();
        for (TrJournalEvent event : getList(TrJournalEvent.class, TrJournalEvent.journalModule().journal(), journal, TrJournalEvent.number().s()))
            SafeMap.safeGet(map, event.getJournalModule(), ArrayList.class).add(event);
        return map;
    }

    private IBrsPreparedSettings getPreparedBrsSettings(final TrJournal journal, final Student student)
    {
        if (student != null && student.getEducationOrgUnit().getGroupOrgUnit() == null)
            throw new ApplicationException("Для направления подготовки «" + student.getEducationOrgUnit().getTitleWithFormAndCondition() + "» не указан деканат.");

        MultiKey cacheKey = new MultiKey("preparedBrsSettings", journal.getId(), student == null ? null : student.getEducationOrgUnit().getGroupOrgUnit().getId());

        IBrsPreparedSettings settings = DaoCache.get(cacheKey);
        if (null != settings)
            return settings;

        // todo rethink it

        EppYearPart yearPart = journal.getYearPart();
        final TrOrgUnitSettings globalSettings = getByNaturalId(new TrOrgUnitSettings.NaturalId(TopOrgUnit.getInstance(), yearPart));
        final TrOrgUnitSettings ouSettings = getByNaturalId(new TrOrgUnitSettings.NaturalId(journal.getRegistryElementPart().getTutorOu(), yearPart));

        final Map<MultiKey, TrBrsCoefficientValue> map = new HashMap<>();

        IDQLExpression or = or(
            eq(property(TrBrsCoefficientValue.owner().fromAlias("v")), value(globalSettings)),
            eq(property(TrBrsCoefficientValue.owner().fromAlias("v")), value(ouSettings)),
            eq(property(TrBrsCoefficientValue.owner().fromAlias("v")), value(journal)),
            eq(property(TrBrsCoefficientValue.owner().fromAlias("v")), property(TrJournalEvent.journalModule().fromAlias("e"))),
            eq(property(TrBrsCoefficientValue.owner().fromAlias("v")), property("e"))
        );

        if (null != student) {
            OrgUnit groupOrgUnit = student.getEducationOrgUnit().getGroupOrgUnit();
            if (null != groupOrgUnit) {
                TrOrgUnitSettings groupOuSettings = getByNaturalId(new TrOrgUnitSettings.NaturalId(groupOrgUnit, yearPart));
                or = or(or, eq(property(TrBrsCoefficientValue.owner().fromAlias("v")), value(groupOuSettings)));
            }
        }

        DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(TrBrsCoefficientValue.class, "v")
        .column("v")
        .fromEntity(TrJournalEvent.class, "e")
        .where(eq(property(TrJournalEvent.journalModule().journal().fromAlias("e")), value(journal)))
        .where(or);
        for (TrBrsCoefficientValue value : dql.createStatement(getSession()).<TrBrsCoefficientValue>list()) {
            boolean localKey = value.getOwner() instanceof TrJournalModule || value.getOwner() instanceof TrJournalEvent;
            map.put(new MultiKey(localKey ? value.getOwner().getId() : null, value.getDefinition().getUserCode()), value);
        }

        settings = new IBrsPreparedSettings()
        {
            @Override
            public TrBrsCoefficientValue getSettings(String defCode)
            {
                return map.get(new MultiKey(null, defCode));
            }

            @Override
            public TrBrsCoefficientValue getModuleSettings(TrJournalModule module, String defCode)
            {
                return map.get(new MultiKey(module.getId(), defCode));
            }

            @Override
            public TrBrsCoefficientValue getEventSettings(TrJournalEvent event, String defCode)
            {
                return map.get(new MultiKey(event.getId(), defCode));
            }
        };

        DaoCache.put(cacheKey, settings);
        return settings;
    }
    
    private IBrsPreparedSettings getPreparedBrsSettings(final EppYearPart yearPart, final OrgUnit groupOu, final OrgUnit tutorOu)
    {
        MultiKey cacheKey = new MultiKey("preparedBrsSettings", yearPart == null ? null : yearPart.getId(), groupOu == null ? null : groupOu.getId());
        
        IBrsPreparedSettings settings = DaoCache.get(cacheKey);
        if (null != settings)
            return settings;
        
        // todo rethink it

        TopOrgUnit topOrgUnitProxy = (TopOrgUnit) getSession().load(TopOrgUnit.class, OrgUnitManager.instance().dao().getTopOrgUnitId());
        final TrOrgUnitSettings globalSettings = getByNaturalId(new TrOrgUnitSettings.NaturalId(topOrgUnitProxy, yearPart));
        final TrOrgUnitSettings groupOuSettings = getByNaturalId(new TrOrgUnitSettings.NaturalId(groupOu, yearPart));
        //final TrOrgUnitSettings tutorOuSettings = getByNaturalId(new TrOrgUnitSettings.NaturalId(tutorOu, year));

        final Map<MultiKey, TrBrsCoefficientValue> map = new HashMap<>();

        DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(TrBrsCoefficientValue.class, "v")
        .column("v")
        .where(or(
            eq(property(TrBrsCoefficientValue.owner().fromAlias("v")), value(globalSettings)),
            eq(property(TrBrsCoefficientValue.owner().fromAlias("v")), value(groupOuSettings))
        ));
        for (TrBrsCoefficientValue value : dql.createStatement(getSession()).<TrBrsCoefficientValue>list()) {
            boolean localKey = value.getOwner() instanceof TrJournalModule || value.getOwner() instanceof TrJournalEvent;
            map.put(new MultiKey(localKey ? value.getOwner().getId() : null, value.getDefinition().getUserCode()), value);
        }

        settings = new IBrsPreparedSettings()
        {
            @Override
            public TrBrsCoefficientValue getSettings(String defCode)
            {
                return map.get(new MultiKey(null, defCode));
            }

            @Override
            public TrBrsCoefficientValue getModuleSettings(TrJournalModule module, String defCode)
            {
                return null;
            }

            @Override
            public TrBrsCoefficientValue getEventSettings(TrJournalEvent event, String defCode)
            {
                return null;
            }
        };

        DaoCache.put(cacheKey, settings);
        return settings;
    }

    private Map<EppStudentWorkPlanElement, IStudentCurrentRatingData> calculateCurrentRating(ISessionBrsDao.ISessionRatingSettings sessionRatingSettings, Map<EppStudentWorkPlanElement, Double> studentSessionMarkData)
    {
        EppFControlActionGroup caGroup = sessionRatingSettings.key().getCaGroup();

        Map<EppStudentWorkPlanElement, Map<EppFControlActionGroup, TrJournal>> journalMap;
        try {
            journalMap = TrJournalManager.instance().dao().fillJournalMap(studentSessionMarkData.keySet(), sessionRatingSettings.key().getDiscipline());
        }
        catch (IllegalStateException e) {
            throw new ApplicationException(e.getMessage());
        }

        Set<TrJournal> journals = new HashSet<>();
        for (Map.Entry<EppStudentWorkPlanElement, Map<EppFControlActionGroup, TrJournal>> entry : journalMap.entrySet()) {
            journals.addAll(entry.getValue().values());
        }

        Map<TrJournal, IBrsPreparedSettings> brsSettingsMap = new HashMap<>();
        for (TrJournal journal : journals) {
            brsSettingsMap.put(journal, getPreparedBrsSettings(journal));
        }

        Map<EppStudentWorkPlanElement, IStudentCurrentRatingData> ratingCalcMap = new HashMap<>();
        for (TrJournal journal: journals) {
            ICurrentRatingCalc calculatedRating = getCalculatedRating(journal, brsSettingsMap.get(journal));
            for (Map.Entry<EppStudentWorkPlanElement, Map<EppFControlActionGroup, TrJournal>> studentEntry : journalMap.entrySet()) {
                for (Map.Entry<EppFControlActionGroup, TrJournal> caEntry : studentEntry.getValue().entrySet()) {
                    if (caGroup.equals(caEntry.getKey()) && journal.equals(caEntry.getValue())) {
                        EppStudentWorkPlanElement student = studentEntry.getKey();
                        ratingCalcMap.put(student, calculatedRating.getCurrentRating(student));
                    }
                }
            }
        }
        return ratingCalcMap;
    }

    @Override
    public IBrsPreparedSettings getPreparedBrsSettings(TrJournal journal) {
        return getPreparedBrsSettings(journal, null);
    }

    private static ISessionBrsDao.ISessionRatingSettings repack(final ISessionBrsDao.ISessionRatingSettings ratingSettings, final IBrsPreparedSettings brsSettings)
    {
        return new ISessionBrsDao.ISessionRatingSettings()
            {
            @Override public ISessionBrsDao.ISessionRatingSettingsKey key() { return ratingSettings.key(); }
            @Override public boolean usePoints() { return ratingSettings.usePoints(); }
            @Override public boolean useCurrentRating() { return ratingSettings.useCurrentRating(); }
            @Override public ISessionBrsDao.ISessionPointsValidator pointsValidator() { return ratingSettings.pointsValidator(); }
            @Override public ISessionBrsDao.IBrsCoefficientValue getCoefficientValue(String defCode) { return brsSettings.getSettings(defCode); }
            };
    }

    protected ISessionMarkCalc getCalculatedSessionMark(
        final IBrsScriptDao.IScriptWrapper<IBrsSessionScriptFunctions, TrOrgUnitSettings> script,
        final Map<ISessionBrsDao.ISessionRatingSettings, List<EppStudentWorkPlanElement>> sessionRatingSettingsMap,
        final Map<EppStudentWorkPlanElement, Double> studentSessionMarkData,
        final Map<EppStudentWorkPlanElement, ISessionBrsDao.IStudentCurrentRatingData> fixedCurrentRatingData)
    {
        // эти параметры должны быть одинаковы
        // todo подумать, как порефакторить, хотя метод и приватный
        ISessionBrsDao.ISessionRatingSettings sampleSettings = sessionRatingSettingsMap.keySet().iterator().next();
        final EppFControlActionGroup caGroup = sampleSettings.key().getCaGroup();
        final EppGradeScale gradeScale = sampleSettings.key().getScale();

        final Map<EppStudentWorkPlanElement, IStudentSessionMarkData> resultMarks = new HashMap<>();
        
        for (Map.Entry<ISessionBrsDao.ISessionRatingSettings, List<EppStudentWorkPlanElement>> entry : sessionRatingSettingsMap.entrySet()) {

            ISessionBrsDao.ISessionRatingSettings sessionRatingSettings = entry.getKey();
            List<EppStudentWorkPlanElement> students = entry.getValue();
            
            // берем фиксированное значение текущего рейтинга, если передали, иначе вычисляем
            // todo не надо бы передавать всех студентов, лучше изменить сигнатуру метода или деление на методы, ибо адъ
            Map<EppStudentWorkPlanElement, IStudentCurrentRatingData> currentRatingCalc = null;
            if (sessionRatingSettings.useCurrentRating() && fixedCurrentRatingData == null)
                currentRatingCalc = calculateCurrentRating(sessionRatingSettings, studentSessionMarkData);

            // журналы
            final Map<EppStudentWorkPlanElement, Map<EppFControlActionGroup, TrJournal>> journalMap = !sessionRatingSettings.useCurrentRating() ? null : TrJournalManager.instance().dao().fillJournalMap(students, sessionRatingSettings.key().getDiscipline());
            
            for (final EppStudentWorkPlanElement student : students) {
    
                Double pointsForSessionCA = studentSessionMarkData.get(student);
    
                ISessionBrsDao.ISessionRatingSettings settingsWithJournal = sessionRatingSettings;
                if (sessionRatingSettings.useCurrentRating()) {
                    TrJournal journal = journalMap == null || journalMap.get(student) == null ? null : journalMap.get(student).get(caGroup);
                    if (null == journal)
                        throw new ApplicationException("Сохранение оценки невозможно: не найден журнал для студента " + student.getStudent().getFio() + ".");
                    IBrsPreparedSettings preparedBrsSettings = getPreparedBrsSettings(journal, student.getStudent());
                    settingsWithJournal = repack(sessionRatingSettings, preparedBrsSettings);
                }

                ISessionBrsDao.IStudentCurrentRatingData studentRatingData = null;
                if (sessionRatingSettings.useCurrentRating()) {
                    // берем фиксированное значение текущего рейтинга, если передали, иначе вычисляем
                    if (fixedCurrentRatingData != null) {
                        studentRatingData = fixedCurrentRatingData.get(student);
                    }
                    else {
                        if (currentRatingCalc == null)
                            throw new IllegalStateException();
                        studentRatingData = currentRatingCalc.get(student);
                    }
                }

                resultMarks.put(student, script.getScript().calculateSessionMark(studentRatingData, pointsForSessionCA, settingsWithJournal));
            }
        }
        
        return new ISessionMarkCalc()
        {
            @Override public TrOrgUnitSettings getScriptOwner() { return script.getScriptOwner(); }
            @Override public EppGradeScale getGradeScale() { return gradeScale; }
            @Override public EppFControlActionGroup getCaGroup() { return caGroup; }
            @Override public IStudentSessionMarkData getSessionMark(EppStudentWorkPlanElement student) { return resultMarks.get(student); }
            @Override public RtfString getPrintableRatingFormula() { return script.getScript().getPrintableRatingFormula(gradeScale, caGroup); }
        };

    }
}
