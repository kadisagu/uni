/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util;

import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;

/**
 * @author oleyba
 * @since 11/27/12
 */
public class DefaultSessionPointsValidator implements ISessionBrsDao.ISessionPointsValidator
{
    private double min;
    private double max;
    private int precision;

    public DefaultSessionPointsValidator(double min, double max, int precision)
    {
        this.min = min;
        this.max = max;
        this.precision = precision;
    }

    @Override
    public double min()
    {
        return min;
    }

    @Override
    public double max()
    {
        return max;
    }

    @Override
    public int precision()
    {
        return precision;
    }
}
