/* $Id: DppOrgUnitTab.java 19031 2011-07-19 14:40:15Z iolshvang $ */
package ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.Tab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.EduGroupList.TrOrgUnitEduGroupList;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.JournalList.TrOrgUnitJournalList;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.TutorOuTab.TrBrsCoefficientTutorOuTab;

/**
 * @author vdanilov
 */
@Configuration
public class TrOrgUnitTab extends BusinessComponentManager
{
    // tab panel
    public static final String TAB_PANEL = "tabPanel";

    public static final String JOURNAL_TAB = "journalsTab";
    public static final String EDUGROUP_TAB = "eduGroupsTab";
    public static final String BRS_TAB = "brsTutorOuTab";

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return this.tabPanelExtPointBuilder(TrOrgUnitTab.TAB_PANEL)
        .addTab(componentTab(TrOrgUnitTab.JOURNAL_TAB, TrOrgUnitJournalList.class).permissionKey("ui:secModel.trJournalTabView").create())
        .addTab(componentTab(TrOrgUnitTab.EDUGROUP_TAB, TrOrgUnitEduGroupList.class).permissionKey("ui:secModel.trEduGroupTabView").create())
        .addTab(componentTab(TrOrgUnitTab.BRS_TAB, TrBrsCoefficientTutorOuTab.class).permissionKey("ui:secModel.trBrsTabView").create())
        .create();
    }
}