/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrEduGroup.ui.AbsenceReasonEdit;

import java.util.Collection;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.entity.catalog.TrAbsenceReason;

/**
 * @author oleyba
 * @since 9/23/11
 */
@Input({
    @Bind(key = TrEduGroupAbsenceReasonEditUI.BINDING_IDS, binding = "ids", required = true)
})
public class TrEduGroupAbsenceReasonEditUI extends UIPresenter
{
    public static final String BINDING_IDS = "ids";

    private Collection<Long> ids;
    private TrAbsenceReason reason;

    private ISelectModel reasonModel = new LazySimpleSelectModel<TrAbsenceReason>(TrAbsenceReason.class);

    public void onClickApply()
    {
        TrJournalManager.instance().eventDao().updateReason(getIds(), getReason());
        deactivate();
    }

    public Collection<Long> getIds()
    {
        return ids;
    }

    public void setIds(Collection<Long> ids)
    {
        this.ids = ids;
    }

    public TrAbsenceReason getReason()
    {
        return reason;
    }

    public void setReason(TrAbsenceReason reason)
    {
        this.reason = reason;
    }

    public ISelectModel getReasonModel()
    {
        return reasonModel;
    }
}
