package ru.tandemservice.unitraining.base.entity.event;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unitraining.base.entity.event.gen.TrEduGroupEventStudentGen;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

/**
 * Студент в событии УГС
 *
 * Создаются автоматически (на основе данных УГС) при определенных условиях
 * (за k дней до дедлайна по событию, например)
 */
public class TrEduGroupEventStudent extends TrEduGroupEventStudentGen implements ITitled
{
    public TrEduGroupEventStudent() {}

    public TrEduGroupEventStudent(TrEduGroupEvent event, EppStudentWorkPlanElement studentSlot) {
        setEvent(event);
        setStudentWpe(studentSlot);
    }

    @Override
    @EntityDSLSupport(parts = TrEduGroupEventStudent.P_GRADE_AS_LONG)
    public Double getGrade()
    {
        return getGradeAsLong() == null ? null : ((double) getGradeAsLong()) / 100;
    }

    public void setGrade(Double grade)
    {
        setGradeAsLong(null == grade ? null : Math.round(grade * 100));
    }

    @Override
    @EntityDSLSupport(parts = TrEduGroupEventStudent.P_ABSENT)
    public Boolean getPresent()
    {
        return null == getAbsent() ? null : !getAbsent();
    }

    @Override
    public String getTitle()
    {
        if (getJournal() == null) {
            return this.getClass().getSimpleName();
        }
        return "Запись в журнале для студента. Журнал: " + getJournal().getCalculatedTitle() + ". Студент: " + getStudentWpe().getStudent().getPerson().getFio() + "Событие: " + getEvent().getJournalEvent().getTitle();
    }

    public TrJournal getJournal()
    {
        return getEvent().getJournalEvent().getJournalModule().getJournal();
    }

    public boolean isHasMarkData() {
        return getAbsent() != null || getGradeAsLong() != null;
    }

}